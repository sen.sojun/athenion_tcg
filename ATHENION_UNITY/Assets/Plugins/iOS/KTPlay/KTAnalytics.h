//
//  KTAnalytics.h
//  Ktplay
//
//  Created by xingbin on 2017/2/24.
//  Copyright © 2017年 ktplay. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KTAnalytics : NSObject

/**
 * @brief Sets a user property to a given value
 * @param name  The name of the user property to set.The name are non-empty string objects with <= 64 characters.
 * @param value The value of the user property.The value can be booleans, integers, floats , string objects with <= 128 characters.
 * @discussion <i>Example usage:</i>
 * <pre><code>
 * [KTAnalytics setUserProperty:@"birth_year" value:@(1981)];
 * </code></pre>
 */
+(void)setUserProperty:(nonnull NSString *)name value:(nonnull id)value;

/**
 * @brief Adds an app specific event to event tracking log
 * @param name The name of the event. The name are non-empty string objects with <= 64 characters.
 * @param properties An dictionary of properties. Passing nil indicates that the event has no properties.<br/>
 * Property keys are non-empty string objects with <= 64 characters.<br/>
 * Property values can be booleans, integers, floats , string objects and with <= 128 characters.
 * @discussion <i>Example usage:</i><br/>
 * The following is an example for uploading data of a puschase event
 * <pre><code>
 * NSDictionary *properties = @{
 *                                  @"order_id":@"P4z37VjvmNjyEVvb",
 *                                  @"product_id":@"0001",
 *                                  @"currency":@"USD", //a ISO4217 currency code
 *                                  @"quantity":@(2),
 *                                  @"price":@(4)
 *
 *                               };
 * [KTAnalytics logEvent:@"purchase" properties:properties];
 * </code></pre>
 * The following is an example of uploading data of a item consumption event
 * <pre><code>
 * NSDictionary *properties = @{
 *                                  @"item_type":@"bomb",
 *                                  @"quantity":@"1"
 *                              };
 * [KTAnalytics logEvent:@"consume_item" properties:properties];
 * </code></pre>
 */
+(void)logEvent:(nonnull NSString *)name properties:(nullable NSDictionary *)properties;

@end
