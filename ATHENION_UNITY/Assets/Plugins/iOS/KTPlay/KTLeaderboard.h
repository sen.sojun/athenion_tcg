/// Copyright © 2016 ktplay. All rights reserved.
/// \file  KTLeaderboard


#import <Foundation/Foundation.h>

/**
 * KTLeaderboard
 */
@interface KTLeaderboard : NSObject

/**
 *  @brief  report leaderboard score
 *  @param leaderboardId leaderboard id
 *  @param score  score value
 *  @param scoreTag  score tag
 *  @param success success callback
 *  @param failure failure callback
 */
+(void)reportScore:(long long)score leaderboardId:(NSString *)leaderboardId scoreTag:(NSString*)scoreTag success:(void (^)(void))success failure:(void(^)(NSError * error))failure;

@end

