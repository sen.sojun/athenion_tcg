﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using Newtonsoft.Json;
using PlayFab;
using PlayFab.ClientModels;

namespace PlayfabAthenion
{
    public class CardDropTableEditor : EditorWindow
    {
        TextAsset cardDBCSV;

        string tableID = "";

        string series = "";
        bool isFoil;
        string rarity = "";

        string result;
        Vector2 ScrollPos;

        // Add menu named "My Window" to the Window menu
        [MenuItem("Window/PlayfabAthenion/DropTable/CardDropTableEditor")]
        static void Init()
        {
            // Get existing open window or if none, make a new one:
            CardDropTableEditor window = (CardDropTableEditor)EditorWindow.GetWindow(typeof(CardDropTableEditor));
            window.Show();
        }

        void OnGUI()
        {
            GUILayout.Label("Card DropTable Editor", EditorStyles.boldLabel);

            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("CardDB_CSV");
            cardDBCSV = EditorGUILayout.ObjectField(cardDBCSV, typeof(TextAsset), true) as TextAsset;
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("TableID");
            tableID = EditorGUILayout.TextField(tableID);
            GUILayout.EndHorizontal();

            GUILayout.Label("Filter", EditorStyles.boldLabel);

            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Series Key");
            series = EditorGUILayout.TextField(series);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("IsFoil");
            isFoil = EditorGUILayout.Toggle(isFoil);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Rarity");
            rarity = EditorGUILayout.TextField(rarity);
            GUILayout.EndHorizontal();

            if (GUILayout.Button("Review"))
            {
                result = "";
                if (cardDBCSV == null || rarity == "" || series == "")
                {
                    return;
                }
                result = GenerateResult();
            }

            ScrollPos = EditorGUILayout.BeginScrollView(ScrollPos);
            result = EditorGUILayout.TextArea(result);
            EditorGUILayout.EndScrollView();

        }

        private string GenerateResult()
        {

            List<List<string>> rawCSVData = new List<List<string>>();
            if (CSVReader.ReadCSVWithTitle(cardDBCSV, out rawCSVData) == false) return "";

            List<string> title = rawCSVData[0];
            int cardIDIndex = GetTitleIndex(title, "Card Name ID");
            int rarityIndex = GetTitleIndex(title, "Rarity");
            int seriesIndex = GetTitleIndex(title, "Series\nKey");
            int typeIndex = GetTitleIndex(title, "Type");

            DropTable result = new DropTable();
            result.TableId = this.tableID;

            for (int i = 1; i < rawCSVData.Count; i++)
            {
                string itemID = isFoil ? rawCSVData[i][cardIDIndex] + ":FOIL" : rawCSVData[i][cardIDIndex];
                string rarity = rawCSVData[i][rarityIndex];
                string series = rawCSVData[i][seriesIndex];
                string type = rawCSVData[i][typeIndex];

                if (series == this.series && rarity == this.rarity)
                {
                    if (type == "Minion_NotInDeck")
                    {
                        Debug.Log(itemID + " " + type);
                        continue;
                    }
                    result.Nodes.Add(new Node("ItemId", itemID, 1));
                }
            }

            return JsonConvert.SerializeObject(result, Formatting.Indented);

        }



        private string GenerateData(List<string> data)
        {
            string result = "";
            foreach (var text in data)
            {
                result += text + ",";
            }
            result.Remove(result.Length - 1);
            return result;
        }

        private int GetTitleIndex(List<string> titleRow, string title)
        {
            for (int i = 0; i < titleRow.Count; i++)
            {
                string t = titleRow[i].Replace("\r", "");
                if (t == title)
                {
                    return i;
                }
            }
            return -1;
            //return titleRow.FindIndex(text => text == title);
        }

        private class DropTable
        {
            public string TableId = "";
            public List<Node> Nodes = new List<Node>();
        }

        private class Node
        {
            public string ResultItemType;
            public string ResultItem;
            public int Weight;

            public Node(string itemType, string itemID, int weight)
            {
                ResultItemType = itemType;
                ResultItem = itemID;
                Weight = weight;
            }
        }
    }

}

