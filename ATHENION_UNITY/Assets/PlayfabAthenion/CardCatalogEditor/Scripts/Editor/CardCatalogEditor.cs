﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using Newtonsoft.Json;
using PlayFab;
using PlayFab.ClientModels;

namespace PlayfabAthenion
{
    public class CardCatalogEditor : EditorWindow
    {
        TextAsset playfabCatalog;
        TextAsset updateCatalog;
        string result;
        Vector2 ScrollPos;

        // Add menu named "My Window" to the Window menu
        [MenuItem("Window/PlayfabAthenion/CardCatalogEditor")]
        static void Init()
        {
            // Get existing open window or if none, make a new one:
            CardCatalogEditor window = (CardCatalogEditor)EditorWindow.GetWindow(typeof(CardCatalogEditor));
            window.Show();
        }

        void OnGUI()
        {
            GUILayout.Label("JSON File", EditorStyles.boldLabel);

            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("PlayfabCatalog");
            playfabCatalog = EditorGUILayout.ObjectField(playfabCatalog, typeof(TextAsset), true) as TextAsset;
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("CardCatalog");
            updateCatalog = EditorGUILayout.ObjectField(updateCatalog, typeof(TextAsset), true) as TextAsset;
            GUILayout.EndHorizontal();

            if (GUILayout.Button("Merge!"))
            {
                result = "";
                if (playfabCatalog == null || updateCatalog == null)
                {
                    return;
                }
                result = GenerateCombineResult(playfabCatalog, updateCatalog);
            }
            ScrollPos = EditorGUILayout.BeginScrollView(ScrollPos);
            result = EditorGUILayout.TextArea(result);
            EditorGUILayout.EndScrollView();

        }

        private string GenerateCombineResult(TextAsset playfabCatalogText, TextAsset updateCatalogText)
        {
            PlayfabCatalog playfabCatalog = JsonConvert.DeserializeObject<PlayfabCatalog>(playfabCatalogText.text);
            PlayfabCatalog updateCatalog = JsonConvert.DeserializeObject<PlayfabCatalog>(updateCatalogText.text);

            UpdateCardInCatalog(playfabCatalog, updateCatalog);

            return JsonConvert.SerializeObject(playfabCatalog, Formatting.Indented);
        }

        private void UpdateCardInCatalog(PlayfabCatalog playfabCatalog, PlayfabCatalog updateCatalog)
        {
            for (int i = 0; i < updateCatalog.Catalog.Count; i++)
            {
                if (IsCard(updateCatalog.Catalog[i].ItemClass))
                {
                    int itemIndex = playfabCatalog.Catalog.FindIndex(item => item.ItemId == updateCatalog.Catalog[i].ItemId);
                    bool isFound = itemIndex != -1;
                    if (isFound)
                    {
                        playfabCatalog.Catalog[itemIndex] = updateCatalog.Catalog[i];
                    }
                    else
                    {
                        playfabCatalog.Catalog.Add(updateCatalog.Catalog[i]);
                    }
                }
            }
        }

        public static bool IsCard(string itemClass)
        {
            string[] classStr = itemClass.Split(':');
            return classStr[0].ToLower() == "card";
        }
    }

    [Serializable]
    public class PlayfabCatalog
    {
        public string CatalogVersion = "";
        public List<CatalogItem> Catalog = new List<CatalogItem>();
        public object DropTables;

        public List<CatalogItem> GetCards()
        {
            List<CatalogItem> result = new List<CatalogItem>();
            foreach (CatalogItem item in Catalog)
            {
                if (CardCatalogEditor.IsCard(item.ItemClass))
                {
                    result.Add(item);
                }
            }
            return result;
        }
    }
}

