﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using Newtonsoft.Json;
using PlayFab;
using PlayFab.ClientModels;
using static CardDB;
using Karamucho;

namespace PlayfabAthenion
{
    public class CardStatChangeHTMLEditor : EditorWindow
    {
        TextAsset currentCardDBCSV;
        TextAsset oldCardDBCSV;

        TextAsset localizeCardNameCSV;
        TextAsset localizeAbilityCSV;
        TextAsset localizeOldAbilityCSV;

        bool isTH;
        string result;

        Vector2 ScrollPos;

        // Add menu named "My Window" to the Window menu
        [MenuItem("Window/PlayfabAthenion/CardStatChangeHTMLEditor")]
        static void Init()
        {
            // Get existing open window or if none, make a new one:
            CardStatChangeHTMLEditor window = (CardStatChangeHTMLEditor)EditorWindow.GetWindow(typeof(CardStatChangeHTMLEditor));
            window.Show();
        }

        void OnGUI()
        {
            GUILayout.Label("CardDB CSV File", EditorStyles.boldLabel);

            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("IsThai");
            isTH = EditorGUILayout.Toggle(isTH);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("CurrentCardDBCSV");
            currentCardDBCSV = EditorGUILayout.ObjectField(currentCardDBCSV, typeof(TextAsset), true) as TextAsset;
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("OldCardDBCSV");
            oldCardDBCSV = EditorGUILayout.ObjectField(oldCardDBCSV, typeof(TextAsset), true) as TextAsset;
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("CurrentLocalizeCardNameDBCSV");
            localizeCardNameCSV = EditorGUILayout.ObjectField(localizeCardNameCSV, typeof(TextAsset), true) as TextAsset;
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("CurrentLocalizeAbilityCardDBCSV");
            localizeAbilityCSV = EditorGUILayout.ObjectField(localizeAbilityCSV, typeof(TextAsset), true) as TextAsset;
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("OldLocalizeAbilityCardDBCSV");
            localizeOldAbilityCSV = EditorGUILayout.ObjectField(localizeOldAbilityCSV, typeof(TextAsset), true) as TextAsset;
            GUILayout.EndHorizontal();

            if (GUILayout.Button("Generate"))
            {
                result = "";
                if (currentCardDBCSV == null || oldCardDBCSV == null || localizeAbilityCSV == null || localizeOldAbilityCSV == null)
                {
                    return;
                }
                result = GenerateResult();
            }
            ScrollPos = EditorGUILayout.BeginScrollView(ScrollPos);
            result = EditorGUILayout.TextArea(result);
            EditorGUILayout.EndScrollView();

        }

        private string GenerateResult()
        {
            List<List<string>> currentCSVData = new List<List<string>>();
            List<List<string>> oldCSVData = new List<List<string>>();

            List<List<string>> localizeCardNameCSVData = new List<List<string>>();

            List<List<string>> currentAbilityCSVData = new List<List<string>>();
            List<List<string>> oldAbilityCSVData = new List<List<string>>();

            List<CardDBData> currentCardDBData = new List<CardDBData>();
            List<CardDBData> oldCardDBData = new List<CardDBData>();

            List<LocalizationDBData> localizeCardNameData = new List<LocalizationDBData>();
            List<LocalizationDBData> currentAbilityLocalizeData = new List<LocalizationDBData>();
            List<LocalizationDBData> oldAbilityLocalizeData = new List<LocalizationDBData>();


            if (CSVReader.ReadCSVWithTitle(currentCardDBCSV, out currentCSVData) == false) return "";
            if (CSVReader.ReadCSVWithTitle(oldCardDBCSV, out oldCSVData) == false) return "";

            if (CSVReader.ReadCSVWithTitle(localizeCardNameCSV, out localizeCardNameCSVData) == false) return "";
            if (CSVReader.ReadCSVWithTitle(localizeAbilityCSV, out currentAbilityCSVData) == false) return "";
            if (CSVReader.ReadCSVWithTitle(localizeOldAbilityCSV, out oldAbilityCSVData) == false) return "";

            // Data
            for (int i = 1; i < currentCSVData.Count; i++)
            {
                currentCardDBData.Add(CreateCardDBDataByCSV(currentCSVData[i]));
            }

            for (int i = 1; i < oldCSVData.Count; i++)
            {
                oldCardDBData.Add(CreateCardDBDataByCSV(oldCSVData[i]));
            }

            // Localize
            for (int i = 1; i < localizeCardNameCSVData.Count; i++)
            {
                localizeCardNameData.Add(CreateLocalizeDBDataByCSV(localizeCardNameCSVData[i]));
            }

            for (int i = 1; i < currentAbilityCSVData.Count; i++)
            {
                currentAbilityLocalizeData.Add(CreateLocalizeDBDataByCSV(currentAbilityCSVData[i]));
            }

            for (int i = 1; i < oldAbilityCSVData.Count; i++)
            {
                oldAbilityLocalizeData.Add(CreateLocalizeDBDataByCSV(oldAbilityCSVData[i]));
            }

            List<StatChangeResult> resultList = new List<StatChangeResult>();
            for (int i = 0; i < oldCardDBData.Count; i++)
            {
                string cardID = oldCardDBData[i].ID.Replace("OLD", "");
                CardDBData currentData = currentCardDBData.Find(data => data.ID == cardID);
                string localizeKey = $"ABILITY_DESCRIPTION_{cardID}";
                int nameIndex = isTH ? 1 : 0;
                currentData.Name = localizeCardNameData.Find((data) => data.Key == "CARD_NAME_" + cardID).TextList[nameIndex];
                LocalizationDBData localizeAbilityData = currentAbilityLocalizeData.Find((data) => data.Key == localizeKey);
                LocalizationDBData localizeOldAbilityData = oldAbilityLocalizeData.Find((data) => data.Key == localizeKey + "OLD");
                resultList.Add(new StatChangeResult(isTH, currentData, oldCardDBData[i], localizeAbilityData, localizeOldAbilityData));
            }

            string result = "";
            for (int i = 0; i < resultList.Count; i++)
            {
                result += resultList[i].GenerateHTMLResult();
            }
            return result;
        }

        private CardDBData CreateCardDBDataByCSV(List<string> row)
        {
            int _hp = int.Parse(row[(int)CardDBIndex.HP]);
            int _atk = int.Parse(row[(int)CardDBIndex.ATK]);
            int _spirit = int.Parse(row[(int)CardDBIndex.Spirit]);
            int _armor = int.Parse(row[(int)CardDBIndex.Armor]);


            List<string> elementList = new List<string>(row[(int)CardDBIndex.ElementList].Split(','));
            List<string> passiveList = new List<string>(row[(int)CardDBIndex.PassiveList].Split(','));
            List<string> feedbackList = new List<string>(row[(int)CardDBIndex.FeedbackList].Split(','));
            List<string> cardDir = new List<string>(row[(int)CardDBIndex.CardDir].Split(','));
            List<string> abilityIDList = new List<string>(row[(int)CardDBIndex.AbilityIDList].Split(','));


            CardDBData data = new CardDBData(
                  row[(int)CardDBIndex.CardID]
                , row[(int)CardDBIndex.Name]
                , row[(int)CardDBIndex.FlavorText]
                , row[(int)CardDBIndex.ImageKey]
                , elementList
                , row[(int)CardDBIndex.SeriesKey]
                , passiveList
                , feedbackList
                , row[(int)CardDBIndex.Rarity]
                , row[(int)CardDBIndex.Type]
                , _hp
                , _atk
                , _spirit
                , _armor
                , cardDir
                , abilityIDList
                , row[(int)CardDBIndex.SpawnEffectKey]
                , row[(int)CardDBIndex.AttackEffectKey]
                , row[(int)CardDBIndex.DeathEffectKey]
                , row[(int)CardDBIndex.InfoScriptKey]
            );
            return data;
        }

        private LocalizationDBData CreateLocalizeDBDataByCSV(List<string> row)
        {
            return LocalizationDB.CreateLocalizationDBDataFromCSVByRow(row);
        }
    }

    public class StatChangeResult
    {
        public bool isThai;

        public CardDBData newData;
        public CardDBData oldData;
        public LocalizationDBData newAbility;
        public LocalizationDBData oldAbility;

        public StatChangeResult(bool isThai, CardDBData newData, CardDBData oldData, LocalizationDBData newAbility, LocalizationDBData oldAbility)
        {
            this.isThai = isThai;

            this.newData = newData;
            this.oldData = oldData;
            this.newAbility = newAbility;
            this.oldAbility = oldAbility;
        }

        public string GenerateHTMLResult()
        {
            string header = isThai == false ? $"<div class=\"box\">\n    <div class=\"header\">{newData.Name}</div>\n    <div class=\"left\"><img src=\"tokenimg/ART_TOKEN_{newData.ImageKey}.png\"></div>\n    <div class=\"right\">\n        <table><tr>\n<th class=\"none\"></th>\n<th class=\"old\">OLD</th>\n<th class=\"new\">NEW</th>\n</tr>" :
                 $"<div class=\"box\">\n    <div class=\"header\">{newData.Name}</div>\n    <div class=\"left\"><img src=\"tokenimg/ART_TOKEN_{newData.ImageKey}.png\"></div>\n    <div class=\"right\">\n        <table><tr>\n<th class=\"none\"></th>\n<th class=\"old\">เก่า</th>\n<th class=\"new\">ใหม่</th>\n</tr>";
            string attackChangeTxt = GenerateHTMLATKResult();
            string hpChangeTxt = GenerateHTMLHPResult();
            string armorChangeTxt = GenerateHTMLArmorResult();
            string directionChangeTxt = GenerateHTMLDirectionResult();
            string soulChangeTxt = GenerateHTMLSoulResult();
            string abilityChangeTxt = GenerateHTMLAbilityResult();
            string footer = " </table>\n    </div>\n    <div class=\"clear\"></div>\n</div>";

            return header + attackChangeTxt + hpChangeTxt + armorChangeTxt + directionChangeTxt + soulChangeTxt + abilityChangeTxt + footer;
        }

        private string GenerateHTMLATKResult()
        {
            if (newData.ATK == oldData.ATK)
                return "";
            else
            {
                return isThai == false ? $"<tr>\n<td>Change ATK from </td>\n<td class=\"old\">{oldData.ATK}</td>\n<td class=\"new\">{newData.ATK}</td>\n</tr>" :
                    $"<tr>\n<td>เปลี่ยน โจมตี จาก</td>\n<td class=\"old\">{oldData.ATK}</td>\n<td class=\"new\">{newData.ATK}</td>\n</tr>";
            }
        }

        private string GenerateHTMLArmorResult()
        {
            if (newData.Armor == oldData.Armor)
                return "";
            else
            {
                return isThai == false ? $"<tr>\n<td>Change Armor from </td>\n<td class=\"old\">{oldData.Armor}</td>\n<td class=\"new\">{newData.Armor}</td>\n</tr>" :
                    $"<tr>\n<td>เปลี่ยน เกราะ จาก</td>\n<td class=\"old\">{oldData.Armor}</td>\n<td class=\"new\">{newData.Armor}</td>\n</tr>";
            }
        }

        private string GenerateHTMLHPResult()
        {
            if (newData.HP == oldData.HP)
                return "";
            else
            {
                return isThai == false ? $"<tr>\n<td>Change HP from </td>\n<td class=\"old\">{oldData.HP}</td>\n<td class=\"new\">{newData.HP}</td>\n</tr>" :
                    $"<tr>\n<td>เปลี่ยน เลือด จาก</td>\n<td class=\"old\">{oldData.HP}</td>\n<td class=\"new\">{newData.HP}</td>\n</tr>";
            }
        }

        private string GenerateHTMLAbilityResult()
        {
            string newAbilityTxt = isThai == false ? newAbility.TextList[0] : newAbility.TextList[1];
            string oldAbilityTxt = isThai == false ? oldAbility.TextList[0] : oldAbility.TextList[1];
            if (newAbilityTxt == oldAbilityTxt)
                return "";
            else
            {
                string result = isThai == false ? $"<tr>\n<td>Change Ability from </td>\n<td class=\"old\">{oldAbilityTxt}</td>\n<td class=\"new\">{newAbilityTxt}</td>\n</tr>" :
                     $"<tr>\n<td>เปลี่ยน ความสามารถ จาก </td>\n<td class=\"old\">{oldAbilityTxt}</td>\n<td class=\"new\">{newAbilityTxt}</td>\n</tr>";
                result = result.Replace("<sprite name=\"S_ATK\">", "ATK");
                result = result.Replace("<sprite name=\"S_HP\">", "HP");
                result = result.Replace("<sprite name=\"S_ARMOR\">", "ARMOR");
                //result = result.Replace("<sprite name=\"S_SOUL\">", "SOUL");
                return result;
            }
        }

        private string GenerateHTMLSoulResult()
        {
            if (newData.Spirit == oldData.Spirit)
                return "";
            else
            {
                return isThai == false ? $"<tr>\n<td>Change Soul from </td>\n<td class=\"old\">{oldData.Spirit}</td>\n<td class=\"new\">{newData.Spirit}</td>\n</tr>" :
                    $"<tr>\n<td>เปลี่ยน โซล จาก</td>\n<td class=\"old\">{oldData.Spirit}</td>\n<td class=\"new\">{newData.Spirit}</td>\n</tr>";
            }
        }

        private string GenerateHTMLDirectionResult()
        {
            string newDataDir = CardDirectionText(newData);
            string oldDataDir = CardDirectionText(oldData);
            if (newDataDir == oldDataDir)
                return "";
            else
            {
                return isThai == false ? $"<tr>\n<td>Change Direction from </td>\n<td class=\"old\">{oldDataDir}</td>\n<td class=\"new\">{newDataDir}</td>\n</tr>" :
                    $"<tr>\n<td>เปลี่ยน ทิศทาง จาก</td>\n<td class=\"old\">{oldDataDir}</td>\n<td class=\"new\">{newDataDir}</td>\n</tr>";
            }
        }

        private string CardDirectionText(CardDBData cardDBData)
        {
            string txt = "";
            for (int i = 0; i < cardDBData.CardDir.Count; i++)
            {
                txt += i == cardDBData.CardDir.Count - 1 ? cardDBData.CardDir[i] : cardDBData.CardDir[i] + ",";
            }
            return txt;
        }

    }

}

