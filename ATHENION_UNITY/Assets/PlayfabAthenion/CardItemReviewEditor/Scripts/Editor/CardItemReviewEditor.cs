﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using Newtonsoft.Json;
using PlayFab;
using PlayFab.ClientModels;

namespace PlayfabAthenion
{
    public class CardItemReviewEditor : EditorWindow
    {
        TextAsset cardDBCsv;
        TextAsset CTCardJson;

        string result;
        Vector2 ScrollPos;

        // Add menu named "My Window" to the Window menu
        [MenuItem("Window/PlayfabAthenion/CardItemReviewEditor")]
        static void Init()
        {
            // Get existing open window or if none, make a new one:
            CardItemReviewEditor window = (CardItemReviewEditor)EditorWindow.GetWindow(typeof(CardItemReviewEditor));
            window.Show();
        }

        void OnGUI()
        {
            GUILayout.Label("CardDB CSV File", EditorStyles.boldLabel);

            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("CardDB.csv");
            cardDBCsv = EditorGUILayout.ObjectField(cardDBCsv, typeof(TextAsset), true) as TextAsset;
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("CT_Card.json");
            CTCardJson = EditorGUILayout.ObjectField(CTCardJson, typeof(TextAsset), true) as TextAsset;
            GUILayout.EndHorizontal();

            if (GUILayout.Button("Review"))
            {
                result = "";
                if (cardDBCsv == null || CTCardJson == null)
                {
                    return;
                }
                result = GenerateResult();
            }
            ScrollPos = EditorGUILayout.BeginScrollView(ScrollPos);
            result = EditorGUILayout.TextArea(result);
            EditorGUILayout.EndScrollView();

        }

        private string GenerateResult()
        {
            PlayfabCatalog cards = JsonConvert.DeserializeObject<PlayfabCatalog>(CTCardJson.text);

            List<List<string>> cardDB = new List<List<string>>();
            if (CSVReader.ReadCSVWithTitle(cardDBCsv, out cardDB) == false) return "";

            List<string> title = cardDB[0];
            int cardIDIndex = GetTitleIndex(title, "Card Name ID");
            int typeIndex = GetTitleIndex(title, "Type");
            int seriesIndex = GetTitleIndex(title, "Series\nKey");

            ResultData result = new ResultData();

            for (int i = 1; i < cardDB.Count; i++)
            {
                string cardID = cardDB[i][cardIDIndex];
                string type = cardDB[i][typeIndex];
                string seriesKey = cardDB[i][seriesIndex];

                if (type == "Minion_NotInDeck") continue;
                if (seriesKey.ToLower().Contains("none")) continue;
                if (cardID.ToLower().Contains("ch")) continue;


                var card = cards.Catalog.Find(item => item.ItemId == cardID);

                if (card == null)
                {
                    if (result.notFoundData.ContainsKey(seriesKey) == false)
                        result.notFoundData.Add(seriesKey, new List<string>());
                    result.notFoundData[seriesKey].Add(GenerateData(cardDB[i]));
                }
                else
                {

                }
            }

            for (int i = 0; i < cards.Catalog.Count; i++)
            {
                string cardID = cards.Catalog[i].ItemId;
                List<string> data = cardDB.Find(list => list[cardIDIndex] == cardID);
                if (data == null || data.Count == 0)
                {
                    result.originalNotContainData.Add(cardID + ":" + cards.Catalog[i].DisplayName);
                }
            }


            return JsonConvert.SerializeObject(result, Formatting.Indented);

        }



        private string GenerateData(List<string> data)
        {
            string result = "";
            foreach (var text in data)
            {
                result += text + ",";
            }
            result.Remove(result.Length - 1);
            return result;
        }

        private int GetTitleIndex(List<string> titleRow, string title)
        {
            for (int i = 0; i < titleRow.Count; i++)
            {
                string t = titleRow[i].Replace("\r", "");
                if (t == title)
                {
                    return i;
                }
            }
            return -1;
            //return titleRow.FindIndex(text => text == title);
        }

        private class ResultData
        {
            public List<string> originalNotContainData = new List<string>();
            public Dictionary<string, List<string>> notFoundData = new Dictionary<string, List<string>>();
        }
    }

}

