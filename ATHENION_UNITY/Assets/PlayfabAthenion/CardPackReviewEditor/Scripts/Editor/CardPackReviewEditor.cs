﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using Newtonsoft.Json;
using PlayFab;
using PlayFab.ClientModels;

namespace PlayfabAthenion
{
    public class CardPackReviewEditor : EditorWindow
    {
        TextAsset cardDBCSV;

        string series = "";
        //string rarity = "";

        string commonRate = "";
        string rareRate = "";
        string epicRate = "";
        string legendRate = "";

        string result;
        Vector2 ScrollPos;

        // Add menu named "My Window" to the Window menu
        [MenuItem("Window/PlayfabAthenion/DropTable/CardPackReviewEditor")]
        static void Init()
        {
            // Get existing open window or if none, make a new one:
            CardPackReviewEditor window = (CardPackReviewEditor)EditorWindow.GetWindow(typeof(CardPackReviewEditor));
            window.Show();
        }

        void OnGUI()
        {
            GUILayout.Label("Card DropTable Editor", EditorStyles.boldLabel);

            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("CardDB_CSV");
            cardDBCSV = EditorGUILayout.ObjectField(cardDBCSV, typeof(TextAsset), true) as TextAsset;
            GUILayout.EndHorizontal();

            GUILayout.Label("Filter", EditorStyles.boldLabel);

            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Series Key");
            series = EditorGUILayout.TextField(series);
            GUILayout.EndHorizontal();

            AddTextFieldGUILayout("CommonRate", ref commonRate);
            AddTextFieldGUILayout("RareRate", ref rareRate);
            AddTextFieldGUILayout("EpicRate", ref epicRate);
            AddTextFieldGUILayout("LegendRate", ref legendRate);

            if (GUILayout.Button("Review"))
            {
                result = "";
                if (cardDBCSV == null || series == "" || commonRate == "" || rareRate == "" || epicRate == "" || legendRate == "")
                {
                    return;
                }
                result = GenerateResult();
            }

            ScrollPos = EditorGUILayout.BeginScrollView(ScrollPos);
            result = EditorGUILayout.TextArea(result);
            EditorGUILayout.EndScrollView();

        }

        private void AddTextFieldGUILayout(string key, ref string param)
        {
            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(key);
            param = EditorGUILayout.TextField(param);
            GUILayout.EndHorizontal();
        }

        private string GenerateResult()
        {
            Dictionary<string, string> CardList = new Dictionary<string, string>();
            List<List<string>> rawCSVData = new List<List<string>>();
            if (CSVReader.ReadCSVWithTitle(cardDBCSV, out rawCSVData) == false) return "";

            List<string> title = rawCSVData[0];
            int cardIDIndex = GetTitleIndex(title, "Card Name ID");
            int rarityIndex = GetTitleIndex(title, "Rarity");
            int seriesIndex = GetTitleIndex(title, "Series\nKey");
            int typeIndex = GetTitleIndex(title, "Type");


            for (int i = 1; i < rawCSVData.Count; i++)
            {
                string itemID = rawCSVData[i][cardIDIndex];
                string rarity = rawCSVData[i][rarityIndex];
                string series = rawCSVData[i][seriesIndex];
                string type = rawCSVData[i][typeIndex];

                if (series == this.series)
                {
                    if (type == "Minion_NotInDeck")
                    {
                        Debug.Log(itemID + " " + type);
                        continue;
                    }
                    string rate = GetRarityRate(rarity);
                    CardList.Add(itemID, rate);
                }
            }

            return JsonConvert.SerializeObject(CardList, Formatting.Indented);

        }

        private string GetRarityRate(string rarity)
        {
            switch (rarity)
            {
                case "Common":
                    return commonRate;
                case "Rare":
                    return rareRate;
                case "Epic":
                    return epicRate;
                case "Legend":
                    return legendRate;
                default:
                    return "";
            }
        }

        private string GenerateData(List<string> data)
        {
            string result = "";
            foreach (var text in data)
            {
                result += text + ",";
            }
            result.Remove(result.Length - 1);
            return result;
        }

        private int GetTitleIndex(List<string> titleRow, string title)
        {
            for (int i = 0; i < titleRow.Count; i++)
            {
                string t = titleRow[i].Replace("\r", "");
                if (t == title)
                {
                    return i;
                }
            }
            return -1;
            //return titleRow.FindIndex(text => text == title);
        }

    }

}

