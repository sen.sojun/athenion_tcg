﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using Newtonsoft.Json;
using PlayFab;
using PlayFab.ClientModels;

namespace PlayfabAthenion
{
    public class CardDBReviewEditor : EditorWindow
    {
        TextAsset _devCSV;
        TextAsset _designerCSV;

        string result;
        string filter;
        Vector2 ScrollPos;

        // Add menu named "My Window" to the Window menu
        [MenuItem("Window/PlayfabAthenion/CardDBReviewEditor")]
        static void Init()
        {
            // Get existing open window or if none, make a new one:
            CardDBReviewEditor window = (CardDBReviewEditor)EditorWindow.GetWindow(typeof(CardDBReviewEditor));
            window.Show();
        }

        void OnGUI()
        {
            GUILayout.Label("CardDB CSV File", EditorStyles.boldLabel);

            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Dev_CSV");
            _devCSV = EditorGUILayout.ObjectField(_devCSV, typeof(TextAsset), true) as TextAsset;
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Designer_CSV");
            _designerCSV = EditorGUILayout.ObjectField(_designerCSV, typeof(TextAsset), true) as TextAsset;
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Remove Title");
            filter = EditorGUILayout.TextField(filter);
            GUILayout.EndHorizontal();


            if (GUILayout.Button("Review"))
            {
                result = "";
                if (_devCSV == null || _designerCSV == null)
                {
                    return;
                }
                result = GenerateResult();
            }
            ScrollPos = EditorGUILayout.BeginScrollView(ScrollPos);
            result = EditorGUILayout.TextArea(result);
            EditorGUILayout.EndScrollView();

        }

        private string GenerateResult()
        {
            List<List<string>> devCSVData = new List<List<string>>();
            List<List<string>> designCSVData = new List<List<string>>();

            if (CSVReader.ReadCSVWithTitle(_devCSV, out devCSVData) == false) return "";
            if (CSVReader.ReadCSVWithTitle(_designerCSV, out designCSVData) == false) return "";

            Dictionary<string, MatchKey> filterTitleDict = GenerateFilterTitleDict(devCSVData, designCSVData);

            if (filter != null && filter != "")
            {
                RemoveFilterKey(filterTitleDict, filter.Split(','));
            }

            ReviewResult result = new ReviewResult();

            string key = "Card Name ID";
            int devCardIDKey = filterTitleDict[key].devTitleIndex;
            int designCardIDKey = filterTitleDict[key].designerIndex;

            for (int i = 1; i < designCSVData.Count; i++)
            {
                string cardID = designCSVData[i][designCardIDKey];
                List<string> data = devCSVData.Find(list => list[devCardIDKey] == cardID);
                //notFound Data
                if (data == null)
                {
                    result.notFoundData.Add(GenerateData(designCSVData[i]));
                }
                //update data
                else
                {
                    UpdateData updateData;
                    if (IsUpdate(filterTitleDict, data, designCSVData[i], out updateData))
                    {
                        result.updateData.Add(updateData);
                    }
                }
            }

            for (int i = 0; i < devCSVData.Count; i++)
            {
                string imageKey = devCSVData[i][devCardIDKey];

                List<string> data = designCSVData.Find(list => list[designCardIDKey] == imageKey);
                //original not contain Data
                if (data == null)
                {
                    result.originalNotContain.Add(GenerateData(devCSVData[i]));
                }
            }

            return JsonConvert.SerializeObject(result, Formatting.Indented);

        }

        private void RemoveFilterKey(Dictionary<string, MatchKey> filterTitleDict, params string[] key)
        {
            for (int i = 0; i < key.Length; i++)
            {
                string realKey = key[i].Replace("\\n", "\n");
                if (filterTitleDict.ContainsKey(realKey))
                    filterTitleDict.Remove(realKey);
            }
        }

        private bool IsUpdate(Dictionary<string, MatchKey> filterTitleDict, List<string> devData, List<string> designData, out UpdateData updateData)
        {
            updateData = new UpdateData();
            bool isUpdate = false;
            foreach (var titleData in filterTitleDict)
            {
                string currentData = devData[titleData.Value.devTitleIndex].Replace(" ", string.Empty).Replace("\n", string.Empty);
                string newData = ParseData(titleData.Key, designData[titleData.Value.designerIndex]).Replace(" ", string.Empty).Replace("\n", string.Empty);

                if (currentData.ToLower() != newData.ToLower())
                {
                    DeltaAttribute delta = new DeltaAttribute() { TitleKey = titleData.Key, CurrentData = devData[titleData.Value.devTitleIndex], NewData = designData[titleData.Value.designerIndex] };
                    updateData.title.Add(delta);
                    isUpdate = true;
                }
            }

            updateData.rawData = GenerateData(designData);
            return isUpdate;
        }

        private string ParseData(string key, string value)
        {
            if (key == "Type")
            {
                value = value == "Unit" ? "Minion" : value;
            }
            else if (key == "Rarity")
            {
                value = value == "None" ? "Common" : value;
                value = value == "Legendary" ? "Legend" : value;
            }
            else if (key == "Direction")
            {
                value = value.Replace("[", "");
                value = value.Replace("]", "");
                value = value == "" ? "-" : value;
            }
            return value;
        }

        private string GenerateData(List<string> data)
        {
            string result = "";
            foreach (var text in data)
            {
                result += text + ",";
            }
            result.Remove(result.Length - 1);
            return result;
        }

        private Dictionary<string, MatchKey> GenerateFilterTitleDict(List<List<string>> devCSVData, List<List<string>> designCSVData)
        {
            Dictionary<string, MatchKey> filterTitleDict = new Dictionary<string, MatchKey>();
            List<string> filterTitle = RemoveEnter(designCSVData[0]);
            List<string> devTitle = RemoveEnter(devCSVData[0]);
            for (int i = 0; i < filterTitle.Count; i++)
            {
                string title = filterTitle[i];
                int devTitleIndex = GetTitleIndex(devTitle, title);
                if (devTitleIndex == -1)
                {
                    continue;
                }
                filterTitleDict.Add(title, new MatchKey() { key = title, devTitleIndex = devTitleIndex, designerIndex = i });
            }

            return filterTitleDict;
        }

        private List<string> RemoveEnter(List<string> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                list[i] = list[i].Replace("\r\n", "\n");
            }
            return list;
        }

        private int GetTitleIndex(List<string> titleRow, string title)
        {
            return titleRow.FindIndex(text => text == title);
        }

    }

    public class ReviewResult
    {
        public List<string> notFoundData = new List<string>();
        public List<string> originalNotContain = new List<string>();
        public List<UpdateData> updateData = new List<UpdateData>();
    }

    public struct MatchKey
    {
        public string key;
        public int devTitleIndex;
        public int designerIndex;
    }

    public struct DeltaAttribute
    {
        public string TitleKey;
        public string CurrentData;
        public string NewData;
    }

    public class UpdateData
    {
        public List<DeltaAttribute> title = new List<DeltaAttribute>();
        public string rawData;
    }

}

