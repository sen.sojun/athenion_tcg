﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

#region Unity Event Class
[System.Serializable]
public class PhotonFailedEvent : UnityEvent<short, string>
{
}

[System.Serializable]
public class DisconnectedEvent : UnityEvent<DisconnectCause>
{
}

[System.Serializable]
public class PhotonPlayerEvent : UnityEvent<Player>
{
}

[System.Serializable]
public class RegionHandlerEvent : UnityEvent<RegionHandler>
{
}

[System.Serializable]
public class RoomInfoListEvent : UnityEvent<List<RoomInfo>>
{
}

[System.Serializable]
public class PhotonHashtableEvent : UnityEvent<ExitGames.Client.Photon.Hashtable>
{
}

[System.Serializable]
public class PhotonPlayerHashtableEvent : UnityEvent<Player, ExitGames.Client.Photon.Hashtable>
{
}

[System.Serializable]
public class FriendInfoListEvent : UnityEvent<List<FriendInfo>>
{
}

[System.Serializable]
public class CustomAuthResponseEvent : UnityEvent<Dictionary<string, object>>
{
}
#endregion

public class PhotonEventCapture : MonoBehaviourPunCallbacks
{
    #region Private Properties
    private UnityEvent _connectEvent = null;
    private DisconnectedEvent _disconnectEvent = null;
    private UnityEvent _connectedToMasterEvent = null;
    private UnityEvent _joinedLobbyEvent = null;
    private UnityEvent _leftLobbyEvent = null;
    private UnityEvent _createdRoomEvent = null;
    private PhotonFailedEvent _createRoomFailedEvent = null;
    private PhotonFailedEvent _joinRoomFailedEvent = null;
    private PhotonFailedEvent _joinRandomFailedEvent = null;
    private UnityEvent _joinedRoomEvent = null;
    private UnityEvent _leftRoomEvent = null;
    private PhotonPlayerEvent _playerEnteredRoomEvent = null;
    private PhotonPlayerEvent _playerLeftRoomEvent = null;
    private PhotonPlayerEvent _masterClientSwitchedEvent = null;
    private RegionHandlerEvent _regionListReceivedEvent = null;
    private RoomInfoListEvent _roomListUpdateEvent = null;
    private PhotonHashtableEvent _roomPropertiesUpdateEvent = null;
    private PhotonPlayerHashtableEvent _playerPropertiesUpdateEvent = null;
    private FriendInfoListEvent _friendListUpdateEvent = null;
    private CustomAuthResponseEvent _customAuthResponseEvent = null;
    #endregion

    #region Connected
    public void SubscribeConnectedEvent(UnityAction callback)
    {
        if (_connectEvent == null)
        {
            _connectEvent = new UnityEvent();
        }

        _connectEvent.AddListener(callback);
    }

    public void UnSubscribeConnectedEvent(UnityAction callback)
    {
        if (_connectEvent != null)
        {
            _connectEvent.RemoveListener(callback);
        }
    }

    public void ClearSubscribeConnectedEvent()
    {
        if (_connectEvent != null)
        {
            _connectEvent.RemoveAllListeners();
        }
    }
    #endregion

    #region Disconnected
    public void SubscribeDisconnectedEvent(UnityAction<DisconnectCause> callback)
    {
        if (_disconnectEvent == null)
        {
            _disconnectEvent = new DisconnectedEvent();
        }

        _disconnectEvent.AddListener(callback);
    }

    public void UnSubscribeDisconnectedEvent(UnityAction<DisconnectCause> callback)
    {
        if (_disconnectEvent != null)
        {
            _disconnectEvent.RemoveListener(callback);
        }
    }

    public void ClearSubscribeDisconnectedEvent()
    {
        if (_disconnectEvent != null)
        {
            _disconnectEvent.RemoveAllListeners();
        }
    }
    #endregion

    #region Connected To Master
    public void SubscribeConnectedToMasterEvent(UnityAction callback)
    {
        if (_connectedToMasterEvent == null)
        {
            _connectedToMasterEvent = new UnityEvent();
        }

        _connectedToMasterEvent.AddListener(callback);
    }

    public void UnSubscribeConnectedToMasterEvent(UnityAction callback)
    {
        if (_connectedToMasterEvent != null)
        {
            _connectedToMasterEvent.RemoveListener(callback);
        }
    }

    public void ClearSubscribeConnectedToMasterEvent()
    {
        if (_connectedToMasterEvent != null)
        {
            _connectedToMasterEvent.RemoveAllListeners();
        }
    }
    #endregion

    #region Joined Lobby 
    public void SubscribeJoinedLobbyEvent(UnityAction callback)
    {
        if (_joinedLobbyEvent == null)
        {
            _joinedLobbyEvent = new UnityEvent();
        }

        _joinedLobbyEvent.AddListener(callback);
    }

    public void UnSubscribeJoinedLobbyEvent(UnityAction callback)
    {
        if (_joinedLobbyEvent != null)
        {
            _joinedLobbyEvent.RemoveListener(callback);
        }
    }

    public void ClearSubscribeJoinedLobbyEvent()
    {
        if (_joinedLobbyEvent != null)
        {
            _joinedLobbyEvent.RemoveAllListeners();
        }
    }
    #endregion

    #region Left Lobby
    public void SubscribeLeftLobbyEvent(UnityAction callback)
    {
        if (_leftLobbyEvent == null)
        {
            _leftLobbyEvent = new UnityEvent();
        }

        _leftLobbyEvent.AddListener(callback);
    }

    public void UnSubscribeLeftLobbyEvent(UnityAction callback)
    {
        if (_leftLobbyEvent != null)
        {
            _leftLobbyEvent.RemoveListener(callback);
        }
    }

    public void ClearSubscribeLeftLobbyEvent()
    {
        if (_leftLobbyEvent != null)
        {
            _leftLobbyEvent.RemoveAllListeners();
        }
    }
    #endregion

    #region Created Room
    public void SubscribeCreatedRoomEvent(UnityAction callback)
    {
        if (_createdRoomEvent == null)
        {
            _createdRoomEvent = new UnityEvent();
        }

        _createdRoomEvent.AddListener(callback);
    }

    public void UnSubscribeCreatedRoomEvent(UnityAction callback)
    {
        if (_createdRoomEvent != null)
        {
            _createdRoomEvent.RemoveListener(callback);
        }
    }

    public void ClearSubscribeCreatedRoomEvent()
    {
        if (_createdRoomEvent != null)
        {
            _createdRoomEvent.RemoveAllListeners();
        }
    }
    #endregion

    #region Create Room Failed
    public void SubscribeCreateRoomFailedEvent(UnityAction<short, string> callback)
    {
        if (_createRoomFailedEvent == null)
        {
            _createRoomFailedEvent = new PhotonFailedEvent();
        }

        _createRoomFailedEvent.AddListener(callback);
    }

    public void UnSubscribeCreateRoomFailedEvent(UnityAction<short, string> callback)
    {
        if (_createRoomFailedEvent != null)
        {
            _createRoomFailedEvent.RemoveListener(callback);
        }
    }

    public void ClearSubscribeCreateRoomFailedEvent()
    {
        if (_createRoomFailedEvent != null)
        {
            _createRoomFailedEvent.RemoveAllListeners();
        }
    }
    #endregion

    #region Join Room Failed
    public void SubscribeJoinRoomFailedEvent(UnityAction<short, string> callback)
    {
        if (_joinRoomFailedEvent == null)
        {
            _joinRoomFailedEvent = new PhotonFailedEvent();
        }

        _joinRoomFailedEvent.AddListener(callback);
    }

    public void UnSubscribeJoinRoomFailedEvent(UnityAction<short, string> callback)
    {
        if (_joinRoomFailedEvent != null)
        {
            _joinRoomFailedEvent.RemoveListener(callback);
        }
    }

    public void ClearSubscribeJoinRoomFailedEvent()
    {
        if (_joinRoomFailedEvent != null)
        {
            _joinRoomFailedEvent.RemoveAllListeners();
        }
    }
    #endregion

    #region Join Random Failed
    public void SubscribeJoinRandomFailedEvent(UnityAction<short, string> callback)
    {
        if (_joinRandomFailedEvent == null)
        {
            _joinRandomFailedEvent = new PhotonFailedEvent();
        }

        _joinRandomFailedEvent.AddListener(callback);
    }

    public void UnSubscribeJoinRandomFailedEvent(UnityAction<short, string> callback)
    {
        if (_joinRandomFailedEvent != null)
        {
            _joinRandomFailedEvent.RemoveListener(callback);
        }
    }

    public void ClearSubscribeJoinRandomFailedEvent()
    {
        if (_joinRandomFailedEvent != null)
        {
            _joinRandomFailedEvent.RemoveAllListeners();
        }
    }
    #endregion

    #region Joined Room 
    public void SubscribeJoinedRoomEvent(UnityAction callback)
    {
        if (_joinedRoomEvent == null)
        {
            _joinedRoomEvent = new UnityEvent();
        }

        _joinedRoomEvent.AddListener(callback);
    }

    public void UnSubscribeJoinedRoomEvent(UnityAction callback)
    {
        if (_joinedRoomEvent != null)
        {
            _joinedRoomEvent.RemoveListener(callback);
        }
    }

    public void ClearSubscribeJoinedRoomEvent()
    {
        if (_joinedRoomEvent != null)
        {
            _joinedRoomEvent.RemoveAllListeners();
        }
    }
    #endregion

    #region Left Room 
    public void SubscribeLeftRoomEvent(UnityAction callback)
    {
        if (_leftRoomEvent == null)
        {
            _leftRoomEvent = new UnityEvent();
        }

        _leftRoomEvent.AddListener(callback);
    }

    public void UnSubscribeLeftRoomEvent(UnityAction callback)
    {
        if (_leftRoomEvent != null)
        {
            _leftRoomEvent.RemoveListener(callback);
        }
    }

    public void ClearSubscribeLeftRoomEvent()
    {
        if (_leftRoomEvent != null)
        {
            _leftRoomEvent.RemoveAllListeners();
        }
    }
    #endregion

    #region Player Entered Room 
    public void SubscribePlayerEnteredRoomEvent(UnityAction<Player> callback)
    {
        if (_playerEnteredRoomEvent == null)
        {
            _playerEnteredRoomEvent = new PhotonPlayerEvent();
        }

        _playerEnteredRoomEvent.AddListener(callback);
    }

    public void UnSubscribePlayerEnteredRoomEvent(UnityAction<Player> callback)
    {
        if (_playerEnteredRoomEvent != null)
        {
            _playerEnteredRoomEvent.RemoveListener(callback);
        }
    }

    public void ClearSubscribePlayerEnteredRoomEvent()
    {
        if (_playerEnteredRoomEvent != null)
        {
            _playerEnteredRoomEvent.RemoveAllListeners();
        }
    }
    #endregion

    #region Player Left Room
    public void SubscribePlayerLeftRoomEvent(UnityAction<Player> callback)
    {
        if (_playerLeftRoomEvent == null)
        {
            _playerLeftRoomEvent = new PhotonPlayerEvent();
        }

        _playerLeftRoomEvent.AddListener(callback);
    }

    public void UnSubscribePlayerLeftRoomEvent(UnityAction<Player> callback)
    {
        if (_playerLeftRoomEvent != null)
        {
            _playerLeftRoomEvent.RemoveListener(callback);
        }
    }

    public void ClearSubscribePlayerLeftRoomEvent()
    {
        if (_playerLeftRoomEvent != null)
        {
            _playerLeftRoomEvent.RemoveAllListeners();
        }
    }
    #endregion

    #region Master Client Switched
    public void SubscribeMasterClientSwitchedEvent(UnityAction<Player> callback)
    {
        if (_masterClientSwitchedEvent == null)
        {
            _masterClientSwitchedEvent = new PhotonPlayerEvent();
        }

        _masterClientSwitchedEvent.AddListener(callback);
    }

    public void UnSubscribeMasterClientSwitchedEvent(UnityAction<Player> callback)
    {
        if (_masterClientSwitchedEvent != null)
        {
            _masterClientSwitchedEvent.RemoveListener(callback);
        }
    }

    public void ClearSubscribeMasterClientSwitchedEvent()
    {
        if (_masterClientSwitchedEvent != null)
        {
            _masterClientSwitchedEvent.RemoveAllListeners();
        }
    }
    #endregion

    #region Region List Received
    public void SubscribeRegionListReceivedEvent(UnityAction<RegionHandler> callback)
    {
        if (_regionListReceivedEvent == null)
        {
            _regionListReceivedEvent = new RegionHandlerEvent();
        }

        _regionListReceivedEvent.AddListener(callback);
    }

    public void UnSubscribeRegionListReceivedEvent(UnityAction<RegionHandler> callback)
    {
        if (_regionListReceivedEvent != null)
        {
            _regionListReceivedEvent.RemoveListener(callback);
        }
    }

    public void ClearSubscribeRegionListReceivedEvent()
    {
        if (_regionListReceivedEvent != null)
        {
            _regionListReceivedEvent.RemoveAllListeners();
        }
    }
    #endregion

    #region Room List Update
    public void SubscribeRoomListUpdateEvent(UnityAction<List<RoomInfo>> callback)
    {
        if (_roomListUpdateEvent == null)
        {
            _roomListUpdateEvent = new RoomInfoListEvent();
        }

        _roomListUpdateEvent.AddListener(callback);
    }

    public void UnSubscribeRoomListUpdateEvent(UnityAction<List<RoomInfo>> callback)
    {
        if (_roomListUpdateEvent != null)
        {
            _roomListUpdateEvent.RemoveListener(callback);
        }
    }

    public void ClearSubscribeRoomListUpdateEvent()
    {
        if (_roomListUpdateEvent != null)
        {
            _roomListUpdateEvent.RemoveAllListeners();
        }
    }
    #endregion

    #region Room Properties Update
    public void SubscribeRoomPropertiesUpdateEvent(UnityAction<ExitGames.Client.Photon.Hashtable> callback)
    {
        if (_roomPropertiesUpdateEvent == null)
        {
            _roomPropertiesUpdateEvent = new PhotonHashtableEvent();
        }

        _roomPropertiesUpdateEvent.AddListener(callback);
    }

    public void UnSubscribeRoomPropertiesUpdateEvent(UnityAction<ExitGames.Client.Photon.Hashtable> callback)
    {
        if (_roomPropertiesUpdateEvent != null)
        {
            _roomPropertiesUpdateEvent.RemoveListener(callback);
        }
    }

    public void ClearSubscribeRoomPropertiesUpdateEvent()
    {
        if (_roomPropertiesUpdateEvent != null)
        {
            _roomPropertiesUpdateEvent.RemoveAllListeners();
        }
    }
    #endregion

    #region Player Properties Update
    public void SubscribePlayerPropertiesUpdateEvent(UnityAction<Player, ExitGames.Client.Photon.Hashtable> callback)
    {
        if (_playerPropertiesUpdateEvent == null)
        {
            _playerPropertiesUpdateEvent = new PhotonPlayerHashtableEvent();
        }

        _playerPropertiesUpdateEvent.AddListener(callback);
    }

    public void UnSubscribePlayerPropertiesUpdateEvent(UnityAction<Player, ExitGames.Client.Photon.Hashtable> callback)
    {
        if (_playerPropertiesUpdateEvent != null)
        {
            _playerPropertiesUpdateEvent.RemoveListener(callback);
        }
    }

    public void ClearSubscribePlayerPropertiesUpdateEvent()
    {
        if (_playerPropertiesUpdateEvent != null)
        {
            _playerPropertiesUpdateEvent.RemoveAllListeners();
        }
    }
    #endregion

    #region Friend List Update
    public void SubscribeFriendListUpdateEvent(UnityAction<List<FriendInfo>> callback)
    {
        if (_friendListUpdateEvent == null)
        {
            _friendListUpdateEvent = new FriendInfoListEvent();
        }

        _friendListUpdateEvent.AddListener(callback);
    }

    public void UnSubscribeFriendListUpdateEvent(UnityAction<List<FriendInfo>> callback)
    {
        if (_friendListUpdateEvent != null)
        {
            _friendListUpdateEvent.RemoveListener(callback);
        }
    }

    public void ClearSubscribeFriendListUpdateEvent()
    {
        if (_friendListUpdateEvent != null)
        {
            _friendListUpdateEvent.RemoveAllListeners();
        }
    }
    #endregion

    #region Custom Auth Response
    public void SubscribeCustomAuthResponseEvent(UnityAction<Dictionary<string, object>> callback)
    {
        if (_customAuthResponseEvent == null)
        {
            _customAuthResponseEvent = new CustomAuthResponseEvent();
        }

        _customAuthResponseEvent.AddListener(callback);
    }

    public void UnSubscribeCustomAuthResponseEvent(UnityAction<Dictionary<string, object>> callback)
    {
        if (_customAuthResponseEvent != null)
        {
            _customAuthResponseEvent.RemoveListener(callback);
        }
    }

    public void ClearSubscribeCustomAuthResponseEvent()
    {
        if (_customAuthResponseEvent != null)
        {
            _customAuthResponseEvent.RemoveAllListeners();
        }
    }
    #endregion

    #region MonoBehaviourPunCallbacks CallBacks
    public override void OnConnected()
    {
        Debug.Log("<Color=Green>OnConnected</Color>");

        if (_connectEvent != null)
        {
            _connectEvent.Invoke();
        }
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        Debug.Log("<Color=Red>OnDisconnected</Color> : " + cause);

        if (_disconnectEvent != null)
        {
            _disconnectEvent.Invoke(cause);
        }
    }

    public override void OnConnectedToMaster()
    {
        Debug.Log("OnConnectedToMaster");

        if (_connectedToMasterEvent != null)
        {
            _connectedToMasterEvent.Invoke();
        }
    }

    public override void OnJoinedLobby()
    {
        Debug.Log("OnJoinedLobby");

        if (_joinedLobbyEvent != null)
        {
            _joinedLobbyEvent.Invoke();
        }
    }

    public override void OnLeftLobby()
    {
        Debug.Log("OnLeftLobby");

        if (_leftLobbyEvent != null)
        {
            _leftLobbyEvent.Invoke();
        }
    }

    public override void OnJoinedRoom()
    {
        Debug.Log("OnJoinedRoom");

        if (_joinedRoomEvent != null)
        {
            _joinedRoomEvent.Invoke();
        }
    }

    public override void OnLeftRoom()
    {
        Debug.Log("OnLeftRoom");

        if (_leftRoomEvent != null)
        {
            _leftRoomEvent.Invoke();
        }
    }

    public override void OnCreatedRoom()
    {
        Debug.Log("OnCreatedRoom");

        if (_createdRoomEvent != null)
        {
            _createdRoomEvent.Invoke();
        }
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        Debug.LogFormat("OnPlayerEnteredRoom : {0}", newPlayer.NickName);

        if (_playerEnteredRoomEvent != null)
        {
            _playerEnteredRoomEvent.Invoke(newPlayer);
        }
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        Debug.LogFormat("OnPlayerLeftRoom : {0}", otherPlayer.NickName);

        if (_playerLeftRoomEvent != null)
        {
            _playerLeftRoomEvent.Invoke(otherPlayer);
        }
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.LogFormat("OnCreateRoomFailed {0} : {1}", returnCode, message);

        if (_createRoomFailedEvent != null)
        {
            _createRoomFailedEvent.Invoke(returnCode, message);
        }
    }

    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        Debug.LogFormat("OnJoinRoomFailed {0} : {1}", returnCode, message);

        if (_joinRoomFailedEvent != null)
        {
            _joinRoomFailedEvent.Invoke(returnCode, message);
        }
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.LogFormat("OnJoinRandomFailed {0} : {1}", returnCode, message);

        if (_joinRandomFailedEvent != null)
        {
            _joinRandomFailedEvent.Invoke(returnCode, message);
        }
    }

    public override void OnMasterClientSwitched(Player newMasterClient)
    {
        Debug.LogFormat("OnMasterClientSwitched : {0}", newMasterClient.NickName);

        if (_masterClientSwitchedEvent != null)
        {
            _masterClientSwitchedEvent.Invoke(newMasterClient);
        }
    }

    public override void OnRegionListReceived(RegionHandler regionHandler)
    {
        Debug.LogFormat("OnRegionListReceived : {0}", regionHandler.ToString());

        if (_regionListReceivedEvent != null)
        {
            _regionListReceivedEvent.Invoke(regionHandler);
        }
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        Debug.LogFormat("OnRoomListUpdate : RoomCount({0})", roomList.Count);

        if (_roomListUpdateEvent != null)
        {
            _roomListUpdateEvent.Invoke(roomList);
        }
    }

    public override void OnRoomPropertiesUpdate(ExitGames.Client.Photon.Hashtable propertiesThatChanged)
    {
        Debug.LogFormat("OnRoomPropertiesUpdate : {0}", propertiesThatChanged.ToStringFull());

        if (_roomPropertiesUpdateEvent != null)
        {
            _roomPropertiesUpdateEvent.Invoke(propertiesThatChanged);
        }
    }

    public override void OnPlayerPropertiesUpdate(Player target, ExitGames.Client.Photon.Hashtable changedProps)
    {
        Debug.LogFormat("OnPlayerPropertiesUpdate : {0} => {1}", target.NickName, changedProps.ToStringFull());

        if (_playerPropertiesUpdateEvent != null)
        {
            _playerPropertiesUpdateEvent.Invoke(target, changedProps);
        }
    }

    public override void OnFriendListUpdate(List<FriendInfo> friendList)
    {
        Debug.LogFormat("OnFriendListUpdate : FriendCount({0})", friendList.Count);

        if (_friendListUpdateEvent != null)
        {
            _friendListUpdateEvent.Invoke(friendList);
        }
    }

    public override void OnCustomAuthenticationResponse(Dictionary<string, object> data)
    {
        Debug.LogFormat("OnCustomAuthenticationResponse : {0}", data.ToStringFull());

        if (_customAuthResponseEvent != null)
        {
            _customAuthResponseEvent.Invoke(data);
        }
    }
    #endregion

    #region IInRoomCallbacks CallBacks

    #endregion
}
