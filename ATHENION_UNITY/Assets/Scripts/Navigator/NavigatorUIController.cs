﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NavigatorUIController : MonoBehaviour
{
    #region Inspector Properties
    public Canvas Canvas;
    [Header("UI")]
    public MenuSlider BotNavBar;

    [Header("Loading")]
    public GameObject LoadingUI;
    #endregion

    #region Properties
    private bool _isCanvasShow = false;
    #endregion

    #region Methods
    public void ShowLoading()
    {
        LoadingUI.SetActive(true);
    }
    public void HideLoading()
    {
        LoadingUI.SetActive(false);
    }

    public void HideAllLoading()
    {
        PopupUIManager.Instance.HideAllLoad();
        LoadingUI.SetActive(false);
    }

    public void HideCanvas()
    {
        if (_isCanvasShow || Canvas.gameObject.activeSelf)
        {
            GameHelper.UITransition_FadeOut(Canvas.gameObject);
            //Canvas.gameObject.SetActive(false);
            _isCanvasShow = false;
        }
    }

    public void ShowCanvas()
    {
        if (!_isCanvasShow || !Canvas.gameObject.activeSelf)
        {
            GameHelper.UITransition_FadeIn(Canvas.gameObject);
            //Canvas.gameObject.SetActive(true);
            _isCanvasShow = true;
        }
    }
    #endregion
}
