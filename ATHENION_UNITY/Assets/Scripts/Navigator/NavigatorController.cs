﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using Karamucho.Network;
using Newtonsoft.Json;

#if UNITY_ANDROID
using GooglePlayGames;
#endif

public class NavigatorController : MonoSingleton<NavigatorController>
{
    public enum PageType { Shop, Deck, Home, Guild, StarMarket }

    public static event Action OnLogout;
    public OutGameUIBase CurrentPage { get; private set; }

    private List<OutGameUIBase> _switchableScene = new List<OutGameUIBase>();
    private bool isNavigating;

    public NavigatorUIController NavUIManager
    {
        get
        {
            if (_navUIManager == null)
                _navUIManager = GameObject.FindObjectOfType<NavigatorUIController>();
            return _navUIManager;
        }
    }
    private NavigatorUIController _navUIManager;
    private TopToolbar _topToolbar;

    //MOCKUP ONLY
    protected override void Awake()
    {
        base.Awake();
        Init();
    }

    private void OnEnable()
    {
        DataManager.OnUpdateFeatureUnlock += UpdateBtn;
    }

    private void OnDisable()
    {
        DataManager.OnUpdateFeatureUnlock -= UpdateBtn;
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        ClearAndExitCurrentPage();
    }

    private void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            // Pause
        }
        else
        {
            // Resume
            PopupUIManager.Instance.Show_SoftLoad(true);

            DataManager.Instance.CheckIsValidUserAndVersion(
                  OnValidUserAndVersion
                , OnInvalidUserAndVersion
            );
        }
    }

    private void OnApplicationQuit()
    {
        DataManager.Instance.ClearFriendMatchRequest(null, null);
    }

    private void Init()
    {
        UnityAction onComplete = delegate ()
        {
            SetButtonEvent();
            GotoHomePage();
        };
        //PlayVideo("TestVideo", false, delegate
        //{
        //    StartCoroutine(Setup(onComplete));
        //});

        StartCoroutine(Setup(onComplete));
    }

    private IEnumerator Setup(UnityAction onComplete)
    {
        PopupUIManager.Instance.Show_SoftLoad(true);

        UpdateBtn();

        bool isVersionValid = false;
        string versionErrorMsg = "";
        Debug.Log("IECheckVersionValid");
        yield return DataManager.Instance.IECheckVersionValid(
            delegate (bool isValid, string errorMsg)
            {
                isVersionValid = isValid;
                versionErrorMsg = errorMsg;
            }
        );

        if (isVersionValid == false)
        {
            OnInvalidUserAndVersion(versionErrorMsg);
            PopupUIManager.Instance.Show_SoftLoad(false);

            yield break;
        }

        Debug.Log("ClearFriendMatchRequest");
        bool isFinish = false;
        DataManager.Instance.ClearFriendMatchRequest(() => isFinish = true, (err) => isFinish = true);
        yield return new WaitUntil(() => isFinish == true);

        Debug.Log("Navigator Setup Complete");
        PopupUIManager.Instance.Show_SoftLoad(false);
        onComplete?.Invoke();
    }

    private void UpdateBtn()
    {
        NavUIManager.BotNavBar.SetInteractable(
             (int)PageType.Shop
           , DataManager.Instance.IsFeatureEnable(FeatureUnlock.Shop)
       );

        NavUIManager.BotNavBar.SetInteractable(
              (int)PageType.Deck
            , DataManager.Instance.IsFeatureEnable(FeatureUnlock.Deck)
        );

        NavUIManager.BotNavBar.SetInteractable(
              (int)PageType.Guild
            , DataManager.Instance.IsFeatureEnable(FeatureUnlock.Guild)
        );

        NavUIManager.BotNavBar.SetInteractable(
              (int)PageType.StarMarket
            , DataManager.Instance.IsFeatureEnable(FeatureUnlock.StarMarket)
        );
    }

    #region Login & Check Session

    private void OnValidUserAndVersion()
    {
        if (!KTPlayManager.Instance.IsLoggedIn())
        {
            KTPlayManager.Instance.StartLogin(
                delegate ()
                {
                    PopupUIManager.Instance.Show_SoftLoad(false);
                }
            );
        }
        else
        {
            PopupUIManager.Instance.Show_SoftLoad(false);
        }
    }

    public void OnInvalidUserAndVersion(string errors)
    {
        List<string> errorList = new List<string>(errors.Split('|'));

        if (errorList.Contains("ERROR_NEW_ASSET_BUNDLE_FOUND"))
        {
            PopupUIManager.Instance.ShowPopup_ConfirmOnly(
                    ""
                , LocalizationManager.Instance.GetText("ERROR_NEW_ASSET_BUNDLE_FOUND")
                , delegate ()
                {
                    Logout(true);
                }
            );

            PopupUIManager.Instance.Show_SoftLoad(false);
        }
        else if (errorList.Contains("ERROR_ASSET_BUNDLE_KEY_NOT_FOUND"))
        {
            PopupUIManager.Instance.ShowPopup_Error(
                  ""
                , LocalizationManager.Instance.GetText("ERROR_ASSET_BUNDLE_KEY_NOT_FOUND")
                , delegate ()
                {
                    Logout(true);
                }
            );

            PopupUIManager.Instance.Show_SoftLoad(false);
        }
        else if (errorList.Contains("ERROR_SESSION_INVALID"))
        {
            PopupUIManager.Instance.ShowPopup_Error(
                  ""
                , LocalizationManager.Instance.GetText("ERROR_SESSION_INVALID")
                , delegate ()
                {
                    Logout(true);
                }
            );

            PopupUIManager.Instance.Show_SoftLoad(false);
        }
        else if (errorList.Contains("VERSION_INVALID"))
        {
            PopupUIManager.Instance.ShowPopup_Error(
                  ""
                , LocalizationManager.Instance.GetText("LOGIN_PLEASE_UPDATE_VERSION_HEADER")
                , delegate ()
                {
                    Logout(true);
                }
            );

            PopupUIManager.Instance.Show_SoftLoad(false);
        }
        else
        {
            Logout(true);
        }
    }

    private IEnumerator IEGoogleSignOut(UnityAction onComplete)
    {
#if UNITY_ANDROID
        PlayGamesPlatform.Instance.SignOut();
        while (PlayGamesPlatform.Instance.IsAuthenticated())
        {
            yield return null;
        }
#endif

        onComplete?.Invoke();

        yield break;
    }

    protected override void OnSceneUnloaded(Scene scene)
    {
        base.OnSceneUnloaded(scene);
        if (scene.name == GameHelper.NavigatorSceneName)
        {
            this.DestroySelf();
        }

    }

    public void Logout(bool isAutoLogin = false)
    {
        PopupUIManager.Instance.HideAllLoad();
        PopupUIManager.Instance.Show_SoftLoad(true);
        GameHelper.DestroySingletons();
        OnLogout?.Invoke();

        if (!isAutoLogin)
        {
            AuthenticationManager.ClearRememberMe(() => ClearLocalData(), null);
        }
        else
        {
            ClearLocalData();
        }
    }

    private void ClearLocalData()
    {
        KTPlayManager.Instance.Logout();

#if UNITY_ANDROID
        PlayGamesPlatform.Instance.SignOut();
        StartCoroutine(IEGoogleSignOut(() =>
        {
            GoToLogin();
            DataManager.ClearAll();
            PopupUIManager.Instance.Show_SoftLoad(false);
        }));
#else
        GoToLogin();
        DataManager.ClearAll(); 
        PopupUIManager.Instance.Show_SoftLoad(false);
#endif
    }

    public void GoToLogin()
    {
        if (CurrentPage != null)
        {
            CurrentPage.OnExit();
            DestroyImmediate(CurrentPage.gameObject);
        }

        GotoLoginScene();
    }

    private void GotoLoginScene()
    {
        LoadScene(GameHelper.LoginSceneName, LoadSceneMode.Single, true, null);
    }

    #endregion

    /// <summary>
    /// Set Active All UI and Nav bar is included.
    /// </summary>
    /// <param name="isActive"></param>
    public void SetActiveUI(bool isActive)
    {
        if (isActive)
        {
            NavUIManager.ShowCanvas();
        }
        else
        {
            NavUIManager.HideCanvas();
        }
    }

    private void SetButtonEvent()
    {
        NavUIManager.BotNavBar.SetOnClickIndexCallback(
                (index) =>
                {
                    if (isNavigating == true)
                        return;
                    OnClickBotNavBar(index);
                }
            );
    }

    private void OnClickBotNavBar(int index)
    {
        switch (index)
        {
            case 0: GotoShopPage(); break;
            case 1: GotoDeckPage(); break;
            case 2: GotoHomePage(); break;
            case 3: break;
            case 4: GotoStarMarketPage(); break;
        }
    }

    public void LoadMatchingScene(UnityAction onComplete)
    {
        LoadScene(GameHelper.MatchingSceneName, LoadSceneMode.Additive, true, onComplete);
    }

    public void LoadMainEventScene(UnityAction onComplete = null)
    {
        LoadScene(GameHelper.MainEventSceneName, LoadSceneMode.Additive, true, onComplete);
    }

    public void LoadEventScene(string sceneName, UnityAction onComplete = null)
    {
        LoadScene(sceneName, LoadSceneMode.Additive, true, onComplete);
    }

    public void LoadEventBattleScene(UnityAction onComplete = null)
    {
        LoadScene(GameHelper.EventBattleSceneName, LoadSceneMode.Additive, true, onComplete);
    }

    public void LoadOpeningPackScene(UnityAction onComplete)
    {
        LoadScene(GameHelper.PackOpeningSceneName, LoadSceneMode.Additive, true, onComplete);
    }

    public void GotoBattleScene()
    {
        ClearAndExitCurrentPage();
        //NavUIManager.HideAllLoading();
        PopupUIManager.Instance.HideAllSoftLoad();
        PopupUIManager.Instance.Show_FullLoad(true);
        LoadScene(GameHelper.BattleSceneName, LoadSceneMode.Single, true, null);
    }

    public void GotoStarMarketPage()
    {
        if (DataManager.Instance.IsFeatureEnable(FeatureUnlock.StarMarket))
        {
            NavUIManager.BotNavBar.MoveHighlightIndex((int)PageType.StarMarket);

            NavUIManager.ShowLoading();
            NavigateToScene(
                  GameHelper.StarExchangeSceneName
                , delegate ()
                {
                    NavUIManager.ShowCanvas();
                    TopToolbar.RemoveLatestSetting();
                    TopToolbar.AddSetting(true, true, GotoHomePage);
                    NavUIManager.HideLoading();
                }
            );
        }
    }

    public void GotoDeckPage()
    {
        NavUIManager.BotNavBar.MoveHighlightIndex((int)PageType.Deck);
        NavUIManager.ShowLoading();
        NavigateToScene(
              GameHelper.CollectionSceneName
            , delegate ()
            {
                NavUIManager.ShowCanvas();
                TopToolbar.RemoveLatestSetting();
                TopToolbar.AddSetting(true, true, GotoHomePage);
                NavUIManager.HideLoading();
            }
        );
    }

    public void GotoHomePage()
    {
        NavUIManager.BotNavBar.MoveHighlightIndex((int)PageType.Home);
        NavUIManager.ShowLoading();
        NavigateToScene(
              GameHelper.HomeSceneName
            , delegate ()
            {
                NavUIManager.ShowCanvas();
                TopToolbar.RemoveLatestSetting();
                TopToolbar.AddSetting(false, true, null);
                NavUIManager.HideLoading();
            }
        );
    }

    public void GotoHomePage(string param)
    {
        NavUIManager.BotNavBar.MoveHighlightIndex((int)PageType.Home);
        NavUIManager.ShowLoading();
        NavigateToScene(GameHelper.HomeSceneName, () =>
        {
            NavUIManager.ShowCanvas();
            TopToolbar.RemoveLatestSetting();
            TopToolbar.AddSetting(false, true, null);
            NavUIManager.HideLoading();
            string[] keys = param.Split('/');

            switch (keys[0].ToLower())
            {
                case "accomplishment":
                {
                    AccomplishmentPanelManager.TabType tab = AccomplishmentPanelManager.TabType.Achievement;
                    if (keys.Length >= 2)
                    {
                        if (!GameHelper.StrToEnum<AccomplishmentPanelManager.TabType>(keys[1], out tab))
                        {
                            tab = AccomplishmentPanelManager.TabType.Achievement;
                        }
                    }

                    HomeManager.Instance.ShowAccomplishment(tab);
                }
                break;

                case "profile":
                {
                    ProfileEditorManager.TabType tab = ProfileEditorManager.TabType.LevelProgression;
                    if (keys.Length >= 2)
                    {
                        if (!GameHelper.StrToEnum<ProfileEditorManager.TabType>(keys[1], out tab))
                        {
                            tab = ProfileEditorManager.TabType.LevelProgression;
                        }
                    }

                    HomeManager.Instance.ShowProfile(tab);
                }
                break;

                case "friend":
                {
                    HomeManager.Instance.ShowFriends();
                }
                break;

                case "lobby":
                {
                    GameMode gameMode = GameMode.Casual;
                    if (keys.Length >= 2)
                    {
                        if (!GameHelper.StrToEnum<GameMode>(keys[1], out gameMode))
                        {
                            gameMode = GameMode.Casual;
                        }
                    }

                    HomeManager.Instance.ShowLobby(gameMode);
                }
                break;

                case "seasonpass":
                {
                    SeasonPassUIManager.TabType tab = SeasonPassUIManager.TabType.Reward;
                    if (keys.Length >= 2)
                    {
                        if (!GameHelper.StrToEnum<SeasonPassUIManager.TabType>(keys[1], out tab))
                        {
                            tab = SeasonPassUIManager.TabType.Reward;
                        }
                    }

                    HomeManager.Instance.ShowSeasonPass(tab);
                }
                break;

                case "adventure":
                {
                    HomeManager.Instance.ShowMainAdventure();
                }
                break;

                case "event":
                {
                    HomeManager.Instance.ShowMainEvent(GameEvent.EventPageType.GameEvent);
                }
                break;

                case "eventbattle":
                {
                    HomeManager.Instance.ShowEventBattle();
                }
                break;

                case "social":
                {
                    HomeManager.Instance.ShowSocial();
                }
                break;

                case "home": default: break;
            }
        });
    }

    public void GoToCardList()
    {
        PopupUIManager.Instance.Show_MidSoftLoad(true);

        GameHelper.LoadSceneAsync(
              "CardListScene"
            , LoadSceneMode.Single
            , delegate ()
            {
                PopupUIManager.Instance.Show_MidSoftLoad(false);
            }
            , null
        );
    }

    public void GoToEffectAttack()
    {
        PopupUIManager.Instance.Show_MidSoftLoad(true);

        GameHelper.LoadSceneAsync(
              "_DebugSoulAttack"
            , LoadSceneMode.Single
            , delegate ()
            {
                PopupUIManager.Instance.Show_MidSoftLoad(false);
            }
            , null
        );
    }

    public void GoToSpawnCard()
    {
        PopupUIManager.Instance.Show_MidSoftLoad(true);

        GameHelper.LoadSceneAsync(
              "_DebugSpawnUnit"
            , LoadSceneMode.Single
            , delegate ()
            {
                PopupUIManager.Instance.Show_MidSoftLoad(false);
            }
            , null
        );
    }

    public void GotoShopPage()
    {
        GotoShopPage(ShopTypes.Promotion);
    }

    public void GotoShopPage(ShopTypes type)
    {
        NavUIManager.BotNavBar.MoveHighlightIndex((int)PageType.Shop);
        NavUIManager.ShowLoading();
        NavigateToScene(GameHelper.ShopSceneName, () =>
        {
            NavUIManager.ShowCanvas();
            TopToolbar.RemoveLatestSetting();
            TopToolbar.AddSetting(true, true, GotoHomePage);
            NavUIManager.HideLoading();
            ShopManager.Instance.ShowUI(type);
        });
    }

    #region NavigateTo Main Scene

    private void NavigateToScene(string sceneName, UnityAction onComplete)
    {
        if (CurrentPage != null)
        {
            if (CurrentPage.SceneName == sceneName) //Click the same button.
            {
                NavUIManager.HideLoading();
                onComplete?.Invoke();
                return;
            }
        }

        StartCoroutine(IENavigateToScene(sceneName, onComplete));
    }

    private IEnumerator IENavigateToScene(string targetSceneName, UnityAction onComplete)
    {
        isNavigating = true;
        _topToolbar = null;
        if (CurrentPage == null)
        {
            yield return IELoadScene(targetSceneName, LoadSceneMode.Additive);
            CurrentPage = GameObject.FindObjectOfType<OutGameUIBase>();
            if (CurrentPage.IsCanSwitchScene)
            {
                _switchableScene.Add(CurrentPage);
            }
            CurrentPage.OnInitialize(onComplete);
            onComplete?.Invoke();
            isNavigating = false;
            yield break;
        }
        else
        {
            // Load new scene
            // Unload old scene. or hide old scene.
            // init. or show ui.

            OutGameUIBase oldPage = CurrentPage;
            oldPage.OnExit();

            yield return LoadNewSceneAndSetCurrentPage(targetSceneName);
            yield return null;
            yield return UnloadOldOutGameUIBase(oldPage);
            yield return null;
            yield return InitCurrentOutGameUIBase(CurrentPage);
            yield return null;
            onComplete?.Invoke();
            isNavigating = false;
        }
        yield break;
    }

    private IEnumerator LoadNewSceneAndSetCurrentPage(string targetSceneName)
    {
        OutGameUIBase newSwitchableScene = _switchableScene.Find(data => data.SceneName == targetSceneName);
        Debug.Log("_switchableScene : " + _switchableScene.Count);
        if (newSwitchableScene != null)
        {
            CurrentPage = newSwitchableScene;
            yield return new WaitForSeconds(0.1f);
        }
        else
        {
            yield return IELoadScene(targetSceneName, LoadSceneMode.Additive);
            OutGameUIBase[] outGameUIBase = GameObject.FindObjectsOfType<OutGameUIBase>();
            CurrentPage = outGameUIBase.Find((obj) => obj.SceneName == targetSceneName);

            if (CurrentPage.IsCanSwitchScene)
            {
                _switchableScene.Add(CurrentPage);
            }
        }
    }

    private IEnumerator UnloadOldOutGameUIBase(OutGameUIBase oldPage)
    {
        if (oldPage.IsCanSwitchScene)
        {
            bool isFinish = false;
            oldPage.HideUI(() => isFinish = true);
            yield return new WaitUntil(() => isFinish == true);
        }
        else
        {
            bool isFinish = false;
            bool isCanUnloadResource = oldPage.IsCanUnloadResourceAfterUnloadScene;
            GameHelper.UnLoadSceneAsync(
                oldPage.SceneName,
                delegate
                {
                    if (oldPage != null)
                    {
                        DestroyImmediate(oldPage.gameObject);
                    }
                    isFinish = true;
                },
                isCanUnloadResource
            );
            yield return new WaitUntil(() => isFinish == true);
        }

    }

    private IEnumerator InitCurrentOutGameUIBase(OutGameUIBase currentPage)
    {
        bool isFinish = false;
        currentPage.ShowUI();
        CurrentPage.OnInitialize(() => isFinish = true);
        yield return new WaitUntil(() => isFinish == true);
    }

    #endregion

    private IEnumerator IELoadScene(string sceneName, LoadSceneMode loadSceneMode)
    {
        bool isFinish = false;
        LoadScene(sceneName, loadSceneMode, true, () => isFinish = true);
        yield return new WaitUntil(() => isFinish == true);
    }

    public void LoadScene(string sceneName, LoadSceneMode loadSceneMode, bool isAsync, UnityAction onComplete)
    {
        if (loadSceneMode == LoadSceneMode.Single)
        {
            ClearAndExitCurrentPage();
            RemoveAllSwitchScenable();
        }

        if (isAsync)
        {
            GameHelper.LoadSceneAsync(sceneName, loadSceneMode, onComplete, null);
        }
        else
        {
            SceneManager.LoadScene(sceneName, loadSceneMode);
            GameHelper.DelayCallback(0.1f, onComplete);
        }
    }

    public void ClearAndExitCurrentPage()
    {
        if (CurrentPage != null)
        {
            CurrentPage.OnExit();
            DestroyImmediate(CurrentPage.gameObject);
            CurrentPage = null;
        }
    }

    public void RemoveAllSwitchScenable()
    {
        for (int i = 0; i < _switchableScene.Count; i++)
        {
            if (_switchableScene[i] != null)
            {
                DestroyImmediate(_switchableScene[i].gameObject);
            }
            _switchableScene.RemoveAt(0);
            i--;
        }
        _switchableScene.Clear();
    }

    /// <summary>
    /// Call this method to play and video which is respected to the video name.
    /// </summary>
    /// <param name="videoName"></param>
    /// <param name="isLoop"></param>
    /// <param name="onComplete"></param>
    public void PlayVideo(string videoName, bool isLoop = false, UnityAction onComplete = null)
    {
        PopupUIManager.Instance.Show_SoftLoad(true);

        if (Screen.orientation == ScreenOrientation.Portrait ||
            Screen.orientation == ScreenOrientation.PortraitUpsideDown)
        {
            LoadScene(
                "TestVideoPortraitScene"
                , LoadSceneMode.Additive
                , true
                , delegate
                {
                    PopupUIManager.Instance.Show_SoftLoad(false);
                    GameObject.FindObjectOfType<VideoSceneManager>().Init(
                        videoName
                        , "TestVideoPortraitScene"
                        , isLoop
                        , onComplete
                    );
                }
            );
        }
        else if (Screen.orientation == ScreenOrientation.Landscape)
        {
            LoadScene(
                "1_VideoLandscape"
                , LoadSceneMode.Additive
                , true
                , delegate
                {
                    PopupUIManager.Instance.Show_SoftLoad(false);
                    GameObject.FindObjectOfType<VideoSceneManager>().Init(
                        videoName
                        , "1_VideoLandscape"
                        , isLoop
                        , onComplete
                    );
                }
            );
        }
    }

    #region Go Feature Methods
    /*
        [ShowShopPage]
        - Promotion
        - BoosterPack
        - Diamond
        - Cosmetic

        [ShowDeckPage]
        - None

        [ShowHomePage]
        - Accomplishment
        - Accomplishment/Achievement
        - Accomplishment/Collection
        - Profile
        - Profile/LevelProgression
        - Profile/FactionLevel
        - Profile/Ranking
        - Friend
        - Lobby
        - Lobby/Casual
        - Lobby/Rank
        - Lobby/Practice
        - Adventure
        - EventBattle
        - Event
        - SeasonPass
        - SeasonPass/Reward
        - SeasonPass/Quest
        - SeasonPass/Challenge

        [ShowStarMarketPage]
        - None

        [ShowMainEvent]
        - None
    */

    public void ShowBannerPopup(string json)
    {
        Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
        string imagePath = rawData["image_path"].ToString();
        string function = rawData.ContainsKey("function") ? rawData["function"].ToString() : null;
        string customData = rawData.ContainsKey("custom_data") ? rawData["custom_data"].ToString() : null;
        UnityAction onClick = null;
        if (function != null)
        {
            if (customData != null)
                onClick = () => NavigatorController.Instance.SendMessage(function, customData, SendMessageOptions.DontRequireReceiver);
            else
                onClick = () => NavigatorController.Instance.SendMessage(function, SendMessageOptions.DontRequireReceiver);
        }

        PopupUIManager.Instance.ShowPopup_BannerPopup("BannerPopup", imagePath, imagePath, onClick, null);
    }

    public void ShowShopPage(string page)
    {
        if (DataManager.Instance.IsFeatureEnable(FeatureUnlock.Shop))
        {
            ShopTypes shopType;
            GameHelper.StrToEnum(page, out shopType);
            GotoShopPage(shopType);
        }
    }

    public void ShowDeckPage()
    {
        if (DataManager.Instance.IsFeatureEnable(FeatureUnlock.Deck))
        {
            GotoDeckPage();
        }
    }

    public void ShowHomePage(string param)
    {
        GotoHomePage(param);
    }

    public void ShowStarMarketPage()
    {
        if (DataManager.Instance.IsFeatureEnable(FeatureUnlock.StarMarket))
        {
            GotoStarMarketPage();
        }
    }

    public void ShowMainEvent()
    {
        if (DataManager.Instance.IsFeatureEnable(FeatureUnlock.Event))
        {
            HomeManager.Instance.ShowMainEvent(GameEvent.EventPageType.GameEvent);
        }
    }

    public void ShowMainEvent(string page)
    {
        if (DataManager.Instance.IsFeatureEnable(FeatureUnlock.Event))
        {
            GameEvent.EventPageType pageType;
            if (GameHelper.StrToEnum(page, out pageType))
            {
                HomeManager.Instance.ShowMainEvent(pageType);
            }
        }
    }

    public void ShowFacebookBannerPopup(string json)
    {
        Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
        string imagePath = rawData["image_path"].ToString();

        PopupUIManager.Instance.ShowFacebookBannerPopup("BannerPopup", imagePath, imagePath);
    }

    public void ShowKTPlayDeeplink(string link)
    {
        if (DataManager.Instance.IsFeatureEnable(FeatureUnlock.Community))
        {
            Debug.Log("ShowKTPlayDeeplink : " + link);
            KTPlayManager.Instance.ShowDeepLink(link);
        }
    }

    public void OpenURL(string url)
    {
        Application.OpenURL(url);
    }

    public void StartShowCardChangeLog()
    {
        HomeManager.Instance.StartShowCardChangeLog();
    }
    #endregion
}
