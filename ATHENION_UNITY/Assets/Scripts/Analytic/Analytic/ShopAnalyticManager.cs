﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopAnalyticManager : AnalyticBase
{
    private DateTime _enterTimeStamp;

    public ShopAnalyticManager(AnalyticManager manager) : base(manager)
    {
    }

    public override void Initialize()
    {
        ShopManager.OnInit += OnEnterShop;
        ShopManager.OnQuit += OnQuitShop;

        ShopManager.OnClaimDailyFree += ShopManager_OnClaimDailyFree;
        
        ShopManager.OnPurchaseComplete += ShopManager_OnPurchaseComplete;
        ShopManager.OnPurchaseFail += ShopManager_OnPurchaseFail;
        ShopManager.OnPurchaseCancel += ShopManager_OnPurchaseCancel;

        ShopManager.OnOpenPack += OpenPackFromShop;
        CardPackShopManager.OnBuyPack += OnBuyMorePack;
        CardPackController.OnSkip += OnSkipOpenPack;
    }

    public override void DeInitialize()
    {
        ShopManager.OnInit -= OnEnterShop;
        ShopManager.OnQuit -= OnQuitShop;

        ShopManager.OnClaimDailyFree -= ShopManager_OnClaimDailyFree;

        ShopManager.OnPurchaseComplete -= ShopManager_OnPurchaseComplete;
        ShopManager.OnPurchaseFail -= ShopManager_OnPurchaseFail;
        ShopManager.OnPurchaseCancel -= ShopManager_OnPurchaseCancel;

        ShopManager.OnOpenPack -= OpenPackFromShop;
        CardPackShopManager.OnBuyPack -= OnBuyMorePack;
        CardPackController.OnSkip -= OnSkipOpenPack;
    }

    #region Pack Analytic

    private void OnBuyMorePack(CardPackData packDetail)
    {
        Manager.LogAnalyticEvent(new AnalyticEventTapBuyMorePackData(PlayerData, PlayerInventory, packDetail.PackID, packDetail.Amount));
    }

    private void OpenPackFromShop(PackReward packReward)
    {
        Manager.LogAnalyticEvent(new AnalyticEventOpenPackData(PlayerData, PlayerInventory, packReward));
    }

    private void OnSkipOpenPack(PackReward packDetail)
    {
        if (packDetail == null)
            return;
        Manager.LogAnalyticEvent(new AnalyticEventSkipOpenPackData(PlayerData, packDetail));
    }

    #endregion

    private void ShopManager_OnPurchaseComplete(ShopItemData item)
    {
        int useCoin, useDiamond, useRM;
        CalculateUseVCResultFromItem(item, out useCoin, out useDiamond, out useRM);

        int gotDI, gotRF, gotFR;
        CalculateGetVCResultFromItem(item, out gotDI, out gotRF, out gotFR);

        bool isDailyDealItem = DataManager.Instance.GetDailyDealRecord().ItemID == item.ItemId;
        bool isDailyFreeItem = DataManager.Instance.GetFreeDealRecord().ItemID == item.ItemId;
        bool isWeeklyDealItem = DataManager.Instance.GetWeeklyDealRecord().ItemID == item.ItemId;

        if (isDailyDealItem)
        {
            Manager.LogAnalyticEvent(new AnalyticEventDailyDealCompletedData(PlayerData, PlayerInventory, item.ItemId, useCoin, useDiamond, useRM, gotDI, gotFR, gotRF));
        }
        else if (isWeeklyDealItem)
        {
            Manager.LogAnalyticEvent(new AnalyticEventWeeklyDealCompletedData(PlayerData, PlayerInventory, item.ItemId, useCoin, useDiamond, useRM, gotDI, gotFR, gotRF));
        }
        else
        {
            Manager.LogAnalyticEvent(new AnalyticEventPurchaseCompletedData(PlayerData, PlayerInventory, item.ItemId, useCoin, useDiamond, useRM, gotDI, gotFR, gotRF));
        }
    }

    private void ShopManager_OnPurchaseFail(ShopItemData item)
    {
        int useCoin, useDiamond, useRM;
        CalculateUseVCResultFromItem(item, out useCoin, out useDiamond, out useRM);

        int gotDI, gotRF, gotFR;
        CalculateGetVCResultFromItem(item, out gotDI, out gotRF, out gotFR);

        bool isDailyDealItem = DataManager.Instance.GetDailyDealRecord().ItemID == item.ItemId;
        bool isDailyFreeItem = DataManager.Instance.GetFreeDealRecord().ItemID == item.ItemId;
        bool isWeeklyDealItem = DataManager.Instance.GetWeeklyDealRecord().ItemID == item.ItemId;

        if (isDailyDealItem)
        {
            Manager.LogAnalyticEvent(new AnalyticEventDailyDealFailedData(PlayerData, PlayerInventory, item.ItemId, useCoin, useDiamond, useRM, gotDI, gotFR, gotRF));
        }
        else
        {
            Manager.LogAnalyticEvent(new AnalyticEventPurchaseFailedData(PlayerData, PlayerInventory, item.ItemId, useCoin, useDiamond, useRM, gotDI, gotFR, gotRF));
        }
    }

    private void CalculateUseVCResultFromItem(ShopItemData item, out int useCoin, out int useDiamond, out int useRM)
    {
        useCoin = item.Currency == VirtualCurrency.CO ? item.Price : 0;
        useDiamond = item.Currency == VirtualCurrency.DI ? item.Price : 0;
        useRM = item.Currency == VirtualCurrency.RM ? item.Price : 0;
    }

    private void CalculateGetVCResultFromItem(ShopItemData item, out int gotDI, out int gotRF, out int gotFR)
    {
        gotDI = 0;
        gotRF = 0;
        gotFR = 0;
        List<ItemData> itemList = item.CustomData.ItemDetail;
        for (int i = 0; i < itemList.Count; i++)
        {
            if (GameHelper.IsVirtualCurrency(itemList[i].ItemID))
            {
                string[] splitText = item.ItemId.Split('_');
                string vcText = splitText[1].ToUpper();
                if (vcText == VirtualCurrency.DI.ToString())
                    gotDI += itemList[i].Amount;
                else if (vcText == VirtualCurrency.RF.ToString())
                    gotRF += itemList[i].Amount;
                else if (vcText == VirtualCurrency.FR.ToString())
                    gotFR += itemList[i].Amount;
            }
        }
    }

    private void ShopManager_OnClaimDailyFree(DealRecord record)
    {
        foreach (var item in record.ItemList)
        {
            string itemID = item.Value.ItemID;
            int amount = item.Value.Amount;
            AnalyticCurrencyType type = ConvertItemIDToAnalyticCurrencyType(itemID);

            Manager.LogAnalyticEvent(new AnalyticEventClaimGiftData(PlayerData, PlayerInventory, type, amount));
            break;
        }
    }

    private AnalyticCurrencyType ConvertItemIDToAnalyticCurrencyType(string itemID)
    {
        if (GameHelper.IsVirtualCurrency(itemID))
        {
            string[] splitText = itemID.Split('_');
            string vcText = splitText[1].ToUpper();
            if (vcText == VirtualCurrency.DI.ToString())
                return AnalyticCurrencyType.diamonds;
            else if (vcText == VirtualCurrency.CO.ToString())
                return AnalyticCurrencyType.coins;
        }
        return AnalyticCurrencyType.nonCurrency;
    }

    private void ShopManager_OnPurchaseCancel(ShopItemData item)
    {
        Manager.LogAnalyticEvent(new AnalyticEventPurchaseCancelData(PlayerData, PlayerInventory, item.ItemId));
    }

    private void OnEnterShop()
    {
        Manager.LogAnalyticEvent(new AnalyticEventEnterShopData(PlayerData, PlayerInventory));
        _enterTimeStamp = DateTimeData.GetDateTimeUTC();
    }

    private void OnQuitShop()
    {
        int seconds = (DateTimeData.GetDateTimeUTC() - _enterTimeStamp).Seconds;
        Manager.LogAnalyticEvent(new AnalyticEventLeaveShopData(PlayerData, PlayerInventory, seconds));
    }
}
