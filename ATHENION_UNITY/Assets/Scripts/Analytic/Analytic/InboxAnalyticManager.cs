﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InboxAnalyticManager : AnalyticBase
{
    public InboxAnalyticManager(AnalyticManager manager) : base(manager)
    {
    }

    public override void Initialize()
    {
        InboxController.OnActive += InboxController_OnActive;
        InboxController.OnOpenMessage += InboxController_OnOpenMessage;
        InboxController.OnReceiveReward += InboxController_OnReceiveReward;
        InboxController.OnDeleteMessage += InboxController_OnDeleteMessage;
        InboxController.OnReceiveAllReward += InboxController_OnReceiveAllReward;
        InboxController.OnDeleteAllMessage += InboxController_OnDeleteAllMessage;
    }

    public override void DeInitialize()
    {
        InboxController.OnActive -= InboxController_OnActive;
        InboxController.OnOpenMessage -= InboxController_OnOpenMessage;
        InboxController.OnReceiveReward -= InboxController_OnReceiveReward;
        InboxController.OnDeleteMessage -= InboxController_OnDeleteMessage;
        InboxController.OnReceiveAllReward -= InboxController_OnReceiveAllReward;
        InboxController.OnDeleteAllMessage -= InboxController_OnDeleteAllMessage;
    }

    private void InboxController_OnActive()
    {
        Manager.LogAnalyticEvent(new AnalyticEventEnterInboxData(PlayerData));
    }

    private void InboxController_OnOpenMessage()
    {
        Manager.LogAnalyticEvent(new AnalyticEventTapMessageData(PlayerData));
    }

    private void InboxController_OnReceiveReward(MessageInboxData data)
    {
        Manager.LogAnalyticEvent(new AnalyticEventReceiveRewardInboxData(PlayerData, data));
    }

    private void InboxController_OnDeleteMessage(MessageInboxData data)
    {
        Manager.LogAnalyticEvent(new AnalyticEventDeleteMessageData(PlayerData, data));
    }

    private void InboxController_OnReceiveAllReward(int messageAmount, List<ItemData> itemDataList)
    {
        Manager.LogAnalyticEvent(new AnalyticEventReceiveAllMessageData(PlayerData, messageAmount, itemDataList));
    }

    private void InboxController_OnDeleteAllMessage(int messageAmount)
    {
        Manager.LogAnalyticEvent(new AnalyticEventDeletellMessageData(PlayerData, messageAmount));
    }
}