﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomeAnalyticManager : AnalyticBase
{
    public HomeAnalyticManager(AnalyticManager manager) : base(manager)
    {
    }

    public override void Initialize()
    {
        HomeManager.OnShowCommunity += MenuManager_OnShowCommunity;
        HomeManager.OnClickDiamondBTN += MenuManager_OnClickDiamondBTN;
        MainBanner.OnClickBannerEvent += MainBanner_OnClickBannerEvent;
    }

    public override void DeInitialize()
    {
        HomeManager.OnShowCommunity -= MenuManager_OnShowCommunity;
        HomeManager.OnClickDiamondBTN -= MenuManager_OnClickDiamondBTN;
        MainBanner.OnClickBannerEvent -= MainBanner_OnClickBannerEvent;
    }

    private void MenuManager_OnClickDiamondBTN()
    {
        Manager.LogAnalyticEvent(new AnalyticEventTapAddDiamondsData(PlayerData, DataManager.Instance.InventoryData, Time.realtimeSinceStartup));
    }

    private void MenuManager_OnShowCommunity()
    {
        Manager.LogAnalyticEvent(new AnalyticEventTapCommunityData(PlayerData, Time.realtimeSinceStartup));
    }

    private void MainBanner_OnClickBannerEvent(BannerData banner)
    {
        string bannerName = banner.ImagePath.Replace("Images/BannerPopup/", "");
        Manager.LogAnalyticEvent(new AnalyticEventTapBannerData(PlayerData, bannerName, Time.realtimeSinceStartup));
    }

}
