﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProfileAnalyticManager : AnalyticBase
{
    public ProfileAnalyticManager(AnalyticManager manager) : base(manager)
    {
    }

    public override void Initialize()
    {
        ProfileEditorManager.OnActive += ProfileEditorManager_OnActive;
        ProfileEditorManager.OnChangeName += DataManager_OnChangeName;
        DataManager.OnChangeTitle += DataManager_OnChangeTitle;
        DataManager.OnChangeAvatar += DataManager_OnChangeAvatar;
        DataManager.OnCompleteAchievement += DataManager_OnCompleteAchievement;
        DataManager.OnCompleteAchievementCardSet += DataManager_OnCompleteAchievementCardSet;
        DataManager.OnPlayerLevelFactionIncrease += DataManager_OnPlayerLevelFactionIncrease;
        DataManager.OnSeasonUpdate += DataManager_OnSeasonUpdate;
    }

    public override void DeInitialize()
    {
        ProfileEditorManager.OnActive -= ProfileEditorManager_OnActive;
        ProfileEditorManager.OnChangeName -= DataManager_OnChangeName;
        DataManager.OnChangeTitle -= DataManager_OnChangeTitle;
        DataManager.OnChangeAvatar -= DataManager_OnChangeAvatar;
        DataManager.OnCompleteAchievement -= DataManager_OnCompleteAchievement;
        DataManager.OnCompleteAchievementCardSet -= DataManager_OnCompleteAchievementCardSet;
        DataManager.OnPlayerLevelFactionIncrease -= DataManager_OnPlayerLevelFactionIncrease;
        DataManager.OnSeasonUpdate -= DataManager_OnSeasonUpdate;
    }

    private void ProfileEditorManager_OnActive()
    {
        Manager.LogAnalyticEvent(new AnalyticEventEnterProfileData(PlayerData));
    }

    private void DataManager_OnChangeName(EditNameInfo info)
    {
        Manager.LogAnalyticEvent(new AnalyticEventChangeNameData(PlayerData, info));
    }

    private void DataManager_OnChangeTitle(string oldTitle, string newTitle)
    {
        Manager.LogAnalyticEvent(new AnalyticEventChangeTitleData(PlayerData, oldTitle, newTitle));
    }

    private void DataManager_OnChangeAvatar(string oldAvatar, string newAvatar)
    {
        Manager.LogAnalyticEvent(new AnalyticEventChangeAvatarData(PlayerData, oldAvatar, newAvatar));
    }

    private void DataManager_OnCompleteAchievement(AchievementData achData)
    {
        Manager.LogAnalyticEvent(new AnalyticEventCompletedAchievementData(PlayerData, achData));
    }

    private void DataManager_OnCompleteAchievementCardSet(string cardsetID)
    {
        Manager.LogAnalyticEvent(new AnalyticEventCompletedCardSetData(PlayerData, cardsetID));
    }

    private void DataManager_OnPlayerLevelFactionIncrease(CardElementType faction)
    {
        Manager.LogAnalyticEvent(new AnalyticEventFactionLevelUpData(PlayerData, faction));
    }

    private void DataManager_OnSeasonUpdate()
    {
        Manager.LogAnalyticEvent(new AnalyticEventCompletedSeasonRankData(PlayerData, PlayerInventory));
    }
}