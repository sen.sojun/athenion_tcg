﻿using GameEvent;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEventAnalyticManager : AnalyticBase
{
    public GameEventAnalyticManager(AnalyticManager manager) : base(manager)
    {
    }

    public override void Initialize()
    {
        EventManager.OnInit += GameEventManager_OnInit;
        GameEventManager.OnSelectEvent += GameEventManager_OnSelectEvent;
        GameEventManager.OnReceiveRewards += GameEventManager_OnReceiveRewards;
        GameEventManager.OnCompleteEvent += GameEventManager_OnCompleteEvent;
    }

    public override void DeInitialize()
    {
        EventManager.OnInit -= GameEventManager_OnInit;
        GameEventManager.OnSelectEvent -= GameEventManager_OnSelectEvent;
        GameEventManager.OnReceiveRewards -= GameEventManager_OnReceiveRewards;
        GameEventManager.OnCompleteEvent -= GameEventManager_OnCompleteEvent;
    }

    private void GameEventManager_OnInit()
    {
        Manager.LogAnalyticEvent(new AnalyticEventEnterInGameEventData(PlayerData));
    }

    private void GameEventManager_OnSelectEvent(string eventKey)
    {
        Manager.LogAnalyticEvent(new AnalyticEventPlayInGameEventData(PlayerData, eventKey));
    }

    private void GameEventManager_OnCompleteEvent(string eventKey)
    {
        Manager.LogAnalyticEvent(new AnalyticEventCompleteInGameEventData(PlayerData, eventKey));
    }

    private void GameEventManager_OnReceiveRewards(string eventKey, List<ItemData> items)
    {
        Manager.LogAnalyticEvent(new AnalyticEventReceiveRewardInGameEventData(PlayerData, items, eventKey));
    }
}
