﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarExchangeAnalyticManager : AnalyticBase
{
    public StarExchangeAnalyticManager(AnalyticManager manager) : base(manager)
    {
    }

    public override void Initialize()
    {
        StarExchangeManager.OnPurchased += StarExchangeManager_OnPurchased;
        StarExchangeManager.OnRefresh += StarExchangeManager_OnRefresh;
    }

    public override void DeInitialize()
    {
        StarExchangeManager.OnPurchased -= StarExchangeManager_OnPurchased;
        StarExchangeManager.OnRefresh -= StarExchangeManager_OnRefresh;
    }

    private void StarExchangeManager_OnPurchased(StarExchangeItem item)
    {
        Manager.LogAnalyticEvent(new AnalyticEventPurchaseStarExchangeData(PlayerData, PlayerInventory, item));
    }

    private void StarExchangeManager_OnRefresh()
    {
        Manager.LogAnalyticEvent(new AnalyticEventRefreshStarExchange(PlayerData, PlayerInventory));
    }

}
