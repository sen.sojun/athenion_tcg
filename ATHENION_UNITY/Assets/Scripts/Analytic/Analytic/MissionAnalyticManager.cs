﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissionAnalyticManager : AnalyticBase
{
    public MissionAnalyticManager(AnalyticManager manager) : base(manager)
    {
    }

    public override void Initialize()
    {
        DataManager.OnMissionComplete += DataManager_OnMissionComplete;
    }

    public override void DeInitialize()
    {
        DataManager.OnMissionComplete -= DataManager_OnMissionComplete;
    }

    private void DataManager_OnMissionComplete(string missionID)
    {
        Manager.LogAnalyticEvent(new AnalyticEventCompletedMissionData(PlayerData, missionID));
    }
}