﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WildOfferAnalyticManager : AnalyticBase
{
    public WildOfferAnalyticManager(AnalyticManager manager) : base(manager)
    {
    }

    public override void Initialize()
    {
        WildOfferManager.OnPurchaseComplete += WildOfferManager_OnPurchaseComplete;
        WildOfferManager.OnOpen += WildOfferManager_OnOpen;
        WildOfferManager.OnClose += WildOfferManager_OnClose;
    }

    public override void DeInitialize()
    {
        WildOfferManager.OnPurchaseComplete -= WildOfferManager_OnPurchaseComplete;
        WildOfferManager.OnOpen -= WildOfferManager_OnOpen;
        WildOfferManager.OnClose -= WildOfferManager_OnClose;
    }

    private void WildOfferManager_OnOpen(ShopItemData item, bool isFirstTime)
    {
        Manager.LogAnalyticEvent(new AnalyticEventOpenWildOfferData(PlayerData, PlayerInventory, item.ItemId, isFirstTime));
    }

    private void WildOfferManager_OnClose(ShopItemData item)
    {
        Manager.LogAnalyticEvent(new AnalyticEventCloseWildOfferData(PlayerData, PlayerInventory, item.ItemId));
    }

    private void WildOfferManager_OnPurchaseComplete(ShopItemData item)
    {
        int useCoin, useDiamond, useRM;
        CalculateUseVCResultFromItem(item, out useCoin, out useDiamond, out useRM);

        int gotDI, gotRF, gotFR;
        CalculateGetVCResultFromItem(item, out gotDI, out gotRF, out gotFR);

        Manager.LogAnalyticEvent(new AnalyticEventPurchaseWildOfferCompletedData(PlayerData, PlayerInventory, item.ItemId, useCoin, useDiamond, useRM, gotDI, gotFR, gotRF));

    }

    private void CalculateUseVCResultFromItem(ShopItemData item, out int useCoin, out int useDiamond, out int useRM)
    {
        useCoin = item.Currency == VirtualCurrency.CO ? item.Price : 0;
        useDiamond = item.Currency == VirtualCurrency.DI ? item.Price : 0;
        useRM = item.Currency == VirtualCurrency.RM ? item.Price : 0;
    }

    private void CalculateGetVCResultFromItem(ShopItemData item, out int gotDI, out int gotRF, out int gotFR)
    {
        gotDI = 0;
        gotRF = 0;
        gotFR = 0;
        List<ItemData> itemList = item.CustomData.ItemDetail;
        for (int i = 0; i < itemList.Count; i++)
        {
            if (GameHelper.IsVirtualCurrency(itemList[i].ItemID))
            {
                string[] splitText = item.ItemId.Split('_');
                string vcText = splitText[1].ToUpper();
                if (vcText == VirtualCurrency.DI.ToString())
                    gotDI += itemList[i].Amount;
                else if (vcText == VirtualCurrency.RF.ToString())
                    gotRF += itemList[i].Amount;
                else if (vcText == VirtualCurrency.FR.ToString())
                    gotFR += itemList[i].Amount;
            }
        }
    }

    private AnalyticCurrencyType ConvertItemIDToAnalyticCurrencyType(string itemID)
    {
        if (GameHelper.IsVirtualCurrency(itemID))
        {
            string[] splitText = itemID.Split('_');
            string vcText = splitText[1].ToUpper();
            if (vcText == VirtualCurrency.DI.ToString())
                return AnalyticCurrencyType.diamonds;
            else if (vcText == VirtualCurrency.CO.ToString())
                return AnalyticCurrencyType.coins;
        }
        return AnalyticCurrencyType.nonCurrency;
    }

}
