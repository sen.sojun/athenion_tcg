﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeasonPassAnalyticManager : AnalyticBase
{
    public SeasonPassAnalyticManager(AnalyticManager manager) : base(manager)
    {
    }

    public override void Initialize()
    {
        DataManager.OnRefreshSeasonPassQuest += DataManager_OnRefreshSeasonPassQuest;
        ShopManager.OnPurchasePremium += OnPurchasePremium;

        SeasonPassManager.OnPurchasePremium += OnPurchasePremium;
        SeasonPassManager.OnPurchaseSeasonPassTier += SeasonPassManager_OnPurchasePassTier;
        SeasonPassManager.OnCompleteQuest += SeasonPassManager_OnCompleteQuest;
        SeasonPassManager.OnCompleteSeasonPassTier += SeasonPassManager_OnCompletePassTier;
    }

    public override void DeInitialize()
    {
        DataManager.OnRefreshSeasonPassQuest -= DataManager_OnRefreshSeasonPassQuest;
        ShopManager.OnPurchasePremium -= OnPurchasePremium;

        SeasonPassManager.OnPurchasePremium -= OnPurchasePremium;
        SeasonPassManager.OnPurchaseSeasonPassTier -= SeasonPassManager_OnPurchasePassTier;
        SeasonPassManager.OnCompleteQuest -= SeasonPassManager_OnCompleteQuest;
        SeasonPassManager.OnCompleteSeasonPassTier -= SeasonPassManager_OnCompletePassTier;
    }

    private void SeasonPassManager_OnCompletePassTier(int lastTier, List<ItemData> rewards)
    {
        Manager.LogAnalyticEvent(new AnalyticEventCompletedTierData(PlayerData, lastTier, rewards));
    }

    private void SeasonPassManager_OnCompleteQuest(string questID)
    {
        Manager.LogAnalyticEvent(new AnalyticEventCompletedQuestData(PlayerData, questID));
    }

    private void SeasonPassManager_OnPurchasePassTier(int tierAmount)
    {
        Manager.LogAnalyticEvent(new AnalyticEventPurchaseTierData(PlayerData, tierAmount));
    }

    private void OnPurchasePremium()
    {
        Manager.LogAnalyticEvent(new AnalyticEventPurchasePremiumData(PlayerData));
    }

    private void DataManager_OnRefreshSeasonPassQuest()
    {
        if(DataManager.Instance.PlayerSeasonPassOld.SeasonPassIndex < DataManager.Instance.PlayerSeasonPass.SeasonPassIndex)
        {
            Manager.LogAnalyticEvent(new AnalyticEventCompletedSeasonPassData(PlayerData));
        }
    }
}