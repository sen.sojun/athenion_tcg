﻿using Karamucho.Collection;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeckAnalyticManager : AnalyticBase
{
    private DateTime _dateTimeStamp;

    public DeckAnalyticManager(AnalyticManager manager) : base(manager)
    {
    }

    public override void Initialize()
    {
        CollectionManager.OnInit += OnEnterDeckScene;
        CollectionManager.OnExitEvent += OnExitDeckScene;
        CollectionManager.OnShowDeckDetail += OnCreateDeck;

        CollectionDeckManager.OnSaveDeck += OnSaveDeck;
        CollectionDeckManager.OnRemoveDeck += OnRemoveDeck;

        CollectionCardManager.OnCraftCardEvent += OnCraftCard;
        CollectionCardManager.OnCraftCardListEvent += OnCraftCardList;
        CollectionCardManager.OnRecycleCardEvent += OnRecycleCard;
        CollectionCardManager.OnRecycleCardListEvent += OnRecycleCardList;
        CollectionCardManager.OnTransformCardEvent += OnTransformCard;
    }

    public override void DeInitialize()
    {
        CollectionManager.OnInit -= OnEnterDeckScene;
        CollectionManager.OnExitEvent -= OnExitDeckScene;
        CollectionManager.OnShowDeckDetail -= OnCreateDeck;

        CollectionDeckManager.OnSaveDeck -= OnSaveDeck;
        CollectionDeckManager.OnRemoveDeck -= OnRemoveDeck;

        CollectionCardManager.OnCraftCardEvent -= OnCraftCard;
        CollectionCardManager.OnCraftCardListEvent -= OnCraftCardList;
        CollectionCardManager.OnRecycleCardEvent -= OnRecycleCard;
        CollectionCardManager.OnRecycleCardListEvent -= OnRecycleCardList;
        CollectionCardManager.OnTransformCardEvent -= OnTransformCard;
    }

    private void OnEnterDeckScene()
    {
        _dateTimeStamp = DateTime.Now;
        Manager.LogAnalyticEvent(new AnalyticEventEnterDeckData(PlayerData));
    }

    private void OnExitDeckScene()
    {
        TimeSpan span = DateTime.Now - _dateTimeStamp;
        Manager.LogAnalyticEvent(new AnalyticEventExitDeckData(PlayerData, float.Parse(span.TotalSeconds.ToString())));
    }

    private void OnCreateDeck(DeckData deckData, bool isNewDeck)
    {
        if (isNewDeck == false)
        {
            return;
        }

        Manager.LogAnalyticEvent(new AnalyticEventCreateDeckData(PlayerData, deckData));
    }

    private void OnSaveDeck(DeckData deck)
    {
        Manager.LogAnalyticEvent(new AnalyticEventSaveDeckData(PlayerData, deck));
    }

    private void OnRemoveDeck(DeckData deck)
    {
        PlayerInventoryData inventory = DataManager.Instance.InventoryData;
        Manager.LogAnalyticEvent(new AnalyticEventDeleteDeckData(PlayerData, inventory, deck));
    }

    private void OnCraftCard(CatalogCardData data)
    {
        PlayerInventoryData inventory = new PlayerInventoryData(DataManager.Instance.InventoryData);
        Dictionary<VirtualCurrency, int> price = data.CraftPrice;
        foreach (KeyValuePair<VirtualCurrency, int> item in price)
        {
            inventory.AddOrSubstractVirtualCurrency(item.Key.ToString(), item.Value);
        }
        Manager.LogAnalyticEvent(new AnalyticEventCraftCardData(PlayerData, inventory, data));
    }

    private void OnCraftCardList(List<CatalogCardData> dataList)
    {
        PlayerInventoryData inventory = new PlayerInventoryData(DataManager.Instance.InventoryData);
        Dictionary<VirtualCurrency, int> price = new Dictionary<VirtualCurrency, int>();
        foreach(CatalogCardData card in dataList)
        {
            foreach (KeyValuePair<VirtualCurrency, int> item in card.CraftPrice)
            {
                inventory.AddOrSubstractVirtualCurrency(item.Key.ToString(), item.Value);
            }
        }
        Manager.LogAnalyticEvent(new AnalyticEventCraftAllCardData(PlayerData, inventory, dataList));
    }

    private void OnRecycleCard(CatalogCardData data)
    {
        PlayerInventoryData inventory = new PlayerInventoryData(DataManager.Instance.InventoryData);
        Dictionary<VirtualCurrency, int> price = data.RecyclePrice;
        foreach (KeyValuePair<VirtualCurrency, int> item in price)
        {
            inventory.AddOrSubstractVirtualCurrency(item.Key.ToString(), -item.Value);
        }
        Manager.LogAnalyticEvent(new AnalyticEventDestroyCardData(PlayerData, inventory, data));
    }

    private void OnRecycleCardList(List<CatalogCardData> dataList)
    {
        PlayerInventoryData inventory = new PlayerInventoryData(DataManager.Instance.InventoryData);
        Dictionary<VirtualCurrency, int> price = new Dictionary<VirtualCurrency, int>();
        foreach (CatalogCardData card in dataList)
        {
            foreach (KeyValuePair<VirtualCurrency, int> item in card.RecyclePrice)
            {
                inventory.AddOrSubstractVirtualCurrency(item.Key.ToString(), -item.Value);
            }
        }
        Manager.LogAnalyticEvent(new AnalyticEventDestroyAllCardData(PlayerData, inventory, dataList));
    }

    private void OnTransformCard(CatalogCardData data)
    {
        PlayerInventoryData inventory = new PlayerInventoryData(DataManager.Instance.InventoryData);
        Dictionary<VirtualCurrency, int> price = data.TransformPrice;
        foreach (KeyValuePair<VirtualCurrency, int> item in price)
        {
            inventory.AddOrSubstractVirtualCurrency(item.Key.ToString(), item.Value);
        }
        Manager.LogAnalyticEvent(new AnalyticEventEnchantCardData(PlayerData, inventory, data));
    }
}