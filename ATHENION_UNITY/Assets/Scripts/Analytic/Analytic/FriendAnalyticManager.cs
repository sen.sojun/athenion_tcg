﻿public class FriendAnalyticManager : AnalyticBase
{
    public FriendAnalyticManager(AnalyticManager manager) : base(manager)
    {
    }

    public override void Initialize()
    {
        FriendController.OnOpenFriendList += FriendController_OnOpenFriendList;
        FriendController.OnTapFriendList += FriendController_OnTapFriendList;
        FriendController.OnTapFriendRequest += FriendController_OnTapFriendRequest;
        FriendController.OnTapAddFriend += FriendController_OnTapAddFriend;
        FriendController.OnTapChat += FriendController_OnTapChat;
        FriendController.OnAddFriend += FriendController_OnAddFriend;
        FriendController.OnAcceptFriend += FriendController_OnAcceptFriend;
        FriendController.OnDeleteFriend += FriendController_OnDeleteFriend;
        DataManager.OnApplyReferral += DataManager_OnApplyReferral;
        DataManager.OnClaimedReferral += DataManager_OnClaimedReferral;
    }

    public override void DeInitialize()
    {
        FriendController.OnOpenFriendList -= FriendController_OnOpenFriendList;
        FriendController.OnTapFriendList -= FriendController_OnTapFriendList;
        FriendController.OnTapFriendRequest -= FriendController_OnTapFriendRequest;
        FriendController.OnTapAddFriend -= FriendController_OnTapAddFriend;
        FriendController.OnTapChat -= FriendController_OnTapChat;
        FriendController.OnAddFriend -= FriendController_OnAddFriend;
        FriendController.OnAcceptFriend -= FriendController_OnAcceptFriend;
        FriendController.OnDeleteFriend -= FriendController_OnDeleteFriend;
        DataManager.OnApplyReferral -= DataManager_OnApplyReferral;
        DataManager.OnClaimedReferral -= DataManager_OnClaimedReferral;
    }

    private void FriendController_OnOpenFriendList()
    {
        Manager.LogAnalyticEvent(new AnalyticEventEnterFriendData(PlayerData));
    }

    private void FriendController_OnTapFriendList()
    {
        Manager.LogAnalyticEvent(new AnalyticEventTapFriendListData(PlayerData));
    }

    private void FriendController_OnTapFriendRequest()
    {
        Manager.LogAnalyticEvent(new AnalyticEventTapFriendRequestData(PlayerData));
    }

    private void FriendController_OnTapAddFriend()
    {
        Manager.LogAnalyticEvent(new AnalyticEventTapAddFriendData(PlayerData));
    }

    private void FriendController_OnTapChat()
    {
        Manager.LogAnalyticEvent(new AnalyticEventTapChatData(PlayerData));
    }

    private void FriendController_OnAddFriend(AddFriendType type, string friendID, string friendName)
    {
        Manager.LogAnalyticEvent(new AnalyticEventAddFriendData(PlayerData, friendID, friendName, type));
    }

    private void FriendController_OnAcceptFriend(PlayerInfoData friendPlayerData)
    {
        Manager.LogAnalyticEvent(new AnalyticEventAcceptFriendData(PlayerData, friendPlayerData));
    }

    private void FriendController_OnDeleteFriend(PlayerInfoData friendPlayerData)
    {
        Manager.LogAnalyticEvent(new AnalyticEventDeleteFriendData(PlayerData, friendPlayerData));
    }

    private void DataManager_OnClaimedReferral()
    {
        Manager.LogAnalyticEvent(new AnalyticEventClaimReferalData(PlayerData));
    }

    private void DataManager_OnApplyReferral()
    {
        Manager.LogAnalyticEvent(new AnalyticEventApplyReferalData(PlayerData));
    }
}