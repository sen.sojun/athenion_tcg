﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeaveGameAnalyticManager : AnalyticBase
{ 
    public LeaveGameAnalyticManager(AnalyticManager manager) : base(manager)
    {
    }

    public override void Initialize()
    { 
        AnalyticManager.OnQuitGame += AnalyticManager_OnQuitGame;
    }

    public override void DeInitialize()
    { 
        AnalyticManager.OnQuitGame -= AnalyticManager_OnQuitGame;
    }
     
    private void AnalyticManager_OnQuitGame()
    {
        Manager.LogAnalyticEvent(new AnalyticEventLeaveGameData(PlayerData, PlayerData.LastActivity.ToString().ToLower(), Time.realtimeSinceStartup));
    }

}
