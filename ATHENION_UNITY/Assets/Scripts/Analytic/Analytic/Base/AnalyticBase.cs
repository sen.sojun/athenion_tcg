﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AnalyticBase
{
    protected AnalyticManager Manager { get; private set; }
    protected PlayerInfoData PlayerData { get { return DataManager.Instance.PlayerInfo; } }
    protected PlayerInventoryData PlayerInventory { get { return DataManager.Instance.InventoryData; } }

    public AnalyticBase(AnalyticManager manager)
    {
        this.Manager = manager;
    }

    public abstract void Initialize();
    public abstract void DeInitialize();

    public virtual void OnApplicationPause(bool pause) { }
    public virtual void OnApplicationQuit() { }
}
