﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameAnalyticsSDK;
using PlayFab;
using PlayFab.ClientModels;
using Firebase.Analytics;
using System;
using Newtonsoft.Json;

#region Global Enums
public enum AnalyticGameMode
{
    none = 0
    , tutorial_1
    , tutorial_2
    , tutorial_3
    , tutorial_4
    , practice_easy
    , practice_normal
    , practice_hard
    , casual_1vs1
    , ranked_1vs1
    , friendly_1vs1
    , event_1vs1
    , arena_1vs1
}

public enum AnalyticElementType
{
    none = 0
    , neutral
    , flame
    , tidal
    , divine
    , terra
    , shadow
    , cyclone
}

public enum AnalyticSlotPosition
{
    none = -1
    , a1 = 0    // 1A
    , b1 = 1    // 1B
    , c1 = 2    // 1C
    , d1 = 3    // 1D
    , a2 = 4    // 2A
    , b2 = 5    // 2B
    , c2 = 6    // 2C
    , d2 = 7    // 2D
    , a3 = 8    // 3A
    , b3 = 9    // 3B
    , c3 = 10   // 3C
    , d3 = 11   // 3D
    , a4 = 12   // 4A
    , b4 = 13   // 4B
    , c4 = 14   // 4C
    , d4 = 15   // 4D
}

public enum AnalyticBoolean
{
    NO
    , YES
}

public enum AnalyticPlayOrder
{
    first
    , second
}

public enum AnalyticPlayerRank
{
    librarian_4
    , librarian_3
    , librarian_2
    , librarian_1
    , athenia_4
    , athenia_3
    , athenia_2
    , athenia_1
    , divine_4
    , divine_3
    , divine_2
    , divine_1
    , cosmicBorn
    , highBorn
    , secondBorn
    , firstBorn
}

public enum AnalyticMatchResult
{
    win
    , lose
}

public enum AnalyticMatchResultCondition
{
    others
    , normal
    , player_surrender
    , opponent_surrender
    , opponent_left
    , player_left
}

public enum AnalyticAbilityTrigger
{
    awaken
    , passive
    , lastWish
    , aura
    , link
    , backstab
    , berserk
    , darkPower
}

public enum AnalyticAbilityAction
{
    atkBuff
    , hpBuff
    , atkHpBuff
    , soulBuff
    , armorBuff
    , arrowBuff
    , atkDebuff
    , hpDebuff
    , atkHpDebuff
    , soulDebuff
    , armorDebuff
    , arrowDebuff
    , dealDamage
    , destroyUnit
    , mulligan
    , taunt
    , freeze
    , moveFriendly
    , moveEnemy
    , healUnit
    , healHero
    , silence
    , darkMatter
    , banish
    , stealth
    , immune
    , summon
    , untargetable
    , draw
    , swapPosition
    , swapStats
    , doubleAttack
    , gainAP
    , lockTile
}

public enum AnalyticCurrencyType
{
    nonCurrency
    , coins
    , diamonds
}

public enum AnalyticCardPackName
{
    none
    , warOfElements
    , grandMaster
}

public enum AnalyticSceneName
{
    shop
    , inventory
    , mainMenu
    , inGameEvent
    , ktPlay
    , profile
    , friend
    , quest
    , deck
    , setting
    , inbox
    , eventBattleLobby
    , practiceLobby
    , casualLobby
    , rankedLobby
    , friendlyLobby
    , eventBattlePlay
    , practicePlay
    , casualPlay
    , rankedPlay
    , friendlyPlay
}

public enum AnalyticAddFriendSource
{
    by_name
    , by_recently
}

public enum AnalyticSeasnPassType
{
    free
    , premium
}

public enum AnalyticQuestType
{
    daily
    , weekly
    , week
    , challenge
}
#endregion

#region Analytic Data

#region Base Structure

#region AnalyticEventData
public abstract class AnalyticEventData
{
    #region Enums
    protected enum EventNameTypes
    {
        none

        // In-Game
        , card_ability
        , card_play
        , card_summon
        , card_destroy
        , card_attack
        , start_turn
        , end_turn
        , rehand
        , start_match
        , end_match
        , start_tutorial
        , end_tutorial

        // Out-Game
        , purchase_completed
        , purchase_failed
        , purchase_cancel
        , daily_deal_completed
        , weekly_deal_completed
        , daily_deal_failed
        , enter_shop
        , leave_shop
        , claim_gift

        , open_pack
        , tap_buyMore_pack
        , tap_goToShop_pack
        , tap_skip_open_pack

        , enter_deck
        , create_deck
        , save_deck
        , cancel_deck
        , craft_card
        , enchant_card
        , destroy_card
        , delete_deck
        , exit_deck
        , craft_all
        , destroy_all

        , enter_profile
        , change_name
        , change_title
        , change_avatar
        , completed_achievement
        , completed_cardset
        , faction_levelup
        , completed_seasonRank

        , completed_mission

        , completed_seasonPass
        , purchase_tier
        , purchase_premium
        , completed_quest
        , completed_tier

        , enter_inGameEvent
        , play_inGameEvent
        , completed_inGameEvent
        , receive_reward_inGameEvent

        , enter_friend
        , tap_friendList
        , tap_friendRequest
        , tap_addFriend
        , tap_chat
        , add_friend
        , accept_friend
        , delete_friend
        , apply_referal
        , claim_referal

        , enter_inbox
        , tap_message
        , receive_reward_inbox
        , delete_message
        , receive_all
        , delete_all

        , tap_banner
        , tap_addDiamonds
        , tap_community

        , redeem_completed
        , refresh_starMarket

        // WildOffer
        , purchase_wildOffer
        , close_wildOffer
        , open_wildOffer

        , leave_game
    }

    private enum DataType
    {
        Decimal
        , Floating
        , Text
    }
    #endregion

    #region Public Properties
    public string EventName { get { return _eventNameType.ToString(); } }
    #endregion

    #region Protected Properties
    protected abstract EventNameTypes _eventNameType { get; }
    protected PlayerInfoData _playerData { get; }
    protected PlayerInventoryData _inventoryData { get; }

    protected string _playerID;
    protected string _playerName;
    protected int _playerLevel;
    protected string _playerRank;

    protected int _playerCO;
    protected int _playerDI;
    protected int _playerFR;
    protected int _playerRF;
    protected int _playerSC;
    protected int _playerSS;
    protected int _playerS1;

    protected Dictionary<string, object> _paramDataList = new Dictionary<string, object>();
    #endregion

    #region Private Properties
    private Dictionary<Type, DataType> _typeDataList = new Dictionary<Type, DataType>()
    {
        { typeof(int), DataType.Decimal }
        , { typeof(uint), DataType.Decimal }
        , { typeof(float), DataType.Floating }
        , { typeof(string), DataType.Text }
    };
    #endregion

    #region Contructors
    public AnalyticEventData(PlayerInfoData playerData)
    {
        this._playerData = playerData;

        _playerID = playerData.PlayerID;
        _playerName = playerData.DisplayName;
        _playerLevel = playerData.LevelData.Level;
        _playerRank = AnalyticManager.ConvertToAnalyticPlayerRank(playerData.GetPlayerCurrentRank()).ToString();
    }

    public AnalyticEventData(PlayerInfoData playerData, PlayerInventoryData inventoryData) : this(playerData)
    {
        this._inventoryData = inventoryData;

        _playerCO = inventoryData.GetVirtualCurrency(VirtualCurrency.CO);
        _playerDI = inventoryData.GetVirtualCurrency(VirtualCurrency.DI);
        _playerFR = inventoryData.GetVirtualCurrency(VirtualCurrency.FR);
        _playerRF = inventoryData.GetVirtualCurrency(VirtualCurrency.RF);
        _playerSC = inventoryData.GetVirtualCurrency(VirtualCurrency.SC);
        _playerSS = inventoryData.GetVirtualCurrency(VirtualCurrency.SS);
        _playerS1 = inventoryData.GetVirtualCurrency(VirtualCurrency.S1);
    }
    #endregion

    #region Set Methods
    protected virtual void SetParamDataList()
    {
        _paramDataList = new Dictionary<string, object>();

        // Build Version
        _paramDataList.Add("build_version", GameHelper.GetVersionText());

        // Player Info Data
        _paramDataList.Add("player_id", _playerID);
        _paramDataList.Add("player_name", _playerName);
        _paramDataList.Add("player_lv", _playerLevel);
        _paramDataList.Add("player_rank", _playerRank);

        // Player Virtual Currency Data
        if (_inventoryData != null)
        {
            _paramDataList.Add("player_coins", _playerCO);
            _paramDataList.Add("player_diamonds", _playerDI);
            _paramDataList.Add("player_fragments", _playerFR);
            _paramDataList.Add("player_rainbowFragments", _playerRF);
        }
    }
    #endregion

    #region Get Methods
    public Dictionary<string, object> GetPlayFabDictionary()
    {
        if (_paramDataList == null || _paramDataList.Count == 0)
        {
            SetParamDataList();
        }

        return new Dictionary<string, object>(_paramDataList);
    }

    public Parameter[] GetFirebaseParameters()
    {
        if (_paramDataList == null || _paramDataList.Count == 0)
        {
            SetParamDataList();
        }

        List<Parameter> paramList = new List<Parameter>();
        Type type;
        DataType dataType;

        foreach (KeyValuePair<string, object> param in _paramDataList)
        {
            type = param.Value.GetType();
            dataType = DataType.Text;
            if (_typeDataList.ContainsKey(type))
            {
                dataType = _typeDataList[type];
            }

            switch (dataType)
            {
                case DataType.Decimal:
                    {
                        paramList.Add(new Parameter(param.Key, long.Parse(param.Value.ToString())));
                    }
                    break;

                case DataType.Floating:
                    {
                        paramList.Add(new Parameter(param.Key, double.Parse(param.Value.ToString())));
                    }
                    break;

                case DataType.Text:
                    {
                        paramList.Add(new Parameter(param.Key, param.Value.ToString()));
                    }
                    break;
            }
        }
        return paramList.ToArray();
    }
    #endregion

    #region DEBUG
    public string GetDebugText()
    {
        string detailStr = string.Empty;
        if (_paramDataList == null || _paramDataList.Count == 0)
        {
            SetParamDataList();
        }

        detailStr = JsonConvert.SerializeObject(_paramDataList);
        string result = string.Format(
            "<color=orange>[Analytic]</color> {0} : {1}"
            , EventName
            , detailStr
        );

        return result;
    }
    #endregion
}
#endregion

#endregion

#region In-Game

#region AnalyticEventCardAbilityData
public class AnalyticEventCardAbilityData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.card_ability;
    #region Private Properties 
    private string _gameMode;
    private string _playerFaction;

    private string _cardID;
    private string _cardName;
    private int _cardCost;
    private string _cardFaction;

    /*-----------------------------*/
    // TODO: Complete CardAbility Data
    //private string _abilityTrigger;
    //private string _abilityAction;
    /*-----------------------------*/

    private string _slotPosition;
    private int _turn;
    #endregion

    #region Contructors
    public AnalyticEventCardAbilityData(
          BattlePlayerData battlePlayerData
        , BattleCardData cardData
        , GameMode gameMode
        , BotLevel level
        , int turn) : base(battlePlayerData.PlayerInfo)
    {
        // player data 
        _gameMode = AnalyticManager.ConvertToAnalyticGameMode(gameMode, level).ToString();
        _playerFaction = AnalyticManager.ConvertToAnalyticElementType(battlePlayerData.Hero.ElementType).ToString();

        // card data
        _cardID = cardData.FullID;
        _cardName = LocalizationManager.Instance.GetCardName(cardData.BaseID, Language.English);
        if (cardData.Type == CardType.Minion || cardData.Type == CardType.Minion_NotInDeck)
        {
            MinionData minion = cardData as MinionData;
            _cardCost = minion.SpiritCurrent;
        }
        _cardFaction = AnalyticManager.ConvertToAnalyticElementType(cardData.Element.ToCardElementType()).ToString();

        /*----------------------------
        TODO: CardAbility Data
        ----------------------------*/

        // others
        _slotPosition = AnalyticManager.ConvertToAnalyticSlotPosition(cardData.SlotID).ToString();
        _turn = turn;
    }

    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("game_mode", _gameMode);
        _paramDataList.Add("player_faction", _playerFaction);
        _paramDataList.Add("card_id", _cardID);
        _paramDataList.Add("card_name", _cardName);
        _paramDataList.Add("card_cost", _cardCost);
        _paramDataList.Add("card_faction", _cardFaction);
        /*----------------------------
        TODO: CardAbility Data
        ----------------------------*/
        _paramDataList.Add("position", _slotPosition);
        _paramDataList.Add("turns", _turn);
    }
    #endregion
}
#endregion

#region AnalyticEventCardPlayData
public class AnalyticEventCardPlayData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.card_play;
    #region Private Properties 
    private string _gameMode;
    private string _playerFaction;

    private string _cardID;
    private string _cardName;
    private int _cardCost;
    private string _cardFaction;

    private string _slotPosition;
    private int _turn;
    #endregion

    #region Contructors
    public AnalyticEventCardPlayData(
          BattlePlayerData battlePlayerData
        , BattleCardData cardData
        , GameMode gameMode
        , BotLevel level
        , int turn) : base(battlePlayerData.PlayerInfo)
    {
        // player data 
        _gameMode = AnalyticManager.ConvertToAnalyticGameMode(gameMode, level).ToString();
        _playerFaction = AnalyticManager.ConvertToAnalyticElementType(battlePlayerData.Hero.ElementType).ToString();

        // card data
        _cardID = cardData.FullID;
        _cardName = LocalizationManager.Instance.GetCardName(cardData.BaseID, Language.English);
        if (cardData.Type == CardType.Minion || cardData.Type == CardType.Minion_NotInDeck)
        {
            MinionData minion = cardData as MinionData;
            _cardCost = minion.SpiritCurrent;
        }
        _cardFaction = AnalyticManager.ConvertToAnalyticElementType(cardData.Element.ToCardElementType()).ToString();

        // others
        _slotPosition = AnalyticManager.ConvertToAnalyticSlotPosition(cardData.SlotID).ToString();
        _turn = turn;
    }

    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        _paramDataList = new Dictionary<string, object>();
        _paramDataList.Add("player_id", _playerID);
        _paramDataList.Add("player_name", _playerName);
        _paramDataList.Add("game_mode", _gameMode);
        _paramDataList.Add("player_faction", _playerFaction);
        _paramDataList.Add("card_id", _cardID);
        _paramDataList.Add("card_name", _cardName);
        _paramDataList.Add("card_cost", _cardCost);
        _paramDataList.Add("card_faction", _cardFaction);
        _paramDataList.Add("position", _slotPosition);
        _paramDataList.Add("turns", _turn);
    }
    #endregion
}
#endregion

#region AnalyticEventCardSummonData
public class AnalyticEventCardSummonData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.card_summon;
    #region Private Properties 
    private string _gameMode;
    private string _playerFaction;

    private string _cardID;
    private string _cardName;
    private int _cardCost;
    private string _cardFaction;

    private string _slotPosition;
    private int _turn;
    #endregion

    #region Contructors
    public AnalyticEventCardSummonData(
          BattlePlayerData battlePlayerData
        , BattleCardData cardData
        , GameMode gameMode
        , BotLevel level
        , int turn) : base(battlePlayerData.PlayerInfo)
    {
        // player data 
        _gameMode = AnalyticManager.ConvertToAnalyticGameMode(gameMode, level).ToString();
        _playerFaction = AnalyticManager.ConvertToAnalyticElementType(battlePlayerData.Hero.ElementType).ToString();

        // card data
        _cardID = cardData.FullID;
        _cardName = LocalizationManager.Instance.GetCardName(cardData.BaseID, Language.English);
        if (cardData.Type == CardType.Minion || cardData.Type == CardType.Minion_NotInDeck)
        {
            MinionData minion = cardData as MinionData;
            _cardCost = minion.SpiritCurrent;
        }
        _cardFaction = AnalyticManager.ConvertToAnalyticElementType(cardData.Element.ToCardElementType()).ToString();

        // others
        _slotPosition = AnalyticManager.ConvertToAnalyticSlotPosition(cardData.SlotID).ToString();
        _turn = turn;
    }

    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("game_mode", _gameMode);
        _paramDataList.Add("player_faction", _playerFaction);
        _paramDataList.Add("card_id", _cardID);
        _paramDataList.Add("card_name", _cardName);
        _paramDataList.Add("card_cost", _cardCost);
        _paramDataList.Add("card_faction", _cardFaction);
        _paramDataList.Add("position", _slotPosition);
        _paramDataList.Add("turns", _turn);
    }
    #endregion
}
#endregion

#region AnalyticEventCardDestroyData
public class AnalyticEventCardDestroyData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.card_destroy;
    #region Private Properties 
    private string _gameMode;
    private string _playerFaction;

    private string _cardID;
    private string _cardName;
    private int _cardCost;
    private string _cardFaction;

    private string _slotPosition;
    private int _turn;
    #endregion

    #region Contructors
    public AnalyticEventCardDestroyData(
          BattlePlayerData battlePlayerData
        , BattleCardData cardData
        , GameMode gameMode
        , BotLevel level
        , int turn) : base(battlePlayerData.PlayerInfo)
    {
        // player data 
        _gameMode = AnalyticManager.ConvertToAnalyticGameMode(gameMode, level).ToString();
        _playerFaction = AnalyticManager.ConvertToAnalyticElementType(battlePlayerData.Hero.ElementType).ToString();

        // card data
        _cardID = cardData.FullID;
        _cardName = LocalizationManager.Instance.GetCardName(cardData.BaseID, Language.English);
        if (cardData.Type == CardType.Minion || cardData.Type == CardType.Minion_NotInDeck)
        {
            MinionData minion = cardData as MinionData;
            _cardCost = minion.SpiritCurrent;
        }
        _cardFaction = AnalyticManager.ConvertToAnalyticElementType(cardData.Element.ToCardElementType()).ToString();

        // others
        _slotPosition = AnalyticManager.ConvertToAnalyticSlotPosition(cardData.SlotID).ToString();
        _turn = turn;
    }

    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("game_mode", _gameMode);
        _paramDataList.Add("player_faction", _playerFaction);
        _paramDataList.Add("card_id", _cardID);
        _paramDataList.Add("card_name", _cardName);
        _paramDataList.Add("card_cost", _cardCost);
        _paramDataList.Add("card_faction", _cardFaction);
        _paramDataList.Add("position", _slotPosition);
        _paramDataList.Add("turns", _turn);
    }
    #endregion
}
#endregion

#region AnalyticEventCardAttackData
public class AnalyticEventCardAttackData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.card_attack;
    #region Private Properties 
    private string _gameMode;
    private string _playerFaction;

    private string _cardID;
    private string _cardName;
    private int _cardCost;
    private string _cardFaction;

    private string _slotPosition;
    private int _turn;
    private int _damage;
    private string _isBackstab;
    #endregion

    #region Contructors
    public AnalyticEventCardAttackData(
          BattlePlayerData battlePlayerData
        , BattleCardData cardData
        , GameMode gameMode
        , BotLevel level
        , int turn
        , int damage
        , bool isBackstab) : base(battlePlayerData.PlayerInfo)
    {
        // player data 
        _gameMode = AnalyticManager.ConvertToAnalyticGameMode(gameMode, level).ToString();
        _playerFaction = AnalyticManager.ConvertToAnalyticElementType(battlePlayerData.Hero.ElementType).ToString();

        // card data
        _cardID = cardData.FullID;
        _cardName = LocalizationManager.Instance.GetCardName(cardData.BaseID, Language.English);
        if (cardData.Type == CardType.Minion || cardData.Type == CardType.Minion_NotInDeck)
        {
            MinionData minion = cardData as MinionData;
            _cardCost = minion.SpiritCurrent;
        }
        _cardFaction = AnalyticManager.ConvertToAnalyticElementType(cardData.Element.ToCardElementType()).ToString();

        // others
        _slotPosition = AnalyticManager.ConvertToAnalyticSlotPosition(cardData.SlotID).ToString();
        _turn = turn;
        _damage = damage;
        _isBackstab = AnalyticManager.ConvertToAnalyticBoolean(isBackstab).ToString();
    }

    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("game_mode", _gameMode);
        _paramDataList.Add("player_faction", _playerFaction);
        _paramDataList.Add("card_id", _cardID);
        _paramDataList.Add("card_name", _cardName);
        _paramDataList.Add("card_cost", _cardCost);
        _paramDataList.Add("card_faction", _cardFaction);
        _paramDataList.Add("position", _slotPosition);
        _paramDataList.Add("turns", _turn);
        _paramDataList.Add("damage", _damage);
        _paramDataList.Add("isBackstab", _isBackstab);
    }
    #endregion
}
#endregion

#region AnalyticEventStartTurnData
public class AnalyticEventStartTurnData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.start_turn;
    #region Private Properties 
    private string _gameMode;
    private string _playerFaction;

    private int _turn;
    private int _playerHP;
    private int _playerSP;
    private int _playerCardGraveyard;
    private int _drawAmount;
    private int _playerUnitCount;
    private int _opponentUnitCount;
    private int _allUnitCount;
    private float _timePassInSecond;
    #endregion

    #region Contructors
    public AnalyticEventStartTurnData(
         BattleData battleData
        , PlayerIndex playerIndex
        , GameMode gameMode
        , BotLevel level
        , int playerSumSpirit
        , int turn
        , int drawAmount
        , float timePassInSecond) : base(battleData.PlayerDataList[(int)playerIndex].PlayerInfo)
    {
        BattlePlayerData battlePlayerData = battleData.PlayerDataList[(int)playerIndex];
        PlayerInfoData playerData = battlePlayerData.PlayerInfo;

        // player data 
        _gameMode = AnalyticManager.ConvertToAnalyticGameMode(gameMode, level).ToString();
        _playerFaction = AnalyticManager.ConvertToAnalyticElementType(battlePlayerData.Hero.ElementType).ToString();

        // others
        _playerHP = battlePlayerData.CurrentHP;
        _playerCardGraveyard = battlePlayerData.CardList[CardZone.Graveyard].Count;
        _playerUnitCount = battlePlayerData.CardList[CardZone.Battlefield].Count;
        _opponentUnitCount = battleData.GetAllMinionsInSlot().Count - _playerUnitCount;
        _allUnitCount = battleData.GetAllMinionsInSlot().Count;

        _turn = turn;
        _playerSP = playerSumSpirit;
        _drawAmount = drawAmount;
        _timePassInSecond = timePassInSecond;
    }

    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("game_mode", _gameMode);
        _paramDataList.Add("player_faction", _playerFaction);
        _paramDataList.Add("turns", _turn);
        _paramDataList.Add("player_hp", _playerHP);
        _paramDataList.Add("player_soul", _playerSP);
        _paramDataList.Add("player_graveyard", _playerCardGraveyard);
        _paramDataList.Add("draw_amount", _drawAmount);
        _paramDataList.Add("unit_player", _playerUnitCount);
        _paramDataList.Add("unit_opponent", _opponentUnitCount);
        _paramDataList.Add("unit_all", _allUnitCount);
        _paramDataList.Add("time_session", _timePassInSecond);
    }
    #endregion
}
#endregion

#region AnalyticEventEndTurnData
public class AnalyticEventEndTurnData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.end_turn;
    #region Private Properties 
    private string _gameMode;
    private string _playerFaction;

    private int _turn;
    private int _playerHP;
    private int _playerSP;
    private int _playerCardHand;
    private int _playerAP;
    private int _playerCardGraveyard;
    private int _playerUnitCount;
    private int _opponentUnitCount;
    private int _allUnitCount;
    private int _allUnitAttackCount;
    private float _timeTurnTakenInSecond;
    #endregion

    #region Contructors
    public AnalyticEventEndTurnData(
          BattleData battleData
        , PlayerIndex playerIndex
        , GameMode gameMode
        , BotLevel level
        , int playerSumSpirit
        , int turn
        , int allUnitAttackCount
        , float timeTurnTakenInSecond) : base(battleData.PlayerDataList[(int)playerIndex].PlayerInfo)
    {
        BattlePlayerData battlePlayerData = battleData.PlayerDataList[(int)playerIndex];
        PlayerInfoData playerData = battlePlayerData.PlayerInfo;

        // player data 
        _gameMode = AnalyticManager.ConvertToAnalyticGameMode(gameMode, level).ToString();
        _playerFaction = AnalyticManager.ConvertToAnalyticElementType(battlePlayerData.Hero.ElementType).ToString();

        // others
        _turn = turn;
        _playerHP = battlePlayerData.CurrentHP;
        _playerSP = playerSumSpirit;
        _playerCardHand = battlePlayerData.CardList[CardZone.Hand].Count;
        _playerAP = battlePlayerData.CurrentAP;
        _playerCardGraveyard = battlePlayerData.CardList[CardZone.Graveyard].Count;
        _playerUnitCount = battlePlayerData.CardList[CardZone.Battlefield].Count;
        _opponentUnitCount = battleData.GetAllMinionsInSlot().Count - _playerUnitCount;
        _allUnitCount = battleData.GetAllMinionsInSlot().Count;
        _allUnitAttackCount = allUnitAttackCount;
        _timeTurnTakenInSecond = timeTurnTakenInSecond;
    }

    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("game_mode", _gameMode);
        _paramDataList.Add("player_faction", _playerFaction);
        _paramDataList.Add("turns", _turn);
        _paramDataList.Add("player_hp", _playerHP);
        _paramDataList.Add("player_soul", _playerSP);
        _paramDataList.Add("player_cards", _playerCardHand);
        _paramDataList.Add("player_ap", _playerAP);
        _paramDataList.Add("player_graveyard", _playerCardGraveyard);
        _paramDataList.Add("unit_player", _playerUnitCount);
        _paramDataList.Add("unit_opponent", _opponentUnitCount);
        _paramDataList.Add("unit_all", _allUnitCount);
        _paramDataList.Add("unit_attack_all", _allUnitAttackCount);
        _paramDataList.Add("time_taken", _timeTurnTakenInSecond);
    }
    #endregion
}
#endregion

#region AnalyticEventRehandData
public class AnalyticEventRehandData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.rehand;
    #region Private Properties 
    private string _gameMode;
    private string _playerFaction;

    private int _amount;
    private string _playerOrder;
    private int _timeLeft;
    #endregion

    #region Contructors
    public AnalyticEventRehandData(
          BattlePlayerData battlePlayerData
        , GameMode gameMode
        , BotLevel level
        , int amount
        , bool isPlayFirst
        , int timeLeft) : base(battlePlayerData.PlayerInfo)
    {
        // player data 
        _gameMode = AnalyticManager.ConvertToAnalyticGameMode(gameMode, level).ToString();
        _playerFaction = AnalyticManager.ConvertToAnalyticElementType(battlePlayerData.Hero.ElementType).ToString();

        // others
        _amount = amount;
        _playerOrder = AnalyticManager.ConvertToAnalyticPlayOrder(isPlayFirst).ToString();
        _timeLeft = timeLeft;
    }

    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("game_mode", _gameMode);
        _paramDataList.Add("player_faction", _playerFaction);
        _paramDataList.Add("amount", _amount);
        _paramDataList.Add("play_order", _playerOrder);
        _paramDataList.Add("time_left", _timeLeft);
    }
    #endregion
}
#endregion

#region AnalyticEventStartMatchData
public class AnalyticEventStartMatchData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.start_match;
    #region Private Properties 
    private string _gameMode;
    private string _playerFaction;
    private string _playerHeroID;
    private string _playerHeroName;
    private float _playerHiddenScore;
    private string _playerAvatar;
    private string _playerTitle;
    private string _playerCardBack;

    private string _optID;
    private string _optName;
    private string _optFaction;
    private string _optHeroID;
    private string _optHeroName;
    private string _optRank;
    private float _optHiddenScore;

    private string _isBot;
    private string _matchID;
    #endregion

    #region Contructors
    public AnalyticEventStartMatchData(
          BattleData battleData
        , PlayerIndex playerIndex
        , PlayerIndex opponentIndex
        , GameMode gameMode
        , BotLevel level
        , bool isBot
        , string matchID) : base(battleData.PlayerDataList[(int)playerIndex].PlayerInfo)
    {
        BattlePlayerData battlePlayerData = battleData.PlayerDataList[(int)playerIndex];
        PlayerInfoData playerData = battlePlayerData.PlayerInfo;

        // player data 
        _gameMode = AnalyticManager.ConvertToAnalyticGameMode(gameMode, level).ToString();
        _playerFaction = AnalyticManager.ConvertToAnalyticElementType(battlePlayerData.Hero.ElementType).ToString();
        _playerHeroID = battlePlayerData.Hero.ID;
        _playerHeroName = LocalizationManager.Instance.GetHeroName(battlePlayerData.Hero.ID, Language.English);
        _playerRank = AnalyticManager.ConvertToAnalyticPlayerRank(playerData.GetPlayerCurrentRank()).ToString();
        _playerHiddenScore = playerData.GetPlayerCasualHiddenScore();
        _playerAvatar = LocalizationManager.Instance.GetItemName(playerData.AvatarID, Language.English);
        _playerTitle = LocalizationManager.Instance.GetItemName(playerData.TitleID, Language.English);
        _playerCardBack = DataManager.Instance.GetCurrentDeck().CardBackID;

        battlePlayerData = battleData.PlayerDataList[(int)opponentIndex];
        playerData = battlePlayerData.PlayerInfo;
        _optID = playerData.PlayerID;
        _optName = playerData.DisplayName;
        _optFaction = AnalyticManager.ConvertToAnalyticElementType(battlePlayerData.Hero.ElementType).ToString();
        _optHeroID = battlePlayerData.Hero.ID;
        _optHeroName = LocalizationManager.Instance.GetHeroName(battlePlayerData.Hero.ID, Language.English);
        _optRank = AnalyticManager.ConvertToAnalyticPlayerRank(playerData.GetPlayerCurrentRank()).ToString();
        _optHiddenScore = playerData.GetPlayerCasualHiddenScore();

        _isBot = AnalyticManager.ConvertToAnalyticBoolean(isBot).ToString();
        _matchID = matchID;
    }

    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("game_mode", _gameMode);
        _paramDataList.Add("player_faction", _playerFaction);
        _paramDataList.Add("player_hero_id", _playerHeroID);
        _paramDataList.Add("player_hero_name", _playerHeroName);
        _paramDataList.Add("player_hidden_score", _playerHiddenScore);
        _paramDataList.Add("player_avatar", _playerAvatar);
        _paramDataList.Add("player_title", _playerTitle);
        _paramDataList.Add("player_cardBack", _playerCardBack);
        _paramDataList.Add("opponent_id", _optID);
        _paramDataList.Add("opponent_name", _optName);
        _paramDataList.Add("opponent_faction", _optFaction);
        _paramDataList.Add("opponent_hero_id", _optHeroID);
        _paramDataList.Add("opponent_hero_name", _optHeroName);
        _paramDataList.Add("opponent_rank", _optRank);
        _paramDataList.Add("opponent_hidden_score", _optHiddenScore);
        _paramDataList.Add("vsBot", _isBot);
        _paramDataList.Add("match_id", _matchID);
    }
    #endregion
}
#endregion

#region AnalyticEventEndMatchData
public class AnalyticEventEndMatchData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.end_match;
    #region Private Properties 
    private string _gameMode;
    private string _playerFaction;
    private int _playerDeck;
    private int _playerSP;
    private int _playerHP;
    private int _playerGraveyard;

    private string _optID;
    private string _optName;
    private string _optFaction;
    private string _optRank;
    private int _optDeck;
    private int _optSP;
    private int _optHP;
    private int _optGraveyard;

    private string _isBot;
    private string _matchID;
    private string _matchResult;
    private string _matchResultCondition;
    private string _playOrder;
    private float _timeSession;
    private int _unitBattlefield;
    private int _turnMain;
    #endregion

    #region Contructors
    public AnalyticEventEndMatchData(
          BattleData battleData
        , PlayerIndex playerIndex
        , PlayerIndex opponentIndex
        , GameMode gameMode
        , BotLevel level
        , int playerSumSpirit
        , int opponentSumSpirit
        , bool isBot
        , string matchID
        , bool isVictory
        , GameManager.EndGameType endResult
        , bool isPlayFirst
        , float timeSession
        , int turnMain) : base(battleData.PlayerDataList[(int)playerIndex].PlayerInfo)
    {
        BattlePlayerData battlePlayerData = battleData.PlayerDataList[(int)playerIndex];
        PlayerInfoData playerData = _playerData;

        _gameMode = AnalyticManager.ConvertToAnalyticGameMode(gameMode, level).ToString();
        _playerFaction = AnalyticManager.ConvertToAnalyticElementType(battlePlayerData.Hero.ElementType).ToString();
        _playerDeck = battlePlayerData.DeckCount;
        _playerSP = playerSumSpirit;
        _playerHP = battlePlayerData.CurrentHP;
        _playerGraveyard = battlePlayerData.CardList[CardZone.Graveyard].Count;

        BattlePlayerData battlePlayerDataOpt = battleData.PlayerDataList[(int)opponentIndex];
        playerData = battlePlayerDataOpt.PlayerInfo;
        _optID = playerData.PlayerID;
        _optName = playerData.DisplayName;
        _optFaction = AnalyticManager.ConvertToAnalyticElementType(battlePlayerDataOpt.Hero.ElementType).ToString();
        _optRank = AnalyticManager.ConvertToAnalyticPlayerRank(playerData.GetPlayerCurrentRank()).ToString();
        _optDeck = battlePlayerDataOpt.DeckCount;
        _optSP = opponentSumSpirit;
        _optHP = battlePlayerDataOpt.CurrentHP;
        _optGraveyard = battlePlayerDataOpt.CardList[CardZone.Graveyard].Count;

        _isBot = AnalyticManager.ConvertToAnalyticBoolean(isBot).ToString();
        _matchID = matchID;
        _matchResult = AnalyticManager.ConvertToAnalyticMatchResult(isVictory).ToString();
        _matchResultCondition = AnalyticManager.ConvertToAnalyticMatchResultCondition(endResult).ToString();
        _playOrder = AnalyticManager.ConvertToAnalyticPlayOrder(isPlayFirst).ToString();
        _timeSession = timeSession;
        _unitBattlefield = battlePlayerData.CardList[CardZone.Battlefield].Count + battlePlayerDataOpt.CardList[CardZone.Battlefield].Count;
        _turnMain = turnMain;
    }

    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("game_mode", _gameMode);
        _paramDataList.Add("player_faction", _playerFaction);
        _paramDataList.Add("player_deck", _playerDeck);
        _paramDataList.Add("player_soul", _playerSP);
        _paramDataList.Add("player_hp", _playerHP);
        _paramDataList.Add("player_graveyard", _playerGraveyard);

        _paramDataList.Add("opponent_id", _optID);
        _paramDataList.Add("opponent_name", _optName);
        _paramDataList.Add("opponent_faction", _optFaction);
        _paramDataList.Add("opponent_rank", _optRank);
        _paramDataList.Add("opponent_deck", _optDeck);
        _paramDataList.Add("opponent_soul", _optSP);
        _paramDataList.Add("opponent_hp", _optHP);
        _paramDataList.Add("opponent_graveyard", _optGraveyard);

        _paramDataList.Add("vsBot", _isBot);
        _paramDataList.Add("match_id", _matchID);
        _paramDataList.Add("match_result", _matchResult);
        _paramDataList.Add("match_result_condition", _matchResultCondition);
        _paramDataList.Add("play_order", _playOrder);
        _paramDataList.Add("time_session", _timeSession);
        _paramDataList.Add("unit_all", _unitBattlefield);
        _paramDataList.Add("turns", _turnMain);
    }
    #endregion
}
#endregion

#region AnalyticEventStartTutorialData
public class AnalyticEventStartTutorialData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.start_tutorial;
    #region Private Properties 
    private string _tutorialMatch;
    private string _isReplay;
    private string _wasCompleted;
    private string _wasCompletedAll;
    #endregion

    #region Contructors
    public AnalyticEventStartTutorialData(
          BattleData battleData
        , PlayerIndex playerIndex
        , GameMode gameMode
        , bool isReplay
        , bool isAlreadyCompleted
        , bool isAlreadyCompletedAll) : base(battleData.PlayerDataList[(int)playerIndex].PlayerInfo)
    {
        BattlePlayerData battlePlayerData = battleData.PlayerDataList[(int)playerIndex];
        PlayerInfoData playerData = battlePlayerData.PlayerInfo;

        _tutorialMatch = AnalyticManager.ConvertToAnalyticGameMode(gameMode).ToString();
        _isReplay = AnalyticManager.ConvertToAnalyticBoolean(isReplay).ToString();
        _wasCompleted = AnalyticManager.ConvertToAnalyticBoolean(isAlreadyCompleted).ToString();
        _wasCompletedAll = AnalyticManager.ConvertToAnalyticBoolean(isAlreadyCompletedAll).ToString();
    }

    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("tutorial_match", _tutorialMatch);
        _paramDataList.Add("isReplay", _isReplay);
        _paramDataList.Add("wasCompleted", _wasCompleted);
        _paramDataList.Add("wasCompleted_all", _wasCompletedAll);
    }
    #endregion
}
#endregion

#region AnalyticEventEndTutorialData
public class AnalyticEventEndTutorialData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.end_tutorial;
    #region Private Properties 
    private string _tutorialMatch;
    private int _playerSP;
    private int _playerHP;
    private int _playerDeck;
    private int _playerGraveyard;

    private int _optSP;
    private int _optHP;
    private int _optDeck;
    private int _optGraveyard;

    private string _matchResult;
    private string _matchResultCondition;
    private float _timeSession;
    private int _unitBattlefield;
    private int _turnMain;
    #endregion

    #region Contructors
    public AnalyticEventEndTutorialData(
          BattleData battleData
        , PlayerIndex playerIndex
        , PlayerIndex opponentIndex
        , GameMode gameMode
        , int playerSumSpirit
        , int opponentSumSpirit
        , bool isVictory
        , string endResult
        , float timeSession
        , int turnMain) : base(battleData.PlayerDataList[(int)playerIndex].PlayerInfo)
    {
        BattlePlayerData battlePlayerData = battleData.PlayerDataList[(int)playerIndex];
        PlayerInfoData playerData = battlePlayerData.PlayerInfo;

        _tutorialMatch = AnalyticManager.ConvertToAnalyticGameMode(gameMode).ToString();
        _playerSP = playerSumSpirit;
        _playerHP = battlePlayerData.CurrentHP;
        _playerDeck = battlePlayerData.DeckCount;
        _playerGraveyard = battlePlayerData.CardList[CardZone.Graveyard].Count;

        BattlePlayerData battlePlayerDataOpt = battleData.PlayerDataList[(int)opponentIndex];
        _optSP = opponentSumSpirit;
        _optHP = battlePlayerDataOpt.CurrentHP;
        _optDeck = battlePlayerDataOpt.DeckCount;
        _optGraveyard = battlePlayerDataOpt.CardList[CardZone.Graveyard].Count;

        _matchResult = AnalyticManager.ConvertToAnalyticMatchResult(isVictory).ToString();
        _matchResultCondition = endResult;
        _timeSession = timeSession;
        _unitBattlefield = battlePlayerData.CardList[CardZone.Battlefield].Count + battlePlayerDataOpt.CardList[CardZone.Battlefield].Count;
        _turnMain = turnMain;
    }

    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("tutorial_match", _tutorialMatch);
        _paramDataList.Add("player_soul", _playerSP);
        _paramDataList.Add("player_hp", _playerHP);
        _paramDataList.Add("player_deck", _playerDeck);
        _paramDataList.Add("player_graveyard", _playerGraveyard);

        _paramDataList.Add("opponent_deck", _optDeck);
        _paramDataList.Add("opponent_soul", _optSP);
        _paramDataList.Add("opponent_hp", _optHP);
        _paramDataList.Add("opponent_graveyard", _optGraveyard);

        _paramDataList.Add("match_result", _matchResult);
        _paramDataList.Add("match_result_condition", _matchResultCondition);
        _paramDataList.Add("unit_all", _unitBattlefield);
        _paramDataList.Add("time_session", _timeSession);
        _paramDataList.Add("turns", _turnMain);
    }
    #endregion
}
#endregion

#endregion

#region WildOffer

#region AnalyticEventPurchaseWildOfferCompletedData
public class AnalyticEventPurchaseWildOfferCompletedData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.purchase_wildOffer;
    #region Private Properties 
    private string _itemID;
    private string _itemName;
    private int _sinkCO;
    private int _sinkDI;
    private int _sinkRM;
    private int _sourceDI;
    private int _sourceFR;
    private int _sourceRF;
    #endregion

    #region Contructors
    public AnalyticEventPurchaseWildOfferCompletedData(
          PlayerInfoData playerData
        , PlayerInventoryData inventoryData
        , string itemID
        , int usedCoin
        , int usedDiamond
        , int usedRealMoney
        , int gotDiamond
        , int gotFragment
        , int gotRainbowFragment) : base(playerData, inventoryData)
    {
        _playerID = playerData.PlayerID;
        _playerName = playerData.DisplayName;
        _playerLevel = playerData.LevelData.Level;
        _playerRank = AnalyticManager.ConvertToAnalyticPlayerRank(playerData.GetPlayerCurrentRank()).ToString();
        _itemID = itemID;
        _itemName = LocalizationManager.Instance.GetItemName(_itemID, Language.English);
        _sinkCO = usedCoin;
        _sinkDI = usedDiamond;
        _sinkRM = usedRealMoney;
        _sourceDI = gotDiamond;
        _sourceFR = gotFragment;
        _sourceRF = gotRainbowFragment;
    }

    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("item_id", _itemID);
        _paramDataList.Add("item_name", _itemName);
        _paramDataList.Add("sink_coins", _sinkCO);
        _paramDataList.Add("sink_diamonds", _sinkDI);
        _paramDataList.Add("sink_realMoney", _sinkRM);
        _paramDataList.Add("source_diamonds", _sourceDI);
        _paramDataList.Add("source_fragments", _sourceFR);
        _paramDataList.Add("source_rainbowFragments", _sourceRF);
    }
    #endregion
}
#endregion

#region AnalyticEventCloseWildOfferData 
public class AnalyticEventCloseWildOfferData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.close_wildOffer;
    #region Private Properties 
    private string _itemID;
    private string _itemName;
    #endregion

    #region Contructors
    public AnalyticEventCloseWildOfferData(PlayerInfoData playerData, PlayerInventoryData playerInventory, string itemID) : base(playerData, playerInventory)
    {
        _itemID = itemID;
        _itemName = LocalizationManager.Instance.GetItemName(_itemID, Language.English);
    }

    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("item_id", _itemID);
        _paramDataList.Add("item_name", _itemName);
    }
    #endregion
}
#endregion

#region AnalyticEventOpenWildOfferData 
public class AnalyticEventOpenWildOfferData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.open_wildOffer;
    #region Private Properties 
    private string _itemID;
    private string _itemName;
    private bool _firstTimeOpen;
    #endregion

    #region Contructors
    public AnalyticEventOpenWildOfferData(PlayerInfoData playerData, PlayerInventoryData playerInventory, string itemID, bool firstTimeOpen) : base(playerData, playerInventory)
    {
        _itemID = itemID;
        _firstTimeOpen = firstTimeOpen;
        _itemName = LocalizationManager.Instance.GetItemName(_itemID, Language.English);
    }

    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("item_id", _itemID);
        _paramDataList.Add("item_name", _itemName);
        _paramDataList.Add("firstTime_open", _firstTimeOpen);
    }
    #endregion
}
#endregion

#endregion

#region Shop

#region AnalyticEventPurchaseCompletedData
public class AnalyticEventPurchaseCompletedData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.purchase_completed;
    #region Private Properties 
    private string _itemID;
    private string _itemName;
    private int _sinkCO;
    private int _sinkDI;
    private int _sinkRM;
    private int _sourceDI;
    private int _sourceFR;
    private int _sourceRF;
    #endregion

    #region Contructors
    public AnalyticEventPurchaseCompletedData(
          PlayerInfoData playerData
        , PlayerInventoryData inventoryData
        , string itemID
        , int usedCoin
        , int usedDiamond
        , int usedRealMoney
        , int gotDiamond
        , int gotFragment
        , int gotRainbowFragment) : base(playerData, inventoryData)
    {
        _playerID = playerData.PlayerID;
        _playerName = playerData.DisplayName;
        _playerLevel = playerData.LevelData.Level;
        _playerRank = AnalyticManager.ConvertToAnalyticPlayerRank(playerData.GetPlayerCurrentRank()).ToString();
        _itemID = itemID;
        _itemName = LocalizationManager.Instance.GetItemName(_itemID, Language.English);
        _sinkCO = usedCoin;
        _sinkDI = usedDiamond;
        _sinkRM = usedRealMoney;
        _sourceDI = gotDiamond;
        _sourceFR = gotFragment;
        _sourceRF = gotRainbowFragment;
    }

    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("item_id", _itemID);
        _paramDataList.Add("item_name", _itemName);
        _paramDataList.Add("sink_coins", _sinkCO);
        _paramDataList.Add("sink_diamonds", _sinkDI);
        _paramDataList.Add("sink_realMoney", _sinkRM);
        _paramDataList.Add("source_diamonds", _sourceDI);
        _paramDataList.Add("source_fragments", _sourceFR);
        _paramDataList.Add("source_rainbowFragments", _sourceRF);
    }
    #endregion
}
#endregion

#region AnalyticEventPurchaseFailedData
public class AnalyticEventPurchaseFailedData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.purchase_failed;
    #region Private Properties 
    private string _itemID;
    private string _itemName;
    private int _sinkCO;
    private int _sinkDI;
    private int _sinkRM;
    private int _sourceDI;
    private int _sourceFR;
    private int _sourceRF;
    #endregion

    #region Contructors
    public AnalyticEventPurchaseFailedData(
          PlayerInfoData playerData
        , PlayerInventoryData inventoryData
        , string itemID
        , int usedCoin
        , int usedDiamond
        , int usedRealMoney
        , int gotDiamond
        , int gotFragment
        , int gotRainbowFragment) : base(playerData, inventoryData)
    {
        _playerID = playerData.PlayerID;
        _playerName = playerData.DisplayName;
        _playerLevel = playerData.LevelData.Level;
        _playerRank = AnalyticManager.ConvertToAnalyticPlayerRank(playerData.GetPlayerCurrentRank()).ToString();
        _itemID = itemID;
        _itemName = LocalizationManager.Instance.GetItemName(_itemID, Language.English);
        _sinkCO = usedCoin;
        _sinkDI = usedDiamond;
        _sinkRM = usedRealMoney;
        _sourceDI = gotDiamond;
        _sourceFR = gotFragment;
        _sourceRF = gotRainbowFragment;
    }
    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("item_id", _itemID);
        _paramDataList.Add("item_name", _itemName);
        _paramDataList.Add("sink_coins", _sinkCO);
        _paramDataList.Add("sink_diamonds", _sinkDI);
        _paramDataList.Add("sink_realMoney", _sinkRM);
        _paramDataList.Add("source_diamonds", _sourceDI);
        _paramDataList.Add("source_fragments", _sourceFR);
        _paramDataList.Add("source_rainbowFragments", _sourceRF);
    }
    #endregion
}
#endregion

#region AnalyticEventPurchaseCancelData
public class AnalyticEventPurchaseCancelData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.purchase_cancel;
    #region Private Properties 
    private string _itemID;
    private string _itemName;
    #endregion

    #region Contructors
    public AnalyticEventPurchaseCancelData(PlayerInfoData playerData, PlayerInventoryData inventoryData, string itemID) : base(playerData, inventoryData)
    {
        _itemID = itemID;
        _itemName = LocalizationManager.Instance.GetItemName(_itemID, Language.English);
    }

    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("item_id", _itemID);
        _paramDataList.Add("item_name", _itemName);
    }
    #endregion
}
#endregion

#region AnalyticEventDailyDealCompletedData
public class AnalyticEventWeeklyDealCompletedData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.weekly_deal_completed;
    #region Private Properties 
    private string _dealID;
    private string _dealName;
    private int _sinkCO;
    private int _sinkDI;
    private int _sinkRM;
    private int _sourceDI;
    private int _sourceFR;
    private int _sourceRF;
    #endregion

    #region Contructors
    public AnalyticEventWeeklyDealCompletedData(
          PlayerInfoData playerData
        , PlayerInventoryData inventoryData
        , string itemID
        , int usedCoin
        , int usedDiamond
        , int usedRealMoney
        , int gotDiamond
        , int gotFragment
        , int gotRainbowFragment) : base(playerData, inventoryData)
    {
        _dealID = itemID;
        _dealName = LocalizationManager.Instance.GetItemName(GameHelper.ConvertItemIDToItemKey(itemID), Language.English);
        _sinkCO = usedCoin;
        _sinkDI = usedDiamond;
        _sinkRM = usedRealMoney;
        _sourceDI = gotDiamond;
        _sourceFR = gotFragment;
        _sourceRF = gotRainbowFragment;
    }

    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("deal_id", _dealID);
        _paramDataList.Add("deal_name", _dealName);
        _paramDataList.Add("sink_coins", _sinkCO);
        _paramDataList.Add("sink_diamonds", _sinkDI);
        _paramDataList.Add("sink_realMoney", _sinkRM);
        _paramDataList.Add("source_diamonds", _sourceDI);
        _paramDataList.Add("source_fragments", _sourceFR);
        _paramDataList.Add("source_rainbowFragments", _sourceRF);
    }
    #endregion
}
#endregion

#region AnalyticEventDailyDealCompletedData
public class AnalyticEventDailyDealCompletedData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.daily_deal_completed;
    #region Private Properties 
    private string _dealID;
    private string _dealName;
    private int _sinkCO;
    private int _sinkDI;
    private int _sinkRM;
    private int _sourceDI;
    private int _sourceFR;
    private int _sourceRF;
    #endregion

    #region Contructors
    public AnalyticEventDailyDealCompletedData(
          PlayerInfoData playerData
        , PlayerInventoryData inventoryData
        , string itemID
        , int usedCoin
        , int usedDiamond
        , int usedRealMoney
        , int gotDiamond
        , int gotFragment
        , int gotRainbowFragment) : base(playerData, inventoryData)
    {
        _dealID = itemID;
        _dealName = LocalizationManager.Instance.GetItemName(GameHelper.ConvertItemIDToItemKey(itemID), Language.English);
        _sinkCO = usedCoin;
        _sinkDI = usedDiamond;
        _sinkRM = usedRealMoney;
        _sourceDI = gotDiamond;
        _sourceFR = gotFragment;
        _sourceRF = gotRainbowFragment;
    }

    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("deal_id", _dealID);
        _paramDataList.Add("deal_name", _dealName);
        _paramDataList.Add("sink_coins", _sinkCO);
        _paramDataList.Add("sink_diamonds", _sinkDI);
        _paramDataList.Add("sink_realMoney", _sinkRM);
        _paramDataList.Add("source_diamonds", _sourceDI);
        _paramDataList.Add("source_fragments", _sourceFR);
        _paramDataList.Add("source_rainbowFragments", _sourceRF);
    }
    #endregion
}
#endregion

#region AnalyticEventDailyDealCompletedData
public class AnalyticEventDailyDealFailedData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.daily_deal_failed;
    #region Private Properties 
    private string _dealID;
    private string _dealName;
    private int _sinkCO;
    private int _sinkDI;
    private int _sinkRM;
    private int _sourceDI;
    private int _sourceFR;
    private int _sourceRF;
    #endregion

    #region Contructors
    public AnalyticEventDailyDealFailedData(
          PlayerInfoData playerData
        , PlayerInventoryData inventoryData
        , string itemID
        , int usedCoin
        , int usedDiamond
        , int usedRealMoney
        , int gotDiamond
        , int gotFragment
        , int gotRainbowFragment) : base(playerData, inventoryData)
    {
        _dealID = itemID;
        _dealName = LocalizationManager.Instance.GetItemName(GameHelper.ConvertItemIDToItemKey(itemID), Language.English);
        _sinkCO = usedCoin;
        _sinkDI = usedDiamond;
        _sinkRM = usedRealMoney;
        _sourceDI = gotDiamond;
        _sourceFR = gotFragment;
        _sourceRF = gotRainbowFragment;
    }

    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("deal_id", _dealID);
        _paramDataList.Add("deal_name", _dealName);
        _paramDataList.Add("sink_coins", _sinkCO);
        _paramDataList.Add("sink_diamonds", _sinkDI);
        _paramDataList.Add("sink_realMoney", _sinkRM);
        _paramDataList.Add("source_diamonds", _sourceDI);
        _paramDataList.Add("source_fragments", _sourceFR);
        _paramDataList.Add("source_rainbowFragments", _sourceRF);
    }
    #endregion
}
#endregion

#region AnalyticEventEnterShopData
public class AnalyticEventEnterShopData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.enter_shop;
    #region Private Properties 
    #endregion

    #region Contructors
    public AnalyticEventEnterShopData(PlayerInfoData playerData, PlayerInventoryData inventoryData) : base(playerData, inventoryData)
    {
    }

    #endregion

    #region Set Methods 
    #endregion
}
#endregion

#region AnalyticEventLeaveShopData
public class AnalyticEventLeaveShopData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.leave_shop;
    #region Private Properties 
    private float _timeSession;
    #endregion

    #region Contructors
    public AnalyticEventLeaveShopData(
          PlayerInfoData playerData
        , PlayerInventoryData inventoryData
        , float timeSpentSecond) : base(playerData, inventoryData)
    {
        _timeSession = timeSpentSecond;
    }

    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("time_session", _timeSession);
    }
    #endregion
}
#endregion

#region AnalyticEventClaimGiftData
public class AnalyticEventClaimGiftData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.claim_gift;

    #region Private Properties 
    private string _sourceCurrency;
    private int _sourceAmount;
    #endregion

    #region Contructors
    public AnalyticEventClaimGiftData(PlayerInfoData playerData, PlayerInventoryData inventoryData, AnalyticCurrencyType sourceCurrency, int sourceAmount) : base(playerData, inventoryData)
    {
        _sourceCurrency = sourceCurrency.ToString();
        _sourceAmount = sourceAmount;
    }

    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("source_currency", _sourceCurrency);
        _paramDataList.Add("source_amount", _sourceAmount);
    }
    #endregion
}
#endregion

#endregion

#region Pack

#region AnalyticEventOpenPackData
public class AnalyticEventOpenPackData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.open_pack;

    #region Private Properties 
    private string _packID;
    private string _packName;
    private int _packAmount;

    private int _commonAmount = 0;
    private int _rareAmount = 0;
    private int _epicAmount = 0;
    private int _legendaryAmount = 0;
    private int _commonFoilAmount = 0;
    private int _rareFoilAmount = 0;
    private int _epicFoilAmount = 0;
    private int _legendaryFoilAmount = 0;
    private int _heroAmount = 0;

    private int _frangmentAmount = 0;
    private int _rainbowFragmentAmount = 0;
    private int _circleSignAmount = 0;
    private int _squareSignAmount = 0;
    private int _novaSignAmount = 0;
    #endregion

    #region Contructors
    public AnalyticEventOpenPackData(PlayerInfoData playerData, PlayerInventoryData inventoryData, PackReward pack) : base(playerData, inventoryData)
    {
        _packID = pack.PackID;
        _packName = AnalyticManager.ConvertToAnalyticCardPackName(_packID).ToString();
        _packAmount = pack.Amount;

        List<CardData> cardList = pack.GetAllCardDataList();
        CountCardRarity(cardList);

        CountCurrencyReward(pack.GetAllCurrencyRewards());
        _heroAmount = pack.BonusReward.Count;
    }

    private void CountCurrencyReward(List<CurrencyReward> list)
    {
        for (int i = 0; i < list.Count; i++)
        {
            switch (list[i].ItemID)
            {
                case "VC_FR":
                    _frangmentAmount += list[i].Amount;
                    break;
                case "VC_RF":
                    _rainbowFragmentAmount += list[i].Amount;
                    break;
                case "VC_SC":
                    _circleSignAmount += list[i].Amount;
                    break;
                case "VC_SS":
                    _squareSignAmount += list[i].Amount;
                    break;
                case "VC_S1":
                    _novaSignAmount += list[i].Amount;
                    break;
                default:
                    break;
            }
        }
    }

    private void CountCardRarity(List<CardData> cardList)
    {
        foreach (CardData card in cardList)
        {
            if (card.IsFoil)
            {
                switch (card.Rarity)
                {
                    case CardRarity.Common:
                        _commonFoilAmount++;
                        break;

                    case CardRarity.Rare:
                        _rareFoilAmount++;
                        break;

                    case CardRarity.Epic:
                        _epicFoilAmount++;
                        break;

                    case CardRarity.Legend:
                        _legendaryFoilAmount++;
                        break;
                }
            }
            else
            {
                switch (card.Rarity)
                {
                    case CardRarity.Common:
                        _commonAmount++;
                        break;

                    case CardRarity.Rare:
                        _rareAmount++;
                        break;

                    case CardRarity.Epic:
                        _epicAmount++;
                        break;

                    case CardRarity.Legend:
                        _legendaryAmount++;
                        break;
                }
            }
        }
    }


    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("pack_id", _packID);
        _paramDataList.Add("pack_name", _packName);
        _paramDataList.Add("pack_amount", _packAmount);

        _paramDataList.Add("common_amount", _commonAmount);
        _paramDataList.Add("rare_amount", _rareAmount);
        _paramDataList.Add("epic_amount", _epicAmount);
        _paramDataList.Add("legendary_amount", _legendaryAmount);

        _paramDataList.Add("commonFoil_amount", _commonFoilAmount);
        _paramDataList.Add("rareFoil_amount", _rareFoilAmount);
        _paramDataList.Add("epicFoil_amount", _epicFoilAmount);
        _paramDataList.Add("legendaryFoil_amount", _legendaryFoilAmount);

        _paramDataList.Add("hero_amount", _heroAmount);

        _paramDataList.Add("fragment_amount", _frangmentAmount);
        _paramDataList.Add("rainbowFragment_amount", _rainbowFragmentAmount);
        _paramDataList.Add("circleSign_amount", _circleSignAmount);
        _paramDataList.Add("squareSign_amount", _squareSignAmount);
        _paramDataList.Add("novaSign_amount", _novaSignAmount);

    }
    #endregion
}
#endregion

#region AnalyticEventTapBuyMorePackData
public class AnalyticEventTapBuyMorePackData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.tap_buyMore_pack;

    #region Private Properties  
    private string _packID;
    private string _packName;
    private int _packAmount;

    #endregion

    #region Contructors
    public AnalyticEventTapBuyMorePackData(PlayerInfoData playerData, PlayerInventoryData inventoryData, string packID, int packAmount) : base(playerData, inventoryData)
    {
        _packID = packID;
        _packName = AnalyticManager.ConvertToAnalyticCardPackName(packID).ToString();
        _packAmount = packAmount;
    }
    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("pack_id", _packID);
        _paramDataList.Add("pack_name", _packName);
        _paramDataList.Add("pack_amount", _packAmount);
    }
    #endregion
}
#endregion

#region AnalyticEventTapGoToShopPackData
public class AnalyticEventTapGoToShopPackData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.tap_goToShop_pack;

    #region Contructors
    public AnalyticEventTapGoToShopPackData(PlayerInfoData playerData, PlayerInventoryData inventoryData) : base(playerData, inventoryData)
    {
    }

    #endregion

}
#endregion

#region AnalyticEventSkipOpenPackData
public class AnalyticEventSkipOpenPackData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.tap_skip_open_pack;
    #region Private Properties 
    private string _skipAnimation;

    private string _packID;
    private string _packName;
    private int _packAmount;
    #endregion

    #region Contructors
    public AnalyticEventSkipOpenPackData(PlayerInfoData playerData, PackReward packDetail) : base(playerData)
    {
        _skipAnimation = AnalyticBoolean.YES.ToString();

        _packID = packDetail.PackID;
        _packName = LocalizationManager.Instance.GetItemName(_packID, Language.English);
        _packAmount = packDetail.Amount;
    }
    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("pack_id", _packID);
        _paramDataList.Add("pack_name", _packName);
        _paramDataList.Add("pack_amount", _packAmount);
    }
    #endregion
}
#endregion

#endregion

#region Deck

#region AnalyticEventEnterDeckData
public class AnalyticEventEnterDeckData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.enter_deck;
    #region Private Properties 
    #endregion

    #region Contructors
    public AnalyticEventEnterDeckData(PlayerInfoData playerData) : base(playerData)
    {
    }

    #endregion

}
#endregion

#region AnalyticEventCreateDeckData
public class AnalyticEventCreateDeckData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.create_deck;
    #region Private Properties 
    private string _heroID;
    private string _heroName;
    private string _factionName;
    #endregion

    #region Contructors
    public AnalyticEventCreateDeckData(PlayerInfoData playerData, DeckData deck) : base(playerData)
    {
        _heroID = deck.HeroID;
        _heroName = LocalizationManager.Instance.GetHeroName(_heroID, Language.English);
        deck.GetElementType(out CardElementType elementType);
        _factionName = AnalyticManager.ConvertToAnalyticElementType(elementType).ToString();
    }

    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("hero_id", _heroID);
        _paramDataList.Add("hero_name", _heroName);
        _paramDataList.Add("faction_name", _factionName);
    }
    #endregion
}
#endregion

#region AnalyticEventSaveDeckData
public class AnalyticEventSaveDeckData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.save_deck;
    #region Private Properties 
    private string _heroID;
    private string _heroName;
    private string _factionName;

    private int _cardAmount;
    private int _soul0Amount;
    private int _soul1Amount;
    private int _soul2Amount;
    private int _soul3Amount;
    private int _mainFactionAmount;
    private int _mercenaryAmount;
    #endregion

    #region Contructors
    public AnalyticEventSaveDeckData(PlayerInfoData playerData, DeckData deck) : base(playerData)
    {
        _heroID = deck.HeroID;
        _heroName = LocalizationManager.Instance.GetHeroName(_heroID, Language.English);
        deck.GetElementType(out CardElementType elementType);
        _factionName = AnalyticManager.ConvertToAnalyticElementType(elementType).ToString();

        _cardAmount = deck.Deck.CountAmount;
        foreach (KeyValuePair<string, int> item in deck.Deck)
        {
            CardData card = CardData.CreateCard(item.Key);
            switch (card.Spirit)
            {
                case 0:
                    {
                        _soul0Amount++;
                    }
                    break;

                case 1:
                    {
                        _soul1Amount++;
                    }
                    break;

                case 2:
                    {
                        _soul2Amount++;
                    }
                    break;

                case 3:
                    {
                        _soul3Amount++;
                    }
                    break;
            }

            if (card.CardElement.ToCardElementType() == elementType)
            {
                _mainFactionAmount++;
            }
            else if (card.CardElement.ToCardElementType() == CardElementType.NEUTRAL)
            {
                _mercenaryAmount++;
            }
        }
    }

    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("hero_id", _heroID);
        _paramDataList.Add("hero_name", _heroName);
        _paramDataList.Add("faction_name", _factionName);
        _paramDataList.Add("card_amount", _cardAmount);
        _paramDataList.Add("soul0_amount", _soul0Amount);
        _paramDataList.Add("soul1_amount", _soul1Amount);
        _paramDataList.Add("soul2_amount", _soul2Amount);
        _paramDataList.Add("soul3_amount", _soul3Amount);
        _paramDataList.Add("mainFaction_amount", _mainFactionAmount);
        _paramDataList.Add("mercenary_amount", _mercenaryAmount);
    }
    #endregion
}
#endregion

#region AnalyticEventCancelDeckData
public class AnalyticEventCancelDeckData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.cancel_deck;
    #region Private Properties 
    private string _heroID;
    private string _heroName;
    private string _factionName;

    private int _cardAmount;
    private int _soul0Amount;
    private int _soul1Amount;
    private int _soul2Amount;
    private int _soul3Amount;
    private int _mainFactionAmount;
    private int _mercenaryAmount;
    #endregion

    #region Contructors
    public AnalyticEventCancelDeckData(PlayerInfoData playerData, HeroData heroData, List<CardData> cardList) : base(playerData)
    {
        _playerID = playerData.PlayerID;
        _playerName = playerData.DisplayName;
        _playerLevel = playerData.LevelData.Level;
        _playerRank = AnalyticManager.ConvertToAnalyticPlayerRank(playerData.GetPlayerCurrentRank()).ToString();

        _heroID = heroData.ID;
        _heroName = LocalizationManager.Instance.GetHeroName(_heroID, Language.English);
        _factionName = AnalyticManager.ConvertToAnalyticElementType(heroData.ElementType).ToString();

        _cardAmount = cardList.Count;
        foreach (CardData card in cardList)
        {
            switch (card.Spirit)
            {
                case 0:
                    {
                        _soul0Amount++;
                    }
                    break;

                case 1:
                    {
                        _soul1Amount++;
                    }
                    break;

                case 2:
                    {
                        _soul2Amount++;
                    }
                    break;

                case 3:
                    {
                        _soul3Amount++;
                    }
                    break;
            }

            if (card.CardElement.ToCardElementType() == heroData.ElementType)
            {
                _mainFactionAmount++;
            }
            else if (card.CardElement.ToCardElementType() == CardElementType.NEUTRAL)
            {
                _mercenaryAmount++;
            }
        }
    }

    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("hero_id", _heroID);
        _paramDataList.Add("hero_name", _heroName);
        _paramDataList.Add("faction_name", _factionName);
        _paramDataList.Add("card_amount", _cardAmount);
        _paramDataList.Add("soul0_amount", _soul0Amount);
        _paramDataList.Add("soul1_amount", _soul1Amount);
        _paramDataList.Add("soul2_amount", _soul2Amount);
        _paramDataList.Add("soul3_amount", _soul3Amount);
        _paramDataList.Add("mainFaction_amount", _mainFactionAmount);
        _paramDataList.Add("mercenary_amount", _mercenaryAmount);
    }
    #endregion
}
#endregion

#region AnalyticEventCraftCardData
public class AnalyticEventCraftCardData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.craft_card;
    #region Private Properties 

    private string _cardID;
    private string _cardName;
    private int _sinkFragments;
    private int _sinkRainbowFragments;
    private int _sinkNova;
    #endregion

    #region Contructors
    /// <param name="inventoryData">Use data before the process has been completed</param>
    public AnalyticEventCraftCardData(
          PlayerInfoData playerData
        , PlayerInventoryData inventoryData
        , CatalogCardData cardData) : base(playerData, inventoryData)
    {
        _cardID = cardData.ItemID;
        CardData card = CardData.CreateCard(_cardID);
        _cardName = LocalizationManager.Instance.GetCardName(card.BaseID, Language.English);

        _sinkFragments = cardData.GetCardCraftPrice(VirtualCurrency.FR);
        _sinkRainbowFragments = cardData.GetCardCraftPrice(VirtualCurrency.RF);
        _sinkNova = cardData.GetCardCraftPrice(VirtualCurrency.S1);
    }

    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("player_nova", _playerS1);
        _paramDataList.Add("card_id", _cardID);
        _paramDataList.Add("card_name", _cardName);
        _paramDataList.Add("sink_fragments", _sinkFragments);
        _paramDataList.Add("sink_rainbowFragments", _sinkRainbowFragments);
        _paramDataList.Add("sink_nova", _sinkNova);
    }
    #endregion
}
#endregion

#region AnalyticEventEnchantCardData
public class AnalyticEventEnchantCardData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.enchant_card;
    #region Private Properties 
    private string _cardID;
    private string _cardName;
    private int _sinkRainbowFragments;
    #endregion

    #region Contructors
    /// <param name="inventoryData">Use data before the process has been completed</param>
    public AnalyticEventEnchantCardData(
          PlayerInfoData playerData
        , PlayerInventoryData inventoryData
        , CatalogCardData cardData) : base(playerData, inventoryData)
    {
        _cardID = cardData.ItemID;
        CardData card = CardData.CreateCard(_cardID);
        _cardName = LocalizationManager.Instance.GetCardName(card.BaseID, Language.English);

        _sinkRainbowFragments = cardData.GetCardTransformPrice(VirtualCurrency.RF);
    }

    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("player_nova", _playerS1);
        _paramDataList.Add("card_id", _cardID);
        _paramDataList.Add("card_name", _cardName);
        _paramDataList.Add("sink_rainbowFragments", _sinkRainbowFragments);
    }
    #endregion
}
#endregion

#region AnalyticEventDestroyCardData
public class AnalyticEventDestroyCardData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.destroy_card;
    #region Private Properties 
    private string _cardID;
    private string _cardName;
    private int _sourceFragments;
    private int _sourceRainbowFragments;
    private int _sourceNova;
    #endregion

    #region Contructors
    /// <param name="inventoryData">Use data before the process has been completed</param>
    public AnalyticEventDestroyCardData(PlayerInfoData playerData, PlayerInventoryData inventoryData, CatalogCardData cardData) : base(playerData, inventoryData)
    {
        _cardID = cardData.ItemID;
        CardData card = CardData.CreateCard(_cardID);
        _cardName = LocalizationManager.Instance.GetCardName(card.BaseID, Language.English);

        _sourceFragments = cardData.GetCardRecyclePrice(VirtualCurrency.FR);
        _sourceRainbowFragments = cardData.GetCardRecyclePrice(VirtualCurrency.RF);
        _sourceNova = cardData.GetCardRecyclePrice(VirtualCurrency.S1);
    }
    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("player_nova", _playerS1);
        _paramDataList.Add("card_id", _cardID);
        _paramDataList.Add("card_name", _cardName);
        _paramDataList.Add("source_fragments", _sourceFragments);
        _paramDataList.Add("source_rainbowFragments", _sourceRainbowFragments);
        _paramDataList.Add("source_nova", _sourceNova);
    }
    #endregion
}
#endregion

#region AnalyticEventDeleteDeckData
public class AnalyticEventDeleteDeckData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.delete_deck;
    #region Private Properties 
    private string _heroID;
    private string _heroName;
    private string _factionName;
    #endregion

    #region Contructors
    public AnalyticEventDeleteDeckData(PlayerInfoData playerData, PlayerInventoryData inventoryData, DeckData deck) : base(playerData, inventoryData)
    {
        _heroID = deck.HeroID;
        _heroName = LocalizationManager.Instance.GetHeroName(_heroID, Language.English);
        deck.GetElementType(out CardElementType elementType);
        _factionName = AnalyticManager.ConvertToAnalyticElementType(elementType).ToString();
    }

    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("player_nova", _playerS1);
        _paramDataList.Add("hero_id", _heroID);
        _paramDataList.Add("hero_name", _heroName);
        _paramDataList.Add("faction_name", _factionName);
    }
    #endregion
}
#endregion

#region AnalyticEventExitDeckData
public class AnalyticEventExitDeckData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.exit_deck;

    #region Private Properties 
    private float _timeSession;
    #endregion

    #region Contructors
    public AnalyticEventExitDeckData(PlayerInfoData playerData, float timeSession) : base(playerData)
    {
        _timeSession = timeSession;
    }
    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("time_session", _timeSession);
    }
    #endregion
}
#endregion

#region AnalyticEventCraftAllCardData
public class AnalyticEventCraftAllCardData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.craft_all;
    #region Private Properties 
    private int _cardAmount;
    private int _sinkFragments;
    private int _sinkRainbowFragments;
    private int _sinkNova;
    #endregion

    #region Contructors
    /// <param name="inventoryData">Use data before the process has been completed</param>
    public AnalyticEventCraftAllCardData(
          PlayerInfoData playerData
        , PlayerInventoryData inventoryData
        , List<CatalogCardData> cardDataList) : base(playerData, inventoryData)
    {
        _cardAmount = cardDataList.Count;
        foreach (CatalogCardData card in cardDataList)
        {
            _sinkFragments += card.GetCardCraftPrice(VirtualCurrency.FR);
            _sinkRainbowFragments += card.GetCardCraftPrice(VirtualCurrency.RF);
            _sinkNova += card.GetCardCraftPrice(VirtualCurrency.S1);
        }
    }

    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("player_nova", _playerS1);
        _paramDataList.Add("card_amount", _cardAmount);
        _paramDataList.Add("sink_fragments", _sinkFragments);
        _paramDataList.Add("sink_rainbowFragments", _sinkRainbowFragments);
        _paramDataList.Add("sink_nova", _sinkNova);
    }
    #endregion
}
#endregion

#region AnalyticEventDestroyAllCardData
public class AnalyticEventDestroyAllCardData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.destroy_all;
    #region Private Properties 
    private int _cardAmount;
    private int _sourceFragments;
    private int _sourceRainbowFragments;
    private int _sourceNova;
    #endregion

    #region Contructors
    /// <param name="inventoryData">Use data before the process has been completed</param>
    public AnalyticEventDestroyAllCardData(PlayerInfoData playerData, PlayerInventoryData inventoryData, List<CatalogCardData> cardDataList) : base(playerData, inventoryData)
    {
        _cardAmount = cardDataList.Count;
        foreach (CatalogCardData card in cardDataList)
        {
            _sourceFragments += card.GetCardRecyclePrice(VirtualCurrency.FR);
            _sourceRainbowFragments += card.GetCardRecyclePrice(VirtualCurrency.RF);
            _sourceNova += card.GetCardRecyclePrice(VirtualCurrency.S1);
        }
    }
    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("player_nova", _playerS1);
        _paramDataList.Add("card_amount", _cardAmount);
        _paramDataList.Add("source_fragments", _sourceFragments);
        _paramDataList.Add("source_rainbowFragments", _sourceRainbowFragments);
        _paramDataList.Add("source_nova", _sourceNova);
    }
    #endregion
}
#endregion

#endregion

#region Profile

#region AnalyticEventEnterProfileData
public class AnalyticEventEnterProfileData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.enter_profile;
    #region Private Properties 
    #endregion

    #region Contructors
    public AnalyticEventEnterProfileData(PlayerInfoData playerData) : base(playerData)
    {
    }

    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
    }
    #endregion
}
#endregion

#region AnalyticEventChangeNameData
public class AnalyticEventChangeNameData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.change_name;
    #region Private Properties 
    private int _playerDiamonds;

    private string _playerNewName;
    private int _sinkDiamonds;
    #endregion

    #region Contructors
    public AnalyticEventChangeNameData(PlayerInfoData playerData, EditNameInfo info) : base(playerData)
    {
        _playerDiamonds = DataManager.Instance.InventoryData.GetVirtualCurrency(VirtualCurrency.DI);
        _playerNewName = info.NewName;
        _sinkDiamonds = info.Price;
    }

    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("player_diamonds", _playerDiamonds);
        _paramDataList.Add("player_new_name", _playerNewName);
        _paramDataList.Add("sink_diamonds", _sinkDiamonds);
    }
    #endregion
}
#endregion

#region AnalyticEventChangeTitleData
public class AnalyticEventChangeTitleData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.change_title;
    #region Private Properties 
    private string _playerOldTitle;
    private string _playerNewTitle;
    #endregion

    #region Contructors
    public AnalyticEventChangeTitleData(PlayerInfoData playerData, string oldTitleID, string newTitleID) : base(playerData)
    {
        _playerOldTitle = LocalizationManager.Instance.GetItemName(oldTitleID, Language.English);
        _playerNewTitle = LocalizationManager.Instance.GetItemName(newTitleID, Language.English);
    }

    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("player_title", _playerOldTitle);
        _paramDataList.Add("player_new_title", _playerNewTitle);
    }
    #endregion
}
#endregion

#region AnalyticEventChangeAvatarData
public class AnalyticEventChangeAvatarData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.change_avatar;
    #region Private Properties  
    private string _playerOldAvatar;
    private string _playerNewAvatar;
    #endregion

    #region Contructors
    public AnalyticEventChangeAvatarData(PlayerInfoData playerData, string oldAvatarID, string newAvatarID) : base(playerData)
    {
        _playerOldAvatar = LocalizationManager.Instance.GetItemName(oldAvatarID, Language.English);
        _playerNewAvatar = LocalizationManager.Instance.GetItemName(newAvatarID, Language.English);
    }

    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("player_avatar", _playerOldAvatar);
        _paramDataList.Add("player_new_avatar", _playerNewAvatar);
    }
    #endregion
}
#endregion

#region AnalyticEventCompletedAchievementData
public class AnalyticEventCompletedAchievementData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.completed_achievement;

    #region Private Properties 
    private string _achievementID;
    private string _achievementName;
    private string _rewardName;
    private int _sourceDiamonds;
    private int _sourceCoins;
    private int _sourceFragments;
    private int _sourceRainbowFragments;
    #endregion

    #region Contructors
    public AnalyticEventCompletedAchievementData(PlayerInfoData playerData, AchievementData achievementData) : base(playerData)
    {
        _achievementID = achievementData.ID;
        _achievementName = LocalizationManager.Instance.GetAchievementTitle(_achievementID, Language.English);

        if (achievementData.ItemDataList.Count > 0)
        {
            _rewardName = LocalizationManager.Instance.GetItemName(achievementData.ItemDataList[0].ItemID, Language.English);

            foreach (ItemData item in achievementData.ItemDataList)
            {
                switch (item.ItemID)
                {
                    case "VC_CO":
                        {
                            _sourceCoins += item.Amount;
                        }
                        break;

                    case "VC_DI":
                        {
                            _sourceDiamonds += item.Amount;
                        }
                        break;

                    case "VC_FR":
                        {
                            _sourceFragments += item.Amount;
                        }
                        break;

                    case "VC_RF":
                        {
                            _sourceRainbowFragments += item.Amount;
                        }
                        break;
                }
            }
        }
    }

    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("achievement_id", _achievementID);
        _paramDataList.Add("achievement_name", _achievementName);
        _paramDataList.Add("reward_name", _rewardName);
        _paramDataList.Add("source_diamonds", _sourceDiamonds);
        _paramDataList.Add("source_coins", _sourceCoins);
        _paramDataList.Add("source_fragments", _sourceFragments);
        _paramDataList.Add("source_rainbowFragments", _sourceRainbowFragments);
    }
    #endregion
}
#endregion

#region AnalyticEventCompletedCardSetData
public class AnalyticEventCompletedCardSetData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.completed_cardset;
    #region Private Properties  
    private string _cardsetID;
    private string _cardsetName;
    #endregion

    #region Contructors
    public AnalyticEventCompletedCardSetData(PlayerInfoData playerData, string cardsetID) : base(playerData)
    {
        _cardsetID = cardsetID;
        _cardsetName = LocalizationManager.Instance.GetText(string.Format("{0}_TITLE", cardsetID), Language.English);
    }

    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("cardset_id", _cardsetID);
        _paramDataList.Add("cardset_name", _cardsetName);
    }
    #endregion
}
#endregion

#region AnalyticEventFactionLevelUpData
public class AnalyticEventFactionLevelUpData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.faction_levelup;
    #region Private Properties  
    private string _playerFaction;
    private int _playerFactionLevel;
    #endregion

    #region Contructors
    public AnalyticEventFactionLevelUpData(PlayerInfoData playerData, CardElementType elementType) : base(playerData)
    {
        _playerFaction = AnalyticManager.ConvertToAnalyticElementType(elementType).ToString();
        _playerFactionLevel = playerData.LevelFactionData.GetLevelFactionData(elementType).Level;
    }
    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("player_faction", _playerFaction);
        _paramDataList.Add("player_faction_lv", _playerFactionLevel);
    }
    #endregion
}
#endregion

#region AnalyticEventCompletedSeasonRankData
public class AnalyticEventCompletedSeasonRankData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.completed_seasonRank;
    #region Private Properties  
    private int _seasonNumber;
    #endregion

    #region Contructors
    public AnalyticEventCompletedSeasonRankData(PlayerInfoData playerData, PlayerInventoryData inventory) : base(playerData, inventory)
    {
        // last season index
        int currentSeasonIndex = DataManager.Instance.GetCurrentSeasonIndex();
        _seasonNumber = currentSeasonIndex - 1;

        // last season rank
        _playerRank = AnalyticManager.ConvertToAnalyticPlayerRank(playerData.GetPlayerCurrentRank(_seasonNumber)).ToString();
    }
    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("player_circle", _playerSC);
        _paramDataList.Add("player_square", _playerSS);
        _paramDataList.Add("player_nova", _playerS1);
        _paramDataList.Add("season_number", _seasonNumber);
    }
    #endregion
}
#endregion

#endregion

#region Missions
#region AnalyticEventCompletedMissionData
public class AnalyticEventCompletedMissionData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.completed_mission;
    #region Private Properties  
    private string _missionStage;
    #endregion

    #region Contructors
    public AnalyticEventCompletedMissionData(PlayerInfoData playerData, string missionID) : base(playerData)
    {
        _missionStage = missionID;
    }
    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("mission_stage", _missionStage);
    }
    #endregion
}
#endregion
#endregion

#region Season Pass

#region AnalyticEventCompletedSeasonPassData
public class AnalyticEventCompletedSeasonPassData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.completed_seasonPass;
    #region Private Properties 
    private int _playerTier;
    private int _nonCompletedQuestAmount;
    private string player_passType;
    #endregion

    #region Contructors
    public AnalyticEventCompletedSeasonPassData(PlayerInfoData playerData) : base(playerData)
    {
        PlayerSeasonPassData data = DataManager.Instance.PlayerSeasonPassOld;
        _playerTier = data.GetCurrentSeasonPassTier();
        player_passType = AnalyticManager.ConvertToAnalyticSeasonPassType(data.IsBePremium()).ToString();

        List<QuestData> questList = new List<QuestData>(DataManager.Instance.PlayerSeasonPassQuestOld.GetMainAllWeekQuestDataList());
        questList.AddRange(DataManager.Instance.PlayerSeasonPassQuestOld.GetChallengeQuestDataList());
        foreach (QuestData quest in questList)
        {
            if (!quest.IsCompleted)
            {
                _nonCompletedQuestAmount++;
            }
        }
    }
    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("player_tier", _playerTier);
        _paramDataList.Add("nonCompletedQuest_amount", _nonCompletedQuestAmount);
        _paramDataList.Add("player_passType", player_passType);
    }
    #endregion
}
#endregion

#region AnalyticEventPurchaseTierData
public class AnalyticEventPurchaseTierData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.purchase_tier;
    #region Private Properties 
    private int _playerTier;
    private int _tierAmount;
    private int _sinkDiamonds;
    #endregion

    #region Contructors
    public AnalyticEventPurchaseTierData(PlayerInfoData playerData, int tierAmount) : base(playerData)
    {
        PlayerSeasonPassData data = DataManager.Instance.PlayerSeasonPass;
        _playerTier = data.GetCurrentSeasonPassTier() - tierAmount;
        _tierAmount = tierAmount;
        _sinkDiamonds = SeasonPassManager.Instance.GetSeasonPassTierPrice().Amount * tierAmount;
        _playerDI = DataManager.Instance.InventoryData.GetVirtualCurrency(VirtualCurrency.DI) + _sinkDiamonds;
    }
    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("player_diamonds", _playerDI);
        _paramDataList.Add("player_tier", _playerTier);
        _paramDataList.Add("tier_amount", _tierAmount);
        _paramDataList.Add("sink_diamonds", _sinkDiamonds);
    }
    #endregion
}
#endregion

#region AnalyticEventPurchasePremiumData
public class AnalyticEventPurchasePremiumData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.purchase_premium;
    #region Private Properties 
    private int _playerTier;
    private int _sinkDiamonds;
    #endregion

    #region Contructors
    public AnalyticEventPurchasePremiumData(PlayerInfoData playerData) : base(playerData)
    {
        PlayerSeasonPassData data = DataManager.Instance.PlayerSeasonPass;
        _playerTier = data.GetCurrentSeasonPassTier();
        _sinkDiamonds = SeasonPassManager.Instance.GetSeasonPassPremiumPrice().Amount;
        _playerDI = DataManager.Instance.InventoryData.GetVirtualCurrency(VirtualCurrency.DI) + _sinkDiamonds;
    }
    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("player_diamonds", _playerDI);
        _paramDataList.Add("player_tier", _playerTier);
        _paramDataList.Add("sink_diamonds", _sinkDiamonds);
    }
    #endregion
}
#endregion

#region AnalyticEventCompletedQuestData
public class AnalyticEventCompletedQuestData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.completed_quest;
    #region Private Properties 
    private int _playerTier;
    private string _questType;
    private int _pointAmount;
    private string _seasonWeek;
    #endregion

    #region Contructors
    public AnalyticEventCompletedQuestData(PlayerInfoData playerData, string questID) : base(playerData)
    {
        PlayerSeasonPassData data = DataManager.Instance.PlayerSeasonPass;
        _playerTier = data.GetCurrentSeasonPassTier();

        QuestData quest = DataManager.Instance.PlayerSeasonPassQuest.GetQuestData(questID);
        List<ItemData> reward = quest.ItemDataList;
        foreach (ItemData item in reward)
        {
            if (item.ItemID == "SCORE")
            {
                _pointAmount += item.Amount;
            }
        }

        _questType = AnalyticManager.ConvertToAnalyticQuestType(quest);
        _seasonWeek = AnalyticManager.GetAnalyticSeasonPassWeekCount();
    }

    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("player_tier", _playerTier);
        _paramDataList.Add("quest_type", _questType);
        _paramDataList.Add("point_amount", _pointAmount);
        _paramDataList.Add("season_week", _seasonWeek);
    }
    #endregion
}
#endregion

#region AnalyticEventCompletedTierData
public class AnalyticEventCompletedTierData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.completed_tier;
    #region Private Properties 
    private int _playerTier;
    private int _tierAmount;
    private int _sourceCoins;
    private int _sourceDiamonds;
    private int _sourceFragments;
    private int _sourceRainbowFragments;
    private int _sourceCircle;
    private int _sourceSquare;
    private int _sourceNova;
    private string _seasonWeek;
    #endregion

    #region Contructors
    public AnalyticEventCompletedTierData(PlayerInfoData playerData, int lasTier, List<ItemData> sourcedItemList) : base(playerData)
    {
        PlayerSeasonPassData data = DataManager.Instance.PlayerSeasonPass;
        _playerTier = data.GetCurrentSeasonPassTier();
        _tierAmount = _playerTier - lasTier;
        _seasonWeek = AnalyticManager.GetAnalyticSeasonPassWeekCount();

        foreach (ItemData item in sourcedItemList)
        {
            string itemKey = item.ItemID.Replace("VC_", "");
            switch (itemKey)
            {
                case "CO": _sourceCoins += item.Amount; break;
                case "DI": _sourceDiamonds += item.Amount; break;
                case "FR": _sourceFragments += item.Amount; break;
                case "RF": _sourceRainbowFragments += item.Amount; break;
                case "SC": _sourceCircle += item.Amount; break;
                case "SS": _sourceSquare += item.Amount; break;
                case "S1": _sourceNova += item.Amount; break;
            }
        }
    }

    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("player_tier", _playerTier);
        _paramDataList.Add("tier_amount", _tierAmount);
        _paramDataList.Add("source_coins", _sourceCoins);
        _paramDataList.Add("source_diamonds", _sourceDiamonds);
        _paramDataList.Add("source_fragments", _sourceFragments);
        _paramDataList.Add("source_rainbowFragments", _sourceRainbowFragments);
        _paramDataList.Add("source_circle", _sourceCircle);
        _paramDataList.Add("source_square", _sourceSquare);
        _paramDataList.Add("source_nova", _sourceNova);
        _paramDataList.Add("season_week", _seasonWeek);
    }
    #endregion
}
#endregion

#endregion

#region Event

#region AnalyticEventEnterInGameEventData
public class AnalyticEventEnterInGameEventData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.enter_inGameEvent;
    #region Contructors
    public AnalyticEventEnterInGameEventData(PlayerInfoData playerData) : base(playerData)
    {
    }
    #endregion

}
#endregion

#region AnalyticEventPlayInGameEventData
public class AnalyticEventPlayInGameEventData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.play_inGameEvent;
    #region Private Properties 
    private string _eventID;
    private string _eventName;
    #endregion

    #region Contructors
    public AnalyticEventPlayInGameEventData(PlayerInfoData playerData, string eventID) : base(playerData)
    {
        _eventID = eventID;
        _eventName = LocalizationManager.Instance.GetEventName(eventID, Language.English);
    }
    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("inGameEvent_id", _eventID);
        _paramDataList.Add("inGameEvent_name", _eventName);
    }
    #endregion
}
#endregion

#region AnalyticEventCompleteInGameEventData

public class AnalyticEventCompleteInGameEventData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.completed_inGameEvent;
    #region Private Properties 
    private string _eventID;
    private string _eventName;
    #endregion

    #region Contructors
    public AnalyticEventCompleteInGameEventData(PlayerInfoData playerData, string eventID) : base(playerData)
    {
        _eventID = eventID;
        _eventName = LocalizationManager.Instance.GetEventName(eventID, Language.English);
    }
    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("inGameEvent_id", _eventID);
        _paramDataList.Add("inGameEvent_name", _eventName);
    }
    #endregion
}

#endregion

#region AnalyticEventReceiveRewardInGameEventData
public class AnalyticEventReceiveRewardInGameEventData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.receive_reward_inGameEvent;
    #region Private Properties 
    private string _eventID;
    private string _eventName;
    private string _rewardName;
    private int _sourceDiamonds;
    private int _sourceCoins;
    private int _sourceFragments;
    private int _sourceRainbowFragments;
    #endregion

    #region Contructors
    public AnalyticEventReceiveRewardInGameEventData(PlayerInfoData playerData, List<ItemData> items, string eventID) : base(playerData)
    {
        _eventID = eventID;
        _eventName = LocalizationManager.Instance.GetEventName(eventID, Language.English);

        List<string> rewardList = new List<string>();
        foreach (var item in items)
        {
            switch (item.ItemID)
            {
                case "VC_CO":
                    _sourceCoins += item.Amount;
                    break;

                case "VC_DI":
                    _sourceDiamonds += item.Amount;
                    break;

                case "VC_FR":
                    _sourceFragments += item.Amount;
                    break;

                case "VC_RF":
                    _sourceRainbowFragments += item.Amount;
                    break;
            }
            rewardList.Add(item.ItemID);
        }
        _rewardName = JsonConvert.SerializeObject(rewardList);
    }

    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("inGameEvent_id", _eventID);
        _paramDataList.Add("inGameEvent_name", _eventName);
        _paramDataList.Add("reward_name", _rewardName);
        _paramDataList.Add("source_diamonds", _sourceDiamonds);
        _paramDataList.Add("source_coins", _sourceCoins);
        _paramDataList.Add("source_fragments", _sourceFragments);
        _paramDataList.Add("source_rainbowFragments", _sourceRainbowFragments);
    }
    #endregion
}
#endregion

#endregion

#region Friends

#region AnalyticEventEnterFriendData
public class AnalyticEventEnterFriendData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.enter_friend;
    #region Contructors
    public AnalyticEventEnterFriendData(PlayerInfoData playerData) : base(playerData)
    {
    }

    #endregion
}
#endregion

#region AnalyticEventTapFriendListData
public class AnalyticEventTapFriendListData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.tap_friendList;
    #region Contructors
    public AnalyticEventTapFriendListData(PlayerInfoData playerData) : base(playerData)
    {
    }

    #endregion
}
#endregion

#region AnalyticEventTapFriendRequestData
public class AnalyticEventTapFriendRequestData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.tap_friendRequest;
    #region Contructors
    public AnalyticEventTapFriendRequestData(PlayerInfoData playerData) : base(playerData)
    {
    }

    #endregion
}
#endregion

#region AnalyticEventTapAddFriendData
public class AnalyticEventTapAddFriendData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.tap_addFriend;

    #region Contructors
    public AnalyticEventTapAddFriendData(PlayerInfoData playerData) : base(playerData)
    {
    }

    #endregion
}
#endregion

#region AnalyticEventTapChatData
public class AnalyticEventTapChatData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.tap_chat;

    #region Contructors
    public AnalyticEventTapChatData(PlayerInfoData playerData) : base(playerData)
    {
    }
    #endregion
}
#endregion

#region AnalyticEventAddFriendData
public class AnalyticEventAddFriendData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.add_friend;
    #region Private Properties 
    private string _friendID;
    private string _friendName;

    private string _addFrom;
    #endregion

    #region Contructors
    public AnalyticEventAddFriendData(PlayerInfoData playerData, string friendID, string friendName, AddFriendType source) : base(playerData)
    {
        _friendID = friendID;
        _friendName = friendName;

        _addFrom = AnalyticManager.ConvertToAddFriendSourceAnalytic(source).ToString();
    }

    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("friend_id", _friendID);
        _paramDataList.Add("friend_name", _friendName);
        _paramDataList.Add("add_from", _addFrom);
    }
    #endregion
}
#endregion

#region AnalyticEventAcceptFriendData
public class AnalyticEventAcceptFriendData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.accept_friend;
    #region Private Properties 
    private string _friendID;
    private string _friendName;
    private int _friendLevel;
    private string _friendRank;
    #endregion

    #region Contructors
    public AnalyticEventAcceptFriendData(PlayerInfoData playerData, PlayerInfoData friendData) : base(playerData)
    {
        _friendID = friendData.PlayerID;
        _friendName = friendData.DisplayName;
        _friendLevel = friendData.LevelData.Level;
        _friendRank = AnalyticManager.ConvertToAnalyticPlayerRank(friendData.GetPlayerCurrentRank()).ToString();
    }

    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("friend_id", _friendID);
        _paramDataList.Add("friend_name", _friendName);
        _paramDataList.Add("friend_lv", _friendLevel);
        _paramDataList.Add("friend_rank", _friendRank);
    }
    #endregion
}
#endregion

#region AnalyticEventDeleteFriendData
public class AnalyticEventDeleteFriendData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.delete_friend;
    #region Private Properties 
    private string _friendID;
    private string _friendName;
    private int _friendLevel;
    private string _friendRank;
    #endregion

    #region Contructors
    public AnalyticEventDeleteFriendData(PlayerInfoData playerData, PlayerInfoData friendData) : base(playerData)
    {
        _friendID = friendData.PlayerID;
        _friendName = friendData.DisplayName;
        _friendLevel = friendData.LevelData.Level;
        _friendRank = AnalyticManager.ConvertToAnalyticPlayerRank(friendData.GetPlayerCurrentRank()).ToString();
    }
    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("friend_id", _friendID);
        _paramDataList.Add("friend_name", _friendName);
        _paramDataList.Add("friend_lv", _friendLevel);
        _paramDataList.Add("friend_rank", _friendRank);
    }
    #endregion
}
#endregion

#region AnalyticEventApplyReferalData
public class AnalyticEventApplyReferalData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.apply_referal;
    #region Private Properties 
    #endregion

    #region Contructors
    public AnalyticEventApplyReferalData(PlayerInfoData playerData) : base(playerData)
    {
    }
    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
    }
    #endregion
}
#endregion

#region AnalyticEventClaimReferalData
public class AnalyticEventClaimReferalData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.claim_referal;
    #region Private Properties 
    #endregion

    #region Contructors
    public AnalyticEventClaimReferalData(PlayerInfoData playerData) : base(playerData)
    {
    }
    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
    }
    #endregion
}
#endregion

#endregion

#region Inbox

#region AnalyticEventEnterInboxData
public class AnalyticEventEnterInboxData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.enter_inbox;
    #region Contructors
    public AnalyticEventEnterInboxData(PlayerInfoData playerData) : base(playerData)
    {
    }
    #endregion
}
#endregion

#region AnalyticEventTapMessageData
public class AnalyticEventTapMessageData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.tap_message;

    #region Contructors
    public AnalyticEventTapMessageData(PlayerInfoData playerData) : base(playerData)
    {
    }
    #endregion

}
#endregion

#region AnalyticEventReceiveRewardInboxData
public class AnalyticEventReceiveRewardInboxData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.receive_reward_inbox;

    #region Private Properties 
    private string _messageDate;
    private string _messageTime;
    private string _messageID;
    private string _messageTitle;

    private int _sourceDiamonds;
    private int _sourceCoins;
    private int _sourceFragments;
    private int _sourceRainbowFragments;
    private int _sourceCircle;
    private int _sourceSquare;
    private int _sourceNova;
    #endregion

    #region Contructors
    public AnalyticEventReceiveRewardInboxData(PlayerInfoData playerData, MessageInboxData messageData) : base(playerData)
    {
        _messageDate = messageData.ReceivedDateTimeUTC.ToString("YYYYMMDD");
        _messageTime = messageData.ReceivedDateTimeUTC.ToString("HHMMSS");
        _messageID = messageData.ID;
        _messageTitle = messageData.GetTitle(Language.English);

        foreach (ItemData item in messageData.ItemDataList)
        {
            string itemKey = item.ItemID.Replace("VC_", "");
            switch (itemKey)
            {
                case "CO": _sourceCoins += item.Amount; break;
                case "DI": _sourceDiamonds += item.Amount; break;
                case "FR": _sourceFragments += item.Amount; break;
                case "RF": _sourceRainbowFragments += item.Amount; break;
                case "SC": _sourceCircle += item.Amount; break;
                case "SS": _sourceSquare += item.Amount; break;
                case "S1": _sourceNova += item.Amount; break;
            }
        }
    }

    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("message_date", _messageDate);
        _paramDataList.Add("message_time", _messageTime);
        _paramDataList.Add("message_title", _messageTitle);
        _paramDataList.Add("source_diamonds", _sourceDiamonds);
        _paramDataList.Add("source_coins", _sourceCoins);
        _paramDataList.Add("source_fragments", _sourceFragments);
        _paramDataList.Add("source_rainbowFragments", _sourceRainbowFragments);
        _paramDataList.Add("source_nova", _sourceNova);
        _paramDataList.Add("source_circle", _sourceCircle);
        _paramDataList.Add("source_square", _sourceSquare);
    }
    #endregion
}
#endregion

#region AnalyticEventDeleteMessageData
public class AnalyticEventDeleteMessageData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.delete_message;
    #region Private Properties 
    private string _messageDate;
    private string _messageTime;
    private string _messageID;
    private string _messageTitle;

    private string _isClaimed;
    #endregion

    #region Contructors
    public AnalyticEventDeleteMessageData(PlayerInfoData playerData, MessageInboxData messageData) : base(playerData)
    {
        _messageDate = messageData.ReceivedDateTimeUTC.ToString("YYYYMMDD");
        _messageTime = messageData.ReceivedDateTimeUTC.ToString("HHMMSS");
        _messageID = messageData.ID;
        _messageTitle = messageData.GetTitle(Language.English);

        _isClaimed = AnalyticManager.ConvertToAnalyticBoolean(messageData.IsClaimed).ToString();
    }

    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("message_date", _messageDate);
        _paramDataList.Add("message_time", _messageTime);
        _paramDataList.Add("message_title", _messageTitle);
        _paramDataList.Add("isClaimed_reward", _isClaimed);
    }
    #endregion
}
#endregion

#region AnalyticEventReceiveAllMessageData
public class AnalyticEventReceiveAllMessageData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.receive_all;
    #region Private Properties 
    private int _messageAmount;
    private int _sourceCoins;
    private int _sourceDiamonds;
    private int _sourceFragments;
    private int _sourceRainbowFragments;
    private int _sourceCircle;
    private int _sourceSquare;
    private int _sourceNova;
    #endregion

    #region Contructors
    public AnalyticEventReceiveAllMessageData(PlayerInfoData playerData, int messageAmount, List<ItemData> sourcedItemList) : base(playerData)
    {
        _messageAmount = messageAmount;
        foreach (ItemData item in sourcedItemList)
        {
            string itemKey = item.ItemID.Replace("VC_", "");
            switch (itemKey)
            {
                case "CO": _sourceCoins += item.Amount; break;
                case "DI": _sourceDiamonds += item.Amount; break;
                case "FR": _sourceFragments += item.Amount; break;
                case "RF": _sourceRainbowFragments += item.Amount; break;
                case "SC": _sourceCircle += item.Amount; break;
                case "SS": _sourceSquare += item.Amount; break;
                case "S1": _sourceNova += item.Amount; break;
            }
        }
    }

    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("message_amount", _messageAmount);
        _paramDataList.Add("source_diamonds", _sourceDiamonds);
        _paramDataList.Add("source_coins", _sourceCoins);
        _paramDataList.Add("source_fragments", _sourceFragments);
        _paramDataList.Add("source_rainbowFragments", _sourceRainbowFragments);
        _paramDataList.Add("source_nova", _sourceNova);
        _paramDataList.Add("source_circle", _sourceCircle);
        _paramDataList.Add("source_square", _sourceSquare);
    }
    #endregion
}
#endregion

#region AnalyticEventDeletellMessageData
public class AnalyticEventDeletellMessageData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.delete_all;
    #region Private Properties 
    private int _messageAmount;
    #endregion

    #region Contructors
    public AnalyticEventDeletellMessageData(PlayerInfoData playerData, int messageAmount) : base(playerData)
    {
        _messageAmount = messageAmount;
    }

    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("message_amount", _messageAmount);
    }
    #endregion
}
#endregion

#endregion

#region Main Menu

#region AnalyticEventTapBannerData
public class AnalyticEventTapBannerData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.tap_banner;
    #region Private Properties 
    private float _timeSession;
    private string _bannerName;
    #endregion

    #region Contructors
    public AnalyticEventTapBannerData(PlayerInfoData playerData, string bannerName, float timeSession) : base(playerData)
    {
        this._timeSession = timeSession;
        this._bannerName = bannerName;
    }
    #endregion

    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("time_session", _timeSession);
        _paramDataList.Add("banner_name", _bannerName);
    }
}
#endregion

#region AnalyticEventTapAddDiamondsData
public class AnalyticEventTapAddDiamondsData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.tap_addDiamonds;
    private float _timeSession;
    #region Contructors
    public AnalyticEventTapAddDiamondsData(PlayerInfoData playerData, PlayerInventoryData inventoryData, float timeSession) : base(playerData, inventoryData)
    {
        this._timeSession = timeSession;
    }
    #endregion

    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("time_session", _timeSession);
    }
}
#endregion

#region AnalyticEventTapCommunityData
public class AnalyticEventTapCommunityData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.tap_community;
    private float _timeSession;
    #region Contructors
    public AnalyticEventTapCommunityData(PlayerInfoData playerData, float timeSession) : base(playerData)
    {
        this._timeSession = timeSession;
    }
    #endregion
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("time_session", _timeSession);
    }
}
#endregion

#endregion

#region StarExchange

#region AnalyticEventPurchaseStarExchange
public class AnalyticEventPurchaseStarExchangeData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.redeem_completed;
    #region Private Properties 
    private StarExchangeItem _item;
    private PlayerInventoryData _inventory;
    #endregion

    #region Contructors
    public AnalyticEventPurchaseStarExchangeData(PlayerInfoData playerData, PlayerInventoryData inventory, StarExchangeItem item) : base(playerData)
    {
        this._item = item;
        this._inventory = inventory;
    }
    #endregion

    protected override void SetParamDataList()
    {
        base.SetParamDataList();

        _paramDataList.Add("player_circle", _playerSC);
        _paramDataList.Add("player_square", _playerSS);

        _paramDataList.Add("item_id", _item.ItemID);
        _paramDataList.Add("item_name", LocalizationManager.Instance.GetText("ITEM_NAME_" + _item.ItemID, Language.English));
    }
}
#endregion

#region AnalyticEventRefreshStarExchange
public class AnalyticEventRefreshStarExchange : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.refresh_starMarket;
    #region Private Properties 
    private StarExchangeItem _item;
    private PlayerInventoryData _inventory;
    #endregion

    #region Contructors
    public AnalyticEventRefreshStarExchange(PlayerInfoData playerData, PlayerInventoryData inventory) : base(playerData, inventory)
    {
    }
    #endregion

    protected override void SetParamDataList()
    {
        base.SetParamDataList();

        _paramDataList.Add("player_circle", _playerSC);
        _paramDataList.Add("player_square", _playerSS);
    }
}
#endregion

#endregion

#region Leave Game

#region AnalyticEventLeaveGameData
public class AnalyticEventLeaveGameData : AnalyticEventData
{
    protected override EventNameTypes _eventNameType => EventNameTypes.leave_game;
    #region Private Properties 
    private string _lastScene;
    private float _timeSession;
    #endregion

    #region Contructors
    public AnalyticEventLeaveGameData(PlayerInfoData playerData, string lastScene, float timeSession) : base(playerData)
    {
        _lastScene = lastScene;
        _timeSession = timeSession;
    }

    #endregion

    #region Set Methods
    protected override void SetParamDataList()
    {
        base.SetParamDataList();
        _paramDataList.Add("time_session", _timeSession);
        _paramDataList.Add("last_screen", _lastScene);
    }
    #endregion
}
#endregion

#endregion

#endregion

#region Analytic Manager
public class AnalyticManager : MonoSingleton<AnalyticManager>
{
    public static event Action OnInit;
    public static event Action OnQuitGame;

    #region Private Properties
    private readonly static bool _isEnableGameAnalytic = true;
    private readonly static bool _isEnablePlayfab = true;
    private readonly static bool _isEnableAppsFlyer = true;

#if UNITY_IOS || UNITY_ANDROID
    private readonly static bool _isEnableFirebase = true;
#else
private readonly static bool _isEnableFirebase = false;
#endif

    private readonly static bool _isEnableDebugLog = true;

    private List<AnalyticBase> analytics = new List<AnalyticBase>();
    #endregion

    #region Event Key Properties
    private readonly static string _battleProgression = "battle";
    private readonly static string _startPlayEventName = "start_play";
    private readonly static string _completePlayEventName = "complete_play";
    private readonly static string _rehandEventName = "rehand";
    private readonly static string _startTurnEventName = "start_turn";

    private readonly static string _cardSpawnEventName = "card_spawn";
    private readonly static string _cardDestroyEventName = "card_destroy";
    private readonly static string _cardAttackEventName = "card_attack";
    private readonly static string _cardAuraEventName = "card_aura";
    private readonly static string _cardTriggerEventName = "card_trigger";
    private readonly static string _cardLinkEventName = "card_link";
    private readonly static string _cardBattleArmorEventName = "card_armor";
    private readonly static string _cardTauntEventName = "card_taunt";
    private readonly static string _cardMulliganEventName = "card_mulligan";
    private readonly static string _cardBoostStatEventName = "card_boost_stat";

    private readonly static string _TurnSummaryEventName = "turn_summary";

    private readonly static string _purchaseEventName = "purchase_log";
    #endregion

    #region Convert Methods
    public static AnalyticGameMode ConvertToAnalyticGameMode(GameMode gameMode, BotLevel level = BotLevel.None)
    {
        switch (gameMode)
        {
            case GameMode.Tutorial_1:
                {
                    return AnalyticGameMode.tutorial_1;
                }
            case GameMode.Tutorial_2:
                {
                    return AnalyticGameMode.tutorial_2;
                }
            case GameMode.Tutorial_3:
                {
                    return AnalyticGameMode.tutorial_3;
                }
            case GameMode.Tutorial_4:
                {
                    return AnalyticGameMode.tutorial_4;
                }
            case GameMode.Casual:
                {
                    return AnalyticGameMode.casual_1vs1;
                }
            case GameMode.Friendly:
                {
                    return AnalyticGameMode.friendly_1vs1;
                }
            case GameMode.Rank:
                {
                    return AnalyticGameMode.ranked_1vs1;
                }
            case GameMode.Arena:
                {
                    return AnalyticGameMode.arena_1vs1;
                }
            case GameMode.Event:
                {
                    return AnalyticGameMode.event_1vs1;
                }
            case GameMode.Practice:
                {
                    switch (level)
                    {
                        case BotLevel.Easy:
                            {
                                return AnalyticGameMode.practice_easy;
                            }
                        case BotLevel.Medium:
                            {
                                return AnalyticGameMode.practice_normal;
                            }
                        case BotLevel.Hard:
                            {
                                return AnalyticGameMode.practice_hard;
                            }
                        default:
                            {
                                return AnalyticGameMode.none;
                            }
                    }
                }
            default:
                {
                    return AnalyticGameMode.none;
                }
        }
    }

    public static AnalyticElementType ConvertToAnalyticElementType(CardElementType elementType)
    {
        switch (elementType)
        {
            case CardElementType.NEUTRAL:
                {
                    return AnalyticElementType.neutral;
                }
            case CardElementType.FIRE:
                {
                    return AnalyticElementType.flame;
                }
            case CardElementType.WATER:
                {
                    return AnalyticElementType.tidal;
                }
            case CardElementType.AIR:
                {
                    return AnalyticElementType.cyclone;
                }
            case CardElementType.EARTH:
                {
                    return AnalyticElementType.terra;
                }
            case CardElementType.HOLY:
                {
                    return AnalyticElementType.divine;
                }
            case CardElementType.DARK:
                {
                    return AnalyticElementType.shadow;
                }
            default:
                {
                    return AnalyticElementType.none;
                }
        }
    }

    public static AnalyticSlotPosition ConvertToAnalyticSlotPosition(int slotIndex)
    {
        if (slotIndex > -1
            && slotIndex < System.Enum.GetValues(typeof(AnalyticSlotPosition)).Length)
        {
            return (AnalyticSlotPosition)slotIndex;
        }
        else
        {
            return AnalyticSlotPosition.none;
        }
    }

    public static AnalyticBoolean ConvertToAnalyticBoolean(bool isYes)
    {
        if (isYes)
        {
            return AnalyticBoolean.YES;
        }
        else
        {
            return AnalyticBoolean.NO;
        }
    }

    public static AnalyticPlayOrder ConvertToAnalyticPlayOrder(bool isPlayFirst)
    {
        if (isPlayFirst)
        {
            return AnalyticPlayOrder.first;
        }
        else
        {
            return AnalyticPlayOrder.second;
        }
    }

    public static AnalyticPlayerRank ConvertToAnalyticPlayerRank(int rankIndex)
    {
        switch (rankIndex)
        {
            case 0:
            case 1:
            case 2:
            case 3:
                {
                    return AnalyticPlayerRank.librarian_4;
                }

            case 4:
            case 5:
            case 6:
            case 7:
                {
                    return AnalyticPlayerRank.librarian_3;
                }

            case 8:
            case 9:
            case 10:
            case 11:
                {
                    return AnalyticPlayerRank.librarian_2;
                }

            case 12:
            case 13:
            case 14:
            case 15:
                {
                    return AnalyticPlayerRank.librarian_1;
                }

            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
                {
                    return AnalyticPlayerRank.athenia_4;
                }

            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
                {
                    return AnalyticPlayerRank.athenia_3;
                }

            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
                {
                    return AnalyticPlayerRank.athenia_2;
                }

            case 31:
            case 32:
            case 33:
            case 34:
            case 35:
                {
                    return AnalyticPlayerRank.athenia_1;
                }

            case 36:
            case 37:
            case 38:
            case 39:
            case 40:
            case 41:
                {
                    return AnalyticPlayerRank.divine_4;
                }

            case 42:
            case 43:
            case 44:
            case 45:
            case 46:
            case 47:
                {
                    return AnalyticPlayerRank.divine_3;
                }

            case 48:
            case 49:
            case 50:
            case 51:
            case 52:
            case 53:
                {
                    return AnalyticPlayerRank.divine_2;
                }

            case 54:
            case 55:
            case 56:
            case 57:
            case 58:
            case 59:
                {
                    return AnalyticPlayerRank.divine_1;
                }

            case 60:
            case 61:
            case 62:
            case 63:
            case 64:
            case 65:
            case 66:
                {
                    return AnalyticPlayerRank.cosmicBorn;
                }

            case 67:
            case 68:
            case 69:
            case 70:
            case 71:
            case 72:
            case 73:
                {
                    return AnalyticPlayerRank.highBorn;
                }

            case 74:
            case 75:
            case 76:
            case 77:
            case 78:
            case 79:
            case 80:
                {
                    return AnalyticPlayerRank.secondBorn;
                }

            case 81:
            case 82:
            case 83:
            case 84:
            case 85:
            case 86:
            case 87:
                {
                    return AnalyticPlayerRank.firstBorn;
                }
        }
        return AnalyticPlayerRank.librarian_4;
    }

    public static AnalyticMatchResult ConvertToAnalyticMatchResult(bool isVictory)
    {
        if (isVictory)
        {
            return AnalyticMatchResult.win;
        }
        else
        {
            return AnalyticMatchResult.lose;
        }
    }

    public static AnalyticCardPackName ConvertToAnalyticCardPackName(string cardPackID)
    {
        switch (cardPackID)
        {
            case "CONT_WOE":
            case "CONT_WOE_LEGEND":
                {
                    return AnalyticCardPackName.warOfElements;
                }

            case "CONT_GM":
                {
                    return AnalyticCardPackName.grandMaster;
                }
        }

        return AnalyticCardPackName.none;
    }

    public static AnalyticMatchResultCondition ConvertToAnalyticMatchResultCondition(GameManager.EndGameType endGameType)
    {
        switch (endGameType)
        {
            case GameManager.EndGameType.Normal: return AnalyticMatchResultCondition.normal;
            case GameManager.EndGameType.OtherLeftRoom: return AnalyticMatchResultCondition.opponent_left;
            case GameManager.EndGameType.Disconnect: return AnalyticMatchResultCondition.player_left;
            case GameManager.EndGameType.Surrender: return AnalyticMatchResultCondition.player_surrender;
            case GameManager.EndGameType.OtherSurrender: return AnalyticMatchResultCondition.opponent_surrender;
        }

        return AnalyticMatchResultCondition.others;
    }

    public static AnalyticSeasnPassType ConvertToAnalyticSeasonPassType(bool isPremium)
    {
        if (isPremium)
        {
            return AnalyticSeasnPassType.premium;
        }
        else
        {
            return AnalyticSeasnPassType.free;
        }
    }

    public static string ConvertToAnalyticQuestType(QuestData data)
    {
        string result = string.Empty;
        AnalyticQuestType questType = AnalyticQuestType.daily;
        switch (data.Type)
        {
            case QuestTypes.SeasonPassMainDaily:
            {
                questType = AnalyticQuestType.daily;
            }
            break;

            case QuestTypes.SeasonPassMainWeekly:
            {
                questType = AnalyticQuestType.weekly;
            }
            break;

            case QuestTypes.SeasonPassMainWeek:
            {
                questType = AnalyticQuestType.week;
            }
            break;

            case QuestTypes.SeasonPassChallenge:
            {
                questType = AnalyticQuestType.challenge;
            }
            break;
        }

        result = questType.ToString();
        if (questType == AnalyticQuestType.week)
        {
            string questID = data.ID;
            int index = DataManager.Instance.PlayerSeasonPassQuest.GetWeekIndexOfMainWeekQuest(questID);
            result += "_" + index;
        }

        return result;
    }

    public static string GetAnalyticSeasonPassWeekCount()
    {
        return string.Format("week_{0}", DataManager.Instance.PlayerSeasonPassQuest.GetWeekCount());
    }

    public static AnalyticAddFriendSource ConvertToAddFriendSourceAnalytic(AddFriendType type)
    {
        switch (type)
        {
            case AddFriendType.Recent:
                return AnalyticAddFriendSource.by_recently;
            case AddFriendType.Name:
                return AnalyticAddFriendSource.by_name;
            default:
                return AnalyticAddFriendSource.by_name;
        }
    }
    #endregion

    #region Analytic Event Methods 
    protected override void InitSelf()
    {
        base.InitSelf();
        analytics.Clear();
        analytics.Add(new ShopAnalyticManager(this));
        analytics.Add(new LeaveGameAnalyticManager(this));
        analytics.Add(new HomeAnalyticManager(this));
        analytics.Add(new GameEventAnalyticManager(this));
        analytics.Add(new StarExchangeAnalyticManager(this));
        analytics.Add(new DeckAnalyticManager(this));
        analytics.Add(new MissionAnalyticManager(this));
        analytics.Add(new SeasonPassAnalyticManager(this));
        analytics.Add(new InboxAnalyticManager(this));
        analytics.Add(new FriendAnalyticManager(this));
        analytics.Add(new ProfileAnalyticManager(this));
        analytics.Add(new WildOfferAnalyticManager(this));

        for (int i = 0; i < analytics.Count; i++)
        {
            analytics[i].Initialize();
        }
        OnInit?.Invoke();
    }

    public void LogAnalyticEvent(AnalyticEventData eventData)
    {
        /*----------------------------
        TODO: GameAnalytic data format
        ----------------------------*/

        if (_isEnableGameAnalytic)
        {
            /*
            GameAnalytics.NewDesignEvent(string.Format("{0}:{1}:{2}", _battleProgression, _startPlayEventName, heroID));
            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, _battleProgression, gameMode.ToString());
            */
        }

        if (_isEnablePlayfab)
        {
            if (PlayFabClientAPI.IsClientLoggedIn())
            {
                PlayFabClientAPI.WritePlayerEvent(
                    new WriteClientPlayerEventRequest
                    {
                        EventName = eventData.EventName
                        ,
                        Body = eventData.GetPlayFabDictionary()
                    }
                    , OnPlayFabResultCallback // result callback
                    , OnPlayFabErrorCallback // error callback
                );
            }
        }

        if (_isEnableFirebase)
        {
            FirebaseAnalytics.LogEvent(
                eventData.EventName
                , eventData.GetFirebaseParameters()
            );
        }

        /*----------------------------
        TODO: AppsFlyer data format
        ----------------------------*/

        if (_isEnableAppsFlyer)
        {
        }

        if (_isEnableDebugLog)
        {
            Debug.Log(eventData.GetDebugText());
        }
    }

    private void OnApplicationPause(bool pause)
    {
        if (GameAnalytics.HasInitializeBeenCalled)
        {
            if (pause)
            {
                if (_isEnableGameAnalytic)
                {
                    GameAnalytics.NewDesignEvent("Application:Pause");
                }
            }
            else
            {
                if (_isEnableGameAnalytic)
                {
                    GameAnalytics.NewDesignEvent("Application:Resume");
                }
            }
            for (int i = 0; i < analytics.Count; i++)
            {
                analytics[i].OnApplicationPause(pause);
            }
        }
    }

    private void OnApplicationQuit()
    {
        OnQuitGame?.Invoke();
        if (_isEnableGameAnalytic)
        {
            GameAnalytics.NewDesignEvent("Application:Quit");
        }
        for (int i = 0; i < analytics.Count; i++)
        {
            analytics[i].OnApplicationQuit();
        }
    }

    #endregion

    #region Methods
    private static void OnPlayFabResultCallback(WriteEventResponse result)
    {
        Debug.Log("OnPlayFabResultCallback completed.");
    }

    private static void OnPlayFabErrorCallback(PlayFabError error)
    {
        Debug.LogErrorFormat("OnPlayFabErrorCallback: {0}", error.Error.ToString());
    }
    #endregion
}
#endregion