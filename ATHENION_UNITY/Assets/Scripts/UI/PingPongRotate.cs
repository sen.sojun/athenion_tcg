﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PingPongRotate : MonoBehaviour {

    public Vector3 MaxAngle;

    private void Start()
    {
        Sequence sq = DOTween.Sequence();
        transform.localRotation = Quaternion.Euler(-MaxAngle);
        sq.Append(transform.DOLocalRotate(MaxAngle, 3.0f).SetEase(Ease.InOutSine));
        sq.Append(transform.DOLocalRotate(-MaxAngle, 3.0f).SetEase(Ease.InOutSine));
        sq.SetLoops(-1);
    }

}
