﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;

namespace Karamucho.UI
{
    public class UIEffectManager : MonoSingleton<UIEffectManager>
    {
        public HeroAttackAnimation HeroAttack_Top;
        public HeroAttackAnimation HeroAttack_Bot;

        #region Hero Attack Effecet(Scene Animation)
        public void HeroBeAttacked(int damage, PlayerIndex playerIndex, UnityAction onImpact, UnityAction onComplete)
        {
            #region Check local
            bool isLocal = GameManager.Instance.IsLocalPlayer(playerIndex);
            UIPlayerDock targetDock, attackerDock;
            if (isLocal)
            {
                targetDock = UIManager.Instance.UIPlayerBottom;
                attackerDock = UIManager.Instance.UIPlayerTop;
            }
            else
            {
                targetDock = UIManager.Instance.UIPlayerTop;
                attackerDock = UIManager.Instance.UIPlayerBottom;
            }
            #endregion

            #region Load VFX Key
            string startKey = "";
            string projectileKey = "";
            string endKey = "";
            int lowDamage = HeroAttackAnimation.LowPower;
            int medDamage = HeroAttackAnimation.MedPower;
            HeroAttackData data = isLocal ? HeroAttack_Top.GetData() : HeroAttack_Bot.GetData();

            startKey = data.StartKey; // Get Start Key
            endKey = data.EndKey; // Get End Key

            if (damage <= lowDamage)
            {
                projectileKey = data.ProjectileKey_1;
            }
            else if (damage <= medDamage)
            {
                projectileKey = data.ProjectileKey_2;
            }
            else
            {
                projectileKey = data.ProjectileKey_3;
            }
            #endregion

            #region Action VFX
            UnityAction actionOnImpact = null;
            UnityAction actionOnProjectile = null;
            // Set onImpact
            actionOnImpact = delegate
            {
                onImpact?.Invoke();
                HeroImpactEffect(endKey, targetDock.GetHeroPosition(), onComplete);
            };

            // Set Projectile
            actionOnProjectile = delegate
            {
                HeroProjectileEffect(projectileKey, attackerDock.GetSoulPosition(), targetDock.GetHeroPosition(), actionOnImpact, 0.5f);
            };

            // Casting
            HeroImpactEffect(startKey, attackerDock.GetSoulPosition(), actionOnProjectile);
            #endregion
        }

        #endregion

        #region Dynamic Effect(Moving Object)
        public void SpiritEffect(string vfxKey,CardVFXPool.VFXType endVFX, Vector3 startPosition, Vector3 endPosition, UnityAction onComplete, float duration = 0.75f)
        {
            // Create Effect
            CardVFXItem effect = CardVFXPool.Instance.CreateItem(CardVFXPool.RootAbilityEFXPath + vfxKey);
            effect.transform.position = startPosition;
            effect.SetStartPosition(startPosition);
            effect.SetEnableUpdateFacing(true);
            effect.transform.LookAt(endPosition);

            Sequence sq = DOTween.Sequence();
            sq.Append(effect.transform.DOMove(endPosition, duration).SetEase(Ease.InOutExpo).OnComplete(delegate {
                SpawnVFX(endVFX, endPosition, delegate {
                    CardVFXPool.Instance.ReleaseItem(effect);
                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                });
            }));
        }

        public void SpiritEffect(string vfxKey, Vector3 startPosition, Vector3 endPosition, UnityAction onComplete, float duration = 0.75f)
        {
            // Create Effect
            CardVFXItem effect = CardVFXPool.Instance.CreateItem(CardVFXPool.RootAbilityEFXPath + vfxKey);
            effect.transform.position = startPosition;
            effect.SetStartPosition(startPosition);
            effect.SetEnableUpdateFacing(true);
            effect.transform.LookAt(endPosition);

            Sequence sq = DOTween.Sequence();
            sq.Append(effect.transform.DOMove(endPosition, duration).SetEase(Ease.InOutSine).OnComplete(delegate {
                CardVFXPool.Instance.ReleaseItem(effect);
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            }));
        }

        public void AbilityEffect(string vfxKey, Vector3 startPosition, Vector3 endPosition, UnityAction onComplete, float duration = 0.3f)
        {
            // Fail Safe
            if (vfxKey == "-" || string.IsNullOrWhiteSpace(vfxKey))
            {
                onComplete?.Invoke();
                return;
            }

            // Create Effect
            CardVFXItem effect = CardVFXPool.Instance.CreateItem(CardVFXPool.RootAbilityEFXPath + vfxKey);
            effect.transform.position = startPosition;
            effect.SetStartPosition(startPosition);
            effect.SetEnableUpdateFacing(true);
            effect.transform.LookAt(endPosition);

            Sequence sq = DOTween.Sequence();
            //sq.Append(effect.transform.DOMove(endPosition, duration).SetEase(Ease.InOutSine));
            sq.Append(effect.transform.DOJump(endPosition, 2.0f, 1, duration).SetEase(Ease.InOutSine));
            sq.OnComplete(delegate
            {
                CardVFXPool.Instance.ReleaseItem(effect);
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            });
        }

        public void HeroProjectileEffect(string vfxKey, Vector3 startPosition, Vector3 endPosition, UnityAction onComplete, float duration = 0.3f)
        {
            // Create Effect
            CardVFXItem effect = CardVFXPool.Instance.CreateItem(CardVFXPool.RootHeroEFXPath + vfxKey);
            effect.transform.position = startPosition;
            effect.SetStartPosition(startPosition);
            effect.SetEnableUpdateFacing(true);
            effect.transform.LookAt(endPosition);

            Sequence sq = DOTween.Sequence();
            //sq.Append(effect.transform.DOMove(endPosition, duration).SetEase(Ease.InOutSine));
            sq.Append(effect.transform.DOJump(endPosition, 2.0f, 1, duration).SetEase(Ease.InOutSine));
            sq.OnComplete(delegate
            {
                CardVFXPool.Instance.ReleaseItem(effect);
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            });
        }
        #endregion

        #region Static Effect(Spawn at target)
        public void SpawnVFX(CardVFXPool.VFXType type, CardUI card,Vector3 targetPosition, UnityAction onComplete, bool isAttach = false, bool destroySelf = true)
        {
            CardVFXItem vfx = CardVFXPool.Instance.CreateItem(type);
            if (isAttach)
            {
                vfx.transform.position = targetPosition;
                vfx.transform.localScale = new Vector3(1, 1, 1);
                vfx.transform.rotation = card.transform.rotation;
                //vfx.transform.SetParent(transform); // ระวัง อย่าลบ parent ก่อนที่จะ releaseItem ไม่งั้นจะทำให้เกิด Null Ref. 

                vfx.AttachGameObject(card.gameObject);

                vfx.SetAnimationComplete(delegate ()
                {
                    CardVFXPool.Instance.ReleaseItem(vfx);
                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                });
            }
            else
            {
                vfx.transform.position = card.transform.position;
                vfx.transform.rotation = card.transform.rotation;
                vfx.transform.SetParent(card.transform); // ระวัง อย่าลบ parent ก่อนที่จะ releaseItem ไม่งั้นจะทำให้เกิด Null Ref. 
                vfx.transform.localScale = new Vector3(1, 1, 1);

                vfx.SetAnimationComplete(delegate ()
                {
                    CardVFXPool.Instance.ReleaseItem(vfx);
                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                });
            }
        }

        public void SpawnVFX(CardVFXPool.VFXType type, CardUI card, Vector3 targetPosition, UnityAction onImpact, UnityAction onComplete, bool isAttach = false, bool destroySelf = true)
        {
            CardVFXItem vfx = CardVFXPool.Instance.CreateItem(type);
            if (isAttach)
            {
                vfx.transform.position = targetPosition;
                vfx.transform.localScale = new Vector3(1, 1, 1);
                vfx.transform.rotation = card.transform.rotation;
                //vfx.transform.SetParent(transform); // ระวัง อย่าลบ parent ก่อนที่จะ releaseItem ไม่งั้นจะทำให้เกิด Null Ref. 

                vfx.AttachGameObject(card.gameObject);
                vfx.SetImpactComplete(delegate
                {
                    if (onImpact != null)
                    {
                        onImpact.Invoke();
                    }
                });
                vfx.SetAnimationComplete(delegate ()
                {
                    CardVFXPool.Instance.ReleaseItem(vfx);
                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                });
            }
            else
            {
                vfx.transform.position = card.transform.position;
                vfx.transform.rotation = card.transform.rotation;
                vfx.transform.SetParent(card.transform); // ระวัง อย่าลบ parent ก่อนที่จะ releaseItem ไม่งั้นจะทำให้เกิด Null Ref. 
                vfx.transform.localScale = new Vector3(1, 1, 1);
                vfx.SetImpactComplete(delegate
                {
                    if (onImpact != null)
                    {
                        onImpact.Invoke();
                    }
                });
                vfx.SetAnimationComplete(delegate ()
                {
                    CardVFXPool.Instance.ReleaseItem(vfx);
                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                });
            }
        }

        public void SpawnVFX(CardVFXPool.VFXType type, Vector3 targetPosition, UnityAction onImpact, UnityAction onComplete = null, bool destroySelf = true)
        {
            CardVFXItem vfx = CardVFXPool.Instance.CreateItem(type);
            vfx.transform.position = targetPosition;
            //vfx.transform.SetParent(targetPosition); // ระวัง อย่าลบ parent ก่อนที่จะ releaseItem ไม่งั้นจะทำให้เกิด Null Ref. 

            vfx.SetImpactComplete(delegate
            {
                if (onImpact != null)
                {
                    onImpact.Invoke();
                }
            });
            vfx.SetAnimationComplete(delegate ()
            {
                CardVFXPool.Instance.ReleaseItem(vfx);
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            });
        }

        public void AbilityGroupEffect(string vfxKey, Vector3 targetPosition, UnityAction onComplete)
        {
            // Fail Safe
            if (vfxKey == "-" || string.IsNullOrWhiteSpace(vfxKey))
            {
                onComplete?.Invoke();
                return;
            }

            // Create Effect
            CardVFXItem vfx = CardVFXPool.Instance.CreateItem(CardVFXPool.RootAbilityEFXPath + vfxKey);
            vfx.transform.position = targetPosition;

            vfx.SetImpactComplete(delegate ()
            {
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            });
            vfx.SetAnimationComplete(delegate ()
            {
                CardVFXPool.Instance.ReleaseItem(vfx);
            });
        }

        public void AbilityRowEffect(string vfxKey, Vector3 targetPosition, List<CardDirectionType> directionList, UnityAction onComplete)
        {
            // Fail Safe
            if (vfxKey == "-" || string.IsNullOrWhiteSpace(vfxKey))
            {
                onComplete?.Invoke();
                return;
            }

            int index = 0;
            foreach (CardDirectionType item in directionList)
            {
                if (index == 0)
                {
                    // Create Effect
                    CardVFXItem vfx = CardVFXPool.Instance.CreateItem(CardVFXPool.RootAbilityEFXPath + vfxKey);
                    vfx.transform.position = targetPosition;
                    Vector3 rotate = new Vector3(0, ((int)item) * 45.0f, 0);
                    vfx.transform.localRotation = Quaternion.Euler(rotate);
                    Debug.LogWarning(item + " <b>LocalRotate:</b> " + vfx.transform.localRotation);
                    Debug.LogWarning(item + " <b>Rotate:</b> " + vfx.transform.rotation);

                    vfx.SetImpactComplete(delegate ()
                    {
                        if (onComplete != null)
                        {
                            onComplete.Invoke();
                        }
                    });
                    vfx.SetAnimationComplete(delegate ()
                    {
                        CardVFXPool.Instance.ReleaseItem(vfx);
                    });
                }
                else
                {
                    // Create Effect
                    CardVFXItem vfx = CardVFXPool.Instance.CreateItem(CardVFXPool.RootAbilityEFXPath + vfxKey);
                    vfx.transform.position = targetPosition;
                    Vector3 rotate = new Vector3(0, ((int)item) * 45.0f, 0);
                    vfx.transform.localRotation = Quaternion.Euler(rotate);
                    Debug.LogWarning(item + " <b>LocalRotate:</b> " + vfx.transform.localRotation);
                    Debug.LogWarning(item + " <b>Rotate:</b> " + vfx.transform.rotation);

                    vfx.SetAnimationComplete(delegate ()
                    {
                        CardVFXPool.Instance.ReleaseItem(vfx);
                    });
                }
                index++;
            }

        }

        public void HeroImpactEffect(string vfxKey, Vector3 targetPosition, UnityAction onComplete)
        {
            // Create Effect
            CardVFXItem vfx = CardVFXPool.Instance.CreateItem(CardVFXPool.RootHeroEFXPath + vfxKey);
            vfx.transform.position = targetPosition;

            vfx.SetImpactComplete(delegate ()
            {
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            });
            vfx.SetAnimationComplete(delegate ()
            {
                CardVFXPool.Instance.ReleaseItem(vfx);
            });
        }

        public void SpawnDeathVFX(string type, CardUI card, Vector3 targetPosition, UnityAction onComplete, bool isAttach = false, bool destroySelf = true)
        {
            CardVFXItem vfx = CardVFXPool.Instance.CreateDeathEffect(type);
            if (isAttach)
            {
                vfx.transform.position = targetPosition;
                //vfx.transform.rotation = transform.rotation;
                //vfx.transform.SetParent(transform); // ระวัง อย่าลบ parent ก่อนที่จะ releaseItem ไม่งั้นจะทำให้เกิด Null Ref. 

                vfx.SetAnimationComplete(delegate ()
                {
                    CardVFXPool.Instance.ReleaseItem(vfx);
                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                });
                vfx.AttachGameObject(card.gameObject);
            }
            else
            {
                vfx.transform.position = card.transform.position;
                vfx.transform.rotation = card.transform.rotation;
                vfx.transform.SetParent(card.transform); // ระวัง อย่าลบ parent ก่อนที่จะ releaseItem ไม่งั้นจะทำให้เกิด Null Ref. 

                vfx.SetAnimationComplete(delegate ()
                {
                    CardVFXPool.Instance.ReleaseItem(vfx);
                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                });
            }
        }

        public void SpawnLastwishVFX(CardUI card, Vector3 targetPosition, UnityAction onComplete, bool isAttach = false, bool destroySelf = true)
        {
            CardVFXItem vfx = CardVFXPool.Instance.CreateDeathEffect("VFX_Death_Lastwish");
            if (isAttach)
            {
                vfx.transform.position = targetPosition;
                //vfx.transform.rotation = transform.rotation;
                //vfx.transform.SetParent(transform); // ระวัง อย่าลบ parent ก่อนที่จะ releaseItem ไม่งั้นจะทำให้เกิด Null Ref. 

                vfx.SetAnimationComplete(delegate ()
                {
                    CardVFXPool.Instance.ReleaseItem(vfx);
                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                });
                vfx.AttachGameObject(card.gameObject);
            }
            else
            {
                vfx.transform.position = card.transform.position;
                vfx.transform.rotation = card.transform.rotation;
                vfx.transform.SetParent(card.transform); // ระวัง อย่าลบ parent ก่อนที่จะ releaseItem ไม่งั้นจะทำให้เกิด Null Ref. 

                vfx.SetAnimationComplete(delegate ()
                {
                    CardVFXPool.Instance.ReleaseItem(vfx);
                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                });
            }
        }

        public void SpawnSummonVFX(string type, CardUI card, Vector3 targetPosition, UnityAction onImpact, UnityAction onComplete, bool isAttach = false, bool destroySelf = true)
        {
            CardVFXItem vfx = CardVFXPool.Instance.CreateSpawnEffect(type);
            if (isAttach)
            {
                vfx.transform.position = targetPosition;
                //vfx.transform.rotation = transform.rotation;
                //vfx.transform.SetParent(transform); // ระวัง อย่าลบ parent ก่อนที่จะ releaseItem ไม่งั้นจะทำให้เกิด Null Ref. 
                vfx.SetImpactComplete(onImpact);
                vfx.SetAnimationComplete(delegate ()
                {
                    CardVFXPool.Instance.ReleaseItem(vfx);
                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                });
                vfx.AttachGameObject(card.gameObject);
            }
            else
            {
                vfx.transform.position = card.transform.position;
                vfx.transform.rotation = card.transform.rotation;
                vfx.transform.SetParent(card.transform); // ระวัง อย่าลบ parent ก่อนที่จะ releaseItem ไม่งั้นจะทำให้เกิด Null Ref. 

                vfx.SetImpactComplete(onImpact);
                vfx.SetAnimationComplete(delegate ()
                {
                    CardVFXPool.Instance.ReleaseItem(vfx);
                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                });
            }
        }

        public void SpawnAttackVFX(string type, Vector3 targetPosition, UnityAction onComplete,Vector3 dir, int range = 0, bool destroySelf = true)
        {
            CardVFXItem vfx = CardVFXPool.Instance.CreateAttackEffect(type);
            vfx.transform.position = targetPosition;
            vfx.transform.LookAt(targetPosition + (dir * 10.0f));
            //vfx.transform.SetParent(targetPosition); // ระวัง อย่าลบ parent ก่อนที่จะ releaseItem ไม่งั้นจะทำให้เกิด Null Ref. 
            
            // Setup pierce attack range.
            CardVFXAttackItem atkVfx = vfx.gameObject.GetComponent<CardVFXAttackItem>();
            if (atkVfx != null)
            {
                atkVfx.SetAttackRange(range);
            }

            // Setup animation OnComplete
            vfx.SetAnimationComplete(delegate ()
            {
                CardVFXPool.Instance.ReleaseItem(vfx);
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            });
        }
        #endregion

        #region Popup Effect(Same as static but has Text inside)
        public void SpawnPopupEffect(PopupPool.PopupType type, int value, Vector3 targetPosition, UnityAction onComplete, bool isAttach = false, bool destroySelf = true)
        {
            // Value Negative
            if (value < 0)
            {
                value = 0;
            }

            PopupItem popup = PopupPool.Instance.CreateItem(type);
            popup.GetComponent<CardPopup>().SetNumber(value);
            if (isAttach)
            {
                popup.transform.position = targetPosition;
                //popup.transform.rotation = transform.rotation;
                //popup.transform.SetParent(transform); // ระวัง อย่าลบ parent ก่อนที่จะ releaseItem ไม่งั้นจะทำให้เกิด Null Ref. 
                popup.GetComponent<CardPopup>().SetAnimationComplete(delegate ()
                {
                    PopupPool.Instance.ReleaseItem(popup);

                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                });
                popup.GetComponent<CardPopup>().PlayPopup();

                if (type == PopupPool.PopupType.DealDamage)
                {
                    SoundManager.PlaySFX(SoundManager.SFX.Minion_Be_Attacked);
                }
                if (type == PopupPool.PopupType.DealDamage_BattleArmor)
                {
                    SoundManager.PlaySFX(SoundManager.SFX.Minion_Armor_Be_Attacked);
                }
                else if (type == PopupPool.PopupType.Heal)
                {
                    SoundManager.PlaySFX(SoundManager.SFX.Heal);
                }
            }
            else
            {
                popup.transform.position = targetPosition;
                popup.transform.rotation = transform.rotation;
                popup.transform.SetParent(transform); // ระวัง อย่าลบ parent ก่อนที่จะ releaseItem ไม่งั้นจะทำให้เกิด Null Ref. 

                popup.GetComponent<CardPopup>().SetAnimationComplete(delegate ()
                {
                    PopupPool.Instance.ReleaseItem(popup);

                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                });

                if (type == PopupPool.PopupType.DealDamage)
                {
                    SoundManager.PlaySFX(SoundManager.SFX.Minion_Be_Attacked);
                }
                if (type == PopupPool.PopupType.DealDamage_BattleArmor)
                {
                    SoundManager.PlaySFX(SoundManager.SFX.Minion_Armor_Be_Attacked);
                }
                else if (type == PopupPool.PopupType.Heal)
                {
                    SoundManager.PlaySFX(SoundManager.SFX.Heal);
                }
            }
        }
        #endregion
    }
}
