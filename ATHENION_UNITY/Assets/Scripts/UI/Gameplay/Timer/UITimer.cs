﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Karamucho.UI
{
    public class UITimer : MonoBehaviour
    {
        #region Inspector
        [SerializeField] private TextMeshPro TEXT_Timer;
        #endregion

        #region Public Properties
        public Color BlueColor;
        public Color RedColor;
        public Color BlueFlareColor;
        public Color RedFlareColor;
        public Sprite BlueFlare;
        public Sprite RedFlare;

        public Animator TimerAnimator;

        [Header("Reference")]
        public SpriteRenderer TimeSprite;
        public SpriteRenderer FlareSprite1;
        public SpriteRenderer FlareSprite2;
        public SpriteRenderer FlareSprite3;
        #endregion

        #region Private Properties
        private int _soundIndex = -1;
        #endregion

        #region Methods

        private void Start()
        {
            gameObject.SetActive(false);
        }

        public void UpdateTime(float time)
        {
            TEXT_Timer.text = time.ToString("N0");
        }

        public void ShowTimer(CardUIManager.PlayerPosition playerPosition, float duration)
        {
            if (!gameObject.activeSelf)
            {
                gameObject.SetActive(true);
                SetAnimationSpeed(duration);
                SetColor(playerPosition);
                TimerAnimator.Play("Timer_Start");

                SoundManager.PlaySFX(SoundManager.SFX.Countdown_Start);
                _soundIndex = SoundManager.PlaySFX(SoundManager.SFX.Countdown_Loop, true);
            }
           
        }

        public void HideTimer()
        {
            if (gameObject.activeSelf)
            {
                TimerAnimator.SetTrigger("isOut");
                gameObject.SetActive(false);
            }

            if (_soundIndex != -1)
            {
                SoundManager.StopSFX(_soundIndex, 0.0f);
            }
        }

        private void SetAnimationSpeed(float duration)
        {
            // Base Duration = 20 second
            TimerAnimator.speed = 20.0f/duration;
        }

        private void SetColor(CardUIManager.PlayerPosition playerPosition)
        {
            switch (playerPosition)
            {
                case CardUIManager.PlayerPosition.Top:
                    TimeSprite.color = RedColor;
                    FlareSprite1.color = RedFlareColor;
                    FlareSprite2.color = RedFlareColor;
                    FlareSprite3.sprite = RedFlare;
                    transform.localRotation = Quaternion.Euler(90, 0, 180);
                    break;
                case CardUIManager.PlayerPosition.Bottom:
                    TimeSprite.color = BlueColor;
                    FlareSprite1.color = BlueFlareColor;
                    FlareSprite2.color = BlueFlareColor;
                    FlareSprite3.sprite = BlueFlare;
                    transform.localRotation = Quaternion.Euler(90, 0, 0);
                    break;
            }
        }
        #endregion


    }
}
