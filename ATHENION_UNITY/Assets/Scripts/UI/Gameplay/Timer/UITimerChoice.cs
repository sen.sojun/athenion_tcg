﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class UITimerChoice : MonoBehaviour, ITimerUI
{
    #region Public Properties
    public Image ProgressImage;
    public TextMeshProUGUI TimeCount;
    public Animator Animator;
    #endregion

    #region Private Properties
    private static readonly int _countdownTime = 5;
    private int _tempTime;
    private float _maxTime;
    private Vector2 _originalSize;
    private bool _isSetSound = false;
    private SoundManager.SFX _tickEFX;
    private SoundManager.SFX _endEFX;
    private UnityAction _onShowComplete = null;
    #endregion

    private void Awake()
    {
        _originalSize = ProgressImage.rectTransform.sizeDelta;
    }

    #region ITimerUI Interface
    void ITimerUI.OnUpdateTime(float remainTime)
    {
        if ((int)remainTime != _tempTime)
        {
            _tempTime = (int)remainTime;
            //Debug.Log(_tempTime);
            UpdateTimeText();

            if (_isSetSound && _tempTime < _countdownTime)
            {
                SoundManager.PlaySFX(_tickEFX);
            }
        }
        UpdateProgressBar(remainTime);

        if (remainTime <= 0.0f)
        {
            if (_isSetSound)
            {
                SoundManager.PlaySFX(_endEFX);
            }
        }
    }

    void ITimerUI.SetMaxTime(float maxTime)
    {
        _maxTime = maxTime;
        TimeCount.text = _maxTime.ToString();
        _isSetSound = false;
    }

    void ITimerUI.SetSound(SoundManager.SFX tickSFX, SoundManager.SFX endSFX)
    {
        _tickEFX = tickSFX;
        _endEFX = endSFX;
        _isSetSound = true;
    }
    #endregion

    public void ShowUI(bool isShow, UnityAction onShowComplete = null)
    {
        _onShowComplete = onShowComplete;

        if (isShow)
        {
            gameObject.SetActive(true);
            Animator.SetBool("IsShow", true);

            if (_onShowComplete != null)
            {
                _onShowComplete.Invoke();
            }
        }
        else
        {
            Animator.SetBool("IsShow", false);
        } 
    }

    public void OnComplete()
    {
        gameObject.SetActive(false);

        if (_onShowComplete != null)
        {
            _onShowComplete.Invoke();
        }
    }

    private void UpdateTimeText()
    {
        TimeCount.text = _tempTime.ToString();

        if (_tempTime < _countdownTime)
        {
            TimeCount.color = Color.red;
        }
        else
        {
            TimeCount.color = Color.white;
        }

        Sequence sq = DOTween.Sequence();
        sq.Append(TimeCount.transform.DOScale(1.5f, 0.3f));
        sq.Append(TimeCount.transform.DOScale(1.0f, 0.3f));
    }

    private void UpdateProgressBar(float remainTime)
    {
        ProgressImage.rectTransform.sizeDelta = new Vector2(
            _originalSize.x * (remainTime/ _maxTime),
            _originalSize.y
        );
    }
}
