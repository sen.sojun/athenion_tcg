﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

namespace Karamucho.UI
{
    public class UIGraveyard : MonoBehaviour
    {

        #region Inspector
        [Header("Reference")]
        [SerializeField] private Transform GROUP_Container;
        [SerializeField] private ScrollRect SCROLL_Main;
        [SerializeField] private TokenUI Prefab_TokenUI;
        [SerializeField] private TextMeshProUGUI TEXT_Title;
        [SerializeField] private Button BTN_Close;
        #endregion

        #region Public Properties
        private const string _localizeKey_your_grave = "GRAVEYARD_PLAYER";
        private const string _localizeKey_enemy_grave = "GRAVEYARD_ENEMY";
        #endregion

        #region Private Properties
        private TokenUI _currentHilight;
        private List<TokenUI> _cellList = new List<TokenUI>();
        #endregion

        #region Methods
        private void CreateTokenUI(List<CardUIData> cardUIDataList)
        {
            for (int i=0;i < cardUIDataList.Count || i < _cellList.Count ; i++)
            {
                if (_cellList.Count > i && i < cardUIDataList.Count)
                {
                    // ard Have token.
                    _cellList[i].SetData(cardUIDataList[i]);
                    _cellList[i].gameObject.SetActive(true);

                }
                else if (_cellList.Count > i && i >= cardUIDataList.Count)
                {
                    _cellList[i].gameObject.SetActive(false);
                }
                else
                {
                    // Don't have token. Create new obj.
                    GameObject obj = Instantiate(Prefab_TokenUI.gameObject, GROUP_Container);
                    TokenUI cell = obj.GetComponent<TokenUI>();
                    cell.SetData(cardUIDataList[i]);
                    cell.SetScroll(SCROLL_Main);
                    _cellList.Add(cell);
                }
            }

        }

        public void ShowUI(List<CardUIData> cardUIDataList, bool isLocal)
        {
            // Set Text
            string title = LocalizationManager.Instance.GetText(isLocal ? _localizeKey_your_grave : _localizeKey_enemy_grave);
            TEXT_Title.text = title;

            GameHelper.UITransition_FadeIn(this.gameObject);
            CreateTokenUI(cardUIDataList);
            SoundManager.PlaySFX(SoundManager.SFX.Open_Close_Graveyard);
        }

        public void HideUI()
        {
            GameHelper.UITransition_FadeOut(this.gameObject);
            HideGraveHilight();
            SoundManager.PlaySFX(SoundManager.SFX.Open_Close_Graveyard);
        }

        public void HideUIGraveyardUI()
        {
            UIManager.Instance.HideGraveyard();
        }

        public void HilightGraveToken(TokenUI cardData)
        {
            if (_currentHilight != null)
            {
                _currentHilight.ShowGlow(false);
                _currentHilight = null;

            }

            cardData.ShowGlow(true);
            _currentHilight = cardData;
            UIManager.Instance.CardUIDetailHover.SetData(cardData.Data); 
            UIManager.Instance.CardUIDetailHover.UpdateGravePosition(_currentHilight.transform.position);
            UIManager.Instance.ShowDetailCardUI(true);

        }

        public void HideGraveHilight()
        {
            if (_currentHilight != null)
            {
                _currentHilight.ShowGlow(false);
                UIManager.Instance.ShowDetailCardUI(false);
                _currentHilight = null;
            }
        }
        #endregion
    }
}
