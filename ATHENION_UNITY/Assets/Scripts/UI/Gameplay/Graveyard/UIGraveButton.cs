﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Karamucho.UI
{
    public class UIGraveButton : MonoBehaviour, IPointerClickHandler
    {
        public LocalPlayerIndex localPlayerIndex;
        public void OnPointerClick(PointerEventData eventData)
        {
            UIManager.Instance.RequestShowGraveyard(localPlayerIndex);
        }
    }
}
