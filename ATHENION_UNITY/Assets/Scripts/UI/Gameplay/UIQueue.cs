﻿using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;

namespace Karamucho.UI
{
    public class UIQueue : GameQueue<UIQueue, UIData>
    {
        public override bool IsReady
        {
            get
            {
                bool isReady = base.IsReady;

                if (_secondQueue != null)
                {
                    isReady &= (_secondQueue.Count <= 0);
                }

                return isReady;
            }
        }

        public bool IsReady2ndNext
        {
            get
            {
                return base.IsReady;
            }
        }

        private List<List<UIData>> _secondQueue;

        protected override void Update()
        {
            if (_queue != null || _secondQueue != null)
            {
                OnUpdate();
            }
        }

        protected override void OnUpdate()
        {
            if (!IsPause)
            {
                if (_queue != null && _queue.Count > 0)
                {
                    switch (_queue[0].PlayMode)
                    {
                        case QueuePlayMode.Parallel:
                        {
                            UIData data = _queue[0];
                            _queue.RemoveAt(0);
                            Active(data);
                        }
                        break;

                        case QueuePlayMode.Sequence:
                        default:
                        {
                            if (IsReadyNext)
                            {
                                UIData data = _queue[0];
                                _queue.RemoveAt(0);
                                Active(data);
                            }
                        }
                        break;
                    }
                }
                else
                {
                    // queue is empty
                    if (_secondQueue != null && _secondQueue.Count > 0)
                    {
                        if (_secondQueue[0].Count > 0)
                        {
                            UIData command = _secondQueue[0][0];
                            _secondQueue[0].RemoveAt(0);

                            _queue.Add(command);
                        }
                        else
                        {
                            _secondQueue.RemoveAt(0);
                        }
                        OnUpdate();
                    }
                }
            }
        }

        public void AddFirstSecondQueue(List<UIData> commandList)
        {
            if (_secondQueue == null)
            {
                _secondQueue = new List<List<UIData>>();
            }

            _secondQueue.Insert(0, commandList);
        }

        public void AddSecondQueue(List<UIData> commandList)
        {
            if (_secondQueue == null)
            {
                _secondQueue = new List<List<UIData>>();
            }

            _secondQueue.Add(commandList);
        }

        public override string GetDebugText()
        {
            string text = base.GetDebugText();

            if (_sb == null) _sb = new StringBuilder();
            _sb.Clear();

            _sb.Append("\n2nd nQueue:");
            if (_secondQueue != null && _secondQueue.Count > 0)
            {
                int i = 0;
                foreach (List<UIData> list in _secondQueue)
                {
                    _sb.AppendFormat(" <b>L</b> ");
                    foreach (UIData command in list)
                    {
                        _sb.AppendFormat(" <b>[{0}]</b>{1}"
                            , command.PlayMode == QueuePlayMode.Parallel ? "J" : "A"
                            , command.GetDebugText()
                        );
                        ++i;
                    }


                    if (i > 10)
                    {
                        _sb.AppendFormat(" [and more....]");
                        break;
                    }
                }
            }
            else
            {
                _sb.Append(" Empty.");
            }

            return text + _sb.ToString();
        }
    }

    public class UIData : QueueData
    {
        #region Constructors
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;

            // Please call this when complete.
            OnActiveComplete();
        }

        public override string GetDebugText()
        {
            return string.Format("[{0}]{1}", _uniqueKey, this.GetType().ToString());
        }
        #endregion
    }

    #region Command UI
    public class UIUpdateCardUICmd : UIData
    {
        #region Private Properties
        private CardUIData _card;
        private string _key;
        #endregion

        #region Constructor
        public UIUpdateCardUICmd(CardUIData card, string key = "")
        {
            _card = new CardUIData(card);
            _key = key;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            UIManager.Instance.UpdateCardUI(_card, this.OnActiveComplete);
        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    public class UIUpdateCardAllPassiveCmd : UIData
    {
        #region Private Properties
        private CardUIData _card;
        private string _key;
        #endregion

        #region Constructor
        public UIUpdateCardAllPassiveCmd(CardUIData card, string key = "")
        {
            _card = new CardUIData(card);
            _key = key;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            Debug.LogFormat("UIUpdateCardAllPassiveCmd: {0}:{1}:{2}", _card.UniqueID, _card.Name, _card.CurrentZone);

            _onComplete = onComplete;
            UIManager.Instance.UpdateCardAllPassiveUI(_card, this.OnActiveComplete);
        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    public class UIUpdateCardPassiveCmd : UIData
    {
        #region Private Properties
        private CardUIData _card;
        private string _key;
        private CardStatusType _statusType;
        private bool _isActive;
        #endregion

        #region Constructor
        public UIUpdateCardPassiveCmd(CardUIData card,CardStatusType cardStatusType, bool isActive, string key = "")
        {
            _card = new CardUIData(card);
            _key = key;
            _statusType = cardStatusType;
            _isActive = isActive;

        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            UIManager.Instance.UpdateCardPassiveUI(_card, _statusType, _isActive, this.OnActiveComplete);
        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    public class UIShowEffectCardActivatingCmd : UIData
    {
        #region Private Properties
        private int _uniqueId;
        private string _key;
        #endregion

        #region Constructor
        public UIShowEffectCardActivatingCmd(int uniqueId, string key = "")
        {
            _uniqueId = uniqueId;
            _key = key;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            UIManager.Instance.ShowEffectAcitvating(_uniqueId, OnActiveComplete);
        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    public class UIHideEffectCardActivatingCmd : UIData
    {
        #region Private Properties
        private int _uniqueId;
        private string _key;
        #endregion

        #region Constructor
        public UIHideEffectCardActivatingCmd(int uniqueId, string key = "")
        {
            _uniqueId = uniqueId;
            _key = key;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            UIManager.Instance.HideEffectAcitvating(_uniqueId, OnActiveComplete);
        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    public class UIDrawCardCmd : UIData
    {
        #region Private Properties
        private CardUIData _card;
        private string _key;
        #endregion

        #region Constructor
        public UIDrawCardCmd(CardUIData card, string key = "")
        {
            _card = card;
            _key = key;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            CardUIManager.Instance.DrawCardOnly(
                  _card
                , delegate ()
                {
                    OnActiveComplete();
                }
            );
        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    public class UIDrawFastCmd : UIData
    {
        #region Private Properties
        private CardUIData _card;
        private CardUIManager.PlayerPosition _playerPosition;
        private string _key;
        #endregion

        #region Constructor
        public UIDrawFastCmd(CardUIData card, CardUIManager.PlayerPosition playerPosition, string key = "")
        {
            _card = card;
            _playerPosition = playerPosition;
            _key = key;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            CardUIManager.Instance.DrawFastCard(
                  _card
                , _playerPosition
                , delegate ()
                {
                    OnActiveComplete();
                }
            );
        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    public class UIDrawToSlotCmd : UIData
    {
        #region Private Properties
        private CardUIData _card;
        private CardUIManager.PlayerPosition _playerPosition;
        private string _key;
        #endregion

        #region Constructor
        public UIDrawToSlotCmd(CardUIData card, CardUIManager.PlayerPosition playerPosition, string key = "")
        {
            _card = card;
            _playerPosition = playerPosition;
            _key = key;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            CardUIManager.Instance.DrawCard(
                  _card
                , _playerPosition
                , delegate ()
                {
                    OnActiveComplete();
                }
            );
        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    public class UIDrawToSlotFromGraveCmd : UIData
    {
        #region Private Properties
        private CardUIData _card;
        private CardUIManager.PlayerPosition _playerPosition;
        private string _key;
        #endregion

        #region Constructor
        public UIDrawToSlotFromGraveCmd(CardUIData card, CardUIManager.PlayerPosition playerPosition, string key = "")
        {
            _card = card;
            _playerPosition = playerPosition;
            _key = key;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            CardUIManager.Instance.DrawCardFromGrave(_card, _playerPosition, OnActiveComplete);
        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    public class UIDrawToDestroyCmd : UIData
    {
        #region Private Properties
        private CardUIData _card;
        private CardUIManager.PlayerPosition _playerPosition;
        private string _key;
        #endregion

        #region Constructor
        public UIDrawToDestroyCmd(CardUIData card, CardUIManager.PlayerPosition playerPosition, string key = "")
        {
            _card = card;
            _playerPosition = playerPosition;
            _key = key;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            CardUIManager.Instance.DrawCardToDestroy(_card, _playerPosition, OnActiveComplete);
        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    public class UINoDrawToDestroyCmd : UIData
    {
        #region Private Properties
        private PlayerIndex _playerIndex;
        private string _key;
        #endregion

        #region Constructor
        public UINoDrawToDestroyCmd(PlayerIndex playerIndex, string key = "")
        {
            _playerIndex = playerIndex;
            _key = key;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            CardUIManager.Instance.DrawNoMoreCardToDestroy(_playerIndex, OnActiveComplete);
        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    public class UIDiscardCmd : UIData
    {
        #region Private Properties
        private CardUIData _card;
        private string _key;
        #endregion

        #region Constructor
        public UIDiscardCmd(CardUIData card, string key = "")
        {
            _card = card;
            _key = key;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            CardUIManager.Instance.DiscardCard(_card, OnActiveComplete);
        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    public class UIRemoveCardCmd : UIData
    {
        #region Private Properties
        private CardUIData _card;
        private string _key;
        #endregion

        #region Constructor
        public UIRemoveCardCmd(CardUIData card, string key = "")
        {
            _card = card;
            _key = key;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            CardUIManager.Instance.RemoveCard(_card, OnActiveComplete);
        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    public class UISpawnTokenToBoardCmd : UIData
    {
        #region Private Properties
        private CardUIData _card;
        private string _key;
        private UnityAction _onUIComplete;
        #endregion

        #region Constructor
        public UISpawnTokenToBoardCmd(CardUIData card, UnityAction onUIComplete, string key = "")
        {
            _card = card;
            _key = key;
            _onUIComplete = onUIComplete;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            CardUIManager.Instance.SpawnTokenToBoard(_card, OnSpawnComplete);
        }

        public void OnSpawnComplete()
        {
            if (_onUIComplete != null)
            {
                _onUIComplete.Invoke();
            }
            OnActiveComplete();
        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    public class UIDestroyToken : UIData
    {
        #region Private Properties
        private int _id;
        private string _key;
        #endregion

        #region Constructor
        public UIDestroyToken(int id, string key = "")
        {
            _id = id;
            _key = key;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            BoardManager.Instance.DestroyTokenByID(_id, OnActiveComplete);
        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );
        }
        #endregion
    }

    public class UIBanishToken : UIData
    {
        #region Private Properties
        private int _id;
        private string _key;
        #endregion

        #region Constructor
        public UIBanishToken(int id, string key = "")
        {
            _id = id;
            _key = key;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            BoardManager.Instance.BanishTokenByID(_id, OnActiveComplete);
        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    public class UIMoveTokenToHandCmd : UIData
    {
        #region Private Properties
        private CardUIData _card;
        private CardUIManager.PlayerPosition _playerPosition;
        private string _key;
        #endregion

        #region Constructor
        public UIMoveTokenToHandCmd(CardUIData card, CardUIManager.PlayerPosition playerPosition, string key = "")
        {
            _card = card;
            _playerPosition = playerPosition;
            _key = key;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            BoardManager.Instance.MoveTokenToHandByID(_card.UniqueID, _playerPosition, OnActiveComplete);
        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    public class UITokenAttackToCmd : UIData
    {
        #region Private Properties
        private CardUIData _atk;
        private CardUIData _def;
        private List<CardUIData> _defList = new List<CardUIData>();
        private int _damage;
        private bool _isCritical;
        private string _key;
        #endregion

        #region Constructor
        public UITokenAttackToCmd(CardUIData atk, CardUIData def, List<CardUIData> defList, int damage, bool isCritical, string key = "")
        {
            _atk = atk;
            _def = def;
            _defList = new List<CardUIData>(defList);
            _damage = damage;
            _isCritical = isCritical;
            _key = key;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            BoardManager.Instance.AttackTo(_atk, _def, _defList, _damage, _isCritical, OnActiveComplete);
        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    public class UIMoveTokenToSlotCmd : UIData
    {
        #region Private Properties
        private CardUIData _token;
        private int _slotId;
        private string _key;
        #endregion

        #region Constructor
        public UIMoveTokenToSlotCmd(CardUIData token, int id, string key = "")
        {
            _token = token;
            _slotId = id;
            _key = key;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            BoardManager.Instance.MoveTo(_token, _slotId, OnActiveComplete);
        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    public class UITokenGlowActivateCmd : UIData
    {
        #region Private Properties
        private int _cardUniqueID;
        private bool _isShow;
        private string _key;
        #endregion

        #region Constructor
        public UITokenGlowActivateCmd(int cardUniqueID, bool isShow, string key = "")
        {
            _cardUniqueID = cardUniqueID;
            _isShow = isShow;
            _key = key;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            BoardManager.Instance.TokenGlowActivate(_cardUniqueID, _isShow);
            OnActiveComplete();
        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    public class UIUpdatePlayerInfoCmd : UIData
    {
        #region Private Properties
        private PlayerIndex _playerIndex;
        private int _power;
        private int _life;
        private int _deckCount;
        private int _graveCount;
        string _key;
        #endregion

        #region Constructor
        public UIUpdatePlayerInfoCmd(PlayerIndex playerIndex, int power, int life, int deckCount, int graveCount, string key = "")
        {
            _playerIndex = playerIndex;
            _power = power;
            _life = life;
            _deckCount = deckCount;
            _graveCount = graveCount;
            _key = key;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            UIManager.Instance.UpdateUIPlayerInfo(_playerIndex,
                _power,
                _life,
                _deckCount,
                _graveCount
                );
            OnActiveComplete();
        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    public class UIDestroyAllCardCmd : UIData
    {
        #region Private Properties
        private PlayerIndex _playerIndex;
        private string _key;
        #endregion

        #region Constructor
        public UIDestroyAllCardCmd(PlayerIndex playerIndex, string key = "")
        {
            _playerIndex = playerIndex;
            _key = key;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            CardUIManager.Instance.DestroyAllCard(_playerIndex, OnActiveComplete);
        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    public class UIHeroDeadCmd : UIData
    {
        #region Private Properties
        private PlayerIndex _playerIndex;
        private UnityAction _onAnimationComplete;
        private string _key;
        #endregion

        #region Constructor
        public UIHeroDeadCmd(PlayerIndex playerIndex, string key = "")
        {
            _playerIndex = playerIndex;
            _key = key;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            UIManager.Instance.ShowHeroDead(_playerIndex, OnActiveComplete);
        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    public class UIShowSelectionSlotCmd : UIData
    {
        #region Private Properties
        private List<int> _listSlotId;
        private int _count;
        private UnityAction<List<int>> _onSelectComplete;
        private string _key;
        private float _decisionTime;
        private string _info;
        private UnityAction _onTimeOut;
        private WaitObject _timer;
        private CardUIData _data;
        private bool _isFinish = false;
        #endregion

        #region Constructor
        public UIShowSelectionSlotCmd(CardUIData data, List<int> listSlotId, int selectCount, float decisionTime, string info, UnityAction<List<int>> onComplete, UnityAction onTimeOut, string key = "")
        {
            _data = data;
            _listSlotId = listSlotId;
            _count = selectCount;
            _onSelectComplete = onComplete;
            _key = key;
            _decisionTime = decisionTime;
            _info = info;
            _onTimeOut = onTimeOut;
            _isFinish = false;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            UIManager.Instance.UISelection.ShowUI(_data, _info);
            if (_decisionTime > 0.0f)
            {
                UIManager.Instance.TimerChoice.ShowUI(true);
            }
            else
            {
                UIManager.Instance.TimerChoice.ShowUI(false);
            }

            BoardManager.Instance.ShowSlotHilight(
                _listSlotId
                , _count
                , delegate (List<int> list)
                {
                    if (_isFinish == false)
                    {
                        _isFinish = true;

                        if (_timer != null)
                        {
                            _timer.StopTimer(false);
                            _timer = null;
                        }

                        _onSelectComplete?.Invoke(list);
                        OnActiveComplete();
                    }
                }
            );

            if (_decisionTime > 0.0f)
            {
                _timer = GameManager.Instance.ActionStartTriggerTimer(
                    _decisionTime
                    , delegate ()
                    {
                        if (_isFinish == false)
                        {
                            _isFinish = true;

                            //UIManager.Instance.EndButtonIsOpen(true);
                            UIManager.Instance.HideSelectionSlot(delegate ()
                            {
                                _onTimeOut?.Invoke();
                                OnActiveComplete();
                            });
                        }
                    }
                    , SoundManager.SFX.Mulligan_Countdown_Tick
                    , SoundManager.SFX.Mulligan_Countdown_Ended
                    , UIManager.Instance.TimerChoice
                );
                _timer.gameObject.name = "SelectSlotTimer ";
            }
            else
            {
                if (_timer != null)
                {
                    _timer.StopTimer(false);
                    _timer = null;
                }
            }

            return;
        }

        public override string GetDebugText()
        {
            return string.Format(
                  "{0}({1})"
                , this.GetType().ToString()
                , _key
            );
        }
        #endregion
    }

    #endregion

    #region Effect UI Command

    public class UIShowPlayerImmuneCmd : UIData
    {
        #region Private Properties
        private PlayerIndex _playerIndex;
        private bool _isImmune;
        private string _key;
        #endregion

        #region Constructor
        public UIShowPlayerImmuneCmd(PlayerIndex playerIndex, bool isImmune, string key = "")
        {
            _playerIndex = playerIndex;
            _isImmune = isImmune;
            _key = key;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            UIManager.Instance.ShowPlayerImmune(_playerIndex, _isImmune);
            OnActiveComplete();
        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    public class UIShowPopupAttackOnCardCmd : UIData
    {
        #region Private Properties
        private int _damage;
        private CardUIData _defender;
        private bool _isCritical;
        private string _key;
        #endregion

        #region Constructor
        public UIShowPopupAttackOnCardCmd(int damage, CardUIData defender, bool isCritical, string key = "")
        {
            _damage = damage;
            _defender = defender;
            _isCritical = isCritical;
            _key = key;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            UIManager.Instance.ShowPopupAttackOnCard(_damage, _defender, _isCritical, OnActiveComplete);
        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    public class UIShowPopupDamageOnCardCmd : UIData
    {
        #region Private Properties
        private int _damage;
        private CardUIData _defender;
        private string _key;
        #endregion

        #region Constructor
        public UIShowPopupDamageOnCardCmd(int damage, CardUIData defender, string key = "")
        {
            _damage = damage;
            _defender = defender;
            _key = key;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            UIManager.Instance.ShowPopupDamageOnCard(_damage, _defender, OnActiveComplete);
        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    public class UIShowPopupHealOnCardCmd : UIData
    {
        #region Private Properties
        private int _value;
        private CardUIData _target;
        private string _key;
        #endregion

        #region Constructor
        public UIShowPopupHealOnCardCmd(int value, CardUIData target, string key = "")
        {
            _value = value;
            _target = target;
            _key = key;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            UIManager.Instance.ShowPopupHealOnCard(_value, _target, OnActiveComplete);
        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    public class UIShowPopupDamageOnPlayerCmd : UIData
    {
        #region Private Properties
        private int _value;
        private PlayerIndex _playerIndex;
        private string _key;
        #endregion

        #region Constructor
        public UIShowPopupDamageOnPlayerCmd(int value, PlayerIndex playerIndex, string key = "")
        {
            _value = value;
            _playerIndex = playerIndex;
            _key = key;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;

            UIManager.Instance.ShowTauntAnimation(_playerIndex, BaseHeroAnimation.HeroAnimationState.HIT);
            UIManager.Instance.ShowPopupDamageOnPlayer(_value, _playerIndex, OnActiveComplete);
        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    public class UIShowPopupHealOnPlayerCmd : UIData
    {
        #region Private Properties
        private int _value;
        private PlayerIndex _playerIndex;
        private string _key;
        #endregion

        #region Constructor
        public UIShowPopupHealOnPlayerCmd(int value, PlayerIndex playerIndex, string key = "")
        {
            _value = value;
            _playerIndex = playerIndex;
            _key = key;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            UIManager.Instance.ShowPopupHealOnPlayer(_value, _playerIndex, OnActiveComplete);
        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    public class UIShowSpiritAtackOnPlayerCmd : UIData
    {
        #region Private Properties
        private int _value;
        private PlayerIndex _playerIndex;
        private string _key;
        #endregion

        #region Constructor
        public UIShowSpiritAtackOnPlayerCmd(int value, PlayerIndex playerIndex, string key = "")
        {
            _value = value;
            _playerIndex = playerIndex;
            _key = key;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            UIManager.Instance.ShowSpiritAttackOnPlayer(_value, _playerIndex, OnActiveComplete);
        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    public class UIShowSpiritAtackOnPlayerTutorialCmd : UIData
    {
        #region Private Properties
        private int _value;
        private PlayerIndex _playerIndex;
        private string _key;
        #endregion

        #region Constructor
        public UIShowSpiritAtackOnPlayerTutorialCmd(int value, PlayerIndex playerIndex, string key = "")
        {
            _value = value;
            _playerIndex = playerIndex;
            _key = key;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            UIManager.Instance.ShowSpiritAttackOnPlayerTutorial(_value, _playerIndex, OnActiveComplete);
        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    public class UIShowDamageFromMinionToMinionSingleCmd : UIData
    {
        #region Private Properties
        private Vector3 _atk;
        private Vector3 _def;
        private int _damage;
        private string _vfxKeyStart;
        private string _vfxKeyMoving;
        private string _vfxKeyEnd;
        private string _key;
        private UnityAction _onEffectComplete;
        #endregion

        #region Constructor
        public UIShowDamageFromMinionToMinionSingleCmd(Vector3 attacker, Vector3 defender, int damage, string vfxKeyStart, string vfxKeyMoving, string vfxKeyEnd, UnityAction onEffectComplete, string key = "")
        {
            _atk = attacker;
            _def = defender;
            _damage = damage;
            _vfxKeyStart = vfxKeyStart;
            _vfxKeyMoving = vfxKeyMoving;
            _vfxKeyEnd = vfxKeyEnd;
            _key = key;
            _onEffectComplete = onEffectComplete;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            UIManager.Instance.ShowDamageGroupAtMinion(_atk, _vfxKeyStart, delegate
            {
                UIManager.Instance.ShowDealDamageFromMinionToMinion(_atk,
                    _def,
                    _damage,
                    _vfxKeyMoving,
                    delegate
                    {
                        UIManager.Instance.ShowDamageGroupAtMinion(_def, _vfxKeyEnd, delegate
                        {
                            _onEffectComplete?.Invoke();
                            OnActiveComplete();
                        });
                    }
                );
            });
            
        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    public class UIShowDamageFromMinionToPlayerCmd : UIData
    {
        #region Private Properties
        private CardUIData _atk;
        private PlayerIndex _player;
        private int _damage;
        private string _vfxKey;
        private string _key;
        #endregion

        #region Constructor
        public UIShowDamageFromMinionToPlayerCmd(CardUIData attacker, PlayerIndex player, int damage, string vfxKey, string key = "")
        {
            _atk = attacker;
            _player = player;
            _damage = damage;
            _vfxKey = vfxKey;
            _key = key;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            UIManager.Instance.ShowDealDamageFromMinionToPlayer(_atk,
                _player,
                _damage,
                _vfxKey,
                OnActiveComplete
                );
        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    public class UIShowBoostAPFromMinionToPlayerCmd : UIData
    {
        #region Private Properties
        private CardUIData _atk;
        private PlayerIndex _player;
        private int _damage;
        private string _vfxKey;
        private string _key;
        #endregion

        #region Constructor
        public UIShowBoostAPFromMinionToPlayerCmd(CardUIData attacker, PlayerIndex player, int damage, string vfxKey, string key = "")
        {
            _atk = attacker;
            _player = player;
            _damage = damage;
            _vfxKey = vfxKey;
            _key = key;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            UIManager.Instance.ShowBoostAPFromMinionToPlayer(_atk,
                _player,
                _damage,
                _vfxKey,
                OnActiveComplete
                );
        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    public class UIShowDealDamageGroupCmd : UIData
    {
        #region Private Properties
        private Vector3 _target;
        private string _vfxKey;
        private string _key;
        private UnityAction _onEffectComplete;
        #endregion

        #region Constructor
        public UIShowDealDamageGroupCmd(Vector3 target, string vfxKey, UnityAction onEffectComplete, string key = "")
        {
            _target = target;
            _vfxKey = vfxKey;
            _key = key;
            _onEffectComplete = onEffectComplete;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            UIManager.Instance.ShowDamageGroupAtMinion(_target, _vfxKey, delegate
            {
                _onEffectComplete?.Invoke();
                OnActiveComplete();
            });
        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    public class UIShowDealDamageRowCmd : UIData
    {
        #region Private Properties
        private Vector3 _target;
        private string _vfxKey;
        private string _key;
        private List<CardDirectionType> _directionList = new List<CardDirectionType>();
        private UnityAction _onEffectComplete;
        #endregion

        #region Constructor
        public UIShowDealDamageRowCmd(Vector3 target, string vfxKey, List<CardDirectionType> directionList, UnityAction onEffectComplete, string key = "")
        {
            _target = target;
            _vfxKey = vfxKey;
            _key = key;
            _directionList = directionList;
            _onEffectComplete = onEffectComplete;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            UIManager.Instance.ShowDamageRowAtSlotWithDirection(_target, _vfxKey, _directionList, delegate
            {
                _onEffectComplete?.Invoke();
                OnActiveComplete();
            });
        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    public class UIShowDealDamageGlobalCmd : UIData
    {
        #region Private Properties
        private string _vfxKey;
        private string _key;
        private UnityAction _onEffectComplete;
        #endregion

        #region Constructor
        public UIShowDealDamageGlobalCmd(string vfxKey, UnityAction onEffecetComplete, string key = "")
        {
            _vfxKey = vfxKey;
            _key = key;
            _onEffectComplete = onEffecetComplete;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            UIManager.Instance.ShowDamageGlobal(_vfxKey, delegate
            {
                _onEffectComplete?.Invoke();
                OnActiveComplete();
            });
        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    public class UIPassiveActivateCmd : UIData
    {
        #region Private Properties
        private CardUIData _target;
        private string _key;
        private AbilityFeedback _abilityFeedback;
        #endregion

        #region Constructor
        public UIPassiveActivateCmd(CardUIData target, AbilityFeedback abilityFeedback, string key = "")
        {
            _target = target;
            _key = key;
            _abilityFeedback = abilityFeedback;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            UIManager.Instance.ShowIconPassiveActivate(_target, _abilityFeedback);
            OnActiveComplete();
        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    public class UIBoostActivateCmd : UIData
    {
        #region Private Properties
        private CardUIData _target;
        private string _key;
        #endregion

        #region Constructor
        public UIBoostActivateCmd(CardUIData target, string key = "")
        {
            _target = target;
            _key = key;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            UIManager.Instance.ShowBoostEffectOnMinion(_target, OnActiveComplete);
        }

        public override string GetDebugText()
        {
            return string.Format(
                  "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    public class UIWeakenActivateCmd : UIData
    {
        #region Private Properties
        private CardUIData _target;
        private string _key;
        #endregion

        #region Constructor
        public UIWeakenActivateCmd(CardUIData target, string key = "")
        {
            _target = target;
            _key = key;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            UIManager.Instance.ShowWeakenEffectOnMinion(_target, OnActiveComplete);
        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    public class UIDirectionActivateCmd : UIData
    {
        #region Private Properties
        private CardUIData _target;
        private string _key;
        #endregion

        #region Constructor
        public UIDirectionActivateCmd(CardUIData target, string key = "")
        {
            _target = target;
            _key = key;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            UIManager.Instance.ShowDirectionChangeEffectOnMinion(_target, OnActiveComplete);

        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    public class UIShowLastWishCmd : UIData
    {
        #region Private Properties
        private CardUI _target;
        private UnityAction _onAnimationComplete;
        private string _key;
        #endregion

        #region Constructor
        public UIShowLastWishCmd(CardUI target, UnityAction onAnimationComplete, string key = "")
        {
            _target = target;
            _onAnimationComplete = onAnimationComplete;
            _key = key;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            UIManager.Instance.ShowLastWishOnMinion(_target, delegate
            {
                if (_onAnimationComplete != null)
                {
                    _onAnimationComplete.Invoke();
                }
                OnActiveComplete();
            });

        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    public class UIShowLockCmd : UIData
    {
        #region Private Properties
        private int _target;
        private int _duration;
        private PlayerIndex _owner;
        private UnityAction _onAnimationComplete;
        private string _key;
        #endregion

        #region Constructor
        public UIShowLockCmd(int targetSlot, int duration, PlayerIndex owner, UnityAction onAnimationComplete, string key = "")
        {
            _target = targetSlot;
            _duration = duration;
            _owner = owner;
            _onAnimationComplete = onAnimationComplete;
            _key = key;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            UIManager.Instance.ShowLockSlotBySlotID(_target, _duration, _owner, delegate
            {
                if (_onAnimationComplete != null)
                {
                    _onAnimationComplete.Invoke();
                }
                OnActiveComplete();
            });

        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    public class UIHideLockCmd : UIData
    {
        #region Private Properties
        private int _target;
        private UnityAction _onAnimationComplete;
        private string _key;
        #endregion

        #region Constructor
        public UIHideLockCmd(int targetSlot, UnityAction onAnimationComplete, string key = "")
        {
            _target = targetSlot;
            _onAnimationComplete = onAnimationComplete;
            _key = key;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            UIManager.Instance.HideLockSlotBySlotID(_target, delegate
            {
                if (_onAnimationComplete != null)
                {
                    _onAnimationComplete.Invoke();
                }
                OnActiveComplete();
            });

        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    public class UIShowDarkMatterCmd : UIData
    {
        #region Private Properties
        private int _target;
        private int _duration;
        private PlayerIndex _owner;
        private UnityAction _onAnimationComplete;
        private string _key;
        #endregion

        #region Constructor
        public UIShowDarkMatterCmd(int targetSlot, PlayerIndex owner, UnityAction onAnimationComplete, string key = "")
        {
            _target = targetSlot;
            _owner = owner;
            _onAnimationComplete = onAnimationComplete;
            _key = key;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            UIManager.Instance.ShowDarkMatterBySlotID(_target, _owner, delegate
            {
                if (_onAnimationComplete != null)
                {
                    _onAnimationComplete.Invoke();
                }
                OnActiveComplete();
            });

        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    public class UIHideDarkMatterCmd : UIData
    {
        #region Private Properties
        private int _target;
        private PlayerIndex _owner;
        private UnityAction _onAnimationComplete;
        private string _key;
        #endregion

        #region Constructor
        public UIHideDarkMatterCmd(int targetSlot, PlayerIndex owner, UnityAction onAnimationComplete, string key = "")
        {
            _target = targetSlot;
            _owner = owner;
            _onAnimationComplete = onAnimationComplete;
            _key = key;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            UIManager.Instance.HideDarkMatterBySlotID(_target, _owner, delegate
            {
                if (_onAnimationComplete != null)
                {
                    _onAnimationComplete.Invoke();
                }
                OnActiveComplete();
            });

        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    public class UIShowLinkCmd : UIData
    {
        #region Private Properties
        private int _target;
        private List<CardDirectionType> _dirList;
        private UnityAction _onAnimationComplete;
        private string _key;
        #endregion

        #region Constructor
        public UIShowLinkCmd(int targetSlot, List<CardDirectionType> dirList, UnityAction onAnimationComplete, string key = "")
        {
            _target = targetSlot;
            _dirList = dirList;
            _onAnimationComplete = onAnimationComplete;
            _key = key;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            UIManager.Instance.ShowLinkOnSlot(_target, _dirList, delegate
            {
                if (_onAnimationComplete != null)
                {
                    _onAnimationComplete.Invoke();
                }
                OnActiveComplete();
            });

        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    public class UISetPlayerAPCmd : UIData
    {
        #region Private Properties
        private PlayerIndex _playerIndex;
        private int _currentAP;
        private UnityAction _onAnimationComplete;
        private string _key;
        #endregion

        #region Constructor
        public UISetPlayerAPCmd(PlayerIndex playerIndex, int currentAP, UnityAction onAnimationComplete, string key = "")
        {
            _playerIndex = playerIndex;
            _currentAP = currentAP;
            _onAnimationComplete = onAnimationComplete;
            _key = key;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            UIManager.Instance.SetPlayerAP(_playerIndex, _currentAP, delegate
            {
                if (_onAnimationComplete != null)
                {
                    _onAnimationComplete.Invoke();
                }
                OnActiveComplete();
            });

        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    #endregion

    #region Utility
    public class DestroyCardUI : UIData
    {
        #region Private Properties
        private CardUI _card;
        #endregion

        #region Constructor
        public DestroyCardUI(CardUI card)
        {
            _card = card;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            if (_card != null)
            {
                Object.Destroy(_card.gameObject);
                _card = null;
            }

            OnActiveComplete();
        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _uniqueKey
            );

        }
        #endregion
    }

    public class CallbackUI : UIData
    {
        #region Private Properties
        private UnityAction _callback;
        private string _key;
        #endregion

        #region Constructor
        public CallbackUI(UnityAction callback, string key = "")
        {
            _callback = callback;
            _key = key;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            if (_callback != null)
            {
                _callback.Invoke();
            }
            OnActiveComplete();
        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , this.GetType().ToString()
                , _key
            );

        }
        #endregion
    }

    public class SequenceQueueUI : UIData
    {
        public delegate Sequence CreateSequenceDelegate();

        #region Private Properties
        private CreateSequenceDelegate _createSequenceDelegate;
        private Sequence _sequence;
        private string _key;
        #endregion

        #region Constructor
        public SequenceQueueUI(CreateSequenceDelegate sequence, string key)
        {
            _createSequenceDelegate = sequence;
            _key = key;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            _sequence = _createSequenceDelegate.Invoke();
            if (_sequence != null)
            {
                _sequence.Play();
                _sequence.OnComplete(OnActiveComplete);
            }
        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , _key
                , _uniqueKey
            );

        }
        #endregion
    }

    public class SequenceAnimationQueueUI : UIData
    {
        #region Private Properties
        private Sequence _sequence;
        private string _key;
        #endregion

        #region Constructor
        public SequenceAnimationQueueUI(Sequence sequence, string key)
        {
            _sequence = sequence;
            _key = key;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            if (_sequence != null)
            {
                _sequence.Play();
                _sequence.OnComplete(OnActiveComplete);
            }
        }

        public override string GetDebugText()
        {
            return string.Format(
                    "{0}({1})"
                , _key
                , _uniqueKey
            );

        }
        #endregion
    }

    public class SFXQueueUI : UIData
    {
        #region Private Properties
        private SoundManager.SFX _sfx;
        private bool _isWaitOnComplete;
        #endregion

        #region Constructor
        public SFXQueueUI(SoundManager.SFX sfx, bool isWaitOnComplete)
        {
            _sfx = sfx;
            _isWaitOnComplete = isWaitOnComplete;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;

            if (_isWaitOnComplete)
            {
                SoundManager.PlaySFX(_sfx, false, OnActiveComplete);
            }
            else
            {
                SoundManager.PlaySFX(_sfx);
                OnActiveComplete();
            }
        }

        public override string GetDebugText()
        {
            return string.Format(
                  "{0}({1})"
                , this.GetType().ToString()
                , _uniqueKey
            );
        }
        #endregion
    }

    public class WaitUI : UIData
    {
        #region Private Properties
        private float _waitTime = 0.0f;
        private WaitObject _waitObj;
        #endregion

        #region Constructors
        public WaitUI(float waitTime)
        {
            _waitTime = waitTime;
        }
        #endregion

        #region Methods
        public override void Active(UnityAction<int> onComplete)
        {
            _onComplete = onComplete;
            if (_waitTime > 0.0f)
            {
                UtilityItem item = UtilityPool.Instance.CreateItem(UtilityPool.UtilityType.WaitObject);
                if (item != null)
                {
                    _waitObj = item.GetComponent<WaitObject>();
                    if (_waitObj != null)
                    {
                        _waitObj.name = "WaitObject_" + _uniqueKey.ToString();
                        _waitObj.StartTimer(_waitTime, this.OnActiveComplete);
                        return;
                    }
                }
            }

            OnActiveComplete();
        }

        public override string GetDebugText()
        {
            if (_waitObj != null && _waitObj.IsStart)
            {
                return string.Format(
                      "{0}({1}, {2})"
                    , this.GetType().ToString()
                    , _uniqueKey
                    , _waitObj.Timer.ToString("0.00")
                );
            }
            else
            {
                return string.Format(
                      "{0}({1})"
                    , this.GetType().ToString()
                    , _uniqueKey
                );
            }
        }
        #endregion
    }
    #endregion
}
