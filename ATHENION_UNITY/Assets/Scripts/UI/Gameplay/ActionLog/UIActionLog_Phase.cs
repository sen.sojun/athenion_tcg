﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIActionLog_Phase : ActionLogItem
{
    #region Public Properties
    public TextMeshProUGUI Message;
    #endregion

    #region Method
    public void SetData(string message)
    {
        Message.text = message;
    }
    #endregion
}
