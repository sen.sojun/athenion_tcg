﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIActionLog_Summary : ActionLogItem
{
    #region Public Properties
    [Header("Local")]
    public TextMeshProUGUI Local_Soul;
    public TextMeshProUGUI Local_Life;

    [Header ("Enemy")]
    public TextMeshProUGUI Enemy_Soul;
    public TextMeshProUGUI Enemy_Life;
    #endregion

    #region Private Properties
    private int _turn;
    #endregion

    #region Methods
    public void SetData(BattlePlayerData battlePlayerData1, BattlePlayerData battlePlayerData2, int turn, int SoulSum1, int SoulSum2)
    {
        _turn = turn;

        if (GameManager.Instance.IsLocalPlayer(battlePlayerData1.PlayerIndex))
        {
            Local_Soul.text = SoulSum1.ToString();
            Local_Life.text = battlePlayerData1.CurrentHP.ToString();
            Enemy_Soul.text = SoulSum2.ToString();
            Enemy_Life.text = battlePlayerData2.CurrentHP.ToString();
        }
        else
        {
            Local_Soul.text = SoulSum2.ToString();
            Local_Life.text = battlePlayerData2.CurrentHP.ToString();
            Enemy_Soul.text = SoulSum1.ToString();
            Enemy_Life.text = battlePlayerData1.CurrentHP.ToString();
        }
    }
    #endregion
}
