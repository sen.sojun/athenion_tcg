﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;
using TMPro;

namespace Karamucho.UI 
{
    public class UIActionLog_Action : ActionLogItem
    {
        public enum ActionIcon
        {
            Attack,
            Effect,
            Buff,
            Debuff
        }

        #region Public Properties
        [Header("Player Color")]
        public Color ColorLocalPlayer;
        public Color ColorEnemyPlayer;

        [Header("OneByOne")]
        public Transform GroupOneByOne;
        public Image Bg_left;
        public Image Bg_right;
        public Image Arrow;
        public Image ArtLeft;
        public Image ArtRight;
        public Image Icon;
        public TextMeshProUGUI TextLog;

        [Header("Single")]
        public Transform GroupSingle;
        public Image Bg_main;
        public Image Artwork;
        public TextMeshProUGUI TextLogSingle;

        [Header("Icon")]
        public Sprite Icon_Attack;
        public Sprite Icon_Effect;
        public Sprite Icon_Buff;
        public Sprite Icon_Debuff;

        [Header("BTN")]
        [SerializeField] private UISelectableExtension BTN_Primary;
        [SerializeField] private UISelectableExtension BTN_Secondary;
        #endregion

        #region Private Properties
        private static readonly string _cardBannerRootPath = "Images/Cards/ArtBannerImages/ART_BANNER_";
        private CardUIData _cardDataRight;
        private CardUIData _cardDataLeft;

        #endregion

        #region Method
        public void SetData(MinionData minion, string message)
        {
            if (minion != null)
            {
                InitBTN();

                // Collect Data
                _cardDataRight = new CardUIData(minion);

                // have 1 minion data.
                GroupOneByOne.gameObject.SetActive(false);
                GroupSingle.gameObject.SetActive(true);

                //Set Color             
                if (GameManager.Instance.IsLocalPlayer(minion.Owner))
                {
                    Bg_main.color = ColorLocalPlayer;
                }
                else
                {
                    Bg_main.color = ColorEnemyPlayer;
                }

                // Set text
                TextLogSingle.text = message;

                // Set Single Row
                Material mat = new Material(Artwork.material);
                Texture tex = ResourceManager.Load<Texture>(_cardBannerRootPath + minion.ImageKey);
                mat.SetTexture("_NewTex_1", tex);
                Artwork.material = mat;
            }
        }

        public void SetData(MinionData minion1, MinionData minion2, string message, ActionIcon actionIcon)
        {
            if (minion1 != null && minion2 != null)
            {
                InitBTN();

                // Collect Data
                _cardDataLeft = new CardUIData(minion1);
                _cardDataRight = new CardUIData(minion2);

                // have 2 minion data.
                GroupOneByOne.gameObject.SetActive(true);
                GroupSingle.gameObject.SetActive(false);

                // Set Color
                SetColor(minion1, minion2);
                SetIcon(actionIcon);

                //Set text
                TextLog.text = message;

                // Set Image Left
                {
                    Material mat = new Material(ArtLeft.material);
                    Texture tex = ResourceManager.Load<Texture>(_cardBannerRootPath + minion1.ImageKey);
                    mat.SetTexture("_NewTex_1", tex);
                    ArtLeft.material = mat;
                }

                // Set Image Right
                {
                    Material mat = new Material(ArtRight.material);
                    Texture tex = ResourceManager.Load<Texture>(_cardBannerRootPath + minion2.ImageKey);
                    mat.SetTexture("_NewTex_1", tex);
                    ArtRight.material = mat;
                }
            }
            else 
            {
                if (minion1 != null)        SetData(minion1, message);
                else if (minion2 != null)   SetData(minion2, message);
            }
        }

        public void SetData(PlayerIndex playerIndex, string message)
        {
            InitBTN();
            // don't have minion.
            GroupOneByOne.gameObject.SetActive(false);
            GroupSingle.gameObject.SetActive(true);

            //Set Color
            if (GameManager.Instance.IsLocalPlayer(playerIndex))
            {
                Bg_main.color = ColorLocalPlayer;
            }
            else
            {
                Bg_main.color = ColorEnemyPlayer;
            }
            Artwork.gameObject.SetActive(false);

            // Set text
            TextLogSingle.text = message;
        }

        private void SetIcon(ActionIcon actionIcon)
        {
            switch (actionIcon)
            {
                case ActionIcon.Attack:
                {
                    Icon.sprite = Icon_Attack;
                }
                break;
                case ActionIcon.Effect:
                {
                    Icon.sprite = Icon_Effect;
                }
                break;
                case ActionIcon.Buff:
                {
                    Icon.sprite = Icon_Buff;
                }
                break;
                case ActionIcon.Debuff:
                {
                    Icon.sprite = Icon_Debuff;
                }
                break;
                default:
                {
                    Icon.sprite = Icon_Effect;
                }
                break;
            }
        }

        private void SetColor(MinionData Minion1, MinionData Minion2)
        {
            // Left part
            if (GameManager.Instance.IsLocalPlayer(Minion1.Owner))
            {
                Bg_left.color = ColorLocalPlayer;
                Arrow.color = ColorLocalPlayer;
            }
            else
            {
                Bg_left.color = ColorEnemyPlayer;
                Arrow.color = ColorEnemyPlayer;
            }

            // Right part
            if (GameManager.Instance.IsLocalPlayer(Minion2.Owner))
            {
                Bg_right.color = ColorLocalPlayer;
            }
            else
            {
                Bg_right.color = ColorEnemyPlayer;
            }
        }
        #endregion

        #region Events
        private void InitBTN()
        {
            BTN_Primary.OnButtonPress.RemoveAllListeners();
            BTN_Primary.OnButtonPress.AddListener(delegate
            {
                EventOnPress(_cardDataRight);
            });

            BTN_Primary.OnButtonRelease.RemoveAllListeners();
            BTN_Primary.OnButtonRelease.AddListener(delegate
            {
                EventOnRelease();
            });

            BTN_Secondary.OnButtonPress.RemoveAllListeners();
            BTN_Secondary.OnButtonPress.AddListener(delegate
            {
                EventOnPress(_cardDataLeft);
            });

            BTN_Secondary.OnButtonRelease.RemoveAllListeners();
            BTN_Secondary.OnButtonRelease.AddListener(delegate
            {
                EventOnRelease();
            });
        }

        public void EventOnPress(CardUIData data)
        {
            // Show Detail Card
            if (data == null) return;

            UIManager.Instance.CardUIDetailHover.SetData(data);
            UIManager.Instance.ShowDetailCardUI(true);

        }

        public void EventOnHold()
        {
            // Nothing
        }

        public void EventOnRelease()
        {
            // Hide Detail Card
            UIManager.Instance.ShowDetailCardUI(false);
        }
        #endregion
    }
}
