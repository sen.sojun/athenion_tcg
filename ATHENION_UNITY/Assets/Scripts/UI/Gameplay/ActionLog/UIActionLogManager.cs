﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Karamucho.UI;

public class UIActionLogManager : MonoBehaviour
{
    #region Public Properties
    public static readonly int ShowLogCount = 20;

    [Header("BTN")]
    public Button BTN_CloseLog;

    [Header("Reference")]
    public Transform Panel;
    public ScrollRect ScrollRect;
    public RectTransform Viewport;
    public RectTransform Content;
    #endregion

    #region Private Properties
    private int _logLimit = ShowLogCount;

    private int _minIndex = int.MaxValue;
    private int _maxIndex = int.MinValue;

    private List<ActionLog> _logDataist = new List<ActionLog>();
    private List<ActionLogItem> _logList = new List<ActionLogItem>();
    private bool _isEnableOptimize = true;
    private bool _isStartCreateLog = false;
    private Coroutine _createLogTask = null;
    #endregion

    #region UI
    private void InitBTN()
    {
        BTN_CloseLog.onClick.RemoveAllListeners();
        BTN_CloseLog.onClick.AddListener(Hide);
    }

    public void ShowUI()
    {
        InitBTN();

        _minIndex = int.MaxValue;
        _maxIndex = int.MinValue;
        InitLog(ShowLogCount); // show 10 logs 

        GameHelper.UITransition_FadeMoveIn(this.gameObject, Panel.gameObject, -1.0f, 0.0f);
        ScrollRect.ScrollToBottom(); // scroll to bottom.
        ScrollRect.onValueChanged.AddListener(OnScrolling);
    }

    public void Hide()
    {
        if (_createLogTask != null)
        {
            StopCoroutine(_createLogTask);
            _createLogTask = null;
        }

        ScrollRect.onValueChanged.RemoveListener(OnScrolling);
        ClearLog();

        GameHelper.UITransition_FadeMoveOut(this.gameObject, Panel.gameObject, -1.0f, 0.0f);
    }
    #endregion

    #region Methods
    private void InitLog(int count)
    {
        ClearLog();

        if (GameManager.Instance.BattleLog != null)
        {
            int lastIndex = GameManager.Instance.BattleLog.Count - 1;
            int minIndex = lastIndex - count;
            int maxIndex = lastIndex;
            minIndex = Mathf.Max(0, minIndex);

            for (int index = minIndex; index <= maxIndex; ++index)
            {
                CreateLog(index);
            }
        }

        UpdateLayout();
    }

    private void CreateLog(int index)
    {
        if (index >= 0 && index < GameManager.Instance.BattleLog.Count)
        {
            ActionLog log = GameManager.Instance.BattleLog[index];
            log.CreateUILog();

            ActionLogItem logUI = _logList[_logList.Count - 1];

            if (log.Index < _minIndex)
            {
                _logDataist.Insert(0, log); // Add Front
                logUI.transform.SetAsFirstSibling();
            }
            else
            {
                _logDataist.Add(log); // Add Last
                logUI.transform.SetAsLastSibling();
            }

            _minIndex = Mathf.Min(log.Index, _minIndex);
            _maxIndex = Mathf.Max(log.Index, _maxIndex);
        }
    }

    private void CreatePreviousLog()
    {
        if (!_isStartCreateLog)
        {
            _isEnableOptimize = false;
            _isStartCreateLog = true;

            _createLogTask = StartCoroutine(OnStartCreatePreviousLog());
        }
    }

    private IEnumerator OnStartCreatePreviousLog()
    {
        if (_minIndex > 0)
        {
            int maxIndex = _minIndex - 1;
            int minIndex = maxIndex - ShowLogCount;
            minIndex = Mathf.Max(0, minIndex); // clamp minimum to 0.

            if (maxIndex >= 0)
            {
                ScrollRect.velocity = Vector2.zero;
                ScrollRect.ScrollToTop();
                ScrollRect.vertical = false;

                //float height_prev = Content.rect.height;

                RectTransform cellRect = Content.GetChild(0).GetComponent<RectTransform>();

                for (int index = maxIndex; index >= minIndex; --index)
                {
                    CreateLog(index);
                    yield return new WaitForEndOfFrame();
                }

                UpdateLayout();

                //float height_current = Content.rect.height;
                //VerticalLayoutGroup layout = Content.GetComponent<VerticalLayoutGroup>();
                //float offset_v = layout.padding.bottom + layout.padding.top;

                //float ratio = (height_current - height_prev) / height_current;
                //float viewport_ratio = ((Viewport.rect.height * 0.5f) - offset_v) / height_current;
                //ratio = Mathf.Abs(ratio + viewport_ratio);
                //ratio = Mathf.Clamp01(1.0f - ratio);

                float pos_y = Mathf.Abs(cellRect.transform.localPosition.y);
                float cell_height = cellRect.rect.height;
                float viewport_height = Viewport.rect.height;
                float max_offset_y = Content.rect.height - viewport_height;
                float center_offset = -cell_height * 0.5f;

                float container_y = pos_y + center_offset;
                container_y = Mathf.Clamp(container_y, 0.0f, max_offset_y);

                Content.transform.localPosition = new Vector3(
                      Content.transform.localPosition.x
                    , container_y
                    , Content.transform.localPosition.z
                );

                ScrollRect.velocity = Vector2.zero;
                ScrollRect.vertical = true;
            }
        }

        _createLogTask = null;
        _isStartCreateLog = false;
        _isEnableOptimize = true;
        yield break;
    }

    private void ClearLog()
    {
        if (_logList != null)
        {
            while (_logList.Count > 0)
            {
                ActionLogItem log = _logList[0];
                _logList.RemoveAt(0);

                ActionLogPool.Instance.ReleaseItem(log);
            }

            _logList.Clear();
        }

        if (_logDataist != null)
        {
            _logDataist.Clear();
        }
    }

    protected void SetEnableAllLayout(bool isEnable)
    {
        if (isEnable)
        {
            foreach (Transform child in Content.transform)
            {
                child.gameObject.SetActive(true);
            }
        }

        SetEnableLayoutComponent(isEnable);
    }

    protected void SetEnableLayoutComponent(bool isEnable)
    {
        // Layout Component
        if (Content.GetComponent<ContentSizeFitter>() != null)
        {
            Content.GetComponent<ContentSizeFitter>().enabled = isEnable;
        }

        // All Layout Group
        LayoutGroup[] layoutGroups = Content.GetComponents<LayoutGroup>();
        if (layoutGroups != null && layoutGroups.Length > 0)
        {
            foreach (LayoutGroup layoutGroup in layoutGroups)
            {
                layoutGroup.enabled = isEnable;
            }
        }
    }

    private void UpdateLayout()
    {
        SetEnableAllLayout(true);

        LayoutRebuilder.ForceRebuildLayoutImmediate(Content);
    }
    #endregion

    #region Create Log Action
    public void CreateLog_Phase(string messsage)
    {
        ActionLogItem log = ActionLogPool.Instance.CreateItem(ActionLogPool.LogType.Phase, Content.transform);
        log.transform.SetAsLastSibling();
        _logList.Add(log);

        log.GetComponent<UIActionLog_Phase>().SetData(messsage);
    }

    public void CreateLog_Summary(BattlePlayerData battlePlayerData1, BattlePlayerData battlePlayerData2, int turn, int SoulSum1, int SoulSum2)
    {
        ActionLogItem log = ActionLogPool.Instance.CreateItem(ActionLogPool.LogType.Summary, Content.transform);
        log.transform.SetAsLastSibling();
        _logList.Add(log);

        log.GetComponent<UIActionLog_Summary>().SetData(battlePlayerData1, battlePlayerData2, turn, SoulSum1, SoulSum2);
    }

    public void CreateLog_Action( MinionData minion, string message)
    {
        UIActionLog_Action logAction = _createLog_Action();
        if (logAction != null)
        {
            logAction.SetData(minion, message);
        }
    }

    public void CreateLog_Action(MinionData minion1, MinionData minion2, string message, UIActionLog_Action.ActionIcon actionIcon)
    {
        UIActionLog_Action logAction = _createLog_Action();
        if (logAction != null)
        {
            logAction.SetData(minion1, minion2, message, actionIcon);
        }
    }

    public void CreateLog_Action(PlayerIndex playerIndex, string message)
    {
        UIActionLog_Action logAction = _createLog_Action();
        if (logAction != null)
        {
            logAction.SetData(playerIndex, message);
        }
    }

    private UIActionLog_Action _createLog_Action()
    {
        ActionLogItem log = ActionLogPool.Instance.CreateItem(ActionLogPool.LogType.Action, Content.transform);
        if (log != null)
        {
            log.transform.SetAsLastSibling();
            _logList.Add(log);
            return log.GetComponent<UIActionLog_Action>();
        }
        else
        {
            return null;
        }
    }
    #endregion

    #region Event
    protected virtual void OnScrolling(Vector2 value)
    {
        if (_isStartCreateLog)
        {
            ScrollRect.verticalNormalizedPosition = 1.0f;
        }

        if (ScrollRect.verticalNormalizedPosition >= 1.0f)
        {
            // Scroll to top
            CreatePreviousLog();
        }

        if (_isEnableOptimize)
        {
            if (Content != null && Viewport != null && Content.transform.childCount > 0)
            {
                SetEnableLayoutComponent(false);

                // Show Only Visible Child.
                RectTransform child_rect = null;
                foreach (Transform child in Content.transform)
                {
                    child_rect = child.GetComponent<RectTransform>();
                    if (child_rect != null)
                    {
                        if (GameHelper.IsRectTouchRect(child_rect, Viewport))
                        {
                            child.gameObject.SetActive(true);
                        }
                        else
                        {
                            child.gameObject.SetActive(false);
                        }
                    }
                }
            }
        }
    }

    public void OnLogUpdated()
    {
        if (gameObject.activeInHierarchy)
        {
            if (GameManager.Instance.BattleLog != null)
            {
                int lastIndex = GameManager.Instance.BattleLog.Count - 1;
                
                CreateLog(lastIndex);

                UpdateLayout();
            }
        }
    }
    #endregion
}

