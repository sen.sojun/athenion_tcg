﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LayoutForceUpdate : MonoBehaviour
{
    private void OnEnable()
    {
        GameHelper.Layout_ForceUpdate(GetComponent<RectTransform>());
    }
}
