﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class UIHeroCutIn : MonoBehaviour {

    #region Public Properties
    public Animator AnimatorCutIn;
    #endregion

    #region Private Properties
    private UnityAction _onComplete;
    #endregion

    public void PlayCutIn(UnityAction onComplete)
    {
        PlayAnimator();
        SetOnComplete(onComplete);
    }

    private void PlayAnimator()
    {
        AnimatorCutIn.SetTrigger("Activate");
    }

    private void SetOnComplete(UnityAction onComplete)
    {
        if (onComplete != null)
        {
            _onComplete = onComplete;
        }
    }

    public void OnAnimationComplete()
    {
        if (_onComplete != null)
        {
            _onComplete.Invoke();
        }
    }
}
