﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using DG.Tweening;
using TMPro;
using UnityEngine.UI;

namespace Karamucho.UI
{
    public interface IUIHeroSkill
    {
        void SetData(HeroData heroData);
        void SetGauge(int current, int max);
    }

    public class UIHeroSkillButton : MonoBehaviour, IUIHeroSkill, IPointerDownHandler, IPointerClickHandler, IPointerUpHandler
    {
        #region Public Properties
        public bool IsLocal;
        public Animator Animator;
        public GameObject GlowOnDown;
        public TextMeshProUGUI GaugeChargeText;
        public Image GaugeImage;

        [Header("Skill Hero")]
        public GameObject SkillInfoBox;
        public TextMeshProUGUI SkillName;
        public TextMeshProUGUI SkillDescription;
        public Image SkillImage;

        #endregion

        #region Private Properties
        private bool _isUsed = false;
        private bool _isDown = false;
        private bool _isPreview = false;
        private float _maxDownTime = 0.1f;
        private float _timer = 0;
        #endregion

        #region Method
        private void Update()
        {
            // Click Delay
            if (_isDown)
            {
                if (_timer < _maxDownTime)
                {
                    _timer += Time.deltaTime;
                }
                else
                {
                    if (!_isPreview)
                    {
                        ShowInfo(true);
                        _isPreview = true;
                    }

                }
            }
        }
        #endregion

        #region Events
        public virtual void SetData(HeroData heroData)
        {
            // Description, skillName, skillImage
            //SkillImage.sprite = ResourceManager.Load<Sprite>(heroData.SkillCard.ImageKey);
            //SkillName.text = heroData.SkillCard.Name;
            //SkillDescription.text = heroData.SkillCard.Description;
        }

        public virtual void SetGauge(int current, int max)
        {
            GaugeChargeText.text = current + "/" + max;
            float endValue = (float)current / (float)max;
            //GaugeImage.fillAmount = (float)current / (float)max;
            GaugeImage.DOFillAmount(endValue, 0.3f);
        }

        private void SkillSetIsReady(bool isReady)
        {
            if (GameManager.Instance.IsCanUseHeroSkill)
            {
                Animator.SetBool("IsReady", isReady);
            }
        }

        private void SkillSetIsOpen(bool isOpen)
        {
            if (GameManager.Instance.IsCanUseHeroSkill)
            {
                Animator.SetBool("IsOpen", isOpen);
            }
        }

        private void SkillSetIsCasting(bool isCast)
        {
            if (GameManager.Instance.IsCanUseHeroSkill)
            {
                Animator.SetBool("IsCasting", isCast);
            }
        }

        public void SkillToOpen(bool isReady)
        {
            if (GameManager.Instance.IsCanUseHeroSkill)
            {
                SkillSetIsOpen(true);
                SkillSetIsCasting(false);

                SetHeroSkillEnable(true);
                SkillSetIsReady(isReady);
            }
        }

        public void SkillToClose()
        {
            if (GameManager.Instance.IsCanUseHeroSkill)
            {
                SkillSetIsOpen(false);
                SkillSetIsCasting(false);

                SetHeroSkillEnable(false);
                SkillSetIsReady(false);
            }
        }

        public void SkillActivateComplete()
        {
            if (GameManager.Instance.IsCanUseHeroSkill)
            {
                SetHeroSkillEnable(false);
                //SkillSetIsOpen(false);
                SkillSetIsCasting(false);
                SkillSetIsReady(false);
            }
        }

        public void SkillAnimationActivate()
        {
            if (GameManager.Instance.IsCanUseHeroSkill)
            {
                SetHeroSkillEnable(false);
                SkillSetIsOpen(true);
                SkillSetIsCasting(true);
                SkillSetIsReady(false);
            }
        }

        public void SetHeroSkillEnable(bool isEnable)
        {
            if (isEnable)
            {
                _isUsed = false;
                SkillSetIsReady(true);
            }
            else
            {
                _isUsed = true;
                SkillSetIsReady(false);
            }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (_isUsed || _isPreview || !GameManager.Instance.IsCanUseHeroSkill)
            {
                return;
            }

            if (IsLocal && !UIManager.Instance.IsSelectionSlotShowing)
            {
                _isUsed = true;

            }
        }

        public void ShowInfo(bool isShow)
        {
            if (GameManager.Instance.IsCanUseHeroSkill)
            {
                SkillInfoBox.SetActive(isShow);
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (GameManager.Instance.IsCanUseHeroSkill)
            {
                _isDown = true;
                _isPreview = false;
                _timer = 0;

                GlowOnDown.SetActive(true);
            }
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (GameManager.Instance.IsCanUseHeroSkill)
            {
                _isDown = false;

                GlowOnDown.SetActive(false);
                ShowInfo(false);
            }
        }
        #endregion
    }
}
