﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Karamucho.UI;

public class UIHeroDead : MonoBehaviour {

    public Animator Animator;
    public float ShakeValue;
    public float ShakeDuration;  

    private UnityAction _onAnimationComplete;

    #region Methods

    public void Show(UnityAction onAnimationComplete)
    {
        SetAnimationComplete(onAnimationComplete);
        Animator.SetBool("IsDead", true);
    }

    public void SetAnimationComplete(UnityAction onAnimationComplete)
    {
        _onAnimationComplete = onAnimationComplete;
    }

    public void OnAnimationComplete()
    {
        //Debug.Log("End Animation");
        if (_onAnimationComplete != null)
        {
            _onAnimationComplete.Invoke();
        }
    }

    public void DeadCameraShake()
    {
        UIManager.Instance.CameraScreenShake(ShakeValue, ShakeDuration);
    }

    public void TimeSpeedChange(float speed)
    {
        Time.timeScale = speed;
    }

    public void ShakeAllToken()
    {
        BoardManager.Instance.ShakeAllTokenList();
    }
    #endregion
}
