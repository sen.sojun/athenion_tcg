﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Playables;

public class UIEndScene : MonoBehaviour {

    #region Public Properties
    public GameObject VictoryObject;
    public GameObject DefeatObject;
    public SpriteRenderer PlayerImageVictory;
    public SpriteRenderer PlayerImageDefeat;
    #endregion

    #region Private Properties
    private PlayableDirector _playableDirector;
    private UnityAction _onComplete;
    #endregion

    #region Methods

    public void SetVictory()
    {
        VictoryObject.SetActive(true);
        DefeatObject.SetActive(false);
    }

    public void SetDefeat()
    {
        DefeatObject.SetActive(true);
        VictoryObject.SetActive(false);
    }
    #endregion
}
