﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Karamucho.UI
{
    public class HeroAttackData
    {
        public string EffectKey { get; private set; }

        public string StartKey { get; private set; }
        public string ProjectileKey_1 { get; private set; }
        public string ProjectileKey_2 { get; private set; }
        public string ProjectileKey_3 { get; private set; }
        public string EndKey { get; private set; }

        private HeroAttackData(string key, bool isLocal)
        {
            EffectKey = key;

            StartKey = EffectKey + "_HERO_START";
            ProjectileKey_1 = EffectKey + "_HERO_HIT_1";
            ProjectileKey_2 = EffectKey + "_HERO_HIT_2";
            ProjectileKey_3 = EffectKey + "_HERO_HIT_3";
            EndKey = EffectKey + "_HERO_END";

            if (EffectKey == "DEFAULT")
            {
                LoadParticleDefault(isLocal);
            }
            else if (EffectKey == "AWAKEN1")
            {
                LoadParticleAwakenCustom(isLocal);
            }

        }

        private void LoadParticleDefault(bool isLocal)
        {
            if (isLocal)
            {
                // Bottom player
                StartKey = "DEFAULT_BLUE_HERO_START";
                ProjectileKey_1 = "DEFAULT_BLUE_HERO_HIT_1";
                ProjectileKey_2 = "DEFAULT_BLUE_HERO_HIT_2";
                ProjectileKey_3 = "DEFAULT_BLUE_HERO_HIT_3";
                EndKey = "DEFAULT_BLUE_HERO_END";
            }
            else
            {
                // Top player
                StartKey = "DEFAULT_RED_HERO_START";
                ProjectileKey_1 = "DEFAULT_RED_HERO_HIT_1";
                ProjectileKey_2 = "DEFAULT_RED_HERO_HIT_2";
                ProjectileKey_3 = "DEFAULT_RED_HERO_HIT_3";
                EndKey = "DEFAULT_RED_HERO_END";
            }
        }

        private void LoadParticleAwakenCustom(bool isLocal)
        {
            if (isLocal)
            {
                // Bottom player
                StartKey = "AWAKEN1_BLUE_HERO_START";
                ProjectileKey_1 = "AWAKEN1_BLUE_HERO_HIT_1";
                ProjectileKey_2 = "AWAKEN1_BLUE_HERO_HIT_2";
                ProjectileKey_3 = "AWAKEN1_BLUE_HERO_HIT_3";
                EndKey = "AWAKEN1_BLUE_HERO_END";
            }
            else
            {
                // Top player
                StartKey = "AWAKEN1_RED_HERO_START";
                ProjectileKey_1 = "AWAKEN1_RED_HERO_HIT_1";
                ProjectileKey_2 = "AWAKEN1_RED_HERO_HIT_2";
                ProjectileKey_3 = "AWAKEN1_RED_HERO_HIT_3";
                EndKey = "AWAKEN1_RED_HERO_END";
            }
        }

        public static HeroAttackData CreateHeroAttackData(string key, bool isLocal)
        {
            HeroAttackData result = new HeroAttackData(key, isLocal);
            return result;
        }
    }

    public class HeroAttackAnimation : MonoBehaviour
    {
        #region Public Properties
        public static readonly int LowPower = 4;
        public static readonly int MedPower = 8;

        #endregion

        #region Private Properties
        private UnityAction _onImpact;
        private UnityAction _onComplete;

        private HeroAttackData _data;

        private SoundManager.SFX _startSound_1 = SoundManager.SFX.Deal_Damage_Start_Magic;
        private SoundManager.SFX _impactSound = SoundManager.SFX.Minion_Be_Attacked;
        private SoundManager.SFX _hitSound_1 = SoundManager.SFX.Deal_Damage_Hit_Magic;
        private SoundManager.SFX _hitSound_2 = SoundManager.SFX.Deal_Damage_Hit_Magic;
        private SoundManager.SFX _hitSound_3 = SoundManager.SFX.Deal_Damage_Hit_Magic;

        private int _shakePower = 1;

        #endregion


        #region Medthods
        public void Setup(HeroAttackData data)
        {
            // Setup Sound and Particle
            _data = data;
        }

        private void SetOnComplete(UnityAction onComplete)
        {
            _onComplete = onComplete;
        }

        private void SetOnImpact(UnityAction onImpact)
        {
            _onImpact = onImpact;
        }

        public void OnImpactEvent()
        {
            if (_onImpact != null)
            {
                _onImpact.Invoke();
            }
        }

        public void OnCompleteEvent()
        {
            if (_onComplete != null)
            {
                _onComplete.Invoke();
            }
        }

        public void PlaySound_SoundATK_Start()
        {
            SoundManager.PlaySFX(_startSound_1);
        }

        public void PlaySound_SoundATK_Impact()
        {
            SoundManager.PlaySFX(_impactSound);
        }

        public void PlaySound_SoundATK_Hit1()
        {
            SoundManager.PlaySFX(_hitSound_1);
        }

        public void PlaySound_SoundATK_Hit2()
        {
            SoundManager.PlaySFX(_hitSound_2);
        }

        public void PlaySound_SoundATK_Hit3()
        {
            SoundManager.PlaySFX(_hitSound_3);
        }

        public void ScreenShake()
        {
            UIManager.Instance.CameraScreenShake(_shakePower * 2);
        }

        public HeroAttackData GetData()
        {
            return _data;
        }

        #endregion
    }
}
