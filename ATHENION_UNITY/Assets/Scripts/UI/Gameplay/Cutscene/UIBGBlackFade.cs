﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class UIBGBlackFade : MonoBehaviour {

    public void ShowBlack()
    {
        gameObject.SetActive(true);
        GetComponent<SpriteRenderer>().DOFade(0.85f, 0.5f);
    }

    public void HideBlack()
    {
        GetComponent<SpriteRenderer>().DOFade(0.0f, 0.5f).OnComplete(delegate { gameObject.SetActive(false); });
    }
}
