﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Events;
using TMPro;

public class UIIntro : MonoBehaviour {

    #region Public Properties
    public GameObject TopCoin;
    public GameObject BotCoin;
    public GameObject TopTextFirst;
    public GameObject BotTextFirst;

    [Header("Player Info")]
    public TextMeshPro TopPlayerName;
    public TextMeshPro TopHeroName;
    public TextMeshPro BotPlayerName;
    public TextMeshPro BotHeroName;
    #endregion

    #region Private Properties
    private PlayableDirector _playableDirector;
    private UnityAction _onComplete;
    #endregion

    #region Methods
    public void SetOnComplete(UnityAction onComplete)
    {
        _onComplete = onComplete;

    }

    public void OnEnable()
    {
        _playableDirector = GetComponent<PlayableDirector>();
        _playableDirector.stopped += OnComplete;
    }

    public void OnDisable()
    {
        _playableDirector = GetComponent<PlayableDirector>();
        _playableDirector.stopped -= OnComplete;
    }

    private void OnComplete(PlayableDirector playableDirector)
    {
        if (playableDirector == GetComponent<PlayableDirector>())
        {
            gameObject.SetActive(false);
            if (_onComplete != null)
            {
                _onComplete.Invoke();
            }
        }
    }

    public void SetTopPlayerInfo(BattlePlayerData data)
    {
        SetTopPlayerName(data.PlayerInfo.DisplayName);
        SetTopHeroName(data.PlayerInfo.DisplayName + "Hero Plz Add Name");
        // SetImage
    }

    public void SetBotPlayerInfo(BattlePlayerData data)
    {
        SetBotPlayerName(data.PlayerInfo.DisplayName);
        SetBotHeroName(data.PlayerInfo.DisplayName + "Hero Plz Add Name");
        // SetImage
    }

    private void SetTopPlayerName(string message)
    {
        TopPlayerName.text = message;
    }

    private void SetBotPlayerName(string message)
    {
        BotPlayerName.text = message;
    }

    private void SetTopHeroName(string message)
    {
        TopHeroName.text = message;
    }

    private void SetBotHeroName(string message)
    {
        BotHeroName.text = message;
    }

    public void SetTopPlayFirst()
    {
        TopCoin.SetActive(true);
        TopTextFirst.SetActive(true);

        BotCoin.SetActive(false);
        BotTextFirst.SetActive(false);
    }

    public void SetBotPlayFirst()
    {
        BotCoin.SetActive(true);
        BotTextFirst.SetActive(true);

        TopCoin.SetActive(false);
        TopTextFirst.SetActive(false);
    }
    #endregion

}
