﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Events;
using Karamucho.UI;

public class BattleIntro : MonoBehaviour {

    #region Public Properties
    public Animator AnimController;
    public TextMeshProUGUI Message;

    [Header("Top")]
    public TextMeshProUGUI TopName;
    public TextMeshProUGUI TopHero;
    public Image TopImage;
    public Image TopAvatar;
    public Image TopRank;
    public Image TopTitle;
    public GameObject TopMask;

    [Header("Bot")]
    public TextMeshProUGUI BotName;
    public TextMeshProUGUI BotHero;
    public Image BotImage;
    public Image BotAvatar;
    public Image BotRank;
    public Image BotTitle;
    public GameObject BotMask;

    [Header("Sound")]
    public SoundManager.SFX Coin;
    public SoundManager.SFX Coin_PlayFirst;
    #endregion

    #region Private Properties
    private GameObject _topImage = null;
    private GameObject _bottomImage = null;
    private UnityAction _onComplete;
    #endregion

    #region Method

    public void SetOnComplete(UnityAction onComplete)
    {
        _onComplete = onComplete;

    }

    public void HideUI()
    {
        gameObject.SetActive(false);
        if (_onComplete != null)
        {
            _onComplete.Invoke();
        }
    }

    public void SoundPlay_Coin()
    {
        SoundManager.PlaySFX(Coin);
    }

    public void SoundPlay_First()
    {
        SoundManager.PlaySFX(Coin_PlayFirst);
    }

    public void ShowUI(bool isLocalPlayFirst)
    {
        gameObject.SetActive(true);
    }

    public void SetTopPlayFirst()
    {
        AnimController.SetBool("isLocalFirst", false);

        string text;
        LocalizationManager.Instance.GetText("BATTLE_PLAY_SECOND", out text);
        Message.text = text;
    }

    public void SetBotPlayFirst()
    {
        AnimController.SetBool("isLocalFirst", true);

        string text;
        LocalizationManager.Instance.GetText("BATTLE_PLAY_FIRST", out text);
        Message.text = text;
    }

    public void SetTopPlayerInfo(BattlePlayerData data)
    {
        SetTopPlayerName(data.PlayerInfo.DisplayName);

        // Set Hero
        SetTopHeroName(data.Hero.Name);
        SetTopHeroImage(data.Hero.ImageKey);
        SetTopAvatar(data.PlayerInfo.AvatarID);
        SetTopRank(data.PlayerInfo.GetPlayerCurrentRank());
        SetTopTitle(data.PlayerInfo.TitleID);
    }

    public void SetBotPlayerInfo(BattlePlayerData data)
    {
        SetBotPlayerName(data.PlayerInfo.DisplayName);

        // Set Hero
        SetBotHeroName(data.Hero.Name);
        SetBotHeroImage(data.Hero.ImageKey);
        SetBotAvatar(data.PlayerInfo.AvatarID);
        SetBotRank(data.PlayerInfo.GetPlayerCurrentRank());
        SetBotTitle(data.PlayerInfo.TitleID);
    }

    private void SetTopPlayerName(string message)
    {
        TopName.text = message;
    }

    private void SetBotPlayerName(string message)
    {
        BotName.text = message;
    }

    private void SetTopHeroName(string message)
    {
        TopHero.text = message;
    }

    private void SetBotHeroName(string message)
    {
        BotHero.text = message;
    }

    private void SetTopHeroImage(string imageKey)
    {
        if (_topImage != null)
        {
            Destroy(_topImage);
            _topImage = null;
        }

        CardResourceManager.Create(
              CardResourceManager.ImageType.Hero
            , imageKey
            , TopMask.transform
            , false
            , out _topImage
        );
    }

    private void SetBotHeroImage(string imageKey)
    {
        if (_bottomImage != null)
        {
            Destroy(_bottomImage);
            _bottomImage = null;
        }

        CardResourceManager.Create(
              CardResourceManager.ImageType.Hero
            , imageKey
            , BotMask.transform
            , false
            , out _bottomImage
        );
    }

    private void SetTopRank(int rankIndex)
    {
        string rankId = GameHelper.GetRankID(rankIndex);
        TopRank.sprite = SpriteResourceHelper.LoadSprite(rankId, SpriteResourceHelper.SpriteType.ICON_Rank);
    }

    private void SetBotRank(int rankIndex)
    {
        string rankId = GameHelper.GetRankID(rankIndex);
        BotRank.sprite = SpriteResourceHelper.LoadSprite(rankId, SpriteResourceHelper.SpriteType.ICON_Rank);
    }

    private void SetTopAvatar(string imageKey)
    {
        TopAvatar.sprite = SpriteResourceHelper.LoadSprite(imageKey, SpriteResourceHelper.SpriteType.Avatar);
    }

    private void SetBotAvatar(string imageKey)
    {
        BotAvatar.sprite = SpriteResourceHelper.LoadSprite(imageKey, SpriteResourceHelper.SpriteType.Avatar);
    }

    private void SetTopTitle(string imageKey)
    {
        TopTitle.sprite = SpriteResourceHelper.LoadSprite(imageKey, SpriteResourceHelper.SpriteType.Title);
    }

    private void SetBotTitle(string imageKey)
    {
        BotTitle.sprite = SpriteResourceHelper.LoadSprite(imageKey, SpriteResourceHelper.SpriteType.Title);
    }
    #endregion
}
