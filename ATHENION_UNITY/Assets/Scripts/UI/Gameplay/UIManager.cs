using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.Events;
using Karamucho.UI;
using TMPro;

public class UIManager : MonoSingleton<UIManager>
{
    #region Public Properties
    [Header("Camara")]
    public Camera MainCamera;

    public const float SLOT_WIDTH = 1.365f;

    [Header("Detail Hover Card")]
    public CardUI_DetailHover CardUIDetailHover;

    [Header("Controller")]
    [SerializeField] private LightingController LightingController;
    [SerializeField] private SceneAnimation SceneAnimation;
    [SerializeField] private PlayboardController PlayboardController;
    [SerializeField] private FloatingTextController FloatTextController;
    [SerializeField] private UIActionLogManager UIActionLogController;
    [SerializeField] private UIPauseMenuIngame UIPauseMenu;
    [SerializeField] private UITurnNotice UITurn;
    [SerializeField] private TutorialPopup UITutorialMap;
    [SerializeField] private UITutorialManager UITutorialManager;

    [Header("Intro & Outtro")]
    [SerializeField] private MatchIntroAnimation IntroAnimation;
    [SerializeField] private MatchOuttroAnimation OuttroAnimation;
    [SerializeField] private ProgressionController ProgressionController;

    [Header("UI Overlay")]
    [SerializeField] private Button BTN_Option;
    [SerializeField] private Button BTN_ActionLog;

    [Header("Panel")]
    public UISelection UISelection;
    public UIGraveyard UIGraveyard;
    public UITimer UITimer;
    public UIHint UIHint;
    public UIWaitingMessage UIWaiting;

    public CardSelectionUI CardSelectionUI;
    public TauntTalkController TauntTalkController;
    public Image Icon_Mute;
    public Image Icon_Unmute;

    [Header("UI Timer")]
    public UITimerChoice TimerRedraw;
    public UITimerChoice TimerChoice;

    [Header("End Scene")]
    public Button EndButton;

    [Header("Top Player")]
    public UIPlayerDock UIPlayerTop;
    public UIGlowPlayerDock TopGlowDock;
    public Transform MarkTopCenter;
    public UIHeroDead TopDeadAnimator;
    public BackCardDeck TopBackCardDeck;
    public GameObject TopGraveDeck;

    [Header("Bottom Player")]
    public UIPlayerDock UIPlayerBottom;
    public UIGlowPlayerDock BottomGlowDock;
    public Transform MarkBottomCenter;
    public UIHeroDead BotDeadAnimator;
    public BackCardDeck BotBackCardDeck;
    public GameObject BotGraveDeck;

    [Header("UI")]
    public UIBTNEndTurn BTN_EndTurn;


    [Header("Effect Prefab")]
    public GameObject SpiritLineBlue;
    public GameObject SpiritLineRed;

    public bool IsHilightBoxShowing { get; private set; }
    public bool IsMulliganShowing { get; private set; }
    public bool IsSelectionSlotShowing { get; private set; }
    public bool IsGraveyardShowing { get; private set; }
    public bool IsReadyToEndTurn
    {
        get
        {
            if (!IsSelectionSlotShowing && !IsMulliganShowing)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
    }
    public bool IsClickedEndGame { get; private set; }
    #endregion

    #region Private Properties
    private float _targetTimer = 20;
    private CardUI _currentTokenActivating;
    private CardUI _cardEnemyHover;
    private Vector3 _defaultCameraPosition;
    private bool _isTimerUIShow = false;

    private UnityAction<PlayerIndex, List<int>> _onRedrawComplete;
    #endregion

    private void Start()
    {
        if (MainCamera == null)
        {
            MainCamera = GameObject.FindObjectOfType<Camera>();
        }

        _defaultCameraPosition = MainCamera.transform.localPosition;
    }

    public void Init()
    {
        InitBTN();
        InitProgrssionController();
    }

    private void InitBTN()
    {
        BTN_ActionLog.onClick.RemoveAllListeners();
        BTN_ActionLog.onClick.AddListener(UIActionLogController.ShowUI);

        BTN_Option.onClick.RemoveAllListeners();
        BTN_Option.onClick.AddListener(UIPauseMenu.ShowUI);
    }

    private void InitProgrssionController()
    {
        ProgressionController.InitData();
    }

    #region UIQueue
    public void InitUIQueue()
    {
        UIQueue.Instance.Init();
    }

    public bool GetUIQueueIsReady()
    {
        return UIQueue.Instance.IsReady;
    }

    public string GetUIQueueDebugText()
    {
        return UIQueue.Instance.GetDebugText();
    }
    #endregion

    public void ShowDetailCardUI(bool isShow)
    {
        if (isShow)
        {
            if (!IsHilightBoxShowing)
            {
                CardUIDetailHover.ShowCard(true);
                IsHilightBoxShowing = true;
            }
        }
        else
        {
            if (IsHilightBoxShowing)
            {
                CardUIDetailHover.ShowCard(false);
                IsHilightBoxShowing = false;
            }
        }
    }

    public void RequestUpdateCardUI(CardUIData targetBattleCard, bool isAdd = true)
    {
        UIUpdateCardUICmd cmd = new UIUpdateCardUICmd(targetBattleCard, "UpdateCardUI");
        if (isAdd)
        {
            UIQueue.Instance.Add(cmd);
        }
        else
        {
            UIQueue.Instance.Join(cmd);
        }
    }

    public void RequestUpdateCardAllPassive(CardUIData targetBattleCard, bool isAdd = true)
    {
        Debug.LogFormat("RequestUpdateCardAllPassive: {0}:{1}:{2}", targetBattleCard.UniqueID, targetBattleCard.Name, targetBattleCard.CurrentZone);

        UIUpdateCardAllPassiveCmd cmd = new UIUpdateCardAllPassiveCmd(targetBattleCard, "UpdateCardPassive");
        if (isAdd)
        {
            UIQueue.Instance.Add(cmd);
        }
        else
        {
            UIQueue.Instance.Join(cmd);
        }
    }

    public void RequestUpdateCardPassive(CardUIData targetBattleCard, CardStatusType cardStatusType, bool isActive, bool isAdd = true)
    {
        UIUpdateCardPassiveCmd cmd = new UIUpdateCardPassiveCmd(targetBattleCard, cardStatusType, isActive,"UpdateCardPassive");
        if (isAdd)
        {
            UIQueue.Instance.Add(cmd);
        }
        else
        {
            UIQueue.Instance.Join(cmd);
        }
    }

    public void UpdateCardUI(CardUIData targetBattleCard, UnityAction onComplete)
    {
        CardUI card = BoardManager.Instance.GetTokenByUniqueID(targetBattleCard.UniqueID);
        if (card != null)
        {
            card.UpdateCardUI(targetBattleCard, onComplete);
            return;
        }
        else
        {
            Debug.LogError("UpdateCardUI: Error CardUI is null. " + targetBattleCard.UniqueID);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void UpdateCardAllPassiveUI(CardUIData targetBattleCard, UnityAction onComplete)
    {
        CardUI card = BoardManager.Instance.GetTokenByUniqueID(targetBattleCard.UniqueID);
        if (card != null)
        {
            card.UpdateAllPassiveUI(targetBattleCard, onComplete);
            return;
        }
        else
        {
            Debug.LogError("UpdateCardAllPassiveUI: Error CardUI is null. " + targetBattleCard.UniqueID);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void UpdateCardPassiveUI(CardUIData targetBattleCard, CardStatusType statusType, bool isActive, UnityAction onComplete)
    {
        CardUI card = BoardManager.Instance.GetTokenByUniqueID(targetBattleCard.UniqueID);
        if (card != null)
        {
            card.UpdatePassiveUI(targetBattleCard, statusType, isActive, onComplete);
            return;
        }
        else
        {
            Debug.LogError("UpdateCardPassiveUI: Error CardUI is null. " + targetBattleCard.UniqueID);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ShowTurnGlow(PlayerIndex playerIndex)
    {
        if (GameManager.Instance.IsLocalPlayer(playerIndex))
        {
            // Is Local
            BottomGlowDock.GlowActive(true);
            TopGlowDock.GlowActive(false);
        }
        else
        {
            TopGlowDock.GlowActive(true);
            BottomGlowDock.GlowActive(false);
        }
    }

    public void SetPlayerAP(PlayerIndex playerIndex, int value)
    {
        if (GameManager.Instance.IsLocalPlayer(playerIndex))
        {
            UIPlayerBottom.RegenPlayerAP(value);
        }
        else
        {
            UIPlayerTop.RegenPlayerAP(value);
        }
    }

    public void SetupPlayboard(string boardID)
    {
        PlayboardController.Setup(boardID);
    }

    #region Camera

    public void CameraScreenShake(float strength = 5)
    {
        MainCamera.DOShakePosition(0.15f, 0.02f * strength, 1, 2).SetEase(Ease.OutElastic).OnComplete(delegate
        {
            MainCamera.transform.localPosition = _defaultCameraPosition;
        });
    }

    public void CameraScreenShake(float duration, float strength, int vibrato, float randomness)
    {
        MainCamera.DOShakePosition(duration, strength, vibrato, randomness).SetEase(Ease.OutElastic).OnComplete(delegate
        {
            MainCamera.transform.localPosition = _defaultCameraPosition;
        });
    }

    public void CameraScreenShake(float strength, float duration)
    {
        MainCamera.DOShakePosition(duration, strength, (int)(15.0f * duration), 45).SetEase(Ease.OutExpo).OnComplete(delegate
        {
            MainCamera.transform.localPosition = _defaultCameraPosition;
        });
    }
    #endregion

    #region AP
    public void SetPlayerAP(PlayerIndex playerIndex, int value, UnityAction onComplete)
    {
        if (GameManager.Instance.IsLocalPlayer(playerIndex))
        {
            UIPlayerBottom.RegenPlayerAP(value);
        }
        else
        {
            UIPlayerTop.RegenPlayerAP(value);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ShowRegenAP(PlayerIndex playerIndex, int value, UnityAction onComplete)
    {
        if (GameManager.Instance.IsLocalPlayer(playerIndex))
        {
            UIPlayerBottom.RegenPlayerAP(value);
        }
        else
        {
            UIPlayerTop.RegenPlayerAP(value);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ShowUseCardAP(PlayerIndex playerIndex, int value, UnityAction onComplete)
    {
        if (GameManager.Instance.IsLocalPlayer(playerIndex))
        {
            UIPlayerBottom.UseCardPlayerAp(value);
        }
        else
        {
            UIPlayerTop.UseCardPlayerAp(value);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ShowUseDrawAP(PlayerIndex playerIndex, int value, UnityAction onComplete)
    {
        if (GameManager.Instance.IsLocalPlayer(playerIndex))
        {
            UIPlayerBottom.UseDrawPlayerAp(value);
        }
        else
        {
            UIPlayerTop.UseDrawPlayerAp(value);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion

    #region IntroUI
    public void StartIntro(bool isLocalFirst, UnityAction onComplete)
    {
        IntroAnimation.InitAnimation(isLocalFirst, onComplete);
    }

    public void SetPlayerInfo(CardUIManager.PlayerPosition playerPosition, BattlePlayerData data)
    {
        if (playerPosition == CardUIManager.PlayerPosition.Top)
        {
            IntroAnimation.SetTopProfile(data);
        }
        else
        {
            IntroAnimation.SetBotProfile(data);
        }
    }
    #endregion

    #region Outtro UI
    public void StartOuttro(bool isLocalVictory,BattlePlayerData profile, UnityAction onComplete)
    {
        OuttroAnimation.SetProfile(profile);
        OuttroAnimation.InitAnimation(isLocalVictory, onComplete);

    }

    #endregion

    #region EndUI

    public void ShowLevelSummary(bool isVictory, UnityAction onComplete)
    {
        // Hide Other Overlay UI
        HideGraveyard();

        // Init End BTN
        EndButton.onClick.RemoveAllListeners();
        EndButton.onClick.AddListener(delegate
        {
            if (!IsClickedEndGame)
            {
                IsClickedEndGame = true;
                SoundManager.PlaySFX(SoundManager.SFX.End_Game_Button_Click);

                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            }
        });
        EndButton.SendMessage("OnEnable", SendMessageOptions.DontRequireReceiver);

        // Check Show EXP
        bool isShowExp = false;
        if (GameManager.Instance.Mode == GameMode.Casual
            || GameManager.Instance.Mode == GameMode.Rank)
        {
            isShowExp = true;
        }

        // Setup Victory
        BattlePlayerData player = GameManager.Instance.Data.PlayerDataList[(int)GameManager.Instance.GetLocalPlayerIndex()];
        CallbackUI callbackUI = new CallbackUI(delegate
        {
            GameManager.Instance.DisconnectPhoton();
            StartOuttro(isVictory, player, delegate
            {
                EndButton.gameObject.SetActive(true);
                // EXP Callback
                if (isShowExp)
                {
                    CallbackUI callbackExpUI = new CallbackUI(delegate
                    {
                        ProgressionController.StartProgression(null);
                    });

                    UIQueue.Instance.Add(callbackExpUI);
                }
            });
        });

        // Add Queue
        WaitUI waitUI = new WaitUI(1.5f);
        UIQueue.Instance.Add(waitUI);
        UIQueue.Instance.Add(callbackUI); 

    }

    #endregion

    #region Redraw UI
    public void ShowRedraw(string title ,string subTitle,bool isPlayFirst, List<CardUIData> cardUIDatas, int count, UnityAction<PlayerIndex, List<int>> onClickComplete)
    {
        IsMulliganShowing = true;
        _onRedrawComplete = onClickComplete;
        ShowSelectionCard(title, subTitle, cardUIDatas, count, delegate(PlayerIndex playerIndex, List<int> cardListID)
        {
            TimerRedraw.ShowUI(false);
            _onRedrawComplete?.Invoke(playerIndex, cardListID);
        });
        TimerRedraw.ShowUI(true);
    }

    public void HideRedraw()
    {
        IsMulliganShowing = false;
        HideSelectionCard();
        TimerRedraw.ShowUI(false);
    }

    public void RequestReturnCardToDeck(CardUIData data)
    {
        CardUIManager.Instance.ReturnCardToDeck(data.Owner, data.UniqueID);
    }

    private void OnRedrawConfirm(PlayerIndex playerIndex, List<int> cardUIDataList)
    {
        _onRedrawComplete.Invoke(GameManager.Instance.GetLocalPlayerIndex(), CardUIManager.Instance.RedrawList);
    }

    public void ClearRedrawList()
    {
        CardUIManager.Instance.ClearRedrawList();
    }
    #endregion

    #region Turn Box
    public void ShowTurnBox(PlayerIndex playerIndex, UnityAction onComplete)
    {
        if (GameManager.Instance.IsLocalPlayer(playerIndex))
        {
            // Is Local
            HideGraveyard();
            UITurn.Show(true, onComplete);
        }
        else
        {
            UITurn.Show(false, onComplete);
        }
    }
    #endregion

    #region Waiting
    public void ShowWaiting()
    {
        UIWaiting.ShowUI();
    }

    public void HideWaiting()
    {
        UIWaiting.HideUI();
    }
    #endregion

    #region UI
    public void ShowDeck(bool isShow, PlayerIndex playerIndex)
    {
        if (GameManager.Instance.IsLocalPlayer(playerIndex))
        {
            UIPlayerBottom.ShowDeck(isShow);
            BotBackCardDeck.gameObject.SetActive(isShow);
        }
        else
        {
            UIPlayerTop.ShowDeck(isShow);
            TopBackCardDeck.gameObject.SetActive(isShow);
        }
    }

    public void ShowGrave(bool isShow, PlayerIndex playerIndex)
    {
        if (GameManager.Instance.IsLocalPlayer(playerIndex))
        {
            UIPlayerBottom.ShowGrave(isShow);
            BotGraveDeck.gameObject.SetActive(isShow);
        }
        else
        {
            UIPlayerTop.ShowGrave(isShow);
            TopGraveDeck.gameObject.SetActive(isShow);
        }
    }
    #endregion

    #region Selection Slot UI
    public void RequestShowSelectionSlot(Requester requester, List<int> listSlotId, int selectCount, float decisionTime, string info, UnityAction<List<int>> onComplete, UnityAction onTimeOut)
    {
        if (IsSelectionSlotShowing == false)
        {
            IsSelectionSlotShowing = true;

            // Clear
            _currentTokenActivating = null;

            if (requester.Type == Requester.RequesterType.Minion)
            {
                FindCardByUniqueID(requester.Minion.UniqueID, out _currentTokenActivating);
                //_currentTokenActivating.EffectActivating(true);
                if (GameManager.Instance.IsNetworkPlay())
                {
                    GameManager.Instance.PhotonRequestShowEffectActivating(_currentTokenActivating.Data.UniqueID, true);
                }
                else
                {
                    _currentTokenActivating.EffectActivating(true);
                }
            }

            HideGraveyard();
            HideCardsIsReady();

            CardUIData data = new CardUIData(requester.GetUniqueID(), requester.Minion.RawData, requester.Minion.CardBackID);
            UIShowSelectionSlotCmd cmd = new UIShowSelectionSlotCmd(data, listSlotId, selectCount, decisionTime, info, onComplete, onTimeOut, "SelectionUI");
            UIQueue.Instance.Join(cmd);
        }
        else
        {
            Debug.LogErrorFormat("RequestShowSelectionSlot: Failed to show slot hightlight !!");
        }
    }

    public void HideSelectionSlot(UnityAction onComplete)
    {
        if (IsSelectionSlotShowing == true)
        {
            IsSelectionSlotShowing = false;
            UISelection.HideUI();
            BoardManager.Instance.HideAllHilight();
            BoardManager.Instance.HideAllSelectSlot();

            UnityAction action = delegate ()
            {
                GameManager.Instance.UpdateLocalHandCard();

                if (_currentTokenActivating != null)
                {
                    if (GameManager.Instance.IsNetworkPlay())
                    {
                        GameManager.Instance.PhotonRequestShowEffectActivating(_currentTokenActivating.Data.UniqueID, false);
                    }
                    else
                    {
                        _currentTokenActivating.EffectActivating(false);
                    }
                }

                onComplete?.Invoke();
            };

            if (GameManager.Instance.IsHaveTimer())
            {
                TimerChoice.ShowUI(false, action);
            }
            else
            {
                action?.Invoke();
            }
        }
        else
        {
            onComplete?.Invoke();
        }
    }
    #endregion

    #region Graveyard UI
    public void RequestShowGraveyard(LocalPlayerIndex localPlayerIndex)
    {
        GameManager.Instance.ActionShowGraveyard(localPlayerIndex);
    }

    public void ShowGraveyard(List<CardUIData> cardUIDataList, bool isLocal)
    {
        if (!IsGraveyardShowing)
        {
            IsGraveyardShowing = true;
            UIGraveyard.ShowUI(cardUIDataList, isLocal);
        }
    }

    public void HideGraveyard()
    {
        if (IsGraveyardShowing)
        {
            IsGraveyardShowing = false;
            UIGraveyard.HideUI();
        }
    }
    #endregion

    #region Card Methods
    public void RequestDrawCardOnly(CardUIData card)
    {
        UIDrawCardCmd cmd = new UIDrawCardCmd(card, "DrawCardOnly");
        UIQueue.Instance.Add(cmd);
    }

    public void RequestDrawFast(CardUIData card, CardUIManager.PlayerPosition playerPosition)
    {
        UIDrawFastCmd cmd = new UIDrawFastCmd(card, playerPosition, "DrawFast");
        UIQueue.Instance.Add(cmd);
    }

    public void RequestDrawCardToSlot(CardUIData card, CardUIManager.PlayerPosition playerPosition)
    {
        UIDrawToSlotCmd cmd = new UIDrawToSlotCmd(card, playerPosition, "DrawToSlot");
        UIQueue.Instance.Add(cmd);
    }

    public void RequestDrawCardFromGrave(CardUIData card, CardUIManager.PlayerPosition playerPosition)
    {
        UIDrawToSlotFromGraveCmd cmd = new UIDrawToSlotFromGraveCmd(card, playerPosition, "DrawToSlotFromGrave");
        UIQueue.Instance.Add(cmd);
    }

    public void RequestDrawCardToDestroy(CardUIData card, CardUIManager.PlayerPosition playerPosition)
    {
        UIDrawToDestroyCmd cmd = new UIDrawToDestroyCmd(card, playerPosition, "DrawToDestroy");
        UIQueue.Instance.Add(cmd);
    }

    public void RequestDrawNoCardToDestroy(PlayerIndex playerIndex)
    {
        UINoDrawToDestroyCmd cmd = new UINoDrawToDestroyCmd(playerIndex, "DrawNoCardToDestroy");
        UIQueue.Instance.Add(cmd);
    }

    public void RequestDiscard(CardUIData card)
    {
        UIDiscardCmd cmd = new UIDiscardCmd(card, "Discard");
        UIQueue.Instance.Add(cmd);
    }

    public void RequestRemoveCard(CardUIData card)
    {
        UIRemoveCardCmd cmd = new UIRemoveCardCmd(card, "Remove");
        UIQueue.Instance.Add(cmd);
    }

    public void DetachAndMoveCardFromHand(int uniqueID, int slotID)
    {
        CardUI card;
        CardUIManager.PlayerPosition playerPosition;
        CardUIManager.Instance.FindCardUIByID(uniqueID, out card, out playerPosition);
        CardUIManager.Instance.DetachCard(playerPosition, card);

    }

    public void ShowHand()
    {
        if (GameHelper.GetScreenFitMode() == ScreenFitMode.Height)
        {
            CardUIManager.Instance.ShowHandCard();
            EnableTauntBTN(true);
        }
    }

    public void HideHand()
    {
        if (GameHelper.GetScreenFitMode() == ScreenFitMode.Height)
        {
            CardUIManager.Instance.HideHandCard();
            EnableTauntBTN(true);
        }
    }

    public void ShowCardsIsReady()
    {
        CardUIManager.Instance.ShowCardReadyToActive(true);
    }

    public void ShowCardsIsReady(int uniqueID)
    {
        CardUIManager.Instance.ShowCardReadyToActive(uniqueID, true);
    }

    public void HideCardsIsReady()
    {
        CardUIManager.Instance.ShowCardReadyToActive(false);
    }

    public void HideCardsIsReady(int uniqueID)
    {
        CardUIManager.Instance.ShowCardReadyToActive(uniqueID, false);
    }

    public void CancelHilight()
    {
        CardUIManager.Instance.PointerIsHolding(false);
        if (CardUIManager.Instance.GetCardIsUsing() || CardUIManager.Instance.IsSelected)
        {
            CardUIManager.Instance.CancelSelected(true);
            CardUIManager.Instance.CancelHilight(true);
        }
        BoardManager.Instance.HideAllHilight();
        BoardManager.Instance.CancelHilight();
        UIGraveyard.HideGraveHilight();
    }

    public void ShowMoveToCardPreUse(int uniqueId, bool isShow, UnityAction onComplete)
    {
        CardUI card;
        if (FindCardByUniqueID(uniqueId, out card))
        {
            if (isShow)
            {
                if (GameManager.Instance.IsLocalPlayer(card.Data.Owner))
                {
                    if (UIQueue.Instance.IsReady)
                    {
                        card.AnimationCardToPosition(MarkBottomCenter, onComplete);
                    }
                }
                else
                {
                    if (UIQueue.Instance.IsReady)
                    {
                        card.AnimationCardToPosition(MarkTopCenter, onComplete);
                    }
                }
            }
            else
            {
                if (GameManager.Instance.IsLocalPlayer(card.Data.Owner))
                {
                    CardUIManager.Instance.PlayAnimationCardSort(CardUIManager.PlayerPosition.Bottom, onComplete);
                }
                else
                {
                    CardUIManager.Instance.PlayAnimationCardSort(CardUIManager.PlayerPosition.Top, onComplete);
                }
            }


        }
    }

    public void ShowSelectionCard(string title, string subTitle, List<CardUIData> cardDatas, int require, UnityAction<PlayerIndex, List<int>> onComfirm)
    {
        CardSelectionUI.ShowCard(title, subTitle, cardDatas, require, onComfirm);
    }

    public void HideSelectionCard()
    {
        CardSelectionUI.HideCard();
    }
    #endregion

    #region Token Methods
    public void RequestSpawnTokenToBoard(CardUIData card, UnityAction onComplete)
    {
        UISpawnTokenToBoardCmd cmd = new UISpawnTokenToBoardCmd(card, onComplete, "SpawnTokenToBoard");
        UIQueue.Instance.Add(cmd);
    }

    public void RequestDestroyToken(CardUIData card, bool isAdd)
    {
        UIDestroyToken cmd = new UIDestroyToken(card.UniqueID, "DestroyToken");
        if (isAdd)
        {
            UIQueue.Instance.Add(cmd);
        }
        else
        {
            UIQueue.Instance.Join(cmd);
        }
    }

    public void RequestBanishToken(CardUIData card, bool isAdd)
    {
        UIBanishToken cmd = new UIBanishToken(card.UniqueID, "BanishToken");
        if (isAdd)
        {
            UIQueue.Instance.Add(cmd);
        }
        else
        {
            UIQueue.Instance.Join(cmd);
        }
    }

    public void RequestMoveTokenToHand(CardUIData card, CardUIManager.PlayerPosition playerPosition)
    {
        UIMoveTokenToHandCmd cmd = new UIMoveTokenToHandCmd(card, playerPosition, "MoveTokenToHand");
        UIQueue.Instance.Add(cmd);
    }

    public void RequestTokenAttackTo(CardUIData attacker, CardUIData defender, List<CardUIData> defenderList, int damage, bool isCritical)
    {
        UITokenAttackToCmd cmd = new UITokenAttackToCmd(attacker, defender, defenderList, damage, isCritical, "TokenAttackTo");
        UIQueue.Instance.Add(cmd);
    }

    public void RequestMoveTokenToSlot(CardUIData token, int slotId, bool isAdd = true)
    {
        UIMoveTokenToSlotCmd cmd = new UIMoveTokenToSlotCmd(token, slotId, "TokenMoveToSlot");
        if (isAdd)
        {
            UIQueue.Instance.Add(cmd);
        }
        else
        {
            UIQueue.Instance.Join(cmd);
        }
    }
    #endregion

    public void RequestUpdateUIInfo(PlayerIndex playerIndex, int power, int life, int deckCount, int graveCount, bool isAdd = true)
    {
        UIUpdatePlayerInfoCmd cmd = new UIUpdatePlayerInfoCmd(playerIndex, power, life, deckCount, graveCount, "UpdatePlayerUI");
        if (isAdd)
        {
            UIQueue.Instance.Add(cmd);
        }
        else
        {
            UIQueue.Instance.Join(cmd);
        }
    }

    public void UpdateUIPlayerInfo(PlayerIndex playerIndex, int power, int life, int deckCount, int graveCount)
    {
        if (GameManager.Instance.IsLocalPlayer(playerIndex))
        {
            // FOR TUTORIAL ONLY --------------------------------
            bool canSetPower = true;
            if (GameManager.Instance.IsStoryMode())
            {
                StoryGameController tutorialController = GameManager.Instance.Controller as StoryGameController;
                canSetPower = tutorialController.CanSetPower();
            }
            if (canSetPower)
            {
                UIPlayerBottom.SetPower(power); // Spirit
            }
            // ---------------------------------------------------- 
            UIPlayerBottom.SetLife(life, GameManager.Instance.Data.PlayerDataList[(int)playerIndex].MaxHP);
            UIPlayerBottom.SetDeckCount(deckCount);
            UIPlayerBottom.SetGraveCount(graveCount);
            BotBackCardDeck.UpdateCardCount(GameManager.Instance.Data.PlayerDataList[(int)playerIndex].DeckCount);

            int soulDmg = 0;
            if (GameManager.Instance.GetPlayerCount() == 2) // 2 players
            {
                if (GameManager.Instance.GetLocalPlayerIndex() == PlayerIndex.One)
                {
                    soulDmg = GameManager.Instance.GetSumSpirit(PlayerIndex.Two);
                }
                else
                {
                    soulDmg = GameManager.Instance.GetSumSpirit(PlayerIndex.One);
                }

                if (life <= soulDmg)
                {
                    UIPlayerBottom.AnimationLifeWarning(true);
                }
                else
                {
                    UIPlayerBottom.AnimationLifeWarning(false);
                }
            }
            else
            {
                // For other GameMode
            }
        }
        else
        {
            // FOR TUTORIAL ONLY --------------------------------
            bool canSetPower = true;
            if (GameManager.Instance.IsStoryMode())
            {
                StoryGameController tutorialController = GameManager.Instance.Controller as StoryGameController;
                canSetPower = tutorialController.CanSetPower();
            }
            if (canSetPower)
            {
                UIPlayerTop.SetPower(power); // Spirit
            }
            // ----------------------------------------------------

            UIPlayerTop.SetLife(life, GameManager.Instance.Data.PlayerDataList[(int)playerIndex].MaxHP);
            UIPlayerTop.SetDeckCount(deckCount);
            UIPlayerTop.SetGraveCount(graveCount);
            TopBackCardDeck.UpdateCardCount(GameManager.Instance.Data.PlayerDataList[(int)playerIndex].DeckCount);

            int soulDmg = 0;
            if (GameManager.Instance.GetPlayerCount() == 2) // 2 players
            {
                soulDmg = GameManager.Instance.GetSumSpirit(GameManager.Instance.GetLocalPlayerIndex());

                if (life <= soulDmg)
                {
                    UIPlayerTop.AnimationLifeWarning(true);
                }
                else
                {
                    UIPlayerTop.AnimationLifeWarning(false);
                }
            }
            else
            {
                // For other GameMode
            }
        }
    }

    public void InitUIPlayerInfo(PlayerIndex playerIndex, string name, string avatar, string id, int power, int life, int deckCount, int graveCount)
    {
        if (GameManager.Instance.IsLocalPlayer(playerIndex))
        {
            UIPlayerBottom.SetHeroImage(id);
            UIPlayerBottom.SetAvatar(avatar);
            UIPlayerBottom.SetName(name);
            UIPlayerBottom.SetPower(power); // Spirit
            UIPlayerBottom.SetLife(life, GameManager.Instance.Data.PlayerDataList[(int)playerIndex].MaxHP);
            UIPlayerBottom.SetDeckCount(deckCount);
            UIPlayerBottom.SetGraveCount(graveCount);
            UIPlayerBottom.SetHUD(true, GameManager.Instance.Data.PlayerDataList[(int)playerIndex].HUDSkinID);
            BotBackCardDeck.InitData(GameManager.Instance.Data.PlayerDataList[(int)playerIndex].CardBackID);
            BotBackCardDeck.UpdateCardCount(GameManager.Instance.Data.PlayerDataList[(int)playerIndex].DeckCount);
            /*
            int soulDmg = 0;

            if (GameManager.Instance.Mode == GameMode.Dual_1vs1casual)
            {
                if (GameManager.Instance.GetLocalPlayerIndex() == PlayerIndex.One)
                {
                    soulDmg = GameManager.Instance.GetSumSpirit(PlayerIndex.Two);
                }
                else
                {
                    soulDmg = GameManager.Instance.GetSumSpirit(PlayerIndex.One);
                }

                if (life <= soulDmg)
                {
                    UIPlayerBottom.AnimationLifeWarning(true);
                }
                else
                {
                    UIPlayerBottom.AnimationLifeWarning(false);
                }
            }
            else
            {
                // For other GameMode
            }
            */
        }
        else
        {
            UIPlayerTop.SetHeroImage(id);
            UIPlayerTop.SetName(name);
            UIPlayerTop.SetAvatar(avatar);
            UIPlayerTop.SetPower(power); // Spirit
            UIPlayerTop.SetLife(life, GameManager.Instance.Data.PlayerDataList[(int)playerIndex].MaxHP);
            UIPlayerTop.SetDeckCount(deckCount);
            UIPlayerTop.SetGraveCount(graveCount);
            UIPlayerTop.SetHUD(false, GameManager.Instance.Data.PlayerDataList[(int)playerIndex].HUDSkinID);
            TopBackCardDeck.InitData(GameManager.Instance.Data.PlayerDataList[(int)playerIndex].CardBackID);
            TopBackCardDeck.UpdateCardCount(GameManager.Instance.Data.PlayerDataList[(int)playerIndex].DeckCount);
            /*
            int soulDmg = 0;
            if (GameManager.Instance.Mode == GameMode.Dual_1vs1casual)
            {
                soulDmg = GameManager.Instance.GetSumSpirit(GameManager.Instance.GetLocalPlayerIndex());

                if (life <= soulDmg)
                {
                    UIPlayerTop.AnimationLifeWarning(true);
                }
                else
                {
                    UIPlayerTop.AnimationLifeWarning(false);
                }
            }
            else
            {
                // For other GameMode
            }
            */
        }
    }

    public void RequestTokenGlowActivate(int cardUniqueID, bool isShow, bool isAdd = true)
    {
        UITokenGlowActivateCmd cmd = new UITokenGlowActivateCmd(cardUniqueID, isShow, "ShowTokenGlowActive");
        if (isAdd)
        {
            UIQueue.Instance.Add(cmd);
        }
        else
        {
            UIQueue.Instance.Join(cmd);
        }
    }

    #region Timer
    public void ShowUITimer(CardUIManager.PlayerPosition playerPosition, float duration)
    {
        _isTimerUIShow = true;
        UITimer.ShowTimer(playerPosition, duration);
    }

    public void HideUITimer()
    {
        _isTimerUIShow = false;
        UITimer.HideTimer();
    }

    public void UpdateTimerText(float time)
    {
        UITimer.UpdateTime(time);
    }

    public void SetTargetTimer(float targetTimer)
    {
        _targetTimer = targetTimer;
    }

    public void TimerUpdate(float time, CardUIManager.PlayerPosition playerPosition)
    {
        if (time < _targetTimer)
        {
            if (_isTimerUIShow == false) HideGraveyard();

            ShowUITimer(playerPosition, _targetTimer);
            UpdateTimerText(time);
        }
        else
        {
            HideUITimer();

        }
    }
    #endregion

    #region Hint UI
    public void ShowHint(string message)
    {
        UIHint.ShowUI(message);
    }

    public void HideHint()
    {
        UIHint.HideUI();
    }
    #endregion

    #region End Turn
    public void EndButtonIsOpen(bool isReady)
    {
        BTN_EndTurn.ButtonIsOpen(isReady);
    }

    public void EndButtonIsOpen(bool isReady, bool isPass)
    {
        BTN_EndTurn.ButtonIsOpen(isReady);
        BTN_EndTurn.ButtonIsPass(isPass);
    }

    public void EndButtonIsPass(bool isPass)
    {
        BTN_EndTurn.ButtonIsPass(isPass);
    }

    public void EndTurnIsFinish(bool isDone)
    {
        BTN_EndTurn.ButtonIsFinish(isDone);
    }

    public void EndBTNComplete()
    {
        BTN_EndTurn.EndButtonAnimation();
    }

    public void ShowBTNOverlayUI(bool isShow)
    {
        if (isShow)
        {
            //BTN_EndTurn.gameObject.SetActive(true);
            BTN_Option.gameObject.SetActive(true);
            BTN_ActionLog.gameObject.SetActive(true);
        }
        else
        {
            //BTN_EndTurn.gameObject.SetActive(false);
            BTN_Option.gameObject.SetActive(false);
            BTN_ActionLog.gameObject.SetActive(false);
        }
    }
    
    public void ShowActionLog(bool isShow)
    {
        if (isShow)
        {
            UIActionLogController.ShowUI();
        }
        else
        {
            UIActionLogController.Hide();
        }
    }

    #endregion

    #region Lighting

    public void FadeBlackIn()
    {
        if (LightingController == null)
        {
            LightingController = GameObject.FindObjectOfType<LightingController>();
        }

        //UIBGBlackFadeStart.ShowBlack();
        LightingController.AdjustSunLightIntensity(0);
    }

    public void FadeBlackOut()
    {
        if (LightingController == null)
        {
            LightingController = GameObject.FindObjectOfType<LightingController>();
        }

        //UIBGBlackFadeStart.HideBlack();
        LightingController.AdjustSunLightIntensity();
    }

    public void SpotLight_ShowHeroTop(bool isShow, UnityAction onComplete = null)
    {
        LightingController.ShowTopSpotLight(isShow, onComplete);
    }

    public void SpotLight_ShowHeroBot(bool isShow, UnityAction onComplete = null)
    {
        LightingController.ShowBotSpotLight(isShow, onComplete);
    }

    public void SpotLight_ShowMid(bool isShow, UnityAction onComplete = null)
    {
        LightingController.ShowMidSpotLight(isShow, onComplete);
    }
    #endregion

    #region Float Text
    public void AddFloatTextAP(string message, Vector2 startPosition, float duration, bool isMoveUp = true)
    {
        FloatTextController.AddFloatTextAP(message, startPosition, duration, isMoveUp);
    }

    public void AddFloatTextSoul(string message, Vector2 startPosition, float duration, bool isMoveUp = true)
    {
        FloatTextController.AddFloatTextSoul(message, startPosition, duration, isMoveUp);
    }

    public void AddFloatTextCustom(Color color, string message, Vector2 startPosition, float duration, bool isMoveUp = true)
    {
        FloatTextController.AddFloatTextCustom(color, message, startPosition, duration, isMoveUp);
    }

    public void AddFloatTextCustom(string colorCode, string message, Vector2 startPosition, float duration, bool isMoveUp = true)
    {
        Color color = GameHelper.GetColorByHtmlCode(colorCode);
        FloatTextController.AddFloatTextCustom(color, message, startPosition, duration, isMoveUp);
    }
    #endregion

    #region Action Log
    public void UpdateActionLog()
    {
        UIActionLogController.OnLogUpdated();
    }

    public void ActionLog_CreatePhase(string messsage)
    {
        UIActionLogController.CreateLog_Phase(messsage);
    }

    public void ActionLog_CreateSummary(BattlePlayerData battlePlayerData1, BattlePlayerData battlePlayerData2, int turn, int SoulSum1, int SoulSum2)
    {
        UIActionLogController.CreateLog_Summary(battlePlayerData1, battlePlayerData1, turn, SoulSum1, SoulSum2);
    }

    public void ActionLog_CreateAction(MinionData minion, string message)
    {
        UIActionLogController.CreateLog_Action(minion, message);
    }

    public void ActionLog_CreateAction(MinionData minion1, MinionData minion2, string message, UIActionLog_Action.ActionIcon actionIcon)
    {
        UIActionLogController.CreateLog_Action(minion1, minion2, message, actionIcon);
    }

    public void ActionLog_CreateAction(PlayerIndex playerIndex, string message)
    {
        UIActionLogController.CreateLog_Action(playerIndex, message);
    }
    #endregion

    #region Events
    public bool IsCardCanSpawn(int slotID, int uniqueID)
    {
        return GameManager.Instance.IsCanSpawnToken(slotID, uniqueID);
    }

    #endregion

    #region Effect Request
    public void RequestShowPlayerImmune(PlayerIndex playerIndex, bool isImmune)
    {
        UIShowPlayerImmuneCmd cmd = new UIShowPlayerImmuneCmd(playerIndex, isImmune);
        UIQueue.Instance.Add(cmd);
    }

    public void RequestShowPopupAttackOnCard(int damage, CardUIData defender, bool isCritical, bool isAdd = true)
    {
        UIShowPopupAttackOnCardCmd cmd = new UIShowPopupAttackOnCardCmd(damage, defender, isCritical, "ShowPopupAttack");
        if (isAdd)
        {
            UIQueue.Instance.Add(cmd);
        }
        else
        {
            UIQueue.Instance.Join(cmd);
        }
    }

    public void RequestShowPopupDamageOnCard(int damage, CardUIData defender, bool isAdd = true)
    {
        UIShowPopupDamageOnCardCmd cmd = new UIShowPopupDamageOnCardCmd(damage, defender, "ShowPopupDamage");
        if (isAdd)
        {
            UIQueue.Instance.Add(cmd);
        }
        else
        {
            UIQueue.Instance.Join(cmd);
        }
    }

    public void RequestShowPopupHealOnCard(int healValue, CardUIData target, bool isAdd = true)
    {
        UIShowPopupHealOnCardCmd cmd = new UIShowPopupHealOnCardCmd(healValue, target, "HealCard");
        if (isAdd)
        {
            UIQueue.Instance.Add(cmd);
        }
        else
        {
            UIQueue.Instance.Join(cmd);
        }
    }

    public void RequestShowPopupDamageOnPlayer(int damageValue, PlayerIndex playerIndex, bool isAdd = true)
    {
        UIShowPopupDamageOnPlayerCmd cmd = new UIShowPopupDamageOnPlayerCmd(damageValue, playerIndex, "DamagePlayer");
        if (isAdd)
        {
            UIQueue.Instance.Add(cmd);
        }
        else
        {
            UIQueue.Instance.Join(cmd);
        }

    }

    public void RequestShowPopupHealOnPlayer(int healValue, PlayerIndex playerIndex)
    {
        UIShowPopupHealOnPlayerCmd cmd = new UIShowPopupHealOnPlayerCmd(healValue, playerIndex, "HealPlayer");
        UIQueue.Instance.Add(cmd);
    }

    public void RequestSpiritAttackOnPlayer(int damage, PlayerIndex playerIndex)
    {
        UIShowSpiritAtackOnPlayerCmd cmd = new UIShowSpiritAtackOnPlayerCmd(damage, playerIndex);
        UIQueue.Instance.Add(cmd);
    }

    public void RequestSpiritAttackOnPlayerTutorial(int damage, PlayerIndex playerIndex)
    {
        UIShowSpiritAtackOnPlayerTutorialCmd cmd = new UIShowSpiritAtackOnPlayerTutorialCmd(damage, playerIndex);
        UIQueue.Instance.Add(cmd);
    }

    public void RequestDealDamageFromMinionToPlayer(CardUIData attacker, PlayerIndex player, int damage, string vfxKey)
    {
        UIShowDamageFromMinionToPlayerCmd cmd = new UIShowDamageFromMinionToPlayerCmd(attacker, player, damage, vfxKey, "DealDamagePlayer");
        UIQueue.Instance.Add(cmd);
    }

    public void RequestBoostAPFromMinionToPlayer(CardUIData booster, PlayerIndex player, int damage, string vfxKey)
    {
        UIShowBoostAPFromMinionToPlayerCmd cmd = new UIShowBoostAPFromMinionToPlayerCmd(booster, player, damage, vfxKey, "BoostPlayerAp");
        UIQueue.Instance.Add(cmd);
    }

    public void RequestDealDamageGroup(Vector3 target, string vfxKey, UnityAction onEffectComplete = null)
    {
        UIShowDealDamageGroupCmd cmd = new UIShowDealDamageGroupCmd(target, vfxKey, onEffectComplete, "DamageGroup");
        UIQueue.Instance.Add(cmd);
    }

    public void RequestDealDamageGroup(CardUIData target, string vfxKey, UnityAction onEffectComplete = null)
    {
        Vector3 targetPos = BoardManager.Instance.GetSlotHolder(target.SlotID).GetHolderPosition();
        UIShowDealDamageGroupCmd cmd = new UIShowDealDamageGroupCmd(targetPos, vfxKey, onEffectComplete, "DamageGroup");
        UIQueue.Instance.Add(cmd);
    }

    public void RequestDealDamageRow(Vector3 target, string vfxKey, List<CardDirectionType> directionList, bool isAdd, UnityAction onEffectComplete = null)
    {
        UIShowDealDamageRowCmd cmd = new UIShowDealDamageRowCmd(target, vfxKey, directionList, onEffectComplete, "DamageRow");
        if (isAdd)
        {
            UIQueue.Instance.Add(cmd);
        }
        else
        {
            UIQueue.Instance.Join(cmd);
        }
    }

    public void RequestDealDamageGlobal(string vfxKey,UnityAction onEffectComplete = null)
    {
        UIShowDealDamageGlobalCmd cmd = new UIShowDealDamageGlobalCmd(vfxKey, onEffectComplete, "GlobalDamage");
        UIQueue.Instance.Add(cmd);
    }

    public void RequestDealDamage_Single(Vector3 attacker, Vector3 defender, int damage, string vfxKeyStart, string vfxKeyMoving, string vfxKeyEnd,bool isAdd, UnityAction onEffectComplete = null)
    {
        UIShowDamageFromMinionToMinionSingleCmd cmd = new UIShowDamageFromMinionToMinionSingleCmd(attacker, defender, damage, vfxKeyStart, vfxKeyMoving, vfxKeyEnd, onEffectComplete, "DealDamageMinion");
        if (isAdd)
        {
            UIQueue.Instance.Add(cmd);
        }
        else
        {
            UIQueue.Instance.Join(cmd);
        }
    }

    public void RequestPassiveActivate(CardUIData target, AbilityFeedback abilityFeedback)
    {
        UIPassiveActivateCmd cmd = new UIPassiveActivateCmd(target, abilityFeedback, "PassiveActivate");
        UIQueue.Instance.Add(cmd);
    }

    public void RequestBoostActivate(CardUIData target, bool isAdd = false)
    {
        UIBoostActivateCmd cmd = new UIBoostActivateCmd(target, "Boost");
        if (isAdd)
        {
            UIQueue.Instance.Add(cmd);
        }
        else
        {
            UIQueue.Instance.Join(cmd);
        }

    }

    public void RequestWeakenActivate(CardUIData target, bool isAdd = false)
    {
        UIWeakenActivateCmd cmd = new UIWeakenActivateCmd(target, "Weaken");
        if (GameManager.Instance.Phase == GamePhase.Battle)
        {
            if (BattleManager.Instance.Phase >= BattleManager.BattlePhase.PreAttack
                && BattleManager.Instance.Phase <= BattleManager.BattlePhase.PostDefend)
            {

                UIQueue.Instance.Add(cmd);
                return;
            }

        }

        if (isAdd)
        {
            UIQueue.Instance.Add(cmd);
        }
        else
        {
            UIQueue.Instance.Join(cmd);
        }


    }

    public void RequestDirEffectChange(CardUIData target)
    {
        UIDirectionActivateCmd cmd = new UIDirectionActivateCmd(target, "DirChange");
        UIQueue.Instance.Add(cmd);
    }

    public void RequestShowLastWish(int cardId, UnityAction onAnimationComplete)
    {
        CardUI card;
        if (FindCardByUniqueID(cardId, out card))
        {
            UIShowLastWishCmd cmd = new UIShowLastWishCmd(card, onAnimationComplete, "LastWish");
            UIQueue.Instance.Add(cmd);
        }
    }

    public void RequestShowLock(int target, int duration, PlayerIndex owner, UnityAction onAnimationComplete)
    {
        UIShowLockCmd cmd = new UIShowLockCmd(target, duration, owner, onAnimationComplete, "Lock");
        UIQueue.Instance.Add(cmd);
    }

    public void RequestHideLock(int target, UnityAction onAnimationComplete)
    {
        UIHideLockCmd cmd = new UIHideLockCmd(target, onAnimationComplete, "Unlock");
        UIQueue.Instance.Add(cmd);
    }

    public void RequestShowDarkMatter(int target, PlayerIndex owner, UnityAction onAnimationComplete)
    {
        UIShowDarkMatterCmd cmd = new UIShowDarkMatterCmd(target, owner, onAnimationComplete, "ShowDarkMatter");
        UIQueue.Instance.Add(cmd);
    }

    public void RequestHideDarkMatter(int target, PlayerIndex owner, UnityAction onAnimationComplete)
    {
        UIHideDarkMatterCmd cmd = new UIHideDarkMatterCmd(target, owner, onAnimationComplete, "HideDarkMatter");
        UIQueue.Instance.Add(cmd);
    }

    public void RequestShowLinkOnSlot(int slotID, List<CardDirectionType> dirList, UnityAction onComplete)
    {
        UIShowLinkCmd cmd = new UIShowLinkCmd(slotID, dirList, onComplete);
        UIQueue.Instance.Join(cmd);
    }

    public void RequestSetPlayerAP(PlayerIndex playerIndex, int currentAP, UnityAction onComplete)
    {
        UISetPlayerAPCmd cmd = new UISetPlayerAPCmd(playerIndex, currentAP, onComplete);
        UIQueue.Instance.Join(cmd);
    }
    #endregion

    #region Effects Event
    public void ShowHeroDead(PlayerIndex playerIndex, UnityAction onAnimationComplete)
    {
        SoundManager.PlaySFX(SoundManager.SFX.End_Game_Hero_Token_Break);

        if (GameManager.Instance.IsLocalPlayer(playerIndex))
        {
            BotDeadAnimator.Show(onAnimationComplete);
        }
        else
        {
            TopDeadAnimator.Show(onAnimationComplete);
        }
    }

    public void ShowPlayerImmune(PlayerIndex playerIndex, bool isImmune)
    {
        if (GameManager.Instance.IsLocalPlayer(playerIndex))
        {
            // Is Local
            UIPlayerBottom.SetImmune(isImmune);
        }
        else
        {
            UIPlayerTop.SetImmune(isImmune);
        }

        //if (isImmune)
        {
            SoundManager.PlaySFX(SoundManager.SFX.Immune_Barrier_Activated);
        }
    }

    public void ShowPopupAttackOnCard(int baseDamage, CardUIData defender, bool isCritical, UnityAction onAnimationComplete)
    {
        //animation goes here
        CardUI card = BoardManager.Instance.GetTokenByUniqueID(defender.UniqueID);
        if (card != null)
        {
            if (defender.ArmorCurrent > 0)
            {
                UIEffectManager.Instance.SpawnPopupEffect(
                      PopupPool.PopupType.DealDamage_BattleArmor
                    , baseDamage - defender.ArmorCurrent
                    , card.transform.position + new Vector3(0, 0.05f, 0)
                    , onAnimationComplete
                );
            }
            else
            {
                UIEffectManager.Instance.SpawnPopupEffect(
                      PopupPool.PopupType.DealDamage
                    , baseDamage
                    , card.transform.position + new Vector3(0, 0.05f, 0)
                    , onAnimationComplete
                );
            }
        }
    }

    public void ShowPopupDamageOnCard(int baseDamage, CardUIData defender, UnityAction onAnimationComplete)
    {
        //animation goes here
        CardUI card = BoardManager.Instance.GetTokenByUniqueID(defender.UniqueID);
        if (card != null)
        {
            UIEffectManager.Instance.SpawnPopupEffect(
                     PopupPool.PopupType.DealDamage
                   , baseDamage
                   , card.transform.position + new Vector3(0, 0.05f, 0)
                   , onAnimationComplete
               );
        }
    }

    public void ShowPopupHealOnCard(int healValue, CardUIData target, UnityAction onAnimationComplete)
    {
        CardUI card = BoardManager.Instance.GetTokenByUniqueID(target.UniqueID);
        if (card != null)
        {
            UIEffectManager.Instance.SpawnVFX(CardVFXPool.VFXType.Status_PowerUp, card.transform.position + new Vector3(0, 0.05f, 0), null);
            UIEffectManager.Instance.SpawnPopupEffect(PopupPool.PopupType.Heal, healValue, card.transform.position + new Vector3(0, 0.05f, 0), onAnimationComplete);
        }
    }

    public void ShowPopupDamageOnPlayer(int damageValue, PlayerIndex playerIndex, UnityAction onAnimationComplete)
    {
        if (GameManager.Instance.IsLocalPlayer(playerIndex))
        {
            UIEffectManager.Instance.SpawnPopupEffect(PopupPool.PopupType.DealDamage, damageValue, UIPlayerBottom.GetHeroPosition(), onAnimationComplete);
        }
        else
        {
            UIEffectManager.Instance.SpawnPopupEffect(PopupPool.PopupType.DealDamage, damageValue, UIPlayerTop.GetHeroPosition(), onAnimationComplete);
        }
    }

    public void ShowPopupDamageOnPlayerTutorial(int damageValue, PlayerIndex playerIndex, UnityAction onAnimationComplete)
    {
        if (GameManager.Instance.IsLocalPlayer(playerIndex))
        {
            UIEffectManager.Instance.SpawnPopupEffect(PopupPool.PopupType.DealDamage_Tutorial, damageValue, UIPlayerBottom.GetHeroPosition(), onAnimationComplete);
        }
        else
        {
            UIEffectManager.Instance.SpawnPopupEffect(PopupPool.PopupType.DealDamage_Tutorial, damageValue, UIPlayerTop.GetHeroPosition(), onAnimationComplete);
        }
    }

    public void ShowPopupHealOnPlayer(int healValue, PlayerIndex playerIndex, UnityAction onAnimationComplete)
    {
        if (GameManager.Instance.IsLocalPlayer(playerIndex))
        {
            UIEffectManager.Instance.SpawnPopupEffect(PopupPool.PopupType.Heal, healValue, UIPlayerBottom.GetLifePosition(), onAnimationComplete);
        }
        else
        {
            UIEffectManager.Instance.SpawnPopupEffect(PopupPool.PopupType.Heal, healValue, UIPlayerTop.GetLifePosition(), onAnimationComplete);
        }

    }

    public void ShowPopupBoostAPOnPlayer(int healValue, PlayerIndex playerIndex, UnityAction onAnimationComplete)
    {
        if (GameManager.Instance.IsLocalPlayer(playerIndex))
        {
            UIEffectManager.Instance.SpawnPopupEffect(PopupPool.PopupType.BoostAp, healValue, UIPlayerBottom.GetAPPosition(), onAnimationComplete);
        }
        else
        {
            UIEffectManager.Instance.SpawnPopupEffect(PopupPool.PopupType.BoostAp, healValue, UIPlayerTop.GetAPPosition(), onAnimationComplete);
        }

    }

    public void ShowSpiritAttackOnPlayer(int damage, PlayerIndex playerIndex, UnityAction onComplete)
    {
        UIEffectManager.Instance.HeroBeAttacked(
            damage
            , playerIndex
            , delegate
            {
                ShowPopupDamageOnPlayer(damage, playerIndex, null);
                CameraScreenShake(damage * 2);
                ShowTauntAnimation(playerIndex, BaseHeroAnimation.HeroAnimationState.HIT);

                GameManager.Instance.OnShowSpiritAttackOnPlayerComplete(playerIndex, damage);
            }
            , delegate
            {
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            }
        );
    }

    public void ShowSpiritAttackOnPlayerTutorial(int damage, PlayerIndex playerIndex, UnityAction onComplete)
    {
        UIEffectManager.Instance.HeroBeAttacked(
            damage
            , playerIndex
            , delegate
            {
                ShowPopupDamageOnPlayerTutorial(damage, playerIndex, null);
                CameraScreenShake(damage * 2);
                ShowTauntAnimation(playerIndex, BaseHeroAnimation.HeroAnimationState.HIT);
                GameManager.Instance.OnShowSpiritAttackOnPlayerComplete(playerIndex, damage);
            }
            , delegate
            {
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            }
        );
    }

    public void ShowDealDamageFromMinionToMinion(Vector3 attacker, Vector3 defender, int damage, string vfxKey, UnityAction onComplete)
    {
        UIEffectManager.Instance.AbilityEffect(
              vfxKey
            , attacker
            , defender
            , delegate
            {
                CameraScreenShake(damage);
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            }
        );
    }

    public void ShowDealDamageFromMinionToPlayer(CardUIData attacker, PlayerIndex player, int damage, string vfxKey, UnityAction onComplete)
    {
        Vector3 startPosition = BoardManager.Instance.GetSlotHolder(attacker.SlotID).GetHolderPosition();
        Vector3 endPosition = new Vector3();
        if (GameManager.Instance.IsLocalPlayer(player))
        {
            endPosition = UIPlayerBottom.GetHeroPosition();
        }
        else
        {
            endPosition = UIPlayerTop.GetHeroPosition();
        }

        UIEffectManager.Instance.AbilityEffect(
              vfxKey
            , startPosition
            , endPosition
            , delegate
            {
                CameraScreenShake(damage);
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            }
        );
    }

    public void ShowBoostAPFromMinionToPlayer(CardUIData attacker, PlayerIndex player, int damage, string vfxKey, UnityAction onComplete)
    {
        Vector3 startPosition = BoardManager.Instance.GetSlotHolder(attacker.SlotID).GetHolderPosition();
        Vector3 endPosition = new Vector3();
        if (GameManager.Instance.IsLocalPlayer(player))
        {
            endPosition = UIPlayerBottom.GetAPPosition();
        }
        else
        {
            endPosition = UIPlayerTop.GetAPPosition();
        }

        UIEffectManager.Instance.AbilityEffect(
              vfxKey
            , startPosition
            , endPosition
            , delegate
            {
                //CameraScreenShake(damage);
                ShowPopupBoostAPOnPlayer(damage, player, null);
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            }
        );
    }

    public void ShowDamageRowAtSlotWithDirection(Vector3 target, string vfxKey, List<CardDirectionType> directionList, UnityAction onComplete)
    {
        UIEffectManager.Instance.AbilityRowEffect(vfxKey, target, directionList, onComplete);
    }

    public void ShowDamageGroupAtMinion(Vector3 target, string vfxKey, UnityAction onComplete)
    {
        UIEffectManager.Instance.AbilityGroupEffect(vfxKey, target, onComplete);
    }

    public void ShowDamageGlobal(string vfxKey, UnityAction onComplete)
    {
        Vector3 targetPosition = Vector3.zero + (new Vector3(0, 0.2f, 0));
        UIEffectManager.Instance.AbilityGroupEffect(vfxKey, targetPosition, onComplete);
    }

    public void ShowIconPassiveActivate(CardUIData target, AbilityFeedback abilityFeedback)
    {
        CardUI card = BoardManager.Instance.GetTokenByUniqueID(target.UniqueID);
        if (card != null)
        {
            card.PassiveActivate(abilityFeedback);
        }
    }

    public void ShowBoostEffectOnMinion(CardUIData target, UnityAction onComplete)
    {
        CardUI card = BoardManager.Instance.GetTokenByUniqueID(target.UniqueID);
        if (card != null)
        {
            SoundManager.PlaySFX(SoundManager.SFX.Buff_Sound);
            UIEffectManager.Instance.SpawnVFX(CardVFXPool.VFXType.Status_PowerUp, card.transform.position + new Vector3(0, 0.05f, 0), onComplete);
        }
        else
        {
            Debug.LogErrorFormat("ShowBoostEffectOnMinion: not found CardUI. id:{0}", target.UniqueID);
            if (onComplete != null)
            {
                onComplete.Invoke();
            }
        }
    }

    public void ShowWeakenEffectOnMinion(CardUIData target, UnityAction onComplete)
    {
        CardUI card = BoardManager.Instance.GetTokenByUniqueID(target.UniqueID);
        if (card != null)
        {
            SoundManager.PlaySFX(SoundManager.SFX.Debuff_Sound);
            UIEffectManager.Instance.SpawnVFX(CardVFXPool.VFXType.Status_PowerDown, card.transform.position + new Vector3(0, 0.05f, 0), onComplete, null);
        }
        else
        {
            Debug.LogErrorFormat("ShowWeakenEffectOnMinion: not found CardUI. id:{0}", target.UniqueID);
            if (onComplete != null)
            {
                onComplete.Invoke();
            }
        }
    }

    public void ShowDirectionChangeEffectOnMinion(CardUIData target, UnityAction onComplete)
    {
        CardUI card = BoardManager.Instance.GetTokenByUniqueID(target.UniqueID);
        if (card != null)
        {
            SoundManager.PlaySFX(SoundManager.SFX.Arrow_Changed);
            UIEffectManager.Instance.SpawnVFX(CardVFXPool.VFXType.TokenArrowChange, card.transform.position + new Vector3(0, 0.05f, 0), onComplete);
        }
        else
        {
            Debug.LogErrorFormat("ShowDirectionChangeEffectOnMinion: not found CardUI. id:{0}", target.UniqueID);
            if (onComplete != null)
            {
                onComplete.Invoke();
            }
        }
    }

    public void ShowLinkOnSlot(int slotID, List<CardDirectionType> dirList, UnityAction onComplete)
    {
        CardUI token = BoardManager.Instance.GetTokenBySlotID(slotID);
        if (token != null)
        {
            PlayerIndex owner = token.Data.Owner;
            BoardManager.Instance.GetSlotHolder(slotID).ShowLink(dirList, owner);
        }
        else
        {
            BoardManager.Instance.GetSlotHolder(slotID).ShowLink(new List<CardDirectionType>(), PlayerIndex.One);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ShowFreezeOnMinion(CardUI card, UnityAction onComplete)
    {
        UIEffectManager.Instance.SpawnVFX(CardVFXPool.VFXType.Status_FreezeIn, card, card.transform.position, onComplete, true);
    }

    public void HideFreezeOnMinion(CardUI card, UnityAction onComplete)
    {
        UIEffectManager.Instance.SpawnVFX(CardVFXPool.VFXType.Status_FreezeOut, card, card.transform.position, onComplete, true);
    }

    public void ShowDarkMatterOnMinion(CardUI card, UnityAction onComplete)
    {
        UIEffectManager.Instance.SpawnVFX(CardVFXPool.VFXType.Status_DarkMatterIn_Token, card, card.transform.position, onComplete, true);
    }

    public void HideDarkMatterOnMinion(CardUI card, UnityAction onComplete)
    {
        UIEffectManager.Instance.SpawnVFX(CardVFXPool.VFXType.Status_DarkMatterOut_Token, card, card.transform.position, onComplete, true);
    }

    public void ShowSilenceOnMinion(CardUI card, UnityAction onComplete)
    {
        //UIEffectManager.Instance.SpawnVFX(CardVFXPool.VFXType.Status_SilenceIn, card, card.transform.position, onComplete, true);

        onComplete?.Invoke();
    }

    public void HideSilenceOnMinion(CardUI card, UnityAction onComplete)
    {
        //UIEffectManager.Instance.SpawnVFX(CardVFXPool.VFXType.Status_SilenceOut, card, card.transform.position, onComplete, true);

        onComplete?.Invoke();
    }

    public void ShowTauntOnMinion(CardUI card, UnityAction onComplete)
    {
        UIEffectManager.Instance.SpawnVFX(CardVFXPool.VFXType.Status_TauntIn, card, card.transform.position, onComplete, true);
    }

    public void ShowAuraOnMinion(CardUI card, UnityAction onComplete)
    {
        UIEffectManager.Instance.SpawnVFX(CardVFXPool.VFXType.Status_AuraIn, card, card.transform.position, onComplete, true);
    }

    public void ShowLastWishOnMinion(CardUI card, UnityAction onComplete)
    {
        UIEffectManager.Instance.SpawnLastwishVFX(card, card.transform.position, onComplete);
    }

    public void ShowLockSlotBySlotID(int index, int duration, PlayerIndex owner, UnityAction onComplete)
    {
        BoardManager.Instance.SetLockDuration(index, duration, owner);
        BoardManager.Instance.ShowLockSlotBySlotID(index, onComplete);
    }

    public void SetLockDuration(int index, int value, PlayerIndex owner)
    {
        BoardManager.Instance.SetLockDuration(index, value, owner);
    }

    public void HideLockSlotBySlotID(int index, UnityAction onComplete)
    {
        BoardManager.Instance.SetLockDuration(index, -1, PlayerIndex.One);
        BoardManager.Instance.HideLockSlotBySlotID(index, onComplete);
    }

    public void ShowDarkMatterBySlotID(int index, PlayerIndex owner, UnityAction onComplete)
    {
        BoardManager.Instance.ShowDarkMatterBySlotID(index, owner, onComplete);
    }

    public void HideDarkMatterBySlotID(int index, PlayerIndex owner, UnityAction onComplete)
    {
        BoardManager.Instance.HideDarkMatterBySlotID(index, owner, onComplete);
    }

    public void ShowEffectAcitvating(int uniqueId, UnityAction onComplete)
    {
        CardUI card;
        if (FindCardByUniqueID(uniqueId, out card))
        {
            SoundManager.PlaySFX(SoundManager.SFX.Ability_Activating);
            card.EffectActivating(true);
        }
    }

    public void HideEffectAcitvating(int uniqueId, UnityAction onComplete)
    {
        CardUI card;
        if (FindCardByUniqueID(uniqueId, out card))
        {
            card.EffectActivating(false);
        }
    }

    public void RequestShowEffectActivating(int uniqueId)
    {
        UIShowEffectCardActivatingCmd cmd = new UIShowEffectCardActivatingCmd(uniqueId, "ShowEffectActivating");
        UIQueue.Instance.Add(cmd);
    }

    public void RequestHideEffectActivating(int uniqueId)
    {
        UIHideEffectCardActivatingCmd cmd = new UIHideEffectCardActivatingCmd(uniqueId, "HideEffectActivating");
        UIQueue.Instance.Add(cmd);
    }

    public void RequestShowDestroyAllCard(PlayerIndex playerIndex)
    {
        UIDestroyAllCardCmd cmd = new UIDestroyAllCardCmd(playerIndex, "DestroyAllCard");
        UIQueue.Instance.Add(cmd);
    }

    public void RequestShowHeroDeadCard(PlayerIndex playerIndex)
    {
        UIHeroDeadCmd cmd = new UIHeroDeadCmd(playerIndex, "HeroDead");
        UIQueue.Instance.Add(cmd);
    }
    #endregion

    #region 
    public void ShowTauntAnimation(PlayerIndex playerIndex, HeroVoice voiceKey, UnityAction onComplete = null)
    {
        BaseHeroAnimation.HeroAnimationState state = BaseHeroAnimation.GetStateFromVoiceIndex(voiceKey);
        ShowTauntAnimation(playerIndex, state, onComplete);
    }

    public void ShowTauntAnimation(PlayerIndex playerIndex, BaseHeroAnimation.HeroAnimationState state, UnityAction onComplete = null)
    {
        if (GameManager.Instance.IsLocalPlayer(playerIndex))
        {
            UIPlayerBottom.PlayAnimationHeroDialog(playerIndex ,state, onComplete);
        }
        else
        {
            UIPlayerTop.PlayAnimationHeroDialog(playerIndex, state, onComplete);
        }
    }

    public void ShowTauntTalkDialog(PlayerIndex playerIndex, string dialog, float duration = 3.0f)
    {
        TauntTalkController.ShowDialog(playerIndex, dialog, duration);
    }

    public void HideTauntChoices()
    {
        TauntTalkController.HideChoices();
        TauntTalkController.SetClickToOpen();
    }

    public void EnableTauntBTN(bool isEnable)
    {
        TauntTalkController.EnableBTN(isEnable);
    }

    public void ToggleTauntTalk()
    {
        // Ignore mute when in story mode.
        if (GameManager.Instance.IsStoryMode()) return;

        if (GameManager.Instance.Phase <= GamePhase.ReHand)
        {
            return;
        }
        GameManager.Instance.ActionSetMessageMute(!GameManager.Instance.IsMessageMute);

        if (GameManager.Instance.IsMessageMute)
        {
            Icon_Mute.gameObject.SetActive(true);
            Icon_Unmute.gameObject.SetActive(false);

            Icon_Mute.GetComponent<CanvasGroup>().alpha = 1.0f;
            Icon_Mute.GetComponent<CanvasGroup>().DOFade(0, 1.0f).SetEase(Ease.InExpo);
        }
        else
        {
            Icon_Unmute.gameObject.SetActive(true);
            Icon_Mute.gameObject.SetActive(false);

            Icon_Unmute.GetComponent<CanvasGroup>().alpha = 1.0f;
            Icon_Unmute.GetComponent<CanvasGroup>().DOFade(0, 1.0f).SetEase(Ease.InExpo);
        }
    }
    #endregion

    #region Tutorial Popup
    public UITutorialManager GetUITutorialManager()
    {
        return UITutorialManager;
    }

    public void ShowTutorialMap(UnityAction onComplete)
    {
        UITutorialMap.gameObject.SetActive(true);
        StartCoroutine(GameHelper.ExecuteIEnumerator(UITutorialMap.ShowFirstTimeAnimation(), delegate
        {
            UITutorialMap.gameObject.SetActive(false);
            onComplete?.Invoke();
        }));
    }
    #endregion

    #region Methods
    public bool FindCardByUniqueID(int uniqueId, out CardUI cardUI)
    {
        if (uniqueId >= 0)
        {

            CardUIManager.PlayerPosition playerPosition;
            if (CardUIManager.Instance.FindCardUIByID(uniqueId, out cardUI, out playerPosition))
            {
                // find on hand
                return true;
            }
            else if (BoardManager.Instance.FindTokenById(uniqueId, out cardUI))
            {
                // find on board
                return true;
            }
            else
            {
                return false;
            }

        }
        else
        {
            cardUI = null;
            return false;
        }


    }
    #endregion

    #region Scene Animation
    public void Tutorial_PlayIntro(UnityAction onComplete = null)
    {
        LightingController.AdjustSunLightIntensity(0.1f);
        SceneAnimation.Play(SceneAnimation.AnimationName.Tutorial_PackOpen, delegate
        {
            LightingController.AdjustSunLightIntensity();
            onComplete?.Invoke();
        });
    }

    public void Tutorial_PlayCardToDeck(UnityAction onComplete = null)
    {
        SceneAnimation.Play(SceneAnimation.AnimationName.Tutorial_Intro, onComplete);
    }
    #endregion

    #region Feedback Event
    public void SendPlayerHoverCard(int uniqueId, bool isSelect)
    {
        if (GameManager.Instance.IsNetworkPlay())
        {
            GameManager.Instance.PhotonRequestHoverCard(uniqueId, isSelect);
        }
    }

    public void ReceivePlayerHoverCard(int uniqueId, bool isSelect)
    {
        if (_cardEnemyHover != null)
        {
            _cardEnemyHover.ShowEnemyHover(false);
            _cardEnemyHover = null;
        }

        CardUI card;
        if (FindCardByUniqueID(uniqueId, out card))
        {
            _cardEnemyHover = card;

            _cardEnemyHover.ShowEnemyHover(isSelect);
        }


    }
    #endregion
}
