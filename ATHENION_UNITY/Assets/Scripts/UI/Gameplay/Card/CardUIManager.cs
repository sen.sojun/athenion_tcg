﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Text;
using UnityEngine.EventSystems;
using DG.Tweening;
namespace Karamucho.UI
{
    public class CardUIManager : MonoSingleton<CardUIManager>
    {
        #region Enum
        public enum PlayerPosition
        {
            Top,
            Bottom
        }

        public enum HighlightHand { AP, HP, SOUL, Armor, Direction }
        #endregion

        #region Public Properties
        [Header("Reference Top")]
        public CardHolder CardHolderTop;
        public Transform CardHandTopParent;
        public Transform TopDeck;
        public Transform TopGrave;
        public Transform TopDiscardTarget;

        [Header("Reference Bottom")]
        public CardHolder CardHolderBottom;
        public Transform CardHandBottomParent;
        public Transform BottomDeck;
        public Transform BottomGrave;
        public Transform BottomDiscardTarget;

        [Header("Redraw UI")]
        public UIRedrawHolder UIRedrawHolder;
        public List<int> RedrawList { get { return _redrawList; } }

        [Header("Reference")]
        public Transform ShowCardAreaTop;
        public Transform ShowCardAreaBottom;
        public Transform CenterPosition;
        public Transform TopPosition;
        public Transform BotPosition;
        public Transform HandShow;
        public Transform HandHide;

        [Header("Prefab")]
        public GameObject CardPrefab;
        public GameObject CardPrefab_Lose;

        public bool IsHilight { get; private set; }
        public bool IsSelected { get; private set; }
        public bool IsDown { get; private set; }
        public bool IsUse { get { return (IsHilight && IsSelected && IsDown); } }
        public List<CardUI> CardTopList { get { return _cardTopList; } }
        public List<CardUI> CardBottomList { get { return _cardBottomList; } }


        [Header("Test")]
        public bool DrawTop;
        public bool DrawBottom;
        public bool DiscardTop;
        public bool DiscardBottom;
        private List<int> _redrawList = new List<int>();
        private List<CardUI> _cardTopList = new List<CardUI>();
        private List<CardUI> _cardBottomList = new List<CardUI>();
        #endregion

        #region Private Properties
        private StringBuilder _debugText;
        private CardUI _currentCardHilight;
        private CardUI _currentCardSelected;
        private int _dragSoundIndex = -1;
        private RaycastHit _hit;
        private Vector3 _downPosition;
        private Vector3 _useOffset;
        private bool _isShowHand = false;
        private float _misclickTime = 0.07f;
        private float _downTime = 0.0f;
        private int _redrawMax = 0;
        private bool _cardIsAnimatePosition = false;

        #endregion

        #region Methods
        private void Start()
        {
            _useOffset = new Vector3(0, Screen.dpi * 0.25f, 0);
            SetHandMarkerPosition();
        }

        public void Update()
        {
            OnRayCast();

            // Test
            //DebugDrawCard();

        }

        private void DebugDrawCard()
        {
            if (DataManager.Instance.GetServerSetting() != ATNVersion.Development) return;

            if (DrawTop)
            {
                DrawTop = false;
                DrawCard(null, PlayerPosition.Top, null);
            }
            if (DrawBottom)
            {
                DrawBottom = false;
                DrawCard(null, PlayerPosition.Bottom, null);
            }
            if (DrawTop)
            {
                DrawTop = false;
                DrawCard(null, PlayerPosition.Top, null);
            }
            if (DrawBottom)
            {
                DrawBottom = false;
                DrawCard(null, PlayerPosition.Bottom, null);
            }
            if (DiscardTop)
            {
                DiscardTop = false;
                if (_cardTopList != null && _cardTopList.Count > 1)
                {
                    List<CardUI> list = new List<CardUI>();
                    list.Add(_cardTopList[0]);
                    list.Add(_cardTopList[1]);
                    DiscardCard(PlayerPosition.Top, list);
                }
            }
            if (DiscardBottom)
            {
                DiscardBottom = false;
                if (_cardBottomList != null && _cardBottomList.Count > 0)
                {
                    List<CardUI> list = new List<CardUI>();
                    list.Add(_cardBottomList[0]);
                    DiscardCard(PlayerPosition.Bottom, list);
                }
            }

            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                ShowHandCard();
            }
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                HideHandCard();
            }
        }

        private void MisClickTimer()
        {
            if (IsDown && _downTime < _misclickTime)
            {
                _downTime += Time.deltaTime;
            }
        }

        private void ResetMisclickTime()
        {
            _downTime = 0.0f;
        }

        public CardUI GetCurrentCard()
        {
            if (_currentCardSelected != null)
            {
                return _currentCardSelected;
            }
            else
            {
                Debug.LogError("Current card selected is invalid.");
                return null;
            }

        }

        public CardUI GetCardUI(int uniqueID)
        {
            CardUI card;
            PlayerPosition owner;
            if (FindCardUIByID(uniqueID, out card, out owner))
            {
                return card;
            }
            else
            {
                Debug.LogError("Cannot find card in hand: " + uniqueID.ToString());
                return null;
            }
        }

        public PlayerPosition GetPlayerPosition(int uniqueID)
        {
            CardUI card;
            PlayerPosition owner;
            if (FindCardUIByID(uniqueID, out card, out owner))
            {
                return owner;
            }
            else
            {
                Debug.LogError("Cannot find card in hand: " + uniqueID.ToString());
                return owner;
            }
        }

        public CardUI GetCurrentCardHighlight()
        {
            return _currentCardHilight;
        }

        private void OnRayCast()
        {
            if (Camera.main != null)
            {
                if (Input.mousePosition.x < 0
                    || Input.mousePosition.x >= Screen.width
                    || Input.mousePosition.y < 0
                    || Input.mousePosition.y >= Screen.height
                )
                {
                    return; // out of screen.
                }

                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out _hit) && IsDown)
                {
                    // Trying to Drag card
                    if (GetCardIsUsing() && _currentCardSelected == null && !UIManager.Instance.IsSelectionSlotShowing)
                    {
                        if (_currentCardHilight != null)
                        {
                            if (UIQueue.Instance.IsReady && CommandQueue.Instance.IsReady)
                            {
                                if (GameManager.Instance.IsCanUseCard(_currentCardHilight.Data.UniqueID))
                                {
                                    CardIsSelected(_currentCardHilight);
                                }

                                CancelHilight();
                                UIManager.Instance.ShowDetailCardUI(false);
                            }
                        }
                    }
                }
            }
        }

        public bool IsSelectedCard()
        {
            return IsDown && IsHilight && !IsDragCard();
        }

        public bool GetSelectedCardID(out string fullID)
        {
            if (IsSelectedCard())
            {
                fullID = _currentCardHilight.Data.FullID;
                return true;
            }

            fullID = "";
            return false;
        }

        public bool IsDragCard()
        {
            return (_currentCardSelected != null);
        }

        public bool GetDragCardID(out string fullID)
        {
            if (IsDragCard())
            {
                fullID = _currentCardSelected.Data.FullID;
                return true;
            }

            fullID = "";
            return false;
        }

        /// <summary>
        /// Use to draw card.
        /// </summary>
        /// <param name="playerPosition">Player position (top, bottom).</param>
        public void DrawCard(CardUIData carduiData, PlayerPosition playerPosition, UnityAction onComplete)
        {
            switch (playerPosition)
            {
                case PlayerPosition.Top:
                    {
                        GameObject objTop = Instantiate(CardPrefab, CardHandTopParent.transform);
                        _cardTopList.Add(objTop.GetComponent<CardUI>());
                        objTop.GetComponent<CardUI>().IsBackCard = true;
                        objTop.GetComponent<CardUI>().InitCard(carduiData, CardUI.UIMode.OBJECT);
                        objTop.transform.position = TopDeck.position;
                        objTop.transform.localScale = TopDeck.localScale;
                        objTop.transform.rotation = TopDeck.rotation;
                        CardHolderTop.CreateCardSlot();

                        SoundManager.PlaySFX(SoundManager.SFX.Draw_Card);
                        PlayAnimationCardSort(playerPosition, onComplete);
                    }
                    break;
                case PlayerPosition.Bottom:
                    {
                        GameObject objBottom = Instantiate(CardPrefab, CardHandBottomParent.transform);
                        _cardBottomList.Add(objBottom.GetComponent<CardUI>());
                        objBottom.GetComponent<CardUI>().InitCard(carduiData, CardUI.UIMode.OBJECT);
                        objBottom.transform.position = BottomDeck.position;
                        objBottom.transform.localScale = BottomDeck.localScale;
                        objBottom.transform.rotation = BottomDeck.rotation;
                        CardHolderBottom.CreateCardSlot();

                        SoundManager.PlaySFX(SoundManager.SFX.Draw_Card);
                        PlayAnimationShowCard(playerPosition, objBottom.GetComponent<CardUI>(),
                            delegate
                            {
                                //WaitQueue(0.1f);
                                PlayAnimationCardSort(playerPosition, onComplete);
                            });
                    }
                    break;
            }
        }

        public void DrawFastCard(CardUIData carduiData, PlayerPosition playerPosition, UnityAction onComplete)
        {
            switch (playerPosition)
            {
                case PlayerPosition.Top:
                    {
                        GameObject objTop = Instantiate(CardPrefab, CardHandTopParent.transform);
                        _cardTopList.Add(objTop.GetComponent<CardUI>());
                        objTop.GetComponent<CardUI>().IsBackCard = true;
                        objTop.GetComponent<CardUI>().InitCard(carduiData, CardUI.UIMode.OBJECT);
                        //objTop.GetComponent<CardUI>().SetStencilRef(stencil);
                        objTop.transform.position = TopDeck.position;
                        objTop.transform.rotation = TopDeck.rotation;
                        CardHolderTop.CreateCardSlot();
                        PlayAnimationCardSort(playerPosition, onComplete, 0.2f);
                        SoundManager.PlaySFX(SoundManager.SFX.Draw_Card);
                    }
                    break;

                case PlayerPosition.Bottom:
                    {
                        GameObject objBottom = Instantiate(CardPrefab, CardHandBottomParent.transform);
                        _cardBottomList.Add(objBottom.GetComponent<CardUI>());
                        objBottom.GetComponent<CardUI>().InitCard(carduiData, CardUI.UIMode.OBJECT);
                        objBottom.transform.position = BottomDeck.position;
                        objBottom.transform.rotation = BottomDeck.rotation;
                        CardHolderBottom.CreateCardSlot();

                        SoundManager.PlaySFX(SoundManager.SFX.Draw_Card);
                        //WaitQueue(0.1f);
                        PlayAnimationCardSort(playerPosition, onComplete, 0.2f);
                    }
                    break;
            }
        }

        public void DrawCardToDestroy(CardUIData cardData, PlayerPosition playerPosition, UnityAction onComplete)
        {
            switch (playerPosition)
            {
                case PlayerPosition.Top:
                    GameObject objTop = Instantiate(CardPrefab, CardHandTopParent.transform);
                    CardUI cardTop = objTop.GetComponent<CardUI>();
                    _cardTopList.Add(cardTop);
                    cardTop.IsBackCard = true;
                    cardTop.InitCard(cardData, CardUI.UIMode.OBJECT);
                    //objTop.GetComponent<CardUI>().SetStencilRef(stencil);
                    objTop.transform.position = TopDeck.position;
                    objTop.transform.rotation = TopDeck.rotation;
                    CardHolderTop.CreateCardSlot();

                    PlayAnimationShowCard(playerPosition, cardTop, delegate
                    {
                        cardTop.AnimationDestroyCardFromDraw(true, onComplete);
                    });
                    DetachCard(PlayerPosition.Top, cardTop);


                    SoundManager.PlaySFX(SoundManager.SFX.Draw_Card);
                    break;

                case PlayerPosition.Bottom:
                    GameObject objBottom = Instantiate(CardPrefab, CardHandBottomParent.transform);
                    CardUI cardBottom = objBottom.GetComponent<CardUI>();
                    _cardBottomList.Add(cardBottom);
                    cardBottom.InitCard(cardData, CardUI.UIMode.OBJECT);
                    objBottom.transform.position = BottomDeck.position;
                    objBottom.transform.rotation = BottomDeck.rotation;
                    CardHolderBottom.CreateCardSlot();

                    PlayAnimationShowCard(playerPosition, cardBottom, delegate
                    {
                        cardBottom.AnimationDestroyCardFromDraw(true, onComplete);
                    });
                    DetachCard(PlayerPosition.Bottom, cardBottom);


                    SoundManager.PlaySFX(SoundManager.SFX.Draw_Card);
                    //WaitQueue(0.1f);

                    break;
            }
        }

        public void DrawCardFromGrave(CardUIData carduiData, PlayerPosition playerPosition, UnityAction onComplete)
        {
            switch (playerPosition)
            {
                case PlayerPosition.Top:
                    {
                        GameObject objTop = Instantiate(CardPrefab, CardHandTopParent.transform);
                        _cardTopList.Add(objTop.GetComponent<CardUI>());
                        objTop.GetComponent<CardUI>().IsBackCard = true;
                        objTop.GetComponent<CardUI>().InitCard(carduiData, CardUI.UIMode.OBJECT);
                        //objTop.GetComponent<CardUI>().SetStencilRef(stencil);
                        objTop.transform.position = TopGrave.position;
                        objTop.transform.localScale = TopGrave.localScale;
                        objTop.transform.rotation = TopGrave.rotation;
                        CardHolderTop.CreateCardSlot();

                        SoundManager.PlaySFX(SoundManager.SFX.Draw_Card);
                        //PlaySFXQueue(SoundManager.SFX.Draw_Card, false, false);
                        PlayAnimationCardSort(playerPosition, onComplete);
                    }
                    break;
                case PlayerPosition.Bottom:
                    {
                        GameObject objBottom = Instantiate(CardPrefab, CardHandBottomParent.transform);
                        _cardBottomList.Add(objBottom.GetComponent<CardUI>());
                        objBottom.GetComponent<CardUI>().InitCard(carduiData, CardUI.UIMode.OBJECT);
                        objBottom.transform.position = BottomGrave.position;
                        objBottom.transform.localScale = BottomGrave.localScale;
                        objBottom.transform.rotation = BottomGrave.rotation;
                        CardHolderBottom.CreateCardSlot();

                        SoundManager.PlaySFX(SoundManager.SFX.Draw_Card);
                        //PlaySFXQueue(SoundManager.SFX.Draw_Card, false, false);
                        PlayAnimationShowCard(playerPosition, objBottom.GetComponent<CardUI>(),
                            delegate
                            {
                                //WaitQueue(0.1f);
                                PlayAnimationCardSort(playerPosition, onComplete);
                            });
                    }
                    break;
            }
        }

        public void DestroyAllCard(PlayerIndex playerIndex, UnityAction onComplete)
        {
            List<UnityAction> actionList = new List<UnityAction>();

            SoundManager.PlaySFX(SoundManager.SFX.End_Game_Hand_Card_Burn);

            if (GameManager.Instance.IsLocalPlayer(playerIndex))
            {
                if (_cardBottomList.Count <= 0)
                {
                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                    return;
                }
                for (int i = 0; i < _cardBottomList.Count; i++)
                {
                    CardUI card = _cardBottomList[i];
                    actionList.Add(delegate
                    {
                        if (i < _cardBottomList.Count - 1)
                        {
                            card.AnimationDestroyCardFromDraw(false, null);
                        }
                        else
                        {
                            card.AnimationDestroyCardFromDraw(false, onComplete);
                        }

                        DetachCard(PlayerPosition.Bottom, card);
                    });
                }
            }
            else
            {
                if (_cardTopList.Count <= 0)
                {
                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                    return;
                }
                for (int i = 0; i < _cardTopList.Count; i++)
                {
                    CardUI card = _cardTopList[i];
                    actionList.Add(delegate
                    {
                        if (i < _cardTopList.Count - 1)
                        {
                            card.AnimationDestroyCardFromDraw(false, null);
                        }
                        else
                        {
                            card.AnimationDestroyCardFromDraw(false, onComplete);
                        }

                        DetachCard(PlayerPosition.Top, card);
                    });
                }
            }

            foreach (UnityAction item in actionList)
            {
                item.Invoke();
            }

            /*
            if (onComplete != null)
            {
                onComplete.Invoke();
            }
            */
        }

        public void DrawNoMoreCardToDestroy(PlayerIndex playerIndex, UnityAction onComplete)
        {
            PlayerPosition playerPosition;
            if (GameManager.Instance.IsLocalPlayer(playerIndex))
            {
                playerPosition = PlayerPosition.Bottom;
            }
            else
            {
                playerPosition = PlayerPosition.Top;
            }

            switch (playerPosition)
            {
                case PlayerPosition.Top:
                    {
                        GameObject objTop = Instantiate(CardPrefab_Lose, CardHandTopParent.transform);
                        CardUI cardTop = objTop.GetComponent<CardUI>();
                        _cardTopList.Add(cardTop);
                        cardTop.InitAnimator();
                        cardTop.SetUIMode(CardUI.UIMode.UI);
                        cardTop.AnimationToCard();
                        //objTop.GetComponent<CardUI>().SetStencilRef(stencil);
                        objTop.transform.position = TopDeck.position;
                        objTop.transform.rotation = TopDeck.rotation;
                        CardHolderTop.CreateCardSlot();

                        SoundManager.PlaySFX(SoundManager.SFX.Draw_Skull);
                        PlayAnimationShowCard(playerPosition, cardTop, delegate
                        {
                            SoundManager.PlaySFX(SoundManager.SFX.Draw_Skull_Burn);
                            cardTop.AnimationDestroyCardFromDraw(true, onComplete);
                        });
                        DetachCard(PlayerPosition.Top, cardTop);
                    }
                    break;

                case PlayerPosition.Bottom:
                    {
                        GameObject objBottom = Instantiate(CardPrefab_Lose, CardHandBottomParent.transform);
                        CardUI cardBottom = objBottom.GetComponent<CardUI>();
                        _cardBottomList.Add(cardBottom);
                        cardBottom.InitAnimator();
                        cardBottom.SetUIMode(CardUI.UIMode.UI);
                        cardBottom.AnimationToCard();
                        objBottom.transform.position = BottomDeck.position;
                        objBottom.transform.rotation = BottomDeck.rotation;
                        CardHolderBottom.CreateCardSlot();

                        SoundManager.PlaySFX(SoundManager.SFX.Draw_Skull);
                        PlayAnimationShowCard(playerPosition, cardBottom, delegate
                        {
                            SoundManager.PlaySFX(SoundManager.SFX.Draw_Skull_Burn);
                            cardBottom.AnimationDestroyCardFromDraw(true, onComplete);
                        }
                        , 0.5f);
                        DetachCard(PlayerPosition.Bottom, cardBottom);
                        //WaitQueue(0.1f);
                    }
                    break;
            }
        }

        [System.Obsolete]
        public void DrawCardOnly(CardUIData cardData, UnityAction onComplete)
        {
            GameObject objBottom = Instantiate(CardPrefab, CardHandBottomParent.transform);
            _cardBottomList.Add(objBottom.GetComponent<CardUI>());
            objBottom.GetComponent<CardUI>().InitCard(cardData, CardUI.UIMode.OBJECT);
            objBottom.transform.position = BottomDeck.position;
            objBottom.transform.localScale = BottomDeck.localScale;
            objBottom.transform.rotation = BottomDeck.rotation;
            CardHolderBottom.CreateCardSlot();


            SoundManager.PlaySFX(SoundManager.SFX.Draw_Card);
            //WaitQueue(0.1f);
            // Play animation to redraw slot.
            //UIRedrawHolder.MoveCardToEmptyMarker(objBottom.GetComponent<CardUI>(), onComplete);

        }

        public CardUI SpawnCard(CardUIData battleCardData, bool isAttach = true)
        {
            GameObject objCard = Instantiate(CardPrefab, CenterPosition);
            CardUI card = objCard.GetComponent<CardUI>();

            if (isAttach)
            {
                if (GameManager.Instance.IsLocalPlayer(battleCardData.Owner))
                {
                    // bottom
                    AttachCard(PlayerPosition.Bottom, card);
                    card.transform.position = BotPosition.position;
                    card.transform.rotation = BotPosition.rotation;
                }
                else
                {
                    // top
                    AttachCard(PlayerPosition.Top, card);
                    card.transform.position = TopPosition.position;
                    card.transform.rotation = TopPosition.rotation;
                }
            }

            UIEffectManager.Instance.SpawnVFX(CardVFXPool.VFXType.CardDiscard, card, transform.position, null, false);
            card.IsBackCard = false;
            card.InitCard(battleCardData, CardUI.UIMode.OBJECT);
            //card.SetStencilRef(stencil);

            return card;
        }

        public void MoveTokenToHand(PlayerPosition playerPosition, CardUI card, UnityAction onComplete)
        {
            switch (playerPosition)
            {
                case PlayerPosition.Top:
                    {
                        _cardTopList.Add(card);
                        card.transform.SetParent(CardHandTopParent);
                        CardHolderTop.CreateCardSlot();
                        {
                            card.IsBackCard = true;
                            card.CardBackUIObject.SetGlow(false);
                            UIEffectManager.Instance.SpawnVFX(CardVFXPool.VFXType.CardDiscard, card, transform.position, null, false);
                            card.AnimationToBack();
                        }
                    }
                    break;
                case PlayerPosition.Bottom:
                    {
                        _cardBottomList.Add(card);
                        card.transform.SetParent(CardHandBottomParent);
                        CardHolderBottom.CreateCardSlot();
                        //WaitQueue(0.1f);
                        {
                            card.IsBackCard = false;
                            card.CardBackUIObject.SetGlow(false);
                            UIEffectManager.Instance.SpawnVFX(CardVFXPool.VFXType.CardDiscard, card, transform.position, null, false);
                            card.AnimationToCard();
                        }
                    }
                    break;
            }

            PlayAnimationShowCard(playerPosition, card.GetComponent<CardUI>(), delegate ()
            {
                PlayAnimationCardSort(playerPosition, onComplete);
                SoundManager.PlaySFX(SoundManager.SFX.Draw_Card);
            });
        }

        public void DiscardCard(PlayerPosition playerPosition, List<CardUI> discardList)
        {
            switch (playerPosition)
            {
                case PlayerPosition.Top:
                    {
                        bool isAdd = true;
                        foreach (CardUI item in discardList)
                        {
                            item.AnimationDiscard(TopDiscardTarget, isAdd);
                            isAdd = false;
                        }
                        foreach (CardUI item in discardList)
                        {
                            CardHolderTop.RemoveSlotAtIndex(_cardTopList.IndexOf(item));
                            _cardTopList.Remove(item);
                        }
                        PlayAnimationCardSort(PlayerPosition.Top, null);
                        break;
                    }
                case PlayerPosition.Bottom:
                    {
                        bool isAdd = true;
                        foreach (CardUI item in discardList)
                        {
                            item.AnimationDiscard(BottomDiscardTarget, isAdd);
                            isAdd = false;
                        }
                        foreach (CardUI item in discardList)
                        {
                            CardHolderBottom.RemoveSlotAtIndex(_cardBottomList.IndexOf(item));
                            _cardBottomList.Remove(item);
                        }
                        PlayAnimationCardSort(PlayerPosition.Bottom, null);
                    }
                    break;
            }
        }

        public void DiscardCard(CardUIData card, UnityAction onComplete)
        {
            if (card != null)
            {
                if (GameManager.Instance.IsLocalPlayer(card.Owner))
                {
                    bool isAdd = true;
                    CardUI item = _cardBottomList.Find(x => x.GetUniqueID() == card.UniqueID);
                    if (item != null)
                    {
                        item.AnimationDiscard(BottomDiscardTarget, isAdd, 0.5f, false);
                        isAdd = false;
                        CardHolderBottom.RemoveSlotAtIndex(_cardBottomList.IndexOf(item));
                        _cardBottomList.Remove(item);
                        PlayAnimationCardSort(PlayerPosition.Bottom, onComplete);
                        return;
                    }
                }
                else
                {
                    bool isAdd = true;
                    CardUI item = _cardTopList.Find(x => x.GetUniqueID() == card.UniqueID);
                    if (item != null)
                    {
                        item.AnimationDiscard(TopDiscardTarget, isAdd, 0.5f, false);
                        isAdd = false;
                        CardHolderTop.RemoveSlotAtIndex(_cardTopList.IndexOf(item));
                        _cardTopList.Remove(item);
                        PlayAnimationCardSort(PlayerPosition.Top, onComplete);
                        return;
                    }
                }
            }

            if (onComplete != null)
            {
                onComplete.Invoke();
            }
        }

        public void RemoveCard(CardUIData card, UnityAction onComplete)
        {
            if (card != null)
            {
                SoundManager.PlaySFX(SoundManager.SFX.End_Game_Hand_Card_Burn);

                if (GameManager.Instance.IsLocalPlayer(card.Owner))
                {
                    bool isAdd = true;
                    CardUI item = _cardBottomList.Find(x => x.GetUniqueID() == card.UniqueID);
                    if (item != null)
                    {
                        item.AnimationRemoveCard(BottomDiscardTarget, isAdd, 0.5f, true);
                        isAdd = false;
                        CardHolderBottom.RemoveSlotAtIndex(_cardBottomList.IndexOf(item));
                        _cardBottomList.Remove(item);
                        PlayAnimationCardSort(PlayerPosition.Bottom, onComplete);
                        return;
                    }
                }
                else
                {
                    bool isAdd = true;
                    CardUI item = _cardTopList.Find(x => x.GetUniqueID() == card.UniqueID);
                    if (item != null)
                    {
                        item.AnimationRemoveCard(TopDiscardTarget, isAdd, 0.5f, true);
                        isAdd = false;
                        CardHolderTop.RemoveSlotAtIndex(_cardTopList.IndexOf(item));
                        _cardTopList.Remove(item);
                        PlayAnimationCardSort(PlayerPosition.Top, onComplete);
                        return;
                    }
                }
            }

            if (onComplete != null)
            {
                onComplete.Invoke();
            }
        }

        public void DetachCard(PlayerPosition playerPosition, CardUI card)
        {
            int index = 0;
            switch (playerPosition)
            {
                case PlayerPosition.Top:
                    index = _cardTopList.IndexOf(card);
                    CardHolderTop.RemoveSlotAtIndex(index);
                    _cardTopList.Remove(card);
                    card.transform.SetParent(null);
                    break;
                case PlayerPosition.Bottom:
                    index = _cardBottomList.IndexOf(card);
                    CardHolderBottom.RemoveSlotAtIndex(index);
                    _cardBottomList.Remove(card);
                    card.transform.SetParent(null);
                    break;
            }

        }

        public void AttachCard(PlayerPosition playerPosition, CardUI card)
        {
            switch (playerPosition)
            {
                case PlayerPosition.Top:
                    CardHolderTop.CreateCardSlot();
                    _cardTopList.Add(card);
                    card.transform.SetParent(CardHandTopParent);
                    card.transform.localScale = new Vector3(1, 1, 1);
                    break;
                case PlayerPosition.Bottom:
                    CardHolderBottom.CreateCardSlot();
                    _cardBottomList.Add(card);
                    card.transform.SetParent(CardHandBottomParent);
                    card.transform.localScale = new Vector3(1, 1, 1);
                    break;
            }
        }

        [System.Obsolete]
        public void DropCardToBoard(CardUI card, Vector3 targetPosition, Transform parent)
        {
            card.transform.position = targetPosition;
            card.transform.eulerAngles = new Vector3(90, 0, 0);
            card.transform.SetParent(parent);
        }

        /// <summary>
        /// Spawn Token from player's hand.
        /// </summary>
        /// <param name="uniqueID"></param>
        /// <param name="slotID"></param>
        /// <param name="onComplete"></param>
        public void SpawnTokenToBoard(CardUIData cardData, UnityAction onComplete)
        {
            BoardSlotHolder slot = BoardManager.Instance.GetSlotHolder(cardData.SlotID);
            CardUI card;
            PlayerPosition playerPosition;
            bool isFound = FindCardUIByID(cardData.UniqueID, out card, out playerPosition);

            if (!isFound)
            {
                card = SpawnCard(cardData);
                if (GameManager.Instance.IsLocalPlayer(card.Data.Owner))
                {
                    playerPosition = PlayerPosition.Bottom;
                }
                else
                {
                    playerPosition = PlayerPosition.Top;
                }
            }

            // Use card
            //card.SetInteractable(false); // already lock when use card.
            card.SetUsed(true);
            card.IsBackCard = false;
            card.SetSlotID(cardData.SlotID);

            Vector3 targetPosition = slot.GetHolderPosition() + new Vector3(0, 2.0f, 0);
            float deltaDistance = Vector3.Distance(targetPosition, card.transform.position);
            float speed = 10.0f;
            float moveTime = deltaDistance / speed;

            // Clear
            UseSelectedCard(slot.transform);
            card.TokenUIObject.ShowGlow(false);
            if (UIManager.Instance.IsGraveyardShowing)
            {
                // Don't Autohide on graveyard ui.
            }
            else
            {
                //BoardManager.Instance.HideAllHilight();
                BoardManager.Instance.CancelHilight(); // Hide card details.
            }

            DetachCard(playerPosition, card);
            card.transform.localScale = Vector3.one;
            BoardManager.Instance.AddTokenToList(card);

            // Move Token to Target Position
            Sequence sq = DOTween.Sequence();
            sq.Append(card.transform.DOMove(targetPosition, moveTime).SetEase(Ease.OutExpo));
            sq.Join(card.transform.DORotate(new Vector3(90, 0, 0), moveTime));
            sq.OnComplete(delegate
            {
                // Find card if possible.
                //if (FindCardUIByID(cardData.UniqueID, out card, out playerPosition))
                {
                    card.AnimationToToken();

                    //Re-sort hand
                    PlayAnimationCardSort(playerPosition, null);
                    /*
                    if (GameManager.Instance.IsLocalPlayer(GameManager.Instance.CurrentPlayerIndex))
                    {
                        HideHandCard();
                    }
                    */

                    // Spawn Effect.
                    UIEffectManager.Instance.SpawnVFX(CardVFXPool.VFXType.CardDiscard1, card, transform.position, null, false);
                    SoundManager.PlayMinionSummonVoice(card.Data.BaseID, null); // play spawn sound at beginning of spawn.
                    UIEffectManager.Instance.SpawnSummonVFX(
                          cardData.SpawnEffectKey
                        , card
                        , slot.GetHolderPosition()
                        , delegate ()
                        {
                            // Shake adjacent token.
                            BoardManager.Instance.ShakeTokenList(BoardManager.Instance.GetAdjacentToken(card.GetUniqueID()));

                            //FAILSAFE: Reset card to correct rotation
                            card.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 0));
                        }
                        , delegate ()
                        {
                            UIManager.Instance.UpdateCardAllPassiveUI(card.Data, delegate ()
                            {
                                card.SetInteractable(true);
                                GameManager.Instance.OnPlayerPlayHandCardComplete(card.Data.UniqueID);

                                if (onComplete != null)
                                {
                                    onComplete.Invoke();
                                }
                            });
                        }
                        , true
                    );

                    if (GameManager.Instance.IsCanShowSpiritEffect())
                    {
                        // TODO : Change Effect to Toggleable and callbackable
                        switch (playerPosition)
                        {
                            case PlayerPosition.Top:
                                {
                                    UIEffectManager.Instance.SpiritEffect(
                                          "Spirit_Red"
                                        , CardVFXPool.VFXType.SpiritCircle_red
                                        , card.transform.position
                                        , UIManager.Instance.UIPlayerTop.GetSoulPosition()
                                        , null
                                    );
                                }
                                break;

                            case PlayerPosition.Bottom:
                                {
                                    UIEffectManager.Instance.SpiritEffect(
                                          "Spirit_Blue"
                                        , CardVFXPool.VFXType.SpiritCircle_blue
                                        , card.transform.position
                                        , UIManager.Instance.UIPlayerBottom.GetSoulPosition()
                                        , null
                                    );
                                }
                                break;
                        }
                    }
                }
            });

            CancelSelected();
        }

        public void SoulEffectFromMinion(List<CardUI> cardData, UnityAction onComplete)
        {
            UnityAction<int> OnLastEffectComplete = delegate (int i)
            {
                if (i == cardData.Count - 1)
                {
                    onComplete?.Invoke();
                }
            };

            foreach (CardUI item in cardData)
            {
                PlayerPosition playerPosition = GameManager.Instance.IsLocalPlayer(item.Data.Owner) ? PlayerPosition.Bottom : PlayerPosition.Top;
                switch (playerPosition)
                {
                    case PlayerPosition.Top:
                        {
                            UIEffectManager.Instance.SpiritEffect(
                                  "Spirit_Red"
                                , CardVFXPool.VFXType.SpiritCircle_red
                                , item.transform.position
                                , UIManager.Instance.UIPlayerTop.GetSoulPosition()
                                , () => OnLastEffectComplete(cardData.IndexOf(item))
                            );
                        }
                        break;

                    case PlayerPosition.Bottom:
                        {
                            UIEffectManager.Instance.SpiritEffect(
                                  "Spirit_Blue"
                                , CardVFXPool.VFXType.SpiritCircle_blue
                                , item.transform.position
                                , UIManager.Instance.UIPlayerBottom.GetSoulPosition()
                                , () => OnLastEffectComplete(cardData.IndexOf(item))
                            );
                        }
                        break;
                }
            }

        }

        /// <summary>
        /// Find CardUI with uniqueID.
        /// </summary>
        /// <param name="uniqueID">UniqueID of selected card.</param>
        /// <param name="cardUI">return CardUI parameter.</param>
        /// <param name="playerPosition">retrun PlayerPosition. (owner of card)</param>
        /// <returns></returns>
        public bool FindCardUIByID(int uniqueID, out CardUI cardUI, out PlayerPosition playerPosition)
        {
            CardUI temp1 = _cardTopList.Find(x => x.Data.UniqueID == uniqueID);
            CardUI temp2 = _cardBottomList.Find(x => x.Data.UniqueID == uniqueID);
            if (temp1 != null)
            {
                cardUI = temp1;
                playerPosition = PlayerPosition.Top;
                return true;
            }
            else if (temp2 != null)
            {
                cardUI = temp2;
                playerPosition = PlayerPosition.Bottom;
                return true;
            }
            else
            {
                //Debug.LogWarning("Cannot find the request card id.");
                cardUI = null;
                playerPosition = PlayerPosition.Bottom;
                return false;
            }
        }

        public void PlayAnimationShowCard(PlayerPosition playerPosition, CardUI card, UnityAction onComplete, float duration = 0.3f)
        {
            if (playerPosition == PlayerPosition.Top)
            {
                card.AnimationCardToPosition(ShowCardAreaTop, onComplete);
            }
            else if (playerPosition == PlayerPosition.Bottom)
            {
                card.AnimationCardToPosition(ShowCardAreaBottom, onComplete, duration);
            }
            else
            {
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            }
        }

        public void PlayAnimationCardSort(PlayerPosition playerPosition, UnityAction onComplete, float duration = 0.5f)
        {
            switch (playerPosition)
            {
                case PlayerPosition.Top:
                    if (_cardTopList != null && _cardTopList.Count > 0)
                    {
                        int count = 0;
                        for (int i = 0; i < _cardTopList.Count; i++)
                        {
                            if (!_cardTopList[i].IsUsed
                                && _cardTopList[i].IsInteractable
                                && _cardTopList[i].Mode != CardUI.CardMode.TOKEN
                            )
                            {
                                _cardTopList[i].AnimationCardToLocalPosition(
                                      CardHolderTop.HolderList[i].transform
                                    , CardHolderTop.HolderList[i].transform.localPosition
                                    , delegate ()
                                    {
                                        count++;
                                        if (count >= _cardTopList.Count)
                                        {
                                            onComplete?.Invoke();
                                        }
                                    }
                                    , duration
                                );

                                _cardTopList[i].SetHolder(CardHolderTop.HolderList[i].transform);
                            }
                        }
                    }
                    else
                    {
                        // Empty hand
                        if (onComplete != null)
                        {
                            onComplete.Invoke();
                        }
                    }
                    break;

                case PlayerPosition.Bottom:
                    CancelSelectCard();
                    if (_cardBottomList != null && _cardBottomList.Count > 0)
                    {
                        int count = 0;
                        for (int i = 0; i < _cardBottomList.Count; i++)
                        {
                            if (!_cardBottomList[i].IsUsed && _cardBottomList[i].IsInteractable )
                            {
                                if(_cardBottomList[i].Mode == CardUI.CardMode.TOKEN)
                                {
                                    _cardBottomList[i].AnimationToCard();
                                }

                                _cardBottomList[i].AnimationCardToLocalPosition(
                                      CardHolderBottom.HolderList[i].transform
                                    , CardHolderBottom.HolderList[i].transform.localPosition
                                    , delegate
                                    {
                                        // OnComplete
                                        count++;
                                        if (count >= _cardBottomList.Count)
                                        {
                                            onComplete?.Invoke();
                                        }

                                    }
                                    , duration
                                );

                                _cardBottomList[i].SetHolder(CardHolderBottom.HolderList[i].transform);
                            }
                        }
                    }
                    else
                    {
                        // Empty hand
                        if (onComplete != null)
                        {
                            onComplete.Invoke();
                        }
                    }
                    break;
            }
        }

        public void SetHandMarkerPosition()
        {
            //Vector3 targetLocation = Camera.main.WorldToScreenPoint(cardUI.transform.position);
            //Vector3 hideLocation = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.1f, Camera.main.transform.position.y));
            //Vector3 showLocation = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.1f, Camera.main.transform.position.y));
            //HandHide.transform.position = new Vector3(hideLocation.x, HandHide.transform.position.y, HandHide.transform.position.z);
            //HandShow.transform.position = new Vector3(showLocation.x, HandShow.transform.position.y, HandShow.transform.position.z);

            if (GameHelper.GetScreenFitMode() == ScreenFitMode.Height)
            {
                // Tablet 4:3 and 16:9
                CardHolderBottom.transform.position = HandShow.position;
                CardHolderBottom.transform.localScale = HandShow.localScale;
                _isShowHand = true;
            }
            else
            {
                // For long screen (iPhone X, etc.)
                CardHolderBottom.transform.position = HandHide.position;
                CardHolderBottom.transform.localScale = HandHide.localScale;
            }

        }

        public void ShowHandCard(bool isAnimate = true)
        {
            if (_cardIsAnimatePosition) return;

            if (!_isShowHand)
            {
                if (isAnimate)
                {
                    SoundManager.PlaySFX(SoundManager.SFX.Toggle_Hand_Card);

                    Sequence sq = DOTween.Sequence();
                    sq.Append(CardHolderBottom.transform.DOMove(HandShow.position, 0.2f).SetEase(Ease.OutExpo));
                    //sq.Join(CardHolderBottom.transform.DOScale(HandShow.transform.localScale, 0.2f).SetEase(Ease.OutExpo));
                    //sq.OnComplete(delegate { PlayAnimationCardToSlot(PlayerPosition.Bottom, null); });
                }
                else
                {
                    SoundManager.PlaySFX(SoundManager.SFX.Toggle_Hand_Card);

                    CardHolderBottom.transform.position = HandShow.position;
                    CardHolderBottom.transform.localScale = HandShow.transform.localScale;
                }
                _isShowHand = true;
            }

            UIManager.Instance.EnableTauntBTN(true);
        }

        public void HideHandCard()
        {
            if (_cardIsAnimatePosition) return;

            if (_isShowHand)
            {
                SoundManager.PlaySFX(SoundManager.SFX.Cancel_Click);

                Sequence sq = DOTween.Sequence();
                sq.Append(CardHolderBottom.transform.DOMove(HandHide.position, 0.2f).SetEase(Ease.OutExpo));

                _isShowHand = false;
            }

            UIManager.Instance.EnableTauntBTN(true);

        }

        public void WaitQueue(float duration)
        {
            WaitUI waitUI = new WaitUI(duration);
            UIQueue.Instance.Add(waitUI);
        }

        public void PlaySFXQueue(SoundManager.SFX sfx, bool isWaitOnComplete, bool isAdd)
        {
            //return; // disable sound.
            SFXQueueUI sfxQueueUI = new SFXQueueUI(sfx, isWaitOnComplete);
            if (isAdd)
            {
                UIQueue.Instance.Add(sfxQueueUI);
            }
            else
            {
                UIQueue.Instance.Join(sfxQueueUI);
            }
        }

        public void PlaySFX(SoundManager.SFX sfx)
        {
            SoundManager.PlaySFX(sfx);
        }
        #endregion

        #region Redraw Method
        public void ReturnCardToDeck(PlayerIndex playerIndex, List<int> uniqueIDList)
        {
            uniqueIDList.Sort();
            foreach (int item in uniqueIDList)
            {
                ReturnCardToDeck(playerIndex, item);
            }
        }

        public void ReturnCardToDeck(PlayerIndex playerIndex, int uniqueIDList)
        {
            CardUI card;
            if (playerIndex == GameManager.Instance.GetLocalPlayerIndex())
            {
                _redrawList = new List<int>(uniqueIDList);
                card = _cardBottomList.Find(x => x.GetUniqueID() == uniqueIDList);
                if (card != null)
                {
                    card.AnimationCardToPosition(BottomDeck.transform, null);
                    card.DestroyCard();
                    DetachCard(PlayerPosition.Bottom, card);
                }
            }
            else
            {
                card = _cardTopList.Find(x => x.GetUniqueID() == uniqueIDList);
                if (card != null)
                {
                    card.AnimationCardToPosition(TopDeck.transform, null);
                    card.DestroyCard();
                    DetachCard(PlayerPosition.Top, card);
                }
            }
        }

        [System.Obsolete]
        public void ShowRedrawUI(int countRequire)
        {
            _redrawMax = countRequire;
            _redrawList.Clear();

            CallbackUI callbackUI = new CallbackUI(delegate
            {
                UIRedrawHolder.ShowUI(CardBottomList);
            });
            UIQueue.Instance.Add(callbackUI);
        }

        public void HideRedrawUI()
        {
            UIRedrawHolder.HideUI();
        }

        public void ClearRedrawList()
        {
            _redrawList.Clear();
        }

        public void CancelRedrawCard()
        {
            foreach (CardUI item in _cardBottomList)
            {
                item.CardUIObject.SetDiscard(false);
            }
        }

        #endregion

        #region Events
        private void IsAnimatePosition(bool isMoving)
        {
            _cardIsAnimatePosition = isMoving;
        }

        public bool CardIsSelected(CardUI card)
        {
            if (card != null)
            {
                _currentCardSelected = card;
                if (GameManager.Instance.Phase == GamePhase.Play
                    && GameManager.Instance.IsLocalPlayer(GameManager.Instance.CurrentPlayerIndex)
                )
                {
                    _currentCardSelected.SetDrag(true);
                    _currentCardHilight.CardUIObject.ShowCard(true);
                    if (GameManager.Instance.IsNetworkPlay())
                    {
                        if (_currentCardSelected != null)
                        {
                            GameManager.Instance.PhotonRequestShowPreUseCard(_currentCardSelected.GetUniqueID(), true);
                        }
                    }
                    IsSelected = true;
                }
                else
                {
                    _currentCardSelected.SetDrag(false);
                    IsSelected = false;
                    CancelHilight();
                    _currentCardSelected = null;
                }

                _dragSoundIndex = SoundManager.PlaySFX(SoundManager.SFX.Drag_Card_Loop, true);

                return true;
            }
            else
            {
                return false;
            }

        }

        public void CancelSelected(bool isAnimation = false)
        {
            if (_currentCardSelected != null)
            {
                _currentCardSelected.SetUsed(false);
                _currentCardSelected.SetDrag(false);
                if (isAnimation)
                {
                    IsAnimatePosition(true);
                    _currentCardSelected.AnimationCardToPosition(_currentCardSelected.GetHolder(), delegate { IsAnimatePosition(false); });
                }
                if (_currentCardSelected.Mode == CardUI.CardMode.TOKEN && !_currentCardSelected.IsUsed)
                {
                    _currentCardSelected.Mode = CardUI.CardMode.CARD;
                    _currentCardSelected.AnimationToCard();
                }
            }
            if (GameManager.Instance.IsNetworkPlay())
            {
                if (_currentCardSelected != null)
                {
                    GameManager.Instance.PhotonRequestShowPreUseCard(_currentCardSelected.GetUniqueID(), false);
                }
            }

            _currentCardSelected = null;
            IsSelected = false;

            SoundManager.StopSFX(_dragSoundIndex, 0.0f);
            _dragSoundIndex = -1;
        }

        public void UseSelectedCard(bool isAnimation = true)
        {
            if (_currentCardSelected != null)
            {
                _currentCardSelected.SetDrag(false);
                /*
                if (isAnimation)
                {
                    _currentCardSelected.AnimationCardToPosition(_currentCardSelected.transform, position, true, 0.5f);
                }
                */
                if (_currentCardSelected.Mode == CardUI.CardMode.CARD)
                {
                    _currentCardSelected.Mode = CardUI.CardMode.TOKEN;
                }
            }
            _currentCardSelected = null;
            IsSelected = false;

            SoundManager.StopSFX(_dragSoundIndex, 0.0f);
            _dragSoundIndex = -1;
        }

        public void CardIsHilight(CardUI card)
        {
            if (_currentCardHilight != null)
            {
                _currentCardHilight.SetHilight(false);
                _currentCardHilight.CardUIObject.ShowCard(true);
                _currentCardHilight.AnimationCardToPosition(_currentCardHilight.GetHolder(), null, 0.1f);
            }
            IsHilight = true;
            _currentCardHilight = card;
            _currentCardHilight.SetHilight(true);
            _currentCardHilight.CardUIObject.ShowCard(false);
            UIManager.Instance.CardUIDetailHover.SetData(card.Data);
            UIManager.Instance.ShowDetailCardUI(true);
            //PlaySFX(SoundManager.SFX.Card_Click);

            if (_currentCardHilight.Data.CurrentZone == CardZone.Hand)
            {
                if (_currentCardHilight.Data.SpiritCurrent <= GameManager.Instance.Data.PlayerDataList[(int)GameManager.Instance.GetLocalPlayerIndex()].CurrentAP)
                {
                    UIManager.Instance.UIPlayerBottom.CastingPlayerAp(_currentCardHilight.Data.SpiritCurrent, true);
                }
            }
        }

        public void CancelHilight(bool playAnimation = false, bool uiAnimate = true)
        {
            if (IsHilight)
            {
                if (_currentCardHilight != null)
                {
                    UIManager.Instance.SendPlayerHoverCard(_currentCardHilight.GetUniqueID(), false);
                    _currentCardHilight.SetHilight(false);
                    _currentCardHilight.CardUIObject.ShowCard(true);
                    if (playAnimation)
                    {
                        _currentCardHilight.AnimationCardToPosition(_currentCardHilight.GetHolder(), null, 0.1f);
                    }
                }
                _currentCardHilight = null;
                IsHilight = false;

                if (uiAnimate)
                {
                    UIManager.Instance.ShowDetailCardUI(false);
                }
            }

        }

        public void PointerIsHolding(bool isHolding)
        {
            IsDown = isHolding;
        }

        public void ShowCardReadyToActive(bool isShow)
        {
            foreach (CardUI item in _cardBottomList)
            {
                item.ShowGlowReady(isShow);
            }
        }

        public void ShowCardReadyToActive(int uniqueID, bool isShow)
        {
            foreach (CardUI item in _cardBottomList)
            {
                if (item.Data.UniqueID == uniqueID)
                {
                    item.ShowGlowReady(isShow);
                }
            }
        }

        public bool GetCardIsUsing()
        {
            if (Input.mousePosition.y > _downPosition.y + _useOffset.y)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public Vector3 GetRayCastPosition()
        {
            return _hit.point;
        }
        #endregion

        #region CardEvent
        private void CardHilightOnEnter(CardUI card)
        {
            if (IsHilight && IsDown && !IsSelected)
            {
                CardIsHilight(card);
                //card.AnimationHilightCard(false);
            }
        }

        private void CardHilightOnDown(CardUI card)
        {
            if (_currentCardHilight != card)
            {
                CardIsHilight(card);
                UIManager.Instance.SendPlayerHoverCard(card.GetUniqueID(), true);
                //card.AnimationHilightCard(false);
            }
        }
        #endregion

        #region OnCard Event
        public void OnCardEnter(CardUI card)
        {
            //if (UIQueue.Instance.IsReady)
            {
                if (card.IsBackCard)
                {
                    return;
                }

                if (GameManager.Instance.Phase == GamePhase.ReHand)
                {

                }
                else if (GameManager.Instance.Phase >= GamePhase.Begin)
                {
                    //if (_downTime < _misclickTime)
                    {
                        //CardHilightOnEnter(card);
                    }

                }
            }
        }

        public void OnCardDown(CardUI card)
        {
            if (
                   UIQueue.Instance.IsReady
                || !GameManager.Instance.IsLocalPlayer(GameManager.Instance.CurrentPlayerIndex)
            )
            {
                if (card.IsBackCard)
                {
                    return;
                }

                if (GameManager.Instance.Phase == GamePhase.ReHand)
                {
                    if (!card.IsUsed)
                    {
                        PointerIsHolding(true);
                    }

                    if (card.Mode == CardUI.CardMode.CARD)
                    {
                        _downPosition = Input.mousePosition;

                        if (GameHelper.GetScreenFitMode() == ScreenFitMode.Height)
                        {
                            if (_isShowHand)
                            {
                                CardHilightOnDown(card);
                                //ResetMisclickTime();
                            }
                            else
                            {
                                if (!card.IsUsed)
                                {
                                    ShowHandCard();
                                }
                            }
                        }
                        else
                        {
                            CardHilightOnDown(card);
                            //ResetMisclickTime();
                        }


                    }
                }
                else if (GameManager.Instance.Phase >= GamePhase.Begin)
                {
                    PointerIsHolding(true);

                    if (card.Mode == CardUI.CardMode.CARD)
                    {
                        _downPosition = Input.mousePosition;

                        if (GameHelper.GetScreenFitMode() == ScreenFitMode.Height)
                        {
                            if (_isShowHand)
                            {
                                CardHilightOnDown(card);
                            }
                            else
                            {
                                if (!card.IsUsed)
                                {
                                    ShowHandCard();
                                }
                            }
                        }
                        else
                        {
                            CardHilightOnDown(card);
                        }
                    }
                }
            }
        }

        public void OnCardUp(CardUI card)
        {
            //if (UIQueue.Instance.IsReady)
            {
                if (card.IsBackCard)
                {
                    return;
                }

                if (GameManager.Instance.Phase == GamePhase.ReHand)
                {
                    PointerIsHolding(false);
                    CancelHilight();
                }
                else if (GameManager.Instance.Phase >= GamePhase.Begin)
                {
                    PointerIsHolding(false);
                    CancelHilight();

                    if (BoardManager.Instance.CurrentSlot != null && _currentCardSelected != null)
                    {
                        _currentCardSelected.SetUsed(true);
                        _currentCardSelected.SetInteractable(false);

                        GameManager.Instance.RequestLocalUseCard(
                              _currentCardSelected.Data.UniqueID
                            , BoardManager.Instance.CurrentSlot.GetSlotID()
                            , false
                            , delegate ()
                            {
                                if (_currentCardSelected != null)
                                {
                                    GameManager.Instance.Controller.OnPlayCardFail();
                                    _currentCardSelected.SetInteractable(true);
                                    CancelSelectCard();
                                }

                            }
                        );
                        BoardManager.Instance.HideAllHilight();
                    }
                    else
                    {
                        CancelSelectCard();
                    }
                }
                UIManager.Instance.UIPlayerBottom.CastingPlayerAp(3, false);
            }
        }

        public void ForceCancelSelected()
        {
            PointerIsHolding(false);
            CancelHilight();
            BoardManager.Instance.HideAllHilight();
            if (_currentCardSelected != null)
            {
                _currentCardSelected.SetInteractable(true);
            }
            CancelSelectCard();
        }

        public void CancelSelectCard()
        {
            if (UIManager.Instance.IsSelectionSlotShowing) return;

            if (GetCardIsUsing() || IsSelected)
            {
                CancelSelected(true);
                BoardManager.Instance.HideAllHilight();
            }
        }
        #endregion

        #region Tutorial

        public void Tutorial_HighlightCard_Direction(int uniqueID, bool isShow)
        {
            GetCardUI(uniqueID).CardUIObject.Tutorial_HilightArrow(isShow);
        }

        public void Tutorial_HighlightCard_Soul(int uniqueID, bool isShow)
        {
            GetCardUI(uniqueID).CardUIObject.Tutorial_HilightSoul(isShow);
        }

        public void Tutorial_HighlightCard_Atk(int uniqueID, bool isShow)
        {
            GetCardUI(uniqueID).CardUIObject.Tutorial_HilightATK(isShow);
        }

        public void Tutorial_HighlightCard_Hp(int uniqueID, bool isShow)
        {
            GetCardUI(uniqueID).CardUIObject.Tutorial_HilightHP(isShow);
        }

        public void Tutorial_HighlightCard_Armor(int uniqueID, bool isShow)
        {
            GetCardUI(uniqueID).CardUIObject.Tutorial_HilightARMOR(isShow);
        }

        public void Tutorial_HighlightCard_Pointer(int uniqueID, bool isShow, Color color)
        {
            GetCardUI(uniqueID).CardUIObject.Tutorial_HintPoniter(isShow, color);
        }
        #endregion
    }
}
