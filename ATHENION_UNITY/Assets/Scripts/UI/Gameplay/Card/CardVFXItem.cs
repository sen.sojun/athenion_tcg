﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CardVFXItem : VFXItem 
{
    #region Public Properties
    public GameObject Socket;
    public string VFXKey { get; protected set; }
    public bool IsUpdateFacing { get { return _isUpdateFacing; } }
    #endregion

    #region Private Properties
    private UnityAction _onImpact;
    private UnityAction _onComplete;
    private GameObject _obj;
    private Vector3 _previousPosition = Vector3.zero;
    private bool _isUpdateFacing = false;

    #endregion

    #region Update
    private void Update()
    {
        if (IsUpdateFacing)
        {
            if (!GameHelper.IsNearlyEqual(transform.position.magnitude, _previousPosition.magnitude))
            {
                Vector3 dir = (transform.position - _previousPosition).normalized;
                transform.LookAt(transform.position + dir * 10.0f);
            }

            _previousPosition = transform.position;
        }

    }
    #endregion
    #region Methods
    public void SetEnableUpdateFacing(bool isEnable)
    {
        _isUpdateFacing = isEnable;
    }

    public void SetVFXKey(string vfxKey)
    {
        VFXKey = vfxKey;
    }

    public void SetStartPosition(Vector3 position)
    {
        _previousPosition = position;
    }

    public void SetAnimationComplete(UnityAction onComplete)
    {
        _onComplete = onComplete;
    }

    public void SetImpactComplete(UnityAction onImpact)
    {
        _onImpact = onImpact;
    }

    public void AttachGameObject(GameObject obj)
    {
        _obj = obj;
        transform.localScale = obj.transform.localScale;
        obj.transform.SetParent(Socket.transform);
        obj.transform.localPosition = Vector3.zero;
        obj.transform.rotation = Socket.transform.rotation;
    }

    public void OnImpact()
    {
        if (_onImpact != null)
        {
            _onImpact.Invoke();
        }
    }

    public void OnComplete()
    {
        if (_obj != null)
        {
            _obj.transform.SetParent(null);
            _obj = null;
        }
        
        if (_onComplete != null)
        {
            _onComplete.Invoke();
        }
    }

    public void CameraShake(int damage = 5)
    {
        UIManager.Instance.CameraScreenShake(damage);
    }

    public void ScreenFadeBlackIn()
    {
        UIManager.Instance.FadeBlackIn();
    }

    public void ScreenFadeBlackOut()
    {
        UIManager.Instance.FadeBlackOut();
    }

    public void PlaySound(SoundManager.SFX sfx)
    {
        SoundManager.PlaySFX(sfx);
    }
    #endregion
}
