﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardEffect_SpawnCard : CardVFXItem
{
    [Header("Use this to play 'Spawn' sound effect. (When card is Spawn)")]
    public SoundManager.SFX Spawn;
    [Header("SFX override")]
    [SerializeField] private string _SFXKey;
    public bool HideWarning = false;

    #region Public Methods
    public void PlaySound_Spawn()
    {
        if (string.IsNullOrEmpty(_SFXKey))
        {
            if(HideWarning == false) Debug.LogWarning("No _SFXKey Assign :" + this.gameObject.name);
            SoundManager.PlaySFX(Spawn);
        }
        else
        {
            SoundManager.PlaySFX(_SFXKey.ToUpper());
        }
            
    }
    #endregion
}
