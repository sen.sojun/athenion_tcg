﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CardVFXAttackItem : CardVFXItem
{
    #region Private
    [SerializeField] private GameObject _PierceEffect;
    private const float SPEED_BASE = 18.0f;
    private const float SPEED_MODIFIER = 8.0f;
    #endregion

    #region Methods
    /// <summary>
    /// Set Attack Range for pierce system
    /// </summary>
    /// <param name="range">Range : [1 for 1 slot][2 for 2 slot][etc.]</param>
    public void SetAttackRange(int range)
    {
        _PierceEffect.gameObject.SetActive(range > 0);

        ParticleSystem ps = _PierceEffect.GetComponent<ParticleSystem>();
        ParticleSystem.MainModule pm = ps.main;
        pm.startSpeed = SPEED_BASE + (range * SPEED_MODIFIER);

    }
    #endregion
}
