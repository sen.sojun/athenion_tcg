﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Karamucho.UI
{
    public class CardCancelZone : MonoBehaviour, IPointerClickHandler
    {

        private void Start()
        {
            gameObject.SetActive(false);
        }

        public void ShowCancelZone(bool isShow)
        {
            gameObject.SetActive(isShow);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (UIManager.Instance.IsHilightBoxShowing)
            {
                if (CardUIManager.Instance.IsHilight)
                {
                    CardUIManager.Instance.CancelHilight();
                    return;
                }
                if (BoardManager.Instance.IsHilight)
                {
                    BoardManager.Instance.CancelHilight();
                    return;
                }
            }


        }

    }
}
