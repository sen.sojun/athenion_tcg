﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Karamucho.UI
{
    public class CardUI : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler
    {
        public enum CardMode
        {
            CARD,
            TOKEN,
            BACK
        }

        public enum UIMode
        {
            OBJECT,
            UI
        }

        #region Public Properties
        public CardMode Mode;
        public static float TrackingRate = 10.0f;
        public static float RotationSpeed = 5.00f;
        public bool IsBackCard = false;
        public CardUIObject CardUIObject;
        public TokenUIObject TokenUIObject;
        public CardBackUIObject CardBackUIObject;
        public CardUIData Data { get; private set; }
        public bool IsUsed { get { return _isUsed; } }
        public bool IsInteractable { get { return (_interactableFlag <= 0); } }
        #endregion

        #region Private Properties
        private int _slotID = -1; // -1 not in any slot.
        private bool _isHilight = false;
        private bool _isDrag = false;
        private bool _isUsed = false;
        private int _interactableFlag = 0;
        private bool _isInBattle = false;
        private Transform _holder;
        private Animator _animator;
        private Sequence _sq = null;
        private RaycastHit _hit;
        private UIMode _uiMode;
        private bool[] _isShowStatus;
        #endregion

        #region Methods
        private void Start()
        {
            _isInBattle = GameHelper.IsHaveObject(typeof(GameManager));
            _isShowStatus = new bool[System.Enum.GetValues(typeof(CardStatusType)).Length + 1];
            System.Array.Clear(_isShowStatus, 0, _isShowStatus.Length);

            //InitCard();
        }

        private void Update()
        {
            CardToPointer();
            /*
            #if UNITY_EDITOR
            if (Mode == CardMode.TOKEN)
            {
                if (Input.GetKeyDown(KeyCode.P))
                {
                    ShowFreeze(null);
                }
                if (Input.GetKeyDown(KeyCode.O))
                {
                    HideFreeze(null);
                }
                if (Input.GetKeyDown(KeyCode.K))
                {
                    ShowFear(null);
                }
                if (Input.GetKeyDown(KeyCode.L))
                {
                    HideFear(null);
                }
                if (Input.GetKeyDown(KeyCode.U))
                {
                    TokenUIObject.SetEffectDirection(Data.CardDir);
                }
                if (Input.GetKeyDown(KeyCode.I))
                {
                    HideStealth(null);
                }
                if (Input.GetKeyDown(KeyCode.E))
                {
                    ShowSilence(null);
                }
                if (Input.GetKeyDown(KeyCode.R))
                {
                    HideSilence(null);
                }
                if (Input.GetKeyDown(KeyCode.G))
                {
                    ShowTaunt(null);
                }
                if (Input.GetKeyDown(KeyCode.H))
                {
                    HideTaunt(null);
                }
            }
            #endif
            */
        }

        public void RefreshCardPosition()
        {
            float moveTime = 0.1f;
            if (_slotID >= 0 )
            {
                Vector3 targetPosition = BoardManager.Instance.GetSlotHolder(_slotID).GetHolderPosition();

                Sequence sq = DOTween.Sequence();
                sq.Append(transform.DOMove(targetPosition, moveTime).SetEase(Ease.OutExpo));
                sq.Join(transform.DORotate(new Vector3(90, 0, 0), moveTime));
            }

        }

        private void CardToPointer()
        {
            if (_isDrag && !IsUsed)
            {
                AnimationCardState();
                UpdateCardToPointer(CardUIManager.Instance.GetRayCastPosition());
                TokenUIObject.ShowGlow(true);
                if (OnRaycastCard())
                {
                    BoardManager.Instance
                        .SetRaycastHit(_hit);
                }
            }
        }

        public void InitAnimator()
        {
            _animator = GetComponent<Animator>();
        }

        public void InitCard(CardUIData data, UIMode mode)
        {
            _animator = GetComponent<Animator>();
            _uiMode = mode;

            if (data != null)
            {
                Data = new CardUIData(data); // Clone
            }
            Mode = CardMode.CARD;

            if (_uiMode == UIMode.OBJECT)
            {
                if (IsBackCard)
                {
                    if (Data != null)
                    {
                        // Card
                        CardUIObject.SetData(Data);

                        // Token
                        TokenUIObject.SetData(Data);

                        // Back
                        CardBackUIObject.SetCardBack(Data);
                    }
                    CardUIObject.gameObject.SetActive(false);
                    TokenUIObject.gameObject.SetActive(false);
                    CardBackUIObject.gameObject.SetActive(true);
                    CardBackUIObject.SetGlow(false);
                }
                else
                {
                    if (Data != null)
                    {
                        // Card
                        CardUIObject.SetData(Data);

                        // Token
                        TokenUIObject.SetData(Data);

                        // Back
                        CardBackUIObject.SetCardBack(Data);
                    }

                    AnimationToCard();
                    CardUIObject.gameObject.SetActive(true);
                    TokenUIObject.gameObject.SetActive(false);
                    CardBackUIObject.gameObject.SetActive(false);
                }
            }
            else
            {
                // UI Mode
                if (IsBackCard)
                {
                    if (Data != null)
                    {
                        // Card
                        CardUIObject.SetData(Data);

                        // Token
                        TokenUIObject.SetData(Data, false);

                        // Back
                        CardBackUIObject.SetCardBack(Data);
                    }
                    CardUIObject.gameObject.SetActive(false);
                    TokenUIObject.gameObject.SetActive(false);
                    CardBackUIObject.gameObject.SetActive(true);
                    CardBackUIObject.SetGlow(false);
                }
                else
                {
                    if (Data != null)
                    {
                        // Card
                        CardUIObject.SetData(Data);

                        // Token
                        TokenUIObject.SetData(Data, false);

                        // Back
                        CardBackUIObject.SetCardBack(Data);
                    }

                    //AnimationToCard();
                    CardUIObject.gameObject.SetActive(true);
                    TokenUIObject.gameObject.SetActive(false);
                    CardBackUIObject.gameObject.SetActive(false);
                }
            }
        }

        public void InitDummyCard(CardMode cardMode, UIMode uiMode)
        {
            _animator = GetComponent<Animator>();

            _uiMode = uiMode;
            Mode = cardMode;

            Data = null;

            if (_uiMode == UIMode.OBJECT)
            {
                if (IsBackCard)
                {
                    if (Data != null)
                    {
                        // Card
                        CardUIObject.SetData(Data);

                        // Token
                        TokenUIObject.SetData(Data);

                        // Back
                        CardBackUIObject.SetCardBack(Data);
                    }
                    CardUIObject.gameObject.SetActive(false);
                    TokenUIObject.gameObject.SetActive(false);
                    CardBackUIObject.gameObject.SetActive(true);
                    CardBackUIObject.SetGlow(false);
                }
                else
                {
                    if (Data != null)
                    {
                        // Card
                        CardUIObject.SetData(Data);

                        // Token
                        TokenUIObject.SetData(Data);

                        // Back
                        CardBackUIObject.SetCardBack(Data);
                    }

                    AnimationToCard();
                    CardUIObject.gameObject.SetActive(true);
                    TokenUIObject.gameObject.SetActive(false);
                    CardBackUIObject.gameObject.SetActive(false);
                }
            }
            else
            {
                // UI Mode
                if (IsBackCard)
                {
                    if (Data != null)
                    {
                        // Card
                        CardUIObject.SetData(Data);

                        // Token
                        TokenUIObject.SetData(Data, false);

                        // Back
                        CardBackUIObject.SetCardBack(Data);
                    }
                    CardUIObject.gameObject.SetActive(false);
                    TokenUIObject.gameObject.SetActive(false);
                    CardBackUIObject.gameObject.SetActive(true);
                    CardBackUIObject.SetGlow(false);
                }
                else
                {
                    if (Data != null)
                    {
                        // Card
                        CardUIObject.SetData(Data);

                        // Token
                        TokenUIObject.SetData(Data, false);

                        // Back
                        CardBackUIObject.SetCardBack(Data);
                    }

                    //AnimationToCard();
                    CardUIObject.gameObject.SetActive(true);
                    TokenUIObject.gameObject.SetActive(false);
                    CardBackUIObject.gameObject.SetActive(false);
                }
            }
        }

        public void SetUIMode(UIMode mode)
        {
            _uiMode = mode;
        }

        public void UpdateCardUI(CardUIData cardUIData, UnityAction onComplete)
        {
            Data = new CardUIData(cardUIData); // Clone
            if (Data != null)
            {
                // Card
                CardUIObject.SetData(Data);

                // Token
                TokenUIObject.SetData(Data, _isInBattle);

            }

            if (onComplete != null)
            {
                onComplete.Invoke();
            }
        }

        public void UpdateAllPassiveUI(CardUIData cardUIData, UnityAction onComplete)
        {
            Data = new CardUIData(cardUIData); // Clone

            foreach (CardStatusType statusType in System.Enum.GetValues(typeof(CardStatusType)))
            {
                // if is same status
                if (_isShowStatus[(int)statusType] == Data.IsMinionStatus(statusType)) continue;

                // Update current status
                bool isShow = (Mode == CardMode.TOKEN && Data.IsMinionStatus(statusType));
                _isShowStatus[(int)statusType] = isShow;

                if (isShow)
                {
                    switch (statusType)
                    {
                        case CardStatusType.Be_Fear:
                        {
                            ShowFear(onComplete);
                        }
                        return;

                        case CardStatusType.Be_Silence:
                        {
                            ShowSilence(onComplete);
                        }
                        return;

                        case CardStatusType.Be_Freeze:
                        {
                            ShowFreeze(onComplete);
                        }
                        return;

                        case CardStatusType.Be_Stealth:
                        {
                            ShowStealth(onComplete);
                        }
                        return;

                        case CardStatusType.TauntActive:
                        {
                            ShowTaunt(onComplete);
                        }
                        return;

                        case CardStatusType.AuraActive:
                        {
                            ShowAura(onComplete);
                        }
                        return;

                        case CardStatusType.BerserkActive:
                        {
                            ShowBerserk(onComplete);
                        }
                        return;

                        case CardStatusType.Be_DarkMatter:
                        {
                            ShowDarkMatter(onComplete);
                        }
                        return;

                        case CardStatusType.Be_Piercing:
                        {
                            ShowPierce(onComplete);
                        }
                        return;
                    }
                }
                else
                {
                    switch (statusType)
                    {
                        case CardStatusType.Be_Fear:
                        {
                            HideFear(onComplete);
                        }
                        return;

                        case CardStatusType.Be_Silence:
                        {
                            HideSilence(onComplete);
                        }
                        return;

                        case CardStatusType.Be_Freeze:
                        {
                            HideFreeze(onComplete);
                        }
                        return;

                        case CardStatusType.Be_Stealth:
                        {
                            HideStealth(onComplete);
                        }
                        return;

                        case CardStatusType.TauntActive:
                        {
                            HideTaunt(onComplete);
                        }
                        return;

                        case CardStatusType.AuraActive:
                        {
                            HideAura(onComplete);
                        }
                        return;

                        case CardStatusType.BerserkActive:
                        {
                            HideBerserk(onComplete);
                        }
                        return;

                        case CardStatusType.Be_DarkMatter:
                        {
                            HideDarkMatter(onComplete);
                        }
                        return;

                        case CardStatusType.Be_Piercing:
                        {
                            HidePierce(onComplete);
                        }
                        return;
                    }
                }
            }


            if (onComplete != null)
            {
                onComplete.Invoke();
            }
        }

        public void UpdatePassiveUI(CardUIData cardUIData, CardStatusType statusType, bool isActive, UnityAction onComplete)
        {
            Data = new CardUIData(cardUIData); // Clone

            // Update current status
            bool isShow = (Mode == CardMode.TOKEN && isActive);
            _isShowStatus[(int)statusType] = isShow;

            if (isShow)
            {
                switch (statusType)
                {
                    case CardStatusType.Be_Fear:
                    {
                        ShowFear(onComplete);
                    }
                    return;

                    case CardStatusType.Be_Silence:
                    {
                        ShowSilence(onComplete);
                    }
                    return;

                    case CardStatusType.Be_Freeze:
                    {
                        ShowFreeze(onComplete);
                    }
                    return;

                    case CardStatusType.Be_Stealth:
                    {
                        ShowStealth(onComplete);
                    }
                    return;

                    case CardStatusType.TauntActive:
                    {
                        ShowTaunt(onComplete);
                    }
                    return;

                    case CardStatusType.AuraActive:
                    {
                        ShowAura(onComplete);
                    }
                    return;

                    case CardStatusType.BerserkActive:
                    {
                        ShowBerserk(onComplete);
                    }
                    return;

                    case CardStatusType.Be_DarkMatter:
                    {
                        ShowDarkMatter(onComplete);
                    }
                    return;

                    case CardStatusType.Be_Piercing:
                    {
                        ShowPierce(onComplete);
                    }
                    return;
                }
            }
            else if (isShow == false && Data.IsMinionStatus(statusType) == false)
            {
                switch (statusType)
                {
                    case CardStatusType.Be_Fear:
                    {
                        HideFear(onComplete);
                    }
                    return;

                    case CardStatusType.Be_Silence:
                    {
                        HideSilence(onComplete);
                    }
                    return;

                    case CardStatusType.Be_Freeze:
                    {
                        HideFreeze(onComplete);
                    }
                    return;

                    case CardStatusType.Be_Stealth:
                    {
                        HideStealth(onComplete);
                    }
                    return;

                    case CardStatusType.TauntActive:
                    {
                        HideTaunt(onComplete);
                    }
                    return;

                    case CardStatusType.AuraActive:
                    {
                        HideAura(onComplete);
                    }
                    return;

                    case CardStatusType.BerserkActive:
                    {
                        HideBerserk(onComplete);
                    }
                    return;

                    case CardStatusType.Be_DarkMatter:
                    {
                        HideDarkMatter(onComplete);
                    }
                    return;

                    case CardStatusType.Be_Piercing:
                    {
                        HidePierce(onComplete);
                    }
                    return;
                }
            }

            if (onComplete != null)
            {
                onComplete.Invoke();
            }
        }

        /// <summary>
        /// Update card position to mouse/pointer.
        /// </summary>
        private void UpdateCardToPointer(Vector3 hit)
        {
            float hoverOffset = 1.5f;
            Vector3 point = new Vector3(
                hit.x,
                hoverOffset,
                hit.z
                );
            Vector3 point2 = new Vector3(
                hit.x,
                0.0f,
                hit.z
                );
            // Update position to pointer.
            transform.position = Vector3.Lerp(transform.position, point, Time.deltaTime * TrackingRate);

            // Update rotation of card.
            Vector3 dir = Vector3.RotateTowards(transform.forward, -transform.position + point2, Time.deltaTime * RotationSpeed, 1.0f);
            transform.rotation = Quaternion.LookRotation(dir, Vector3.forward);

        }

        public void DestroyCard()
        {
            DestroyCardUI destroyCardUI = new DestroyCardUI(this);
            UIQueue.Instance.Add(destroyCardUI);
        }

        private bool OnRaycastCard()
        {
            if (!IsUsed)
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out _hit))
                {
                    return true;
                }
                return false;
                //if (Physics.Raycast(transform.position, transform.forward, out _hit, 5.0f))
                //Debug.DrawRay(Camera.main.transform.position, (targetPos - Camera.main.transform.position).normalized * 100f, Color.red);
                //if (Physics.Raycast(Camera.main.transform.position, targetPos - Camera.main.transform.position, out _hit, 100.0f))
                //{
                //    //Debug.Log(_hit.collider.name);
                //    return true;
                //}
                //else
                //{
                //    return false;
                //}
            }
            else
            {
                return false;
            }

        }
        #endregion

        #region Animation
        /// <summary>
        /// Move card to target position.
        /// </summary>
        /// <param name="targetPosition">Position of slot.</param>
        public void AnimationCardToPosition(Transform targetTransform, UnityAction onComplete, float duration = 0.5f)
        {
            Sequence sq = DOTween.Sequence();
            // Animation sequence 
            sq.Append(transform.DOMove(targetTransform.position, duration).SetEase(Ease.OutExpo));
            sq.Join(transform.DOScale(targetTransform.localScale, duration).SetEase(Ease.OutExpo));
            sq.Join(transform.DOLocalRotateQuaternion(targetTransform.localRotation, duration).SetEase(Ease.OutQuart));
            sq.AppendInterval(0.1f);
            sq.OnComplete(delegate
            {
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            });
        }

        public void AnimationCardToLocalPosition(Transform targetTransform, Vector3 targetPosition, UnityAction onComplete, float duration = 0.5f)
        {
            Sequence sq = DOTween.Sequence();
            // Animation sequence 
            sq.Append(transform.DOLocalMove(targetPosition, duration).SetEase(Ease.OutExpo));
            sq.Join(transform.DOLocalRotateQuaternion(targetTransform.localRotation, duration).SetEase(Ease.OutExpo));
            sq.Join(transform.DOScale(targetTransform.localScale, duration).SetEase(Ease.OutExpo));
            sq.OnComplete(delegate
            {
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            });
        }

        public void AnimationHilightCard(bool isQueue = true)
        {
            Sequence sq;
            if (!isQueue)
            {
                if (_sq != null)
                {
                    if (_sq.onComplete != null) _sq.onComplete.Invoke();
                    _sq.Kill();
                }

                _sq = DOTween.Sequence();
                sq = _sq;
                sq.OnKill(delegate ()
                {
                    _sq = null;
                });
            }
            else
            {
                sq = DOTween.Sequence();
            }

            _isHilight = true;

            // Animation sequence 
            Vector3 hilightPosition = new Vector3(
                _holder.transform.position.x,
                _holder.transform.position.y + 0.3f,
                _holder.transform.position.z + 0.5f
            );
            sq.Append(transform.DOLocalMove(hilightPosition, 0.1f).SetEase(Ease.OutExpo));

            if (!isQueue)
            {
                sq.Play();
            }
            else
            {
                sq.Pause();
                SequenceAnimationQueueUI animationQueueUI = new SequenceAnimationQueueUI(sq, "Hilight");
                UIQueue.Instance.Join(animationQueueUI);
            }
        }

        public void AnimationDiscard(Transform target, bool isAdd, float duration = 0.5f, bool IsMoveup = true)
        {
            Sequence sq = DOTween.Sequence();

            _animator.SetTrigger("discard");
            UIEffectManager.Instance.SpawnVFX(CardVFXPool.VFXType.CardDiscard, this, transform.position, DestroyCard);

            if (IsMoveup)
            {
                Vector3 targetPosition = new Vector3(_holder.position.x, _holder.position.y, target.position.z);

                // Animation sequence 
                sq.Append(transform.DOMove(targetPosition, duration).SetEase(Ease.OutExpo));
                sq.Join(transform.DOLocalRotate(new Vector3(90, 0, 0), duration).SetEase(Ease.OutExpo));
                sq.Join(transform.DOScale(target.localScale, duration).SetEase(Ease.OutExpo));
            }

        }

        public void AnimationRemoveCard(Transform target, bool isAdd, float duration = 0.5f, bool IsMoveup = true)
        {
            Sequence sq = DOTween.Sequence();

            UnityAction onComplete = delegate
            {
                _animator.SetTrigger("discard");
                UIEffectManager.Instance.SpawnVFX(CardVFXPool.VFXType.CardDiscardFromDraw, this, transform.position, DestroyCard);
            };

            if (IsMoveup)
            {
                Vector3 targetPosition = new Vector3(_holder.position.x, _holder.position.y, target.position.z);

                // Animation sequence 
                sq.Append(transform.DOMove(targetPosition, duration).SetEase(Ease.OutExpo));
                sq.Join(transform.DOLocalRotate(new Vector3(90, 0, 0), duration).SetEase(Ease.OutExpo));
                sq.Join(transform.DOScale(target.localScale, duration).SetEase(Ease.OutExpo));
                sq.OnComplete(delegate
                {
                    onComplete.Invoke();
                });
            }
            else
            {
                onComplete.Invoke();
            }

        }

        public void AnimationDestroyCardFromDraw(bool isAdd,UnityAction onImpact, UnityAction onComplete = null)
        {
            _animator.SetTrigger("discard");
            UIEffectManager.Instance.SpawnVFX(CardVFXPool.VFXType.CardDiscardFromDraw, this, transform.position,
                delegate
                {
                    if (onImpact != null)
                    {
                        onImpact.Invoke();
                    }
                },
                delegate
                {
                    DestroyCard();
                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                }
            , true);
        }

        public void AnimationDestroyToken(Transform target, UnityAction onComplete, float duration = 0.5f, bool isMove = false)
        {
            SoundManager.PlaySFX(SoundManager.SFX.Token_Destroy);

            Sequence sq = DOTween.Sequence();

            _animator.SetTrigger("discard");
            UIEffectManager.Instance.SpawnDeathVFX(Data.DeathEffectKey, this, transform.position, delegate
            {
                DestroyCard();
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            });

            if (isMove)
            {
                Vector3 targetPosition = new Vector3(target.position.x, target.position.y, target.position.z);
                // Animation sequence 
                sq.Append(transform.DOMove(targetPosition, duration).SetEase(Ease.OutExpo));
            }
        }

        public void AnimationBanishToken(Transform target, UnityAction onComplete, float duration = 0.5f, bool isMove = false)
        {
            Sequence sq = DOTween.Sequence();

            _animator.SetTrigger("discard");
            UIEffectManager.Instance.SpawnDeathVFX("VFX_Death_Banish", this, transform.position, delegate
            {
                DestroyCard();
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            });

            if (isMove)
            {
                Vector3 targetPosition = new Vector3(target.position.x, target.position.y, target.position.z);
                // Animation sequence 
                sq.Append(transform.DOMove(targetPosition, duration).SetEase(Ease.OutExpo));
            }
        }

        private void AnimationCardState()
        {
            if (Mode == CardMode.CARD && CardUIManager.Instance.GetCardIsUsing())
            {
                AnimationToToken();
                UIEffectManager.Instance.SpawnVFX(CardVFXPool.VFXType.CardDiscard, this, transform.position, null);
            }
            else if (Mode == CardMode.TOKEN && !CardUIManager.Instance.GetCardIsUsing())
            {
                AnimationToCard();
                UIEffectManager.Instance.SpawnVFX(CardVFXPool.VFXType.CardDiscard, this, transform.position, null);
            }
        }

        public void AnimationToToken()
        {
            /*
            _animator.ResetTrigger("discard");
            _animator.ResetTrigger("tokenMode");
            _animator.ResetTrigger("cardMode");
            _animator.ResetTrigger("backMode");
            */

            _animator.SetBool("TokenMode", true);
            _animator.SetBool("CardMode", false);
            _animator.SetBool("BackMode", false);
            Mode = CardMode.TOKEN;
        }

        public void AnimationToCard()
        {
            /*
            _animator.ResetTrigger("discard");
            _animator.ResetTrigger("tokenMode");
            _animator.ResetTrigger("cardMode");
            _animator.ResetTrigger("backMode");
            */
            _animator.SetBool("CardMode", true);
            _animator.SetBool("TokenMode", false);
            _animator.SetBool("BackMode", false);
            Mode = CardMode.CARD;
        }

        public void AnimationToBack()
        {
            /*
            _animator.ResetTrigger("discard");
            _animator.ResetTrigger("tokenMode");
            _animator.ResetTrigger("cardMode");
            _animator.ResetTrigger("backMode");
            */
            _animator.SetBool("BackMode", true);
            _animator.SetBool("CardMode", false);
            _animator.SetBool("TokenMode", false);
            Mode = CardMode.BACK;
        }

        #endregion

        #region Effect
        public void ShowGlowReady(bool isShow)
        {
            CardUIObject.ShowGlowActive(isShow, Data.IsCanActiveChain());
        }

        public void ShowEnemyHover(bool isShow)
        {
            if (Mode == CardMode.TOKEN)
            {
                TokenUIObject.ShowEnemyHover(isShow);
            }
            else if (Mode == CardMode.BACK || Mode == CardMode.CARD)
            {
                CardBackUIObject.SetEnemyHoverGlow(isShow);
            }
        }

        public void ShowFreeze(UnityAction onComplete)
        {
            UIManager.Instance.ShowFreezeOnMinion(this, delegate
            {
                TokenUIObject.ShowFreeze();
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            });
        }

        public void HideFreeze(UnityAction onComplete)
        {
            TokenUIObject.HideFreeze();
            UIManager.Instance.HideFreezeOnMinion(this, onComplete);
        }

        public void ShowFear(UnityAction onComplete)
        {
            TokenUIObject.ShowFear();
            if (onComplete != null)
            {
                onComplete.Invoke();
            }
        }

        public void HideFear(UnityAction onComplete)
        {
            TokenUIObject.HideFear();
            if (onComplete != null)
            {
                onComplete.Invoke();
            }
        }

        public void ShowStealth(UnityAction onComplete)
        {
            TokenUIObject.ShowStealth();
            if (onComplete != null)
            {
                onComplete.Invoke();
            }
        }

        public void HideStealth(UnityAction onComplete)
        {
            TokenUIObject.HideStealth();
            if (onComplete != null)
            {
                onComplete.Invoke();
            }
        }

        public void ShowSilence(UnityAction onComplete)
        {
            UIManager.Instance.ShowSilenceOnMinion(this, delegate
            {
                TokenUIObject.ShowSilence();
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            });
        }

        public void HideSilence(UnityAction onComplete)
        {
            TokenUIObject.HideSilence();
            UIManager.Instance.HideSilenceOnMinion(this, onComplete);
        }

        public void ShowTaunt(UnityAction onComplete)
        {
            UIManager.Instance.ShowTauntOnMinion(this, delegate
            {
                TokenUIObject.ShowTaunt();
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            });
        }

        public void HideTaunt(UnityAction onComplete)
        {
            TokenUIObject.HideTaunt();
            if (onComplete != null)
            {
                onComplete.Invoke();
            }
        }

        public void ShowAura(UnityAction onComplete)
        {
            UIManager.Instance.ShowAuraOnMinion(this, delegate
            {
                TokenUIObject.ShowAura();
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            });
        }

        public void HideAura(UnityAction onComplete)
        {
            TokenUIObject.HideAura();
            if (onComplete != null)
            {
                onComplete.Invoke();
            }
        }

        public void ShowBerserk(UnityAction onComplete)
        {
            TokenUIObject.ShowBerserk();
            if (onComplete != null)
            {
                onComplete.Invoke();
            }
        }

        public void HideBerserk(UnityAction onComplete)
        {
            TokenUIObject.HideBerserk();
            if (onComplete != null)
            {
                onComplete.Invoke();
            }
        }

        public void ShowDarkMatter(UnityAction onComplete)
        {
            UIManager.Instance.ShowDarkMatterOnMinion(this, delegate
            {
                TokenUIObject.ShowDarkMatter();
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            });
        }

        public void HideDarkMatter(UnityAction onComplete)
        {
            TokenUIObject.HideDarkMatter();
            UIManager.Instance.HideDarkMatterOnMinion(this, onComplete);
        }

        public void ShowPierce(UnityAction onComplete)
        {
            TokenUIObject.ShowPierce();
            if (onComplete != null)
            {
                onComplete.Invoke();
            }
        }

        public void HidePierce(UnityAction onComplete)
        {
            TokenUIObject.HidePierce();
            if (onComplete != null)
            {
                onComplete.Invoke();
            }
        }

        public void PassiveActivate(AbilityFeedback abilityFeedback)
        {
            TokenUIObject.PassiveActivate(abilityFeedback);
        }

        public void SetTriggerDiscard()
        {
            _animator.SetTrigger("discard");
        }

        public void EffectActivating(bool isShow)
        {
            TokenUIObject.ShowEffectActivating(isShow);
        }
        #endregion

        #region Events
        public void SetHolder(Transform holder)
        {
            _holder = holder;
        }

        public Transform GetHolder()
        {
            return _holder;
        }

        public void SetHilight(bool isHilight)
        {
            _isHilight = isHilight;
        }

        public void SetDrag(bool isDrag)
        {
            _isDrag = isDrag;
        }

        public void SetUsed(bool isUsed)
        {
            _isUsed = isUsed;
        }

        public void SetInteractable(bool isInteractable)
        {
            if (isInteractable)
            {
                _interactableFlag--;

                if (_interactableFlag < 0)
                {
                    _interactableFlag = 0;
                }
            }
            else
            {
                _interactableFlag++;
            }
        }

        public void ForceSetInteractable(bool isInteractable)
        {
            if (isInteractable)
            {
                _interactableFlag = 0;
            }
            else
            {
                _interactableFlag = 1;
            }
        }

        public void SetSlotID(int slotID)
        {
            _slotID = slotID;
        }

        public int GetSlotID()
        {
            return _slotID;
        }

        public int GetUniqueID()
        {
            if (Data != null)
            {
                return Data.UniqueID;
            }

            return -1;
        }

        public void HilightToken()
        {
            if (IsInteractable && _isInBattle)
            {
                if (!_isHilight)
                {
                    if (CardUIManager.Instance.IsHilight)
                    {
                        CardUIManager.Instance.CancelHilight(true, false);
                    }
                    BoardManager.Instance.TokenIsHilight(this);
                }
            }
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (IsInteractable && _isInBattle)
            {
                if (_uiMode == UIMode.OBJECT)
                {
                    // Return when click on other hand card.
                    if (Data.CurrentZone == CardZone.Hand && !GameManager.Instance.IsLocalPlayer(Data.Owner))
                    {
                        return;
                    }
                    CardUIManager.Instance.OnCardEnter(this);
                }
                else if (_uiMode == UIMode.UI)
                {
                    //
                }
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (IsInteractable && _isInBattle)
            {
                if (_uiMode == UIMode.OBJECT)
                {
                    // Return when click on other hand card.
                    if (Data.CurrentZone == CardZone.Hand && !GameManager.Instance.IsLocalPlayer(Data.Owner))
                    {
                        return;
                    }
                    CardUIManager.Instance.OnCardDown(this);
                }
                else if (_uiMode == UIMode.UI)
                {
                    //
                }
            }
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (IsInteractable && _isInBattle)
            {
                if (_uiMode == UIMode.OBJECT)
                {
                    // Return when click on other hand card.
                    if (Data.CurrentZone == CardZone.Hand && !GameManager.Instance.IsLocalPlayer(Data.Owner))
                    {
                        return;
                    }
                    CardUIManager.Instance.OnCardUp(this);
                }
                else if (_uiMode == UIMode.UI)
                {
                    //
                }
            }
        }
        #endregion
    }
}
