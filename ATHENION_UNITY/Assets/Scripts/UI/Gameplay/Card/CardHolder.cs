﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class CardHolder : MonoBehaviour {

    #region Public Properties
    public float Angle;
    public bool InvertSort = false;
    public int CardCount = 5;
    public bool trigger = false;
    public float AnimationDuration = 1.5f;
    public float Distance;
    public GameObject CardHolderSlotPrefab;
    public List<Transform> HolderList { get {return _holderList; } }
    public static float CardSpacing = 0.05f;
    #endregion

    #region Private Properties
    private List<Transform> _holderList = new List<Transform>();
    private float _maxAngle;
    private float _minAngle;
    private float _maxDistance;
    private float _minDistance;
    #endregion

    #region Methods
    private void Start () {
        // Test
        //InitCard(CardCount); 
        //UpdateCardPosition();
    }

    private void Update()
    {
        /*
        if (GameHelper.IsATNVersion_RealDevelopment)
        {
            if (trigger)
            {
                trigger = !trigger;
                UpdateCardPosition();
            }
            if (Input.GetKeyDown(KeyCode.G))
            {
                InitCard(CardCount);
            }    
        }
        */
    }

    /// <summary>
    /// Init card slot position for sorting.
    /// </summary>
    /// <param name="cardCount">Amount of cards.</param>
    private void InitCard(int cardCount)
    {
        for (int i = 0; i < cardCount; i++)
        {
            GameObject obj = Instantiate(CardHolderSlotPrefab, transform);
            _holderList.Add(obj.transform);
        }
        UpdateCardPosition();
    }

    /// <summary>
    /// Create empty card slot.
    /// </summary>
    public void CreateCardSlot()
    {
        GameObject obj = Instantiate(CardHolderSlotPrefab, transform);
        _holderList.Add(obj.transform);
        UpdateCardPosition();
    }

    /// <summary>
    /// Remove card slot from list.
    /// </summary>
    /// <param name="indexList">Target index.</param>
    public void RemoveSlotAtIndex(int index)
    {
        Transform obj = _holderList[index];
        _holderList.Remove(obj);
        Destroy(obj.gameObject);
        
        UpdateCardPosition();
    }

    /// <summary>
    /// Update card slot position.
    /// </summary>
    private void UpdateCardPosition()
    {
        CardCount = _holderList.Count;
        _minAngle = Angle;
        _maxAngle = (-Angle);
        
        float deltaDistance = Distance / 2;
        _minDistance = (-deltaDistance);
        _maxDistance = deltaDistance;

        float stepAngle = ((_maxAngle - _minAngle) / (CardCount + 1));
        float stepDistance = (_maxDistance-_minDistance) / (CardCount + 1);
        //Debug.Log("Step Angle: " + stepAngle);
        //Debug.Log("Step Distance: " + stepDistance);
        for (int i = 0; i < _holderList.Count;i++)
        {
            if (i >= CardCount)
            {
                _holderList[i].gameObject.SetActive(false);
            }
            else
            {
                _holderList[i].gameObject.SetActive(true);
            }

            float startDistance;
            float targetDistance = 0;

            if (InvertSort)
            {
                startDistance = _maxDistance;
                targetDistance = 0.0f + startDistance - (stepDistance * (i + 1));
            }
            else
            {
                startDistance = _minDistance;
                targetDistance = 0.0f + startDistance + (stepDistance * (i + 1));
            }

            float targetAngle = 0.0f + _minAngle + (stepAngle * (i+1));
            //Debug.Log("Distance: " +i + " " + targetDistance);

            int resultY = 0;
            int cal = (CardCount - 1) - i;
            if (cal > i)
            {
                resultY = i;
            }
            else
            {
                resultY = cal;
            }
            float Zoffset = resultY * 0.1f;

            _holderList[i].transform.rotation = Quaternion.Euler(new Vector3(_holderList[i].transform.eulerAngles.x, 0, targetAngle));
            _holderList[i].transform.localPosition = new Vector3(targetDistance, (-CardSpacing * i),Zoffset);
            
        }
    }
	
    #endregion
}
