﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;
using TMPro;

namespace Karamucho.UI
{
    public class CardUIObject : MonoBehaviour
    {
        public GameObject Group;

        [Header("Game Stat")]
        public TextMeshPro TEXT_CardName;
        public TextMeshPro TEXT_Atk;
        public TextMeshPro TEXT_Hp;
        public TextMeshPro TEXT_Soul;
        public TextMeshPro TEXT_Armor;
        public TextMeshPro TEXT_Description;

        [Header("Icon Stat")]
        public SpriteRenderer ICON_ATK;
        public SpriteRenderer ICON_HP;
        public SpriteRenderer ICON_ARMOR;
        public SpriteRenderer ICON_SERIES;

        [Header("Artwork")]
        public GameObject GROUP_ARTWORK;
        public SpriteRenderer Artwork;
        public SpriteRenderer EffectRainbow;
        public SpriteRenderer FRAME;
        public SpriteRenderer FRAMEBOX;
        public SpriteRenderer Cross;

        public GameObject Foil;

        [Header("Card Back")]
        public Transform CardBackGroup;

        [Header("Glow")]
        public SpriteRenderer GlowEnemyHover;
        public SpriteRenderer GlowActive;
        public ParticleSystem P_GlowReady;

        [Header("Glow Color")]
        [SerializeField] private Color NormalColor;
        [SerializeField] private Color ChainColor;

        [Header("Arrow")]
        [SerializeField] private List<GameObject> ArrowDir = new List<GameObject>();
        [SerializeField] private List<GameObject> ArrowHilight = new List<GameObject>();

        [Header("Tutorial")]
        [SerializeField] private GameObject GROUP_TU_ARROW;
        [SerializeField] private GameObject PTU_Soul;
        [SerializeField] private GameObject PTU_ATK;
        [SerializeField] private GameObject PTU_HP;
        [SerializeField] private GameObject PTU_ARMOR;
        [SerializeField] private GameObject PTU_HINT;

        #region Methods

        private void Start()
        {
            ShowGlowActive(false);
            SetDiscard(false);
        }

        public void SetData(CardUIData data)
        {
            // Image 
            ShowDirection(data.CardDir);
            SetFrame(data);
            SetImage(data);
            SetFoil(data);
            SetCardBack(data);
            SetPackIcon(data);

            // Game Stat
            SetDescription(data);
            SetName(data.Name);
            SetAtk(data.ATKCurrent, data);
            SetHp(data.HPCurrent, data);
            SetSpirit(data.SpiritCurrent);
            SetArmor(data.ArmorCurrent, data);
        }

        public void SetDiscard(bool isDiscard)
        {
            Cross.gameObject.SetActive(isDiscard);
        }

        public void SetName(string message)
        {
            TEXT_CardName.text = message;
        }

        public void SetAtk(int value, CardUIData data)
        {
            Color color = new Color();
            if (value < data.ATKBase)
            {
                color = GameHelper.GetColor_Number_Decrease();
            }
            else
            {
                if (data.ATKBase > data.ATKDefault)
                {
                    color = GameHelper.GetColor_Number_Increase();
                }
                else
                {
                    color = GameHelper.GetColor_Number_Normal();
                }
            }

            TEXT_Atk.text = value.ToString();
            TEXT_Atk.color = color;
        }

        public void SetArmor(int value, CardUIData data)
        {
            if (data.ArmorCurrent <= 0)
            {
                TEXT_Armor.gameObject.SetActive(false);
                ICON_ARMOR.gameObject.SetActive(false);
            }
            else
            {
                TEXT_Armor.gameObject.SetActive(true);
                ICON_ARMOR.gameObject.SetActive(true);
            }

            Color color = new Color();
            if (value < data.ArmorBase)
            {
                color = GameHelper.GetColor_Number_Decrease();
            }
            else
            {
                if (data.ArmorBase > data.ArmorDefault)
                {
                    GameHelper.GetColor_Number_Increase();
                }
                else
                {
                    color = GameHelper.GetColor_Number_Normal();
                }
            }
            TEXT_Armor.text = value.ToString();
            TEXT_Armor.color = color;
        }

        public void SetHp(int value, CardUIData data)
        {
            Color color = new Color();
            if (value < data.HPBase)
            {
                color = GameHelper.GetColor_Number_Decrease();
            }
            else
            {
                if (data.HPBase > data.HPDefault)
                {
                    color = GameHelper.GetColor_Number_Increase();
                }
                else
                {
                    color = GameHelper.GetColor_Number_Normal();
                }
            }
            TEXT_Hp.text = value.ToString();
            TEXT_Hp.color = color;
        }

        public void SetSpirit(int value)
        {
            TEXT_Soul.text = value.ToString();
            //Spirit.text = GameHelper.GetIntToRomanNumber(value).ToString();
        }

        public void SetDescription(CardUIData data)
        {
            TEXT_Description.text = data.Description;
            SetDynamicBox(data);
        }

        public void SetDynamicBox(CardUIData data)
        {
            RectTransform rectText = TEXT_Description.GetComponent<RectTransform>();
            float offset = 2.0f;
            float minSize = 4.0f;
            float boxHeight = TEXT_Description.preferredHeight + offset;
            if (boxHeight > minSize)
            {
                FRAMEBOX.GetComponent<SpriteRenderer>().size = new Vector2(FRAMEBOX.size.x, boxHeight);
                rectText.sizeDelta = new Vector2(rectText.sizeDelta.x, TEXT_Description.preferredHeight);
            }
            else
            {
                FRAMEBOX.GetComponent<SpriteRenderer>().size = new Vector2(FRAMEBOX.size.x, minSize);
                rectText.sizeDelta = new Vector2(rectText.sizeDelta.x, rectText.sizeDelta.y);
            }
           
        }

        private void SetFrame(CardUIData data)
        {
            Sprite tempSprite;
            Sprite tempSpriteMask;
            Sprite frameboxSprite;
            CardResourceManager.LoadCardFrame(data.Rarity, data.Element.ToCardElementType(), out tempSprite);
            CardResourceManager.LoadCardMaskFrame(data.Rarity, data.Element.ToCardElementType(), out tempSpriteMask);
            CardResourceManager.LoadCardFrameBox(data.Rarity, out frameboxSprite);
            FRAME.sprite = tempSprite;
            FRAMEBOX.sprite = frameboxSprite;
            Artwork.sprite = tempSpriteMask;
        }

        private void SetPackIcon(CardUIData data)
        {
            Sprite sprite;
            CardResourceManager.LoadSeriesIcon(data.SeriesKey, out sprite);

            ICON_SERIES.sprite = sprite;
        }

        public void ShowGlowActive(bool isShow, bool isChain = false)
        {
            if (GlowActive == null) return;
            // Setup Color
            Color normalColor = NormalColor;
            Color chainColor = ChainColor;

            // Set Show
            GlowActive.gameObject.SetActive(isShow);

            // Set Is Chain
            if (P_GlowReady != null)
            {
                ParticleSystem ps = P_GlowReady.GetComponent<ParticleSystem>();
                ParticleSystem.MainModule pm = ps.main;
                pm.startColor = isChain ? chainColor : normalColor;
            }
            GlowActive.color = isChain ? chainColor : normalColor;


        }

        public void ShowDirection(CardDirection dirValue)
        {
            foreach (CardDirectionType item in System.Enum.GetValues(typeof(CardDirectionType)))
            {
                bool isShow = dirValue.IsDirection(item);
                ArrowDir[(int)item].SetActive(isShow);
                ArrowHilight[(int)item].SetActive(isShow);
            }
        }

        private void SetFoil(CardUIData data)
        {
            Foil.SetActive(data.IsFoil);
        }

        private void SetImage(CardUIData data)
        {
            string path = "Images/Cards/ArtCardImages/ART_CARD_" + data.ImageKey;
            Texture texture = ResourceManager.Load(path) as Texture;

            if (texture != null)
            {
                Artwork.material.SetTexture("_NewTex_1", texture);
            }
            else
            {
                path = "Images/Cards/ArtCardImages/ART_CARD_" + "IM0000";
                texture = ResourceManager.Load(path) as Texture;
                Artwork.material.SetTexture("_NewTex_1", texture);
            }

            /*
            // Clear previous image
            foreach (Transform child in ArtworkGroup.transform)
            {
                Destroy(child.gameObject);
            }

            GameObject obj;
            if(CardResourceManager.Create(CardResourceManager.ImageType.Card, data.ImageKey, ArtworkGroup.transform, false, out obj))
            {
                obj.transform.localPosition = Vector3.zero;
                obj.transform.localRotation = Quaternion.identity;
            }
            */
        }

        private void SetCardBack(CardUIData data)
        {
            string path = "Prefabs/CardBacks/Prefabs/CardBack_" + data.CardBackID;
            GameObject obj;
            obj = ResourceManager.Load(path) as GameObject;

            if (obj == null)
            {
                //Debug.LogWarning("Cannot find cardback prefab:" + data.CardBackID);
                // Error path
                path = "Prefabs/CardBacks/Prefabs/CardBack_CBDEFAULT";
                obj = ResourceManager.Load(path) as GameObject;

                GameObject cardback = Instantiate(obj, CardBackGroup);

                // Create failsafe for cardback.
                SpriteRenderer mr = cardback.GetComponent<SpriteRenderer>();
                mr.sprite = SpriteResourceHelper.LoadSprite_CardBack(data.CardBackID);
            }
            else
            {
                Instantiate(obj, CardBackGroup);
            }
        }

        public void ShowCard(bool isShow)
        {
            Group.SetActive(isShow);
        }
        #endregion

        #region Tutorial
        public void Tutorial_HilightArrow(bool isShow)
        {
            GROUP_TU_ARROW.SetActive(isShow);
        }

        public void Tutorial_HilightSoul(bool isShow)
        {
            PTU_Soul.SetActive(isShow);
        }

        public void Tutorial_HilightATK(bool isShow)
        {
            PTU_ATK.SetActive(isShow);
        }

        public void Tutorial_HilightHP(bool isShow)
        {
            PTU_HP.SetActive(isShow);
        }

        public void Tutorial_HilightARMOR(bool isShow)
        {
            PTU_ARMOR.SetActive(isShow);
        }

        public void Tutorial_HintPoniter(bool isShow, Color color)
        {
            PTU_HINT.SetActive(isShow);

            ParticleSystem ps = PTU_HINT.GetComponent<ParticleSystem>();
            ParticleSystem.MainModule em = ps.main;
            em.startColor = color;
            ps.Play();
            em.playOnAwake = true;
        }

        #endregion
    }
}
