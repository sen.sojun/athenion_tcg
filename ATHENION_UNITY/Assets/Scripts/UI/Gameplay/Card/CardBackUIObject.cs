﻿using Karamucho;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardBackUIObject : MonoBehaviour {

    public GameObject Glow;
    public GameObject EnemyHoverGlow;

    [Header("Card Back")]
    public Transform CardBackGroup;

    public void SetGlow(bool isShow)
    {
        Glow.SetActive(isShow);
    }

    public void SetEnemyHoverGlow(bool isShow)
    {
        EnemyHoverGlow.SetActive(isShow);
    }

    public void SetCardBack(CardUIData data)
    {
        string path = "Prefabs/CardBacks/Prefabs/CardBack_" + data.CardBackID;
        GameObject obj;
        obj = ResourceManager.Load(path) as GameObject;

        if (obj == null)
        {
            Debug.LogWarning("Cannot find cardback prefab:" + data.CardBackID);
            // Error path
            path = "Prefabs/CardBacks/Prefabs/CardBack_CBDEFAULT";
            obj = ResourceManager.Load(path) as GameObject;

            GameObject cardback = Instantiate(obj, CardBackGroup);

            // Create failsafe for cardback.
            SpriteRenderer mr = cardback.GetComponent<SpriteRenderer>();
            mr.sprite = SpriteResourceHelper.LoadSprite_CardBack(data.CardBackID);
        }
        else
        {
            Instantiate(obj, CardBackGroup);
        }
       
    }
}
