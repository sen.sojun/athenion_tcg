﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

public class CardPopup : PopupItem {

    public TextMeshPro NumberText;
    private UnityAction _onComplete;
    private GameObject _obj;

    public void SetAnimationComplete(UnityAction onComplete)
    {
        _onComplete = onComplete;
    }

    public void SetNumber(int value)
    {
        NumberText.text = value.ToString();
    }

    public void PlayPopup()
    {
        GetComponent<Animation>().Play();
    }

    public void OnComplete()
    {
        if (_obj != null)
        {
            _obj.transform.SetParent(null);
            _obj = null;
        }

        if (_onComplete != null)
        {
            _onComplete.Invoke();
        }
    }
}
