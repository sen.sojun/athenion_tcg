﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class CardUIData
{
    #region Public Properties
    public int UniqueID { get; private set; }

    // unchangeable data
    public string BaseID { get; private set; }
    public string FullID { get; private set; }
    public string Name { get; private set; }
    public string FlavorText { get; private set; }
    public string Description { get; private set; }
    public string ImageKey { get; private set; }
    public string CardBackID { get; private set; }
    public CardRarity Rarity { get; private set; }
    public CardType Type { get; private set; }
    public CardElement Element { get; private set; }
    public CardStatus CardStatus { get; private set; }
    public string SpawnEffectKey { get; private set; }
    public string AttackEffectKey { get; private set; }
    public string DeathEffectKey { get; private set; }
    public string InfoScriptKey { get; private set; }
    public string SeriesKey { get; private set; }
    public bool IsFoil { get; private set; }

    public List<AbilityFeedback> FeedbackList { get { return _feedbackList; } }
    public List<AbilityKeyword> KeywordList { get { return _abilityKeywordList; } }

    // changeable data
    public CardDirection CardDir { get; private set; }
    public int HPBase { get; private set; }
    public int ATKBase { get; private set; }
    public int SpiritBase { get; private set; }
    public int ArmorBase { get; private set; }

    public int HPCurrent { get; private set; }
    public int ATKCurrent { get; private set; }
    public int SpiritCurrent { get; private set; }
    public int ArmorCurrent { get; private set; }

    public int HPDefault { get; private set; }
    public int ATKDefault { get; private set; }
    public int SpiritDefault { get; private set; }
    public int ArmorDefault { get; private set; }

    // battle data
    public int SlotID { get; private set; }
    public PlayerIndex Owner { get; private set; }
    public CardZone CurrentZone { get; private set; }
    #endregion

    #region Private Properties
    private List<AbilityFeedback> _feedbackList;
    private List<AbilityKeyword> _abilityKeywordList;
    #endregion

    #region Constructor
    /*
    public CardUIData(BattleCardData battleCardData) : this(battleCardData, "")
    {
        if (GameHelper.IsHaveObject(typeof(GameManager)))
        {
            SetCardBackID(GameManager.Instance.Data.PlayerDataList[(int)battleCardData.Owner].CardBackID);
        }
        else
        {
            SetCardBackID(DataManager.Instance.DefaultCardBackIDList[0]);
        }
    }
    */

    public CardUIData(BattleCardData battleCardData)
    {
        UniqueID = battleCardData.UniqueID;
        BaseID = battleCardData.BaseID;
        FullID = battleCardData.FullID;
        Name = battleCardData.Name;
        //FlavorText = battleCardData.FlavorText;
        Description = battleCardData.Description;
        ImageKey = battleCardData.ImageKey;
        Rarity = battleCardData.Rarity;
        Type = battleCardData.Type;
        Element = battleCardData.Element;
        InfoScriptKey = battleCardData.InfoScriptKey;
        SeriesKey = battleCardData.SeriesKey;
        IsFoil = battleCardData.IsFoil;
        CardBackID = battleCardData.CardBackID;

        SlotID = battleCardData.SlotID;
        Owner = battleCardData.Owner;
        CurrentZone = battleCardData.CurrentZone;

        switch (Type)
        {
            case CardType.Minion:
            case CardType.Minion_NotInDeck:
            case CardType.Hero:
            {
                MinionData minion = battleCardData as MinionData;
                CardStatus = new CardStatus(minion.GetCardStatus());
                CardDir = minion.CardDir;

                HPDefault = minion.HPDefault;
                ATKDefault = minion.ATKDefault;
                SpiritDefault = minion.SpiritDefault;
                ArmorDefault = minion.ArmorDefault;

                HPBase = minion.HPMax;
                ATKBase = minion.ATKCurrent;
                SpiritBase = minion.SpiritCurrent;
                ArmorBase = minion.ArmorCurrent;

                HPCurrent = minion.HPCurrent;
                ATKCurrent = minion.ATKCurrent;
                SpiritCurrent = minion.SpiritCurrent;
                ArmorCurrent = minion.ArmorCurrent;

                SpawnEffectKey = minion.SpawnEffectKey;
                AttackEffectKey = minion.AttackEffectKey;
                DeathEffectKey = minion.DeathEffectKey;

                SetAbilityFeedback(minion.FeedbackList);
                SetAbilityKeyword(minion.GetAbilityKeywordList());
            }
            break;

            case CardType.Spell:
            case CardType.Spell_NotInDeck:
            {
                SpellData spell = battleCardData as SpellData;
                CardStatus = new CardStatus();
                CardDir = new CardDirection();

                HPDefault = 0;
                ATKDefault = 0;
                SpiritDefault = 0;
                ArmorDefault = 0;

                HPBase = 0;
                ATKBase = 0;
                SpiritBase = spell.RawData.Spirit;
                ArmorBase = spell.RawData.Armor;

                HPCurrent = 0;
                ATKCurrent = 0;
                SpiritCurrent = spell.RawData.Spirit;
                ArmorCurrent = 0;

                SpawnEffectKey = "";
                AttackEffectKey = "";
                DeathEffectKey = "";

                SetAbilityFeedback(spell.FeedbackList);
                SetAbilityKeyword(spell.GetAbilityKeywordList());
            }
            break;
        }
    }

    public CardUIData(int uniqueID, CardData cardData, string cardBackID)
    {
        UniqueID = uniqueID;
        BaseID = cardData.BaseID;
        FullID = cardData.FullID;
        Name = cardData.Name;
        FlavorText = cardData.FlavorText;
        Description = cardData.Description;
        ImageKey = cardData.ImageKey;
        Rarity = cardData.Rarity;
        Type = cardData.Type;
        Element = cardData.CardElement;
        CardStatus = new CardStatus();
        CardDir = cardData.CardDir;

        HPBase = cardData.HP;
        ATKBase = cardData.ATK;
        SpiritBase = cardData.Spirit;
        ArmorBase = cardData.Armor;

        HPCurrent = cardData.HP;
        ATKCurrent = cardData.ATK;
        SpiritCurrent = cardData.Spirit;
        ArmorCurrent = cardData.Armor;

        HPDefault = cardData.HP;
        ATKDefault = cardData.ATK;
        SpiritDefault = cardData.Spirit;
        ArmorDefault = cardData.Armor;

        SlotID = -1;
        Owner = PlayerIndex.One;
        CurrentZone = CardZone.Deck;

        SpawnEffectKey = cardData.SpawnEffectKey;
        AttackEffectKey = cardData.AttackEffectKey;
        DeathEffectKey = cardData.DeathEffectKey;
        InfoScriptKey = cardData.InfoScriptKey;
        SeriesKey = cardData.SeriesKey;
        IsFoil = cardData.IsFoil;

        CardBackID = cardBackID;

        SetAbilityFeedback(cardData.FeedbackList);
        SetAbilityKeyword(cardData.GetAbilityKeywordList());
    }

    public CardUIData(CardUIData data)
    {
        UniqueID = data.UniqueID;
        BaseID = data.BaseID;
        FullID = data.FullID;
        Name = data.Name;
        FlavorText = data.FlavorText;
        Description = data.Description;
        ImageKey = data.ImageKey;
        Rarity = data.Rarity;
        Type = data.Type;
        Element = data.Element;
        CardStatus = data.CardStatus;
        CardDir = data.CardDir;

        HPBase = data.HPBase;
        ATKBase = data.ATKBase;
        SpiritBase = data.SpiritBase;
        ArmorBase = data.ArmorBase;

        HPCurrent = data.HPCurrent;
        ATKCurrent = data.ATKCurrent;
        SpiritCurrent = data.SpiritCurrent;
        ArmorCurrent = data.ArmorCurrent;

        HPDefault = data.HPDefault;
        ATKDefault = data.ATKDefault;
        SpiritDefault = data.SpiritDefault;
        ArmorDefault = data.ArmorDefault;

        SlotID = data.SlotID;
        Owner = data.Owner;
        CurrentZone = data.CurrentZone;

        SpawnEffectKey = data.SpawnEffectKey;
        AttackEffectKey = data.AttackEffectKey;
        DeathEffectKey = data.DeathEffectKey;
        InfoScriptKey = data.InfoScriptKey;
        SeriesKey = data.SeriesKey;
        IsFoil = data.IsFoil;

        CardBackID = data.CardBackID;

        SetAbilityFeedback(data._feedbackList);
        SetAbilityKeyword(data._abilityKeywordList);
    }

    public CardUIData(int uniqueID, CardUIData data) : this(data)
    {
        UniqueID = uniqueID;
    }
    #endregion

    #region Methods
    public void SetCardBackID(string cardBackID)
    {
        CardBackID = cardBackID;
    }

    public bool IsAbilityFeedback(AbilityFeedback abilityFeedback)
    {
        if (_feedbackList != null)
        {
            return (_feedbackList.Contains(abilityFeedback));
        }

        return false;
    }

    public bool IsAbilityKeyword(AbilityKeyword abilityKeyword)
    {
        if (_abilityKeywordList != null)
        {
            return (_abilityKeywordList.Contains(abilityKeyword));
        }

        return false;
    }

    public int GetAbilityKeywordCount()
    {
        if(_abilityKeywordList == null)
        {
            _abilityKeywordList = new List<AbilityKeyword>();
        }

        int count = 0;
        foreach (AbilityKeyword keyword in _abilityKeywordList)
        {
            switch (keyword)
            {
                case AbilityKeyword.Trigger:
                continue;
            }
            count++;
        }
        if (InfoScriptKey != "" && InfoScriptKey != "-")
        {
            count++;
        }

        return count;
    }

    public bool IsMinionStatus(CardStatusType status)
    {
        if (CardStatus != null)
        {
            return (CardStatus.IsCardStatus(status));
        }

        return false;
    }

    public bool IsCanActiveChain()
    {
        if (SceneManager.GetActiveScene().name == GameHelper.BattleSceneName)
        {
            BattleCardData battleCard;
            if (GameManager.Instance.FindBattleCard(this.UniqueID, out battleCard))
            {
                return battleCard.IsCanActiveChain();
            }
        }

        return false;
    }

    public bool IsCanActiveChain(int chainIndex)
    {
        if (SceneManager.GetActiveScene().name == GameHelper.BattleSceneName)
        {
            BattleCardData battleCard;
            if (GameManager.Instance.FindBattleCard(this.UniqueID, out battleCard))
            {
                return battleCard.IsCanActiveChain(chainIndex);
            }
        }

        return false;
    }

    private void SetAbilityFeedback(List<AbilityFeedback> abilityFeedbackList)
    {
        _feedbackList = new List<AbilityFeedback>(abilityFeedbackList);
    }

    private void SetAbilityKeyword(List<AbilityKeyword> abilityKeywordList)
    {
        _abilityKeywordList = new List<AbilityKeyword>(abilityKeywordList);
    }
    #endregion
}