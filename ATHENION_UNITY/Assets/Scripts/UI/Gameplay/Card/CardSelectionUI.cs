﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;
using Karamucho.UI;
using UnityEngine.Events;
using System;
using UnityEngine.EventSystems;

public class CardSelectionUI : MonoBehaviour  {

    #region Public Properties
    [Header("Prefab")]
    [SerializeField] private CardUI_CardSelectionCell _Prefab_CardUISelection;

    [Header("Ref")]
    [SerializeField] private Transform _GROUP_Container;
    [SerializeField] private ScrollRect GROUP_Scroll;
    [SerializeField] private TextMeshProUGUI _TEXT_Title;
    [SerializeField] private TextMeshProUGUI _TEXT_SubTitle;

    [Header("BTN")]
    [SerializeField] private Button _BTN_Confirm;
    #endregion

    #region Private Propereties
    private int _cardRequireCount = 0;
    private int _cardSelectedCount = 0;
    private List<CardUIData> _cardDataList = new List<CardUIData>();
    private List<CardUI_CardSelectionCell> _cardList = new List<CardUI_CardSelectionCell>();
    private List<int> _redrawList = new List<int>();
    private UnityAction<PlayerIndex, List<int>> _onComfirm;
    #endregion

    #region Method
    public void ShowCard(string title ,string subTitle, List<CardUIData> cardDatas, int require, UnityAction<PlayerIndex,List<int>> onComfirm)
    {
        _cardSelectedCount = 0;
        _cardRequireCount = require;
        _redrawList = new List<int>();
        _cardDataList = cardDatas;
        _onComfirm = onComfirm;
        _TEXT_Title.text = title;
        _TEXT_SubTitle.text = subTitle;

        InitBTN();
        InitCard(_cardDataList);

        GameHelper.UITransition_FadeIn(this.gameObject);

        // Don't need to select but still can select card
        _BTN_Confirm.gameObject.SetActive(_cardRequireCount == 0);

    }

    public void HideCard()
    {
        DeinitBTN();
        GameHelper.UITransition_FadeOut(this.gameObject);
    }

    private void DeinitBTN()
    {
        _BTN_Confirm.onClick.RemoveAllListeners();
    }

    private void InitBTN()
    {
        _BTN_Confirm.onClick.RemoveAllListeners();
        _BTN_Confirm.onClick.AddListener(delegate
        {
            _onComfirm?.Invoke(GameManager.Instance.GetLocalPlayerIndex(), _redrawList);
            HideCard();
            DeinitBTN();
        });
    }

    private void InitCard(List<CardUIData> cardDatas)
    {
        ResetCard();

        for (int i = 0; i < cardDatas.Count; i++)
        {
            if (_cardList.Count <= i)
            {
                GameObject obj = Instantiate(_Prefab_CardUISelection.gameObject, _GROUP_Container);
                CardUI_CardSelectionCell card = obj.GetComponent<CardUI_CardSelectionCell>();
                card.SetData(cardDatas[i]);
                // Event
                card.OnCardClick += OnCard_Click;
                //card.OnCardDown += OnCard_Click;
                card.OnBeginDragEvent += OnCard_Scroll_BeginDrag;
                card.OnDragEvent += OnCard_Scroll_Drag;
                card.OnEndDragEvent += OnCard_Scroll_EndDrag;

                _cardList.Add(card);
            }
            else
            {
                _cardList[i].SetData(cardDatas[i]);
                _cardList[i].gameObject.SetActive(true);
            }
        }

    }

    private void ResetCard()
    {
        foreach (CardUI_CardSelectionCell item in _cardList)
        {
            item.gameObject.SetActive(false);
            item.SetSelected(false, null);
        }
    }

    public void AddRedraw(int uniqueId)
    {
        _redrawList.Add(uniqueId);
        _cardSelectedCount = _redrawList.Count;
        CheckRequireCount();
    }

    public void RemoveRedraw(int uniqueId)
    {
        if (_redrawList.Contains(uniqueId))
        {
            _redrawList.Remove(uniqueId);
            _cardSelectedCount = _redrawList.Count;
            CheckRequireCount();
        }


    }

    private void CheckRequireCount()
    {
        if (_cardRequireCount == 0)
        {
            return;
        }
        else
        {
            if ((_cardSelectedCount == _cardRequireCount || _cardSelectedCount == _cardDataList.Count) && _cardRequireCount > 0)
            {
                _BTN_Confirm.gameObject.SetActive(true);
            }
            else
            {
                _BTN_Confirm.gameObject.SetActive(false);
            }
        }


    }

    public int GetCardRequireCount()
    {
        return _cardRequireCount;
    }

    public List<int> GetRedrawList()
    {
        return _redrawList;
    }
    #endregion

    #region Events
    private void OnCard_Click(CardUI_CardSelectionCell card)
    {
        if (card.GetSelected() == false)
        {
            if (_redrawList.Count < _cardRequireCount || _cardRequireCount == 0)
            {
                card.SetSelected(true, delegate
                {
                    UIManager.Instance.CardSelectionUI.AddRedraw(card.Data.UniqueID);
                    SoundManager.PlaySFX(SoundManager.SFX.Draw_Card);
                });
            }
        }
        else
        {
            card.SetSelected(false, delegate
            {
                UIManager.Instance.CardSelectionUI.RemoveRedraw(card.Data.UniqueID);
                SoundManager.PlaySFX(SoundManager.SFX.Cancel_Click);
            });
        }
    }

    private void OnCard_Scroll_BeginDrag(PointerEventData eventData)
    {
        GROUP_Scroll.OnBeginDrag(eventData);
    }

    private void OnCard_Scroll_Drag(PointerEventData eventData)
    {
        GROUP_Scroll.OnDrag(eventData);
    }

    private void OnCard_Scroll_EndDrag(PointerEventData eventData)
    {
        GROUP_Scroll.OnEndDrag(eventData);
    }
    #endregion

}
