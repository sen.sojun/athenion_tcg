﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;
namespace Karamucho.UI
{
    public class BoardManager : MonoSingleton<BoardManager>
    {
        public class SelectSlotEvent : UnityEvent<List<int>>
        {
        }

        #region Public Properties
        public List<Transform> RowList;
        public Transform DiscardTarget;
        public BoardSlotHolder CurrentSlot { get { return _currentSlot; } }
        public bool IsHilight { get { return _isHilight; } }
        #endregion

        #region Private Properties
        private List<BoardSlotHolder> _slotHolderList = new List<BoardSlotHolder>();
        private List<CardUI> _tokenList = new List<CardUI>();
        private RaycastHit _raycastHit;
        private BoardSlotHolder _currentSlot;
        private CardUI _currentHilight;
        private bool _isHilight;
        private bool _isShowSelectSlot = false;

        // Select Slot Section
        private List<int> _selectSlotList;
        private List<int> _selectedList = new List<int>();
        private List<int> _DeactivatedSlotList = new List<int>();
        private int _selectCount = 0;
        private SelectSlotEvent _onSelectedComplete = new SelectSlotEvent();
        private UnityAction<int> _onSelectSlot;
        #endregion

        #region Methods
        private void Start()
        {
            // Test Init.
            InitSlotList();
        }

        private void Update()
        {
            RaycastHilightSlot();
        }

        /// <summary>
        /// Use to Init list of slot on the board with reference from RowList.
        /// </summary>
        private void InitSlotList()
        {
            // Check if list is valid.
            if (RowList.Count == 0)
            {
                Debug.LogWarning("<color=red><b>INIT SLOT IS INVALID.</b></color>");
                return;
            }

            // Loop at into list.
            for (int i = 0; i < RowList.Count; i++)
            {
                foreach (Transform item in RowList[i])
                {
                    _slotHolderList.Add(item.GetComponent<BoardSlotHolder>().InitSlotID(_slotHolderList.Count));
                }
            }
            //Debug.Log("<b>Init Slot total :" + _slotHolderList.Count + "</b>");
        }

        /// <summary>
        /// Use to Move minion token to target slot.
        /// </summary>
        /// <param name="token">Current minion token.</param>
        /// <param name="targetPosition">Target slot position.</param>
        public void MoveTo(CardUIData target, int slotID, UnityAction onComplete, bool isJoin = false)
        {
            CardUI currentToken = _tokenList.Find(x => x.GetUniqueID() == target.UniqueID);
            Vector3 targetPosition = _slotHolderList.Find(y => y.GetSlotID() == slotID).GetHolderPosition();
            currentToken.SetSlotID(slotID);

            SoundManager.PlaySFX(SoundManager.SFX.Minion_Move);
            Sequence sq = DOTween.Sequence();
            sq.Append(currentToken.transform.DOLocalMoveY(1.0f, 0.5f).SetEase(Ease.InOutExpo));
            sq.Append(currentToken.transform.DOMove(targetPosition, 0.5f).SetEase(Ease.OutExpo));
            sq.OnComplete(delegate
            {
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            });
        }

        public bool FindTokenById(int uniqueID, out CardUI cardUI)
        {
            CardUI card = _tokenList.Find(x => x.GetUniqueID() == uniqueID);

            if (card != null)
            {
                cardUI = card;
                return true;
            }
            else
            {
                cardUI = null;
                return false;
            }
        }

        private void RaycastHilightSlot()
        {
            if (_raycastHit.collider != null && CardUIManager.Instance.IsSelected && CardUIManager.Instance.IsDown)
            {
                if (_raycastHit.collider.gameObject.tag == "BoardSlot")
                {
                    if (CardUIManager.Instance.GetCurrentCard() != null)
                    {
                        if (CardUIManager.Instance.GetCurrentCard().Data.Type == CardType.Minion || CardUIManager.Instance.GetCurrentCard().Data.Type == CardType.Minion_NotInDeck)
                        {
                            // Clear previous hilight
                            if (_currentSlot != null)
                            {
                                if (_currentSlot != _raycastHit.collider.GetComponent<BoardSlotHolder>())
                                {
                                    _currentSlot.ShowHilight(false);
                                    _currentSlot = _raycastHit.collider.GetComponent<BoardSlotHolder>();
                                    HideAllHilight();
                                }
                            }
                            else
                            {
                                _currentSlot = _raycastHit.collider.GetComponent<BoardSlotHolder>();
                            }

                            if (_currentSlot != null)
                            {
                                if (UIManager.Instance.IsCardCanSpawn(_currentSlot.GetSlotID(), CardUIManager.Instance.GetCurrentCard().Data.UniqueID))
                                {
                                    CardUI token = _tokenList.Find(x => x.GetSlotID() == _currentSlot.GetSlotID());
                                    // Check slot is usable by ask UImanager.
                                    _currentSlot.ShowHilight(true);
                                    ShowSlotByCurrentSlotWithDir(_currentSlot.GetSlotID(), CardUIManager.Instance.GetCurrentCard().Data.CardDir);
                                }
                            }
                        }
                    }

                }
                else
                {
                    if (_currentSlot != null)
                    {
                        _currentSlot.ShowHilight(false);
                        HideAllHilight();
                        _currentSlot = null;
                    }
                }
            }
        }

        public void AttackTo(CardUIData attacker, CardUIData defender, List<CardUIData> defList, int baseDamage, bool isCritical, UnityAction onComplete)
        {
            CardUI atkToken = _tokenList.Find(x => x.GetUniqueID() == attacker.UniqueID);
            CardUI defToken = _tokenList.Find(x => x.GetUniqueID() == defender.UniqueID);

            List<CardUI> defTokenList = new List<CardUI>();
            foreach (CardUIData item in defList)
            {
                CardUI card;
                if (card = _tokenList.Find(x => x.GetUniqueID() == item.UniqueID))
                {
                    defTokenList.Add(card);
                }
                else
                {
                    Debug.LogWarning("Cannot find card in board :" + item.UniqueID);
                }
            }

            Sequence sq = DOTween.Sequence();
            Vector3 attackerPosition = GetSlotHolder(attacker.SlotID).GetHolderPosition();
            Vector3 defenderPosition = GetSlotHolder(defender.SlotID).GetHolderPosition();
            float distance = (defenderPosition - attackerPosition).magnitude;
            Vector3 dir = (defenderPosition - attackerPosition).normalized;
            Vector3 attackOffset = attackerPosition + (dir * distance * 0.5f);

            sq.Append(atkToken.transform.DOLocalMoveY(0.5f, 0.3f).SetEase(Ease.InExpo).OnComplete(delegate
            {
                SoundManager.PlaySFX(SoundManager.SFX.Minion_Attack);
            }));
            sq.Append(atkToken.transform.DOMove(attackOffset, 0.1f).SetEase(Ease.InExpo).OnComplete(delegate
            {
                if (isCritical) SoundManager.PlaySFX(SoundManager.SFX.Ability_Backstab_In);

                UIEffectManager.Instance.SpawnAttackVFX(attacker.AttackEffectKey, attackOffset, null, dir, defTokenList.Count);
                UIManager.Instance.CameraScreenShake(baseDamage);
                UIManager.Instance.ShowPopupAttackOnCard(baseDamage, defender, isCritical, null);
                UIManager.Instance.UpdateCardUI(defender, null);

                // Attack Pierce System
                if (defList.Count > 0)
                {
                    foreach (CardUIData item in defList)
                    {
                        UIManager.Instance.ShowPopupAttackOnCard(baseDamage, item, isCritical, null);
                        UIManager.Instance.UpdateCardUI(item, null);
                    }
                }
            }));
            sq.Append(atkToken.transform.DOMove(attackerPosition, 0.15f).SetEase(Ease.OutExpo));
            sq.Join(defToken.transform.DOShakePosition(0.15f, 0.2f, 5, 5).SetEase(Ease.OutExpo));
            sq.Join(defToken.transform.DOShakeRotation(0.15f, 5, 5, 10).SetEase(Ease.OutExpo));
            sq.OnComplete(delegate
            {
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            });
        }

        private bool DestroyTokenAtIndex(int index, UnityAction onComplete)
        {
            if (_tokenList.Count > 0)
            {
                CardUI obj = _tokenList[index];
                _tokenList.Remove(obj);
                obj.AnimationDestroyToken(DiscardTarget, onComplete);
                return true;
            }

            return false;
        }

        public bool DestroyTokenByID(int uniqueID, UnityAction onComplete)
        {
            if (_tokenList.Count > 0)
            {
                CardUI obj = _tokenList.Find(x => x.Data.UniqueID == uniqueID);
                if (obj != null)
                {
                    _tokenList.Remove(obj);
                    obj.AnimationDestroyToken(DiscardTarget, onComplete);
                    return true;
                }
                else
                {
                    Debug.LogError("DestroyTokenByID: [Error] Not found token. " + uniqueID);
                }
            }
            else
            {
                Debug.LogError("DestroyTokenByID: [Error] _tokenList is empty.");
            }

            if (onComplete != null)
            {
                onComplete.Invoke();
            }
            return false;
        }

        public bool BanishTokenByID(int uniqueID, UnityAction onComplete)
        {
            if (_tokenList.Count > 0)
            {
                CardUI obj = _tokenList.Find(x => x.Data.UniqueID == uniqueID);
                if (obj != null)
                {
                    _tokenList.Remove(obj);
                    obj.AnimationBanishToken(DiscardTarget, onComplete);
                    return true;
                }
                else
                {
                    Debug.LogError("BanishTokenByID: [Error] Not found token. " + uniqueID);
                }
            }
            else
            {
                Debug.LogError("BanishTokenByID: [Error] _tokenList is empty.");
            }

            if (onComplete != null)
            {
                onComplete.Invoke();
            }
            return false;
        }

        public void MoveTokenToHandByID(int uniqueID, CardUIManager.PlayerPosition playerPosition, UnityAction onComplete)
        {
            if (_tokenList.Count > 0)
            {
                CardUI obj = _tokenList.Find(x => x.Data.UniqueID == uniqueID);
                if (obj != null)
                {
                    _tokenList.Remove(obj);
                    obj.SetUsed(false);
                    obj.TokenUIObject.ResetPassiveIcon();
                    CardUIManager.Instance.MoveTokenToHand(playerPosition, obj, onComplete);
                }
            }
        }

        public void ShowLockSlotBySlotID(int index, UnityAction onComplete)
        {
            _slotHolderList[index].ShowLockEffect();
            if (onComplete != null)
            {
                onComplete.Invoke();
            }

            //UIEffectManager.Instance.SpawnVFX(CardVFXPool.VFXType.Status_LockIn, _slotHolderList[index].GetHolderPosition(), delegate {               
            //});
        }

        public void HideLockSlotBySlotID(int index, UnityAction onComplete)
        {
            _slotHolderList[index].HideLockEffect();
            UIEffectManager.Instance.SpawnVFX(CardVFXPool.VFXType.Status_LockOut, _slotHolderList[index].GetHolderPosition(), delegate
            {
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            }, null);
        }

        public void SetLockDuration(int index, int value, PlayerIndex owner)
        {
            _slotHolderList[index].SetLockDuration(value, owner);
        }

        public void ShowDarkMatterBySlotID(int index, PlayerIndex owner, UnityAction onComplete)
        {
            CardVFXPool.VFXType vfxType;
            if (GameManager.Instance.IsLocalPlayer(owner))
            {
                vfxType = CardVFXPool.VFXType.Status_DarkMatterIn_Ally;
            }
            else
            {
                vfxType = CardVFXPool.VFXType.Status_DarkMatterIn_Enemy;
            }

            _slotHolderList[index].ShowDarkMatterEffect(owner);
            if (onComplete != null)
            {
                onComplete.Invoke();
            }

            //UIEffectManager.Instance.SpawnVFX(vfxType, _slotHolderList[index].GetHolderPosition(), delegate {           
            //});
        }

        public void HideDarkMatterBySlotID(int index, PlayerIndex owner, UnityAction onComplete)
        {
            CardVFXPool.VFXType vfxType;
            if (GameManager.Instance.IsLocalPlayer(owner))
            {
                vfxType = CardVFXPool.VFXType.Status_DarkMatterOut_Ally;
            }
            else
            {
                vfxType = CardVFXPool.VFXType.Status_DarkMatterOut_Enemy;
            }

            _slotHolderList[index].HideDarkMatterEffect(owner);
            UIEffectManager.Instance.SpawnVFX(vfxType, _slotHolderList[index].GetHolderPosition(), delegate
            {
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            });
        }

        public void AddTokenToList(CardUI card)
        {
            _tokenList.Add(card);
        }

        public CardUI GetTokenBySlotID(int slotID)
        {
            return _tokenList.Find(x => x.GetSlotID() == slotID);
        }

        public CardUI GetTokenByUniqueID(int uniqueID)
        {
            return _tokenList.Find(x => x.GetUniqueID() == uniqueID);
        }

        public BoardSlotHolder GetSlotHolder(int slotID)
        {
            return _slotHolderList[slotID];
        }

        private void ShowSlotByCurrentSlotWithDir(int slotID, CardDirection cardDirection)
        {
            foreach (CardDirectionType item in System.Enum.GetValues(typeof(CardDirectionType)))
            {
                bool isShow = cardDirection.IsDirection(item);

                int targetSlot = -1;
                if (GetAdjacentSlot(slotID, item, out targetSlot))
                {
                    BoardSlotHolder slot = _slotHolderList[targetSlot];
                    slot.ShowHilight(isShow, BoardSlotHolder.SlotHilightType.Normal);
                }

            }
        }

        public bool GetAdjacentSlot(int slotID, CardDirectionType cardDirection, out int index)
        {
            int maxBound = GameManager.Instance.BoardWidth * GameManager.Instance.BoardHeight;

            switch (cardDirection)
            {
                case CardDirectionType.N:
                    if (GetNearbySlotID(slotID, CardDirectionType.N) >= 0)
                    {
                        index = GetNearbySlotID(slotID, CardDirectionType.N);
                        return true;
                    }
                    break;
                case CardDirectionType.NE:
                    if (GetNearbySlotID(slotID, CardDirectionType.N) >= 0)
                    {
                        if (GetNearbySlotID(slotID, CardDirectionType.NE) % GameManager.Instance.BoardWidth != 0)
                        {
                            index = GetNearbySlotID(slotID, CardDirectionType.NE);
                            return true;
                        }
                    }
                    break;
                case CardDirectionType.NW:
                    if (GetNearbySlotID(slotID, CardDirectionType.N) >= 0)
                    {
                        if (GetNearbySlotID(slotID, CardDirectionType.N) % GameManager.Instance.BoardWidth != 0)
                        {
                            index = GetNearbySlotID(slotID, CardDirectionType.NW);
                            return true;
                        }
                    }
                    break;
                case CardDirectionType.E:
                    if (GetNearbySlotID(slotID, CardDirectionType.E) % GameManager.Instance.BoardWidth != 0)
                    {
                        index = GetNearbySlotID(slotID, CardDirectionType.E);
                        return true;
                    }
                    break;
                case CardDirectionType.SE:
                    if (GetNearbySlotID(slotID, CardDirectionType.S) < maxBound)
                    {
                        if (GetNearbySlotID(slotID, CardDirectionType.SE) % GameManager.Instance.BoardWidth != 0)
                        {
                            index = GetNearbySlotID(slotID, CardDirectionType.SE);
                            return true;
                        }
                    }
                    break;
                case CardDirectionType.S:
                    if (GetNearbySlotID(slotID, CardDirectionType.S) < maxBound)
                    {
                        index = GetNearbySlotID(slotID, CardDirectionType.S);
                        return true;
                    }
                    break;
                case CardDirectionType.SW:
                    if (GetNearbySlotID(slotID, CardDirectionType.S) < maxBound)
                    {
                        if (GetNearbySlotID(slotID, CardDirectionType.S) % GameManager.Instance.BoardWidth != 0)
                        {
                            index = GetNearbySlotID(slotID, CardDirectionType.SW);
                            return true;
                        }
                    }
                    break;
                case CardDirectionType.W:
                    if (slotID % GameManager.Instance.BoardWidth != 0)
                    {
                        index = GetNearbySlotID(slotID, CardDirectionType.W);
                        return true;
                    }
                    break;
            }
            index = -1;
            return false;
        }

        private int GetNearbySlotID(int slotID, CardDirectionType directionType)
        {
            int northIndex = slotID - GameManager.Instance.BoardWidth;
            int eastIndex = slotID + 1;
            int southIndex = slotID + GameManager.Instance.BoardWidth;
            int westIndex = slotID - 1;

            switch (directionType)
            {
                case CardDirectionType.N: return northIndex;
                case CardDirectionType.NE: return northIndex + 1;
                case CardDirectionType.E: return eastIndex;
                case CardDirectionType.SE: return southIndex + 1;
                case CardDirectionType.S: return southIndex;
                case CardDirectionType.SW: return southIndex - 1;
                case CardDirectionType.W: return westIndex;
                case CardDirectionType.NW: return northIndex - 1;
            }

            return slotID;
        }

        public List<int> GetAdjacentSlot(int slotID)
        {
            List<int> result = new List<int>();
            foreach (CardDirectionType item in System.Enum.GetValues(typeof(CardDirectionType)))
            {
                int index;
                if (GetAdjacentSlot(slotID, item, out index))
                {
                    result.Add(index);
                }
            }

            return result;
        }

        public List<CardUI> GetAdjacentToken(int uniqueId)
        {
            List<CardUI> result = new List<CardUI>();
            CardUI card = GetTokenByUniqueID(uniqueId);
            if (card != null)
            {
                List<int> slotIdList = GetAdjacentSlot(card.GetSlotID());
                foreach (int item in slotIdList)
                {
                    CardUI token = GetTokenBySlotID(item);
                    if (token != null)
                    {
                        result.Add(token);
                    }
                }
            }

            return result;
        }

        public void ShakeTokenList(List<CardUI> cardUIList)
        {
            //Debug.Log("ShakeBoard");
            foreach (CardUI item in cardUIList)
            {
                item.TokenUIObject.CardShake();
            }
        }

        public void ShakeAllTokenList()
        {
            //Debug.Log("ShakeBoard");
            foreach (CardUI item in _tokenList)
            {
                item.TokenUIObject.CardShake(3);
            }
        }
        #endregion

        #region Events
        public void SetRaycastHit(RaycastHit hit)
        {
            _raycastHit = hit;
        }

        public void HideCurrent()
        {
            if (_currentSlot != null)
            {
                _currentSlot.ShowHilight(false);
                _currentSlot = null;
            }
        }

        public void HideAllHilight()
        {
            // Reset Hilight
            if (_currentSlot != null)
            {
                _currentSlot.ShowHilight(false);
                _currentSlot = null;
            }
            foreach (BoardSlotHolder item in _slotHolderList)
            {
                item.ShowHilight(false);
            }
        }

        public void TokenIsHilight(CardUI card)
        {
            if (_currentHilight != null)
            {
                _currentHilight.SetHilight(false);
                _currentHilight.TokenUIObject.ShowGlow(false);
                _currentHilight = null;
            }
            _currentHilight = card;
            UIManager.Instance.SendPlayerHoverCard(card.GetUniqueID(), true);
            _currentHilight.SetHilight(true);
            _isHilight = true;
            UIManager.Instance.CardUIDetailHover.SetData(card.Data);
            UIManager.Instance.ShowDetailCardUI(true);
            //UIManager.Instance.EnableCancelZone(true);
            _currentHilight.TokenUIObject.ShowGlow(true);
        }

        public void TokenGlowActivate(int cardUniqueID, bool isShow)
        {
            CardUI card;
            if (UIManager.Instance.FindCardByUniqueID(cardUniqueID, out card))
            {
                card.TokenUIObject.ShowGlowAttacking(isShow);
            }
        }

        public void CancelHilight()
        {
            if (_currentHilight != null)
            {
                UIManager.Instance.SendPlayerHoverCard(_currentHilight.GetUniqueID(), false);
                _currentHilight.SetHilight(false);
                _currentHilight.TokenUIObject.ShowGlow(false);
                _currentHilight = null;
            }
            UIManager.Instance.ShowDetailCardUI(false);
            _isHilight = false;
            //UIManager.Instance.EnableCancelZone(false);
        }

        public void AddEventOnSelectedComplete(UnityAction<List<int>> onSelectedComplete)
        {
            _onSelectedComplete.AddListener(onSelectedComplete);
        }

        public void SetEventOnSelectSlot(UnityAction<int> onSelectSlot)
        {
            _onSelectSlot = onSelectSlot;
        }

        public void ClearEventOnSelectSlot()
        {
            _onSelectSlot = null;
        }

        public void ShowSlotHilight(List<int> listSlotId, int selectCount, UnityAction<List<int>> onSelectedComplete)
        {
            _selectCount = selectCount;
            _onSelectedComplete.AddListener(onSelectedComplete);
            _selectedList.Clear();
            _selectSlotList = new List<int>(listSlotId);
            foreach (int item in _selectSlotList)
            {
                BoardSlotHolder slot = _slotHolderList.Find(x => x.GetSlotID() == item);
                if (slot != null)
                {
                    slot.ShowHilight(true);
                }
            }

            _isShowSelectSlot = true;
        }

        public void HideAllSelectSlot()
        {
            foreach (int item in _selectedList)
            {
                HideSelectSlot(item);
            }

            _isShowSelectSlot = false;
        }

        public void HideSelectSlot(int slotId)
        {
            BoardSlotHolder slot = _slotHolderList.Find(x => x.GetSlotID() == slotId);
            if (slot != null)
            {
                slot.HideSelectedEffect();
            }
        }

        public void SetDeactivatedSlotList(List<int> slotList)
        {
            _DeactivatedSlotList = slotList;
        }

        public void ClearOnSelectedCompleteEvent()
        {
            _onSelectedComplete.RemoveAllListeners();
        }

        public void ClearDeactivatedSlotList()
        {
            _DeactivatedSlotList.Clear();
        }

        public void SelectSlot(int slotId)
        {
            if (_DeactivatedSlotList.Contains(slotId))
                return;

            if (_isShowSelectSlot)
            {
                if (_selectSlotList != null && _selectSlotList.Contains(slotId))
                {
                    BoardSlotHolder slot = _slotHolderList.Find(x => x.GetSlotID() == slotId);
                    slot.ShowSelectedEffect();
                    slot.ShowHilight(false);

                    if (!_selectedList.Contains(slotId))
                    {
                        _selectedList.Add(slotId);
                        _selectCount--;

                        _onSelectSlot?.Invoke(slotId);
                        // Check if selecting is done
                        if (_selectCount == 0 || (_selectedList.Count == _selectSlotList.Count))
                        {
                            _selectSlotList = null; // clear
                            UIManager.Instance.HideSelectionSlot(
                                delegate ()
                                {
                                    _onSelectedComplete?.Invoke(_selectedList);
                                    _onSelectedComplete.RemoveAllListeners();
                                }
                            );
                        }
                    }
                    else
                    {
                        // Select Slot already selected.
                        return;
                    }
                }
                else
                {
                    Debug.LogWarningFormat("BoardManager/SelectSlot: Select invalid slot. {0}", slotId);
                }
            }
        }
        #endregion

        #region Tutorials
        public void Tutorial_SlotHilight(List<int> listSlotId, bool isShow, BoardSlotHolder.SlotHilightType hilightType, bool showArrow)
        {
            foreach (int item in listSlotId)
            {
                BoardSlotHolder slot = _slotHolderList.Find(x => x.GetSlotID() == item);
                if (slot != null)
                {
                    slot.Tutorial_ShowHilight(isShow, hilightType, showArrow);
                }
            }
        }

        /// <summary>
        /// Show Custom Color Hilight on slot.
        /// </summary>
        /// <param name="listSlotId"></param>
        /// <param name="isShow"></param>
        /// <param name="showArrow"></param>
        /// <param name="color"></param>
        public void Tutorial_SlotHilight(List<int> listSlotId, bool isShow, Color color, bool showArrow)
        {
            foreach (int item in listSlotId)
            {
                BoardSlotHolder slot = _slotHolderList.Find(x => x.GetSlotID() == item);
                if (slot != null)
                {
                    slot.Tutorial_ShowCustomHilight(isShow, color, showArrow);
                }
            }
        }

        public void Tutorial_SlotHilight(List<int> listSlotId, List<bool> isShow, List<BoardSlotHolder.SlotHilightType> hilightType, List<bool> showArrow)
        {
            foreach (int item in listSlotId)
            {
                int index = listSlotId.IndexOf(item);
                BoardSlotHolder slot = _slotHolderList.Find(x => x.GetSlotID() == item);
                if (slot != null)
                {
                    slot.Tutorial_ShowHilight(isShow[index], hilightType[index], showArrow[index]);
                }
            }
        }

        public void Tutorial_SlotHilightArrow(List<int> slotID, bool isShow)
        {
            foreach (int item in slotID)
            {
                CardUI card = GetTokenBySlotID(item);
                card.TokenUIObject.Tutorial_HilightDirection(isShow);
            }
        }

        public void Tutorial_SlotHilightArrow(int slotID, CardDirection dirValue, bool isShow)
        {
            CardUI card = GetTokenBySlotID(slotID);
            Tutorial_TokenHilightArrow(card.GetUniqueID(), dirValue, isShow);
        }

        public void Tutorial_TokenHilightArrow(int uniqueID, bool isShow)
        {
            CardUI card;
            if (UIManager.Instance.FindCardByUniqueID(uniqueID, out card))
            {
                card.TokenUIObject.Tutorial_HilightDirection(isShow);
            }
        }

        public void Tutorial_TokenHilightArrow(List<int> uniqueID, bool isShow)
        {
            foreach (int item in uniqueID)
            {
                Tutorial_TokenHilightArrow(item, isShow);
            }
        }

        public void Tutorial_TokenHilightArrow(int uniqueID, CardDirection dirValue, bool isShow)
        {
            CardUI card;
            if (UIManager.Instance.FindCardByUniqueID(uniqueID, out card))
            {
                card.TokenUIObject.Tutorial_HilightDirection(isShow, dirValue);
            }
        }

        public void Tutorial_TokenHilightSoul(int uniqueID, bool isShow)
        {
            CardUI card;
            if (UIManager.Instance.FindCardByUniqueID(uniqueID, out card))
            {
                card.TokenUIObject.Tutorial_HilightSoul(isShow);
            }
        }

        public void Tutorial_TokenHilightSoul(List<int> uniqueID, bool isShow)
        {
            foreach (int item in uniqueID)
            {
                Tutorial_TokenHilightSoul(item, isShow);
            }
        }

        public void Tutorial_TokenHilightAtk(int uniqueID, bool isShow)
        {
            CardUI card;
            if (UIManager.Instance.FindCardByUniqueID(uniqueID, out card))
            {
                card.TokenUIObject.Tutorial_HilightATK(isShow);
            }
        }

        public void Tutorial_TokenHilightSoulBySlots(List<int> slots, bool isShow)
        {
            for (int i = 0; i < slots.Count; i++)
            {
                int slotID = slots[i];
                CardUI card = GetTokenBySlotID(slotID);
                card.TokenUIObject.Tutorial_HilightSoul(isShow);
            }
        }

        public void Tutorial_TokenHilightHp(int uniqueID, bool isShow)
        {
            CardUI card;
            if (UIManager.Instance.FindCardByUniqueID(uniqueID, out card))
            {
                card.TokenUIObject.Tutorial_HilightHP(isShow);
            }
        }

        public void Tutorial_TokenHilightArmor(int uniqueID, bool isShow)
        {
            CardUI card;
            if (UIManager.Instance.FindCardByUniqueID(uniqueID, out card))
            {
                card.TokenUIObject.Tutorial_HilightARMOR(isShow);
            }
        }

        public void Tutorial_TokenHilightArmorBySlots(List<int> slots, bool isShow)
        {
            for (int i = 0; i < slots.Count; i++)
            {
                int slotID = slots[i];
                CardUI card = GetTokenBySlotID(slotID);
                if (card != null)
                {
                    card.TokenUIObject.Tutorial_HilightARMOR(isShow);
                }
            }
        }

        #endregion
    }
}
