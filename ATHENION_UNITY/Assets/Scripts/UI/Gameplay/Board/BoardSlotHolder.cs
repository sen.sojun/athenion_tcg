﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;
using DG.Tweening;

namespace Karamucho.UI
{
    public class BoardSlotHolder : MonoBehaviour,IPointerDownHandler, IPointerUpHandler, IPointerClickHandler
    {
        #region Enum
        public enum SlotHilightType
        {
            Normal,
            Hilight,
            Attack,
            Target,
            Custom
        }
        #endregion

        #region Inspector Properties
        [SerializeField] private Transform Holder;

        [Header("Hilight Color")]
        [SerializeField] private GameObject HilightSlot;
        [SerializeField] private GameObject HilightTUSlot;

        [Header("Hilight Slot")]
        [SerializeField] private GameObject P_SlotNormal;
        [SerializeField] private GameObject P_SlotUseable;
        [SerializeField] private GameObject P_SlotAttack;
        [SerializeField] private GameObject P_SlotTarget;

        [Header("Hilight Tutorial Slot")]
        [SerializeField] private GameObject P_TU_SlotNormal;
        [SerializeField] private GameObject P_TU_SlotUseable;
        [SerializeField] private GameObject P_TU_SlotAttack;
        [SerializeField] private GameObject P_TU_SlotTarget;
        [SerializeField] private GameObject P_TU_SlotCustom;

        [Header("Lock")]
        [SerializeField] private GameObject LockEffect;
        [SerializeField] private TextMeshPro CountText;

        [Header("Dark Matter")]
        [SerializeField] private GameObject DarkMatter_Ally;
        [SerializeField] private GameObject DarkMatter_Enemy;

        [Header("Link")]
        [SerializeField] private Color LinkAllyColor;
        [SerializeField] private Color LinkEnemyColor;
        [SerializeField] private List<GameObject> LinkList;
        [SerializeField] private List<GameObject> ArrowList;

        [Header("Misc")]
        [SerializeField] private Sprite UsableSprite;
        [SerializeField] private Sprite AttackSprite;
        [SerializeField] private GameObject SelectedEffect;
        [SerializeField] private GameObject SlotArrow;

        public int DurationValue { get; private set; }
        #endregion

        #region Private Properties
        private CardUI _currentCard;
        private int _id;
        private bool _isShow;
        private float _clickDelay = 0.2f;
        private float _time = 0;
        private bool _isDown;
        private bool _isPreview;
        #endregion

        #region Methods
        private void Update()
        {
            ClickDelayUpdate();
        }

        private void ClickDelayUpdate()
        {
            if (_isDown && UIManager.Instance.IsSelectionSlotShowing)
            {
                if (_time <= 0.0f)
                {
                    if (_currentCard != null)
                    {
                        _currentCard.HilightToken();
                        _isPreview = true;
                    }
                    else
                    {
                        _isPreview = false;
                    }
                }
                else
                {
                    _isPreview = false;
                    _time -= Time.deltaTime;
                }
            }
        }

        public BoardSlotHolder InitSlotID(int id)
        {
            _id = id;
            return this;
        }

        public int GetSlotID()
        {
            return _id;
        }

        public Vector3 GetHolderPosition()
        {
            return Holder.transform.position;
        }

        public void ShowHilight(bool isShow, SlotHilightType hilightType = SlotHilightType.Hilight)
        {
            HilightSlot.SetActive(isShow);

            if (isShow)
            {
                SetHilight(hilightType);
                _isShow = true;
            }
            else
            {
                _isShow = false;
            }
        }

        public void Tutorial_ShowHilight(bool isShow, SlotHilightType hilightType, bool showPointer)
        {
            HilightTUSlot.SetActive(isShow);

            if (isShow)
            {
                SetPointer(showPointer);
                SetTUHilight(hilightType);
            }
            else
            {
                SetPointer(false);
            }

        }

        public void Tutorial_ShowCustomHilight(bool isShow, Color color, bool showPointer)
        {
            HilightTUSlot.SetActive(isShow);

            if (isShow)
            {
                SetPointer(showPointer);
                SetTUHilight(isShow, color);
            }
            else
            {
                SetPointer(false);
            }

        }

        public void Tutorial_ShowAttackHilight(bool isShow, bool showArrow)
        {
            HilightTUSlot.SetActive(isShow);

            if (isShow)
            {
                SetPointer(showArrow);
                SetTUHilight(SlotHilightType.Attack);
            }
            else
            {
                SetPointer(false);
            }
        }

        private void SetHilight(SlotHilightType hilightType)
        {
            P_SlotNormal.SetActive(hilightType == SlotHilightType.Normal);
            P_SlotUseable.SetActive(hilightType == SlotHilightType.Hilight);
            P_SlotTarget.SetActive(hilightType == SlotHilightType.Target);
            P_SlotAttack.SetActive(hilightType == SlotHilightType.Attack);
   
        }

        private void SetTUHilight(SlotHilightType hilightType)
        {
            P_TU_SlotNormal.SetActive(hilightType == SlotHilightType.Normal);
            P_TU_SlotUseable.SetActive(hilightType == SlotHilightType.Hilight);
            P_TU_SlotTarget.SetActive(hilightType == SlotHilightType.Target);
            P_TU_SlotAttack.SetActive(hilightType == SlotHilightType.Attack);
            P_TU_SlotCustom.SetActive(hilightType == SlotHilightType.Custom);

        }

        private void SetTUHilight(bool isShow , Color color)
        {
            P_TU_SlotCustom.SetActive(isShow);
            ParticleSystem ps = P_TU_SlotCustom.GetComponent<ParticleSystem>();
            ParticleSystem.MainModule pm = ps.main;
            pm.startColor = color; 
            ps.Play();
        }

        private void SetPointer(bool showPointer)
        {
            SlotArrow.SetActive(showPointer);
        }

        public void SetLockDuration(int value, PlayerIndex owner)
        {
            DurationValue = value;
            CountText.text = DurationValue.ToString();

            if(DurationValue > 0)
            {
                if (GameManager.Instance.IsLocalPlayer(owner))
                {
                    CountText.color = GameHelper.GetColor_PlayerBlue();
                }
                else
                {
                    CountText.color = GameHelper.GetColor_PlayerRed();
                }

                Sequence sq = DOTween.Sequence();
                sq.Append(CountText.transform.DOScale(1.5f, 0.3f));
                sq.Append(CountText.transform.DOScale(1.0f, 0.3f));
            }
        }

        public void ShowLockEffect()
        {
            SoundManager.PlaySFX(SoundManager.SFX.Ability_Lock_In);
            LockEffect.SetActive(true);
        }

        public void HideLockEffect()
        {
            SoundManager.PlaySFX(SoundManager.SFX.Ability_Lock_Out);
            LockEffect.SetActive(false);
        }

        public void ShowDarkMatterEffect(PlayerIndex playerIndex)
        {
            if (GameManager.Instance.IsLocalPlayer(playerIndex))
            {
                DarkMatter_Ally.SetActive(true);
                DarkMatter_Enemy.SetActive(false);
            }
            else
            {
                DarkMatter_Ally.SetActive(false);
                DarkMatter_Enemy.SetActive(true);
            }
        }

        public void HideDarkMatterEffect(PlayerIndex playerIndex)
        {
            if (GameManager.Instance.IsLocalPlayer(playerIndex))
            {
                DarkMatter_Ally.SetActive(false);
                DarkMatter_Enemy.SetActive(false);
            }
            else
            {
                DarkMatter_Ally.SetActive(false);
                DarkMatter_Enemy.SetActive(false);
            }
        }

        public void ShowSelectedEffect()
        {
            SelectedEffect.SetActive(true);
        }

        public void HideSelectedEffect()
        {
            SelectedEffect.SetActive(false);
        }

        public void ShowLink(List<CardDirectionType> dirList, PlayerIndex playerIndex)
        {
            foreach (CardDirectionType item in System.Enum.GetValues(typeof(CardDirectionType)))
            {
                if (dirList.Contains(item))
                {
                    LinkList[(int)item].SetActive(true);
                    ArrowList[(int)item].SetActive(true);
                    if (GameManager.Instance.IsLocalPlayer(playerIndex))
                    {
                        LinkList[(int)item].GetComponent<SpriteRenderer>().color = LinkAllyColor;
                        ArrowList[(int)item].GetComponent<SpriteRenderer>().color = LinkAllyColor;
                    }
                    else
                    {
                        LinkList[(int)item].GetComponent<SpriteRenderer>().color = LinkEnemyColor;
                        ArrowList[(int)item].GetComponent<SpriteRenderer>().color = LinkEnemyColor;
                    }

                    //Debug.LogWarning(item + " " + LinkList[(int)item].activeSelf);
                }
                else
                {
                    LinkList[(int)item].SetActive(false);
                    ArrowList[(int)item].SetActive(false);
                }
            }
        }
        #endregion

        #region Events
        public void OnPointerDown(PointerEventData eventData)
        {
            Debug.LogFormat("BoardSlotHolder: OnPointerDown ({0})", _id);
            CardUI card = BoardManager.Instance.GetTokenBySlotID(_id);
            _currentCard = card;

            // If card is not interactable. reject this event.
            if (_currentCard != null)
            {
                if (!_currentCard.IsInteractable)
                {
                    return;
                }
            }

            if (card != null)
            {
                _isDown = true;
                if (UIManager.Instance.IsSelectionSlotShowing)
                {
                    _time = _clickDelay;
                    _isPreview = false;
                }
                else
                {
                    _time = 0.0f;
                    card.HilightToken();
                    _isPreview = true;

                    SoundManager.PlaySFX(SoundManager.SFX.Card_Click);
                }
            }
            else
            {
                _isPreview = false;
                if (!UIManager.Instance.IsSelectionSlotShowing)
                {
                    CardUIManager.Instance.HideHandCard();
                }

                if (BoardManager.Instance.IsHilight)
                {
                    BoardManager.Instance.CancelHilight();
                }
                if (CardUIManager.Instance.IsHilight)
                {
                    CardUIManager.Instance.CancelHilight(); 
                }
            }
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            //Debug.LogFormat("BoardSlotHolder: OnPointerUp ({0})", _id);

            // If card is not interactable. reject this event.
            if (_currentCard != null)
            {
                if (!_currentCard.IsInteractable)
                {
                    return;
                }
            }

            _isDown = false;
            if (BoardManager.Instance.IsHilight)
            {
                BoardManager.Instance.CancelHilight();
            }
            if (CardUIManager.Instance.IsHilight)
            {
                CardUIManager.Instance.CancelHilight();
            }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            //Debug.LogFormat("BoardSlotHolder: OnPointerClick ({0})", _id);

            // If card is not interactable. reject this event.
            if (_currentCard != null)
            {
                if (!_currentCard.IsInteractable)
                {
                    return;
                }
            }
            else
            {
                SoundManager.PlaySFX(SoundManager.SFX.Slot_Click);
            }

            _isDown = false;
            if (UIManager.Instance.IsSelectionSlotShowing)
            {
                if (_isShow && !_isPreview)
                {
                    BoardManager.Instance.SelectSlot(_id);
                }
            }
        }
        #endregion
    }
}
