﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;

public class ScrollViewSnapGrid : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler{

    #region Public Properties
    public RectTransform View;
    public GridLayoutGroup GridGroup;
    public RectTransform RectTransform;

    [Header("Button")]
    public Button BTN_Next;
    public Button BTN_Back;
    #endregion

    #region Private Properties
    private int _maxPage = 0;
    private int _currentPage = 0;
    private float _dragThreshold = 200.0f;
    private Vector3 _startPosition;
    private Vector3 _dragStart;
    private Vector3 _dragEnd;
    private List<float> _cellListPosX = new List<float>();
    private Sequence _sq;

    #endregion

    #region Method
    private void Start()
    {
        _sq = DOTween.Sequence();

    }

    public void InitScrollView()
    {
        InitCellSpacing();
        InitCell();
        InitButton();

        UpdatePage();
    }

    public void SetMaxPage(int value)
    {
        _maxPage = value;
    }

    private void InitButton()
    {
        BTN_Next.onClick.RemoveAllListeners();
        BTN_Back.onClick.RemoveAllListeners();

        BTN_Next.onClick.AddListener(delegate
        {
            NextPage();
        });
        BTN_Back.onClick.AddListener(delegate
        {
            PreviousPage();
        });
    }

    private void InitCellSpacing()
    {
        GridGroup.padding.left = ((int)View.rect.width / 2 - (int)GridGroup.cellSize.x / 2);
        GridGroup.padding.right = ((int)View.rect.width / 2 - (int)GridGroup.cellSize.x / 2);
        GridGroup.spacing = new Vector2(GridGroup.padding.left, 0);

    }

    private void InitCell()
    {
        for (int i = 0; i < _maxPage; i++)
        {
            float cellPosX = (i) * (-1) * (GridGroup.spacing.x + GridGroup.cellSize.x);
            _cellListPosX.Add(cellPosX);

        }
    }

    public void UpdatePage()
    {
        if (_maxPage == 1)
        {
            BTN_Back.gameObject.SetActive(false);
            BTN_Next.gameObject.SetActive(false);
        }
        else
        {
            if (_currentPage == 0)
            {
                BTN_Back.gameObject.SetActive(false);
                BTN_Next.gameObject.SetActive(true);
            }
            else if (_currentPage == _maxPage - 1)
            {
                BTN_Back.gameObject.SetActive(true);
                BTN_Next.gameObject.SetActive(false);
            }
            else
            {
                BTN_Back.gameObject.SetActive(true);
                BTN_Next.gameObject.SetActive(true);
            }
        }

    }

    public void MoveToPage(int page)
    {
        _sq.Kill();
        _sq.Append(RectTransform.DOAnchorPosX(_cellListPosX[page], 0.5f).SetEase(Ease.OutExpo));
        UpdatePage();
    }
    #endregion

    #region Events
    public void OnBeginDrag(PointerEventData eventData)
    {
        _sq.Kill();
        _dragStart = eventData.position;
        _startPosition = RectTransform.anchoredPosition;
    }

    public void OnDrag(PointerEventData eventData)
    {
        float deltaX = eventData.position.x - _dragStart.x;
        RectTransform.anchoredPosition = new Vector2(_startPosition.x + deltaX, 0);

        // at start cell
        if (RectTransform.anchoredPosition.x > 0)
        {
            RectTransform.anchoredPosition = new Vector2(0, 0);
        }

        // at last cell
        if (RectTransform.anchoredPosition.x < _cellListPosX[_cellListPosX.Count - 1])
        {
            RectTransform.anchoredPosition = new Vector2(_cellListPosX[_cellListPosX.Count - 1], 0);
        }

    }

    public void NextPage()
    {
        _currentPage++;
        if (_currentPage > _maxPage - 1)
        {
            _currentPage = _maxPage - 1;
        }
        MoveToPage(_currentPage);
        //Debug.Log("Next Page");
    }

    public void PreviousPage()
    {
        _currentPage--;
        if (_currentPage < 0)
        {
            _currentPage = 0;
        }
        MoveToPage(_currentPage);
        //Debug.Log("Previous Page");
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        _dragEnd = eventData.position;
        if (Vector3.Distance(_dragStart, _dragEnd) > _dragThreshold)
        {
            if (_dragStart.x > _dragEnd.x)
            {
                NextPage();
            }
            else if (_dragStart.x < _dragEnd.x)
            {
                PreviousPage();
            }
        }
        else
        {
            MoveToPage(_currentPage);
        }

    }

    #endregion
}
