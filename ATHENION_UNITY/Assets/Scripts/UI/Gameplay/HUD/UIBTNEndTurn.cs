﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIBTNEndTurn : MonoBehaviour, IPointerDownHandler, IPointerClickHandler, IPointerUpHandler
{

    #region Public Properties
    public Animator Animator;
    public GameObject GlowOnDown;
    #endregion

    #region Private Properties
    private bool _isOpen;
    private bool _isFinish;
    private bool _isPass;

    private bool _isDown = false;
    private bool _isPreview = false;
    private float _maxDownTime = 0.3f;
    private float _timer = 0;
    private bool _isEnable = true;
    #endregion

    #region Methods
    private void Start()
    {
        SetEnable(true);
    }

    private void Update()
    {
        // Click Delay
        if (_isDown)
        {
            if (_timer < _maxDownTime)
            {
                _timer += Time.deltaTime;
            }
            else
            {
                if (!_isPreview)
                {
                    _isPreview = true;
                }

            }
        }
    }

    public void ButtonIsPass(bool isPass)
    {
        _isPass = isPass;
        Animator.SetBool("IsPass", _isPass);
    }

    public void ButtonIsOpen(bool isOpen)
    {
        _isOpen = isOpen;
        Animator.SetBool("IsOpen", _isOpen);
    }

    public void ButtonIsFinish(bool isFinish)
    {
        _isFinish = isFinish;
        Animator.SetBool("IsFinish", _isFinish);
    }

    public void ResetAll()
    {
        ButtonIsPass(false);
        ButtonIsOpen(false);
        ButtonIsFinish(false);
    }

    public void EndButtonAnimation()
    {
        StartCoroutine(EndButtonAnimationSequence());
    }

    private IEnumerator EndButtonAnimationSequence()
    {
        if (!_isFinish)
        {
            ButtonIsFinish(true);
        }

        yield return new WaitForSeconds(1.0f);

        ResetAll();
    }

    private void EndTurn()
    {
        //Debug.Log("End Turn");
        if (UIManager.Instance.IsReadyToEndTurn)
        {
            if (GameManager.Instance.IsCanEndTurn(GameManager.Instance.GetLocalPlayerIndex()))
            {
                SoundManager.PlaySFX(SoundManager.SFX.End_Turn_Click);
                EndButtonAnimation();
                GameManager.Instance.RequestLocalEndTurn(true);
            }
        }
    }

    public void SetEnable(bool isEnable)
    {
        _isEnable = isEnable;
    }
    #endregion

    #region Event
    public void OnPointerClick(PointerEventData eventData)
    {
        if (_isEnable == false) return;
        if (_isPreview) return;

        if (UIManager.Instance.IsReadyToEndTurn && _isOpen)
        {
            bool isSkipTurn = false;
            int handCount = GameManager.Instance.GetHandCount(GameManager.Instance.GetLocalPlayerIndex());
            int currentSP = GameManager.Instance.GetSumSpirit(GameManager.Instance.GetLocalPlayerIndex());
            int actionCount = GameManager.Instance.PlayCardCount;

            isSkipTurn = (currentSP == 0 && handCount > 0 && actionCount == 0) && GameManager.Instance.IsTutorialMode() == false;

            if (_isPass || isSkipTurn)
            {
                string head = LocalizationManager.Instance.GetText("TEXT_SKIP_TURN");
                string message = LocalizationManager.Instance.GetText("TEXT_SKIP_TURN_DESCRIPTION");
                PopupUIManager.Instance.ShowPopup_SureCheck(head, message, EndTurn, null);
            }
            else
            {
                EndTurn();
            }

        }
    }

    public void ShowDown(bool isShow)
    {
        GlowOnDown?.gameObject.SetActive(isShow);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (_isEnable == false) return;

        _isDown = true;
        _isPreview = false;
        _timer = 0;

        if (UIManager.Instance.IsReadyToEndTurn && _isOpen)
        {
            GlowOnDown?.gameObject.SetActive(true);
        }


    }

    public void OnPointerUp(PointerEventData eventData)
    {
        _isDown = false;

        GlowOnDown?.gameObject.SetActive(false);
        ShowDown(false);
    }
    #endregion

}
