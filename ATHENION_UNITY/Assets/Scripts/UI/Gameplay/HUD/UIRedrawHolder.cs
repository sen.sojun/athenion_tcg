﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;

namespace Karamucho.UI
{
    public class UIRedrawHolder : MonoBehaviour
    {

        #region Public Properties
        public bool IsShow { get { return _isShow; } }
        public List<GameObject> MarkerList;
        #endregion

        #region Private Properties
        private List<int> _emptySlotList = new List<int>();
        private bool _isShow = false;
        #endregion

        #region Methods
        public void ShowUI(List<CardUI> cardUIList)
        {
            gameObject.SetActive(true);
            MoveCardListToMarker(cardUIList);
            _isShow = true;
        }

        public void HideUI()
        {
            gameObject.SetActive(false);
            _isShow = false;
        }

        public void MoveCardListToMarker(List<CardUI> cardUIList)
        {
            int i = 0;
            foreach (CardUI item in cardUIList)
            {
                item.AnimationCardToPosition(MarkerList[i].transform, null);
                item.ShowGlowReady(false);
                i++;
            }
        }

        public void AddEmptySlot(int index)
        {
            _emptySlotList.Add(index);
            _emptySlotList.Sort();
        }

        public void RemoveEmptySlot(int index)
        {
            _emptySlotList.Remove(index);
            _emptySlotList.Sort();
        }

        public void MoveCardToEmptyMarker(CardUI cardUI, UnityAction onComplete)
        {
            if (_emptySlotList.Count > 0)
            {
                cardUI.AnimationCardToPosition(MarkerList[_emptySlotList[0]].transform, onComplete, 0.5f);
                _emptySlotList.RemoveAt(0);
            }
            else
            {
                Debug.LogError("No Empty Slot");
            }

        }

        #endregion
    }
}
