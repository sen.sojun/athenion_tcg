﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class UIWaitingMessage : MonoBehaviour {

    #region Public Properties
    public Image BGBlack;
    public TextMeshProUGUI Message;
    #endregion

    private bool _isShow = false;

    public void ShowUI()
    {
        if (!_isShow)
        {
            _isShow = true;
            gameObject.SetActive(true);

            BGBlack.color = new Color(1, 1, 1, 0);
            Message.color = new Color(1, 1, 1, 0);

            Sequence sq = DOTween.Sequence();
            sq.Append(BGBlack.DOFade(1.0f, 0.5f));
            sq.Join(Message.DOFade(1.0f, 0.5f));
        }

    }

    public void HideUI()
    {
        if (_isShow)
        {
            _isShow = false;
            Sequence sq = DOTween.Sequence();
            sq.Append(BGBlack.DOFade(0.0f, 0.5f));
            sq.Join(Message.DOFade(0.0f, 0.5f));
            sq.OnComplete(delegate () {
                gameObject.SetActive(false);
            });
        }

    }
}
