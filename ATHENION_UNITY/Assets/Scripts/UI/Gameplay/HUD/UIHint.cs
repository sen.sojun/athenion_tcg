﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIHint : MonoBehaviour {

    public TextMeshProUGUI Message;

    #region Methods
    public void SetMessage(string message)
    {
        Message.text = message;
    }

    public void ShowUI(string message)
    {
        if (gameObject.activeSelf == false)
        {
            GameHelper.UITransition_FadeIn(this.gameObject);
        }

        SetMessage(message);
    }

    public void HideUI()
    {
        GameHelper.UITransition_FadeOut(this.gameObject);
    }
    #endregion
}
