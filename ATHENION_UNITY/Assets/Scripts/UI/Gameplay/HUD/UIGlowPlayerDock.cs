﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class UIGlowPlayerDock : MonoBehaviour {

    public GameObject Dust;
    public GameObject Smoke;

    private void Start()
    {


    }

    public void GlowActive(bool isShow)
    {
        if (isShow)
        {
            gameObject.SetActive(isShow);
            ParticleSystem _ps = Smoke.GetComponent<ParticleSystem>();
            ParticleSystem _ps1 = Dust.GetComponent<ParticleSystem>();
            GetComponent<SpriteRenderer>().DOFade(0.6f, 1.0f);
            ParticleSystem.EmissionModule pe = _ps.emission;
            ParticleSystem.EmissionModule pe1 = _ps1.emission;
            pe.enabled = true;
            pe1.enabled = true;

        }
        else
        {
            ParticleSystem _ps = Smoke.GetComponent<ParticleSystem>();
            ParticleSystem _ps1 = Dust.GetComponent<ParticleSystem>();
            ParticleSystem.EmissionModule pe = _ps.emission;
            ParticleSystem.EmissionModule pe1 = _ps1.emission;
            pe.enabled = false;
            pe1.enabled = false;

            GetComponent<SpriteRenderer>().DOFade(0.0f, 1.0f).OnComplete(delegate { gameObject.SetActive(isShow); });
        }
        
    }

}
