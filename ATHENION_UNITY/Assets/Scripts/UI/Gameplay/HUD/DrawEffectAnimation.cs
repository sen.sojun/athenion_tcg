﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DrawEffectAnimation : MonoBehaviour {

    private UnityAction _onComplete = null;

    public void SetOnComplete(UnityAction onComplete)
    {
        if (_onComplete != null)
        {
            OnCompleteActive();
        }

        _onComplete = onComplete;
    }

    public void OnCompleteActive()
    {
        if (_onComplete != null)
        {
            _onComplete.Invoke();
            _onComplete = null;
        }
    }
}
