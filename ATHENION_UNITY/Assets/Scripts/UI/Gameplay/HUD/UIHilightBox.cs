﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.U2D;
using DG.Tweening;
using TMPro;

namespace Karamucho.UI
{
    public class UIHilightBox : MonoBehaviour
    {

        #region Public Properties
        public Transform TargetPosition;

        [Header("Reference")]
        public TextMeshProUGUI CardName;
        public TextMeshProUGUI Detail;
        public TextMeshProUGUI Attack;
        public TextMeshProUGUI Hp;
        public TextMeshProUGUI Spirit;
        public Image AttackImage;
        public Image HpImage;
        public Image SpiritImage;
        public Image FrameImage;
        public Image RarityImage;
        public Image GemSpellImage;
        public Image Artwork;
        public Image Mask;

        [Header("Mask")]
        public Sprite MinionMask;
        public Sprite SpellMask;

        [Header("Rarity")]
        public Sprite RarityCommon;
        public Sprite RarityRare;
        public Sprite RarityEpic;
        public Sprite RarityLegendary;

        [Header("Token Frame")]
        public Sprite CopperToken;
        public Sprite SilverToken;
        public Sprite GoldToken;
        public Sprite FrameSpellCopper;
        public Sprite FrameSpellSilver;
        public Sprite FrameSpellGold;

        [Header("Detail Frame")]
        public Sprite CopperFrame;
        public Sprite SilverFrame;
        public Sprite GoldFrame;

        [Header("Element Atlas")]
        public SpriteAtlas ElementAtlas;

        [Header("Arrow")]
        public List<GameObject> ArrowDir = new List<GameObject>();
        #endregion

        #region Private Properties
        private Vector3 _defalutPosition;
        private CardUIData _currentHilightData;
        private Sequence _sq;
        #endregion

        #region Methods
        private void Start()
        {
            _defalutPosition = transform.localPosition;
        }

        public void SetData(CardUIData data)
        {
            _currentHilightData = data;
            UpdateDetail();
        }

        private void UpdateDetail()
        {
            if (_currentHilightData != null)
            {
                CardName.text = _currentHilightData.Name;
                Detail.text = _currentHilightData.Description;
                Attack.text = _currentHilightData.ATKCurrent.ToString();
                Hp.text = _currentHilightData.HPCurrent.ToString();
                Spirit.text = GameHelper.GetIntToRomanNumber(_currentHilightData.SpiritCurrent).ToString();
                SetFrame(_currentHilightData.SpiritCurrent);
                SetElement(_currentHilightData);
                ShowDirection(_currentHilightData.CardDir);
                SetRarity(_currentHilightData);
                SetImage(_currentHilightData);

                if (_currentHilightData.Type == CardType.Minion || _currentHilightData.Type == CardType.Minion_NotInDeck)
                {
                    AttackImage.gameObject.SetActive(true);
                    HpImage.gameObject.SetActive(true);
                    Attack.gameObject.SetActive(true);
                    Hp.gameObject.SetActive(true);
                    Spirit.gameObject.SetActive(true);
                    GemSpellImage.gameObject.SetActive(false);
                    Mask.sprite = MinionMask;
                }
                else if (_currentHilightData.Type == CardType.Spell)
                {
                    AttackImage.gameObject.SetActive(false);
                    HpImage.gameObject.SetActive(false);
                    Attack.gameObject.SetActive(false);
                    Hp.gameObject.SetActive(false);
                    Spirit.gameObject.SetActive(false);
                    GemSpellImage.gameObject.SetActive(true);
                    Mask.sprite = SpellMask;
                }
            }
        }

        private void SetFrame(int value)
        {
            if (_currentHilightData.Type == CardType.Minion || _currentHilightData.Type == CardType.Minion_NotInDeck)
            {
                switch (value)
                {
                    case 0:
                    GetComponent<Image>().sprite = CopperFrame;
                    FrameImage.sprite = CopperToken;
                    break;
                    case 1:
                    GetComponent<Image>().sprite = CopperFrame;
                    FrameImage.sprite = CopperToken;
                    break;
                    case 2:
                    GetComponent<Image>().sprite = SilverFrame;
                    FrameImage.sprite = SilverToken;
                    break;
                    case 3:
                    GetComponent<Image>().sprite = GoldFrame;
                    FrameImage.sprite = GoldToken;
                    break;
                    default:
                    GetComponent<Image>().sprite = GoldFrame;
                    FrameImage.sprite = GoldToken;
                    break;
                }
            }
            else if (_currentHilightData.Type == CardType.Spell)
            {
                switch (_currentHilightData.SpiritCurrent)
                {
                    case 0:
                    FrameImage.sprite = FrameSpellCopper;
                    break;
                    case 1:
                    FrameImage.sprite = FrameSpellCopper;
                    break;
                    case 2:
                    FrameImage.sprite = FrameSpellSilver;
                    break;
                    case 3:
                    FrameImage.sprite = FrameSpellGold;
                    break;
                    case 4:
                    FrameImage.sprite = FrameSpellGold;
                    break;
                    default:
                    FrameImage.sprite = FrameSpellGold;
                    break;
                }
            }
        }

        public void ShowDirection(CardDirection dirValue)
        {
            foreach (CardDirectionType item in System.Enum.GetValues(typeof(CardDirectionType)))
            {
                bool isShow = dirValue.IsDirection(item);
                ArrowDir[(int)item].SetActive(isShow);
            }
        }

        private void SetElement(CardUIData data)
        {
            SpiritImage.sprite = ElementAtlas.GetSprite("BgClass_" + data.Element.ToCardElementType().ToString());
            for (int i = 0; i < ArrowDir.Count; i++)
            {
                Sprite filename = ElementAtlas.GetSprite("ArrowClass_" + data.Element.ToCardElementType().ToString() + "-" + (i + 1));
                ArrowDir[i].GetComponent<Image>().sprite = filename;
            }
        }

        private void SetRarity(CardUIData data)
        {
            switch (data.Rarity)
            {
                case CardRarity.Common:
                RarityImage.sprite = RarityCommon;
                break;
                case CardRarity.Rare:
                RarityImage.sprite = RarityRare; ;
                break;
                case CardRarity.Epic:
                RarityImage.sprite = RarityEpic;
                break;
                case CardRarity.Legend:
                RarityImage.sprite = RarityLegendary;
                break;
                default:
                RarityImage.sprite = RarityCommon;
                break;
            }
        }

        public void AnimationShowHilight()
        {
            _sq.Kill();
            _sq = DOTween.Sequence();
            gameObject.SetActive(true);
            _sq.Append(transform.DOLocalMove(TargetPosition.localPosition, 0.3f).SetEase(Ease.OutExpo));
        }

        public void AnimationHideHilight()
        {
            _sq.Kill();
            _sq = DOTween.Sequence();
            _sq.Append(transform.DOLocalMove(_defalutPosition, 0.3f).SetEase(Ease.OutExpo).OnComplete(delegate { gameObject.SetActive(false); }));
        }


        private void SetImage(CardUIData data)
        {
            // Clear previous image
            {
                foreach (Transform child in Mask.transform)
                {
                    Destroy(child.gameObject);
                }
            }

            GameObject obj;
            if (CardResourceManager.Create(CardResourceManager.ImageType.InfoCard, data.ImageKey, Mask.transform, false, out obj))
            {
                obj.transform.localPosition = Vector3.zero;
                obj.transform.localRotation = Quaternion.identity;
            }
        }
        #endregion

    }
}
