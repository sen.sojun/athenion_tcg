﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

namespace Karamucho.UI
{
    public class UISelection : MonoBehaviour
    {
        #region Public Properties
        public bool IsShow { get; private set; }

        public Animator Animator;
        public TextMeshProUGUI Ability;
        public GameObject TokenGroup;
        public TextMeshProUGUI AbilitySub;

        [Header("Reference")]
        public TextMeshProUGUI Atk;
        public TextMeshProUGUI Hp;
        public TextMeshProUGUI Spirit;
        public RectTransform ArtGroup;
        public Image Frame;
        public Image Glow;
        //public Image SpiritImage;
        public Image AttackImage;
        public Image HpImage;

        [Header("Passive Icon")]
        public Image PassiveIcon;
        //public Image PassiveFlashIcon;
        public Sprite SpriteLastWish;
        public Sprite SpriteCritical;
        public Sprite SpriteTrigger;
        public Sprite SpriteLink;
        public Sprite SpriteBerserk;

        [Header("Arrow Materials")]
        public Sprite ArrowBlue;
        public Sprite ArrowRed;

        [Header("Arrow")]
        public List<GameObject> ArrowDir = new List<GameObject>();

        #endregion

        #region Methods
        public void ShowUI(CardUIData data, string info)
        {
            gameObject.SetActive(true);
            IsShow = true;
            CheckInfo(data, info);
            Animator.SetBool("IsShow", true);
        }

        public void HideUI()
        {
            IsShow = false;
            Animator.SetBool("IsShow", false);

        }

        public void SetActiveSelf()
        {
            gameObject.SetActive(true);
        }

        public void DisableSelf()
        {
            gameObject.SetActive(false);
        }
        #endregion

        #region SetToken
        private void CheckInfo(CardUIData data, string info)
        {
            string infoKey = info;

            if (infoKey == "-" || infoKey.Length <= 0)
            {
                infoKey = data.InfoScriptKey;
                if (infoKey.Contains(","))
                {
                    string[] ids = infoKey.Split(',');
                    infoKey = ids[0];
                }
            }

            if (infoKey != "-" && infoKey.Length > 0)
            {
                CardData cardData = CardData.CreateCard(infoKey);
                CardUIData uiData = new CardUIData(0, cardData, data.CardBackID);
                SetData(uiData);
                Ability.gameObject.SetActive(false);
                TokenGroup.SetActive(true);
                AbilitySub.gameObject.SetActive(true);
            }
            else
            {
                Ability.gameObject.SetActive(true);
                TokenGroup.SetActive(false);
                AbilitySub.gameObject.SetActive(false);
            }

            Ability.text = data.Description;
            AbilitySub.text = data.Description;
        }

        private void SetData(CardUIData data)
        {
            Color color = new Color();
            SetAtk(data.ATKDefault, data);
            SetHp(data.HPDefault, data);
            SetSpirit(data.SpiritDefault);
            ShowDirection(data.CardDir);
            SetFrame(data);
            SetImage(data);
            ShowPassiveIcon(data);

            //Color
            color = GameHelper.GetColor_PlayerBlue();
            SetColorPlayer(color, ArrowBlue);

            if (data.Type == CardType.Minion || data.Type == CardType.Minion_NotInDeck)
            {
                AttackImage.gameObject.SetActive(true);
                HpImage.gameObject.SetActive(true);
                Atk.gameObject.SetActive(true);
                Hp.gameObject.SetActive(true);
            }
            else if (data.Type == CardType.Spell)
            {
                AttackImage.gameObject.SetActive(false);
                HpImage.gameObject.SetActive(false);
                Atk.gameObject.SetActive(false);
                Hp.gameObject.SetActive(false);
                PassiveIcon.gameObject.SetActive(false);
            }
        }

        private void SetAtk(int value, CardUIData data)
        {
            Color color = new Color();
            if (value < data.ATKBase)
            {
                color = Color.red;
            }
            else
            {
                if (data.ATKBase > data.ATKDefault)
                {
                    color = Color.green;
                }
                else
                {
                    color = Color.white;
                }
            }
            Atk.text = value.ToString();
            Atk.color = color;
        }

        private void SetHp(int value, CardUIData data)
        {
            Color color = new Color();
            if (value < data.HPBase)
            {
                color = Color.red;
            }
            else
            {
                if (data.HPBase > data.HPDefault)
                {
                    color = Color.green;
                }
                else
                {
                    color = Color.white;
                }
            }
            Hp.text = value.ToString();
            Hp.color = color;
        }

        private void SetSpirit(int value)
        {
            Spirit.text = value.ToString();
            //Spirit.text = GameHelper.GetIntToRomanNumber(value).ToString();
        }

        private void SetColorPlayer(Color color, Sprite mat)
        {
            foreach (GameObject item in ArrowDir)
            {
                item.GetComponent<Image>().sprite = mat;
            }
        }

        private void SetFrame(CardUIData data)
        {
            if (data.Type == CardType.Minion || data.Type == CardType.Minion_NotInDeck || data.Type == CardType.Spell)
            {
                Sprite tempSprite;
                CardResourceManager.LoadTokenFrame(data.Rarity, out tempSprite);
                Frame.sprite = tempSprite;
            }
        }

        private void ShowDirection(CardDirection dirValue)
        {
            foreach (CardDirectionType item in System.Enum.GetValues(typeof(CardDirectionType)))
            {
                bool isShow = dirValue.IsDirection(item);
                ArrowDir[(int)item].SetActive(isShow);
            }
        }

        private void SetImage(CardUIData data)
        {
            // Clear previous image
            {
                foreach (Transform child in ArtGroup.transform)
                {
                    Destroy(child.gameObject);
                }
            }

            GameObject obj;
            if (CardResourceManager.Create(CardResourceManager.ImageType.InfoToken, data.ImageKey, ArtGroup.transform, false, out obj))
            {
                obj.transform.localPosition = Vector3.zero;
                obj.transform.localRotation = Quaternion.identity;
            }
        }

        public void ShowPassiveIcon(CardUIData cardUIData)
        {
            if (cardUIData.FeedbackList != null && cardUIData.FeedbackList.Count > 0)
            {
                foreach (AbilityFeedback icon in cardUIData.FeedbackList)
                {
                    switch (icon)
                    {
                        case AbilityFeedback.Trigger:
                        {
                            PassiveIcon.sprite = SpriteTrigger;
                            PassiveIcon.gameObject.SetActive(true);
                            return;
                        }
                        break;

                        case AbilityFeedback.Backstab:
                        {
                            PassiveIcon.sprite = SpriteCritical;
                            PassiveIcon.gameObject.SetActive(true);
                            return;
                        }
                        break;

                        case AbilityFeedback.Lastwish:
                        {
                            PassiveIcon.sprite = SpriteLastWish;
                            PassiveIcon.gameObject.SetActive(true);
                            return;
                        }
                        break;

                        case AbilityFeedback.Link:
                        {
                            PassiveIcon.sprite = SpriteLink;
                            PassiveIcon.gameObject.SetActive(true);
                            return;
                        }
                        break;

                        case AbilityFeedback.Berserk:
                        {
                            PassiveIcon.sprite = SpriteBerserk;
                            PassiveIcon.gameObject.SetActive(true);
                            return;
                        }
                        break;

                        default:
                        {
                            PassiveIcon.gameObject.SetActive(false);
                        }
                        break;
                    }
                }
            }
            else
            {
                PassiveIcon.gameObject.SetActive(false);
            }
        }
        #endregion
    }
}
