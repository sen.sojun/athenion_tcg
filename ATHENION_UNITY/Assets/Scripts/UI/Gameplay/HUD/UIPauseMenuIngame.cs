﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIPauseMenuIngame : MonoBehaviour
{
    #region Public Properties
    [Header("Options")]
    public OptionController Options;

    [Header("BTN")]
    public Button BTN_Surrender;
    public Button BTN_Options;
    public Button BTN_BlackClose;
    public Button BTN_Close;
    #endregion

    #region Methods
    public void ShowUI()
    {
        InitBTN();

        GameHelper.UITransition_FadeIn(this.gameObject);
    }

    public void HideUI()
    {
        Options.HideUI();
        GameHelper.UITransition_FadeOut(this.gameObject);
    }

    private void InitBTN()
    {
        BTN_Surrender.onClick.RemoveAllListeners();
        BTN_Surrender.onClick.AddListener(delegate
        {
            string title = LocalizationManager.Instance.GetText("TEXT_SURRENDER");
            string message = LocalizationManager.Instance.GetText("TEXT_SURRENDER_DESCRIPTION");
            PopupUIManager.Instance.ShowPopup_SureCheck(title, message, OnClickQuit, null);
        });

        BTN_Options.onClick.RemoveAllListeners();
        BTN_Options.onClick.AddListener(Options.ShowUI);

        BTN_BlackClose.onClick.RemoveAllListeners();
        BTN_BlackClose.onClick.AddListener(HideUI);

        BTN_Close.onClick.RemoveAllListeners();
        BTN_Close.onClick.AddListener(HideUI);
    }

    private void OnClickQuit()
    {
        HideUI();
        if (GameHelper.IsHaveObject(typeof(GameManager)))
        {
            GameManager.Instance.ForceExitGame();
        }        
    }    
    #endregion
}
