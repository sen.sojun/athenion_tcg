﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class APIcon : MonoBehaviour {

    private bool _isCasting = false;
    private bool _isSpare = false;
    private bool _isStandby = true;

    public void ShowCasting(bool isShow)
    {
        if (_isCasting != isShow)
        {
            _isCasting = isShow;
            GetComponent<Animator>().SetBool("IsCasting", _isCasting);
        }

    }

    public void ShowRegen()
    {
        if (_isStandby)
        {
            GetComponent<Animator>().SetTrigger("IsRegen");
            _isStandby = false;
        }
    }

    public void ShowSpare()
    {
        GetComponent<Animator>().SetTrigger("IsSpare");
    }

    public void ShowCardUseEffect()
    {
        GetComponent<Animator>().SetTrigger("UseCard");
        GetComponent<Animator>().SetBool("IsCasting", false);
        _isStandby = true;
        _isCasting = false;
    }

    public void ShowDrawUseEffect()
    {
        GetComponent<Animator>().SetTrigger("UseDraw");
        _isStandby = true;
    }
}
