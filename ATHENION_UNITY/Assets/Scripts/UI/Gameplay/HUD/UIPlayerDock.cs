﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;
using UnityEngine.UI;
using Karamucho;
using Karamucho.UI;
using UnityEngine.Events;

public class UIPlayerDock : MonoBehaviour
{

    public enum HighlightDock { AP, HP, SOUL }

    #region Public Properties
    [Header("UI GROUP")]
    [SerializeField] private Transform GROUP_HERO;
    [SerializeField] private Transform GROUP_SOUL;
    [SerializeField] private Transform GROUP_HP;
    [SerializeField] private Transform GROUP_Name;
    [SerializeField] private Transform GROUP_AP;

    [Header("Player")]
    [SerializeField] private SpriteRenderer Avatar;
    [SerializeField] private Transform HERO_TARGET;

    [Header("Hero Spine")]
    [SerializeField] private Transform HERO_SpineGroup;
    [SerializeField] private Transform HERO_SpineMask;

    [Header("Hero Sprite")]
    [SerializeField] private bool ForceSprite = false;
    [SerializeField] private Transform HERO_SpriteGroup;
    [SerializeField] private GameObject HERO_SpriteFrame;
    [SerializeField] private GameObject HERO_SpriteMask;

    [Header("TEXT")]
    [SerializeField] private TextMeshPro TEXT_Soul;
    [SerializeField] private TextMeshPro TEXT_Life;
    [SerializeField] private TextMeshPro TEXT_PlayerName;
    [SerializeField] private TextMeshPro TEXT_AP;
    [SerializeField] private TextMeshPro TEXT_TurnCount;

    [Header("Count")]
    [SerializeField] private GameObject GROUP_DeckCount;
    [SerializeField] private GameObject GROUP_GraveCount;
    [SerializeField] private TextMeshPro TEXT_DeckCount;
    [SerializeField] private TextMeshPro TEXT_GraveCount;

    [Header("HUD")]
    [SerializeField] private Transform HUDGroup;

    [Header("Animator")]
    [SerializeField] private Animator ANIMATOR_HP;

    [Header("Soul Warning")]
    [SerializeField] private GameObject P_SOUL1;
    [SerializeField] private GameObject P_SOUL2;
    [SerializeField] private GameObject P_SOUL3;

    [Header("Cost")]
    [SerializeField] private List<GameObject> CostList;

    [Header("Effect")]
    [SerializeField] private GameObject Effect_Immune;
    [SerializeField] private GameObject Effect_DeckAdd;
    [SerializeField] private GameObject Effect_GraveRemove;

    [Header("Tutorial")]
    [SerializeField] private GameObject Hilight_HP;
    [SerializeField] private GameObject Hilight_AP;
    [SerializeField] private GameObject Hilight_SOUL;

    [Header("Introduction")]
    [SerializeField] private GameObject P_Splash;
    [SerializeField] private Transform MARKER_CenterScreen;
    #endregion

    #region Private Properties
    private string _heroID;
    private int _currentPower = 0;
    private int _currentLife = 0;
    private int _currentAP = 0;
    private int _currentGrave = 0;
    private int _currentDeck = 40;
    private string _currentImgKey;
    private IngameHeroAnimation _spineHero;
    private GameObject _imageObject;
    private Sequence _sq;
    private bool _isVoicePlaying = false;
    #endregion

    #region Methods
    public void SetAvatar(string imgKey)
    {
        Avatar.sprite = SpriteResourceHelper.LoadSprite(imgKey, SpriteResourceHelper.SpriteType.ICON_Avatar);
    }

    public void SetName(string messsage)
    {
        TEXT_PlayerName.text = messsage;
    }

    public void SetHeroImage(string id)
    {
        _heroID = id;
        bool isAnimated = false && !ForceSprite;
        GameObject obj = null;

        if (CardResourceManager.LoadHero(_heroID, out obj) && !ForceSprite)
        {
            isAnimated = true;
            _imageObject = Instantiate(obj, HERO_SpineGroup);
            _spineHero = _imageObject.GetComponent<IngameHeroAnimation>();
            _spineHero.SetHeroID(_heroID);
        }
        else
        {
            isAnimated = false;
            HERO_SpriteGroup.GetComponent<SpriteRenderer>().sprite = SpriteResourceHelper.LoadSprite(_heroID, SpriteResourceHelper.SpriteType.SKIN_Hero);
        }

        // Show spine
        HERO_SpineGroup.gameObject.SetActive(isAnimated);
        HERO_SpineMask.gameObject.SetActive(isAnimated);

        //Show sprite
        HERO_SpriteGroup.gameObject.SetActive(!isAnimated);
        HERO_SpriteMask.gameObject.SetActive(!isAnimated);
        HERO_SpriteFrame.gameObject.SetActive(!isAnimated);
    }

    public void SetPower(int value)
    {
        if (_currentPower != value)
        {
            _currentPower = value;
            GameHelper.UIAnimation_Bounce(TEXT_Soul.gameObject);
            //Sequence sq = DOTween.Sequence();
            //sq.Append(TEXT_Soul.transform.DOScale(1.5f, 0.3f));
            //sq.Append(TEXT_Soul.transform.DOScale(1.0f, 0.3f));
        }
        TEXT_Soul.text = value.ToString();

        // Soul warning effect
        if (_currentPower < 1)
        {
            P_SOUL1.SetActive(false);
            P_SOUL2.SetActive(false);
            P_SOUL3.SetActive(false);
        }
        else if (_currentPower < 4)
        {
            P_SOUL1.SetActive(true);
            P_SOUL2.SetActive(false);
            P_SOUL3.SetActive(false);
        }
        else if(_currentPower < 7)
        {
            P_SOUL1.SetActive(false);
            P_SOUL2.SetActive(true);
            P_SOUL3.SetActive(false);
        }
        else
        {
            P_SOUL1.SetActive(false);
            P_SOUL2.SetActive(false);
            P_SOUL3.SetActive(true);
        }



    }

    public void SetLife(int value, int maxValue)
    {
        if (_currentLife != value)
        {
            _currentLife = value;
            GameHelper.UIAnimation_Bounce(TEXT_Life.gameObject);
            //Sequence sq = DOTween.Sequence();
            //sq.Append(TEXT_Life.transform.DOScale(1.5f, 0.3f));
            //sq.Append(TEXT_Life.transform.DOScale(1.0f, 0.3f));
        }
        Color color = new Color();
        if (value < maxValue)
        {
            color = GameHelper.GetColor_Number_Decrease();
        }
        else if (value > maxValue)
        {
            color = GameHelper.GetColor_Number_Increase();
        }
        else
        {
            color = Color.white;
        }
        TEXT_Life.text = value.ToString();
        TEXT_Life.color = color;
    }

    public void SetDeckCount(int value)
    {
        if (value > _currentDeck)
        {
            GameHelper.UIAnimation_Bounce(TEXT_DeckCount.gameObject, 2.0f);
            //Effect_DeckAdd.gameObject.SetActive(true);
        }
        _currentDeck = value;

        TEXT_DeckCount.text = value.ToString();
    }

    public void SetGraveCount(int value)
    {
        if (value < _currentGrave)
        {
            GameHelper.UIAnimation_Bounce(TEXT_GraveCount.gameObject, 2.0f);
            //Effect_GraveRemove.gameObject.SetActive(true);
        }
        _currentGrave = value;
        TEXT_GraveCount.text = value.ToString();
    }

    public void SetImmune(bool isImmune, int turnCount = 1)
    {
        if (isImmune)
        {
            //Immune.SetBool("IsImmune", isImmune);
            Effect_Immune.SetActive(true);
            TEXT_TurnCount.text = turnCount.ToString();
        }
        else
        {
            Effect_Immune.SetActive(false);
        }

    }

    public void SetHUD(bool isLocal, string key)
    {
        string path = "Prefabs/HUDs/Prefabs/HUD_";
        string location = "";
        GameObject obj;
        if (isLocal)
        {
            location = "BOT_";
        }
        else
        {
            location = "TOP_";
        }

        obj = ResourceManager.Load(path + location + key) as GameObject;

        // Cannot find hud prefab then clone new prefab.
        if (obj == null)
        {
            // Load Default prefab
            path = path + location + "HUD0000";
            obj = ResourceManager.Load(path) as GameObject;

            // Try to load sprite
            string spritePath = "Prefabs/HUDs/Images/HUD_" + location + key;
            Sprite img = ResourceManager.Load<Sprite>(spritePath);

            GameObject objectHUD = Instantiate(obj, HUDGroup);
            objectHUD.transform.localPosition = Vector3.zero;

            // Assign to sprite renderer
            SpriteRenderer spriteObj = objectHUD.GetComponent<SpriteRenderer>();
            spriteObj.sprite = img;

            Debug.LogWarning("Failsafe: Cannot find hud." + spritePath);
        }
        else
        {
            GameObject objectHUD = Instantiate(obj, HUDGroup);
            objectHUD.transform.localPosition = Vector3.zero;
        }

    }

    public void AnimationLifeWarning(bool isShow)
    {
        if (isShow)
        {
            TEXT_Life.color = Color.white;
            ANIMATOR_HP.SetBool("IsDanger", true);

        }
        else
        {
            ANIMATOR_HP.SetBool("IsDanger", false);
        }

    }

    public void ShowDeck(bool isShow)
    {
        GROUP_DeckCount.SetActive(isShow);
    }

    public void ShowGrave(bool isShow)
    {
        GROUP_GraveCount.SetActive(isShow);
    }

    public void PlayAnimationHeroDialog(PlayerIndex playerIndex, BaseHeroAnimation.HeroAnimationState state, UnityAction onComplete = null)
    {
        // Create Sound
        HeroVoice heroVoice = BaseHeroAnimation.GetVoiceIndexFromAnimationState(state);
        SoundHeroData soundHero = new SoundHeroData(_heroID, heroVoice);

        if (_spineHero != null)
        {
            // Play Spine Animation
            _spineHero.PlayAnimation(soundHero, state, onComplete);
        }
        else
        {
            // Play Voice Only
            SoundManager.PlayHeroVoice(soundHero);
            //if (_isVoicePlaying) return;
            //_isVoicePlaying = true;
            //SoundManager.PlayHeroVoice(soundHero, () => _isVoicePlaying = false);
        }

        ShowHeroDialog(playerIndex, soundHero);

    }

    public void ShowHeroDialog(PlayerIndex playerIndex, SoundHeroData soundData)
    {
        string dialog;
        if (LocalizationManager.Instance.GetText(soundData.VoiceKey, out dialog))
        {
            UIManager.Instance.ShowTauntTalkDialog(playerIndex, dialog);
        }
    }
    #endregion

    #region Get Method
    public Vector3 GetHeroPosition()
    {
        return HERO_TARGET.transform.position;
    }

    public Vector3 GetLifePosition()
    {
        return TEXT_Life.transform.position;
    }

    public Vector3 GetSoulPosition()
    {
        return TEXT_Soul.transform.position;
    }

    public Vector3 GetAPPosition()
    {
        return GROUP_AP.transform.position;
    }
    #endregion

    #region AP Animation
    public void RegenPlayerAP(int value)
    {
        _currentAP += value;
        SetAPText(_currentAP);
        for (int i = 0; i < CostList.Count; i++)
        {
            if (i < _currentAP)
            {
                CostList[i].GetComponent<APIcon>().ShowRegen();
            }
        }
    }

    public void CastingPlayerAp(int value, bool isCast)
    {
        for (int i = _currentAP - 1; i >= 0; i--)
        {
            if (i < CostList.Count)
            {
                if (value > 0)
                {
                    if (isCast)
                    {
                        CostList[i].GetComponent<APIcon>().ShowCasting(true);

                    }
                    else
                    {
                        CostList[i].GetComponent<APIcon>().ShowCasting(false);
                    }
                }
            }
            value--;
        }
    }

    public void UseCardPlayerAp(int value)
    {
        int temp = value;
        for (int i = _currentAP - 1; i >= 0; i--)
        {
            if (value > 0)
            {
                if (i < CostList.Count)
                {
                    CostList[i].GetComponent<APIcon>().ShowCardUseEffect();
                }
            }
            value--;
        }
        _currentAP -= temp;
        SetAPText(_currentAP);
    }

    public void UseDrawPlayerAp(int value)
    {
        for (int i = _currentAP - 1; i >= 0; i--)
        {
            CostList[i].GetComponent<APIcon>().ShowDrawUseEffect();
        }
    }

    private void SetAPText(int ap)
    {
        TEXT_AP.text = string.Format("{0}/{1}", ap, GameManager.Instance.ActionPoint);
    }

    #endregion

    #region Introduction Animation
    public void Intro_State_Default(UnityAction onComplete = null)
    {
        _sq?.Kill();
        _sq = DOTween.Sequence();

        float duration = 0.5f;
        _sq.Append(GROUP_HERO.DOLocalMove(Vector3.zero, duration).SetEase(Ease.InBack)).OnComplete(delegate
        {
            SoundManager.PlaySFX(SoundManager.SFX.Minion_Landing_Gold);
            ShowHUD(true);
            UIManager.Instance.CameraScreenShake(20);
            P_Splash.gameObject.SetActive(true);
            onComplete?.Invoke();
        });
    }

    public void Intro_State_ShowAtCenter(UnityAction onComplete = null)
    {
        _sq?.Kill();
        _sq = DOTween.Sequence();

        float duration = 0.5f;
        ShowHUD(false);
        _sq.Append(GROUP_HERO.DOMove(MARKER_CenterScreen.position, duration).SetEase(Ease.OutExpo)).OnComplete(delegate
        {
            onComplete?.Invoke();
        });
    }

    public void ShowHUD(bool isShow)
    {
        GROUP_SOUL.gameObject.SetActive(isShow);
        GROUP_AP.gameObject.SetActive(isShow);
        GROUP_HP.gameObject.SetActive(isShow);
        GROUP_Name.gameObject.SetActive(isShow);
        GROUP_DeckCount.gameObject.SetActive(isShow);
        GROUP_GraveCount.gameObject.SetActive(isShow);
    }
    #endregion

    #region Tutorial
    private Coroutine _highlightSoulRoutine = null;
    public void Tutorial_Hilight_Soul(bool isShow, float duration)
    {
        if (isShow)
        {
            _highlightSoulRoutine = StartCoroutine(IEShowHilight_Soul(duration));
        }
        else
        {
            if (_highlightSoulRoutine != null)
            {
                StopCoroutine(_highlightSoulRoutine);
                Hilight_SOUL.SetActive(false);
            }
        }
    }

    private IEnumerator IEShowHilight_Soul(float duration)
    {
        Hilight_SOUL.SetActive(true);
        yield return new WaitForSeconds(duration);
        Hilight_SOUL.SetActive(false);
    }

    private Coroutine _highlightHpRoutine = null;
    public void Tutorial_Hilight_HP(bool isShow, float duration)
    {
        if (isShow)
        {
            _highlightHpRoutine = StartCoroutine(IEShowHilight_HP(duration));
        }
        else
        {
            if (_highlightHpRoutine != null)
            {
                StopCoroutine(_highlightHpRoutine);
                Hilight_HP.SetActive(false);
            }
        }
    }

    private IEnumerator IEShowHilight_HP(float duration)
    {
        Hilight_HP.SetActive(true);
        yield return new WaitForSeconds(duration);
        Hilight_HP.SetActive(false);
    }

    private Coroutine _highlightApRoutine = null;
    public void Tutorial_Hilight_AP(bool isShow, float duration)
    {
        if (isShow)
        {
            _highlightApRoutine = StartCoroutine(IEShowHilight_AP(duration));
        }
        else
        {
            if (_highlightApRoutine != null)
            {
                StopCoroutine(_highlightApRoutine);
                Hilight_AP.SetActive(false);
            }
        }
    }

    private IEnumerator IEShowHilight_AP(float duration)
    {
        Hilight_AP.SetActive(true);
        yield return new WaitForSeconds(duration);
        Hilight_AP.SetActive(false);
    }

    #endregion
}
