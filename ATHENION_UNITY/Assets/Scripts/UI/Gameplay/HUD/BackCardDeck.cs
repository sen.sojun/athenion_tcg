﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackCardDeck : MonoBehaviour
{
    public GameObject Card1, Card2, Card3;

    public void InitData(string cardBackid)
    {
        Card1.GetComponent<SpriteRenderer>().sprite = SpriteResourceHelper.LoadSprite_CardBack(cardBackid);
        Card2.GetComponent<SpriteRenderer>().sprite = SpriteResourceHelper.LoadSprite_CardBack(cardBackid);
        Card3.GetComponent<SpriteRenderer>().sprite = SpriteResourceHelper.LoadSprite_CardBack(cardBackid);
    }

    public void UpdateCardCount(int count)
    {
        Card1.SetActive(count != 0);
        Card2.SetActive(count > 5);
        Card3.SetActive(count > 10);
    }


}
