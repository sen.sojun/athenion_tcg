﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TokenUIFeedbackIcon : MonoBehaviour
{
    [SerializeField] private SpriteRenderer _SpriteIcon;
    [SerializeField] private GameObject _FlashIcon;

    // One time only
    private bool _isLinkActivated = false;

    private AbilityFeedback _abilityFeedback;

    public void Setup(AbilityFeedback abilityFeedback)
    {
        _abilityFeedback = abilityFeedback;

        // Set Icon
        _SpriteIcon.gameObject.SetActive(true);
        _FlashIcon.SetActive(false);
        Sprite sprite = SpriteResourceHelper.LoadSprite(abilityFeedback.ToString().ToUpper(), SpriteResourceHelper.SpriteType.ICON_Keyword);
        _SpriteIcon.sprite = sprite;
    }

    public void ResetFlashState()
    {
        _isLinkActivated = false;
    }

    public void ShowFlash(AbilityFeedback abilityFeedback)
    {
        // Calculate Logic
        if(_abilityFeedback == abilityFeedback)
        {
            if (_abilityFeedback == AbilityFeedback.Link)
            {
                if (_isLinkActivated == true) return;

                _FlashIcon.SetActive(true);

                _isLinkActivated = true;
            }
            else
            {
                _FlashIcon.SetActive(true);
            }
        }
    }
}
