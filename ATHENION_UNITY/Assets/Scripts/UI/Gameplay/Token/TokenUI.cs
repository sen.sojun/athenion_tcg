﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using UnityEngine.EventSystems;

namespace Karamucho.UI
{
    public class TokenUI : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler, IBeginDragHandler, IEndDragHandler
    {
        public CardUIData Data { get; private set; }

        [Header("Reference")]
        public TextMeshProUGUI Atk;
        public TextMeshProUGUI Hp;
        public TextMeshProUGUI Spirit;
        public TextMeshProUGUI Armor;
        public RectTransform ArtGroup;
        public Image Frame;
        public Image Glow;
        //public Image SpiritImage;
        public Image AttackImage;
        public Image HpImage;
        public Image RarityImage;
        public Image ArmorImage;
        public GameObject Foil;

        [Header("Passive Icon")]
        public List<Image> PassiveIconList;

        [Header("Arrow Materials")]
        public Sprite ArrowBlue;
        public Sprite ArrowRed;

        [Header("Arrow")]
        public List<GameObject> ArrowDir = new List<GameObject>();

        private ScrollRect _scroll_graveyard;

        private List<AbilityFeedback> _feedbackPriorityList = new List<AbilityFeedback>()
        {
            AbilityFeedback.Trigger
            , AbilityFeedback.Lastwish
            , AbilityFeedback.Link
            , AbilityFeedback.Berserk
            , AbilityFeedback.Backstab
            , AbilityFeedback.Untargetable
        };

        #region Methods

        private void Start()
        {
            ShowGlow(false);
        }

        public void ShowGlow(bool isShow)
        {
            Glow.gameObject.SetActive(isShow);
        }

        public void SetData(CardUIData data, bool isInBattle = true)
        {
            Color color = new Color();
            SetAtk(data.ATKDefault, data);
            SetHp(data.HPDefault, data);
            SetSpirit(data.SpiritDefault);
            SetArmor(data.ArmorDefault, data);
            ShowDirection(data.CardDir);
            SetFrame(data);
            SetFoil(data);
            SetImage(data);
            ShowPassiveIcon(data);
            Data = data;

            // Set color
            if (isInBattle)
            {
                if (GameManager.Instance.IsLocalPlayer(data.Owner))
                {
                    color = GameHelper.GetColor_PlayerBlue();
                }
                else
                {
                    color = GameHelper.GetColor_PlayerRed();
                }
                SetColorPlayer(color, GameManager.Instance.IsLocalPlayer(data.Owner) ? ArrowBlue : ArrowRed);
            }
            else
            {
                color = GameHelper.GetColor_PlayerBlue();
                SetColorPlayer(color, ArrowBlue);
            }

            if (data.Type == CardType.Minion || data.Type == CardType.Minion_NotInDeck)
            {
                AttackImage.gameObject.SetActive(true);
                HpImage.gameObject.SetActive(true);
                Atk.gameObject.SetActive(true);
                Hp.gameObject.SetActive(true);
            }
            else if (data.Type == CardType.Spell)
            {
                AttackImage.gameObject.SetActive(false);
                HpImage.gameObject.SetActive(false);
                Atk.gameObject.SetActive(false);
                Hp.gameObject.SetActive(false);
                ResetPassiveIcon();
            }
        }

        public void SetScroll(ScrollRect scroll)
        {
            _scroll_graveyard = scroll;
        }

        private void SetFoil(CardUIData data)
        {
            Foil.SetActive(data.IsFoil);
        }

        public void SetArmor(int value, CardUIData data)
        {
            if (data.ArmorDefault <= 0)
            {
                Armor.gameObject.SetActive(false);
                ArmorImage.gameObject.SetActive(false);
            }
            else
            {
                Armor.gameObject.SetActive(true);
                ArmorImage.gameObject.SetActive(true);
            }

            Color color = new Color();
            if (value < data.ArmorBase)
            {
                color = Color.yellow;
            }
            else
            {
                if (data.ArmorBase > data.ArmorDefault)
                {
                    color = Color.green;
                }
                else
                {
                    color = Color.white;
                }
            }
            Armor.text = value.ToString();
            Armor.color = color;
        }

        private void SetAtk(int value, CardUIData data)
        {
            Color color = new Color();
            if (value < data.ATKBase)
            {
                color = Color.red;
            }
            else
            {
                if (data.ATKBase > data.ATKDefault)
                {
                    color = Color.green;
                }
                else
                {
                    color = Color.white;
                }
            }

            Atk.text = value.ToString();
            Atk.color = color;
        }

        private void SetHp(int value, CardUIData data)
        {
            Color color = new Color();
            if (value < data.HPBase)
            {
                color = Color.red;
            }
            else
            {
                if (data.HPBase > data.HPDefault)
                {
                    color = Color.green;
                }
                else
                {
                    color = Color.white;
                }
            }

            Hp.text = value.ToString();
            Hp.color = color;
        }

        private void SetSpirit(int value)
        {
            Spirit.text = value.ToString();
            //Spirit.text = GameHelper.GetIntToRomanNumber(value).ToString();
        }

        private void SetColorPlayer(Color color, Sprite mat)
        {
            foreach (GameObject item in ArrowDir)
            {
                item.GetComponent<Image>().sprite = mat;
            }
        }

        private void SetFrame(CardUIData data)
        {
            if (data.Type == CardType.Minion || data.Type == CardType.Minion_NotInDeck || data.Type == CardType.Spell)
            {
                Sprite tempSprite;
                CardResourceManager.LoadTokenFrame(data.Rarity, out tempSprite);
                Frame.sprite = tempSprite;
            }
        }

        private void ShowDirection(CardDirection dirValue)
        {
            foreach (CardDirectionType item in System.Enum.GetValues(typeof(CardDirectionType)))
            {
                bool isShow = dirValue.IsDirection(item);
                ArrowDir[(int)item].SetActive(isShow);
            }
        }

        public void SetEffectDirection(CardDirection dirValue)
        {
            UIEffectManager.Instance.SpawnVFX(CardVFXPool.VFXType.TokenArrowChange, transform.position, null);
            foreach (CardDirectionType item in System.Enum.GetValues(typeof(CardDirectionType)))
            {
                bool isShow = dirValue.IsDirection(item);
                ArrowDir[(int)item].SetActive(isShow);
            }
        }

        private void SetImage(CardUIData data)
        {
            // Clear previous image
            {
                foreach (Transform child in ArtGroup.transform)
                {
                    Destroy(child.gameObject);
                }
            }

            GameObject obj;
            if (CardResourceManager.Create(CardResourceManager.ImageType.InfoToken, data.ImageKey, ArtGroup.transform, false, out obj))
            {
                obj.transform.localPosition = Vector3.zero;
                obj.transform.localRotation = Quaternion.identity;
                SetMask(data);
            }
        }

        private void SetMask(CardUIData data)
        {
            foreach (Transform child in ArtGroup.transform)
            {
                if (data.Type == CardType.Minion || data.Type == CardType.Minion_NotInDeck || data.Type == CardType.Spell)
                {
                    Sprite tempSprite;
                    CardResourceManager.LoadTokenMaskFrame(data.Rarity, out tempSprite);
                    child.GetComponent<Image>().sprite = tempSprite;
                }
            }
        }
        #endregion

        #region Passive
        public void ShowPassiveIcon(CardUIData cardUIData)
        {
            // Reset
            int count = 0;
            ResetPassiveIcon();

            if (cardUIData.FeedbackList != null && cardUIData.FeedbackList.Count > 0)
            {
                foreach (AbilityFeedback feedback in _feedbackPriorityList)
                {
                    if (cardUIData.FeedbackList.Contains(feedback))
                    {
                        if (count < PassiveIconList.Count)
                        {
                            PassiveIconList[count].gameObject.SetActive(true);
                            SetupIconPassive(PassiveIconList[count], feedback);
                            count++;
                        }
                        else
                        {
                            return;
                        }

                    }
                }
            }
        }

        public void ResetPassiveIcon()
        {
            foreach (Image item in PassiveIconList)
            {
                item.gameObject.SetActive(false);
            }
        }

        public void SetupIconPassive(Image icon, AbilityFeedback abilityFeedback)
        {
            // Set Icon
            icon.gameObject.SetActive(true);
            Sprite sprite = SpriteResourceHelper.LoadSprite(abilityFeedback.ToString().ToUpper(), SpriteResourceHelper.SpriteType.ICON_Keyword);
            icon.sprite = sprite;
        }

        #endregion

        #region Events
        public void OnPointerDown(PointerEventData eventData)
        {
            if (!UIManager.Instance.IsHilightBoxShowing)
            {
                //Debug.Log("Down");
                SoundManager.PlaySFX(SoundManager.SFX.Graveyard_Click);
                UIManager.Instance.UIGraveyard.HilightGraveToken(this);
            }
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (UIManager.Instance.IsHilightBoxShowing)
            {
                //Debug.Log("Up");
                UIManager.Instance.UIGraveyard.HideGraveHilight();
            }
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            _scroll_graveyard.OnBeginDrag(eventData);
        }

        public void OnDrag(PointerEventData eventData)
        {
            _scroll_graveyard.OnDrag(eventData);
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            _scroll_graveyard.OnEndDrag(eventData);

        }
        #endregion
    }
}

