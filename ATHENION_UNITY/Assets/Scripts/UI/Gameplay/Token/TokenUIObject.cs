﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;
using TMPro;
using DG.Tweening;

namespace Karamucho.UI
{
    public class TokenUIObject : MonoBehaviour
    {
        public GameObject Group;

        [Header("Reference")]
        public TextMeshPro Atk;
        public TextMeshPro Hp;
        public TextMeshPro Spirit;
        public TextMeshPro Armor;
        public GameObject ArtworkGroup;
        public SpriteRenderer Artwork;
        public SpriteRenderer Frame;
        public SpriteRenderer Glow;
        public SpriteRenderer GlowAttacking;
        public ParticleSystem P_GlowAttacking;
        public SpriteRenderer EnemyHoverGlow;
        public SpriteRenderer ColorGlow;
        public SpriteRenderer AtkImage;
        public SpriteRenderer HpImage;
        public SpriteRenderer ArmorImage;
        public GameObject EffectActivation;
        public GameObject Foil;

        [Header("Status Effect")]
        public GameObject Freeze;
        public GameObject Fear;
        public GameObject Stealth;
        public GameObject Silence;
        public GameObject SilenceCast;
        public GameObject Taunt;
        public GameObject Aura;
        public GameObject Berserk;
        public GameObject DarkMatter;
        public GameObject NormalArrow;
        public GameObject Pierce;

        [Header("Passive Icon")]
        public List<TokenUIFeedbackIcon> PassiveIconList;

        [Header("Arrow Materials")]
        public Sprite ArrowBlue;
        public Sprite ArrowRed;
        public Sprite ArrowBlue_Pierce;
        public Sprite ArrowRed_Pierce;

        [Header("Arrow")]
        [SerializeField] private List<GameObject> ArrowDir = new List<GameObject>();
        [SerializeField] private List<GameObject> ArrowHilight = new List<GameObject>();
        [SerializeField] private List<GameObject> ArrowPierce = new List<GameObject>();

        [Header("Tutorial")]
        [SerializeField] private GameObject PTU_Soul;
        [SerializeField] private GameObject PTU_ATK;
        [SerializeField] private GameObject PTU_HP;
        [SerializeField] private GameObject PTU_ARMOR;
        [SerializeField] private GameObject PTU_Passive;

        #region Private Properties
        private CardUIData _data;
        private int _tempAtk;
        private int _tempHp;
        private int _tempPower;
        private Sequence _sqAtk, _sqHp, _sqPower, _sqToken;

        private List<AbilityFeedback> _feedbackPriorityList = new List<AbilityFeedback>()
        {
            AbilityFeedback.Trigger
            , AbilityFeedback.Lastwish
            , AbilityFeedback.Link
            , AbilityFeedback.Berserk
            , AbilityFeedback.Backstab
            , AbilityFeedback.Untargetable
        };

        #endregion

        #region Methods

        private void Start()
        {
            ShowGlow(false);
            //EffectPassiveAnimation();
        }

        private void Update()
        {

        }

        public void ShowGlow(bool isShow)
        {
            Glow.gameObject.SetActive(isShow);
        }

        public void ShowGlowAttacking(bool isShow)
        {
            Color color = new Color();
            if (GameManager.Instance.IsLocalPlayer(_data.Owner))
            {
                color = GameHelper.GetColor_ATK_PlayerBlue();
            }
            else
            {
                color = GameHelper.GetColor_ATK_PlayerRed();
            }
            var main = P_GlowAttacking.main;
            main.startColor = color;
            GlowAttacking.color = color;
            GlowAttacking.gameObject.SetActive(isShow);
        }

        public void ShowEnemyHover(bool isShow)
        {
            EnemyHoverGlow.gameObject.SetActive(isShow);
        }

        public void SetData(CardUIData data, bool isInBattle = true)
        {
            _data = data;
            Color color = new Color();
            SetAtk(data.ATKCurrent, data);
            SetHp(data.HPCurrent, data);
            SetSpirit(data.SpiritCurrent, data);
            SetArmor(data.ArmorCurrent, data);
            ShowDirection(data.CardDir);
            SetFoil(data);
            SetFrame(data);
            SetImage(data);
            ShowPassiveIcon(data);

            // Set color
            if (isInBattle)
            {
                if (GameManager.Instance.IsLocalPlayer(data.Owner))
                {
                    color = GameHelper.GetColor_PlayerBlue();
                }
                else
                {
                    color = GameHelper.GetColor_PlayerRed();
                }
                SetColorPlayer(color, GameManager.Instance.IsLocalPlayer(data.Owner) ? ArrowBlue : ArrowRed);
                SetColorPlayer_Pierce(color, GameManager.Instance.IsLocalPlayer(data.Owner) ? ArrowBlue_Pierce : ArrowRed_Pierce);
            }
            else
            {
                color = GameHelper.GetColor_PlayerBlue();
                SetColorPlayer(color, ArrowBlue);
                SetColorPlayer_Pierce(color, ArrowBlue_Pierce);
            }

            if (data.Type == CardType.Minion || data.Type == CardType.Minion_NotInDeck)
            {
                AtkImage.gameObject.SetActive(true);
                HpImage.gameObject.SetActive(true);
                Atk.gameObject.SetActive(true);
                Hp.gameObject.SetActive(true);
                Spirit.gameObject.SetActive(true);
                //Spirit.gameObject.SetActive(true);
            }
            else if (data.Type == CardType.Spell || data.Type == CardType.Spell_NotInDeck || data.Type == CardType.Hero)
            {
                AtkImage.gameObject.SetActive(false);
                HpImage.gameObject.SetActive(false);
                Atk.gameObject.SetActive(false);
                Hp.gameObject.SetActive(false);
                Spirit.gameObject.SetActive(false);
                ResetPassiveIcon();
            }
        }

        public void SetArmor(int value, CardUIData data)
        {
            if (data.ArmorCurrent <= 0)
            {
                Armor.gameObject.SetActive(false);
                ArmorImage.gameObject.SetActive(false);
            }
            else
            {
                Armor.gameObject.SetActive(true);
                ArmorImage.gameObject.SetActive(true);
            }

            Color color = new Color();
            if (value < data.ArmorBase)
            {
                color = GameHelper.GetColor_Number_Decrease();
            }
            else
            {
                if (data.ArmorBase > data.ArmorDefault)
                {
                    color = GameHelper.GetColor_Number_Increase();
                }
                else
                {
                    color = GameHelper.GetColor_Number_Normal();
                }
            }

            Armor.text = value.ToString();
            Armor.color = color;
        }

        private void SetAtk(int value, CardUIData data)
        {
            Color color = new Color();
            if (value < data.ATKBase)
            {
                color = GameHelper.GetColor_Number_Decrease();
            }
            else
            {
                if (data.ATKBase > data.ATKDefault)
                {
                    color = GameHelper.GetColor_Number_Increase();
                }
                else
                {
                    color = GameHelper.GetColor_Number_Normal();
                }
            }

            Atk.text = value.ToString();
            Atk.color = color;

            if (value != _tempAtk)
            {
                _tempAtk = value;
                _sqAtk.Kill();
                Atk.transform.localScale = new Vector3(1, 1, 1);
                _sqAtk = DOTween.Sequence();
                _sqAtk.Append(Atk.transform.DOScale(1.5f, 0.3f));
                _sqAtk.Append(Atk.transform.DOScale(1.0f, 0.3f));
            }

        }

        private void SetHp(int value, CardUIData data)
        {
            Color color = new Color();
            if (value < data.HPBase)
            {
                color = GameHelper.GetColor_Number_Decrease();
            }
            else
            {
                if (data.HPBase > data.HPDefault)
                {
                    color = GameHelper.GetColor_Number_Increase();
                }
                else
                {
                    color = GameHelper.GetColor_Number_Normal();
                }
            }

            Hp.text = value.ToString();
            Hp.color = color;

            if (value != _tempHp)
            {
                _tempHp = value;
                _sqHp.Kill();
                Hp.transform.localScale = new Vector3(1, 1, 1);
                _sqHp = DOTween.Sequence();
                _sqHp.Append(Hp.transform.DOScale(1.5f, 0.3f));
                _sqHp.Append(Hp.transform.DOScale(1.0f, 0.3f));
            }
        }

        private void SetSpirit(int value, CardUIData data)
        {            
            Color color = new Color();
            if (value < data.SpiritBase)
            {
                color = GameHelper.GetColor_Number_Decrease();
            }
            else
            {
                if (data.SpiritBase > data.SpiritDefault)
                {
                    color = GameHelper.GetColor_Number_Increase();
                }
                else
                {
                    color = Color.white;
                }
            }

            //Spirit.text = GameHelper.GetIntToRomanNumber(value).ToString();
            Spirit.text = value.ToString();
            Spirit.color = color;

            if (value != _tempPower)
            {
                _tempPower = value;
                _sqPower.Kill();
                Spirit.transform.localScale = new Vector3(1, 1, 1);
                _sqPower = DOTween.Sequence();
                _sqPower.Append(Spirit.transform.DOScale(1.5f, 0.3f));
                _sqPower.Append(Spirit.transform.DOScale(1.0f, 0.3f));
            }
        }

        private void SetColorPlayer(Color color, Sprite mat)
        {
            ColorGlow.color = color;
            foreach (GameObject item in ArrowDir)
            {
                item.GetComponent<SpriteRenderer>().sprite = mat;
            }
        }

        private void SetColorPlayer_Pierce(Color color, Sprite mat)
        {
            ColorGlow.color = color;
            foreach (GameObject item in ArrowPierce)
            {
                item.GetComponent<SpriteRenderer>().sprite = mat;
            }
        }


        private void SetFoil(CardUIData data)
        {
            Foil.SetActive(data.IsFoil);
        }

        private void SetFrame(CardUIData data)
        {
            if (data.Type == CardType.Minion || data.Type == CardType.Minion_NotInDeck || data.Type == CardType.Spell)
            {
                Sprite tempSprite;
                Sprite tempSpriteMask;
                CardResourceManager.LoadTokenFrame(data.Rarity, out tempSprite);
                CardResourceManager.LoadTokenMaskFrame(data.Rarity, out tempSpriteMask);
                Frame.sprite = tempSprite;
                Artwork.sprite = tempSpriteMask;
            }
        }

        private void ShowDirection(CardDirection dirValue)
        {
            foreach (CardDirectionType item in System.Enum.GetValues(typeof(CardDirectionType)))
            {
                bool isShow = dirValue.IsDirection(item);
                ArrowDir[(int)item].SetActive(isShow);
                ArrowPierce[(int)item].SetActive(isShow);
            }
        }

        public void SetEffectDirection(CardDirection dirValue)
        {
            UIEffectManager.Instance.SpawnVFX(CardVFXPool.VFXType.TokenArrowChange, transform.position, null);
            foreach (CardDirectionType item in System.Enum.GetValues(typeof(CardDirectionType)))
            {
                bool isShow = dirValue.IsDirection(item);
                ArrowDir[(int)item].SetActive(isShow);
            }
        }

        private void SetImage(CardUIData data)
        {
            string path = "Images/Cards/ArtTokenImages/ART_TOKEN_" + data.ImageKey;
            Texture texture = ResourceManager.Load(path) as Texture;

            if (texture != null)
            {
                Artwork.material.SetTexture("_NewTex_1", texture);
            }
            else
            {
                path = "Images/Cards/ArtTokenImages/ART_TOKEN_" + "IM0000";
                texture = ResourceManager.Load(path) as Texture;
                Artwork.material.SetTexture("_NewTex_1", texture);
            }

            /*
            // Clear previous image
            foreach (Transform child in ArtworkGroup.transform)
            {
                Destroy(child.gameObject);
            }

            GameObject obj;
            if (CardResourceManager.Create(CardResourceManager.ImageType.Token, data.ImageKey, ArtworkGroup.transform, false, out obj))
            {
                obj.transform.localPosition = Vector3.zero;
                obj.transform.localRotation = Quaternion.identity;
            }
            */
        }

        public void ShowCard(bool isShow)
        {
            if (!isShow)
            {
                Group.SetActive(isShow);
            }
        }

        public void CardShake(float power = 1)
        {
            Vector3 jumpPosition = Vector3.back * 0.2f * power;
            Vector3 punchRotation = Vector3.up * 5f;
            if (_data != null)
            {
                if (_data.UniqueID % 2 == 0)
                {
                    punchRotation = Vector3.up * 5f;
                }
                else
                {
                    punchRotation = Vector3.down * 5f;
                }
            }
            if (_sqToken != null)
            {
                _sqToken.Kill();
                _sqToken = null;
            }
            _sqToken = DOTween.Sequence();
            _sqToken.Append(transform.DOPunchPosition(jumpPosition, 0.25f,5,0.5f));
            _sqToken.Join(transform.DOPunchRotation(punchRotation, 0.4f, 8, 0.7f));
            _sqToken.OnKill(delegate {
                transform.localRotation = Quaternion.identity;
                transform.localPosition = Vector3.zero;
            });
        }
        #endregion

        #region Effect
        public void ShowFreeze()
        {
            Freeze.SetActive(true);
        }

        public void HideFreeze()
        {
            Freeze.SetActive(false);
        }

        public void ShowFear()
        {
            Fear.SetActive(true);
        }

        public void HideFear()
        {
            Fear.SetActive(false);
        }

        public void ShowStealth()
        {
            SoundManager.PlaySFX(SoundManager.SFX.Ability_Stealth_In);
            Stealth.SetActive(true);
        }

        public void HideStealth()
        {
            Stealth.SetActive(false);
        }

        public void ShowSilence()
        {
            SoundManager.PlaySFX(SoundManager.SFX.Ability_Silence_In);
            SilenceCast.SetActive(true);
            Silence.SetActive(true);
        }

        public void HideSilence()
        {
            Silence.SetActive(false);
        }

        public void ShowTaunt()
        {
            Taunt.SetActive(true);
        }

        public void HideTaunt()
        {
            Taunt.SetActive(false);
        }

        public void ShowAura()
        {
            Aura.SetActive(true);
        }

        public void HideAura()
        {
            Aura.SetActive(false);
        }

        public void ShowBerserk()
        {
            Berserk.SetActive(true);
        }

        public void HideBerserk()
        {
            Berserk.SetActive(false);
        }

        public void ShowDarkMatter()
        {
            DarkMatter.SetActive(true);
        }

        public void HideDarkMatter()
        {
            DarkMatter.SetActive(false);
        }

        public void ShowPierce()
        {
            SoundManager.PlaySFX(SoundManager.SFX.Arrow_Changed);
            Pierce.SetActive(true);
            NormalArrow.SetActive(false);
        }

        public void HidePierce()
        {
            Pierce.SetActive(false);
            NormalArrow.SetActive(true);
        }

        public void ShowEffectActivating(bool isShow)
        {
            EffectActivation.SetActive(isShow);
        }
        #endregion

        #region Passive
        public void ShowPassiveIcon(CardUIData cardUIData)
        {
            // Reset
            int count = 0;
            ResetPassiveIcon();

            if (cardUIData.FeedbackList != null && cardUIData.FeedbackList.Count > 0)
            {
                foreach(AbilityFeedback feedback in _feedbackPriorityList)
                {
                    if(cardUIData.FeedbackList.Contains(feedback))
                    {
                        if (count < PassiveIconList.Count)
                        {
                            PassiveIconList[count].gameObject.SetActive(true);
                            PassiveIconList[count].Setup(feedback);
                            count++;
                        }
                        else
                        {
                            return;
                        }

                    }
                }
            }
        }

        public void ResetPassiveIcon()
        {
            foreach (TokenUIFeedbackIcon item in PassiveIconList)
            {
                item.ResetFlashState();
                item.gameObject.SetActive(false);
            }
        }

        public void PassiveActivate(AbilityFeedback abilityFeedback)
        {
            foreach (TokenUIFeedbackIcon item in PassiveIconList)
            {
                item.ShowFlash(abilityFeedback);
            }
        }

        #endregion

        #region Tutorial
        public void Tutorial_HilightDirection(bool isShow)
        {
            foreach (CardDirectionType item in System.Enum.GetValues(typeof(CardDirectionType)))
            {
                bool isShowDir = _data.CardDir.IsDirection(item);
                ArrowHilight[(int)item].SetActive(isShowDir && isShow);
            }
        }

        public void Tutorial_HilightDirection(bool isShow, CardDirection dirValue)
        {
            foreach (CardDirectionType item in System.Enum.GetValues(typeof(CardDirectionType)))
            {
                bool isShowDir = dirValue.IsDirection(item) && _data.CardDir.IsDirection(item);
                ArrowHilight[(int)item].SetActive(isShowDir && isShow);
            }
        }

        public void Tutorial_HilightSoul(bool isShow)
        {
            PTU_Soul.SetActive(isShow);
        }

        public void Tutorial_HilightATK(bool isShow)
        {
            PTU_ATK.SetActive(isShow);
        }

        public void Tutorial_HilightHP(bool isShow)
        {
            PTU_HP.SetActive(isShow);
        }

        public void Tutorial_HilightARMOR(bool isShow)
        {
            PTU_ARMOR.SetActive(isShow);
        }

        public void Tutorial_HilightPassive(bool isShow)
        {
            PTU_Passive.SetActive(isShow);
        }
        #endregion
    }
}
