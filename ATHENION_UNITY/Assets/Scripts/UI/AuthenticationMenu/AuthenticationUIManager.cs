using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

#if UNITY_ANDROID
using GooglePlayGames;
#endif

public class AuthenticationUIManager : MonoBehaviour
{
    [Header("Panel")]
    public GameObject LoginPanel;
    public GameObject RegisterPanel;
    public GameObject RecoverAccountPanel;
    public GameObject ReferralCodePanel;
    public GameObject DisplayNameSettingPanel;
    public GameObject ConnectingPanel;
    [System.Obsolete("",true)]public Animator Anim_ConnectPanel;
    public UIPopupBox PopupBox;

    [Header("Login")]
    public TMP_InputField EmailLoginInput;
    public TMP_InputField PasswordLoginInput;
    public TMP_Text ReportTextLogin;

    [Header("Register")]
    public TMP_InputField EmailRegisterInput;
    public TMP_InputField PasswordRegisterInput;
    public TMP_InputField ConfirmPasswordRegisterInput;
    public TMP_Text ReportTextRegister;

    [Header("Recover Account")]
    public TMP_InputField EmailRecoverInput;
    public TMP_Text RecoverRespondText;
    public TMP_Text ReportTextRecover;

    [Header("ReferralCode Panel")]
    public TMP_InputField ReferralCodeInput;
    public TMP_Text ReportTextReferral;

    [Header("DisplayName Setting")]
    public TMP_InputField DisplayNameInput;
    public TMP_Text ReportTextDisplayName;

    [Header("Other")]
    //public LoadingUI LoadingUI;

    private string _displayName;
    private float _initTimeStart = 0.0f;
    private List<UnityAction<int, UnityAction<int>, UnityAction<int, string>, UnityAction<int, float>>> _taskList = null;
    private List<UnityAction<int, float>> _onUpdateList = null;

    private float[] _taskProgess = null;

    private void Start()
    {
        GameHelper.FirstInit();
        SoundManager.PlayBGM(SoundManager.BGM.Login, 1.0f);

        if (AuthenticationManager.IsAutoAuth())
        {
            DoAutoLogin();
        }
    }

    #region Auto Login
    public void DoAutoLogin()
    {
        OpenConnectingPanel(true);
        AuthTypes authtype = AuthenticationManager.GetAuthType();
        switch (authtype)
        {
            case AuthTypes.Guest:
            {
                AuthenticationManager.AuthenticateAsGuest(OnGuestComplete, OnGuestFail);
            }
            break;

            case AuthTypes.EmailAndPassword:
            case AuthTypes.Facebook:
            case AuthTypes.GooglePlayGames:
            {
                AuthenticationManager.AuthenticateAutoAuth(OnLoginComplete, OnLoginFail);
            }
            break;

            default:
            {
                OpenConnectingPanel(false);
                AuthenticationManager.SetAutoAuth(0);
            }
            break;
        }
    }
    #endregion

    #region Guest Login
    public void DoGuest()
    {
        ClearReportText();
        OpenConnectingPanel(true);
        AuthenticationManager.AuthenticateAsGuest(OnGuestComplete, OnGuestFail);
    }

    private void OnGuestComplete()
    {
        OnAuthenticationComplete();
        OpenConnectingPanel(false);
    }

    private void OnGuestFail(string error)
    {
        OpenConnectingPanel(false);
        ReportTextLogin.text = error;
        AuthenticationManager.SetAutoAuth(0);
    }
    #endregion

    #region EmailPassword Login
    public void DoLogin()
    {
        ClearReportText();
        if (!IsInputEmpty(EmailLoginInput, PasswordLoginInput))
        {
            OpenConnectingPanel(true);
            string email = EmailLoginInput.text;
            string password = PasswordLoginInput.text;
            AuthenticationManager.AuthenticateEmailPassword(email, password, OnLoginComplete, OnLoginFail);
        }
        else
        {
            ReportTextLogin.text = LocalizationManager.Instance.GetText("ERROR_EMPTY_INPUTFIELD");
        }
    }

    private void OnLoginComplete()
    {
        CheckDisplayNameValid();
        OpenConnectingPanel(false);
    }

    private void OnLoginFail(string error)
    {
        OpenConnectingPanel(false);
        ReportTextLogin.text = LocalizationManager.Instance.GetText(string.Format("ERROR_PLAYFAB_{0}", error));
        AuthenticationManager.SetAutoAuth(0);
    }
    #endregion

    #region EmailPassword Register
    public void DoRegister()
    {
        ClearReportText();
        if (!IsInputEmpty(EmailRegisterInput, PasswordRegisterInput, ConfirmPasswordRegisterInput))
        {
            if (PasswordRegisterInput.text == ConfirmPasswordRegisterInput.text)
            {
                OpenConnectingPanel(true);
                string email = EmailRegisterInput.text;
                string password = PasswordRegisterInput.text;
                AuthenticationManager.RegisterEmailPassword(email, password, OnRegisterComplete, OnRegisterFail);
            }
            else
            {
                ReportTextRegister.text = LocalizationManager.Instance.GetText("ERROR_PASSWORD_NOT_MATCH"); ;
            }
        }
        else
        {
            ReportTextLogin.text = LocalizationManager.Instance.GetText("ERROR_EMPTY_INPUTFIELD");
        }
    }

    private void OnRegisterComplete()
    {
        DataManager.Instance.UpdateContactEmail(EmailRegisterInput.text
            , delegate ()
            {
                OpenConnectingPanel(false);
                OpenReferralCodePanel(RegisterPanel);
            }
            , OnRegisterFail
        );
    }

    private void OnRegisterFail(string error)
    {
        ReportTextRegister.text = LocalizationManager.Instance.GetText(string.Format("ERROR_PLAYFAB_{0}", error));
        OpenConnectingPanel(false);
    }
    #endregion

    #region Check Player Info
    private void CheckDisplayNameValid()
    {
        if (DataManager.Instance.IsDisplayNameValid())
        {
            OnAuthenticationComplete();
        }
        else
        {
            OpenReferralCodePanel(LoginPanel);
        }
    }
    #endregion

    #region Referral
    public void DoRegisterReferralCode()
    {
        ClearReportText();
        if (!IsInputEmpty(ReferralCodeInput))
        {
            DataManager.Instance.RegisterFriendReferralCode(ReferralCodeInput.text, OnRegisterReferralCodeComplete, OnRegisterReferralCodeFail);
        }
        else
        {
            OpenDisplayNameSetting(ReferralCodePanel);
        }
    }

    public void SkipRegisterReferralCode()
    {
        ClearReportText();
        OpenDisplayNameSetting(ReferralCodePanel);
    }

    private void OnRegisterReferralCodeComplete(List<ItemData> rewardClaimedList)
    {
        if (rewardClaimedList.Count > 0)
        {
            PopupUIManager.Instance.ShowPopup_GotReward(
                                LocalizationManager.Instance.GetText("POPUP_REFERRAL_TITLE_REGISTERED"),
                                LocalizationManager.Instance.GetText("POPUP_REFERRAL_BODY_REGISTERED_WITH_REWARD"),
                                rewardClaimedList);
        }
        else
        {
            PopupUIManager.Instance.ShowPopup_ConfirmOnly(
                                LocalizationManager.Instance.GetText("POPUP_REFERRAL_TITLE_REGISTERED"),
                                LocalizationManager.Instance.GetText("POPUP_REFERRAL_BODY_REGISTERED_NO_REWARD"));
        }

        OpenDisplayNameSetting(ReferralCodePanel);
    }

    private void OnRegisterReferralCodeFail(string error)
    {
        ReportTextReferral.text = LocalizationManager.Instance.GetText(error);

    }
    #endregion

    #region SetDisplayName
    public void DoSetDisplayName()
    {
        ClearReportText();
        if (!IsInputEmpty(DisplayNameInput))
        {
            DataManager.Instance.UpdatePlayerDisplayName(DisplayNameInput.text, OnDisplayNameSettingComplete, OnDisplayNameSettingFail);
        }
        else
        {
            ReportTextDisplayName.text = LocalizationManager.Instance.GetText("ERROR_EMPTY_INPUTFIELD");
        }
    }

    private void OnDisplayNameSettingComplete()
    {
        OnAuthenticationComplete();
        OpenConnectingPanel(false);
    }

    private void OnDisplayNameSettingFail(string error)
    {
        OpenConnectingPanel(false);
        ReportTextDisplayName.text = error;
    }
    #endregion

    #region Facebook Login
    public void DoFacebookLogin()
    {
        ClearReportText();
        OpenConnectingPanel(true);
        AuthenticationManager.AuthenticateFacebook(OnFacebookLoginComplete, OnFacebookLoginFail);
    }

    private void OnFacebookLoginComplete()
    {
        CheckDisplayNameValid();
        OpenConnectingPanel(false);
    }

    private void OnFacebookLoginFail(string error)
    {
        ReportTextLogin.text = LocalizationManager.Instance.GetText(string.Format("ERROR_PLAYFAB_{0}", error));
        OpenConnectingPanel(false);
    }
    #endregion

    #region GooglePlayGames Login
    public void DoGooglePlayGamesLogin()
    {
#if UNITY_ANDROID
        ClearReportText();
        OpenConnectingPanel(true);
        AuthenticationManager.AuthenticateGooglePlayGames(OnGoogleLoginComplete, OnGoogleLoginFail);
#endif
    }

    private void OnGoogleLoginComplete()
    {
        CheckDisplayNameValid();
        OpenConnectingPanel(false);
    }

    private void OnGoogleLoginFail(string error)
    {
        StartCoroutine(IEGoogleSignOut(
                delegate ()
                {
                    ReportTextLogin.text = LocalizationManager.Instance.GetText(string.Format("ERROR_PLAYFAB_{0}", error));
                    OpenConnectingPanel(false);
                }
            )
        );
    }

    private IEnumerator IEGoogleSignOut(UnityAction onComplete)
    {
        #if UNITY_ANDROID
        PlayGamesPlatform.Instance.SignOut();
        while (PlayGamesPlatform.Instance.IsAuthenticated())
        {
            yield return null;
        }
        #endif

        onComplete?.Invoke();

        yield break;
    }
    #endregion

    #region RecoverPassword
    public void DoRecoverPassword()
    {
        ClearReportText();
        if (!IsInputEmpty(EmailRecoverInput))
        {
            OpenConnectingPanel(true);
            AuthenticationManager.SendRecoveryEmail(EmailRecoverInput.text
                , delegate ()
                {
                    OpenConnectingPanel(false);
                    OpenLogin(RecoverAccountPanel);
                }
                , delegate (string error)
                {
                    OpenConnectingPanel(false);
                    ReportTextRecover.text = LocalizationManager.Instance.GetText(string.Format("ERROR_PLAYFAB_{0}", error));
                }
            );
        }
    }
    #endregion

    #region Panel Management
    public void OpenLogin(GameObject originPanel)
    {
        ClearReportText();
        originPanel.SetActive(false);
        LoginPanel.SetActive(true);
    }

    public void OpenRegister(GameObject originPanel)
    {
        ClearReportText();
        originPanel.SetActive(false);
        RegisterPanel.SetActive(true);
    }

    public void OpenRecoverAccount(GameObject originPanel)
    {
        ClearReportText();
        originPanel.SetActive(false);
        EmailRecoverInput.text = string.Empty;
        RecoverAccountPanel.SetActive(true);
    }

    public void OpenReferralCodePanel(GameObject originPanel)
    {
        ClearReportText();
        originPanel.SetActive(false);
        ReferralCodePanel.SetActive(true);
    }

    public void OpenDisplayNameSetting(GameObject originPanel)
    {
        ClearReportText();
        originPanel.SetActive(false);
        DisplayNameSettingPanel.SetActive(true);
    }

    private void OpenConnectingPanel(bool isOn)
    {
       
        if (isOn)
        {
            GameHelper.UITransition_FadeIn(ConnectingPanel.gameObject);
            //ConnectingPanel.SetActive(isOn);
        }
        else
        {
            GameHelper.UITransition_FadeOut(ConnectingPanel.gameObject);
        }

        //Anim_ConnectPanel.SetBool("IsOpen", isOn);
    }

    private void ClearReportText()
    {
        ReportTextLogin.text = "";
        ReportTextRegister.text = "";
        ReportTextDisplayName.text = "";
        ReportTextRecover.text = "";
    }

    private bool IsInputEmpty(params TMP_InputField[] inputs)
    {
        foreach (TMP_InputField item in inputs)
        {
            if (string.IsNullOrEmpty(item.text))
            {
                return true;
            }
        }
        return false;
    }
    #endregion

    #region Scene Management
    private void ShowLoading()
    {
        PopupUIManager.Instance.Show_FullLoad(true);
    }

    private void HideLoading()
    {
        PopupUIManager.Instance.HideAllLoad();
    }

    public void OnAuthenticationComplete()
    {
        //loading alpha
        PopupUIManager.Instance.Show_SoftLoad(true);

        _initTimeStart = Time.time;
        _taskProgess = null;

        DataManager.Instance.LoadValidVersion(delegate ()
        {
            #if !UNITY_EDITOR
            ATNVersion serverSettingVersion = DataManager.Instance.GetServerSetting();
            if (GameHelper.ATNVersion != serverSettingVersion)
            {
                GameHelper.SetATNVersion(serverSettingVersion);
                GameHelper.FirstInit();

                PopupUIManager.Instance.Show_SoftLoad(false);
                DoAutoLogin();

                return;
            }
            #endif

            ClientStatus status = DataManager.Instance.GetVersionStatus();
            if (DataManager.Instance.IsWhiteList())
            {
                Debug.unityLogger.logEnabled = true;

                status = ClientStatus.Open;
            }

            switch (status)
            {
                case ClientStatus.Open:
                    {
                        // valid version.
                        Debug.LogFormat("version <color=green>Valid</color>. [<color=blue>{0}</color>]", GameHelper.GetVersionText());
                        
                        DataManager.Instance.CheckAcceptedCondition(
                            delegate (bool isAccepted)
                            {
                                PopupUIManager.Instance.Show_SoftLoad(false);

                                UnityAction onAcceptedComplete = delegate ()
                                {
                                    ShowLoading();
                                    _taskList = new List<UnityAction<int, UnityAction<int>, UnityAction<int, string>, UnityAction<int, float>>>();
                                    _onUpdateList = new List<UnityAction<int, float>>();

                                    _taskList.Add(LoadAsset);
                                    _onUpdateList.Add(OnLoadAssetUpdate);

                                    _taskList.Add(SyncPlayerData);
                                    _onUpdateList.Add(OnSyncPlayerDataUpdate);

                                    _taskProgess = new float[_taskList.Count];

                                    // Start execute task. (queue)
                                    if (_taskList != null && _taskList.Count > 0)
                                    {
                                        // execute first task.
                                        _taskList[0].Invoke(0, OnTaskComplete, OnTaskFail, _onUpdateList[0]);
                                    }
                                };

                                if (!isAccepted)
                                {
                                    PopupUIManager.Instance.ShowTermService(
                                        delegate ()
                                        {
                                            PopupUIManager.Instance.Show_MidSoftLoad(true);

                                            DataManager.Instance.UpdateAcceptedCondition(
                                                  true
                                                , delegate ()
                                                {
                                                    onAcceptedComplete.Invoke();
                                                    PopupUIManager.Instance.Show_MidSoftLoad(false);
                                                }
                                                , delegate (string errorMsg)
                                                {
                                                    PopupUIManager.Instance.ShowPopup_Error(
                                                          LocalizationManager.Instance.GetText("ERROR_POPUP_HEADER")
                                                        , LocalizationManager.Instance.GetText("ERROR_FAIL_TO_CONNECT_SERVER")
                                                        , delegate ()
                                                        {
                                                            OnTaskFail(-1, LocalizationManager.Instance.GetText("ERROR_FAIL_TO_CONNECT_SERVER"));
                                                        }
                                                    );
                                                    PopupUIManager.Instance.Show_MidSoftLoad(false);
                                                }
                                            );
                                        }
                                    );
                                }
                                else
                                {
                                    onAcceptedComplete.Invoke();
                                }

                            }
                            , delegate (string errorMsg)
                            {
                                PopupUIManager.Instance.Show_SoftLoad(false);
                                PopupUIManager.Instance.ShowPopup_Error(
                                      LocalizationManager.Instance.GetText("ERROR_POPUP_HEADER")
                                    , LocalizationManager.Instance.GetText("ERROR_FAIL_TO_CONNECT_SERVER")
                                    , delegate ()
                                    {
                                        OnTaskFail(-1, LocalizationManager.Instance.GetText("ERROR_FAIL_TO_CONNECT_SERVER"));
                                    }
                                );
                            }
                        );
                    }
                    break;

                case ClientStatus.Update:
                    {
                        PopupUIManager.Instance.Show_SoftLoad(false);
                        //ShowLoading();
                        // out-date version.
                        Debug.LogFormat("version <color=red>Out-Date</color>. [<color=blue>{0}</color>]", GameHelper.GetVersionText());

                        PopupUIManager.Instance.ShowPopup_ConfirmOnly(
                              LocalizationManager.Instance.GetText("LOGIN_PLEASE_UPDATE_VERSION_HEADER")
                            , LocalizationManager.Instance.GetText("LOGIN_PLEASE_UPDATE_VERSION_DESCRIPTION")
                            , delegate ()
                            {
                                ShowLoadAthenion();
                            }
                            , false
                            , true
                        );
                    }
                    break;

                case ClientStatus.Close:
                    {
                        PopupUIManager.Instance.Show_SoftLoad(false);
                        //ShowLoading();
                        // closed version.
                        Debug.LogFormat("version <color=red>Closed</color>. [<color=blue>{0}</color>]", GameHelper.GetVersionText());

                        PopupUIManager.Instance.ShowPopup_TwoChoices_ServerClose(
                              LocalizationManager.Instance.GetText("LOGIN_SERVER_CLOSE_HEADER")
                            , LocalizationManager.Instance.GetText("LOGIN_SERVER_CLOSE_DESCRIPTION")
                            , LocalizationManager.Instance.GetText("BUTTON_FACEBOOK_TH")
                            , LocalizationManager.Instance.GetText("BUTTON_FACEBOOK_GLOBAL")
                            , delegate ()
                            {
                                ShowFacebook_TH();
                                //HideLoading();
                            }
                            , delegate ()
                            {
                                ShowFacebook_Global();
                                //HideLoading();
                            }
                            , false
                            , true
                        );
                    }
                    break;
            }
        }
        , delegate (string error)
        {
            HideLoading();
            Debug.LogError("GoToMenu : " + error);
        });
    }

    public void ShowLoadAthenion()
    {
        Application.OpenURL(GameHelper.GetLoadATNLink());
    }

    public void ShowFacebook_TH()
    {
        Application.OpenURL(GameHelper.ATNFacebookTHLink);
    }

    public void ShowFacebook_Global()
    {
        Application.OpenURL(GameHelper.ATNFacebookGlobalLink);
    }

    private void LoadSceneAsync(string sceneName, LoadSceneMode loadSceneMode, UnityAction onComplete)
    {
        SetLoadingText(LocalizationManager.Instance.GetText("TEXT_LOADING_HOME_SCENE"));
        StartCoroutine(LoadingSceneAsync(sceneName, loadSceneMode, onComplete));
    }

    private IEnumerator LoadingSceneAsync(string sceneName, LoadSceneMode loadSceneMode, UnityAction onComplete)
    {
        //Debug.LogFormat("Loading scene {0}", sceneName);

        //yield return new WaitForSeconds(0.1f);
        //DBManager.LoadDB();
        yield return new WaitForSeconds(0.4f);
        UnityAction<float> onLoading = PopupUIManager.Instance.GetCallbackLoadingProgress();

        GameHelper.LoadSceneAsync(sceneName, loadSceneMode, onComplete, onLoading);
    }

    public void UpdateLoadingProgress(float ratio)
    {
        PopupUIManager.Instance.SetLoadingProgress(ratio);
    }

    public void SetLoadingText(string text)
    {
        PopupUIManager.Instance.SetLoadingText(text);
    }

    public void SetDefaultLoadingText()
    {
        PopupUIManager.Instance.SetLoadingText("");
    }
    #endregion

    #region Task
    private void LoadAsset(int taskIndex, UnityAction<int> onComplete, UnityAction<int, string> onFail, UnityAction<int, float> onUpdate)
    {
        //Caching.ClearCache();

        // Start Loading Asset.
        Debug.Log("Start Load Asset...");
        SetLoadingText(LocalizationManager.Instance.GetText("TEXT_LOADING_ASSET_BUNDLE"));

        AssetBundleManager.StartLoadingAsset(delegate ()
        {
            Debug.Log("<color=green>Completed</color> Load Asset");
            Karamucho.ResourceManager.Clear();
            DBManager.LoadDB();
            if (onComplete != null)
            {
                onComplete.Invoke(taskIndex);
            }
        }
        , delegate (string error)
        {
            Debug.Log("<color=red>Failed</color> Load Asset");
            if (onFail != null)
            {
                onFail(taskIndex, error);
            }
        }
        , delegate (float progress)
        {
            if (onUpdate != null)
            {
                onUpdate(taskIndex, progress);
            }
        }
        );
    }

    private void SyncPlayerData(int taskIndex, UnityAction<int> onComplete, UnityAction<int, string> onFail, UnityAction<int, float> onUpdate)
    {
        SetLoadingText(LocalizationManager.Instance.GetText("TEXT_LOADING_SETUP_ACCOUNT"));

        DataManager.Instance.CheckFirstTimeLogin(
            delegate ()
            {
                SetLoadingText(LocalizationManager.Instance.GetText("TEXT_LOADING_SYNC_PLAYER_DATA"));
                DataManager.Instance.SyncPlayerData(
                    delegate ()
                    {
                        if (onComplete != null)
                        {
                            onComplete.Invoke(taskIndex);
                        }
                    }
                    , delegate (string error)
                    {
                        if (onFail != null)
                        {
                            onFail(taskIndex, error);
                        }
                    }
                    , delegate (float progress)
                    {
                        if (onUpdate != null)
                        {
                            onUpdate(taskIndex, progress);
                        }
                    }
                );
            }
            , delegate (string error)
            {
                if (onFail != null)
                {
                    onFail(taskIndex, error);
                }
            }
        );
    }

    private void OnTaskComplete(int taskIndex)
    {
        if (_taskList != null)
        {
            int nextTaskIndex = taskIndex + 1;
            if (nextTaskIndex < _taskList.Count)
            {
                SetDefaultLoadingText();
                UpdateLoadingProgress(0.0f);

                _taskList[nextTaskIndex].Invoke(nextTaskIndex, OnTaskComplete, OnTaskFail, _onUpdateList[nextTaskIndex]);
            }
            else
            {
                // Complete All.
                Debug.LogFormat("Overall Load Time : {0}", Time.time - _initTimeStart);

                KTPlayManager.Instance.StartLogin(
                    delegate ()
                    {
                        LoadSceneAsync(GameHelper.NavigatorSceneName, LoadSceneMode.Single, null);
                    }
                );
            }
        }
    }

    private void OnTaskFail(int taskIndex, string error)
    {
        HideLoading();
        GameHelper.DestroySingletons();

        Debug.LogErrorFormat("[{0}]OnTaskFail : {1}", taskIndex, error);

        PopupUIManager.Instance.ShowPopup_Error(
              LocalizationManager.Instance.GetText("ERROR_POPUP_HEADER")
            , error
            , delegate ()
            {

            }
        );
    }

    private void OnTaskUpdate(int taskIndex, float ratio)
    {
        if (_taskProgess != null && _taskProgess.Length > 0 && taskIndex < _taskProgess.Length)
        {
            _taskProgess[taskIndex] = ratio;
            float avgRatio = 0.0f;
            foreach (float progress in _taskProgess)
            {
                avgRatio += progress;
            }
            avgRatio = avgRatio / ((float)_taskProgess.Length);

            UpdateLoadingProgress(avgRatio);
        }
        else
        {
            UpdateLoadingProgress(0.0f);
            SetDefaultLoadingText();
        }
    }

    private void OnLoadAssetUpdate(int taskIndex, float ratio)
    {
        UpdateLoadingProgress(ratio);
    }

    private void OnSyncPlayerDataUpdate(int taskIndex, float ratio)
    {
        UpdateLoadingProgress(ratio);
    }
    #endregion
}