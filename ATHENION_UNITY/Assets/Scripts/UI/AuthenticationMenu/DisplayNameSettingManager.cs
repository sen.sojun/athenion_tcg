﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class DisplayNameSettingManager : MonoBehaviour
{
    #region Public Properties
    #endregion

    #region Private Properties
    private string _inputDisplayName = string.Empty;
    private UnityAction _onComplete = null;
    private UnityAction<string> _onFail = null;
    #endregion

    #region Private UI Properties
    [SerializeField] private TMP_InputField _InputDisplayName;
    [SerializeField] private Button BTN_Okay;
    #endregion

    #region Methods

    #region Utilities
    private bool IsInputEmpty(params TMP_InputField[] inputs)
    {
        foreach (TMP_InputField item in inputs)
        {
            if (string.IsNullOrEmpty(item.text))
            {
                return true;
            }
        }
        return false;
    }

    private void ShowLoading()
    {
        PopupUIManager.Instance.Show_MidSoftLoad(true);
    }

    private void HideLoading()
    {
        PopupUIManager.Instance.Show_MidSoftLoad(false);
    }

    public void ShowUI()
    {
        GameHelper.UITransition_FadeIn(this.gameObject);
        Refresh();
        InitBTN();
    }

    public void HideUI()
    {
        GameHelper.UITransition_FadeOut(this.gameObject);
    }

    private void InitBTN()
    {
        BTN_Okay.onClick.RemoveAllListeners();
        BTN_Okay.onClick.AddListener(DoUpdateDisplayName);
    }

    private void Refresh()
    {
        _InputDisplayName.text = "";
    }
    #endregion

    public void SetCallbacks(UnityAction onComplete, UnityAction<string> onFail)
    {
        _onComplete = onComplete;
        _onFail = onFail;
    }

    public void DoUpdateDisplayName()
    {
        ShowLoading();
        if (!IsInputEmpty(_InputDisplayName))
        {
            _inputDisplayName = _InputDisplayName.text;
            DataManager.Instance.UpdatePlayerDisplayName(_inputDisplayName, OnDisplayNameSettingComplete, OnDisplayNameSettingFail);
        }
        else
        {
            HideLoading();
            PopupUIManager.Instance.ShowPopup_Error("ERROR_INPUT_EMPTY", "ERROR_INPUT_EMPTY_DESCRIPTION");
        }
    }

    private void OnDisplayNameSettingComplete()
    {
        HideLoading();
        _onComplete?.Invoke();
        HideUI();
    }

    private void OnDisplayNameSettingFail(string error)
    {
        HideLoading();
        _onFail?.Invoke(error);
    }
    #endregion
}