using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.U2D;
using DG.Tweening;
using TMPro;

namespace Karamucho.UI
{
    public class UIDetailCard : MonoBehaviour
    {
        public enum HighlightList
        {
            None = 0,
            Soul = 1,
            HP = 2,
            ATK = 3,
            Armor = 4,
            Ability = 5,
            Arrow = 6
        }
        #region Public Properties
        public GameObject Group;
        public Camera TargetCamera;

        [Header("Reference")]
        public TextMeshProUGUI CardName;
        public TextMeshProUGUI Detail;
        public TextMeshProUGUI Attack;
        public TextMeshProUGUI Hp;
        public TextMeshProUGUI Spirit;
        public TextMeshProUGUI Armor;
        public Image SeriesIcon;
        public Image AttackImage;
        public Image HpImage;
        public Image ArmorImage;
        public Image Frame;
        public RectTransform Artwork;
        public Image ShadowPlayer;
        public Image ShadowEnemy;
        public GameObject Foil;

        [Header("Tooltip Position")]
        public bool ForceHide = false;
        public UITooltipKeywordGroup TooltipGroup;
        public float XRightOffset;
        public float XLeftOffset;
        public float YTopOffset;
        public float YBotOffset;

        [Header("Tutorial")]
        [SerializeField] private GameObject GROUP_Tutorial;
        [SerializeField] private GameObject GROUP_ArrowHilight;
        [SerializeField] private GameObject Label_Arrow;
        [SerializeField] private GameObject Label_Soul;
        [SerializeField] private GameObject Label_Atk;
        [SerializeField] private GameObject Label_Hp;
        [SerializeField] private GameObject Label_Armor;
        [SerializeField] private GameObject Label_Ability;

        [Header("Arrow")]
        [SerializeField] private List<GameObject> ArrowDir = new List<GameObject>();
        [SerializeField] private List<GameObject> ArrowHilight = new List<GameObject>();
        #endregion

        #region Private Properties
        private Vector3 _defaultScale;
        private Vector3 _showOffset = new Vector3(0, 0, 0);
        private CardUIData _data;
        private Sequence _sq;
        private float _delayTime = 0.1f;
        private float _showTimer;
        private bool _tooltipIsShow = false;

        private const float _leftOffset = 0.28f;
        private const float _rightOffset = 0.72f;
        private const float _normalOffset = 0.7f;
        private const float _handOffset = 0.35f;

        #endregion

        #region Methods
        private void Update()
        {
            if (gameObject.activeSelf)
            {
                if (_showTimer < _delayTime)
                {
                    _showTimer += Time.deltaTime;
                    //Debug.Log(_showTimer.ToString("F2"));
                }
                else
                {
                    if (!ForceHide)
                    {
                        SetTooltip(_data);
                    }

                }
            }

        }

        public void SetData(CardUIData data, bool isInBattle = true)
        {
            _defaultScale = Group.transform.localScale;
            if (_data != null)
            {
                //if (_data.UniqueID != data.UniqueID)
                {
                    _data = data;
                }
            }
            else
            {
                _data = data;
            }

            if (isInBattle && TargetCamera != null)
            {
                if (_data.CurrentZone == CardZone.Battlefield || _data.CurrentZone == CardZone.Hand)
                {
                    UpdatePosition(_data.UniqueID);
                }
            }

            AnimateShow();

            SetName(_data.Name);
            SetAtk(_data.ATKCurrent, _data);
            SetHp(_data.HPCurrent, _data);
            SetSpirit(_data.SpiritCurrent, _data);
            SetArmor(_data.ArmorCurrent, _data);
            ShowDirection(_data.CardDir);
            SetDescription(_data.Description);
            SetFrame(_data);
            SetImage(_data);
            SetFoil(_data);
            SetPackIcon(_data);


            if (isInBattle)
            {
                ShowTutorial();
                SetColorPlayer();
            }
            else
            {
                GROUP_Tutorial.SetActive(false);
            }
        }

        private void SetTooltip(CardUIData data)
        {
            if (data != null)
            {
                TooltipGroup.SetData(data);
                if (!_tooltipIsShow)
                {
                    TooltipGroup.ShowTooltip();
                    _tooltipIsShow = true;
                }
            }
        }

        private void SetColorPlayer()
        {
            ShadowPlayer.gameObject.SetActive(GameManager.Instance.IsLocalPlayer(_data.Owner));
            ShadowEnemy.gameObject.SetActive(!GameManager.Instance.IsLocalPlayer(_data.Owner));
        }

        public void SetName(string message)
        {
            CardName.text = message;
        }

        public void SetAtk(int value, CardUIData data)
        {

            Color color = new Color();
            if (value < data.ATKBase)
            {
                color = GameHelper.GetColor_Number_Decrease();
            }
            else
            {
                if (data.ATKBase > data.ATKDefault)
                {
                    color = GameHelper.GetColor_Number_Increase();
                }
                else
                {
                    color = GameHelper.GetColor_Number_Normal();
                }
            }

            Attack.color = color;
            string valueText = "";
            valueText = value.ToString();
            if (value != data.ATKBase) valueText += GetDefaultValue(data.ATKBase);

            Attack.text = valueText;

        }

        public void SetHp(int value, CardUIData data)
        {
            Color color = new Color();
            if (value < data.HPBase)
            {
                color = GameHelper.GetColor_Number_Decrease();
            }
            else
            {
                if (data.HPBase > data.HPDefault)
                {
                    color = GameHelper.GetColor_Number_Increase();
                }
                else
                {
                    color = GameHelper.GetColor_Number_Normal();
                }
            }
            Hp.color = color;
            string valueText = "";
            valueText = value.ToString();
            if (value != data.HPBase) valueText += GetDefaultValue(data.HPBase);

            Hp.text = valueText;

        }

        public void SetArmor(int value, CardUIData data)
        {
            if (data.ArmorBase <= 0)
            {
                Armor.gameObject.SetActive(false);
                ArmorImage.gameObject.SetActive(false);
            }
            else
            {
                Armor.gameObject.SetActive(true);
                ArmorImage.gameObject.SetActive(true);
            }

            Color color = new Color();
            if (value < data.ArmorBase)
            {
                color = GameHelper.GetColor_Number_Decrease();
            }
            else
            {
                if (data.ArmorBase > data.ArmorDefault)
                {
                    color = GameHelper.GetColor_Number_Increase();
                }
                else
                {
                    color = GameHelper.GetColor_Number_Normal();
                }
            }

            Armor.text = value.ToString();
            Armor.color = color;
        }

        private string GetDefaultValue(int value)
        {
            return string.Format("<color=#FFFFFF><size=60>/{0}</size></color>", value);
        }

        public void SetSpirit(int value, CardUIData data)
        {
            Color color = new Color();
            if (value < data.SpiritBase)
            {
                color = GameHelper.GetColor_Number_Decrease();
            }
            else
            {
                if (data.SpiritBase > data.SpiritDefault)
                {
                    color = GameHelper.GetColor_Number_Increase();
                }
                else
                {
                    color = Color.white;
                }
            }

            Spirit.text = value.ToString();
            Spirit.color = color;
            //Spirit.text = GameHelper.GetIntToRomanNumber(value).ToString();
        }

        public void SetDescription(string message)
        {
            Detail.text = message;
        }

        private void SetFrame(CardUIData data)
        {
            if (data.Type == CardType.Minion || data.Type == CardType.Minion_NotInDeck || data.Type == CardType.Spell)
            {
                Sprite tempSprite;
                CardResourceManager.LoadCardFrame(data.Rarity, data.Element.ToCardElementType(), out tempSprite);
                Frame.sprite = tempSprite;
            }
        }

        private void SetFoil(CardUIData data)
        {
            Foil.SetActive(data.IsFoil);
        }

        public void ShowDirection(CardDirection dirValue)
        {
            foreach (CardDirectionType item in System.Enum.GetValues(typeof(CardDirectionType)))
            {
                bool isShow = dirValue.IsDirection(item);
                ArrowDir[(int)item].SetActive(isShow);
                ArrowHilight[(int)item].SetActive(isShow);
            }
        }

        private void SetImage(CardUIData data)
        {
            // Clear previous image
            {
                foreach (Transform child in Artwork.transform)
                {
                    Destroy(child.gameObject);
                }
            }

            GameObject obj = null;
            if (CardResourceManager.Create(CardResourceManager.ImageType.InfoCard, data.ImageKey, Artwork.transform, false, out obj))
            {
                obj.transform.localPosition = Vector3.zero;
                obj.transform.localRotation = Quaternion.identity;
                SetMask(data);
            }
        }

        private void SetMask(CardUIData data)
        {
            foreach (Transform child in Artwork.transform)
            {
                if (data.Type == CardType.Minion || data.Type == CardType.Minion_NotInDeck || data.Type == CardType.Spell)
                {
                    Sprite tempSprite;
                    CardResourceManager.LoadCardMaskFrame(data.Rarity, data.Element.ToCardElementType(), out tempSprite);
                    child.GetComponent<Image>().sprite = tempSprite;
                }
            }
        }

        private void SetPackIcon(CardUIData data)
        {
            Sprite sprite;
            CardResourceManager.LoadSeriesIcon(data.SeriesKey, out sprite);

            SeriesIcon.sprite = sprite;
        }

        #endregion

        #region UI
        public void UpdatePosition(int cardUniqueID)
        {
            CardUI cardUI;
            BattleCardData battleCardData;
            UIManager.Instance.FindCardByUniqueID(cardUniqueID, out cardUI);
            GameManager.Instance.FindBattleCard(cardUniqueID, out battleCardData);

            if (cardUI != null && battleCardData != null)
            {
                // Reset
                _showOffset = Vector3.zero;

                // Convert to viewport to find percentage of view
                Vector3 location = cardUI.transform.position;
                Vector3 targetLocation = TargetCamera.WorldToViewportPoint(location);

                // Get value from parent canvas
                float width = transform.parent.GetComponent<RectTransform>().sizeDelta.x;
                float height = transform.parent.GetComponent<RectTransform>().sizeDelta.y;
                Vector3 leftOffset = new Vector3(width * _leftOffset, height * _normalOffset, 0.0f);
                Vector3 rightOffset = new Vector3(width * _rightOffset, height * _normalOffset, 0.0f);

                bool isRight = false;
                bool onHand = false;

                // Move to target location
                //Vector3 targetLocation = Camera.main.WorldToScreenPoint(cardUI.transform.position);

                if (battleCardData.CurrentZone == CardZone.Battlefield)
                {
                    onHand = false;
                    // In battlefield
                    switch (cardUI.GetSlotID() % GameManager.Instance.BoardWidth)
                    {
                        case 0: // 1st Column
                        case 1: // 2nd Column
                        default:
                            {
                                // Show detail at right side of screen.

                                //_showOffset = new Vector3(Screen.width * 0.7f, targetLocation.y, 0.0f);
                                _showOffset = rightOffset;
                                isRight = true;
                            }
                            break;

                        case 2: // 3rd Column
                        case 3: // 4th Column
                            {
                                // Show detail at left side of screen.

                                //_showOffset = new Vector3(Screen.width * 0.3f, targetLocation.y, 0.0f);
                                _showOffset = leftOffset;
                                isRight = false;
                            }
                            break;
                    }
                }
                else // On Hand
                {
                    onHand = true;
                    if (targetLocation.x < 0.5f)
                    {
                        // Show detail at left side of screen.
                        _showOffset += leftOffset;
                        isRight = false;
                    }
                    else //if (targetLocation.x >= Screen.width * 0.5f)
                    {
                        // Show detail at right side of screen.
                        _showOffset += rightOffset;
                        isRight = true;
                    }
                    _showOffset = new Vector3(_showOffset.x, height * _handOffset, 0.0f);
                }

                GetComponent<RectTransform>().anchoredPosition = _showOffset;
                SetTooltipOffset(onHand, isRight);

            }
        }

        /// <summary>
        /// Use this when update position in card collection.
        /// </summary>
        /// <param name="location"></param>
        public void UpdatePosition(Vector3 location)
        {
            // Reset
            _showOffset = Vector3.zero;

            // Convert to viewport to find percentage of view
            Vector3 targetLocation = TargetCamera.WorldToViewportPoint(location);

            // Get value from parent canvas
            float width = transform.parent.GetComponent<RectTransform>().sizeDelta.x;
            float height = transform.parent.GetComponent<RectTransform>().sizeDelta.y;
            Vector3 leftOffset = new Vector3(width * _leftOffset, height * _normalOffset, 0.0f);
            Vector3 rightOffset = new Vector3(width * _rightOffset, height * _normalOffset, 0.0f);

            bool isRight = false;
            bool onHand = false;

            if (targetLocation.x < 0.5f)
            {
                //Debug.Log("Left Side");
                _showOffset += rightOffset;
                isRight = false;
            }
            else //if (targetLocation.x >= Screen.width * 0.5f)
            {
                //Debug.Log("Right Side");
                _showOffset += leftOffset;
                isRight = true;
            }

            GetComponent<RectTransform>().anchoredPosition = _showOffset;
            SetTooltipOffset(onHand, isRight);

        }

        public void UpdateGravePosition(Vector3 location)
        {
            // Reset
            _showOffset = Vector3.zero;

            // Get value from parent canvas
            float width = transform.parent.GetComponent<RectTransform>().sizeDelta.x;
            float height = transform.parent.GetComponent<RectTransform>().sizeDelta.y;
            Vector3 leftOffset = new Vector3(width * _leftOffset, height * _normalOffset, 0.0f);
            Vector3 rightOffset = new Vector3(width * _rightOffset, height * _normalOffset, 0.0f);

            // Find object in percentage of view
            Vector3 targetLocation = location;

            bool onHand = false;
            bool isRight = false;

            if (targetLocation.x < width * 0.5f)
            {
                Debug.Log("Left Side");
                _showOffset += rightOffset;
                isRight = false;
            }
            else //if (targetLocation.x >= Screen.width * 0.5f)
            {
                Debug.Log("Right Side");
                _showOffset += leftOffset;
                isRight = true;
            }

            GetComponent<RectTransform>().anchoredPosition = _showOffset;
            SetTooltipOffset(onHand, isRight);
        }

        private void SetTooltipOffset(bool onHand, bool isRight)
        {
            if (onHand)
            {
                if (isRight)
                {
                    // Show tooltip on left side of detail UI.
                    TooltipGroup.GetComponent<VerticalLayoutGroup>().childAlignment = TextAnchor.LowerRight;
                    TooltipGroup.transform.localPosition = new Vector3(XLeftOffset, TooltipGroup.transform.localPosition.y);
                    TooltipGroup.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.0f);
                    TooltipGroup.GetComponent<RectTransform>().DOPivotX(1.0f, 0.3f);
                }
                else
                {
                    // Show tooltip on right side of detail UI.
                    TooltipGroup.GetComponent<VerticalLayoutGroup>().childAlignment = TextAnchor.LowerLeft;
                    TooltipGroup.transform.localPosition = new Vector3(XRightOffset, TooltipGroup.transform.localPosition.y);
                    TooltipGroup.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.0f);
                    TooltipGroup.GetComponent<RectTransform>().DOPivotX(0.0f, 0.3f);
                }
            }
            else
            {
                // Show tooltip on bot side of detail UI.
                TooltipGroup.GetComponent<VerticalLayoutGroup>().childAlignment = TextAnchor.MiddleCenter;
                TooltipGroup.transform.localPosition = new Vector3(0, YBotOffset);
                TooltipGroup.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
                TooltipGroup.GetComponent<RectTransform>().DOPivotY(1.0f, 0.3f);
            }
        }

        public void ShowCard(bool isShow)
        {
            if (isShow)
            {
                gameObject.SetActive(true);

            }
            else
            {
                gameObject.SetActive(false);
            }
        }

        private void AnimateShow()
        {
            _sq.Kill();
            Group.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);

            _sq = DOTween.Sequence();
            _sq.Append(Group.transform.DOScale(_defaultScale, 0.25f).SetEase(Ease.OutExpo));

            TooltipGroup.HideAllKeywordIcon();
            _showTimer = 0.0f;
            _tooltipIsShow = false;
        }
        #endregion

        #region Tutorial


        public void ShowHighlightLabel(List<HighlightList> highlightList)
        {
            if (highlightList != null)
            {
                foreach (HighlightList highlight in highlightList)
                {
                    switch (highlight)
                    {
                        default:
                        case HighlightList.None:
                            HideAllTutorial();
                            break;

                        case HighlightList.Soul:
                            Label_Soul.SetActive(true);
                            break;

                        case HighlightList.HP:
                            Label_Hp.SetActive(true);
                            break;

                        case HighlightList.ATK:
                            Label_Atk.SetActive(true);
                            break;

                        case HighlightList.Armor:
                            Label_Armor.SetActive(true);
                            break;

                        case HighlightList.Ability:
                            Label_Ability.SetActive(true);
                            break;

                        case HighlightList.Arrow:
                            GROUP_ArrowHilight.SetActive(true);
                            Label_Arrow.SetActive(true);
                            break;

                    }
                }
            }
        }
        
        private void ShowTutorial()
        {
            HideAllTutorial();

            if (GameManager.Instance.IsStoryMode())
            {
                GameHelper.UITransition_FadeIn(GROUP_Tutorial);
                StoryGameController tutorialController = GameManager.Instance.Controller as StoryGameController;
                //List<HighlightList> highlights = tutorialController.OnTouchUIDetailCard();

                //ShowHighlightLabel(highlights);

                //if (GameManager.Instance.Mode == GameMode.Tutorial_1)
                //{
                //    // Dir / Atk / Hp
                //    GROUP_ArrowHilight.SetActive(true);
                //    Label_Atk.SetActive(true);
                //    Label_Arrow.SetActive(true);
                //    Label_Hp.SetActive(true);
                //    if (GameManager.Instance.SubTurn >= 12)
                //    {
                //        Label_Armor.SetActive(true);
                //    }

                //}
                //else if (GameManager.Instance.Mode == GameMode.Tutorial_2)
                //{

                //    if (GameManager.Instance.SubTurn >= 10)
                //    {
                //        // Soul / Ability
                //        Label_Soul.SetActive(true);
                //        Label_Ability.SetActive(true);
                //    }
                //    else if (GameManager.Instance.SubTurn >= 6)
                //    {
                //        // Soul 
                //        Label_Soul.SetActive(true);
                //    }
                //}
                //else if (GameManager.Instance.Mode == GameMode.Tutorial_3)
                //{
                //    // Ability
                //    Label_Ability.SetActive(true);
                //}

            }
        }

        private void HideAllTutorial()
        {
            GROUP_ArrowHilight.SetActive(false);
            Label_Ability.SetActive(false);
            Label_Armor.SetActive(false);
            Label_Arrow.SetActive(false);
            Label_Atk.SetActive(false);
            Label_Hp.SetActive(false);
            Label_Soul.SetActive(false);
        }
        #endregion

    }
}

