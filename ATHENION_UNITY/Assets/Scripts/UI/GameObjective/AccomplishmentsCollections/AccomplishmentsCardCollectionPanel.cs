﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using TMPro;
using Karamucho.UI;

public class AccomplishmentsCardCollectionPanel : MonoBehaviour
{
    #region Public Properties
    [Header("Prefabs")]
    public GameObject Prefab_AccomCardCollectionSet;

    [Header("Ref")]
    public ScrollRect ScrollRect;
    public RectTransform Rect_ViewPort;
    public Transform Container;
    public BaseCardUI Popup_CardDetail;

    [Header("Text")]
    public TextMeshProUGUI TXT_CollectionName;

    [Header("Botton")]
    public Button BTN_ClaimReward;
    #endregion

    #region Private Properties
    private bool _isInitialized = false;
    private bool _isPlayVoice = false;
    private List<Cell_AccomCardCollection> _cardCellList;
    #endregion

    #region Static Event Properties
    public static event Action OnRefreshCardSet;
    #endregion

    private void OnEnable()
    {
        InitData();
    }
    
    public void InitData()
    {
        if (DataManager.Instance.IsFeatureEnable(FeatureUnlock.CardSet))
        {
            if (!_isInitialized)
            {
                InitializePanel();
                _isInitialized = true;
            }
            else
            {
                UpdatePanel();
            }

            TriggerOnRefreshCardSet();
        }
    }

    private void InitializePanel()
    {
        _cardCellList = new List<Cell_AccomCardCollection>();

        List<AchievementCardSetData> cardSetList = DataManager.Instance.PlayerAchievementCardSet.AchievementList; 

        foreach (AchievementCardSetData data in cardSetList)
        {
            CardSetUIData uiData = new CardSetUIData(data);

            GameObject cardCell = Instantiate(Prefab_AccomCardCollectionSet, Container);
            Cell_AccomCardCollection cellComp = cardCell.GetComponent<Cell_AccomCardCollection>();
            if (cellComp != null)
            {
                cardCell.SetActive(true);
                cellComp.InitData(uiData, data.IsCleared);
                cellComp.SetPanelReference(this);

                _cardCellList.Add(cellComp);
            }
        }

        ScrollRect.onValueChanged.AddListener(OnScrolling);
    }

    private void UpdatePanel()
    {
        foreach (Cell_AccomCardCollection cell in _cardCellList)
        {
            cell.UpdateData();
        }

    }

    public void ShowCardDetailPopup(CardUIData data)
    {
        Popup_CardDetail.SetData(data);
        GameHelper.UITransition_FadeIn(Popup_CardDetail.transform.parent.gameObject);

        if (!_isPlayVoice)
        {
            _isPlayVoice = true;
            SoundManager.PlayMinionSummonVoice(data.BaseID, () => _isPlayVoice = false);
        }
    }

    private void OnScrolling(Vector2 value)
    {
        Container.GetComponent<ContentSizeFitter>().enabled = false;
        Container.GetComponent<VerticalLayoutGroup>().enabled = false;

        RectTransform rect_cell = null;

        foreach (Cell_AccomCardCollection cell in _cardCellList)
        {
            rect_cell = cell.GetComponent<RectTransform>();
            if (GameHelper.IsRectTouchRect(rect_cell, Rect_ViewPort))
            {
                cell.gameObject.SetActive(true);
            }
            else
            {
                cell.gameObject.SetActive(false);
            }
        }
    }

    #region Event
    public static void TriggerOnRefreshCardSet()
    {
        OnRefreshCardSet?.Invoke();
    }
    #endregion
}
