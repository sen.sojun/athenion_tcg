﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class CardSetUIData : BaseGameObjectiveUIData
{
    #region Private Properties
    private AchievementCardSetData _localData;
    #endregion

    #region Contructors
    public CardSetUIData()
    {
    }

    public CardSetUIData(AchievementCardSetData data)
    {
        _localData = data as AchievementCardSetData;
        SetupData(data);
    }

    #endregion

    #region Methods
    public List<string> GetCardSetIDList()
    {
        return _localData.CardSetIDList;
    }

    public List<string> GetCompletedCardSetIDList()
    {
        return _localData.GetCurrentProgressCardIDList();
    }

    protected override string GetTitle()
    {
        return LocalizationManager.Instance.GetAchievementTitle(ID);
    }
    protected override string GetDescription()
    {
        return "";
    }
    #endregion
}

public class Cell_AccomCardCollection : MonoBehaviour
{
    #region Public Properties
    [Header("Prefabs")]
    public GameObject Prefab_CardCell;
    public GameObject Prefab_RewardCell;

    [Header("Ref")]
    public Transform Container;
    public GameObject Header;
    public GameObject Body;
    [SerializeField] private ScrollRect _ViewScrollRect;
    [SerializeField] private ScrollRect _BodyScrollRect;

    [Header("Text")]
    public TextMeshProUGUI TXT_CollectionName;

    [Header("Botton")]
    public Button BTN_ClaimReward;
    public Button BTN_RewardClaimed;
    public Button BTN_GetCard;
    #endregion

    #region Private Properties
    private CardSetUIData _data;
    private Dictionary<string, CardUI_SetCollection> _cardCellDict = new Dictionary<string, CardUI_SetCollection>();
    private AccomplishmentsCardCollectionPanel _panel;
    private RewardCell_SeasonPass _rewardCell;
    #endregion

    #region Methods
    public void InitData(CardSetUIData data, bool isCleared, UnityAction showDetailAction = null)
    {
        _data = data;

        if (_cardCellDict != null && _cardCellDict.Count > 0)
        {
            UpdateData();
        }
        else
        {
            SetupData(data);
            UpdateData();

            BTN_ClaimReward.onClick.RemoveAllListeners();
            BTN_ClaimReward.onClick.AddListener(ClaimReward);
        }

        UIInputEvent bodyEvent = _BodyScrollRect.GetComponent<UIInputEvent>();
        if (bodyEvent != null)
        {
            bodyEvent.OnBeginDragEvent += OnCardCell_BeginDrag;
            bodyEvent.OnDragEvent += OnCardCell_Drag;
            bodyEvent.OnEndDragEvent += OnCardCell_EndDrag;
        }

    }

    public void UpdateData()
    {
        foreach (KeyValuePair<string, CardUI_SetCollection> data in _cardCellDict)
        {
            bool unlocked = _data.GetCompletedCardSetIDList().Contains(data.Key);

            data.Value.SetOwned(unlocked);
        }

        BTN_RewardClaimed.gameObject.SetActive(_data.IsCleared);
        BTN_GetCard.gameObject.SetActive(!_data.IsCleared && !_data.IsCompleted);
        BTN_ClaimReward.gameObject.SetActive(_data.IsCompleted && !_data.IsCleared);

        if (BTN_RewardClaimed.gameObject.activeSelf)
        {
            _rewardCell.gameObject.transform.SetParent(BTN_RewardClaimed.transform);
            _rewardCell.ShowBigCheckIcon(true);
        }
        else if (BTN_GetCard.gameObject.activeSelf)
        {
            _rewardCell.gameObject.transform.SetParent(BTN_GetCard.transform);
            _rewardCell.ShowBigCheckIcon(false);
        }
        else if (BTN_ClaimReward.gameObject.activeSelf)
        {
            _rewardCell.gameObject.transform.SetParent(BTN_ClaimReward.transform);
            _rewardCell.ShowBigCheckIcon(false);
        }
        _rewardCell.transform.SetAsLastSibling();
        //_rewardCell.gameObject.transform.SetAsFirstSibling();
    }

    public void SetPanelReference(AccomplishmentsCardCollectionPanel panel)
    {
        _panel = panel;
    }

    private void SetupData(CardSetUIData data)
    {
        //TXT_CollectionName.text = data.Title; //Old School
        TXT_CollectionName.text = LocalizationManager.Instance.GetText(data.Title);

        foreach (string id in data.GetCardSetIDList())
        {
            CardUI_SetCollection card = GenCardCell(id);
            if (card != null) _cardCellDict.Add(id, card);
        }

        _rewardCell = GenRewardCell(new ItemData(data.ImageKey, data.RewardCount));
        _rewardCell.gameObject.transform.SetAsFirstSibling();
        _rewardCell.transform.localScale = new Vector3(0.65f, 0.65f, _rewardCell.transform.localScale.z);
    }

    private void ClaimReward()
    {
        PopupUIManager.Instance.Show_MidSoftLoad(true);
        DataManager.Instance.CheckServerAchievementCardSet(
            _data.ID,
            delegate
            {
                PopupUIManager.Instance.Show_MidSoftLoad(false);
                List<ItemData> showReward = new List<ItemData>();
                showReward.Add(new ItemData(_data.ImageKey, _data.RewardCount));
                PopupUIManager.Instance.ShowPopup_GotReward(LocalizationManager.Instance.GetText("TEXT_GOTREWARDTITLE"), LocalizationManager.Instance.GetText("TEXT_YOUVEGOTREWARD"), showReward);

                BTN_ClaimReward.onClick.RemoveAllListeners();
                BTN_ClaimReward.interactable = false;
                UpdateData();

                AccomplishmentsCardCollectionPanel.TriggerOnRefreshCardSet();
            },
            delegate (string failData)
            {
                string errorText = failData;

                PopupUIManager.Instance.Show_MidSoftLoad(false);
                PopupUIManager.Instance.ShowPopup_Error(LocalizationManager.Instance.GetText("ERROR_POPUP_HEADER"), errorText);
            }
            );
    }
    #endregion

    #region Event
    private void OnEnable()
    {
        _BodyScrollRect.horizontalNormalizedPosition = 1f;
    }

    private void OnDisable()
    {
        //_BodyScrollRect.horizontalNormalizedPosition = 1f;
    }

    private void OnCardCell_BeginDrag(PointerEventData eventData)
    {
        _ViewScrollRect.OnBeginDrag(eventData);
        _BodyScrollRect.OnBeginDrag(eventData);
    }

    private void OnCardCell_Drag(PointerEventData eventData)
    {
        _ViewScrollRect.OnDrag(eventData);
        _BodyScrollRect.OnDrag(eventData);
    }

    private void OnCardCell_EndDrag(PointerEventData eventData)
    {
        _ViewScrollRect.OnEndDrag(eventData);
        _BodyScrollRect.OnEndDrag(eventData);
    }

    private void OnShowCardUI(string cardID)
    {
        if (CardData.GetCardData(cardID, out CardData cardData))
        {
            string cardbackID = DataManager.Instance.DefaultCardBackIDList[0];
            CardUIData data = new CardUIData(0, cardData, cardbackID);
            _panel.ShowCardDetailPopup(data);
        }
    }
    #endregion

    #region Cell Gen
    private CardUI_SetCollection GenCardCell(string cardID)
    {
        GameObject cardCell = Instantiate(Prefab_CardCell, Container);
        CardUI_SetCollection cellComp = cardCell.GetComponent<CardUI_SetCollection>();

        if (CardData.GetCardData(cardID, out CardData cardData))
        {
            cardCell.SetActive(true);

            string cardbackID = DataManager.Instance.DefaultCardBackIDList[0];
            CardUIData data = new CardUIData(0, cardData, cardbackID);
            cellComp.SetData(data);

            //Set on click
            //cellComp.OnCardClick += delegate { _panel.ShowCardDetailPopup(data); };
            cellComp.OnCardClick += delegate { OnShowCardUI(cardID); };

            //Subscribe on drag
            cellComp.OnBeginDragEvent += OnCardCell_BeginDrag;
            cellComp.OnDragEvent += OnCardCell_Drag;
            cellComp.OnEndDragEvent += OnCardCell_EndDrag;
        }

        return cellComp;
    }

    private RewardCell_SeasonPass GenRewardCell(ItemData itemData)
    {
        GameObject rewCell = Instantiate(Prefab_RewardCell, BTN_ClaimReward.transform);
        rewCell.SetActive(true);
        RewardCell_SeasonPass cellComp = rewCell.GetComponent<RewardCell_SeasonPass>();
        cellComp.InitData(itemData.ItemID, itemData.Amount);

        return cellComp;
    }
    #endregion
}
