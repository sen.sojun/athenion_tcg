﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class AccomplishmentPanelManager : MonoBehaviour
{
    #region Enums
    public enum TabType
    {
          Achievement
        , Collection
    }
    #endregion

    #region Event

    public static event Action OnCardCollectionActive;

    #endregion

    #region Public Properties

    [Header("Ref")]
    public AccomplishmentsCardCollectionPanel CardCollectionPanel;
    public AchievementPanelUI ArchievementPanel;
    public Image IMG_ImageSlider;
    public Button BTN_TapToClose;
    public Button BTN_TabAchievement;
    public Button BTN_TabCollection;

    public bool UI_IsShowCollection { get; private set; }
    public bool UI_IsShowAchievement { get; private set; }
    
    #endregion

    #region Private Properties
    private bool _initialized = false;
    private TabType _currentTab = TabType.Achievement;
    private Tweener _sliderTweener;
    #endregion

    #region Method
    private void OnEnable()
    {
        CardCollectionPanel?.InitData();
        ArchievementPanel?.InitData();

        InitializeData();
        UpdateLockIcon();
    }

    private void InitializeData()
    {
        if (DataManager.Instance.IsFeatureEnable(FeatureUnlock.Accomplishment))
        {
            TopToolbar.AddSetting(true, true, HideUI);

            if (!_initialized)
            {
                _initialized = true;

                InitButton();

                UI_IsShowAchievement = true;
                UI_IsShowCollection = false;

                ChangePage(_currentTab);
            }
        }
    }

    private void UpdateLockIcon()
    {
        BTN_TabAchievement.interactable = DataManager.Instance.IsFeatureEnable(FeatureUnlock.Achievement);
        BTN_TabCollection.interactable = DataManager.Instance.IsFeatureEnable(FeatureUnlock.CardSet);
    }

    private void HideUI()
    {
        TopToolbar.RemoveLatestSetting();
        GameHelper.UITransition_FadeOut(gameObject);
        HomeManager.Instance.ShowMainCanvas();
    }

    private void InitButton()
    {
        BTN_TapToClose.onClick.RemoveAllListeners();
        BTN_TapToClose.onClick.AddListener(HideUI);

        BTN_TabAchievement.onClick.RemoveAllListeners();
        BTN_TabAchievement.onClick.AddListener
        (
            delegate
            {
                ChangePage(TabType.Achievement);
            }
        );
        BTN_TabAchievement.interactable = DataManager.Instance.IsFeatureEnable(FeatureUnlock.Achievement);

        BTN_TabCollection.onClick.RemoveAllListeners();
        BTN_TabCollection.onClick.AddListener
        (
            delegate
            {
                ChangePage(TabType.Collection);
            }
        );
        BTN_TabCollection.interactable = DataManager.Instance.IsFeatureEnable(FeatureUnlock.CardSet);
    }

    public void SetStarterTab(TabType tab)
    {
        _currentTab = tab;
    }

    private void ChangePage(TabType tab)
    {
        Transform parent = null;

        switch (tab)
        {
            case TabType.Achievement:
            {
                UI_IsShowAchievement = true;
                UI_IsShowCollection = false;

                GameHelper.UITransition_FadeIn(ArchievementPanel.gameObject);
                ArchievementPanel.InitData();
                CardCollectionPanel.gameObject.SetActive(false);

                parent = BTN_TabAchievement.transform;
            }
            break;

            case TabType.Collection:
            {
                UI_IsShowAchievement = false;
                UI_IsShowCollection = true;

                GameHelper.UITransition_FadeIn(CardCollectionPanel.gameObject);
                OnCardCollectionActive?.Invoke();
                ArchievementPanel.gameObject.SetActive(false);

                parent = BTN_TabCollection.transform;
            }
            break;
        }

        if (_sliderTweener != null)
        {
            _sliderTweener.Kill();
        }
        IMG_ImageSlider.transform.SetParent(parent);
        IMG_ImageSlider.transform.SetAsFirstSibling();
        _sliderTweener = IMG_ImageSlider.transform.DOLocalMove(new Vector2(0f, 0f), 0.5f).SetEase(Ease.OutExpo);

        LayoutRebuilder.ForceRebuildLayoutImmediate(gameObject.transform.GetComponent<RectTransform>());
    }
    #endregion
}
