﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;

public class EventQuestUIData : BaseGameObjectiveUIData
{
    #region Constructor
    public EventQuestUIData()
    {

    }

    public EventQuestUIData(QuestData data)
    {
        SetupData(data);
    }
    #endregion

    #region Methods
    protected override string GetTitle()
    {
        return LocalizationManager.Instance.GetQuestTitle(ID);
    }

    protected override string GetDescription()
    {
        return LocalizationManager.Instance.GetQuestDescription(ID);
    }
    #endregion
}

public class EventQuestCell : BaseGameObjectiveUICell
{
    #region Methods
    public void InitData(EventQuestUIData data, UnityAction onClaimReward)
    {
        base.InitData(data, onClaimReward);
        SetQuestIsClear(_data.IsCleared);
    }

    public void SetQuestIsClear(bool isComplete)
    {
        if (isComplete)
        {
            BLOCK_BlackLayer.SetActive(true);
            //gameObject.SetActive(false);
        }
    }
    #endregion
}
