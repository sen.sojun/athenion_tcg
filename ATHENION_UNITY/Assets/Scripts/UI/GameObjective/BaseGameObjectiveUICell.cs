﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;

public abstract class BaseGameObjectiveUIData
{
    #region Public Properties
    public string ID { get; protected set; }
    public string Title { get { return GetTitle(); } }
    public string Description { get { return GetDescription(); } }
    public string ImageKey { get; protected set; }
    public int RewardCount { get; protected set; }
    public int ProgressCurrent { get; protected set; }
    public int ProgressMax { get; protected set; }
    public List<ItemData> ItemDataList { get { return _data.ItemDataList; } }

    // Logic Flag
    public bool IsCompleted { get { return GetIsCompleted(); } }
    public bool IsClaimable { get { return GetIsClaimable(); } }
    public bool IsCleared { get { return GetIsCleared(); } }
    #endregion

    #region Protected Properties
    protected GameObjectiveData _data;
    #endregion

    #region Methods
    public void SetupData(GameObjectiveData data)
    {
        _data = data;
        ID = _data.ID;
        foreach (ItemData item in _data.ItemDataList)
        {
            ImageKey = item.ItemID;
            RewardCount = item.Amount;
            break;
        }
        ProgressCurrent = _data.CurrentProgress;
        ProgressMax = _data.MaxProgress;
    }

    protected bool GetIsClaimable()
    {
        if (!IsCleared)
        {
            return _data.IsCompleted;
        }
        return false;
    }

    protected bool GetIsCompleted()
    {
        return _data.IsCompleted;
    }

    protected bool GetIsCleared()
    {
        return _data.IsCleared;
    }

    protected abstract string GetTitle();

    protected abstract string GetDescription();
    #endregion
}

public abstract class BaseGameObjectiveUICell : MonoBehaviour
{
    #region Inspector Properties
    [Header("Message")]
    [SerializeField] protected TextMeshProUGUI TEXT_Head;
    [SerializeField] protected TextMeshProUGUI TEXT_Detail;
    [SerializeField] protected TextMeshProUGUI TEXT_RewardCount;
    [SerializeField] protected TextMeshProUGUI TEXT_Progression;

    [Header("Ref")]
    [SerializeField] protected GameObject Icon_Group;
    [SerializeField] protected Image RewardImage;
    [SerializeField] protected Image RewardGlow;
    [SerializeField] protected Slider SLIDER_Progression;
    [SerializeField] protected GameObject BLOCK_BlackLayer;
    [SerializeField] protected Button BTN_Reward;
    [SerializeField] protected TooltipAttachment Tooltip;
    #endregion

    #region Public Properties
    public BaseGameObjectiveUIData Data { get { return _data; } }
    #endregion

    #region Protected Properties
    protected BaseGameObjectiveUIData _data;
    #endregion

    #region Virtual Methods

    public virtual void InitData(BaseGameObjectiveUIData UIData, UnityAction onClaimReward)
    {
        _data = UIData;

        if (_data.IsCleared)
        {
            // Clear
            BLOCK_BlackLayer.SetActive(true);
            if(RewardGlow != null) RewardGlow.gameObject.SetActive(false);
        }
        else
        {
            if (_data.IsClaimable)
            {
                // Claimable
                BLOCK_BlackLayer.SetActive(false);
                if (RewardGlow != null) RewardGlow.gameObject.SetActive(true);
            }
            else
            {
                // Not Claimable
                BLOCK_BlackLayer.SetActive(false);
                if (RewardGlow != null) RewardGlow.gameObject.SetActive(false);
            }

        }

        SetHead(_data.Title);
        SetDetail(_data.Description);
        SetImage(_data.ImageKey);
        SetRewardCount(_data.RewardCount);
        SetProgress(_data.ProgressCurrent, _data.ProgressMax);
        SetSort(_data.IsClaimable, _data.IsCleared);
        SetBTN(onClaimReward);
    }
    #endregion

    #region Setup Methods
    public void SetSort(bool isClaimable, bool isClear)
    {
        if (isClaimable)
        {
            transform.SetAsFirstSibling();
        }
        else if (isClear)
        {
            transform.SetAsLastSibling();
        }
        else
        {
            // nothing
        }
    }

    public void SetRewardButtonInteractable(bool interactable)
    {
        if (BTN_Reward != null) BTN_Reward.interactable = interactable;
    }

    private void SetHead(string message)
    {
        TEXT_Head.text = message;
    }

    private void SetDetail(string detail)
    {
        TEXT_Detail.text = detail;
    }

    private void SetRewardCount(int count)
    {
        TEXT_RewardCount.text = count.ToString("N0");
    }

    private void SetProgress(int currentValue, int maxValue)
    {
        float currentProgress = (float)currentValue / (float)maxValue;
        SLIDER_Progression.value = currentProgress;
        TEXT_Progression.text = currentValue + "/" + maxValue;
    }

    private void SetImage(string imageKey)
    {
        Sprite sprite = SpriteResourceHelper.LoadSprite(imageKey, SpriteResourceHelper.SpriteType.ICON_Item);
        RewardImage.sprite = sprite;

        Tooltip.Setup(imageKey);
    }

    private void SetBTN(UnityAction onClaimReward)
    {
        BTN_Reward.onClick.RemoveAllListeners();
        BTN_Reward.onClick.AddListener(delegate
        {
            if (onClaimReward != null)
            {
                onClaimReward.Invoke();
            }
        });

    }
    #endregion
}
