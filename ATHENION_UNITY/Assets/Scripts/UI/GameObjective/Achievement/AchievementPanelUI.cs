﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Data;

public class AchievementPanelUI : MonoBehaviour
{
    #region Public Properties
    [Header("Prefabs")]
    public GameObject Prefab_AcheivementCell;

    [Header("Ref")]
    public ScrollRect ScrollRect;
    public RectTransform Rect_ViewPort;
    public Transform Container;
    #endregion

    #region Private Properties
    private List<AchievementCell> _cellList = new List<AchievementCell>();
    #endregion

    #region Static Event Properties
    public static event Action OnRefreshAchievement;
    #endregion

    #region Methods
    public void InitData()
    {
        if (DataManager.Instance.IsFeatureEnable(FeatureUnlock.Achievement))
        {
            List<AchievementData> achievementList = GetAchievementList();

            if (_cellList.Count > 0)
            {
                UpdateCell(achievementList);
            }
            else
            {
                foreach (AchievementData item in achievementList)
                {
                    // Create Achievement Cell
                    GameObject obj = Instantiate(Prefab_AcheivementCell, Container);
                    AchievementCell achievementCell = obj.GetComponent<AchievementCell>();
                    _cellList.Add(achievementCell);

                    AchievementUIData data = new AchievementUIData(item);
                    achievementCell.InitData(data,
                    delegate
                    {
                        PopupUIManager.Instance.Show_MidSoftLoad(true);
                        DataManager.Instance.CheckServerAchievement(data.ID
                            , delegate
                            {
                                PopupUIManager.Instance.Show_MidSoftLoad(false);
                                UpdateCell(achievementList);
                                ShowGotReward(data);
                                TriggerOnRefreshAchievement();
                                SortCells();
                            }
                            , delegate (string error)
                            {
                                PopupUIManager.Instance.Show_MidSoftLoad(false);
                                PopupUIManager.Instance.ShowPopup_Error("ERROR", error);
                            });
                    });

                    if (!data.IsClaimable)
                    {
                        achievementCell.ShowClaimButton(false);
                    }
                }

                // Sort
                SortCells();
            }

            TriggerOnRefreshAchievement();
            ScrollRect.onValueChanged.AddListener(OnScrolling);
        }
    }

    private static List<AchievementData> GetAchievementList()
    {
        return DataManager.Instance.PlayerAchievement.AchievementList;
    }

    private void UpdateCell(List<AchievementData> dataList)
    {
        int index = 0;
        foreach (AchievementData item in dataList)
        {
            AchievementUIData data = new AchievementUIData(item);

            // Check Achievement is complete and generate block.
            _cellList[index].InitData(data, delegate
            {
                PopupUIManager.Instance.Show_MidSoftLoad(true);
                DataManager.Instance.CheckServerAchievement(data.ID
                    , delegate
                    {
                        PopupUIManager.Instance.Show_MidSoftLoad(false);
                        UpdateCell(GetAchievementList());
                        ShowGotReward(data);
                        TriggerOnRefreshAchievement();
                    }
                    , delegate (string error)
                    {
                        PopupUIManager.Instance.Show_MidSoftLoad(false);
                        PopupUIManager.Instance.ShowPopup_Error("ERROR", error);
                    });
            });

            if (data.IsCleared || !data.IsCompleted)
            {
                _cellList[index].SetRewardButtonInteractable(false);
            }

            index++;
        }
        SortCells();
    }

    public void SortCells()
    {
        foreach (AchievementCell cell in _cellList)
        {
            cell.gameObject.SetActive(true);
            if (cell.Data.IsClaimable)
            {
                cell.transform.SetAsFirstSibling();
            }
            else if (cell.Data.IsCleared)
            {
                cell.transform.SetAsLastSibling();
            }
        }

        Container.GetComponent<ContentSizeFitter>().enabled = true;
        Container.GetComponent<VerticalLayoutGroup>().enabled = true;
        
        //ToggleVisibleCells();
    }

    private void ShowGotReward(AchievementUIData data)
    {
        string title = LocalizationManager.Instance.GetText("TEXT_GOT_REWARD");
        List<ItemData> itemList = new List<ItemData>();
        ItemData item = new ItemData(data.ImageKey, data.RewardCount);
        itemList.Add(item);

        PopupUIManager.Instance.ShowPopup_GotReward(
            title
            , data.Description
            , itemList
            );
    }

    private void ToggleVisibleCells()
    {
        RectTransform rect_cell = null;

        foreach (AchievementCell cell in _cellList)
        {
            rect_cell = cell.GetComponent<RectTransform>();
            if (GameHelper.IsRectTouchRect(rect_cell, Rect_ViewPort))
            {
                cell.gameObject.SetActive(true);
            }
            else
            {
                cell.gameObject.SetActive(false);
            }
        }
    }

    private void OnScrolling(Vector2 value)
    {
        Container.GetComponent<ContentSizeFitter>().enabled = false;
        Container.GetComponent<VerticalLayoutGroup>().enabled = false;
        ToggleVisibleCells();
    }

    public static void TriggerOnRefreshAchievement()
    {
        OnRefreshAchievement?.Invoke();
    }
    #endregion
}
