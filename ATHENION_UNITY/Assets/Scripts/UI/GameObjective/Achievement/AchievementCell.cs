﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;

public class AchievementUIData : BaseGameObjectiveUIData
{
    #region Contructors
    public AchievementUIData()
    {
    }

    public AchievementUIData(AchievementData data)
    {
        SetupData(data);
    }
    #endregion

    #region Methods
    protected override string GetTitle()
    {
        return LocalizationManager.Instance.GetAchievementTitle(ID);
    }

    protected override string GetDescription()
    {
        return LocalizationManager.Instance.GetAchievementDescription(ID);
    }
    #endregion
}

public class AchievementCell : BaseGameObjectiveUICell
{
    [Header("New Achievement Cell")]
    public RewardCell Prefab_RewardCell;
    public Button BTN_ClaimReward;

    #region Events
    private UnityAction _onClaimReward;
    #endregion
    private void OnEnable()
    {

    }

    private void OnDisable()
    {

    }

    #region Methods
    public void InitData(AchievementUIData data, UnityAction onClaimReward)
    {
        base.InitData(data, null);

        _onClaimReward = onClaimReward;
        if (BTN_ClaimReward != null)
        {
            BTN_ClaimReward.onClick.RemoveAllListeners();
            BTN_ClaimReward.onClick.AddListener(OnClickClaimReward);
        }

        if (Prefab_RewardCell != null)
        {
            Prefab_RewardCell.InitData(data.ImageKey, data.RewardCount);
        }

        ShowClaimButton(data.IsClaimable);
    }

    private void OnClickClaimReward()
    {
        _onClaimReward?.Invoke();
    }

    public void ShowClaimButton(bool show)
    {
        if(BTN_ClaimReward != null) BTN_ClaimReward.gameObject.SetActive(show);
    }
    #endregion
}
