﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Video;

public class VideoSceneManager : MonoSingleton<VideoSceneManager>
{
    #region Public Properties

    public string VideoKey => _videoKey;
    public string SceneName => _sceneName;
    public Dictionary<double, VideoSubtitleData> VideoSubtitleData => _videoSubtitleData;

    #endregion

    #region UI Element Properties

    [Header("Video Reader Element.")]
    [SerializeField] private VideoReader _videoReaderObject;

    [Header("Text UI Element.")]
    [SerializeField] private TextMeshProUGUI _subtitleText;

    [Header("Image UI Element.")]
    [SerializeField] private RawImage _videoRendererImage;
    [SerializeField] private Image _fadeImage;
    [SerializeField] private Image _blackBgImage;
    [SerializeField] private Color _defaultFadeColor;
    [SerializeField] private Image _optionPanelImage;
    [SerializeField] private Button _pauseButton;

    #endregion

    #region Private Properties

    private string _videoKey;
    private string _sceneName;
    private Dictionary<double, VideoSubtitleData> _videoSubtitleData;
    
    private double _currentSubtitle;
    private UnityAction _onComplete;

    #endregion

    #region Methods

    private void Start()
    {
        Init("TestVideo", "");
    }

    public void Init(string videoKey, string sceneName, bool isLoop = false, UnityAction onComplete = null)
    {
        _videoKey = videoKey;
        _sceneName = sceneName;
        _onComplete = onComplete;
        _videoSubtitleData = VideoLocalizationManager.Instance.GetSubtitle(_videoKey.ToUpper());

        _videoReaderObject.Init(
            delegate
            {
                _videoReaderObject.SetVideo(videoKey);

                VideoClip clip = _videoReaderObject.GetVideoClip();
            }
        );

        VideoReader.OnThisVideoPlaying += UpdateSubtitleAtSecond;
        VideoReader.OnThisVideoPlaying += RemoveSubtitleAtSecond;
        VideoReader.OnVideoEnd += FinishVideo;

        _videoReaderObject.Looping(isLoop);
        StartVideo();
    }

    public void StartVideo()
    {
        _pauseButton.enabled = true;
        _fadeImage.DOFade(1, 1).SetEase(Ease.Linear).onComplete += delegate
        {
            _blackBgImage.gameObject.SetActive(true);
            _videoReaderObject.gameObject.SetActive(true);
            _subtitleText.gameObject.SetActive(true);

            _fadeImage.DOFade(0, 1).SetEase(Ease.Linear).onComplete += delegate
            {
                _fadeImage.enabled = false;
                //_fadeImage.color = _defaultFadeColor;
                _videoReaderObject.Play();
            };
        };
    }

    public void FinishVideo()                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
    {
        _optionPanelImage.gameObject.SetActive(false);
        _pauseButton.enabled = false;
        if (!_videoReaderObject.Loop)
        {
            _fadeImage.enabled = true;
            _fadeImage.DOFade(1, 1).SetEase(Ease.Linear).onComplete += delegate
            {
                _blackBgImage.gameObject.SetActive(false);
                _videoReaderObject.gameObject.SetActive(false);
                _subtitleText.gameObject.SetActive(false);

                StartCoroutine(WaitTimeEnumerator(1,
                    delegate
                    {
                        _fadeImage.DOFade(0, 1).SetEase(Ease.Linear).onComplete += Close;
                    })
                );
                 
            };
        }
    }

    public void PauseVideo()
    {
        _videoReaderObject.Pause();
        _optionPanelImage.gameObject.SetActive(true);
    }

    public void ResumeVideo()
    {
        _optionPanelImage.gameObject.SetActive(false);
        _videoReaderObject.Play();
    }

    public void RemoveSubtitleAtSecond(double second)
    {
        if (!VideoSubtitleData.ContainsKey(_currentSubtitle))
            return;

        if (VideoSubtitleData[_currentSubtitle].EndSecond <= second && !string.IsNullOrEmpty(_subtitleText.text))
        {
            Debug.Log($"TEST on second: {second}");
            _subtitleText.text = "";
        }
    }

    public void UpdateSubtitleAtSecond(double second)
    {
        if (!VideoSubtitleData.ContainsKey(second))
            return;

        if (_currentSubtitle != second)
        {
            _currentSubtitle = second;
            _subtitleText.text = VideoSubtitleData[_currentSubtitle].Text;
        }
    }

    /// <summary>
    /// Call this method to unload (close) current event template scene.
    /// </summary>
    public void Close()
    {
        VideoReader.OnThisVideoPlaying -= UpdateSubtitleAtSecond;
        VideoReader.OnThisVideoPlaying -= RemoveSubtitleAtSecond;
        VideoReader.OnVideoEnd -= FinishVideo;
        SceneManager.UnloadSceneAsync(SceneName);
    }

    protected override void OnSceneUnloaded(Scene current)
    {
        if (current.name == SceneName)
        {
            _onComplete?.Invoke();
            DestroySelf();
        }
    }

    protected override void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        Debug.Log(scene.name == SceneName ? "Test" : "Test2");
    }

    private IEnumerator WaitTimeEnumerator(float second, UnityAction onComplete)
    {
        yield return new WaitForSeconds(second);
        onComplete?.Invoke();
    }
    #endregion
}
