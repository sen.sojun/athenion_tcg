﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Coffee.UIExtensions;

public class ScaleRatio_Attachment : MonoBehaviour
{
    [SerializeField] private UIParticle UIParticle;
    private float _defaultScreenWidth = 1080;
    private float _defaultScreenHeight = 1920;

    private void OnEnable()
    {
        float resultScale = 1;
        resultScale = CalculateScale(resultScale);
        UIParticle.scale = resultScale;

    }


    private float CalculateScale(float resultScale)
    {
        switch (GameHelper.GetScreenFitMode())
        {
            case ScreenFitMode.Width:
            return Screen.width * 1f / _defaultScreenWidth;
            case ScreenFitMode.Height:
            return Screen.height * 1f / _defaultScreenHeight;
            default:
            return 1f;
        }
    }
}
