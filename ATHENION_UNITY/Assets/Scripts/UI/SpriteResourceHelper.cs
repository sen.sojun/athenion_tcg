﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Karamucho;

public class SpriteResourceHelper
{
    #region Enum
    public enum SpriteType
    {
        ICON_Rank
        , BannerSeason

        //FTUE
        , TU_DynamicPopup //for tutorial dynamic popup

        // Item Icon
        , ICON_Item
        , ICON_Avatar
        , ICON_CardBack
        , ICON_HeroSkin
        , ICON_Title
        , ICON_HUD
        , ICON_Keyword
        , ICON_WildOffer
        , ICON_Card

        // Cosmetic
        , Hero_Square
        , SKIN_Hero
        , Avatar
        , Title
        , CardBack_IMG
        , HUD_SS

        // Event
        , FactLvl_CustomImage
        , SSPass_CustomImage

        // Event
        , GAME_Event
        , TMP_Event

        // lesson
        , Lesson

        // Adventure
        , AdventureStage
    }
    #endregion

    #region Path
    // Default Error path
    private static readonly string _errorPath = "Images/ERROR_MISSING";

    // FTUE Path
    private static readonly string _tuDynamicPopupPath = "Images/Tutorial/DynamicPopupImages/";

    // Icon Path
    private static readonly string _rankIconPath = "Images/Icon_Ranks/";
    private static readonly string _bannerSeasonPath = "Images/BannerSeasons/" + "SeasonBanner_";

    // Item Icon Path
    private static readonly string _itemIconPath = "Images/Items/";
    private static readonly string _avatarIconPath = "Images/Items/Icon_Avatars/";
    private static readonly string _cardBackIconPath = "Images/Items/Icon_CardBacks/";
    private static readonly string _cardIconPath = "Images/Items/Icon_Cards/";
    private static readonly string _heroSkinIconPath = "Images/Items/Icon_HeroSkins/";
    private static readonly string _wildOfferIconPath = "Images/Items/Icon_WildOffer/";
    private static readonly string _titleIconPath = "Images/Items/Icon_Titles/";
    private static readonly string _hudIconPath = "Images/Items/Icon_HUDs/";
    private static readonly string _keywordIconPath = "Images/KeywordIcons/KEYWORD_";

    // Cosmetic Path
    private static readonly string _heroSquarePath = "Images/Hero_Square/SQ_";
    private static readonly string _heroFullPath = "Images/Hero_Full/FULL_";
    private static readonly string _avatarPath = "Images/Cosmetics/Avatars/";
    private static readonly string _titlePath = "Images/Cosmetics/Titles/";
    private static readonly string _cardBackImagePath = "Prefabs/CardBacks/Images/";
    private static readonly string _hudSSPath = "Images/HUD_SS/SS_";

    // Faction Level Custom Images
    private static readonly string _flvlCustomImagePath = "Images/FactionLevelCustomImages/";

    // Season Pass Path
    private static readonly string _sspassCustomPrize = "Images/SeasonPassCustomImages/";

    // Lesson Path
    private static readonly string _lessonPath = "Images/Tutorial/Lessons/";

    // Event Path
    private static readonly string _gameEventPath = "Events/EventImages/";
    private static readonly string _eventImagePath = "Events/{0}/Sprites/"; // event template

    //Adventure Path
    private static readonly string _adventureStagePath = "Images/Adventure/Stages/";
    #endregion

    #region Methods

    public static Texture LoadTexture_CardBack(string key)
    {
        string path = _cardBackImagePath;

        // Try load sprite
        string targetPath = path + key;
        Texture tex;

        tex = ResourceManager.Load<Texture>(targetPath);

        // Case cannot find texture !
        if (tex == null)
        {
            // Try load default
            string defaultPath = path + "CBDEFAULT";
            tex = ResourceManager.Load(defaultPath) as Texture;

            // if still cannot find show error cardback.
            if (tex == null)
            {
                tex = ResourceManager.Load(_errorPath) as Texture;
                Debug.LogWarning("Cannot Find Default CardBack Texture : " + defaultPath);
            }
            else
            {
                Debug.LogWarning("Cannot Find Texture Use default : " + targetPath);
            }
        }

        return tex;
    }

    public static Sprite LoadSprite_CardBack(string key)
    {
        string path = _cardBackImagePath;

        // Try load sprite
        string targetPath = path + key;
        Sprite tex;

        tex = ResourceManager.Load<Sprite>(targetPath);

        // Case cannot find texture !
        if (tex == null)
        {
            // Try load default
            string defaultPath = path + "CB0000";
            tex = ResourceManager.Load<Sprite>(defaultPath);

            // if still cannot find show error cardback.
            if (tex == null)
            {
                tex = ResourceManager.Load<Sprite>(_errorPath);
                Debug.LogWarning("Cannot Find Default CardBack Texture : " + defaultPath);
            }
            else
            {
                Debug.LogWarning("Cannot Find Texture Use default : " + targetPath);
            }
        }

        return tex;
    }

    public static Sprite LoadSubEventImage(string eventKey, string imageName, bool nullable = false)
    {
        // Try load sprite
        string targetPath = string.Format(_eventImagePath, eventKey) + imageName;
        Sprite sprite = null;
        if (nullable)
        {
            sprite = GetSpriteNullable(targetPath);
        }
        else
        {
            sprite = GetSprite(targetPath);
        }
        return sprite;
    }

    public static Sprite LoadSprite(string key, SpriteType type, bool nullable = false)
    {
        string path = "";

        // Assign path
        switch (type)
        {
            case SpriteType.ICON_Rank:
                {
                    path = _rankIconPath;
                }
                break;

            case SpriteType.BannerSeason:
                {
                    path = _bannerSeasonPath;
                }
                break;

            case SpriteType.ICON_WildOffer:
                {
                    path = _wildOfferIconPath;
                }
                break;
            // Item Icon
            case SpriteType.ICON_Item:
                {
                    //"ITEM_NAME_XXXX_XXXXX"
                    string id = key.Replace("ITEM_NAME_", "");

                    if (id.StartsWith("AV"))
                    {
                        path = _avatarIconPath;
                    }
                    else if (id.StartsWith("CB"))
                    {
                        path = _cardBackIconPath;
                    }
                    else if (id.StartsWith("CONT"))
                    {
                        path = _itemIconPath;
                    }
                    else if (id.StartsWith("HUD"))
                    {
                        path = _hudIconPath;
                    }
                    else if (id.StartsWith("TT"))
                    {
                        path = _titleIconPath;
                    }
                    else if (id.StartsWith("H"))
                    {
                        path = _heroSkinIconPath;
                    }                    
                    else if (id.StartsWith("C"))
                    {
                        key = key.Replace(":", "_");
                        path = _cardIconPath;
                    }
                    else
                    {
                        path = _itemIconPath;
                    }
                    break;
                }

            case SpriteType.ICON_Avatar:
                {
                    path = _avatarIconPath;
                }
                break;

            case SpriteType.ICON_CardBack:
                {
                    path = _cardBackIconPath;
                }
                break;

            case SpriteType.ICON_Card:
                {
                    path = _cardIconPath;
                }
                break;

            case SpriteType.ICON_HeroSkin:
                {
                    path = _heroSkinIconPath;
                }
                break;

            case SpriteType.ICON_Title:
                {
                    path = _titleIconPath;
                }
                break;

            case SpriteType.ICON_HUD:
                {
                    path = _hudIconPath;
                }
                break;

            case SpriteType.ICON_Keyword:
                {
                    path = _keywordIconPath;
                }
                break;

            // Cosmetic
            case SpriteType.Hero_Square:
                {
                    path = _heroSquarePath;
                }
                break;

            case SpriteType.Avatar:
                {
                    path = _avatarPath;
                }
                break;

            case SpriteType.Title:
                {
                    path = _titlePath;
                }
                break;

            case SpriteType.CardBack_IMG:
                {
                    path = _cardBackImagePath;
                }
                break;

            case SpriteType.HUD_SS:
                {
                    path = _hudSSPath;
                }
                break;

            case SpriteType.SKIN_Hero:
                {
                    path = _heroFullPath;
                }
                break;

            case SpriteType.GAME_Event:
                {
                    path = _gameEventPath;
                }
                break;

            case SpriteType.TMP_Event:
                {
                    path = _eventImagePath;
                }
                break;

            case SpriteType.TU_DynamicPopup:
                {
                    path = _tuDynamicPopupPath;
                }
                break;
                
            case SpriteType.SSPass_CustomImage:
                {
                    path = _sspassCustomPrize;
                }
                break;
                
            case SpriteType.FactLvl_CustomImage:
                {
                    path = _flvlCustomImagePath;
                }
                break;

            case SpriteType.Lesson:
                {
                    path = _lessonPath;
                }
            break;

            case SpriteType.AdventureStage:
                {
                    path = _adventureStagePath;
                }
                break;

            default:
                {
                    path = _errorPath;
                }
                break;
        }

        // Try load sprite
        string targetPath = path + key;
        Sprite sprite;
        if (nullable)
        {
            sprite = GetSpriteNullable(targetPath);
        }
        else
        {
            sprite = GetSprite(targetPath);
        }
        return sprite;
    }

    private static Sprite GetSprite(string path)
    {
        Sprite sprite = null;
        sprite = ResourceManager.Load<Sprite>(path);

        // Case cannot find sprite !
        if (sprite == null)
        {
            sprite = ResourceManager.Load<Sprite>(_errorPath);
            Debug.LogWarning("Cannot Find Sprite : " + path);
        }

        return sprite;
    }

    private static Sprite GetSpriteNullable(string path)
    {
        Sprite sprite = null;
        sprite = ResourceManager.Load<Sprite>(path);

        // Case cannot find sprite !
        if (sprite == null)
        {
            //sprite = ResourceManager.Load<Sprite>(_errorPath);
            Debug.LogWarning("Cannot Find Sprite : " + path);
        }

        return sprite;
    }

    public static Sprite LoadSprite()
    {
        // Case cannot find sprite !
        return ResourceManager.Load<Sprite>(_errorPath);
    }
    #endregion
}
