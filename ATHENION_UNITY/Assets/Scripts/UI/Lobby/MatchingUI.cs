﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;

public class MatchingUI : MonoBehaviour
{
    #region Public Properties
    public TextMeshProUGUI GameModeText;
    public TextMeshProUGUI StatusText;
    public TextMeshProUGUI TimerText;
    public Button CancelButton;

    public bool IsTimer { get { return _isTimer; } }
    #endregion

    #region Private Properties
    private bool _isTimer = false;
    private bool _isClickCancel = false;
    private UnityAction _onClickCancel = null;
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        
    }

    #region Update
    // Update is called once per frame
    void Update()
    {
    }

    public void UpdateTimer(float timer)
    {
        int min = Mathf.FloorToInt(timer / 60);
        int sec = Mathf.FloorToInt(timer % 60);

        TimerText.text = string.Format(
              LocalizationManager.Instance.GetText("MENU_MATCHING_TIMER")
            , min
            , sec
        );
    }
    #endregion

    #region Methods
    public void ShowUI(string status, UnityAction onClickCancel)
    {
        _isTimer = false;

        SetStatusTitle(status);
        HideTimer();
        SetOnClickCancel(onClickCancel);

        CancelButton.onClick.AddListener(OnClickCancel);

        SoundManager.PlaySFX(SoundManager.SFX.End_Game_Result_Flag);
        gameObject.SetActive(true);
    }

    public void HideUI()
    {
        CancelButton.onClick.RemoveListener(OnClickCancel);
        gameObject.SetActive(false);
    }

    public void SetGameMode(GameMode gameMode)
    {
        string textKey = ("MENU_MATCHING_MODE_" + gameMode.ToString()).ToUpper();        
        GameModeText.text = LocalizationManager.Instance.GetText(textKey);
    }
    
    public void ShowCancelButton()
    {
        CancelButton.gameObject.SetActive(true);
    }

    public void HideCancelButton()
    {
        CancelButton.gameObject.SetActive(false);
    }

    public void ShowTimer()
    {
        TimerText.gameObject.SetActive(true);
    }

    public void HideTimer()
    {
        TimerText.gameObject.SetActive(false);
    }

    public void SetStatusTitle(string status)
    {
        StatusText.text = status;
    }

    public void SetOnClickCancel(UnityAction onClickCancel)
    {
        _isClickCancel = false;
        _onClickCancel = onClickCancel;
    }

    private void OnClickCancel()
    {
        if (_isClickCancel == true) return;
        _isClickCancel = true;

        if (_onClickCancel != null)
        {
            _onClickCancel.Invoke();
        }
    }
    #endregion
}
