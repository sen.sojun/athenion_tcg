﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using Photon.Pun;
using Photon.Realtime;
using Newtonsoft.Json;

namespace Karamucho.Network
{
    public struct MatchSetting
    {
        public GameMode Mode;
        public PhotonRegion Region;
        public string RoomName;
        public string FriendPlayfabID;

        public MatchSetting(GameMode mode)
        {
            Mode = mode;
            Region = PhotonRegion.None;
            RoomName = "";
            FriendPlayfabID = "";
        }

        public MatchSetting(GameMode mode, PhotonRegion region, string roomName)
        {
            Mode = mode;
            Region = region;
            RoomName = roomName;
            FriendPlayfabID = "";
        }

        public MatchSetting(GameMode mode, string friendPlayfabID)
        {
            Mode = mode;
            Region = PhotonRegion.None;
            RoomName = "";
            FriendPlayfabID = friendPlayfabID;
        }
    }

    public struct FriendMatchPlayerInfo
    {
        public PlayerIndex PlayerIndex;
        public string PlayerID;
        public string DisplayName;
        public bool IsReady;

        public FriendMatchPlayerInfo(PlayerIndex playerIndex, string playerID, string displayName)
        {
            PlayerIndex = playerIndex;
            PlayerID = playerID;
            DisplayName = displayName;
            IsReady = false;
        }
    }

    public class MatchingManager : MonoBehaviourPunCallbacks
    {
        #region Enums
        public enum MatchingStatus
        {
            Idle
            , Init
            , Connecting
            , Connected
            , Disconnecting
            , Disconnected

            , JoiningLobby
            , InLobby

            , JoiningRoom
            , CreatingRoom
            , InRoom
            , LeavingRoom
        }
        #endregion

        public delegate void MatchingLobbyEventHandler(GameMode mode);
        public delegate void MatchingEventHandler();

        public static event MatchingLobbyEventHandler OnInLobby;
        public static event MatchingEventHandler OnExit;

        #region Private Serializable Fields
        [Tooltip("The maximum number of players per room")]
        [SerializeField]
        private byte maxPlayersPerRoom = 2;
        #endregion

        #region Private Fields
        private readonly float _widthMargin = 20.0f;
        private readonly float _heightMargin = 20.0f;
        private readonly bool _isAutoJoin = true;

        private readonly float _waitTime_Min = 6.0f;
        private readonly float _waitTime_Max = 8.0f;

        private float _matchingResetTime = 181.0f;

        private Dictionary<GameMode, float[]> _matchingTime = new Dictionary<GameMode, float[]>() {
              { GameMode.Casual, new float[] { 90.0f, 90.0f } }
            , { GameMode.Rank, new float[] { 30.0f, 20.0f, 20.0f, -1.0f } } // ค่าติดลบหมายถึง ไม่นับเวลา
        };

        private Dictionary<GameMode, int[]> _scoreOffset = new Dictionary<GameMode, int[]>() {
              { GameMode.Casual, new int[] { 500, 1000 } }
            , { GameMode.Rank, new int[] { 11, 15, 20, 20 } } // ค่าติดลบหมายถึง ไม่นับเวลา
        };

        private Dictionary<PlayerTierType, float[]> _matchingTime_Newbie = new Dictionary<PlayerTierType, float[]>(){
              { PlayerTierType.Newbie1, new float[] { 10.0f } }
            , { PlayerTierType.Newbie2, new float[] { 10.0f } }
            , { PlayerTierType.Newbie3, new float[] { 10.0f } }
            , { PlayerTierType.Newbie4, new float[] { 10.0f } }
            , { PlayerTierType.Newbie5, new float[] { 20.0f } }
            , { PlayerTierType.Newbie6, new float[] { 20.0f } }
        };

        // { minMMR, maxMMR, minNSScore, MaxNSScore }
        private Dictionary<PlayerTierType, List<int[]>> _scoreOffset_Newbie = new Dictionary<PlayerTierType, List<int[]>>() {
              { PlayerTierType.Newbie1, new List<int[]> { (new int[] { -1, -1, 0, 30 }) } }
            , { PlayerTierType.Newbie2, new List<int[]> { (new int[] { -1, -1, 15, 24 }) } }
            , { PlayerTierType.Newbie3, new List<int[]> { (new int[] { -1, -1, 15, 24 }) } }
            , { PlayerTierType.Newbie4, new List<int[]> { (new int[] { -1, -1, 0, 15 }) } }
            , { PlayerTierType.Newbie5, new List<int[]> { (new int[] { 0, 250, 0, 15 }) } }
            , { PlayerTierType.Newbie6, new List<int[]> { (new int[] { 0, 500, 0, 15 }) } }
        };

        private Dictionary<int, float[]> _matchingTime_Appentice = new Dictionary<int, float[]>(){
              { -8, new float[] { 10.0f } }
            , { -7, new float[] { 10.0f } }
            , { -6, new float[] { 10.0f } }
            , { -5, new float[] { 10.0f } }
            , { -4, new float[] { 10.0f } }
            , { -3, new float[] { 20.0f } }
            , { -2, new float[] { 20.0f } }
            , { -1, new float[] { 20.0f } }
        };

        // { minRank, maxRank }
        private Dictionary<int, List<int[]>> _scoreOffset_Appentice = new Dictionary<int, List<int[]>>() {
              { -8, new List<int[]> { (new int[] { -8, -8 }) } }
            , { -7, new List<int[]> { (new int[] { -7, -7 }) } }
            , { -6, new List<int[]> { (new int[] { -6, -4 }) } }
            , { -5, new List<int[]> { (new int[] { -6, -4 }) } }
            , { -4, new List<int[]> { (new int[] { -6, -4 }) } }
            , { -3, new List<int[]> { (new int[] { -3, 7 }) } }
            , { -2, new List<int[]> { (new int[] { -3, 7 }) } }
            , { -1, new List<int[]> { (new int[] { -3, 7 }) } }
        };

        private Dictionary<BotLevel, int[]> _matchBotHiddenScore = new Dictionary<BotLevel, int[]>() { 
              { BotLevel.Easy, new int[] {-1, -1 } }
            , { BotLevel.Medium, new int[] { 0, 500 } }
            , { BotLevel.Hard, new int[] { 501, 3000 } }
        };

        // beyond
        private float[] _matchingTime_Beyond = { 40.0f, 30.0f, 20.0f, 20.0f, -1.0f };
        private int[] _scoreOffset_Beyond = { 0, 11, 15, 20, 20 };

        private readonly int ReconnectRetryTime = 15; // ถ้าเป็น -1 คือ Retry ได้ไม่จำกัด

        /// <summary>
        /// Keep track of the current process. Since connection is asynchronous and is based on several callbacks from Photon, 
        /// we need to keep track of this to properly adjust the behavior when we receive call back by Photon.
        /// Typically this is used for the OnConnectedToMaster() callback.
        /// </summary>
        private bool _isRequestReconnect = false;
        private int _reconnectRetryTime = 0;

        private MatchSetting _matchSetting;

        private MatchingUIManager _matchingUIManager = null;
        private MatchingStatus _matchingStatus = MatchingStatus.Init;

        private bool _isConnected = false;
        private bool _isMatched = false;
        private bool _isTimeout = false;
        private bool _isTimer = false;

        private bool _isResetTimer = true;
        private int _retryIndex = 0;
        private float _waitTimer = 0.0f;
        private float _roomTimer = 0.0f;
        private float _matchingTimeoutTimer = 0.0f;
        private float _timer = 0.0f;
        private float _matchingTimeout = 0.0f;
        private bool _isUpdateStatusTitle = true;

        private bool _isStartDummyMatching = false;
        private Coroutine _dummyMatching = null;

        private UnityAction _onDisconnectComplete = null;
        private UnityAction _onLeaveRoomComplete = null;

        /// <summary>
        /// This client's version number. Users are separated from each other by gameVersion (which allows you to make breaking changes).
        /// </summary>
        string gameVersion = "ATN_1";
        private List<string> _logList;
        private List<RoomInfo> _roomInfoList;

        private PhotonView _photonView;
        private MatchData _matchData;
        private MatchPlayerData _myData;
        private MatchPlayerData _receivedData;
        private StringBuilder _debugText;
        private Rect _debugTextRect;

        private Dictionary<PlayerIndex, FriendMatchPlayerInfo> _friendInfoList;
        #endregion

        #region Public Fields
        public static readonly int PlayerTtl = 60000;
        public static readonly int EmptyRoomTtl = 60000;
        public static readonly int PlayerTtl_Matching = 10000;
        public static readonly int EmptyRoomTtl_Matching = 5000;

        public static readonly float WaitTime_Bot_Min = 0.0f;
        public static readonly float WaitTime_Bot_Max = 5.0f;

        public GameMode Mode { get { return _matchSetting.Mode; } }
        public MatchSetting Setting { get { return _matchSetting; } }
        public bool IsFriendReady
        {
            get
            {
                if (_friendInfoList != null && _friendInfoList.Count > 0)
                {
                    foreach (KeyValuePair<PlayerIndex, FriendMatchPlayerInfo> friend in _friendInfoList)
                    {
                        if (!friend.Value.IsReady)
                        {
                            return false;
                        }
                    }

                    return true;
                }

                return false;
            }
        }
        public bool IsConnected { get { return _isConnected; } }
        #endregion

        #region MonoBehaviour CallBacks
        /// <summary>
        /// MonoBehaviour method called on GameObject by Unity during early initialization phase.
        /// </summary>
        void Awake()
        {
            // #Critical
            // this makes sure we can use PhotonNetwork.LoadLevel() on the master client and all clients in the same room sync their level automatically
            PhotonNetwork.AutomaticallySyncScene = true;

            _debugTextRect = new Rect(
                 _widthMargin
               , _heightMargin
               , Screen.width - (2.0f * _widthMargin)
               , Screen.height - (2.0f * _heightMargin)
           );

            LogFeedback("", false);
        }

        private void Update()
        {
#if UNITY_EDITOR
            if (Input.GetKeyDown(KeyCode.A))
            {
                ExitMatching();
            }
#endif

            if (_isTimer)
            {
                _waitTimer += Time.deltaTime;
                UpdateTimer(_waitTimer);

                _timer += Time.deltaTime;
                _matchingTimeoutTimer += Time.deltaTime;

                if (!_isStartDummyMatching)
                {
                    if (_matchingStatus == MatchingStatus.InRoom)
                    {
                        _roomTimer -= Time.deltaTime;
                    }

                    if (!_isMatched)
                    {
                        if (Mode != GameMode.Friendly)
                        {
                            if (!IsBusy())
                            {
                                if (_timer >= _matchingTimeout && _matchingTimeout >= 0.0f)
                                {
                                    OnMatchingTimeout();
                                    return;
                                }

                                if (_matchingStatus == MatchingStatus.InRoom && _roomTimer <= 0.0f)
                                {
                                    OnRetryMatching();
                                    return;
                                }

                                if (_matchingTimeoutTimer >= _matchingResetTime)
                                {
                                    OnReInitMatching();
                                    return;
                                }
                            }
                        }
                    }
                }
            }
        }

        private void OnGUI()
        {
            if (GameHelper.IsATNVersion_RealDevelopment)
            {
                if (_debugText == null) _debugText = new StringBuilder();
                _debugText.Clear();

                if (PhotonNetwork.IsConnected)
                {
                    string regionStr = GetServerRegion(PhotonNetwork.CloudRegion).ToString();

                    string roomName = "Empty";
                    if (PhotonNetwork.CurrentRoom != null)
                    {
                        roomName = PhotonNetwork.CurrentRoom.Name;
                    }

                    string roomDetail = string.Format(
                        "<color=#7CFC00>Connected Region : {0}\nMode : {1}\nRoomName : {2}</color>\n"
                        , regionStr
                        , Mode
                        , roomName
                    );

                    _debugText.Append(roomDetail);
                }

                string playerDetail = string.Format(
                          "<color=#7CFC00>Tier: {0} Score: {1} Rank: {2} MMR: {3}</color>\n"
                        , DataManager.Instance.PlayerInfo.GetPlayerTier()
                        , DataManager.Instance.PlayerInfo.GetPlayerCasualHiddenScore()
                        , DataManager.Instance.GetPlayerRankIndex()
                        , GetMyMMR()
                    );

                _debugText.Append(playerDetail);

                if (_logList != null)
                {
                    for (int index = _logList.Count - 1; index >= 0; --index)
                    {
                        if (_debugText.Length > 0)
                        {
                            _debugText.Append("\n" + _logList[index]);
                        }
                        else
                        {
                            _debugText.Append(_logList[index]);
                        }
                    }
                }

                GUI.Label(_debugTextRect, "<color=yellow>" + _debugText + "</color>");
            }
        }

        private void OnApplicationPause(bool pause)
        {
            if (pause)
            {
                // Pause
            }
            else
            {
                // Resume
                ExitMatching();
            }
        }
        #endregion

        #region Methods
        private void UpdateTimer(float timer)
        {
            if (_matchingUIManager != null)
            {
                _matchingUIManager.UpdateTimer(timer);
            }
        }

        public void SetPhotonSetting(MatchSetting matchSetting)
        {
            _matchSetting = matchSetting;
        }

        /*
        public void Connect()
        {
            if (!PhotonNetwork.IsConnected)
            {
                isConnecting = true;

                LogFeedback("Connecting...");
                _matchingUIManager.SetStatusTitle(LocalizationManager.Instance.GetText("MENU_MATCHING_STATUS_CONNECTING"));

                // #Critical, we must first and foremost connect to Photon Online Server.
                PhotonNetwork.GameVersion = this.gameVersion;
                PhotonNetwork.PhotonServerSettings.AppSettings.FixedRegion = null;
                PhotonNetwork.ConnectUsingSettings();

                Debug.LogFormat(
                      "Connecting to <color=blue>PUN</color> Server ID :<color=blue> [{0}]:{1}</color>"
                    , GetServerRegion(PhotonNetwork.PhotonServerSettings.AppSettings.FixedRegion).ToString()
                    , PhotonNetwork.PhotonServerSettings.AppSettings.AppIdRealtime
                );

                PhotonNetwork.AuthValues = new AuthenticationValues();
                PhotonNetwork.AuthValues.UserId = DataManager.Instance.PlayerInfo.PlayerID;
                Debug.Log(DataManager.Instance.PlayerInfo.PlayerID);
            }
        }
        */

        /// <summary>
        // Connect this application instance to Photon Cloud Network
        /// </summary>
        public void ConnectWithSetting(PhotonRegion region)
        {
            //if (!PhotonNetwork.IsConnected)
            {
                LogFeedback("Connecting...");
                SetStatusTitle(LocalizationManager.Instance.GetText("MENU_MATCHING_STATUS_CONNECTING"));

                // #Critical, we must first and foremost connect to Photon Online Server.
                PhotonNetwork.GameVersion = this.gameVersion;
                if (region == PhotonRegion.None)
                {
                    PhotonNetwork.PhotonServerSettings.AppSettings.FixedRegion = null;
                }
                else
                {
                    PhotonNetwork.PhotonServerSettings.AppSettings.FixedRegion = GetServerRegion(region);
                }

                _matchingStatus = MatchingStatus.Connecting;
                bool isSuccess = PhotonNetwork.ConnectUsingSettings();

                Debug.LogFormat(
                      "Connecting to <color=blue>PUN</color> Server ID :<color=blue> [{0}]:{1}</color>"
                    , GetServerRegion(PhotonNetwork.PhotonServerSettings.AppSettings.FixedRegion).ToString()
                    , PhotonNetwork.PhotonServerSettings.AppSettings.AppIdRealtime
                );

                PhotonNetwork.AuthValues = new AuthenticationValues();
                PhotonNetwork.AuthValues.UserId = DataManager.Instance.PlayerInfo.PlayerID;
                Debug.Log(DataManager.Instance.PlayerInfo.PlayerID);

                if (!isSuccess)
                {
                    _matchingStatus = MatchingStatus.Idle;
                }
            }
            //else
            //{
            //    Debug.LogErrorFormat("ConnectWithSetting: Already connected. {0}", region);

            //    PhotonNetwork.Disconnect();
            //    _matchingUIManager.ShowCancelButton();

            //    PopupUIManager.Instance.ShowPopup_Error(
            //          LocalizationManager.Instance.GetText("ERROR_POPUP_HEADER")
            //        , LocalizationManager.Instance.GetText("ERROR_FAIL_ALREADY_CONNECTED")
            //        , delegate () { ExitMatching(false); }
            //    );
            //}
        }

        public void Reconnect(bool isCountRetry = true)
        {
            UnityAction action = delegate ()
            {
                _isRequestReconnect = false;

                PopupUIManager.Instance.ShowPopup_Error(
                      LocalizationManager.Instance.GetText("ERROR_POPUP_HEADER")
                    , LocalizationManager.Instance.GetText("ERROR_FAIL_TO_CONNECT_SERVER")
                    , delegate () { ExitMatching(); }
                );
            };

            try
            {
                if (isCountRetry)
                {
                    _reconnectRetryTime++;

                    if (_reconnectRetryTime > ReconnectRetryTime && ReconnectRetryTime > 0)
                    {
                        // Retry exceed limit.
                        action?.Invoke();
                    }
                }

                if (!PhotonNetwork.Reconnect())
                {
                    action?.Invoke();
                }
            }
            catch(System.Exception e)
            {
                action?.Invoke();
                PopupUIManager.Instance.Show_SoftLoad(false);
            }
        }

        public void Disconnect(UnityAction onDisconnectComplete = null)
        {
            Debug.Log("Try Disconnect...");

            _onDisconnectComplete = onDisconnectComplete;

            if (!PhotonNetwork.OfflineMode)
            {
                if (
                    (_matchingStatus != MatchingStatus.Init
                    && (PhotonNetwork.NetworkingClient != null)
                    && (PhotonNetwork.NetworkingClient.State != ClientState.Disconnected)
                    )
                    || PhotonNetwork.IsConnected
                )
                {
                    _matchingStatus = MatchingStatus.Disconnecting;
                    PhotonNetwork.Disconnect();
                    return;
                }
            }

            Debug.LogWarningFormat("Disconnected: Fail to disconnect. status[{0}]", PhotonNetwork.NetworkClientState);
            _isConnected = false;

            _onDisconnectComplete = null;
            onDisconnectComplete?.Invoke();
        }

        public void JoinLobby(TypedLobby typedLobby)
        {
            _matchingStatus = MatchingStatus.JoiningLobby;
            if (!PhotonNetwork.JoinLobby(typedLobby))
            {
                _matchingStatus = MatchingStatus.Idle;
            }
        }

        public void Init(MatchingUIManager matchingUIManager)
        {
            _matchingUIManager = matchingUIManager;
            _matchingStatus = MatchingStatus.Init;
            _matchingUIManager.HideCancelButton();

            SetupSettingValue();

            _isMatched = false;
            _waitTimer = 0.0f;
            _matchingTimeoutTimer = 0.0f;
            _reconnectRetryTime = 0;
            _isUpdateStatusTitle = true;

            StopTimer();

            _debugTextRect = new Rect(
                _widthMargin
                , _heightMargin
                , Screen.width - (2.0f * _widthMargin)
                , Screen.height - (2.0f * _heightMargin)
            );
            LogFeedback("", false);

            // #Critical
            // this makes sure we can use PhotonNetwork.LoadLevel() on the master client and all clients in the same room sync their level automatically
            PhotonNetwork.AutomaticallySyncScene = true;

            SetPlayerName(DataManager.Instance.PlayerInfo.DisplayName);
            _photonView = PhotonView.Get(this);
            _matchingUIManager.ShowMatching("", this.OnClickCancel);

            OnInLobby?.Invoke(Mode);

            switch (Mode)
            {
                case GameMode.Casual:
                {
                    switch (DataManager.Instance.PlayerInfo.GetPlayerTier())
                    {
                        case PlayerTierType.Newbie1:
                        {
                            StartDummyMatch(
                                  WaitTime_Bot_Min
                                , WaitTime_Bot_Max
                                , delegate ()
                                {
                                    StartBotMatch(1); // vs bot level 1
                                }
                            );
                        }
                        break;

                        case PlayerTierType.Loser1:
                        {
                            StartDummyMatch(
                                  WaitTime_Bot_Min
                                , WaitTime_Bot_Max
                                , delegate ()
                                {
                                    StartBotMatch(5); // vs bot level 5
                                }
                            );
                        }
                        break;

                        case PlayerTierType.Loser2:
                        {
                            StartDummyMatch(
                                  WaitTime_Bot_Min
                                , WaitTime_Bot_Max
                                , delegate ()
                                {
                                    StartBotMatch(2); // vs bot level 2
                                }
                            );
                        }
                        break;

                        default:
                        {
                            ConnectWithSetting(_matchSetting.Region);
                        }
                        break;
                    }
                }
                break;

                case GameMode.Rank:
                {
                    switch (DataManager.Instance.PlayerInfo.GetPlayerCurrentRank())
                    {
                        case -8: // Apprentice 4
                        { 
                            StartDummyMatch(
                                  WaitTime_Bot_Min
                                , WaitTime_Bot_Max
                                , delegate () 
                                {
                                    StartBotMatch(1); // vs bot level 1
                                }
                            );
                        }
                        break;

                        default:
                        {
                            ConnectWithSetting(_matchSetting.Region);
                        }
                        break;
                    }
                }
                break;


                case GameMode.Event:
                {
                    if (EventBattleDB.Instance.IsVSBot)
                    {
                        StartDummyMatch(0.0f, 0.5f, StartBotEventBattleMatch);
                    }
                    else
                    {
                        ConnectWithSetting(_matchSetting.Region);
                    }
                }
                break;

                default:
                {
                    ConnectWithSetting(_matchSetting.Region);
                }
                break;
            }
        }

        private void SetupSettingValue()
        {
            object value = null;

            if (DataManager.Instance.GetGameValue("MatchingValue", out value))
            {
                Dictionary<string, object> dict = JsonConvert.DeserializeObject<Dictionary<string, object>>(value.ToString());

                if (dict.ContainsKey("MatchingTime"))
                {
                    Dictionary<string, float[]> matchingTime = JsonConvert.DeserializeObject<Dictionary<string, float[]>>(dict["MatchingTime"].ToString());
                    if (matchingTime != null && matchingTime.Count > 0)
                    {
                        Dictionary<GameMode, float[]> dict_temp = new Dictionary<GameMode, float[]>();

                        GameMode gameMode;
                        foreach (KeyValuePair<string, float[]> item in matchingTime)
                        {
                            if (GameHelper.StrToEnum(item.Key, out gameMode))
                            {
                                dict_temp.Add(gameMode, item.Value);
                            }
                        }

                        _matchingTime = new Dictionary<GameMode, float[]>(dict_temp);
                    }
                }
                else
                {
                    Debug.LogErrorFormat("MatchingManager/SetupSettingValue: Failed to get MatchingTime.");
                }

                if (dict.ContainsKey("MatchingScoreOffset"))
                {
                    Dictionary<string, int[]> matchingScoreOffset = JsonConvert.DeserializeObject<Dictionary<string, int[]>>(dict["MatchingScoreOffset"].ToString());
                    if (matchingScoreOffset != null && matchingScoreOffset.Count > 0)
                    {
                        Dictionary<GameMode, int[]> dict_temp = new Dictionary<GameMode, int[]>();

                        GameMode gameMode;
                        foreach (KeyValuePair<string, int[]> item in matchingScoreOffset)
                        {
                            if (GameHelper.StrToEnum(item.Key, out gameMode))
                            {
                                dict_temp.Add(gameMode, item.Value);
                            }
                        }

                        _scoreOffset = new Dictionary<GameMode, int[]>(dict_temp);
                    }
                }
                else
                {
                    Debug.LogErrorFormat("MatchingManager/SetupSettingValue: Failed to get MatchingScoreOffset.");
                }

                if (dict.ContainsKey("MatchingBotHiddenScore"))
                {
                    Dictionary<string, int[]> matchingBotHiddenScore = JsonConvert.DeserializeObject<Dictionary<string, int[]>>(dict["MatchingBotHiddenScore"].ToString());
                    if (matchingBotHiddenScore != null && matchingBotHiddenScore.Count > 0)
                    {
                        Dictionary<BotLevel, int[]> dict_temp = new Dictionary<BotLevel, int[]>();

                        BotLevel botLevel;
                        foreach (KeyValuePair<string, int[]> item in matchingBotHiddenScore)
                        {
                            if (GameHelper.StrToEnum(item.Key, out botLevel))
                            {
                                dict_temp.Add(botLevel, item.Value);
                            }
                        }

                        _matchBotHiddenScore = new Dictionary<BotLevel, int[]>(dict_temp);
                    }
                }
                else
                {
                    Debug.LogErrorFormat("MatchingManager/SetupSettingValue: Failed to get MatchingBotHiddenScore.");
                }

                if (dict.ContainsKey("MatchingResetTime"))
                {
                    _matchingResetTime = float.Parse(dict["MatchingResetTime"].ToString());
                }
                else
                {
                    Debug.LogErrorFormat("MatchingManager/SetupSettingValue: Failed to get MatchingResetTime.");
                }
            }
            else
            {
                Debug.LogErrorFormat("MatchingManager/SetupSettingValue: Failed to get MatchingValue.");
            }
        }

        private void StartDummyMatch(float minWaitTime, float maxWaitTime, UnityAction onStartMatch)
        {
            if (_dummyMatching != null)
            {
                StopCoroutine(_dummyMatching);
                _dummyMatching = null;
            }

            _dummyMatching = StartCoroutine(IEStartDummyMatch(minWaitTime, maxWaitTime, onStartMatch));
        }

        private IEnumerator IEStartDummyMatch(float minWaitTime, float maxWaitTime, UnityAction onStartMatch)
        {
            _isStartDummyMatching = true;
            LogFeedback("Start Dummy Match ...");
            SetStatusTitle(LocalizationManager.Instance.GetText("MENU_MATCHING_STATUS_CONNECTING"));
            yield return new WaitForSeconds(1.0f);
            SetStatusTitle(LocalizationManager.Instance.GetText("MENU_MATCHING_STATUS_CONNECTED"));
            yield return new WaitForSeconds(0.25f);
            SetStatusTitle(LocalizationManager.Instance.GetText("MENU_MATCHING_STATUS_JOINING_LOBBY"));
            yield return new WaitForSeconds(0.5f);
            SetStatusTitle(LocalizationManager.Instance.GetText("MENU_MATCHING_STATUS_JOINED_LOBBY"));
            yield return new WaitForSeconds(0.25f);
            SetStatusTitle(LocalizationManager.Instance.GetText("MENU_MATCHING_STATUS_MATCHING"));
            _matchingUIManager.ShowCancelButton();
            float matchingTimer = Random.Range(minWaitTime, maxWaitTime);
            _waitTimer = 0.0f;
            _isTimer = true;
            _matchingUIManager.ShowTimer();
            yield return new WaitForSeconds(matchingTimer);

            LogFeedback("Generate MatchData ...");
            _matchingUIManager.HideCancelButton();

            onStartMatch?.Invoke();

            _isStartDummyMatching = false;
            _dummyMatching = null;
            yield break;
        }

        public static bool GetBotLevel(int level, out BotLevel botLevel, out BotLevel deckLevel)
        {
            switch (level)
            {
                case 1:
                botLevel = BotLevel.Easy;
                deckLevel = BotLevel.Easy;
                return true;

                case 2:
                botLevel = BotLevel.Medium;
                deckLevel = BotLevel.Easy;
                return true;

                case 3:
                botLevel = BotLevel.Medium;
                deckLevel = BotLevel.Medium;
                return true;

                case 4:
                botLevel = BotLevel.Hard;
                deckLevel = BotLevel.Medium;
                return true;

                case 5:
                botLevel = BotLevel.Hard;
                deckLevel = BotLevel.Hard;
                return true;

                default:
                botLevel = BotLevel.Hard;
                deckLevel = BotLevel.Hard;
                return false;
            }
        }

        private void StartBotMatch(int level)
        {
            LogFeedback(string.Format("StartBotMatch BotLevel({0}) ...", level));

            BotLevel botLevel = BotLevel.Easy;
            BotLevel deckLevel = BotLevel.Easy;
            GetBotLevel(level, out botLevel, out deckLevel);

            List<MatchPlayerData> matchPlayerDataList = new List<MatchPlayerData>();

            // Local Player
            {
                MatchPlayerData localMatchPlayerData = MatchPlayerData.CreateLocalMatchPlayerData();
                matchPlayerDataList.Add(localMatchPlayerData);
            }

            // Bot
            {
                MatchPlayerData matchBotData = Bot.CreateNewbieBotCasualMatchPlayerData(botLevel, deckLevel);
                matchPlayerDataList.Add(matchBotData);
            }

            MatchProperty matchProperty = new MatchProperty(
                  gameMode: Mode
                , playerCount: 2
                , isNetwork: false
                , isHaveTimer: true
                , isVSBot: true
                , botType: BotType.AIBot
                , botLevel: botLevel
                , adventureStageData: null
            );

            _matchData = new MatchData(matchProperty, matchPlayerDataList);

            StartPlay(_matchData);
        }

        private void StartBotEventBattleMatch()
        {
            LogFeedback(string.Format("StartBotEventBattleMatch ..."));

            List<MatchPlayerData> matchPlayerDataList = new List<MatchPlayerData>();
            List<EventBattleDBData> profileList = null;
            if (EventBattleDB.Instance.GetAllData(out profileList))
            {
                List<EventBattleDBData> currentActiveProfileList = GetCurrentActiveEventBattleProfileList(profileList);

                // Local Player
                {
                    if (EventBattleDB.Instance.DoUsePlayerDeck)
                    {
                        MatchPlayerData playerData;
                        if (EventBattleDB.Instance.DoAddCustomCardsToPlayerDeck)
                        {
                            DeckData currentDeck = DataManager.Instance.GetCurrentDeck();
                            DeckData eventBattleDeck = currentActiveProfileList[0].Deck;
                            DeckData customDeck = new DeckData(
                                  currentDeck.DeckID
                                , currentDeck.DeckName
                                , currentDeck.ElementID
                                , currentDeck.HeroID
                                , currentDeck.CardBackID
                                , eventBattleDeck.HUDSkinID
                                , currentDeck.Deck
                            );

                            foreach (KeyValuePair<string, int> card in eventBattleDeck.Deck)
                            {
                                customDeck.Deck.AddCard(card.Key, card.Value);
                            }

                            playerData = new MatchPlayerData(
                              DataManager.Instance.PlayerInfo
                            , customDeck
                        );
                        }
                        else
                        {
                            DeckData currentDeck = DataManager.Instance.GetCurrentDeck();
                            DeckData eventBattleDeck = currentActiveProfileList[0].Deck;
                            currentDeck.SetHUDSkinID(eventBattleDeck.HUDSkinID); //Override HUD skin
                            playerData = new MatchPlayerData(
                              DataManager.Instance.PlayerInfo
                            , currentDeck
                        );
                        }

                        matchPlayerDataList.Add(playerData);
                    }
                    else
                    {
                        string playerProfileID = "";
                        int playerDeckAmountOffset = Mathf.Clamp(EventBattleDB.Instance.PlayerDecksAmount, 1, currentActiveProfileList.Count - 1);
                        int index = UnityEngine.Random.Range(0, playerDeckAmountOffset); // index [0, n-1]
                        playerProfileID = currentActiveProfileList[index].ProfileID;

                        MatchPlayerData playerData = new MatchPlayerData(
                              DataManager.Instance.PlayerInfo
                            , DataManager.Instance.GetEventBattleDeck(playerProfileID)
                        );

                        matchPlayerDataList.Add(playerData);
                    }
                }

                // Bot
                int indexForBot = 0;
                {
                    MatchPlayerData botData;
                    if (EventBattleDB.Instance.DoUsePlayerDeck)
                    {
                        if (EventBattleDB.Instance.DoAddCustomCardsToPlayerDeck)
                        {
                            indexForBot = UnityEngine.Random.Range(1, currentActiveProfileList.Count);
                        }
                        else
                        {
                            indexForBot = UnityEngine.Random.Range(0, currentActiveProfileList.Count);
                        }
                    }
                    else
                    {
                        int playerDeckAmountOffset = Mathf.Clamp(EventBattleDB.Instance.PlayerDecksAmount, 1, currentActiveProfileList.Count - 1);
                        indexForBot = UnityEngine.Random.Range(playerDeckAmountOffset, currentActiveProfileList.Count);
                    }

                    botData = Bot.CreateBotEventBattleMatchPlayerData(currentActiveProfileList[indexForBot]);
                    matchPlayerDataList.Add(botData);
                }

                MatchProperty matchProperty = new MatchProperty(
                      gameMode: Mode
                    , playerCount: 2
                    , isNetwork: false
                    , isHaveTimer: false
                    , isVSBot: true
                    , botType: BotType.AIBot
                    , botLevel: currentActiveProfileList[indexForBot].BotLevel
                    , adventureStageData: null
                );

                _matchData = new MatchData(matchProperty, matchPlayerDataList);

                StartPlay(_matchData);
            }
            else
            {
                LogFeedback("Failed to generate MatchData ...");
                OnClickCancel();
            }
        }

        private List<EventBattleDBData> GetCurrentActiveEventBattleProfileList(List<EventBattleDBData> profileList)
        {
            System.DateTime now = DateTimeData.GetDateTimeUTC();
            List<EventBattleDBData> currentActiveProfileList = new List<EventBattleDBData>();
            foreach (var battleDBData in profileList)
            {
                if (battleDBData.IsValidDate(now))
                {
                    currentActiveProfileList.Add(battleDBData);
                }
            }

            //Fail safe
            if (currentActiveProfileList.Count == 0)
            {
                currentActiveProfileList = profileList;
            }

            return currentActiveProfileList;
        }

        //private void StartBattleEvent_VSBot()
        //{
        //    if (_dummyMatching != null)
        //    {
        //        StopCoroutine(_dummyMatching);
        //        _dummyMatching = null;
        //    }

        //    _dummyMatching = StartCoroutine(IEStartDummyMatch(0.0f, 0.1f, StartBotEventBattleMatch));
        //}

        //private IEnumerator IEStartEventBattle()
        //{
        //    _isStartDummyMatching = true;
        //    LogFeedback("Start Dummy Match ...");
        //    SetStatusTitle(LocalizationManager.Instance.GetText("MENU_MATCHING_STATUS_CONNECTING"));
        //    yield return new WaitForSeconds(0.05f);
        //    SetStatusTitle(LocalizationManager.Instance.GetText("MENU_MATCHING_STATUS_CONNECTED"));
        //    yield return new WaitForSeconds(0.05f);
        //    SetStatusTitle(LocalizationManager.Instance.GetText("MENU_MATCHING_STATUS_JOINING_LOBBY"));
        //    yield return new WaitForSeconds(0.05f);
        //    SetStatusTitle(LocalizationManager.Instance.GetText("MENU_MATCHING_STATUS_JOINED_LOBBY"));
        //    yield return new WaitForSeconds(0.05f);
        //    SetStatusTitle(LocalizationManager.Instance.GetText("MENU_MATCHING_STATUS_MATCHING"));
        //    _matchingUIManager.ShowCancelButton();
        //    _waitTimer = 0.0f;
        //    _isTimer = true;
        //    _matchingUIManager.ShowTimer();
        //    yield return new WaitForSeconds(0.05f);

        //    LogFeedback("Generate MatchData ...");
        //    List<MatchPlayerData> matchPlayerDataList = new List<MatchPlayerData>();
        //    _matchingUIManager.HideCancelButton();

        //    List<EventBattleDBData> profileList = null;
        //    if (EventBattleDB.Instance.GetAllData(out profileList))
        //    {
        //        // Local Player
        //        {
        //            if (EventBattleDB.Instance.DoUsePlayerDeck)
        //            {
        //                MatchPlayerData playerData;
        //                if (EventBattleDB.Instance.DoAddCustomCardsToPlayerDeck)
        //                {
        //                    DeckData currentDeck = DataManager.Instance.GetCurrentDeck();
        //                    DeckData eventBattleDeck = DataManager.Instance.GetEventBattleDeck("EVP001");
        //                    DeckData customDeck = new DeckData(currentDeck.DeckID, currentDeck.DeckName, currentDeck.ElementID, currentDeck.HeroID, currentDeck.CardBackID, eventBattleDeck.HUDSkinID, currentDeck.Deck);
        //                    foreach (KeyValuePair<string, int> card in eventBattleDeck.Deck)
        //                    {
        //                        customDeck.Deck.AddCard(card.Key, card.Value);
        //                    }

        //                    playerData = new MatchPlayerData(
        //                      DataManager.Instance.PlayerInfo
        //                    , customDeck
        //                );
        //                }
        //                else
        //                {
        //                    playerData = new MatchPlayerData(
        //                      DataManager.Instance.PlayerInfo
        //                    , DataManager.Instance.GetCurrentDeck()
        //                );
        //                }

        //                matchPlayerDataList.Add(playerData);
        //            }
        //            else
        //            {
        //                string playerProfileID = "";
        //                int index = UnityEngine.Random.Range(0, profileList.Count - 1); // index [0, n-1]
        //                playerProfileID = profileList[index].ProfileID;

        //                MatchPlayerData playerData = new MatchPlayerData(
        //                      DataManager.Instance.PlayerInfo
        //                    , DataManager.Instance.GetEventBattleDeck(playerProfileID)
        //                );

        //                matchPlayerDataList.Add(playerData);
        //            }
        //        }

        //        // Bot
        //        {
        //            MatchPlayerData botData = Bot.CreateBotEventBattleMatchPlayerData(profileList[profileList.Count - 1]); // last index
        //            matchPlayerDataList.Add(botData);
        //        }

        //        MatchProperty matchProperty = new MatchProperty(
        //              gameMode: Mode
        //            , playerCount: 2
        //            , isNetwork: false
        //            , isHaveTimer: false
        //            , isVSBot: true
        //            , botType: BotType.AIBot
        //            , botLevel: BotLevel.Hard
        //            , adventureStageData: null
        //        );

        //        _matchData = new MatchData(matchProperty, matchPlayerDataList);

        //        StartPlay(_matchData);

        //        _isStartDummyMatching = false;
        //        _dummyMatching = null;
        //        yield break;
        //    }
        //    else
        //    {
        //        LogFeedback("Failed to generate MatchData ...");
        //        OnClickCancel();
        //    }

        //    yield break;
        //}

        public void SetPlayerName(string name)
        {
            // #Important
            if (string.IsNullOrEmpty(name))
            {
                //Debug.LogError("Player Name is null or empty");
                return;
            }

            PhotonNetwork.NickName = name;
            //DataManager.Instance.SetPlayerDisplayName(name);

            LogFeedback("SetPlayerName " + name);
        }

        public void SetStatusTitle(string text)
        {
            if (_isUpdateStatusTitle)
            {
                _matchingUIManager.SetStatusTitle(text);
            }
        }

        public int GetMyMMR()
        {
            return Mathf.CeilToInt(DataManager.Instance.PlayerInfo.PlayerMMR.CurrentMMRValue);
        }

        public int GetMyNewbieCasualScore()
        {
            return DataManager.Instance.PlayerInfo.GetNewbieCasualScore();
        }

        public string CreateModeLobbyFilter(string gameVersion, GameMode mode)
        {
            return string.Format("((C0 = \"{0}\") AND (C1 = \"{1}\"))", gameVersion, mode);
        }

        public string CreateMMRLobbyFilter(int minMMR, int maxMMR)
        {
            return string.Format("(C2 >={0} AND C2 <={1})", minMMR, maxMMR);
        }

        public string CreateRankLobbyFilter(int minRank, int maxRank)
        {
            return string.Format("(C3 >={0} AND C3 <={1})", minRank, maxRank);
        }

        public string CreateExcludePlayerNameLobbyFilter(string playerName)
        {
            return string.Format("(C4 != \"{0}\")", playerName);
        }

        public string CreateNSScoreLobbyFilter(int minNS, int maxNS)
        {
            return string.Format("(C5 >={0} AND C5 <={1})", minNS, maxNS);
        }

        public void JoinRandomRoom(int retryIndex)
        {
            // C0 : Game Version
            // C1 : Game Mode
            // C2 : MMR
            // C3 : Rank Index
            // C4 : Player Name
            // C5 : NS Score

            LogFeedback(string.Format("JoinRandomRoom({0})", retryIndex));

            // Queries can be sent in JoinRandomGame operation.
            // The filtering queries are basically SQL WHERE conditions based on the "C0" .. "C9" values.
            // Find the list of all SQLite supported operators and how to use them link below.
            // https://sqlite.org/lang_expr.html#binaryops

            string sqlLobbyFilter = "";
            switch (Mode)
            {
                case GameMode.Casual:
                {
                    int myMMR = GetMyMMR();
                    int minMMR = myMMR - _scoreOffset[GameMode.Casual][retryIndex];
                    int maxMMR = myMMR + _scoreOffset[GameMode.Casual][retryIndex];

                    if (IsTierNewbie())
                    {
                        PlayerTierType tier = DataManager.Instance.PlayerInfo.GetPlayerTier();

                        if (retryIndex < _scoreOffset_Newbie[tier].Count)
                        {
                            minMMR = _scoreOffset_Newbie[tier][retryIndex][0];
                            maxMMR = _scoreOffset_Newbie[tier][retryIndex][1];
                            int minNSScore = _scoreOffset_Newbie[tier][retryIndex][2];
                            int maxNSScore = _scoreOffset_Newbie[tier][retryIndex][3];

                            sqlLobbyFilter = CreateModeLobbyFilter(this.gameVersion, Mode);

                            if (minMMR >= 0 && maxMMR >= 0)
                            {
                                // Filter by MMR or NSScore.
                                sqlLobbyFilter += string.Format(" AND ({0} OR {1})"
                                    , CreateMMRLobbyFilter(minMMR, maxMMR)
                                    , CreateNSScoreLobbyFilter(minNSScore, maxNSScore)
                                );
                            }
                            else
                            {
                                // Filter by NSScore.
                                sqlLobbyFilter += " AND " + CreateNSScoreLobbyFilter(minNSScore, maxNSScore);
                            }
                        }
                        else
                        {
                            sqlLobbyFilter = CreateModeLobbyFilter(this.gameVersion, Mode);
                        }
                    }
                    else
                    {
                        if (retryIndex < _scoreOffset[GameMode.Casual].Length && _scoreOffset[GameMode.Casual][retryIndex] >= 0)
                        {
                            sqlLobbyFilter = CreateModeLobbyFilter(this.gameVersion, Mode);

                            // Filter by MMR.
                            sqlLobbyFilter += " AND " + CreateMMRLobbyFilter(minMMR, maxMMR);

                            // Filter ignore last match player
                            string lastMatchPlayerName = DataManager.Instance.LastMatchPlayerInfo.DisplayName;
                            if (lastMatchPlayerName != null && lastMatchPlayerName.Length > 0)
                            {
                                sqlLobbyFilter += " AND " + CreateExcludePlayerNameLobbyFilter(lastMatchPlayerName);
                            }
                        }
                        else
                        {
                            // Don't care hidden score.
                            sqlLobbyFilter = CreateModeLobbyFilter(this.gameVersion, Mode);
                        }
                    }
                }
                break;

                case GameMode.Rank:
                {
                    int myRank = DataManager.Instance.GetPlayerRankIndex();
                    int minRank = myRank;
                    int maxRank = myRank;

                    if (IsRankApprentice())
                    {
                        if (retryIndex < _scoreOffset_Appentice[myRank].Count)
                        {
                            minRank = _scoreOffset_Appentice[myRank][retryIndex][0];
                            maxRank = _scoreOffset_Appentice[myRank][retryIndex][1];

                            sqlLobbyFilter = CreateModeLobbyFilter(this.gameVersion, Mode);

                            // Filter by rank.
                            sqlLobbyFilter += " AND " + CreateRankLobbyFilter(minRank, maxRank);

                            // Filter ignore last match player
                            string lastMatchPlayerName = DataManager.Instance.LastMatchPlayerInfo.DisplayName;
                            if (lastMatchPlayerName != null && lastMatchPlayerName.Length > 0)
                            {
                                sqlLobbyFilter += " AND " + CreateExcludePlayerNameLobbyFilter(lastMatchPlayerName);
                            }
                        }
                        else
                        {
                            // Don't care rank.
                            sqlLobbyFilter = CreateModeLobbyFilter(this.gameVersion, Mode);
                        }
                    }
                    else
                    {
                        int[] scoreOffset;

                        if (IsRankBeyond())
                        {
                            scoreOffset = _scoreOffset_Beyond;
                        }
                        else
                        {
                            scoreOffset = _scoreOffset[GameMode.Rank];
                        }

                        if (retryIndex < scoreOffset.Length && scoreOffset[retryIndex] >= 0)
                        {
                            minRank = myRank - scoreOffset[retryIndex];
                            maxRank = myRank + scoreOffset[retryIndex];

                            sqlLobbyFilter = CreateModeLobbyFilter(this.gameVersion, Mode);

                            // Filter by rank.
                            sqlLobbyFilter += " AND " + CreateRankLobbyFilter(minRank, maxRank);

                            // Filter ignore last match player
                            string lastMatchPlayerName = DataManager.Instance.LastMatchPlayerInfo.DisplayName;
                            if (lastMatchPlayerName != null && lastMatchPlayerName.Length > 0)
                            {
                                sqlLobbyFilter += " AND " + CreateExcludePlayerNameLobbyFilter(lastMatchPlayerName);
                            }
                        }
                        else
                        {
                            // Don't care rank.
                            sqlLobbyFilter = CreateModeLobbyFilter(this.gameVersion, Mode);
                        }
                    }
                }
                break;

                default:
                {
                    // general format
                    sqlLobbyFilter = CreateModeLobbyFilter(this.gameVersion, Mode);
                }
                break;
            }

            JoinRandomRoom(sqlLobbyFilter);
        }

        public void JoinRandomRoom(string sqlLobbyFilter)
        {
            // C0 : Game Version
            // C1 : Game Mode
            // C2 : MMR
            // C3 : Rank Index
            // C4 : Player Name
            // C5 : NS Score

            LogFeedback(string.Format("JoinRandomRoom({0})", sqlLobbyFilter));

            // Queries can be sent in JoinRandomGame operation.
            // The filtering queries are basically SQL WHERE conditions based on the "C0" .. "C9" values.
            // Find the list of all SQLite supported operators and how to use them link below.
            // https://sqlite.org/lang_expr.html#binaryops

            _matchingStatus = MatchingStatus.JoiningRoom;
            if (!PhotonNetwork.JoinRandomRoom(
                  null
                , this.maxPlayersPerRoom
                , MatchmakingMode.FillRoom
                , GetLobbyType()
                , sqlLobbyFilter
            ))
            {
                _matchingStatus = MatchingStatus.Idle;
            }
        }

        public void JoinRoom(string roomName)
        {
            LogFeedback(string.Format("Start JoinRoom({0})...", roomName));

            _matchingStatus = MatchingStatus.JoiningRoom;
            if (!PhotonNetwork.JoinRoom(roomName))
            {
                _matchingStatus = MatchingStatus.Idle;
            }
        }

        private string[] CreateCustomRoomPropertiesForLobby()
        {
            return new string[] { "C0", "C1", "C2", "C3", "C4", "C5" };
        }

        private ExitGames.Client.Photon.Hashtable CreateRoomProperties()
        {
            ExitGames.Client.Photon.Hashtable customRoomProperties = new ExitGames.Client.Photon.Hashtable() {
                    { "C0", this.gameVersion }                              // C0 : Game Version
                  , { "C1", Mode.ToString() }                               // C1 : Game Mode
                  , { "C2", GetMyMMR() }                                    // C2 : MMR
                  , { "C3", DataManager.Instance.GetPlayerRankIndex() }     // C3 : Rank Index
                  , { "C4", DataManager.Instance.PlayerInfo.DisplayName }   // C4 : Player Name
                  , { "C5", GetMyNewbieCasualScore() }                                  // C5 : Ns Score
            };

            return customRoomProperties;
        }

        public void CreateRoom()
        {
            string[] customRoomPropertiesForLobby = CreateCustomRoomPropertiesForLobby();
            ExitGames.Client.Photon.Hashtable customRoomProperties = CreateRoomProperties();

            RoomOptions roomOption = new RoomOptions();
            roomOption.MaxPlayers = this.maxPlayersPerRoom;
            roomOption.CustomRoomPropertiesForLobby = customRoomPropertiesForLobby;
            roomOption.CustomRoomProperties = customRoomProperties;
            roomOption.PlayerTtl = PlayerTtl_Matching;
            roomOption.EmptyRoomTtl = EmptyRoomTtl_Matching;
            roomOption.CleanupCacheOnLeave = false;
            roomOption.IsVisible = (Mode != GameMode.Friendly); // Hide this room, If this room play in Friendly mode.

            System.DateTime dateTime = DateTimeData.GetDateTimeUTC();
            string roomName = string.Format("{0}_{1}", PhotonNetwork.NickName, dateTime.ToString("yyyyMMdd_HHmmss"));

            _matchingStatus = MatchingStatus.CreatingRoom;
            if (!PhotonNetwork.CreateRoom(
                  roomName
                , roomOption
                , GetLobbyType()
            ))
            {
                _matchingStatus = MatchingStatus.Idle;
            }
        }

        public void LeaveRoom(bool isAutoReconnect, UnityAction onLeaveRoomComplete)
        {
            _isRequestReconnect = isAutoReconnect;
            _onLeaveRoomComplete = onLeaveRoomComplete;

            _matchingStatus = MatchingStatus.LeavingRoom;
            if (!PhotonNetwork.LeaveRoom(false))
            {
                _matchingStatus = MatchingStatus.Idle;
                _onLeaveRoomComplete?.Invoke();
            }
        }

        public void StartPlay(MatchData matchData)
        {
            LogFeedback("StartPlay()");

            StopTimer();
            _matchingUIManager.HideCancelButton();
            SetStatusTitle(LocalizationManager.Instance.GetText("MENU_MATCHING_STATUS_STARTING"));

            MatchDataContainer container;
            MatchData.CreateMatchContainer(matchData, out container);
            DontDestroyOnLoad(container.gameObject);

            Debug.Log("1: " + matchData.GetDebugText());

            if (matchData.Property.IsNetwork)
            {
                // change room properties
                PhotonNetwork.CurrentRoom.PlayerTtl = PlayerTtl;
                PhotonNetwork.CurrentRoom.EmptyRoomTtl = EmptyRoomTtl;

                // VS Player.
                PhotonNetwork.CurrentRoom.IsVisible = false;    // Hide this room from room list.
                PhotonNetwork.CurrentRoom.IsOpen = true;        // Open for re-join.
                DataManager.Instance.SetRoomName(PhotonNetwork.CurrentRoom.Name);

                PopupUIManager.Instance.Show_FullLoad(true);
                if (PhotonNetwork.IsMasterClient)
                {
                    // Load battle scene. 
                    LoadScene(
                         GameHelper.BattleSceneName
                       , LoadSceneMode.Single
                       , true
                       , null // loading popup will close after battle scene init finish
                   );
                }
                else
                {
                    //TODO: remove something before auto load scene.
                    NavigatorController.Instance.ClearAndExitCurrentPage();
                    NavigatorController.Instance.RemoveAllSwitchScenable();
                }
            }
            else
            {
                // VS Bot.
                DataManager.Instance.SetRoomName(string.Format("LocalRoom_{0}", DateTimeData.GetDateTimeUTC().ToString("yyyyMMddHHmmss")));

                // Load battle scene.
                PopupUIManager.Instance.Show_FullLoad(true);
                LoadScene(
                      GameHelper.BattleSceneName
                    , LoadSceneMode.Single
                    , true
                    , null // loading popup will close after battle scene init finish
                );
            }

            if (GameHelper.IsHaveObject(typeof(EventBattleManager)))
            {
                EventBattleManager.Instance.ExitEventBattle();
            }
        }

        public void StartPlayWithBot()
        {
            Debug.Log("StartPlayWithBot !!");

            List<MatchPlayerData> matchPlayerDataList = new List<MatchPlayerData>();

            // Local Player
            {
                MatchPlayerData localMatchPlayerData = MatchPlayerData.CreateLocalMatchPlayerData();
                matchPlayerDataList.Add(localMatchPlayerData);
            }

            // คำนวนความยากของ Bot จากคะแนน HiddenScore ของผู้เล่น
            BotLevel botLevel = BotLevel.Easy;
            int myScore = GetMyMMR();

            { 
                foreach (BotLevel level in System.Enum.GetValues(typeof(BotLevel)))
                {
                    if (_matchBotHiddenScore.ContainsKey(level))
                    {
                        if (myScore >= _matchBotHiddenScore[level][0] && myScore <= _matchBotHiddenScore[level][1])
                        {
                            botLevel = level;
                            break;
                        }
                    }
                }

                MatchPlayerData matchBotData = Bot.CreateCasualBotMatchPlayerData(botLevel);
                matchPlayerDataList.Add(matchBotData);
            }

            MatchProperty matchProperty = new MatchProperty(
                  gameMode: Mode
                , playerCount: 2
                , isNetwork: false
                , isHaveTimer: true
                , isVSBot: true
                , botType: BotType.AIBot
                , botLevel: botLevel
                , adventureStageData: null
            );

            _matchData = new MatchData(matchProperty, matchPlayerDataList);

            StartPlay(_matchData);
        }

        public void StartTimer(int retryIndex)
        {
            _timer = 0.0f;

            switch (Mode)
            {
                case GameMode.Rank:
                {
                    if (IsRankBeyond())
                    {
                        if (_matchingTime_Beyond != null && retryIndex < _matchingTime_Beyond.Length)
                        {
                            _matchingTimeout = _matchingTime_Beyond[retryIndex];
                        }
                        else
                        {
                            _matchingTimeout = -1.0f; // เวลาติดลบหมายถึง ไม่มีหมดเวลา
                        }
                    }
                    else if (IsRankApprentice())
                    {
                        int rankIndex = DataManager.Instance.PlayerInfo.GetPlayerCurrentRank();
                        if (_matchingTime_Appentice != null && retryIndex < _matchingTime_Appentice[rankIndex].Length)
                        {
                            _matchingTimeout = _matchingTime_Appentice[rankIndex][retryIndex];
                        }
                        else
                        {
                            _matchingTimeout = -1.0f; // เวลาติดลบหมายถึง ไม่มีหมดเวลา
                        }
                    }
                    else
                    {
                        if (_matchingTime != null && _matchingTime.ContainsKey(Mode) && retryIndex < _matchingTime[Mode].Length)
                        {
                            _matchingTimeout = _matchingTime[Mode][retryIndex];
                        }
                        else
                        {
                            _matchingTimeout = -1.0f; // เวลาติดลบหมายถึง ไม่มีหมดเวลา
                        }
                    }
                }
                break;

                case GameMode.Casual:
                {
                    if (IsTierNewbie())
                    {
                        PlayerTierType tier = DataManager.Instance.PlayerInfo.GetPlayerTier();
                        if (_matchingTime != null && retryIndex < _matchingTime_Newbie[tier].Length)
                        {
                            _matchingTimeout = _matchingTime_Newbie[tier][retryIndex];
                        }
                        else
                        {
                            _matchingTimeout = -1.0f; // เวลาติดลบหมายถึง ไม่มีหมดเวลา
                        }
                    }
                    else
                    {
                        if (_matchingTime != null && _matchingTime.ContainsKey(Mode) && retryIndex < _matchingTime[Mode].Length)
                        {
                            _matchingTimeout = _matchingTime[Mode][retryIndex];
                        }
                        else
                        {
                            _matchingTimeout = -1.0f; // เวลาติดลบหมายถึง ไม่มีหมดเวลา
                        }
                    }
                }
                break;

                default:
                {
                    if (_matchingTime != null && _matchingTime.ContainsKey(Mode) && retryIndex < _matchingTime[Mode].Length)
                    {
                        _matchingTimeout = _matchingTime[Mode][retryIndex];
                    }
                    else
                    {
                        _matchingTimeout = -1.0f; // เวลาติดลบหมายถึง ไม่มีหมดเวลา
                    }
                }
                break;

            }

            _matchingUIManager.ShowTimer();

            _isTimer = true;
            _isTimeout = false;
        }

        public void StopTimer()
        {
            _isTimer = false;
            _isTimeout = false;

            _matchingUIManager.HideTimer();
        }

        public void ExitFriendMatching()
        {
            DataManager.Instance.ClearFriendMatchRequest(
                  () => { ExitMatching(); }
                , (err) => { ExitMatching(); }
            );
        }

        public void ExitMatching(bool isCallDisconnect = true)
        {
            if (isCallDisconnect)
            {
                if (_matchingStatus == MatchingStatus.Disconnecting)
                {
                    Debug.Log("ExitMatching: Reject this ExitMatching..");
                    return;
                }
            }

            _isTimer = false;
            PopupUIManager.Instance.Show_SoftLoad(true);
            if (Mode == GameMode.Event)
            {
                SoundManager.PlayEventBGM(1.5f);
            }
            else
            {
                SoundManager.PlayBGM(SoundManager.BGM.Menu, 3.0f);
            }

            if (_matchingUIManager != null)
            {
                _matchingUIManager.HideCanvas();
            }

            _isRequestReconnect = false;
            UnityAction callback = delegate ()
            {
                // already load menu scene. 
                GameHelper.UnLoadSceneAsync(
                      GameHelper.MatchingSceneName
                    , delegate ()
                    {
                        PopupUIManager.Instance.Show_SoftLoad(false);
                        OnExit?.Invoke();
                    }
                );
            };

            if (isCallDisconnect)
            {
                Disconnect(callback);
            }
            else
            {
                callback.Invoke();
            }
        }

        public void OnClickCancel()
        {
            LogFeedback("MatchingManager: OnClickCancel");
            SetStatusTitle(LocalizationManager.Instance.GetText("MENU_MATCHING_STATUS_LEAVING"));
            _retryIndex = 0;
            _isTimer = false;

            if (_dummyMatching != null || _isStartDummyMatching)
            {
                // Newbie & Loser Tier Matching

                PopupUIManager.Instance.Show_SoftLoad(true);

                if (_dummyMatching != null)
                {
                    StopCoroutine(_dummyMatching);
                    _dummyMatching = null;
                }
                _isStartDummyMatching = false;

                PopupUIManager.Instance.Show_SoftLoad(false);
                ExitMatching();
            }
            else
            {
                // Player Tier Matching

                if (Mode == GameMode.Friendly)
                {
                    if (_matchingStatus == MatchingStatus.InRoom)
                    {
                        UnityAction action = delegate ()
                        {
                            PopupUIManager.Instance.Show_SoftLoad(true);
                            LeaveRoom(
                                  false
                                , delegate ()
                                {
                                    Disconnect(
                                        delegate ()
                                        {
                                            PopupUIManager.Instance.Show_SoftLoad(false);
                                            ExitMatching();
                                        }
                                    );
                                }
                            );
                        };

                        DataManager.Instance.ClearFriendMatchRequest(
                              action
                            , delegate (string error)
                            {
                                action?.Invoke();
                            }
                        );

                        return;
                    }
                    else
                    {
                        PopupUIManager.Instance.Show_SoftLoad(true);

                        DataManager.Instance.ClearFriendMatchRequest(
                            delegate ()
                            {
                                PopupUIManager.Instance.Show_SoftLoad(false);
                                ExitMatching();
                            }
                            , delegate (string error)
                            {
                                PopupUIManager.Instance.Show_SoftLoad(false);
                                ExitMatching();
                            }
                        );
                    }
                }
                else
                {
                    if (_matchingStatus == MatchingStatus.InRoom)
                    {
                        LeaveRoom(
                              false
                            , delegate ()
                            {
                                ExitMatching();
                                PopupUIManager.Instance.Show_SoftLoad(false);
                            }
                        );

                        return;
                    }
                    else
                    {
                        ExitMatching();
                        PopupUIManager.Instance.Show_SoftLoad(false);
                    }
                }
            }
        }

        private void OnMatchingTimeout()
        {
            Debug.Log("OnMatchingTimeout");

            if (!_isTimeout && !_isMatched)
            {
                int nextRetryIndex = _retryIndex + 1;

                switch (Mode)
                {
                    case GameMode.Casual:
                    {
                        if (IsTierNewbie())
                        {
                            PlayerTierType myTier = DataManager.Instance.PlayerInfo.GetPlayerTier();
                            if (nextRetryIndex < _scoreOffset_Newbie[myTier].Count)
                            {
                                AttemptFindingOpponent(nextRetryIndex);
                            }
                            else
                            {
                                switch (myTier)
                                {
                                    case PlayerTierType.Newbie1:
                                    {
                                        StartMatchWithCustomCallback(
                                            delegate ()
                                            {
                                                StartBotMatch(1); // vs bot level 1
                                            }
                                        );
                                    }
                                    break;

                                    case PlayerTierType.Newbie2:
                                    {
                                        StartMatchWithCustomCallback(
                                            delegate ()
                                            {
                                                StartBotMatch(2); // vs bot level 2
                                            }
                                        );
                                    }
                                    break;

                                    case PlayerTierType.Newbie3:
                                    {
                                        StartMatchWithCustomCallback(
                                            delegate ()
                                            {
                                                StartBotMatch(3); // vs bot level 3
                                            }
                                        );
                                    }
                                    break;

                                    case PlayerTierType.Newbie4:
                                    {
                                        StartMatchWithCustomCallback(
                                            delegate ()
                                            {
                                                StartBotMatch(4); // vs bot level 4
                                            }
                                        );
                                    }
                                    break;

                                    case PlayerTierType.Newbie5:
                                    {
                                        StartMatchWithCustomCallback(
                                            delegate ()
                                            {
                                                StartBotMatch(5); // vs bot level 5
                                            }
                                        );
                                    }
                                    break;

                                    case PlayerTierType.Newbie6:
                                    {
                                        StartMatchWithCustomCallback(
                                            delegate ()
                                            {
                                                StartBotMatch(5); // vs bot level 5
                                            }
                                        );
                                    }
                                    break;
                                }
                            }
                        }
                        else
                        {
                            if (nextRetryIndex < _scoreOffset[GameMode.Casual].Length)
                            {
                                // Can attempt next retry.
                                AttemptFindingOpponent(nextRetryIndex);
                            }
                            else
                            {
                                _retryIndex = nextRetryIndex;

                                // Start matching with bot.
                                StartMatchWithBot();
                            }
                        }
                    }
                    break;

                    case GameMode.Rank:
                    {
                        if (IsRankApprentice())
                        {
                            int myRank = DataManager.Instance.PlayerInfo.GetPlayerCurrentRank();
                            if (nextRetryIndex < _scoreOffset_Appentice[myRank].Count)
                            {
                                AttemptFindingOpponent(nextRetryIndex);
                            }
                            else
                            {
                                switch (myRank)
                                {
                                    case -8: // Apprentice 4
                                    {
                                        StartMatchWithCustomCallback(
                                            delegate ()
                                            {
                                                StartBotMatch(1); // vs bot level 1
                                            }
                                        );
                                    }
                                    break;
                                     
                                    case -7: // Apprentice 3
                                    {
                                        StartMatchWithCustomCallback(
                                            delegate ()
                                            {
                                                StartBotMatch(2); // vs bot level 2
                                            }
                                        );
                                    }
                                    break;

                                    case -6: // Apprentice 2 - 0 star
                                    case -5: // Apprentice 2 - 1 star
                                    case -4: // Apprentice 2 - 2 stars
                                    {
                                        StartMatchWithCustomCallback(
                                            delegate ()
                                            {
                                                StartBotMatch(3); // vs bot level 3
                                            }
                                        );
                                    }
                                    break;

                                    case -3: // Apprentice 1 - 0 star
                                    case -2: // Apprentice 1 - 1 star
                                    case -1: // Apprentice 1 - 2 stars
                                    {
                                        StartMatchWithCustomCallback(
                                            delegate ()
                                            {
                                                StartBotMatch(4); // vs bot level 4
                                            }
                                        );
                                    }
                                    break;
                                }
                            }
                        }
                        else
                        {
                            if (nextRetryIndex < _scoreOffset[GameMode.Rank].Length)
                            {
                                // Can attempt next retry.
                                AttemptFindingOpponent(nextRetryIndex);
                            }
                            else
                            {
                                _retryIndex = nextRetryIndex;

                                // Start matching with bot.
                                StartMatchWithBot();
                            }
                        }
                    }
                    break;
                }
            }
        }

        private void AttemptFindingOpponent(int nextRetryIndex)
        {
            // Can attempt next retry.

            if (_matchingStatus == MatchingStatus.InRoom)
            {
                _isTimeout = true;
                _isUpdateStatusTitle = false;

                LeaveRoom(
                      true
                    , delegate ()
                    {
                        _retryIndex = nextRetryIndex;
                        _isResetTimer = true;
                    }
                );
            }
            else if (!IsBusy())
            {
                _isTimeout = true;
                _isUpdateStatusTitle = false;

                _isResetTimer = true;
                StartFindingOpponent(nextRetryIndex, true);
            }
        }

        private void StartMatchWithBot()
        {
            // Start matching with bot.
            StartMatchWithCustomCallback(StartPlayWithBot);
        }

        private void StartMatchWithCustomCallback(UnityAction callback)
        {
            // Start matching with bot.

            _isUpdateStatusTitle = true;
            SetStatusTitle(LocalizationManager.Instance.GetText("MENU_MATCHING_STATUS_MATCHED"));
            _isUpdateStatusTitle = false;

            _isMatched = true;
            _isTimeout = true;
            _matchingUIManager.HideCancelButton();

            UnityAction action = delegate ()
            {
                Disconnect(
                    delegate ()
                    {
                        callback?.Invoke();
                    }
                );
            };

            if (_matchingStatus == MatchingStatus.InRoom)
            {
                LeaveRoom(false, action);
            }
            else
            {
                action?.Invoke();
            }
        }

        private void OnRetryMatching()
        {
            Debug.Log("OnRetryMatching");

            _isUpdateStatusTitle = false;
            _isResetTimer = false;
            _roomTimer = float.MaxValue;

            // Can attempt next retry.
            if (_matchingStatus == MatchingStatus.InRoom)
            {
                LeaveRoom(true, null);
            }
            else
            {
                StartFindingOpponent(_retryIndex, false);
            }
        }

        private void OnReInitMatching()
        {
            Debug.Log("OnReInitMatching");

            _isRequestReconnect = false;
            _isMatched = true;
            _isTimeout = true;
            _isUpdateStatusTitle = false;

            Disconnect(
                delegate ()
                {
                    Debug.Log("ReInit Matching...");
                    _matchingStatus = MatchingStatus.Init;

                    GameHelper.DelayCallback(
                          1.0f
                        , delegate ()
                        {
                            _isTimer = true;

                            _isRequestReconnect = true;
                            _isMatched = false;
                            _isTimeout = false;

                            _retryIndex = 0;
                            _isResetTimer = true;
                            _roomTimer = 0.0f;
                            _timer = 0.0f;
                            _matchingTimeoutTimer = 0.0f;

                            ConnectWithSetting(_matchSetting.Region);
                        }
                    );

                    /*
                    PopupUIManager.Instance.ShowPopup_Error(
                          LocalizationManager.Instance.GetText("ERROR_POPUP_HEADER")
                        , LocalizationManager.Instance.GetText("ERROR_MATCHING_TIMEOUT")
                        , ExitMatching
                    );
                    */
                }
            );
        }

        private TypedLobby GetLobbyType()
        {
            //return TypedLobby.Default;

            return (new TypedLobby(string.Format("{0} Lobby", Mode), LobbyType.SqlLobby));
        }

        private void LoadScene(string sceneName, LoadSceneMode loadSceneMode, bool isAsync, UnityAction onComplete)
        {
            if (loadSceneMode == LoadSceneMode.Single)
            {
                NavigatorController.Instance.ClearAndExitCurrentPage();
                NavigatorController.Instance.RemoveAllSwitchScenable();
            }

            if (isAsync)
            {
                PopupUIManager.Instance.Show_FullLoad(true);
                GameHelper.LoadSceneAsync(sceneName, loadSceneMode, onComplete, PopupUIManager.Instance.SetLoadingProgress);

                //StartCoroutine(LoadingSceneAsync(sceneName, loadSceneMode, onComplete));
            }
            else
            {
                SceneManager.LoadScene(sceneName, loadSceneMode);
            }
        }

        /*
        private IEnumerator LoadingSceneAsync(string sceneName, LoadSceneMode loadSceneMode, UnityAction onComplete)
        {
            LogFeedback(string.Format("Loading scene {0}", sceneName));

            yield return new WaitForSeconds(0.5f);
            AsyncOperation asyncLoadUI = SceneManager.LoadSceneAsync(sceneName, loadSceneMode);

            // Wait until the asynchronous scene fully loads
            while (!asyncLoadUI.isDone)
            {
                _matchingUIManager.SetLoadProgress(asyncLoadUI.progress);

                yield return null;
            }

            if (onComplete != null)
            {
                onComplete.Invoke();
            }
        }
        */

        void LogFeedback(string message, bool isAdd = true)
        {
            if (GameHelper.IsATNVersion_RealDevelopment)
            {
                if (_logList == null)
                {
                    _logList = new List<string>();
                }

                if (!isAdd)
                {
                    _logList.Clear();
                }

                if (message != null && message.Length > 0)
                {
                    Debug.Log(message);
                    _logList.Add(message);
                }
                if (_logList.Count > 12)
                {
                    _logList.RemoveAt(0);
                }
            }
        }

        private void StartFindingOpponent(int retryIndex, bool isResetTimer)
        {
            _retryIndex = retryIndex;

            if (
                   (Mode == GameMode.Casual)
                || (Mode == GameMode.Rank)
            )
            {
                LogFeedback(string.Format("StartFindingOpponent(Mode:{0}, retryIndex:{1})", Mode, retryIndex));

                if (isResetTimer)
                {
                    StartTimer(_retryIndex);
                }

                JoinRandomRoom(_retryIndex);
            }
            else
            {
                if (isResetTimer)
                {
                    StartTimer(0);
                }

                JoinRandomRoom(0);
            }
        }

        public bool CheckFriendRoomValid(FriendMatchData invitee)
        {
            return (_matchSetting.FriendPlayfabID == invitee.PlayerID);
        }

        public bool IsHost()
        {
            if (PhotonNetwork.IsConnected)
            {
                return PhotonNetwork.IsMasterClient && (PhotonNetwork.CurrentRoom != null);
            }

            return false;
        }

        public bool IsRankBeyond()
        {
            return (GameHelper.GetRankID(DataManager.Instance.PlayerInfo.GetPlayerCurrentRank()) == "RK088");
        }

        public bool IsRankApprentice()
        {
            return DataManager.Instance.PlayerInfo.IsBeApprentice();
        }

        public bool IsTierNewbie()
        {
            return DataManager.Instance.PlayerInfo.IsBeNewbieCasual();
        }

        public int GetRoomPlayerCount()
        {
            if (PhotonNetwork.IsConnected)
            {
                if (PhotonNetwork.CurrentRoom != null)
                {
                    return PhotonNetwork.CurrentRoom.PlayerCount;
                }
            }

            return 0;
        }

        private PlayerIndex GetMyPlayerIndex()
        {
            if (IsHost())
            {
                return PlayerIndex.One;
            }
            else
            {
                return PlayerIndex.Two;
            }
        }

        private PlayerIndex GetOpponentPlayerIndex()
        {
            if (GetMyPlayerIndex() == PlayerIndex.One)
            {
                return PlayerIndex.Two;
            }
            else
            {
                return PlayerIndex.One;
            }
        }

        public bool IsJoiningToRoom()
        {
            if (
                   (PhotonNetwork.NetworkClientState == ClientState.ConnectedToMasterServer)
                || (PhotonNetwork.NetworkClientState == ClientState.ConnectingToGameServer)
                || (PhotonNetwork.NetworkClientState == ClientState.ConnectingToNameServer)
                || (PhotonNetwork.NetworkClientState == ClientState.ConnectedToMasterServer)
                || (PhotonNetwork.NetworkClientState == ClientState.ConnectedToGameServer)
                || (PhotonNetwork.NetworkClientState == ClientState.ConnectedToNameServer)
                || (PhotonNetwork.NetworkClientState == ClientState.JoiningLobby)
                || (PhotonNetwork.NetworkClientState == ClientState.JoinedLobby)
                || (PhotonNetwork.NetworkClientState == ClientState.Joining)
            )
            {
                return true;
            }

            return false;
        }

        private void SetFriendReady(PlayerIndex playerIndex, bool isReady)
        {
            if (_friendInfoList != null && _friendInfoList.Count > 0)
            {
                if (_friendInfoList.ContainsKey(playerIndex))
                {
                    FriendMatchPlayerInfo playerInfo = _friendInfoList[playerIndex];
                    playerInfo.IsReady = isReady;
                    _friendInfoList[playerIndex] = playerInfo;
                }
            }
        }

        public bool IsBusy()
        {
            switch (_matchingStatus)
            {

                case MatchingStatus.Connecting:
                case MatchingStatus.Disconnecting:
                case MatchingStatus.JoiningLobby:
                case MatchingStatus.JoiningRoom:
                case MatchingStatus.CreatingRoom:
                case MatchingStatus.LeavingRoom:
                    {
                        return true;
                    }
            }

            return false;
        }

        public static string GetServerRegion(PhotonRegion region)
        {
            switch (region)
            {
                case PhotonRegion.Singapore: return "asia";
                case PhotonRegion.Melbourne: return "au";
                case PhotonRegion.Montreal: return "cae";
                case PhotonRegion.Amsterdam: return "eu";
                case PhotonRegion.Chennai: return "in";
                case PhotonRegion.Tokyo: return "jp";
                case PhotonRegion.Moscow: return "ru";
                case PhotonRegion.Khabarovsk: return "rue";
                case PhotonRegion.SaoPaulo: return "sa";
                case PhotonRegion.Seoul: return "kr";
                case PhotonRegion.Washington: return "us";
                case PhotonRegion.SanJose: return "usw";
                default: return "auto";
            }
        }

        public static PhotonRegion GetServerRegion(string regionStr)
        {
            if (regionStr != null && regionStr.Length > 0)
            {
                switch (regionStr.ToLower())
                {
                    case "asia": return PhotonRegion.Singapore;
                    case "au": return PhotonRegion.Melbourne;
                    case "cae": return PhotonRegion.Montreal;
                    case "eu": return PhotonRegion.Amsterdam;
                    case "in": return PhotonRegion.Chennai;
                    case "jp": return PhotonRegion.Tokyo;
                    case "ru": return PhotonRegion.Moscow;
                    case "rue": return PhotonRegion.Khabarovsk;
                    case "sa": return PhotonRegion.SaoPaulo;
                    case "kr": return PhotonRegion.Seoul;
                    case "us": return PhotonRegion.Washington;
                    case "usw": return PhotonRegion.SanJose;

                    default: return PhotonRegion.None;
                }
            }

            return PhotonRegion.None;
        }
        #endregion

        #region Lobby Event Methods
        private void OnHostFriendRoomReady()
        {
            PopupUIManager.Instance.Show_MidSoftLoad(true);

            if (!string.IsNullOrEmpty(_matchSetting.FriendPlayfabID))
            {
                // Friendly Match Real Host
                PhotonRegion region = GetServerRegion(PhotonNetwork.CloudRegion);
                string roomName = PhotonNetwork.CurrentRoom.Name;
                string friendPlayfabID = _matchSetting.FriendPlayfabID;

                // Start send message to invite friend into room.
                InviteFriendModeRequest request = new InviteFriendModeRequest(friendPlayfabID, region, roomName);

                DataManager.Instance.InviteFriendModeRequest(
                      request
                    , delegate ()
                    {
                        Debug.LogFormat("Invite sent to {0}.", friendPlayfabID);
                        PopupUIManager.Instance.Show_MidSoftLoad(false);
                    }
                    , delegate (string errorMsg)
                    {
                        Debug.LogFormat("Invite send failed to {0}. {1}", friendPlayfabID, errorMsg);

                        PopupUIManager.Instance.ShowPopup_Error(
                            LocalizationManager.Instance.GetText("FRIENDLY_MATCH_TITLE")
                            , LocalizationManager.Instance.GetText("ERROR_FRIENDLY_MATCH_INVITE_FAIL")
                            , delegate ()
                            {
                                HomeManager.Instance.HideSelectDeck();
                            }
                        );

                        /*
                        NotificationController.Instance.ShowUI(
                              FeedbackUI.Status.Fail
                            , "Fail to send invite :D"
                            , string.Format("Invite send failed to {0}", friendPlayfabID)
                            , delegate ()
                            {
                                HomeManager.Instance.HideSelectDeck();
                                NotificationController.Instance.HideUI();
                            }
                        );
                        */

                        PopupUIManager.Instance.Show_MidSoftLoad(false);
                        ExitMatching();
                    }
                );
            }
            else
            {
                PopupUIManager.Instance.ShowPopup_Error(
                      LocalizationManager.Instance.GetText("FRIENDLY_MATCH_TITLE")
                    , LocalizationManager.Instance.GetText("ERROR_FRIENDLY_MATCH_ROOM_INVALID")
                    , delegate ()
                    {
                        HomeManager.Instance.HideSelectDeck();
                        PopupUIManager.Instance.Show_MidSoftLoad(false);
                    }
                );
                /*
                NotificationController.Instance.ShowUI(
                      FeedbackUI.Status.Fail
                    , "Room is not longer available :O"
                    , string.Format("Room is not longer available {0}", _matchSetting.RoomName)
                    , delegate ()
                    {
                        HomeManager.Instance.HideSelectDeck();
                        HomeManager.Instance.HideAllLoading();
                        NotificationController.Instance.HideUI();
                    }
                );
                */

                ExitFriendMatching();
            }
        }

        public void OnRoomReady()
        {
            // Room Ready
            LogFeedback("OnRoomReady()");

            switch (Mode)
            {
                case GameMode.Friendly:
                    {
                        _photonView.RPC("RpcStartFriendMatch", RpcTarget.All);
                    }
                    break;

                default:
                    {
                        // Start sync data.
                        _photonView.RPC("RpcStartSyncData", RpcTarget.All);
                    }
                    break;
            }
        }

        private void OnSelectedDeckComplete()
        {
            _photonView.RPC("RpcSelectedDeck", RpcTarget.All, (int)GetMyPlayerIndex());
        }

        private void OnCancelSelectDeck()
        {
            _photonView.RPC("RpcCancelSelectDeck", RpcTarget.All, (int)GetMyPlayerIndex());
        }
        #endregion

        #region RPC Methods
        [PunRPC]
        private void RpcStartFriendMatch()
        {
            _isUpdateStatusTitle = true;

            LogFeedback("RpcStartFriendMatch()");
            PopupUIManager.Instance.Show_SoftLoad(true);

            SetFriendReady(GetMyPlayerIndex(), false);
            DataManager.Instance.ClearFriendMatchRequest(null, null);

            if (GameHelper.IsHaveObject(typeof(HomeManager)))
            {
                /*
                _matchingUIManager.HideMatching();
                HomeManager.Instance.ShowSelectDeck(Mode, OnSelectedDeckComplete);

                HomeManager.Instance.HideAllLoading();
                */

                _photonView.RPC(
                      "RpcSendFirstPlayerData"
                    , RpcTarget.All
                    , (int)GetMyPlayerIndex()
                    , DataManager.Instance.PlayerInfo.PlayerID
                    , DataManager.Instance.PlayerInfo.DisplayName
                );
            }
            else
            {
                PopupUIManager.Instance.Show_SoftLoad(false);
                ExitMatching();
            }
        }

        [PunRPC]
        private void RpcSendFirstPlayerData(int playerIndexIndex, string playerID, string displayName)
        {
            PlayerIndex playerIndex = (PlayerIndex)playerIndexIndex;

            LogFeedback(string.Format("RpcSendFirstPlayerData {0} {1} {2}", playerIndex, playerID, displayName));

            FriendMatchPlayerInfo playerInfo = new FriendMatchPlayerInfo(playerIndex, playerID, displayName);
            if (_friendInfoList == null)
            {
                _friendInfoList = new Dictionary<PlayerIndex, FriendMatchPlayerInfo>();
            }
            _friendInfoList.Add(playerIndex, playerInfo);

            if (_friendInfoList.Count >= 2)
            {
                _matchingUIManager.HideMatching();
                //HomeManager.Instance.HomeUIManager.DeckSelectUI.SetChat(_friendInfoList[GetOpponentPlayerIndex()].PlayerID);
                HomeManager.Instance.HomeUIManager.DeckSelectUI.SetFriendStatus(false);
                HomeManager.Instance.HomeUIManager.ShowSelectDeck(
                      Mode
                    , null
                    , delegate (int deckIndex)
                    {
                        List<DeckData> deckList = DataManager.Instance.GetAllDeck();
                        DataManager.Instance.SetCurrentDeckID(deckList[deckIndex].DeckID);

                        OnSelectedDeckComplete();
                    }
                );

                PopupUIManager.Instance.Show_SoftLoad(false);
            }
        }

        [PunRPC]
        private void RpcStartSyncData()
        {
            _isUpdateStatusTitle = true;

            LogFeedback("StartSyncData()");

            if (Mode == GameMode.Friendly)
            {
                _matchingUIManager.ShowMatching(LocalizationManager.Instance.GetText("MENU_MATCHING_STATUS_MATCHED"), null);
            }
            else
            {
                SetStatusTitle(LocalizationManager.Instance.GetText("MENU_MATCHING_STATUS_MATCHED"));
            }

            _isMatched = true;
            _matchingUIManager.HideCancelButton();

            switch (_matchSetting.Mode)
            {
                case GameMode.Event:
                    {
                        if(EventBattleDB.Instance.DoUsePlayerDeck)
                        {
                            if (EventBattleDB.Instance.DoAddCustomCardsToPlayerDeck)
                            {
                                string profileID = PhotonNetwork.IsMasterClient ? "EVP001" : "EVP002";

                                // Add additional event cards to custom deck
                                DeckData currentDeck = DataManager.Instance.GetCurrentDeck();
                                DeckData eventBattleDeck = DataManager.Instance.GetEventBattleDeck(profileID);
                                DeckData customDeck = new DeckData(currentDeck.DeckID, currentDeck.DeckName, currentDeck.ElementID, currentDeck.HeroID, currentDeck.CardBackID, eventBattleDeck.HUDSkinID, currentDeck.Deck);
                                foreach (KeyValuePair<string, int> card in eventBattleDeck.Deck)
                                {
                                    customDeck.Deck.AddCard(card.Key, card.Value);
                                }

                                _myData = MatchPlayerData.CreateLocalMatchPlayerData(customDeck);
                            }
                            else
                            {
                                // Create my match data.
                                _myData = MatchPlayerData.CreateLocalMatchPlayerData();
                            }

                            _photonView.RPC("RpcSendPlayerData", RpcTarget.Others, _myData.ToJson());

                        }
                        else
                        {
                            if (PhotonNetwork.IsMasterClient)
                            {
                                _photonView.RPC("RpcEventBattleRequestPlayerInfo", RpcTarget.Others);
                            }
                        }
                    }
                    break;

                default:
                    {
                        // Create my match data.
                        _myData = MatchPlayerData.CreateLocalMatchPlayerData();
                        _photonView.RPC("RpcSendPlayerData", RpcTarget.Others, _myData.ToJson());
                    }
                    break;
            }
        }

        [PunRPC]
        private void RpcEventBattleRequestPlayerInfo()
        {
            LogFeedback("RpcEventBattleRequestPlayerInfo ");

            _photonView.RPC("RpcEventBattleReceivePlayerInfo", RpcTarget.Others, DataManager.Instance.PlayerInfo.ToJson());
        }

        [PunRPC]
        private void RpcEventBattleReceivePlayerInfo(string clientPlayerInfoJson)
        {
            if (!PhotonNetwork.IsMasterClient) return;

            LogFeedback("RpcEventBattleReceivePlayerInfo (1):" + clientPlayerInfoJson);

            PlayerInfoData clientInfo = PlayerInfoData.ConvertFromJson(clientPlayerInfoJson);
            PlayerInfoData hostInfo = DataManager.Instance.PlayerInfo;

            List<EventBattleDBData> profileList;
            if (EventBattleDB.Instance.GetAllData(out profileList))
            {
                string hostProfileID = "";
                string clientProfileID = "";
                List<EventBattleDBData> currentActiveProfileList = GetCurrentActiveEventBattleProfileList(profileList);

                {
                    int index = UnityEngine.Random.Range(0, currentActiveProfileList.Count);
                    hostProfileID = currentActiveProfileList[index].ProfileID;
                    currentActiveProfileList.RemoveAt(index);
                }
                {
                    int index = UnityEngine.Random.Range(0, currentActiveProfileList.Count);
                    clientProfileID = currentActiveProfileList[index].ProfileID;
                    currentActiveProfileList.RemoveAt(index);
                }

                MatchPlayerData hostMatchPlayerData = new MatchPlayerData(
                      hostInfo
                    , DataManager.Instance.GetEventBattleDeck(hostProfileID)
                );

                MatchPlayerData clientMatchPlayerData = new MatchPlayerData(
                      clientInfo
                    , DataManager.Instance.GetEventBattleDeck(clientProfileID)
                );

                _photonView.RPC("RpcEventBattleSendEventPlayerData", RpcTarget.All, hostMatchPlayerData.ToJson(), clientMatchPlayerData.ToJson());
            }
        }

        [PunRPC]
        private void RpcEventBattleSendEventPlayerData(string text_1, string text_2)
        {
            bool isSuccess = true;
            MatchPlayerData data_1 = null;
            MatchPlayerData data_2 = null;

            isSuccess &= MatchPlayerData.JsonToMatchPlayerData(text_1, out data_1);
            isSuccess &= MatchPlayerData.JsonToMatchPlayerData(text_2, out data_2);

            if (isSuccess)
            {
                LogFeedback(string.Format("Got Data_1: Name={0} Deck={1} RandomSeed={2}"
                    , data_1.PlayerInfo.DisplayName
                    , data_1.Deck.ToString()
                    , data_1.RandomSeed
                ));

                LogFeedback(string.Format("Got Data_2: Name={0} Deck={1} RandomSeed={2}"
                    , data_2.PlayerInfo.DisplayName
                    , data_2.Deck.ToString()
                    , data_2.RandomSeed
                ));

                List<MatchPlayerData> playerList = new List<MatchPlayerData>();
                playerList.Add(data_1);
                playerList.Add(data_2);

                MatchProperty matchProperty = new MatchProperty(
                      gameMode: Mode
                    , playerCount: 2
                    , isNetwork: true
                    , isHaveTimer: true
                    , isVSBot: false
                    , botType: BotType.None
                    , botLevel: BotLevel.None
                    , adventureStageData: null
                );

                _matchData = new MatchData(matchProperty, playerList);
                LogFeedback(_matchData.ToString());

                StartPlay(_matchData);
            }
        }

        [PunRPC]
        private void RpcSendPlayerData(string text)
        {
            LogFeedback("RpcSendPlayerData " + text);

            _matchData = null;
            if (MatchPlayerData.JsonToMatchPlayerData(text, out _receivedData))
            {
                LogFeedback(string.Format("Got Data: Name={0} Deck={1} RandomSeed={2}"
                    , _receivedData.PlayerInfo.DisplayName
                    , _receivedData.Deck.ToString()
                    , _receivedData.RandomSeed
                ));

                List<MatchPlayerData> playerList = new List<MatchPlayerData>();

                if (PhotonNetwork.IsMasterClient)
                {
                    playerList.Add(_myData);        // I am player one.
                    playerList.Add(_receivedData);
                }
                else
                {
                    playerList.Add(_receivedData);
                    playerList.Add(_myData);        // I am player two.
                }

                MatchProperty matchProperty = new MatchProperty(
                      gameMode: Mode
                    , playerCount: 2
                    , isNetwork: true
                    , isHaveTimer: true
                    , isVSBot: false
                    , botType: BotType.None
                    , botLevel: BotLevel.None
                    , adventureStageData: null
                );

                _matchData = new MatchData(matchProperty, playerList);

                LogFeedback(_matchData.ToString());

                StartPlay(_matchData);
            }
        }

        [PunRPC]
        private void RpcSelectedDeck(int playerIndexIndex)
        {
            PlayerIndex playerIndex = (PlayerIndex)playerIndexIndex;
            SetFriendReady(playerIndex, true);
            if (playerIndex == GetOpponentPlayerIndex())
            {
                HomeManager.Instance.HomeUIManager.DeckSelectUI.SetFriendStatus(true);
            }
            else
            {
                _matchingUIManager.ShowMatching(LocalizationManager.Instance.GetText("MENU_MATCHING_STATUS_WAIT_SELECT_DECK"), OnCancelSelectDeck);
                HomeManager.Instance.HomeUIManager.DeckSelectUI.HideUI();
            }

            if (IsFriendReady)
            {
                if (IsHost())
                {
                    // I am host. // Start sync data process.
                    _photonView.RPC("RpcStartSyncData", RpcTarget.All);
                }
            }
        }

        [PunRPC]
        private void RpcCancelSelectDeck(int playerIndexIndex)
        {
            if (!IsFriendReady && !_isMatched)
            {
                PlayerIndex playerIndex = (PlayerIndex)playerIndexIndex;
                SetFriendReady(playerIndex, false);
                if (playerIndex == GetOpponentPlayerIndex())
                {
                    HomeManager.Instance.HomeUIManager.DeckSelectUI.SetFriendStatus(false);
                }
                else
                {
                    PopupUIManager.Instance.Show_SoftLoad(true);

                    _matchingUIManager.HideMatching();
                    HomeManager.Instance.HomeUIManager.ShowSelectDeck(
                          Mode
                        , null
                        , delegate (int deckIndex)
                        {
                            List<DeckData> deckList = DataManager.Instance.GetAllDeck();
                            DataManager.Instance.SetCurrentDeckID(deckList[deckIndex].DeckID);

                            OnSelectedDeckComplete();
                        }
                    );

                    PopupUIManager.Instance.Show_SoftLoad(false);
                }
            }
        }
        #endregion

        #region MonoBehaviourPunCallbacks CallBacks
        // below, we implement some callbacks of PUN
        // you can find PUN's callbacks in the class MonoBehaviourPunCallbacks

        /// <summary>
        /// Called after the connection to the master is established and authenticated
        /// </summary>
        public override void OnConnectedToMaster()
        {
            LogFeedback("OnConnectedToMaster");
            SetStatusTitle(LocalizationManager.Instance.GetText("MENU_MATCHING_STATUS_CONNECTED"));

            _matchingStatus = MatchingStatus.Connected;
            _isConnected = true;

            LogFeedback("Start JoinLobby...");
            SetStatusTitle(LocalizationManager.Instance.GetText("MENU_MATCHING_STATUS_JOINING_LOBBY"));

            JoinLobby(GetLobbyType());
        }

        /// <summary>
        /// Called when a JoinRandom() call failed. The parameter provides ErrorCode and message.
        /// </summary>
        /// <remarks>
        /// Most likely all rooms are full or no rooms are available. <br/>
        /// </remarks>
        public override void OnJoinRandomFailed(short returnCode, string message)
        {
            LogFeedback("<Color=Red>OnJoinRandomFailed</Color>: Next -> Create a new Room");
            //LogFeedback("PUN Basics Tutorial/Launcher:OnJoinRandomFailed() was called by PUN. No random room available, so we create one.\nCalling: PhotonNetwork.CreateRoom");

            //_matchingStatus = MatchingStatus.Idle;

            // #Critical: we failed to join a random room, 
            // maybe none exists or they are all full. No worries, we create a new room.
            CreateRoom();
        }

        /// <summary>
        /// Called after disconnecting from the Photon server.
        /// </summary>
        public override void OnDisconnected(DisconnectCause cause)
        {
            LogFeedback(string.Format("<Color=Red>OnDisconnected</Color> {0} retry:{1}", cause, _reconnectRetryTime));
            //Debug.LogError("PUN Basics Tutorial/Launcher:Disconnected");

            // #Critical: we failed to connect or got disconnected. 
            // There is not much we can do. Typically, 
            // a UI system should be in place to let the user attemp to connect again.

            _matchingStatus = MatchingStatus.Disconnected;
            _isConnected = false;

            if (_onDisconnectComplete != null)
            {
                UnityAction onDisconnectComplete = _onDisconnectComplete;
                _onDisconnectComplete = null; // clear callback

                onDisconnectComplete?.Invoke();
            }

            if (_isMatched && !_isTimeout)
            {
                ExitMatching();
            }
            else
            {
                if (_isRequestReconnect)
                {
                    GameHelper.DelayCallback(
                          1.0f
                        , delegate ()
                        {
                            Reconnect(cause != DisconnectCause.DisconnectByClientLogic);
                        }
                    );
                }
            }
        }

        /// <summary>
        /// Called when entering a room (by creating or joining it). Called on all clients (including the Master Client).
        /// </summary>
        /// <remarks>
        /// This method is commonly used to instantiate player characters.
        /// If a match has to be started "actively", you can call an [PunRPC](@ref PhotonView.RPC) triggered by a user's button-press or a timer.
        ///
        /// When this is called, you can usually already access the existing players in the room via PhotonNetwork.PlayerList.
        /// Also, all custom properties should be already available as Room.customProperties. Check Room..PlayerCount to find out if
        /// enough players are in the room to start playing.
        /// </remarks>
        public override void OnJoinedRoom()
        {
            LogFeedback("OnJoinedRoom() with " + PhotonNetwork.CurrentRoom.PlayerCount + " Player(s)");
            //LogFeedback("PUN Basics Tutorial/Launcher: OnJoinedRoom() called by PUN. Now this client is in a room.\nFrom here on, your game would be running.");
            LogFeedback(string.Format("{0} joined room {1}.", PhotonNetwork.NickName, PhotonNetwork.CurrentRoom.Name));

            _friendInfoList = null;

            _matchingStatus = MatchingStatus.InRoom;
            _roomTimer = Random.Range(_waitTime_Min, _waitTime_Max);

            if (Mode == GameMode.Friendly)
            {
                _isRequestReconnect = false;

                if (PhotonNetwork.IsMasterClient)
                {
                    // I am host
                    OnHostFriendRoomReady();
                }
            }
            else
            {
                _isRequestReconnect = true; // reconnect when something happen
            }
        }

        public override void OnJoinRoomFailed(short returnCode, string message)
        {
            LogFeedback(string.Format("OnJoinRoomFailed({0},{1})", returnCode, message));

            //_matchingStatus = MatchingStatus.Idle;

            if (Mode == GameMode.Friendly)
            {
                _isRequestReconnect = false;
                PopupUIManager.Instance.Show_SoftLoad(true);

                UnityAction popupAction = delegate ()
                {
                    PopupUIManager.Instance.ShowPopup_Error(
                          LocalizationManager.Instance.GetText("FRIENDLY_MATCH_TITLE")
                        , LocalizationManager.Instance.GetText("ERROR_FRIENDLY_MATCH_JOINROOM_FAIL")
                        , delegate ()
                        {
                            ExitMatching();
                        }
                    );

                    PopupUIManager.Instance.Show_SoftLoad(false);
                };

                UnityAction clearFriendListAction = delegate ()
                {
                    DataManager.Instance.ClearFriendMatchRequest(
                      delegate () { popupAction?.Invoke(); }
                    , (err) => { popupAction?.Invoke(); }
                );
                };

                Disconnect(clearFriendListAction);
            }
            else
            {
                _isRequestReconnect = true;
                Disconnect();
            }
        }

        public override void OnLeftRoom()
        {
            LogFeedback("OnLeftRoom()");

            //_matchingStatus = MatchingStatus.Idle;

            UnityAction callback = _onLeaveRoomComplete;
            _onLeaveRoomComplete = null;

            if (callback != null)
            {
                callback.Invoke();
            }
        }

        public override void OnPlayerEnteredRoom(Player newPlayer)
        {
            LogFeedback(string.Format("OnPlayerEnteredRoom({0})", newPlayer.NickName));
            LogFeedback(string.Format("{0} joined room.", newPlayer.NickName));

            if (PhotonNetwork.IsMasterClient)
            {
                // I am host
                if (PhotonNetwork.CurrentRoom.PlayerCount == maxPlayersPerRoom)
                {
                    _roomTimer = _waitTime_Max;

                    // Have 2 players.
                    OnRoomReady();
                }
            }
        }

        public override void OnPlayerLeftRoom(Player otherPlayer)
        {
            if (_isMatched && !_isTimeout)
            {
                ExitMatching();
            }
            else
            {
                LogFeedback(string.Format("OnPlayerLeftRoom({0})", otherPlayer.NickName));
                LogFeedback(string.Format("{0} left room.", otherPlayer.NickName));

                if (PhotonNetwork.IsMasterClient)
                {
                    // Now I am the room owner.
                    //PhotonNetwork.CurrentRoom.Name = PhotonNetwork.NickName;
                }

                LogFeedback(string.Format("New room owner is {0}", PhotonNetwork.MasterClient.NickName));
            }
        }

        public override void OnMasterClientSwitched(Player newMasterClient)
        {
            LogFeedback(string.Format("OnMasterClientSwitched({0})", newMasterClient.NickName));
            LogFeedback(string.Format("Room name : {0}.", PhotonNetwork.CurrentRoom.Name));
        }

        public override void OnRoomPropertiesUpdate(ExitGames.Client.Photon.Hashtable propertiesThatChanged)
        {
            LogFeedback(string.Format("OnRoomPropertiesUpdate({0})", propertiesThatChanged.ToStringFull()));
            LogFeedback(string.Format("Room name : {0}.", PhotonNetwork.CurrentRoom.Name));
        }

        public override void OnRoomListUpdate(List<RoomInfo> roomList)
        {
            LogFeedback(string.Format("OnRoomListUpdate() Count:{0}", roomList.Count));

            if (_roomInfoList == null)
            {
                _roomInfoList = new List<RoomInfo>();
            }

            foreach (RoomInfo info in roomList)
            {
                if (_roomInfoList.Contains(info))
                {
                    if (info.RemovedFromList)
                    {
                        _roomInfoList.Remove(info);
                    }
                }
                else
                {
                    if (
                           info.CustomProperties.ContainsKey("C0")
                        && info.CustomProperties.ContainsKey("C1")
                    )
                    {
                        string version = info.CustomProperties["C0"].ToString();
                        GameMode mode = Mode;
                        if (GameHelper.StrToEnum<GameMode>(info.CustomProperties["C1"].ToString(), out mode))
                        {
                            if (
                                   version == this.gameVersion
                                && mode == Mode
                                && info.IsOpen
                            )
                            {
                                _roomInfoList.Add(info);
                            }
                        }
                    }
                }
            }
        }

        public override void OnJoinedLobby()
        {
            LogFeedback("OnJoinedLobby()");
            SetStatusTitle(LocalizationManager.Instance.GetText("MENU_MATCHING_STATUS_JOINED_LOBBY"));

            _matchingStatus = MatchingStatus.InLobby;

            if (_isAutoJoin)
            {
                if (_matchSetting.RoomName != null && _matchSetting.RoomName.Length > 0)
                {
                    // Join specific room
                    SetStatusTitle(LocalizationManager.Instance.GetText("MENU_MATCHING_STATUS_MATCHING"));

                    _isRequestReconnect = false;
                    JoinRoom(_matchSetting.RoomName);
                }
                else
                {
                    SetStatusTitle(LocalizationManager.Instance.GetText("MENU_MATCHING_STATUS_MATCHING"));

                    _isRequestReconnect = true;
                    StartFindingOpponent(_retryIndex, _isResetTimer);

                    _matchingUIManager.ShowCancelButton();
                }
            }
        }

        public override void OnLeftLobby()
        {
            LogFeedback("OnLeftLobby()");
            SetStatusTitle(LocalizationManager.Instance.GetText("MENU_MATCHING_STATUS_LEFT_LOBBY"));

            //_matchingStatus = MatchingStatus.Idle;
        }
        #endregion
    }
}