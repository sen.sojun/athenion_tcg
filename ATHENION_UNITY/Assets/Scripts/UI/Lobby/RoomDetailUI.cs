﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;

public class RoomDetailUI : MonoBehaviour 
{
    [SerializeField]
    private TextMeshProUGUI _text;

    [SerializeField]
    private Button _joinButton;

    private RoomDetailUIData _data;
    private UnityAction<string> _onClickJoin;

    // Use this for initialization
    void Start () 
    {
	}
	
	// Update is called once per frame
	void Update () 
    {
	}

    #region Methods
    public void ShowUI(RoomDetailUIData data, UnityAction<string> onClickJoin)
    {
        _data = data;
        _onClickJoin = onClickJoin;
        _text.text = data.RoomName;

        _joinButton.onClick.RemoveAllListeners();
        _joinButton.onClick.AddListener(OnClickJoin);

        gameObject.SetActive(true);
    }

    public void HideUI()
    {
        gameObject.SetActive(false);
    }

    private void OnClickJoin()
    {
        if (_onClickJoin != null)
        {
            _onClickJoin.Invoke(_data.RoomKey);
        }
    }
    #endregion
}
