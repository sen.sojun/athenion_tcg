﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomDetailUIData
{
    #region Public Properties
    public string RoomKey { get; private set; }
    public string RoomName { get; private set; }
    #endregion

    #region Constructors
    public RoomDetailUIData(string key, string name)
    {
        RoomKey = key;
        RoomName = name;
    }
    #endregion
}
