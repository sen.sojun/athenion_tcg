﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;

public class RoomUI : MonoBehaviour 
{
    #region Private Serializable Properties
    [SerializeField]
    private TextMeshProUGUI _roomHeader;

    [SerializeField]
    private TextMeshProUGUI _logText;

    [SerializeField]
    private Button _leaveButton;
    #endregion

    #region Private Properties
    private readonly int _logMaxCount = 20;

    private List<string> _logList;
    private UnityAction _onLeaveRoom;
    #endregion

    #region MonoBehaviour CallBacks
    // Use this for initialization
    void Start ()
    {
	}
	
	// Update is called once per frame
	void Update ()
    {
	}
    #endregion

    #region Methods
    public void ShowUI(string roomHeader, UnityAction onLeaveRoom)
    {
        _roomHeader.text = roomHeader;
        _onLeaveRoom = onLeaveRoom;

        _leaveButton.onClick.RemoveAllListeners();
        _leaveButton.onClick.AddListener(OnClickLeaveRoom);

        gameObject.SetActive(true);
    }

    public void SetRoomHeader(string roomHeader)
    {
        _roomHeader.text = roomHeader;
    }

    public void ClearRoomLog()
    {
        if (_logList != null)
        {
            _logList.Clear();
        }
        UpdateLog();
    }

    public void AddRoomLog(string text)
    {
        if (_logList == null)
        {
            _logList = new List<string>();
        }

        _logList.Add(text);

        if (_logList.Count > _logMaxCount)
        {
            _logList.RemoveAt(0);
        }

        UpdateLog();
    }

    private void UpdateLog()
    {
        string logText = "";
        if (_logList != null && _logList.Count > 0)
        {
            for (int index = _logList.Count - 1; index >= 0; --index)
            {
                if (logText.Length > 0)
                {
                    logText += "\n" + _logList[index];
                }
                else
                {
                    logText += _logList[index];
                }
            }
        }

        _logText.text = logText;
    }

    public void HideUI()
    {
        gameObject.SetActive(false);
    }

    public void OnClickLeaveRoom()
    {
        if (_onLeaveRoom != null)
        {
            _onLeaveRoom.Invoke();
        }
    }
    #endregion
}
