﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class LobbyUI : MonoBehaviour 
{
    [SerializeField]
    private RoomListUI _roomListUI;

    [SerializeField]
    private Button _joinRandomButton;

    [SerializeField]
    private Button _createButton;

    [SerializeField]
    private Button _backButton;

    // Use this for initialization
    void Start () 
    {
	}
	
	// Update is called once per frame
	void Update ()
    {
	}

    public void ShowUI(UnityAction onClickJoinRandom, UnityAction onClickCreate, UnityAction onClickBack)
    {
        _joinRandomButton.onClick.RemoveAllListeners();
        if (onClickJoinRandom != null)  _joinRandomButton.onClick.AddListener(onClickJoinRandom);

        _backButton.onClick.RemoveAllListeners();
        if(onClickBack != null) _backButton.onClick.AddListener(onClickBack);

        _createButton.onClick.RemoveAllListeners();
        if (onClickCreate != null) _createButton.onClick.AddListener(onClickCreate);

        gameObject.SetActive(true);
    }

    public void UpdateRoomList(List<RoomDetailUIData> dataList, UnityAction<string> onClickJoin)
    {
        _roomListUI.UpdateRoomList(dataList, onClickJoin);
    }

    public void ClearRoomList()
    {
        _roomListUI.ClearRoomList();
    }

    public void HideUI()
    {
        gameObject.SetActive(false);
    }
}
