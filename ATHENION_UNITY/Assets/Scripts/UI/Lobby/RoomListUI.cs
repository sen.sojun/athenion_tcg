﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;

public class RoomListUI : MonoBehaviour 
{
    #region Private Serializable Properties
    [SerializeField]
    private GameObject _content;

    [SerializeField]
    private GameObject _roomDetailPrefab;

    [SerializeField]
    private TextMeshProUGUI _emptyText;
    #endregion

    #region Private Properties
    private UnityAction<string> _onClickJoin;
    private List<RoomDetailUIData> _roomDataList;
    private List<RoomDetailUI> _roomUIList;
    #endregion

    #region MonoBehaviour CallBacks
    // Use this for initialization
    void Start ()
    {	
	}
	
	// Update is called once per frame
	void Update ()
    {	
	}
    #endregion

    #region Methods
    public void UpdateRoomList(List<RoomDetailUIData> dataList, UnityAction<string> onClickJoin)
    {
        _roomDataList = new List<RoomDetailUIData>(dataList);
        _onClickJoin = onClickJoin;

        if (_roomUIList == null)
        {
            _roomUIList = new List<RoomDetailUI>();
        }

        for (int index = 0; index < _roomDataList.Count || index < _roomUIList.Count; ++index)
        {
            if (index < _roomDataList.Count)
            {
                // Have data
                if (index < _roomUIList.Count)
                {
                    // Already create detail UI.
                    // Assign data
                    _roomUIList[index].ShowUI(_roomDataList[index], OnClickJoin); 
                }
                else
                {
                    // don't have room
                    // Create
                    GameObject obj = Instantiate(_roomDetailPrefab, _content.transform) as GameObject;
                    RoomDetailUI ui = obj.GetComponent<RoomDetailUI>();
                    ui.ShowUI(_roomDataList[index], OnClickJoin);
                    _roomUIList.Add(ui);
                }
            }
            else
            {
                // No data
                if (index < _roomUIList.Count)
                {
                    // Already create detail UI. 
                    // Hide
                    _roomUIList[index].HideUI(); 
                }
            }
        }

        _emptyText.gameObject.SetActive(_roomDataList.Count <= 0);
    }

    public void ClearRoomList()
    {
        if (_roomUIList != null && _roomUIList.Count > 0)
        {
            for (int index = 0; index < _roomUIList.Count; ++index)
            {
                // Hide
                _roomUIList[index].HideUI();
            }
        }
    }

    private void OnClickJoin(string roomKey)
    {
        Debug.Log("OnClickJoin " + roomKey);

        if (_onClickJoin != null)
        {
            _onClickJoin.Invoke(roomKey);
        }
    }
    #endregion
}
