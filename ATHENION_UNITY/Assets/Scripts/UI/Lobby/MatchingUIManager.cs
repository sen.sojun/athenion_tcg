﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;
using Karamucho.Network;

public class MatchingUIManager : MonoBehaviour
{
    #region Public Properties
    public MatchingUI MatchingPanel;
    #endregion

    #region Private Properties
    private MatchingManager _matchingManager;
    #endregion

    /*
    // Start is called before the first frame update
    void Start()
    {        
    }
    */

    #region Update
    // Update is called once per frame
    void Update()
    {
    }
    #endregion

    #region Methods
    public void Init(MatchSetting matchSetting)
    {
        _matchingManager = FindObjectOfType<MatchingManager>();

        _matchingManager.SetPhotonSetting(matchSetting);
        _matchingManager.Init(this);

        MatchingPanel.SetGameMode(matchSetting.Mode);

        SoundManager.PlayBGM(SoundManager.BGM.Matching, 3.0f);
    }

    public void ShowMatching(string status, UnityAction onClickCancel)
    {
        MatchingPanel.ShowUI(status, onClickCancel);
    }

    public void HideMatching()
    {
        MatchingPanel.HideUI();
    }

    public void ShowCanvas()
    {
        gameObject.SetActive(true);
    }

    public void HideCanvas()
    {
        gameObject.SetActive(false);
    }

    public void ShowCancelButton()
    {
        MatchingPanel.ShowCancelButton();
    }

    public void HideCancelButton()
    {
        MatchingPanel.HideCancelButton();
    }

    public void SetStatusTitle(string status)
    {
        MatchingPanel.SetStatusTitle(status);
    }

    public void ShowTimer()
    {
        MatchingPanel.ShowTimer();
    }

    public void HideTimer()
    {
        MatchingPanel.HideTimer();
    }

    public void UpdateTimer(float timer)
    {
        MatchingPanel.UpdateTimer(timer);
    }

    public void SetOnClickCancel(UnityAction onClickCancel)
    {
        MatchingPanel.SetOnClickCancel(onClickCancel);
    }
    #endregion
}
