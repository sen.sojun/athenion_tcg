﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

namespace Karamucho.Network
{
    #region ILauncherLog Interface 
    public interface ILauncherLog
    {
        void SetMessageLog(string message);
    }
    #endregion

    public class LobbyManager : MonoBehaviourPunCallbacks
    {
        #region Private Serializable Fields
        [SerializeField]
        private LobbyUI _lobbyUI;

        [SerializeField]
        private RoomUI _roomUI;

        [SerializeField]
        private LoadingUI _loadingOverlayUI;

        [SerializeField]
        private LoadingUI _loadingUI;

        [Tooltip("The maximum number of players per room")]
        [SerializeField]
        private byte maxPlayersPerRoom = 2;
        #endregion

        #region Private Fields
        private readonly float _widthMargin = 20.0f;
        private readonly float _heightMargin = 20.0f;
        private readonly int _maxText = 3000;
        private readonly int _playerTtl = 20000;
        private readonly int _emptyRoomTtl = 5000;

        /// <summary>
        /// Keep track of the current process. Since connection is asynchronous and is based on several callbacks from Photon, 
        /// we need to keep track of this to properly adjust the behavior when we receive call back by Photon.
        /// Typically this is used for the OnConnectedToMaster() callback.
        /// </summary>
        bool isConnecting;
        private bool _isRequestReconnect = false;

        /// <summary>
        /// This client's version number. Users are separated from each other by gameVersion (which allows you to make breaking changes).
        /// </summary>
        string gameVersion = "1";
        private List<string> _logList;
        private List<RoomInfo> _roomInfoList;

        private PhotonView _photonView;
        private MatchData _matchData;
        private MatchPlayerData _myData;
        private MatchPlayerData _receivedData;
        private StringBuilder _debugText;
        private Rect _debugTextRect;
        #endregion

        #region MonoBehaviour CallBacks
        /// <summary>
        /// MonoBehaviour method called on GameObject by Unity during early initialization phase.
        /// </summary>
        void Awake()
        {
            // #Critical
            // this makes sure we can use PhotonNetwork.LoadLevel() on the master client and all clients in the same room sync their level automatically
            PhotonNetwork.AutomaticallySyncScene = true;

            _debugTextRect = new Rect(
                  _widthMargin
                , _heightMargin
                , Screen.width - (2.0f * _widthMargin)
                , Screen.height - (2.0f * _heightMargin)
            );

            LogFeedback("", false);
            Init();
        }

        private void OnGUI()
        {
            if (_debugText == null) _debugText = new StringBuilder();
            _debugText.Clear();

            if (_logList != null)
            {
                for (int index = _logList.Count - 1; index >= 0; --index)
                {
                    if (_debugText.Length > 0)
                    {
                        _debugText.Append("\n" + _logList[index]);
                    }
                    else
                    {
                        _debugText.Append(_logList[index]);
                    }
                }
            }

            GUI.Label(_debugTextRect, "<color=yellow>" + _debugText + "</color>");
        }
        #endregion

        #region Methods
        /// <summary>
        // Connect this application instance to Photon Cloud Network
        /// </summary>
        public void Connect()
        {
            if (!PhotonNetwork.IsConnected)
            {
                isConnecting = true;

                if (_loadingOverlayUI != null)
                {
                    _loadingOverlayUI.ShowUI();
                }

                LogFeedback("Connecting...");
                // #Critical, we must first and foremost connect to Photon Online Server.
                PhotonNetwork.GameVersion = this.gameVersion;
                PhotonNetwork.ConnectUsingSettings();
            }
        }

        /// <summary>
        /// Logs the feedback in the UI view for the player, as opposed to inside the Unity Editor for the developer.
        /// </summary>
        /// <param name="message">Message.</param>
        void LogFeedback(string message, bool isAdd = true)
        {
            if (_logList == null)
            {
                _logList = new List<string>();
            }

            if (!isAdd)
            {
                _logList.Clear();
            }
            _logList.Add(message);
            if (_logList.Count > 10)
            {
                _logList.RemoveAt(0);
            }

#if UNITY_EDITOR
            string logText = "";
            for (int index = _logList.Count - 1; index >= 0; --index)
            {
                if (logText.Length > 0)
                {
                    logText += "\n" + _logList[index];
                }
                else
                {
                    logText += _logList[index];
                }
            }
#endif
        }

        private void Init()
        {
            _loadingUI.HideUI();

            SetPlayerName(DataManager.Instance.PlayerInfo.DisplayName);
            _lobbyUI.ShowUI(JoinRandomRoom, CreateRoom, null);
            _photonView = PhotonView.Get(this);

            LogFeedback("Connect...");
            Connect();
        }

        public void SetPlayerName(string name)
        {
            // #Important
            if (string.IsNullOrEmpty(name))
            {
                //Debug.LogError("Player Name is null or empty");
                return;
            }

            PhotonNetwork.NickName = name;
            //DataManager.Instance.SetPlayerDisplayName(name);

            LogFeedback("SetPlayerName " + name);
        }

        public void JoinRandomRoom()
        {
            LogFeedback("JoinRandomRoom()");

            PhotonNetwork.JoinRandomRoom();
        }

        public void JoinRoom(string roomName)
        {
            LogFeedback(string.Format("Start JoinRoom({0})...", roomName));

            PhotonNetwork.JoinRoom(roomName);
        }

        public void CreateRoom()
        {
            ExitGames.Client.Photon.Hashtable customRoomProperties = new ExitGames.Client.Photon.Hashtable() {
                { "CreateTime", DateTimeData.GetDateTimeUTC().ToLongDateString() },
                { "OwnerName", PhotonNetwork.NickName }
            };

            PhotonNetwork.CreateRoom(
                  PhotonNetwork.NickName
                , new RoomOptions
                {
                    MaxPlayers = this.maxPlayersPerRoom
                    ,
                    CustomRoomProperties = customRoomProperties
                    ,
                    PlayerTtl = _playerTtl
                    ,
                    EmptyRoomTtl = _emptyRoomTtl
                }
            );
        }

        public void StartPlay(MatchData matchData)
        {
            LogFeedback("StartPlay()");

            MatchDataContainer container;
            MatchData.CreateMatchContainer(matchData, out container);
            DontDestroyOnLoad(container.gameObject);

            PhotonNetwork.CurrentRoom.IsVisible = false;
            //PhotonNetwork.CurrentRoom.IsOpen = false; // For re-join.
            DataManager.Instance.SetRoomName(PhotonNetwork.CurrentRoom.Name);

            if (PhotonNetwork.IsMasterClient)
            {
                // Load battle scene.
                LoadScene(GameHelper.BattleSceneName, LoadSceneMode.Single, true, null);
            }
        }

        public void BackToMenu()
        {
            if (PhotonNetwork.IsConnected)
            {
                PhotonNetwork.Disconnect();
            }

            _loadingUI.ShowUI();
            LoadScene(GameHelper.NavigatorSceneName, LoadSceneMode.Single, true, null);
        }

        private void LoadScene(string sceneName, LoadSceneMode loadSceneMode, bool isAsync, UnityAction onComplete)
        {
            if (isAsync)
            {
                StartCoroutine(LoadingSceneAsync(sceneName, loadSceneMode, onComplete));
            }
            else
            {
                SceneManager.LoadScene(sceneName, loadSceneMode);
            }
        }

        private IEnumerator LoadingSceneAsync(string sceneName, LoadSceneMode loadSceneMode, UnityAction onComplete)
        {
            Debug.LogFormat("Loading scene {0}", sceneName);

            yield return new WaitForSeconds(0.5f);
            AsyncOperation asyncLoadUI = SceneManager.LoadSceneAsync(sceneName, loadSceneMode);

            // Wait until the asynchronous scene fully loads
            while (!asyncLoadUI.isDone)
            {
                if (_loadingUI != null)
                {
                    _loadingUI.SetProgress(asyncLoadUI.progress);
                }

                yield return null;
            }

            if (onComplete != null)
            {
                onComplete.Invoke();
            }
        }
        #endregion

        #region Lobby Event Methods
        public void OnRoomReady()
        {
            // Start sync data.
            _photonView.RPC("RpcStartSyncData", RpcTarget.All);
        }
        #endregion

        #region RPC Methods
        [PunRPC]
        private void RpcStartSyncData()
        {
            LogFeedback("StartSyncData()");

            // Create my match data.
            _myData = MatchPlayerData.CreateLocalMatchPlayerData();
            _photonView.RPC("RpcSendPlayerData", RpcTarget.Others, _myData.ToJson());
        }

        [PunRPC]
        private void RpcSendPlayerData(string text)
        {
            LogFeedback("RpcSendPlayerData");

            _matchData = null;
            if (MatchPlayerData.JsonToMatchPlayerData(text, out _receivedData))
            {
                LogFeedback(string.Format("Got Data: Name={0} Deck={1} RandomSeed={2}"
                    , _receivedData.PlayerInfo.DisplayName
                    , _receivedData.Deck.ToString()
                    , _receivedData.RandomSeed
                ));

                List<MatchPlayerData> playerList = new List<MatchPlayerData>();

                if (PhotonNetwork.IsMasterClient)
                {
                    playerList.Add(_myData);        // I am player one.
                    playerList.Add(_receivedData);
                }
                else
                {
                    playerList.Add(_receivedData);
                    playerList.Add(_myData);        // I am player two.

                }

                MatchProperty matchProperty = new MatchProperty(
                      gameMode: GameMode.Casual
                    , playerCount: 2
                    , isNetwork: true
                    , isHaveTimer: true
                    , isVSBot: false
                    , botType: BotType.None
                    , botLevel: BotLevel.None
                    , adventureStageData: null
                );
                _matchData = new MatchData(matchProperty, playerList);

                LogFeedback(_matchData.ToString());

                StartPlay(_matchData);
            }
        }
        #endregion

        #region MonoBehaviourPunCallbacks CallBacks
        // below, we implement some callbacks of PUN
        // you can find PUN's callbacks in the class MonoBehaviourPunCallbacks

        /// <summary>
        /// Called after the connection to the master is established and authenticated
        /// </summary>
        public override void OnConnectedToMaster()
        {
            LogFeedback("OnConnectedToMaster");

            LogFeedback("Start JoinLobby...");
            PhotonNetwork.JoinLobby();
        }

        /// <summary>
        /// Called when a JoinRandom() call failed. The parameter provides ErrorCode and message.
        /// </summary>
        /// <remarks>
        /// Most likely all rooms are full or no rooms are available. <br/>
        /// </remarks>
        public override void OnJoinRandomFailed(short returnCode, string message)
        {
            LogFeedback("<Color=Red>OnJoinRandomFailed</Color>: Next -> Create a new Room");
            //Debug.Log("PUN Basics Tutorial/Launcher:OnJoinRandomFailed() was called by PUN. No random room available, so we create one.\nCalling: PhotonNetwork.CreateRoom");

            // #Critical: we failed to join a random room, 
            // maybe none exists or they are all full. No worries, we create a new room.
            CreateRoom();
        }

        /// <summary>
        /// Called after disconnecting from the Photon server.
        /// </summary>
        public override void OnDisconnected(DisconnectCause cause)
        {
            LogFeedback("<Color=Red>OnDisconnected</Color> " + cause);
            //Debug.LogError("PUN Basics Tutorial/Launcher:Disconnected");

            // #Critical: we failed to connect or got disconnected. 
            // There is not much we can do. Typically, 
            // a UI system should be in place to let the user attemp to connect again.

            isConnecting = false;
            if (_loadingOverlayUI != null)
            {
                _loadingOverlayUI.HideUI();
            }

            if (_isRequestReconnect)
            {
                PhotonNetwork.Reconnect();
            }
        }

        /// <summary>
        /// Called when entering a room (by creating or joining it). Called on all clients (including the Master Client).
        /// </summary>
        /// <remarks>
        /// This method is commonly used to instantiate player characters.
        /// If a match has to be started "actively", you can call an [PunRPC](@ref PhotonView.RPC) triggered by a user's button-press or a timer.
        ///
        /// When this is called, you can usually already access the existing players in the room via PhotonNetwork.PlayerList.
        /// Also, all custom properties should be already available as Room.customProperties. Check Room..PlayerCount to find out if
        /// enough players are in the room to start playing.
        /// </remarks>
        public override void OnJoinedRoom()
        {
            LogFeedback("OnJoinedRoom() with " + PhotonNetwork.CurrentRoom.PlayerCount + " Player(s)");
            Debug.Log("PUN Basics Tutorial/Launcher: OnJoinedRoom() called by PUN. Now this client is in a room.\nFrom here on, your game would be running.");

            _roomUI.ClearRoomLog();
            _roomUI.AddRoomLog(string.Format("{0} joined room {1}.", PhotonNetwork.NickName, PhotonNetwork.CurrentRoom.Name));
            _roomUI.ShowUI(PhotonNetwork.CurrentRoom.Name, delegate () { PhotonNetwork.LeaveRoom(); });
        }

        public override void OnJoinRoomFailed(short returnCode, string message)
        {
            LogFeedback(string.Format("OnJoinRoomFailed({0},{1})", returnCode, message));

            _lobbyUI.ClearRoomList();

            _isRequestReconnect = true;
            PhotonNetwork.Disconnect();
        }

        public override void OnLeftRoom()
        {
            LogFeedback("OnLeftRoom()");
            _roomUI.HideUI();
        }

        public override void OnPlayerEnteredRoom(Player newPlayer)
        {
            LogFeedback(string.Format("OnPlayerEnteredRoom({0})", newPlayer.NickName));
            _roomUI.AddRoomLog(string.Format("{0} joined room.", newPlayer.NickName));

            if (PhotonNetwork.CurrentRoom.PlayerCount == 2)
            {
                if (PhotonNetwork.IsMasterClient)
                {
                    // Have 2 players.
                    OnRoomReady();
                }
            }
        }

        public override void OnPlayerLeftRoom(Player otherPlayer)
        {
            LogFeedback(string.Format("OnPlayerLeftRoom({0})", otherPlayer.NickName));
            _roomUI.AddRoomLog(string.Format("{0} left room.", otherPlayer.NickName));

            if (PhotonNetwork.IsMasterClient)
            {
                // Now I am the room owner.
                //PhotonNetwork.CurrentRoom.Name = PhotonNetwork.NickName;
            }

            LogFeedback(string.Format("Room owner is {0}", PhotonNetwork.MasterClient.NickName));
            _roomUI.SetRoomHeader(PhotonNetwork.CurrentRoom.Name);
            _roomUI.AddRoomLog(string.Format("SetRoomHeader {0}.", PhotonNetwork.CurrentRoom.Name));
        }

        public override void OnMasterClientSwitched(Player newMasterClient)
        {
            LogFeedback(string.Format("OnMasterClientSwitched({0})", newMasterClient.NickName));
            _roomUI.SetRoomHeader(PhotonNetwork.CurrentRoom.Name);
            _roomUI.AddRoomLog(string.Format("SetRoomHeader {0}.", PhotonNetwork.CurrentRoom.Name));
        }

        public override void OnRoomPropertiesUpdate(ExitGames.Client.Photon.Hashtable propertiesThatChanged)
        {
            LogFeedback(string.Format("OnRoomPropertiesUpdate({0})", propertiesThatChanged.ToStringFull()));
            _roomUI.SetRoomHeader(PhotonNetwork.CurrentRoom.Name);
            _roomUI.AddRoomLog(string.Format("SetRoomHeader {0}.", PhotonNetwork.CurrentRoom.Name));
        }

        public override void OnRoomListUpdate(List<RoomInfo> roomList)
        {
            LogFeedback(string.Format("OnRoomListUpdate() Count:{0}", roomList.Count));

            if (_roomInfoList == null)
            {
                _roomInfoList = new List<RoomInfo>(roomList);
            }
            else
            {
                foreach (RoomInfo info in roomList)
                {
                    if (info.RemovedFromList)
                    {
                        _roomInfoList.Remove(info);
                    }
                    else
                    {
                        if (!_roomInfoList.Contains(info))
                        {
                            _roomInfoList.Add(info);
                        }
                    }
                }
            }

            List<RoomDetailUIData> roomUIDataList = new List<RoomDetailUIData>();
            foreach (RoomInfo room in _roomInfoList)
            {
                RoomDetailUIData roomUIData = new RoomDetailUIData(room.Name, room.Name);
                roomUIDataList.Add(roomUIData);
            }

            _lobbyUI.UpdateRoomList(roomUIDataList, JoinRoom);
        }

        public override void OnJoinedLobby()
        {
            LogFeedback("OnJoinedLobby()");

            _isRequestReconnect = false;
            if (_loadingOverlayUI != null)
            {
                _loadingOverlayUI.HideUI();
            }
        }

        public override void OnLeftLobby()
        {
            LogFeedback("OnLeftLobby()");
        }
        #endregion
    }
}