﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class InventoryPageContainer : MonoBehaviour
{
    [SerializeField]
    private Toggle _preFab;
    [SerializeField]
    private Transform spawnPoint;

    private List<Toggle> toggles = new List<Toggle>();
    private UnityAction<int> _onClickToggle;

    public void SetData(int max)
    {
        if (max <= 1)
        {
            HideUI();
            ClearToggles();
            return;
        }
        ClearToggles();
        GenerateToggles(max);
    }

    private void ClearToggles()
    {
        for (int i = 0; i < toggles.Count; i++)
        {
            Destroy(toggles[i].gameObject);
            toggles.RemoveAt(i);
            i--;
        }
    }

    private void GenerateToggles(int amount)
    {
        for (int i = 0; i < amount; i++)
        {
            int toggleIndex = i;
            Toggle toggle = Instantiate(_preFab, spawnPoint);
            toggle.onValueChanged.AddListener((isOn) =>
            {
                if (isOn)
                {
                    if (_onClickToggle != null)
                        _onClickToggle.Invoke(toggleIndex);
                }
            });
            toggle.isOn = false;
            toggle.gameObject.SetActive(true);
            toggles.Add(toggle);
        }
    }

    public void SetCurrent(int current)
    {
        if (current <= toggles.Count - 1)
            toggles[current].isOn = true;
    }

    public void ShowUI()
    {
        gameObject.SetActive(true);
    }

    public void HideUI()
    {
        gameObject.SetActive(false);
    }

    public void SetCallback(UnityAction<int> onClick)
    {
        _onClickToggle = onClick;
    }
}
