﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;
using System;
using Karamucho;

public class OpenPackPopupUI : MonoBehaviour
{
    [Header("PackDetail")]

    [SerializeField]
    private Image _packImage;
    [SerializeField]
    private TextMeshProUGUI _packIDText;

    [Header("ConfigAmount")]

    [SerializeField]
    private Button _decreaseButton;
    [SerializeField]
    private TextMeshProUGUI _amountText;
    [SerializeField]
    private Button _increaseButton;

    [Header("Confirmation")]

    [SerializeField]
    private Button _maxButton;

    [SerializeField]
    private Button _cancelButton;

    public Button _confirmButton;

    private CardPackData _pack;
    private int _amount;

    private UnityAction<int> _onConfirmValue;


    private void Awake()
    {
        _cancelButton.onClick.AddListener(HideUI);
        _confirmButton.onClick.AddListener(CallbackOnClickConfirmButton);
        _maxButton.onClick.AddListener(CallbackOnClickMaxButton);

        _increaseButton.onClick.AddListener(CallbackOnClickIncreaseButton);
        _decreaseButton.onClick.AddListener(CallbackOnClickDecreaseButton);
    }

    public void SetData(CardPackData pack)
    {
        this._pack = pack;
         
        _packImage.sprite = SpriteResourceHelper.LoadSprite(pack.PackID, SpriteResourceHelper.SpriteType.ICON_Item);

        _packIDText.text = LocalizationManager.Instance.GetText("SHOP_ITEM_NAME_" + pack.PackID);
        UpdateAmountValue(1);
    }

    public void ShowUI()
    {
        GameHelper.UITransition_FadeIn(this.gameObject);
    }

    public void HideUI()
    {
        GameHelper.UITransition_FadeOut(this.gameObject);
    }

    private void UpdateAmountValue(int value)
    {
        _amount = _pack.Amount >= 10 ? Mathf.Clamp(value, 1, 10) : Mathf.Clamp(value, 1, _pack.Amount);
        _amountText.text = _amount.ToString();
    }

    public void SetCallbackOnClickConfirmButton(UnityAction<int> onClick)
    {
        _onConfirmValue = onClick;
    }

    #region CallbackUI

    public void CallbackOnClickDecreaseButton()
    {
        UpdateAmountValue(_amount - 1);
    }

    public void CallbackOnClickIncreaseButton()
    {
        UpdateAmountValue(_amount + 1);
    }

    public void CallbackOnClickMaxButton()
    {
        UpdateAmountValue(10);
    }

    private void CallbackOnClickConfirmButton()
    {
        if (_onConfirmValue != null)
            _onConfirmValue.Invoke(_amount);
    }

    #endregion
}
