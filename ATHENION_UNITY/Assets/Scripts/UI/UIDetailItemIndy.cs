﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.U2D;
using DG.Tweening;
using TMPro;
using Karamucho.UI;

public class UIDetailItemIndy : MonoBehaviour
{
    #region Public Properties
    public GameObject Group;

    [Header("Reference")]
    public TextMeshProUGUI CardName;
    public TextMeshProUGUI Detail;
    public Image Frame;
    public RectTransform Artwork;

    #endregion

    #region Private Properties
    private CurrencyReward _data;
    #endregion

    #region Calling Function
    public void ShowCard(CurrencyReward data)
    {
        _data = data;
        SetData();
        gameObject.SetActive(true);
    }

    public void HideCard()
    {
        gameObject.SetActive(false);
    }
    #endregion

    #region Data
    public void SetData()
    {
        SetName();
        SetDescription();
        SetFrame();
        SetImage();
    }

    public void SetName()
    {
        string head = LocalizationManager.Instance.GetItemName(_data.ItemID);
        CardName.text = head;
    }

    public void SetDescription()
    {
        string message = _data.Amount.ToString();
        Detail.text = message;
    }

    private void SetFrame()
    {
        Sprite tempSprite;
        CardResourceManager.LoadItemFrame(_data.Rarity, out tempSprite);
        Frame.sprite = tempSprite;
    }

    private void SetImage()
    {
        // Clear previous image
        {
            foreach (Transform child in Artwork.transform)
            {
                Destroy(child.gameObject);
            }
        }

        GameObject obj = null;
        if (CardResourceManager.Create(CardResourceManager.ImageType.InfoItem, _data.BundleID, Artwork.transform, false, out obj))
        {
            obj.transform.localPosition = Vector3.zero;
            obj.transform.localRotation = Quaternion.identity;
            SetMask();
        }
    }

    private void SetMask()
    {
        foreach (Transform child in Artwork.transform)
        {
            Sprite tempSprite;
            CardResourceManager.LoadItemMaskFrame(_data.Rarity, out tempSprite);
            child.GetComponent<Image>().sprite = tempSprite;
        }
    }

    #endregion



}
