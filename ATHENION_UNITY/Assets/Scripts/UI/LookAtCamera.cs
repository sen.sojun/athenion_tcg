﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtCamera : MonoBehaviour {

	// Use this for initialization
	private void Start () {
        transform.rotation = Quaternion.LookRotation(Camera.main.transform.forward);

    }
	

}
