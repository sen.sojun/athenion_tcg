﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;
using UnityEngine.Events;

public class UITutorialPanel : MonoBehaviour
{

    #region Inspector
    private UnityAction<GameMode> _onPlayTutorial;

    [Header("StageCell")]
    [SerializeField] private Slider SLIDER_Progress;
    [SerializeField] private TutorialStageCell TutorialStage_0;
    [SerializeField] private TutorialStageCell TutorialStage_1;
    [SerializeField] private TutorialStageCell TutorialStage_2;
    [SerializeField] private TutorialStageCell TutorialStage_3;

    [Header("BTN")]
    [SerializeField] private Button BTN_Play;
    [SerializeField] private Button BTN_Close;
    [SerializeField] private Button BTN_Skip;
    [SerializeField] private Button BTN_Cancel;

    [Header("Reward")]
    [SerializeField] private GameObject RewardIcon;
    #endregion

    #region Private Properties
    private const float _stageValue_0cp = 0.0f;
    private const float _stageValue_1cp = 0.45f;
    private const float _stageValue_2cp = 1.0f;
    private int _currentStage = -1;
    private bool _isStartPlay = false;
    #endregion

    #region Event

    public static event Action OnInit;
    public static event Action OnQuit;

    #endregion

    #region Methods

    public void ShowUI(bool isFirstTime = false)
    {
        _isStartPlay = false;
        BTN_Skip.gameObject.SetActive(isFirstTime);

        gameObject.SetActive(true);
        InitData();
        UpdateBTN();

        // Check Skip
        BTN_Close.gameObject.SetActive(!isFirstTime);
        OnInit?.Invoke();
    }

    public void SetOnSelectTutorial(UnityAction<GameMode> onClickPlay)
    {
        _onPlayTutorial = onClickPlay;
    }

    public void SetOnSkipEvent(UnityAction onClick)
    {
        BTN_Skip.onClick.RemoveAllListeners();
        BTN_Skip.onClick.AddListener(onClick);
    }

    public void HideUI()
    {
        gameObject.SetActive(false);
        OnQuit?.Invoke();
    }

    private void InitData()
    {
        SelectStage(-1);

        //Set Reward
        RewardIcon.SetActive(!DataManager.Instance.IsCompletedTutorial(GameMode.Tutorial_3));

        if (DataManager.Instance.IsCompletedTutorial(GameMode.Tutorial_3))
        {
            SetProgress(_stageValue_2cp, _stageValue_2cp);
        }
        else if (DataManager.Instance.IsCompletedTutorial(GameMode.Tutorial_2))
        {
            SetProgress(_stageValue_1cp, _stageValue_2cp);
        }
        else if (DataManager.Instance.IsCompletedTutorial(GameMode.Tutorial_1))
        {
            SetProgress(_stageValue_0cp, _stageValue_1cp);
        }
    }

    private void UpdateBTN()
    {
        BTN_Play.onClick.RemoveAllListeners();

        if (_currentStage >= 0)
        {
            BTN_Play.interactable = true;
            BTN_Play.onClick.AddListener(delegate
            {
                if (!_isStartPlay)
                {
                    GameMode mode = GameMode.Tutorial_1;
                    switch (_currentStage)
                    {
                        case 0: mode = GameMode.Tutorial_1; break;
                        case 1: mode = GameMode.Tutorial_2; break;
                        case 2: mode = GameMode.Tutorial_3; break;
                        case 3: mode = GameMode.Tutorial_4; break;
                    }

                    _isStartPlay = true;
                    _onPlayTutorial?.Invoke(mode);
                }
            });
        }
        else
        {
            BTN_Play.interactable = false;
        }

        BTN_Cancel.onClick.RemoveAllListeners();
        BTN_Cancel.onClick.AddListener(delegate
        {
            SelectStage(-1);
        });
    }

    private void SetProgress(float start, float end)
    {
        if (start != end)
        {
            float duration = 1.5f;
            SLIDER_Progress.value = start;
            UpdateAnimationProgress();
            SLIDER_Progress.DOValue(end, duration).OnComplete(delegate
            {
                UpdateAnimationProgress();
            });
        }
        else
        {
            SLIDER_Progress.value = end;
            UpdateAnimationProgress(true);
        }

    }

    private void UpdateAnimationProgress(bool isAllClear = false)
    {
        if (isAllClear)
        {
            TutorialStage_0.Setup(TutorialStageCell.StageState.Complete, delegate
            {
                SelectStage(0);
            });
            TutorialStage_1.Setup(TutorialStageCell.StageState.Complete, delegate
            {
                SelectStage(1);
            });
            TutorialStage_2.Setup(TutorialStageCell.StageState.Complete, delegate
            {
                SelectStage(2);
            });
            TutorialStage_3.Setup(TutorialStageCell.StageState.Complete, delegate
            {
                SelectStage(3);
            });
        }
        else
        {
            if (SLIDER_Progress.value == _stageValue_0cp)
            {
                TutorialStage_0.Setup(TutorialStageCell.StageState.Complete, delegate
                {
                    SelectStage(0);
                });
                TutorialStage_1.Setup(TutorialStageCell.StageState.Lock, delegate
                {
                    //SelectStage(1);
                });
                TutorialStage_2.Setup(TutorialStageCell.StageState.Lock, delegate
                {
                    //SelectStage(2);
                });
                TutorialStage_3.Setup(TutorialStageCell.StageState.Lock, delegate
                {
                    //SelectStage(3);
                });
            }
            else if (SLIDER_Progress.value == _stageValue_1cp)
            {
                TutorialStage_0.Setup(TutorialStageCell.StageState.Complete, delegate
                {
                    SelectStage(0);
                });
                TutorialStage_1.Setup(TutorialStageCell.StageState.Unlock, delegate
                {
                    SelectStage(1);
                });
                TutorialStage_2.Setup(TutorialStageCell.StageState.Lock, delegate
                {
                    //SelectStage(2);
                });
                TutorialStage_3.Setup(TutorialStageCell.StageState.Lock, delegate
                {
                    //SelectStage(3);
                });
            }
            else if (SLIDER_Progress.value == _stageValue_2cp)
            {
                TutorialStage_0.Setup(TutorialStageCell.StageState.Complete, delegate
                {
                    SelectStage(0);
                });
                TutorialStage_1.Setup(TutorialStageCell.StageState.Complete, delegate
                {
                    SelectStage(1);
                });
                TutorialStage_2.Setup(TutorialStageCell.StageState.Unlock, delegate
                {
                    SelectStage(2);
                });
                TutorialStage_3.Setup(TutorialStageCell.StageState.Unlock, delegate
                {
                    SelectStage(3);
                });
            }
        }
    }
    #endregion

    #region Event
    private void SelectStage(int stage)
    {
        _currentStage = stage;
        UpdateBTN();

        switch (_currentStage)
        {
            case 0:
                {
                    TutorialStage_0.SetSelect(true);
                    TutorialStage_1.SetSelect(false);
                    TutorialStage_2.SetSelect(false);
                    break;
                }
            case 1:
                {
                    TutorialStage_0.SetSelect(false);
                    TutorialStage_1.SetSelect(true);
                    TutorialStage_2.SetSelect(false);
                    break;
                }
            case 2:
                {
                    TutorialStage_0.SetSelect(false);
                    TutorialStage_1.SetSelect(false);
                    TutorialStage_2.SetSelect(true);
                    break;
                }
            default:
                {
                    TutorialStage_0.SetSelect(false);
                    TutorialStage_1.SetSelect(false);
                    TutorialStage_2.SetSelect(false);
                    break;
                }
        }
    }

    #endregion
}
