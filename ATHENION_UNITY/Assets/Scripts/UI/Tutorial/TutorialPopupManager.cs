﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Karamucho.UI;
using DG.Tweening;

public class TutorialPopupManager : MonoBehaviour
{
    public static TutorialPopupManager Instance => _instance;
    private static TutorialPopupManager _instance;

    #region Public Properties
    [Header("Reference")]
    public RectTransform PopupParent;
    public Transform GROUP_Overhead;
    public Transform GROUP_Popup;
    public Transform GROUP_Overlay;
    public GameObject Prefab_DynamicPopup;
    public GameObject Prefab_CardUI;
    public GameObject Prefab_TooltipPopup;
    public GameObject AlphaOverlay;
    public GameObject BlackFadeTop;

    public Button BTNShowDynamicPopup;

    [Header("Effect")]
    public RectTransform FX_DPUP_Projectile;
    public RectTransform FX_DPUP_HelpNotice;
    public RectTransform FX_DPUP_HandPointing;
    public RectTransform FX_DPUP_HandPointingOnSlot;
    public RectTransform HandPointing_DefaultPosition;
    public RectTransform FX_DPUP_HighlightHand;
    public RectTransform Help;
    #endregion

    #region Private Properties 
    Tweener _projectileTween = null;
    Dictionary<int, string> _popupDataDict = new Dictionary<int, string>();
    Coroutine _cardUICoroutine;
    Coroutine _hilightFingerAnim;
    int _currentIndex;
    TutorialDynamicPopup _dynamicPopup = null;
    CardUI_DetailHover _cardUIPopup = null;
    TutorialTooltipPopup _autohidePopup = null;
    TutorialCardPopupHighlight _highlighter;
    #endregion

    #region Methods
    private void Awake()
    {
        _instance = this;
    }

    public void CreateDynamicPopup(string id, Vector3 fxPos, bool show = false, UnityAction onComplete = null)
    {
        //Instantiate Prefab_DynamicPopup if it doesn't existed
        if (_dynamicPopup == null)
        {
            GameObject newPopup = Instantiate(Prefab_DynamicPopup, GROUP_Popup);
            _dynamicPopup = newPopup.GetComponent<TutorialDynamicPopup>();
        }

        int index = _popupDataDict.Count;
        _popupDataDict.Add(index, id);

        if (show)
        {
            fxPos = Camera.main.WorldToScreenPoint(Prefab_DynamicPopup.transform.position);
            EnableButton();
            _dynamicPopup.ActiveNewWindow(id, delegate () 
            {
                DisableAlphaOverlay();

                LaunchFXProjectile(fxPos, null);

            } + onComplete);
            _currentIndex = index;
            EnableAlphaOverlay();
        }
        else
        {
            LaunchFXProjectile(fxPos, EnableButton);
            onComplete?.Invoke();
        }
    }

    //No projectile launch, just winky fx at btn;
    public void CreateDynamicPopup(string id, bool show = false, UnityAction onComplete = null)
    {
        //Instantiate Prefab_DynamicPopup if it doesn't existed
        if (_dynamicPopup == null)
        {
            GameObject newPopup = Instantiate(Prefab_DynamicPopup, GROUP_Popup);
            _dynamicPopup = newPopup.GetComponent<TutorialDynamicPopup>();
        }

        if (!BTNShowDynamicPopup.gameObject.activeSelf)
        {
            onComplete += EnableButton;
        }

        int index = _popupDataDict.Count;
        _popupDataDict.Add(index, id);

        if (show)
        {
            _dynamicPopup.ActiveNewWindow(id, delegate () { DisableAlphaOverlay(); LaunchFXProjectile(BTNShowDynamicPopup.transform.position, null, 0.01f); } + onComplete);
            _currentIndex = index;
            EnableAlphaOverlay();
        }
        else
        {
            onComplete?.Invoke();
        }
        LaunchFXProjectile(BTNShowDynamicPopup.transform.position, null, 0.01f);
    }

    public void ShowLastDynamicPopup(UnityAction onComplete = null)
    {
        //Instantiate Prefab_DynamicPopup if it doesn't existed
        if (_dynamicPopup == null)
        {
            GameObject newPopup = Instantiate(Prefab_DynamicPopup, GROUP_Popup);
            _dynamicPopup = newPopup.GetComponent<TutorialDynamicPopup>();
        }

        string lastID = GetLast(out bool isFirst, out bool isLast);
        _dynamicPopup.ShowWindow(lastID, !isFirst, !isLast, onComplete);
        EnableAlphaOverlay();
    }

    public void ShowCardDetailUI(int uniqueID, float showTime, List<TutorialCardPopupHighlight.HighlightType> highlightList, UnityAction onComplete = null)
    {
        //Instantiate Prefab_CardUI if it doesn't existed
        if (_cardUIPopup == null)
        {
            GameObject newPopup = Instantiate(Prefab_CardUI, GROUP_Overlay);
            _cardUIPopup = newPopup.GetComponent<CardUI_DetailHover>();

            _highlighter = newPopup.GetComponentInChildren<TutorialCardPopupHighlight>();

            //Animation
            newPopup.transform.localPosition = new Vector2(newPopup.transform.localPosition.x, -1000);
            newPopup.transform.DOLocalMoveY(0, 1.0f).SetEase(Ease.OutExpo);
        }

        if (GameManager.Instance.FindBattleCard(uniqueID, out BattleCardData card))
        {
            CardUIData cardUIData = new CardUIData(card);

            _cardUIPopup.SetData(cardUIData, true);
            GameHelper.UITransition_FadeIn(_cardUIPopup.gameObject);
            _highlighter.ShowHighlight(highlightList);
            _cardUICoroutine = StartCoroutine(ShowCardUI(showTime, DisableAlphaOverlay + onComplete));

            EnableAlphaOverlay();
        }
        else
        {
            onComplete?.Invoke();
        }
    }

    public void ShowCardUIManual(int uniqueID, UnityAction onComplete = null)
    {
        //Instantiate Prefab_CardUI if it doesn't existed
        if (_cardUIPopup == null)
        {
            GameObject newPopup = Instantiate(Prefab_CardUI, GROUP_Overlay);
            _cardUIPopup = newPopup.GetComponent<CardUI_DetailHover>();

            _highlighter = newPopup.GetComponentInChildren<TutorialCardPopupHighlight>();

        }

        if (GameManager.Instance.FindBattleCard(uniqueID, out BattleCardData card))
        {
            CardUIData cardUIData = new CardUIData(card);

            _cardUIPopup.SetData(cardUIData, true);
            GameHelper.UITransition_FadeIn(_cardUIPopup.gameObject);
            _highlighter.ShowHighlight(null);

            _cardUIPopup.transform.localPosition = Prefab_CardUI.transform.localPosition;
            EnableAlphaOverlay();
        }
        else
        {
            onComplete?.Invoke();
        }
    }

    public void ShowCardUIHighlight(List<TutorialCardPopupHighlight.HighlightType> highlightList)
    {
        if (_cardUIPopup.gameObject.activeSelf && _highlighter != null)
        {
            _highlighter.ShowHighlight(highlightList);
        }
    }

    public void ShowHighlightLabel(List<TutorialCardPopupHighlight.HighlightType> highlightList)
    {
        if (_cardUIPopup.gameObject.activeSelf && _highlighter != null)
        {
            _highlighter.ShowHighlightLabel(highlightList);
        }
    }

    private Sequence _sq = null;
    public void ShowPointingHand(Vector3 startPosition, Vector3 endPosition)
    {
        if (_sq != null && _sq.IsPlaying())
        {
            _sq.Kill();
        }

        float loopDuration = 2.0f;
        Vector2 startPos = Camera.main.WorldToScreenPoint(startPosition);
        Vector2 targetPos = Camera.main.WorldToScreenPoint(endPosition);

        FX_DPUP_HandPointing.gameObject.SetActive(true);

        _sq.Pause();
        _sq.Append(FX_DPUP_HandPointing.DOMove(startPos, 0));
        _sq.Append(FX_DPUP_HandPointing.DOMove(targetPos, loopDuration).SetLoops(-1).SetEase(Ease.InOutSine));
        _sq.Play();
    }

    public void ShowPointingHand(Vector3 endPosition)
    {
        _sq.Kill();
        float loopDuration = 2.0f;
        Vector2 targetPos = Camera.main.WorldToScreenPoint(endPosition);

        FX_DPUP_HandPointing.gameObject.SetActive(true);

        _sq.Pause();
        _sq.Append(FX_DPUP_HandPointing.DOMove(HandPointing_DefaultPosition.position, 0));
        _sq.Append(FX_DPUP_HandPointing.DOMove(targetPos, loopDuration).SetLoops(-1).SetEase(Ease.InOutSine));
        _sq.Play();
    }

    public void ShowPointingHandOnSlot(Vector3 endPosition)
    {
        Vector2 targetPos = Camera.main.WorldToScreenPoint(endPosition);

        FX_DPUP_HandPointingOnSlot.gameObject.SetActive(true);
        FX_DPUP_HandPointingOnSlot.transform.position = endPosition;

    }

    public void HidePointerHandOnSlot()
    {
        FX_DPUP_HandPointingOnSlot.gameObject.SetActive(false);
    }

    public void HidePointingHand()
    {
        _sq.Kill();
        FX_DPUP_HandPointing.gameObject.SetActive(false);
    }

    public void ShowHighlightHand()
    { 
        FX_DPUP_HighlightHand.gameObject.SetActive(true);
        Help.gameObject.SetActive(true);
    }

    public void HideHighlightHand()
    {
        FX_DPUP_HighlightHand.gameObject.SetActive(false);
        Help.gameObject.SetActive(false);
    }

    public void ClearAllHighlight()
    {
        if (_cardUIPopup.gameObject.activeSelf && _highlighter != null)
        {
            _highlighter.ClearAllHighlight();
        }
    }

    public void HideAllCardUIHighlight()
    {
        if (_highlighter != null)
        {
            _highlighter.DisableAllHighlight();
        }
    }

    public void HideCardUI(UnityAction onComplete = null)
    {
        GameHelper.UITransition_FadeOut(_cardUIPopup.gameObject);
        GameHelper.UITransition_FadeOut(AlphaOverlay, onComplete);

        DisableAlphaOverlay();
    }

    public void LaunchFXProjectile(Vector2 startPos, UnityAction onComplete, float time = 1.25f)
    {
        if (_projectileTween != null)
        {
            _projectileTween.Kill();
            _projectileTween = null;
        }

        FX_DPUP_Projectile.position = startPos;
        FX_DPUP_Projectile.gameObject.SetActive(true);
        FX_DPUP_HelpNotice.gameObject.SetActive(false);

        _projectileTween = FX_DPUP_Projectile.DOMove(BTNShowDynamicPopup.transform.position, time)
                                             .OnComplete(delegate ()
                                             {
                                                 FX_DPUP_Projectile.gameObject.SetActive(false);
                                                 FX_DPUP_HelpNotice.gameObject.SetActive(true);
                                                 _projectileTween = null;
                                             })
                                             .SetEase(Ease.InOutSine);
    }

    public void ShowAutoHideTooltip(string msg, float showTime, Vector3 showPos, UnityAction onShowComplete, UnityAction onHideComplete)
    {
        //Instantiate Prefab_TooltipPopup if it doesn't existed
        if (_autohidePopup == null)
        {
            GameObject newPopup = Instantiate(Prefab_TooltipPopup, GROUP_Overhead);
            _autohidePopup = newPopup.GetComponent<TutorialTooltipPopup>();
        }

        _autohidePopup.Show(msg, showTime, showPos, onShowComplete, DisableAlphaOverlay + onHideComplete);
    }

    public void ShowTooltip(string msg, Vector3 showPos, UnityAction onShowComplete)
    {
        //Instantiate Prefab_TooltipPopup if it doesn't existed
        if (_autohidePopup == null)
        {
            GameObject newPopup = Instantiate(Prefab_TooltipPopup, GROUP_Overhead);
            _autohidePopup = newPopup.GetComponent<TutorialTooltipPopup>();
        }

        _autohidePopup.ShowUntilClose(msg, showPos, onShowComplete);
    }

    public void HideTooltip(UnityAction onHideComplete)
    {
        //Instantiate Prefab_TooltipPopup if it doesn't existed
        if (_autohidePopup == null)
        {
            GameObject newPopup = Instantiate(Prefab_TooltipPopup, GROUP_Overhead);
            _autohidePopup = newPopup.GetComponent<TutorialTooltipPopup>();
        }

        _autohidePopup.HidePopup(onHideComplete);
    }

    public void ShowFadeBlack_Top()
    {
        GameHelper.UITransition_FadeIn(BlackFadeTop);
    }

    public void HideFadeBlack_Top()
    {
        GameHelper.UITransition_FadeOut(BlackFadeTop);
    }

    public string GetPrevious(out bool isFirst, out bool isLast)
    {
        _currentIndex--;
        int index = Mathf.Clamp(_currentIndex, 0, _popupDataDict.Count - 1);
        isFirst = index == 0;
        isLast = index == _popupDataDict.Count - 1;

        if (_popupDataDict.ContainsKey(index))
        {
            _currentIndex = index;
            return _popupDataDict[index];
        }

        return null;
    }

    public string GetNext(out bool isFirst, out bool isLast)
    {
        _currentIndex++;
        int index = Mathf.Clamp(_currentIndex, 0, _popupDataDict.Count - 1);
        isFirst = index == 0;
        isLast = index == _popupDataDict.Count - 1;

        if (_popupDataDict.ContainsKey(index))
        {
            _currentIndex = index;
            return _popupDataDict[index];
        }

        return null;
    }

    public string GetLast(out bool isFirst, out bool isLast)
    {
        int index = _popupDataDict.Count - 1;
        isFirst = index == 0;
        isLast = true;

        if (_popupDataDict.ContainsKey(index))
        {
            _currentIndex = index;
            return _popupDataDict[index];
        }

        return null;
    }

    public void EnableAlphaOverlay()
    {
        GameHelper.UITransition_FadeIn(AlphaOverlay);
    }

    public void DisableAlphaOverlay()
    {
        GameHelper.UITransition_FadeOut(AlphaOverlay);
    }

    private void EnableButton()
    {
        if (BTNShowDynamicPopup.gameObject.activeSelf) return;

        BTNShowDynamicPopup.gameObject.SetActive(true);
        //Init button
        BTNShowDynamicPopup.onClick.RemoveAllListeners();
        BTNShowDynamicPopup.onClick.AddListener(
            delegate ()
            {
                ShowLastDynamicPopup(DisableAlphaOverlay);
                HideHighlightHand();
            }
        );
    }

    protected IEnumerator ShowCardUI(float showTime, UnityAction onComplete = null)
    {
        GameHelper.UITransition_FadeIn(AlphaOverlay);

        yield return new WaitForSeconds(showTime);

        HideCardUI(onComplete);

    }

    public void TestAddData()
    {
        string testID = "TU1_DPUP_" + Random.Range(1, 10).ToString();
        //CreateDynamicPopup(testID, Vector3.zero, true, null);
    }
    #endregion
}