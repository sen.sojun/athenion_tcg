﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class BlinkingCanvasGroup : MonoBehaviour
{
    private Sequence _sq;

    private void OnEnable()
    {
        CanvasGroup TargetImage = this.gameObject.GetComponent<CanvasGroup>();
        _sq?.Kill();
        _sq = DOTween.Sequence();

        TargetImage.alpha = 0;
        _sq.Append(TargetImage.DOFade(1.0f, 0.5f));
        _sq.Append(TargetImage.DOFade(0.1f, 0.5f));
        _sq.SetLoops(-1);
    }

    private void OnDisable()
    {
        _sq?.Kill();
    }
}
