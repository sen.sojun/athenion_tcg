﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class UITutorialManager : MonoBehaviour
{
    #region Inspector Properties
    [SerializeField] private Transform Section_Main;
    #endregion

    #region Private Properties
    private List<UITutorialPopup> _listPopup = new List<UITutorialPopup>();
    private UITutorialPopup _currentPopup;
    #endregion

    private void Start()
    {
        InitPopupList();
    }

    #region Init
    private void InitPopupList()
    {
        _listPopup.Clear();

        foreach (Transform child in Section_Main.transform)
        {
            UITutorialPopup cell = child.GetComponent<UITutorialPopup>();
            if (cell != null)
            {
                _listPopup.Add(cell);
            }
            else
            {
                string error = child.name + "Is missing UITutorialPopup component. Plz fix";
                Debug.LogWarning(error);
                PopupUIManager.Instance.ShowPopup_Error("Missing Component", error);
            }
        }
    }
    #endregion

    #region Method
    public void RequestShowPopup(string ID, UnityAction onShowComplete = null, UnityAction onHideComplete = null)
    {
        if (GameManager.Instance.IsEndGame)
        {
            onShowComplete?.Invoke();
            onHideComplete?.Invoke();

            return;
        }

        UITutorialPopup cell = _listPopup.Find(x => x.GetID().ToUpper() == ID.ToUpper());
        if (cell != null)
        {
            _currentPopup = cell;
            _currentPopup.Show(onShowComplete, onHideComplete);
        }
        else
        {
            string error = string.Format("{0} is missing from list. Plz fix!", ID);
            Debug.LogWarningFormat(error);
            PopupUIManager.Instance.ShowPopup_Error("Missing ID", error);
        }
    }

    public void RequestHidePopup(string ID, UnityAction onHideComplete = null)
    {
        UITutorialPopup cell = _listPopup.Find(x => x.GetID().ToUpper() == ID.ToUpper());
        if (cell != null)
        {
            _currentPopup = cell;
            _currentPopup.Hide(onHideComplete);
        }
        else
        {
            string error = string.Format("{0} is missing from list. Plz fix!", ID);
            Debug.LogWarningFormat(error);
            PopupUIManager.Instance.ShowPopup_Error("Missing ID", error);
        }
    }

    public void RequestShowArrow(List<string> tileIDList)
    {
        string key = "T_ArrowOn";
        string tileKey;
        foreach (string item in tileIDList)
        {
            tileKey = string.Format("{0}_{1}", key, item.ToUpper());
            UITutorialPopup cell = _listPopup.Find(x => x.GetID().Contains(tileKey));
            if (cell != null)
            {
                _currentPopup = cell;
                _currentPopup.Show(null, null);
            }
            else
            {
                string error = string.Format("{0} is missing from list. Plz fix!", tileKey);
                Debug.LogWarningFormat(error);
                PopupUIManager.Instance.ShowPopup_Error("Missing ID", error);
            }
        }      
    }

    public void RequestHideAllArrow()
    {
        string key = "T_ArrowOn";
        List<UITutorialPopup> cellList = _listPopup.FindAll(x => x.GetID().ToUpper().Contains(key.ToUpper()));
        foreach (UITutorialPopup item in cellList)
        {
            if (item != null)
            {
                _currentPopup = item;
                _currentPopup.Hide(null);
            }
            else
            {
                string error = string.Format("{0} is missing from list. Plz fix!", item);
                Debug.LogWarningFormat(error);
                PopupUIManager.Instance.ShowPopup_Error("Missing ID", error);
            }
        }
    }
    #endregion

    #region GetMethods
    public UITutorialPopup GetCurrentPopup()
    {
        return _currentPopup;
    }
    #endregion


}
