﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

public class UITutorialPopup_MultiPage : UITutorialPopup
{
    #region Inspector Properties
    [Header("BTN")]
    [SerializeField] private Button BTN_Previous;
    [SerializeField] private Button BTN_Next;
    [SerializeField] private Button BTN_Okay;

    [SerializeField] private Image IMG_Character;
    [SerializeField] private Image IMG_Image;
    [SerializeField] private TextMeshProUGUI TEXT_Head;
    [SerializeField] private TextMeshProUGUI TEXT_Message;
    [SerializeField] private TextMeshProUGUI TEXT_Page;

    [SerializeField] private List<Sprite> CharacterList = new List<Sprite>();
    [SerializeField] private List<Sprite> ImageList = new List<Sprite>();
    [SerializeField] private List<string> LocalizeHeadList = new List<string>();
    [SerializeField] private List<string> LocalizeMessageList = new List<string>();
    #endregion

    #region Privater Properties
    private int _maxPage = 0;
    private int _currentPage = 0;

    #endregion

    #region Methods
    private void Start()
    {
        //debug
        //Show(null, null);
    }

    public override void Show(UnityAction onShowComplete, UnityAction onHideComplete)
    {
        base.Show(onShowComplete, onHideComplete);

        Setup();

        BTN_Okay.onClick.RemoveAllListeners();
        BTN_Okay.onClick.AddListener(OnClick);

        BTN_Previous.onClick.RemoveAllListeners();
        BTN_Previous.onClick.AddListener(PreviousPage);

        BTN_Next.onClick.RemoveAllListeners();
        BTN_Next.onClick.AddListener(NextPage);

    }

    private void OnClick()
    {
        base.Hide(_onHideComplete);
    }

    private void Setup()
    {
        _currentPage = 0;
        _maxPage = LocalizeMessageList.Count;

        UpdatePage();

    }

    private void UpdateSprite()
    {
        IMG_Character.sprite = CharacterList[_currentPage];
        IMG_Image.sprite = ImageList[_currentPage];
    }

    private void UpdateText()
    {
        GameHelper.UITransition_FadeIn(TEXT_Message.gameObject);
        TEXT_Page.text = (_currentPage + 1) + " / " + (_maxPage);
        TEXT_Head.text = LocalizationManager.Instance.GetText(LocalizeHeadList[_currentPage]);
        TEXT_Message.text = LocalizationManager.Instance.GetText(LocalizeMessageList[_currentPage]);
    }

    private void UpdateBTN()
    {
        BTN_Okay.gameObject.SetActive(_currentPage == _maxPage-1);
        BTN_Previous.gameObject.SetActive(_currentPage > 0);
        BTN_Next.gameObject.SetActive(_currentPage < _maxPage-1);
    }
    #endregion

    #region Navigator
    private void UpdatePage()
    {
        UpdateBTN();
        UpdateSprite();
        UpdateText();
    }

    private void NextPage()
    {
        if (_currentPage < _maxPage-1)
        {
            _currentPage++;
            UpdatePage();
        }
    }

    private void PreviousPage()
    {
        if (_currentPage > 0)
        {
            _currentPage--;
            UpdatePage();
        }
    }
    #endregion
}
