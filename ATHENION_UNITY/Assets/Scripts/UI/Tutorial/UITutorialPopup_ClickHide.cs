﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UITutorialPopup_ClickHide : UITutorialPopup
{
    #region Inspector Properties
    [Header("BTN")]
    [SerializeField] private Button BTN_Okay;
    #endregion

    #region Methods
    public override void Show(UnityAction onShowComplete, UnityAction onHideComplete)
    {
        base.Show(onShowComplete, onHideComplete);

        BTN_Okay.onClick.RemoveAllListeners();
        BTN_Okay.onClick.AddListener(OnClick);

    }

    private void OnClick()
    {
        base.Hide(_onHideComplete);
    }
    #endregion
}
