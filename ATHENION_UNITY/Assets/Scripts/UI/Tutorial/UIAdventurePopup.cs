﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIAdventurePopup : MonoBehaviour
{
    [Header("ChapterButtons")]
    [SerializeField] private Button BTN_Prologue;

    [Header("Panel that keep stage cells")]
    [SerializeField] private GameObject StageCells_Panel;
    [SerializeField] private Transform StageCells_Content;
    [SerializeField] private TextMeshProUGUI _TEXT_Title;
    [SerializeField] private TextMeshProUGUI _TEXT_Detail;

    [Header("Cell prefab")]
    [SerializeField] private AdventureStageCell AdventureStageCellPrefab;

    [Header("BTN")]
    [SerializeField] private Button _BTN_CloseStage;
    [SerializeField] private Button _BTN_Exit;

    public void ShowUI()
    {
        TopToolbar.AddSetting(true, true, HideUI);

        InitBTN();
        InitBTNStage();
        GenerateStageCells(DataManager.Instance.GetAdventureDataDict()[AdventureChapterID.AT], StageCells_Content);
        SetStory(AdventureChapterID.AT);
        ShowStagePanel();

        GameHelper.UITransition_FadeIn(this.gameObject);
    }

    public void HideUI()
    {
        TopToolbar.RemoveLatestSetting();
        GameHelper.UITransition_FadeOut(this.gameObject);
    }

    private void InitBTN()
    {
        _BTN_CloseStage.onClick.RemoveAllListeners();
        _BTN_CloseStage.onClick.AddListener(HideStagePanel);

        _BTN_Exit.onClick.RemoveAllListeners();
        _BTN_Exit.onClick.AddListener(HideUI);
    }

    private void InitBTNStage()
    {
        BTN_Prologue.onClick.RemoveAllListeners();
        BTN_Prologue.onClick.AddListener(delegate
        {
            GenerateStageCells(DataManager.Instance.GetAdventureDataDict()[AdventureChapterID.AT], StageCells_Content);
            SetStory(AdventureChapterID.AT);
            ShowStagePanel();
        });
    }

    public void GenerateStageCells(List<AdventureStageData> stageDataList, Transform transform)
    {
        // Clear old Cell
        foreach (var oldCell in StageCells_Content.GetComponentsInChildren<AdventureStageCell>())
        {
            Destroy(oldCell.gameObject);
        }
        // Create new cell
        for (int i = 0; i < stageDataList.Count; i++)
        {
            AdventureStageCell stageCell = Instantiate(AdventureStageCellPrefab, StageCells_Content);
            stageCell.Setup(stageDataList[i], i + 1);
        }
    }

    #region Stage Panel
    public void ShowStagePanel()
    {
        GameHelper.UITransition_FadeIn(StageCells_Panel);
        //_BTN_Exit.gameObject.SetActive(false);
    }

    public void HideStagePanel()
    {
        GameHelper.UITransition_FadeOut(StageCells_Panel);
        //_BTN_Exit.gameObject.SetActive(true);
    }

    private void SetStory(AdventureChapterID chapter)
    {
        string head = LocalizationManager.Instance.GetText("TEXT_ADVENTURE_UI_TITLE_" + chapter.ToString());
        string detail = LocalizationManager.Instance.GetText("TEXT_ADVENTURE_UI_DESCRIPTION_" + chapter.ToString());

        _TEXT_Title.text = head;
        _TEXT_Detail.text = detail;
    }
    #endregion
}
