﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class UITutorialPopup_AutoHide : UITutorialPopup
{
    #region Inspector Properties
    [Header("Auto Hide Duration")]
    [SerializeField] private float Duration = 4;
    #endregion

    #region Methods
    public override void Show(UnityAction onShowComplete, UnityAction onHideComplete)
    {
        base.Show(onShowComplete, onHideComplete);
        StartCoroutine(AutoHideDuration());
    }

    private IEnumerator AutoHideDuration()
    {
        yield return new WaitForSeconds(Duration);
        base.Hide(_onHideComplete);
    }
    #endregion


}
