﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;
using Karamucho.UI;
using DG.Tweening;

public class TutorialTooltipPopup : MonoBehaviour
{
    #region Public Properties
    [Header("Reference")]
    public TextMeshProUGUI TXTMessage;

    public Vector3 OriginalTop = new Vector3(140,375);
    public Vector3 OriginalBottom = new Vector3(-134,-152);
    #endregion

    #region Private Properties
    Coroutine _countdownTimer;
    Sequence _sq;

    #endregion
    
    #region Methods
    public void Show(string localizeCode, float time, Vector3 showPos, UnityAction onShowComplete, UnityAction onHideComplete)
    {
        if(_countdownTimer != null)
        {
            StopCoroutine(_countdownTimer);
        }

        gameObject.transform.localPosition = showPos;

        TXTMessage.text = LocalizationManager.Instance.GetText(localizeCode);

        if(_sq != null)
        {
            _sq.Kill();
        }
        _sq = GameHelper.UITransition_FadeIn(this.gameObject, onShowComplete);

        RectTransform[] allRects = GetComponentsInChildren<RectTransform>();
        foreach (RectTransform rect in allRects)
        {
            LayoutRebuilder.ForceRebuildLayoutImmediate(rect);
        }

        _countdownTimer = StartCoroutine(AutoHideDuration(time, onHideComplete));
    }

    public void ShowUntilClose(string localizeCode, Vector3 showPos, UnityAction onShowComplete)
    {
        if (_countdownTimer != null)
        {
            StopCoroutine(_countdownTimer);
        }

        gameObject.transform.position = showPos;

        TXTMessage.text = LocalizationManager.Instance.GetText(localizeCode);

        if (_sq != null)
        {
            _sq.Kill();
        }
        _sq = GameHelper.UITransition_FadeIn(this.gameObject, onShowComplete);
    }

    public void HidePopup(UnityAction onHideComplete)
    {
        if (_countdownTimer != null)
        {
            StopCoroutine(_countdownTimer);
            _countdownTimer = null;
        }

        if (_sq != null)
        {
            _sq.Kill();
        }
        _sq = GameHelper.UITransition_FadeOut(this.gameObject, onHideComplete);
    }

    private IEnumerator AutoHideDuration(float time, UnityAction onHideComplete)
    {
        yield return new WaitForSeconds(time);

        if (_sq != null)
        {
            _sq.Kill();
        }
        _sq = GameHelper.UITransition_FadeOut(this.gameObject, onHideComplete);
    }
    #endregion
}
