﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class UITutorialPopup : MonoBehaviour
{
    #region Inspector Properties
    [SerializeField] private string PopupID;
    #endregion

    #region Private Properties
    protected UnityAction _onShowComplete;
    protected UnityAction _onHideComplete;
    protected bool _isShow = false;
    #endregion

    public virtual void Show(UnityAction onShowComplete, UnityAction onHideComplete)
    {
        if (!_isShow)
        {
            _isShow = true;
            _onShowComplete = onShowComplete;
            _onHideComplete = onHideComplete;

            GameHelper.UITransition_FadeIn(this.gameObject, _onShowComplete);
        }
        
    }

    public virtual void Hide(UnityAction onHideComplete)
    {
        if (_isShow)
        {
            _isShow = false;
            _onHideComplete = onHideComplete;

            GameHelper.UITransition_FadeOut(this.gameObject, _onHideComplete);
        }
       
    }

    public string GetID()
    {
        return PopupID;
    }

}
