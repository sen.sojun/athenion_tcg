﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class TutorialStageCell : MonoBehaviour
{
    #region ENUM
    public enum StageState
    {
        Lock,
        Unlock,
        Complete
    }
    #endregion

    #region Inspector
    [Header("BTN")]
    [SerializeField] private Button BTN_Select;

    [Header("Ref")]
    [SerializeField] private GameObject IMG_Selecting;
    #endregion

    #region Private Properties
    private StageState _currentStageState;
    private UnityAction _onClick;
    #endregion

    #region Method
    public void Setup(StageState stage, UnityAction onClick)
    {
        _currentStageState = stage;
        _onClick = onClick;

        UpdateAnimationState();
        InitBTN();
    }

    public void SetSelect(bool isSelected)
    {
        IMG_Selecting.SetActive(isSelected);
    }

    private void InitBTN()
    {
        BTN_Select.onClick.RemoveAllListeners();
        BTN_Select.onClick.AddListener(_onClick);
    }
    #endregion

    #region Sound
    public void PlaySound_Unlock()
    {
        SoundManager.PlaySFX(SoundManager.SFX.Tutorial_UnlockStage);
    }
    #endregion


    #region Animation
    private void UpdateAnimationState()
    {
        Animator animator = GetComponent<Animator>();
        string stateName = "";

        switch (_currentStageState)
        {
            case StageState.Lock:
            {
                // DO nothing
                break;
            }

            case StageState.Unlock:
            {
                stateName = "IsUnlock";
                break;
            }

            case StageState.Complete:
            {
                stateName = "IsComplete";
                break;
            }

            default:
            {
                // DO nothing
                break;
            }
        }

        if (stateName != "")
        {
            animator.SetTrigger(stateName);
        }

    }
    #endregion
}
