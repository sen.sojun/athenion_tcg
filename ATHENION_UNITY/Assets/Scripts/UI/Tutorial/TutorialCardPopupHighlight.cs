﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

public class TutorialCardPopupHighlight : MonoBehaviour
{
    public enum HighlightType
    {
        None = 0,
        Soul = 1,
        HP = 2,
        ATK = 3,
        Armor = 4,
        Ability = 5,
        Arrow = 6,
        Rarity
    }

    #region Public Properties
    [Header("Description")]
    public GameObject Des_SoulHighlight;
    public GameObject Des_HPHighlight;
    public GameObject Des_ATKHighlight;
    public GameObject Des_ArmorHighlight;
    public GameObject Des_AbilityHighlight;
    public GameObject Des_ArrowHighlight;
    public GameObject Des_RarityHighlight;

    [Header("Highlight")]
    public GameObject SoulHighlight;
    public GameObject HPHighlight;
    public GameObject ATKHighlight;
    public GameObject ArmorHighlight;
    public GameObject AbilityHighlight;
    public GameObject ArrowHighlight;
    public GameObject RarityHighlight;
    #endregion

    #region Private Properties
    #endregion

    public void ClearAllHighlight()
    {
        DisableAllHighlight();
    }

    public void ShowHighlightLabel(List<HighlightType> highlightList)
    {
        if (highlightList != null)
        {
            foreach (HighlightType highlight in highlightList)
            {
                switch (highlight)
                {
                    default:
                    case HighlightType.None:
                    break;

                    case HighlightType.Soul:
                    GameHelper.UITransition_ZoomFadeIn(Des_SoulHighlight);
                    break;

                    case HighlightType.HP:
                    GameHelper.UITransition_ZoomFadeIn(Des_HPHighlight);
                    break;

                    case HighlightType.ATK:
                    GameHelper.UITransition_ZoomFadeIn(Des_ATKHighlight);
                    break;

                    case HighlightType.Armor:
                    GameHelper.UITransition_ZoomFadeIn(Des_ArmorHighlight);
                    break;

                    case HighlightType.Ability:
                    GameHelper.UITransition_ZoomFadeIn(Des_AbilityHighlight);
                    break;

                    case HighlightType.Arrow:
                    GameHelper.UITransition_ZoomFadeIn(Des_ArrowHighlight);
                    break;

                    case HighlightType.Rarity:
                    GameHelper.UITransition_ZoomFadeIn(Des_RarityHighlight);
                    break;

                }
            }
        }
    }

    public void ShowHighlight(List<HighlightType> highlightList)
    {
        if (highlightList != null)
        {
            foreach (HighlightType highlight in highlightList)
            {
                switch (highlight)
                {
                    default:
                    case HighlightType.None:
                    break;

                    case HighlightType.Soul:
                    SoulHighlight.SetActive(true);
                    break;

                    case HighlightType.HP:
                    HPHighlight.SetActive(true);
                    break;

                    case HighlightType.ATK:
                    ATKHighlight.SetActive(true);
                    break;

                    case HighlightType.Armor:
                    ArmorHighlight.SetActive(true);
                    break;

                    case HighlightType.Ability:
                    AbilityHighlight.SetActive(true);
                    break;

                    case HighlightType.Arrow:
                    ArrowHighlight.SetActive(true);
                    break;

                    case HighlightType.Rarity:
                    RarityHighlight.SetActive(true);
                    break;
                }
            }
        }
    }

    public void DisableAllHighlight()
    {
        SoulHighlight.gameObject.SetActive(false);
        HPHighlight.gameObject.SetActive(false);
        ATKHighlight.gameObject.SetActive(false);
        ArmorHighlight.gameObject.SetActive(false);
        AbilityHighlight.SetActive(false);
        ArrowHighlight.SetActive(false);
        RarityHighlight.SetActive(false);

        Des_SoulHighlight.gameObject.SetActive(false);
        Des_HPHighlight.gameObject.SetActive(false);
        Des_ATKHighlight.gameObject.SetActive(false);
        Des_ArmorHighlight.gameObject.SetActive(false);
        Des_AbilityHighlight.SetActive(false);
        Des_ArrowHighlight.SetActive(false);
        Des_RarityHighlight.SetActive(false);
    }

    private void OnDisable()
    {
        DisableAllHighlight();
    }

}
