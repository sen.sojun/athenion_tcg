﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using Karamucho;
using Karamucho.UI;

public class UITutorialMenu : MonoBehaviour
{
    #region Public Properties
    public Image TutorialImage;

    public Button PrevButton;
    public Button NextButton;

    public int MaxPageIndex = 5 - 1; // 1, 2, 3, 4, ... 
    #endregion

    #region Private Properties
    private static readonly string _rootPath = "Images/Tutorials/";
    private int _index = 0;
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    #region Public Properties
    public void ShowUI()
    {
        _index = 0;

        PrevButton.onClick.RemoveAllListeners();
        PrevButton.onClick.AddListener(PrevPage);
        PrevButton.SendMessage("OnEnable", SendMessageOptions.DontRequireReceiver);

        NextButton.onClick.RemoveAllListeners();
        NextButton.onClick.AddListener(NextPage);
        NextButton.SendMessage("OnEnable", SendMessageOptions.DontRequireReceiver);

        ShowPage(_index);

        gameObject.SetActive(true);
    }

    public void HideUI()
    {
        gameObject.SetActive(false);
    }

    private void ShowPage(int index)
    {
        PrevButton.gameObject.SetActive(_index > 0);
        NextButton.gameObject.SetActive(_index < MaxPageIndex);

        string path = _rootPath + string.Format(
              "tutorial_menu_{0}_1_{1}"
            , ((LocalizationManager.Instance.CurrentLanguage == Language.Thai) ? "th" : "en")
            , (index + 1)
        );

        Sprite sprite = ResourceManager.Load<Sprite>(path);
        TutorialImage.sprite = sprite;
    }

    public void PrevPage()
    {
        _index--;
        if (_index < 0)
        {
            _index = 0;
        }

        ShowPage(_index);
    }

    public void NextPage()
    {
        _index++;
        if (_index > MaxPageIndex)
        {
            _index = MaxPageIndex;
        }

        ShowPage(_index);
    }
    #endregion
}
