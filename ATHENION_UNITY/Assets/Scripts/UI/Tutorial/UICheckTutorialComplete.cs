﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICheckTutorialComplete : MonoBehaviour
{
    bool _isClaim;

    private void OnEnable()
    {
        _isClaim = DataManager.Instance.GetIsClaimTutorialReward();
    }

    private void Update()
    {
       
        if (_isClaim)
        {
            if(gameObject.activeSelf) gameObject.SetActive(false);
        }
        else
        {
            if (!gameObject.activeSelf) gameObject.SetActive(true);
        }
        
    }


}
