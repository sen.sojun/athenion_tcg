﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

public class TutorialDynamicPopup : MonoBehaviour
{
    #region Public Properties

    [Header("Reference")]
    public GameObject GROUP_Frame;
    public Image MainImage;
    public TextMeshProUGUI HeaderText;
    public TextMeshProUGUI DetailText;
    public Button OkayButton;
    public Button LeftArrow;
    public Button RightArrow;
    public RectTransform PanelRect;
    public List<RectTransform> UpdateRectList;
    #endregion

    #region Private Properties
    UnityAction _onHideComplete;

    string ImagePath = "";
    #endregion

    #region Methods
    private void Start()
    {
        OkayButton.onClick.RemoveAllListeners();
        OkayButton.onClick.AddListener(DeactiveWindow);

        LeftArrow.onClick.RemoveAllListeners();
        LeftArrow.onClick.AddListener(ShowPrevious);

        RightArrow.onClick.RemoveAllListeners();
        RightArrow.onClick.AddListener(ShowNext);
    }
    public void ActiveNewWindow(string id, UnityAction onHideComplete = null)
    {
        GameHelper.UITransition_FadeIn(gameObject);
        ShowData(id, false, false);
        _onHideComplete = onHideComplete;
    }
    public void ShowWindow(string id, bool showPrev, bool showNext, UnityAction onHideComplete = null)
    {
        GameHelper.UITransition_FadeIn(gameObject);
        ShowData(id, showPrev, showNext);
        _onHideComplete = onHideComplete;
    }
    public void DeactiveWindow()
    {
        //GameHelper.UITransition_FadeOut(gameObject, _onHideComplete);
        GameHelper.UITransition_Shrink(gameObject, _onHideComplete);
    }
    public void ShowNext()
    {
        string id = TutorialPopupManager.Instance.GetNext(out bool isFirst, out bool isLast);
        ShowData(id, !isFirst, !isLast);
    }
    public void ShowPrevious()
    {
        string id = TutorialPopupManager.Instance.GetPrevious(out bool isFirst, out bool isLast);
        ShowData(id, !isFirst, !isLast);
    }
    public void ShowLast()
    {
        string id = TutorialPopupManager.Instance.GetLast(out bool isFirst, out bool isLast);
        ShowData(id, !isFirst, !isLast);
    }
    public void ToggleNavButton(bool activeLeft, bool activeRight)
    {
        LeftArrow.gameObject.SetActive(activeLeft);
        RightArrow.gameObject.SetActive(activeRight);
    }

    private void ShowData(string id, bool showPrevBTN, bool showNextBTN)
    {
        HeaderText.text = LocalizationManager.Instance.GetText(id + "_HEADER");
        DetailText.text = LocalizationManager.Instance.GetText(id + "_DETAIL");

        Sprite img = SpriteResourceHelper.LoadSprite(id, SpriteResourceHelper.SpriteType.TU_DynamicPopup, true);
        if (img != null)
        {
            MainImage.sprite = img;
            GROUP_Frame.gameObject.SetActive(true);
        }
        else
        {
            GROUP_Frame.gameObject.SetActive(false);
        }

        ToggleNavButton(showPrevBTN, showNextBTN);

        //Force update canvases, so those content size fitters won't bug :D
        //LayoutRebuilder.ForceRebuildLayoutImmediate(PanelRect);
        foreach(RectTransform rect in UpdateRectList)
        {
            LayoutRebuilder.ForceRebuildLayoutImmediate(rect);
        }
    }
    #endregion
}
