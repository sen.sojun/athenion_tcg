﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Animate_PingPongUpDown : MonoBehaviour
{
    public float MinHigh = 50f;
    public float Time = 0.5f;
    private Sequence _sq;

    private void Awake()
    {
        RectTransform rect = gameObject.GetComponent<RectTransform>();
        _sq.Kill();
        _sq = DOTween.Sequence();

        _sq.Append(rect.DOAnchorPosY(rect.anchoredPosition.y - MinHigh, Time)).SetEase(Ease.OutExpo);
        _sq.Append(rect.DOAnchorPosY(rect.anchoredPosition.y, Time));
        _sq.SetLoops(-1);
    }
}
