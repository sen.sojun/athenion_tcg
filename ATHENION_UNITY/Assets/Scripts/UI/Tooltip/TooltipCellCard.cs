﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Karamucho;
using DG.Tweening;
using Karamucho.Collection;

public class TooltipCellCard : MonoBehaviour
{
    [SerializeField] private CardUI_Collection _CardUI;
    [SerializeField] private TextMeshProUGUI _TEXT_Title;
    [SerializeField] private TextMeshProUGUI _TEXT_Detail;

    [Header("Tooltip Keyword")]
    [SerializeField] protected UITooltipKeywordGroup UITooltipKeywordGroup;
    [SerializeField] protected UITooltipKeyword_TriggerAttachment UITooltipKeywordGroup_Trigger;

    private GameObject _currentAnimatedHero = null;

    private Sequence _sq;

    #region Methods
    public void Show(string imgKey, string head, string message)
    {
        SetText(head, message);
        SetImage(imgKey);

        if (_sq != null) _sq.Kill();
        _sq = GameHelper.UITransition_ZoomFadeIn(this.gameObject);
    }

    public void Hide()
    {
        if (_sq != null) _sq.Kill();
        _sq = GameHelper.UITransition_ZoomFadeOut(this.gameObject);
    }
    #endregion

    #region Setup
    private void SetText(string head, string message)
    {
        _TEXT_Title.text = head;
        _TEXT_Detail.text = message;
    }

    private void SetImage(string key)
    {
        CardData cardData = CardData.CreateCard(key);
        CardUIData cardUIData = new CardUIData(-1, cardData, DataManager.Instance.DefaultCardBackIDList[0]);
        _CardUI.SetData(cardUIData);
        _CardUI.SetCardAmount(1);
        UITooltipKeywordGroup.SetData(cardUIData);
        UITooltipKeywordGroup_Trigger.SetKeyword(cardUIData.GetAbilityKeywordCount());
    }
    #endregion
}
