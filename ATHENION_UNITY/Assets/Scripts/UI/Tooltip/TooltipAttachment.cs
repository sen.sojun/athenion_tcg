﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class TooltipAttachment : UIInputEvent
{
    [Header("Key")]
    public string KeyID;
    public bool isFullDetail;

    #region Setup
    public void Setup(string keyID, bool isFull = true)
    {
        KeyID = keyID;
        isFullDetail = isFull;
    }
    #endregion

    #region Event
    public override void OnEventClick(PointerEventData eventData)
    {
        if (isFullDetail)
        {
            TooltipManager.Instance.ShowTooltip_Full(KeyID, eventData.position);
        }
        else
        {
            TooltipManager.Instance.ShowTooltip_TextOnly(KeyID, eventData.position);
        }
    }

    public override void OnEventUnityPointerDown(PointerEventData eventData)
    {
        //if (isFullDetail)
        //{
        //    TooltipManager.Instance.ShowTooltip_Full(KeyID, eventData.position);
        //}
        //else
        //{
        //    TooltipManager.Instance.ShowTooltip_TextOnly(KeyID, eventData.position);
        //}
    }

    public override void OnEventBeginHold(PointerEventData eventData)
    {
        //Debug.Log("Start Hold" + _imgKey);
        //if (isFullDetail)
        //{
        //    TooltipManager.Instance.ShowTooltip_Full(KeyID, eventData.position);
        //}
        //else
        //{
        //    TooltipManager.Instance.ShowTooltip_TextOnly(KeyID, eventData.position);
        //}

    }

    public override void OnEventEndHold(PointerEventData eventData)
    {
        //Debug.Log("Stop Hold" + _imgKey);
        //if (isFullDetail)
        //{
        //    TooltipManager.Instance.HideTooltip_Full();
        //}
        //else
        //{
        //    TooltipManager.Instance.HideTooltip_TextOnly();
        //}

    }
    #endregion

}
