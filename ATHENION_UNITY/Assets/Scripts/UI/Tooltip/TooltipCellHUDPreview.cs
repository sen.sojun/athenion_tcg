﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Karamucho;
using DG.Tweening;

public class TooltipCellHUDPreview : MonoBehaviour
{
    [SerializeField] private Image _IMG_HUD;
    [SerializeField] private TextMeshProUGUI _TEXT_Title;
    [SerializeField] private TextMeshProUGUI _TEXT_Detail;

    private GameObject _currentAnimatedHero = null;

    private Sequence _sq;

    #region Methods
    public void Show(string imgKey, string head, string message)
    {
        SetText(head, message);
        SetImage(imgKey);

        if (_sq != null) _sq.Kill();
        _sq = GameHelper.UITransition_ZoomFadeIn(this.gameObject);
    }

    public void Hide()
    {
        if (_sq != null) _sq.Kill();
        _sq = GameHelper.UITransition_ZoomFadeOut(this.gameObject);
    }
    #endregion

    #region Setup
    private void SetText(string head, string message)
    {
        _TEXT_Title.text = head;
        _TEXT_Detail.text = message;
    }

    private void SetImage(string key)
    {
        _IMG_HUD.sprite = SpriteResourceHelper.LoadSprite(key, SpriteResourceHelper.SpriteType.HUD_SS);

    }
    #endregion
}
