﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Karamucho;
using DG.Tweening;

public class TooltipCellHeroPreview : MonoBehaviour
{
    private readonly string _animtedHeroPath = "Prefabs/AnimatedHero/";
    private readonly string _prefix = "GROUP_";

    [SerializeField] private Transform _GROUP_Character;
    [SerializeField] private TextMeshProUGUI _TEXT_Title;
    [SerializeField] private TextMeshProUGUI _TEXT_Detail;
    [SerializeField] private Button _BTN_Click_Hello;

    private GameObject _currentAnimatedHero = null;
    private OutgameHeroAnimation _hero;
    private string _heroID;

    private Sequence _sq;

    private void Start()
    {
        _BTN_Click_Hello?.onClick.RemoveAllListeners();
        _BTN_Click_Hello?.onClick.AddListener(delegate 
        {
            PlayAnimation(BaseHeroAnimation.HeroAnimationState.GREETING);
        });
    }

    #region Methods
    public void Show(string imgKey, string head, string message)
    {
        _heroID = imgKey;

        SetText(head, message);
        SetImage(imgKey);

        if (_sq != null) _sq.Kill();
        _sq = GameHelper.UITransition_ZoomFadeIn(this.gameObject);

    }

    public void Hide()
    {
        if (_sq != null) _sq.Kill();
        _sq = GameHelper.UITransition_ZoomFadeOut(this.gameObject);

    }
    #endregion

    #region Animation
    private void PlayAnimation(BaseHeroAnimation.HeroAnimationState state)
    {
        if (_hero != null)
        {
            if (_hero.GetIsPlaying()) return;

            SoundHeroData soundHeroData = new SoundHeroData(_heroID, state);
            _hero.PlayAnimation(soundHeroData, state);
        }
    }
    #endregion

    #region Setup
    private void SetText(string head, string message)
    {
        _TEXT_Title.text = head;
        _TEXT_Detail.text = message;
    }

    private void SetImage(string key)
    {
        // Reset
        if (_currentAnimatedHero != null)
        {
            Destroy(_currentAnimatedHero);
            _hero = null;
        }

        // Load Character
        string path = string.Format("{0}{1}{2}", _animtedHeroPath, _prefix, key);
        ResourceManager.LoadAsync<GameObject>(
              path
            , delegate (GameObject prefab)
            {
                if (prefab != null)
                {
                    _currentAnimatedHero = Instantiate(prefab, _GROUP_Character);
                    _currentAnimatedHero.transform.localPosition = Vector3.zero;
                    _hero = _currentAnimatedHero.GetComponent<OutgameHeroAnimation>();
                    _hero.SetHeroID(key);
                }
                else
                {
                    Debug.LogWarningFormat("Cannot find prefab: {0}", path);

                    // Try default prefab
                    path = string.Format("{0}{1}{2}", _animtedHeroPath, _prefix, "H0001");
                    ResourceManager.LoadAsync<GameObject>(
                          path
                        , delegate (GameObject defaultPrefab)
                        {
                            if (defaultPrefab != null)
                            {
                                _currentAnimatedHero = Instantiate(prefab, transform);
                                _currentAnimatedHero.transform.localPosition = Vector3.zero;
                            }
                            else
                            {
                                Debug.LogWarningFormat("Cannot find default prefab: {0}", path);
                            }
                        }
                    );
                }
            }
        );
    }
    #endregion
}
