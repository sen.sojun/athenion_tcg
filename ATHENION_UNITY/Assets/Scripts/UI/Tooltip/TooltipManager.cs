﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TooltipManager : MonoSingleton<TooltipManager>
{
    #region Serialize Properties
    [Header ("Tooltip Ref")]
    [SerializeField] private TooltipCell _Tooltip_FullDetail;
    [SerializeField] private TooltipCellTextOnly _Tooltip_TextOnly;
    [SerializeField] private TooltipCellHeroPreview _Tooltip_HeroPreview;
    [SerializeField] private TooltipCellHUDPreview _Tooltip_HUDPreview;
    [SerializeField] private TooltipCellCard _Tooltip_Card;

    [Header("BTN Close")]
    [SerializeField] private Button _BTN_Close;
    #endregion

    #region Private Properties
    private static readonly string _namePrefix = "ITEM_NAME_";
    private static readonly string _infoPrefix = "ITEM_DESCRIPTION_";
    #endregion

    #region Method
    private void Start()
    {
        InitBTN();
        HideAll();
    }

    private void InitBTN()
    {
        _BTN_Close?.onClick.RemoveAllListeners();
        _BTN_Close?.onClick.AddListener(HideAll);
    }

    private void MoveToPostion(GameObject obj, Vector3 position)
    {
        // Move to pointer position system
        /*
        float percentage = 0.15f;
        float normalOffsetPercentage = 0.02f;
        float offsetBorderX = 0;
        float offsetBorderY = 0;
        // Check X border
        if (position.x > Screen.width * 0.8f)
        {
            offsetBorderX = (-1) * Screen.width * percentage;
        }
        else if(position.x < Screen.width * 0.2f)
        {
            offsetBorderX = (1) * Screen.width * percentage;
        }

        // Check Y border
        if (position.y > Screen.height * 0.8f)
        {
            offsetBorderY = (-1) * Screen.height * percentage * 4;
        }
        else if (position.y < Screen.height * 0.2f) 
        {
            offsetBorderY = 0;
        }

        // Normal offset

        float offsetX = 0;
        float offsetY = Screen.height * normalOffsetPercentage; // percentage offset
        //obj.transform.position = new Vector3(position.x + offsetX + offsetBorderX, position.y + offsetY + offsetBorderY, 0);
        */

        obj.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
    }

    private void EnableCloseBTN(bool isEnable)
    {
        if (_BTN_Close != null)
        {
            if (isEnable)
            {
                GameHelper.UITransition_FadeIn(_BTN_Close.gameObject);
            }
            else
            {
                GameHelper.UITransition_FadeOut(_BTN_Close.gameObject);
            }
           
        }
        
    }

    public void ShowTooltip_Full(string key, Vector3 worldPosition)
    {
        EnableCloseBTN(true);

        string baseKey = key.Split(':')[0];
        string titleText = LocalizationManager.Instance.GetText(_namePrefix + baseKey);
        string detailText = LocalizationManager.Instance.GetText(_infoPrefix + baseKey);

        if (key.StartsWith("HUD"))
        {
            MoveToPostion(_Tooltip_HUDPreview.gameObject, worldPosition);
            _Tooltip_HUDPreview.Show(key, titleText, detailText);
        }
        else if (key.StartsWith("H"))
        {
            MoveToPostion(_Tooltip_HeroPreview.gameObject, worldPosition);
            _Tooltip_HeroPreview.Show(key, titleText, detailText);
        }
        else if (GameHelper.IsCardKey(key))
        {
            MoveToPostion(_Tooltip_Card.gameObject, worldPosition);
            _Tooltip_Card.Show(key, titleText, detailText);
        }
        else
        {
            MoveToPostion(_Tooltip_FullDetail.gameObject, worldPosition);
            _Tooltip_FullDetail.Show(key, titleText, detailText);
        }

    }

    public void HideTooltip_Full()
    {
        _Tooltip_FullDetail.Hide();
        _Tooltip_HeroPreview.Hide();
        _Tooltip_Card.Hide();
        _Tooltip_HUDPreview.Hide();
    }

    public void ShowTooltip_TextOnly(string key, Vector3 worldPosition)
    {
        EnableCloseBTN(true);

        MoveToPostion(_Tooltip_TextOnly.gameObject, worldPosition);
        string titleText = LocalizationManager.Instance.GetText(_namePrefix + key);
        string detailText = LocalizationManager.Instance.GetText(_infoPrefix + key);
        _Tooltip_TextOnly.Show(titleText, detailText);
    }

    public void HideTooltip_TextOnly()
    {
        _Tooltip_TextOnly.Hide();
    }

    public void HideAll()
    {
        _Tooltip_FullDetail.Hide();
        _Tooltip_TextOnly.Hide();
        _Tooltip_HeroPreview.Hide();
        _Tooltip_HUDPreview.Hide();
        _Tooltip_Card.Hide();
        EnableCloseBTN(false);
    }
    #endregion



}
