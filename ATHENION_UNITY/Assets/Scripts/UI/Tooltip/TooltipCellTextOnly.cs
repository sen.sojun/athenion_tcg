﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class TooltipCellTextOnly : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _TEXT_Title;
    [SerializeField] private TextMeshProUGUI _TEXT_Detail;

    private Sequence _sq;

    #region Methods
    public void Show(string head, string message)
    {
        SetText(head, message);

        if (_sq != null) _sq.Kill();
        _sq = GameHelper.UITransition_ZoomFadeIn(this.gameObject);
    }

    public void Hide()
    {
        if (_sq != null) _sq.Kill();
        _sq = GameHelper.UITransition_ZoomFadeOut(this.gameObject);
    }
    #endregion

    #region Setup
    private void SetText(string head, string message)
    {
        _TEXT_Title.text = head;
        _TEXT_Detail.text = message;
    }
    #endregion
}
