﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIHideWhenAtLogin : MonoBehaviour
{
    private void OnEnable()
    {
        gameObject.SetActive(SceneManager.GetActiveScene().name != GameHelper.LoginSceneName);
    }
}
