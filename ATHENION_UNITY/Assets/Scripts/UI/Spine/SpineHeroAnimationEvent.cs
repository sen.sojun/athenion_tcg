﻿using System.Collections;
using System.Collections.Generic;
using Spine;
using UnityEngine;

public class SpineHeroAnimationEvent : SpineAnimationEvent
{
    [SerializeField] GameObject P_Effect1;
    private string _e_eventKey1 = "Cha_Vfx";

    private void Start()
    {
        InitEvent();
    }

    protected override void EventHandle(TrackEntry trackEntry, Spine.Event e)
    {
        TriggerEvent(_e_eventKey1, e, Event_Glitter01);
    }

    private void Event_Glitter01()
    {
        P_Effect1.SetActive(true);
    }
}
