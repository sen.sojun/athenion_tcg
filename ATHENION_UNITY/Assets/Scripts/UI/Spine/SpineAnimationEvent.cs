﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;
using Spine;
using UnityEngine.Events;

public abstract class SpineAnimationEvent : MonoBehaviour
{
    [Header("Skeleton Graphic")]
    [SerializeField] private SkeletonGraphic PackSkeletonGraphic;

    private Spine.AnimationState _animationState;
    private bool _isInitAnimation = false;
    private UnityAction _onOpenPackComplete;

    protected abstract void EventHandle(TrackEntry trackEntry, Spine.Event e);

    protected void TriggerEvent(string eventName, Spine.Event e, UnityAction callback)
    {
        if (e.Data.Name == eventName)
        {
            callback?.Invoke();
        }
    }

    protected void InitEvent()
    {
        _animationState = PackSkeletonGraphic.AnimationState;

        if (!_isInitAnimation)
        {
            _animationState.Event += EventHandle;
            _isInitAnimation = true;
        }
    }
}
