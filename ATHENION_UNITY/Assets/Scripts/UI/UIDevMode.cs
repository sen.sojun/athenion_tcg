﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIDevMode : MonoBehaviour
{
    private void Awake()
    {
        gameObject.SetActive(GameHelper.IsATNVersion_RealDevelopment);
    }
}
