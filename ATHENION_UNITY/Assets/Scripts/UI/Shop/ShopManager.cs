﻿using Karamucho;
using Newtonsoft.Json;
using PlayFab;
using PlayFab.ClientModels;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Purchasing;
using UnityEngine.SceneManagement;

public class ShopManager : OutGameUIBase
{
    public override string SceneName { get { return GameHelper.ShopSceneName; } }
    #region Event  
    public static event Action<PackReward> OnOpenPack;

    public static event Action<DealRecord> OnClaimDailyFree;

    public static event Action<ShopItemData> OnPurchaseComplete;
    public static event Action<ShopItemData> OnPurchaseFail;
    public static event Action<ShopItemData> OnPurchaseCancel;

    public static event Action OnPurchasePremium;

    public static event Action OnQuit;
    public static event Action OnInit;
    public static event Action OnShowPage;

    public ShopItemPage CurrentPage { get; private set; }
    public override bool IsCanUnloadResourceAfterUnloadScene => false;
    #endregion

    #region Struct

    #endregion

    #region Constant
    public const string ImagePath = "Images/Shops/";

    public const string ItemIDPrefix = "com.zerobit.athenion.";
    public const string ItemConfigDescriptionPrefix = "SHOP_CONFIG_DESCRIPTION";

    public const string ItemPromotionImagePrefix = "SHOP_PROMOTION_IMG";
    public const string ItemPromotionIconPrefix = "SHOP_PROMOTION_ICON";

    public const string ItemBoosterPackImagePrefix = "SHOP_BOOSTERPACK_IMG";
    public const string ItemBoosterPackBannerImagePrefix = "SHOP_BOOSTERPACK_IMG_BANNER";

    public const string ItemDiamondImagePrefix = "SHOP_DIAMOND_IMG";
    public const string ItemCosmeticImagePrefix = "SHOP_COSMETIC_IMG";

    public const string ItemMiscImagePrefix = "SHOP_MISC_IMG";
    public const string ItemSubscriptionImagePrefix = "SHOP_SUBSCRIPTION_IMG";

    public const string ItemDailyDealImagePrefix = "SHOP_DAILYDEAL_IMG";
    public const string ItemFreeGiftImagePrefix = "SHOP_FREEGIFT_IMG";
    public const string ItemWeeklyDealImagePrefix = "SHOP_WEEKLYDEAL_IMG";

    public const string ItemWildOfferPrefix = "SHOP_WILDOFFER_IMG";

    // Localize 
    public const string ShopDailyErrorAleadyBuy_LocalizeKey = "SHOP_DAILY_ERROR_ALREADY_BUY";
    public const string ShopDailyErrorNotFoundItem_LocalizeKey = "SHOP_DAILY_ERROR_NOT_FOUND_ITEM";
    public const string ShopDailyErrorNotFoundPrice_LocalizeKey = "SHOP_DAILY_ERROR_NOT_FOUND_PRICE";
    public const string ShopDailyErrorUnknown_LocalizeKey = "SHOP_DAILY_ERROR_UNKNOWN";

    public const string ShopFreeErrorAleadyClaim_LocalizeKey = "SHOP_FREE_ERROR_UNKNOWN";
    public const string ShopFreeErrorUnknown_LocalizeKey = "SHOP_FREE_ERROR_UNKNOWN";

    public const string PRICE_INVALID_TEXT = "Not Available";

    //public const string CATALOG_STORE_KEY = "store_data";
    #endregion

    #region Public Properties

    private void OnEnable()
    {
        CardPackController.OnStartSystem += CardPackController_OnStartSystem;
        CardPackController.OnObjectDestroy += CardPackController_OnObjectDestroy;
    }

    private void OnDisable()
    {
        CardPackController.OnStartSystem -= CardPackController_OnStartSystem;
        CardPackController.OnObjectDestroy -= CardPackController_OnObjectDestroy;
    }

    private void CardPackController_OnStartSystem(PackReward packDetail)
    {
        NavigatorController.Instance.SetActiveUI(false);

        CurrentPage.HideUI();
        UIManager.HideUI();
    }

    private void CardPackController_OnObjectDestroy()
    {
        NavigatorController.Instance.SetActiveUI(true);

        CurrentPage.ShowUI();
        UIManager.ShowUI();
    }

    public static ShopManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<ShopManager>();
                DontDestroyOnLoad(_instance.gameObject);
            }
            return _instance;
        }
    }
    private static ShopManager _instance;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            DestroyImmediate(gameObject);
            return;
        }
        else if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
    }

    public override void OnInitialize(UnityAction onComplete = null)
    {
        UIManager = GameObject.FindObjectOfType<ShopUIManager>();

        UIManager.ShowMidLoading();

        UIManager.Initialize();
        UIManager.SetMenuEventCallback(ShowUI);

        StartCoroutine(LoadData(onComplete));
        OnInit?.Invoke();
    }
    public ShopItemData CacheLastItem { get; private set; }
    public ShopUIManager UIManager;

    #endregion

    #region Private Properties 
    [SerializeField]
    private List<ShopItemPage> _pageList = new List<ShopItemPage>();
    #endregion

    #region LoadShopData
    private IEnumerator LoadData(UnityAction onComplete = null)
    {
        StartCoroutine(RefreshWeeklyDeal());
        StartCoroutine(RefreshDailyDeal());
        StartCoroutine(RefreshFreeDeal());

        yield return InitializeIAP();

        UIManager.HideMidLoading();
        onComplete?.Invoke();
    }

    /// <summary>
    /// Clear and Generate all button with new data.
    /// </summary>
    /// <param name="data"></param>
    public void UpdatePageDataAndRefreshPage(ShopTypeData data)
    {
        if (data == null) return;

        if (data.ShopType == ShopTypes.DailyDeal || data.ShopType == ShopTypes.WeeklyDeal) return; // Ignore daily deal shop.

        if (CurrentPage != null)
        {
            CurrentPage.OnExit();
        }
        ShopItemPage page = GetPage(data.ShopType);
        CurrentPage = page;
        CurrentPage.Initialize();
    }

    public ShopItemPage GetPage(ShopTypes type)
    {
        return _pageList.Find(page => page.Type == type);
    }

    private IEnumerator RefreshWeeklyDeal(UnityAction onComplete = null)
    {
        bool isFinish = false;
        DateTime endDate = Convert.ToDateTime(DataManager.Instance.GetWeeklyDealRecord().TimeExpireStr).ToUniversalTime();
        if (DateTimeData.GetDateTimeUTC() > endDate)
        {
            DataManager.Instance.RefreshWeeklyDeal(() => isFinish = true, (err) => isFinish = true);
        }
        else
        {
            isFinish = true;
        }

        yield return new WaitUntil(() => isFinish);
        onComplete?.Invoke();
    }

    private IEnumerator RefreshDailyDeal(UnityAction onComplete = null)
    {
        bool isFinish = false;
        DateTime endDate = Convert.ToDateTime(DataManager.Instance.GetDailyDealRecord().TimeExpireStr).ToUniversalTime();
        if (DateTimeData.GetDateTimeUTC() > endDate)
        {
            DataManager.Instance.RefreshDailyDeal(() => isFinish = true, (err) => isFinish = true);
        }
        else
        {
            isFinish = true;
        }

        yield return new WaitUntil(() => isFinish);
        onComplete?.Invoke();
    }

    private IEnumerator RefreshFreeDeal(UnityAction onComplete = null)
    {
        bool isFinish = false;
        DateTime endDate = Convert.ToDateTime(DataManager.Instance.GetFreeDealRecord().TimeExpireStr).ToUniversalTime();
        if (DateTimeData.GetDateTimeUTC() > endDate)
        {
            DataManager.Instance.RefreshFreeDeal(() => isFinish = true, (err) => isFinish = true);
        }
        else
        {
            isFinish = true;
        }

        yield return new WaitUntil(() => isFinish);
        onComplete?.Invoke();
    }

    private IEnumerator InitializeIAP()
    {
        bool isFinish = false;

        IAPManager.Instance.InitializePurchasing(delegate (bool isSuccess, string error)
        {
            if (isSuccess == false)
            {
                PopupUIManager.Instance.ShowPopup_Error("IAP ERROR", error);
            }
            Debug.Log("IAPManager Init success : " + isSuccess);
            isFinish = true;
        });

        yield return new WaitUntil(() => isFinish);
    }

    #endregion

    #region Methods
    public void ShowUI(ShopTypes shopType)
    {
        if (CurrentPage != null && CurrentPage.Type == shopType)
            return;
        RefreshData(shopType);
    }

    private void RefreshData(ShopTypes shopType)
    {
        ShopTypeData data = DataManager.Instance.GetShopTypeData(shopType);
        UpdatePageDataAndRefreshPage(data);

        UIManager.UpdateTopbarTap(shopType);
        HideAllPage();
        ShopItemPage page = GetPage(shopType);
        page.ShowUI();

        OnShowPage?.Invoke();
        Debug.Log("OpenPage : " + data.GetDebugText());
    }

    private void HideAllPage()
    {
        foreach (ShopItemPage page in _pageList)
        {
            page.HideUI();
        }
    }

    public static string GetPriceText(ShopItemData data)
    {
        if (data != null)
        {
            if (data.Currency == VirtualCurrency.RM)
            {
                if (IAPManager.Instance.PriceStringList != null)
                {
                    if (IAPManager.Instance.PriceStringList.ContainsKey(data.ItemId))
                    {
                        return string.Format("{0}", IAPManager.Instance.PriceStringList[data.ItemId]);
                    }
                }
            }
            else
            {
                return string.Format("{0} {1}", data.Price.ToString("N0"), GameHelper.TMPro_GetEmojiCurrency(data.Currency));
            }
        }

        return PRICE_INVALID_TEXT;
    }

    public static string GetPriceCurrency(ShopItemData data)
    {
        if (data.Currency == VirtualCurrency.RM)
        {
            if (IAPManager.Instance.PriceStringList != null)
            {
                if (IAPManager.Instance.PriceStringList.ContainsKey(data.ItemId))
                {
                    return IAPManager.Instance.PriceCurrencyStringList[data.ItemId];
                }
            }
        }
        else
        {
            return GameHelper.TMPro_GetEmojiCurrency(data.Currency);
        }
        return PRICE_INVALID_TEXT;
    }

    public string GetLimitText(string itemID, ShopOption option)
    {
        if (option.MaxCount > 0)
        {
            int amount = DataManager.Instance.InventoryData.GetAmountByItemID(itemID);
            return string.Format(LocalizationManager.Instance.GetText("SHOP_CONFIG_DESCRIPTION_PURCHASE_BY_COUNT"), option.MaxCount - amount, option.MaxCount);
        }
        else
            return "";
    }

    public static Sprite LoadPackBanner(string packID)
    {
        string path = string.Format("{0}_{1}", ItemBoosterPackBannerImagePrefix, packID);
        string fullPath = ImagePath + path;
        Sprite sprite = ResourceManager.Load<Sprite>(fullPath);
        if (sprite == null)
        {
            sprite = SpriteResourceHelper.LoadSprite();
            Debug.LogWarning("Missing Sprite path : " + fullPath);
        }

        return sprite;
    }

    public static Sprite LoadImageByItemKey(ShopTypes type, string itemKey)
    {
        string path = "";
        switch (type)
        {
            case ShopTypes.Promotion:
                path = string.Format("{0}_{1}", ItemPromotionImagePrefix, itemKey);
                break;
            case ShopTypes.BoosterPack:
                path = string.Format("{0}_{1}", ItemBoosterPackImagePrefix, itemKey);
                break;
            case ShopTypes.Diamond:
                path = string.Format("{0}_{1}", ItemDiamondImagePrefix, itemKey);
                break;
            case ShopTypes.Cosmetic:
                path = string.Format("{0}_{1}", ItemCosmeticImagePrefix, itemKey);
                break;
            case ShopTypes.Miscellaneous:
                path = string.Format("{0}_{1}", ItemMiscImagePrefix, itemKey);
                break;
            case ShopTypes.Subscription:
                path = string.Format("{0}_{1}", ItemSubscriptionImagePrefix, itemKey);
                break;
            case ShopTypes.DailyDeal:
                path = string.Format("{0}_{1}", ItemDailyDealImagePrefix, itemKey);
                break;
            case ShopTypes.WeeklyDeal:
                path = string.Format("{0}_{1}", ItemWeeklyDealImagePrefix, itemKey);
                break;
            case ShopTypes.WildOffer:
                path = string.Format("{0}_{1}", ItemWildOfferPrefix, itemKey);
                break;
        }

        string fullPath = ImagePath + path;
        Sprite sprite = ResourceManager.Load<Sprite>(fullPath);
        if (sprite == null)
        {
            sprite = SpriteResourceHelper.LoadSprite();
            Debug.LogWarning("Missing Sprite path : " + fullPath);
        }

        return sprite;
    }

    public override void OnExit()
    {
        if (CurrentPage != null)
        {
            CurrentPage.OnExit();
        }

        OnQuit?.Invoke();
    }

    #endregion

    #region Shop Global Event

    public void InvokeOnClaimDailyFree(DealRecord deal)
    {
        OnClaimDailyFree?.Invoke(deal);
    }

    public void InvokeOnOpenPack(PackReward result)
    {
        OnOpenPack?.Invoke(result);
    }

    public void InvokeOnPurchasePremium()
    {
        OnPurchasePremium?.Invoke();
    }

    public void InvokeOnPurchaseComplete(ShopItemData data)
    {
        OnPurchaseComplete?.Invoke(data);
    }
    public void InvokeOnPurchaseFail(ShopItemData data)
    {
        OnPurchaseFail?.Invoke(data);
    }
    public void InvokeOnPurchaseCancel(ShopItemData data)
    {
        OnPurchaseCancel?.Invoke(data);
    }

    #endregion

    #region Purchase Process

    /// <summary>
    /// Start Purchase Process.
    /// </summary>
    /// <param name="data"></param>
    /// <param name="onComplete"></param>
    /// <param name="ExtraTask">Task that process before update inventory that always invoke onComplete itself.</param>
    public void RequestPurchaseItem(ShopItemData data, UnityAction<PurchaseResult> onComplete, UnityAction<UnityAction> ExtraTask = null)
    {
        CacheLastItem = data;
        StartCoroutine(RequestPurchaseItemProcess(data, onComplete, ExtraTask));
    }

    public static IEnumerator RequestPurchaseItemProcess(ShopItemData data, UnityAction<PurchaseResult> onComplete, UnityAction<UnityAction> ExtraTask = null)
    {
        // Check Session is valid
        bool isSessionValid = false;
        yield return DataManager.Instance.IECheckSessionValid((isValid) => isSessionValid = isValid);
        if (isSessionValid == false)
        {
            NavigatorController.Instance.Logout();
            yield break;
        }

        if (data.IsTimeValid == false)
        {
            PurchaseResult failResult = new PurchaseResult(data) { ErrorCode = "ERROR_SHOP_INVALID_TIME" };
            onComplete?.Invoke(failResult);
            yield break;
        }

        // Check can buy limited item.
        if (data.IsAtLimitAmount)
        {
            bool isItemExist = DataManager.Instance.InventoryData.IsContainItem(data.ItemId);
            PlayFabError error = new PlayFabError() { Error = PlayFabErrorCode.InvalidRequest };
            PurchaseResult failResult = new PurchaseResult(data) { PlayfabError = error };
            onComplete?.Invoke(failResult);
            yield break;

        }

        if (data.IsInAppPurchase)
        {
            if (GameHelper.ATNVersion == ATNVersion.PreProduction)
            {
                PurchaseResult failResult = new PurchaseResult(data) { ErrorCode = "ERROR_SERVER_INVALID" };
                onComplete?.Invoke(failResult);
                yield break;
            }
            else
            {
                InAppPurchaseProcess(data, onComplete, ExtraTask);
            }
        }
        else // purchase via PLAYFAB API.
        {
            bool isEnoughMoney = (DataManager.Instance.InventoryData.GetVirtualCurrency(data.Currency) >= data.Price);
            if (!isEnoughMoney)
            {
                onComplete?.Invoke(new PurchaseResult(data) { IsNoMoney = true });
            }
            else
            {
                PlayfabPurchaseProcess(data, onComplete, ExtraTask);
            }
        }
    }

    private static void InAppPurchaseProcess(ShopItemData data, UnityAction<PurchaseResult> onComplete, UnityAction<UnityAction> ExtraTask = null)
    {
        UnityAction<UnityAction> _extraTask;
        if (ExtraTask != null)
        {
            _extraTask = ExtraTask;
        }
        else
        {
            _extraTask = delegate (UnityAction complete) { complete?.Invoke(); };
        }

        IAPManager.Instance.BuyProductID(
              data.ItemId
            , delegate (Product product)
            {
                PurchaseResult purchaseResult = new PurchaseResult()
                {
                    PurchaseItemData = data,
                    PurchaseCurrency = VirtualCurrency.RM,
                    PurchaseProduct = product
                };

                try
                {
#if UNITY_ANDROID
                    {
                        string currencyCode = product.metadata.isoCurrencyCode;
                        uint purchasePrice = (uint)(product.metadata.localizedPrice * 100);
                        var googleReceipt = GooglePurchase.FromJson(product.receipt);
                        string receiptJson = googleReceipt.PayloadData.json;
                        string signature = googleReceipt.PayloadData.signature;

                        GameAnalyticsSDK.GameAnalytics.NewBusinessEventGooglePlay(
                              product.metadata.isoCurrencyCode
                            , (int)(product.metadata.localizedPrice * 100)
                            , data.ShopType.ToString()
                            , data.ItemId
                            , "InAppPurchase"
                            , receiptJson
                            , signature
                        );
                    }
#elif UNITY_IOS
                    {
                        string currencyCode = product.metadata.isoCurrencyCode;
                        int purchasePrice = (int)(product.metadata.localizedPrice * 100);
                        string receiptData = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string>>(product.receipt)["Payload"];

                        GameAnalyticsSDK.GameAnalytics.NewBusinessEventIOS(
                              product.metadata.isoCurrencyCode
                            , (int)(product.metadata.localizedPrice * 100)
                            , data.ShopType.ToString()
                            , data.ItemId
                            , "InAppPurchase"
                            , receiptData
                        );
                    }
#endif
                }
                catch (System.Exception e)
                {
                    Debug.LogWarningFormat("Failed to update stat to GameAnalytics. {0}", e.Message);
                }

                DataManager.Instance.UpdatePurchaseLog(purchaseResult.PurchaseItemData, null);

                // Refresh Inventory
                UnityAction RefreshInventory = delegate ()
                {
                    DataManager.Instance.LoadInventory(
                        delegate ()
                        {
                            onComplete?.Invoke(purchaseResult);
                        }
                        , delegate (string errorCode)
                        {
                            purchaseResult.ErrorCode = errorCode;

                            // Refresh inventory failed.
                            onComplete?.Invoke(purchaseResult);
                        }
                    );
                };

                if (data.IsLimited)
                {
                    DataManager.Instance.RequestPurchaseLimitedItem(data.ItemId, () =>
                    {
                        _extraTask?.Invoke(() =>
                        {
                            RefreshInventory?.Invoke();
                        });
                    });
                }
                else
                {
                    _extraTask?.Invoke(() =>
                    {
                        RefreshInventory?.Invoke();
                    });
                }
            }
            , delegate (IAPError error)
            {
                PurchaseResult purchaseResult = new PurchaseResult()
                {
                    PurchaseItemData = data,
                    PurchaseCurrency = VirtualCurrency.RM,
                    IAPError = error
                };

                onComplete?.Invoke(purchaseResult);
            }
        );
    }

    private static void PlayfabPurchaseProcess(ShopItemData data, UnityAction<PurchaseResult> onComplete, UnityAction<UnityAction> ExtraTask = null)
    {
        UnityAction<UnityAction> _extraTask;
        if (ExtraTask != null)
        {
            _extraTask = ExtraTask;
        }
        else
        {
            _extraTask = delegate (UnityAction complete) { complete?.Invoke(); };
        }

        PlayFabManager.Instance.PurchaseStoreItems(
              PlayFabManager.CatalogMainID
            , data.StoreID
            , data.ItemId
            , data.Currency.ToString()
            , data.Price
            , delegate (PurchaseItemResult result)
            {
                _extraTask?.Invoke(
                    delegate ()
                    {
                        DataManager.Instance.LoadInventory(
                            delegate ()
                            {
                                Debug.Log("load inventory complete");

                                onComplete?.Invoke(new PurchaseResult()
                                {
                                    PlayFabItemResult = result,
                                    PurchaseItemData = data,
                                    PurchaseCurrency = data.Currency
                                });
                            },
                            delegate (string errorCode)
                            {
                                onComplete?.Invoke(new PurchaseResult()
                                {
                                    PlayFabItemResult = result,
                                    PurchaseItemData = data,
                                    PurchaseCurrency = data.Currency,
                                    ErrorCode = errorCode
                                });
                            }
                        );
                    }
                );
            }
            , delegate (PlayFabError error)
            {
                onComplete?.Invoke(new PurchaseResult()
                {
                    PlayfabError = error,
                    PurchaseItemData = data,
                    PurchaseCurrency = data.Currency
                });
            }
        );
    }
    #endregion
}

public class PurchaseResult
{
    #region Public Properties 
    // Playfab result
    public PurchaseItemResult PlayFabItemResult;

    // Data
    public ShopItemData PurchaseItemData;
    public VirtualCurrency PurchaseCurrency;

    // Receipt
    public Product PurchaseProduct = null;

    // Error
    public bool HasError { get { return ErrorCode != null || IsNoMoney || IAPError.HasError || PlayfabError != null; } }
    public bool IsNoMoney;
    public IAPError IAPError;
    public PlayFabError PlayfabError;
    public string ErrorCode = null;
    #endregion

    #region Constructors
    public PurchaseResult()
    {

    }

    public PurchaseResult(ShopItemData data)
    {
        this.PurchaseItemData = data;
        this.PurchaseCurrency = data.Currency;
    }
    #endregion

    #region Methods
    public string GetErrorText()
    {
        if (IsNoMoney)
        {
            return string.Format(
                  LocalizationManager.Instance.GetText("SHOP_PURCHASE_FAIL_NO_MONEY")
                , GameHelper.GetLocalizeCurrencyText(PurchaseCurrency)
            );
        }
        else if (ErrorCode != null && ErrorCode.Length > 0)
        {
            return ErrorCode;
        }
        else if (PlayfabError != null)
        {
            string errorMsg = "";
            switch (PlayfabError.Error)
            {
                default:
                    {
                        errorMsg = string.Format("{0} (Error:{1})", PlayfabError.ErrorMessage, (int)PlayfabError.Error);
                    }
                    break;
            }

            return errorMsg;
        }
        else
        {
            return IAPError.ToString();
        }
    }
    #endregion
}
