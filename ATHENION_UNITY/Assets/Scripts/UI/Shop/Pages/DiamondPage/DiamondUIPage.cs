﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class DiamondUIPage : ShopPageUI
{
    [Header("Prefab")]

    [SerializeField] private LongVIPShopItemUICell _vipPrefab;

    [SerializeField] private TextMeshProUGUI _limitedTimeText;
    [SerializeField] private ShopItemUICell _limitedPrefab;

    [SerializeField] private ShopItemUICell _diamondPrefab;

    [Header("SpawnPoint")]

    [SerializeField] private Transform _limitedSpawnPoint;
    [SerializeField] private Transform _highValueSpawnPoint;
    [SerializeField] private Transform _subscriptionSpawnPoint;
    [SerializeField] private Transform _lowValuespawnPoint;

    private List<GameObject> _shopUIElementList = new List<GameObject>();
    private UnityAction<ShopItemData> _onClickBuy;


    private DiamondPage _page;
    private ShopData _subscriptionShop;

    private Coroutine _limitedTimer;

    public void Initialize(DiamondPage page, ShopData subscriptionShop)
    {
        this._page = page;
        this._subscriptionShop = subscriptionShop;

        ClearItemList();
        // create subscription shop ui.
        CreateSubscriptionItem(_subscriptionShop.ShopItemDataList);

        ShopData diamondShopData = page.Data.ShopDict[page.Data.StoreID[0]];
        CreateButtons(diamondShopData.ShopItemDataList);

        base.UpdateLayout();
    }

    public void ClearItemList()
    {
        if (_shopUIElementList != null && _shopUIElementList.Count > 0)
        {
            foreach (GameObject button in _shopUIElementList)
            {
                DestroyImmediate(button.gameObject);
            }

            _shopUIElementList.Clear();
        }
    }

    public void CreateButtons(List<ShopItemData> itemList)
    {
        if (itemList == null) return;

        for (int i = 0; i < itemList.Count; i++)
        {
            ShopItemData item = itemList[i];
            //if (item.IsTimeValid == false)
            //    continue;
            Transform parent = GetSpawnPoint(i, item);
            ShopItemUICell button = Instantiate(GetShopItemUICellPrefab(i, item), parent).GetComponent<ShopItemUICell>();
            button.SetData(item);
            button.BuyButton.onClick.RemoveAllListeners();
            button.BuyButton.onClick.AddListener(delegate
            {
                _onClickBuy?.Invoke(item);
            });
            _shopUIElementList.Add(button.gameObject);
        }
    }

    public void CreateSubscriptionItem(List<ShopItemData> itemList)
    {
        if (itemList == null) return;

        foreach (ShopItemData item in itemList)
        {
            //if (item.IsTimeValid == false)
            //    continue;

            LongVIPShopItemUICell button = Instantiate(_vipPrefab.gameObject, _subscriptionSpawnPoint).GetComponent<LongVIPShopItemUICell>();
            button.SetData(item);
            button.BuyButton.onClick.RemoveAllListeners();
            button.BuyButton.onClick.AddListener(delegate
            {
                _onClickBuy?.Invoke(item);
            });

            if (button.MoreButton != null)
            {
                button.MoreButton.onClick.RemoveAllListeners();
                button.MoreButton.onClick.AddListener(delegate ()
                {
                    _onClickBuy?.Invoke(item);
                });
            }
            _shopUIElementList.Add(button.gameObject);
        }
    }

    public override void SetEventOnClickBuyItem(UnityAction<ShopItemData> OnClickBuy)
    {
        _onClickBuy = OnClickBuy;
    }

    private GameObject GetShopItemUICellPrefab(int dataIndex, ShopItemData item)
    {
        if (item.IsLimited)
        {
            return _limitedPrefab.gameObject;
        }
        return _diamondPrefab.gameObject;
    }

    private Transform GetSpawnPoint(int dataIndex, ShopItemData item)
    {
        if (item.IsLimited)
        {
            return _limitedSpawnPoint;
        }
        else if (dataIndex < 6)
        {
            return _highValueSpawnPoint;
        }
        else
        {
            return _lowValuespawnPoint;
        }
    }

    public override void ShowUI()
    {
        if (gameObject.activeSelf)
            return;
        gameObject.SetActive(true);
        ScrollRect.onValueChanged.AddListener(OnScrolling);
        _limitedTimer = StartCoroutine(UpdateLimitedTimer());
        base.UpdateLayout();
    }

    public override void HideUI()
    {
        gameObject.SetActive(false);
        ScrollRect.onValueChanged.RemoveListener(OnScrolling);
        if (_limitedTimer != null)
            StopCoroutine(_limitedTimer);
    }

    private IEnumerator UpdateLimitedTimer()
    {
        int currentSeasonIndex = DataManager.Instance.GetCurrentSeasonIndex();
        SeasonInfo targetSeason = DataManager.Instance.GetSeasonInfoData(currentSeasonIndex + 1);
        while (true)
        {
            TimeSpan time = targetSeason.StartDateTimeUTC - DateTimeData.GetDateTimeUTC();
            if (time.Days > 0)
            {
                _limitedTimeText.text = time.Days + " " + LocalizationManager.Instance.GetText("DAILY_DAY");
                break;
            }
            else if (time.TotalSeconds > 0)
            {
                _limitedTimeText.text = time.ToString(@"hh\:mm\:ss");
            }
            else
            {
                _limitedTimeText.text = "00:00:00";
                DisableLimitedItemButton();
                break;
            }
            yield return new WaitForSeconds(1);
        }
        _limitedTimer = null;
    }

    private void DisableLimitedItemButton()
    {
        for (int i = 0; i < _shopUIElementList.Count; i++)
        {
            ShopItemUICell shopItemUI = _shopUIElementList[i].GetComponent<ShopItemUICell>();
            if (shopItemUI != null)
            {
                if (shopItemUI.Data.IsLimited)
                {
                    shopItemUI.BuyButton.interactable = false;
                    if (shopItemUI.MoreButton != null)
                    {
                        shopItemUI.MoreButton.interactable = false;
                    }
                }
            }
        }
    }

}
