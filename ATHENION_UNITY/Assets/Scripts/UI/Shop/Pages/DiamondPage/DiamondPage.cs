﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;
using System;

public class DiamondPage : ShopItemPage
{
    public override ShopTypes Type => ShopTypes.Diamond;

    public override ShopPageUI UI => _UI;
    [SerializeField] private DiamondUIPage _UI;

    public override void Initialize()
    {
        if (Data.StoreID.Length == 0) return;

        ShopTypeData subscriptionShopTypeData = DataManager.Instance.GetShopTypeData(ShopTypes.Subscription);
        ShopData _subscriptionShop = subscriptionShopTypeData.ShopDict[subscriptionShopTypeData.StoreID[0]];
        _UI.Initialize(this, _subscriptionShop);

        _UI.SetEventOnClickBuyItem(OnClickShopItem);
    }

    public override void OnClickShopItem(ShopItemData data)
    {
        if (ShopManager.GetPriceText(data) == ShopManager.PRICE_INVALID_TEXT) return;

        Debug.Log("OnClickShopItemUI " + data.GetDebugText());
        UIManager.ShowPopupDetail(data,
            delegate ()
            {
                UIManager.ShowMidLoading();
                ShopManager.Instance.RequestPurchaseItem(
                        data,
                        (result) =>
                        {
                            PurchaseCompleteCallback(data, result);
                            Debug.Log("HideAllLoadingScreen");
                            UIManager.HideAllSoftLoading();
                        },
                        CheckTopupPromotion
                    );
            },
            delegate ()
            {
                ShopManager.Instance.InvokeOnPurchaseCancel(data);
            }
        );
    }

    public void CheckTopupPromotion(UnityAction onComplete)
    {
        if (DataManager.Instance.FirstTopupStatus == false)
        {
            string itemID = ShopManager.Instance.CacheLastItem.ItemId;
            ShopData diamondShopData = Data.ShopDict[Data.StoreID[0]];
            if (diamondShopData.FindItem(itemID) == null)
            {
                onComplete?.Invoke();
                return;
            }

            DataManager.Instance.CheckAndUpdateFirstTopupItem((reward) =>
            {
                PopupUIManager.Instance.HideAllSoftLoad();
                if (reward != null)
                {
                    string title = LocalizationManager.Instance.GetText("TEXT_FIRST_TOPUP_TITLE");
                    string message = LocalizationManager.Instance.GetText("TEXT_FIRST_TOPUP_MESSAGE");
                    PopupUIManager.Instance.ShowPopup_GotReward(
                        title,
                        message,
                        new List<ItemData>() { reward },
                        delegate ()
                        {
                            PopupUIManager.Instance.Show_MidSoftLoad(true);
                            onComplete?.Invoke();
                        }
                    );
                }
                else
                {
                    onComplete?.Invoke();
                }
            });
        }
        else
        {
            onComplete?.Invoke();
        }
    }

    public override void ShowUI()
    {
        _UI.ShowUI();
    }

    public override void HideUI()
    {
        _UI.HideUI();
    }

}
