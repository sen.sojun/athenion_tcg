﻿using UnityEngine;
using TMPro;

public class BannerTag : MonoBehaviour
{
    public TextMeshProUGUI Text;
    public void SetText(string text)
    {
        Text.text = text;
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

}
