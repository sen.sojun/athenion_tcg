﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Coffee.UIExtensions;

public class PackUICell : MonoBehaviour
{
    #region Public Properties
    public Image Image;
    public Button OpenButton;

    public TextMeshProUGUI NameText;
    public TextMeshProUGUI AmountText;

    [Header("Effect")]
    public UIShiny Effect_Shiny;

    public string PackID { get; private set; }
    public int CurrentAmount { get; private set; }
    #endregion

    public void SetData(string packID, int currentAmount)
    {
        this.PackID = packID;
        this.CurrentAmount = currentAmount;
        UpdateUI();
    }

    public void SetCurrentAmount(int currentAmount)
    {
        this.CurrentAmount = currentAmount;
        UpdateUI();
    }

    public void SetOnClickOpenEvent(UnityAction<string> onClickOpenButton)
    {
        OpenButton.onClick.RemoveAllListeners();
        OpenButton.onClick.AddListener(() =>
            {
                onClickOpenButton?.Invoke(PackID);
            }
        );
    }

    private void UpdateUI()
    {
        NameText.text = LocalizationManager.Instance.GetItemName(PackID);
        //string path = "Images/Shops/" + ShopManager.Instance.GetImageByItemKey(ShopType.BoosterPack, storeKey);
     
        Sprite sprite = ShopManager.LoadPackBanner(PackID);
        Image.sprite = sprite;

        AmountText.text = CurrentAmount.ToString();

        Effect_Shiny.enabled = CurrentAmount > 0;
    }

}
