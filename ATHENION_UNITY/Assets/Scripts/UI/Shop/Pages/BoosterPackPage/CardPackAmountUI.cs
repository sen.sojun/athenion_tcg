﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CardPackAmountUI : MonoBehaviour
{
    [SerializeField] private GameObject _CardPackTextObject;
    [SerializeField] private TextMeshProUGUI _CardPackText;

    private void OnEnable()
    {
        DataManager.OnUpdateInventory += SetUI;
        SetUI();
    }

    private void OnDisable()
    {
        DataManager.OnUpdateInventory -= SetUI;
    }

    public void SetUI()
    {
        int amount = 0;
        List<CardPackData> cardPackDataList = DataManager.Instance.InventoryData.CardPackDataList;
        for (int i = 0; i < cardPackDataList.Count; i++)
        {
            amount += cardPackDataList[i].Amount;
        }
        _CardPackText.text = amount.ToString();
        if (amount > 0)
        {
            _CardPackTextObject.SetActive(true);
        }
        else
        {
            _CardPackTextObject.SetActive(false);
        }
    }
}
