﻿using Karamucho;
using PlayFab.ClientModels;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class BoosterPackPage : ShopItemPage
{
    #region Properties
    public override ShopPageUI UI => _UI;
    [SerializeField] private BoosterPackUIPage _UI;

    public CardPackData LastOpenPack { get; private set; }

    public override ShopTypes Type => ShopTypes.BoosterPack;
    #endregion

    #region Base function

    public override void Initialize()
    {
        _UI.Initialize(this);

        _UI.SetEventOnClickBuyPack(OnClickBuyPack);
        _UI.SetEventOnClickOpen(OnClickOpenPack);
        _UI.SetEventOnClickBuyItem(OnClickShopItem);
        _UI.SetEventOnClickMoreInfo(OnMoreInfoClick);
        _UI.SetEventOnClickCosmeticRewardPreview(OnPreviewClick);

        DataManager.OnUpdateInventory += RefreshCardPackAmount;
    }

    public override void OnExit()
    {
        DataManager.OnUpdateInventory -= RefreshCardPackAmount;
    }
    #endregion

    #region Methods

    public BoosterPackUIPage GetUI()
    {
        return _UI;
    }

    public override void ShowUI()
    {
        UI.ShowUI();
    }

    public override void HideUI()
    {
        UI.HideUI();
    }

    public OpenPackPopupUI GetOpenPackPopup()
    {
        return _UI.GetOpenPackPopup();
    }

    private void RefreshCardPackAmount()
    {
        List<CardPackData> _cardPackData = new List<CardPackData>();
        ShopTypeData shopTypeData = DataManager.Instance.GetShopTypeData(ShopTypes.BoosterPack);
        foreach (KeyValuePair<string, ShopData> shopData in shopTypeData.ShopDict)
        {
            BoosterPackShopData data = (BoosterPackShopData)shopData.Value;
            _cardPackData.Add(new CardPackData(data.PackID, GetPackAmount(data.PackID)));
        }
        _UI.RefreshData(_cardPackData);
    }

    private void OnMoreInfoClick(CardPackUICell banner)
    {
        _UI.OnClickMoreInfo(banner);
    }

    private void OnPreviewClick(CardPackUICell banner)
    {
        _UI.OnClickPreview(banner);
    }

    public void ShowGotItemResult(string bundleID, List<ItemData> itemList, CustomButton customButton = null)
    {
        string title = LocalizationManager.Instance.GetText("SHOP_PURCHASE_SUCCESS_HEADER");
        string message = string.Format(LocalizationManager.Instance.GetText("SHOP_YOU_GOT_ITEM"), LocalizationManager.Instance.GetItemName(bundleID));

        PopupUIManager.Instance.ShowPopup_GotReward(title, message, itemList, null, customButton);
    }

    public override void PurchaseCompleteCallback(ShopItemData itemData, PurchaseResult purchaseResult)
    {
        if (purchaseResult.HasError)
        {
            string title = LocalizationManager.Instance.GetText("SHOP_PURCHASE_FAIL_HEADER");
            string message = purchaseResult.GetErrorText();

            PopupUIManager.Instance.ShowPopup_Error(title, message);
            ShopManager.Instance.InvokeOnPurchaseFail(itemData);
        }
        else
        {
            ShopItemData item = purchaseResult.PurchaseItemData;
            CustomButton openPackButton = null;
            if (item.IsInAppPurchase == false)
            {
                openPackButton = purchaseResult.PurchaseItemData.IsPack ? GetOpenPackCustomButton(() => OpenPackAfterPurchase(purchaseResult.PlayFabItemResult)) : null;
            }
            Debug.Log("ShowGotItemResult");
            UIManager.ShowGotItemResult(item.ItemId, item.CustomData.ItemDetail, openPackButton);

            base.RefreshPage();

            // Add analytic event 
            ShopManager.Instance.InvokeOnPurchaseComplete(purchaseResult.PurchaseItemData);
        }
    }

    #region Pack
    public void OpenPack(string packID, int amount, UnityAction<PackReward> onComplete, UnityAction<string> error)
    {
        DataManager.Instance.OpenCardPack(
              packID
            , amount
            , delegate (PackReward result)
            {
                DataManager.Instance.LoadInventory(() => onComplete?.Invoke(result), error);
                ShopManager.Instance.InvokeOnOpenPack(result);
            }
            , error
        );
    }

    private void OnClickOpenPackPopupConfirmButton(string packID, int openValue)
    {
        LastOpenPack = new CardPackData(packID, openValue);
        PopupUIManager.Instance.Show_MidSoftLoad(true);
        OpenPack(
              packID
            , openValue
            , delegate (PackReward result)
            {
                NavigatorController.Instance.LoadOpeningPackScene(
                    delegate ()
                    {
                        CardPackInventoryManager.Instance.Initialize();
                        CardPackController.Instance.StartSystem(result, OnOpenPackComplete);
                    }
                );
                //OnClickOpenPack?.Invoke(result);
            }
            , delegate (string error)
            {
                Debug.Log(error);
                PopupUIManager.Instance.ShowPopup_Error("", error);
                OnOpenPackComplete();
            }
        );
    }

    public void OnClickOpenPack(string packID)
    {
        CardPackData packData = FindCardPackData(packID);
        if (packData == null || packData.Amount <= 0)
            return;

        _UI.ShowOpenPackComfirm(packData, (_packID, amount) =>
        {
            OnClickOpenPackPopupConfirmButton(_packID, amount);

            _UI.HideOpenPackConfirm();
        });
    }

    public bool CanOpenPack(string packID, int amount)
    {
        if (packID == "") return false;

        CardPackData packData = FindCardPackData(packID);
        if (packData == null) return false;

        return packData.Amount >= amount;
    }

    private CardPackData FindCardPackData(string packID)
    {
        return new CardPackData(packID, GetPackAmount(packID));
    }

    public void OnClickBuyPack(BoosterPackShopData shopData)
    {
        _UI.ShowPurchasePackConfirm(
            shopData,
             _UI.HidePuchasePackConfirm, // close purchase pack panel.
             delegate (ShopItemData data) //confirm for purchase.
             {
                 _UI.HidePuchasePackConfirm();
                 UIManager.ShowMidLoading();
                 ShopManager.Instance.RequestPurchaseItem(
                     data,
                     delegate (PurchaseResult result)
                     {
                         PurchaseCompleteCallback(data, result);
                         UIManager.HideAllSoftLoading();
                     });
             },
            delegate (ShopItemData data) // not enough money
            {
                _UI.HidePuchasePackConfirm();
                CustomButton customBtn = null;
                if (data.Currency == VirtualCurrency.DI)
                {
                    customBtn = new CustomButton()
                    {
                        Action = () => ShopManager.Instance.ShowUI(ShopTypes.Diamond),
                        Color = CustomButton.ButtonColor.Green,
                        Text = LocalizationManager.Instance.GetText("BUTTON_SHOP")
                    };
                }
                PopupUIManager.Instance.ShowNotEnoughCurrency(
                    "VC_" + data.Currency,
                    LocalizationManager.Instance.GetText("TITLE_ERROR"),
                    string.Format(LocalizationManager.Instance.GetText("TEXT_NOT_ENOUGH_CURRENCY"), GameHelper.GetLocalizeCurrencyText(data.Currency)),
                    null,
                    customBtn
                );
            }
        );
    }

    public void ShowLastBuyPack()
    {
        _UI.ShowPurchasePackConfirm();
    }

    public int GetPackAmount(string packID)
    {
        return DataManager.Instance.InventoryData.GetAmountByItemID(packID);
    }

    public Sprite GetPackSprite(string packID)
    {
        string imagePath = "Images/Inventory/INVENTORY_IMG_PACK_" + packID.ToUpper();
        return ResourceManager.Load<Sprite>(imagePath);
    }

    private void OpenPackAfterPurchase(PurchaseItemResult purchaseResult)
    {
        //Open Pack 
        ItemData itemDetail = ParseToItemDetail(purchaseResult);
        if (itemDetail != null)
        {
            string itemID = itemDetail.ItemID;
            int amount = itemDetail.Amount;
            PopupUIManager.Instance.Show_MidSoftLoad(true);
            OpenPack(
                      itemID
                    , amount
                    , delegate (PackReward result)
                    {
                        NavigatorController.Instance.LoadOpeningPackScene(
                            delegate ()
                            {
                                CardPackShopManager.Instance.Initialize();
                                CardPackController.Instance.StartSystem(result, OnOpenPackComplete);
                            }
                        );
                        //OnClickOpenPack?.Invoke(result);
                    }
                    , delegate (string error)
                    {
                        Debug.Log(error);
                        PopupUIManager.Instance.ShowPopup_Error("", error);
                        OnOpenPackComplete();
                    }
                );
        }
    }

    private void OnOpenPackComplete()
    {
        base.RefreshPage();
        PopupUIManager.Instance.Show_MidSoftLoad(false);
    }

    #endregion

    public ItemData ParseToItemDetail(PurchaseItemResult result)
    {
        bool isEmpty = result.Items == null || result.Items.Count == 0;
        bool isContainer = !isEmpty && result.Items.Count == 1;
        bool isBundle = !isEmpty && result.Items.Count > 1;

        if (isEmpty)
            return null;

        ItemData item = null;
        if (isContainer)
        {
            item = new ItemData(result.Items[0].ItemId, (int)result.Items[0].UsesIncrementedBy);
            return item;
        }
        else if (isBundle)
        {
            if (result.Items[0].BundleContents == null || result.Items[0].BundleContents.Count == 0)
                return null;
            item = new ItemData(result.Items[0].BundleContents[0], result.Items[0].BundleContents.Count);
        }
        return item;
    }

    private CustomButton GetOpenPackCustomButton(UnityAction action)
    {
        CustomButton custom = new CustomButton()
        {
            Color = CustomButton.ButtonColor.Green,
            Action = action,
            Text = LocalizationManager.Instance.GetText("BUTTON_OPEN")
        };
        return custom;
    }

    #endregion
}
