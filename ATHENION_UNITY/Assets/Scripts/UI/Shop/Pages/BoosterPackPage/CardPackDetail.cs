﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CardPackDetail : MonoBehaviour
{
    public class CardPackValue
    {
        public bool IsHeroSkin;
        public CardData Card;
        public string Value;

        public CardPackValue(CardData cardData, string value, bool isHeroSkin = false)
        {
            this.Card = cardData;
            this.Value = value;
            this.IsHeroSkin = isHeroSkin;
        }
    }

    public class ItemPackValue
    {
        public string Name;
        public string Value;
        public CardRarity Rarity;

        public ItemPackValue(string name, string value, CardRarity rarity)
        {
            this.Name = name;
            this.Value = value;
            this.Rarity = rarity;
        }
    }

    [SerializeField] private ScrollRect _scrollRect;
    [SerializeField] private RectTransform _viewport;
    [SerializeField] private RectTransform _Content;

    [SerializeField] private TextMeshProUGUI[] _RarityTextList;

    [SerializeField] private Button _closeButton;
    [SerializeField] private Transform _spawnPoint;
    [SerializeField] private GameObject _itemDetailHeader;
    [SerializeField] private Transform _itemDetailSpawnPoint;
    [SerializeField] private CardPackDetailCell _prefab;

    private List<CardPackDetailCell> detailList = new List<CardPackDetailCell>();
    private List<CardPackDetailCell> itemDetailList = new List<CardPackDetailCell>();

    private void OnEnable()
    {

    }

    private void OnDisable()
    {
        SetEnableLayoutComponent(_spawnPoint.GetComponent<RectTransform>(), true);
        SetEnableLayoutComponent(_itemDetailSpawnPoint.GetComponent<RectTransform>(), true);
    }

    public void SetData(BoosterPackShopData data)
    {
        ClearAllData();
        GenerateInfo(data);

        _closeButton.onClick.RemoveAllListeners();
        _closeButton.onClick.AddListener(HideUI);
    }

    public void ShowUI()
    {
        _scrollRect.onValueChanged.AddListener(OnScrolling);
        GameHelper.UITransition_FadeIn(this.gameObject);
        _spawnPoint.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
    }

    public void HideUI()
    {
        _scrollRect.onValueChanged.RemoveListener(OnScrolling);
        GameHelper.UITransition_FadeOut(this.gameObject);
    }

    public void RebuildUI()
    {
        LayoutRebuilder.ForceRebuildLayoutImmediate(_Content);
    }

    private void ClearAllData()
    {
        ClearDetailCell(detailList);
        ClearDetailCell(itemDetailList);
    }

    private void ClearDetailCell(List<CardPackDetailCell> cellList)
    {
        if (cellList == null) return;

        for (int i = 0; i < cellList.Count; i++)
        {
            var obj = cellList[i];
            DestroyImmediate(obj.gameObject);
        }
        cellList.Clear();
    }

    private void GenerateInfo(BoosterPackShopData data)
    {
        SetDropRate(data.Rate);

        List<CardPackValue> cardDataList = CreateCardPackValueList(data.CardList);
        CreateCardDetailUICell(cardDataList);

        if (data.ItemList != null)
        {
            _itemDetailSpawnPoint.gameObject.SetActive(true);
            _itemDetailHeader.SetActive(true);
            List<ItemPackValue> itemPacksList = CreateItemPackValueList(data.ItemList);
            CreateItemDetailUICell(itemPacksList);
        }
        else
        {
            _itemDetailHeader.SetActive(false);
            _itemDetailSpawnPoint.gameObject.SetActive(false);
        }

    }

    private void SetDropRate(Dictionary<string, string> rate)
    {
        _RarityTextList[0].text = rate["Common"];
        _RarityTextList[1].text = rate["Rare"];
        _RarityTextList[2].text = rate["Epic"];
        _RarityTextList[3].text = rate["Legend"];
    }

    #region CardDetail

    private List<CardPackValue> CreateCardPackValueList(Dictionary<string, string> cardList)
    {
        List<CardPackValue> result = new List<CardPackValue>();
        foreach (KeyValuePair<string, string> item in cardList)
        {
            string cardFullID = item.Key;
            string rate = item.Value;
            bool isHeroSkin = cardFullID.Contains("_HERO_");
            CardData cardData = CardData.CreateCard(cardFullID);
            result.Add(new CardPackValue(cardData, rate, isHeroSkin));
        }

        return result;
    }

    private void CreateCardDetailUICell(List<CardPackValue> cardDataList)
    {
        cardDataList = cardDataList.OrderByDescending(data => data.Card.Rarity).ThenBy(data => data.Card.Name).ToList();
        foreach (CardPackValue data in cardDataList)
        {
            CardPackDetailCell detail = Instantiate(_prefab, _spawnPoint).GetComponent<CardPackDetailCell>();

            string cardName = data.IsHeroSkin ? data.Card.Name + " " + LocalizationManager.Instance.GetText("HERO_SKIN") : data.Card.Name;

            detail.SetData(data.Card.Rarity, cardName, data.Value);
            detailList.Add(detail);
        }
    }

    #endregion

    #region ItemDetail

    private List<ItemPackValue> CreateItemPackValueList(Dictionary<string, string> itemList)
    {
        StarItemDB itemDB = DataManager.Instance.GetStarItemDB();
        List<ItemPackValue> result = new List<ItemPackValue>();
        foreach (KeyValuePair<string, string> item in itemList)
        {
            CurrencyReward currencyData = itemDB.GetStarItem(item.Key);
            string name = string.Format("{0} x{1}", LocalizationManager.Instance.GetItemName(currencyData.ItemID), currencyData.Amount);
            string rate = item.Value;
            result.Add(new ItemPackValue(name, rate, currencyData.Rarity));
        }
        return result;
    }

    private void CreateItemDetailUICell(List<ItemPackValue> itemDataList)
    {
        itemDataList = itemDataList.OrderByDescending(data => data.Rarity).ThenBy(data => data.Name).ToList();
        foreach (ItemPackValue data in itemDataList)
        {
            CardPackDetailCell detail = Instantiate(_prefab, _itemDetailSpawnPoint).GetComponent<CardPackDetailCell>();

            detail.SetData(data.Rarity, data.Name, data.Value);
            detailList.Add(detail);
        }
    }

    #endregion

    protected virtual void OnScrolling(Vector2 value)
    {
        OnContentScrolling(_spawnPoint.GetComponent<RectTransform>(), value);
        OnContentScrolling(_itemDetailSpawnPoint.GetComponent<RectTransform>(), value);
    }

    protected virtual void OnContentScrolling(RectTransform content, Vector2 value)
    {
        if (content != null && _viewport != null && content.transform.childCount > 0)
        {
            SetEnableLayoutComponent(content, false);

            // Show Only Visible Child.
            RectTransform child_rect = null;
            foreach (Transform child in content.transform)
            {
                child_rect = child.GetComponent<RectTransform>();
                if (child_rect != null)
                {
                    if (GameHelper.IsRectTouchRect(child_rect, _viewport))
                    {
                        child.gameObject.SetActive(true);
                    }
                    else
                    {
                        child.gameObject.SetActive(false);
                    }
                }
            }
        }
    }

    protected void SetEnableLayoutComponent(RectTransform content, bool isEnable)
    {
        // If enable show all child.
        if (isEnable)
        {
            foreach (Transform child in content.transform)
            {
                child.gameObject.SetActive(true);
            }
        }

        // Layout Component
        if (content.GetComponent<ContentSizeFitter>() != null)
        {
            content.GetComponent<ContentSizeFitter>().enabled = isEnable;
        }

        // All Layout Group
        LayoutGroup[] layoutGroups = content.GetComponents<LayoutGroup>();
        if (layoutGroups != null && layoutGroups.Length > 0)
        {
            foreach (LayoutGroup layoutGroup in layoutGroups)
            {
                layoutGroup.enabled = isEnable;
            }
        }
    }
}
