﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class BoosterPackUIPage : ShopPageUI
{
    #region Inspector Properties
    [SerializeField] private Button _LegendPackButton;

    [SerializeField] private CardPackUICell _cardPackUICell;

    [SerializeField] private CardPackDetail _cardPackDetailPage;
    [SerializeField] private CosmeticRewardPopup _cosmeticRewardPopup;

    [SerializeField] private OpenPackPopupUI _openPackPopupUI;
    [SerializeField] private PurchasePackPopupUI _purchasePopupUI;

    [SerializeField] private Transform _starPackTransform;
    [SerializeField] private Transform _spawnTransform;

    [SerializeField] private VerticalLayoutGroup _verticalGroup;

    [SerializeField] private PopupLegendPackListUI _popupLegendPackListUI;
    #endregion

    private List<CardPackUICell> _cardPackUICellList = new List<CardPackUICell>();

    private BoosterPackPage _page;

    public void Initialize(BoosterPackPage page)
    {
        this._page = page;
        ClearItemList();
        _LegendPackButton.gameObject.SetActive(DataManager.Instance.InventoryData.IsHasLegendPack());
        CreateButtons();
        UpdateLayout();
    }

    public override void ShowUI()
    {
        if (gameObject.activeSelf)
            return;
        gameObject.SetActive(true);
    }

    public override void HideUI()
    {
        gameObject.SetActive(false);
        _popupLegendPackListUI.HideUI();
    }

    public List<CardPackUICell> GetCardPackUICellList()
    {
        return _cardPackUICellList;
    }

    public void ClearItemList()
    {
        if (_cardPackUICellList.Count > 0)
        {
            foreach (CardPackUICell button in _cardPackUICellList)
            {
                DestroyImmediate(button.gameObject);
            }
            _cardPackUICellList.Clear();
        }
    }

    public OpenPackPopupUI GetOpenPackPopup()
    {
        return _openPackPopupUI;
    }

    public void CreateButtons()
    {
        foreach (KeyValuePair<string, ShopData> data in _page.Data.ShopDict)
        {
            if (data.Value == null) continue;
            BoosterPackShopData boosterPackData = (BoosterPackShopData)data.Value;
            if (boosterPackData.IsValidDate == false)
                continue;

            int containPackAmount = _page.GetPackAmount(boosterPackData.PackID);
            Transform parent = boosterPackData.PackID == "CONT_STAR" ? _starPackTransform : _spawnTransform;
            CardPackUICell button = Instantiate(_cardPackUICell, parent).GetComponent<CardPackUICell>();
            button.SetData(boosterPackData, containPackAmount);
            _cardPackUICellList.Add(button);
        }
    }

    public void OnClickPreview(CardPackUICell banner)
    {
        _cosmeticRewardPopup.Initialize(banner.Data.CosmeticRewards);
        _cosmeticRewardPopup.ShowUI();
    }

    public void OnClickMoreInfo(CardPackUICell banner)
    {
        _cardPackDetailPage.SetData((BoosterPackShopData)banner.Data);
        _cardPackDetailPage.ShowUI();
        _cardPackDetailPage.RebuildUI();
    }

    public void SetEventOnClickBuyPack(UnityAction<BoosterPackShopData> OnClickBuy)
    {
        for (int i = 0; i < _cardPackUICellList.Count; i++)
        {
            _cardPackUICellList[i].SetOnClickBuyEvent(OnClickBuy);
        }
    }

    public void SetEventOnClickOpen(UnityAction<string> OnClickOpen)
    {
        for (int i = 0; i < _cardPackUICellList.Count; i++)
        {
            _cardPackUICellList[i].SetOnClickOpenEvent(OnClickOpen);
        }

        _LegendPackButton.onClick.RemoveAllListeners();
        _LegendPackButton.onClick.AddListener(() => OpenLegendPackUIList(OnClickOpen));
    }

    private void OpenLegendPackUIList(UnityAction<string> OnClickOpen)
    {
        _popupLegendPackListUI.ShowUI();
        List<CardPackData> cardPackDatas = DataManager.Instance.InventoryData.CardPackDataList.FindAll(pack => GameHelper.IsLegendPack(pack.PackID) == true);
        _popupLegendPackListUI.SetData(cardPackDatas);
        _popupLegendPackListUI.SetEvent(OnClickOpen);
    }

    public void SetEventOnClickMoreInfo(UnityAction<CardPackUICell> OnClickMoreInfo)
    {
        for (int i = 0; i < _cardPackUICellList.Count; i++)
        {
            _cardPackUICellList[i].SetOnClickMoreInfoEvent(OnClickMoreInfo);
        }
    }

    public void SetEventOnClickCosmeticRewardPreview(UnityAction<CardPackUICell> OnClickPreview)
    {
        for (int i = 0; i < _cardPackUICellList.Count; i++)
        {
            _cardPackUICellList[i].SetOnClickPreviewEvent(OnClickPreview);
        }
    }

    public override void UpdateLayout()
    {
        Content.GetComponent<ContentSizeFitter>().enabled = true;
        _verticalGroup.enabled = true;
        Content.anchoredPosition = Vector3.zero;
        LayoutRebuilder.ForceRebuildLayoutImmediate(Content);
    }

    public void RefreshData(List<CardPackData> cardPackData)
    {
        _LegendPackButton.gameObject.SetActive(DataManager.Instance.InventoryData.IsHasLegendPack());
        for (int i = 0; i < cardPackData.Count; i++)
        {
            CardPackUICell cell = _cardPackUICellList.Find(element => element.Data.PackID == cardPackData[i].PackID);
            if (cell)
            {
                cell.SetCurrentAmount(cardPackData[i].Amount);
            }
        }
    }

    public void ShowOpenPackComfirm(CardPackData packData, UnityAction<string, int> onComfirm)
    {
        _openPackPopupUI.SetData(packData);
        _openPackPopupUI.SetCallbackOnClickConfirmButton((amount) =>
        {
            onComfirm?.Invoke(packData.PackID, amount);
        });

        _openPackPopupUI.ShowUI();
    }

    public void HideOpenPackConfirm()
    {
        _openPackPopupUI.HideUI();
    }

    public void ShowPurchasePackConfirm(BoosterPackShopData data, UnityAction OnClose, UnityAction<ShopItemData> OnBuy, UnityAction<ShopItemData> NotEnoughMoney)
    {
        _purchasePopupUI.SetData(data);
        _purchasePopupUI.SetEvent(OnClose, OnBuy, NotEnoughMoney);
        _purchasePopupUI.ShowUI();
    }

    public void ShowPurchasePackConfirm()
    {
        _purchasePopupUI.ShowUI();
    }

    public void HidePuchasePackConfirm()
    {
        _purchasePopupUI.HideUI();
    }

}
