﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using DG.Tweening;

public abstract class ShopItemPage : MonoBehaviour
{
    #region Properties
    public abstract ShopTypes Type { get; }
    public abstract ShopPageUI UI { get; }
    public ShopUIManager UIManager { get { return ShopManager.Instance.UIManager; } }
    public ShopTypeData Data { get => DataManager.Instance.GetShopTypeData(Type); }

    #endregion

    #region Methods 

    public abstract void Initialize();


    public abstract void ShowUI();
    public abstract void HideUI();
    
    public virtual void OnExit() { }

    #region Layout

    public void RefreshPage()
    {
        ShopManager.Instance.UpdatePageDataAndRefreshPage(Data);
    }

    public virtual void OnClickShopItem(ShopItemData data)
    {
        if (ShopManager.GetPriceText(data) == ShopManager.PRICE_INVALID_TEXT) return;

        Debug.Log("OnClickShopItemUI " + data.GetDebugText());
        UIManager.ShowPopupDetail(data,
            delegate ()
            {
                UIManager.ShowMidLoading();
                ShopManager.Instance.RequestPurchaseItem(
                        data,
                        (result) =>
                        {
                            Action OnComplete = delegate
                            {
                                PurchaseCompleteCallback(data, result);
                                Debug.Log("HideAllLoadingScreen");
                                UIManager.HideAllSoftLoading();
                            };

                            if (data.IsLimited)
                            {
                                if (result.HasError == false)
                                {
                                    DataManager.Instance.RequestPurchaseLimitedItem(data.ItemId, () => { OnComplete?.Invoke(); });
                                }
                                else
                                {
                                    OnComplete?.Invoke();
                                }
                            }
                            else
                            {
                                OnComplete?.Invoke();
                            }
                        }
                    );
            },
            delegate ()
            {
                ShopManager.Instance.InvokeOnPurchaseCancel(data);
            }
        );
    }

    public virtual void PurchaseCompleteCallback(ShopItemData itemData, PurchaseResult purchaseResult)
    {
        if (purchaseResult.HasError)
        {
            string title = LocalizationManager.Instance.GetText("SHOP_PURCHASE_FAIL_HEADER");
            string message = purchaseResult.GetErrorText();

            PopupUIManager.Instance.ShowPopup_Error(title, message);
            ShopManager.Instance.InvokeOnPurchaseFail(itemData);
        }
        else
        {
            ShopItemData item = purchaseResult.PurchaseItemData;
            Debug.Log("ShowGotItemResult");
            UIManager.ShowGotItemResult(item.ItemId, item.CustomData.ItemDetail);

            RefreshPage();

            // Add analytic event 
            ShopManager.Instance.InvokeOnPurchaseComplete(purchaseResult.PurchaseItemData);
        }
    }


    #endregion

    #endregion
}
