﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;
using System.Linq;

public class PromotionPage : ShopItemPage
{
    #region Public Properties
    public override ShopTypes Type => ShopTypes.Promotion;

    public override ShopPageUI UI => _UI;
    [SerializeField] private PromotionUIPage _UI;
    #endregion

    #region Methods 

    public PromotionUIPage GetPromotionUIPage()
    {
        return _UI;
    }

    public override void Initialize()
    {
        if (Data.StoreID.Length == 0) return;
        ShopData promotionShop = Data.ShopDict[Data.StoreID[0]];
        ShopData promotionSequenceShop = Data.ShopDict[Data.StoreID[1]];
        _UI.Initialize(promotionShop, promotionSequenceShop, GetWeeklyDealItem(), GetDailyDealItem());
        _UI.SetEvent(OnClickShopItem, OnClickBuySeasonPass, OnClickFreeDeal, OnClickWeeklyDeal, OnClickDailyDeal);

        // Set Seen 
        PlayerSeenListManager.Instance.SetSeenListData(SeenItemTypes.Promotion, true, promotionShop.GetAllShopItemIDList((obj) => obj.IsTimeValid == true));
    }

    public ShopItemData GetDailyDealItem()
    {
        DealRecord record = DataManager.Instance.GetDailyDealRecord();
        ShopTypeData shopTypeData = DataManager.Instance.GetShopTypeData(ShopTypes.DailyDeal);

        // Find Item 
        foreach (KeyValuePair<string, ShopData> shop in shopTypeData.ShopDict)
        {
            ShopItemData item = shop.Value.FindItem(record.ItemID);
            if (item != null)
            {
                return item;
            }
        }
        return null;
    }

    public ShopItemData GetWeeklyDealItem()
    {
        DealRecord record = DataManager.Instance.GetWeeklyDealRecord();
        ShopTypeData shopTypeData = DataManager.Instance.GetShopTypeData(ShopTypes.WeeklyDeal);

        // Find Item 
        foreach (KeyValuePair<string, ShopData> shop in shopTypeData.ShopDict)
        {
            ShopItemData item = shop.Value.FindItem(record.ItemID);
            if (item != null)
            {
                return item;
            }
        }
        return null;
    }
    // Weekly Deal
    public void OnClickWeeklyDeal()
    {
        UIManager.ShowSoftLoading();
        Action refreshPromotionPage = () =>
        {
            base.RefreshPage();
            UIManager.HideAllSoftLoading();
        };

        DealRecord record = DataManager.Instance.GetWeeklyDealRecord();
        UIManager.HideSoftLoading();

        if (record.IsRedeem)
        {
            // already buy this item.
            string errorMsg = LocalizationManager.Instance.GetText("ERROR_WEEKLYDEAL_IS_ALREADY_REDEEM");
            PopupUIManager.Instance.ShowPopup_Error(LocalizationManager.Instance.GetText("SHOP_PURCHASE_FAIL_HEADER"), errorMsg);
            return;
        }

        ShopItemData buyItem = GetWeeklyDealItem();
        if (buyItem == null)
        {
            string errorMsg = LocalizationManager.Instance.GetText(ShopManager.ShopDailyErrorNotFoundItem_LocalizeKey);
            PopupUIManager.Instance.ShowPopup_Error(LocalizationManager.Instance.GetText("SHOP_PURCHASE_FAIL_HEADER"), errorMsg);
            return;
        }

        UIManager.ShowPopupDetail(buyItem,
            delegate ()
            {
                UIManager.ShowMidLoading();
                ShopManager.Instance.RequestPurchaseItem(
                         buyItem
                     , delegate (PurchaseResult purchaseResult)
                     {
                         if (purchaseResult.HasError)
                         {
                             PurchaseCompleteCallback(buyItem, purchaseResult);
                             refreshPromotionPage?.Invoke();
                         }
                         else
                         {
                             DataManager.Instance.RevokeWeeklyDeal(
                                 delegate
                                 {
                                     PurchaseCompleteCallback(buyItem, purchaseResult);
                                     refreshPromotionPage?.Invoke();
                                 }
                                 , (error) =>
                                 {
                                     PurchaseCompleteCallback(buyItem, purchaseResult);
                                     refreshPromotionPage?.Invoke();
                                 }
                             );
                         }
                     }
                 );
            },
            delegate ()
            {
                ShopManager.Instance.InvokeOnPurchaseCancel(buyItem);
            }
        );

    }

    // Daily Deal
    public void OnClickDailyDeal()
    {
        //UIManager.ShowSoftLoading();
        Action refreshPromotionPage = () =>
        {
            base.RefreshPage();
            UIManager.HideAllSoftLoading();
        };

        DealRecord record = DataManager.Instance.GetDailyDealRecord();
        //UIManager.HideSoftLoading();

        if (record.IsRedeem)
        {
            // already buy this item.
            string errorMsg = LocalizationManager.Instance.GetText(ShopManager.ShopDailyErrorAleadyBuy_LocalizeKey);
            PopupUIManager.Instance.ShowPopup_Error(LocalizationManager.Instance.GetText("SHOP_PURCHASE_FAIL_HEADER"), errorMsg);
            return;
        }

        ShopItemData buyItem = GetDailyDealItem();

        if (ShopManager.GetPriceText(buyItem) == ShopManager.PRICE_INVALID_TEXT) return;

        if (buyItem == null)
        {
            string errorMsg = LocalizationManager.Instance.GetText(ShopManager.ShopDailyErrorNotFoundItem_LocalizeKey);
            PopupUIManager.Instance.ShowPopup_Error(LocalizationManager.Instance.GetText("SHOP_PURCHASE_FAIL_HEADER"), errorMsg);
            return;
        }

        UIManager.ShowPopupDetail(buyItem,
            delegate ()
            {
                UIManager.ShowMidLoading();
                ShopManager.Instance.RequestPurchaseItem(
                        buyItem
                    , delegate (PurchaseResult purchaseResult)
                    {
                        if (purchaseResult.HasError)
                        {
                            PurchaseCompleteCallback(buyItem, purchaseResult);
                            refreshPromotionPage?.Invoke();
                        }
                        else
                        {
                            DataManager.Instance.RevokeDailyDeal(
                                delegate
                                {
                                    PurchaseCompleteCallback(buyItem, purchaseResult);
                                    refreshPromotionPage?.Invoke();
                                }
                                , (error) =>
                                {
                                    PurchaseCompleteCallback(buyItem, purchaseResult);
                                    refreshPromotionPage?.Invoke();
                                }
                            );
                        }
                    }
                );
            },
            delegate ()
            {
                ShopManager.Instance.InvokeOnPurchaseCancel(buyItem);
            }
        );

    }

    // Free Deal / Gift
    public void OnClickFreeDeal()
    {
        UIManager.ShowSoftLoading();
        string errorMsg = LocalizationManager.Instance.GetText(ShopManager.ShopFreeErrorUnknown_LocalizeKey);

        DataManager.Instance.ClaimFreeDeal(
            delegate ()
            {
                DataManager.Instance.LoadInventory(
                    delegate ()
                    {
                        DealRecord result = DataManager.Instance.GetFreeDealRecord();
                        ShopManager.Instance.InvokeOnClaimDailyFree(result);
                        UIManager.ShowGotItemResult(result.ItemID, result.ToListItemData());

                        base.RefreshPage();
                        UIManager.HideSoftLoading();
                    }
                    , delegate (string error)
                    {
                        PopupUIManager.Instance.ShowPopup_Error(LocalizationManager.Instance.GetText("SHOP_FREE_ERROR_HEADER"), errorMsg);
                        base.RefreshPage();
                        UIManager.HideSoftLoading();
                    }
                );
            }
            , delegate (string error)
            {
                PopupUIManager.Instance.ShowPopup_Error(LocalizationManager.Instance.GetText("SHOP_FREE_ERROR_HEADER"), errorMsg);
                base.RefreshPage();
                UIManager.HideSoftLoading();
            }
        );
    }

    public override void ShowUI()
    {
        _UI.ShowUI();
    }

    public override void HideUI()
    {
        _UI.HideUI();
    }

    public void OnClickBuySeasonPass()
    {
        PopupUIManager.Instance.ShowPopup_PurchaseSeasonPass(() =>
        {
            PriceData priceData = SeasonPassManager.Instance.GetSeasonPassPremiumPrice();
            bool isEnoughMoney = DataManager.Instance.InventoryData.GetVirtualCurrency(priceData.CurrencyType) >= priceData.Amount;
            if (isEnoughMoney)
            {
                UIManager.ShowMidLoading();
                DataManager.Instance.CheckSessionValid(
                    delegate ()
                    {
                        SeasonPassManager.Instance.PurchaseSeasonPassPremium(
                            (rewardList) =>
                            {
                                UIManager.ShowGotItemResult("BUND_SEASON_PASS", rewardList);
                                UIManager.HideMidLoading();
                                //ShowUI(ShopTypes.Promotion);
                                //RefreshData(ShopTypes.Promotion);
                                base.RefreshPage();
                                ShopManager.Instance.InvokeOnPurchasePremium();
                            }
                            , (error) =>
                            {
                                UIManager.HideMidLoading();
                            }
                        );
                    },
                    delegate (string error)
                    {
                        NavigatorController.Instance.Logout();
                    }
                );
            }
            else
            {
                //UIManager.HidePuchasePackConfirm();
                CustomButton customBtn = null;
                if (priceData.CurrencyType == VirtualCurrency.DI)
                {
                    customBtn = new CustomButton()
                    {
                        Action = () => ShopManager.Instance.ShowUI(ShopTypes.Diamond),
                        Color = CustomButton.ButtonColor.Green,
                        Text = LocalizationManager.Instance.GetText("BUTTON_SHOP")
                    };
                }
                PopupUIManager.Instance.ShowNotEnoughCurrency(
                    "VC_" + priceData.CurrencyType,
                    LocalizationManager.Instance.GetText("TITLE_ERROR"),
                    string.Format(LocalizationManager.Instance.GetText("TEXT_NOT_ENOUGH_CURRENCY"), GameHelper.GetLocalizeCurrencyText(priceData.CurrencyType)),
                    null,
                    customBtn
                );
            }
        });

    }

    #endregion
}
