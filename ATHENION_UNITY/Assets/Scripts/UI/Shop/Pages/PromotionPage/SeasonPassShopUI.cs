﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;
using System;
using Karamucho;

public class SeasonPassShopUI : MonoBehaviour
{
    [SerializeField] private Image _Image;
    [SerializeField] private Button _BuyButton;
    [SerializeField] private Button _MoreButton;
    [SerializeField] private TextMeshProUGUI _TimeText;

    #region Private Properties  

    private Coroutine _countDownCoroutine;

    #endregion

    private void OnEnable()
    {
        StartCounting();
    }

    private void OnDisable()
    {
        if (_countDownCoroutine != null)
            StopCoroutine(_countDownCoroutine);
    }

    private void StartCounting()
    {
        if (_countDownCoroutine != null)
            StopCoroutine(_countDownCoroutine);
        _countDownCoroutine = StartCoroutine(Counting());
    }

    private IEnumerator Counting()
    {
        DateTime targetDate = SeasonPassManager.Instance.GetCurrentSeasonPassDBData().EndDateTime;
        _BuyButton.interactable = SeasonPassManager.Instance.IsSeasonPassPremiumCanPurchase();
        while (true)
        {
            TimeSpan timeLeft = targetDate - DateTimeData.GetDateTimeUTC();
            if (timeLeft.Days > 0)
            {
                _TimeText.text = string.Format(LocalizationManager.Instance.GetText("TEXT_DAY_LEFT"), timeLeft.Days);
                yield break;
            }
            else if (timeLeft.TotalSeconds > 0)
            {
                _TimeText.text = timeLeft.ToString(@"hh\:mm\:ss");
            }
            else
            {
                timeLeft = TimeSpan.Zero;
                _TimeText.text = timeLeft.ToString(@"hh\:mm\:ss");
                _BuyButton.interactable = false;
                _MoreButton.interactable = _BuyButton.interactable;
                yield break;
            }
            yield return new WaitForSeconds(1f);
        }
    }

    #region Methods
    public void SetData()
    {
        string spriteKey = "BUND_SEASON_PASS_" + SeasonPassManager.Instance.GetCurrentSeasonPassDBData().SeasonPassIndex;
        _Image.sprite = ShopManager.LoadImageByItemKey(ShopTypes.Promotion, spriteKey);
        PriceData priceData = SeasonPassManager.Instance.GetSeasonPassPremiumPrice();

        TextMeshProUGUI priceText = _BuyButton.GetComponentInChildren<TextMeshProUGUI>();
        if (SeasonPassManager.Instance.IsSeasonPassBePremium())
        {
            priceText.text = LocalizationManager.Instance.GetText("TEXT_PURCHASED");
        }
        else
        {
            priceText.text = priceData.Amount + " " + GameHelper.TMPro_GetEmojiCurrency(priceData.CurrencyType);
        }

        bool canPurchase = IsCanPurchase();
        _BuyButton.interactable = canPurchase;
        _MoreButton.interactable = _BuyButton.interactable;
    }

    public bool IsCanPurchase()
    {
        return SeasonPassManager.Instance.IsSeasonPassPremiumCanPurchase();
    }

    public void SetEvent(UnityAction onClickBuySeasonPass)
    {
        _BuyButton.onClick.RemoveAllListeners();
        _BuyButton.onClick.AddListener(onClickBuySeasonPass);

        _MoreButton.onClick.RemoveAllListeners();
        _MoreButton.onClick.AddListener(onClickBuySeasonPass);
    }

    #endregion

}
