﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PromotionUIPage : ShopPageUI
{
    public PromotionDailyFreeUI FreeDeal;
    public PromotionDailyDealUI DailyDeal;
    public PromotionWeeklyDealUI WeeklyDeal;

    [Header("Season Pass")]
    [SerializeField] private GameObject _SeasonUITransform;
    [SerializeField] private SeasonPassShopUI _SeasonPassUI;

    [Header("Promotion Sequence")]
    private LongShopItemUICell _PromoSequence;

    [Header("Banner")]
    [SerializeField] private GameObject _BannerTransform;
    [SerializeField] private ShopBanner _Banner;

    [Header("Promotion")]
    [SerializeField]
    private LongShopItemUICell prefab;

    [SerializeField]
    private Transform _spawnTransform;

    [SerializeField]
    private VerticalLayoutGroup _verticalGroup;

    private List<LongShopItemUICell> _shopUIElementList;

    public static event Action onUpdateSeconds;
    private Coroutine _updateTickRoutine;

    private ShopItemData weeklyDealItem;
    private ShopItemData dailyDealItem;

    private UnityAction<ShopItemData> _onClickBuy;

    private void OnEnable()
    {
        if (_updateTickRoutine != null)
        {
            StopCoroutine(_updateTickRoutine);
        }
        _updateTickRoutine = StartCoroutine(UpdateTick());
    }

    private void OnDisable()
    {
        if (_updateTickRoutine != null)
        {
            StopCoroutine(_updateTickRoutine);
        }
    }

    IEnumerator UpdateTick()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            onUpdateSeconds?.Invoke();
        }
    }

    public void Initialize(ShopData promotionShopData, ShopData promotionSequenceShopData, ShopItemData weeklyDealItem, ShopItemData dailyDealItem)
    {
        ClearItemList();
        SetSequenceItem(promotionSequenceShopData);
        CreateButtons(promotionShopData.ShopItemDataList);
        if (_SeasonPassUI.IsCanPurchase())
        {
            _SeasonPassUI.SetData();
        }
        else
        {
            if (_SeasonUITransform != null)
            {
                _SeasonUITransform.transform.SetParent(null);
                Destroy(_SeasonUITransform);
                _SeasonUITransform = null;
            }
        }

        if (DataManager.Instance.FirstTopupStatus == true)
        {
            if (_BannerTransform != null)
            {
                _BannerTransform.transform.SetParent(null);
                Destroy(_BannerTransform);
                _BannerTransform = null;
            }
        }

        this.weeklyDealItem = weeklyDealItem;
        this.dailyDealItem = dailyDealItem;

        base.UpdateLayout();
    }

    private void SetSequenceItem(ShopData promotionSequenceShopData)
    {
        List<ShopItemData> itemList = promotionSequenceShopData.ShopItemDataList;
        if (itemList == null) return;

        bool isCreated = false;
        foreach (ShopItemData item in itemList)
        {
            if (item.IsBuyable == false)
                continue;
            
            if (_PromoSequence == null)
            {
                _PromoSequence = Instantiate(prefab, _verticalGroup.transform).GetComponent<LongShopItemUICell>();
                _PromoSequence.transform.SetSiblingIndex(_spawnTransform.GetSiblingIndex());
            }
            // set.
            _PromoSequence.SetData(item);
            _PromoSequence.BuyButton.onClick.RemoveAllListeners();
            _PromoSequence.BuyButton.onClick.AddListener(delegate ()
            {
                _onClickBuy?.Invoke(item);
            });

            _PromoSequence.MoreButton.onClick.RemoveAllListeners();
            _PromoSequence.MoreButton.onClick.AddListener(delegate ()
            {
                _onClickBuy?.Invoke(item);
            });

            isCreated = true;
            break;
        }
        
        if (isCreated == false && _PromoSequence != null)
        {
            Destroy(_PromoSequence.gameObject);
            _PromoSequence = null;
        }
    }

    public void SetEvent(UnityAction<ShopItemData> OnClickShopItem, UnityAction OnClickBuySeasonPass, UnityAction OnClickFreeDeal, UnityAction OnClickWeeklyDeal, UnityAction OnClickDailyDeal)
    {
        // Setup Weekly Deal 
        WeeklyDeal.SetData(weeklyDealItem, DataManager.Instance.GetWeeklyDealRecord(), OnClickWeeklyDeal);
        // Setup Daily Deal 
        DailyDeal.SetData(dailyDealItem, DataManager.Instance.GetDailyDealRecord(), OnClickDailyDeal);
        // Setup Free Deal
        FreeDeal.SetData(DataManager.Instance.GetFreeDealRecord(), OnClickFreeDeal);

        _SeasonPassUI.SetEvent(OnClickBuySeasonPass);
        SetEventOnClickBuyItem(OnClickShopItem);
    }

    public override void ShowUI()
    {
        if (gameObject.activeSelf)
            return;
        gameObject.SetActive(true);
        ScrollRect.onValueChanged.AddListener(OnScrolling);
    }

    public override void HideUI()
    {
        gameObject.SetActive(false);
        ScrollRect.onValueChanged.RemoveListener(OnScrolling);
    }



    private void CreateButtons(List<ShopItemData> itemList)
    {
        if (itemList == null) return;

        if (_shopUIElementList == null)
            _shopUIElementList = new List<LongShopItemUICell>();

        foreach (ShopItemData item in itemList)
        {
            if (item.IsBuyable == false)
                continue;

            LongShopItemUICell button = Instantiate(prefab, _verticalGroup.transform).GetComponent<LongShopItemUICell>();
            button.transform.SetSiblingIndex(_spawnTransform.GetSiblingIndex());
            button.SetData(item);
            button.BuyButton.onClick.RemoveAllListeners();
            button.BuyButton.onClick.AddListener(delegate ()
            {
                _onClickBuy?.Invoke(item);
            });

            button.MoreButton.onClick.RemoveAllListeners();
            button.MoreButton.onClick.AddListener(delegate ()
            {
                _onClickBuy?.Invoke(item);
            });

            _shopUIElementList.Add(button);
        }
    }

    public override void SetEventOnClickBuyItem(UnityAction<ShopItemData> OnClickBuy)
    {
        _onClickBuy = OnClickBuy;
    }

    private void ClearItemList()
    {
        if (_shopUIElementList != null && _shopUIElementList.Count > 0)
        {
            foreach (LongShopItemUICell button in _shopUIElementList)
            {
                DestroyImmediate(button.gameObject);
            }

            _shopUIElementList.Clear();
        }
    }
}
