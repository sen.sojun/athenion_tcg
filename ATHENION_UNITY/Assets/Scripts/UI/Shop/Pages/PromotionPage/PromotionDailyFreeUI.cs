﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;
using Karamucho;

public class PromotionDailyFreeUI : MonoBehaviour
{
    #region Public Properties    
    public TextMeshProUGUI DetailText;
    public TextMeshProUGUI TimeText;
    public TextMeshProUGUI PriceText;
    public Image Image;
    public Button BuyButton;
    public Button MoreButton;

    public DealRecord Record { get; private set; }
    #endregion

    #region Private Properties
    private DateTime _targetDate;
    private UnityAction _onClickButton = null;

    private Coroutine countDownCoroutine;
    #endregion

    private void OnEnable()
    {
        StartCounting();
    }

    private void OnDisable()
    {
        if (countDownCoroutine != null)
            StopCoroutine(countDownCoroutine);
    }

    private void StartCounting()
    {
        if (countDownCoroutine != null)
            StopCoroutine(countDownCoroutine);
        countDownCoroutine = StartCoroutine(Counting());
    }

    private IEnumerator Counting()
    {
        while (true)
        {
            UpdateCountdown();
            yield return new WaitForSeconds(1f);
        }
    }

    #region Methods
    public void SetData(DealRecord record, UnityAction onClickButton)
    {
        Record = record;
        _onClickButton = onClickButton;

        _targetDate = GameHelper.ISOTimeStrToUTCDateTime(Record.TimeExpireStr);
        _targetDate = _targetDate.ToLocalTime(); // In thai, GMT+7 

        SetNameText();
        //SetImage();

        BuyButton.onClick.RemoveAllListeners();
        BuyButton.onClick.AddListener(this.OnClickButton);
        BuyButton.interactable = !record.IsRedeem;
        PriceText.text = Record.IsRedeem ? LocalizationManager.Instance.GetText("BUTTON_CLAIMED") : LocalizationManager.Instance.GetText("BUTTON_FREE");

        MoreButton.onClick = BuyButton.onClick;
        MoreButton.interactable = BuyButton.interactable;

        UpdateCountdown();
    }

    private void SetImage()
    {
        string path = "Images/Shops/" + ShopManager.ItemFreeGiftImagePrefix + "_" + Record.ItemID;
        Debug.Log(path);

        Sprite sprite = ResourceManager.Load<Sprite>(path);
        Image.sprite = sprite;

        if (sprite == null)
        {
            Image.color = Color.red;
        }
    }

    private void UpdateCountdown()
    {
        TimeSpan timeLeft = _targetDate - DateTimeData.GetDateTimeUTC().ToLocalTime();
        if (timeLeft.TotalSeconds < 0)
        {
            timeLeft = TimeSpan.Zero;
        }
        TimeText.text = timeLeft.ToString(@"hh\:mm\:ss");
    }

    private void SetNameText()
    {
        DetailText.text = "";
        //DetailText.text = string.Format(
        //     "{0}{1}"
        //    , LocalizationManager.Instance.GetItemDescription(Record.ItemID)
        //    , ((Record.IsRedeem) ? " - CLAIMED" : "")
        //);
    }

    #endregion

    #region Event Methods
    public void OnClickButton()
    {
        _onClickButton?.Invoke();
    }
    #endregion
}
