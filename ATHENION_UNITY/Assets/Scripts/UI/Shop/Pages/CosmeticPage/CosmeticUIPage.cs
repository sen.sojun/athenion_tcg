﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

public class CosmeticUIPage : ShopPageUI
{
    [SerializeField] protected ShopItemUICell _prefab;
    [SerializeField] protected Transform _spawnPoint;

    private List<ShopItemUICell> _shopUIElementList;

    private UnityAction<ShopItemData> _onClickBuy;
    private UnityAction<int> _onClickDropdown;

    [SerializeField] TMP_Dropdown _Dropdown;

    public void Initialize()
    {
        ClearItemList();
        _Dropdown.options = new List<TMP_Dropdown.OptionData>() { new TMP_Dropdown.OptionData(LocalizationManager.Instance.GetText("TEXT_COSMETIC_ALL")) };
        CosmeticType[] types = (CosmeticType[])Enum.GetValues(typeof(CosmeticType));
        foreach (CosmeticType type in types)
        {
            string key = string.Format("{0}_{1}", "TEXT_COSMETIC", type.ToString());
            string localizeText = LocalizationManager.Instance.GetText(key);
            _Dropdown.options.Add(new TMP_Dropdown.OptionData(localizeText));
        }

        _Dropdown.onValueChanged.RemoveAllListeners();
        _Dropdown.onValueChanged.AddListener((index) => _onClickDropdown?.Invoke(index));
    }

    public void SetData(List<ShopItemData> shopItemDataList)
    {
        ClearItemList();
        CreateButtons(shopItemDataList);
        base.UpdateLayout();
    }

    public void ForceSetDropdownUI(int value)
    {
        string key = "";
        if (value == 0)
        {
            key = "TEXT_COSMETIC_ALL";
        }
        else
        {
            int cosmeticTypeIndex = value - 1;
            key = string.Format("{0}_{1}", "TEXT_COSMETIC", ((CosmeticType)cosmeticTypeIndex).ToString());
        }
        _Dropdown.captionText.text = LocalizationManager.Instance.GetText(key);

        _Dropdown.value = value;
    }

    public override void SetEventOnClickBuyItem(UnityAction<ShopItemData> OnClickBuy)
    {
        _onClickBuy = OnClickBuy;
    }

    public void SetEventOnClickDropdown(UnityAction<int> OnClickDropdown)
    {
        _onClickDropdown = OnClickDropdown;
    }

    public override void HideUI()
    {
        gameObject.SetActive(false);
        ScrollRect.onValueChanged.RemoveListener(OnScrolling);
    }

    public override void ShowUI()
    {
        if (gameObject.activeSelf)
            return;
        gameObject.SetActive(true);
        ScrollRect.onValueChanged.AddListener(OnScrolling);
    }

    public virtual void ClearItemList()
    {
        if (_shopUIElementList != null && _shopUIElementList.Count > 0)
        {
            foreach (ShopItemUICell button in _shopUIElementList)
            {
                DestroyImmediate(button.gameObject);
            }

            _shopUIElementList.Clear();
        }
    }

    public virtual void CreateButtons(List<ShopItemData> itemList)
    {
        if (itemList == null) return;

        if (_shopUIElementList == null)
            _shopUIElementList = new List<ShopItemUICell>();

        foreach (ShopItemData item in itemList)
        {
            if (item.IsTimeValid == false)
                continue;

            ShopItemUICell button = Instantiate(_prefab, _spawnPoint).GetComponent<ShopItemUICell>();
            button.SetData(item);
            button.BuyButton.onClick.RemoveAllListeners();
            button.BuyButton.onClick.AddListener(delegate
            {
                _onClickBuy?.Invoke(item);
            });

            _shopUIElementList.Add(button);
        }
    }

}
