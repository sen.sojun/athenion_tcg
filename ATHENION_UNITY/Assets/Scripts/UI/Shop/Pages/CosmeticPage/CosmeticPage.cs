﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class CosmeticPage : ShopItemPage
{
    #region Inspector Properties 
    public override ShopPageUI UI => _UI;
    [SerializeField] private CosmeticUIPage _UI;

    public Dictionary<CosmeticType, List<ShopItemData>> CosmeticDict = new Dictionary<CosmeticType, List<ShopItemData>>();

    private readonly List<CosmeticType> _allType = new List<CosmeticType>() { CosmeticType.AVATAR, CosmeticType.CARDBACK, CosmeticType.CONSOLE, CosmeticType.SKIN, CosmeticType.TITLE };
    private int selectDropdownIndex = 0;
    #endregion

    #region Properties
    public override ShopTypes Type => ShopTypes.Cosmetic;
    #endregion

    public override void Initialize()
    {
        CosmeticDict = CreateCosmeticDict();
        CategorizeCosmeticDict(CosmeticDict, Data.GetFirstShopData());

        _UI.Initialize();

        if (selectDropdownIndex == 0)
        {
            _UI.SetData(GetShopItemDataList(_allType));
        }
        else
        {
            OnClickDropdown(selectDropdownIndex);
            _UI.ForceSetDropdownUI(selectDropdownIndex);
        }

        _UI.SetEventOnClickBuyItem(OnClickShopItem);
        _UI.SetEventOnClickDropdown(OnClickDropdown);
    }

    private void OnClickDropdown(int index)
    {
        Debug.Log(index);
        selectDropdownIndex = index;
        // 0 is mean all.
        if (index == 0)
        {
            _UI.SetData(GetShopItemDataList(_allType));
        }
        else
        {
            int cosmeticIndex = index - 1;
            //SKIN, CARDBACK, AVATAR, TITLE, CONSOLE 
            CosmeticType type = (CosmeticType)cosmeticIndex;
            if (CosmeticDict.ContainsKey(type) == true)
            {
                _UI.SetData(CosmeticDict[type]);
            }
            else
            {
                return;
            }
        }
    }

    private void CategorizeCosmeticDict(Dictionary<CosmeticType, List<ShopItemData>> cosmeticDict, ShopData data)
    {
        List<ShopItemData> shopItemDataList = data.ShopItemDataList;
        for (int i = 0; i < shopItemDataList.Count; i++)
        {
            CosmeticType type = shopItemDataList[i].CustomData.CosmeticType;
            ShopItemData item = shopItemDataList[i];
            cosmeticDict[type].Add(item);
        }
    }

    private Dictionary<CosmeticType, List<ShopItemData>> CreateCosmeticDict()
    {
        Dictionary<CosmeticType, List<ShopItemData>> result = new Dictionary<CosmeticType, List<ShopItemData>>();
        CosmeticType[] types = (CosmeticType[])Enum.GetValues(typeof(CosmeticType));
        foreach (CosmeticType type in types)
        {
            if (result.ContainsKey(type) == false)
            {
                result.Add(type, new List<ShopItemData>());
            }
        }
        return result;
    }

    public List<ShopItemData> GetShopItemDataList(List<CosmeticType> filter)
    {
        ShopData cosmeticShopData = Data.GetFirstShopData();
        return cosmeticShopData.GetAllShopItemList((item) =>
        {
            return filter.Contains(item.CustomData.CosmeticType);
        });
    }

    public override void ShowUI()
    {
        _UI.ShowUI();
    }

    public override void HideUI()
    {
        _UI.HideUI();
    }

}
