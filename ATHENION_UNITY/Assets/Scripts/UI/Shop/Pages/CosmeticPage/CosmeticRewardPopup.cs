﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CosmeticRewardPopup : MonoBehaviour
{
    private class DetailPage
    {
        public string cardID;
        public List<ItemData> itemDatas;

        public DetailPage(string cardID, List<ItemData> itemDatas)
        {
            this.cardID = cardID;
            this.itemDatas = itemDatas;
        }
    }

    public GameObject ItemPrefab;
    public Transform SpawnTransform;
    public TextMeshProUGUI NameText;

    public Button LeftButton;
    public Button RightButton;

    public Image AlternativeCardPromotionImage;

    [Header("Item Preview")]
    [SerializeField] private TooltipCell _Tooltip_FullDetail;
    [SerializeField] private TooltipCellHeroPreview _Tooltip_HeroPreview;
    [SerializeField] private TooltipCellHUDPreview _Tooltip_HUDPreview;
    [SerializeField] private TooltipCellCard _Tooltip_Card;

    #region Private Properties
    private static readonly string _namePrefix = "ITEM_NAME_";
    private static readonly string _infoPrefix = "ITEM_DESCRIPTION_";

    private PromotionItemDetailUICell _currentItem;
    private List<PromotionItemDetailUICell> _itemDetailUIList = new List<PromotionItemDetailUICell>();

    private int currentItem;

    public int CurrentPage { get => _currentPage; set => _currentPage = Mathf.Clamp(value, 0, PageList.Count - 1); }
    private int _currentPage;

    private List<DetailPage> PageList = new List<DetailPage>();


    #endregion

    public void Initialize(Dictionary<string, List<ItemData>> datas)
    {
        PageList = new List<DetailPage>();
        foreach (KeyValuePair<string, List<ItemData>> item in datas)
        {
            PageList.Add(new DetailPage(item.Key, item.Value));
        }

        CurrentPage = 0;
        LeftButton.onClick.RemoveAllListeners();
        LeftButton.onClick.AddListener(() =>
        {
            CurrentPage -= 1;
            GoPageIndex(CurrentPage);
        });

        RightButton.onClick.RemoveAllListeners();
        RightButton.onClick.AddListener(() =>
        {
            CurrentPage += 1;
            GoPageIndex(CurrentPage);
        });

        GoPageIndex(CurrentPage);
    }

    private void GoPageIndex(int index)
    {
        LeftButton.gameObject.SetActive(false);
        RightButton.gameObject.SetActive(false);

        bool isCanShowArrow = PageList.Count > 1;
        if (isCanShowArrow)
        {
            if (index == 0)
                RightButton.gameObject.SetActive(true);
            else if (index == PageList.Count - 1)
                LeftButton.gameObject.SetActive(true);
            else
            {
                LeftButton.gameObject.SetActive(true);
                RightButton.gameObject.SetActive(true);
            }
        }

        UpdateUI(PageList[index]);
    }

    private void UpdateUI(DetailPage detailPage)
    {
        string cardBaseID = GameHelper.GetCardBaseID(detailPage.cardID);

        AlternativeCardPromotionImage.sprite = ShopManager.LoadImageByItemKey(ShopTypes.BoosterPack, cardBaseID);
        ClearItemInfoObjects();
        GenerateItemInfoObjects(detailPage.itemDatas);
        NameText.text = LocalizationManager.Instance.GetCardName(cardBaseID);
        SelectItem(_itemDetailUIList[0].Data.ItemID);
    }

    public void ShowUI()
    {
        gameObject.SetActive(true);
    }

    public void HideUI()
    {
        gameObject.SetActive(false);
    }

    private void SelectItem(string id)
    {
        if (_currentItem?.Data.ItemID == id) return;

        _currentItem = _itemDetailUIList.Find(x => id == x.Data.ItemID);

        if (_currentItem != null)
        {
            foreach (PromotionItemDetailUICell item in _itemDetailUIList)
            {
                item.SetGlow(false);
            }

            _currentItem.SetGlow(true);
            UpdatePreviewItem(_currentItem.Data.ItemID);
        }
    }

    private void UpdatePreviewItem(string key)
    {
        HideTooltip_Full();
        ShowTooltip_Full(key);
    }

    private void ClearItemInfoObjects()
    {
        for (int i = 0; i < _itemDetailUIList.Count; i++)
        {
            Destroy(_itemDetailUIList[i].gameObject);
            _itemDetailUIList.RemoveAt(i);
            i--;
        }
    }

    private void GenerateItemInfoObjects(List<ItemData> itemDetail)
    {
        foreach (ItemData item in itemDetail)
        {
            PromotionItemDetailUICell itemUI = Instantiate(ItemPrefab, SpawnTransform).GetComponent<PromotionItemDetailUICell>();
            itemUI.SetData(item, () => SelectItem(item.ItemID));
            _itemDetailUIList.Add(itemUI);
        }
    }

    #region Show Preview
    public void ShowTooltip_Full(string key)
    {
        string baseKey = key.Split(':')[0];
        string titleText = LocalizationManager.Instance.GetText(_namePrefix + baseKey);
        string detailText = LocalizationManager.Instance.GetText(_infoPrefix + baseKey);

        if (key.StartsWith("HUD"))
        {
            _Tooltip_HUDPreview.Show(key, titleText, detailText);
        }
        else if (key.StartsWith("H"))
        {
            _Tooltip_HeroPreview.Show(key, titleText, detailText);
        }
        else if (GameHelper.IsCardKey(key))
        {
            _Tooltip_Card.Show(key, titleText, detailText);
        }
        else
        {

            _Tooltip_FullDetail.Show(key, titleText, detailText);
        }

    }

    public void HideTooltip_Full()
    {
        _Tooltip_FullDetail.Hide();
        _Tooltip_HeroPreview.Hide();
        _Tooltip_Card.Hide();
        _Tooltip_HUDPreview.Hide();
    }

    #endregion
}
