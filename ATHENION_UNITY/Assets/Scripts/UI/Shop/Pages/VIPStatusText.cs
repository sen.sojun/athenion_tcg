﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class VIPStatusText : MonoBehaviour
{
    public TextMeshProUGUI TEXT_VIPExpire;
    public GameObject Group_VIP;

    private PlayerVIPData VIPData;
    private Coroutine countDownCoroutine;

    public void Initialize()
    {
        //VIPData = DataManager.Instance.InventoryData.VIPData;
        //bool isValid = IsVIPValid(VIPData);
        //if (isValid)
        //{
        //    Group_VIP.SetActive(true);
        //    StartCounting();
        //}
        //else
        //    Group_VIP.SetActive(false);
    }

    private void OnEnable()
    {
        VIPData = DataManager.Instance.InventoryData.VIPData;
        bool isValid = IsVIPValid(VIPData);
        if (isValid)
        {
            Group_VIP.SetActive(true);
            StartCounting();
        }
        else
            Group_VIP.SetActive(false);
    }

    private bool IsVIPValid(PlayerVIPData VIPData)
    {
        bool isExpired = DateTimeData.GetDateTimeUTC() > VIPData.ExpiredDateTime;
        bool isVIP = VIPData.IsVIP();
        return (isExpired == false && isVIP);
    }

    private void OnDisable()
    {
        if (countDownCoroutine != null)
            StopCoroutine(countDownCoroutine);
    }

    private void StartCounting()
    {
        if (countDownCoroutine != null)
            StopCoroutine(countDownCoroutine);
        countDownCoroutine = StartCoroutine(Counting());
    }

    private void StopCounting()
    {
        if (countDownCoroutine != null)
            StopCoroutine(countDownCoroutine);
    }

    private IEnumerator Counting()
    {
        while (true)
        {
            UpdateVIPExpire();
            yield return new WaitForSeconds(1f);
        }
    }

    private void UpdateVIPExpire()
    {
        if (IsVIPValid(VIPData))
        {
            DateTime vipExpire = VIPData.ExpiredDateTime;
            TimeSpan remainingValue = vipExpire.Subtract(DateTimeData.GetDateTimeUTC());

            string vipText = "";
            if (remainingValue.Days > 0)
            {
                vipText = string.Format(
                      LocalizationManager.Instance.GetText("PROFILE_VIP_EXPIRE_DATE_TIME")
                    , remainingValue.Days
                    , (remainingValue.Days > 1) ? "s" : ""
                    , remainingValue.Hours
                    , remainingValue.Minutes
                    , remainingValue.Seconds
                );
            }
            else
            {
                vipText = string.Format(
                      LocalizationManager.Instance.GetText("PROFILE_VIP_EXPIRE_TIME")
                    , remainingValue.Hours
                    , remainingValue.Minutes
                    , remainingValue.Seconds
                );
            }

            TEXT_VIPExpire.text = vipText;
        }
        else
        {
            StopCounting();
        }
    }
}
