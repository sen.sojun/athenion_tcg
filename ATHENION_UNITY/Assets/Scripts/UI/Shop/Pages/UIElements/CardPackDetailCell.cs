﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class CardPackDetailCell : MonoBehaviour
{
    public const string CARD_RARITY_PRIFIX = "CARD_RARITY";

    [SerializeField]
    public TMP_ColorGradient[] preset;
    public TextMeshProUGUI Rarity;
    public TextMeshProUGUI CardName;
    public TextMeshProUGUI Rate;

    public void SetData(CardRarity rarity, string cardName, string rate)
    {
        Rarity.colorGradientPreset = GetRarityTextColorPreset(rarity);

        string rarityText = GetRarityLocalize(rarity);
        Rarity.text = rarityText;
        CardName.text = cardName;
        Rate.text = rate + "%";
    }

    private TMP_ColorGradient GetRarityTextColorPreset(CardRarity cardRarity)
    {
        int index = (int)cardRarity;
        if (index < preset.Length)
            return preset[index];
        else
            return preset[0];
    }

    private string GetRarityLocalize(CardRarity rarity)
    {
        return LocalizationManager.Instance.GetText(CARD_RARITY_PRIFIX + "_" + rarity.ToString().ToUpper());
    }
}
