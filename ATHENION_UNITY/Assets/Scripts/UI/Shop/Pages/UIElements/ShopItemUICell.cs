﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Karamucho;

public class ShopItemUICell : MonoBehaviour
{
    #region Public Properties
    public TextMeshProUGUI NameText;
    public Button BuyButton;
    public Button MoreButton;
    public Image Image;
    public TextMeshProUGUI ButtonText;
    public ShopItemData Data { get; private set; }
    public GameObject OwnPanel;

    private VirtualCurrency _currency;
    #endregion

    #region Private Properties 
    #endregion

    #region Methods
    public void SetData(ShopItemData data)
    {
        this.Data = data;
        this._currency = data.Currency;
        UpdateUI();
    }

    private void UpdateUI()
    {
        SetupButton();
        UpdateImage();
    }

    private void UpdateImage()
    {
        string itemKey = GameHelper.ConvertItemIDToItemKey(Data.ItemId);
        //string path = "Images/Shops/" + ShopManager.Instance.GetImageByItemKey(Data.ShopType, itemKey);

        Sprite sprite = ShopManager.LoadImageByItemKey(Data.ShopType, itemKey);
        Image.sprite = sprite;
    }

    private void SetupButton()
    {
        NameText.text = LocalizationManager.Instance.GetItemName(Data.ItemId);

        if (OwnPanel != null)
        {
            OwnPanel.SetActive(Data.IsAtLimitAmount);
        }
        ButtonText.text = ShopManager.GetPriceText(Data);
        BuyButton.interactable = Data.IsBuyable;
        ButtonText.transform.parent.GetComponent<Image>().color = Data.IsBuyable ? Color.white : new Color(0.5f, 0.5f, 0.5f, 1);
    }

    public void ClearData()
    {
        Data = null;
    }

    #endregion
}
