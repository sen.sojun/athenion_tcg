﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Karamucho;

public class LongShopItemUICell : MonoBehaviour
{
    #region Public Properties
    public static readonly int ShowItemIcon = 5; // Show 5 icons.

    public TextMeshProUGUI NameText;
    public TextMeshProUGUI TimeText;
    public TextMeshProUGUI ConfigText;

    public Button MoreButton;

    public TextMeshProUGUI ButtonText;
    public Button BuyButton;

    public Image Image;
    public ShopItemData Data { get; private set; }

    [Header("MoreInfo_Cell")]
    [SerializeField]
    private GameObject _InfoSpawnPoint;
    [SerializeField]
    private GameObject _ItemInfoCell;
    [SerializeField]
    private GameObject _MoreButton;

    [Header("Discount")]
    [SerializeField] private GameObject _DiscountPanel;
    [SerializeField] private TextMeshProUGUI _DiscountText;

    [Header("Tag")]

    [SerializeField] private BannerTag _RedTag;
    [SerializeField] private BannerTag _OrangeTag;


    public VirtualCurrency _currency;
    #endregion

    #region Private Properties 
    #endregion

    #region Methods

    public virtual void SetData(ShopItemData data)
    {
        this.Data = data;
        this._currency = data.Currency;
        UpdateUI();

        int dayLeft = (Data.EndDate - DateTimeData.GetDateTimeUTC()).Days;
        if (dayLeft < 1)
        {
            PromotionUIPage.onUpdateSeconds += UpdateExpireDateSecondFormat;
        }
    }

    private void OnDestroy()
    {
        PromotionUIPage.onUpdateSeconds -= UpdateExpireDateSecondFormat;
    }

    private void UpdateUI()
    {
        SetButtonText();
        SetDiscountPanel();

        //ShortDetailText.text = LocalizationManager.Instance.GetItemShortDescription(Data.ItemId);
        UpdateExpireDate();
        ConfigText.text = ShopManager.Instance.GetLimitText(Data.ItemId, Data.CustomData.Option);
        UpdateImage();
        UpdateTag();
        SetMoreInfoCell();
    }

    private void UpdateTag()
    {
        if (_RedTag != null && _OrangeTag != null)
        {
            _OrangeTag.Hide();
            _RedTag.Hide();

            ShopOption.TagOption tag = Data.CustomData.Option.Tag;
            if (tag != ShopOption.TagOption.NONE)
            {
                string text = LocalizationManager.Instance.GetText("TAG_" + tag.ToString().ToUpper());
                ShopOption.TagColorOption tagColor = Data.CustomData.Option.TagColor;

                if (tagColor == ShopOption.TagColorOption.ORANGE)
                {
                    _OrangeTag.SetText(text);
                    _OrangeTag.Show();
                }
                else if (tagColor == ShopOption.TagColorOption.RED)
                {
                    _RedTag.SetText(text);
                    _RedTag.Show();
                }
            }
        }
    }

    private void UpdateExpireDate()
    {
        int dayLeft = (Data.EndDate - DateTimeData.GetDateTimeUTC()).Days;
        if (dayLeft > 2)
        {
            bool isInfinityDay = ((Data.EndDate - Data.ReleaseDate).Days > 3650);
            if (isInfinityDay)
            {
                TimeText.text = string.Empty;
            }
            else
            {
                string expireDate = string.Format(LocalizationManager.Instance.GetText("TEXT_DAY_LEFT"), (Data.EndDate - DateTimeData.GetDateTimeUTC()).Days.ToString());
                TimeText.text = "<sprite name=S_CLOCK> " + expireDate;
            }
        }
        else
        {
            UpdateExpireDateSecondFormat();
        }
    }

    private void UpdateExpireDateSecondFormat()
    {
        TimeText.text = "<sprite name=S_CLOCK> " + (Data.EndDate - DateTimeData.GetDateTimeUTC()).ToString(GameHelper.TimeFormat);
    }

    private void SetMoreInfoCell()
    {
        List<ItemData> itemList = Data.CustomData.ItemDetail;
        for (int i = 0; i < itemList.Count; i++)
        {
            if (i >= ShowItemIcon)
            {
                if (itemList.Count > ShowItemIcon)
                {
                    _MoreButton.SetActive(true);
                    _MoreButton.transform.SetAsLastSibling();
                }
                else
                {
                    CreateItemInfoCell(itemList[i]);
                }
                break;
            }
            else
            {
                CreateItemInfoCell(itemList[i]);
            }
        }
    }

    private void CreateItemInfoCell(ItemData itemData)
    {
        GameObject infoObject = Instantiate(_ItemInfoCell.gameObject, _InfoSpawnPoint.transform);
        infoObject.transform.GetChild(0).GetComponent<Image>().sprite = SpriteResourceHelper.LoadSprite(itemData.ItemID, SpriteResourceHelper.SpriteType.ICON_Item);
        infoObject.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = itemData.Amount > 1 ? itemData.Amount.ToString("N0") : "";
        infoObject.GetComponent<TooltipAttachment>().Setup(itemData.ItemID);
    }

    private void UpdateImage()
    {
        string itemKey = GameHelper.ConvertItemIDToItemKey(Data.ItemId);
        //string path = "Images/Shops/" + ShopManager.Instance.GetImageByItemKey(Data.ShopType, itemKey);

        Sprite sprite = ShopManager.LoadImageByItemKey(Data.ShopType, itemKey);
        Image.sprite = sprite;
    }

    private void SetButtonText()
    {
        NameText.text = LocalizationManager.Instance.GetItemName(Data.ItemId);
        ButtonText.text = ShopManager.GetPriceText(Data);
    }

    private void SetDiscountPanel()
    {
        if (_DiscountPanel != null)
        {
            if (Data.DiscountTag == "")
            {
                _DiscountPanel.SetActive(false);
            }
            else
            {
                _DiscountPanel.SetActive(true);
                _DiscountText.text = Data.DiscountTag;
            }
        }
    }

    #endregion
}
