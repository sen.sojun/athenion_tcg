﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Karamucho;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class PromotionItemDetailUICell : MonoBehaviour, IPointerClickHandler
{
    public Image Image;
    public TextMeshProUGUI ItemIDText;
    public TextMeshProUGUI AmountText;
    public TooltipAttachment Tooltip;
    public GameObject GlowSelected;
    public ItemData Data { get { return _data; } }

    #region Private Properties
    private ItemData _data;
    private UnityAction _onClick;
    #endregion

    public void SetData(ItemData item, UnityAction onClick = null)
    {
        _data = item;
        _onClick = onClick;

        string itemKey = GameHelper.ConvertItemIDToItemKey(_data.ItemID);
        ItemIDText.text = LocalizationManager.Instance.GetItemName(_data.ItemID);
        AmountText.text = "x " + item.Amount.ToString("N0");
        Tooltip.Setup(itemKey);

        Sprite sprite = SpriteResourceHelper.LoadSprite(_data.ItemID, SpriteResourceHelper.SpriteType.ICON_Item); 
        Image.sprite = sprite;

    }


    public void OnPointerClick(PointerEventData eventData)
    {
        if (_onClick != null)
        {
            GlowSelected.SetActive(true);
            _onClick?.Invoke();
        }
    }

    public void SetGlow(bool isActive)
    {
        GlowSelected.SetActive(isActive);
    }

}
