﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LongVIPShopItemUICell : LongShopItemUICell
{
    [SerializeField]
    private VIPStatusText vipStatusText;

    public override void SetData(ShopItemData data)
    {
        base.SetData(data);
        vipStatusText.Initialize();
    }
}
