﻿using Karamucho;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Coffee.UIExtensions;

public class CardPackUICell : MonoBehaviour
{
    #region Public Properties
    public Image Image;
    public Button ShopButton;
    public Button OpenButton;
    public Button MoreButton;
    public Button PreviewButton;
    public TextMeshProUGUI NameText;
    public TextMeshProUGUI AmountText;
    public BoosterPackShopData Data { get; private set; }
    public int CurrentAmount { get; private set; }

    [Header("Effect")]
    public UIShiny Effect_Shiny;
    #endregion

    public void SetData(BoosterPackShopData data, int currentAmount)
    {
        this.Data = data;
        this.CurrentAmount = currentAmount;
        UpdateUI();

    }

    public void SetCurrentAmount(int currentAmount)
    {
        this.CurrentAmount = currentAmount;
        UpdateUI();
    }

    public void SetOnClickOpenEvent(UnityAction<string> onClickOpenButton)
    {
        OpenButton.onClick.RemoveAllListeners();
        OpenButton.onClick.AddListener(() => onClickOpenButton.Invoke(Data.PackID));
    }

    public void SetOnClickBuyEvent(UnityAction<BoosterPackShopData> onClickBuy)
    {
        ShopButton.onClick.RemoveAllListeners();
        ShopButton.onClick.AddListener(() => onClickBuy.Invoke(Data));
    }

    public void SetOnClickMoreInfoEvent(UnityAction<CardPackUICell> OnMoreInfoClick)
    {
        MoreButton.onClick.RemoveAllListeners();
        MoreButton.onClick.AddListener(() => OnMoreInfoClick.Invoke(this));
    }

    public void SetOnClickPreviewEvent(UnityAction<CardPackUICell> OnPreviewClick)
    {
        if (PreviewButton != null)
        {
            PreviewButton.onClick.RemoveAllListeners();
            PreviewButton.onClick.AddListener(() => OnPreviewClick.Invoke(this));
        }
    }

    private void UpdateUI()
    {
        NameText.text = LocalizationManager.Instance.GetText(Data.ShopName);
        Sprite sprite = ShopManager.LoadPackBanner(Data.PackID);
        Image.sprite = sprite;

        AmountText.text = CurrentAmount.ToString();

        Effect_Shiny.enabled = CurrentAmount > 0;

        if (PreviewButton != null)
        {
            bool hasSkin = Data.CosmeticRewards != null;
            PreviewButton.gameObject.SetActive(hasSkin);
        }
    }

}
