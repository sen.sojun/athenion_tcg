﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using DG.Tweening;

public abstract class ShopPageUI : MonoBehaviour
{
    public RectTransform Content { get { if (ScrollRect != null) return ScrollRect.content; else return null; } }
    #region Inspector Properties
    [SerializeField] public ScrollRect ScrollRect;
    [SerializeField] protected RectTransform Viewport;
    [SerializeField] protected CanvasGroup _canvasGroup;
    #endregion

    #region Methods 
    
    public abstract void ShowUI();
    public abstract void HideUI();

    public virtual void SetEventOnClickBuyItem(UnityAction<ShopItemData> OnClickBuy) { }
    public virtual void OnExit() { }

    #region Layout

    public virtual void UpdateLayout()
    {
        SetEnableAllLayout(true);

        Content.anchoredPosition = Vector3.zero;
        LayoutRebuilder.ForceRebuildLayoutImmediate(Content);
    }

    public void SetScrollVerticalNormalize(float normalize, UnityAction onComplete = null)
    {
        DOVirtual.Float(1, normalize, .3f, (f) => ScrollRect.verticalNormalizedPosition = f).SetEase(Ease.OutBack).OnComplete(() => onComplete?.Invoke());
    }

    protected void OnScrolling(Vector2 value)
    {
        if (Content != null && Viewport != null && Content.transform.childCount > 0)
        {
            SetEnableLayoutComponent(false);

            // Show Only Visible Child.
            RectTransform child_rect = null;
            foreach (Transform child in Content.transform)
            {
                child_rect = child.GetComponent<RectTransform>();
                if (child_rect != null)
                {
                    if (GameHelper.IsRectTouchRect(child_rect, Viewport))
                    {
                        child.gameObject.SetActive(true);
                    }
                    else
                    {
                        child.gameObject.SetActive(false);
                    }
                }
            }
        }
    }

    protected void SetEnableAllLayout(bool isEnable)
    {
        if (isEnable)
        {
            foreach (Transform child in Content.transform)
            {
                child.gameObject.SetActive(true);
            }
        }

        SetEnableLayoutComponent(isEnable);
    }

    protected void SetEnableLayoutComponent(bool isEnable)
    {
        // Layout Component
        if (Content.GetComponent<ContentSizeFitter>() != null)
        {
            Content.GetComponent<ContentSizeFitter>().enabled = isEnable;
        }

        // All Layout Group
        LayoutGroup[] layoutGroups = Content.GetComponents<LayoutGroup>();
        if (layoutGroups != null && layoutGroups.Length > 0)
        {
            foreach (LayoutGroup layoutGroup in layoutGroups)
            {
                layoutGroup.enabled = isEnable;
            }
        }
    }
     
    #endregion

    #endregion
}
