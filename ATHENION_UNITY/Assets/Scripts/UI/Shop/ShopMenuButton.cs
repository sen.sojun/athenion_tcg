﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;

[RequireComponent(typeof(Button))]
public class ShopMenuButton : MonoBehaviour
{
    public ShopTypes pageType;

    private Button button;

    public void SetEvent(UnityAction<ShopTypes> onClick)
    {
        button = GetComponent<Button>();
        button.onClick.RemoveAllListeners();
        button.onClick.AddListener(() => onClick?.Invoke(pageType));
    }
}
