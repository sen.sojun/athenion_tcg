﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ShopUIManager : MonoBehaviour
{
    #region Public Properties
    public PopupConfirmPurchaseUI PopupConfirmUI;

    [SerializeField]
    private Canvas _canvas;
    [SerializeField]
    private PopupPromotionItemInfoUI PopupDetail;
    [SerializeField]
    private MenuSlider menuSlider;

    public PlayerCoinTopBarUI PlayerCoin;
    public PlayerDiamondTopBarUI PlayerDiamond;

    [SerializeField]
    private OpenPackPopupUI _openPackPopupUI;
    [SerializeField]
    private PurchasePackPopupUI _purchasePopupUI;

    #endregion

    #region Private Properties
    private ShopTypes _currentPageType;

    [SerializeField]
    private List<ShopMenuButton> _menuList = new List<ShopMenuButton>();

    #endregion

    #region Methods

    public void Initialize()
    {
        HidePopupConfirm();
    }

    public void SetMenuEventCallback(UnityAction<ShopTypes> onClickMenu)
    {
        for (int i = 0; i < _menuList.Count; i++)
        {
            _menuList[i].SetEvent(onClickMenu);
        }
    }

    public void UpdateTopbarTap(ShopTypes type)
    {
        SetMenuSlider(type);
    }

    private void SetMenuSlider(ShopTypes type)
    {
        int menuIndex = _menuList.FindIndex(menu => menu.pageType == type);
        if (menuIndex == -1)
        {
            Debug.LogWarning("Not found ShopTypes : " + type);
            menuIndex = 0;
        }
        menuSlider.MoveHighlightIndex(menuIndex);
    }

    public void HideUI()
    {
        _canvas.gameObject.SetActive(false);
    }

    public void ShowUI()
    {
        _canvas.gameObject.SetActive(true);
    }

    public void ShowPopupConfirm(ShopItemData data, VirtualCurrency currency, UnityAction onConfirm)
    {
        this.PopupConfirmUI.ShowUI(data, currency, onConfirm);
    }

    public void HidePopupConfirm()
    {
        this.PopupConfirmUI.HideUI();
    }

    //TODO: Implement show with different type here.
    public void ShowPopupDetail(ShopItemData data, UnityAction onConfirm, UnityAction onCancel)
    {
        if (data.CustomData.ShopItemType == ShopItemType.COSMETIC)
        {
            PopupUIManager.Instance.ShowPopup_PreviewShopBundle(data.ItemId, data, onConfirm, onCancel);
        }
        else // ETC.
        {
            PopupDetail.SetData(data);
            PopupDetail.ShowUI(
                delegate
                {
                    PopupDetail.HideUI();
                    onConfirm?.Invoke();
                }
                , delegate
                {
                    PopupDetail.HideUI();
                    onCancel?.Invoke();
                }
            );
        }
    }

    public void ShowMidLoading()
    {
        PopupUIManager.Instance.Show_MidSoftLoad(true);
    }

    public void HideMidLoading()
    {
        PopupUIManager.Instance.Show_MidSoftLoad(false);
    }

    public void ShowSoftLoading()
    {
        PopupUIManager.Instance.Show_SoftLoad(true);
    }

    public void HideSoftLoading()
    {
        PopupUIManager.Instance.Show_SoftLoad(false);
    }

    public void HideAllSoftLoading()
    {
        PopupUIManager.Instance.HideAllLoad();
    }
    #endregion

    #region Event Methods

    public void ShowGotItemResult(ItemData itemData)
    {
        string title = LocalizationManager.Instance.GetText("SHOP_PURCHASE_SUCCESS_HEADER");
        string message = string.Format(LocalizationManager.Instance.GetText("SHOP_YOU_GOT_ITEM"), LocalizationManager.Instance.GetItemName(itemData.ItemID));

        PopupUIManager.Instance.ShowPopup_GotReward(title, message, new List<ItemData>() { itemData });
    }

    public void ShowGotItemResult(string bundleID, List<ItemData> itemList, CustomButton customButton = null)
    {
        string title = LocalizationManager.Instance.GetText("SHOP_PURCHASE_SUCCESS_HEADER");
        string message = string.Format(LocalizationManager.Instance.GetText("SHOP_YOU_GOT_ITEM"), LocalizationManager.Instance.GetItemName(bundleID));

        PopupUIManager.Instance.ShowPopup_GotReward(title, message, itemList, null, customButton);
    }

    #endregion
}
