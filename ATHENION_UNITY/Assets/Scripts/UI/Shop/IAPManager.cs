﻿using Newtonsoft.Json;
using PlayFab;
using PlayFab.ClientModels;
using PlayFab.Json;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Purchasing;

public class IAPManager : IStoreListener
{
    private static IAPManager _instance;
    public static IAPManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = new IAPManager();
            return _instance;
        }
    }

    #region Public Properties
    // We are initialized when StoreController and Extensions are set and we are logged in
    public bool IsInitialized { get { return (_StoreController != null); } }
    public Dictionary<string, string> PriceStringList { get; private set; }
    public Dictionary<string, string> PriceCurrencyStringList { get; private set; }
    #endregion

    #region Private Properties
    // The Unity Purchasing system
    private IStoreController _StoreController;
    private UnityAction<bool, string> _initCallback;

    private string _productID = "";
    private UnityAction<Product> _onPurchaseComplete;
    private UnityAction<IAPError> _onPurchaseFail;
    #endregion

    #region Methods
    /// <summary>
    /// Initialize UnityIAP
    /// </summary>
    /// <param name="onComplete">Callback Initialize is success or fail</param>
    public void InitializePurchasing(UnityAction<bool, string> onComplete)
    {
        this._initCallback = onComplete;

        try
        {
            if (IsInitialized)
            {
                Debug.Log("IAP has already Initialized.");
                onComplete.Invoke(true, "");
                return;
            }
            // Create a builder for IAP service
#if UNITY_ANDROID
            var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance(AppStore.GooglePlay));
#elif UNITY_IOS
            var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance(AppStore.AppleAppStore));
#else
            Debug.LogError("IAPManager/InitializePurchasing: Failed not available valid store");
            onComplete?.Invoke(false); 
            return;
#endif

            RegisterProduct(builder);
            // Trigger IAP service initialization
            UnityPurchasing.Initialize(this, builder);
        }
        catch (Exception e)
        {
            Debug.LogError(e.Message);

            OnInitializeFailed(InitializationFailureReason.AppNotKnown);
        }
    }

    public void BuyProductID(string productID, UnityAction<Product> onComplete, UnityAction<IAPError> onFailed)
    {
        if (IsInitialized == false)
        {
            onFailed?.Invoke(new IAPError() { IAPErrorLog = "IAPManager : IsInitialized = false" });
            return;
        }

        _productID = productID;
        _onPurchaseComplete = onComplete;
        _onPurchaseFail = onFailed;

        _StoreController.InitiatePurchase(productID);
    }
    #endregion

    #region IAP Methods
    public string GetFailureReasonLocalizeText(PurchaseFailureReason failureReason)
    {
        return LocalizationManager.Instance.GetText("SHOP_PURCHASE_FAIL_" + failureReason.ToString().ToUpper());
    }

    private void RegisterProduct(ConfigurationBuilder builder)
    {
        foreach (KeyValuePair<ShopTypes, ShopTypeData> shopTypeDataElement in DataManager.Instance.GetShopTypeDataList())
        {
            foreach (KeyValuePair<string, ShopData> shopDataElement in shopTypeDataElement.Value.ShopDict)
            {
                AddBuilderProduct(builder, shopDataElement.Value);
            }
        }
    }

    /// <summary>
    /// Register each item from the catalog to builder
    /// </summary>
    /// <param name="builder"></param>
    /// <param name="value"></param>
    private void AddBuilderProduct(ConfigurationBuilder builder, ShopData value)
    {
        foreach (ShopItemData item in value.ShopItemDataList)
        {
            if (item.IsInAppPurchase)
            {
                builder.AddProduct(item.ItemId, ProductType.Consumable);
                //Debug.Log(item.ItemId + " is added.");
            }
        }
    }

    // This is automatically invoked automatically when IAP service is initialized
    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        _StoreController = controller;

        PriceStringList = new Dictionary<string, string>();
        PriceCurrencyStringList = new Dictionary<string, string>();
        string priceText;
        foreach (Product product in _StoreController.products.all)
        {
            string priceValueText = string.Format(GameHelper.PRICE_STRING_FORMAT, product.metadata.localizedPrice);
            priceText = string.Format("{0} {1}", product.metadata.isoCurrencyCode, priceValueText);
            PriceStringList.Add(product.definition.id, priceText);
            PriceCurrencyStringList.Add(product.definition.id, product.metadata.isoCurrencyCode);
        }

        //Debug.Log("OnInitialized");
        if (_initCallback != null)
        {
            _initCallback.Invoke(true, "");
        }
    }

    // This is automatically invoked automatically when IAP service failed to initialized
    public void OnInitializeFailed(InitializationFailureReason error)
    {
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
        if (_initCallback != null)
        {
            _initCallback.Invoke(false, "InitializationFailureReason : " + error);
        }
    }

    // This is automatically invoked automatically when purchase failed
    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        string log = GetFailureReasonLocalizeText(failureReason);
        Debug.Log(log);

        if (_onPurchaseFail != null)
        {
            _onPurchaseFail.Invoke(new IAPError() { IAPErrorLog = log });
        }
    }

    // This is invoked automatically when succesful purchase is ready to be processed
    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
    {
        // NOTE: this code does not account for purchases that were pending and are
        // delivered on application start.
        // Production code should account for such case:
        // More: https://docs.unity3d.com/ScriptReference/Purchasing.PurchaseProcessingResult.Pending.html

        if (!IsInitialized)
        {
            if (_onPurchaseFail != null)
            {
                Debug.Log("Error: IAP is not initialized.");
                _onPurchaseFail.Invoke(new IAPError() { IAPErrorLog = GetFailureReasonLocalizeText(PurchaseFailureReason.PurchasingUnavailable) });
            }
            return PurchaseProcessingResult.Complete;
        }
        // Test edge case where product is unknown
        if (e.purchasedProduct == null)
        {
            Debug.LogWarning("Attempted to process purchasewith unknown product. Ignoring");
            if (_onPurchaseFail != null)
            {
                _onPurchaseFail.Invoke(new IAPError() { IAPErrorLog = GetFailureReasonLocalizeText(PurchaseFailureReason.ProductUnavailable) });
            }
            return PurchaseProcessingResult.Complete;
        }

        // Test edge case where purchase has no receipt
        if (string.IsNullOrEmpty(e.purchasedProduct.receipt))
        {
            Debug.LogWarning("Attempted to process purchase with no receipt: ignoring");
            if (_onPurchaseFail != null)
            {
                _onPurchaseFail.Invoke(new IAPError() { IAPErrorLog = GetFailureReasonLocalizeText(PurchaseFailureReason.SignatureInvalid) });
            }
            return PurchaseProcessingResult.Complete;
        }

        Debug.Log("Processing transaction: " + e.purchasedProduct.transactionID);
        Debug.Log("Processing receipt: " + e.purchasedProduct.receipt);

#if UNITY_ANDROID
        {
            // Deserialize receipt
            var googleReceipt = GooglePurchase.FromJson(e.purchasedProduct.receipt);

            // Invoke receipt validation
            // This will not only validate a receipt, but will also grant player corresponding items
            // only if receipt is valid.

            string currencyCode = e.purchasedProduct.metadata.isoCurrencyCode;
            uint purchasePrice = (uint)(e.purchasedProduct.metadata.localizedPrice * 100);
            string receiptJson = googleReceipt.PayloadData.json;
            string signature = googleReceipt.PayloadData.signature;

            PlayFabClientAPI.ValidateGooglePlayPurchase(
                new ValidateGooglePlayPurchaseRequest()
                {
                    CurrencyCode = currencyCode   // Pass in currency code in ISO format
                    ,
                    PurchasePrice = purchasePrice // Convert and set Purchase price
                    ,
                    ReceiptJson = receiptJson     // Pass in the receipt
                    ,
                    Signature = signature         // Pass in the signature
                }
                , delegate (ValidateGooglePlayPurchaseResult result)
                {
                    Debug.Log("Validation successful!");

                    try
                    {
                        // Add appsflyer event
                        AppsFlyerObject.AddInAppPurchaseEvent(
                              googleReceipt.TransactionID
                            , DataManager.Instance.PlayerInfo.PlayerID
                            , _productID
                            , currencyCode
                            , purchasePrice
                            , 1
                        );
                    }
                    catch (Exception exception)
                    {
                        Debug.LogError(exception.Message);
                    }

                    if (_onPurchaseComplete != null)
                    {
                        _onPurchaseComplete.Invoke(e.purchasedProduct);
                    }
                }
                , delegate (PlayFabError error)
                {
                    Debug.LogFormat("Validation failed: Error:{0} {1}", error.Error, error.ErrorMessage);

                    PurchaseResult purchaseResult = new PurchaseResult()
                    {
                        PlayfabError = error
                    };

                    if (_onPurchaseFail != null)
                    {
                        _onPurchaseFail.Invoke(new IAPError() { IAPErrorLog = purchaseResult.GetErrorText() });
                    }
                }
            );
        }
        #elif UNITY_IOS
        {
            string currencyCode = e.purchasedProduct.metadata.isoCurrencyCode;
            int purchasePrice = (int)(e.purchasedProduct.metadata.localizedPrice * 100);
            Dictionary<string, string> receipt = JsonConvert.DeserializeObject<Dictionary<string, string>>(e.purchasedProduct.receipt);
            string receiptData = receipt["Payload"];

            PlayFabClientAPI.ValidateIOSReceipt(
                new ValidateIOSReceiptRequest()
                {
                    CurrencyCode = currencyCode   // Pass in currency code in ISO format
                    ,
                    PurchasePrice = purchasePrice // Convert and set Purchase price
                    ,
                    ReceiptData = receiptData     // Set receipt data
                }
                , delegate (ValidateIOSReceiptResult result)
                {
                    Debug.Log("Validation successful!");

                    try
                    {
                        // Add appsflyer event
                        AppsFlyerObject.AddInAppPurchaseEvent(
                              e.purchasedProduct.transactionID
                            , DataManager.Instance.PlayerInfo.PlayerID
                            , _productID
                            , currencyCode
                            , (uint)purchasePrice
                            , 1
                        );
                    }
                    catch (Exception exception)
                    {
                        Debug.LogError(exception.Message);
                    }

                    if (_onPurchaseComplete != null)
                    {
                        _onPurchaseComplete.Invoke(e.purchasedProduct);
                    }
                }
                , delegate (PlayFabError error)
                {
                    Debug.LogFormat("Validation failed: Error:{0} {1}", error.Error, error.ErrorMessage);

                    PurchaseResult purchaseResult = new PurchaseResult()
                    {
                        PlayfabError = error
                    };

                    if (_onPurchaseFail != null)
                    {
                        _onPurchaseFail.Invoke(new IAPError() { IAPErrorLog = purchaseResult.GetErrorText() });
                    }
                }
            );
        }
#endif

        return PurchaseProcessingResult.Complete;
    }
    #endregion
}

public struct IAPError
{
    public string IAPErrorLog;
    public string PlayfabErrorLog;
    public bool HasError { get { return IAPErrorLog != null || PlayfabErrorLog != null; } }

    public override string ToString()
    {
        if (IAPErrorLog == null) IAPErrorLog = "";
        if (PlayfabErrorLog == null) PlayfabErrorLog = "";

        return IAPErrorLog + PlayfabErrorLog;
    }
}

// The following classes are used to deserialize JSON results provided by IAP Service
// Please, note that Json fields are case-sensetive and should remain fields to support Unity Deserialization via JsonUtilities
public class JsonData
{
    // Json Fields, ! Case-sensetive

    public string orderId;
    public string packageName;
    public string productId;
    public long purchaseTime;
    public int purchaseState;
    public string purchaseToken;
}

public class PayloadData
{
    public JsonData JsonData;

    // Json Fields, ! Case-sensetive
    public string signature;
    public string json;

    public static PayloadData FromJson(string json)
    {
        var payload = JsonUtility.FromJson<PayloadData>(json);
        payload.JsonData = JsonUtility.FromJson<JsonData>(payload.json);
        return payload;
    }
}

public class GooglePurchase
{
    public PayloadData PayloadData;

    // Json Fields, ! Case-sensetive
    public string Store;
    public string TransactionID;
    public string Payload;

    public static GooglePurchase FromJson(string json)
    {
        var purchase = JsonUtility.FromJson<GooglePurchase>(json);
        purchase.PayloadData = PayloadData.FromJson(purchase.Payload);
        return purchase;
    }
}