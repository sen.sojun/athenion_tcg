﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Karamucho;
using UnityEngine.Events;

public class PopupPromotionItemInfoUI : MonoBehaviour
{
    public GameObject Prefab;
    public Transform SpawnTransform;

    public Transform LongImagePos;
    public Transform ShortImagePos;

    public Button CloseButton;
    public Button BuyButton;

    public TextMeshProUGUI BuyButtonText;

    public Image ItemImage;
    public TextMeshProUGUI NameText;

    private ShopItemData _data;
    private List<PromotionItemDetailUICell> _itemDetailUIList;

    public void ShowUI(UnityAction onBuy, UnityAction onClose)
    {
        BuyButton.onClick.RemoveAllListeners();
        BuyButton.onClick.AddListener(onBuy);

        CloseButton.onClick.RemoveAllListeners();
        CloseButton.onClick.AddListener(onClose);

        GameHelper.UITransition_FadeIn(this.gameObject);
    }

    public void HideUI()
    {
        GameHelper.UITransition_FadeOut(this.gameObject);
    }

    public void SetData(ShopItemData data)
    {
        if (_itemDetailUIList == null)
            _itemDetailUIList = new List<PromotionItemDetailUICell>();
        else
            ClearItemInfoObjects();
        this._data = data;
        GenerateItemInfoObjects(data.CustomData.ItemDetail);
        SetDetail(data);
        UpdateImage();
    }

    private void UpdateImage()
    {
        string itemKey = GameHelper.ConvertItemIDToItemKey(_data.ItemId);

        Sprite sprite = _data.ImagePath == string.Empty ? ShopManager.LoadImageByItemKey(_data.ShopType, itemKey) : ResourceManager.Load<Sprite>(ShopManager.ImagePath + _data.ImagePath);
        ItemImage.sprite = sprite;

        if (sprite != null)
        {
            float ratio = sprite.rect.width / sprite.rect.height;
            ItemImage.transform.position = ratio > 2f ? LongImagePos.position : ShortImagePos.position;
        }
    }

    private void SetDetail(ShopItemData data)
    {
        bool isDailyDealItem = DataManager.Instance.GetDailyDealRecord().ItemID == data.ItemId;
        bool isWeeklyDealItem = DataManager.Instance.GetWeeklyDealRecord().ItemID == data.ItemId;

        NameText.text = (isDailyDealItem || isWeeklyDealItem) ? "" : LocalizationManager.Instance.GetItemName(data.ItemId);
        //FullDetailText.text = LocalizationManager.Instance.GetItemDescription(data.ItemId);
        BuyButtonText.text = ShopManager.GetPriceText(data);
    }

    private void ClearItemInfoObjects()
    {
        for (int i = 0; i < _itemDetailUIList.Count; i++)
        {
            Destroy(_itemDetailUIList[i].gameObject);
            _itemDetailUIList.RemoveAt(i);
            i--;
        }
    }

    private void GenerateItemInfoObjects(List<ItemData> itemDetail)
    {
        foreach (ItemData item in itemDetail)
        {
            PromotionItemDetailUICell itemUI = Instantiate(Prefab, SpawnTransform).GetComponent<PromotionItemDetailUICell>();
            itemUI.SetData(item);
            _itemDetailUIList.Add(itemUI);
        }
    }
}
