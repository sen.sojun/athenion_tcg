﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using UnityEngine.Events;

public class PopupConfirmPurchaseUI : MonoBehaviour
{
    public Button ConfirmButton;
    public Button CloseButton;
    public TextMeshProUGUI TitleText;
    public TextMeshProUGUI DetailText;

    public void ShowUI(ShopItemData data, VirtualCurrency currency, UnityAction onConfirm, UnityAction onCancel = null)
    {
        GameHelper.UITransition_FadeIn(this.gameObject);
        SetData(data, currency);
        ConfirmButton.onClick.RemoveAllListeners();
        if (onConfirm != null) { ConfirmButton.onClick.AddListener(() => onConfirm.Invoke()); }

        CloseButton.onClick.RemoveAllListeners();
        CloseButton.onClick.AddListener(() =>
        {
            onCancel?.Invoke();
            HideUI();
        });
    }

    public void HideUI()
    {
        GameHelper.UITransition_FadeOut(this.gameObject);
    }

    private void SetData(ShopItemData data, VirtualCurrency currency)
    {
        string itemName = LocalizationManager.Instance.GetItemName(data.ItemId);
        string priceText = ShopManager.GetPriceText(data);

        string formatText = string.Format(LocalizationManager.Instance.GetText("SHOP_PURCHASE_DETAIL"), itemName, priceText);

        DetailText.text = formatText;
    }

}
