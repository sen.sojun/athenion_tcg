﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PopupLegendPackListUI : MonoBehaviour
{
    [SerializeField]
    private Button _CloseBtn;

    [SerializeField]
    private Transform _SpawnPoint;
    [SerializeField]
    private PackUICell _Prefab;

    private List<CardPackData> packDataList = new List<CardPackData>();
    private List<PackUICell> cellList = new List<PackUICell>();

    private void Awake()
    {
        _CloseBtn.onClick.AddListener(HideUI);
    }

    public void ShowUI()
    {
        GameHelper.UITransition_FadeIn(this.gameObject);
    }

    public void HideUI()
    {
        GameHelper.UITransition_FadeOut(this.gameObject);
    }

    public void SetData(List<CardPackData> data)
    {
        ClearCellList();
        packDataList = data;
        CreateUI();
    }

    private void ClearCellList()
    {
        for (int i = 0; i < cellList.Count; i++)
        {
            GameObject.Destroy(cellList[i].gameObject);
            cellList.RemoveAt(i);
            i--;
        }
    }

    private void CreateUI()
    {
        for (int i = 0; i < packDataList.Count; i++)
        {
            PackUICell obj = Instantiate(_Prefab.gameObject, _SpawnPoint).GetComponent<PackUICell>();
            obj.SetData(packDataList[i].PackID, packDataList[i].Amount);
            cellList.Add(obj);
        }
    }

    public void SetEvent(UnityAction<string> onClickOpen)
    {
        for (int i = 0; i < cellList.Count; i++)
        {
            cellList[i].SetOnClickOpenEvent(onClickOpen);
        }
    }
}
