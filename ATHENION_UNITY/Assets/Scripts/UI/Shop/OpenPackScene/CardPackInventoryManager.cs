﻿using PlayFab;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardPackInventoryManager : MonoBehaviour
{
    #region Event

    public delegate void CardPackInventoryManagerOpenPackHandler(PackReward packReward);
    public static event CardPackInventoryManagerOpenPackHandler OnOpenPack;

    #endregion

    public static CardPackInventoryManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<CardPackInventoryManager>();
            return _instance;
        }
    }
    private static CardPackInventoryManager _instance;

    private bool isInitialize;

    /// <summary>
    /// Should call one time only.
    /// </summary>
    public void Initialize()
    {
        Debug.Log("CardPackInventoryManager Initialize");
        CardPackController.Instance.SetBTNOpenAgain(OnClickOpenAgain);
        UpdateOpenAgainButton();
        isInitialize = true;
    }

    private void UpdateOpenAgainButton()
    {
        //PackManager.Instance.UpdateCardPackData();
        BoosterPackPage boosterPack = ShopManager.Instance.GetPage(ShopTypes.BoosterPack) as BoosterPackPage;
        CardPackData lastPackOpened = boosterPack.LastOpenPack;
        bool isValid = boosterPack.CanOpenPack(lastPackOpened.PackID, lastPackOpened.Amount);
        CardPackController.Instance.SetBTNOpenAgain_Enable(isValid);
    }

    #region Open again Process.

    private void OnClickOpenAgain()
    {
        PopupUIManager.Instance.Show_MidSoftLoad(true);

        BoosterPackPage boosterPack = ShopManager.Instance.GetPage(ShopTypes.BoosterPack) as BoosterPackPage;
        CardPackData lastOpenItem = boosterPack.LastOpenPack;
        boosterPack.OpenPack(lastOpenItem.PackID, lastOpenItem.Amount,
            delegate (PackReward pack)
            {
                Debug.Log("OnClickOpenAgain: Success!");

                CardPackController.Instance.StartSystem(
                      pack
                    , delegate ()
                    {
                        PopupUIManager.Instance.Show_MidSoftLoad(false);
                        UpdateOpenAgainButton();
                    }
                );
                OnOpenPack?.Invoke(pack);
            },
            delegate (string errorCode)
            {
                Debug.Log(errorCode);

                PopupUIManager.Instance.Show_MidSoftLoad(false);
                UpdateOpenAgainButton();
            }
        );
    }

    #endregion
}
