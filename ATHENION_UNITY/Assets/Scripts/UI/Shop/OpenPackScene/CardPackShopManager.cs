﻿using PlayFab;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardPackShopManager : MonoBehaviour
{
    #region Event
    public delegate void CardPackShopManagerPurchasePackHandler(CardPackData packDetail);
    public static event CardPackShopManagerPurchasePackHandler OnBuyPack;

    public static CardPackShopManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<CardPackShopManager>();
            return _instance;
        }
    }
    private static CardPackShopManager _instance;

    #endregion
    [SerializeField]
    private PopupPromotionItemInfoUI PopupDetail;

    private bool _isInitialize;


    private void Awake()
    {
        CardPackController.OnObjectDestroy += OnCardPackControllerDestroy;
    }

    public void Initialize()
    {
        Debug.Log("CardPackShopManager Initialize");
        CardPackController.Instance.SetBTNBuyMore(OnClickBuyAgain);

        UpdateBuyAgainButton();
        _isInitialize = true;
    }

    private void UpdateBuyAgainButton()
    {
        ShopItemData lastBuyItem = ShopManager.Instance.CacheLastItem;
        bool isEnoughMoney = DataManager.Instance.InventoryData.GetVirtualCurrency(lastBuyItem.Currency) >= lastBuyItem.Price;

        CardPackController.Instance.SetBTNBuyMore_Enable(isEnoughMoney);
    }

    private void OnCardPackControllerDestroy()
    {
        CardPackController.OnObjectDestroy -= OnCardPackControllerDestroy;
    }

    #region BuyAgain Process.

    private void OnClickBuyAgain()
    {
        CardPackController.Instance.BackToMenu();
        ((BoosterPackPage)ShopManager.Instance.GetPage(ShopTypes.BoosterPack)).ShowLastBuyPack();
    }

    #endregion
}
