﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class VersionTextUI : MonoBehaviour 
{
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnEnable()
    {
        string text = GameHelper.GetVersionText();

        Text unityText = GetComponent<Text>();
        if (unityText)
        {
            unityText.text = text;
        }

        TextMeshProUGUI textMeshProUGUIText = GetComponent<TextMeshProUGUI>();
        if (textMeshProUGUIText)
        {
            textMeshProUGUIText.text = text;
        }

        TextMeshPro TextMeshProText = GetComponent<TextMeshPro>();
        if (TextMeshProText)
        {
            TextMeshProText.text = text;
        }
    }
}
