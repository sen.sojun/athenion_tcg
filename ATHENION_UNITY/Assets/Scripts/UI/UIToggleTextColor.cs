﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIToggleTextColor : MonoBehaviour
{
    public TextMeshProUGUI TEXT_Target;
    public Color ToggleColor;

    private Color StartColor;

    private void Start()
    {
        StartColor = TEXT_Target.color;
    }

    private void Update()
    {
        if (GetComponent<Toggle>().isOn)
        {
            TEXT_Target.color = ToggleColor;
        }
        else
        {
            TEXT_Target.color = StartColor;
        }
    }
}
