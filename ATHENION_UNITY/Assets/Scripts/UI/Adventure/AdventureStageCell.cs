﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class AdventureStageCell : MonoBehaviour
{
    #region ENUM
    #endregion

    #region Inspector
    [Header("BTN")]
    [SerializeField] private Button BTN_Select;

    [Header("Text")]
    [SerializeField] private TMP_Text StageNumber;
    [SerializeField] private TMP_Text StageTitle;
    [SerializeField] private TMP_Text StageDescription;
    [SerializeField] private TMP_Text RewardAmount;
    [SerializeField] private TooltipAttachment Toolltip;

    [Header("Deco")]
    [SerializeField] private Image RewardImage;
    [SerializeField] private Image StageSpriteImage;
    [SerializeField] private GameObject IsPassedArrow;
    [SerializeField] private GameObject Icon_Passed;
    [SerializeField] private GameObject StageNumberGroup;
    #endregion

    #region Private Properties
    private int _stageIndex = 0;
    private AdventureStageData _data;
    private UnityAction _onClick;
    #endregion

    #region Method
    public void Setup(AdventureStageData stageData, int stageNumberIndex)
    {
        _stageIndex = stageNumberIndex;
        _data = stageData;
        _onClick = () => OnClickStartAdventure(stageData);

        UpdateAnimationState();
        InitBTN();
        SetText();

        if (stageData.ShowSpriteAtStageSelect)
        {
            StageSpriteImage.sprite = SpriteResourceHelper.LoadSprite(stageData.AdventureStageID.ToString(), SpriteResourceHelper.SpriteType.AdventureStage, true);
            StageSpriteImage.gameObject.SetActive(true);
            StageNumberGroup.SetActive(false);
        }
        else
        {
            StageSpriteImage.gameObject.SetActive(false);
            StageNumberGroup.SetActive(true);
        }

        // Check Reward
        DataManager.Instance.GetRewardedAdventureDict().TryGetValue(stageData.AdventureStageID.ToString(), out bool isPassed);
        Icon_Passed.SetActive(isPassed);

        // Setup Reward
        var rewardData = stageData.Rewards.First();
        Sprite sprite = SpriteResourceHelper.LoadSprite(rewardData.Key, SpriteResourceHelper.SpriteType.ICON_Item);
        
        RewardAmount.text = rewardData.Value.ToString();
        RewardImage.sprite = sprite;
        Toolltip.Setup(rewardData.Key);
    }

    private void OnClickStartAdventure(AdventureStageData adventureStageData)
    {
        if (adventureStageData.PlayerUseCustomDeck)
        {
            if (DataManager.Instance.IsHasDeckAvailable())
            {
                HomeManager.Instance.HomeUIManager.ShowSelectDeck(
                    GameMode.Adventure
                    , delegate(GameMode mode) 
                    {
                        PopupUIManager.Instance.Show_SoftLoad(true);
                        HomeManager.Instance.HomeUIManager.HideSelectDeck();
                        HomeManager.Instance.StartAdventure(adventureStageData.AdventureChapterID, adventureStageData.AdventureStageID);
                    },
                    delegate (int index)
                    {
                        List<DeckData> deckList = DataManager.Instance.GetAllDeck();
                        DataManager.Instance.SetCurrentDeckID(deckList[index].DeckID);

                        Debug.Log("OnSelectedDeck " + deckList[index].DeckName + " " + deckList[index].DeckID);
                    },
                    null);
            }
            else
            {
                string title = LocalizationManager.Instance.GetText("ERROR_FIND_DECK_NOT_FOUND");
                string message = LocalizationManager.Instance.GetText("");
                PopupUIManager.Instance.ShowPopup_ConfirmOnlyWithWarning(title, message);
            }
        }
        else
        {
            HomeManager.Instance.StartAdventure(adventureStageData.AdventureChapterID, adventureStageData.AdventureStageID);
        }
    }

    private void InitBTN()
    {
        BTN_Select.onClick.RemoveAllListeners();
        BTN_Select.onClick.AddListener(_onClick);
    }

    private void SetText()
    {
        StageNumber.text = _stageIndex.ToString();
        StageTitle.text = LocalizationManager.Instance.GetText("TEXT_ADVENTURE_UI_TITLE_" + _data.AdventureStageID.ToString());
        StageDescription.text = LocalizationManager.Instance.GetText("TEXT_ADVENTURE_UI_DESCRIPTION_" + _data.AdventureStageID.ToString());
    }
    #endregion

    #region Sound
    public void PlaySound_Unlock()
    {
        SoundManager.PlaySFX(SoundManager.SFX.Tutorial_UnlockStage);
    }
    #endregion


    #region Animation
    private void UpdateAnimationState()
    {
        /* TODO
        Animator animator = GetComponent<Animator>();
        string stateName = "";

        switch (_currentStageState)
        {
            case StageState.Lock:
                {
                    // DO nothing
                    break;
                }

            case StageState.Unlock:
                {
                    stateName = "IsUnlock";
                    break;
                }

            case StageState.Complete:
                {
                    stateName = "IsComplete";
                    break;
                }

            default:
                {
                    // DO nothing
                    break;
                }
        }

        if (stateName != "")
        {
            animator.SetTrigger(stateName);
        }
        */

    }
    #endregion
}
