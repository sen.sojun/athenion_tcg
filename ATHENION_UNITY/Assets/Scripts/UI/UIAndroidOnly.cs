﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIAndroidOnly : MonoBehaviour
{
    private void Awake()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            gameObject.SetActive(true);
        }
        else
        {
            gameObject.SetActive(false);
        }
    }
}
