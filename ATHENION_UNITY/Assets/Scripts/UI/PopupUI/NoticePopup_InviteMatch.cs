﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;

public class NoticePopup_InviteMatch : PopupUI
{
    [SerializeField] private TextMeshProUGUI _TEXT_Title;
    [SerializeField] private TextMeshProUGUI _TEXT_Message;
    [SerializeField] private Button _BTN_Okay;
    [SerializeField] private Button _BTN_Cancel;
    [SerializeField] private FriendInfoUI _FriendInfo;

    public override void Show(UnityAction onShowComplete)
    {
        SoundManager.PlaySFX(SoundManager.SFX.Intro_Versus);
        base.Show(onShowComplete);
    }

    public override void Hide()
    {
        base.Hide();
    }

    #region Setting
    public void SetBTN(UnityAction onClickOkay, UnityAction onClickCancel)
    {
        _BTN_Okay.onClick.RemoveAllListeners();
        _BTN_Okay.onClick.AddListener(delegate
        {
            onClickOkay?.Invoke();
            Hide();
        });

        _BTN_Cancel.onClick.RemoveAllListeners();
        _BTN_Cancel.onClick.AddListener(delegate
        {
            onClickCancel?.Invoke();
            Hide();
        });
    }

    public void SetText(string title, string message)
    {
        _TEXT_Title.text = title;
        _TEXT_Message.text = message;
    }

    public void SetFriendInfo(PlayerInfoUIData friendData)
    {
        _FriendInfo.SetData(friendData);
    }
    #endregion
}
