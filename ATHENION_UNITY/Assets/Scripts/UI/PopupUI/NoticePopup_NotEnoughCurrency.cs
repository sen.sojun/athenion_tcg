﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Karamucho.UI;
using TMPro;

public class NoticePopup_NotEnoughCurrency : PopupUI
{
    public Image CurrencyImage;
    public TextMeshProUGUI TitleText;
    public TextMeshProUGUI DetailText;

    public Button CloseBTN;
    public Button CancelBTN;
    public Button CustomBTN;

    public void SetData(string itemID, string title, string detail)
    {
        this.CurrencyImage.sprite = SpriteResourceHelper.LoadSprite(itemID, SpriteResourceHelper.SpriteType.ICON_Item);
        TitleText.text = title;
        DetailText.text = detail;
    }

    public void SetEvent(UnityAction onClose, CustomButton customButton = null)
    {
        ResetToDefault();
        SetCustomBtn(CustomBTN, customButton);
        if (customButton == null)
        {
            CloseBTN.onClick.AddListener(() =>
            {
                Hide();
                onClose?.Invoke();
            });
            CloseBTN.gameObject.SetActive(true);
        }
        else
        {
            CancelBTN.onClick.AddListener(() =>
            {
                Hide();
                onClose?.Invoke();
            });
            CancelBTN.gameObject.SetActive(true);
        }
    }

    private void ResetToDefault()
    {
        CloseBTN.onClick.RemoveAllListeners();
        CloseBTN.gameObject.SetActive(false);

        CancelBTN.onClick.RemoveAllListeners();
        CancelBTN.gameObject.SetActive(false);

        CustomBTN.onClick.RemoveAllListeners();
        CustomBTN.gameObject.SetActive(false);
    }
}
