﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

#region Global Enums
public enum PopupUITypes
{
    None = 0
    , Error
    , Confirm
    , SureCheck
    , InviteMatch
}
#endregion

public class PopupUIData
{
    #region Public Properties
    public string ID { get { return GetPopupID(); } }
    public string Title { get; private set; }
    public string Message { get; private set; }
    public PopupUITypes Type { get; private set; }
    #endregion

    #region Contructors
    public PopupUIData()
    {
    }

    public PopupUIData(string title, string message, PopupUITypes type)
    {
        Title = title;
        Message = message;
        Type = type;
    }

    public PopupUIData(PopupUIData data)
    {
        Title = data.Title;
        Message = data.Message;
        Type = data.Type;
    }
    #endregion

    #region Methods
    private string GetPopupID()
    {
        return string.Format("{0}-{1}-{2}", Type.ToString(), Title, Message);
    }
    #endregion
}

public class PopupUI : MonoBehaviour
{
    #region Public Properties
    public PopupUIData Data
    {
        get
        {
            if (_data == null)
                _data = new PopupUIData();
            return _data;
        }
    }
    #endregion

    #region Private Properties
    private PopupUIData _data;
    protected bool _isDontClose = false;
    private UnityAction _onHideComplete;
    #endregion

    #region Methods
    public void SetData(PopupUIData data)
    {
        _data = new PopupUIData(data);
    }

    public void SetDontClose(bool isDontClose)
    {
        _isDontClose = isDontClose;
    }

    public void SetOnHideComplete(UnityAction onComplete)
    {
        _onHideComplete = onComplete;
    }

    protected void SetCustomBtn(Button button, CustomButton custom)
    {
        if (custom == null)
        {
            button.gameObject.SetActive(false);
            return;
        }
        button.gameObject.SetActive(true);
        button.image.sprite = PopupUIManager.Instance.GetButtonSpriteColor(custom.Color);
        button.transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().text = custom.Text;
        button.onClick.RemoveAllListeners();
        button.onClick.AddListener(delegate
        {
            custom.Action?.Invoke();
            Hide();
        });
    }
    #endregion

    #region Virtual Methods
    public virtual void Show(UnityAction onShowComplete)
    {
        GameHelper.UITransition_FadeIn(this.gameObject, onShowComplete);
    }

    public virtual void Hide()
    {
        if (!_isDontClose)
        {
            GameHelper.UITransition_FadeOut(this.gameObject, _onHideComplete);
        }
        else
        {
            _onHideComplete?.Invoke();
        }
    }

    #endregion

    #region Static Methods
    #endregion
}
