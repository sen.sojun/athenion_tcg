﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;

public class NoticePopup_SeasonPassPopup : PopupUI
{
    [SerializeField] private Image _Image;
    [SerializeField] private Button _CancelButton;
    [SerializeField] private Button _BuyButton;
    [SerializeField] private TextMeshProUGUI _DetailText;

    public void Initialize()
    {
        string spriteKey = "BUND_SEASON_PASS_" + SeasonPassManager.Instance.GetCurrentSeasonPassDBData().SeasonPassIndex;
        _Image.sprite = ShopManager.LoadImageByItemKey(ShopTypes.Promotion, spriteKey);
        PriceData priceData = SeasonPassManager.Instance.GetSeasonPassPremiumPrice();

        TextMeshProUGUI priceText = _BuyButton.GetComponentInChildren<TextMeshProUGUI>();
        priceText.text = priceData.Amount + " " + GameHelper.TMPro_GetEmojiCurrency(priceData.CurrencyType);

        _DetailText.text = string.Format(LocalizationManager.Instance.GetText("TEXT_SEASONPASS_ADS"), SeasonPassManager.Instance.GetCurrentSeasonPassDBData().SeasonPassIndex);
    }

    public void SetEvent(UnityAction onBuy, UnityAction onCancel = null)
    {
        _CancelButton.onClick.RemoveAllListeners();
        _CancelButton.onClick.AddListener(() =>
        {
            onCancel?.Invoke();
            Hide();
        });

        _BuyButton.onClick.RemoveAllListeners();
        _BuyButton.onClick.AddListener(() =>
        {
            onBuy?.Invoke();
            Hide();
        });
    }

}
