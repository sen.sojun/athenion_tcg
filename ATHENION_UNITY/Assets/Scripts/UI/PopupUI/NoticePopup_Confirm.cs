﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;

public class NoticePopup_Confirm : PopupUI
{
    [SerializeField] protected TextMeshProUGUI _TEXT_Title;
    [SerializeField] protected TextMeshProUGUI _TEXT_Message;
    [SerializeField] protected Button _BTN_Okay;

    private UnityAction _onClickOkay;
     
    public override void Show(UnityAction onShowComplete)
    {
        _TEXT_Title.text = Data.Title;
        _TEXT_Message.text = Data.Message;

        base.Show(onShowComplete);
    }

    public void SetBTN(UnityAction onClickOkay)
    {
        _onClickOkay = onClickOkay; 

        _BTN_Okay.onClick.RemoveAllListeners();
        _BTN_Okay.onClick.AddListener(OnHide);
    }

    public void OnHide()
    {
        if (_isDontClose == false)
        {
            Hide();
        }
        _onClickOkay?.Invoke();
    }
     
}
