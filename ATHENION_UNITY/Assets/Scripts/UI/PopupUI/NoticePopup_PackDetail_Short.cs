﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class NoticePopup_PackDetail_Short : PopupUI
{
    #region Serialize Properties
    [SerializeField] private Toggle _TOGGLE_Agree;

    [SerializeField] private TextMeshProUGUI _TEXT_Title;
    [SerializeField] private TextMeshProUGUI _TEXT_Message;
    [SerializeField] private Button _BTN_Okay;

    [SerializeField] private GameObject _TITLE_BANNER;

    [Header("Prefab reward")]
    [SerializeField] private RewardCell _PrefabReward;
    [SerializeField] private Transform _Container;

    [SerializeField] private Button _CustomBtn;
    #endregion

    #region Private Properties
    private List<ItemData> _currentItemDataList = new List<ItemData>();
    private List<RewardCell> _rewardCellList = new List<RewardCell>();
    #endregion

    #region Method
    public override void Show(UnityAction onShowComplete)
    {
        if (Data.Title == null || Data.Title == "")
        {
            _TEXT_Title.gameObject.SetActive(false);
        }
        else
        {
            _TEXT_Title.text = Data.Title;
        }

        if (Data.Message == null || Data.Message == "")
        {
            _TEXT_Message.gameObject.SetActive(false);
        }
        else
        {
            _TEXT_Message.text = Data.Message;
        }

        base.Show(onShowComplete);
    }

    public void SetBTN(UnityAction onClickOkay, CustomButton customBtnData = null)
    {
        SetBTN(onClickOkay);
        SetCustomBtn(customBtnData);
    }

    public void SetBTN(UnityAction onClickOkay)
    {
        _BTN_Okay.onClick.RemoveAllListeners();
        _BTN_Okay.onClick.AddListener(delegate
        {
            Hide();
            onClickOkay?.Invoke();
        });
    }

    public void SetReward(List<ItemData> itemDataList)
    {
        foreach (ItemData item in itemDataList)
        {
            _rewardCellList.Add(CreateReward(item.ItemID, item.Amount));
        }
    }

    private void SetCustomBtn(CustomButton custom)
    {
        if (custom == null)
        {
            _CustomBtn.gameObject.SetActive(false);
            return;
        }
        _CustomBtn.gameObject.SetActive(true);
        _CustomBtn.image.sprite = PopupUIManager.Instance.GetButtonSpriteColor(custom.Color);
        _CustomBtn.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = custom.Text;
        _CustomBtn.onClick.RemoveAllListeners();
        _CustomBtn.onClick.AddListener(delegate
        {
            custom.Action?.Invoke();
            Hide();
        });
    }

    #endregion

    #region Cell Generate
    private RewardCell CreateReward(string rewardKey, int count)
    {
        GameObject obj = Instantiate(_PrefabReward.gameObject, _Container);
        RewardCell cell = obj.GetComponent<RewardCell>();
        cell.InitData(rewardKey, count);

        return cell;
    }
    #endregion

}
