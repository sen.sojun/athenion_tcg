﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;

public class NoticePopup_SureCheck : PopupUI
{
    [SerializeField] private TextMeshProUGUI _TEXT_Title;
    [SerializeField] private TextMeshProUGUI _TEXT_Message;
    [SerializeField] private Button _BTN_Okay;
    [SerializeField] private Button _BTN_Cancel;

    public override void Show(UnityAction onShowComplete)
    {
        _TEXT_Title.text = Data.Title;
        _TEXT_Message.text = Data.Message;

        base.Show(onShowComplete);
    }

    public void SetBTN(UnityAction onClickOkay, UnityAction onClickCancel)
    {
        _BTN_Okay.onClick.RemoveAllListeners();
        _BTN_Okay.onClick.AddListener(delegate
        {
            if (_isDontClose == false)
            {
                Hide();
            }
            onClickOkay?.Invoke();
        });

        _BTN_Cancel.onClick.RemoveAllListeners();
        _BTN_Cancel.onClick.AddListener(delegate
        {
            if (_isDontClose == false)
            {
                Hide();
            }
            onClickCancel?.Invoke();
        });
    }
    
}
