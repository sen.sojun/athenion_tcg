﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class NoticePopup_GotRewards : NoticePopup_Confirm
{
    #region Serialize Properties
    [Header("Prefab reward")]
    [SerializeField] private RewardCell _PrefabReward;
    [SerializeField] private Transform _Container;

    [SerializeField] private Button _CustomBtn;
    #endregion

    #region Private Properties
    private List<ItemData> _currentItemDataList = new List<ItemData>();
    private List<RewardCell> _rewardCellList = new List<RewardCell>();
    #endregion

    #region Method
    public void SetBTN(UnityAction onClickOkay,CustomButton customBtnData=null)
    {
        base.SetBTN(onClickOkay);
        SetCustomBtn(customBtnData);
    }

    public void SetReward(List<ItemData> itemDataList)
    {
        foreach (ItemData item in itemDataList)
        {
            _rewardCellList.Add(CreateReward(item.ItemID, item.Amount)); 
        }
    }

    private void SetCustomBtn(CustomButton custom)
    {
        if (custom == null)
        {
            _CustomBtn.gameObject.SetActive(false);
            return;
        }
        _CustomBtn.gameObject.SetActive(true);
        _CustomBtn.image.sprite = PopupUIManager.Instance.GetButtonSpriteColor(custom.Color);
        _CustomBtn.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = custom.Text;
        _CustomBtn.onClick.RemoveAllListeners();
        _CustomBtn.onClick.AddListener(delegate
        {
            custom.Action?.Invoke();
            Hide();
        });
    }

    #endregion

    #region Cell Generate
    private RewardCell CreateReward(string rewardKey, int count)
    {
        GameObject obj = Instantiate(_PrefabReward.gameObject, _Container);
        RewardCell cell = obj.GetComponent<RewardCell>();
        cell.InitData(rewardKey, count);

        return cell;
    }
    #endregion

}
