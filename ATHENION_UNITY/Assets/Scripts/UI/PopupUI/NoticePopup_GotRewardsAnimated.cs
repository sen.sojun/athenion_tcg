﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class NoticePopup_GotRewardsAnimated : NoticePopup_Confirm
{
    #region Serialize Properties
    [Header("Prefab reward")]
    [SerializeField] private RewardCell _PrefabReward;
    [SerializeField] private Transform _Container;
    #endregion

    #region Private Properties
    private List<ItemData> _currentItemDataList = new List<ItemData>();
    private List<RewardCell> _rewardCellList = new List<RewardCell>();

    private float _maxScale = 8f;
    private float _minScale = 1f;
    private float _animDur = 0.25f;
    private float _showDelay = 0.1f;
    private Ease _animEase = Ease.OutExpo;
    private GameObject _finishFx;
    #endregion

    #region Method

    public void SetReward(List<ItemData> itemDataList)
    {
        foreach (ItemData item in itemDataList)
        {
            RewardCell cell = CreateReward(item.ItemID, item.Amount);
            _rewardCellList.Add(cell);
            cell.gameObject.SetActive(false);
            //Debug.Log(string.Format("Rank {0} : Reward = {1},{2}", _rankIndex, item.Key, item.Value));
        }
        
        Invoke("ShowRewardRespectively", _showDelay);
    }

    public void ShowRewardRespectively()
    {
        if(_rewardCellList.Count > 0)
        {
            SoundManager.PlaySFX(SoundManager.SFX.GotReward_ItemPop);
            Transform curTrans = _rewardCellList[0].transform;
            _rewardCellList.Remove(_rewardCellList[0]);
            curTrans.gameObject.SetActive(true);
            curTrans.DOScale(_minScale, _animDur)
                .SetEase(_animEase)
                .OnComplete
                (
                    delegate ()
                    {
                        if(_rewardCellList.Count > 0)
                        {
                            //ShowRewardRespectively();
                            Invoke("ShowRewardRespectively", _showDelay);
                        }
                    }
                );
        }
    }
    
    public NoticePopup_GotRewardsAnimated SetMaxScale(float maxScale = 1.5f)
    {
        _maxScale = maxScale;
        return this;
    }
    public NoticePopup_GotRewardsAnimated SetMinScale(float minScale = 1f)
    {
        _minScale = minScale;
        return this;
    }
    public NoticePopup_GotRewardsAnimated SetAnimationDuration(float animDur = 0.75f)
    {
        _animDur = animDur;
        return this;
    }
    public NoticePopup_GotRewardsAnimated SetDelay(float showDelay = 0.5f)
    {
        _showDelay = showDelay;
        return this;
    }
    public NoticePopup_GotRewardsAnimated SetAnimationEase(Ease easing = Ease.Linear)
    {
        _animEase = easing;
        return this;
    }
    public NoticePopup_GotRewardsAnimated SetFinishEffect(GameObject finishFx)
    {
        _finishFx = finishFx;
        return this;
    }
    #endregion

    #region Cell Generate
    private RewardCell CreateReward(string rewardKey, int count)
    {
        GameObject obj = Instantiate(_PrefabReward.gameObject, _Container);
        obj.transform.localScale = new Vector3(_maxScale, _maxScale, obj.transform.localScale.z); //set scale(x and y) to _maxScale
        RewardCell cell = obj.GetComponent<RewardCell>();
        cell.InitData(rewardKey, count);

        return cell;
    }
    #endregion

}

