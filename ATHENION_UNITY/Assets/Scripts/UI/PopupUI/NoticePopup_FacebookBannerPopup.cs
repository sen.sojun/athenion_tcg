﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Karamucho;

public class NoticePopup_FacebookBannerPopup : PopupUI
{
    [Header("BTN")]
    [SerializeField] private Button BTN_Close;
    [SerializeField] private Button BTN_Global;
    [SerializeField] private Button BTN_TH;

    [Header("IMG Banner")]
    [SerializeField] private Image IMG_Popup;

    public void SetImage(string path)
    {
        Sprite sprite = ResourceManager.Load<Sprite>(path);
        if (sprite == null)
        {
            // if missing !
            sprite = SpriteResourceHelper.LoadSprite("Error", SpriteResourceHelper.SpriteType.BannerSeason);
            Debug.LogWarning("Missing Path:" + path);
        }
        IMG_Popup.sprite = sprite;
    }

    public void SetBTN()
    {
        BTN_Close.onClick.RemoveAllListeners();
        BTN_Close.onClick.AddListener(delegate
        {
            Hide();
        });

        BTN_Global.onClick.RemoveAllListeners();
        BTN_Global.onClick.AddListener(delegate
        {
            Hide();
            HomeManager.Instance.ClickFaceboook_Global();
        });

        BTN_TH.onClick.RemoveAllListeners();
        BTN_TH.onClick.AddListener(delegate
        {
            Hide();
            HomeManager.Instance.ClickFaceboook_TH();
        });
    }
}
