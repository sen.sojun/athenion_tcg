﻿using Karamucho;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class NoticePopup_ItemBundlePreview : PopupUI
{
    public Button BuyButton;
    public Button CancelButton;

    public GameObject ItemPrefab;
    public Transform SpawnTransform;

    public TextMeshProUGUI BuyButtonText;
    public TextMeshProUGUI NameText;

    [Header("Item Preview")]
    [SerializeField] private TooltipCell _Tooltip_FullDetail;
    [SerializeField] private TooltipCellHeroPreview _Tooltip_HeroPreview;
    [SerializeField] private TooltipCellHUDPreview _Tooltip_HUDPreview;
    [SerializeField] private TooltipCellCard _Tooltip_Card;

    #region Private Properties
    private static readonly string _namePrefix = "ITEM_NAME_";
    private static readonly string _infoPrefix = "ITEM_DESCRIPTION_";

    private PromotionItemDetailUICell _currentItem;
    private ShopItemData _data;
    private List<PromotionItemDetailUICell> _itemDetailUIList = new List<PromotionItemDetailUICell>();
    #endregion

    public void SetData(string key, PopupUIData popupData, ShopItemData shopItemData)
    {
        base.SetData(popupData);

        ClearItemInfoObjects();
        this._data = shopItemData;
        GenerateItemInfoObjects(shopItemData.CustomData.ItemDetail);
        SetDetail(shopItemData);
        SelectItem(_itemDetailUIList[0].Data.ItemID);
    }

    public void SetBTN(UnityAction onBuy, UnityAction onCancel)
    {
        BuyButton.onClick.RemoveAllListeners();
        BuyButton.onClick.AddListener(delegate
        {
            Hide();
            onBuy?.Invoke();
        });

        CancelButton.onClick.RemoveAllListeners();
        CancelButton.onClick.AddListener(delegate
        {
            Hide();
            onCancel?.Invoke();
        });
    }

    private void SelectItem(string id)
    {
        if (_currentItem?.Data.ItemID == id) return;

        _currentItem = _itemDetailUIList.Find(x => id == x.Data.ItemID);

        if (_currentItem != null)
        {
            foreach (PromotionItemDetailUICell item in _itemDetailUIList)
            {
                item.SetGlow(false);
            }

            _currentItem.SetGlow(true);
            UpdatePreviewItem(_currentItem.Data.ItemID);
        }
    }

    private void UpdatePreviewItem(string key)
    {
        HideTooltip_Full();
        ShowTooltip_Full(key);
    }

    private void SetDetail(ShopItemData data)
    {
        NameText.text = LocalizationManager.Instance.GetItemName(data.ItemId);
        BuyButtonText.text = ShopManager.GetPriceText(data);
    }

    private void ClearItemInfoObjects()
    {
        for (int i = 0; i < _itemDetailUIList.Count; i++)
        {
            Destroy(_itemDetailUIList[i].gameObject);
            _itemDetailUIList.RemoveAt(i);
            i--;
        }
    }

    private void GenerateItemInfoObjects(List<ItemData> itemDetail)
    {
        foreach (ItemData item in itemDetail)
        {
            PromotionItemDetailUICell itemUI = Instantiate(ItemPrefab, SpawnTransform).GetComponent<PromotionItemDetailUICell>();
            itemUI.SetData(item, () => SelectItem(item.ItemID));
            _itemDetailUIList.Add(itemUI);
        }
    }

    #region Show Preview
    public void ShowTooltip_Full(string key)
    {
        string baseKey = key.Split(':')[0];
        string titleText = LocalizationManager.Instance.GetText(_namePrefix + baseKey);
        string detailText = LocalizationManager.Instance.GetText(_infoPrefix + baseKey);

        if (key.StartsWith("HUD"))
        {
            _Tooltip_HUDPreview.Show(key, titleText, detailText);
        }
        else if (key.StartsWith("H"))
        {
            _Tooltip_HeroPreview.Show(key, titleText, detailText);
        }
        else if (GameHelper.IsCardKey(key))
        {
            _Tooltip_Card.Show(key, titleText, detailText);
        }
        else
        {
            _Tooltip_FullDetail.Show(key, titleText, detailText);
        }

    }

    public void HideTooltip_Full()
    {
        _Tooltip_FullDetail.Hide();
        _Tooltip_HeroPreview.Hide();
        _Tooltip_Card.Hide();
        _Tooltip_HUDPreview.Hide();
    }

    #endregion
}
