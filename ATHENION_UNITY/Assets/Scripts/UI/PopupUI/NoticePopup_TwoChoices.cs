﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class NoticePopup_TwoChoices : NoticePopup_SureCheck
{
    [Header("BTN TEXT")]
    [SerializeField] private LocalizeTextUI _TEXT_Okay;
    [SerializeField] private LocalizeTextUI _TEXT_Cancel;

    public void SetBTNText(string okayText, string cancelText)
    {
        _TEXT_Okay.LocalizeKey = okayText;
        _TEXT_Cancel.LocalizeKey = cancelText;
    }
}
