﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;

public class NoticePopup_TermService : PopupUI
{
    [SerializeField] private Toggle _TOGGLE_Agree;

    [SerializeField] private Button _BTN_Term;
    [SerializeField] private Button _BTN_Privacy;
    [SerializeField] private Button _BTN_Okay;

    public override void Show(UnityAction onShowComplete)
    {
        _BTN_Term.onClick.RemoveAllListeners();
        _BTN_Term.onClick.AddListener(
            delegate()
            {
                Application.OpenURL(GameHelper.TermConditionLink);
            }
        );

        _BTN_Privacy.onClick.RemoveAllListeners();
        _BTN_Privacy.onClick.AddListener(
            delegate ()
            {
                Application.OpenURL(GameHelper.PrivacyLink);
            }
        );

        _TOGGLE_Agree.onValueChanged.RemoveAllListeners();
        _TOGGLE_Agree.onValueChanged.AddListener(OnToggleAgree);

        _TOGGLE_Agree.isOn = false;
        OnToggleAgree(false);

        base.Show(onShowComplete);
    }

    public void SetBTN(UnityAction onClickOkay)
    {
        _BTN_Okay.onClick.RemoveAllListeners();
        _BTN_Okay.onClick.AddListener(delegate
        {
            Hide();
            onClickOkay?.Invoke();
        });
    }

    private void OnToggleAgree(bool isAgree)
    {
        _BTN_Okay.interactable = isAgree;
    }
}

