﻿using Karamucho;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class NoticePopup_WildOfferPopup : PopupUI
{
    public Button BuyButton;
    public Button CancelButton;
    public TextMeshProUGUI TimeText;

    public GameObject Prefab;
    public Transform SpawnTransform;

    public Transform LongImagePos;
    public Transform ShortImagePos;

    public TextMeshProUGUI BuyButtonText;

    public Image ItemImage;
    public TextMeshProUGUI FullDetailText;
    [Obsolete]
    public TextMeshProUGUI NameText;

    private List<PromotionItemDetailUICell> _itemDetailUIList = new List<PromotionItemDetailUICell>();

    private ShopItemData _data;
    private DateTime _expireDate;
    private Coroutine _timer;

    private void OnEnable()
    {
        if (_timer != null)
            StopCoroutine(_timer);
        _timer = StartCoroutine(CountDown(_expireDate));
    }

    private void OnDisable()
    {
        if (_timer != null)
            StopCoroutine(_timer);
        _timer = null;
    }

    private IEnumerator CountDown(DateTime targetDate)
    {
        while (true)
        {
            TimeSpan timeLeft = targetDate - DateTimeData.GetDateTimeUTC();
            if (timeLeft.TotalSeconds > 0)
            {
                TimeText.text = timeLeft.ToTimeFormat();
            }
            else
            {
                TimeText.text = "00:00:00";
                yield break;
            }
            yield return new WaitForSeconds(1f);
        }
    }

    public void SetData(string key, PopupUIData popupData, ShopItemData shopItemData)
    {
        TimeText.text = "";
        base.SetData(popupData);

        ClearItemInfoObjects();
        this._data = shopItemData;
        GenerateItemInfoObjects(shopItemData.CustomData.ItemDetail);
        SetDetail(shopItemData);
        UpdateImage();

        Dictionary<string, WildOfferData> playerWildoffer = WildOfferManager.Instance.PlayerWildOfferData.FindValidWildOffer();

        if (playerWildoffer.ContainsKey(key))
        {
            if (shopItemData.ItemId == playerWildoffer[key].ItemID)
            {
                _expireDate = playerWildoffer[key].ExpireDate;
            }
        }
    }
    public void SetBTN(UnityAction onBuy, UnityAction onCancel)
    {
        BuyButton.onClick.RemoveAllListeners();
        BuyButton.onClick.AddListener(delegate
        {
            Hide();
            onBuy?.Invoke();
        });

        CancelButton.onClick.RemoveAllListeners();
        CancelButton.onClick.AddListener(delegate
        {
            Hide();
            onCancel?.Invoke();
        });
    }

    private void UpdateImage()
    {
        string itemKey = GameHelper.ConvertItemIDToItemKey(_data.ItemId);

        Sprite sprite = _data.ImagePath == string.Empty ? ShopManager.LoadImageByItemKey(_data.ShopType, itemKey) : ResourceManager.Load<Sprite>(ShopManager.ImagePath + _data.ImagePath);
        ItemImage.sprite = sprite;

        if (sprite != null)
        {
            float ratio = sprite.rect.width / sprite.rect.height;
            ItemImage.transform.position = ratio > 2f ? LongImagePos.position : ShortImagePos.position;
        }
    }

    private void SetDetail(ShopItemData data)
    {
        NameText.text = LocalizationManager.Instance.GetItemName(data.ItemId);
        FullDetailText.text = LocalizationManager.Instance.GetItemDescription(data.ItemId);
        BuyButtonText.text = ShopManager.GetPriceText(data);
    }

    private void ClearItemInfoObjects()
    {
        for (int i = 0; i < _itemDetailUIList.Count; i++)
        {
            Destroy(_itemDetailUIList[i].gameObject);
            _itemDetailUIList.RemoveAt(i);
            i--;
        }
    }

    private void GenerateItemInfoObjects(List<ItemData> itemDetail)
    {
        foreach (ItemData item in itemDetail)
        {
            PromotionItemDetailUICell itemUI = Instantiate(Prefab, SpawnTransform).GetComponent<PromotionItemDetailUICell>();
            itemUI.SetData(item);
            _itemDetailUIList.Add(itemUI);
        }
    }
}
