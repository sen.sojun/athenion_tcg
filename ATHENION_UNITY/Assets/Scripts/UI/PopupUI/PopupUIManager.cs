﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System;

public class PopupUIManager : MonoSingleton<PopupUIManager>
{
    #region Inspector Properties
    [Header("Prefab")]
    [SerializeField] private GameObject _Prefab_PopupConfirm;
    [SerializeField] private GameObject _Prefab_PopupConfirmCorrect;
    [SerializeField] private GameObject _Prefab_PopupConfirmWarning;
    [SerializeField] private GameObject _Prefab_SureCheck;
    [SerializeField] private GameObject _Prefab_SureCheck_Mary;
    [SerializeField] private GameObject _Prefab_TwoChoices;
    [SerializeField] private GameObject _Prefab_TwoChoices_ServerClose;
    [SerializeField] private GameObject _Prefab_TwoChoices_TapBlankAreaCancel;
    [SerializeField] private GameObject _Prefab_ErrorNotice;
    [SerializeField] private GameObject _Prefab_InviteMatch;
    [SerializeField] private GameObject _Prefab_NoticeOnly;
    [SerializeField] private GameObject _Prefab_GotReward;
    [SerializeField] private GameObject _Prefab_GotLegendPack;
    [SerializeField] private GameObject _Prefab_PreviewReward;
    [SerializeField] private GameObject _Prefab_GotRewardAnimated;
    [SerializeField] private GameObject _Prefab_GotRewardAuto;
    [SerializeField] private GameObject _Prefab_RankUpdate;
    [SerializeField] private GameObject _Prefab_LevelUpGotRewardAnimated;
    [SerializeField] private GameObject _Prefab_SeasonGotRewardAnimated;
    [SerializeField] private GameObject _Prefab_BannerPopup;
    [SerializeField] private GameObject _Prefab_FacebookBannerPopup;
    [SerializeField] private GameObject _Prefab_FacebookBannerPopup_FirstTime;
    [SerializeField] private GameObject _Prefab_TermService;
    [SerializeField] private GameObject _Prefab_PackDetail_Short;
    [SerializeField] private GameObject _Prefab_NotEnoughCurrency;
    [SerializeField] private NoticePopup_WildOfferPopup _Prefab_WildOffer;
    [SerializeField] private NoticePopup_ItemBundlePreview _Prefab_BundlePreview;
    [SerializeField] private NoticePopup_SeasonPassPopup _Prefab_SeasonPass;
    [SerializeField] private GameObject _Prefab_PopupDetailWindow;
    [SerializeField] private GameObject _Prefab_TapToCloseWithItemDetail;
    [SerializeField] private GameObject _Prefab_MultipleLineMessage;

    [Header("Section")]
    [SerializeField] private PopupUISection _Section_Top;
    [SerializeField] private PopupUISection _Section_Mid;
    [SerializeField] private PopupUISection _Section_Bot;
    [SerializeField] private PopupUISection _Section_Error;

    [Header("Loading UI")]
    [SerializeField] private LoadingUI _Loading_Mid;
    [SerializeField] private LoadingUI _Loading_SmallCorner;
    [SerializeField] private LoadingUI _Loading_Full;
    [SerializeField] private LoadingUIMidProgress _Loading_Mid_Progress;

    [Header("CustomBtnColor")]
    [SerializeField] private Sprite _GoldSpriteBtn;
    [SerializeField] private Sprite _GreySpriteBtn;
    [SerializeField] private Sprite _GreenSpriteBtn;

    #endregion

    #region Public Properties
    #endregion

    #region Private Properties
    #endregion

    #region Popup General
    public void ShowPopup_Error(string title, string message, UnityAction onClickOkay = null, bool isShowRepeat = false, bool isDontClose = false)
    {
        PopupUIData data = new PopupUIData(title, message, PopupUITypes.Error);
        if (!IsCanShowPopupUI(data.ID, _Section_Error, isShowRepeat))
        {
            return;
        }

        GameObject obj = Instantiate(_Prefab_ErrorNotice);
        NoticePopup_Confirm cell = obj.GetComponent<NoticePopup_Confirm>();
        cell.SetData(data);
        cell.SetBTN(onClickOkay);
        cell.SetDontClose(isDontClose);
        AddPopupUI(obj, _Section_Error);
    }

    public void ShowPopup_MultipleLine(string title, List<string> messageList, UnityAction onClickOkay = null, bool isShowRepeat = false, bool isDontClose = false)
    {
        PopupUIData data = new PopupUIData(title, messageList[0], PopupUITypes.Error);
        if (!IsCanShowPopupUI(data.ID, _Section_Error, isShowRepeat))
        {
            return;
        }

        GameObject obj = Instantiate(_Prefab_MultipleLineMessage);
        NoticePopup_MultipleLine cell = obj.GetComponent<NoticePopup_MultipleLine>();
        cell.SetData(data);
        cell.SetBTN(onClickOkay);
        cell.SetMessage(messageList);
        cell.SetDontClose(isDontClose);
        AddPopupUI(obj, _Section_Mid);
    }

    public void ShowPopup_ConfirmOnly(string title, string message, UnityAction onClickOkay = null, bool isShowRepeat = false, bool isDontClose = false)
    {
        PopupUIData data = new PopupUIData(title, message, PopupUITypes.Confirm);
        if (!IsCanShowPopupUI(data.ID, _Section_Mid, isShowRepeat))
        {
            return;
        }

        GameObject obj = Instantiate(_Prefab_PopupConfirm);
        NoticePopup_Confirm cell = obj.GetComponent<NoticePopup_Confirm>();
        cell.SetData(data);
        cell.SetBTN(onClickOkay);
        cell.SetDontClose(isDontClose);
        AddPopupUI(obj, _Section_Mid);
    }

    public void ShowPopup_DetailWindow(string title, string message, UnityAction onClickOkay = null, bool isShowRepeat = false, bool isDontClose = false)
    {
        PopupUIData data = new PopupUIData(title, message, PopupUITypes.Confirm);
        if (!IsCanShowPopupUI(data.ID, _Section_Mid, isShowRepeat))
        {
            return;
        }

        GameObject obj = Instantiate(_Prefab_PopupDetailWindow);
        NoticePopup_Confirm cell = obj.GetComponent<NoticePopup_Confirm>();
        cell.SetData(data);
        cell.SetBTN(onClickOkay);
        cell.SetDontClose(isDontClose);
        AddPopupUI(obj, _Section_Mid);
    }

    public void ShowPopup_ConfirmOnlyWithCorrect(string title, string message, UnityAction onClickOkay = null, bool isShowRepeat = false, bool isDontClose = false)
    {
        PopupUIData data = new PopupUIData(title, message, PopupUITypes.Confirm);
        if (!IsCanShowPopupUI(data.ID, _Section_Mid, isShowRepeat))
        {
            return;
        }

        GameObject obj = Instantiate(_Prefab_PopupConfirmCorrect);
        NoticePopup_Confirm cell = obj.GetComponent<NoticePopup_Confirm>();
        cell.SetData(new PopupUIData(title, message, PopupUITypes.Confirm));
        cell.SetBTN(onClickOkay);
        cell.SetDontClose(isDontClose);
        AddPopupUI(obj, _Section_Mid);
    }

    public void ShowPopup_ConfirmOnlyWithWarning(string title, string message, UnityAction onClickOkay = null, bool isShowRepeat = false, bool isDontClose = false)
    {
        PopupUIData data = new PopupUIData(title, message, PopupUITypes.Confirm);
        if (!IsCanShowPopupUI(data.ID, _Section_Mid, isShowRepeat))
        {
            return;
        }

        GameObject obj = Instantiate(_Prefab_PopupConfirmWarning);
        NoticePopup_Confirm cell = obj.GetComponent<NoticePopup_Confirm>();
        cell.SetData(new PopupUIData(title, message, PopupUITypes.Confirm));
        cell.SetBTN(onClickOkay);
        cell.SetDontClose(isDontClose);
        AddPopupUI(obj, _Section_Mid);
    }

    public void ShowPopup_ConfirmOnlyWithItemDetail(string title, string message, string itemID, UnityAction onClickOkay = null, bool isShowRepeat = false, bool isDontClose = false)
    {
        PopupUIData data = new PopupUIData(title, message, PopupUITypes.Confirm);
        if (!IsCanShowPopupUI(data.ID, _Section_Mid, isShowRepeat))
        {
            return;
        }

        GameObject obj = Instantiate(_Prefab_TapToCloseWithItemDetail);
        NoticePopup_TapWithItemDetail cell = obj.GetComponent<NoticePopup_TapWithItemDetail>();
        cell.SetData(data);
        cell.SetItem(itemID);
        cell.SetBTN(onClickOkay);
        cell.SetDontClose(isDontClose);
        AddPopupUI(obj, _Section_Mid);
    }

    public void ShowPopup_NoticeOnly(string head, string title, string message, UnityAction onClickOkay = null, bool isShowRepeat = false, bool isDontClose = false)
    {
        PopupUIData data = new PopupUIData(title, message, PopupUITypes.Confirm);
        if (!IsCanShowPopupUI(data.ID, _Section_Top, isShowRepeat))
        {
            return;
        }

        GameObject obj = Instantiate(_Prefab_NoticeOnly);
        NoticePopup_TopNotice cell = obj.GetComponent<NoticePopup_TopNotice>();
        cell.SetData(data);
        cell.SetText(head, title, message);
        cell.SetBTN(onClickOkay);
        cell.SetDontClose(isDontClose);
        AddPopupUI(obj, _Section_Top);
    }

    public void ShowPopup_SureCheck(string title, string message, UnityAction onClickOkay, UnityAction onClickCancel, bool isShowRepeat = false, bool isDontClose = false)
    {
        PopupUIData data = new PopupUIData(title, message, PopupUITypes.SureCheck);
        if (!IsCanShowPopupUI(data.ID, _Section_Mid, isShowRepeat))
        {
            return;
        }

        GameObject obj = Instantiate(_Prefab_SureCheck);
        NoticePopup_SureCheck cell = obj.GetComponent<NoticePopup_SureCheck>();
        cell.SetData(data);
        cell.SetBTN(onClickOkay, onClickCancel);
        cell.SetDontClose(isDontClose);
        AddPopupUI(obj, _Section_Mid);
    }

    public void ShowPopup_SureCheck_Mary(string title, string message, UnityAction onClickOkay, UnityAction onClickCancel, bool isShowRepeat = false, bool isDontClose = false)
    {
        PopupUIData data = new PopupUIData(title, message, PopupUITypes.SureCheck);
        if (!IsCanShowPopupUI(data.ID, _Section_Mid, isShowRepeat))
        {
            return;
        }

        GameObject obj = Instantiate(_Prefab_SureCheck_Mary);
        NoticePopup_SureCheck cell = obj.GetComponent<NoticePopup_SureCheck>();
        cell.SetData(data);
        cell.SetBTN(onClickOkay, onClickCancel);
        cell.SetDontClose(isDontClose);
        AddPopupUI(obj, _Section_Mid);
    }

    public void ShowPopup_BannerPopup(string title, string message, string path, UnityAction onClickOkay, UnityAction onClickCancel, bool isShowRepeat = false, bool isDontClose = false)
    {
        PopupUIData data = new PopupUIData(title, message, PopupUITypes.SureCheck);
        if (!IsCanShowPopupUI(data.ID, _Section_Mid, isShowRepeat))
        {
            return;
        }

        GameObject obj = Instantiate(_Prefab_BannerPopup);
        NoticePopup_BannerPopup cell = obj.GetComponent<NoticePopup_BannerPopup>();
        cell.SetData(data);
        cell.SetBTN(onClickOkay, onClickCancel);
        cell.SetImage(path);
        cell.SetDontClose(isDontClose);
        AddPopupUI(obj, _Section_Mid);
    }

    public void ShowPopup_PurchaseWildOffer(string key, ShopItemData shopItemData, UnityAction onClickBuy, UnityAction onClickCancel)
    {
        PopupUIData data = new PopupUIData("WildOffer", "", PopupUITypes.SureCheck);
        if (!IsCanShowPopupUI(data.ID, _Section_Mid, false))
        {
            return;
        }
        NoticePopup_WildOfferPopup obj = Instantiate(_Prefab_WildOffer.gameObject, _Section_Mid.transform).GetComponent<NoticePopup_WildOfferPopup>();
        obj.SetData(key, data, shopItemData);
        obj.SetBTN(onClickBuy, onClickCancel);
        obj.SetDontClose(false);
        AddPopupUI(obj.gameObject, _Section_Mid);
    }

    public void ShowPopup_PreviewShopBundle(string key, ShopItemData shopItemData, UnityAction onClickBuy, UnityAction onClickCancel)
    {
        PopupUIData data = new PopupUIData("ShowBundlePreview", "", PopupUITypes.SureCheck);
        if (!IsCanShowPopupUI(data.ID, _Section_Mid, false))
        {
            return;
        }
        NoticePopup_ItemBundlePreview obj = Instantiate(_Prefab_BundlePreview.gameObject, _Section_Mid.transform).GetComponent<NoticePopup_ItemBundlePreview>();
        obj.SetData(key, data, shopItemData);
        obj.SetBTN(onClickBuy, onClickCancel);
        obj.SetDontClose(false);
        AddPopupUI(obj.gameObject, _Section_Mid);
    }

    public void ShowPopup_PurchaseSeasonPass(UnityAction onClickBuy, UnityAction onClickCancel = null)
    {
        PopupUIData data = new PopupUIData("PurchaseSeasonPass", "", PopupUITypes.SureCheck);
        if (!IsCanShowPopupUI(data.ID, _Section_Mid, false))
        {
            return;
        }
        NoticePopup_SeasonPassPopup obj = Instantiate(_Prefab_SeasonPass.gameObject, _Section_Mid.transform).GetComponent<NoticePopup_SeasonPassPopup>();
        obj.Initialize();
        obj.SetEvent(onClickBuy, onClickCancel);
        obj.SetDontClose(false);
        AddPopupUI(obj.gameObject, _Section_Mid);
    }

    public void ShowPopup_TwoChoices(string title, string message, string onOkayText, string onCancelText, UnityAction onClickOkay, UnityAction onClickCancel, bool isShowRepeat = false, bool isDontClose = false)
    {
        PopupUIData data = new PopupUIData(title, message, PopupUITypes.SureCheck);
        if (!IsCanShowPopupUI(data.ID, _Section_Mid, isShowRepeat))
        {
            return;
        }

        GameObject obj = Instantiate(_Prefab_TwoChoices);
        NoticePopup_TwoChoices cell = obj.GetComponent<NoticePopup_TwoChoices>();
        cell.SetData(data);
        cell.SetBTN(onClickOkay, onClickCancel);
        cell.SetBTNText(onOkayText, onCancelText);
        cell.SetDontClose(isDontClose);
        AddPopupUI(obj, _Section_Mid);
    }

    public void ShowPopup_TwoChoices_ServerClose(string title, string message, string onOkayText, string onCancelText, UnityAction onClickOkay, UnityAction onClickCancel, bool isShowRepeat = false, bool isDontClose = false)
    {
        PopupUIData data = new PopupUIData(title, message, PopupUITypes.SureCheck);
        if (!IsCanShowPopupUI(data.ID, _Section_Mid, isShowRepeat))
        {
            return;
        }

        GameObject obj = Instantiate(_Prefab_TwoChoices_ServerClose);
        NoticePopup_TwoChoices cell = obj.GetComponent<NoticePopup_TwoChoices>();
        cell.SetData(data);
        cell.SetBTN(onClickOkay, onClickCancel);
        cell.SetBTNText(onOkayText, onCancelText);
        cell.SetDontClose(isDontClose);
        AddPopupUI(obj, _Section_Mid);
    }

    public void ShowPopup_TwoChoices_Tap_BlankArea_Cancel(string title, string message, string onOkayText, string onCancelText, UnityAction onClickOkay, UnityAction onClickCancel, bool isShowRepeat = false, bool isDontClose = false)
    {
        PopupUIData data = new PopupUIData(title, message, PopupUITypes.SureCheck);
        if (!IsCanShowPopupUI(data.ID, _Section_Mid, isShowRepeat))
        {
            return;
        }

        GameObject obj = Instantiate(_Prefab_TwoChoices_TapBlankAreaCancel);
        NoticePopup_TwoChoices cell = obj.GetComponent<NoticePopup_TwoChoices>();
        cell.SetData(data);
        cell.SetBTN(onClickOkay, onClickCancel);
        cell.SetBTNText(onOkayText, onCancelText);
        cell.SetDontClose(isDontClose);
        AddPopupUI(obj, _Section_Mid);
    }

    public NoticePopup_GotRewards ShowPopup_GotReward(string title, string message, List<ItemData> itemDataList, UnityAction onClickOkay = null, CustomButton customButton = null, bool isShowRepeat = false)
    {
        PopupUIData data = new PopupUIData(title, message, PopupUITypes.Confirm);
        if (!IsCanShowPopupUI(data.ID, _Section_Mid, isShowRepeat))
        {
            return null;
        }

        GameObject obj = Instantiate(_Prefab_GotReward);
        NoticePopup_GotRewards cell = obj.GetComponent<NoticePopup_GotRewards>();
        cell.SetData(data);
        cell.SetReward(itemDataList);
        cell.SetBTN(onClickOkay, customButton);
        AddPopupUI(obj, _Section_Mid);

        SoundManager.PlaySFX(SoundManager.SFX.GotReward_ItemPop);

        return cell;
    }

    public void ShowPopup_GotLegendPack(string title, string message, UnityAction onClickOkay = null, bool isShowRepeat = false, bool isDontClose = false)
    {
        PopupUIData data = new PopupUIData(title, message, PopupUITypes.Confirm);
        if (!IsCanShowPopupUI(data.ID, _Section_Mid, isShowRepeat))
        {
            return;
        }

        GameObject obj = Instantiate(_Prefab_GotLegendPack);
        NoticePopup_Confirm cell = obj.GetComponent<NoticePopup_Confirm>();
        cell.SetData(data);
        cell.SetBTN(onClickOkay);
        cell.SetDontClose(isDontClose);
        AddPopupUI(obj, _Section_Mid);
    }

    public NoticePopup_GotRewards ShowPopup_PreviewReward(string title, string message, List<ItemData> itemDataList, UnityAction onClickOkay = null, CustomButton customButton = null, bool isShowRepeat = false)
    {
        PopupUIData data = new PopupUIData(title, message, PopupUITypes.Confirm);
        if (!IsCanShowPopupUI(data.ID, _Section_Mid, isShowRepeat))
        {
            return null;
        }

        GameObject obj = Instantiate(_Prefab_PreviewReward);
        NoticePopup_GotRewards cell = obj.GetComponent<NoticePopup_GotRewards>();
        cell.SetData(data);
        cell.SetReward(itemDataList);
        cell.SetBTN(onClickOkay, customButton);
        AddPopupUI(obj, _Section_Mid);

        return cell;
    }

    public NoticePopup_GotRewardsAnimated ShowPopup_GotRewardAnimated(string title, string message, List<ItemData> itemDataList, UnityAction onClickOkay = null, bool isShowRepeat = false)
    {
        PopupUIData data = new PopupUIData(title, message, PopupUITypes.Confirm);
        if (!IsCanShowPopupUI(data.ID, _Section_Mid, isShowRepeat))
        {
            return null;
        }

        GameObject obj = Instantiate(_Prefab_GotRewardAnimated);
        NoticePopup_GotRewardsAnimated cell = obj.GetComponent<NoticePopup_GotRewardsAnimated>();
        cell.SetData(data);
        cell.SetReward(itemDataList);
        cell.SetBTN(onClickOkay);
        AddPopupUI(obj, _Section_Mid);

        return cell;
    }

    public NoticePopup_GotRewardsAuto ShowPopup_GotRewardAuto(List<ItemData> itemDataList, UnityAction onComplete, bool isShowRepeat = false)
    {
        PopupUIData data = new PopupUIData("", "", PopupUITypes.Confirm);
        if (!IsCanShowPopupUI(data.ID, _Section_Mid, isShowRepeat))
        {
            return null;
        }

        GameObject obj = Instantiate(_Prefab_GotRewardAuto);
        NoticePopup_GotRewardsAuto cell = obj.GetComponent<NoticePopup_GotRewardsAuto>();
        cell.SetData(data);
        cell.SetReward(itemDataList);
        cell.StartAnim(onComplete);
        AddPopupUI(obj, _Section_Mid);

        return cell;
    }

    public void ShowPopup_PackDetail_Short(string title, string message, List<ItemData> itemDataList, UnityAction onClickOkay = null, CustomButton customButton = null, bool isShowRepeat = false)
    {
        PopupUIData data = new PopupUIData(title, message, PopupUITypes.Confirm);
        if (!IsCanShowPopupUI(data.ID, _Section_Mid, isShowRepeat))
        {
            return;
        }

        GameObject obj = Instantiate(_Prefab_PackDetail_Short);
        NoticePopup_PackDetail_Short cell = obj.GetComponent<NoticePopup_PackDetail_Short>();
        cell.SetData(data);
        cell.SetReward(itemDataList);
        cell.SetBTN(onClickOkay, customButton);
        AddPopupUI(obj, _Section_Mid);

        //SoundManager.PlaySFX(SoundManager.SFX.Coin_Flick);
    }
    #endregion

    #region Popup Specific UI
    public void ShowNotEnoughCurrency(string itemID, string title, string message, UnityAction onClose, CustomButton customButton = null, bool isShowRepeat = false)
    {
        PopupUIData data = new PopupUIData(title, message, PopupUITypes.Confirm);
        if (!IsCanShowPopupUI(data.ID, _Section_Mid, isShowRepeat))
        {
            return;
        }

        GameObject obj = Instantiate(_Prefab_NotEnoughCurrency);
        NoticePopup_NotEnoughCurrency cell = obj.GetComponent<NoticePopup_NotEnoughCurrency>();
        cell.SetData(itemID, title, message);
        cell.SetEvent(onClose, customButton);
        AddPopupUI(obj, _Section_Mid);
    }

    public void ShowRankUpdate(string title, string message, int startRankIndex, int endRankIndex, UnityAction onClickOkay = null, bool isShowRepeat = false)
    {
        // Fail Safe when rank not change !
        if (startRankIndex == endRankIndex) return;
        if (GameManager.Instance.IsStoryMode()) return;

        PopupUIData data = new PopupUIData(title, message, PopupUITypes.InviteMatch);
        if (!IsCanShowPopupUI(data.ID, _Section_Mid, isShowRepeat))
        {
            return;
        }

        GameObject obj = Instantiate(_Prefab_RankUpdate);
        NoticePopup_RankUpdate cell = obj.GetComponent<NoticePopup_RankUpdate>();
        string avatar = DataManager.Instance.PlayerInfo.AvatarID;
        cell.SetData(data);
        cell.SetRank(startRankIndex, endRankIndex);
        cell.SetBTN(onClickOkay);
        cell.SetAvatar(avatar);
        AddPopupUI(obj, _Section_Mid);

    }

    public void ShowInviteMatch(string title, string message, PlayerInfoUIData playerData, UnityAction onYes, UnityAction onNo, bool isShowRepeat = false)
    {
        PopupUIData data = new PopupUIData(title, message, PopupUITypes.InviteMatch);
        if (!IsCanShowPopupUI(data.ID, _Section_Mid, isShowRepeat))
        {
            return;
        }

        GameObject obj = Instantiate(_Prefab_InviteMatch);
        NoticePopup_InviteMatch cell = obj.GetComponent<NoticePopup_InviteMatch>();
        cell.SetData(data);
        cell.SetText(title, message);
        cell.SetFriendInfo(playerData);
        cell.SetBTN(onYes, onNo);
        AddPopupUI(obj, _Section_Mid);
    }

    public void ShowLevelUpReward(string title, string message, List<ItemData> itemDataList, UnityAction onClickOkay = null, bool isShowRepeat = false)
    {
        PopupUIData data = new PopupUIData(title, message, PopupUITypes.Confirm);
        if (!IsCanShowPopupUI(data.ID, _Section_Mid, isShowRepeat))
        {
            return;
        }

        GameObject obj = Instantiate(_Prefab_LevelUpGotRewardAnimated);
        NoticePopup_GotRewardsAnimated cell = obj.GetComponent<NoticePopup_GotRewardsAnimated>();
        cell.SetData(data);
        cell.SetReward(itemDataList);
        cell.SetBTN(onClickOkay);
        AddPopupUI(obj, _Section_Mid);
    }

    public void ShowSeasonReward(string title, string message, List<ItemData> itemDataList, UnityAction onClickOkay = null, bool isShowRepeat = false)
    {
        PopupUIData data = new PopupUIData(title, message, PopupUITypes.Confirm);
        if (!IsCanShowPopupUI(data.ID, _Section_Mid, isShowRepeat))
        {
            return;
        }

        GameObject obj = Instantiate(_Prefab_SeasonGotRewardAnimated);
        NoticePopup_GotRewardsAnimated cell = obj.GetComponent<NoticePopup_GotRewardsAnimated>();
        cell.SetData(data);
        cell.SetReward(itemDataList);
        cell.SetBTN(onClickOkay);
        AddPopupUI(obj, _Section_Mid);
    }

    public void ShowTermService(UnityAction onClickOkay = null, bool isShowRepeat = false)
    {
        PopupUIData data = new PopupUIData();
        if (!IsCanShowPopupUI(data.ID, _Section_Mid, isShowRepeat))
        {
            return;
        }

        GameObject obj = Instantiate(_Prefab_TermService);
        NoticePopup_TermService cell = obj.GetComponent<NoticePopup_TermService>();
        cell.SetData(data);
        cell.SetBTN(onClickOkay);
        AddPopupUI(obj, _Section_Mid);
    }

    public void ShowFacebookBannerPopup(string title, string message, string path, bool isShowRepeat = false, bool isDontClose = false)
    {
        PopupUIData data = new PopupUIData(title, message, PopupUITypes.SureCheck);
        if (!IsCanShowPopupUI(data.ID, _Section_Mid, isShowRepeat))
        {
            return;
        }

        GameObject obj = Instantiate(_Prefab_FacebookBannerPopup);
        NoticePopup_FacebookBannerPopup cell = obj.GetComponent<NoticePopup_FacebookBannerPopup>();
        cell.SetData(data);
        cell.SetImage(path);
        cell.SetBTN();
        cell.SetDontClose(isDontClose);
        AddPopupUI(obj, _Section_Mid);
    }

    public void ShowFacebookBannerPopup_FirstTime(string title, string message, string path, bool isShowRepeat = false, bool isDontClose = false)
    {
        PopupUIData data = new PopupUIData(title, message, PopupUITypes.SureCheck);
        if (!IsCanShowPopupUI(data.ID, _Section_Mid, isShowRepeat))
        {
            return;
        }

        GameObject obj = Instantiate(_Prefab_FacebookBannerPopup_FirstTime);
        NoticePopup_FacebookBannerPopup cell = obj.GetComponent<NoticePopup_FacebookBannerPopup>();
        cell.SetData(data);
        cell.SetImage(path);
        cell.SetBTN();
        cell.SetDontClose(isDontClose);
        AddPopupUI(obj, _Section_Mid);
    }

    #endregion

    #region Soft Loading UI
    public void Show_SoftLoad(bool isShow)
    {
        if (isShow)
        {
            _Loading_SmallCorner.ShowUI();
        }
        else
        {
            _Loading_SmallCorner.HideUI();
        }
    }

    public void Show_MidSoftLoad(bool isShow)
    {
        if (isShow)
        {
            _Loading_Mid.ShowUI();
        }
        else
        {
            _Loading_Mid.HideUI();
        }
    }

    public void Show_MidSoftLoad_Progress(bool isShow)
    {
        if (isShow)
        {
            _Loading_Mid_Progress.ShowUI();
        }
        else
        {
            _Loading_Mid_Progress.HideUI();
        }
    }

    public void SetMidSoftLoadProgress(int current, int max)
    {
        _Loading_Mid_Progress.SetProgress(current, max);
    }

    public void Show_FullLoad(bool isShow, string loadingText = "")
    {
        if (isShow)
        {
            _Loading_Full.ShowUI();
            SetLoadingText(loadingText);
        }
        else
        {
            _Loading_Full.HideUI();
        }
    }

    public void SetLoadingText(string loadingText)
    {
        if (loadingText != null && loadingText.Length > 0)
        {
            _Loading_Full.SetLoadingText(loadingText);
        }
        else
        {
            _Loading_Full.SetDefaultLoadingText();
        }
    }

    public void SetLoadingProgress(float ratio)
    {
        _Loading_Full.SetProgress(ratio);
    }

    public UnityAction<float> GetCallbackLoadingProgress()
    {
        return _Loading_Full.SetProgress;
    }

    public void HideAllLoad()
    {
        _Loading_Mid.HideUI();
        _Loading_SmallCorner.HideUI();
        _Loading_Full.HideUI();
    }

    public void HideAllSoftLoad()
    {
        _Loading_Mid.HideUI();
        _Loading_SmallCorner.HideUI();
    }
    #endregion

    #region Section
    public void ClearPopup_SectionMid()
    {
        _Section_Mid.ClearPopup();
    }

    public void ClearPopup_SectionTop()
    {
        _Section_Top.ClearPopup();
    }

    public void ClearPopup_SectionBot()
    {
        _Section_Bot.ClearPopup();
    }

    public void ClearPopup_SectionError()
    {
        _Section_Error.ClearPopup();
    }
    #endregion

    #region Method
    /// <summary>
    /// 
    /// </summary>
    /// <param name="obj">Must setData from outside first.</param>
    /// <param name="target"></param>
    public void AddPopupUI(GameObject obj, PopupUISection target)
    {
        PopupUI cell = obj.GetComponent<PopupUI>();
        target.AddQueue(obj);
    }

    private bool IsCanShowPopupUI(string popupID, PopupUISection target, bool isShowRepeat)
    {
        if (target == null)
            return false;

        if (!isShowRepeat)
        {
            if (target.IsHasPopupUIInList(popupID))
            {
                return false;
            }
        }

        return true;
    }


    public Sprite GetButtonSpriteColor(CustomButton.ButtonColor color)
    {
        switch (color)
        {
            case CustomButton.ButtonColor.Grey:
                return _GreySpriteBtn;
            case CustomButton.ButtonColor.Gold:
                return _GoldSpriteBtn;
            case CustomButton.ButtonColor.Green:
                return _GreenSpriteBtn;
        }
        return null;
    }
    #endregion
}

public class CustomButton
{
    public enum ButtonColor { Grey, Gold, Green }
    public ButtonColor Color;
    public string Text;
    public UnityAction Action;
}
