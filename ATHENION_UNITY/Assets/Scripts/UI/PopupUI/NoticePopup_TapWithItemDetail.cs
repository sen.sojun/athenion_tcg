﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;

public class NoticePopup_TapWithItemDetail : NoticePopup_Confirm
{
    [SerializeField] private Image IMG_Icon;

    public void SetItem(string id)
    {
        IMG_Icon.sprite = SpriteResourceHelper.LoadSprite(id, SpriteResourceHelper.SpriteType.ICON_Item);
    }

}
