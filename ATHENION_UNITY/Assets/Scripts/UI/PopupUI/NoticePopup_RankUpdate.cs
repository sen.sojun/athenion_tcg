﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;

public class NoticePopup_RankUpdate : PopupUI
{
    [SerializeField] private TextMeshProUGUI _TEXT_Title;
    [SerializeField] private TextMeshProUGUI _TEXT_Message;

    [Header("Rank Info")]
    [SerializeField] private TextMeshProUGUI _TEXT_Head;
    [SerializeField] private TextMeshProUGUI _TEXT_Rank;
    [SerializeField] private Image IMG_StartRank;
    [SerializeField] private Image IMG_EndRank;
    [SerializeField] private Image IMG_AvatarOld;
    [SerializeField] private Image IMG_AvatarNew;
    [SerializeField] private Button _BTN_Okay;

    private bool _isRankUp = false;

    public override void Show(UnityAction onShowComplete)
    {
        _TEXT_Title.text = Data.Title;
        _TEXT_Message.text = Data.Message;

        base.Show(onShowComplete);
        ShowAnimation();
    }

    public void SetBTN(UnityAction onClickOkay)
    {
        _BTN_Okay.onClick.RemoveAllListeners();
        _BTN_Okay.onClick.AddListener(delegate
        {
            Hide();
            onClickOkay?.Invoke();
        });
    }

    public void SetRank(int startRankIndex, int endRankIndex)
    {
        string head = "";
        IMG_StartRank.sprite = SpriteResourceHelper.LoadSprite(GameHelper.GetRankID(startRankIndex), SpriteResourceHelper.SpriteType.ICON_Rank);
        IMG_EndRank.sprite = SpriteResourceHelper.LoadSprite(GameHelper.GetRankID(endRankIndex), SpriteResourceHelper.SpriteType.ICON_Rank);

        if (endRankIndex > startRankIndex)
        {
            head = LocalizationManager.Instance.GetText("TEXT_RANK_UP");
            _isRankUp = true;
        }
        else
        {
            head = LocalizationManager.Instance.GetText("TEXT_RANK_DOWN");
            _isRankUp = false;
        }


        SetText(head, GameHelper.GetRankName(endRankIndex));
    }

    public void SetAvatar(string avatarKey)
    {
        IMG_AvatarOld.sprite = SpriteResourceHelper.LoadSprite(avatarKey, SpriteResourceHelper.SpriteType.Avatar);
        IMG_AvatarNew.sprite = SpriteResourceHelper.LoadSprite(avatarKey, SpriteResourceHelper.SpriteType.Avatar);
    }

    private void SetText(string title, string message)
    {
        _TEXT_Head.text = title;
        _TEXT_Rank.text = message;
    }

    #region Animation
    private void ShowAnimation()
    {
        if (_isRankUp)
        {
            PlayRankUp();
        }
        else
        {
            PlayRankDown();
        }
    }

    private void PlayRankUp()
    {
        GetComponent<Animator>().SetTrigger("RankUp");
    }

    private void PlayRankDown()
    {
        GetComponent<Animator>().SetTrigger("RankDown");
    }
    #endregion

}
