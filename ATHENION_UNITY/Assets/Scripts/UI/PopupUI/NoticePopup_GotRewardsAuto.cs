﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;

public class NoticePopup_GotRewardsAuto : NoticePopup_Confirm
{
    #region Serialize Properties
    [Header("Prefab reward")]
    [SerializeField] private RewardCell _PrefabReward;
    [SerializeField] private Transform _Container;
    [SerializeField] private CanvasGroup _FloatingObj;
    #endregion

    #region Private Properties
    private List<ItemData> _currentItemDataList = new List<ItemData>();
    private List<RewardCell> _rewardCellList = new List<RewardCell>();

    private Ease _animEase = Ease.OutExpo;
    private GameObject _finishFx;
    private Vector2 _originalPosition;
    private Sequence _animSeq;
    #endregion

    #region Method

    public void SetReward(List<ItemData> itemDataList)
    {
        foreach (ItemData item in itemDataList)
        {
            RewardCell cell = CreateReward(item.ItemID, item.Amount);
            _rewardCellList.Add(cell);
            cell.gameObject.SetActive(true);
        }
    }

    public void StartAnim(UnityAction onComplete = null)
    {
        ResetAnim();

        if (_animSeq != null)
        {
            _animSeq.Kill();
            _animSeq = null;
        }

        _animSeq = DOTween.Sequence();
        
        _animSeq.Append(_FloatingObj.transform.DOLocalMoveY(130f, 0.66f));
        _animSeq.Append(_FloatingObj.DOFade(0.0f, 0.33f));
        _animSeq.Join(_FloatingObj.transform.DOLocalMoveY(140f, 0.33f));
        _animSeq.OnComplete(delegate { onComplete?.Invoke(); Hide(); });
        _animSeq.Play();
    }

    private void ResetAnim()
    {
        _originalPosition = Vector2.zero;
        _FloatingObj.transform.localPosition = _originalPosition;
    }
    #endregion

    #region Cell Generate
    private RewardCell CreateReward(string rewardKey, int count)
    {
        GameObject obj = Instantiate(_PrefabReward.gameObject, _Container);
        RewardCell cell = obj.GetComponent<RewardCell>();
        cell.InitData(rewardKey, count);

        return cell;
    }
    #endregion

}

