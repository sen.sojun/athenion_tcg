﻿using GameEvent;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

#region Global Enums
public enum NoticeTypes
{
    None = 0
    , SeasonPass = 1
    , SeasonPass_MainQuest = 2
    , SeasonPass_ChallengeQuest = 3
    , Profile = 4
    , Profile_LevelProgression = 5
    , Friend_FriendRequest = 6
    , Inbox_ReadableClaimable = 7
    , Profile_FactionEarth = 8
    , Profile_FactionAir = 9
    , Profile_FactionFire = 10
    , Profile_FactionWater = 11
    , Profile_FactionHoly = 12
    , Profile_FactionDark = 13
    , Accomplishment_AchievementComplete = 14
    , Accomplishment_CardSetComplete = 15
    , SeasonPass_NewMainQuest = 16
    , Tutorial_Claim_Tutorial_Reward = 17
    , Navigator_Shop = 18
    , Shop_Promotion = 19
    , Shop_BoosterPack = 20
    , Event = 21
}
#endregion

public class NoticeDotManager : MonoSingleton<NoticeDotManager>
{
    #region Events
    public static event Action OnNoticeUpdate;
    #endregion

    #region Methods

    private void OnEnable()
    {
        InitEvents();
    }

    private void OnDisable()
    {
        DeinitEvents();
    }

    #region Events Methods
    private void InitEvents()
    {
        // subscribe event
        SeasonPassUIManager.OnCompletedQuest += InvokeUpdateNotice;
        SeasonPassUIManager.OnClose += InvokeUpdateNotice;
        SeasonPassMainQuestPanel.OnClose += InvokeUpdateNotice;
        FriendController.OnRefreshFriendList += InvokeUpdateNotice;
        ProfileEditorFactionLevel.OnCompletedRecieveReward += InvokeUpdateNotice;
        ProfileEditorLevelProgression.OnCompleteClaimReward += InvokeUpdateNotice;
        InboxController.OnRefreshInbox += InvokeUpdateNotice;
        AchievementPanelUI.OnRefreshAchievement += InvokeUpdateNotice;
        AccomplishmentsCardCollectionPanel.OnRefreshCardSet += InvokeUpdateNotice;
        DataManager.OnUpdateInventory += InvokeUpdateNotice;

        ShopManager.OnShowPage += InvokeUpdateNotice;
        EventManager.OnInit += InvokeUpdateNotice;
    }

    private void DeinitEvents()
    {
        //unsubscribe event
        SeasonPassUIManager.OnCompletedQuest -= InvokeUpdateNotice;
        SeasonPassUIManager.OnClose -= InvokeUpdateNotice;
        SeasonPassMainQuestPanel.OnClose -= InvokeUpdateNotice;
        FriendController.OnRefreshFriendList -= InvokeUpdateNotice;
        ProfileEditorFactionLevel.OnCompletedRecieveReward -= InvokeUpdateNotice;
        ProfileEditorLevelProgression.OnCompleteClaimReward -= InvokeUpdateNotice;
        InboxController.OnRefreshInbox -= InvokeUpdateNotice;
        AchievementPanelUI.OnRefreshAchievement -= InvokeUpdateNotice;
        AccomplishmentsCardCollectionPanel.OnRefreshCardSet -= InvokeUpdateNotice;
        DataManager.OnUpdateInventory -= InvokeUpdateNotice;

        ShopManager.OnShowPage -= InvokeUpdateNotice;
        EventManager.OnInit -= InvokeUpdateNotice;
    }
    #endregion

    #region Public Methods
    public void UpdateNoticeDotStatus(NoticeDotUIAttachment requester)
    {
        switch (requester.NoticeType)
        {
            case NoticeTypes.SeasonPass_MainQuest:
            {
                CheckSeasonPassMainQuest(requester);
            }
            break;

            case NoticeTypes.SeasonPass_ChallengeQuest:
            {
                CheckSeasonPassChallengeQuest(requester);
            }
            break;

            case NoticeTypes.Friend_FriendRequest:
            {
                CheckFriendRequest(requester);
            }
            break;

            case NoticeTypes.Inbox_ReadableClaimable:
            {
                CheckInboxReadableClaimable(requester);
            }
            break;

            case NoticeTypes.Profile_LevelProgression:
            {
                CheckLevelProgressionClaimable(requester);
            }
            break;

            case NoticeTypes.Profile_FactionDark:
            {
                CheckFactionLevelReward(requester, CardElementType.DARK);
            }
            break;

            case NoticeTypes.Profile_FactionEarth:
            {
                CheckFactionLevelReward(requester, CardElementType.EARTH);
            }
            break;

            case NoticeTypes.Profile_FactionFire:
            {
                CheckFactionLevelReward(requester, CardElementType.FIRE);
            }
            break;

            case NoticeTypes.Profile_FactionHoly:
            {
                CheckFactionLevelReward(requester, CardElementType.HOLY);
            }
            break;

            case NoticeTypes.Profile_FactionWater:
            {
                CheckFactionLevelReward(requester, CardElementType.WATER);
            }
            break;

            case NoticeTypes.Profile_FactionAir:
            {
                CheckFactionLevelReward(requester, CardElementType.AIR);
            }
            break;

            case NoticeTypes.Accomplishment_AchievementComplete:
            {
                CheckAchievementComplete(requester);
            }
            break;

            case NoticeTypes.Accomplishment_CardSetComplete:
            {
                CheckCardSetComplete(requester);
            }
            break;

            case NoticeTypes.SeasonPass_NewMainQuest:
            {
                CheckSeasonPassNewMainQuest(requester);
            }
            break;

            case NoticeTypes.Tutorial_Claim_Tutorial_Reward:
            {
                CheckClaimTutorialReward(requester);
            }
            break;

            case NoticeTypes.Navigator_Shop:
            case NoticeTypes.Shop_Promotion:
            {
                CheckNavigatorShopBTNNotice(requester);
            }
            break;
                
            case NoticeTypes.Event:
            {
                CheckEventNotice(requester);
            }
            break;

            default:
            {
                requester.UpdateNoticeStatus(false);
            }
            break;
        }
    }

    #endregion

    #region Private Methods
    private void InvokeUpdateNotice()
    {
        OnNoticeUpdate?.Invoke();
    }

    private void CheckClaimTutorialReward(NoticeDotUIAttachment requester)
    {
        bool shouldNotice = DataManager.Instance.IsCompletedTutorial(GameMode.Tutorial_4) == false;
        requester.UpdateNoticeStatus(shouldNotice);
    }

    private void CheckNavigatorShopBTNNotice(NoticeDotUIAttachment requester)
    {
        if (DataManager.Instance.IsFeatureEnable(FeatureUnlock.Shop) == false)
        {
            requester.UpdateNoticeStatus(false);
            return;
        }

        if (PlayerSeenListManager.Instance.IsSeenPromotion() == false)
        {
            // show new
            requester.UpdateNoticeStatus(true, NoticeImageTypes.New);
        }
        else if (DataManager.Instance.GetFreeDealRecord().IsRedeem == false)
        {
            // show free
            requester.UpdateNoticeStatus(true, NoticeImageTypes.Free);
        }
        else
        {
            requester.UpdateNoticeStatus(false);
        }
    }

    private void CheckEventNotice(NoticeDotUIAttachment requester)
    {
        // If Not Seen
        requester.UpdateNoticeStatus(PlayerSeenListManager.Instance.IsSeenEvent() == false);
    }
    
    private void CheckSeasonPassMainQuest(NoticeDotUIAttachment requester)
    {
        if (DataManager.Instance.IsFeatureEnable(FeatureUnlock.SeasonPass))
        {
            bool isBePremium = DataManager.Instance.PlayerSeasonPass.IsBePremium();

            List<QuestData> questList = new List<QuestData>();
            questList.AddRange(DataManager.Instance.PlayerSeasonPassQuest.GetMainDailyQuestDataList());
            questList.AddRange(DataManager.Instance.PlayerSeasonPassQuest.GetMainAllWeekQuestDataList());
            foreach (QuestData quest in questList)
            {
                if (quest.IsPremium && !isBePremium)
                {
                    continue;
                }

                if (quest.IsCompleted && !quest.IsCleared)
                {
                    requester.UpdateNoticeStatus(true);
                    return;
                }
            }
        }

        requester.UpdateNoticeStatus(false);
    }

    private void CheckSeasonPassChallengeQuest(NoticeDotUIAttachment requester)
    {
        if (DataManager.Instance.IsFeatureEnable(FeatureUnlock.SeasonPass))
        {
            List<QuestData> questList = new List<QuestData>();
            questList.AddRange(DataManager.Instance.PlayerSeasonPassQuest.GetChallengeQuestDataList());
            foreach (QuestData quest in questList)
            {
                if (quest.IsCompleted && !quest.IsCleared)
                {
                    requester.UpdateNoticeStatus(true);
                    return;
                }
            }
        }

        requester.UpdateNoticeStatus(false);
    }

    private void CheckFactionLevelReward(NoticeDotUIAttachment requester, CardElementType elem)
    {
        if (DataManager.Instance.IsFeatureEnable(FeatureUnlock.FactionLevel))
        {
            //foreach (CardElementType elem in Enum.GetValues(typeof(CardElementType)))
            LevelFactionRewardDB.Instance.GetData(elem.ToString(), out LevelFactionRewardDBData rewData);

            LevelFactionData flData = DataManager.Instance.PlayerInfo.LevelFactionData.GetLevelFactionData(elem);
            if (flData == null)
            {
                requester.UpdateNoticeStatus(false);
                return;
            }

            for (int i = 1; i <= flData.Level; i++)
            {
                List<ItemData> rewAtLevel = rewData.GetRewardItemList(i);
                if (rewData != null && rewAtLevel.Count > 0)
                {
                    if (DataManager.Instance.PlayerLevelFactionRewardRecord.IsRewardClaimed(elem, i))
                    {
                        //Claimed
                        continue;
                    }
                    else
                    {
                        //Claimable
                        requester.UpdateNoticeStatus(true);
                        return;
                    }
                }
            }
        }

        requester.UpdateNoticeStatus(false);
    }

    private void CheckLevelProgressionClaimable(NoticeDotUIAttachment requester)
    {
        int playerLevel = DataManager.Instance.PlayerInfo.LevelData.Level;

        for (int i = 1; i <= playerLevel; i++)
        {
            if (!DataManager.Instance.PlayerLevelRewardRecord.IsRewardClaimed(i))
            {
                if (LevelRewardDB.Instance.GetData(i.ToString(), out LevelRewardDBData data))
                {
                    List<ItemData> itemDataList = data.RewardItemList;
                    if (itemDataList != null && itemDataList.Count > 0)
                    {
                        requester.UpdateNoticeStatus(true);
                        return;
                    }
                }
            }
        }

        requester.UpdateNoticeStatus(false);
    }

    private void CheckFriendRequest(NoticeDotUIAttachment requester)
    {
        List<PlayerInfoData> requestList = DataManager.Instance.GetFriendList(FriendStatus.Request_Sender);

        bool isHaveFriendRequest = (requestList != null && requestList.Count > 0);

        requester.UpdateNoticeStatus(isHaveFriendRequest);
    }

    private void CheckInboxReadableClaimable(NoticeDotUIAttachment requester)
    {
        List<MessageInboxData> allMessageInboxData = DataManager.Instance.GetAllMessageInboxData();

        foreach (MessageInboxData message in allMessageInboxData)
        {
            if (message.IsCanClaim() || message.IsCanRead())
            {
                requester.UpdateNoticeStatus(true);
                return;
            }
        }

        requester.UpdateNoticeStatus(false);
    }

    private void CheckAchievementComplete(NoticeDotUIAttachment requester)
    {
        if (DataManager.Instance.IsFeatureEnable(FeatureUnlock.Achievement))
        {
            List<AchievementData> achievementList = DataManager.Instance.PlayerAchievement.AchievementList;

            foreach (AchievementData item in achievementList)
            {
                if (item.IsCanClaim())
                {
                    requester.UpdateNoticeStatus(true);
                    return;
                }
            }
        }

        requester.UpdateNoticeStatus(false);
    }

    private void CheckCardSetComplete(NoticeDotUIAttachment requester)
    {
        if (DataManager.Instance.IsFeatureEnable(FeatureUnlock.CardSet))
        {
            List<AchievementCardSetData> cardSetList = DataManager.Instance.PlayerAchievementCardSet.AchievementList;

            foreach (AchievementCardSetData item in cardSetList)
            {
                if (item.IsCanClaim())
                {
                    requester.UpdateNoticeStatus(true);
                    return;
                }
            }
        }

        requester.UpdateNoticeStatus(false);
    }

    private void CheckSeasonPassNewMainQuest(NoticeDotUIAttachment requester)
    {
        if (DataManager.Instance.IsFeatureEnable(FeatureUnlock.SeasonPass))
        {
            List<QuestData> playerQuestList = DataManager.Instance.PlayerSeasonPassQuest.GetMainAllWeekQuestDataList();
            foreach (QuestData quest in playerQuestList)
            {
                if (quest.IsNew)
                {
                    requester.UpdateNoticeStatus(true, NoticeImageTypes.New);
                    return;
                }
            }
        }

        requester.UpdateNoticeStatus(false);
    }
    #endregion

    #endregion
}