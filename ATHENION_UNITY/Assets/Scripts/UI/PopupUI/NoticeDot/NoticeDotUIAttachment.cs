﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class NoticeDotUIAttachment : MonoBehaviour
{
    #region Public Properties
    public bool IsNoticedSelf { get; private set; }
    public string NameTag { get { return _NameTage; } }
    public NoticeTypes NoticeType { get { return _NoticeType; } }
    public NoticeImageTypes NoticeImageType { get { return _noticeImageType; } }
    #endregion

    #region Private Properties
    private NoticeImageTypes _noticeImageType = NoticeImageTypes.Dot;
    #endregion

    #region Serialize Properties
    [SerializeField] private string _NameTage;
    [SerializeField] private NoticeTypes _NoticeType;
    [SerializeField] private NoticeDotImage _NoticeImage;
    [SerializeField] private List<NoticeDotUIAttachment> _NoticeDotSubComponents = new List<NoticeDotUIAttachment>();
    #endregion

    #region Methods
    private void OnEnable()
    {
        InitEvents();
        RequestUpdateNoticeDot();
    }

    private void OnDisable()
    {
        DeinitEvents();
    }

    private void InitEvents()
    {
        //subscribe event
        NoticeDotManager.OnNoticeUpdate += OnUpdateNoticeDot;
    }

    private void DeinitEvents()
    {
        //unsubscribe event
        NoticeDotManager.OnNoticeUpdate -= OnUpdateNoticeDot;
    }

    private void OnUpdateNoticeDot()
    {
        if(gameObject.activeSelf)
        {
            RequestUpdateNoticeDot();
        }
    }

    public void RequestUpdateNoticeDot()
    {
        NoticeDotManager.Instance.UpdateNoticeDotStatus(this);
    }

    private void UpdateNoticeDotUIImage()
    {
        if(_NoticeImage == null)
        {
            return;
        }

        if(IsNoticedSelf)
        {
            _NoticeImage.ShowNoticeImage(_noticeImageType);
        }
        else
        {
            _NoticeImage.HideNoticeImage();
        }
    }

    public void UpdateNoticeStatus(bool isNoticed, NoticeImageTypes noticeImageTypes = NoticeImageTypes.Dot)
    {
        _noticeImageType = noticeImageTypes;

        IsNoticedSelf = isNoticed;
        foreach (NoticeDotUIAttachment subNotice in _NoticeDotSubComponents)
        {
            if (subNotice == null)
            {
                continue;
            }

            subNotice.RequestUpdateNoticeDot();
            IsNoticedSelf |= subNotice.IsNoticedSelf;

            if (subNotice.IsNoticedSelf)
            {
                if ((int)subNotice.NoticeImageType > (int)_noticeImageType)
                {
                    _noticeImageType = subNotice.NoticeImageType;
                }
            }
        }

        UpdateNoticeDotUIImage();
    }
    #endregion
}