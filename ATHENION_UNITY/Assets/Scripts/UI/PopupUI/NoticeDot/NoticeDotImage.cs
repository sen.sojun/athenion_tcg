﻿using System.Collections.Generic;
using System;
using System.Linq;
using UnityEngine;

#region Global Enums
public enum NoticeImageTypes
{
    Dot = 0
    , Free = 1
    , New = 2
}
#endregion

public class NoticeDotImage : MonoBehaviour
{
    #region Inspector Properties
    [SerializeField] private GameObject[] _NoticeImages;
    #endregion

    #region Methods
    public void ShowNoticeImage(NoticeImageTypes type)
    {
        HideNoticeImage();

        for(int index = Enum.GetValues(typeof(NoticeImageTypes)).Length - 1; index >= 0; index--)
        {
            if(type == (NoticeImageTypes)index)
            {
                if(index < _NoticeImages.Length)
                {
                    _NoticeImages[index].SetActive(true);
                    break;
                }
            }
        }
    }

    public void HideNoticeImage()
    {
        foreach(GameObject image in _NoticeImages)
        {
            image.SetActive(false);
        }
    }
    #endregion
}