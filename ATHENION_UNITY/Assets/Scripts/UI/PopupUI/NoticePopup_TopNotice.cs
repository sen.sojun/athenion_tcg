﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;

public class NoticePopup_TopNotice : PopupUI
{
    [SerializeField] private TextMeshProUGUI _TEXT_Head;
    [SerializeField] private TextMeshProUGUI _TEXT_Title;
    [SerializeField] private TextMeshProUGUI _TEXT_Message;
    [SerializeField] private Button _BTN_Okay;
    [SerializeField] private float AutoHideDuration = 3.0f;

    public override void Show(UnityAction onShowComplete)
    {
        _TEXT_Title.text = Data.Title;
        _TEXT_Message.text = Data.Message;

        base.Show(onShowComplete);
        StartCoroutine(AutoHide());
    }

    public void SetBTN(UnityAction onClickOkay)
    {
        _BTN_Okay.onClick.RemoveAllListeners();
        _BTN_Okay.onClick.AddListener(delegate
        {
            StopCoroutine(AutoHide());
            base.Hide();
            onClickOkay?.Invoke();
        });
    }

    private IEnumerator AutoHide()
    {
        yield return new WaitForSeconds(AutoHideDuration);
        base.Hide();
    }

    public void SetText(string head,string title, string message)
    {
        _TEXT_Head.text = head;
        _TEXT_Title.text = title;
        _TEXT_Message.text = message;
    }
}
