﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;


public class PopupUISection : MonoBehaviour
{
    #region Private Properties
    private List<PopupUI> _notificationList = new List<PopupUI>();
    private PopupUI _currentPopup = null;
    private bool _isShow = false;
    #endregion

    #region Show/Hide  
    private void ShowPopup()
    {
        if (!_isShow)
        {
            if (_notificationList.Count > 0)
            {
                _currentPopup = _notificationList[0];
                _currentPopup.SetOnHideComplete(() =>
                { 
                    _notificationList.RemoveAt(0);
                    ShowPopupNext();
                });
                _currentPopup.Show(null);
                _isShow = true;
            }
            else
            {
                // queue empty
            }
        }
    }

    private void ShowPopupNext()
    {
        _isShow = false;
        Destroy(_currentPopup.gameObject);
        ShowPopup();
    }

    public void ClearPopup()
    {
        _isShow = false;
        if (_notificationList.Count > 0)
        {
            _notificationList.Clear();
        }

        if (_currentPopup != null)
        {
            _currentPopup.Hide();
        }


    }
    #endregion

    #region Queue
    public void AddQueue(GameObject obj, bool isAddFirst = false)
    {
        // set up
        obj.SetActive(false);
        obj.transform.SetParent(this.transform);
        RectTransform rect = obj.GetComponent<RectTransform>();
        rect.anchoredPosition = Vector2.zero;
        rect.localScale = Vector3.one;
        GameHelper.RectUI_SetAndStretchToParentSize(rect);

        PopupUI popup = obj.GetComponent<PopupUI>();
        if (popup != null)
        {
            if (isAddFirst)
            {
                _notificationList.Insert(0, popup);
            }
            else
            {
                _notificationList.Add(popup);
            }
            ShowPopup();
        }
        else
        {
            Debug.LogWarning(obj + ": Don't have NotificationUI");
        }
    }

    private void RemoveQueue(PopupUI data)
    {

    }
    #endregion

    #region Methods
    public bool IsHasPopupUIInList(string notificationID)
    {
        //Debug.Log("in");
        PopupUI target = _notificationList.Find(x => x.Data.ID == notificationID);
        if (target != null)
        {
            //Debug.Log(target.Data.ID);
        }

        return target != null;
    }
    #endregion
}