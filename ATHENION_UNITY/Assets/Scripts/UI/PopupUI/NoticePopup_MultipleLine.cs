﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoticePopup_MultipleLine : NoticePopup_Confirm
{
    public void SetMessage(List<string> messageList)
    {
        string totalMessage = "";
        foreach (string item in messageList)
        {
            totalMessage += item + "\n";
        }
        _TEXT_Message.text = totalMessage;
    }
}
