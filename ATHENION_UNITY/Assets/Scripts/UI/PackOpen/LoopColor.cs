﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class LoopColor : MonoBehaviour
{
    public Image TargetImage;

    [Header("Color Loop")]
    public Gradient GradientColor;

    private Sequence _sq;

    private void OnEnable()
    {
        _sq.Kill();
        _sq = DOTween.Sequence();

        _sq.Append(TargetImage.DOGradientColor(GradientColor, 1.0f));
        _sq.SetLoops(-1);
    }

}
