using DG.Tweening;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Karamucho;
using Karamucho.UI;
using UnityEngine.SceneManagement;
using Newtonsoft.Json;
using System;
using Random = UnityEngine.Random;

public class CardPackController : MonoSingleton<CardPackController>
{
    #region Event

    public delegate void CardPackControllerSkipOpeningHandler(PackReward packDetail);
    public static event CardPackControllerSkipOpeningHandler OnStartSystem;
    public static event CardPackControllerSkipOpeningHandler OnSkip;

    public delegate void CardPackControllerHandler();
    public static event CardPackControllerHandler OnSpawnCards;

    #endregion

    #region Public Properties
    public GameObject PrefabCard;
    public GameObject PrefabItem;
    public GameObject PrefabMarker;
    public Transform Canvas;
    public Transform CardGroup;
    public Transform Marker;
    public Transform MarkerCenter;
    public Transform HandContainer;
    public List<Transform> TableMarker;

    [Header("Pack Page")]
    public GameObject GROUP_PackCard;
    public GameObject Container_Pack;
    public GameObject GROUP_CardOpen;

    [Header("Result")]
    public GameObject ResultPage;
    public Transform ResultContent;
    public GameObject PrefabCard_small;
    public GameObject PrefabItem_small;

    [Header("Animator")]
    private PackAnimationModule Anim_Pack;
    public CanvasGroup Fade_White;
    public CanvasGroup Fade_Black;
    public Animator Anim_GROUP_PACK;

    [Header("UI")]
    public TextMeshProUGUI TEXT_PackCount;
    public TextMeshProUGUI TEXT_Result;
    [Header("Card Detail")]
    public BaseCardUI DetailCard;
    public GameObject DetailGroup;
    [Header("Item Detail")]
    public CardUI_Item DetailItem;
    public GameObject DetailItemGroup;

    [Header("BTN")]
    public Button BTN_OpenPack;
    public Button BTN_Next;
    public Button BTN_BackToMenu;
    public Button BTN_Left;
    public Button BTN_Right;
    public Button BTN_BuyMore;
    public Button BTN_OpenAgain;
    public Button BTN_Skip;

    public static event Action OnObjectDestroy;

    #endregion

    #region Private Properties
    private static int _cardAmount = 5;
    private static float _startAngle = 30;
    private static float _endAngle = -30;
    private static int _resultCountPerPage = 12;
    private List<Transform> _listHandMarker = new List<Transform>();

    private float _ZorderOffset = (3.5f);
    private List<GameObject> _listCard = new List<GameObject>();
    private List<GameObject> _listOpened = new List<GameObject>();
    private List<ItemReward> _currentCardDataList = new List<ItemReward>();
    private List<CurrencyReward> _currentItemDataList = new List<CurrencyReward>();
    private List<PackResult> _cardPackList = new List<PackResult>();
    private List<ItemReward> _resultData = new List<ItemReward>();
    private List<string> _bonusRewardList = new List<string>();
    List<List<ItemReward>> _pageData = new List<List<ItemReward>>();

    private FlexiblePooling CardPool;
    private FlexiblePooling ItemPool;

    private Sequence _sq;
    private CardRarity _packRarity;
    private int _currentOpenedCard = 0;
    private int _packRemainingCount = 0;
    private int _currentPackIndex = 0;

    private int _currentPage = 0;
    private int _maxPage;

    private List<BaseCardPackPrefab> _cardResultList = new List<BaseCardPackPrefab>();

    private bool _isMoveCardOut;
    private bool _canOpenAgain;
    private bool _canBuyMore;

    private PackReward _PackReward;
    private GameObject _currentAnimatedPack;
    private string _animatePackPath = "Prefabs/Packs/";
    private string _animatePackPrefix = "PACK_";
    #endregion

    protected override void Awake()
    {
        base.Awake();
        BTN_BuyMore.gameObject.SetActive(false);
        BTN_OpenAgain.gameObject.SetActive(false);

        CardPool = new FlexiblePooling(ResultContent, PrefabCard_small, 12);
        ItemPool = new FlexiblePooling(ResultContent, PrefabItem_small, 4);
    }

    public void StartSystem(PackReward packReward, UnityAction onSystemStartComplete)
    {
        Debug.Log(JsonConvert.SerializeObject(packReward));
        Debug.Log("OpenPack:" + packReward.PackID);
        //// Setup Data
        _PackReward = packReward;
        _currentPackIndex = 0;
        _currentPage = 0;
        _cardPackList = packReward.PackResultList;
        _packRemainingCount = packReward.Amount;
        _bonusRewardList = packReward.BonusReward;
        Debug.Log("Bonus: " + packReward.BonusReward.Count);
        _cardAmount = _cardPackList[_currentPackIndex].ItemResultList.Count;

        InitPack(packReward.PackID);
        InitCardPackData(_cardPackList[_currentPackIndex]);
        InitResult(_cardPackList);
        InitBTN();

        onSystemStartComplete?.Invoke();
        OnStartSystem?.Invoke(packReward);

        // Show Pack Count & BTN
        BTN_OpenPack_Enable(true);
        BTN_Next_Enable(false);
        SetShowResult(false);
        SetShowPack(true);
        Anim_Pack.InitData();
        Anim_Pack.ShowPack();
    }

    private void StartNextPack()
    {
        // Show Pack Count & BTN
        TEXT_PackCount.gameObject.SetActive(true);
        BTN_Next_Enable(false);
        BTN_OpenPack_Enable(true);
        BTN_Skip.gameObject.SetActive(true);

        // Init Data Again
        InitCardPackData(_cardPackList[_currentPackIndex]);
        InitBTN();

        Anim_Pack.ShowPack();

    }


    #region Method
    public void InitCardPackData(PackResult pack)
    {
        // Reset
        _listOpened.Clear();
        _currentOpenedCard = 0;
        _currentCardDataList.Clear();
        _currentItemDataList.Clear();

        UpdatePackCountUI();
        _packRemainingCount--;
        _currentPackIndex++;

        // Find Rarity and Add to result (data only)
        CardRarity cardRarity = CardRarity.Common;
        // Card
        foreach (ItemReward card in pack.CardList)
        {
            _currentCardDataList.Add(card);
            CardData data = CardData.CreateCard(card.BaseID);
            if (data.Rarity > cardRarity)
            {
                cardRarity = data.Rarity;
            }
            Debug.Log(card.BaseID + "is add");
        }
        // Item
        foreach (CurrencyReward item in pack.ItemList)
        {
            _currentItemDataList.Add(item);
            if (item.Rarity > cardRarity)
            {
                cardRarity = item.Rarity;
            }
            Debug.Log(item.BundleID + " :" + item.ItemID + "is add");
        }

        _packRarity = cardRarity;

    }

    public void InitPack(string packID)
    {
        if (_currentAnimatedPack == null)
        {
            GameObject prefabPack = ResourceManager.Load<GameObject>(_animatePackPath + _animatePackPrefix + packID);
            if (prefabPack == null)
            {
                Debug.LogWarning("Cannot find:" + _animatePackPath + _animatePackPrefix + packID);
                prefabPack = ResourceManager.Load<GameObject>(_animatePackPath + _animatePackPrefix + "BASE");
            }
            _currentAnimatedPack = Instantiate(prefabPack, Container_Pack.transform);
            _currentAnimatedPack.transform.localPosition = Vector3.zero;
            Anim_Pack = _currentAnimatedPack.GetComponent<PackAnimationModule>();
        }
    }

    private void InitBTN()
    {
        BTN_OpenPack.onClick.RemoveAllListeners();
        BTN_OpenPack.onClick.AddListener(StartOpenPack);

        BTN_Skip.onClick.RemoveAllListeners();
        BTN_Skip.onClick.AddListener(SkipOpening);

        BTN_Next.onClick.RemoveAllListeners();
        BTN_Next.onClick.AddListener(OpenNextPack);

        BTN_BackToMenu.onClick.RemoveAllListeners();
        BTN_BackToMenu.onClick.AddListener(BackToMenu);

        BTN_Left.onClick.RemoveAllListeners();
        BTN_Left.onClick.AddListener(PreviousPage);

        BTN_Right.onClick.RemoveAllListeners();
        BTN_Right.onClick.AddListener(NextPage);
    }

    #endregion

    #region Generate Card Prefab
    public void InitCard()
    {
        ClearCard();
        //card
        foreach (ItemReward item in _currentCardDataList)
        {
            GameObject obj = Instantiate(PrefabCard, CardGroup);
            _listCard.Add(obj);
            obj.transform.localPosition = Marker.localPosition;
            obj.GetComponent<CardPackPrefab_card>().SetData(item.BaseID);
        }
        //item
        foreach (CurrencyReward item in _currentItemDataList)
        {
            GameObject obj = Instantiate(PrefabItem, CardGroup);
            _listCard.Add(obj);
            obj.transform.localPosition = Marker.localPosition;
            obj.GetComponent<CardPackPrefab_item>().SetData(item.BundleID);
        }

        //Sort
        _listCard.Shuffle();
        InitHandMarker(SortTable);
        OnSpawnCards?.Invoke();
    }

    public void ClearCard()
    {
        if (_listCard.Count > 0)
        {
            foreach (GameObject item in _listCard)
            {
                Destroy(item);
            }
            _listCard.Clear();
        }

    }
    #endregion

    #region Result Page
    public void InitResult(List<PackResult> packData)
    {
        _resultData.Clear();

        foreach (PackResult itemList in packData)
        {
            foreach (ItemReward item in itemList.ItemResultList)
            {
                _resultData.Add(item);
            }
        }
        List<ItemReward> cardGroup = _resultData.FindAll((item) => item.IsCard == true);
        IEnumerable<ItemReward> cardQuery = cardGroup.OrderByDescending(item => item.Rarity).ThenByDescending(item => item.Value).ThenBy(item => item.BaseID);

        List<ItemReward> itemGroup = _resultData.FindAll((item) => item.IsCard == false);
        IEnumerable<ItemReward> itemQuery = itemGroup.OrderByDescending(item => item.Rarity).ThenByDescending(item => item.Value).ThenBy(item => item.BaseID);

        _resultData = new List<ItemReward>();
        _resultData.AddRange(cardQuery);
        _resultData.AddRange(itemQuery);

        _pageData.Clear();

        while (_resultData.Count > 0)
        {
            _pageData.Add(_resultData.CutFirst(12));
        }

        string debugText = "";
        foreach (ItemReward item in _resultData)
        {
            debugText += "-" + item.BaseID;
        }
        //Debug.Log(debugText);

        //Cal Page
        _maxPage = _pageData.Count - 1;

        SetCardResult();

    }

    public void SetShowResult(bool isShow)
    {
        // Result UI
        ResultPage.SetActive(isShow);
        BTN_BackToMenu.gameObject.SetActive(isShow);
        BTN_Left.gameObject.SetActive(isShow);
        BTN_Right.gameObject.SetActive(isShow);
        TEXT_Result.gameObject.SetActive(isShow);

        if (isShow)
        {
            SoundManager.PlaySFX(SoundManager.SFX.Pack_Flip_Rare);
            BTN_OpenAgain.gameObject.SetActive(_canOpenAgain);
            BTN_BuyMore.gameObject.SetActive(_canBuyMore);
        }
        else
        {
            BTN_OpenAgain.gameObject.SetActive(false);
            BTN_BuyMore.gameObject.SetActive(false);
        }
    }

    public void SetShowPack(bool isShow)
    {
        GROUP_CardOpen.SetActive(isShow);
        GROUP_PackCard.SetActive(isShow);
        TEXT_PackCount.gameObject.SetActive(isShow);
        BTN_OpenPack_Enable(isShow);
        BTN_Skip.gameObject.SetActive(isShow);

    }

    private BaseCardPackPrefab GetItemRewardObject(ItemReward item)
    {
        GameObject obj = item.IsCard ? CardPool.GetObject() : ItemPool.GetObject();
        obj.transform.SetAsLastSibling();
        return obj.GetComponent<BaseCardPackPrefab>();
    }

    public void ResetAllCard()
    {
        for (int i = 0; i < _cardResultList.Count; i++)
        {
            _cardResultList[i].gameObject.SetActive(false);
        }
        _cardResultList.Clear();
    }

    public void SetCardResult()
    {
        foreach (ItemReward item in _pageData[_currentPage])
        {
            BaseCardPackPrefab obj = GetItemRewardObject(item);
            obj.gameObject.SetActive(true);
            obj.SetData(item.BaseID);
            obj.OpenCard(item.Rarity, null);
            _cardResultList.Add(obj);
        }

    }

    public void UpdatePackCountUI()
    {
        TEXT_PackCount.text = string.Format(LocalizationManager.Instance.GetText("MENU_PACK_PACKREMAIN"), _packRemainingCount);
    }

    public void UpdatePageUI()
    {
        if (_currentPage > 0 && _currentPage < _maxPage)
        {
            BTN_Left_PageEnable(true);
            BTN_Right_PageEnable(true);
        }
        else if (_currentPage == 0 && _currentPage < _maxPage)
        {
            BTN_Left_PageEnable(false);
            BTN_Right_PageEnable(true);
        }
        else if (_currentPage == _maxPage && _currentPage > 0)
        {
            BTN_Left_PageEnable(true);
            BTN_Right_PageEnable(false);
        }
        else if (_currentPage == 0 && _currentPage == _maxPage)
        {
            BTN_Left_PageEnable(false);
            BTN_Right_PageEnable(false);
        }
    }
    #endregion

    #region Event
    public void StartOpenPack()
    {
        // Close BTN
        BTN_OpenPack_Enable(false);
        TEXT_PackCount.gameObject.SetActive(false);

        BTN_Skip.gameObject.SetActive(false);

        // Start Pack
        PlayOpenPack(_packRarity, InitCard);

        // Update TEXT
        UpdatePackCountUI();
    }

    public void OpenCard(BaseCardPackPrefab item)
    {
        if (_isMoveCardOut)
        {
            // Block Click when opening next pack.
            return;
        }

        // Open same card to show detail
        if (item.IsOpened)
        {
            if (item.GetComponent<CardPackPrefab_card>())
            {
                ShowDetailCard(item);
                return;
            }
            else if (item.GetComponent<CardPackPrefab_item>())
            {
                ShowDetailItem((CardPackPrefab_item)item);
                return;
            }

        }

        if (_currentOpenedCard >= _listCard.Count || _listCard.Count == 0)
        {
            return;
        }

        // Check if card is all opened
        _listOpened.Add(item.gameObject);
        item.OpenCard(
            item.GetRarity()
            , delegate
        {
            if (_listOpened.Count == _listCard.Count)
            {
                BTN_Next_Enable(true);
            }
        });

        _currentOpenedCard++;
    }

    public void ShowDetailCard(BaseCardPackPrefab card)
    {
        CardData data;
        CardData.GetCardData(card.ItemID, out data);
        CardUIData cardUIData = new CardUIData(-1, data, DataManager.Instance.DefaultCardBackIDList[0]);
        DetailCard.SetData(cardUIData);
        DetailGroup.SetActive(true);
    }

    public void ShowDetailItem(CardPackPrefab_item card)
    {
        DetailItem.SetData(card.GetData());
        DetailItemGroup.SetActive(true);
    }

    public void HideDetailCard()
    {
        DetailGroup.SetActive(false);
    }

    public void HideDetailItem()
    {
        DetailItemGroup.SetActive(false);
    }

    public void OpenNextPack()
    {
        BTN_Next_Enable(false);

        if (_packRemainingCount > 0)
        {
            if (_isMoveCardOut)
            {
                return;
            }

            MoveCardOut(StartNextPack);
        }
        else
        {
            MoveCardOut(EndProcess);
            // Back to result / Menu
        }
    }

    #endregion

    #region BTN
    public void SetBTNBuyMore(UnityAction onClick)
    {
        BTN_BuyMore.onClick.RemoveAllListeners();
        BTN_BuyMore.onClick.AddListener(onClick);
    }

    public void SetBTNOpenAgain(UnityAction onClick)
    {
        BTN_OpenAgain.onClick.RemoveAllListeners();
        BTN_OpenAgain.onClick.AddListener(onClick);
    }

    public void SetBTNOpenAgain_Enable(bool isEnable)
    {
        BTN_OpenAgain.gameObject.SetActive(isEnable);
        _canOpenAgain = isEnable;
    }

    public void SetBTNBuyMore_Enable(bool isEnable)
    {
        BTN_BuyMore.gameObject.SetActive(isEnable);
        _canBuyMore = isEnable;
    }

    public void BTN_OpenPack_Enable(bool isShow)
    {
        BTN_OpenPack.gameObject.SetActive(isShow);
    }

    public void BTN_Next_Enable(bool isShow)
    {
        BTN_Next.gameObject.SetActive(isShow);
    }

    public void BTN_Left_PageEnable(bool isShow)
    {
        BTN_Left.gameObject.SetActive(isShow);
    }

    public void BTN_Right_PageEnable(bool isShow)
    {
        BTN_Right.gameObject.SetActive(isShow);
    }
    #endregion

    #region Navigation
    public void BackToMenu()
    {
        OnObjectDestroy?.Invoke();
        SceneManager.UnloadSceneAsync(GameHelper.PackOpeningSceneName);
        this.DestroySelf();
    }

    public void SkipOpening()
    {
        EndProcess();
        OnSkip?.Invoke(_PackReward);
    }

    private void EndProcess()
    {
        SetShowPack(false);
        SetShowResult(true);
        ResetAllCard();
        SetCardResult();
        UpdatePageUI();
    }

    public void PreviousPage()
    {
        if (_currentPage > 0)
        {
            _currentPage--;
            ResetAllCard();
            SetCardResult();
            UpdatePageUI();
        }
    }

    public void NextPage()
    {
        if (_currentPage < _maxPage)
        {
            _currentPage++;
            ResetAllCard();
            SetCardResult();
            UpdatePageUI();
        }
    }
    #endregion

    #region Animation
    public void FadeWhite(float frontDuration = 0.1f, float holdDuration = 0.4f, float backDuration = 0.5f)
    {
        Fade_White.alpha = 0.0f;

        Sequence sq = DOTween.Sequence();
        sq.Append(Fade_White.DOFade(1.0f, frontDuration));
        sq.Append(Fade_White.DOFade(1.0f, holdDuration));
        sq.Append(Fade_White.DOFade(0.0f, backDuration));
    }

    public void FadeBlack(float duration = 0.5f)
    {
        Fade_Black.alpha = 0.0f;

        Sequence sq = DOTween.Sequence();
        sq.Append(Fade_Black.DOFade(1.0f, duration));
        sq.Append(Fade_Black.DOFade(0.0f, duration));
    }

    public void PlayOpenPack(CardRarity rarity, UnityAction onComplete)
    {
        Anim_Pack.gameObject.SetActive(true);
        Anim_Pack.PlayOpenPackAnimation(rarity, onComplete);
    }

    private void InitHandMarker(UnityAction onComplete)
    {
        //Debug.Log("InitMaker");
        while (_listHandMarker.Count < _listCard.Count)
        {
            GameObject obj = Instantiate(PrefabMarker, HandContainer);
            _listHandMarker.Add(obj.transform);
        }

        LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)HandContainer.transform);

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    private void SortTable()
    {
        //Debug.Log("SortCard");
        _sq.Kill();
        _sq = DOTween.Sequence();
        float angleSpace = Mathf.DeltaAngle(_startAngle, _endAngle) / (_listCard.Count - 1);
        float duration = 0.05f;

        for (int i = 0; i < _listCard.Count; i++)
        {
            Vector3 cardPos = TableMarker[i].position + new Vector3(0, _ZorderOffset * i, 0);
            Vector3 cardRot = new Vector3(0, 0, 0);

            //_listCard[i].transform.localScale = Vector3.one;

            _sq.Append(_listCard[i].transform.DOMove(MarkerCenter.position, duration).SetEase(Ease.OutCubic));
            _sq.Join(_listCard[i].transform.DOLocalRotate(new Vector3(0, 0, 0), duration).SetEase(Ease.OutExpo));
            _sq.Append(_listCard[i].transform.DOMove(cardPos, duration).SetEase(Ease.OutBack));
            _sq.Join(_listCard[i].transform.DOLocalRotate(cardRot, duration).SetEase(Ease.OutBack));

        }
    }

    private void MoveCardOut(UnityAction onComplete)
    {
        _isMoveCardOut = true;
        _sq.Kill();
        _sq = DOTween.Sequence();
        for (int i = 0; i < _listOpened.Count; i++)
        {
            GameObject obj = _listOpened[i];
            _sq.Join(obj.transform.DOLocalMoveX(2000.0f, 1.0f).SetEase(Ease.InExpo).OnComplete(delegate
            {
                obj.SetActive(false);
            }));
        }
        _sq.OnComplete(delegate
        {
            if (onComplete != null)
            {
                onComplete.Invoke();
            }
            _isMoveCardOut = false;
            BG_Reset();
        });
    }

    public void BG_Open()
    {
        Anim_GROUP_PACK.SetBool("Open", true);
    }

    public void BG_Reset()
    {
        Anim_GROUP_PACK.SetTrigger("Reset");
        Anim_GROUP_PACK.SetBool("Open", false);
    }
    #endregion
}
