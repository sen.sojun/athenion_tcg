using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.U2D;
using DG.Tweening;
using TMPro;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using Karamucho.UI;

public abstract class BaseCardPackPrefab : MonoBehaviour, IPointerClickHandler
{
    #region Public Properties
    public string ItemID { protected set; get; }
    [SerializeField] protected Animator Animator;

    [Header("Reference")]
    [SerializeField] protected TextMeshProUGUI TEXT_CardName;
    [SerializeField] protected TextMeshProUGUI TEXT_CardDetail;

    [SerializeField] protected Image GROUP_FRAME;
    [SerializeField] protected Image GROUP_FRAMEBOX;
    [SerializeField] protected GameObject GROUP_ART;
    public bool IsOpened { protected set; get; }

    [Header("Sound")]
    [SerializeField] protected bool MuteSound;

    #endregion

    #region Private Properties
    private float _ratioDeference = 1.0f;
    private UnityAction _onComplete;
    #endregion

    #region Method
    public abstract void SetData(string id);

    public abstract CardRarity GetRarity();

    #region Utility
    protected void SetDynamicFont()
    {
        if (transform.parent.GetComponent<GridLayoutGroup>())
        {
            // Get cell size
            float cellSize = transform.parent.GetComponent<GridLayoutGroup>().cellSize.x;
            _ratioDeference = (float)(cellSize / BaseCardUI.Get_DEFAULT_CARD_WIDTH());
        }
        else
        {
            // Get cell size
            float cellSize = GetComponent<RectTransform>().sizeDelta.x;
            _ratioDeference = (float)(cellSize / BaseCardUI.Get_DEFAULT_CARD_WIDTH());
        }

        // Calculate Dynamic Margin
        TEXT_CardDetail.fontSize = BaseCardUI.Get_DEFAULT_FONT_SIZE() * _ratioDeference;
        TEXT_CardDetail.margin = new Vector4(
            BaseCardUI.Get_DEFAULT_FONT_MARGIN().x * _ratioDeference,
            BaseCardUI.Get_DEFAULT_FONT_MARGIN().y * _ratioDeference,
            BaseCardUI.Get_DEFAULT_FONT_MARGIN().z * _ratioDeference,
            BaseCardUI.Get_DEFAULT_FONT_MARGIN().w * _ratioDeference
            );
        TEXT_CardDetail.GetComponent<LayoutElement>().minHeight = BaseCardUI.Get_DEFAULT_FRAMEBOX_MIN_HEIGHT() * _ratioDeference;

    }
    #endregion
    #endregion

    #region Animation
    public void SetOpenCardComplete(UnityAction onComplete)
    {
        _onComplete = onComplete;
    }

    public void OnOpenComplete()
    {
        if (_onComplete != null)
        {
            _onComplete.Invoke();
        }
    }

    public void OpenCard(CardRarity rarity,UnityAction onComplete)
    {
        SetOpenCardComplete(onComplete);
        IsOpened = true;
        switch (rarity)
        {
            case CardRarity.None:
            case CardRarity.Common:
            {
                Animator.SetTrigger("Open");
                break;
            }
            case CardRarity.Rare:
            {
                Animator.SetTrigger("OpenRare");
                break;
            }
            case CardRarity.Epic:
            {
                Animator.SetTrigger("OpenEpic");
                break;
            }
            case CardRarity.Legend:
            {
                Animator.SetTrigger("OpenLegend");
                break;
            }
        }      

    }

    #endregion

    #region Sound
    public void Play_FlipNormal()
    {
        if (!MuteSound) SoundManager.PlaySFX(SoundManager.SFX.Draw_Card);
    }

    public void Play_FlipRare()
    {
        if (!MuteSound) SoundManager.PlaySFX(SoundManager.SFX.Pack_Flip_Rare);
    }

    public void Play_FlipEpic()
    {
        if (!MuteSound)
        {
            SoundManager.PlaySFX(SoundManager.SFX.Pack_Flip_Epic);
            //SoundManager.PlayMinionSummonVoice(_data.BaseID);
        }
    }

    public void Play_FlipLegendary()
    {
        if (!MuteSound)
        {
            SoundManager.PlaySFX(SoundManager.SFX.Pack_Flip_Legendary);
            //SoundManager.PlayMinionSummonVoice(ItemID);
        }
    }
    #endregion

    public void OnPointerClick(PointerEventData eventData)
    {
        SoundManager.PlaySFX(SoundManager.SFX.Card_Click);
        CardPackController.Instance.OpenCard(this);
    }
}
