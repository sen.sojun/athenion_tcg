﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class BlinkingText : MonoBehaviour
{
    public TextMeshProUGUI TargetImage;

    private Sequence _sq;

    private void Awake()
    {
        _sq.Kill();
        _sq = DOTween.Sequence();

        _sq.Append(TargetImage.DOFade(0.2f, 0.5f));
        _sq.Append(TargetImage.DOFade(1.0f, 0.5f));
        _sq.SetLoops(-1);
    }
}
