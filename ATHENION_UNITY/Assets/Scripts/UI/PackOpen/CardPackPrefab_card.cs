using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.U2D;
using DG.Tweening;
using TMPro;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using Karamucho.UI;

public class CardPackPrefab_card : BaseCardPackPrefab
{
    #region Inspector Properties
    [Header("Game Stat")]
    [SerializeField] private TextMeshProUGUI _TEXT_ATK;
    [SerializeField] private TextMeshProUGUI _TEXT_HP;
    [SerializeField] private TextMeshProUGUI _TEXT_SOUL;
    [SerializeField] private TextMeshProUGUI _TEXT_ARMOR;

    [Header("Icon Stat")]
    [SerializeField] private Image _ICON_ATK;
    [SerializeField] private Image _ICON_HP;
    [SerializeField] private Image _ICON_ARMOR;
    [SerializeField] private Image _ICON_SERIES;
    [SerializeField] private GameObject _EFFECT_Foil;

    [Header ("Notification")]
    [SerializeField] private GameObject _GROUP_NewNotice;
    [SerializeField] private TextMeshProUGUI _TEXT_UnlockHero;

    [Header("Arrow")]
    [SerializeField] private List<GameObject> _ArrowDir = new List<GameObject>();
    #endregion

    #region Private Properties
    private CardUIData _data;

    private UnityAction _onComplete;
    #endregion

    #region Method
    public override void SetData(string itemID)
    {
        if (itemID != null)
        {
            ItemID = itemID;
        }
        else
        {
            Debug.LogWarning("Item ID is missing.");
        }

        CardData data = CardData.CreateCard(itemID);
        CardUIData cardUIData = new CardUIData(0, data, DataManager.Instance.DefaultCardBackIDList[0]);
        _data = cardUIData;

        IsOpened = false;
        SetName(_data.Name);
        SetAtk(_data.ATKCurrent, _data);
        SetHp(_data.HPCurrent, _data);
        SetSpirit(_data.SpiritCurrent);
        SetArmor(_data.ArmorCurrent, _data);
        ShowDirection(_data.CardDir);
        SetDescription(_data.Description);
        SetFrame(_data);
        SetImage(_data);
        SetGlow(_data);
        SetFoil();
        SetNew();
        SetNewHero();
        SetSeries(_data);

        if (_data.Type == CardType.Minion || _data.Type == CardType.Minion_NotInDeck)
        {
            _ICON_ATK.gameObject.SetActive(true);
            _ICON_HP.gameObject.SetActive(true);
            _TEXT_ATK.gameObject.SetActive(true);
            _TEXT_HP.gameObject.SetActive(true);
            _TEXT_SOUL.gameObject.SetActive(true);
        }
        else if (_data.Type == CardType.Spell || _data.Type == CardType.Spell_NotInDeck || _data.Type == CardType.Hero)
        {
            _ICON_ATK.gameObject.SetActive(false);
            _ICON_HP.gameObject.SetActive(false);
            _TEXT_ATK.gameObject.SetActive(false);
            _TEXT_HP.gameObject.SetActive(false);
            _TEXT_SOUL.gameObject.SetActive(false);
        }
       

    }

    public override CardRarity GetRarity()
    {
        return _data.Rarity;
    }

    private void SetNew()
    {
        _GROUP_NewNotice.gameObject.SetActive(PlayerSeenListManager.Instance.IsNewCard(_data.FullID));
    }

    private void SetNewHero()
    {
        _TEXT_UnlockHero.gameObject.SetActive(_data.FullID.Contains("_HERO"));
    }

    private void SetFoil()
    {
        _EFFECT_Foil.SetActive(_data.IsFoil);
    }

    private void SetName(string message)
    {
        TEXT_CardName.text = message;
    }

    private void SetArmor(int value, CardUIData data)
    {
        if (data.ArmorDefault <= 0)
        {
            _TEXT_ARMOR.gameObject.SetActive(false);
            _ICON_ARMOR.gameObject.SetActive(false);
        }
        else
        {
            _TEXT_ARMOR.gameObject.SetActive(true);
            _ICON_ARMOR.gameObject.SetActive(true);
        }

        _TEXT_ARMOR.text = value.ToString();

    }

    private void SetAtk(int value, CardUIData data)
    {
        _TEXT_ATK.text = value.ToString();
    }

    private void SetHp(int value, CardUIData data)
    {
        _TEXT_HP.text = value.ToString();
    }

    private void SetSpirit(int value)
    {
        _TEXT_SOUL.text = value.ToString();
    }

    private void SetDescription(string message)
    {
        TEXT_CardDetail.text = message;
        SetDynamicFont();
    }

    private void SetFrame(CardUIData data)
    {
        Sprite tempSprite;
        Sprite boxSprite;
        CardResourceManager.LoadCardFrame(data.Rarity, data.Element.ToCardElementType(), out tempSprite);
        CardResourceManager.LoadCardFrameBox(data.Rarity, out boxSprite);
        GROUP_FRAME.sprite = tempSprite;
        GROUP_FRAMEBOX.sprite = boxSprite;
    }

    private void ShowDirection(CardDirection dirValue)
    {
        foreach (CardDirectionType item in System.Enum.GetValues(typeof(CardDirectionType)))
        {
            bool isShow = dirValue.IsDirection(item);
            _ArrowDir[(int)item].SetActive(isShow);
        }
    }

    private void SetImage(CardUIData data)
    {
        if (GROUP_ART == null)
        {
            Debug.LogError("CollectionCardUI/SetImage: ArtworkGroup is null.");
            return;
        }

        // Clear previous image
        {
            foreach (Transform child in GROUP_ART.transform)
            {
                Destroy(child.gameObject);
            }
        }

        GameObject obj = null;
        if (CardResourceManager.Create(CardResourceManager.ImageType.InfoCard, data.ImageKey, GROUP_ART.transform, false, out obj))
        {
            obj.transform.localPosition = Vector3.zero;
            obj.transform.localRotation = Quaternion.identity;
        }
    }

    private void SetGlow(CardUIData data)
    {
        switch (data.Rarity)
        {
            case CardRarity.None:
            case CardRarity.Common:
            {
                break;
            }
            case CardRarity.Rare:
            {
                Animator.SetTrigger("Rare");
                break;
            }
            case CardRarity.Epic:
            {
                Animator.SetTrigger("Epic");
                break;
            }
            case CardRarity.Legend:
            {
                Animator.SetTrigger("Legend");
                break;
            }
        }
    }

    private void SetSeries(CardUIData data)
    {
        Sprite sprite;
        CardResourceManager.LoadSeriesIcon(data.SeriesKey, out sprite);

        _ICON_SERIES.sprite = sprite;
    }

    public CardUIData GetData()
    {
        return _data;
    }
    #endregion

}
