﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;

public class PackAnimation : MonoBehaviour
{
    public Animator Animator;

    private UnityAction _onComplete;

    #region Method
    public void ShowPack(bool isShow)
    {
        if (isShow)
        {
            gameObject.SetActive(true);
            gameObject.transform.localPosition = new Vector3(-50, 0);
            gameObject.transform.DOLocalMove(Vector3.zero, 0.75f).SetEase(Ease.OutExpo);
        }
        else
        {
            gameObject.transform.DOLocalMove(new Vector3(15, 0), 0.5f).SetEase(Ease.OutExpo).OnComplete(() => { gameObject.SetActive(false); } );
        }
    }

    public void PlayAnimation(CardRarity rarity)
    {
        switch (rarity)
        {
            case CardRarity.None:
            case CardRarity.Common:
            case CardRarity.Rare:
            {
                Animator.SetTrigger("Normal");
                break;
            }
            case CardRarity.Epic:
            {
                Animator.SetTrigger("Epic");
                break;
            }
            case CardRarity.Legend:
            {
                Animator.SetTrigger("Legend");
                break;
            }
            default:
            {
                break;
            }
        }
    }

    public void SetAnimationComplete(UnityAction onComplete)
    {
        _onComplete = onComplete;
    }

    public void OnComplete()
    {
        if (_onComplete != null)
        {
            _onComplete.Invoke();
        }
    }

    public void HidePack()
    {
        gameObject.SetActive(false);
    }
    #endregion

    #region Sound
    public void Play_OpenPack()
    {
        SoundManager.PlaySFX(SoundManager.SFX.Pack_Opening);
    }
    #endregion
}
