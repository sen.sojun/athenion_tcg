using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;
using Spine;
using UnityEngine.Events;

public class PackAnimationModule : MonoBehaviour
{
    #region Inspector
    [Header("Skeleton Graphic")]
    [SerializeField] private SkeletonGraphic PackSkeletonGraphic;

    [Header("Particle")]
    [SerializeField] private GameObject P_StarIdle;
    [SerializeField] private GameObject P_Glitter01;
    [SerializeField] private GameObject P_Glitter02;
    [SerializeField] private GameObject P_Glitter03;
    [SerializeField] private GameObject P_Glitter04;
    [SerializeField] private GameObject P_Glitter05;
    [SerializeField] private List<GameObject> P_CircleWhite;
    [SerializeField] private List<GameObject> P_CircleGold;
    [SerializeField] private GameObject P_GoldParticle;

    [Header("Rarity Flare")]
    [SerializeField] private GameObject P_Flare_Normal;
    [SerializeField] private GameObject P_Flare_Epic;
    [SerializeField] private GameObject P_Flare_Legend;
    #endregion

    #region Private Properties
    private Spine.AnimationState _animationState;

    //Anim
    private string _anim_idle = "Idle";
    private string _anim_normal = "Open_Normal";
    private string _anim_epic = "Open_Epic";
    private string _anim_legend = "Open_Legend";

    //Event
    private string _e_fadeIn = "Fade/Fade_In";
    private string _e_fadeOut = "Fade/Fade_Out";
    private string _e_giltter01 = "Glitter/Glitter01";
    private string _e_giltter02 = "Glitter/Glitter02";
    private string _e_giltter03 = "Glitter/Glitter03";
    private string _e_giltter04 = "Glitter/Glitter04";
    private string _e_giltter05 = "Glitter/Glitter05";
    private string _e_circleGold = "Circle/Circle_Gold01";
    private string _e_circleWhite = "Circle/Circle_White01";
    private string _e_goldParticle = "Particle/Particle_Gold";
    private string _e_flare_normal = "Flare/Flare_Normal";
    private string _e_flare_epic = "Flare/Flare_Epic";
    private string _e_flare_legend = "Flare/Flare_Legend";

    private bool _isInitAnimation = false;
    private UnityAction _onOpenPackComplete;
    #endregion

    public void InitData()
    {
        _animationState = PackSkeletonGraphic.AnimationState;
        HidePUI();
    }

    private void HidePUI()
    {
        P_StarIdle.SetActive(false);
        P_Glitter01.SetActive(false);
        P_Glitter02.SetActive(false);
        P_Glitter03.SetActive(false);
        P_Glitter04.SetActive(false);
        P_Glitter05.SetActive(false);
        P_GoldParticle.SetActive(false);
        P_Flare_Normal.SetActive(false);
        P_Flare_Epic.SetActive(false);
        P_Flare_Legend.SetActive(false);

        PoolActive(P_CircleWhite, false);
        PoolActive(P_CircleGold, false);
    }

    public void ShowPack()
    {
        Show();
    }

    #region Event Animation
    private void Show()
    {
        GameHelper.UITransition_FadeIn(this.gameObject);
        PlayAnimationIdle();
    }

    private void Hide()
    {
        GameHelper.UITransition_FadeOut(this.gameObject);
    }

    private void PlayAnimationIdle()
    {
        _animationState.SetAnimation(0, _anim_idle,true);
        P_StarIdleEnable(true);
    }

    private void P_StarIdleEnable(bool isEmissive)
    {
        P_StarIdle.SetActive(true);
        ParticleSystem.EmissionModule pe = P_StarIdle.GetComponent<ParticleSystem>().emission;
        pe.enabled = isEmissive;
    }

    public void PlayOpenPackAnimation(CardRarity rarity, UnityAction onComplete = null)
    {
        // Play SFX
        PlaySFX_PackOpen();
        P_StarIdleEnable(false);

        // Play anim
        switch (rarity)
        {
            case CardRarity.None:
            case CardRarity.Common:
            case CardRarity.Rare:
            {
                _animationState.SetAnimation(0, _anim_normal, false);
                break;
            }
            case CardRarity.Epic:
            {
                _animationState.SetAnimation(0, _anim_epic, false);
                break;
            }
            case CardRarity.Legend:
            {
                _animationState.SetAnimation(0, _anim_legend, false);
                break;
            }
            default:
            {
                break;
            }
        }

        _onOpenPackComplete = onComplete;
        InitEvent();

    }

    private void EventHandle(TrackEntry trackEntry, Spine.Event e)
    {
        TriggerEvent(_e_fadeIn, e, Event_FadeIn);
        // Particle
        TriggerEvent(_e_giltter01, e, Event_Glitter01);
        TriggerEvent(_e_giltter02, e, Event_Glitter02);
        TriggerEvent(_e_giltter03, e, Event_Glitter03);
        TriggerEvent(_e_giltter04, e, Event_Glitter04);
        TriggerEvent(_e_giltter05, e, Event_Glitter05);
        TriggerEvent(_e_circleWhite, e, Event_CircleWhite);
        TriggerEvent(_e_circleGold, e, Event_CircleGold);
        TriggerEvent(_e_goldParticle, e, Event_GoldParticle);
        // Flare
        TriggerEvent(_e_flare_normal, e, Event_Flare_Normal);
        TriggerEvent(_e_flare_epic, e, Event_Flare_Epic);
        TriggerEvent(_e_flare_legend, e, Event_Flare_Legend);

    }

    private void TriggerEvent(string eventName, Spine.Event e, UnityAction callback)
    {
        if (e.Data.Name == eventName)
        {
            callback?.Invoke();
        }
    }
    #endregion

    #region Effect
    private void PlaySFX_PackOpen()
    {
        SoundManager.PlaySFX(SoundManager.SFX.Pack_Opening);
    }
    #endregion

    private void InitEvent()
    {
        if (!_isInitAnimation)
        {
            _animationState.Event += EventHandle;
            _animationState.Complete += CompleteHandle;
            _isInitAnimation = true;
        }
    }

    private void DeInitEvent()
    {
        if (_isInitAnimation)
        {
            _animationState.Event -= EventHandle;
            _animationState.Complete -= CompleteHandle;
            _isInitAnimation = false;
        }
    }

    private void CompleteHandle(TrackEntry trackEntry)
    {
        _onOpenPackComplete.Invoke();
        Hide();
        DeInitEvent();
    }

    #region Event
    private void Event_FadeIn()
    {
        CardPackController.Instance.FadeWhite();
        CardPackController.Instance.BG_Open();
    }

    private void Event_Glitter01()
    {
        Debug.Log("------Glitter01");
        P_Glitter01.SetActive(true);
    }

    private void Event_Glitter02()
    {
        P_Glitter02.SetActive(true);
    }

    private void Event_Glitter03()
    {
        P_Glitter03.SetActive(true);
    }

    private void Event_Glitter04()
    {
        P_Glitter04.SetActive(true);
    }

    private void Event_Glitter05()
    {
        P_Glitter05.SetActive(true);
    }

    private void Event_CircleWhite()
    {
        PoolListActive(P_CircleWhite);
    }

    private void Event_CircleGold()
    {
        PoolListActive(P_CircleGold);
    }

    private void Event_GoldParticle()
    {
        P_GoldParticle.SetActive(true);
    }

    private void Event_Flare_Normal()
    {
        P_Flare_Normal.SetActive(true);
    }

    private void Event_Flare_Epic()
    {
        P_Flare_Epic.SetActive(true);
    }

    private void Event_Flare_Legend()
    {
        P_Flare_Legend.SetActive(true);
    }
    #endregion

    #region Pool
    private void PoolListActive(List<GameObject> objList)
    {
        foreach (GameObject item in objList)
        {
            if (item.activeSelf)
            {
                continue;
            }
            else
            {
                item.SetActive(true);
                break;
            }
        }
    }

    private void PoolActive(List<GameObject> objList,bool active)
    {
        foreach (GameObject item in objList)
        {
            item.SetActive(active);
        }
    }
    #endregion


}
