using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.U2D;
using DG.Tweening;
using TMPro;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using Karamucho.UI;

public class CardPackPrefab_item : BaseCardPackPrefab
{
    #region Public Properties
    #endregion

    #region Private Properties
    private CurrencyReward _data;

    private UnityAction _onComplete;
    #endregion

    #region Method
    public override void SetData(string bundleID)
    {  

        _data = DataManager.Instance.GetStarItemDB().GetStarItem(bundleID);
        string name = LocalizationManager.Instance.GetItemName(_data.ItemID);
        string detail = _data.Amount.ToString();

        IsOpened = false;
        SetName(name);
        SetDescription(detail + " " + name);
        SetFrame();
        SetImage();
        SetGlow();

    }

    public override CardRarity GetRarity()
    {
        return _data.Rarity;
    }

    public CurrencyReward GetData()
    {
        return _data;
    }

    private void SetName(string message)
    {
        TEXT_CardName.text = message;
    }

    private void SetDescription(string message)
    {
        TEXT_CardDetail.text = message;
        SetDynamicFont();
    }

    private void SetFrame()
    {
        Sprite tempSprite;
        Sprite boxSprite;
        CardResourceManager.LoadItemFrame(_data.Rarity, out tempSprite);
        CardResourceManager.LoadCardFrameBox(_data.Rarity, out boxSprite);
        GROUP_FRAME.sprite = tempSprite;
        GROUP_FRAMEBOX.sprite = boxSprite;

    }

    private void SetImage()
    {
        if (GROUP_ART == null)
        {
            Debug.LogError("CollectionCardUI/SetImage: ArtworkGroup is null.");
            return;
        }

        // Clear previous image
        {
            foreach (Transform child in GROUP_ART.transform)
            {
                Destroy(child.gameObject);
            }
        }

        GameObject obj = null;
        if (CardResourceManager.Create(CardResourceManager.ImageType.InfoItem, _data.BundleID, GROUP_ART.transform, false, out obj))
        {
            obj.transform.localPosition = Vector3.zero;
            obj.transform.localRotation = Quaternion.identity;
        }
    }

    private void SetGlow()
    {
        switch (_data.Rarity)
        {
            case CardRarity.None:
            case CardRarity.Common:
            {
                break;
            }
            case CardRarity.Rare:
            {
                Animator.SetTrigger("Rare");
                break;
            }
            case CardRarity.Epic:
            {
                Animator.SetTrigger("Epic");
                break;
            }
            case CardRarity.Legend:
            {
                Animator.SetTrigger("Legend");
                break;
            }
        }
    }


    #endregion

}
