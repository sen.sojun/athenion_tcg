﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.U2D;
using DG.Tweening;
using TMPro;
using Karamucho.UI;

public class UIDetailCardIndy : MonoBehaviour
{
    #region Public Properties
    public GameObject Group;

    [Header("Reference")]
    public TextMeshProUGUI CardName;
    public TextMeshProUGUI Detail;
    public TextMeshProUGUI Attack;
    public TextMeshProUGUI Hp;
    public TextMeshProUGUI Spirit;
    public TextMeshProUGUI Armor;
    public Image SeriesIcon;
    public Image AttackImage;
    public Image HpImage;
    public Image ArmorImage;
    public Image Frame;
    public RectTransform Artwork;
    public GameObject Effect_Foil;

    [Header("Tooltip Position")]
    public UITooltipKeywordGroup TooltipGroup;
    public float XRightOffset;
    public float XLeftOffset;

    [Header("Arrow")]
    public List<GameObject> ArrowDir = new List<GameObject>();
    #endregion

    #region Private Properties
    private CardUIData _data;
    private bool _tooltipIsShow = false;
    #endregion

    #region Calling Function
    public void ShowCard(string baseID)
    {
        CardData data = CardData.CreateCard(baseID);
        CardUIData uidata = new CardUIData(0, data, DataManager.Instance.DefaultCardBackIDList[0]);
        SetData(uidata);
        gameObject.SetActive(true);
    }

    public void HideCard()
    {
        gameObject.SetActive(false);
    }
    #endregion

    #region Data
    public void SetData(CardUIData data, bool isInBattle = true)
    {
        if (_data != null)
        {
            //if (_data.UniqueID != data.UniqueID)
            {
                _data = data;
            }
        }
        else
        {
            _data = data;
        }

        SetName(_data.Name);
        SetAtk(_data.ATKCurrent, _data);
        SetHp(_data.HPCurrent, _data);
        SetSpirit(_data.SpiritCurrent, _data);
        SetArmor(_data.ArmorCurrent, _data);
        ShowDirection(_data.CardDir);
        SetDescription(_data.Description);
        SetFrame(_data);
        SetImage(_data);
        SetPackIcon(_data);
        SetFoil();

        if (_data.Type == CardType.Minion || _data.Type == CardType.Minion_NotInDeck)
        {
            AttackImage.gameObject.SetActive(true);
            HpImage.gameObject.SetActive(true);
            Attack.gameObject.SetActive(true);
            Hp.gameObject.SetActive(true);
            Spirit.gameObject.SetActive(true);
        }
        else if (_data.Type == CardType.Spell || _data.Type == CardType.Spell_NotInDeck || _data.Type == CardType.Hero)
        {
            AttackImage.gameObject.SetActive(false);
            HpImage.gameObject.SetActive(false);
            Attack.gameObject.SetActive(false);
            Hp.gameObject.SetActive(false);
            Spirit.gameObject.SetActive(false);
        }
    }

    private void SetFoil()
    {
        Effect_Foil.SetActive(_data.IsFoil);
    }

    private void SetTooltip(CardUIData data)
    {
        if (data != null)
        {
            TooltipGroup.SetData(data);
            if (!_tooltipIsShow)
            {
                TooltipGroup.ShowTooltip();
                _tooltipIsShow = true;
            }
        }
    }

    public void SetArmor(int value, CardUIData data)
    {
        if (data.ArmorDefault <= 0)
        {
            Armor.gameObject.SetActive(false);
            ArmorImage.gameObject.SetActive(false);
        }
        else
        {
            Armor.gameObject.SetActive(true);
            ArmorImage.gameObject.SetActive(true);
        }

        Color color = new Color();
        if (value < data.ArmorBase)
        {
            color = Color.yellow;
        }
        else
        {
            if (data.ArmorBase > data.ArmorDefault)
            {
                color = Color.green;
            }
            else
            {
                color = Color.white;
            }
        }

        Armor.text = value.ToString();
        Armor.color = color;
    }

    public void SetName(string message)
    {
        CardName.text = message;
    }

    public void SetAtk(int value, CardUIData data)
    {
        Color color = new Color();
        if (value < data.ATKBase)
        {
            color = Color.red;
        }
        else
        {
            if (data.ATKBase > data.ATKDefault)
            {
                color = Color.green;
            }
            else
            {
                color = Color.white;
            }
        }

        Attack.text = value.ToString();
        Attack.color = color;
    }

    public void SetHp(int value, CardUIData data)
    {
        Color color = new Color();
        if (value < data.HPBase)
        {
            color = Color.red;
        }
        else
        {
            if (data.HPBase > data.HPDefault)
            {
                color = Color.green;
            }
            else
            {
                color = Color.white;
            }
        }

        Hp.text = value.ToString();
        Hp.color = color;
    }

    public void SetSpirit(int value, CardUIData data)
    {
        Color color = new Color();
        if (value < data.SpiritBase)
        {
            color = Color.red;
        }
        else
        {
            if (data.SpiritBase > data.SpiritDefault)
            {
                color = Color.green;
            }
            else
            {
                color = Color.white;
            }
        }

        Spirit.text = value.ToString();
        Spirit.color = color;
        //Spirit.text = GameHelper.GetIntToRomanNumber(value).ToString();
    }

    public void SetDescription(string message)
    {
        Detail.text = message;
    }

    private void SetFrame(CardUIData data)
    {
        if (data.Type == CardType.Minion || data.Type == CardType.Minion_NotInDeck || data.Type == CardType.Spell)
        {
            Sprite tempSprite;
            CardResourceManager.LoadCardFrame(data.Rarity, data.Element.ToCardElementType(), out tempSprite);
            Frame.sprite = tempSprite;
        }
    }

    public void ShowDirection(CardDirection dirValue)
    {
        foreach (CardDirectionType item in System.Enum.GetValues(typeof(CardDirectionType)))
        {
            bool isShow = dirValue.IsDirection(item);
            ArrowDir[(int)item].SetActive(isShow);
        }
    }

    private void SetImage(CardUIData data)
    {
        // Clear previous image
        {
            foreach (Transform child in Artwork.transform)
            {
                Destroy(child.gameObject);
            }
        }

        GameObject obj = null;
        if (CardResourceManager.Create(CardResourceManager.ImageType.InfoCard, data.ImageKey, Artwork.transform, false, out obj))
        {
            obj.transform.localPosition = Vector3.zero;
            obj.transform.localRotation = Quaternion.identity;
            SetMask(data);
        }
    }

    private void SetMask(CardUIData data)
    {
        foreach (Transform child in Artwork.transform)
        {
            if (data.Type == CardType.Minion || data.Type == CardType.Minion_NotInDeck || data.Type == CardType.Spell)
            {
                Sprite tempSprite;
                CardResourceManager.LoadCardMaskFrame(data.Rarity, data.Element.ToCardElementType(), out tempSprite);
                child.GetComponent<Image>().sprite = tempSprite;
            }
        }
    }

    private void SetPackIcon(CardUIData data)
    {
        Sprite sprite;
        CardResourceManager.LoadSeriesIcon(data.SeriesKey, out sprite);

        SeriesIcon.sprite = sprite;
    }

    #endregion



}
