﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using DG.Tweening;
using TMPro;
using UnityEngine.EventSystems;
using Karamucho.UI;

public class BaseCardUI : CollectionItem
{
    #region Inspector Properties
    public CardUIData Data { get { return _data; } }

    [Header("Reference")]
    [SerializeField] protected GameObject _GROUP_MAIN;
    [SerializeField] protected GameObject _GROUP_ART;

    [Header("Card Stat TEXT")]
    [SerializeField] protected TextMeshProUGUI _TEXT_CardName;
    [SerializeField] protected TextMeshProUGUI _TEXT_Description;
    [SerializeField] protected TextMeshProUGUI _TEXT_Attack;
    [SerializeField] protected TextMeshProUGUI _TEXT_HP;
    [SerializeField] protected TextMeshProUGUI _TEXT_Soul;
    [SerializeField] protected TextMeshProUGUI _TEXT_Armor;

    [Header("Icon")]
    [SerializeField] protected Image _ICON_Attack;
    [SerializeField] protected Image _ICON_Hp;
    [SerializeField] protected Image _ICON_Armor;
    [SerializeField] protected Image _ICON_Series;

    [Header("Frame")]
    [SerializeField] protected Image _IMG_FRAME_Card;
    [SerializeField] protected Image _IMG_FRAME_Description;

    [Header("Effect")]
    [SerializeField] protected GameObject _EFFECT_Foil;

    [Header("Arrow")]
    public List<GameObject> ArrowDir = new List<GameObject>();
    #endregion

    #region Protected Properties
    protected CardUIData _data;
    #endregion

    #region Private Properties
    private static float _DEFAULT_CARD_WIDTH = 804.0f;
    private static float _DEFAULT_FONT_SIZE = 36.0f;
    private static float _DEFAULT_FRAMEBOX_MIN_HEIGHT = 350.0f;
    private static Vector4 _DEFAULT_FONT_MARGIN = new Vector4(70, 50, 70, 110);
    private float _ratioDeference = 1.0f;
    #endregion

    #region Public Methods
    public virtual void SetData(CardUIData data)
    {
        #region Check Data
        _data = data;
        #endregion

        // Game Stat
        SetName(_data.Name);
        SetAtk(_data.ATKCurrent, _data);
        SetHp(_data.HPCurrent, _data);
        SetSoul(_data.SpiritCurrent);
        SetArmor(_data.ArmorCurrent, _data);
        ShowDirection(_data.CardDir);
        SetPackIcon(_data);

        // Card
        SetDescription(_data.Description);
        SetFrame(_data);
        SetImage(_data);
        SetFoil(_data);

    }
    #endregion

    #region Private Methods

    #region GAME STAT
    protected virtual void SetName(string message)
    {
        _TEXT_CardName.text = message;
    }

    protected virtual void SetSoul(int value)
    {
        _TEXT_Soul.text = value.ToString();
    }

    protected virtual void SetAtk(int value, CardUIData data)
    {
        Color color = new Color();
        if (value < data.ATKBase)
        {
            color = GameHelper.GetColor_Number_Decrease();
        }
        else
        {
            if (data.ATKBase > data.ATKDefault)
            {
                color = GameHelper.GetColor_Number_Increase();
            }
            else
            {
                color = GameHelper.GetColor_Number_Normal();
            }
        }
        _TEXT_Attack.text = value.ToString();
        _TEXT_Attack.color = color;
    }

    protected virtual void SetHp(int value, CardUIData data)
    {
        Color color = new Color();
        if (value < data.HPBase)
        {
            color = GameHelper.GetColor_Number_Decrease();
        }
        else
        {
            if (data.HPBase > data.HPDefault)
            {
                color = GameHelper.GetColor_Number_Increase();
            }
            else
            {
                color = GameHelper.GetColor_Number_Normal();
            }
        }
        _TEXT_HP.text = value.ToString();
        _TEXT_HP.color = color;
    }

    protected virtual void SetArmor(int value, CardUIData data)
    {
        if (value <= 0)
        {
            _TEXT_Armor.gameObject.SetActive(false);
            _ICON_Armor.gameObject.SetActive(false);
        }
        else
        {
            _TEXT_Armor.gameObject.SetActive(true);
            _ICON_Armor.gameObject.SetActive(true);
        }

        Color color = new Color();
        if (value < data.ArmorBase)
        {
            color = GameHelper.GetColor_Number_Decrease();
        }
        else
        {
            if (data.ArmorBase > data.ArmorDefault)
            {
                color = GameHelper.GetColor_Number_Increase();
            }
            else
            {
                color = GameHelper.GetColor_Number_Normal();
            }
        }
        _TEXT_Armor.text = value.ToString();
        _TEXT_Armor.color = color;
    }

    protected virtual void SetDescription(string message)
    {
        if (string.IsNullOrEmpty(message) || string.IsNullOrWhiteSpace(message))
        {
            _TEXT_Description.text = "\n\n\n";
        }
        else
        {
            _TEXT_Description.text = message;
        }

        SetDynamicFont();
    }

    private void ShowDirection(CardDirection dirValue)
    {
        foreach (CardDirectionType item in System.Enum.GetValues(typeof(CardDirectionType)))
        {
            bool isShow = dirValue.IsDirection(item);
            ArrowDir[(int)item].SetActive(isShow);
        }
    }

    private void SetPackIcon(CardUIData data)
    {
        Sprite sprite;
        CardResourceManager.LoadSeriesIcon(data.SeriesKey, out sprite);

        _ICON_Series.sprite = sprite;
    }
    #endregion

    #region CARD
    private void SetFrame(CardUIData data)
    {
        Sprite tempSprite;
        Sprite boxSprite;
        CardResourceManager.LoadCardFrame(data.Rarity, data.Element.ToCardElementType(), out tempSprite);
        CardResourceManager.LoadCardFrameBox(data.Rarity, out boxSprite);
        _IMG_FRAME_Card.sprite = tempSprite;
        _IMG_FRAME_Description.sprite = boxSprite;
    }

    private void SetImage(CardUIData data)
    {
        if (_GROUP_ART == null)
        {
            Debug.LogError("CollectionCardUI/SetImage: ArtworkGroup is null.");
            return;
        }

        // Clear previous image
        {
            foreach (Transform child in _GROUP_ART.transform)
            {
                Destroy(child.gameObject);
            }
        }

        CardResourceManager.CreateAsync(
            CardResourceManager.ImageType.InfoCard
            , data.ImageKey
            , _GROUP_ART.transform
            , false
            , delegate (GameObject obj)
            {
                if (obj != null)
                {
                    obj.transform.localPosition = Vector3.zero;
                    obj.transform.localRotation = Quaternion.identity;
                }
                SetMask(data);
            }
        );
    }

    private void SetMask(CardUIData data)
    {
        foreach (Transform child in _GROUP_ART.transform)
        {
            Sprite tempSprite;
            CardResourceManager.LoadCardMaskFrame(data.Rarity, data.Element.ToCardElementType(), out tempSprite);
            child.GetComponent<Image>().sprite = tempSprite;
        }
    }

    private void SetFoil(CardUIData data)
    {
        _EFFECT_Foil.SetActive(data.IsFoil);
    }

    #endregion

    #region Utility
    protected void SetDynamicFont()
    {
        if (transform.parent.GetComponent<GridLayoutGroup>())
        {
            // Get cell size
            float cellSize = transform.parent.GetComponent<GridLayoutGroup>().cellSize.x;
            _ratioDeference = (float)(cellSize / _DEFAULT_CARD_WIDTH);
        }
        else
        {
            // Get cell size
            float cellSize = GetComponent<RectTransform>().sizeDelta.x;
            _ratioDeference = (float)(cellSize / _DEFAULT_CARD_WIDTH);
        }

        // Calculate Dynamic Margin
        _TEXT_Description.fontSize = _DEFAULT_FONT_SIZE * _ratioDeference;
        _TEXT_Description.margin = new Vector4(
            _DEFAULT_FONT_MARGIN.x * _ratioDeference,
            _DEFAULT_FONT_MARGIN.y * _ratioDeference,
            _DEFAULT_FONT_MARGIN.z * _ratioDeference,
            _DEFAULT_FONT_MARGIN.w * _ratioDeference
            );
        _TEXT_Description.GetComponent<LayoutElement>().minHeight = _DEFAULT_FRAMEBOX_MIN_HEIGHT * _ratioDeference;

    }

    public float GetDescriptionHeight()
    {
        return _IMG_FRAME_Description.rectTransform.rect.height;
    }
    #endregion

    #region Static Get Methods
    public static float Get_DEFAULT_CARD_WIDTH()
    {
        return _DEFAULT_CARD_WIDTH;
    }

    public static float Get_DEFAULT_FONT_SIZE()
    {
        return _DEFAULT_FONT_SIZE;
    }

    public static float Get_DEFAULT_FRAMEBOX_MIN_HEIGHT()
    {
        return _DEFAULT_FRAMEBOX_MIN_HEIGHT;
    }

    public static Vector4 Get_DEFAULT_FONT_MARGIN()
    {
        return _DEFAULT_FONT_MARGIN;
    }
    #endregion

    #endregion
}

