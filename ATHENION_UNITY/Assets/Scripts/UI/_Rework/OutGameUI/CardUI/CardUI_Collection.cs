﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using DG.Tweening;
using TMPro;
using UnityEngine.EventSystems;
using Karamucho.UI;

namespace Karamucho.Collection
{
    public class CardUI_Collection : BaseCardUI
    {
        #region Inspector
        [Header("Card Shadow")]
        [SerializeField] private Image Shadow;
        [SerializeField] private GameObject NewCard;

        [Header("Card Count")]
        [SerializeField] private GameObject EFFECT_GreyMask;
        [SerializeField] private GameObject GROUP_CardAmount;
        [SerializeField] private TextMeshProUGUI TEXT_CardAmount;
        #endregion

        #region Private Properties
        private int _cardAmount = 0;

        private PointerEventDataEvent _onUnityBeginDrag = null;
        private PointerEventDataEvent _onUnityDrag = null;
        private PointerEventDataEvent _onUnityEndDrag = null;
        #endregion

        #region Methods
        public void SetCardAmount(int value, bool isMaxCard = false)
        {
            _cardAmount = value;
            // Set Text
            TEXT_CardAmount.text = "x" + _cardAmount.ToString();

            // Update card
            if (_cardAmount <= 0)
            {
                GROUP_CardAmount.SetActive(false);
                SetOwned(false);
            }
            else
            {
                GROUP_CardAmount.SetActive(_cardAmount > 1);
                SetOwned(!isMaxCard);
            }

        }

        public void ShowShadow(bool isShow)
        {
            Shadow.gameObject.SetActive(isShow);
        }

        public void SetOwned(bool isShow)
        {
            EFFECT_GreyMask.SetActive(!isShow);
        }

        public void SetNew(bool isNew)
        {
            NewCard.gameObject.SetActive(isNew);
        }
        #endregion

        #region Events
        public event UnityAction<CardUI_Collection> OnCardClick;
        public event UnityAction<CardUI_Collection> OnCardDoubleClick;
        public event Action<CardUI_Collection> OnCardBeginDragEvent;
        public event Action<CardUI_Collection> OnCardEndDragEvent;
        public event Action<CardUI_Collection> OnCardBeginHoldEvent;
        public event Action<CardUI_Collection> OnCardEndHoldEvent;
        #endregion

        #region Input Event
        public void AddListenerOnUnityBeginDrag(UnityAction<PointerEventData> callback)
        {
            if (_onUnityBeginDrag == null) _onUnityBeginDrag = new PointerEventDataEvent();

            _onUnityBeginDrag.AddListener(callback);
        }

        public void AddListenerOnUnityDrag(UnityAction<PointerEventData> callback)
        {
            if (_onUnityDrag == null) _onUnityDrag = new PointerEventDataEvent();

            _onUnityDrag.AddListener(callback);
        }

        public void AddListenerOnUnityEndDrag(UnityAction<PointerEventData> callback)
        {
            if (_onUnityEndDrag == null) _onUnityEndDrag = new PointerEventDataEvent();

            _onUnityEndDrag.AddListener(callback);
        }

        public override void OnEventUnityPointerDown(PointerEventData eventData)
        {
            base.OnEventUnityPointerDown(eventData);

            //if (IsHaveObject(typeof(CollectionManager_OLD)))
            //{
            //    if (CollectionManager_OLD.Instance.UI_IsCraftingShow) return;

            //    ShowShadow(true);
            //}
            //else if (IsHaveObject(typeof(StarChamberManager)))
            //{
            //    if (StarChamberManager.Instance.UI_IsCraftingShow) return;

            //    ShowShadow(true);
            //}
            //else if (IsHaveObject(typeof(EventBattleManager)))
            //{
            //    if (EventBattleManager.Instance.Is_UIDetailCardShow) return;

            //    ShowShadow(true);
            //}
            //else
            //{
            //    //ShowShadow(true);
            //}
        }

        public override void OnEventUnityPointerUp(PointerEventData eventData)
        {
            base.OnEventUnityPointerUp(eventData);

            //if (IsHaveObject(typeof(CollectionManager_OLD)))
            //{
            //    if (CollectionManager_OLD.Instance.UI_IsCraftingShow) return;

            //    ShowShadow(false);
            //}
            //else if (IsHaveObject(typeof(StarChamberManager)))
            //{
            //    if (StarChamberManager.Instance.UI_IsCraftingShow) return;

            //    ShowShadow(false);
            //}
            //else if (IsHaveObject(typeof(EventBattleManager)))
            //{
            //    if (EventBattleManager.Instance.Is_UIDetailCardShow) return;

            //    ShowShadow(false);
            //}
            //else
            //{
            //    //ShowShadow(false);
            //}
        }

        public override void OnEventUnityBeginDrag(PointerEventData eventData)
        {
            base.OnEventUnityBeginDrag(eventData);

            if (_onUnityBeginDrag != null)
            {
                _onUnityBeginDrag.Invoke(eventData);
            }
        }

        public override void OnEventUnityDrag(PointerEventData eventData)
        {
            base.OnEventUnityDrag(eventData);

            if (_onUnityDrag != null)
            {
                _onUnityDrag.Invoke(eventData);
            }
        }

        public override void OnEventUnityEndDrag(PointerEventData eventData)
        {
            base.OnEventUnityEndDrag(eventData);

            if (_onUnityEndDrag != null)
            {
                _onUnityEndDrag.Invoke(eventData);
            }
        }

        public override void OnEventClick(PointerEventData eventData)
        {
            base.OnEventClick(eventData);
            OnCardClick?.Invoke(this);
        }

        public override void OnEventDoubleClick(PointerEventData eventData)
        {
            base.OnEventDoubleClick(eventData);
            OnCardDoubleClick?.Invoke(this);
        }

        public override void OnEventBeginDrag(PointerEventData eventData)
        {
            base.OnEventBeginDrag(eventData);
            OnCardBeginDragEvent?.Invoke(this);
        }

        public override void OnEventDrag(PointerEventData eventData)
        {
            base.OnEventDrag(eventData);
        }

        public override void OnEventEndDrag(PointerEventData eventData)
        {
            base.OnEventEndDrag(eventData);
            OnCardEndDragEvent?.Invoke(this);
        }

        public override void OnEventBeginHold(PointerEventData eventData)
        {
            base.OnEventBeginHold(eventData);
            OnCardBeginHoldEvent?.Invoke(this);
        }

        public override void OnEventEndHold(PointerEventData eventData)
        {
            base.OnEventEndHold(eventData);
            OnCardEndHoldEvent?.Invoke(this);
        }
        #endregion

        #region Get
        public int GetCardAmount()
        {
            return _cardAmount;
        }
        #endregion
    }
}

