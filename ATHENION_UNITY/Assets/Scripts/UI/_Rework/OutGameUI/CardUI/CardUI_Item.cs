﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Karamucho.UI;
using UnityEngine.UI;

public class CardUI_Item : BaseCardUI
{
    #region Private Properties
    private CurrencyReward _rewardData;
    #endregion

    public void SetData(CurrencyReward data)
    {
        _rewardData = data;
        SetName();
        SetDescription();
        SetFrame();
        SetImage();
    }

    #region Data

    public void SetName()
    {
        string head = LocalizationManager.Instance.GetItemName(_rewardData.ItemID);
        _TEXT_CardName.text = head;
    }

    public void SetDescription()
    {
        string head = LocalizationManager.Instance.GetItemName(_rewardData.ItemID);
        string message = _rewardData.Amount.ToString() + " " + head;
        _TEXT_Description.text = message;
        SetDynamicFont();
    }

    private void SetFrame()
    {
        Sprite tempSprite;
        Sprite boxSprite;
        CardResourceManager.LoadItemFrame(_rewardData.Rarity, out tempSprite);
        CardResourceManager.LoadCardFrameBox(_rewardData.Rarity, out boxSprite);
        _IMG_FRAME_Card.sprite = tempSprite;
        _IMG_FRAME_Description.sprite = boxSprite;
    }

    private void SetImage()
    {
        // Clear previous image
        {
            foreach (Transform child in _GROUP_ART.transform)
            {
                Destroy(child.gameObject);
            }
        }

        GameObject obj = null;
        if (CardResourceManager.Create(CardResourceManager.ImageType.InfoItem, _rewardData.BundleID, _GROUP_ART.transform, false, out obj))
        {
            obj.transform.localPosition = Vector3.zero;
            obj.transform.localRotation = Quaternion.identity;
            SetMask();
        }
    }

    private void SetMask()
    {
        foreach (Transform child in _GROUP_ART.transform)
        {
            Sprite tempSprite;
            CardResourceManager.LoadItemMaskFrame(_rewardData.Rarity, out tempSprite);
            child.GetComponent<Image>().sprite = tempSprite;
        }
    }

    #endregion

}
