﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using DG.Tweening;
using TMPro;
using UnityEngine.EventSystems;
using Karamucho.UI;

public class CardUI_SetCollection : BaseCardUI
{
    #region Inspectors
    [Header("Owned")]
    [SerializeField] private GameObject EFFECT_GreyMask;
    #endregion

    #region Private Properties
    private PointerEventDataEvent _onUnityBeginDrag = null;
    private PointerEventDataEvent _onUnityDrag = null;
    private PointerEventDataEvent _onUnityEndDrag = null;
    #endregion

    #region Methods
    public void SetOwned(bool isShow)
    {
        EFFECT_GreyMask.SetActive(!isShow);
    }

    protected override void SetArmor(int value, CardUIData data)
    {
        _TEXT_Armor.gameObject.SetActive(false);
        _ICON_Armor.gameObject.SetActive(false);
    }

    protected override void SetDescription(string message)
    {
        _TEXT_Description.text = "\n\n\n";
        SetDynamicFont();
    }
    #endregion

    #region Events
    public event UnityAction<CardUI_SetCollection> OnCardClick;
    public event UnityAction<CardUI_SetCollection> OnCardDoubleClick;
    public event Action<CardUI_SetCollection> OnCardBeginDragEvent;
    public event Action<CardUI_SetCollection> OnCardEndDragEvent;
    #endregion

    #region Input Event
    public void AddListenerOnUnityBeginDrag(UnityAction<PointerEventData> callback)
    {
        if (_onUnityBeginDrag == null) _onUnityBeginDrag = new PointerEventDataEvent();

        _onUnityBeginDrag.AddListener(callback);
    }

    public void AddListenerOnUnityDrag(UnityAction<PointerEventData> callback)
    {
        if (_onUnityDrag == null) _onUnityDrag = new PointerEventDataEvent();

        _onUnityDrag.AddListener(callback);
    }

    public void AddListenerOnUnityEndDrag(UnityAction<PointerEventData> callback)
    {
        if (_onUnityEndDrag == null) _onUnityEndDrag = new PointerEventDataEvent();

        _onUnityEndDrag.AddListener(callback);
    }

    public override void OnEventUnityPointerDown(PointerEventData eventData)
    {
        base.OnEventUnityPointerDown(eventData);

        //if (IsHaveObject(typeof(CollectionManager_OLD)))
        //{
        //    if (CollectionManager_OLD.Instance.UI_IsCraftingShow) return;

        //    ShowShadow(true);
        //}
        //else if (IsHaveObject(typeof(StarChamberManager)))
        //{
        //    if (StarChamberManager.Instance.UI_IsCraftingShow) return;

        //    ShowShadow(true);
        //}
        //else if (IsHaveObject(typeof(EventBattleManager)))
        //{
        //    if (EventBattleManager.Instance.Is_UIDetailCardShow) return;

        //    ShowShadow(true);
        //}
        //else
        //{
        //    //ShowShadow(true);
        //}
    }

    public override void OnEventUnityPointerUp(PointerEventData eventData)
    {
        base.OnEventUnityPointerUp(eventData);

        //if (IsHaveObject(typeof(CollectionManager_OLD)))
        //{
        //    if (CollectionManager_OLD.Instance.UI_IsCraftingShow) return;

        //    ShowShadow(false);
        //}
        //else if (IsHaveObject(typeof(StarChamberManager)))
        //{
        //    if (StarChamberManager.Instance.UI_IsCraftingShow) return;

        //    ShowShadow(false);
        //}
        //else if (IsHaveObject(typeof(EventBattleManager)))
        //{
        //    if (EventBattleManager.Instance.Is_UIDetailCardShow) return;

        //    ShowShadow(false);
        //}
        //else
        //{
        //    //ShowShadow(false);
        //}
    }

    public override void OnEventUnityBeginDrag(PointerEventData eventData)
    {
        base.OnEventUnityBeginDrag(eventData);

        if (_onUnityBeginDrag != null)
        {
            _onUnityBeginDrag.Invoke(eventData);
        }
    }

    public override void OnEventUnityDrag(PointerEventData eventData)
    {
        base.OnEventUnityDrag(eventData);

        if (_onUnityDrag != null)
        {
            _onUnityDrag.Invoke(eventData);
        }
    }

    public override void OnEventUnityEndDrag(PointerEventData eventData)
    {
        base.OnEventUnityEndDrag(eventData);

        if (_onUnityEndDrag != null)
        {
            _onUnityEndDrag.Invoke(eventData);
        }
    }

    public override void OnEventClick(PointerEventData eventData)
    {
        base.OnEventClick(eventData);
        OnCardClick?.Invoke(this);
    }

    public override void OnEventDoubleClick(PointerEventData eventData)
    {
        base.OnEventDoubleClick(eventData);
        OnCardDoubleClick?.Invoke(this);
    }

    public override void OnEventBeginDrag(PointerEventData eventData)
    {
        base.OnEventBeginDrag(eventData);
        OnCardBeginDragEvent?.Invoke(this);
    }

    public override void OnEventDrag(PointerEventData eventData)
    {
        base.OnEventDrag(eventData);
    }

    public override void OnEventEndDrag(PointerEventData eventData)
    {
        base.OnEventEndDrag(eventData);
        OnCardEndDragEvent?.Invoke(this);
    }

    public override void OnEventBeginHold(PointerEventData eventData)
    {
        base.OnEventBeginHold(eventData);

        //if (IsHaveObject(typeof(CollectionManager_OLD)))
        //{
        //    if (CollectionManager_OLD.Instance.UI_IsCraftingShow) return;

        //    //_isCardSnap = true;
        //    CollectionManager_OLD.Instance.OnCard_StartHold(this);
        //}
    }

    public override void OnEventEndHold(PointerEventData eventData)
    {
        base.OnEventEndHold(eventData);

        //if (IsHaveObject(typeof(CollectionManager_OLD)))
        //{
        //    if (CollectionManager_OLD.Instance.UI_IsCraftingShow) return;

        //    CollectionManager_OLD.Instance.OnCard_EndHold(this);
        //}
    }
    #endregion
}
