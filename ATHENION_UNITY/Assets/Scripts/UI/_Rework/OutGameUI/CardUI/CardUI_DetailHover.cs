﻿using DG.Tweening;
using Karamucho.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardUI_DetailHover : BaseCardUI
{
    public enum HighlightList
    {
        None = 0,
        Soul = 1,
        HP = 2,
        ATK = 3,
        Armor = 4,
        Ability = 5,
        Arrow = 6
    }

    #region Inspector
    [Header("Owner")]
    [SerializeField] private Image _IMG_Shadow_Player;
    [SerializeField] private Image _IMG_Shadow_Enemy;

    [Header("Tooltip Position")]
    [SerializeField] private bool _ForceHide = false;
    [SerializeField] private UITooltipKeywordGroup _TooltipGroup;
    [SerializeField] private float _XRightOffset;
    [SerializeField] private float _XLeftOffset;
    [SerializeField] private float _YTopOffset;
    [SerializeField] private float _YBotOffset;

    [Header("Tutorial")]
    [SerializeField] private GameObject GROUP_Tutorial;
    [SerializeField] private GameObject GROUP_ArrowHilight;
    [SerializeField] private GameObject Label_Arrow;
    [SerializeField] private GameObject Label_Soul;
    [SerializeField] private GameObject Label_Atk;
    [SerializeField] private GameObject Label_Hp;
    [SerializeField] private GameObject Label_Armor;
    [SerializeField] private GameObject Label_Ability;
    [SerializeField] private List<GameObject> ArrowHilight = new List<GameObject>();
    [SerializeField] private GameObject HIGHLIGHT_Soul;
    [SerializeField] private GameObject HIGHLIGHT_Atk;
    [SerializeField] private GameObject HIGHLIGHT_Hp;
    [SerializeField] private GameObject HIGHLIGHT_Armor;
    [SerializeField] private GameObject HIGHLIGHT_Ability;
    #endregion

    #region Private
    private Sequence _sq;
    private Vector3 _DEFAULT_SCALE = Vector3.one;
    private bool _tooltipIsShow = false;

    // Timer Show Animtion
    private float _delayTime = 0.1f;
    private float _showTimer;

    // Screen offset
    private Vector3 _showOffset = new Vector3(0, 0, 0);
    private const float _OFFSET_LEFT = 0.28f;
    private const float _OFFSET_RIGHT = 0.72f;
    private const float _OFFSET_MID = 0.50f;
    private const float _OFFSET_NORMAL = 0.7f;
    private const float _OFFSET_HAND = 0.35f;
    #endregion

    #region Events
    public event Action OnHideUI;
    #endregion

    private new void Update()
    {
        base.Update();

        ShowCardTimer();

    }

    #region Timer
    private void ShowCardTimer()
    {
        if (gameObject.activeSelf)
        {
            if (_showTimer < _delayTime)
            {
                _showTimer += Time.deltaTime;
            }
            else
            {
                if (!_ForceHide)
                {
                    SetTooltip(_data);
                }

            }
        }
    }
    #endregion

    #region Methods

    public void ShowCard(bool isShow)
    {
        if (isShow)
        {
            gameObject.SetActive(true);

        }
        else
        {
            OnHideUI?.Invoke();
            gameObject.SetActive(false);
        }
    }

    public void SetData(CardUIData data, bool isInBattle = true)
    {
        base.SetData(data);

        // Update Hover Position
        if (isInBattle && Camera.main != null)
        {
            if (_data.CurrentZone == CardZone.Battlefield || _data.CurrentZone == CardZone.Hand)
            {
                UpdatePosition(_data.UniqueID);
            }
        }

        // Start Show Card
        AnimateShow();

        // Optional
        if (isInBattle)
        {
            ShowTutorial();
            SetColorPlayer(GameManager.Instance.IsLocalPlayer(_data.Owner));
        }
        else
        {
            if (GROUP_Tutorial != null)
            {
                GROUP_Tutorial?.SetActive(false);
                Destroy(GROUP_Tutorial);
            }
        }

    }

    private void SetColorPlayer(bool isLocalPlayer)
    {
        _IMG_Shadow_Player.gameObject.SetActive(isLocalPlayer);
        _IMG_Shadow_Enemy.gameObject.SetActive(!isLocalPlayer);
    }

    private void SetTooltip(CardUIData data)
    {
        if (data != null)
        {
            _TooltipGroup.SetData(data);
            if (!_tooltipIsShow)
            {
                _TooltipGroup.ShowTooltip();
                _tooltipIsShow = true;
            }
        }
    }

    private void SetTooltipOffset(bool onHand, bool isRight)
    {
        if (onHand)
        {
            if (isRight)
            {
                // Show tooltip on left side of detail UI.
                _TooltipGroup.GetComponent<VerticalLayoutGroup>().childAlignment = TextAnchor.LowerRight;
                _TooltipGroup.transform.localPosition = new Vector3(_XLeftOffset, _TooltipGroup.transform.localPosition.y);
                _TooltipGroup.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.0f);
                _TooltipGroup.GetComponent<RectTransform>().DOPivotX(1.0f, 0.3f);
            }
            else
            {
                // Show tooltip on right side of detail UI.
                _TooltipGroup.GetComponent<VerticalLayoutGroup>().childAlignment = TextAnchor.LowerLeft;
                _TooltipGroup.transform.localPosition = new Vector3(_XRightOffset, _TooltipGroup.transform.localPosition.y);
                _TooltipGroup.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.0f);
                _TooltipGroup.GetComponent<RectTransform>().DOPivotX(0.0f, 0.3f);
            }
        }
        else
        {
            // Show tooltip on bot side of detail UI.
            _TooltipGroup.GetComponent<VerticalLayoutGroup>().childAlignment = TextAnchor.MiddleCenter;
            _TooltipGroup.transform.localPosition = new Vector3(0, _YBotOffset);
            _TooltipGroup.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
            _TooltipGroup.GetComponent<RectTransform>().DOPivotY(1.0f, 0.3f);
        }
    }
    #endregion

    #region Override Methods
    protected override void SetAtk(int value, CardUIData data)
    {
        base.SetAtk(value, data);

        /*
        if (value != data.ATKDefault)
        {
            _TEXT_Attack.text = string.Format("{0}/{1}", value, data.ATKDefault);
        }
        */
    }

    protected override void SetHp(int value, CardUIData data)
    {
        base.SetHp(value, data);

        if (value < data.HPBase)
        {
            _TEXT_HP.text = string.Format("{0}/{1}", value, data.HPBase);
        }

    }

    protected override void SetArmor(int value, CardUIData data)
    {
        base.SetArmor(value, data);
    }
    #endregion

    #region Card Position
    public void UpdatePosition(int cardUniqueID)
    {
        CardUI cardUI;
        BattleCardData battleCardData;
        UIManager.Instance.FindCardByUniqueID(cardUniqueID, out cardUI);
        GameManager.Instance.FindBattleCard(cardUniqueID, out battleCardData);

        if (cardUI != null && battleCardData != null)
        {
            // Reset
            _showOffset = Vector3.zero;

            // Convert to viewport to find percentage of view
            Vector3 location = cardUI.transform.position;
            Vector3 targetLocation = Camera.main.WorldToViewportPoint(location);

            // Get value from parent canvas
            float width = transform.parent.GetComponent<RectTransform>().sizeDelta.x;
            float height = transform.parent.GetComponent<RectTransform>().sizeDelta.y;
            Vector3 leftOffset = new Vector3(width * _OFFSET_LEFT, height * _OFFSET_NORMAL, 0.0f);
            Vector3 rightOffset = new Vector3(width * _OFFSET_RIGHT, height * _OFFSET_NORMAL, 0.0f);
            Vector3 midOffset = new Vector3(width * _OFFSET_MID, height * _OFFSET_NORMAL, 0.0f);

            bool isRight = false;
            bool onHand = false;

            // Move to target location
            //Vector3 targetLocation = Camera.main.WorldToScreenPoint(cardUI.transform.position);

            if (battleCardData.CurrentZone == CardZone.Battlefield)
            {
                onHand = false;

                // if Tutorial 1
                if(GameManager.Instance.Mode == GameMode.Tutorial_1)
                {
                    _showOffset = midOffset;
                }
                else
                {
                    // In battlefield
                    switch (cardUI.GetSlotID() % GameManager.Instance.BoardWidth)
                {
                    case 0: // 1st Column
                    case 1: // 2nd Column
                    default:
                    {
                        // Show detail at right side of screen.

                        //_showOffset = new Vector3(Screen.width * 0.7f, targetLocation.y, 0.0f);
                        _showOffset = rightOffset;
                        isRight = true;
                    }
                    break;

                    case 2: // 3rd Column
                    case 3: // 4th Column
                    {
                        // Show detail at left side of screen.

                        //_showOffset = new Vector3(Screen.width * 0.3f, targetLocation.y, 0.0f);
                        _showOffset = leftOffset;
                        isRight = false;
                    }
                    break;
                }
                }
            }
            else // On Hand
            {
                onHand = true;
                if (targetLocation.x < 0.5f)
                {
                    // Show detail at left side of screen.
                    _showOffset += leftOffset;
                    isRight = false;
                }
                else //if (targetLocation.x >= Screen.width * 0.5f)
                {
                    // Show detail at right side of screen.
                    _showOffset += rightOffset;
                    isRight = true;
                }
                _showOffset = new Vector3(_showOffset.x, height * _OFFSET_HAND, 0.0f);
            }

            GetComponent<RectTransform>().anchoredPosition = _showOffset;
            SetTooltipOffset(onHand, isRight);

        }
    }

    /// <summary>
    /// Use this when update position in card collection.
    /// </summary>
    /// <param name="location"></param>
    public void UpdatePosition(Vector2 location)
    {
        // Reset
        _showOffset = Vector3.zero;

        // Convert to viewport to find percentage of view
        Vector2 targetLocation = Camera.main.ScreenToViewportPoint(location);

        // Get value from parent canvas
        float width = transform.parent.GetComponent<RectTransform>().sizeDelta.x;
        float height = transform.parent.GetComponent<RectTransform>().sizeDelta.y;
        Vector3 leftOffset = new Vector3(width * _OFFSET_LEFT, height * _OFFSET_NORMAL, 0.0f);
        Vector3 rightOffset = new Vector3(width * _OFFSET_RIGHT, height * _OFFSET_NORMAL, 0.0f);

        bool isRight = false;
        bool onHand = false;

        if (targetLocation.x < 0.5f)
        {
            //Debug.Log("Left Side");
            _showOffset += rightOffset;
            isRight = false;
        }
        else //if (targetLocation.x >= Screen.width * 0.5f)
        {
            //Debug.Log("Right Side");
            _showOffset += leftOffset;
            isRight = true;
        }

        GetComponent<RectTransform>().anchoredPosition = _showOffset;
        SetTooltipOffset(onHand, isRight);

    }

    public void UpdateGravePosition(Vector3 location)
    {
        // Reset
        _showOffset = Vector3.zero;

        // Get value from parent canvas
        float width = transform.parent.GetComponent<RectTransform>().sizeDelta.x;
        float height = transform.parent.GetComponent<RectTransform>().sizeDelta.y;
        Vector3 leftOffset = new Vector3(width * _OFFSET_LEFT, height * _OFFSET_NORMAL, 0.0f);
        Vector3 rightOffset = new Vector3(width * _OFFSET_RIGHT, height * _OFFSET_NORMAL, 0.0f);

        // Find object in percentage of view
        Vector3 targetLocation = location;

        bool onHand = false;
        bool isRight = false;

        if (targetLocation.x < width * 0.5f)
        {
            //Debug.Log("Left Side");
            _showOffset += rightOffset;
            isRight = false;
        }
        else //if (targetLocation.x >= Screen.width * 0.5f)
        {
            //Debug.Log("Right Side");
            _showOffset += leftOffset;
            isRight = true;
        }

        GetComponent<RectTransform>().anchoredPosition = _showOffset;
        SetTooltipOffset(onHand, isRight);
    }
    #endregion

    #region Animation
    private void AnimateShow()
    {
        _sq.Kill();
       transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);

        _sq = DOTween.Sequence();
        _sq.Append(transform.DOScale(_DEFAULT_SCALE, 0.25f).SetEase(Ease.OutExpo));

        _TooltipGroup.HideAllKeywordIcon();
        _showTimer = 0.0f;
        _tooltipIsShow = false;
    }
    #endregion

    #region Tutorial
    public void ShowHighlightLabel(List<HighlightList> highlightList)
    {
        if (highlightList != null)
        {
            foreach (HighlightList highlight in highlightList)
            {
                switch (highlight)
                {
                    default:
                    case HighlightList.None:
                    HideAllTutorial();
                    break;

                    case HighlightList.Soul:
                    Label_Soul.SetActive(true);
                    HIGHLIGHT_Soul.SetActive(true);
                    break;

                    case HighlightList.HP:
                    Label_Hp.SetActive(true);
                    HIGHLIGHT_Hp.SetActive(true);
                    break;

                    case HighlightList.ATK:
                    Label_Atk.SetActive(true);
                    HIGHLIGHT_Atk.SetActive(true);
                    break;

                    case HighlightList.Armor:
                    Label_Armor.SetActive(true);
                    HIGHLIGHT_Armor.SetActive(true);
                    break;

                    case HighlightList.Ability:
                    Label_Ability.SetActive(true);
                    HIGHLIGHT_Ability.SetActive(true);
                    break;

                    case HighlightList.Arrow:
                    {
                        foreach (CardDirectionType item in System.Enum.GetValues(typeof(CardDirectionType)))
                        {
                            bool isShow = Data.CardDir.IsDirection(item);
                            ArrowHilight[(int)item].SetActive(isShow);
                        }
                    }
                    GROUP_ArrowHilight.SetActive(true);
                    Label_Arrow.SetActive(true);
                    break;

                }
            }
        }
    }

    private void ShowTutorial()
    {
        HideAllTutorial();

        if (GameManager.Instance.IsStoryMode())
        {
            GameHelper.UITransition_FadeIn(GROUP_Tutorial);
            StoryGameController tutorialController = GameManager.Instance.Controller as StoryGameController;
            List<HighlightList> highlights = tutorialController.OnTouchUIDetailCard();

            ShowHighlightLabel(highlights);
        }
    }

    private void HideAllTutorial()
    {

        // Label
        Label_Ability.SetActive(false);
        Label_Armor.SetActive(false);
        Label_Arrow.SetActive(false);
        Label_Atk.SetActive(false);
        Label_Hp.SetActive(false);
        Label_Soul.SetActive(false);

        // Highlight
        GROUP_ArrowHilight.SetActive(false);
        HIGHLIGHT_Soul.SetActive(false);
        HIGHLIGHT_Hp.SetActive(false);
        HIGHLIGHT_Atk.SetActive(false);
        HIGHLIGHT_Armor.SetActive(false);
        HIGHLIGHT_Ability.SetActive(false);
    }
    #endregion
}
