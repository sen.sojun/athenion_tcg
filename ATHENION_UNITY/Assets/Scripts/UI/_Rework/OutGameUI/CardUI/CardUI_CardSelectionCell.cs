﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using DG.Tweening;
using TMPro;
using UnityEngine.EventSystems;
using Karamucho.UI;

public class CardUI_CardSelectionCell : BaseCardUI
{
    #region Inspector
    [Header("Selection")]
    [SerializeField] private GameObject _ICON_Selection;
    #endregion

    #region Private Properties
    private bool _isSelected = false;
    private PointerEventDataEvent _onUnityBeginDrag = null;
    private PointerEventDataEvent _onUnityDrag = null;
    private PointerEventDataEvent _onUnityEndDrag = null;
    #endregion

    #region Methods
    public void SetSelected(bool isSelect, UnityAction onSelect)
    {
        _isSelected = isSelect;
        _ICON_Selection.SetActive(_isSelected);

        onSelect?.Invoke();
    }

    public bool GetSelected()
    {
        return _isSelected;
    }
    #endregion

    #region Events
    public event UnityAction<CardUI_CardSelectionCell> OnCardDown;
    public event UnityAction<CardUI_CardSelectionCell> OnCardClick;
    public event UnityAction<CardUI_CardSelectionCell> OnCardDoubleClick;
    public event Action<CardUI_CardSelectionCell> OnCardBeginDragEvent;
    public event Action<CardUI_CardSelectionCell> OnCardEndDragEvent;
    #endregion

    #region Input Event
    public void AddListenerOnUnityBeginDrag(UnityAction<PointerEventData> callback)
    {
        if (_onUnityBeginDrag == null) _onUnityBeginDrag = new PointerEventDataEvent();

        _onUnityBeginDrag.AddListener(callback);
    }

    public void AddListenerOnUnityDrag(UnityAction<PointerEventData> callback)
    {
        if (_onUnityDrag == null) _onUnityDrag = new PointerEventDataEvent();

        _onUnityDrag.AddListener(callback);
    }

    public void AddListenerOnUnityEndDrag(UnityAction<PointerEventData> callback)
    {
        if (_onUnityEndDrag == null) _onUnityEndDrag = new PointerEventDataEvent();

        _onUnityEndDrag.AddListener(callback);
    }

    public override void OnEventUnityPointerDown(PointerEventData eventData)
    {
        base.OnEventUnityPointerDown(eventData);
        OnCardDown?.Invoke(this);
    }

    public override void OnEventUnityPointerUp(PointerEventData eventData)
    {
        base.OnEventUnityPointerUp(eventData);
    }

    public override void OnEventUnityBeginDrag(PointerEventData eventData)
    {
        base.OnEventUnityBeginDrag(eventData);

        if (_onUnityBeginDrag != null)
        {
            _onUnityBeginDrag.Invoke(eventData);
        }
    }

    public override void OnEventUnityDrag(PointerEventData eventData)
    {
        base.OnEventUnityDrag(eventData);

        if (_onUnityDrag != null)
        {
            _onUnityDrag.Invoke(eventData);
        }
    }

    public override void OnEventUnityEndDrag(PointerEventData eventData)
    {
        base.OnEventUnityEndDrag(eventData);

        if (_onUnityEndDrag != null)
        {
            _onUnityEndDrag.Invoke(eventData);
        }
    }

    public override void OnEventClick(PointerEventData eventData)
    {
        base.OnEventClick(eventData);
        OnCardClick?.Invoke(this);
    }

    public override void OnEventDoubleClick(PointerEventData eventData)
    {
        base.OnEventDoubleClick(eventData);
        OnCardDoubleClick?.Invoke(this);
    }

    public override void OnEventBeginDrag(PointerEventData eventData)
    {
        base.OnEventBeginDrag(eventData);
        OnCardBeginDragEvent?.Invoke(this);
    }

    public override void OnEventDrag(PointerEventData eventData)
    {
        base.OnEventDrag(eventData);
    }

    public override void OnEventEndDrag(PointerEventData eventData)
    {
        base.OnEventEndDrag(eventData);
        OnCardEndDragEvent?.Invoke(this);
    }

    public override void OnEventBeginHold(PointerEventData eventData)
    {
        base.OnEventBeginHold(eventData);

    }

    public override void OnEventEndHold(PointerEventData eventData)
    {
        base.OnEventEndHold(eventData);
    }
    #endregion
}
