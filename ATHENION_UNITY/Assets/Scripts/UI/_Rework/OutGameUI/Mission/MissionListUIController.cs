﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;


public class MissionListUIController : MonoBehaviour
{
    #region Inspector
    [Header("Mission Cell")]
    [SerializeField] private MissionCell _Prefab_MissionCell;

    [Header("Container")]
    [SerializeField] private Transform _Container;
    [SerializeField] private Button _BTN_Close;
    [SerializeField] private ScrollRect _ScrollRect;
    #endregion

    #region Private
    private List<MissionData> _missionList = new List<MissionData>();
    private List<MissionCell> _missionCellList = new List<MissionCell>();
    #endregion

    #region Event

    public static event Action OnActive;
    public bool IsReady { get; private set; }

    #endregion

    #region Methods
    public void ShowUI()
    {
        HomeManager.Instance.HideMainCanvas();
        PopupUIManager.Instance.Show_SoftLoad(true);
        TopToolbar.AddSetting(true, true, HideUI);
        OnActive?.Invoke();
        InitBTN();
        GameHelper.UITransition_FadeIn(this.gameObject, delegate
        {
            StartCoroutine(UpdateData(() =>
            {
                PopupUIManager.Instance.Show_SoftLoad(false);
                IsReady = true;
            }));
        });
    }

    public void HideUI()
    {
        HomeManager.Instance.ShowMainCanvas();
        TopToolbar.RemoveLatestSetting();
        StopAllCoroutines();
        GameHelper.UITransition_FadeOut(this.gameObject);
    }

    public void SetEnableScrollRect(bool status)
    {
        _ScrollRect.vertical = status;
    }
    #endregion

    #region Mission Cell

    public IEnumerator UpdateData(UnityAction onComplete)
    {
        _missionList = new List<MissionData>(DataManager.Instance.PlayerMission.MissionList);

        for (int i = 0; i < _missionList.Count; ++i)
        {
            if (i < _missionCellList.Count)
            {
                MissionUIData dataUI = new MissionUIData(_missionList[i]);
                _missionCellList[i].InitData(dataUI, OnEventGo, OnEventGet);
            }
            else
            {
                GenerateCell(_missionList[i]);
            }

            // Sorting
            foreach (MissionCell cell in _missionCellList)
            {
                if (cell.Data.IsCleared)
                {
                    cell.transform.SetAsLastSibling();
                }
            }

            yield return new WaitForEndOfFrame();
        }

        onComplete?.Invoke();
    }

    private void HideAllCell()
    {
        foreach (MissionCell item in _missionCellList)
        {
            item.gameObject.SetActive(false);
        }
    }

    public void GenerateCell(MissionData data)
    {
        GameObject obj = Instantiate(_Prefab_MissionCell.gameObject, _Container);
        MissionCell cell = obj.GetComponent<MissionCell>();
        MissionUIData dataUI = new MissionUIData(data);
        cell.InitData(dataUI, OnEventGo, OnEventGet);

        _missionCellList.Add(cell);
    }

    public MissionCell GetMissionCell(string missionID)
    {
        foreach (MissionCell missionCell in _missionCellList)
        {
            if (missionCell.Data.ID == missionID)
                return missionCell;
        }
        return null;
    }

    private void InitBTN()
    {
        _BTN_Close?.onClick.RemoveAllListeners();
        _BTN_Close?.onClick.AddListener(HideUI);
    }
    #endregion

    #region Event Methods
    private void OnEventGo(MissionCell cell)
    {
        Debug.LogFormat("Go!! {0}({1})", cell.Data.GoFuncName, cell.Data.GoFuncParam);

        HideUI();

        NavigatorController.Instance.SendMessage(
              cell.Data.GoFuncName
            , cell.Data.GoFuncParam
            , SendMessageOptions.DontRequireReceiver
        );
    }

    private void OnEventGet(MissionCell cell)
    {
        PopupUIManager.Instance.Show_MidSoftLoad(true);

        Dictionary<string, FeatureUnlockData> featureUnlockData = new Dictionary<string, FeatureUnlockData>(DataManager.Instance.PlayerFeatureUnlock);
        MissionManager.Instance.RequestClaimMissionReward(
             cell.Data.ID
            , delegate (List<ItemData> rewardList) // onComplete
            {
                StartCoroutine(
                    UpdateData(delegate ()
                    {
                        DataManager.Instance.LoadInventory(
                            delegate() // onComplete
                            {
                                // reward
                                PopupUIManager.Instance.ShowPopup_GotReward(
                                      LocalizationManager.Instance.GetText("TEXT_MISSION_YOUVEGOTREWARDTITLE")
                                    , LocalizationManager.Instance.GetText("TEXT_MISSION_YOUVEGOTREWARD")
                                    , rewardList
                                );

                                // feature unlock
                                Dictionary<string, FeatureUnlockData> newFeatureUnlockData = DataManager.Instance.PlayerFeatureUnlock;
                                List<string> featureKeyList = new List<string>();
                                foreach (KeyValuePair<string, FeatureUnlockData> feature in newFeatureUnlockData)
                                {
                                    if (featureUnlockData.ContainsKey(feature.Key) == false)
                                    {
                                        featureKeyList.Add(LocalizationManager.Instance.GetText(string.Format("TEXT_FEATURE_UNLOCK_{0}", feature.Key)));
                                    }
                                }

                                if (featureKeyList.Count > 0)
                                {
                                    PopupUIManager.Instance.ShowPopup_MultipleLine(LocalizationManager.Instance.GetText("TEXT_MISSION_FEATUREUNLOCK"), featureKeyList);
                                }

                                PopupUIManager.Instance.Show_MidSoftLoad(false);
                            }
                            , delegate(string errorMsg) // onFail
                            {
                                PopupUIManager.Instance.ShowPopup_Error(
                                      LocalizationManager.Instance.GetText("ERROR_POPUP_HEADER")
                                    , LocalizationManager.Instance.GetText(errorMsg)
                                );

                                PopupUIManager.Instance.Show_MidSoftLoad(false);
                            }
                        );
                    }
                ));
            }
            , delegate (string errorMsg) // onFail
            {
                StartCoroutine(
                    UpdateData(delegate ()
                    {
                        Debug.LogErrorFormat("OnEventGet/RequestClaimMission: Failed. {0}", errorMsg);

                        PopupUIManager.Instance.ShowPopup_Error(
                              LocalizationManager.Instance.GetText("ERROR_POPUP_HEADER")
                            , LocalizationManager.Instance.GetText("ERROR_MISSION_FAIL_CLAIM")
                        );

                        PopupUIManager.Instance.Show_MidSoftLoad(false);
                    }
                ));
            }
        );
    }
    #endregion
}
