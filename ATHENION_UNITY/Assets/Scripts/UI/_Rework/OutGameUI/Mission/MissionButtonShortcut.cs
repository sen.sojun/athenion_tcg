﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;

public class MissionButtonShortcut : MonoBehaviour
{
    #region Inspector
    [Header("Reward")]
    [SerializeField] private Image _ICON_Reward;
    [SerializeField] private TextMeshProUGUI _TEXT_RewardCount;

    [Header("Current Mission")]
    [SerializeField] private TextMeshProUGUI _TEXT_MissionTitle;
    [SerializeField] private TextMeshProUGUI _TEXT_MissionDetail;
    [SerializeField] private TextMeshProUGUI _TEXT_Progress;

    [Header("BTN")]
    [SerializeField] private Button _BTN_Go;
    [SerializeField] private Button _BTN_Get;

    #endregion

    #region Private
    private List<MissionData> _missionList = new List<MissionData>();
    private MissionUIData _currentMissionUIData;
    private UnityAction _onGo;
    private UnityAction _onGet;
    #endregion

    #region Methods
    private void OnEnable()
    {
        PlayerStatManager.OnCountPlayerStatCompleted += OnUpdateData;

        InitData();
    }

    private void OnDisable()
    {
        PlayerStatManager.OnCountPlayerStatCompleted -= OnUpdateData;
    }

    private void InitData(UnityAction onComplete = null)
    {
        _currentMissionUIData = null;
        _missionList = DataManager.Instance.PlayerMission.MissionList;

        // Get leasted mission
        foreach (MissionData item in _missionList)
        {
            if (item.IsCleared) continue;

            MissionUIData uiData = new MissionUIData(item);
            if (uiData.ImageKey != null)
            {
                _currentMissionUIData = uiData;
                break;
            }
        }

        // Check Available Mission
        if (_currentMissionUIData == null)
        {
            gameObject.SetActive(false);
        }
        else
        {
            Setup();
            InitBTN();
        }

        onComplete?.Invoke();
    }

    private void Setup()
    {
        SetText();
        SetReward();
        gameObject.SetActive(true);
    }

    private void InitBTN()
    {
        _BTN_Get.gameObject.SetActive(_currentMissionUIData.IsClaimable == true);
        _BTN_Go.gameObject.SetActive(_currentMissionUIData.IsCompleted == false);

        _BTN_Get?.onClick.RemoveAllListeners();
        _BTN_Get?.onClick.AddListener(() => OnEventGet(_currentMissionUIData));

        _BTN_Go?.onClick.RemoveAllListeners();
        _BTN_Go?.onClick.AddListener(() => OnEventGo(_currentMissionUIData));
    }

    private void SetText()
    {
        _TEXT_MissionTitle.text = _currentMissionUIData.Title;
        _TEXT_MissionDetail.text = _currentMissionUIData.Description;
        _TEXT_Progress.text = string.Format("{0}/{1}", _currentMissionUIData.ProgressCurrent, _currentMissionUIData.ProgressMax);
    }

    private void SetReward()
    {
        _ICON_Reward.sprite = SpriteResourceHelper.LoadSprite(_currentMissionUIData.ImageKey, SpriteResourceHelper.SpriteType.ICON_Item);
        _TEXT_RewardCount.text = _currentMissionUIData.RewardCount.ToString();
    }

    public void SetEnableBTN(bool isEnable)
    {
        _BTN_Get.interactable = isEnable;
        _BTN_Go.interactable = isEnable;
    }
    #endregion

    #region Event Methods
    private void OnUpdateData()
    {
        InitData(null);
    }

    private void OnEventGo(MissionUIData data)
    {
        Debug.LogFormat("Go!! {0}({1})", data.GoFuncName, data.GoFuncParam);

        NavigatorController.Instance.SendMessage(
              data.GoFuncName
            , data.GoFuncParam
            , SendMessageOptions.DontRequireReceiver
        );
    }

    private void OnEventGet(MissionUIData data)
    {
        PopupUIManager.Instance.Show_MidSoftLoad(true);

        Dictionary<string, FeatureUnlockData> featureUnlockData = new Dictionary<string, FeatureUnlockData>(DataManager.Instance.PlayerFeatureUnlock);
        MissionManager.Instance.RequestClaimMissionReward(
             data.ID
            , delegate (List<ItemData> rewardList) // onComplete
            {
                InitData(delegate ()
                {
                    DataManager.Instance.LoadInventory(
                        delegate () // onComplete
                        {
                            PopupUIManager.Instance.ShowPopup_GotReward(
                                    LocalizationManager.Instance.GetText("TEXT_MISSION_YOUVEGOTREWARDTITLE")
                                , LocalizationManager.Instance.GetText("TEXT_MISSION_YOUVEGOTREWARD")
                                , rewardList
                            );

                            // feature unlock
                            Dictionary<string, FeatureUnlockData> newFeatureUnlockData = DataManager.Instance.PlayerFeatureUnlock;
                            List<string> featureKeyList = new List<string>();
                            foreach (KeyValuePair<string, FeatureUnlockData> feature in newFeatureUnlockData)
                            {
                                if (featureUnlockData.ContainsKey(feature.Key) == false)
                                {
                                    featureKeyList.Add(LocalizationManager.Instance.GetText(string.Format("TEXT_FEATURE_UNLOCK_{0}", feature.Key)));
                                }
                            }

                            if (featureKeyList.Count > 0)
                            {
                                PopupUIManager.Instance.ShowPopup_MultipleLine(LocalizationManager.Instance.GetText("TEXT_MISSION_FEATUREUNLOCK"), featureKeyList);
                            }

                            PopupUIManager.Instance.Show_MidSoftLoad(false);
                        }
                        , delegate (string errorMsg) // onFail
                        {
                            PopupUIManager.Instance.ShowPopup_Error(
                                  LocalizationManager.Instance.GetText("ERROR_POPUP_HEADER")
                                , LocalizationManager.Instance.GetText(errorMsg)
                            );

                            PopupUIManager.Instance.Show_MidSoftLoad(false);
                        }
                    );
                });
            }
            , delegate (string errorMsg) // onFail
            {
                InitData(delegate ()
                {
                    Debug.LogErrorFormat("OnEventGet/RequestClaimMission: Failed. {0}", errorMsg);

                    PopupUIManager.Instance.ShowPopup_Error(
                          LocalizationManager.Instance.GetText("ERROR_POPUP_HEADER")
                        , LocalizationManager.Instance.GetText("ERROR_MISSION_FAIL_CLAIM")
                    );

                    PopupUIManager.Instance.Show_MidSoftLoad(false);
                });
            }
        );
    }
    #endregion
}
