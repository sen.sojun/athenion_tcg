﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;

public class MissionUIData : BaseGameObjectiveUIData
{
    public string GoFuncName { get; protected set; }
    public string GoFuncParam { get; protected set; }

    private MissionData _missionData;

    #region Constructor
    public MissionUIData()
    {
        GoFuncName = "";
        GoFuncParam = "";
    }

    public MissionUIData(MissionData data)
    {
        _missionData = data;

        GoFuncName = data.GoFuncName;
        GoFuncParam = data.GoFuncParam;

        SetupData(_missionData);
    }
    #endregion

    #region Methods
    protected override string GetTitle()
    {
        return LocalizationManager.Instance.GetQuestTitle(ID);
    }

    protected override string GetDescription()
    {
        return LocalizationManager.Instance.GetQuestDescription(ID);
    }

    public MissionData GetMissionData()
    {
        return _missionData;
    }
    #endregion
}

public class MissionCell : MonoBehaviour
{
    #region Inspector
    [Header("Item")]
    [SerializeField] private Image _ICON_Item;
    [SerializeField] private TextMeshProUGUI _TEXT_RewardCount;

    [Header("Progress")]
    [SerializeField] private TextMeshProUGUI _TEXT_Title;
    [SerializeField] private TextMeshProUGUI _TEXT_Detail;
    [SerializeField] private TextMeshProUGUI _TEXT_Progress;
    [SerializeField] private Slider _SLIDER_Progress;
    [SerializeField] private GameObject _GROUP_Lock;
    [SerializeField] private GameObject _GROUP_Complete;

    [Header("BTN")]
    [SerializeField] private Button _BTN_GO;
    [SerializeField] private Button _BTN_Get;
    #endregion

    #region Public 
    public MissionUIData Data { get { return _data; } }
    #endregion

    #region Private 
    private MissionUIData _data;
    private UnityAction<MissionCell> _onGo, _onGet;
    #endregion

    #region Methods
    public void InitData(MissionUIData data, UnityAction<MissionCell> onGo, UnityAction<MissionCell> onGet)
    {
        _data = data;
        _onGo = onGo;
        _onGet = onGet;

        Setup();
        SetReward();
        InitBTN();
    }

    public Button GetGoButton()
    {
        return _BTN_GO;
    }

    public Button GetClaimButton()
    {
        return _BTN_Get;
    }

    private void InitBTN()
    {
        if (_data != null)
        {
            _BTN_Get.gameObject.SetActive(_data.IsClaimable == true);
            _BTN_GO.gameObject.SetActive(_data.IsCompleted == false);
        }

        _BTN_GO?.onClick.RemoveAllListeners();
        _BTN_GO?.onClick.AddListener(() => _onGo?.Invoke(this));

        _BTN_Get?.onClick.RemoveAllListeners();
        _BTN_Get?.onClick.AddListener(() => _onGet?.Invoke(this));
    }

    private void Setup()
    {
        _TEXT_Title.text = _data.Title;
        _TEXT_Detail.text = _data.Description;
        _SLIDER_Progress.value = (float)_data.ProgressCurrent / (float)_data.ProgressMax;
        _TEXT_Progress.text = _data.ProgressCurrent.ToString() + " / " + _data.ProgressMax;

        bool isLock = _data.GetMissionData().IsEnabled == false;
        bool isClear = _data.IsCleared;

        if (isClear) transform.SetAsLastSibling();

        _GROUP_Lock.SetActive(isLock);
        _GROUP_Complete.SetActive(isClear);
    }

    private void SetReward()
    {
        if (_data.ImageKey != null)
        {
            _TEXT_RewardCount.text = _data.RewardCount.ToString();
            _ICON_Item.sprite = SpriteResourceHelper.LoadSprite(_data.ImageKey, SpriteResourceHelper.SpriteType.ICON_Item);
            gameObject.SetActive(true);
        }
        else
        {
            gameObject.SetActive(false);
        }
    }
    #endregion
}
