﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Karamucho.Collection;
using System.Linq;

#region Global Enums
public enum CraftCardEventTypes
{
    Craft
    , Recycle
    , Enchant
}
#endregion

#region Data
public class CollectionDataManager
{
    #region Public Properties
    public List<CardData> CardDataList { get { return _cardDataList; } }
    public CardListData RefCardDataStack { get { return _refCardDataStack; } }

    public List<DeckData> DeckDataList { get { return _deckDataList; } }
    public int DeckSlotAmount { get { return _deckSlotAmount; } }
    #endregion

    #region Private Properties
    private List<CatalogCardData> _catalogCardDataList = new List<CatalogCardData>();
    private List<CardData> _cardDataList = new List<CardData>();
    private CardListData _cardDataStack = new CardListData();
    private CardListData _refCardDataStack = new CardListData();

    private List<DeckData> _deckDataList = new List<DeckData>();
    private int _deckSlotAmount = 0;
    #endregion

    #region Contructors
    public CollectionDataManager()
    {
        InitData();
    }
    #endregion

    #region Methods
    private void InitData()
    {
        InitCardData();
        InitDeckData();
    }

    #region Card Data
    private void InitCardData()
    {
        List<CardData> cardDataList = new List<CardData>();
        _catalogCardDataList = DataManager.Instance.GetCardCatalog();

        // Load Card from Catalog
        foreach (CatalogCardData catalogCardData in _catalogCardDataList)
        {
            CardData cardData = null;
            if (CardData.GetCardData(catalogCardData.ItemCatalogID, out cardData))
            {
                if (cardData.Type == CardType.Minion) // Show only minion. 
                {
                    cardDataList.Add(cardData);
                    _cardDataStack.AddCard(catalogCardData.ItemCatalogID, 0);
                }
            }
        }

        // Load Card from Inventory
        CardListData playerInventory = DataManager.Instance.InventoryData.CardInventoryList;
        foreach (KeyValuePair<string, int> item in playerInventory)
        {
            CardData cardData = CardData.CreateCard(item.Key);
            if (cardData != null)
            {
                if (cardData.Type == CardType.Minion) // Show only minion. 
                {
                    if (cardDataList.Find(x => x.FullID == cardData.FullID) == null)
                    {
                        cardDataList.Add(cardData);
                        _cardDataStack.AddCard(item.Key, 0);
                    }
                    _cardDataStack[item.Key] += item.Value;
                }
            }
            else
            {
                Debug.LogErrorFormat("InitData/CreateCard: Fail to create card {0}", item.Key);
            }
        }

        _refCardDataStack = new CardListData(_cardDataStack);
        _cardDataList = SortCardDataList(cardDataList);
    }

    public void UpdateData()
    {
        // Load Card from Inventory
        CardListData playerInventory = DataManager.Instance.InventoryData.CardInventoryList;
        foreach (KeyValuePair<string, int> item in playerInventory)
        {
            _cardDataStack[item.Key] = item.Value;
        }

        _refCardDataStack = new CardListData(_cardDataStack);
    }

    private List<CardData> SortCardDataList(List<CardData> unsortedData)
    {
        // Sort by CardElement ASC => CardRarity DESC => CardSpirit DESC => CardID ASC
        IEnumerable<CardData> query = unsortedData.OrderBy(card => card.Spirit).ThenBy(card => card.Rarity).ThenBy(card => card.BaseID);
        return new List<CardData>(query.ToList());
    }

    public void AddCard(string cardFullID, int amount = 1)
    {
        if (_cardDataStack.ContainKey(cardFullID))
        {
            _cardDataStack[cardFullID] += amount;
        }
    }

    public void RemoveCard(string cardFullID, int amount = 1)
    {
        if (_cardDataStack.ContainKey(cardFullID))
        {
            _cardDataStack[cardFullID] -= amount;
        }
    }

    public void ResetCardDataStack()
    {
        _cardDataStack = new CardListData(_refCardDataStack);
    }

    public CardData GetCardData(string cardFullID)
    {
        return _cardDataList.Find(x => x.FullID == cardFullID);
    }

    public CatalogCardData GetCatalogCardData(string cardFullID)
    {
        return _catalogCardDataList.Find(x => x.ItemCatalogID == cardFullID);
    }

    public int GetCardAmount(string cardFullID)
    {
        int result = 0;
        if (_cardDataStack.TryGetValue(cardFullID, out result))
        {
            return result;
        }

        return 0;
    }

    public int GetCardRawAmount(string cardFullID)
    {
        int result = 0;
        if (_refCardDataStack.TryGetValue(cardFullID, out result))
        {
            return result;
        }

        return 0;
    }

    public Dictionary<VirtualCurrency, int> GetCardCraftPrice(string cardFullID)
    {
        int index = _catalogCardDataList.FindIndex(x => x.ItemCatalogID == cardFullID);
        if (index < 0)
        {
            return new Dictionary<VirtualCurrency, int>();
        }

        return _catalogCardDataList[index].CraftPrice;
    }

    public Dictionary<VirtualCurrency, int> GetCardRecyclePrice(string cardFullID)
    {
        int index = _catalogCardDataList.FindIndex(x => x.ItemCatalogID == cardFullID);
        if (index < 0)
        {
            return new Dictionary<VirtualCurrency, int>();
        }

        return _catalogCardDataList[index].RecyclePrice;
    }

    public Dictionary<VirtualCurrency, int> GetCardTransformPrice(string cardFullID)
    {
        int index = _catalogCardDataList.FindIndex(x => x.ItemCatalogID == cardFullID);
        if (index < 0)
        {
            return new Dictionary<VirtualCurrency, int>();
        }

        return _catalogCardDataList[index].TransformPrice;
    }

    public bool IsCardCraftable(string cardFullID)
    {
        int index = _catalogCardDataList.FindIndex(x => x.ItemCatalogID == cardFullID);
        if (index < 0)
        {
            return false;
        }

        return _catalogCardDataList[index].CraftPrice.Count > 0;
    }

    public bool IsCardRecycleable(string cardFullID)
    {
        int index = _catalogCardDataList.FindIndex(x => x.ItemCatalogID == cardFullID);
        if (index < 0)
        {
            return false;
        }

        return _catalogCardDataList[index].RecyclePrice.Count > 0;
    }

    public bool IsCardTransformable(string cardFullID)
    {
        int index = _catalogCardDataList.FindIndex(x => x.ItemCatalogID == cardFullID);
        if (index < 0)
        {
            return false;
        }

        return _catalogCardDataList[index].TransformPrice.Count > 0;
    }

    public string GetCardTransformItemID(string cardFullID)
    {
        int index = _catalogCardDataList.FindIndex(x => x.ItemCatalogID == cardFullID);
        if (index < 0)
        {
            return string.Empty;
        }

        return _catalogCardDataList[index].TransformItemID;
    }

    public int GetPlayerVirtualCurrency(VirtualCurrency vc)
    {
        return DataManager.Instance.InventoryData.GetVirtualCurrency(vc);
    }
    #endregion

    #region Deck Data
    public void InitDeckData()
    {
        _deckSlotAmount = DataManager.Instance.GetDeckSlot();
        _deckDataList = DataManager.Instance.GetAllDeck();
        if (_deckDataList == null)
        {
            _deckDataList = new List<DeckData>();
        }
    }

    public bool TryGetDeckData(string deckID, out DeckData deck)
    {
        deck = _deckDataList.Find(x => x.DeckID == deckID);
        if(deck == null)
        {
            return false;
        }

        return true;
    }

    public bool TryGetDeckIndex(DeckData deck, out int index)
    {
        return TryGetDeckIndex(deck.DeckID, out index);
    }

    public bool TryGetDeckIndex(string deckID, out int index)
    {
        index = _deckDataList.FindIndex(x => x.DeckID == deckID);
        if(index < 0)
        {
            return false;
        }

        return true;
    }

    public void AddCardIntoDeck(string deckID, string cardFullID, int amount)
    {
        if (TryGetDeckData(deckID, out DeckData deck))
        {
            deck.Deck.AddCard(cardFullID, amount);
        }
    }

    public void RemoveCardFromDeck(string deckID, string cardFullID, int amount)
    {
        if (TryGetDeckData(deckID, out DeckData deck))
        {
            deck.Deck.RemoveCard(cardFullID, amount);
        }
    }

    public bool IsDeckSlotFull()
    {
        return _deckSlotAmount <= _deckDataList.Count;
    }

    public void AddDeck(DeckData deck)
    {
        if (IsDeckSlotFull() == false)
        {
            _deckDataList.Add(deck);
        }
    }

    public void RemoveDeck(DeckData deck)
    {
        if (TryGetDeckIndex(deck, out int index))
        {
            _deckDataList.RemoveAt(index);
        }
    }

    public void SetExistingDeckData(DeckData deck)
    {
        if (TryGetDeckIndex(deck, out int index))
        {
            _deckDataList[index] = new DeckData(deck);
        }
    }
    #endregion

    #endregion
}
#endregion

public class CollectionManager : OutGameUIBase
{
    #region Public Properties
    public override string SceneName { get { return GameHelper.CollectionSceneName; } }
    public static CollectionManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<CollectionManager>();
                DontDestroyOnLoad(_instance.gameObject);
            }
            return _instance;
        }
    }

    public CollectionDataManager Data { get { return _data; } }
    #endregion

    #region Private Properties
    private static CollectionManager _instance;
    private Vector3 _holdPosition;

    private CollectionDataManager _data;
    #endregion

    #region Inspector Properties
    public CollectionMissionUIContainer MissionUIContainer;
    #endregion

    #region Event
    public static event Action OnInit;
    public static event Action OnExitEvent;
    public static event Action OnShowPage;

    public static event Action<DeckData, bool> OnShowDeckDetail;
    public static event Action OnHideDeckDetail;
    public static event Action<DeckData> OnUpdateDeckDetail;
    public static event Action<DeckData> OnAutoDeckDetail;
    public static event Action OnCreateNewDeck;

    public static event Action<DropZoneName, GameObject, CardUIData, bool> OnActivateFloatingCard;
    public static event Action<bool> OnDeactivateFloatingCard;
    public static event Action<DropZoneName, string> OnUpdateCardZone;
    public static event Action<DropZoneName> OnUpdateCardDropZone;
    public static event Action<DropZoneName, string> OnSuccessUpdateCardZone;

    public static event Action OnActivateFloatingCardFromDeck;
    public static event Action OnActivateFloatingCardFromCard;
    public static event Action OnUpdateDeckDetailComplete;
    public static event Action OnShowCardDetailComplete;
    public static event Action OnUpdateCollectionFilter;

    public static event Action<string> OnShowCardDetail;
    public static event Action<string> OnShowCardDetail_Hover;
    public static event Action OnHideCardDetail_Hover;
    public static event Action OnHideCardDetail;
    public static event Action OnCraftCardComplete;
    public static event Action<DeckData, bool> OnShowCraftAllMissing;
    #endregion

    #region Methods
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            DestroyImmediate(gameObject);
            return;
        }
        else if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
    }

    public override void OnInitialize(UnityAction onComplete)
    {
        _data = new CollectionDataManager();

        onComplete?.Invoke();
        OnInit?.Invoke();
        OnShowPage?.Invoke();
    }

    public override void OnExit()
    {
        OnExitEvent?.Invoke();
        base.OnExit();
        CollectionFilterManager.Instance.DestroySelf();
    }

    #region Request Methods

    #region Manage Deck Detail
    public void RequestShowDeckDetail(DeckData deck, bool isNewDeck)
    {
        OnShowDeckDetail?.Invoke(deck, isNewDeck);
    }

    public void RequestUpdateDeckDetail(DeckData deck)
    {
        OnUpdateDeckDetail?.Invoke(deck);
    }

    public void RequestAutoDeckDetail(DeckData deck)
    {
        OnAutoDeckDetail?.Invoke(deck);
    }

    public void RequestHideDeckDetail()
    {
        _data.InitDeckData();
        OnHideDeckDetail?.Invoke();
    }

    public void RequestCreateNewDeck()
    {
        OnCreateNewDeck?.Invoke();
    }
    #endregion

    #region Manage Craft
    public void RequestShowCardDetail(string cardFullID)
    {
        OnShowCardDetail?.Invoke(cardFullID);
    }

    public void RequestHideCardDetail()
    {
        OnHideCardDetail?.Invoke();
    }

    public void RequestShowCardDetail_Hover(string cardFullID)
    {
        OnShowCardDetail_Hover?.Invoke(cardFullID);
    }

    public void RequestHideCardDetail_Hover()
    {
        OnHideCardDetail_Hover?.Invoke();
    }

    public void RequestShowCardDetailComplete()
    {
        OnShowCardDetailComplete?.Invoke();
    }

    public void RequestCraftCardComplete()
    {
        OnCraftCardComplete?.Invoke();
    }

    public void RequestShowCraftAllMissing(DeckData data, bool isNewDeck)
    {
        OnShowCraftAllMissing?.Invoke(data, isNewDeck);
    }
    #endregion

    #region Manage Floating Card
    public void RequestActivateFloatingCard(DropZoneName startZone, GameObject targetObject, CardUIData card, bool updatePosition = false)
    {
        OnActivateFloatingCard?.Invoke(startZone, targetObject, card, updatePosition);

        switch (startZone)
        {
            case DropZoneName.CollectionCard:
            {
                OnActivateFloatingCardFromCard?.Invoke();
            }
            break;

            case DropZoneName.CollectionDeckDetail:
            {
                OnActivateFloatingCardFromDeck?.Invoke();
            }
            break;
        }
    }

    public void RequestDeactivateFloatingCard(bool isSuccess)
    {
        OnDeactivateFloatingCard?.Invoke(isSuccess);
    }
    #endregion

    #region Manage Card Zone
    public void RequestUpdateCardZone(DropZoneName zoneName, string cardFullID)
    {
        OnUpdateCardZone?.Invoke(zoneName, cardFullID);
    }

    public void RequestUpdateCardDropZone(DropZoneName zoneName)
    {
        OnUpdateCardDropZone?.Invoke(zoneName);
    }

    public void RequestSuccessUpdateCardZone(DropZoneName zoneName, string cardFullID)
    {
        OnSuccessUpdateCardZone?.Invoke(zoneName, cardFullID);
    }

    public void RequestOnUpdateDeckDetailComplete()
    {
        OnUpdateDeckDetailComplete?.Invoke();
    }
    #endregion

    public void RequestUpdateCollectionFilter()
    {
        OnUpdateCollectionFilter?.Invoke();
    }
    #endregion

    #region Hold Position
    public void SetHoldPosition(Vector3 position)
    {
        _holdPosition = position;
    }

    public Vector3 GetHoldPosition()
    {
        return _holdPosition;
    }
    #endregion


    #endregion
}