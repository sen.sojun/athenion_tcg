﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Karamucho.Collection
{
    public class CollectionDeckManager : MonoBehaviour
    {
        #region Public Properties
        public CollectionDataManager Data { get { return _data; } }
        #endregion

        #region Private Properties
        private CollectionDataManager _data;
        private string _currentDeckID = string.Empty;
        private string _newDeckID = string.Empty;
        #endregion

        #region Events
        public static event Action<CollectionDeckManager> OnInit;
        public static event Action<CardListData, bool> OnShowDeckDetailCallback;
        public static event Action<CardListData> OnUpdateDeckDetailCallback;
        public static event Action OnHideDeckDetail;
        public static event Action OnUpdateCurrentDeckDetail;
        public static event Action OnUpdateCollectionCardDataList;
        public static event Action<DeckData> OnCreateNewDeck;
        public static event Action<DeckData> OnSaveDeck;
        public static event Action OnSaveDeckConfirm;
        public static event Action OnSaveDeckCancel;
        public static event Action<DeckData> OnRemoveDeck;
        public static event Action OnSaveNewDeck;
        #endregion

        #region Methods
        private void OnEnable()
        {
            InitEvents();
        }

        private void OnDisable()
        {
            DeinitEvents();
        }

        private void InitEvents()
        {
            CollectionManager.OnInit += Init;
            CollectionManager.OnUpdateCardZone += UpdateCardZone;
            CollectionManager.OnSuccessUpdateCardZone += SuccessUpdateCardZone;
            CollectionManager.OnHideDeckDetail += HideDeckDetail;
            CollectionManager.OnCreateNewDeck += CreateNewDeck;
            CollectionManager.OnCraftCardComplete += UpdateCollectionCardDataList;
        }

        private void DeinitEvents()
        {
            CollectionManager.OnInit -= Init;
            CollectionManager.OnUpdateCardZone -= UpdateCardZone;
            CollectionManager.OnSuccessUpdateCardZone -= SuccessUpdateCardZone;
            CollectionManager.OnHideDeckDetail -= HideDeckDetail;
            CollectionManager.OnCreateNewDeck -= CreateNewDeck;
            CollectionManager.OnCraftCardComplete -= UpdateCollectionCardDataList;
        }

        public void Init()
        {
            InitData();

            OnInit?.Invoke(this);
        }

        private void InitData()
        {
            _data = CollectionManager.Instance.Data;
        }

        public DeckData GetCurrentDeckData()
        {
            _data.TryGetDeckData(_currentDeckID, out DeckData deck);
            return deck;
        }

        private int GetCardAmoutInCurrentDeck(string fullID)
        {
            string[] cardIDSplit = fullID.Split('_', ':');
            string baseID = cardIDSplit[0];

            int result = 0;
            DeckData deck = GetCurrentDeckData();
            CardData card;
            foreach (KeyValuePair<string, int> item in deck.Deck)
            {
                card = CardData.CreateCard(item.Key);
                if (card.BaseID == baseID)
                {
                    result += item.Value;
                }
            }

            return result;
        }

        private void AddCardIntoDeck(string deckID, string cardFullID, int amount)
        {
            _data.AddCardIntoDeck(deckID, cardFullID, amount);
        }

        private void RemoveCardFromDeck(string deckID, string cardFullID, int amount)
        {
            _data.RemoveCardFromDeck(deckID, cardFullID, amount);
        }

        private void UpdateCardZone(DropZoneName zoneName, string cardFullID)
        {
            switch (zoneName)
            {
                case DropZoneName.CollectionDeckDetail:
                {
                    if(_currentDeckID == string.Empty)
                    {
                        return;
                    }

                    if (GetCardAmoutInCurrentDeck(cardFullID) >= GameHelper.MaxCardPerBaseID)
                    {
                        PopupUIManager.Instance.ShowPopup_ConfirmOnlyWithWarning(
                            LocalizationManager.Instance.GetText("ERROR_TITLE_COLLECTION_DECK_CARD_MAX")
                            , LocalizationManager.Instance.GetText("ERROR_TEXT_COLLECTION_DECK_CARD_MAX")
                        );
                        return;
                    }

                    CollectionManager.Instance.RequestSuccessUpdateCardZone(zoneName, cardFullID);
                    AddCardIntoDeck(_currentDeckID, cardFullID, 1);
                    OnUpdateCurrentDeckDetail?.Invoke();
                }
                break;
            }
        }

        private void SuccessUpdateCardZone(DropZoneName zoneName, string cardFullID)
        {
            switch (zoneName)
            {
                case DropZoneName.CollectionCard:
                {
                    RemoveCardFromDeck(_currentDeckID, cardFullID, 1);
                    OnUpdateCurrentDeckDetail?.Invoke();
                }
                break;
            }
        }

        private void CreateNewDeck()
        {
            if (_data.IsDeckSlotFull())
            {
                string title = LocalizationManager.Instance.GetText("ERROR_TITLE_COLLECTION_DECK_LIST_FULL");
                string message = LocalizationManager.Instance.GetText("ERROR_TEXT_COLLECTION_DECK_LIST_FULL");
                PopupUIManager.Instance.ShowPopup_Error(title, message);
                return;
            }

            // TODO: HERO SELECTION

            DateTime nowDateTime = DateTimeData.GetDateTimeUTC();
            _currentDeckID = string.Format("deck_{0}{1}{2}{3}{4}{5}{6}"
                , nowDateTime.Year.ToString("D4")
                , nowDateTime.Month.ToString("D2")
                , nowDateTime.Day.ToString("D2")
                , nowDateTime.Hour.ToString("D2")
                , nowDateTime.Minute.ToString("D2")
                , nowDateTime.Second.ToString("D2")
                , nowDateTime.Millisecond.ToString("D2")
            );

            _newDeckID = _currentDeckID;
            DeckData newDeck = new DeckData(_currentDeckID, "New Deck", "FIRE", "H0000", "CB0000", "HUD0000", new CardListData());
            _data.AddDeck(newDeck);

            OnCreateNewDeck?.Invoke(newDeck);
        }

        private void UpdateCollectionCardDataList()
        {
            OnUpdateCollectionCardDataList?.Invoke();
        }

        private void HideDeckDetail()
        {
            OnHideDeckDetail?.Invoke();
        }

        #region Request Methods
        public void RequestShowDeckDetail(string deckID)
        {
            _currentDeckID = deckID;
            CollectionManager.Instance.RequestShowDeckDetail(GetCurrentDeckData(), false);

            CardListData cardList = _data.RefCardDataStack;
            OnShowDeckDetailCallback?.Invoke(cardList, false);
        }

        public void RequestCreateNewDeck(DeckData deck)
        {
            CollectionManager.Instance.RequestShowDeckDetail(deck, true);
            _data.SetExistingDeckData(deck);

            CardListData cardList = _data.RefCardDataStack;
            deck.RefreshDeck(cardList);
            OnShowDeckDetailCallback?.Invoke(cardList, true);
        }

        public void RequestCancelNewDeck(DeckData deck)
        {
            _data.RemoveDeck(deck);
        }

        public void RequestRemoveCardFromCurrentDeck(string cardFullID)
        {
            CollectionManager.Instance.RequestUpdateCardZone(DropZoneName.CollectionCard, cardFullID);
        }

        public void RequestShowCardDetail(string cardFullID)
        {
            CollectionManager.Instance.RequestShowCardDetail(cardFullID);
        }

        public void RequestShowCardDetail_Hover(string cardFullID)
        {
            CollectionManager.Instance.RequestShowCardDetail_Hover(cardFullID);
        }

        public void RequestHideCardDetail_Hover()
        {
            CollectionManager.Instance.RequestHideCardDetail_Hover();
        }

        public void RequestUpdateCurrentDeckData(DeckData data)
        {
            _data.SetExistingDeckData(data);
            OnUpdateCurrentDeckDetail?.Invoke();
        }

        public void RequestUpdateCurrentDeckDetail()
        {
            RequestUpdateDeckDetail(GetCurrentDeckData());
        }

        public void RequestAutoDeck()
        {
            DeckData deck = GetCurrentDeckData();
            deck.AutoDeck();
            RequestUpdateDeckDetail(deck);
        }

        public void RequestShowCraftAllMissing(bool isNewDeck)
        {
            CollectionManager.Instance.RequestShowCraftAllMissing(GetCurrentDeckData(), isNewDeck);
        }

        private void RequestUpdateDeckDetail(DeckData deck)
        {
            if(deck == null)
            {
                deck = new DeckData();
            }

            CollectionManager.Instance.RequestUpdateDeckDetail(deck);
            CardListData cardList = _data.RefCardDataStack;
            OnUpdateDeckDetailCallback?.Invoke(cardList);
        }

        #region Save Deck
        public void RequestSaveCurrentDeckDetail()
        {
            List<DeckData> deckList = DataManager.Instance.GetAllDeck();
            DeckData deck = GetCurrentDeckData();
            DeckData refDeck = deckList.Find(x => x.DeckID == _currentDeckID);
            deck.Deck.ClearEmptyCards();
            if (refDeck == null)
            {
                refDeck = new DeckData(deck);
                refDeck.Deck.Clear();
            }

            bool isChanged = refDeck.ToJson() != deck.ToJson();
            if (isChanged)
            {
                string title = LocalizationManager.Instance.GetText("COLLECTION_TITLE_SURE_SAVE_DECK");
                string subTitle = LocalizationManager.Instance.GetText("COLLECTION_TEXT_SURE_SAVE_DECK");
                PopupUIManager.Instance.ShowPopup_SureCheck(
                    title
                    , subTitle
                    , delegate ()
                    {
                        SaveDeckDetail(deck);
                        OnSaveDeckConfirm?.Invoke();
                    }
                    , delegate()
                    {
                        OnSaveDeckCancel?.Invoke();
                    }
                );
            }
            else
            {
                OnSaveDeckComplete();
            }
        }

        public void RequestCancelCurrentDeckDetail()
        {
            List<DeckData> deckList = DataManager.Instance.GetAllDeck();
            DeckData deck = GetCurrentDeckData();
            DeckData refDeck = deckList.Find(x => x.DeckID == _currentDeckID);
            deck.Deck.ClearEmptyCards();
            if (refDeck == null)
            {
                refDeck = new DeckData(deck);
                refDeck.Deck.Clear();
            }

            bool isChanged = refDeck.ToJson() != deck.ToJson();
            if (isChanged)
            {
                string title = LocalizationManager.Instance.GetText("COLLECTION_TITLE_SURE_CANCEL_EDIT_DECK");
                string subTitle = LocalizationManager.Instance.GetText("COLLECTION_TEXT_SURE_CANCEL_EDIT_DECK");
                PopupUIManager.Instance.ShowPopup_SureCheck(
                    title
                    , subTitle
                    , delegate ()
                    {
                        OnSaveDeckComplete();
                    }
                    , null
                );
            }
            else
            {
                OnSaveDeckComplete();
            }
        }

        private void SaveDeckDetail(DeckData deck)
        {
            UnityAction<DeckData, bool> onSaveComplete = delegate (DeckData data, bool newDeck)
            {
                if(newDeck)
                {
                    OnSaveNewDeck?.Invoke();
                }

                OnSaveDeck?.Invoke(data);

                PopupUIManager.Instance.Show_MidSoftLoad(false);
                OnSaveDeckComplete();
            };

            UnityAction<string> onSaveFail = delegate (string error)
            {
                string title = LocalizationManager.Instance.GetText("COLLECTION_ERROR_SAVE_DECK");
                PopupUIManager.Instance.Show_MidSoftLoad(false);
                PopupUIManager.Instance.ShowPopup_Error(title, error);
            };

            PopupUIManager.Instance.Show_MidSoftLoad(true);
            bool isNewDeck = _newDeckID != string.Empty;
            if (!isNewDeck)
            {
                DataManager.Instance.SaveDeck(
                    deck.DeckID
                    , deck
                    , delegate()
                    {
                        onSaveComplete(deck, isNewDeck);
                    }
                    , onSaveFail
                );
            }
            else
            {
                DataManager.Instance.AddDeck(
                    deck
                    , delegate ()
                    {
                        onSaveComplete(deck, isNewDeck);
                    }
                    , onSaveFail
                );
            }
        }

        private void OnSaveDeckComplete()
        {
            _currentDeckID = string.Empty;
            _newDeckID = string.Empty;

            CollectionManager.Instance.RequestHideDeckDetail();
        }
        #endregion

        #region Remove Deck
        public void RequestRemoveCurrentDeck()
        {
            string title = LocalizationManager.Instance.GetText("COLLECTION_TITLE_SURE_REMOVE_DECK");
            string subTitle = LocalizationManager.Instance.GetText("COLLECTION_TEXT_SURE_REMOVE_DECK");
            DeckData deck = GetCurrentDeckData();
            PopupUIManager.Instance.ShowPopup_SureCheck(
                    title
                    , subTitle
                    , delegate ()
                    {
                        RemoveDeck(deck);
                    }
                    , null
                );
        }

        private void RemoveDeck(DeckData deck)
        {
            UnityAction<DeckData> onRemoveComplete = delegate (DeckData data)
            {
                PopupUIManager.Instance.Show_MidSoftLoad(false);
                OnRemoveDeck?.Invoke(data);
                OnRemoveDeckComplete();
            };

            UnityAction<string> onRemoveFail = delegate (string error)
            {
                PopupUIManager.Instance.Show_MidSoftLoad(false);
                PopupUIManager.Instance.ShowPopup_Error("COLLECTION_ERROR_REMOVE_DECK", error);
            };

            PopupUIManager.Instance.Show_MidSoftLoad(true);
            DataManager.Instance.RemoveDeck(
                deck.DeckID
                , delegate ()
                {
                    onRemoveComplete(deck);
                }
                , onRemoveFail
            );
        }

        private void OnRemoveDeckComplete()
        {
            _currentDeckID = string.Empty;
            _newDeckID = string.Empty;

            CollectionManager.Instance.RequestHideDeckDetail();
        }
        #endregion

        #region Clear Deck
        public void RequestClearCurrentDeck()
        {
            DeckData data = GetCurrentDeckData();
            data.Deck.Clear();
            RequestUpdateDeckDetail(data);
        }
        #endregion

        #region Manage Floating Card
        public void RequestActivateFloatingCard(DropZoneName startZone, GameObject targetObject, CardUIData card, bool updatePosition = false)
        {
            CollectionManager.Instance.RequestActivateFloatingCard(startZone, targetObject, card, updatePosition);
        }

        public void RequestDeactivateFloatingCard()
        {
            CollectionManager.Instance.RequestDeactivateFloatingCard(true);
        }
        #endregion

        public void RequestOnUpdateDeckDetailComplete()
        {
            CollectionManager.Instance.RequestOnUpdateDeckDetailComplete();
        }

        #endregion

        #endregion
    }
}