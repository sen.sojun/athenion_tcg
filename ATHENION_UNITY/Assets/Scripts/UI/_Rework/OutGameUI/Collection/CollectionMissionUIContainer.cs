﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectionMissionUIContainer : MonoBehaviour
{
    #region Inspector Properties
    [Header("Button Group")]
    public Button BTN_CreateDeck;
    public Button BTN_CreateDeck_CustomDeck;
    public Button BTN_CreateDeck_Confirm;
    public List<Button> BTN_CreateDeck_Faction;
    public Button BTN_CreateDeck_Faction_Holy;
    public Button BTN_Customize_Confirm;
    public Button BTN_DeckDetail_AutoDeck;
    public Button BTN_Filter_CraftCard;
    public Button BTN_CardDetail_Recycle;
    public Button BTN_CardDetail_Craft;
    public Button BTN_CardDetail_Enchant;
    public Button BTN_DeckDetail_Done;

    [Header("Area Group")]
    public RectTransform AREA_CreateDeck_FactionList;
    public RectTransform AREA_CreateDeck_DeckPresetList;
    public RectTransform AREA_CreateDeck_DeckPresetBTNList;
    public RectTransform AREA_DeckDetail_CardList;
    public RectTransform AREA_CardCollection;
    public RectTransform AREA_CardDetail_Recycle;
    public RectTransform AREA_CardDetail_Craft;
    public RectTransform AREA_CardDetail_Close;
    #endregion
}