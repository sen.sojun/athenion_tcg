﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Karamucho.Collection
{
    public class CollectionDeckUIManager : MonoBehaviour
    {
        #region Inspector Properties
        [Header("Deck Creation")]
        [SerializeField] private CollectionCreateDeck _DeckInit;

        [Header("Card Detail")]
        [SerializeField] private CardUI_DetailHover _DetailHoverCard;
        #endregion

        #region Public Properties
        #endregion

        #region Private Properties
        private CollectionDeckManager _manager;
        #endregion

        #region Events
        public static event Action<CollectionDeckUIManager> OnInit;

        public static event Action OnShowDeckDetail;
        public static event Action<CardListData, bool> OnShowDeckDetailCallback;
        public static event Action<CardListData> OnUpdateDeckDetailCallback;

        public static event Action OnHideDeckDetail;
        public static event Action OnUpdateDeckDetail;
        public static event Action OnUpdateCollectionCardDataList;
        #endregion

        #region Methods
        private void OnEnable()
        {
            InitEvents();
        }

        private void OnDisable()
        {
            DeinitEvents();
        }

        private void InitEvents()
        {
            CollectionDeckManager.OnInit += Init;
            CollectionDeckManager.OnUpdateCurrentDeckDetail += UpdateCurrentDeckDetail;
            CollectionDeckManager.OnShowDeckDetailCallback += ShowDeckDetailCallback;
            CollectionDeckManager.OnUpdateDeckDetailCallback += UpdateDeckDetailCallback;
            CollectionDeckManager.OnHideDeckDetail += HideDeckDetail;
            CollectionDeckManager.OnUpdateCollectionCardDataList += UpdateCollectionCardDataList;
            CollectionDeckManager.OnCreateNewDeck += CreateNewDeck;

            _DeckInit.OnCreateNewDeck += ConfirmCreateNewDeck;
            _DeckInit.OnCancelNewDeck += CancelCreateNewDeck;
        }

        private void DeinitEvents()
        {
            CollectionDeckManager.OnInit -= Init;
            CollectionDeckManager.OnUpdateCurrentDeckDetail -= UpdateCurrentDeckDetail;
            CollectionDeckManager.OnShowDeckDetailCallback -= ShowDeckDetailCallback;
            CollectionDeckManager.OnUpdateDeckDetailCallback -= UpdateDeckDetailCallback;
            CollectionDeckManager.OnHideDeckDetail -= HideDeckDetail;
            CollectionDeckManager.OnUpdateCollectionCardDataList -= UpdateCollectionCardDataList;
            CollectionDeckManager.OnCreateNewDeck -= CreateNewDeck;

            _DeckInit.OnCreateNewDeck -= ConfirmCreateNewDeck;
            _DeckInit.OnCancelNewDeck -= CancelCreateNewDeck;
        }

        private void Init(CollectionDeckManager manager)
        {
            _manager = manager;

            OnInit?.Invoke(this);
        }

        #region Get Methods
        public List<DeckData> GetDeckDataList()
        {
            List<DeckData> result = new List<DeckData>();
            if(_manager != null)
            {
                result = new List<DeckData>(_manager.Data.DeckDataList);
            }

            return result;
        }

        public int GetDeckSlotAmount()
        {
            int result = 0;
            if (_manager != null)
            {
                result = _manager.Data.DeckSlotAmount;
            }

            return result;
        }

        public DeckData GetCurrentDeckData()
        {
            return _manager.GetCurrentDeckData();
        }
        #endregion

        private void UpdateCurrentDeckDetail()
        {
            OnUpdateDeckDetail?.Invoke();
        }

        private void ShowDeckDetailCallback(CardListData data, bool isNewDeck)
        {
            OnShowDeckDetailCallback?.Invoke(data, isNewDeck);
        }

        private void UpdateDeckDetailCallback(CardListData data)
        {
            OnUpdateDeckDetailCallback?.Invoke(data);
        }

        private void HideDeckDetail()
        {
            OnHideDeckDetail?.Invoke();
        }

        private void UpdateCollectionCardDataList()
        {
            OnUpdateCollectionCardDataList?.Invoke();
        }

        private void CreateNewDeck(DeckData deck)
        {
            _DeckInit.ShowUI(deck);
        }

        private void ConfirmCreateNewDeck(DeckData deck)
        {
            _manager.RequestCreateNewDeck(deck);
        }

        private void CancelCreateNewDeck(DeckData deck)
        {
            _manager.RequestCancelNewDeck(deck);
        }

        #region Request Methods
        public void RequestShowDeckDetail(string deckID)
        {
            OnShowDeckDetail?.Invoke();
            _manager.RequestShowDeckDetail(deckID);
        }

        public void RequestSaveCurrentDeckDetail()
        {         
            _manager.RequestSaveCurrentDeckDetail();
        }

        public void RequestCancelCurrentDeckDetail()
        {
            _manager.RequestCancelCurrentDeckDetail();
        }

        public void RequestActivateFloatingCard(DropZoneName startZone, GameObject targetObject, CardUIData card, bool updatePosition = false)
        {
            _manager.RequestActivateFloatingCard(startZone, targetObject, card, updatePosition);
        }

        public void RequestDeactivateFloatingCard()
        {
            _manager.RequestDeactivateFloatingCard();
        }

        public void RequestRemoveCardFromCurrentDeck(string cardFullID)
        {
            _manager.RequestRemoveCardFromCurrentDeck(cardFullID);
        }

        public void RequestShowCardDetail(string cardFullID)
        {
            _manager.RequestShowCardDetail(cardFullID);
        }

        public void RequestShowCardDetail_Hover(string cardFullID)
        {
            _manager.RequestShowCardDetail_Hover(cardFullID);
        }

        public void RequestHideCardDetail_Hover()
        {
            _manager.RequestHideCardDetail_Hover();
        }

        public void RequestClearCurrentDeckDetail()
        {
            _manager.RequestClearCurrentDeck();
        }

        public void RequestUpdateCurrentDeckData(DeckData data)
        {
            _manager.RequestUpdateCurrentDeckData(data);
        }

        public void RequestUpdateCurrentDeckDetail()
        {
            _manager.RequestUpdateCurrentDeckDetail();
        }

        public void RequestRemoveCurrentDeck()
        {
            _manager.RequestRemoveCurrentDeck();
        }

        public void RequestAutoDeck()
        {
            _manager.RequestAutoDeck();
        }

        public void RequestShowCraftAllMissing(bool isNewDeck)
        {
            _manager.RequestShowCraftAllMissing(isNewDeck);
        }

        public void RequestOnUpdateDeckDetailComplete()
        {
            _manager.RequestOnUpdateDeckDetailComplete();
        }
        #endregion

        #endregion
    }
}