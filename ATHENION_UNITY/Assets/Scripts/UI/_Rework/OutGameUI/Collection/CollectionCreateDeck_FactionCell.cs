﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.Events;

namespace Karamucho.Collection
{
    public class CollectionCreateDeck_FactionCell : MonoBehaviour
    {
        #region Inspecstors
        [SerializeField] private CardElementType _ElementType;
        [Header("UI")]
        [SerializeField] private Image _Glow;
        [SerializeField] private Button _BTN_Click;
        #endregion

        #region Private Properties
        private Action<CardElementType> _onClick;
        #endregion

        #region Methods
        private void OnEnable()
        {
            _BTN_Click.onClick.RemoveAllListeners();
            _BTN_Click.onClick.AddListener(OnClick);
        }

        private void OnClick()
        {
            _onClick?.Invoke(_ElementType);
        }

        public void SetOnClick(Action<CardElementType> onClick)
        {
            _onClick = onClick;
        }

        public void SetSelected(CardElementType element)
        {
            _Glow.gameObject.SetActive(element == _ElementType);
        }
        #endregion
    }
}
