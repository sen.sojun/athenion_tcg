﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Karamucho.Collection
{
    public class CollectionDeckListUIManager : MonoBehaviour
    {
        #region Inspector Properties
        [Header("Main")]
        [SerializeField] private GameObject _DeckListGroup;
        [SerializeField] private ScrollRect _DeckListScrollRect;
        [SerializeField] private Transform _DeckListContainer;
        #endregion

        #region Public Properties
        #endregion

        #region Private Properties
        private CollectionDeckUIManager _manager;
        private List<CollectionDeckCell> _deckCellList = new List<CollectionDeckCell>();
        private int _deckSlotCurrentAmount = 0;
        private int _deckSlotMaxAmount = 0;
        private bool _isDragScroll = false;
        #endregion

        #region Methods
        private void OnEnable()
        {
            InitEvents();
        }

        private void OnDisable()
        {
            DeinitEvents();
        }

        private void InitEvents()
        {
            CollectionDeckUIManager.OnInit += Init;
            CollectionDeckUIManager.OnShowDeckDetail += ShowDeckDetail;
            CollectionDeckUIManager.OnHideDeckDetail += HideDeckDetail;
            CollectionDeckUIManager.OnUpdateCollectionCardDataList += UpdateData;
        }

        private void DeinitEvents()
        {
            CollectionDeckUIManager.OnInit -= Init;
            CollectionDeckUIManager.OnShowDeckDetail -= ShowDeckDetail;
            CollectionDeckUIManager.OnHideDeckDetail -= HideDeckDetail;
            CollectionDeckUIManager.OnUpdateCollectionCardDataList -= UpdateData;
        }

        private void Init(CollectionDeckUIManager manager)
        {
            _manager = manager;
            InitData();
        }

        private void InitData()
        {
            List<DeckData> deckDataList = _manager.GetDeckDataList();
            _deckSlotMaxAmount = _manager.GetDeckSlotAmount();
            _deckSlotCurrentAmount = deckDataList.Count;

            int index = 0;
            foreach(DeckData deck in deckDataList)
            {
                InitDeckCell();
                SetupDeckCell(_deckCellList[index], deck);
                index++;
            }
        }

        private void UpdateData()
        {
            List<DeckData> deckDataList = _manager.GetDeckDataList();
            _deckSlotMaxAmount = _manager.GetDeckSlotAmount();
            _deckSlotCurrentAmount = deckDataList.Count;
            int index = 0;
            foreach (DeckData deck in deckDataList)
            {
                if(_deckCellList.Count <= index)
                {
                    InitDeckCell();
                }

                SetupDeckCell(_deckCellList[index], deck);
                _deckCellList[index].gameObject.SetActive(true);
                index++;
            }

            for(int i = index; i < _deckCellList.Count; i++)
            {
                _deckCellList[i].gameObject.SetActive(false);
            }
        }

        private void InitDeckCell()
        {
            CollectionItem item = CollectionPool.Instance.CreateCollectionDeck();
            CollectionDeckCell deckCell = item.GetComponent<CollectionDeckCell>();

            // Setup CardUI position
            deckCell.transform.SetParent(_DeckListContainer);
            deckCell.transform.localScale = Vector3.one;
            deckCell.transform.localRotation = Quaternion.identity;

            // subscribe event
            deckCell.OnBeginDragEvent += OnDeckList_BeginDrag;
            deckCell.OnDragEvent += OnDeckList_Drag;
            deckCell.OnEndDragEvent += OnDeckList_EndDrag;

            _deckCellList.Add(deckCell);
        }

        private void SetupDeckCell(CollectionDeckCell deckCell, DeckData data)
        {
            deckCell.ShowDeckError(DataManager.Instance.IsLocalDeckValid(data) != DeckValidStatus.Valid);
            deckCell.SetupData(data);
            deckCell.SetOnClickEvent(
                delegate ()
                {
                    if (_isDragScroll)
                    {
                        return;
                    }

                    _manager.RequestShowDeckDetail(deckCell.Data.DeckID);
                }
            );
        }

        private void HideDeckDetail()
        {
            UpdateData();
            _DeckListGroup.SetActive(true);
        }

        private void ShowDeckDetail()
        {
            _DeckListGroup.SetActive(false);
        }
        #endregion

        #region Event ScrollDeck
        public void OnDeckList_BeginDrag(PointerEventData eventData)
        {
            _isDragScroll = true;
            _DeckListScrollRect.OnBeginDrag(eventData);
        }

        public void OnDeckList_Drag(PointerEventData eventData)
        {
            _DeckListScrollRect.OnDrag(eventData);
        }

        public void OnDeckList_EndDrag(PointerEventData eventData)
        {
            _isDragScroll = false;
            _DeckListScrollRect.OnEndDrag(eventData);
        }
        #endregion
    }
}