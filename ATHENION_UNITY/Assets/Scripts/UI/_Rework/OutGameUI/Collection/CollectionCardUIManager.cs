﻿using DG.Tweening;
using Karamucho.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Karamucho.Collection
{
    public class CollectionCardUIManager : MonoBehaviour
    {
        #region Inspector Properties
        [Header("Button")]
        [SerializeField] private Button _BTN_PreviousPage;
        [SerializeField] private Button _BTN_NextPage;
        [SerializeField] private Button _BTN_RecycleAllCards;
        [SerializeField] private Button _BTN_CraftCards;
        [SerializeField] private Button _BTN_CraftDeckCards;

        [Header("Table")]
        [SerializeField] private Transform _START_INDEX;
        [SerializeField] private Transform _FullTableGroup;

        [Header("Card Detail")]
        [SerializeField] private DetailCard_Collection _DetailCard;
        [SerializeField] private CardUI_DetailHover _DetailHoverCard;

        [Header("Recycle Duplicate")]
        [SerializeField] private PopupRecycleDuplicate _RecycleDuplicate;

        [Header("Craft All Missing")]
        [SerializeField] private PopupCraftMissingCard _PopupCraftMissingCard;
        #endregion

        #region Private Properties
        private CollectionCardManager _manager;
        private List<CardUI_Collection> _currentCardUIList;

        private RectTransform _rectTransformTable;
        private GridLayoutGroup _gridTableLayout;

        private int _currentPage = 0;
        private int _maxPage = 0;
        private Coroutine _updateCardDataRoutine = null;
        private PlayerInventoryData _inventory;

        private Vector3 _holdPosition;
        #endregion

        #region Events
        public static event Action OnCancelNewDeckCraftAllMissing;
        #endregion

        #region Methods
        private void OnEnable()
        {
            InitEvents();
        }

        private void OnDisable()
        {
            DeinitEvent();
        }

        private void InitEvents()
        {
            CollectionCardManager.OnInit += Init;
            CollectionCardManager.OnShowDeckDetail += MinimizeCollectionCardTable;
            CollectionCardManager.OnHideDeckDetail += MaximizeCollectionCardTable;
            CollectionCardManager.OnUpdateResetCardDataList += ResetCardPage;
            CollectionCardManager.OnUpdateCardDataList += UpdateCardPage;
            CollectionCardManager.OnShowCardDetail += ShowCardDetail;
            CollectionCardManager.OnShowCardDetail_Hover += ShowCardDetail_Hover;
            CollectionCardManager.OnHideCardDetail_Hover += HideCardDetail_Hover;

            CollectionCardManager.OnCraftComplete += OnCraftCardComplete;
            CollectionCardManager.OnCraftFail += OnCraftCardFail;
            CollectionCardManager.OnRecycleAllComplete += OnRecycleAllComplete;
            CollectionCardManager.OnShowCraftAllMissing += ShowCraftAllMissing;
            CollectionCardManager.OnCraftAllMissingComplete += OnCraftAllMissingComplete;

            // Craft Detail
            _DetailCard.OnHideUI += HideCardDetail;
            _DetailCard.OnCraftCard += CraftCard;
            _DetailCard.OnRecycleCard += RecycleCard;
            _DetailCard.OnTransformCard += TransformCard;
        }

        private void DeinitEvent()
        {
            CollectionCardManager.OnInit -= Init;
            CollectionCardManager.OnShowDeckDetail -= MinimizeCollectionCardTable;
            CollectionCardManager.OnHideDeckDetail -= MaximizeCollectionCardTable;
            CollectionCardManager.OnUpdateResetCardDataList -= ResetCardPage;
            CollectionCardManager.OnUpdateCardDataList -= UpdateCardPage;
            CollectionCardManager.OnShowCardDetail -= ShowCardDetail;
            CollectionCardManager.OnShowCardDetail_Hover -= ShowCardDetail_Hover;
            CollectionCardManager.OnHideCardDetail_Hover -= HideCardDetail_Hover;

            CollectionCardManager.OnCraftComplete -= OnCraftCardComplete;
            CollectionCardManager.OnCraftFail -= OnCraftCardFail;
            CollectionCardManager.OnRecycleAllComplete -= OnRecycleAllComplete;
            CollectionCardManager.OnShowCraftAllMissing -= ShowCraftAllMissing;
            CollectionCardManager.OnCraftAllMissingComplete -= OnCraftAllMissingComplete;

            _DetailCard.OnHideUI -= HideCardDetail;
            _DetailCard.OnCraftCard -= CraftCard;
            _DetailCard.OnRecycleCard -= RecycleCard;
            _DetailCard.OnTransformCard -= TransformCard;
        }

        private void Init(CollectionCardManager manager)
        {
            // Assign Manager
            _manager = manager;

            // Init CardUI List
            _currentCardUIList = new List<CardUI_Collection>();

            // Get Grid
            _gridTableLayout = _FullTableGroup.GetComponent<GridLayoutGroup>();
            _rectTransformTable = _FullTableGroup.GetComponent<RectTransform>();

            _inventory = DataManager.Instance.InventoryData;
            InitButtons();
            InitCardPage();

            StartCoroutine(IEInit());
        }

        private IEnumerator IEInit()
        {
            yield return new WaitForEndOfFrame();
            //InitCardPage();
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
        }

        private void InitCardPage()
        {
            int cellCount = GetTableCellCount();
            Debug.Log("CellCount:" + cellCount);

            List<CardData> nextCardDataList = _manager.GetCurrentCardDataList((_currentPage + 1), cellCount);
            Debug.Log("NextCardDataList:" + nextCardDataList.Count);

            _BTN_NextPage.gameObject.SetActive(nextCardDataList.Count > 0);

            for(int i = 0; i < cellCount; i++)
            {
                InitCollectionCard();
            }

            UpdateCardPage();
        }

        private void UpdateCardPage()
        {
            if(_updateCardDataRoutine != null)
            {
                StopCoroutine(_updateCardDataRoutine);
            }

            _updateCardDataRoutine = StartCoroutine(IEUpdateCardPage());
        }

        private IEnumerator IEUpdateCardPage()
        {
            int tableCount = GetTableCellCount();
            List<CardData> currentCardDataList = _manager.GetCurrentCardDataList((_currentPage), tableCount);
            List<CardData> nextCardDataList = _manager.GetCurrentCardDataList((_currentPage + 1), tableCount);

            _BTN_PreviousPage.gameObject.SetActive(_currentPage > 0);
            _BTN_NextPage.gameObject.SetActive(nextCardDataList.Count > 0);

            //TODO: Update UI
            foreach(CardUI_Collection card in _currentCardUIList)
            {
                card.gameObject.SetActive(false);
            }

            string cardbackID = DataManager.Instance.DefaultCardBackIDList[0];
            for (int i = 0; i < currentCardDataList.Count; i++)
            {
                CardUIData data = new CardUIData(0, currentCardDataList[i], cardbackID);
                if(_currentCardUIList.Count <= i)
                {
                    InitCollectionCard();
                }

                _currentCardUIList[i].SetData(data);
                _currentCardUIList[i].SetCardAmount(_manager.GetCardAmount(data.FullID));
                _currentCardUIList[i].SetNew(PlayerSeenListManager.Instance.IsNewCard(data.FullID));
                _currentCardUIList[i].gameObject.SetActive(true);
            }

            yield break;
        }

        private void InitCollectionCard()
        {
            CollectionItem item = CollectionPool.Instance.CreateCollectionCard();
            CardUI_Collection card = item.GetComponent<CardUI_Collection>();

            // Setup CardUI position
            card.transform.SetParent(_FullTableGroup);
            card.transform.SetSiblingIndex(_START_INDEX.transform.GetSiblingIndex());
            card.transform.localScale = Vector3.one;
            card.transform.localRotation = Quaternion.identity;

            //TODO: card animation

            // Subscibe events
            card.OnCardClick += OnCardTable_Click;
            card.OnCardDoubleClick += OnCardTable_DoubleClick;
            card.OnCardBeginDragEvent += OnCardTable_BeginDrag;
            card.OnCardEndDragEvent += OnCardTable_EndDrag;
            card.OnCardBeginHoldEvent += OnCardTable_BeginHold;
            card.OnCardEndHoldEvent += OnCardTable_EndHold;

            _currentCardUIList.Add(card);
        }

        private void InitButtons()
        {
            _BTN_NextPage.onClick.RemoveAllListeners();
            _BTN_NextPage.onClick.AddListener(NextCardPage);
            _BTN_NextPage.gameObject.SetActive(false);

            _BTN_PreviousPage.onClick.RemoveAllListeners();
            _BTN_PreviousPage.onClick.AddListener(PreviousCardPage);
            _BTN_PreviousPage.gameObject.SetActive(false);

            _BTN_RecycleAllCards.onClick.RemoveAllListeners();
            _BTN_RecycleAllCards.onClick.AddListener(ShowRecycleAllCards);

            bool isCanCraft = DataManager.Instance.IsFeatureEnable(FeatureUnlock.CardCrafting);
            _BTN_RecycleAllCards.interactable = isCanCraft;
            _BTN_CraftCards.interactable = isCanCraft;
            _BTN_CraftDeckCards.interactable = isCanCraft;

        }

        private void ResetCardPage()
        {
            // REset Page
            _currentPage = 0;
            UpdateCardPage();
        }

        private void NextCardPage()
        {
            // Next page
            _currentPage++;
            UpdateCardPage();
        }

        private void PreviousCardPage()
        {
            //Previous page
            _currentPage--;
            UpdateCardPage();
        }

        public void HideUI()
        {
            ClearUI();
        }

        private void ClearUI()
        {
            // Clear Card
            if (_currentCardUIList != null && _currentCardUIList.Count > 0)
            {
                for (int index = 0; index < _currentCardUIList.Count; ++index)
                {
                    Destroy(_currentCardUIList[index]);
                }
                _currentCardUIList.Clear();
            }

            CollectionPool.Instance.Clear();
        }

        private void ShowCardDetail(CardData data, int amount, CatalogCardData catalogData)
        {
            string cardbackID = DataManager.Instance.DefaultCardBackIDList[0];
            CardUIData uiData = new CardUIData(0, data, cardbackID);
            _DetailCard.UpdateCard(uiData, amount, catalogData);
            _DetailCard.Show();
        }

        private void HideCardDetail()
        {
            CardUI_Collection card = _currentCardUIList.Find(x => x.Data.FullID == _DetailCard.Data.FullID);
            if(card != null)
            {
                card.SetNew(PlayerSeenListManager.Instance.IsNewCard(card.Data.FullID));
            }
            _manager.RequestHideCardDetail();
        }

        private void ShowCardDetail_Hover(CardData data)
        {
            string cardbackID = DataManager.Instance.DefaultCardBackIDList[0];
            CardUIData uiData = new CardUIData(0, data, cardbackID);
            _DetailHoverCard.SetData(uiData, false);
            _DetailHoverCard.UpdatePosition(CollectionManager.Instance.GetHoldPosition());
            _DetailHoverCard.ShowCard(true);
        }

        private void HideCardDetail_Hover()
        {
            _DetailHoverCard.ShowCard(false);
        }

        #region Craft Card
        private void CraftCard(string cardFullID)
        {
            _manager.RequestCraftCard(cardFullID);
        }

        private void RecycleCard(string cardFullID)
        {
            _manager.RequestRecycleCard(cardFullID);
        }

        private void TransformCard(string cardFullID)
        {
            _manager.RequestTransformCard(cardFullID);
        }

        private void ShowRecycleAllCards()
        {
            _RecycleDuplicate.Show(
                _manager.GetCalculatedRecycleAllCardsPrice()
                , _manager.RequestRecycleAllCards
            );
        }

        private void ShowCraftAllMissing(DeckData data, bool isNewDeck)
        {
            _PopupCraftMissingCard.Show(
                _manager.GetCalculatedCraftAllMissingCardsPrice(data)
                , () => _manager.RequestCraftAllMissingCards(data)
                , () =>
                {
                    if(isNewDeck)
                    {
                        OnCancelNewDeckCraftAllMissing?.Invoke();
                    }
                }
            );
        }

        private void OnCraftCardComplete(CardData data, int amount, CatalogCardData catalogData)
        {
            string cardbackID = DataManager.Instance.DefaultCardBackIDList[0];
            CardUIData uiData = new CardUIData(0, data, cardbackID);
            _DetailCard.OnCraftComplete(uiData, amount, catalogData);
        }

        private void OnCraftCardFail()
        {
            _DetailCard.OnCraftFail();
        }

        private void OnRecycleAllComplete()
        {
            _RecycleDuplicate.Hide();
        }

        private void OnCraftAllMissingComplete()
        {
            _PopupCraftMissingCard.Hide();
        }
        #endregion

        #region Manage CardList Table
        private int GetTableCellCount()
        {
            RectOffset gridPadding = _gridTableLayout.padding;
            float tableWidth = _rectTransformTable.rect.size.x - gridPadding.left - gridPadding.right;
            float tableHeight = _rectTransformTable.rect.size.y - gridPadding.top - gridPadding.bottom;
            float cellWidth = _gridTableLayout.cellSize.x + _gridTableLayout.spacing.x;
            float cellHeight = _gridTableLayout.cellSize.y + _gridTableLayout.spacing.y;

            int columnCount = (int)(tableWidth/ cellWidth);
            int rowCount = (int)(tableHeight / cellHeight);

            return columnCount * rowCount;
        }

        private void MaximizeCollectionCardTable()
        {
            _rectTransformTable.RectTransformSetRight(0);
            ResetCardPage();
        }

        private void MinimizeCollectionCardTable()
        {
            float cellWidth = _gridTableLayout.cellSize.x;
            _rectTransformTable.RectTransformSetRight(cellWidth);
            ResetCardPage();
        }
        #endregion

        #endregion

        #region Drag Card Events
        private void OnCardTable_Click(CardUI_Collection card)
        {
            _manager.RequestShowCardDetail(card.Data.FullID);
        }

        private void OnCardTable_DoubleClick(CardUI_Collection card)
        {
            _manager.RequestRemoveCardFromTable(card.Data.FullID);
        }

        private void OnCardTable_BeginDrag(CardUI_Collection card)
        {
            _manager.RequestActivateFloatingCard(DropZoneName.CollectionCard, card.gameObject, card.Data, true);
        }

        private void OnCardTable_EndDrag(CardUI_Collection card)
        {
            _manager.RequestDeactivateFloatingCard(card.Data.FullID);
        }

        private void OnCardTable_BeginHold(CardUI_Collection card)
        {
            CollectionManager.Instance.SetHoldPosition(card.transform.position);
            _manager.RequestShowCardDetail_Hover(card.Data.FullID);
        }

        private void OnCardTable_EndHold(CardUI_Collection card)
        {
            CollectionManager.Instance.SetHoldPosition(Vector3.zero);
            _manager.RequestHideCardDetail_Hover();
        }
        #endregion
    }
}