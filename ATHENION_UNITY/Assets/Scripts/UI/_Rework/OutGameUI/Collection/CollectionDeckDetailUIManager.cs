﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Karamucho.UI;
using TMPro;
using System.Linq;
using System.Collections;

namespace Karamucho.Collection
{
    public class CollectionDeckDetailUIData
    {
        #region Public Properties
        public DeckData CurrentDeckData { get { return _currentDeckData; } }
        #endregion

        #region Private Properties
        private DeckData _currentDeckData = new DeckData();
        private CardListData _allCardDataStack = new CardListData();
        #endregion

        #region Contructors
        public CollectionDeckDetailUIData()
        { 
        }
        #endregion

        #region Methods
        public void SetCurrentDeckData(DeckData deck)
        {
            _currentDeckData = new DeckData(deck);
        }

        public void SetAllCardDataStack(CardListData data)
        {
            _allCardDataStack = new CardListData(data);
        }

        private int GetCardAmountInCollection(string cardFullID)
        {
            if(_allCardDataStack.ContainKey(cardFullID))
            {
                return _allCardDataStack[cardFullID];
            }

            return 0;
        }

        private int GetCardAmountInDeck(string cardFullID)
        {
            if (_currentDeckData.Deck.ContainKey(cardFullID))
            {
                return _currentDeckData.Deck[cardFullID];
            }

            return 0;
        }

        public int GetOwnedCardCount(string cardFullID)
        {
            int cardCount = GetCardAmountInDeck(cardFullID);
            int collectionCardCount = GetCardAmountInCollection(cardFullID);
            return Mathf.Min(collectionCardCount, cardCount);
        }

        public int GetNotOwnedCardCount(string cardFullID)
        {
            int cardCount = GetCardAmountInDeck(cardFullID);
            return cardCount - GetOwnedCardCount(cardFullID);
        }

        public CardListData GetSortedCurrentCardDataStack()
        {
            CardListData result = SortCardListData(_currentDeckData.Deck);
            return result;
        }

        private CardListData SortCardListData(CardListData data)
        {
            CardListData result = new CardListData();
            List<CardData> cardDataList = new List<CardData>();
            foreach (KeyValuePair<string, int> card in data)
            {
                cardDataList.Add(CardData.CreateCard(card.Key));
            }

            IEnumerable<CardData> query = cardDataList.OrderBy(card => card.Spirit).ThenBy(card => card.Rarity).ThenBy(card => card.BaseID);
            foreach (CardData card in query)
            {
                result.AddCard(card.FullID, data[card.FullID]);
            }

            return result;
        }
        #endregion
    }

    public class CollectionDeckDetailUIManager : MonoBehaviour
    {
        #region Inspector Properties
        [Header("Main")]
        [SerializeField] private GameObject _GROUP_MENU;
        [SerializeField] private GameObject _GROUP_CraftBar;
        [SerializeField] private GameObject _DeckDetailGroup;
        [SerializeField] private ScrollRect _DeckDetailScrollRect;
        [SerializeField] private Transform _DeckDetailContainer;
        [SerializeField] private CollectionDeckDetailUICustomize _DeckDetailCustomize;

        [Header("Deck Summary")]
        [SerializeField] private PopupDeckSummary _DeckSummary;

        [Header("Deck Stat")]
        [SerializeField] private Image ICON_Hero;
        [SerializeField] private TextMeshProUGUI TEXT_DeckName;
        [SerializeField] private TextMeshProUGUI TEXT_AP1;
        [SerializeField] private TextMeshProUGUI TEXT_AP2;
        [SerializeField] private TextMeshProUGUI TEXT_AP3;
        [SerializeField] private TextMeshProUGUI TEXT_CardInDeck;
        [SerializeField] private TextMeshProUGUI TEXT_MaxCardInDeck;

        [Header("Button")]
        [SerializeField] private Button _BTN_SaveDeck;
        [SerializeField] private Button _BTN_CancelDeck;
        [SerializeField] private Button _BTN_More;
        [SerializeField] private Button _BTN_AutoDeck;
        [SerializeField] private Button _BTN_CraftAll;
        [SerializeField] private GameObject _Lock_CraftAll;

        [Header("Prefab")]
        [SerializeField] private GameObject _CardBarPrefab;
        #endregion

        #region Public Properties
        #endregion

        #region Private Properties
        private Coroutine _updateDeckDetailRoutine = null;
        private CollectionDeckUIManager _manager;
        private List<CollectionCardBar> _collectionCardBarList = new List<CollectionCardBar>();
        private CollectionDeckDetailUIData _data = new CollectionDeckDetailUIData();
        private string _currentDeckID = string.Empty;
        private bool _isDragCard = false;
        #endregion

        #region Methods
        private void OnEnable()
        {
            InitEvents();
        }

        private void OnDisable()
        {
            DeinitEvents();
        }

        private void InitEvents()
        {
            CollectionDeckUIManager.OnInit += Init;
            CollectionDeckUIManager.OnUpdateDeckDetail += UpdateData;
            CollectionDeckUIManager.OnShowDeckDetailCallback += ShowDeckDetailCallback;
            CollectionDeckUIManager.OnUpdateDeckDetailCallback += UpdateDeckDetailCallback;
            CollectionDeckUIManager.OnHideDeckDetail += HideDeckDetail;
            CollectionDeckUIManager.OnUpdateCollectionCardDataList += UpdateDeckDetail;

            _DeckDetailCustomize.OnDeckDetailCustomizeComplete += UpdateDeckData;
            _DeckDetailCustomize.OnClearDeck += ClearDeckDetail;
            _DeckDetailCustomize.OnRemoveDeck += RemoveDeck;
        }

        private void DeinitEvents()
        {
            CollectionDeckUIManager.OnInit -= Init;
            CollectionDeckUIManager.OnUpdateDeckDetail -= UpdateData;
            CollectionDeckUIManager.OnShowDeckDetailCallback -= ShowDeckDetailCallback;
            CollectionDeckUIManager.OnUpdateDeckDetailCallback -= UpdateDeckDetailCallback;
            CollectionDeckUIManager.OnHideDeckDetail -= HideDeckDetail;
            CollectionDeckUIManager.OnUpdateCollectionCardDataList -= UpdateDeckDetail;

            _DeckDetailCustomize.OnDeckDetailCustomizeComplete -= UpdateDeckData;
            _DeckDetailCustomize.OnClearDeck -= ClearDeckDetail;
            _DeckDetailCustomize.OnRemoveDeck -= RemoveDeck;
        }

        private void Init(CollectionDeckUIManager manager)
        {
            _manager = manager;
            InitButton();
        }

        private void InitButton()
        {
            _BTN_SaveDeck.onClick.RemoveAllListeners();
            _BTN_SaveDeck.onClick.AddListener(SaveDeckDetail);

            _BTN_CancelDeck.onClick.RemoveAllListeners();
            _BTN_CancelDeck.onClick.AddListener(CancelDeckDetail);

            _BTN_More.onClick.RemoveAllListeners();
            _BTN_More.onClick.AddListener(ShowDeckDetailCustomize);

            _BTN_AutoDeck.onClick.RemoveAllListeners();
            _BTN_AutoDeck.onClick.AddListener(AutoDeck);

            _BTN_CraftAll.onClick.RemoveAllListeners();
            _BTN_CraftAll.onClick.AddListener(CraftAllMissing);
            _BTN_CraftAll.interactable = DataManager.Instance.IsFeatureEnable(FeatureUnlock.CardCrafting);
            _Lock_CraftAll.SetActive(!DataManager.Instance.IsFeatureEnable(FeatureUnlock.CardCrafting));
        }

        private void UpdateBTNAutoDeck()
        {
            bool isCraftable = false;
            foreach(KeyValuePair<string, int> card in _data.CurrentDeckData.Deck)
            {
                if(_data.GetNotOwnedCardCount(card.Key) > 0)
                {
                    isCraftable = true;
                    break;
                }
            }
            _BTN_CraftAll.gameObject.SetActive(isCraftable);
            _BTN_AutoDeck.gameObject.SetActive(!isCraftable);
            _BTN_AutoDeck.interactable = _data.CurrentDeckData.Deck.CountAmount < GameHelper.MaxCardPerDeck;
        }

        private void SetupData()
        {
            if(_collectionCardBarList.Count > 0)
            {
                UpdateData();
            }
            else
            {
                InitData();
            }
        }

        private void InitData()
        {
            if(_updateDeckDetailRoutine != null)
            {
                StopCoroutine(_updateDeckDetailRoutine);
            }

            _updateDeckDetailRoutine = StartCoroutine(IEInitData());
        }

        private IEnumerator IEInitData()
        {
            _data.SetCurrentDeckData(_manager.GetCurrentDeckData());
            CardListData currentCardDeckStack = _data.GetSortedCurrentCardDataStack();
            UpdateDeckStat(_data.CurrentDeckData);

            _currentDeckID = _data.CurrentDeckData.DeckID;

            string cardbackID = DataManager.Instance.DefaultCardBackIDList[0];
            int ownedCount = 0;
            int notOwnedCount = 0;
            int index = 0;
            foreach (KeyValuePair<string, int> card in currentCardDeckStack)
            {
                ownedCount = _data.GetOwnedCardCount(card.Key);
                notOwnedCount = _data.GetNotOwnedCardCount(card.Key);

                CardData cardData = CardData.CreateCard(card.Key);
                if (cardData != null)
                {
                    CardUIData uiData = new CardUIData(0, cardData, cardbackID);

                    if(ownedCount > 0)
                    {
                        InitCardBar(index);
                        _collectionCardBarList.Last().SetupData(uiData);
                        _collectionCardBarList.Last().SetCardCount(ownedCount);
                        _collectionCardBarList.Last().SetOwned(true);
                        index++;
                        yield return new WaitForEndOfFrame();
                    }

                    if (notOwnedCount > 0)
                    {
                        InitCardBar(index);
                        _collectionCardBarList.Last().SetupData(uiData);
                        _collectionCardBarList.Last().SetCardCount(notOwnedCount);
                        _collectionCardBarList.Last().SetOwned(false);
                        index++;
                        yield return new WaitForEndOfFrame();
                    }
                }
            }

            _manager.RequestOnUpdateDeckDetailComplete();
            yield break;
        }

        private void UpdateData()
        {
            if (_updateDeckDetailRoutine != null)
            {
                StopCoroutine(_updateDeckDetailRoutine);
            }

            _updateDeckDetailRoutine = StartCoroutine(IEUpdateData());
        }

        private IEnumerator IEUpdateData()
        {
            _data.SetCurrentDeckData(_manager.GetCurrentDeckData());
            CardListData currentCardDeckStack = _data.GetSortedCurrentCardDataStack();
            UpdateDeckStat(_data.CurrentDeckData);

            if(_data.CurrentDeckData.DeckID != _currentDeckID)
            {
                for (int i = 0; i < _collectionCardBarList.Count; i++)
                {
                    _collectionCardBarList[i].gameObject.SetActive(false);
                }
            }
            _currentDeckID = _data.CurrentDeckData.DeckID;

            string cardbackID = DataManager.Instance.DefaultCardBackIDList[0];
            int index = 0;
            int ownedCount = 0;
            int notOwnedCount = 0;
            foreach (KeyValuePair<string, int> card in currentCardDeckStack)
            {
                ownedCount = _data.GetOwnedCardCount(card.Key);
                notOwnedCount = _data.GetNotOwnedCardCount(card.Key);

                CardData cardData = CardData.CreateCard(card.Key);
                if (cardData != null)
                {
                    CardUIData uiData = new CardUIData(0, cardData, cardbackID);
                    if(ownedCount > 0)
                    {
                        if (_collectionCardBarList.Count <= index)
                        {
                            InitCardBar(index);
                        }

                        _collectionCardBarList[index].SetupData(uiData);
                        _collectionCardBarList[index].SetCardCount(ownedCount);
                        _collectionCardBarList[index].SetOwned(true);
                        _collectionCardBarList[index].gameObject.SetActive(true);
                        index++;
                    }

                    if (notOwnedCount > 0)
                    {
                        if (_collectionCardBarList.Count <= index)
                        {
                            InitCardBar(index);
                        }

                        _collectionCardBarList[index].SetupData(uiData);
                        _collectionCardBarList[index].SetCardCount(notOwnedCount);
                        _collectionCardBarList[index].SetOwned(false);
                        _collectionCardBarList[index].gameObject.SetActive(true);
                        index++;
                    }
                }

                yield return new WaitForEndOfFrame();
            }

            for (int i = index; i < _collectionCardBarList.Count; i++)
            {
                _collectionCardBarList[i].gameObject.SetActive(false);
            }

            _manager.RequestOnUpdateDeckDetailComplete();
            yield break;
        }

        private void UpdateDeckStat(DeckData deck)
        {
            // Count ap
            int ap1 = 0;
            int ap2 = 0;
            int ap3 = 0;
            CountDeckAP(deck.Deck, out ap1, out ap2, out ap3);

            // Set Hero
            string key = deck.HeroID + "_ALPHA";
            ICON_Hero.sprite = SpriteResourceHelper.LoadSprite(key, SpriteResourceHelper.SpriteType.Hero_Square);

            // Set Data
            TEXT_DeckName.text = deck.DeckName;
            TEXT_CardInDeck.text = deck.Deck.CountAmount.ToString();
            TEXT_MaxCardInDeck.text = "/" + GameHelper.MaxCardPerDeck.ToString();
            TEXT_AP1.text = ap1.ToString();
            TEXT_AP2.text = ap2.ToString();
            TEXT_AP3.text = ap3.ToString();

            // Set Excess Color
            if (deck.Deck.CountAmount != GameHelper.MaxCardPerDeck)
            {
                TEXT_CardInDeck.color = GameHelper.GetColor_Number_Decrease();
            }
            else
            {
                TEXT_CardInDeck.color = GameHelper.GetColorByHtmlCode("#CDAC7F");
            }

            // Update Deck Sumary
            _DeckSummary.InitData(deck.Deck);

            // Update BTN
            UpdateBTNAutoDeck();
        }

        private void CountDeckAP(CardListData tempDeck, out int ap1, out int ap2, out int ap3)
        {
            ap1 = 0;
            ap2 = 0;
            ap3 = 0;

            // Count each card
            foreach (KeyValuePair<string, int> item in tempDeck)
            {
                CardData card = CardData.CreateCard(item.Key);

                switch (card.Spirit)
                {
                    case 1:
                    {
                        ap1 += 1 * item.Value;
                    }
                    break;

                    case 2:
                    {
                        ap2 += 1 * item.Value;
                    }
                    break;

                    case 3:
                    {
                        ap3 += 1 * item.Value;
                    }
                    break;
                }

            }
        }

        private void InitCardBar(int index)
        {
            CollectionItem item = CollectionPool.Instance.CreateCollectionCardBar();
            CollectionCardBar cardBar = item.GetComponent<CollectionCardBar>();

            // Setup CardUI position
            cardBar.transform.SetParent(_DeckDetailContainer);
            cardBar.transform.localScale = Vector3.one;
            cardBar.transform.localRotation = Quaternion.identity;
            cardBar.transform.SetSiblingIndex(index);

            // subscribe events
            cardBar.OnCardBarClick += OnCardBar_Click;
            cardBar.OnCardBarDoubleClick += OnCardBar_DoubleClick;
            cardBar.OnCardBarBeginHold += OnCardBar_BeginHold;
            cardBar.OnCardBarEndHold += OnCardBar_EndHold;
            cardBar.OnBeginDragEvent += OnCardBarScroll_BeginDrag;
            cardBar.OnDragEvent += OnCardBarScroll_Drag;
            cardBar.OnEndDragEvent += OnCardBarScroll_EndDrag;
            cardBar.OnCardBarBeginDragEvent += OnCardBar_BeginDrag;
            cardBar.OnCardBarDragEvent += OnCardBar_Drag;
            cardBar.OnCardBarEndDragEvent += OnCardBar_EndDrag;

            _collectionCardBarList.Add(cardBar);
        }

        private void ShowDeckDetailCallback(CardListData data, bool isNewDeck)
        {
            UpdateDeckDetailCallback(data);

            if (isNewDeck)
            {
                _DeckDetailCustomize.ShowUI(_data.CurrentDeckData, isNewDeck);
            }
            else
            {
                ShowDeckDetail(true);
                NavigatorController.Instance.SetActiveUI(false);
            }
        }

        private void UpdateDeckDetailCallback(CardListData data)
        {
            _data.SetAllCardDataStack(data);
            SetupData();
        }

        private void UpdateDeckDetail()
        {
            _manager.RequestUpdateCurrentDeckDetail();
        }

        private void HideDeckDetail()
        {
            ShowDeckDetail(false);
            NavigatorController.Instance.SetActiveUI(true);
        }

        private void SaveDeckDetail()
        {
            _manager.RequestSaveCurrentDeckDetail();
        }

        private void CancelDeckDetail()
        {
            _manager.RequestCancelCurrentDeckDetail();
        }

        private Dictionary<CardDirectionType, int> GetCurrentDeckCardDirectionTypeData()
        {
            Dictionary<CardDirectionType, int> result = new Dictionary<CardDirectionType, int>();
            foreach(CollectionCardBar cardBar in _collectionCardBarList)
            {
                List<CardDirectionType> cardDir = cardBar.Data.CardDir.GetAllDirection();
                foreach (CardDirectionType dir in cardDir)
                {
                    if(result.ContainsKey(dir) == false)
                    {
                        result.Add(dir, 0);
                    }

                    result[dir] += 1;
                }
            }

            return result;
        }

        private void ClearDeckDetail()
        {
            _manager.RequestClearCurrentDeckDetail();
        }

        private void RemoveDeck()
        {
            _manager.RequestRemoveCurrentDeck();
        }

        private void ShowDeckDetailCustomize()
        {
            _DeckDetailCustomize.ShowUI(_data.CurrentDeckData, false);
        }

        private void UpdateDeckData(DeckData deck, bool isNewDeck)
        {
            if(isNewDeck)
            {
                ShowDeckDetail(true);    
                NavigatorController.Instance.SetActiveUI(false);

                foreach (KeyValuePair<string, int> card in _data.CurrentDeckData.Deck)
                {
                    if (_data.GetNotOwnedCardCount(card.Key) > 0)
                    {
                        _manager.RequestShowCraftAllMissing(true);
                        break;
                    }
                }
            }

            _manager.RequestUpdateCurrentDeckData(deck);
        }

        private void AutoDeck()
        {
            _manager.RequestAutoDeck();
        }

        private void CraftAllMissing()
        {
            _manager.RequestShowCraftAllMissing(false);
        }

        private void ShowDeckDetail(bool isShow)
        {
            if (isShow)
            {
                GameHelper.UITransition_FadeMoveIn(_DeckDetailGroup, _DeckDetailGroup, 1.0f, 0.0f);
                _GROUP_MENU.SetActive(false);
                _GROUP_CraftBar.SetActive(true);
            }
            else
            {
                GameHelper.UITransition_FadeMoveOut(_DeckDetailGroup, _DeckDetailGroup, 1.0f, 0.0f);
                _GROUP_MENU.SetActive(true);
                _GROUP_CraftBar.SetActive(false);
            }
        }
        #endregion

        #region Event ScrollCardBar
        private void OnCardBar_Click(CollectionCardBar cardBar)
        {
            _manager.RequestShowCardDetail(cardBar.Data.FullID);
        }

        private void OnCardBar_DoubleClick(CollectionCardBar cardBar)
        {
            _manager.RequestRemoveCardFromCurrentDeck(cardBar.Data.FullID);
        }

        private void OnCardBar_BeginHold(CollectionCardBar cardBar)
        {
            _manager.RequestShowCardDetail_Hover(cardBar.Data.FullID);
        }

        private void OnCardBar_EndHold(CollectionCardBar cardBar)
        {
            _manager.RequestHideCardDetail_Hover();
        }

        private void OnCardBar_BeginDrag(CollectionCardBar cardBar)
        {

        }

        private void OnCardBar_Drag(CollectionCardBar cardBar)
        {
            _manager.RequestActivateFloatingCard(DropZoneName.CollectionDeckDetail, cardBar.gameObject, cardBar.Data, true);
        }

        private void OnCardBar_EndDrag()
        {
            _manager.RequestDeactivateFloatingCard();
        }

        private void OnCardBarScroll_BeginDrag(PointerEventData eventData)
        {
            _DeckDetailScrollRect.OnBeginDrag(eventData);
        }

        private void OnCardBarScroll_Drag(PointerEventData eventData)
        {
            _DeckDetailScrollRect.OnDrag(eventData);
        }

        private void OnCardBarScroll_EndDrag(PointerEventData eventData)
        {
            _DeckDetailScrollRect.OnEndDrag(eventData);
        }
        #endregion
    }
}