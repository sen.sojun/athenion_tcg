﻿using Karamucho.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace Karamucho.Collection
{
    public class CollectionCardManager : MonoBehaviour
    {
        #region Private Properties
        private CollectionDataManager _data;
        private List<CardData> _filteredCardDataList = new List<CardData>();
        private bool _isShowDeckDetail = false;
        #endregion

        #region Events
        public static event Action<CollectionCardManager> OnInit;
        public static event Action OnShowDeckDetail;
        public static event Action OnHideDeckDetail;
        public static event Action OnUpdateResetCardDataList;
        public static event Action OnUpdateCardDataList;
        public static event Action<CardData, int, CatalogCardData> OnShowCardDetail;
        public static event Action<CardData> OnShowCardDetail_Hover;
        public static event Action OnHideCardDetail_Hover;
        public static event Action<CardData, int, CatalogCardData> OnCraftComplete;
        public static event Action OnCraftFail;
        public static event Action OnRecycleAllComplete;
        public static event Action<DeckData, bool> OnShowCraftAllMissing;
        public static event Action OnCraftAllMissingComplete;

        public static event Action<CatalogCardData> OnCraftCardEvent;
        public static event Action<CatalogCardData> OnRecycleCardEvent;
        public static event Action<List<CatalogCardData>> OnRecycleCardListEvent;
        public static event Action<List<CatalogCardData>> OnCraftCardListEvent;
        public static event Action<CatalogCardData> OnTransformCardEvent;
        #endregion

        #region Methods
        private void OnEnable()
        {
            InitEvents();
        }

        private void OnDisable()
        {
            DeinitEvents();
        }

        private void InitEvents()
        {
            CollectionManager.OnInit += Init;
            CollectionManager.OnShowDeckDetail += ShowDeckDetail;
            CollectionManager.OnUpdateDeckDetail += UpdateDeckDetail;
            CollectionManager.OnHideDeckDetail += HideDeckDetail;
            CollectionManager.OnUpdateCardZone += UpdateCardZone;
            CollectionManager.OnSuccessUpdateCardZone += SuccessUpdateCardZone;
            CollectionManager.OnShowCardDetail += ShowCardDetail;
            CollectionManager.OnShowCardDetail_Hover += ShowCardDetail_Hover;
            CollectionManager.OnHideCardDetail_Hover += HideCardDetail_Hover;
            CollectionManager.OnShowCraftAllMissing += ShowCraftAllMissing;

            CollectionFilterNode.OnFilterNodeUpdate += UpdateCollectionFilteredCardData;
            CollectionManager.OnUpdateCollectionFilter += UpdateCollectionFilteredCardData;
        }

        private void DeinitEvents()
        {
            CollectionManager.OnInit -= Init;
            CollectionManager.OnShowDeckDetail -= ShowDeckDetail;
            CollectionManager.OnUpdateDeckDetail -= UpdateDeckDetail;
            CollectionManager.OnHideDeckDetail -= HideDeckDetail;
            CollectionManager.OnUpdateCardZone -= UpdateCardZone;
            CollectionManager.OnSuccessUpdateCardZone -= SuccessUpdateCardZone;
            CollectionManager.OnShowCardDetail -= ShowCardDetail;
            CollectionManager.OnShowCardDetail_Hover -= ShowCardDetail_Hover;
            CollectionManager.OnHideCardDetail_Hover -= HideCardDetail_Hover;
            CollectionManager.OnShowCraftAllMissing -= ShowCraftAllMissing;

            CollectionFilterNode.OnFilterNodeUpdate -= UpdateCollectionFilteredCardData;
            CollectionManager.OnUpdateCollectionFilter -= UpdateCollectionFilteredCardData;
        }

        public void Init()
        {
            // Init Data
            _data = CollectionManager.Instance.Data;

            OnInit?.Invoke(this);

            CollectionFilterManager.Instance.AddFilterParam(CollectionFilterTypes.Owned, OwnedFilterTypes.Owned.ToString());  
            UpdateCollectionFilteredCardData();
        }

        #region Methods
        private void UpdateFilteredCardDataList()
        {
            _filteredCardDataList = CollectionFilterManager.Instance.GetCalculatedFilterData(_data.CardDataList);
        }

        private CardListData GetFilteredCardDataStack()
        {
            CardListData result = new CardListData();
            foreach(CardData card in _filteredCardDataList)
            {
                result.AddCard(card.FullID, _data.GetCardAmount(card.FullID));
            }

            return result;
        }

        public List<CardData> GetCurrentCardDataList(int setIndex, int countPerSet)
        {
            List<CardData> result = new List<CardData>();
            int startIndex = countPerSet * setIndex;
            int endIndex = startIndex + countPerSet;
            for (int i = startIndex; i < endIndex; i++)
            {
                if(_filteredCardDataList.Count > i)
                {
                    result.Add(_filteredCardDataList[i]);
                }
                else
                {
                    break;
                }
            }

            return result;
        }

        public int GetCardAmount(string cardFullID)
        {
            return _data.GetCardAmount(cardFullID);
        }

        private void UpdateCardZone(DropZoneName zoneName, string cardFullID)
        {
            int rawAmount = _data.GetCardRawAmount(cardFullID);
            int currentAmount = _data.GetCardAmount(cardFullID);

            switch (zoneName)
            {
                case DropZoneName.CollectionCard:
                {
                    if(currentAmount < rawAmount)
                    {
                        AddCardIntoCardDataList(cardFullID);
                    }
                    CollectionManager.Instance.RequestSuccessUpdateCardZone(zoneName, cardFullID);
                }
                break;
            }
        }

        private void SuccessUpdateCardZone(DropZoneName zoneName, string cardFullID)
        {
            switch (zoneName)
            {
                case DropZoneName.CollectionDeckDetail:
                {
                    RemoveCardFromCardDataList(cardFullID);
                }
                break;
            }
        }

        private void AddCardIntoCardDataList(string cardFullID, int amount = 1)
        {
            _data.AddCard(cardFullID, amount);
            UpdateFilteredCardDataList();
            OnUpdateCardDataList?.Invoke();
        }

        private void RemoveCardFromCardDataList(string cardFullID, int amount = 1)
        {
            _data.RemoveCard(cardFullID, amount);
            UpdateFilteredCardDataList();
            OnUpdateCardDataList?.Invoke();
        }

        private void ShowDeckDetail(DeckData deck, bool isNewDeck)
        {
            _isShowDeckDetail = true;

            CollectionFilterManager.Instance.ResetFilter(CollectionFilterTypes.Faction);
            CollectionFilterManager.Instance.AddDefaultFilterParam(CollectionFilterTypes.Faction, CardElementType.NEUTRAL.ToString());
            CollectionFilterManager.Instance.AddDefaultFilterParam(CollectionFilterTypes.Faction, deck.ElementID);
            CollectionFilterManager.Instance.SetFilterNodeFaction(deck.ElementID);
            UpdateFilteredCardDataList();

            foreach (KeyValuePair<string, int> card in deck.Deck)
            {
                _data.RemoveCard(card.Key, card.Value);
            }

            OnShowDeckDetail?.Invoke();
        }

        private void UpdateDeckDetail(DeckData deck)
        {
            _data.UpdateData();
            _data.ResetCardDataStack();
            UpdateFilteredCardDataList();

            foreach (KeyValuePair<string, int> card in deck.Deck)
            {
                _data.RemoveCard(card.Key, card.Value);
            }

            OnUpdateCardDataList?.Invoke();
        }

        private void HideDeckDetail()
        {
            _isShowDeckDetail = false;

            CollectionFilterManager.Instance.ResetFilter(CollectionFilterTypes.Faction);
            CollectionFilterManager.Instance.ResetDefaultFilter();
            CollectionFilterManager.Instance.ResetFilterNodeFaction();
            UpdateFilteredCardDataList();
            _data.ResetCardDataStack();

            OnHideDeckDetail?.Invoke();
        }

        private void UpdateCollectionFilteredCardData()
        {
            UpdateFilteredCardDataList();
            OnUpdateResetCardDataList?.Invoke();
        }

        private void ShowCardDetail(string cardFullID)
        {
            OnShowCardDetail?.Invoke(_data.GetCardData(cardFullID), _data.GetCardRawAmount(cardFullID), _data.GetCatalogCardData(cardFullID));
            CollectionManager.Instance.RequestShowCardDetailComplete();
        }

        private void ShowCardDetail_Hover(string cardFullID)
        {
            OnShowCardDetail_Hover?.Invoke(_data.GetCardData(cardFullID));
        }

        private void HideCardDetail_Hover()
        {
            OnHideCardDetail_Hover?.Invoke();
        }

        #region Craft Methods
        private void ShowCraftAllMissing(DeckData data, bool isNewDeck)
        {
            OnShowCraftAllMissing?.Invoke(data, isNewDeck);
        }

        private void OnCraftCardComplete(string cardFullID)
        {
            PopupUIManager.Instance.Show_MidSoftLoad(false);
            CollectionManager.Instance.RequestCraftCardComplete();
            OnCraftComplete?.Invoke(_data.GetCardData(cardFullID), _data.GetCardRawAmount(cardFullID), _data.GetCatalogCardData(cardFullID));
        }

        private void OnCraftCardFail(string error)
        {
            PopupUIManager.Instance.Show_MidSoftLoad(false);
            PopupUIManager.Instance.ShowPopup_Error(
                LocalizationManager.Instance.GetText("COLLECTION_ERROR_CRAFT")
                , LocalizationManager.Instance.GetText(error)
            );
            OnCraftFail?.Invoke();
        }

        private void OnRecycleAllCardsComplete()
        {
            PopupUIManager.Instance.Show_MidSoftLoad_Progress(false);
            CollectionManager.Instance.RequestCraftCardComplete();
            OnRecycleAllComplete?.Invoke();
        }

        public Dictionary<CardRarity, RecycleCellData> GetCalculatedRecycleAllCardsPrice()
        {
            Dictionary<CardRarity, RecycleCellData> result = new Dictionary<CardRarity, RecycleCellData>();
            List<ItemData> itemDatas = GetDuplicateCards();
            foreach (ItemData item in itemDatas)
            {
                CardData cardData;
                if (CardData.GetCardData(item.ItemID, out cardData))
                {
                    if (result.ContainsKey(cardData.Rarity) == false)
                    {
                        result.Add(cardData.Rarity, new RecycleCellData());
                    }

                    result[cardData.Rarity].AddCardAmount(item.Amount);

                    List<CardPriceData> priceDataList = _data.GetCatalogCardData(item.ItemID).GetCardPriceDataList(item.Amount);
                    foreach (CardPriceData priceData in priceDataList)
                    {
                        result[cardData.Rarity].AddRecyclePrice(VirtualCurrency.FR, priceData.GetCardRecyclePrice(VirtualCurrency.FR));
                        result[cardData.Rarity].AddRecyclePrice(VirtualCurrency.RF, priceData.GetCardRecyclePrice(VirtualCurrency.RF));
                        result[cardData.Rarity].AddRecyclePrice(VirtualCurrency.S1, priceData.GetCardRecyclePrice(VirtualCurrency.S1));
                    }
                }
            }

            return result;
        }

        private List<ItemData> GetDuplicateCards()
        {
            List<ItemData> duplicateStack = new List<ItemData>();
            int maxCardAmount = GameHelper.MaxCardPerBaseID;
            int currentCardAmount = 0;
            foreach (CardData item in _data.CardDataList)
            {
                if (_data.IsCardRecycleable(item.FullID) == false)
                {
                    continue;
                }

                currentCardAmount = _data.GetCardRawAmount(item.FullID);
                if (currentCardAmount > maxCardAmount)
                {
                    duplicateStack.Add(new ItemData(item.FullID, (currentCardAmount - maxCardAmount)));
                }
            }

            return duplicateStack;
        }

        private void OnCraftAllMissingCardsComplete()
        {
            PopupUIManager.Instance.Show_MidSoftLoad_Progress(false);
            CollectionManager.Instance.RequestCraftCardComplete();
            OnCraftAllMissingComplete?.Invoke();
        }

        public Dictionary<CardRarity, CraftCellData> GetCalculatedCraftAllMissingCardsPrice(DeckData data)
        {
            Dictionary<CardRarity, CraftCellData> result = new Dictionary<CardRarity, CraftCellData>();
            List<ItemData> itemDatas = GetMissingCards(data.Deck);
            foreach (ItemData item in itemDatas)
            {
                CardData cardData;
                if (CardData.GetCardData(item.ItemID, out cardData))
                {
                    if (result.ContainsKey(cardData.Rarity) == false)
                    {
                        result.Add(cardData.Rarity, new CraftCellData());
                    }

                    result[cardData.Rarity].AddCardAmount(item.Amount);

                    List<CardPriceData> priceDataList = _data.GetCatalogCardData(item.ItemID).GetCardPriceDataList(item.Amount);
                    foreach (CardPriceData priceData in priceDataList)
                    {
                        result[cardData.Rarity].AddCraftPrice(VirtualCurrency.FR, priceData.GetCardCraftPrice(VirtualCurrency.FR));
                        result[cardData.Rarity].AddCraftPrice(VirtualCurrency.RF, priceData.GetCardCraftPrice(VirtualCurrency.RF));
                        result[cardData.Rarity].AddCraftPrice(VirtualCurrency.S1, priceData.GetCardCraftPrice(VirtualCurrency.S1));
                    }
                }
            }

            return result;
        }

        private List<ItemData> GetMissingCards(CardListData cardListData)
        {
            List<ItemData> missingStack = new List<ItemData>();
            int cardCurrentAmount = 0;
            foreach (KeyValuePair<string, int> card in cardListData)
            {
                if (_data.IsCardCraftable(card.Key) == false)
                {
                    continue;
                }

                cardCurrentAmount = _data.GetCardRawAmount(card.Key);
                if (cardCurrentAmount < card.Value)
                {
                    missingStack.Add(new ItemData(card.Key, (card.Value - cardCurrentAmount)));
                }
            }

            return missingStack;
        }
        #endregion

        #endregion

        #region Request Methods

        #region Floating Card
        public void RequestActivateFloatingCard(DropZoneName startZone, GameObject targetObject, CardUIData card, bool updatePosition = false)
        {
            if(_isShowDeckDetail)
            {
                CollectionManager.Instance.RequestActivateFloatingCard(startZone, targetObject, card, updatePosition);
            }
        }

        public void RequestDeactivateFloatingCard(string cardFullID)
        {
            CollectionManager.Instance.RequestDeactivateFloatingCard(true);
        }
        #endregion

        #region Add/Remove Card
        public void RequestRemoveCardFromTable(string cardFullID)
        {
            //if (_data.GetCardAmount(cardFullID) > 0)
            {
                CollectionManager.Instance.RequestUpdateCardZone(DropZoneName.CollectionDeckDetail, cardFullID);
            }
        }
        #endregion

        #region Detail Card
        public void RequestShowCardDetail(string cardFullID)
        {
            ShowCardDetail(cardFullID);
        }

        public void RequestShowCardDetail_Hover(string cardFullID)
        {
            ShowCardDetail_Hover(cardFullID);
        }

        public void RequestHideCardDetail_Hover()
        {
            HideCardDetail_Hover();
        }

        #endregion

        #region Craft Card
        public void RequestHideCardDetail()
        {
            CollectionManager.Instance.RequestHideCardDetail();
        }

        public void RequestCraftCard(string cardFullID)
        {
            if (_data.IsCardCraftable(cardFullID) == false)
            {
                OnCraftCardFail("UNCRAFTABLE");
                return;
            }

            Dictionary<VirtualCurrency, int> price = _data.GetCardCraftPrice(cardFullID);
            foreach (KeyValuePair<VirtualCurrency, int> item in price)
            {
                if (DataManager.Instance.InventoryData.GetVirtualCurrency(item.Key) < item.Value)
                {
                    OnCraftCardFail("INSUFFICIENT_FUNDS");
                    return;
                }
            }

            PopupUIManager.Instance.Show_MidSoftLoad(true);

            CatalogCardData data = new CatalogCardData(_data.GetCatalogCardData(cardFullID));
            DataManager.Instance.CraftCard(
                cardFullID
                , delegate ()
                {
                    OnCraftCardEvent?.Invoke(data);
                    OnCraftCardComplete(cardFullID);
                }
                , OnCraftCardFail
            );
        }

        public void RequestRecycleCard(string cardFullID)
        {
            if (_data.IsCardRecycleable(cardFullID) == false)
            {
                OnCraftCardFail("UNRECYCLEABLE");
                return;
            }

            PopupUIManager.Instance.Show_MidSoftLoad(true);

            CatalogCardData data = new CatalogCardData(_data.GetCatalogCardData(cardFullID));
            DataManager.Instance.RecycleCard(
                cardFullID
                , delegate ()
                {
                    OnRecycleCardEvent?.Invoke(data);
                    OnCraftCardComplete(cardFullID);
                }
                , OnCraftCardFail
            );
        }

        public void RequestTransformCard(string cardFullID)
        {
            if (_data.IsCardTransformable(cardFullID) == false)
            {
                OnCraftCardFail("UNTRANSFORMABLE");
                return;
            }

            Dictionary<VirtualCurrency, int> price = _data.GetCardTransformPrice(cardFullID);
            foreach (KeyValuePair<VirtualCurrency, int> item in price)
            {
                if (DataManager.Instance.InventoryData.GetVirtualCurrency(item.Key) < item.Value)
                {
                    OnCraftCardFail("INSUFFICIENT_FUNDS");
                    return;
                }
            }

            PopupUIManager.Instance.Show_MidSoftLoad(true);

            CatalogCardData data = new CatalogCardData(_data.GetCatalogCardData(cardFullID));
            DataManager.Instance.TransformCard(
                cardFullID
                , delegate ()
                {
                    OnTransformCardEvent?.Invoke(data);
                    OnCraftCardComplete(cardFullID);
                }
                , OnCraftCardFail
            );
        }

        public void RequestRecycleAllCards()
        {
            List<ItemData> cardList = GetDuplicateCards();
            int amount = cardList.Count;
            int currentProgress = 0;

            PopupUIManager.Instance.Show_MidSoftLoad_Progress(true);

            List<CatalogCardData> dataList = new List<CatalogCardData>();
            foreach(ItemData item in cardList)
            {
                dataList.Add(new CatalogCardData(_data.GetCatalogCardData(item.ItemID)));
            }

            DataManager.Instance.RecycleCardList(
                  cardList
                , delegate ()
                {
                    PopupUIManager.Instance.ShowPopup_ConfirmOnlyWithCorrect(
                          LocalizationManager.Instance.GetText("TEXT_RECYCLE_DUPLICATE")
                        , LocalizationManager.Instance.GetText("TEXT_RECYCLE_DUPLICATE_SUCCESS_DESCRIPTION")
                    );

                    OnRecycleCardListEvent?.Invoke(dataList);
                    OnRecycleAllCardsComplete();
                }
                , delegate (string error)
                {
                    PopupUIManager.Instance.ShowPopup_Error(LocalizationManager.Instance.GetText("TEXT_RECYCLE_DUPLICATE"), LocalizationManager.Instance.GetText(error));
                    PopupUIManager.Instance.Show_MidSoftLoad_Progress(false);
                }
                , delegate (Dictionary<string, int> finishItem)
                {
                    currentProgress += finishItem.Count;
                    PopupUIManager.Instance.SetMidSoftLoadProgress(currentProgress, amount);
                }
            );
        }

        public void RequestCraftAllMissingCards(DeckData deck)
        {
            List<ItemData> cardList = GetMissingCards(deck.Deck);
            int amount = cardList.Count;
            int currentProgress = 0;

            PopupUIManager.Instance.Show_MidSoftLoad_Progress(true);

            List<CatalogCardData> dataList = new List<CatalogCardData>();
            foreach (ItemData item in cardList)
            {
                dataList.Add(new CatalogCardData(_data.GetCatalogCardData(item.ItemID)));
            }

            DataManager.Instance.CraftCardList(
                  cardList
                , delegate ()
                {
                    PopupUIManager.Instance.ShowPopup_ConfirmOnlyWithCorrect(
                          LocalizationManager.Instance.GetText("TEXT_CRAFT_ALL_MISSING")
                        , LocalizationManager.Instance.GetText("TEXT_CRAFT_ALL_MISSING_SUCCESS_DESCRIPTION")
                    );

                    OnCraftCardListEvent?.Invoke(dataList);
                    OnCraftAllMissingCardsComplete();
                }
                , delegate (string error)
                {
                    PopupUIManager.Instance.ShowPopup_Error(LocalizationManager.Instance.GetText("TEXT_CRAFT_ALL_MISSING"), LocalizationManager.Instance.GetText(error));
                    PopupUIManager.Instance.Show_MidSoftLoad_Progress(false);
                }
                , delegate (Dictionary<string, int> finishItem)
                {
                    currentProgress += finishItem.Count;
                    PopupUIManager.Instance.SetMidSoftLoadProgress(currentProgress, amount);
                }
            );
        }
        #endregion

        #endregion

        #endregion
    }
}