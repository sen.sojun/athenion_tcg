﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Karamucho.UI;
using System;

namespace Karamucho.Collection
{
    #region Data
    public class CollectionCreateDeckData
    {
        #region Public Properties
        public string DeckName { get { return _deckName; } }
        public string DeckID { get { return _deckID; } }
        public string HeroID { get { return _heroID; } }
        public string ElementID { get { return _elementID; } }
        #endregion

        #region Private Properties
        private string _deckID = string.Empty;
        private string _deckName = string.Empty;
        private string _heroID = string.Empty;
        private string _elementID = string.Empty;
        private string _cardbackID = string.Empty;
        private string _hudSkinID = string.Empty;
        private CardListData _cardList = new CardListData();
        #endregion

        #region Contructors
        public CollectionCreateDeckData(DeckData deck)
        {
            InitDeckData(deck);
        }
        #endregion

        #region Methods
        private void InitDeckData(DeckData deck)
        {
            SetDeckID(deck.DeckID);
            SetDeckName(deck.DeckName);
            SetCardBackID(deck.CardBackID);
            SetHudSkinID(deck.HUDSkinID);
            SetCardListData(deck.Deck);
            SetHeroID(deck.HeroID);
            SetElementID(deck.ElementID);
        }

        public void SetDeckData(DeckData deck)
        {
            SetCardListData(deck.Deck);
            SetHeroID(deck.HeroID);
            SetElementID(deck.ElementID);
        }

        private void SetDeckID(string deckID)
        {
            _deckID = deckID;
        }

        private void SetCardBackID(string cardbackID)
        {
            _cardbackID = cardbackID;
        }

        private void SetHudSkinID(string hudID)
        {
            _hudSkinID = hudID;
        }

        private void SetCardListData(CardListData data)
        {
            _cardList = new CardListData(data);
        }

        public void SetCardListDataEmpty()
        {
            _cardList.Clear();
        }

        public void SetDeckName(string deckName)
        {
            _deckName = deckName;
        }

        public void SetHeroID(string heroID)
        {
            _heroID = heroID;
        }

        public void SetElementID(string elementID)
        {
            _elementID = elementID;
        }

        public DeckData GetCurrentDeckData()
        {
            DeckData result = new DeckData(
                _deckID
                , _deckName
                , _elementID
                , _heroID
                , _cardbackID
                , _hudSkinID
                , _cardList
            );

            return result;
        }
        #endregion
    }
    #endregion

    public class CollectionCreateDeck : MonoBehaviour
    {
        #region Public Properties
        [Header("Prefab")]
        public CollectionHeroCell PrefabCollectionHeroCell;

        [Header("Popup CardList")]
        [SerializeField] private CardListPopup _Popup_CardList;

        [Header("BG Faction Image")]
        public Image BG_Faction;
        public Sprite BGF_Fire;
        public Sprite BGF_Water;
        public Sprite BGF_Air;
        public Sprite BGF_Earth;
        public Sprite BGF_Holy;
        public Sprite BGF_Dark;

        [Header("Hero")]
        public Transform GROUP_HERO;

        [Header("Text")]
        public TextMeshProUGUI TEXT_Faction;
        public TextMeshProUGUI Description;

        [Header("BTN")]
        public Button BTN_Confirm;
        public Button BTN_Cancel;

        [Header("Filter")]
        [SerializeField] private CollectionCreateDeck_FactionCell _BTN_Fire;
        [SerializeField] private CollectionCreateDeck_FactionCell _BTN_Water;
        [SerializeField] private CollectionCreateDeck_FactionCell _BTN_Earth;
        [SerializeField] private CollectionCreateDeck_FactionCell _BTN_Air;
        [SerializeField] private CollectionCreateDeck_FactionCell _BTN_Holy;
        [SerializeField] private CollectionCreateDeck_FactionCell _BTN_Dark;

        [Header("Deck Preset")]
        [SerializeField] private CollectionCreateDeck_DeckCell _Prefab_DeckPresetCell;
        [SerializeField] private CollectionCreateDeck_DeckCell _DeckPresetCell_Custom;
        [SerializeField] private Transform _GROUP_Preset;
        #endregion

        #region Private Properties
        private CardElementType _currentElementType;
        private List<CollectionHeroCell> _heroCellList = new List<CollectionHeroCell>();
        private GameObject _heroAnimated;
        private CollectionCreateDeckData _data;

        private Dictionary<CardElementType, string> _heroFaction = new Dictionary<CardElementType, string>()
        {
            { CardElementType.FIRE, "H0001" }
            , { CardElementType.WATER, "H0002" }
            , { CardElementType.EARTH, "H0003" }
            , { CardElementType.AIR, "H0004" }
            , { CardElementType.HOLY, "H0005" }
            , { CardElementType.DARK, "H0006" }
        };

        private List<CollectionCreateDeck_DeckCell> _presetCellList = new List<CollectionCreateDeck_DeckCell>();
        #endregion

        #region Events
        public event Action<DeckData> OnCreateNewDeck;
        public event Action<DeckData> OnCancelNewDeck;
        #endregion

        #region Methods
        public void ShowUI(DeckData deck)
        {
            _data = new CollectionCreateDeckData(deck);
            InitButton();
            InitDeckPresetCell();
            SelectFaction(CardElementType.FIRE);

            GameHelper.UITransition_FadeIn(this.gameObject);
        }

        private void HideUI()
        {
            GameHelper.UITransition_FadeOut(this.gameObject);
        }

        private void InitButton()
        {
            // BTN cancel
            BTN_Cancel.onClick.RemoveAllListeners();
            BTN_Cancel.onClick.AddListener(CancelNewDeck);

            // BTN confirm
            BTN_Confirm.onClick.RemoveAllListeners();
            BTN_Confirm.onClick.AddListener(CreateNewDeck);

            _BTN_Fire.SetOnClick(SelectFaction);
            _BTN_Water.SetOnClick(SelectFaction);
            _BTN_Air.SetOnClick(SelectFaction);
            _BTN_Earth.SetOnClick(SelectFaction);
            _BTN_Holy.SetOnClick(SelectFaction);
            _BTN_Dark.SetOnClick(SelectFaction);

        }

        private void SelectFaction(CardElementType elementType)
        {
            // Update UI
            UpdateFactionBG(elementType);
            UpdateFullHeroImage(_heroFaction[elementType]);
            UpdateText(elementType);

            // Update BTN
            _BTN_Fire.SetSelected(elementType);
            _BTN_Water.SetSelected(elementType);
            _BTN_Air.SetSelected(elementType);
            _BTN_Earth.SetSelected(elementType);
            _BTN_Holy.SetSelected(elementType);
            _BTN_Dark.SetSelected(elementType);

            // Update Data
            _data.SetElementID(elementType.ToString());
            _data.SetHeroID(_heroFaction[elementType]);
            _data.SetCardListDataEmpty();

            _currentElementType = elementType;

            SelectDeckPresetFaction(elementType);
        }

        private void CreateNewDeck()
        {
            OnCreateNewDeck?.Invoke(_data.GetCurrentDeckData());
            HideUI();
        }

        private void CancelNewDeck()
        {
            OnCancelNewDeck?.Invoke(_data.GetCurrentDeckData());
            HideUI();
        }

        private void UpdateFullHeroImage(string heroID)
        {
            // Load  Hero Animation
            string key = heroID;
            GameObject prefab;
            CardResourceManager.LoadUIHero(key, out prefab);

            if (_heroAnimated != null) Destroy(_heroAnimated);
            _heroAnimated = Instantiate(prefab, GROUP_HERO);
            _heroAnimated.transform.localPosition = Vector3.zero;

            GameHelper.UITransition_FadeIn(GROUP_HERO.gameObject);
        }

        private void UpdateFactionBG(CardElementType cardElement)
        {
            switch (cardElement)
            {
                case CardElementType.FIRE:
                {
                    BG_Faction.sprite = BGF_Fire;
                    break;
                }
                case CardElementType.WATER:
                {
                    BG_Faction.sprite = BGF_Water;
                    break;
                }
                case CardElementType.AIR:
                {
                    BG_Faction.sprite = BGF_Air;
                    break;
                }
                case CardElementType.EARTH:
                {
                    BG_Faction.sprite = BGF_Earth;
                    break;
                }
                case CardElementType.HOLY:
                {
                    BG_Faction.sprite = BGF_Holy;
                    break;
                }
                case CardElementType.DARK:
                {
                    BG_Faction.sprite = BGF_Dark;
                    break;
                }
                default:
                {
                    BG_Faction.sprite = BGF_Fire;
                    break;
                }

            }
        }

        private void UpdateText(CardElementType factionID)
        {
            HeroData data = HeroData.CreateHero(_heroFaction[factionID], PlayerIndex.One);
            GameHelper.StrToEnum(data.ElementID, out factionID);
            Color FactionColor = Color.white;
            TEXT_Faction.text = LocalizationManager.Instance.GetText("HERO_FLAVOR_" + data.ID);
            Description.text = LocalizationManager.Instance.GetText("HERO_PLAYSTYLE_" + data.ID);

            // Faction Color
            switch (factionID)
            {
                case CardElementType.FIRE:
                {
                    FactionColor = GameHelper.GetColor_Faction_Fire();
                }
                break;

                case CardElementType.WATER:
                {
                    FactionColor = GameHelper.GetColor_Faction_Water();
                }
                break;

                case CardElementType.AIR:
                {
                    FactionColor = GameHelper.GetColor_Faction_Air();
                }
                break;

                case CardElementType.EARTH:
                {
                    FactionColor = GameHelper.GetColor_Faction_Earth();
                }
                break;

                case CardElementType.HOLY:
                {
                    FactionColor = GameHelper.GetColor_Faction_Holy();
                }
                break;

                case CardElementType.DARK:
                {
                    FactionColor = GameHelper.GetColor_Faction_Dark();
                }
                break;

                default:
                {
                    FactionColor = Color.white;
                }
                break;
            }

            TEXT_Faction.color = FactionColor;
        }

        private void InitDeckPresetCell()
        {
            // Reset
            foreach (CollectionCreateDeck_DeckCell item in _presetCellList)
            {
                Destroy(item.gameObject);
            }
            _presetCellList.Clear();

            // Get Data
            DeckDB.Instance.GetPresetDecks(out List<DeckData> deckList);

            // Create List
            foreach (DeckData deck in deckList)
            {
                GameObject obj = Instantiate(_Prefab_DeckPresetCell.gameObject, _GROUP_Preset);
                CollectionCreateDeck_DeckCell cell = obj.GetComponent<CollectionCreateDeck_DeckCell>();
                cell.SetData(deck);
                cell.SetOnClick(SelectDeckPreset);
                cell.SetOnCardList(ShowCardList);

                _presetCellList.Add(cell);
            }

            // Set Event Custom
            _DeckPresetCell_Custom.InitBTN();
            _DeckPresetCell_Custom.SetName(LocalizationManager.Instance.GetText("COLLECTION_DECKPRESET_CUSTOM"));
            _DeckPresetCell_Custom.SetOnClick(SelectDeckPresetEmpty);
        }

        private void SelectDeckPresetFaction(CardElementType factionID)
        {
            _DeckPresetCell_Custom.SetSelected("");
            foreach (CollectionCreateDeck_DeckCell item in _presetCellList)
            {
                item.SetEnabled(factionID);
                item.SetSelected("");
            }
        }

        private void SelectDeckPreset(DeckData data)
        {
            _data.SetDeckData(data);
            _DeckPresetCell_Custom.SetSelected(data.DeckID);
            foreach (CollectionCreateDeck_DeckCell item in _presetCellList)
            {
                item.SetSelected(data.DeckID);
            }
        }

        private void SelectDeckPresetEmpty(DeckData data)
        {
            _data.SetElementID(_currentElementType.ToString());
            _data.SetHeroID(_heroFaction[_currentElementType]);
            _data.SetCardListDataEmpty();

            _DeckPresetCell_Custom.SetSelected("");
            foreach (CollectionCreateDeck_DeckCell item in _presetCellList)
            {
                item.SetSelected("");
            }
        }
        #endregion

        #region Card List
        public void ShowCardList(DeckData data)
        {
            _Popup_CardList.ShowUI();
            _Popup_CardList.InitData(data);
        }

        public void HideCardList()
        {
            _Popup_CardList.HideUI();
        }
        #endregion
    }
}