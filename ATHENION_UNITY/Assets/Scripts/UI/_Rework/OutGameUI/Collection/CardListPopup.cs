﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Karamucho.Collection;
using Karamucho.UI;
using UnityEngine.EventSystems;
using TMPro;

public class CardListPopup : MonoBehaviour
{
    #region Inspector
    [Header("Prefab")]
    [SerializeField] private CardUI_Collection _Prefab_CardUI;

    [Header("TEXT")]
    [SerializeField] private TextMeshProUGUI TEXT_Name;

    [Header("BTN")]
    [SerializeField] private Button _BTN_Close;

    [Header("Container")]
    [SerializeField] private Transform _GROUP_Container;
    [SerializeField] private ScrollRect _ScrollView;

    [Header("Popup Card Detail")]
    [SerializeField] private DetailCard _Popup_CardDetail;
    #endregion

    #region Private Properties
    private CardListData _data;
    private List<CardUI_Collection> _listCardUI = new List<CardUI_Collection>();
    #endregion

    #region Methods
    public void ShowUI()
    {
        GameHelper.UITransition_FadeIn(this.gameObject);

    }

    public void HideUI()
    {
        GameHelper.UITransition_FadeOut(this.gameObject);
    }

    public void InitData(DeckData data)
    {
        SetName(data.DeckID);
        InitBTN();
        ResetCard();

        _data = data.Deck;

        // Create Card
        int index = 0;
        foreach (KeyValuePair<string, int> item in _data)
        {
            bool isOwned = false;
            // Check Inventory
            if (DataManager.Instance.InventoryData.CardInventoryList.ContainKey(item.Key))
            {
                isOwned = DataManager.Instance.InventoryData.CardInventoryList[item.Key] >= item.Value;
            }
            
            // Assign Data
            CardData cardData = CardData.CreateCard(item.Key);
            CardUIData uiData = new CardUIData(0, cardData, DataManager.Instance.DefaultCardBackIDList[0]);
            if (index < _listCardUI.Count)
            {
                _listCardUI[index].SetData(uiData);
                _listCardUI[index].SetCardAmount(item.Value);
                _listCardUI[index].SetOwned(isOwned);
                _listCardUI[index].gameObject.SetActive(true);
            }
            else
            {
                CardUI_Collection card = InitCard();
                card.SetData(uiData);
                card.SetCardAmount(item.Value);
                card.SetOwned(isOwned);
                card.gameObject.SetActive(true);
            }
            index++;
        }

    }

    private void InitBTN()
    {
        _BTN_Close.onClick.RemoveAllListeners();
        _BTN_Close.onClick.AddListener(HideUI);
    }

    private void ResetCard()
    {
        foreach (CardUI_Collection item in _listCardUI)
        {
            item.gameObject.SetActive(false);
        }
    }

    private CardUI_Collection InitCard()
    {
        CollectionItem item = CollectionPool.Instance.CreateCollectionCard();
        CardUI_Collection card = item.GetComponent<CardUI_Collection>();

        //Setup Position
        card.transform.SetParent(_GROUP_Container);
        card.transform.localScale = Vector3.one;
        card.transform.localRotation = Quaternion.identity;

        //Set Event
        card.OnBeginDragEvent += OnCard_BeginDrag;
        card.OnDragEvent += OnCard_Drag;
        card.OnEndDragEvent += OnCard_EndDrag;
        card.OnCardClick += OnCard_Click;

        _listCardUI.Add(card);
        return card;
    }

    public void SetName(string id)
    {
        string name = LocalizationManager.Instance.GetText(id);
        TEXT_Name.text = name;
    }

    #endregion

    #region Event
    private void OnCard_Click(CardUI_Collection card)
    {
        _Popup_CardDetail.UpdateCard(card.Data, card.GetCardAmount());
        _Popup_CardDetail.Show();
    }

    private void OnCard_BeginDrag(PointerEventData eventData)
    {
        _ScrollView.OnBeginDrag(eventData);
    }

    private void OnCard_Drag(PointerEventData eventData)
    {
        _ScrollView.OnDrag(eventData);
    }

    private void OnCard_EndDrag(PointerEventData eventData)
    {
        _ScrollView.OnEndDrag(eventData);
    }
    #endregion
}
