﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

namespace Karamucho.Collection
{
    public class CollectionCreateDeck_DeckCell : MonoBehaviour
    {
        #region Public 
        public DeckData Data { get { return _data; } }
        #endregion

        #region Inspector Properties
        [Header("Image")]
        [SerializeField] private Image _IMG_Thumbnail;
        [SerializeField] private GameObject _Glow;

        [Header("BTN")]
        [SerializeField] private Button _BTN_Select;
        [SerializeField] private Button _BTN_CardList;

        [Header("TEXT")]
        [SerializeField] private TextMeshProUGUI _TEXT_Name;
        [SerializeField] private TextMeshProUGUI _TEXT_CardList;
        [SerializeField] private TextMeshProUGUI _TEXT_CardCount;
        #endregion

        #region Private Properties
        private const string _deckPresetIconPath = "Images/DeckPresetIcons/";
        private Action<DeckData> _onClick;
        private Action<DeckData> _onCardList;
        private DeckData _data = new DeckData();
        #endregion

        #region Methods
        public void SetData(DeckData data)
        {
            _data = data;
            SetName(_data.DeckID);
            SetImage(_data.DeckID);
            SetCardCount(_data.Deck);

            InitBTN();
        }

        public void SetOnClick(Action<DeckData> onClick)
        {
            _onClick = onClick;
        }

        public void SetOnCardList(Action<DeckData> onCardList)
        {
            _onCardList = onCardList;
        }

        public void SetEnabled(CardElementType element)
        {
            gameObject.SetActive(_data.ElementID == element.ToString());
        }

        public void SetSelected(string id)
        {
            _Glow.SetActive(_data.DeckID == id);
        }

        public void InitBTN()
        {
            _BTN_Select?.onClick.RemoveAllListeners();
            _BTN_Select?.onClick.AddListener(OnClick);

            _BTN_CardList?.onClick.RemoveAllListeners();
            _BTN_CardList?.onClick.AddListener(OnShowCardList);
        }

        private void OnClick()
        {
            _onClick?.Invoke(_data);
        }

        private void OnShowCardList()
        {
            _onCardList?.Invoke(_data);
        }

        public void SetName(string id)
        {
            string name = LocalizationManager.Instance.GetText(id);
            _TEXT_Name.text = name;
        }

        public void SetCardCount(CardListData data)
        {
            int ownedCard = 0;

            //Check Owned Card
            foreach (KeyValuePair<string,int> item in data)
            {
                if (DataManager.Instance.InventoryData.CardInventoryList.ContainKey(item.Key))
                {
                    int cardPlayerCount = DataManager.Instance.InventoryData.CardInventoryList[item.Key];
                    if (item.Value > cardPlayerCount)
                    {
                        ownedCard += cardPlayerCount;
                    }
                    else
                    {
                        ownedCard += item.Value;
                    }
                    
                }
            }

            //Set Color
            string color = "<color=white>";
            if (ownedCard >= GameHelper.MaxCardPerDeck)
                color = "<color=green>";
            else
                color = "<color=red>";

            _TEXT_CardCount.text = color + ownedCard.ToString() +"</color> / " + GameHelper.MaxCardPerDeck.ToString();
        }

        private void SetImage(string key)
        {
            _IMG_Thumbnail.sprite = ResourceManager.Load<Sprite>(_deckPresetIconPath + key);
        }
        #endregion
    }
}
