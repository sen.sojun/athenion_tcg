﻿using System.Collections;
using System.Collections.Generic;
using Karamucho.Collection;
using UnityEngine;
using UnityEngine.UI;

namespace Karamucho.Collection
{
    public class CollectionUIManager : MonoBehaviour
    {
        #region Inspector Properties
        [Header("Button")]
        [SerializeField] private Button _BTN_CreateDeck;
        [SerializeField] private Button _BTN_Filter;

        [Header("Card Object")]
        [SerializeField] private GameObject _FloatingCard;

        [Header("Landing Zone")]
        [SerializeField] private CollectionUIDropZone _CollectionCardZone;
        [SerializeField] private CollectionUIDropZone _CollectionDeckZone;
        [SerializeField] private GameObject _Effect_DropZone;

        [Header("Popup")]
        [SerializeField] private CollectionFilterPanel _Popup_Filter;
        #endregion

        #region Public Properties
        #endregion

        #region Private Properties
        // Floating Card Adjustment
        private float _trackingRate = 10.0f;
        private DropZoneName _startDropZoneName = DropZoneName.None;
        private DropZoneName _currentDropZoneName = DropZoneName.None;
        private CardUIData _currenFloatingCardUIData;
        private bool _isActivated = false;
        #endregion

        #region Methods
        private void OnEnable()
        {
            InitEvents();
        }

        private void OnDisable()
        {
            DeinitEvents();
        }

        private void InitEvents()
        {
            CollectionManager.OnInit += Init;
            CollectionManager.OnActivateFloatingCard += ActivateFloatingCard;
            CollectionManager.OnDeactivateFloatingCard += DeactivateFloatingCard;
            _CollectionCardZone.OnPointerEnterEvent += UpdateCurrentDropZone;
            _CollectionDeckZone.OnPointerEnterEvent += UpdateCurrentDropZone;
            _CollectionCardZone.OnPointerExitEvent += ClearCurrentDropZone;
            _CollectionDeckZone.OnPointerExitEvent += ClearCurrentDropZone;
        }

        private void DeinitEvents()
        {
            CollectionManager.OnInit -= Init;
            CollectionManager.OnActivateFloatingCard -= ActivateFloatingCard;
            CollectionManager.OnDeactivateFloatingCard -= DeactivateFloatingCard;
            _CollectionCardZone.OnPointerEnterEvent -= UpdateCurrentDropZone;
            _CollectionDeckZone.OnPointerEnterEvent -= UpdateCurrentDropZone;
            _CollectionCardZone.OnPointerExitEvent -= ClearCurrentDropZone;
            _CollectionDeckZone.OnPointerExitEvent -= ClearCurrentDropZone;
        }

        private void Update()
        {
            UpdateFloatingCardToPointer();
        }

        public void Init()
        {
            InitButtons();
        }

        private void InitButtons()
        {
            _BTN_CreateDeck.onClick.RemoveAllListeners();
            _BTN_CreateDeck.onClick.AddListener(CreateNewDeck);

            _BTN_Filter.onClick.RemoveAllListeners();
            _BTN_Filter.onClick.AddListener(() => ShowPopupFilter(true));
        }

        private void OnBack()
        {
            SoundManager.PlaySFX(SoundManager.SFX.Cancel_Click);
        }

        #region Floating Card
        private void UpdateCurrentDropZone(DropZoneName zoneName)
        {
            _currentDropZoneName = zoneName;

            if (_FloatingCard != null && _FloatingCard.activeSelf)
            {
                _Effect_DropZone.SetActive(_currentDropZoneName == DropZoneName.CollectionDeckDetail);
            }
        }

        private void ClearCurrentDropZone(DropZoneName zoneName)
        {
            UpdateCurrentDropZone(DropZoneName.None);
        }

        private void ClearAllDropZone()
        {
            UpdateCurrentDropZone(DropZoneName.None);
            _startDropZoneName = DropZoneName.None;
        }

        private void UpdateFloatingCardToPointer()
        {
            if (_FloatingCard != null && _FloatingCard.activeSelf)
            {
                Vector2 targetPos = Input.mousePosition;
                _FloatingCard.transform.position = Vector2.Lerp(_FloatingCard.transform.position, targetPos, Time.deltaTime * _trackingRate);
            }
        }

        private void ActivateFloatingCard(DropZoneName startZone, GameObject targetObject, CardUIData card, bool updatePosition = false)
        {
            _startDropZoneName = startZone;

            if (_FloatingCard != null)
            {
                _FloatingCard.SetActive(true);
            }

            if (updatePosition)
            {
                float OffsetY = 2.0f;
                Vector3 targetPosition = new Vector3(targetObject.transform.position.x,
                    targetObject.transform.position.y + OffsetY,
                    targetObject.transform.position.z);

                _FloatingCard.transform.position = targetPosition;
            }

            _isActivated = true;
            _currenFloatingCardUIData = card;
            _FloatingCard.GetComponent<CardUI_FloatingCard>().SetData(card);
        }

        private void DeactivateFloatingCard(bool isSuccess)
        {
            // call after dropping floating card
            if(_currentDropZoneName != DropZoneName.None
                && _currentDropZoneName != _startDropZoneName
                && isSuccess
                && _isActivated)
            {
                CollectionManager.Instance.RequestUpdateCardZone(_currentDropZoneName, _currenFloatingCardUIData.FullID);
            }

            CollectionManager.Instance.RequestUpdateCardDropZone(_currentDropZoneName);

            _isActivated = false;
            ClearAllDropZone();

            if (_FloatingCard != null)
            {
                _FloatingCard.SetActive(false);
            }
        }
        #endregion

        #region Create Deck
        private void CreateNewDeck()
        {
            CollectionManager.Instance.RequestCreateNewDeck();
        }
        #endregion

        #region Filter
        public void ShowPopupFilter(bool isShow)
        {
            if (isShow)
                _Popup_Filter.ShowUI();
            else
                _Popup_Filter.HideUI();
        }

        #endregion

        #endregion
    }
}