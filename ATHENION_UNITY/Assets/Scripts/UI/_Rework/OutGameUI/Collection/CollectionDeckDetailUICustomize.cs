﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;
using System;

namespace Karamucho.Collection {

    #region Data
    public class CollectionDeckDetailUICustomizeData
    {
        #region Public Properties
        public string DeckName { get { return _deckName; } }
        public string HeroID { get { return _heroID; } }
        public string CardBackID { get { return _cardbackID; } }
        public string HudID { get { return _hudID; } }

        public List<HeroDBData> HeroDBList { get { return _heroDB; } }
        public List<CardBackDBData> CardBackDBList { get { return _cardBackDB; } }
        public List<DockDBData> HudDBList { get { return _hudDB; } }
        #endregion

        #region Private Properties
        private DeckData _deck = new DeckData();

        private string _deckName = string.Empty;
        private string _heroID = string.Empty;
        private string _cardbackID = string.Empty;
        private string _hudID = string.Empty;

        private List<HeroDBData> _heroDB = new List<HeroDBData>();
        private List<CardBackDBData> _cardBackDB = new List<CardBackDBData>();
        private List<DockDBData> _hudDB = new List<DockDBData>();

        private List<string> _heroDBInventory = new List<string>();
        private List<string> _cardBackDBInventory = new List<string>();
        private List<string> _hudDBInventory = new List<string>();
        #endregion

        #region Contructors
        public CollectionDeckDetailUICustomizeData(DeckData deck)
        {
            InitData(deck);
        }
        #endregion

        #region Methods
        private void InitData(DeckData deck)
        {
            _deck = new DeckData(deck);

            SetDeckName(_deck.DeckName);
            SetHeroID(_deck.HeroID);
            SetCardBackID(_deck.CardBackID);
            SetHudID(_deck.HUDSkinID);

            SetupHeroDB();
            SetupCardBackDB();
            SetupHudDB();
        }

        private void SetupHeroDB()
        {
            HeroDB.Instance.GetAllData(out _heroDB);
            _heroDBInventory = DataManager.Instance.InventoryData.HeroIDList;
        }

        private void SetupCardBackDB()
        {
            CardBackDB.Instance.GetAllData(out _cardBackDB);
            _cardBackDBInventory = DataManager.Instance.InventoryData.CardBackIDList;
        }

        private void SetupHudDB()
        {
            DockDB.Instance.GetAllData(out _hudDB);
            _hudDBInventory = DataManager.Instance.InventoryData.DockIDList;
        }

        public bool IsHeroIDAvailable(string heroID)
        {
            return _heroDBInventory.Contains(heroID);
        }

        public bool IsCardBackIDAvailable(string cardbackID)
        {
            return _cardBackDBInventory.Contains(cardbackID);
        }

        public bool IsHudIDAvailable(string heroID)
        {
            return _hudDBInventory.Contains(heroID);
        }

        public void SetDeckName(string deckName)
        {
            _deckName = deckName;
        }

        public void SetHeroID(string heroID)
        {
            _heroID = heroID;
        }

        public void SetCardBackID(string cardBackID)
        {
            _cardbackID = cardBackID;
        }

        public void SetHudID(string hudID)
        {
            _hudID = hudID;
        }

        public DeckData GetCurrentDeckData()
        {
            DeckData result = new DeckData(
                _deck.DeckID
                , _deckName
                , _deck.ElementID
                , _heroID
                , _cardbackID
                , _hudID
                , _deck.Deck
            );

            return result;
        }
        #endregion
    }
    #endregion

    public class CollectionDeckDetailUICustomize : MonoBehaviour
    {
        #region Inspector Properties
        [SerializeField] private SkinSlotCell _Prefab_SkinCell;

        [Header("Button")]
        [SerializeField] private Button _BTN_Okay;
        [SerializeField] private Button _BTN_ClearDeck;
        [SerializeField] private Button _BTN_RemoveDeck;

        [Header("Deck Name")]
        [SerializeField] private TMP_InputField _INPUT_DeckName;

        [Header("Hero Skin")]
        [SerializeField] private Transform _Container_HeroSkin;

        [Header("CardBack Skin")]
        [SerializeField] private Transform _Container_CardBack;

        [Header("HUD Skin")]
        [SerializeField] private Transform _Container_HUD;
        #endregion

        #region Public Properties
        #endregion

        #region Private Properties
        private List<SkinSlotCell> _heroCellList = new List<SkinSlotCell>();
        private List<SkinSlotCell> _cardBackCellList = new List<SkinSlotCell>();
        private List<SkinSlotCell> _hudCellList = new List<SkinSlotCell>();

        //private DeckData _currentDeck;
        private string _currentHero;
        private string _currentCardBack;
        private bool _isNewDeck = false;

        private CollectionDeckDetailUICustomizeData _data;
        #endregion

        #region Events
        public event Action<DeckData, bool> OnDeckDetailCustomizeComplete;
        public event Action OnClearDeck;
        public event Action OnRemoveDeck;
        #endregion

        #region Methods
        public void ShowUI(DeckData deck, bool isNewDeck)
        {
            _isNewDeck = isNewDeck;
            _BTN_ClearDeck.gameObject.SetActive(!_isNewDeck);
            _BTN_RemoveDeck.gameObject.SetActive(!_isNewDeck);

            InitData(deck);
            InitUIElements();

            ShowUIPopup();
        }

        private void InitUIElements()
        {
            _INPUT_DeckName.text = _data.DeckName;
            _INPUT_DeckName.onEndEdit.RemoveAllListeners();
            _INPUT_DeckName.onEndEdit.AddListener(UpdateDeckName);

            _BTN_Okay.onClick.RemoveAllListeners();
            _BTN_Okay.onClick.AddListener(DeckDetailCustomizeComplete);

            _BTN_ClearDeck.onClick.RemoveAllListeners();
            _BTN_ClearDeck.onClick.AddListener(ClearDeckDetail);

            _BTN_RemoveDeck.onClick.RemoveAllListeners();
            _BTN_RemoveDeck.onClick.AddListener(RemoveDeck);

            SelectHeroSkin(_data.HeroID);
            SelectCardBackSkin(_data.CardBackID);
            SelectHUDSkin(_data.HudID);
        }

        private void InitData(DeckData deck)
        {
            _data = new CollectionDeckDetailUICustomizeData(deck);

            ClearAll();

            // Create Heroes
            foreach (HeroDBData item in _data.HeroDBList)
            {
                if (item.ElementID == deck.ElementID)
                {
                    if (_data.IsHeroIDAvailable(item.ID))
                    {
                        _heroCellList.Add(
                            CreateSkinSlot(
                                item.ID
                                , SkinSlotCell.SkinType.Hero
                                , _Container_HeroSkin
                                , delegate
                                {
                                    SelectHeroSkin(item.ID);
                                }
                            )
                        );
                    }
                }             
            }

            // Create Card Back
            foreach (CardBackDBData item in _data.CardBackDBList)
            {
                if (_data.IsCardBackIDAvailable(item.CardBackID))
                {
                    _cardBackCellList.Add(
                        CreateSkinSlot(
                            item.CardBackID
                            , SkinSlotCell.SkinType.CardBack
                            , _Container_CardBack
                            , delegate
                            {
                                SelectCardBackSkin(item.CardBackID);
                            }
                        )
                    );
                }
            }

            // Create HUD
            foreach (DockDBData item in _data.HudDBList)
            {
                if (_data.IsHudIDAvailable(item.DockID))
                {
                    _hudCellList.Add(
                        CreateSkinSlot(
                            item.DockID
                            , SkinSlotCell.SkinType.HUD
                            , _Container_HUD
                            , delegate
                            {
                                SelectHUDSkin(item.DockID);
                            }
                        )
                    );
                }
            }
        }

        private void ShowUIPopup()
        {
            GameHelper.UITransition_FadeIn(this.gameObject);
        }

        private void HideUIPopup()
        {
            GameHelper.UITransition_FadeOut(this.gameObject);
        }

        private void UpdateDeckName(string deckName)
        {
            _data.SetDeckName(deckName);
        }

        private void SelectHeroSkin(string id)
        {
            _data.SetHeroID(id);

            foreach (SkinSlotCell item in _heroCellList)
            {
                item.ShowGlow(item.ID == id);
            }
        }

        private void SelectCardBackSkin(string id)
        {
            _data.SetCardBackID(id);

            foreach (SkinSlotCell item in _cardBackCellList)
            {
                item.ShowGlow(item.ID == id);
            }
        }

        private void SelectHUDSkin(string id)
        {
            _data.SetHudID(id);

            foreach (SkinSlotCell item in _hudCellList)
            {
                item.ShowGlow(item.ID == id);
            }
        }

        private void DeckDetailCustomizeComplete()
        {
            OnDeckDetailCustomizeComplete?.Invoke(_data.GetCurrentDeckData(), _isNewDeck);
            HideUIPopup();
        }

        private void ClearDeckDetail()
        {
            OnClearDeck?.Invoke();
            HideUIPopup();
        }

        private void RemoveDeck()
        {
            OnRemoveDeck?.Invoke();
            HideUIPopup();
        }
        #endregion

        #region Cell Generate
        private SkinSlotCell CreateSkinSlot(string id, SkinSlotCell.SkinType skinType, Transform parent,UnityAction onClick)
        {
            GameObject obj = Instantiate(_Prefab_SkinCell.gameObject, parent);
            SkinSlotCell cell = obj.GetComponent<SkinSlotCell>();
            cell.InitData(id, skinType, true, onClick);
            return cell;
        }

        private void ClearAll()
        {
            // Clear Hero Skin
            foreach (SkinSlotCell item in _heroCellList)
            {
                Destroy(item.gameObject);
            }
            _heroCellList.Clear();

            // Clear Card Back Skin
            foreach (SkinSlotCell item in _cardBackCellList)
            {
                Destroy(item.gameObject);
            }
            _cardBackCellList.Clear();

            // Clear Card Back Skin
            foreach (SkinSlotCell item in _hudCellList)
            {
                Destroy(item.gameObject);
            }
            _hudCellList.Clear();
        }
        #endregion
    }
}