﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class OutGameUIBase : MonoBehaviour
{
    public abstract string SceneName { get; }
    public virtual bool IsCanSwitchScene { get; }
    public virtual bool IsCanUnloadResourceAfterUnloadScene => true;

    public abstract void OnInitialize(UnityAction onComplete);

    public virtual void ShowUI() { }
    public virtual void HideUI(UnityAction onComplete) { onComplete?.Invoke(); }

    public virtual void OnExit() { }
}
