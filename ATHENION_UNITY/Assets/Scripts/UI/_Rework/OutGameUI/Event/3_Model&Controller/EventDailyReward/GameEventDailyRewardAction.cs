﻿using GameEvent;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameEventDailyRewardData : GameEventBaseData
{
    #region Static and Constant Value
    private static readonly string ValidActionString = "valid_action";
    #endregion

    #region Public Properties
    public List<List<ItemData>> RewardList => new List<List<ItemData>>(_rewardList);
    public Dictionary<string, object> ReceiveRewardDict => new Dictionary<string, object>(_receiveRewardDict);
    public bool IsActionValid => _isActionValid;
    #endregion

    #region Private Properties
    private List<List<ItemData>> _rewardList;
    private Dictionary<string, object> _receiveRewardDict;
    private bool _isActionValid;
    #endregion

    public GameEventDailyRewardData(string eventKey, string queryDataMethodName, UnityAction onComplete) : base(eventKey)
    {
        OnComplete = onComplete;

        DataManager.Instance.ExcecuteEventCloudScript(
            queryDataMethodName
            , new
            {
                EventKey = eventKey
            }
            , InitData
            , delegate(string err)
            {
                Debug.LogError("GameEventDailyRewardData/Ctor: " + err);
                IsLoaded = false;
            }
        );
    }

    protected override void InitData(CloudScriptResultProcess eventData)
    {
        #region Base Data
        base.InitData(eventData);
        if (!IsLoaded)
        {
            OnComplete?.Invoke();
            return;
        }
        #endregion

        #region Reward Data

        _rewardList = new List<List<ItemData>>();

        IsLoaded &= EventDataDict.ContainsKey(WinRewardsKey);
        if (!IsLoaded)
        {
            OnComplete?.Invoke();
            return;
        }

        IsLoaded &= GameHelper.TryDeserializeJSONStr(EventDataDict[WinRewardsKey].ToString(), out List<object> rewardDict);
        if (!IsLoaded)
        {
            OnComplete?.Invoke();
            return;
        }

        foreach (object item in rewardDict)
        {
            
            IsLoaded &= GameHelper.TryDeserializeJSONStr(item.ToString(), out Dictionary<string, int> itemDict);
            if (!IsLoaded)
            {
                OnComplete?.Invoke();
                return;
            }

            _rewardList.Add(DataManager.ConvertDictionaryToItemDataList(itemDict));
        }
        #endregion

        #region Player Data
        IsLoaded &= PlayerDataDict.ContainsKey(ReceiveRewardKey);
        if (!IsLoaded)
        {
            OnComplete?.Invoke();
            return;
        }

        IsLoaded &= GameHelper.TryDeserializeJSONStr(PlayerDataDict[ReceiveRewardKey].ToString(), out _receiveRewardDict);
        if (!IsLoaded)
        {
            OnComplete?.Invoke();
            return;
        }
        #endregion

        #region Other Data
        IsLoaded &= eventData.CustomData.ContainsKey(ValidActionString);
        if (!IsLoaded)
        {
            OnComplete?.Invoke();
            return;
        }

        IsLoaded &= GameHelper.TryDeserializeJSONStr(eventData.CustomData[ValidActionString], out _isActionValid);
        if (!IsLoaded)
        {
            OnComplete?.Invoke();
            return;
        }
        #endregion

        OnComplete?.Invoke();
    }

    public void UpdateReceiveRewardData(CloudScriptResultProcess eventData)
    {
        IsLoaded &= GameHelper.TryDeserializeJSONStr(eventData.CustomData[ReceiveRewardKey], out _receiveRewardDict);
        IsLoaded &= GameHelper.TryDeserializeJSONStr(eventData.CustomData[ValidActionString], out _isActionValid);
    }
}

public class GameEventDailyRewardAction : GameEventBaseAction
{
    #region Static Value
    #region Localization Key
    private static readonly string ActionNotValidTitle = "GAME_EVENT_ACTION_NOT_VALID_TITLE_";
    private static readonly string ActionNotValidMessage = "GAME_EVENT_ACTION_NOT_VALID_MESSAGE_";
    #endregion
    #endregion

    #region Private Properties
    private GameEventDailyRewardData _data;
    #endregion

    #region Methods
    public override void Init(string eventKey, UnityAction<GameEventBaseData> onComplete, UnityAction<string> onFail)
    {
        _data = new GameEventDailyRewardData(eventKey, QueryDataMethod.ToString(), () =>
        {
            if (_data.IsLoaded)
            {
                onComplete?.Invoke(_data);
            }
            else
            {
                string title = LocalizationManager.Instance.GetText(InitFailTitle);
                string message = LocalizationManager.Instance.GetText(InitFailMessage);

                PopupUIManager.Instance.ShowPopup_Error(title,
                    message
                    , () => { onFail?.Invoke("EventDailyRewardAction/Init: Initial Data Fail."); });
            }
        });
    }

    protected override void Prepare(int actionIndex, UnityAction<bool> onComplete)
    {
        if (_data.IsActionValid)
        {
            Debug.Log("Prepare phase success.");
            onComplete?.Invoke(true);
        }
        else
        {
            string title = LocalizationManager.Instance.GetText(ActionNotValidTitle + _data.EventKey);
            string message = LocalizationManager.Instance.GetText(ActionNotValidMessage + _data.EventKey);

            PopupUIManager.Instance.ShowPopup_Error(title,
                message
                , () => { onComplete?.Invoke(false); });
        }
    }

    protected override void OnAction(int actionIndex, UnityAction onComplete)
    {
        DataManager.Instance.ExcecuteEventCloudScript(
            ActionMethod.ToString()
            , new
            {
                EventKey = _data.EventKey
            }
            , result =>
            {
                if (!result.IsSuccess)
                {
                    _data.UpdateActionStatus(false);
                    string title = LocalizationManager.Instance.GetText(ActionFailTitle);
                    string message = LocalizationManager.Instance.GetText(ActionFailMessage);
                    PopupUIManager.Instance.ShowPopup_Error(title
                        , message
                        , onComplete);
                    return;
                }

                _data.UpdateReceiveRewardData(result);
                _data.UpdateActionStatus(true);
                DataManager.Instance.LoadInventory(onComplete, null);
                Debug.Log("OnAction is success");

                // Add Analytic data.
                int rewardIndex = _data.ReceiveRewardDict.Count - 1;
                if (_data.ReceiveRewardDict.Count == _data.RewardList.Count) GameEventManager.Instance.OnEventComplete(_data.EventKey);
                GameEventManager.Instance.OnReceiveEventReward(_data.EventKey, _data.RewardList[rewardIndex]);
            }
            , delegate (string err)
            {
                Debug.LogError(err);
                _data.UpdateActionStatus(false);
                onComplete?.Invoke();
            }
        );
    }

    protected override void OnUpdate(UnityAction<GameEventBaseData> onComplete)
    {
        Debug.Log("OnUpdate is success");
        onComplete?.Invoke(_data);
    }
    #endregion
}
