﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace Karamucho.GameEvent
{
    public class GameEventOutGameManager : MonoSingleton<GameEventOutGameManager>, IGameEventManager
    {
        #region Public Properties
        #endregion

        #region Private Properties
        private string _sceneName;
        private GameEventBaseAction _gameEventActionController;
        private string _eventKey;
        [SerializeField] private CanvasGroup _canvasFadeIn;
        [SerializeField] private CanvasGroup _canvasFadeOut;
        #endregion

        #region UI Properties

        private IList<IGameEventInit> _gameUiInitList;
        private IList<IGameEventUpdate> _gameUiUpdateList;
        private IList<IGameEventAction> _gameUiActionList;
        private IList<IGameEventActivate> _gameUiActivateList;
        private IList<IGameEventDeactivate> _gameUiDeactivateList;

        #endregion

        #region Event
        public static event Action OnInit;
        #endregion

        #region Methods

        #region Unity Override Methods
        protected override void Awake()
        {
            base.Awake();

            _gameUiInitList = new List<IGameEventInit>();
            _gameUiUpdateList = new List<IGameEventUpdate>();
            _gameUiActionList = new List<IGameEventAction>();
            _gameUiActivateList = new List<IGameEventActivate>();
            _gameUiDeactivateList = new List<IGameEventDeactivate>();
        }
        #endregion

        #region Init Methods
        /// <summary>
        /// Call this method to initialize the current event.
        /// </summary>
        /// <param name="sceneName"></param>
        /// <param name="eventKey"></param>
        public void Init(string eventKey, string sceneName)
        {
            PopupUIManager.Instance.Show_SoftLoad(true);
            
            _sceneName = sceneName;
            _eventKey = eventKey;
            _gameEventActionController.Init(_eventKey
                , data =>
                {
                    InitUI(data, () => {
                        ActivateUI(() =>
                        {
                            GameHelper.UITransition_FadeIn(_canvasFadeIn.gameObject, () => PopupUIManager.Instance.Show_SoftLoad(false));
                        });
                    });
                }
                , err =>
                {
                    Debug.LogError(err);
                    PopupUIManager.Instance.Show_SoftLoad(false);
                }
            );
        }

        /// <summary>
        /// Call this method to initialize event action controller.
        /// </summary>
        /// <param name="controller"></param>
        public void InitEventActionController(GameEventBaseAction controller)
        {
            _gameEventActionController = controller;
        }

        public void AddGameEventInit(IGameEventInit uiInstance)
        {
            _gameUiInitList.Add(uiInstance);
        }

        public void AddGameEventUpdate(IGameEventUpdate uiInstance)
        {
            _gameUiUpdateList.Add(uiInstance);
        }

        public void AddGameEventActivate(IGameEventActivate uiInstance)
        {
            _gameUiActivateList.Add(uiInstance);
        }

        public void AddGameEventDeactivate(IGameEventDeactivate uiInstance)
        {
            _gameUiDeactivateList.Add(uiInstance);
        }

        #endregion

        #region UI Controller Methods

        private void InitUI(GameEventBaseData data, UnityAction onComplete = null)
        {
            int count = _gameUiInitList.Count;
            UnityAction action = onComplete;

            foreach (var uiInit in _gameUiInitList)
            {
                uiInit.Init(data, () =>
                {
                    count--;
                    if (count == 0)
                    {
                        action?.Invoke();
                        action = null;
                    }
                });
            }
        }

        private void UpdateUI(GameEventBaseData data, UnityAction onComplete = null)
        {
            int count = _gameUiUpdateList.Count;
            UnityAction action = onComplete;

            foreach (var uiUpdate in _gameUiUpdateList)
            {
                uiUpdate.EventUpdate(data, () =>
                {
                    count--;
                    if (count == 0)
                    {
                        action?.Invoke();
                        action = null;
                    }
                });
            }
        }

        private void ActivateUI(UnityAction onComplete = null)
        {
            int count = _gameUiActivateList.Count;
            UnityAction action = onComplete;

            foreach (var uiActivate in _gameUiActivateList)
            {
                uiActivate.Activate(delegate
                {
                    count--;
                    if (count == 0)
                    {
                        action?.Invoke();
                        action = null;
                    }
                });
            }
        }

        private void DeactivateUI(UnityAction onComplete = null)
        {
            int count = _gameUiActivateList.Count;
            UnityAction action = onComplete;

            foreach (var uiDeactivate in _gameUiDeactivateList)
            {
                uiDeactivate.Deactivate(() =>
                {
                    count--;
                    if (count == 0)
                    {
                        action?.Invoke();
                        action = null;
                    }
                });
            }
        }
        #endregion

        #region Request Methods

        public void RequestEventExit()
        {
            GameHelper.UITransition_FadeOut(_canvasFadeOut.gameObject, () => SceneManager.UnloadSceneAsync(_sceneName));
        }

        public void RequestEventAction(int actionIndex)
        {
            DeactivateUI(() =>
            {
                _gameEventActionController.Action(actionIndex);
            });
        }

        public void RequestEventUpdate(GameEventBaseData data)
        {
            UpdateUI(data, () =>
            {
                ActivateUI(); 
            });
        }

        #endregion

        #region Scene Management Methods

        protected override void OnSceneUnloaded(Scene current)
        {
            ResetEvent();
            if (current.name == _sceneName)
            {
                DestroySelf();
            }
        }

        public void ResetEvent()
        {
            OnInit = null;
        }

        private void SceneFadeIn(UnityAction onComplete)
        {
            _canvasFadeIn.alpha = 0;
            _canvasFadeIn.DOFade(1, 0.5f).SetEase(Ease.OutCirc).onComplete += delegate
            {
                onComplete?.Invoke();
            };
        }

        private void SceneFadeOut(UnityAction onComplete)
        {
            _canvasFadeOut.DOFade(0, 0.5f).SetEase(Ease.OutCirc).onComplete += delegate
            {
                onComplete?.Invoke();
            };
        }

        #endregion

        #endregion
    }
}