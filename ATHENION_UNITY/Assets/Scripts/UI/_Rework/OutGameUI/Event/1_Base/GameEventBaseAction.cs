﻿using System.Collections.Generic;
using Karamucho.GameEvent;
using UnityEngine;
using UnityEngine.Events;

#region Enums
public enum PlayFabMethodName
{
    //-----------Query Data Methods-------------
    GetPlayerDailyRewardData
    , GetSlotMachinePlayerData
    , GetEventDailyRewardPlayerData

    //-------------Action Methods---------------
    , GetEventDailyRewardAction
    , GetEventDailyRewardAction_New
}
#endregion

#region Data
public abstract class GameEventBaseData
{
    #region Dictionary Keys
    public static readonly string WinRewardsKey = "win_rewards";
    public static readonly string ItemIDKey = "itemID";
    public static readonly string ItemAmountKey = "amount";
    public static readonly string PlayerDataKey = "player_data";
    public static readonly string ReceiveRewardKey = "received_reward";
    #endregion

    #region Public Properties

    public string EventKey { get; }

    public bool IsActionSuccess => _isActionSuccess;
    public bool IsLoaded { get; protected set; }
    public GameEventStartType PlayTriggerType => _playTriggerType;
    public List<int> RequireTicket => _requireTicket;
    public int DefaultPlaytime => _defaultPlaytime;
    public float DefaultCooldown => _defaultCooldown;
    public bool IsRepeatable => _isRepeatable;
    public ItemData TicketData => _ticketData;
    public GameEventThemeInfo EventThemeInfo => _eventThemeInfo;
    #endregion

    #region Protected Properties
    protected GameEventStartType _playTriggerType;
    protected List<int> _requireTicket;
    protected int _defaultPlaytime;
    protected float _defaultCooldown;
    protected bool _isRepeatable;
    protected bool _isActionSuccess;

    protected ItemData _ticketData;
    protected GameEventThemeInfo _eventThemeInfo;

    protected Dictionary<string, object> EventDataDict;
    protected Dictionary<string, object> PlayerDataDict;
    protected UnityAction OnComplete;
    #endregion

    #region Constructor
    protected GameEventBaseData(string eventKey)
    {
        EventKey = eventKey;
        _eventThemeInfo = DataManager.Instance.GetGameEventDB().GetCurrentTheme();
    }
    #endregion

    #region Methods
    protected virtual void InitData(CloudScriptResultProcess eventData)
    {
        bool isSuccess = true;

        isSuccess &= GameHelper.TryDeserializeJSONStr(eventData.CustomData[GameEventTemplateKeys.EventDataKey], out EventDataDict);
        isSuccess &= GameHelper.StrToEnum(EventDataDict[GameEventTemplateKeys.EventPlayTriggerKey].ToString(), out _playTriggerType);
        isSuccess &= GameHelper.TryDeserializeJSONStr(EventDataDict[GameEventTemplateKeys.EventRequireTicketKey].ToString(), out _requireTicket);
        isSuccess &= int.TryParse(EventDataDict[GameEventTemplateKeys.EventDefaultPlaytimeKey].ToString(), out _defaultPlaytime);
        isSuccess &= float.TryParse(EventDataDict[GameEventTemplateKeys.EventDefaultCooldownKey].ToString(), out _defaultCooldown);
        isSuccess &= bool.TryParse(EventDataDict[GameEventTemplateKeys.EventDailyRepeatableKey].ToString(), out _isRepeatable);

        isSuccess &= GameHelper.TryDeserializeJSONStr(eventData.CustomData[PlayerDataKey], out PlayerDataDict);

        IsLoaded = isSuccess;
    }

    public void UpdateActionStatus(bool status)
    {
        _isActionSuccess = status;
    }
    #endregion
}
#endregion

public abstract class GameEventBaseAction : MonoBehaviour
{
    #region Static Value
    #region Localization Key
    protected static readonly string InitFailTitle = "GAME_EVENT_INIT_FAILED_TITLE";
    protected static readonly string InitFailMessage = "GAME_EVENT_INIT_FAILED_MESSAGE";
    protected static readonly string ActionFailTitle = "GAME_EVENT_ACTION_FAIL_TITLE";
    protected static readonly string ActionFailMessage = "GAME_EVENT_ACTION_FAIL_MESSAGE";
    #endregion
    #endregion

    #region Inspector Properties
    [Header("Main Setting")]
    [SerializeField] protected PlayFabMethodName QueryDataMethod;
    [SerializeField] protected PlayFabMethodName ActionMethod;
    #endregion

    #region Methods
    protected virtual void Awake()
    {
        GameEventOutGameManager.Instance.InitEventActionController(this);
    }

    public abstract void Init(string eventKey, UnityAction<GameEventBaseData> onComplete, UnityAction<string> onFail);

    protected abstract void Prepare(int actionIndex, UnityAction<bool> onComplete);

    protected abstract void OnAction(int actionIndex, UnityAction onComplete);

    protected abstract void OnUpdate(UnityAction<GameEventBaseData> onComplete);

    public virtual void Action(int actionIndex)
    {
        Prepare(actionIndex
            , prepareResult =>
            {
                if (prepareResult)
                {
                    OnAction(actionIndex
                    , () =>
                    {
                        OnUpdate(GameEventOutGameManager.Instance.RequestEventUpdate);
                    });
                }
                else
                {
                    OnUpdate(GameEventOutGameManager.Instance.RequestEventUpdate);
                }
            });
    }

    #endregion
}