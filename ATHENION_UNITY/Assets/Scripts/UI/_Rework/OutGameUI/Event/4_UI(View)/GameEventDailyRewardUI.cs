﻿using System;
using System.Collections.Generic;
using Karamucho.GameEvent;
using UnityEngine;
using UnityEngine.Events;

public class GameEventDailyRewardUI : MonoBehaviour, IGameEventInit, IGameEventActivate, IGameEventDeactivate, IGameEventUpdate, IGameEventManager
{
    #region Inspector Properties

    [Header("Daily Reward Item UI Setting.")]
    [SerializeField] private List<GameEventDailyRewardItemUI> _dailyRewardItemList;
    [SerializeField] private Color _backgroundImageColor;
    #endregion

    #region Private Properties

    private IList<IGameEventActivate> _activateList;
    private IList<IGameEventDeactivate> _deactivateList;

    #endregion

    #region Methods

    #region Unity Callback Methods

    private void Start()
    {
        GameEventOutGameManager.Instance.AddGameEventInit(this);
        GameEventOutGameManager.Instance.AddGameEventActivate(this);
        GameEventOutGameManager.Instance.AddGameEventDeactivate(this);
        GameEventOutGameManager.Instance.AddGameEventUpdate(this);

        _activateList = new List<IGameEventActivate>();
        _deactivateList = new List<IGameEventDeactivate>();
    }

    #endregion

    #region Interface Methods

    public void Init(GameEventBaseData data, UnityAction onComplete = null)
    {
        if (_dailyRewardItemList != null && _dailyRewardItemList.Count > 0)
        {
            var eventData = (GameEventDailyRewardData) data;
            int currentRewardIndex = 0;
            if (eventData.IsActionValid)
                currentRewardIndex = eventData.ReceiveRewardDict.Count;

            for (int i = 0; i < _dailyRewardItemList.Count; i++)
            {
                DailyRewardStatus status;
                if (i >= eventData.ReceiveRewardDict.Count)
                {
                    status = DailyRewardStatus.None;
                }
                else
                {
                    status = eventData.ReceiveRewardDict[i.ToString()].ToString() == DailyRewardStatus.Missed.ToString()
                        ? DailyRewardStatus.Missed
                        : DailyRewardStatus.Received;
                }

                _dailyRewardItemList[i].Init(eventData.EventKey, eventData.RewardList[i], i, status, this);
                if(i == currentRewardIndex && eventData.IsActionValid && status == DailyRewardStatus.None)
                    _dailyRewardItemList[i].ShinyController.Play();

                if(i > currentRewardIndex)
                    _dailyRewardItemList[i].SetBackgroundImageActive(_backgroundImageColor);
            }
        }

        onComplete?.Invoke();
    }

    public void Activate(UnityAction onComplete = null)
    {
        int count = _activateList.Count;
        UnityAction action = onComplete;

        foreach (var activate in _activateList)
        {
            activate.Activate(() =>
            {
                count--;
                if (count == 0)
                {
                    action?.Invoke();
                    action = null;
                }
            });
        }
    }

    public void Deactivate(UnityAction onComplete = null)
    {
        int count = _deactivateList.Count;
        UnityAction action = onComplete;

        foreach (var deactivate in _deactivateList)
        {
            deactivate.Deactivate(() =>
            {
                count--;
                if (count == 0)
                {
                    action?.Invoke();
                    action = null;
                }
            });
        }
    }

    public void EventUpdate(GameEventBaseData data, UnityAction onComplete = null)
    {
        var eventData = (GameEventDailyRewardData) data;

        if (!eventData.IsActionSuccess)
        {
            onComplete?.Invoke();
            return;
        }

        int index = eventData.ReceiveRewardDict.Count - 1;
        _dailyRewardItemList[index].ClaimRewardStatusUpdate(onComplete);
    }

    #region Regis Methods

    public void AddGameEventInit(IGameEventInit uiInstance)
    {
        // Not Used.
    }

    public void AddGameEventUpdate(IGameEventUpdate uiInstance)
    {
        // Not Used.
    }

    public void AddGameEventActivate(IGameEventActivate uiInstance)
    {
        _activateList.Add(uiInstance);
    }

    public void AddGameEventDeactivate(IGameEventDeactivate uiInstance)
    {
        _deactivateList.Add(uiInstance);
    }

    #endregion

    #endregion

    #endregion
}
