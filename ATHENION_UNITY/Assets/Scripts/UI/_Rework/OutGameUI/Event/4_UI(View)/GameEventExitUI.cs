﻿using Karamucho.GameEvent;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class GameEventExitUI : MonoBehaviour, IGameEventInit ,IGameEventActivate, IGameEventDeactivate
{
    #region UI Properties
    [Header("Exit Button UI Element")]
    [SerializeField] private Button _exitButton;
    #endregion

    #region Methods
    private void Start()
    {
        GameEventOutGameManager.Instance.AddGameEventInit(this);
        GameEventOutGameManager.Instance.AddGameEventActivate(this);
        GameEventOutGameManager.Instance.AddGameEventDeactivate(this);
    }

    /// <summary>
    /// Call this method to initialize exit button.
    /// </summary>
    /// <param name="data"></param>
    /// <param name="onComplete"></param>
    public void Init(GameEventBaseData data, UnityAction onComplete = null)
    {
        _exitButton.onClick.AddListener(GameEventOutGameManager.Instance.RequestEventExit);

        onComplete?.Invoke();
    }

    public void Activate(UnityAction onComplete = null)
    {
        _exitButton.gameObject.SetActive(true);
        _exitButton.enabled = true;

        onComplete?.Invoke();
    }

    public void Deactivate(UnityAction onComplete = null)
    {
        _exitButton.enabled = false;

        onComplete?.Invoke();
    }
    #endregion
}
