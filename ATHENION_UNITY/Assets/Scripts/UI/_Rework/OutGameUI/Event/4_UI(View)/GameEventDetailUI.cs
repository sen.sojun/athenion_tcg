﻿using DG.Tweening;
using Karamucho.GameEvent;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class GameEventDetailUI : MonoBehaviour, IGameEventInit, IGameEventActivate, IGameEventDeactivate
{
    #region Constant Value

    private const string EventDetailLocalizePrefixKey = "EVENT_DETAIL_";
    private const string EventDetailHeaderLocalizeKey = "EVENT_DETAIL_HEADER";
    #endregion

    #region UI Properties
    [Header("Event Detail UI Elements")]
    [SerializeField] private Button _readMoreButton;
    [SerializeField] private Button _hideButton;
    [SerializeField] private CanvasGroup _eventDetailImage;
    [SerializeField] private TextMeshProUGUI _eventDetailText;
    #endregion

    #region Private Properties

    private string _eventKey;

    #endregion

    #region Methods

    private void Start()
    {
        GameEventOutGameManager.Instance.AddGameEventInit(this);
        GameEventOutGameManager.Instance.AddGameEventActivate(this);
        GameEventOutGameManager.Instance.AddGameEventDeactivate(this);
    }

    public void Init(GameEventBaseData data, UnityAction onComplete = null)
    {
        _eventKey = data.EventKey;
        _readMoreButton.onClick.AddListener(ShowDetailImage);
        _hideButton.onClick.AddListener(HideDetailImage);
        _eventDetailText.text = LocalizationManager.Instance.GetText(EventDetailLocalizePrefixKey + _eventKey);

        onComplete?.Invoke();
    }

    public void Activate(UnityAction onComplete = null)
    {
        _readMoreButton.gameObject.SetActive(true);
        _readMoreButton.enabled = true;

        onComplete?.Invoke();
    }

    public void Deactivate(UnityAction onComplete = null)
    {
        _readMoreButton.enabled = false;

        onComplete?.Invoke();
    }

    private void ShowDetailImage()
    {
        //_eventDetailImage.gameObject.SetActive(true);
        //_eventDetailImage.DOFade(1, 0.5f);
        string title = LocalizationManager.Instance.GetText(EventDetailHeaderLocalizeKey);
        string message = LocalizationManager.Instance.GetText(EventDetailLocalizePrefixKey + _eventKey);

        PopupUIManager.Instance.ShowPopup_DetailWindow(title, message);
    }

    private void HideDetailImage()
    {
        _eventDetailImage.DOFade(0, 0.5f).onComplete += () =>
        {
            _eventDetailImage.gameObject.SetActive(false);
        };
    }

    #endregion
}
