﻿using System.Collections;
using System.Collections.Generic;
using Karamucho.GameEvent;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class GameEventCreateUI : MonoBehaviour, IGameEventInit, IGameEventActivate, IGameEventDeactivate, IGameEventUpdate, IGameEventManager
{
    #region Public Properties
    #endregion

    #region Private Properties
    private IList<IGameEventActivate> _activateList;
    private IList<IGameEventDeactivate> _deactivateList;

    private List<object> _itemList;
    #endregion

    #region UI Properties
    [Header("UI Elements")]
    [SerializeField] private RectTransform _itemsParent;
    [SerializeField] private GameObject _itemPrefab;
    #endregion

    #region Methods
    #region Unity Callback Methods
    private void Start()
    {
        GameEventOutGameManager.Instance.AddGameEventInit(this);
        GameEventOutGameManager.Instance.AddGameEventActivate(this);
        GameEventOutGameManager.Instance.AddGameEventDeactivate(this);
        GameEventOutGameManager.Instance.AddGameEventUpdate(this);

        _activateList = new List<IGameEventActivate>();
        _deactivateList = new List<IGameEventDeactivate>();
    }
    #endregion

    #region Interface Methods
    public void Init(GameEventBaseData data, UnityAction onComplete = null)
    {
        switch (data.GetType().ToString())
        {
        }
    }

    public void Activate(UnityAction onComplete = null)
    {
        int count = _activateList.Count;
        UnityAction action = onComplete;

        foreach (var activate in _activateList)
        {
            activate.Activate(() =>
            {
                count--;
                if (count == 0)
                {
                    action?.Invoke();
                    action = null;
                }
            });
        }
    }

    public void Deactivate(UnityAction onComplete = null)
    {
        int count = _deactivateList.Count;
        UnityAction action = onComplete;

        foreach (var deactivate in _deactivateList)
        {
            deactivate.Deactivate(() =>
            {
                count--;
                if (count == 0)
                {
                    action?.Invoke();
                    action = null;
                }
            });
        }
    }

    public void EventUpdate(GameEventBaseData data, UnityAction onComplete = null)
    {
        switch (data.GetType().ToString())
        {
            
        }
    }
    #endregion

    #region Request Methods
    public void RequestSelectAction(int actionIndex)
    {
        // TODO: Sent request to Main game event manager for execute action.
    }
    #endregion

    #region Regis Methods

    public void AddGameEventInit(IGameEventInit uiInstance)
    {
        // Not Used.
    }

    public void AddGameEventUpdate(IGameEventUpdate uiInstance)
    {
        // Not Used.
    }

    public void AddGameEventActivate(IGameEventActivate uiInstance)
    {
        _activateList.Add(uiInstance);
    }

    public void AddGameEventDeactivate(IGameEventDeactivate uiInstance)
    {
        _deactivateList.Add(uiInstance);
    }
    #endregion

    #region Generate Methods
    #endregion

    #region Update Generated Item Methods
    #endregion
    #endregion
}
