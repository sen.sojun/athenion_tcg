﻿using Karamucho.GameEvent;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class GameEventActionUI : MonoBehaviour, IGameEventInit, IGameEventActivate, IGameEventDeactivate, IGameEventUpdate
{
    #region Static & Constant Value
    private const string EventButtonImageKey = "ACTION_BUTTON";
    private const string EventButtonTextKey = "EVENT_START_BUTTON_TEXT_";
    #endregion

    #region Public Properties
    #endregion

    #region Private Properties
    private int _actionIndex = -1;
    private string _eventKey;
    private bool _isActionValid;
    #endregion

    #region Inspector Properties
    [Header("UI Elements.")]
    [SerializeField] protected Image EventButtonImage;
    [SerializeField] protected TextMeshProUGUI EventButtonText;
    [SerializeField] protected Button EventButton;
    [SerializeField] protected Material EventButtonDisableMaterial;
    #endregion

    #region Methods
    #region Unity Callback Methods
    protected void Start()
    {
        GameEventOutGameManager.Instance.AddGameEventInit(this);
        GameEventOutGameManager.Instance.AddGameEventActivate(this);
        GameEventOutGameManager.Instance.AddGameEventDeactivate(this);
        GameEventOutGameManager.Instance.AddGameEventUpdate(this);
    }
    #endregion

    #region Interface Methods
    public void Init(GameEventBaseData data, UnityAction onComplete = null)
    {
        switch (data)
        {
            case GameEventDailyRewardData dailyRewardData: InitGameEventDailyRewardAction(dailyRewardData, onComplete); break;
            default: onComplete?.Invoke(); break;
        }
    }

    public void Activate(UnityAction onComplete = null)
    {
        EventButton.gameObject.SetActive(true);
        if (EventButtonText != null) EventButtonText.gameObject.SetActive(true);

        if (_isActionValid)
        {
            EventButton.enabled = true;
            EventButtonImage.material = null;
        }

        onComplete?.Invoke();
    }

    public void Deactivate(UnityAction onComplete = null)
    {
        EventButton.enabled = false;
        if (EventButtonDisableMaterial != null) EventButtonImage.material = EventButtonDisableMaterial;
        onComplete?.Invoke();
    }

    public void EventUpdate(GameEventBaseData data, UnityAction onComplete = null)
    {
        switch (data)
        {
            case GameEventDailyRewardData dailyRewardData: UpdateGameEventDailyRewardAction(dailyRewardData, onComplete); break;
            default: onComplete?.Invoke(); break;
        }
    }
    #endregion

    #region Game Event Action Init Methods
    private void InitGameEventDailyRewardAction(GameEventDailyRewardData data, UnityAction onComplete = null)
    {
        _eventKey = data.EventKey;
        _isActionValid = data.IsActionValid && data.ReceiveRewardDict.Count < data.RewardList.Count;

        if (EventButtonImage != null)
        {
            EventButtonImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, EventButtonImageKey);
            if (!_isActionValid) EventButtonImage.material = EventButtonDisableMaterial;
        }
        if(EventButtonText != null) EventButtonText.text = LocalizationManager.Instance.GetText(EventButtonTextKey + _eventKey);
        if (EventButton != null)
        {
            EventButton.onClick.AddListener(() => { GameEventOutGameManager.Instance.RequestEventAction(_actionIndex); });
            EventButton.enabled = _isActionValid;
        }

        onComplete?.Invoke();
    }
    #endregion

    #region Game Event Action Update Methods
    private void UpdateGameEventDailyRewardAction(GameEventDailyRewardData data, UnityAction onComplete = null)
    {
        if (data.IsLoaded && !data.IsActionValid && data.IsActionSuccess)
        {
            _isActionValid = false;
        }
        onComplete?.Invoke();
    }
    #endregion
    #endregion
}