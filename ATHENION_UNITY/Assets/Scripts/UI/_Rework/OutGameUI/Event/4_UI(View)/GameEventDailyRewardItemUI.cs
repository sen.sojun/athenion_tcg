﻿using System;
using System.Collections;
using System.Collections.Generic;
using Coffee.UIExtensions;
using DG.Tweening;
using MissionCommandData;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GameEventDailyRewardItemUI : MonoBehaviour, IGameEventActivate, IGameEventDeactivate
{
    #region Static Value
    private static readonly string RewardBgKey = "REWARD_BG";
    private static readonly string RewardReceivedKey = "REWARD_RECEIVED";
    private static readonly string RewardMissedKey = "REWARD_MISSED";
    private static readonly string RewardTextLabelKey = "REWARD_LABEL";
    private static readonly string RewardIconKey = "REWARD_ICON_";

    private static readonly string RewardTitleKey = "EVENT_CONGRATS";
    private static readonly string RewardMessageKey = "EVENT_YOUVEGOT";
    private static readonly string RewardDetailTitleKey = "EVENT_REWARD_TITLE_";
    private static readonly string RewardDetailMessageKey = "EVENT_REWARD_MESSAGE_";
    #endregion

    #region Public Properties
    public List<ItemData> ItemData => _itemData;
    public UIShiny ShinyController => _uiShinyController;
    public int RewardIndex => _rewardIndex;
    #endregion

    #region Private Properties
    private string _eventKey;
    private List<ItemData> _itemData;
    private UIShiny _uiShinyController;
    private int _rewardIndex;
    #endregion

    #region UI Properties
    [Header("UI Elements.")]
    [SerializeField] private Image _rewardBgImage;
    [SerializeField] private Image _rewardIconImage;
    [SerializeField] private Image _rewardStatusImage;
    [SerializeField] private Image _rewardTextLabelImage;
    [SerializeField] private Button _rewardButton;

    [Header("Other Setting.")]
    [SerializeField] private SoundManager.SFX _receiveSound;
    #endregion

    #region Methods
    #region Initial Methods
    public void Init(string eventKey, List<ItemData> itemData, int rewardIndex, DailyRewardStatus status, IGameEventManager superiorManager)
    {
        #region Set Properties
        _eventKey = eventKey;
        _itemData = itemData;
        _uiShinyController = GetComponent<UIShiny>();
        _rewardIndex = rewardIndex;
        #endregion

        #region Image
        if (_rewardBgImage != null) _rewardBgImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, RewardBgKey);
        if (_rewardTextLabelImage != null) _rewardTextLabelImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, RewardTextLabelKey);
        if (_rewardIconImage != null) _rewardIconImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, RewardIconKey + _rewardIndex);
        SetRewardStatusImage(status);
        #endregion

        #region Button

        if (_rewardButton != null)
        {
            _rewardButton.onClick.RemoveAllListeners();
            _rewardButton.onClick.AddListener(ShowItemPackDetail);
        }

        #endregion

        #region Higher-Manager Registration
        superiorManager?.AddGameEventActivate(this);
        superiorManager?.AddGameEventDeactivate(this);
        #endregion
    }
    #endregion

    #region Interface Methods
    public void Activate(UnityAction onComplete = null)
    {
        if(_rewardBgImage != null) _rewardBgImage.gameObject.SetActive(true);
        if(_rewardIconImage != null) _rewardIconImage.gameObject.SetActive(true);
        if(_rewardStatusImage != null) _rewardStatusImage.gameObject.SetActive(true);
        if(_rewardTextLabelImage != null) _rewardTextLabelImage.gameObject.SetActive(true);
        onComplete?.Invoke();
    }

    public void Deactivate(UnityAction onComplete = null)
    {
        onComplete?.Invoke();
    }
    #endregion

    #region Other Methods

    private void ShowItemPackDetail()
    {
        PopupUIManager.Instance.ShowPopup_PreviewReward(
            string.Format(LocalizationManager.Instance.GetText(RewardDetailTitleKey + _eventKey), (RewardIndex + 1))
            , string.Format(LocalizationManager.Instance.GetText(RewardDetailMessageKey + _eventKey), (RewardIndex + 1))
            , _itemData
        );
    }

    public void ClaimRewardStatusUpdate(UnityAction onComplete)
    {
        _uiShinyController.Stop();
        SetRewardStatusImage(DailyRewardStatus.Received);
        _rewardStatusImage.color = new Color(1,1,1,0);
        _rewardStatusImage.rectTransform.localScale = Vector3.one * 1.5f; 

        _rewardStatusImage.DOFade(1, 0.3f).SetEase(Ease.Linear);
        _rewardStatusImage.rectTransform.DOScale(Vector3.one * 1.75f, 0.25f).SetEase(Ease.Linear).onComplete += () =>
        {
            _rewardStatusImage.rectTransform.DOScale(Vector3.one, 0.15f).SetEase(Ease.Linear).onComplete += () =>
            {
                SoundManager.PlaySFX(_receiveSound);
                _uiShinyController.loop = false;
                _uiShinyController.Play();

                StartCoroutine(WaitForSec(1, () =>
                {
                    PopupUIManager.Instance.ShowPopup_GotRewardAnimated(
                            LocalizationManager.Instance.GetText(RewardTitleKey)
                            , LocalizationManager.Instance.GetText(RewardMessageKey)
                            , _itemData
                            , onComplete
                        ).SetAnimationEase(Ease.OutExpo)
                        .SetDelay(0.1f)
                        .SetAnimationDuration(0.25f)
                        .SetMaxScale(8f);
                }));
            };
        };
    }

    private IEnumerator WaitForSec(float second, UnityAction onComplete)
    {
        yield return new WaitForSeconds(second);
        onComplete?.Invoke();
    }

    private void SetRewardStatusImage(DailyRewardStatus status)
    {
        if(_rewardStatusImage == null) return;
        switch (status)
        {
            case DailyRewardStatus.None:
                _rewardStatusImage.sprite = null;
                _rewardStatusImage.enabled = false;
                break;
            case DailyRewardStatus.Received:
                _rewardStatusImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, RewardReceivedKey);
                _rewardStatusImage.enabled = true;
                break;
            case DailyRewardStatus.Missed:
                _rewardStatusImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, RewardMissedKey);
                _rewardStatusImage.enabled = true;
                break;
        }
    }

    public void SetBackgroundImageActive(Color color)
    {
        _rewardBgImage.color = color;
    }
    #endregion
    #endregion
}
