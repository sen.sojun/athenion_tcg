﻿using System.Collections.Generic;
using Karamucho.GameEvent;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class GameEventStaticUI : MonoBehaviour, IGameEventInit, IGameEventActivate, IGameEventDeactivate
{
    #region Static & Constant Value
    private const string EventHeaderKey = "EVENT_HEADER_";
    private const string EventObjectiveKey = "EVENT_OBJECTIVE_";

    private const string EventMainBackgroundSuffix = "_INGAME";
    private const string EventHeaderBannerSuffix = "_BANNER";
    private const string StaticNativeImagePrefixKey = "IMG_";
    private const string StaticNonNativeImagePrefixKey = "N_IMG_";
    #endregion

    #region UI Propeties
    [Header("Image UI Elements")]
    [SerializeField] private Image _eventMainBgImage;
    [SerializeField] private Image _eventHeaderBgImage;
    [SerializeField] private List<Image> _nativeStaticImageList;
    [SerializeField] private List<Image> _noneNativeStaticImageList;

    [Header("Text UI Elements")]
    [SerializeField] private TextMeshProUGUI _eventHeaderText;
    [SerializeField] private TextMeshProUGUI _eventObjectiveText;
    #endregion

    #region Methods
    private void Start()
    {
        GameEventOutGameManager.Instance.AddGameEventInit(this);
        GameEventOutGameManager.Instance.AddGameEventActivate(this);
        GameEventOutGameManager.Instance.AddGameEventDeactivate(this);
    }

    public void Init(GameEventBaseData data, UnityAction onComplete = null)
    {
        #region Image
        _eventMainBgImage.sprite = SpriteResourceHelper.LoadSubEventImage(data.EventKey, data.EventThemeInfo.Theme + EventMainBackgroundSuffix, true) == null 
            ? SpriteResourceHelper.LoadSprite(data.EventThemeInfo.Theme, SpriteResourceHelper.SpriteType.GAME_Event) 
            : SpriteResourceHelper.LoadSubEventImage(data.EventKey, data.EventThemeInfo.Theme + EventMainBackgroundSuffix);

        if(_eventHeaderBgImage != null) _eventHeaderBgImage.sprite = SpriteResourceHelper.LoadSprite(data.EventThemeInfo.Theme + EventHeaderBannerSuffix, SpriteResourceHelper.SpriteType.GAME_Event);

        if (_nativeStaticImageList != null)
        {
            for (int i = 0; i < _nativeStaticImageList.Count; i++)
            {
                _nativeStaticImageList[i].sprite = SpriteResourceHelper.LoadSubEventImage(data.EventKey, StaticNativeImagePrefixKey + i);
                _nativeStaticImageList[i].SetNativeSize();
            }
        }

        if (_noneNativeStaticImageList != null)
        {
            for (int i = 0; i < _noneNativeStaticImageList.Count; i++)
            {
                _noneNativeStaticImageList[i].sprite = SpriteResourceHelper.LoadSubEventImage(data.EventKey, StaticNonNativeImagePrefixKey + i);
                _noneNativeStaticImageList[i].enabled = true;
            }
        }
        #endregion

        #region Text
        if (_eventHeaderText != null) _eventHeaderText.text = LocalizationManager.Instance.GetText(EventHeaderKey + data.EventKey);
        if (_eventObjectiveText != null) _eventObjectiveText.text = LocalizationManager.Instance.GetText(EventObjectiveKey + data.EventKey);
        #endregion

        onComplete?.Invoke();
    }

    public void Activate(UnityAction onComplete = null)
    {
        #region Image
        _eventMainBgImage.gameObject.SetActive(true);
        if (_eventHeaderBgImage != null) _eventHeaderBgImage.gameObject.SetActive(true);

        _eventMainBgImage.enabled = true;
        if (_eventHeaderBgImage != null) _eventHeaderBgImage.enabled = true;

        if (_nativeStaticImageList != null)
        {
            foreach (var image in _nativeStaticImageList)
            {
                image.enabled = true;
            }
        }
        if (_noneNativeStaticImageList != null)
        {
            foreach (var image in _noneNativeStaticImageList)
            {
                image.enabled = true;
            }
        }
        #endregion

        #region Text
        if (_eventHeaderText != null) _eventHeaderText.gameObject.SetActive(true);
        if (_eventObjectiveText != null) _eventObjectiveText.gameObject.SetActive(true);
        #endregion

        onComplete?.Invoke();
    }

    public void Deactivate(UnityAction onComplete = null)
    {
        onComplete?.Invoke();
    }
    #endregion
}
