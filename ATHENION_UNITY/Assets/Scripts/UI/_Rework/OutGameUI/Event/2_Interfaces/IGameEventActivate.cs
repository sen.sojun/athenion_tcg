﻿using UnityEngine.Events;

public interface IGameEventActivate
{
    void Activate(UnityAction onComplete = null);
}
