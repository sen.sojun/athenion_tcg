﻿using UnityEngine.Events;

public interface IGameEventInit
{
    void Init(GameEventBaseData data, UnityAction onComplete = null);
}
