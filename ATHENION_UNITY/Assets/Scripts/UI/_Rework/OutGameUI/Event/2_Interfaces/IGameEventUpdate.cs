﻿using UnityEngine.Events;

public interface IGameEventUpdate
{
    void EventUpdate(GameEventBaseData data, UnityAction onComplete = null);
}
