﻿public interface IGameEventManager
{
    void AddGameEventInit(IGameEventInit uiInstance);
    void AddGameEventUpdate(IGameEventUpdate uiInstance);
    void AddGameEventActivate(IGameEventActivate uiInstance);
    void AddGameEventDeactivate(IGameEventDeactivate uiInstance);
}
