﻿using UnityEngine.Events;

public interface IGameEventDeactivate
{
    void Deactivate(UnityAction onComplete = null);
}
