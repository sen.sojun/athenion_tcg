﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;

public class SelectPackGroupUI : MonoBehaviour
{
    public Image ActiveImage;
    public Button AddBTN;
    public Button SubBTN;
    public TextMeshProUGUI AmountText;

    public void SetEvent(UnityAction onClickAdd, UnityAction onClickSub)
    {
        AddBTN.onClick.RemoveAllListeners();
        AddBTN.onClick.AddListener(onClickAdd);

        SubBTN.onClick.RemoveAllListeners();
        SubBTN.onClick.AddListener(onClickSub);
    }

    public void SetAmount(int amount)
    {
        AmountText.text = amount.ToString();
    }

    public void SetActive(bool isActive)
    {
       gameObject.SetActive(isActive);
    }
}
