﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

public class PurchasePackPopupUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _priceDetailText;
    [SerializeField] private Image _packImage;

    private int CurrentDIAmount;
    private int CurrentCOAmount;

    private bool CanActiveCoinPanel;
    private bool CanActiveDiamondPanel;

    public SelectPackGroupUI SelectCoinGroup;
    public SelectPackGroupUI SelectDiamondGroup;

    public Button ConfirmBTN;
    public Button CancelBTN;

    public BoosterPackShopData Data { get; private set; }

    List<ShopItemData> diamondItemList;
    List<ShopItemData> coinItemList;

    public PurchasePackConfirmUI ConfirmUI;

    public ShopItemData CurrentItemData { get; set; }

    public void SetData(BoosterPackShopData data)
    {
        ResetToDefault();
        this.Data = data;

        _packImage.sprite = SpriteResourceHelper.LoadSprite(data.PackID, SpriteResourceHelper.SpriteType.ICON_Item);

        coinItemList = data.ShopItemDataList.FindAll(item => item.Currency == VirtualCurrency.CO);
        coinItemList.Sort((a, b) => a.Price.CompareTo(b.Price));
        diamondItemList = data.ShopItemDataList.FindAll(item => item.Currency == VirtualCurrency.DI);
        diamondItemList.Sort((a, b) => a.Price.CompareTo(b.Price));

        SelectCoinGroup.SetEvent(OnClickAddCoinButton, OnClickSubCoinButton);
        SelectDiamondGroup.SetEvent(OnClickAddDiamondButton, OnClickSubDiamondButton);

        CanActiveCoinPanel = coinItemList.Count > 0;
        SelectCoinGroup.SetActive(CanActiveCoinPanel);

        CanActiveDiamondPanel = diamondItemList.Count > 0;
        SelectDiamondGroup.SetActive(CanActiveDiamondPanel);

    }

    public void ResetToDefault()
    {
        CurrentItemData = null;

        CurrentCOAmount = 0;
        CurrentDIAmount = 0;

        SelectCoinGroup.SetAmount(0);
        SelectCoinGroup.SetActive(true);

        SelectDiamondGroup.SetAmount(0);
        SelectDiamondGroup.SetActive(true);

        _priceDetailText.text = "";
    }

    private ItemData GetItemData(List<ShopItemData> shopItemData, int index)
    {
        return shopItemData[index].CustomData.ItemDetail[0];
    }

    public void OnClickAddCoinButton()
    {
        CurrentCOAmount++;
        if (CurrentCOAmount > coinItemList.Count)
            CurrentCOAmount = coinItemList.Count;
        SetAmountPanel(SelectCoinGroup, coinItemList, CurrentCOAmount);
        OnAmountChange();
    }

    public void OnClickSubCoinButton()
    {
        CurrentCOAmount--;
        if (CurrentCOAmount < 0)
            CurrentCOAmount = 0;
        SetAmountPanel(SelectCoinGroup, coinItemList, CurrentCOAmount);
        OnAmountChange();
    }

    private void SetAmountPanel(SelectPackGroupUI panel, List<ShopItemData> itemList, int amount)
    {
        if (amount > 0)
        {
            int index = amount - 1;
            ItemData itemData = GetItemData(itemList, index);
            CurrentItemData = itemList[index];
            panel.SetAmount(itemData.Amount);
            _priceDetailText.text = CalculatePrice(CurrentItemData);
        }
        else
        {
            CurrentItemData = null;
            panel.SetAmount(0);
            _priceDetailText.text = "";
        }
    }

    private string CalculatePrice(ShopItemData shopItemData)
    {
        return LocalizationManager.Instance.GetText("TEXT_TOTAL") + " " + shopItemData.Price + " " + GameHelper.TMPro_GetEmojiCurrency(shopItemData.Currency);
    }

    public void OnClickAddDiamondButton()
    {
        CurrentDIAmount++;
        if (CurrentDIAmount > diamondItemList.Count)
            CurrentDIAmount = diamondItemList.Count;
        SetAmountPanel(SelectDiamondGroup, diamondItemList, CurrentDIAmount);
        OnAmountChange();
    }

    public void OnClickSubDiamondButton()
    {
        CurrentDIAmount--;
        if (CurrentDIAmount < 0)
            CurrentDIAmount = 0;
        SetAmountPanel(SelectDiamondGroup, diamondItemList, CurrentDIAmount);
        OnAmountChange();
    }

    private void OnAmountChange()
    {
        SelectCoinGroup.SetActive(true && CanActiveCoinPanel);
        SelectDiamondGroup.SetActive(true && CanActiveDiamondPanel);

        if (CurrentCOAmount > 0)
            SelectDiamondGroup.SetActive(false);
        if (CurrentDIAmount > 0)
            SelectCoinGroup.SetActive(false);
    }

    public void SetEvent(UnityAction onClose, UnityAction<ShopItemData> onBuy, UnityAction<ShopItemData> NotEnoughMoney)
    {
        CancelBTN.onClick.RemoveAllListeners();
        CancelBTN.onClick.AddListener(onClose);

        ConfirmBTN.onClick.RemoveAllListeners();
        ConfirmBTN.onClick.AddListener(() =>
        {
            if (CurrentItemData == null)
                return;

            bool isEnoughMoney = DataManager.Instance.InventoryData.GetVirtualCurrency(CurrentItemData.Currency) >= CurrentItemData.Price;
            if (isEnoughMoney)
            {
                ConfirmUI.SetData(CurrentItemData);
                ConfirmUI.SetEvent(
                    delegate
                    {
                        ConfirmUI.HideUI();
                        //onClose?.Invoke();
                    },
                    delegate (ShopItemData itemData)
                    {
                        ConfirmUI.HideUI();
                        onBuy?.Invoke(itemData);
                    });
                ConfirmUI.ShowUI();
            }
            else
            {
                NotEnoughMoney?.Invoke(CurrentItemData);
            }
        });
    }

    public void ShowUI()
    {
        GameHelper.UITransition_FadeIn(this.gameObject);
    }

    public void HideUI()
    {
        GameHelper.UITransition_FadeOut(this.gameObject);
    }

}
