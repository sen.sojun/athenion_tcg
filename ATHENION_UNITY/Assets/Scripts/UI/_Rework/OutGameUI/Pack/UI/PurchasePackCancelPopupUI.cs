﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PurchasePackCancelPopupUI : MonoBehaviour
{
    public float AutoFadeDuration;
    public float FadeDuration;
    public CanvasGroup canvasGroup;
    public Button ClickBTN;

    private void OnEnable()
    {
        ClickBTN.onClick.RemoveAllListeners();
        ClickBTN.onClick.AddListener(OnClick);
    }

    private void OnDisable()
    {

    }

    private void OnClick()
    {
        HideUI();
    }

    public void ShowUI()
    {
        canvasGroup.alpha = 1;
        this.gameObject.SetActive(true);
        StartCoroutine(AutoFade());
    }

    public void HideUI()
    {
        this.gameObject.SetActive(false);
        StopAllCoroutines();
    }

    IEnumerator AutoFade()
    {
        yield return new WaitForSeconds(AutoFadeDuration);
        float counter = 0;
        while (counter < FadeDuration)
        {
            canvasGroup.alpha = Mathf.Lerp(1, 0, counter / FadeDuration);
            counter += Time.deltaTime;
            yield return null;
        }
        HideUI();
    }

}
