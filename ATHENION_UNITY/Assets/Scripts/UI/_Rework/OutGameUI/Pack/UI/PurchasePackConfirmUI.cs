﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using UnityEngine.Events;

public class PurchasePackConfirmUI : MonoBehaviour
{
    public ShopItemData Data { get; private set; }
    public Image ItemImage;
    public TextMeshProUGUI DetailText;

    public Button CancelBTN;
    public Button ConfirmBTN;

    public void SetData(ShopItemData itemData)
    {
        Data = itemData;

        string packID = itemData.CustomData.ItemDetail[0].ItemID;
        ItemImage.sprite = SpriteResourceHelper.LoadSprite(packID, SpriteResourceHelper.SpriteType.ICON_Item);

        // do you want to buy "WOE Pack" 100 diamond ? 
        DetailText.text = string.Format(
            LocalizationManager.Instance.GetText("TEXT_CONFIRM_PURCHASE_DESCRIPTION"),
            LocalizationManager.Instance.GetItemName(packID),
            itemData.Price + " " + GameHelper.TMPro_GetEmojiCurrency(itemData.Currency));

    }

    public void SetEvent(UnityAction onCancel, UnityAction<ShopItemData> onConfirm)
    {
        CancelBTN.onClick.RemoveAllListeners();
        CancelBTN.onClick.AddListener(onCancel);

        ConfirmBTN.onClick.RemoveAllListeners();
        ConfirmBTN.onClick.AddListener(() => onConfirm?.Invoke(this.Data));

    }

    public void ShowUI()
    {
        GameHelper.UITransition_FadeIn(this.gameObject);
    }

    public void HideUI()
    {
        GameHelper.UITransition_FadeOut(this.gameObject);
    }
}
