﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;


public class LessonPopup : MonoBehaviour
{
    #region Inspector
    [Header("Lesson")]
    [SerializeField] private GameObject GROUP_Art;
    [SerializeField] private Image IMG_Lesson;
    [SerializeField] private TextMeshProUGUI TEXT_Title;
    [SerializeField] private TextMeshProUGUI TEXT_Detail;

    [Header("BTN")]
    [SerializeField] private Button BTN_Back;
    [SerializeField] private Button BTN_Next;
    [SerializeField] private Button BTN_Confirm;

    [Header("Dot Group")]
    [SerializeField] private GameObject DotPrefab;
    [SerializeField] private Transform GROUP_Dot;
    [SerializeField] private Color COLOR_Normal;
    [SerializeField] private Color COLOR_Active;
    [SerializeField] private TextMeshProUGUI TEXT_Page;
    #endregion

    #region Private
    private List<string> _idList;
    private int _currentPage = 0;
    private int _maxPage = 0;
    private List<Image> _dotList = new List<Image>();
    private UnityAction _onCloseAction;
    #endregion

    #region Methods
    public void ShowUI(string lessonID, List<string> idList, UnityAction onClose = null)
    {
        _idList = idList;
        _onCloseAction = onClose;
        _maxPage = idList.Count;
        _currentPage = 0;
        TEXT_Title.text = LocalizationManager.Instance.GetText("LESSON_DESCRIPTION_" + lessonID);

        InitBTN();
        RefreshPage();

        GameHelper.UITransition_FadeIn(this.gameObject);
    }

    public void HideUI()
    {
        GameHelper.UITransition_FadeOut(this.gameObject, _onCloseAction);
        _onCloseAction = null;
    }

    private void InitBTN()
    {
        BTN_Back.onClick.RemoveAllListeners();
        BTN_Back.onClick.AddListener(PreviousPage);

        BTN_Next.onClick.RemoveAllListeners();
        BTN_Next.onClick.AddListener(NextPage);

        BTN_Confirm.onClick.RemoveAllListeners();
        BTN_Confirm.onClick.AddListener(HideUI);
    }

    private void UpdatePage()
    {
        GameHelper.UITransition_FadeIn(GROUP_Art.gameObject);

        IMG_Lesson.sprite = SpriteResourceHelper.LoadSprite(_idList[_currentPage], SpriteResourceHelper.SpriteType.Lesson);
        TEXT_Detail.text = LocalizationManager.Instance.GetText("LESSON_DESCRIPTION_" + _idList[_currentPage]);
    }
    #endregion

    #region Navigator
    private void RefreshPage()
    {
        RefreshDotPages();
        RefreshPageNumber();
        UpdatePage();
    }

    private void RefreshDotPages()
    {
        BTN_Back.gameObject.SetActive(_currentPage != 0);
        BTN_Next.gameObject.SetActive(_currentPage != _maxPage - 1);

        // Clear Dot
        foreach (Image item in _dotList)
        {
            Destroy(item.gameObject);
        }
        _dotList.Clear();

        // Create Dot
        while (_dotList.Count < _maxPage)
        {
            CreateDotPage();
        }

        // Change Dot Color
        for (int i = 0; i < _dotList.Count; i++)
        {
            if (i == _currentPage)
            {
                _dotList[i].color = COLOR_Active;
            }
            else
            {
                _dotList[i].color = COLOR_Normal;
            }
        }
    }

    private void RefreshPageNumber()
    {
        TEXT_Page.text = string.Format("{0} / {1}", _currentPage + 1, _maxPage);
    }

    private void CreateDotPage()
    {
        GameObject obj = Instantiate(DotPrefab, GROUP_Dot);
        _dotList.Add(obj.GetComponent<Image>());
    }

    private void NextPage()
    {
        if (_currentPage < _maxPage)
        {
            _currentPage++;
        }

        RefreshPage();
    }

    private void PreviousPage()
    {
        if (_currentPage > 0)
        {
            _currentPage--;
        }

        RefreshPage();
    }

    #endregion
}
