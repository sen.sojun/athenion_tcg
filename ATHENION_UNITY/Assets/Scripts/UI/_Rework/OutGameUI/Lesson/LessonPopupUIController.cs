﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;

public class LessonPopupUIController : MonoBehaviour
{
    #region Inspector
    [SerializeField] private GameObject Prefab_LessonCell;
    [SerializeField] private Transform GROUP_content;

    [SerializeField] private LessonPopup Popup_Lesson;
    [SerializeField] private Button BTN_Close;
    #endregion

    #region Private
    private Dictionary<string, List<string>> _lessonDict = new Dictionary<string, List<string>>();
    private List<LessonCell> _cellList = new List<LessonCell>();
    #endregion

    #region Methods
    public void ShowUI()
    {
        TopToolbar.AddSetting(true, true, HideUI);
        InitData();
        InitBTN();
        GameHelper.UITransition_FadeIn(this.gameObject);
    }

    public void HideUI()
    {
        TopToolbar.RemoveLatestSetting();
        GameHelper.UITransition_FadeOut(this.gameObject);
    }

    private void InitData()
    {
        if (_cellList.Count > 0) return;

        LessonDB.Instance.GetAllLesson(out _lessonDict);
        int index = 0;
        foreach(KeyValuePair<string, List<string>> item in _lessonDict)
        {
            string lessonID = item.Key;
            List<string> idList = item.Value;
            string title = LocalizationManager.Instance.GetText("LESSON_DESCRIPTION_" + item.Key);
            CreateCell(index, title, delegate(int cellIndex)
            {
                Popup_Lesson.ShowUI(lessonID, idList, null);
            });
            index++;
        }
    }

    private void InitBTN()
    {
        BTN_Close?.onClick.RemoveAllListeners();
        BTN_Close?.onClick.AddListener(HideUI);
    }
    #endregion

    #region Cell Generation
    public void CreateCell(int index, string title, UnityAction<int> onClick)
    {
        GameObject obj = Instantiate(Prefab_LessonCell, GROUP_content);
        LessonCell cell = obj.GetComponent<LessonCell>();
        cell.Setup(index, title, onClick);

        _cellList.Add(cell);

    }
    #endregion



}
