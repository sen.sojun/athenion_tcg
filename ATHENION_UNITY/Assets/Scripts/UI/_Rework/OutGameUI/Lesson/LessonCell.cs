﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;

public class LessonCell : MonoBehaviour
{
    #region Inspectors
    [SerializeField] private TextMeshProUGUI _TEXT_Title;
    [SerializeField] private TextMeshProUGUI _TEXT_LessonIndex;
    [SerializeField] private Button _BTN_Learn;
    #endregion

    private int _index;
    private UnityAction<int> _onClick;

    public void Setup(int index, string title, UnityAction<int> onClick)
    {
        _index = index;
        _onClick = onClick;

        // Set Text
        _TEXT_LessonIndex.text = (index + 1).ToString();
        _TEXT_Title.text = title.ToString();

        // Set OnClick
        _BTN_Learn?.onClick.RemoveAllListeners();
        _BTN_Learn?.onClick.AddListener(OnClick);
    }

    private void OnClick()
    {
        _onClick.Invoke(_index);
    }

}
