﻿using System;
using System.Collections;
using System.Collections.Generic;
using GameEvent;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;
using Karamucho;

public class HomeUIManager : MonoBehaviour
{
    [Header("Canvas")]
    public Canvas Canvas_Main;
    public Canvas Canvas_Popup;

    [Header("BTN")]
    public Button BTN_Inbox;
    public Button BTN_Friend;
    public Button BTN_DailyLogin;
    public Button BTN_Option;
    public Button BTN_MainEvent;
    public Button BTN_Play;
    public Button BTN_Mode;
    public Button BTN_SubMenu;
    public Button BTN_TestAdventure;
    public Button BTN_EditProfile;
    public Button BTN_Mission;
    public Button BTN_SeasonPass;
    public Button BTN_LinkAccount;
    public Button BTN_Term;
    public Button BTN_ContactUs;
    public Button BTN_Logout;
    public PlayerDiamondTopBarUI DiamondTopBar;

    [Header("Social BTN")]
    public Button BTN_FacebookTH;
    public Button BTN_FacebookEN;
    public Button BTN_KTPlay;
    public Button BTN_Twitter;
    public Button BTN_YouTube;
    public Button BTN_Reddit;

    [Header("SubMenu BTN")]
    public Button BTN_SubMenu_Accomplishment;
    public Button BTN_SubMenu_Social;
    public Button BTN_SubMenu_Tutorial;
    public Button BTN_SubMenu_Leaderboard;
    public Button BTN_SubMenu_Cosmetic;
    public Button BTN_SubMenu_MainEvent;
    public Button BTN_SubMenu_DailyLogin;
    public Button BTN_SubMenu_Lesson;

    [Header("Popup")]
    public SpecialLoginController PopupSpecialLogin;
    public PopupDailyLoginReward Popup_DailyLoginReward;
    public UIPopupRankingUpdate Popup_RankingUpdate;
    public FriendController Popup_Friends;
    public UIPopupCardChange Popup_CardChange;
    public InboxController Popup_Inbox;
    public TutorialPopup TutorialPanel;
    public OptionController OptionPanel;
    public DeckSelectUI DeckSelectUI;
    public PopupPlayModeUI PopupPlayModeUI;
    public PopupSubMenuUI PopupSubMenu;
    public UIAdventurePopup UIAdventurePopup;
    public ProfileEditorManager EditProfilePanel;
    public AccomplishmentPanelManager AccomplishmentPanel;
    public SeasonPassUIManager SeasonPassPanel;
    public MissionListUIController MissionPanel;
    public CollectionCosmetic Popup_CosmeticPanel;
    public LessonPopupUIController Popup_Lesson;
    public LinkAccountController Popup_LinkAccount;
    public PopupSocial Popup_Social; 

    [Header("WildOffer")]
    public WildOfferPanelUI WildOfferPanel;

    [Header("Player Profile")]
    /*
    public TextMeshProUGUI TXT_PlayerName;
    public TextMeshProUGUI TXT_PlayerLevelText;
    public Image IMG_PlayerAvatar;
    public Image IMG_PlayerTitle;
    public Image IMG_PlayerRankBorder;
    */
    public PlayerProfileTopBarUI TopBarProfile;

    [Header("Mission")]
    public MissionButtonShortcut MissionBTN;

    #region Private Properties
    private bool _isFirstInitialized = false;
    #endregion

    #region SetButtonEvent
    public void SetBtnFacebookEN(UnityAction onClick)
    {
        if (BTN_FacebookEN != null)
        {
            BTN_FacebookEN.onClick.RemoveAllListeners();
            BTN_FacebookEN.onClick.AddListener(onClick);
        }
        else
        {
            Debug.LogErrorFormat("SetBtnFacebookEN: BTN_FacebookEN is null.");
        }
    }

    public void SetBtnFacebookTH(UnityAction onClick)
    {
        if (BTN_FacebookTH != null)
        {
            BTN_FacebookTH.onClick.RemoveAllListeners();
            BTN_FacebookTH.onClick.AddListener(onClick);
        }
        else
        {
            Debug.LogErrorFormat("SetBtnFacebookTH: BTN_FacebookTH is null.");
        }
    }

    public void SetBtnKtplay(UnityAction onClick)
    {
        if (BTN_KTPlay != null)
        {
            BTN_KTPlay.onClick.RemoveAllListeners();
            BTN_KTPlay.onClick.AddListener(onClick);
        }
        else
        {
            Debug.LogErrorFormat("SetBtnKtplay: BTN_KTPlay is null.");
        }
    }

    public void SetBtnTwitter(UnityAction onClick)
    {
        if (BTN_Twitter != null)
        {
            BTN_Twitter.onClick.RemoveAllListeners();
            BTN_Twitter.onClick.AddListener(onClick);
        }
        else
        {
            Debug.LogErrorFormat("SetBtnTwitter: BTN_Twitter is null.");
        }
    }

    public void SetBtnYouTube(UnityAction onClick)
    {
        if (BTN_YouTube != null)
        {
            BTN_YouTube.onClick.RemoveAllListeners();
            BTN_YouTube.onClick.AddListener(onClick);
        }
        else
        {
            Debug.LogErrorFormat("SetBtnYoutube: BTN_Youtube is null.");
        }
    }

    public void SetBtnReddit(UnityAction onClick)
    {
        if (BTN_Reddit != null)
        {
            BTN_Reddit.onClick.RemoveAllListeners();
            BTN_Reddit.onClick.AddListener(onClick);
        }
        else
        {
            Debug.LogErrorFormat("SetBtnReddit: BTN_Reddit is null.");
        }
    }

    public void SetBtnContactUs(UnityAction onClick)
    {
        if (BTN_ContactUs != null)
        {
            BTN_ContactUs.onClick.RemoveAllListeners();
            BTN_ContactUs.onClick.AddListener(onClick);
        }
        else
        {
            Debug.LogErrorFormat("SetBtnInBoxEvent: BTN_ContactUs is null.");
        }
    }

    public void SetBtnTerm(UnityAction onClick)
    {
        if (BTN_Term != null)
        {
            BTN_Term.onClick.RemoveAllListeners();
            BTN_Term.onClick.AddListener(onClick);
        }
        else
        {
            Debug.LogErrorFormat("SetBtnInBoxEvent: BTN_Term is null.");
        }
    }

    public void SetBtnLinkAccountEvent(UnityAction onClick)
    {
        if (BTN_LinkAccount != null)
        {
            BTN_LinkAccount.onClick.RemoveAllListeners();
            BTN_LinkAccount.onClick.AddListener(onClick);
        }
        else
        {
            Debug.LogErrorFormat("SetBtnInBoxEvent: BTN_LinkAccount is null.");
        }
    }

    public void SetBtnInBoxEvent(UnityAction onClick)
    {
        if (BTN_Inbox != null)
        {
            BTN_Inbox.onClick.RemoveAllListeners();
            BTN_Inbox.onClick.AddListener(onClick);
        }
        else
        {
            Debug.LogErrorFormat("SetBtnInBoxEvent: BTN_Inbox is null.");
        }
    }

    public void SetBtnFriendsEvent(UnityAction onClick)
    {
        if (BTN_Friend != null)
        {
            BTN_Friend.onClick.RemoveAllListeners();
            BTN_Friend.onClick.AddListener(onClick);
        }
        else
        {
            Debug.LogErrorFormat("SetBtnFriendsEvent: BTN_Friend is null.");
        }
    }

    public void SetBtnLessonEvent(UnityAction onClick)
    {
        if (BTN_SubMenu_Lesson)
        {
            BTN_SubMenu_Lesson.onClick.RemoveAllListeners();
            BTN_SubMenu_Lesson.onClick.AddListener(onClick);
        }
        else
        {
            Debug.LogErrorFormat("SetBtnFriendsEvent: BTN_Friend is null.");
        }
    }

    public void SetBtnDailyLoginEvent(UnityAction onClick)
    {
        if (BTN_DailyLogin != null)
        {
            BTN_DailyLogin.onClick.RemoveAllListeners();
            BTN_DailyLogin.onClick.AddListener(onClick);
        }
        else
        {
            Debug.LogErrorFormat("SetBtnDailyLoginEvent: BTN_DailyLogin is null.");
        }

        if (BTN_SubMenu_DailyLogin != null)
        {
            BTN_SubMenu_DailyLogin.onClick.RemoveAllListeners();
            BTN_SubMenu_DailyLogin.onClick.AddListener(onClick);
        }
        else
        {
            Debug.LogErrorFormat("SetBtnDailyLoginEvent: BTN_SubMenu_DailyLogin is null.");
        }
    }

    public void SetBtnOptionEvent(UnityAction onClick)
    {
        if (BTN_Option != null)
        {
            BTN_Option.onClick.RemoveAllListeners();
            BTN_Option.onClick.AddListener(onClick);
        }
        else
        {
            Debug.LogErrorFormat("SetBtnOptionEvent: BTN_Option is null.");
        }
    }

    public void SetShowTutorialCallback(UnityAction onClick)
    {
        if (BTN_SubMenu_Tutorial != null)
        {
            BTN_SubMenu_Tutorial.onClick.RemoveAllListeners();
            BTN_SubMenu_Tutorial.onClick.AddListener(onClick);
        }
        else
        {
            Debug.LogErrorFormat("SetBtnTutorialEvent: BTN_Tutorial is null.");
        }
    }

    public WildOfferPanelUI GetWildOfferPanel()
    {
        return WildOfferPanel;
    }

    public void SetBtnPlayMode(UnityAction onClick)
    {
        if (BTN_Play != null)
        {
            BTN_Play.onClick.RemoveAllListeners();
            BTN_Play.onClick.AddListener(onClick);
        }
        else
        {
            Debug.LogErrorFormat("SetBtnPlayMode: BTN_Play is null.");
        }
    }

    public void SetBtnSelectMode(UnityAction onClick)
    {
        if (BTN_Mode != null)
        {
            BTN_Mode.onClick.RemoveAllListeners();
            BTN_Mode.onClick.AddListener(onClick);
        }
        else
        {
            Debug.LogErrorFormat("SetBtnSelectMode: BTN_Mode is null.");
        }
    }

    internal void SetBtnSubMenu(UnityAction onClick)
    {
        if (BTN_SubMenu != null)
        {
            BTN_SubMenu.onClick.RemoveAllListeners();
            BTN_SubMenu.onClick.AddListener(onClick);
        }
        else
        {
            Debug.LogErrorFormat("SetBtnSubMenu: BTN_SubMenu is null.");
        }
    }

    public void SetBtnTestAdventureMode(UnityAction onClick)
    {
        if (BTN_TestAdventure != null)
        {
            BTN_TestAdventure.onClick.RemoveAllListeners();
            BTN_TestAdventure.onClick.AddListener(onClick);
        }
        else
        {
            Debug.LogErrorFormat("SetBtnTestAdventureMode: BTN_TestAdventure is null.");
        }
    }

    public void SetBtnProfileEvent(UnityAction onClick)
    {
        if (BTN_EditProfile != null)
        {
            BTN_EditProfile.onClick.RemoveAllListeners();
            BTN_EditProfile.onClick.AddListener(onClick);
        }
        else
        {
            Debug.LogErrorFormat("SetBtnProfileEvent: BTN_EditProfile is null.");
        }
    }

    public void SetAccomplishmentEvent(UnityAction onClick)
    {
        if (BTN_SubMenu_Accomplishment != null)
        {
            BTN_SubMenu_Accomplishment.onClick.RemoveAllListeners();
            BTN_SubMenu_Accomplishment.onClick.AddListener(onClick);
        }
        else
        {
            Debug.LogErrorFormat("SetAccomplishmentEvent: BTN_Accomplishment is null.");
        }
    }

    public void SetShowSocialCallback(UnityAction onClick)
    {
        if (BTN_SubMenu_Social != null)
        {
            BTN_SubMenu_Social.onClick.RemoveAllListeners();
            BTN_SubMenu_Social.onClick.AddListener(onClick);
        }
        else
        {
            Debug.LogErrorFormat("SetSocialEvent: BTN_Social is null.");
        }
    }

    public void SetTutorialEvent(UnityAction onClick)
    {
        if (BTN_SubMenu_Tutorial != null)
        {
            BTN_SubMenu_Tutorial.onClick.RemoveAllListeners();
            BTN_SubMenu_Tutorial.onClick.AddListener(onClick);
        }
        else
        {
            Debug.LogErrorFormat("SetTutorialEvent: BTN_Tutorial is null.");
        }
    }

    public void SetShowLeaderboardCallback(UnityAction onClick)
    {
        if (BTN_SubMenu_Leaderboard != null)
        {
            BTN_SubMenu_Leaderboard.onClick.RemoveAllListeners();
            BTN_SubMenu_Leaderboard.onClick.AddListener(onClick);
        }
        else
        {
            Debug.LogErrorFormat("SetLeaderboardEvent: BTN_Leaderboard is null.");
        }
    }

    public void SetShowCosmeticCallback(UnityAction onClick)
    {
        if (BTN_SubMenu_Cosmetic != null)
        {
            BTN_SubMenu_Cosmetic.onClick.RemoveAllListeners();
            BTN_SubMenu_Cosmetic.onClick.AddListener(onClick);
        }
        else
        {
            Debug.LogErrorFormat("SetCosmeticEvent: BTN_Cosmetic is null.");
        }
    }

    public void SetShowMainEventCallback(UnityAction onClick)
    {
        if (BTN_MainEvent != null)
        {
            BTN_MainEvent.onClick.RemoveAllListeners();
            BTN_MainEvent.onClick.AddListener(onClick);
        }
        else
        {
            Debug.LogErrorFormat("SetMainEventCallback: BTN_MainEvent is null.");
        }

        if (BTN_SubMenu_MainEvent != null)
        {
            BTN_SubMenu_MainEvent.onClick.RemoveAllListeners();
            BTN_SubMenu_MainEvent.onClick.AddListener(
                delegate ()
                {
                    PopupSubMenu.HideUI();
                    onClick?.Invoke();
                }
            );
        }
        else
        {
            Debug.LogErrorFormat("SetMainEventCallback: BTN_SubMenu_MainEvent is null.");
        }
    }

    public void SetSeasonPassEvent(UnityAction onClick)
    {
        if (BTN_SeasonPass != null)
        {
            BTN_SeasonPass.onClick.RemoveAllListeners();
            BTN_SeasonPass.onClick.AddListener(onClick);
        }
        else
        {
            Debug.LogErrorFormat("SetSeasonPassEvent: BTN_SeasonPass is null.");
        }
    }

    public void SetLogoutEvent(UnityAction onClick)
    {
        if (BTN_Logout != null)
        {
            BTN_Logout.onClick.RemoveAllListeners();
            BTN_Logout.onClick.AddListener(onClick);
        }
        else
        {
            Debug.LogErrorFormat("SetBtnProfileEvent: BTN_Logout is null.");
        }
    }

    public void SetMissionEvent(UnityAction onClick)
    {
        if (BTN_Mission != null)
        {
            BTN_Mission.onClick.RemoveAllListeners();
            BTN_Mission.onClick.AddListener(onClick);
        }
        else
        {
            Debug.LogErrorFormat("SetBtnProfileEvent: BTN_Mission is null.");
        }
    }

    #endregion

    #region Methods
    private void Start()
    {
        InitHomeUI();

        _isFirstInitialized = true;
    }

    private void Update()
    {
        //OnEscapeKey();
    }

    private void OnEscapeKey()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (DataManager.Instance.PlayerInfo.ActiveStatus == PlayerActiveStatus.Online)
            {
                string title = LocalizationManager.Instance.GetText("QUIT_TITLE");
                string des = LocalizationManager.Instance.GetText("QUIT_DESCRIPTION");
                PopupUIManager.Instance.ShowPopup_SureCheck_Mary(title, des, delegate
                {
                    Application.Quit();
                }, 
                null);

            }
        }
    }

    private void InitHomeUI()
    {
        UpdatePlayerProfile();

        UpdateFeatureUnlock();

        DataManager.OnUpdateFeatureUnlock += UpdateFeatureUnlock;
        Popup_LinkAccount.OnCompleteLink += UpdatePlayerProfile;
    }

    private void OnEnable()
    {
        if (_isFirstInitialized)
        {
            UpdatePlayerProfile();
        }
    }

    private void OnDestroy()
    {
        DataManager.OnUpdateFeatureUnlock -= UpdateFeatureUnlock;
    }

    private void UpdateFeatureUnlock()
    {
        BTN_SeasonPass.interactable = DataManager.Instance.IsFeatureEnable(FeatureUnlock.SeasonPass);

        BTN_SubMenu_Accomplishment.interactable = DataManager.Instance.IsFeatureEnable(FeatureUnlock.Accomplishment);
        BTN_SubMenu_MainEvent.interactable = DataManager.Instance.IsFeatureEnable(FeatureUnlock.Event);
        BTN_MainEvent.interactable = DataManager.Instance.IsFeatureEnable(FeatureUnlock.Event);
        BTN_MainEvent.interactable = DataManager.Instance.IsFeatureEnable(FeatureUnlock.Event);

        BTN_KTPlay.interactable = DataManager.Instance.IsFeatureEnable(FeatureUnlock.Community);
    }

    public void ShowSoftLoad(bool isShow)
    {
        PopupUIManager.Instance.Show_SoftLoad(isShow);
    }

    public void ShowFullLoad(bool isShow)
    {
        PopupUIManager.Instance.Show_FullLoad(isShow);
    }

    public void SetLoadingProgress(float ratio)
    {
        PopupUIManager.Instance.SetLoadingProgress(ratio);
    }

    public void HideAllLoading()
    {
        PopupUIManager.Instance.HideAllLoad();
    }

    public void ShowHomePopupCanvas()
    {
        if (Canvas_Popup != null)
        {
            Canvas_Popup.gameObject.SetActive(true);
        }
    }

    public void HideHomePopupCanvas()
    {
        PopupSubMenu.HideUI();
        if (Canvas_Popup != null)
        {
            Canvas_Popup.gameObject.SetActive(false);
        }
    }

    public void ShowMainCanvas()
    {
        if (Canvas_Main != null)
        {
            Canvas_Main.gameObject.SetActive(true);
        }
    }

    public void HideMainCanvas()
    {
        if (Canvas_Main != null)
        {
            Canvas_Main.gameObject.SetActive(false);
        }
    }

    public void ShowTutorialPanel()
    {
        OptionPanel.HideUI();
        TutorialPanel.ShowUI();
    }

    public void ShowProfileEditor(ProfileEditorManager.TabType tab)
    {
        EditProfilePanel.SetStarterTab(tab);
        GameHelper.UITransition_FadeIn(EditProfilePanel.gameObject, delegate { HomeManager.Instance.HideMainCanvas(); });
        EditProfilePanel.SetOnCloseEvent(delegate { UpdatePlayerProfile(); HomeManager.Instance.ShowMainCanvas(); });
    }

    public void ShowAccomplishmentPanel(AccomplishmentPanelManager.TabType tab)
    {
        AccomplishmentPanel.SetStarterTab(tab);
        GameHelper.UITransition_FadeIn(AccomplishmentPanel.gameObject, delegate { HomeManager.Instance.HideMainCanvas(); });
        //EditProfilePanel.SetOnCloseEvent(UpdatePlayerProfile);
    }

    public void ShowSocialPanel()
    {
        Popup_Social.Show();
    }

    public void HideSocialPanel()
    {
        Popup_Social.Hide();
    }

    public void ShowSeasonPassPanel(SeasonPassUIManager.TabType tab)
    {
        SeasonPassPanel.SetStarterTab(tab);
        GameHelper.UITransition_FadeIn(SeasonPassPanel.gameObject, delegate { HomeManager.Instance.HideMainCanvas(); });
        //EditProfilePanel.SetOnCloseEvent(UpdatePlayerProfile);
    }

    public void ShowEventPanel(EventPageType tab)
    {
        EventManager eventManager = GameObject.Instantiate(ResourceManager.Load<GameObject>("Events/Prefabs/Popup_EventPanel"), Canvas_Popup.transform).GetComponent<EventManager>();
        eventManager.Initialize();
        eventManager.ShowPage(EventPageType.GameEvent);
        //Popup_Event_Prefab.SetStarterTab(tab);
        //GameHelper.UITransition_FadeIn(Popup_Event.gameObject);
    }

    public void ShowCosmeticPanel()
    {
        Popup_CosmeticPanel.Show(HomeManager.Instance.HideMainCanvas);
    }

    public void HideCosmeticPanel()
    {
        HomeManager.Instance.ShowMainCanvas();
        Popup_CosmeticPanel.Hide();
    }

    public void ShowAdventurePanel()
    {
        OptionPanel.HideUI();
        UIAdventurePopup.ShowUI();
    }

    public void ShowMissionPanel()
    {
        MissionPanel.ShowUI();
    }

    public void HideMissionPanel()
    {
        MissionPanel.HideUI();
    }

    public void ShowSelectDeck(GameMode mode, UnityAction<GameMode> onPlay, UnityAction<int> onDeckSelectComplete, List<string> elementFilterList = null)
    {
        DeckSelectUI.SetEvent(onPlay, onDeckSelectComplete);
        DeckSelectUI.ShowUI(mode, elementFilterList);
        HideMainCanvas();
        //OnSelectMode?.Invoke(gameMode);
    }

    public void HideSelectDeck()
    {
        ShowMainCanvas();
        DeckSelectUI.HideUI();
    }

    public void UpdatePlayerProfile()
    {
        TopBarProfile.UpdateUI();
    }

    public void Debug_ShowCardList()
    {
        NavigatorController.Instance.GoToCardList();
    }

    public void Debug_ShowEffectAttack()
    {
        NavigatorController.Instance.GoToEffectAttack();
    }

    public void Debug_ShowSpawnCard()
    {
        NavigatorController.Instance.GoToSpawnCard();
    }

    #endregion

    #region Get Object
    public MissionButtonShortcut GetMissionBTN()
    {
        return MissionBTN;
    }
    #endregion
}
