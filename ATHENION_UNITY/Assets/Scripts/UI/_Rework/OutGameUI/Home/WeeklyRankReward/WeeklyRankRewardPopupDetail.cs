﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class WeeklyRankRewardPopupDetail : MonoBehaviour
{
    public WeeklyRankRewardUI WeeklyRankRewardUI;
    public Image CurrentChestSprite;
    public TMP_Text CountdownTextInPopup;
    public TMP_Text WeeklyRankProgressText;
    public TMP_Text WeeklyRankRewardRangeInfo;
    public GameObject[] ProgressBars;
    public GameObject CurrentFlare;
    public Transform RewardSection;
    public PromotionItemDetailUICell ItemDetailPrefab;
    public Button LeftArrow;
    public Button RightArrow;

    private WeeklyRankRewardData _weeklyRankRewardData;
    private bool _hasInitUI = false;
    private int _chestIndexForCurrentPlayerRank;
    private int _currentShowingChestIndex;

    private void OnEnable()
    {
        if (_hasInitUI)
        {
            UpdateUI(_chestIndexForCurrentPlayerRank);
            return;
        }

		InitUI();
        _hasInitUI = true;
    }

    private void InitUI()
    {
        _weeklyRankRewardData = DataManager.Instance.GetWeeklyRankRewardData();
        _chestIndexForCurrentPlayerRank = _weeklyRankRewardData.GetRewardListIndexByCurrentRank(DataManager.Instance.PlayerInfo.GetPlayerCurrentRank());
        _currentShowingChestIndex = _chestIndexForCurrentPlayerRank;
        UpdateUI(_currentShowingChestIndex);

        LeftArrow.onClick.AddListener(() => OnClickLeftArrow());
        RightArrow.onClick.AddListener(() => OnClickRightArrow());
    }

    private void UpdateUI(int chestIndex)
    {
        _currentShowingChestIndex = chestIndex;
        CurrentFlare.SetActive(chestIndex == _chestIndexForCurrentPlayerRank);
        string minRankName = GameHelper.GetRankName(_weeklyRankRewardData.RewardChestList[chestIndex].MinRankForClaimingReward);
        string maxRankName = GameHelper.GetRankName(_weeklyRankRewardData.RewardChestList[chestIndex].MaxRankForClaimingReward);
        WeeklyRankRewardRangeInfo.text = minRankName + " - " + maxRankName;

        //Arrows
        LeftArrow.gameObject.SetActive(chestIndex != 0);
        RightArrow.gameObject.SetActive(chestIndex != _weeklyRankRewardData.RewardChestList.Count - 1);

        //Chest sprite
        Sprite chestSprite;
        if (WeeklyRankRewardUI.IsRewardReady && chestIndex == _chestIndexForCurrentPlayerRank)
        {
            chestSprite = WeeklyRankRewardUI.OpenChestSprites[chestIndex];
        }
        else
        {
            chestSprite = WeeklyRankRewardUI.ChestSprites[chestIndex];
        }
        
        CurrentChestSprite.sprite = chestSprite;

        foreach (var oldRewardCell in RewardSection.GetComponentsInChildren<PromotionItemDetailUICell>())
        {
            Destroy(oldRewardCell.gameObject);
        }
        foreach (var rewardDetail in _weeklyRankRewardData.RewardChestList[chestIndex].RewardList)
        {
            PromotionItemDetailUICell newCell = Instantiate(ItemDetailPrefab, RewardSection);
            newCell.SetData(rewardDetail);
        }
    }

    private void OnClickLeftArrow()
    {
        if (_currentShowingChestIndex > 0)
        {
            _currentShowingChestIndex--;
            UpdateUI(_currentShowingChestIndex);
        }
    }

    private void OnClickRightArrow()
    {
        if (_currentShowingChestIndex < _weeklyRankRewardData.RewardChestList.Count - 1)
        {
            _currentShowingChestIndex++;
            UpdateUI(_currentShowingChestIndex);
        }
    }

}
