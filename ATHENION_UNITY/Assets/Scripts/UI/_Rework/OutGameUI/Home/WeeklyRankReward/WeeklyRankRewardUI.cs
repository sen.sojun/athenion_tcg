﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class WeeklyRankRewardUI : MonoBehaviour
{
    public bool IsRewardReady { get { return _rewardReady; } }
    public Sprite[] ChestSprites;
    public Sprite[] OpenChestSprites;
    public Image CurrentChestSprite;
    public Button ChestButton;
    public TMP_Text WeeklyRankProgressText;
    public TMP_Text CountdownText;
    public GameObject[] ProgressBars;
    public WeeklyRankRewardPopupDetail WeeklyRankRewardPopupDetail;
    public Button BTN_Chest;

    private DateTime _timeCounter;
    private Coroutine _countDownCoroutine;
    private WeeklyRankRewardData _weeklyRankRewardData;

    private bool _rewardReady = false;
    private Sprite _currentSprite;

    private void OnEnable()
    {
        _weeklyRankRewardData = DataManager.Instance.GetWeeklyRankRewardData();
        WeeklyRankRewardController.RefreshWeeklyRankRewardEvent += UpdateUI;
        UpdateUI();

        if (_countDownCoroutine != null)
        {
            StopCoroutine(_countDownCoroutine);
        }

        ChestButton.onClick.RemoveAllListeners();
        ChestButton.onClick.AddListener(() => ShowWeeklyRankRewardDetailPopup());
        _countDownCoroutine = StartCoroutine(Counting());

        InitBTN();

    }

    private void OnDisable()
    {
        WeeklyRankRewardController.RefreshWeeklyRankRewardEvent -= UpdateUI;

        if (_countDownCoroutine != null)
            StopCoroutine(_countDownCoroutine);
    }

    public void InitBTN()
    {
        BTN_Chest.onClick.RemoveAllListeners();
        BTN_Chest.onClick.AddListener(ShowWeeklyRankRewardDetailPopup);
    }

    public void ShowWeeklyRankRewardDetailPopup()
    {
        WeeklyRankRewardPopupDetail.gameObject.SetActive(true);
    }

    public Sprite GetChestSprite(int index)
    {
        return _currentSprite;
    }

    private void UpdateUI()
    {
        //Chest
        int chestIndex = _weeklyRankRewardData.GetRewardListIndexByCurrentRank(DataManager.Instance.PlayerInfo.GetPlayerCurrentRank());

        // Check Reward
        if (_weeklyRankRewardData.CurrentProgress 
            >= _weeklyRankRewardData.MaxProgress)
        {
            _rewardReady = true;
            _currentSprite = OpenChestSprites[chestIndex];
        }
        else
        {
            _rewardReady = false;
            _currentSprite = ChestSprites[chestIndex];
        }
        CurrentChestSprite.sprite = _currentSprite;
        WeeklyRankRewardPopupDetail.CurrentChestSprite.sprite = _currentSprite;

        //Text
        WeeklyRankProgressText.text = string.Format(LocalizationManager.Instance.GetText("TEXT_WEEKLY_RANK_REWARD_WINS_SHORT"), _weeklyRankRewardData.CurrentProgress, _weeklyRankRewardData.MaxProgress);
        WeeklyRankRewardPopupDetail.WeeklyRankProgressText.text = string.Format(LocalizationManager.Instance.GetText("TEXT_WEEKLY_RANK_REWARD_REWARD_WINS_LONG"), _weeklyRankRewardData.CurrentProgress, _weeklyRankRewardData.MaxProgress);

        //Bars
        for (int i=0; i < ProgressBars.Length; i++)
        {
            ProgressBars[i].SetActive(i < _weeklyRankRewardData.CurrentProgress);
        }
        for (int i = 0; i < WeeklyRankRewardPopupDetail.ProgressBars.Length; i++)
        {
            WeeklyRankRewardPopupDetail.ProgressBars[i].SetActive(i < _weeklyRankRewardData.CurrentProgress);
        }
    }

    private IEnumerator Counting()
    {
        while (true)
        {
            DateTime nextWeekDate = GameHelper.GetNextWeekDate(_weeklyRankRewardData.LastUpdateTimestamp);
            TimeSpan timeLeft = nextWeekDate - DateTimeData.GetDateTimeUTC();
            string text = GameHelper.GetCountdownText(timeLeft);
            CountdownText.text = text;
            WeeklyRankRewardPopupDetail.CountdownTextInPopup.text = string.Format("{0} {1}", LocalizationManager.Instance.GetText("TEXT_WEEKLY_RANK_REWARD_GET_REWARD_IN"), text);
            if (timeLeft.TotalSeconds < 0)
            {
                bool isFinishedRefreshWeeklyRankReward = false;
                WeeklyRankRewardController.RefreshWeeklyRankReward(() => isFinishedRefreshWeeklyRankReward = true);
                while (!isFinishedRefreshWeeklyRankReward)
                {
                    yield return null;
                }
            }

            yield return new WaitForSeconds(1f);
        }
    }

}
