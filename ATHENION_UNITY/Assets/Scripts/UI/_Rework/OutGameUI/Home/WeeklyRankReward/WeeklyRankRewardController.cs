﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public static class WeeklyRankRewardController
{
    public delegate void WeeklyRankRewardDel();
    public static event WeeklyRankRewardDel RefreshWeeklyRankRewardEvent;

    public static void RefreshWeeklyRankReward(UnityAction onComplete = null)
    {
        PopupUIManager.Instance.Show_MidSoftLoad(true);
        DataManager.Instance.RefreshWeeklyRankReward(
            (rewardClaimedList) =>
            {
                if (rewardClaimedList != null && rewardClaimedList.Count > 0)
                {
                    DataManager.Instance.LoadInventory(() =>
                    {
                        PopupUIManager.Instance.Show_MidSoftLoad(false);
                        //Show reward claimed from last week
                        if (rewardClaimedList != null && rewardClaimedList.Count > 0)
                        {
                            PopupUIManager.Instance.ShowPopup_GotReward(
                                LocalizationManager.Instance.GetText("POPUP_WEEKLY_RANK_REWARD_GET_REWARD_TITLE"),
                                LocalizationManager.Instance.GetText("POPUP_WEEKLY_RANK_REWARD_GET_REWARD_BODY"),
                                rewardClaimedList);
                        }
                        RefreshWeeklyRankRewardEvent?.Invoke();
                        onComplete?.Invoke();
                    },
                    null);
                }
                else
                {
                    PopupUIManager.Instance.Show_MidSoftLoad(false);
                    RefreshWeeklyRankRewardEvent?.Invoke();
                    onComplete?.Invoke();
                }
            },
            (errorMessage) =>
            {
                //Same flow as success case
                PopupUIManager.Instance.Show_MidSoftLoad(false);
                RefreshWeeklyRankRewardEvent?.Invoke();
                onComplete?.Invoke();
            }
        );
    }
}
