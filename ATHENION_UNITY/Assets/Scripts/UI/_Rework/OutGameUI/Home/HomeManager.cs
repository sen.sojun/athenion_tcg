﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using System;
using Newtonsoft.Json;
using Karamucho.Network;
using System.Linq;
using GameEvent;
#if UNITY_ANDROID
using GooglePlayGames;
#endif

public class HomeManager : OutGameUIBase
{
    #region Event 
    public static event Action<GameMode> OnSelectMode;

    public static event Action OnInit;
    public static event Action OnExecuteInturuptedTask;

    public static event Action OnFinishShowAllPopup;

    public static event Action OnShowCommunity;
    public static event Action OnClickDiamondBTN;
    #endregion

    #region Properties
    public override string SceneName { get { return GameHelper.HomeSceneName; } }

    public HomeUIManager HomeUIManager
    {
        get
        {
            if (_homeUIManager == null)
                _homeUIManager = GameObject.FindObjectOfType<HomeUIManager>();
            return _homeUIManager;
        }
    }
    private HomeUIManager _homeUIManager;

    public static HomeManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<HomeManager>();
                DontDestroyOnLoad(_instance.gameObject);
            }
            return _instance;
        }
    }
    private static HomeManager _instance;

    private List<IEnumerator> _interuptProcessList = null;
    private Coroutine _starterTask = null;

    public override bool IsCanSwitchScene => true;
    #endregion

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            DestroyImmediate(gameObject);
            return;
        }
        else if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
    }

    #region Override Methods
    public override void OnInitialize(UnityAction onComplete)
    {
        SetUpBTNCallback();
        onComplete?.Invoke();
        StartCoroutine(InitMainMenu());
    }

    public override void OnExit()
    {
        base.OnExit();
        if (TaskLoopInterval.Instance != null)
            TaskLoopInterval.Instance.StopAllTask();
    }
    #endregion

    #region Methods

    #region SetUp

    private void SetUpBTNCallback()
    {
        // PlayMode
        HomeUIManager.SetBtnSelectMode(delegate
        {
            HomeUIManager.PopupSubMenu.HideUI();
            HomeUIManager.PopupPlayModeUI.ShowUI();
        });
        SetupCallbackSelectGameModeEvent();

        //
        HomeUIManager.SetBtnInBoxEvent(HomeUIManager.Popup_Inbox.Show);
        HomeUIManager.SetBtnFriendsEvent(ShowFriends);
        HomeUIManager.SetBtnDailyLoginEvent(ShowDailyLoginReward);
        HomeUIManager.SetBtnOptionEvent(HomeUIManager.OptionPanel.ShowUI);
        HomeUIManager.SetBtnSubMenu(HomeUIManager.PopupSubMenu.ShowUI);
        HomeUIManager.SetBtnLinkAccountEvent(HomeUIManager.Popup_LinkAccount.Show);
        HomeUIManager.SetBtnContactUs(ClickContactUs);
        HomeUIManager.SetBtnTerm(ClickTermCondition);

        // Social
        HomeUIManager.SetBtnFacebookEN(ClickFaceboook_Global);
        HomeUIManager.SetBtnFacebookTH(ClickFaceboook_TH);
        HomeUIManager.SetBtnKtplay(ShowCommunity);
        HomeUIManager.SetBtnTwitter(ClickTwitter);
        HomeUIManager.SetBtnYouTube(ClickYouTube);
        HomeUIManager.SetBtnReddit(ClickReddit);

        //Mission
        HomeUIManager.SetMissionEvent(ShowMainMission);

        //Adventure
        HomeUIManager.SetBtnTestAdventureMode(delegate
        {
            HomeUIManager.PopupSubMenu.HideUI();
            ShowMainAdventure();
        });

        //Play
        HomeUIManager.SetBtnPlayMode(delegate
        {
            HomeUIManager.PopupSubMenu.HideUI();
            ClickPlayBtn();
        });

        //Profile
        HomeUIManager.SetBtnProfileEvent(ShowProfile);

        //Option
        HomeUIManager.SetLogoutEvent(
            delegate ()
            {
                PopupUIManager.Instance.ShowPopup_SureCheck(
                    LocalizationManager.Instance.GetText("TEXT_LOGOUT_HEAD")
                    , LocalizationManager.Instance.GetText("TEXT_LOGOUT_DESCRIPTION")
                    , delegate ()
                    {
                        NavigatorController.Instance.Logout();
                    }
                    , null
                );
            }
        );

        //WildOfferUI
        InitialWildOfferPanelUI();

        //Season Pass
        HomeUIManager.SetSeasonPassEvent(ShowSeasonPass);

        // =================== Popup Sub Menu ====================

        // Accomplishment
        HomeUIManager.SetAccomplishmentEvent(ShowAccomplishment);

        // Social
        HomeUIManager.SetShowSocialCallback(ShowSocial);

        // Tutorial
        HomeUIManager.SetShowTutorialCallback(HomeUIManager.ShowTutorialPanel);
        HomeUIManager.TutorialPanel.SetOnSkipEvent(() =>
        {
            Debug.Log("Skip Tutorial");
            DataManager.Instance.SetSkipTutorial(true);
            HomeUIManager.TutorialPanel.HideUI();
            if (DataManager.Instance.LastGameMode.ToString().Contains("Tutorial"))
            {
                DataManager.Instance.LastGameMode = GameMode.None;
            }
            ShowStarterPopup();
        });
        HomeUIManager.TutorialPanel.SetOnCloseEvent(() =>
        {
            Debug.Log("Close Tutorial");
            DataManager.Instance.SetSkipTutorial(true);
            HomeUIManager.TutorialPanel.HideUI();
            if (DataManager.Instance.LastGameMode.ToString().Contains("Tutorial"))
            {
                DataManager.Instance.LastGameMode = GameMode.None;
            }
            ShowStarterPopup();
        });
        HomeUIManager.TutorialPanel.SetOnSelectTutorial(StartTutorialGame);

        // Leaderboard
        HomeUIManager.SetShowLeaderboardCallback(ShowLeaderboard);

        // Cosmetic
        HomeUIManager.SetShowCosmeticCallback(ShowCosmetic);

        // Main Event
        HomeUIManager.SetShowMainEventCallback(() => ShowMainEvent(EventPageType.MiniGameEvent));

        // Lesson
        HomeUIManager.SetBtnLessonEvent(ShowLesson);
    }

    private void InitialWildOfferPanelUI()
    {
        WildOfferPanelUI wildOfferPanel = HomeUIManager.GetWildOfferPanel();
        wildOfferPanel.Initialize();
    }

    private void SetupCallbackSelectGameModeEvent()
    {
        UnityAction<GameMode> onClickPlay = delegate (GameMode mode)
        {
            PopupUIManager.Instance.Show_SoftLoad(true);
            HomeUIManager.HideSelectDeck();

            switch (mode)
            {
                case GameMode.Casual: StartCasualMatch(); break;
                case GameMode.Rank: StartRankMatch(); break;
                case GameMode.Practice: StartPracticeMatch(); break;

                default:
                    {
                        PopupUIManager.Instance.Show_SoftLoad(false);
                    }
                    break;
            }
        };
        UnityAction<int> onSelectDeck = delegate (int deckIndex)
        {
            List<DeckData> deckList = DataManager.Instance.GetAllDeck();
            DataManager.Instance.SetCurrentDeckID(deckList[deckIndex].DeckID);

            Debug.Log("OnSelectedDeck " + deckList[deckIndex].DeckName + " " + deckList[deckIndex].DeckID);
        };

        HomeUIManager.PopupPlayModeUI.SetCasualBtnEvent(() =>
        {
            HomeUIManager.PopupPlayModeUI.HideUI();
            HomeUIManager.ShowSelectDeck(GameMode.Casual, onClickPlay, onSelectDeck);
        });
        HomeUIManager.PopupPlayModeUI.SetRankBtnEvent(() =>
        {
            HomeUIManager.PopupPlayModeUI.HideUI();
            HomeUIManager.ShowSelectDeck(GameMode.Rank, onClickPlay, onSelectDeck);
        });
        HomeUIManager.PopupPlayModeUI.SetPracticeBtnEvent(() =>
        {
            HomeUIManager.PopupPlayModeUI.HideUI();
            StartPracticeMatch();
        });
        HomeUIManager.PopupPlayModeUI.SetEventBattleBtnEvent(() =>
        {
            HomeUIManager.PopupPlayModeUI.HideUI();
            ShowEventBattle();
        });
        HomeUIManager.PopupPlayModeUI.SetFriendBtnEvent(() =>
        {
            HomeUIManager.PopupPlayModeUI.HideUI();
            ShowFriends();
        });
    }

    public IEnumerator InitMainMenu()
    {
        HomeUIManager.SetLoadingProgress(1.0f); // บุ๋นขอ
                                                //HomeUIManager.ShowSoftLoad(true);
        SoundManager.PlayBGM(SoundManager.BGM.Menu, 3.0f);

        HomeUIManager.ShowSoftLoad(false);

        OnInit?.Invoke();

        bool isFirstTime = DataManager.Instance.IsCompletedTutorial(GameMode.Tutorial_1) == false;
        if (isFirstTime)
        {
            //HomeUIManager.TutorialPanel.ShowUI();
            StartTutorialGame(GameMode.Tutorial_1);
            yield break;
        }
        else if (DataManager.Instance.IsCompletedTutorial(GameMode.Tutorial_2) == false)
        {
            HomeUIManager.ShowFullLoad(false);
            HomeUIManager.ShowSoftLoad(false);
            HomeUIManager.ShowTutorialPanel();

            yield break;
        }
        else if (DataManager.Instance.LastGameMode.ToString().Contains("Tutorial"))
        {
            HomeUIManager.ShowFullLoad(false);
            HomeUIManager.ShowSoftLoad(false);
            HomeUIManager.ShowTutorialPanel();

            yield break;
        }
        else if (
               !DataManager.Instance.IsPlayedTutorial(GameMode.Tutorial_3)
            || !DataManager.Instance.IsPlayedTutorial(GameMode.Tutorial_4)
        )
        {
            Debug.Log("IsSkipTutorial : " + DataManager.Instance.IsSkipTutorial());
            HomeUIManager.ShowFullLoad(false);
            if (!DataManager.Instance.IsSkipTutorial())
            {
                HomeUIManager.ShowTutorialPanel();
                HomeUIManager.ShowSoftLoad(false);

                yield break;
            }
        }

        yield return new WaitForEndOfFrame();
        HomeUIManager.ShowFullLoad(false); // Disable full load from login scene.
                                           // Tutorial 

        //yield return OnShowStarterPopup();
        ShowStarterPopup();
    }

    public void StartTutorialGame(GameMode mode)
    {
        SoundManager.PlaySFX("TUT_SFX_WAR");

        switch (mode)
        {
            case GameMode.Tutorial_1:
            case GameMode.Tutorial_2:
            case GameMode.Tutorial_3:
            case GameMode.Tutorial_4:
                break;

            default: return;
        }

        DataManager.Instance.CheckIsValidUserAndVersion(
            delegate ()
            {
                List<MatchPlayerData> matchPlayerDataList = new List<MatchPlayerData>();

                // Local Player
                {
                    MatchPlayerData localMatchPlayerData = MatchPlayerData.CreateTutorialMatchPlayerData(mode);
                    matchPlayerDataList.Add(localMatchPlayerData);
                }

                // Bot
                {
                    MatchPlayerData matchBotData = MatchPlayerData.CreateTutorialMatchBotData(mode);
                    matchPlayerDataList.Add(matchBotData);
                }

                MatchProperty matchProperty = new MatchProperty(
                      gameMode: mode
                    , playerCount: 2
                    , isNetwork: false
                    , isHaveTimer: false
                    , isVSBot: true
                    , botType: BotType.TutorialBot
                    , botLevel: BotLevel.None
                    , adventureStageData: null
                );

                MatchDataContainer container;
                MatchData.CreateMatchContainer(matchProperty, matchPlayerDataList, out container);
                DontDestroyOnLoad(container.gameObject);

                //HomeUIManager.ShowSoftLoad(true);
                NavigatorController.Instance.GotoBattleScene();
            }
            , NavigatorController.Instance.OnInvalidUserAndVersion
        );
    }

    public void StartAdventure(AdventureChapterID chapter, AdventureStageID stage)
    {
        PopupUIManager.Instance.Show_SoftLoad(true);

        DataManager.Instance.CheckIsValidUserAndVersion(
            delegate ()
            {
                AdventureStageData stageData = DataManager.Instance.GetAdventureDataDict()[chapter].Find(x => x.AdventureStageID == stage);

                MatchProperty matchProperty = new MatchProperty(
                      gameMode: GameMode.Adventure
                    , playerCount: 2
                    , isNetwork: false
                    , isHaveTimer: false
                    , isVSBot: true
                    , botType: BotType.AdventureBot
                    , botLevel: stageData.GetOpponentBotLevel()
                    , adventureStageData: stageData
                );

                MatchPlayerData.CreateAdventureMatchData(stageData, out List<MatchPlayerData> matchPlayerDataList);
                MatchDataContainer container;
                MatchData.CreateMatchContainer(matchProperty, matchPlayerDataList, out container);
                DontDestroyOnLoad(container.gameObject);

                NavigatorController.Instance.GotoBattleScene();
                PopupUIManager.Instance.Show_SoftLoad(false);
            }
            , NavigatorController.Instance.OnInvalidUserAndVersion
        );
    }

    private void ShowStarterPopup()
    {
        if (_starterTask != null)
        {
            StopCoroutine(_starterTask);
            _starterTask = null;
        }

        OnExecuteInturuptedTask?.Invoke();
        if (_interuptProcessList != null && _interuptProcessList.Count > 0)
        {
            _starterTask = StartCoroutine(OnInteruptProcess(delegate ()
            {
                _starterTask = StartCoroutine(OnShowStarterPopup());
            }));
        }
        else
        {
            _starterTask = StartCoroutine(OnShowStarterPopup());
        }
    }

    private IEnumerator OnInteruptProcess(UnityAction onComplete)
    {
        while (_interuptProcessList != null && _interuptProcessList.Count > 0)
        {
            yield return _interuptProcessList[0];
            _interuptProcessList[0] = null;
            _interuptProcessList.RemoveAt(0);
        }

        _starterTask = null;

        onComplete?.Invoke();
        yield break;
    }

    private IEnumerator OnShowStarterPopup()
    {
        TaskLoopInterval.Instance.AddTask("RefreshInboxListInterval", 50.0f, HomeUIManager.Popup_Inbox.RefreshInboxListInterval);

        TaskLoopInterval.Instance.AddTask("RefreshFriendListInterval", 10.0f, HomeUIManager.Popup_Friends.RefreshFriendListInterval);
        TaskLoopInterval.Instance.AddTask("RefreshFriendMatchInterval", 8.0f, HomeUIManager.Popup_Friends.RefreshFriendMatchInterval);

        //yield return ShowTutorialReward();
        yield return ShowPopupRankingUpdate();
        yield return ShowSpecialLogin();
        yield return ShowDailyLogin();

        yield return ShowCardChangeLog();
        yield return ShowWildOffers();

        Debug.Log("Show Popup Finish");

        // Connect to chat server
        //ChatManager.Instance.Connect(PhotonChatRegion.Singapore);

        OnFinishShowAllPopup?.Invoke();
        _starterTask = null;
    }

    public void SetInitInteruptProcess(IEnumerator process)
    {
        if (_interuptProcessList == null)
        {
            _interuptProcessList = new List<IEnumerator>();
        }

        _interuptProcessList.Add(process);
    }

    public void OnClickDiamond()
    {
        OnClickDiamondBTN?.Invoke();
    }

    #endregion

    #region CardChangeLog

    private Coroutine showChangeLogRoutine;
    public void StartShowCardChangeLog()
    {
        if (showChangeLogRoutine != null) return;
        showChangeLogRoutine = StartCoroutine(ShowCardChangeLog(true, () => showChangeLogRoutine = null));
    }

    private IEnumerator ShowCardChangeLog(bool forceShow = false, Action onComplete = null)
    {
        bool isFinish = false;
        CardChangeLogData log = DataManager.Instance.GetCardChangeLogData();
        CardChangeLogDetail logDetail = log.GetCurrentChangeLogDetail();
        if (log == null || logDetail == null)
            yield break;

        // Update Player Save ----------------------------------------------------------
        if (forceShow == false)
        {
            bool canShow = CanShowChangeLogAndUpdateStatus(logDetail);
            if (canShow == false)
            {
                onComplete?.Invoke();
                yield break;
            }
        }
        // -----------------------------------------------------------------------------

        List<string> cardChangeIDList = log.GetCurrentChangeLogCardList();
        List<CardUIData> cardCurrentList = new List<CardUIData>();
        List<CardUIData> cardOldList = new List<CardUIData>();

        // No Data
        if (cardChangeIDList.Count == 0)
        {
            onComplete?.Invoke();
            yield break;
        }

        foreach (string id in cardChangeIDList)
        {
            CardData dataCurrent = CardData.CreateCard(id);
            CardData dataOld = CardData.CreateCardOld(id);

            if (dataCurrent != null && dataOld != null)
            {
                CardUIData currentUIdata = new CardUIData(-1, dataCurrent, DataManager.Instance.DefaultCardBackIDList[0]);
                CardUIData oldUIdata = new CardUIData(-1, dataOld, DataManager.Instance.DefaultCardBackIDList[0]);
                cardCurrentList.Add(currentUIdata);
                cardOldList.Add(oldUIdata);
            }
        }

        HomeUIManager.Popup_CardChange.ShowUI(logDetail, cardOldList, cardCurrentList, () => isFinish = true);
        yield return new WaitUntil(() => isFinish == true);
        onComplete?.Invoke();
    }

    private bool CanShowChangeLogAndUpdateStatus(CardChangeLogDetail logDetail)
    {
        string cardChangeLogSaveKey = "card_change_log";
        Dictionary<string, object> playerSaveData = DataManager.Instance.GetPlayerSave();

        if (!playerSaveData.ContainsKey(cardChangeLogSaveKey))
            playerSaveData.Add(cardChangeLogSaveKey, "[]");

        List<string> changeLogVersionIDList = JsonConvert.DeserializeObject<List<string>>(playerSaveData[cardChangeLogSaveKey].ToString());
        if (changeLogVersionIDList.Contains(logDetail.ID))
        {
            return false;
        }

        changeLogVersionIDList.Add(logDetail.ID);
        DataManager.Instance.SetPlayerSave(cardChangeLogSaveKey, changeLogVersionIDList);
        return true;
    }

    #endregion

    #region Starter Popup

    private IEnumerator ShowTutorialReward()
    {
        PopupUIManager.Instance.Show_SoftLoad(true);

        bool isClaimed = true;
        if (
               DataManager.Instance.IsCompletedTutorial(GameMode.Tutorial_1)
            && DataManager.Instance.IsCompletedTutorial(GameMode.Tutorial_2)
            && DataManager.Instance.IsCompletedTutorial(GameMode.Tutorial_3)
            && DataManager.Instance.IsCompletedTutorial(GameMode.Tutorial_4)
        )
        {
            if (!DataManager.Instance.GetIsClaimTutorialReward())
            {
                isClaimed = false;

                DataManager.Instance.RequestClaimTutorialReward(
                    delegate (List<ItemData> result)
                    {
                        DataManager.Instance.LoadInventory(
                            delegate ()
                            {
                                PopupUIManager.Instance.ShowPopup_GotRewardAnimated(
                                      LocalizationManager.Instance.GetText("TUTORIAL_REWARD_TITLE")
                                    , LocalizationManager.Instance.GetText("TUTORIAL_GOT_REWARD")
                                    , result
                                    , delegate ()
                                    {
                                        isClaimed = true;
                                    }
                                );
                                PopupUIManager.Instance.Show_SoftLoad(false);
                            }
                            , delegate (string errorMsg)
                            {
                                PopupUIManager.Instance.ShowPopup_Error(
                                      LocalizationManager.Instance.GetText("TUTORIAL_REWARD_TITLE")
                                    , LocalizationManager.Instance.GetText(errorMsg)
                                    , delegate ()
                                    {
                                        isClaimed = true;
                                    }
                                );
                                PopupUIManager.Instance.Show_SoftLoad(false);
                            }
                        );
                    }
                    , delegate (string errorMsg)
                    {
                        PopupUIManager.Instance.ShowPopup_Error(
                              LocalizationManager.Instance.GetText("TUTORIAL_REWARD_TITLE")
                            , LocalizationManager.Instance.GetText(errorMsg)
                            , delegate ()
                            {
                                isClaimed = true;
                            }
                        );
                        PopupUIManager.Instance.Show_SoftLoad(false);
                    }
                );
            }
        }

        yield return new WaitUntil(() => isClaimed == true);

        PopupUIManager.Instance.Show_SoftLoad(false);
    }

    private IEnumerator ShowPopupRankingUpdate()
    {
        bool isFinish = false;
        SeasonData seasonData = DataManager.Instance.GetSeasonData();

        Action<bool> updateSeasonData = delegate (bool isUpdateSeason)
        {
            if ((isUpdateSeason && seasonData.CurrentSeason > 1))
            {
                seasonData.IsUpdateSeason = false;
                int prevSeason = seasonData.CurrentSeason - 1;
                int previousRank = DataManager.Instance.PlayerInfo.GetPlayerCurrentRank(prevSeason);
                int currentRank = DataManager.Instance.PlayerInfo.GetPlayerCurrentRank();

                List<ItemData> rewardItemList = new List<ItemData>();
                SeasonRankRewardDBData data;
                if (SeasonRankRewardDB.Instance.GetData(prevSeason.ToString(), out data))
                {
                    rewardItemList = data.GetRewardItemList(previousRank);
                }

                UnityAction onClick = () =>
                {
                    isFinish = true;
                    HomeUIManager.Popup_RankingUpdate.HideUI();
                };
                HomeUIManager.Popup_RankingUpdate.ShowUI(onClick, previousRank, currentRank, rewardItemList);
            }
            else
            {
                isFinish = true;
            }
        };

        if (GameHelper.IsNewDayUTC(DataManager.Instance.GetSeasonData().refreshTimeStamp))
        {
            PopupUIManager.Instance.Show_SoftLoad(true);
            DataManager.Instance.LoadSeasonData(() =>
            {
                seasonData = DataManager.Instance.GetSeasonData();
                PopupUIManager.Instance.Show_SoftLoad(false);
                updateSeasonData?.Invoke(seasonData.IsUpdateSeason);
            }, (error) => isFinish = true);
        }
        else
        {
            seasonData = DataManager.Instance.GetSeasonData();
            updateSeasonData?.Invoke(seasonData.IsUpdateSeason);
        }
        yield return new WaitUntil(() => isFinish == true);
    }

    private IEnumerator ShowSpecialLogin()
    {
        bool isFinish = false;

        SpecialLoginRewardData data = DataManager.Instance.GetSpecialLoginData().GetDailyLoginRewardData() as SpecialLoginRewardData;
        UnityAction ShowPopup = () =>
        {
            HomeUIManager.PopupSpecialLogin.SetData(DataManager.Instance.GetSpecialLoginData().GetDailyLoginRewardData() as SpecialLoginRewardData);
            HomeUIManager.PopupSpecialLogin.ShowUI(() =>
            {
                data = DataManager.Instance.GetSpecialLoginData().GetDailyLoginRewardData() as SpecialLoginRewardData;
                data.isNewDay = false;
                PopupUIManager.Instance.Show_SoftLoad(true);
                Debug.Log(data.rewardItem.itemID + " " + data.rewardItem.amount);
                isFinish = true;
            });
        };

        if (GameHelper.IsNewDayUTC(data.timestamp))
        {
            PopupUIManager.Instance.Show_SoftLoad(true);
            DataManager.Instance.RefreshLoginReward(() =>
            {
                PopupUIManager.Instance.Show_SoftLoad(false);
                ShowPopup.Invoke();
            }, (failMessage) => isFinish = true);
        }
        else
        {
            if (data.isNewDay && DataManager.Instance.GetSpecialLoginData().IsActionValid)
                ShowPopup.Invoke();
            else
                isFinish = true;
        }

        yield return new WaitUntil(() => isFinish == true);

        HomeUIManager.ShowSoftLoad(false);
        HomeUIManager.PopupSpecialLogin.HideUI();
    }

    private IEnumerator ShowDailyLogin()
    {
        bool isFinish = false;
        DailyLoginRewardData data = DataManager.Instance.GetDailyLoginData().GetDailyLoginRewardData();
        Action ShowPopupAndLoadInventory = delegate ()
        {
            HomeUIManager.Popup_DailyLoginReward.SetData(DataManager.Instance.GetDailyLoginData().GetDailyLoginRewardData());
            HomeUIManager.Popup_DailyLoginReward.ShowUI(() =>
            {
                data = DataManager.Instance.GetDailyLoginData().GetDailyLoginRewardData();
                data.isNewDay = false;
                PopupUIManager.Instance.Show_SoftLoad(true);
                Debug.Log(data.rewardItem.itemID + " " + data.rewardItem.amount);
                DataManager.Instance.LoadInventory(() => isFinish = true, (string failMessage) => isFinish = true);
            });
        };

        if (GameHelper.IsNewDayUTC(data.timestamp))
        {
            PopupUIManager.Instance.Show_SoftLoad(true);
            DataManager.Instance.RefreshLoginReward(() =>
            {
                PopupUIManager.Instance.Show_SoftLoad(false);
                ShowPopupAndLoadInventory?.Invoke();
            }, (failMessage) => isFinish = true);
        }
        else
        {
            if (data.isNewDay)
                ShowPopupAndLoadInventory?.Invoke();
            else
                isFinish = true;
        }

        yield return new WaitUntil(() => isFinish == true);

        HomeUIManager.ShowSoftLoad(false);
        HomeUIManager.Popup_DailyLoginReward.HideUI();
    }

    private IEnumerator ShowWildOffers()
    {
        Dictionary<string, ShowingOfferData> showingOffers = WildOfferManager.Instance.GetShowingOfferWithSortingByPriority();

        while (showingOffers.Count > 0)
        {
            KeyValuePair<string, ShowingOfferData> firstOfferData = showingOffers.First();

            bool isFinish = false;
            UnityAction onClick = () =>
            {
                isFinish = true;
                InitialWildOfferPanelUI();
            };
            ShowingOfferData offerData = firstOfferData.Value;
            WildOfferManager.Instance.AddDataAndShowWildOffer(firstOfferData.Key, offerData.shopItemData.ItemId, onClick, onClick, true, offerData.customData);
            yield return new WaitUntil(() => isFinish == true);
            //showingOffers.Remove(firstOfferData.Key);
        }

        yield break;
    }

    #endregion

    #region UI

    public override void ShowUI()
    {
        HomeUIManager.ShowMainCanvas();
        HomeUIManager.ShowHomePopupCanvas();
    }

    public override void HideUI(UnityAction onComplete)
    {
        HideMainCanvas();
        HomeUIManager.HideHomePopupCanvas();
        onComplete?.Invoke();
    }

    public void ShowFriends()
    {
        if (AuthenticationManager.IsBeGuest())
        {
            PopupUIManager.Instance.ShowPopup_ConfirmOnly(
                LocalizationManager.Instance.GetText("POPUP_FRIEND_TITLE_NOT_AVAILABLE_FOR_GUEST"),
                LocalizationManager.Instance.GetText("POPUP_FRIEND_BODY_NOT_AVAILABLE_FOR_GUEST")
                );
        }
        else
        {
            HomeUIManager.Popup_Friends.Show();
        }
    }

    public void HideFriends()
    {
        HomeUIManager.Popup_Friends.Hide();
    }

    public void ShowSubMenu()
    {
        HomeUIManager.PopupSubMenu.ShowUI();
    }

    public void HideSubMenu()
    {
        HomeUIManager.PopupSubMenu.HideUI();
    }

    public void ShowMainCanvas()
    {
        HomeUIManager.ShowMainCanvas();
    }

    public void HideMainCanvas()
    {
        HomeUIManager.HideMainCanvas();
    }
    #endregion

    #region Select GameMode

    public void ClickPlayBtn()
    {
        if (DataManager.Instance.IsHasDeckAvailable())
        {
            GameMode lastplayMode;
            GameHelper.StrToEnum(PlayerPrefs.GetString("LastplayMode"), out lastplayMode);

            if (lastplayMode == GameMode.Rank)
            {
                if (DataManager.Instance.CanPlayRankMode)
                {
                    ShowLobby(GameMode.Rank);
                }
                else
                {
                    ShowLobby(GameMode.Casual);
                }

            }
            else
            {
                ShowLobby(GameMode.Casual);
            }

        }
        else
        {
            string title = LocalizationManager.Instance.GetText("ERROR_FIND_DECK_NOT_FOUND");
            string message = LocalizationManager.Instance.GetText("");
            PopupUIManager.Instance.ShowPopup_ConfirmOnlyWithWarning(title, message);
        }
    }

    public void StartCasualMatch()
    {
        DataManager.Instance.CheckIsValidUserAndVersion(
            delegate ()
            {
                StartMatching(GameMode.Casual);
            }
            , NavigatorController.Instance.OnInvalidUserAndVersion
        );
    }

    public void StartRankMatch()
    {
        if (DataManager.Instance.IsFeatureEnable(FeatureUnlock.RankMode))
        {
            if (DataManager.Instance.CanPlayRankMode == false)
            {
                PopupUIManager.Instance.ShowPopup_Error("Season Ended", DataManager.Instance.GetSeasonStatus(), null);
                PopupUIManager.Instance.Show_SoftLoad(false);
                return;
            }
            DataManager.Instance.CheckIsValidUserAndVersion(
                delegate ()
                {
                    StartMatching(GameMode.Rank);
                }
                , NavigatorController.Instance.OnInvalidUserAndVersion
            );
        }
    }

    public void StartFriendlyMatch(string friendPlayfabID)
    {
        if (DataManager.Instance.IsHasDeckAvailable())
        {
            PopupUIManager.Instance.Show_SoftLoad(true);

            DataManager.Instance.CheckIsValidUserAndVersion(
                delegate ()
                {
                    StartMatching(GameMode.Friendly, friendPlayfabID);
                }
                , NavigatorController.Instance.OnInvalidUserAndVersion
            );
        }
        else
        {
            string title = LocalizationManager.Instance.GetText("ERROR_FIND_DECK_NOT_FOUND");
            string message = LocalizationManager.Instance.GetText("");
            PopupUIManager.Instance.ShowPopup_ConfirmOnlyWithWarning(title, message);
        }

    }

    public void StartEventBattleMatch()
    {
        PopupUIManager.Instance.Show_SoftLoad(true);
        DataManager.Instance.CheckIsValidUserAndVersion(
                 delegate ()
                 {
                     PopupUIManager.Instance.Show_SoftLoad(false);
                     StartMatching(GameMode.Event);
                 }
                 , NavigatorController.Instance.OnInvalidUserAndVersion
             );
    }

    public void JoinFriendlyMatch(PhotonRegion region, string roomName)
    {
        PopupUIManager.Instance.Show_SoftLoad(true);

        NavigatorController.Instance.LoadMatchingScene(delegate ()
        {
            MatchingUIManager matchingUIManager = FindObjectOfType<MatchingUIManager>();

            MatchSetting matchSetting = new MatchSetting(GameMode.Friendly, region, roomName);
            matchingUIManager.Init(matchSetting);

            PopupUIManager.Instance.Show_SoftLoad(false);
        });
    }

    public void StartPracticeMatch()
    {
        HomeUIManager.ShowSelectDeck(
             GameMode.Practice,
            delegate (GameMode mode)
            {
                HomeUIManager.DeckSelectUI.ShowSelectBotUI(delegate (string heroID, BotLevel botLevel)
                {
                    HomeUIManager.ShowSoftLoad(true);
                    DataManager.Instance.CheckIsValidUserAndVersion(
                        delegate ()
                        {
                            DataManager.Instance.ReloadDeckInventory(delegate
                            {
                                List<MatchPlayerData> matchPlayerDataList = new List<MatchPlayerData>();

                                // Local Player
                                {
                                    MatchPlayerData localMatchPlayerData = MatchPlayerData.CreateLocalMatchPlayerData();
                                    matchPlayerDataList.Add(localMatchPlayerData);
                                }

                                // Bot
                                {
                                    MatchPlayerData matchBotData = Bot.CreatePracticeBotMatchPlayerData(heroID, botLevel);
                                    matchPlayerDataList.Add(matchBotData);
                                }

                                MatchProperty matchProperty = new MatchProperty(
                                      gameMode: GameMode.Practice
                                    , playerCount: 2
                                    , isNetwork: false
                                    , isHaveTimer: false
                                    , isVSBot: true
                                    , botType: BotType.AIBot
                                    , botLevel: botLevel
                                    , adventureStageData: null
                                );

                                MatchDataContainer container;
                                MatchData.CreateMatchContainer(matchProperty, matchPlayerDataList, out container);
                                DontDestroyOnLoad(container.gameObject);

                                HomeUIManager.ShowSoftLoad(false);

                                if (DataManager.Instance.IsLocalDeckValid(matchPlayerDataList[0].Deck.DeckID))
                                {
                                    HomeUIManager.ShowSoftLoad(true);
                                    NavigatorController.Instance.GotoBattleScene();
                                }
                            }
                            , delegate
                            {
                                HomeUIManager.ShowSoftLoad(false);
                                Debug.LogError("Load Deck Failed");
                            });

                        }
                        , NavigatorController.Instance.OnInvalidUserAndVersion
                    );
                });
            },
            delegate (int index)
            {
                List<DeckData> deckList = DataManager.Instance.GetAllDeck();
                DataManager.Instance.SetCurrentDeckID(deckList[index].DeckID);

                Debug.Log("OnSelectedDeck " + deckList[index].DeckName + " " + deckList[index].DeckID);
            }
        );
    }

    public void StartMatching(GameMode mode, string opponentPlayfabID = "")
    {
        HomeUIManager.Popup_Friends.Hide();

        bool isEnable = true;
        switch (mode)
        {
            case GameMode.Casual: isEnable = DataManager.Instance.IsFeatureEnable(FeatureUnlock.CasualMode); break;
            case GameMode.Rank: isEnable = DataManager.Instance.IsFeatureEnable(FeatureUnlock.RankMode); break;
            case GameMode.Friendly: isEnable = DataManager.Instance.IsFeatureEnable(FeatureUnlock.FriendMode); break;
            case GameMode.Practice: isEnable = DataManager.Instance.IsFeatureEnable(FeatureUnlock.PracticeMode); break;
            case GameMode.Event: isEnable = DataManager.Instance.IsFeatureEnable(FeatureUnlock.EventBattleMode); break;
        }
        if (!isEnable)
        {
            PopupUIManager.Instance.Show_SoftLoad(false);
            return;
        }

        PopupUIManager.Instance.Show_SoftLoad(true);
        switch (mode)
        {
            case GameMode.Event:
                {
                    if (EventBattleDB.Instance.DoUsePlayerDeck)
                    {
                        DataManager.Instance.ReloadDeckInventory(delegate ()
                        {
                            if (DataManager.Instance.IsLocalDeckValid(DataManager.Instance.GetCurrentDeckID()))
                            {
                                NavigatorController.Instance.LoadMatchingScene(delegate ()
                                {
                                    MatchingUIManager matchingUIManager = FindObjectOfType<MatchingUIManager>();

                                    MatchSetting matchSetting = new MatchSetting(GameMode.Event);
                                    matchingUIManager.Init(matchSetting);

                                    PopupUIManager.Instance.Show_SoftLoad(false);
                                });
                            }
                            else
                            {
                                PopupUIManager.Instance.Show_SoftLoad(false);
                                Debug.LogWarning("Local deck In-valid !!!!");
                            }
                        }
                        , delegate (string error)
                        {
                            PopupUIManager.Instance.Show_SoftLoad(false);
                            Debug.LogError("Load Deck Failed " + error);
                        });
                    }
                    else
                    {
                        NavigatorController.Instance.LoadMatchingScene(delegate ()
                        {
                            MatchingUIManager matchingUIManager = FindObjectOfType<MatchingUIManager>();

                            MatchSetting matchSetting = new MatchSetting(GameMode.Event);
                            matchingUIManager.Init(matchSetting);

                            PopupUIManager.Instance.Show_SoftLoad(false);
                        });
                    }
                }
                break;

            default:
                {
                    DataManager.Instance.ReloadDeckInventory(delegate ()
                    {
                        if (DataManager.Instance.IsLocalDeckValid(DataManager.Instance.GetCurrentDeckID()))
                        {
                            NavigatorController.Instance.LoadMatchingScene(delegate ()
                            {
                                MatchingUIManager matchingUIManager = FindObjectOfType<MatchingUIManager>();

                                MatchSetting matchSetting = new MatchSetting(mode, opponentPlayfabID);
                                matchingUIManager.Init(matchSetting);

                                PopupUIManager.Instance.Show_SoftLoad(false);
                            });
                        }
                        else
                        {
                            PopupUIManager.Instance.Show_SoftLoad(false);
                            Debug.LogWarning("Local deck In-valid !!!!");
                        }
                    }
                    , delegate (string error)
                    {
                        PopupUIManager.Instance.Show_SoftLoad(false);
                        Debug.LogError("Load Deck Failed " + error);
                    });
                }
                break;
        }
    }

    public void HideSelectDeck()
    {
        // HOTFIX
        if (GameHelper.IsHaveObject(typeof(MatchingManager)))
        {
            MatchingManager matchingManager = GameObject.FindObjectOfType<MatchingManager>();
            matchingManager.ExitMatching();
        }

        HomeUIManager.HideSelectDeck();
    }
    #endregion

    public void ShowProfile()
    {
        ShowProfile(ProfileEditorManager.TabType.LevelProgression);
    }

    public void ShowProfile(ProfileEditorManager.TabType tab)
    {
        HomeUIManager.ShowProfileEditor(tab);
    }

    public void ShowMainEvent(EventPageType tab)
    {
        if (DataManager.Instance.IsFeatureEnable(FeatureUnlock.Event))
        {
            HomeUIManager.ShowEventPanel(tab);
        }
    }

    public void ShowDailyLoginReward()
    {
        DailyLoginRewardData data = DataManager.Instance.GetDailyLoginData().GetDailyLoginRewardData();
        HomeUIManager.Popup_DailyLoginReward.SetData(data);

        HomeUIManager.Popup_DailyLoginReward.SetIsClaimReward(true);
        HomeUIManager.Popup_DailyLoginReward.ShowUI(delegate
        {
            HomeUIManager.Popup_DailyLoginReward.HideUI();
        });
    }

    public void ShowLesson()
    {
        HomeUIManager.Popup_Lesson.ShowUI();
    }

    public void ShowAccomplishment()
    {
        ShowAccomplishment(AccomplishmentPanelManager.TabType.Achievement);
    }

    public void ShowAccomplishment(AccomplishmentPanelManager.TabType tab)
    {
        if (DataManager.Instance.IsFeatureEnable(FeatureUnlock.Accomplishment))
        {
            HomeUIManager.ShowAccomplishmentPanel(tab);
        }
    }

    public void ShowSocial()
    {
        HomeUIManager.ShowSocialPanel();
    }

    public void ShowCommunity()
    {
        if (DataManager.Instance.IsFeatureEnable(FeatureUnlock.Community))
        {
            KTPlayManager.Instance.ShowCommunity();
            OnShowCommunity?.Invoke();
        }
    }

    public void ShowSeasonPass()
    {
        ShowSeasonPass(SeasonPassUIManager.TabType.Reward);
    }

    public void ShowSeasonPass(SeasonPassUIManager.TabType tab)
    {
        if (DataManager.Instance.IsFeatureEnable(FeatureUnlock.SeasonPass))
        {
            HomeUIManager.ShowSeasonPassPanel(tab);
        }
    }

    public void ShowLeaderboard()
    {
    }

    public void ShowCosmetic()
    {
        HomeUIManager.ShowCosmeticPanel();
    }

    public void HideCosmetic()
    {
        HomeUIManager.HideCosmeticPanel();
    }

    public void ShowEventBattle()
    {
        PopupUIManager.Instance.Show_SoftLoad(true);
        NavigatorController.Instance.LoadEventBattleScene(delegate ()
        {
            EventBattleManager.Instance.Init();

            PopupUIManager.Instance.Show_SoftLoad(false);
        });
    }

    public void ShowMainAdventure()
    {
        HomeUIManager.ShowAdventurePanel();
    }

    public void ShowMainMission()
    {
        HomeUIManager.ShowMissionPanel();
    }

    public void ExitMainMission()
    {
        ShowStarterPopup();
    }

    public void ShowLobby(GameMode gameMode, List<string> elementFilterList = null)
    {
        HomeUIManager.ShowSelectDeck(
              gameMode
            , delegate (GameMode mode)
            {
                PopupUIManager.Instance.Show_SoftLoad(true);
                HomeUIManager.HideSelectDeck();

                switch (mode)
                {
                    case GameMode.Casual:
                        {
                            StartCasualMatch();
                        }
                        break;
                    case GameMode.Rank:
                        {
                            StartRankMatch();
                        }
                        break;

                    case GameMode.Event:
                        {
                            StartEventBattleMatch();
                        }
                        break;

                    default:
                        PopupUIManager.Instance.Show_SoftLoad(false);
                        break;
                }

                // Save Lastplay Mode.
                PlayerPrefs.SetString("LastplayMode", mode.ToString());
                PlayerPrefs.Save();
            },
            delegate (int index)
            {
                List<DeckData> deckList = DataManager.Instance.GetAllDeck();
                DataManager.Instance.SetCurrentDeckID(deckList[index].DeckID);

                Debug.Log("OnSelectedDeck " + deckList[index].DeckName + " " + deckList[index].DeckID);
            },
            elementFilterList
        );
    }
    #endregion

    #region BannerPopup Methods
    public void ClickFaceboook_TH()
    {
        Application.OpenURL(GameHelper.ATNFacebookTHLink);
    }

    public void ClickFaceboook_Global()
    {
        Application.OpenURL(GameHelper.ATNFacebookGlobalLink);
    }

    public void ClickTwitter()
    {
        Application.OpenURL(GameHelper.ATNTwitterLink);
    }

    public void ClickYouTube()
    {
        Application.OpenURL(GameHelper.ATNYouTubeLink);
    }

    public void ClickReddit()
    {
        Application.OpenURL(GameHelper.ATNRedditLink);
    }

    public void ClickTermCondition()
    {
        Application.OpenURL(GameHelper.TermConditionLink);
    }

    public void ClickContactUs()
    {
        GameHelper.LaunchEmail();
    }
    #endregion
}