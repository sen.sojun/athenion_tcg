﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class TopToolbarSetting
{
    public bool IsShowHomeBTN;
    public bool IsShowTopupBTN;
    public UnityAction OnClickHomeCallback;
}

public class TopToolbar : MonoBehaviour
{
    #region Inspector Properties
    [SerializeField] private Button BTN_Home;
    #endregion

    #region Properties
    private PlayerCoinTopBarUI _coinUI = null;
    private PlayerDiamondTopBarUI _diamondUI = null;
    private UIBatteryMeter _batteryUI = null;
    #endregion

    #region Static Methods
    private static List<TopToolbar> _instanceList = null;
    public static TopToolbar Instance { get { return GetInstance(); } }
    private static TopToolbar GetInstance()
    {
        if (_instanceList != null && _instanceList.Count > 0)
        {
            return _instanceList[_instanceList.Count - 1];
        }

        return null;
    }
    private static List<TopToolbarSetting> _settingList = null;

    private static bool _isShowHomeBTN_Default = false;
    private static bool _isShowTopupBTN_Default = false;
    private static UnityAction _onClickHome_Default = null;
    #endregion

    #region MonoBehaviour Methods
    //// Start is called before the first frame update
    //void Start()
    //{
    //}

    //// Update is called once per frame
    //void Update()
    //{
    //}

    private void OnEnable()
    {
        if (_instanceList == null)
        {
            _instanceList = new List<TopToolbar>();
        }
        _instanceList.Add(this);

        Init();
    }

    private void OnDisable()
    {
        Deinit();

        if (_instanceList != null)
        {
            _instanceList.Remove(this);
        }
    }
    #endregion

    #region Methods
    public static void AddSetting(bool isShowHome, bool isShowTooltip, UnityAction onClickHome)
    {
        TopToolbarSetting setting = new TopToolbarSetting();
        setting.IsShowHomeBTN = isShowHome;
        setting.IsShowTopupBTN = isShowTooltip;
        setting.OnClickHomeCallback = onClickHome;

        if (_settingList == null)
        {
            _settingList = new List<TopToolbarSetting>();
        }
        _settingList.Add(setting);

        foreach (TopToolbar toolbar in _instanceList)
        {
            toolbar.UpdateSetting();
        }
    }

    public static void RemoveLatestSetting()
    {
        if (_settingList != null && _settingList.Count > 0)
        {
            _settingList.RemoveAt(_settingList.Count - 1);

            foreach (TopToolbar toolbar in _instanceList)
            {
                toolbar.UpdateSetting();
            }
        }
    }

    public static TopToolbarSetting GetLatestSetting()
    {
        if (_settingList != null && _settingList.Count > 0)
        {
            return _settingList[_settingList.Count - 1];
        }
        else
        {
            TopToolbarSetting setting = new TopToolbarSetting();

            setting.IsShowHomeBTN = _isShowHomeBTN_Default;
            setting.IsShowTopupBTN = _isShowHomeBTN_Default;
            setting.OnClickHomeCallback = null;

            return setting;
        }
    }

    private void Init()
    {
        BTN_Home.onClick.RemoveAllListeners();
        BTN_Home.onClick.AddListener(this.OnClickHome);

        _coinUI = gameObject.GetComponentInChildren<PlayerCoinTopBarUI>();
        _diamondUI = gameObject.GetComponentInChildren<PlayerDiamondTopBarUI>();
        _batteryUI = gameObject.GetComponentInChildren<UIBatteryMeter>();

        UpdateSetting();
    }

    private void Deinit()
    {
        BTN_Home.onClick.RemoveListener(this.OnClickHome);
    }

    private void UpdateSetting()
    {
        TopToolbarSetting setting = GetLatestSetting();

        BTN_Home.gameObject.SetActive(setting.IsShowHomeBTN);

        if (Instance._diamondUI != null)
        {
            _diamondUI.TopupButton.gameObject.SetActive(setting.IsShowTopupBTN);
        }
    }
    #endregion

    #region Event Methods
    private void OnClickHome()
    {
        TopToolbarSetting setting = GetLatestSetting();

        setting.OnClickHomeCallback?.Invoke();
    }
    #endregion
}