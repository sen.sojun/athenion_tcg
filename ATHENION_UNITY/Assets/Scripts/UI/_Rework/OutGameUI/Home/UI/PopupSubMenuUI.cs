﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupSubMenuUI : MonoBehaviour
{
    [SerializeField] private Button _BTN_BlackClose;
    [SerializeField] private Button _BTN_Close;
    [SerializeField] private Transform _GROUP_Panel;

    #region Nagivation
    public void ShowUI()
    {
        GameHelper.UITransition_FadeMoveIn(this.gameObject, _GROUP_Panel.gameObject, 1.0f, 0.0f);
        SetBtnClose();
    }

    public void HideUI()
    {
        GameHelper.UITransition_FadeMoveOut(this.gameObject, _GROUP_Panel.gameObject, 1.0f, 0.0f);
    }
    #endregion

    #region Events
    public void SetBtnClose()
    {
        _BTN_BlackClose.onClick.RemoveAllListeners();
        _BTN_BlackClose.onClick.AddListener(HomeManager.Instance.HideSubMenu);

        _BTN_Close.onClick.RemoveAllListeners();
        _BTN_Close.onClick.AddListener(HomeManager.Instance.HideSubMenu);
    }
    #endregion
}
