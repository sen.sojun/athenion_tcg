﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Karamucho;
using System;

public class PopupPlayModeUI : MonoBehaviour
{
    [SerializeField] private Button BTN_Casual;
    [SerializeField] private Button BTN_Rank;
    [SerializeField] private Button BTN_Friend;
    [SerializeField] private Button BTN_Practice;
    [SerializeField] private Button BTN_EventBattle;
    [SerializeField] private Image IMG_EventBattle;

    public RectTransform Content;

    public static Action OnShowUI;
    public static Action OnHideUI;

    public void SetCasualBtnEvent(UnityAction onClick)
    {
        BTN_Casual.onClick.RemoveAllListeners();
        BTN_Casual.onClick.AddListener(onClick);
        BTN_Casual.interactable = DataManager.Instance.IsFeatureEnable(FeatureUnlock.CasualMode);
    }

    public void SetRankBtnEvent(UnityAction onClick)
    {
        BTN_Rank.onClick.RemoveAllListeners();
        BTN_Rank.onClick.AddListener(onClick);
        BTN_Rank.interactable = DataManager.Instance.IsFeatureEnable(FeatureUnlock.RankMode);
    }

    public void SetFriendBtnEvent(UnityAction onClick)
    {
        BTN_Friend.onClick.RemoveAllListeners();
        BTN_Friend.onClick.AddListener(onClick);
        BTN_Friend.interactable = DataManager.Instance.IsFeatureEnable(FeatureUnlock.FriendMode);
    }

    public void SetPracticeBtnEvent(UnityAction onClick)
    {
        BTN_Practice.onClick.RemoveAllListeners();
        BTN_Practice.onClick.AddListener(onClick);
        BTN_Practice.interactable = DataManager.Instance.IsFeatureEnable(FeatureUnlock.PracticeMode);
    }

    public void SetEventBattleBtnEvent(UnityAction onClick)
    {
        BTN_EventBattle.onClick.RemoveAllListeners();
        BTN_EventBattle.onClick.AddListener(onClick);
        BTN_EventBattle.interactable = DataManager.Instance.IsFeatureEnable(FeatureUnlock.EventBattleMode);

        string eventBattlePosterPath = "Events/EventBattles/Images/Menu_Event_Battle";
        Sprite sprite = ResourceManager.Load<Sprite>(eventBattlePosterPath);
        IMG_EventBattle.sprite = sprite;
    }

    public void ShowUI()
    {
        OnShowUI?.Invoke();

        BTN_Casual.interactable = DataManager.Instance.IsFeatureEnable(FeatureUnlock.CasualMode);
        BTN_Rank.interactable = DataManager.Instance.IsFeatureEnable(FeatureUnlock.RankMode);
        BTN_Friend.interactable = DataManager.Instance.IsFeatureEnable(FeatureUnlock.FriendMode);
        BTN_Practice.interactable = DataManager.Instance.IsFeatureEnable(FeatureUnlock.PracticeMode);
        BTN_EventBattle.interactable = DataManager.Instance.IsFeatureEnable(FeatureUnlock.EventBattleMode);

        this.gameObject.SetActive(true);
        Content.anchoredPosition = new Vector2(0, 0);
    }

    public void HideUI()
    {
        OnHideUI?.Invoke();
        this.gameObject.SetActive(false);
    }
}
