﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class WildOfferPanelUI : MonoBehaviour
{
    public WildOfferCell prefab;
    [SerializeField] private Transform _spawnPoint;

    private List<WildOfferCell> wildOfferCells = new List<WildOfferCell>();

    private Coroutine counter;

    public void Initialize()
    {
        RefreshData();
    }

    private void OnEnable()
    {
        if (counter != null)
            StopCoroutine(counter);
        counter = StartCoroutine(UpdateTick());

        DataManager.OnWildOfferAdded += RefreshData;
    }

    private void OnDisable()
    {
        if (counter != null)
            StopCoroutine(counter);

        DataManager.OnWildOfferAdded -= RefreshData;
    }

    private void RefreshData()
    {
        SetData(WildOfferManager.Instance.GetShowIconOffer());
        SetEvent((cell) =>
        {
            WildOfferManager.Instance.ShowWildOffer(cell.Key, cell.Data.ItemId, RefreshData);
        });
    }

    public void SetData(Dictionary<string, WildOfferData> data)
    {
        ClearCells();
        foreach (KeyValuePair<string, WildOfferData> offer in data)
        {
            ShopItemData shopItemData = WildOfferManager.Instance.WildOfferDB.GetShopItemData(offer.Value.ItemID);
            if (IsAtLimitAmount(shopItemData) == false)
            {
                DateTime expireDate = offer.Value.ExpireDate;
                WildOfferCell cell = Instantiate(prefab.gameObject, _spawnPoint).GetComponent<WildOfferCell>();
                cell.gameObject.SetActive(true);
                cell.SetData(offer.Key, offer.Value.ItemID, shopItemData, expireDate);
                wildOfferCells.Add(cell);
            }
        }
        if (counter != null)
            StopCoroutine(counter);
        counter = StartCoroutine(UpdateTick());
    }

    private bool IsAtLimitAmount(ShopItemData shopItemData)
    {
        bool isAtLimit = shopItemData.IsAtLimitAmount;
        try
        {
            ShopItemType shopItemType = shopItemData.CustomData.ShopItemType;
            if (shopItemType == ShopItemType.COSMETIC)
            {
                CosmeticType cosmeticType = shopItemData.CustomData.CosmeticType;
                if (cosmeticType == CosmeticType.CONSOLE)
                {
                    string[] text = shopItemData.ItemId.Split('_');
                    string ConsoleID = text[1].ToUpper();
                    PlayerInventoryData inventory = DataManager.Instance.InventoryData;
                    return inventory.GetAmountByItemID(ConsoleID) > 0;
                }
            }
        }
        catch (Exception)
        {
            //
        }        
        return isAtLimit;
    }

    private void ClearCells()
    {
        for (int i = 0; i < wildOfferCells.Count; i++)
        {
            Destroy(wildOfferCells[i].gameObject);
            wildOfferCells.RemoveAt(i);
            i--;
        }
    }

    public void SetEvent(UnityAction<WildOfferCell> onClick)
    {
        for (int i = 0; i < wildOfferCells.Count; i++)
        {
            int index = i;
            wildOfferCells[i].SetEvent(() => { onClick?.Invoke(wildOfferCells[index]); });
        }
    }

    private IEnumerator UpdateTick()
    {
        while (wildOfferCells.Count > 0)
        {
            CheckValidWildOfferCells();
            UpdateTimeTextWildOfferCells();
            yield return new WaitForSeconds(1);
        }
        counter = null;
        yield break;
    }

    private void UpdateTimeTextWildOfferCells()
    {
        for (int i = 0; i < wildOfferCells.Count; i++)
        {
            wildOfferCells[i].UpdateTick();
        }
    }

    private void CheckValidWildOfferCells()
    {
        for (int i = 0; i < wildOfferCells.Count; i++)
        {
            if (wildOfferCells[i].IsValidTime() == false)
            {
                Destroy(wildOfferCells[i].gameObject);
                wildOfferCells.RemoveAt(i);
                i--;
            }
        }
    }
}
