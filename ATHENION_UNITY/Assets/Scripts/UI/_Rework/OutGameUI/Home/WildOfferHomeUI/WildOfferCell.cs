﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;

public class WildOfferCell : MonoBehaviour
{
    [SerializeField] private Image _image;
    [SerializeField] private TextMeshProUGUI _timeText;
    [SerializeField] private Button _button;

    public string Key { get; private set; }
    public string ItemID { get; private set; }
    public ShopItemData Data { get; private set; }

    private DateTime expireDate;

    public bool IsValidTime()
    {
        TimeSpan time = expireDate - DateTimeData.GetDateTimeUTC();
        return time.TotalSeconds > 0;
    }

    public void SetData(string key, string itemID, ShopItemData data, DateTime expireDate)
    {
        this.Key = key;
        this.ItemID = itemID;
        this.Data = data;
        this.expireDate = expireDate;
        SetupUI();
    }

    public void SetEvent(UnityAction onClick)
    {
        _button.onClick.RemoveAllListeners();
        _button.onClick.AddListener(onClick);
    }

    private void SetupUI()
    {
        string itemKey = GameHelper.ConvertItemIDToItemKey(ItemID);
        _image.sprite = SpriteResourceHelper.LoadSprite(itemKey, SpriteResourceHelper.SpriteType.ICON_WildOffer);
    }

    public void UpdateTick()
    {
        TimeSpan time = expireDate - DateTimeData.GetDateTimeUTC();
        if (time.TotalSeconds > 0)
        {
            _timeText.text = time.ToTimeFormat();
        }
        else
        {
            _timeText.text = "00:00:00";
        }
    }
}
