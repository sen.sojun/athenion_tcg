﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Karamucho;

public class RandomLoadingArt : MonoBehaviour
{
    [SerializeField] private Image Art;

    private const string _loadingArtRootPath = "Images/LoadingArts/";
    private const string _loadingArtPrefix = "LOADINGART_";
    private const float _maxRandomIndex = 11;

    private void OnEnable()
    {
        SetArt(GetRandomArt());
    }

    private void SetArt(Sprite sprite)
    {
        Art.sprite = sprite;
    }

    private Sprite GetRandomArt()
    {
        int index = (int)(Random.Range(0, _maxRandomIndex + 1));
        string path = string.Format("{0}{1}{2}", _loadingArtRootPath, _loadingArtPrefix, index);
        Sprite sprite = ResourceManager.Load<Sprite>(path);

        if (sprite != null)
        {
            return sprite;
        }
        else
        {
            index = 0;
            path = string.Format("{0}{1}{2}", _loadingArtRootPath, _loadingArtPrefix, index);
            Sprite defaultSprite = ResourceManager.Load<Sprite>(path);
            return defaultSprite;
        }
    }
}
