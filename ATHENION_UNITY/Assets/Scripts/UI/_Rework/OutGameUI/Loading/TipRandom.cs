﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;
using UnityEngine.UI;

public class TipRandom : MonoBehaviour
{
    public TextMeshProUGUI TEXT_Tip;
    public Button BTN_NextTip;

    private int _randomIndex = 0;
    private int _maxIndex = 36;
    private Sequence _sq;

    private void OnEnable()
    {   
        InitBTN();
        Show();      
    }

    private void Show()
    {
        _sq.Kill();
        _sq = DOTween.Sequence();

        float duration = 1.5f;
        TEXT_Tip.alpha = 0.0f;
        _sq.Append(TEXT_Tip.DOFade(1.0f, duration));

        _randomIndex = Random.Range(1, _maxIndex);
        string index = string.Format("{0:D4}", _randomIndex);
        string tipKey = "TIP" + index;
        //Debug.LogWarning("Show tip:" + tipKey);

        TEXT_Tip.text = LocalizationManager.Instance.GetText(tipKey);


    }

    public void NextTip()
    {
        Show();
    }

    private void InitBTN()
    {
        BTN_NextTip.onClick.RemoveAllListeners();
        BTN_NextTip.onClick.AddListener(NextTip);
    }


}
