﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupSocial : MonoBehaviour
{
    [SerializeField] private Button _BTN_Close;

    public void Show()
    {
        InitBTN();
        GameHelper.UITransition_FadeIn(this.gameObject);
    }

    public void Hide()
    {
        GameHelper.UITransition_FadeOut(this.gameObject);
    }

    private void InitBTN()
    {
        _BTN_Close.onClick.RemoveAllListeners();
        _BTN_Close.onClick.AddListener(Hide);
    }
}
