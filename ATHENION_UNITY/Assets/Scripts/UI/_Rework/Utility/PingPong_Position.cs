﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PingPong_Position : MonoBehaviour
{
    public Vector3 TargetOffset;
    public float Duration = 0.5f;
    public Ease ease;

    #region Private Properties
    private Sequence _sq;
    private Vector3 _originalPosition;
    #endregion

    private void Start()
    {
        _originalPosition = gameObject.transform.localPosition;
        _sq.Kill();
        _sq = DOTween.Sequence();

        _sq.Append(gameObject.transform.DOLocalMove(_originalPosition+TargetOffset, Duration/2).SetEase(ease));
        _sq.Append(gameObject.transform.DOLocalMove(_originalPosition, Duration/2).SetEase(ease));
        _sq.SetLoops(-1);
    }
}
