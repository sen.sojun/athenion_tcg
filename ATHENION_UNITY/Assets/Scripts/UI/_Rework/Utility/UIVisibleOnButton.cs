﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIVisibleOnButton : MonoBehaviour
{
    #region Public Properties
    [Header("MainButton")]
    public GameObject MainObject;

    [Header("Show On Enable")]
    public List<GameObject> EnableShowList;

    [Header("Show On Disable")]
    public List<GameObject> DisbleShowList;
    #endregion

    #region Private Properties
    private bool isInteractable = false;
    #endregion


    #region Methods
    private void Update()
    {
        //if (isInteractable != MainObject.activeSelf)
        {
            isInteractable = MainObject.activeSelf;
            UpdateState();
        }

    }

    private void UpdateState()
    {
        // Show
        SetGameObjectVisibleList(isInteractable, EnableShowList);

        // Hide
        SetGameObjectVisibleList(!isInteractable, DisbleShowList);
    }

    private void SetGameObjectVisibleList(bool isShow, List<GameObject> targetList)
    {
        foreach (GameObject item in targetList)
        {
            item.SetActive(isShow);
        }
    }
    #endregion


}
