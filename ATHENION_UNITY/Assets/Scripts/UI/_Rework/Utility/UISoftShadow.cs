﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Coffee.UIExtensions.UIEffect))]
[RequireComponent(typeof(Coffee.UIExtensions.UIShadow))]
public class UISoftShadow : MonoBehaviour
{
    private void Start()
    {
        Coffee.UIExtensions.UIEffect effect = GetComponent<Coffee.UIExtensions.UIEffect>();
        Coffee.UIExtensions.UIShadow shadow = GetComponent<Coffee.UIExtensions.UIShadow>();

    }
}
