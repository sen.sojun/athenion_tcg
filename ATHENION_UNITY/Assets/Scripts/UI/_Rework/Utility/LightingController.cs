﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;

public class LightingController : MonoBehaviour
{
    [Header("Dir Light")]
    [SerializeField] private Light L_Sun;
    [SerializeField] private float L_Sun_DefaultIntensity = 0.9f;

    [Header("Spot Light")]
    [SerializeField] private Light L_TopHero;
    [SerializeField] private Light L_Mid;
    [SerializeField] private Light L_BotHero;
    [SerializeField] private float L_Spotlight_DefaultIntensity = 4.0f;


    private void Start()
    {
        ResetLight();
    }

    #region Methods
    public void ResetLight()
    {
        L_Sun.intensity = L_Sun_DefaultIntensity;
        L_TopHero.intensity = 0;
        L_BotHero.intensity = 0;
        L_Mid.intensity = 0;
    }

    public void AdjustSunLightIntensity(float intensity = 0.9f, float duration = 0.5f, UnityAction onComplete = null)
    {
        L_Sun.DOIntensity(intensity, duration).OnComplete(() => onComplete.Invoke());
    }
    #endregion

    #region SpotLight
    public void ShowTopSpotLight(bool isShow, UnityAction onComplete = null)
    {
        ShowLight(L_TopHero, isShow, L_Spotlight_DefaultIntensity);
    }

    public void ShowBotSpotLight(bool isShow, UnityAction onComplete = null)
    {
        ShowLight(L_BotHero, isShow, L_Spotlight_DefaultIntensity);
    }

    public void ShowMidSpotLight(bool isShow, UnityAction onComplete = null)
    {
        ShowLight(L_Mid, isShow, L_Spotlight_DefaultIntensity);
    }
    #endregion

    private void ShowLight(Light light, bool isShow, float intensity, float duration = 0.5f, UnityAction onComplete = null)
    {
        if (isShow)
        {
            light.gameObject.SetActive(isShow);
            light.intensity = 0;
            light.DOIntensity(intensity, duration);
            AdjustSunLightIntensity(0, duration, onComplete);
        }
        else
        {
            light.DOIntensity(0, duration).OnComplete(()=> light.gameObject.SetActive(isShow));
        }
    }

}
