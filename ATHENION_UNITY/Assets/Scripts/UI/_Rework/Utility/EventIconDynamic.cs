﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EventIconDynamic : MonoBehaviour
{
    private void OnEnable()
    {
        string key = "ICON_EVENT_" + GameHelper.GetEventThemeKey();
        GetComponent<Image>().sprite = SpriteResourceHelper.LoadSprite(key, SpriteResourceHelper.SpriteType.ICON_Item);
    }
}
