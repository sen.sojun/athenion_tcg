﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class FilterUI : MonoBehaviour
{
    [Serializable]
    public class TabUI
    {
        public Button Button;
        public bool isActive;

        public void Toggle()
        {
            isActive = !isActive;
        }

        public void SetActive(bool active)
        {
            isActive = active;
        }
    }

    [SerializeField] private Button _AllButton;
    [SerializeField] private TabUI[] _Tab;

    private bool[] _activeTabList;

    private UnityAction<bool[]> _onUpdateFilter;

    private void Awake()
    {
        _AllButton.onClick.RemoveAllListeners();
        _AllButton.onClick.AddListener(OnClickAllButton);

        for (int i = 0; i < _Tab.Length; i++)
        {
            int index = i;
            _Tab[i].Button.onClick.RemoveAllListeners();
            _Tab[i].Button.onClick.AddListener(() =>
            {
                OnClickTab(index);
            });
        }
        _activeTabList = new bool[_Tab.Length];
    }

    public void SetEvent(UnityAction<bool[]> onUpdateFilter)
    {
        _onUpdateFilter = onUpdateFilter;
    }

    private void OnClickAllButton()
    {
        for (int i = 0; i < _Tab.Length; i++)
        {
            _Tab[i].SetActive(true);
        }
        UpdateActiveTabList();
    }

    private void OnClickTab(int index)
    {
        _Tab[index].Toggle();
        UpdateActiveTabList();
    }

    private void UpdateActiveTabList()
    {
        for (int i = 0; i < _Tab.Length; i++)
        {
            _activeTabList[i] = _Tab[i].isActive;
        }
        _onUpdateFilter?.Invoke(_activeTabList);
    }
}
