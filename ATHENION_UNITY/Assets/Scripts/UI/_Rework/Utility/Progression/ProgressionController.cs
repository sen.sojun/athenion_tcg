﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using DG.Tweening;
using TMPro;

public class ProgressionController : MonoBehaviour
{
    #region Inspectors
    [SerializeField] private ProgressionCell _levelProgressionCell;
    [SerializeField] private ProgressionCell _levelFactionProgressionCell;
    #endregion

    #region Private Properties
    private PlayerLevelProgressionData _levelProgression;
    private PlayerLevelFactionProgressionData _levelFactionProgression;
    #endregion

    #region Methods
    public void InitData()
    {
        _levelProgression = new PlayerLevelProgressionData();
        _levelFactionProgression = new PlayerLevelFactionProgressionData();

        _levelProgression.InitData();
        _levelFactionProgression.InitData();
    }

    public void StartProgression(UnityAction onComplete)
    {
        // Update Lasted Data
        GameHelper.UITransition_FadeIn(this.gameObject);

        _levelProgression.StampData();
        _levelFactionProgression.StampData();

        SetProgressionCell(_levelProgression, _levelProgressionCell);
        SetProgressionCell(_levelFactionProgression, _levelFactionProgressionCell);

        UnityAction actionFactionLevel = delegate
        {
            _levelFactionProgressionCell.Show(delegate
            {
                _levelFactionProgressionCell.StartProgressionLoop(delegate
                {
                    onComplete?.Invoke();
                });
            });
        };

        UnityAction actionLevel = delegate
        {
            _levelProgressionCell.StartProgressionLoop(delegate
            {      
                StartCoroutine(WaitFor(delegate
                {
                    _levelProgressionCell.Hide();
                    actionFactionLevel?.Invoke();
                }));
                
            });
        };

        // Start Playing Progression Slider
        _levelProgressionCell.Show(actionLevel);
    }

    private IEnumerator WaitFor(UnityAction onWaitComplete)
    {
        yield return new WaitForSeconds(1.5f);
        onWaitComplete?.Invoke();
    }

    private void SetProgressionCell(ProgressionData data, ProgressionCell cell)
    {
        cell.SetupData(new ProgrssionUIData(data));
    }
    #endregion
}