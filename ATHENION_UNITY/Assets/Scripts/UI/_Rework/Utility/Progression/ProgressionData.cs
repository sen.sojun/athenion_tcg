﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProgressionTable
{
    public int Index { get; private set; }
    public int MinValue { get; private set; }
    public int MaxValue { get; private set; }
    public int StartValue { get; private set; }
    public int EndValue { get; private set; }

    public ProgressionTable(int index, int minValue, int maxValue)
    {
        Index = index;
        MinValue = minValue;
        MaxValue = maxValue;
        StartValue = 0;
        EndValue = 0;
    }

    public ProgressionTable(ProgressionTable data)
    {
        Index = data.Index;
        MinValue = data.MinValue;
        MaxValue = data.MaxValue;
        StartValue = data.StartValue;
        EndValue = data.EndValue;
    }

    public void SetStartValue(int value)
    {
        StartValue = value;
    }

    public void SetEndValue(int value)
    {
        EndValue = value;
    }
}

public abstract class ProgressionData
{
    protected List<ProgressionTable> _progressionTableData = new List<ProgressionTable>();

    public List<ProgressionTable> GetProgression()
    {
        List<ProgressionTable> result = new List<ProgressionTable>();
        int startProgress = GetStartProgress();
        int endProgress = GetEndProgress();
        int currentProgress = startProgress;
        foreach (ProgressionTable item in _progressionTableData)
        {
            if (currentProgress >= item.MinValue && currentProgress <= item.MaxValue)
            {
                item.SetStartValue(currentProgress);

                currentProgress = endProgress;
                if (currentProgress > item.MaxValue)
                {
                    currentProgress = item.MaxValue;
                }

                ProgressionTable table = new ProgressionTable(item);
                table.SetEndValue(currentProgress);
                result.Add(table);
            }
            else if (currentProgress > item.MaxValue)
            {
                continue;
            }
            else
            {
                break;
            }
        }

        return result;
    }

    public abstract void InitData();

    protected abstract void SetupProgressionTable();

    public abstract void StampData();

    protected abstract int GetStartProgress();

    protected abstract int GetEndProgress();
}

public class PlayerLevelProgressionData : ProgressionData
{
    private PlayerLevelData _oldData;
    private PlayerLevelData _newData;

    public override void InitData()
    {
        _oldData = new PlayerLevelData(DataManager.Instance.PlayerInfo.LevelData);
        _newData = new PlayerLevelData(_oldData);

        SetupProgressionTable();
    }

    protected override void SetupProgressionTable()
    {
        List<PlayerExpDBData> data;
        PlayerExpDB.Instance.GetAllData(out data);

        int startExp = 0;
        int nextExp = 0;
        for (int i = 0; i < data.Count; i++)
        {
            startExp = data[i].AccumulativeExp;
            nextExp = startExp;
            if (i+1 < data.Count)
            {
                nextExp = data[i + 1].AccumulativeExp;
            }

            _progressionTableData.Add(new ProgressionTable(i + 1, startExp, nextExp));
        }
    }

    public override void StampData()
    {
        _newData = new PlayerLevelData(DataManager.Instance.PlayerInfo.LevelData);
    }

    protected override int GetStartProgress()
    {
        return _oldData.AccExp;
    }

    protected override int GetEndProgress()
    {
        return _newData.AccExp;
    }
}

public class PlayerLevelFactionProgressionData : ProgressionData
{
    private PlayerLevelFactionData _oldData;
    private PlayerLevelFactionData _newData;

    public override void InitData()
    {
        _oldData = new PlayerLevelFactionData(DataManager.Instance.PlayerInfo.LevelFactionData);
        _newData = new PlayerLevelFactionData(_oldData);

        SetupProgressionTable();
    }

    protected override void SetupProgressionTable()
    {
        List<PlayerExpFactionDBData> data;
        PlayerExpFactionDB.Instance.GetAllData(out data);

        int startExp = 0;
        int nextExp = 0;
        for (int i = 0; i < data.Count; i++)
        {
            int level = i;
            startExp = data[i].AccumulativeExp;
            nextExp = startExp;
            if (i + 1 < data.Count)
            {
                nextExp = data[i + 1].AccumulativeExp;
                level = i + 1;
            }

            _progressionTableData.Add(new ProgressionTable(level, startExp, nextExp));
        }
    }

    public override void StampData()
    {   
        _newData = new PlayerLevelFactionData(DataManager.Instance.PlayerInfo.LevelFactionData);
    }

    public CardElementType GetCardElementType()
    {
        return GameManager.Instance.Data.PlayerDataList[(int)GameManager.Instance.GetLocalPlayerIndex()].Hero.ElementType;
    }

    public LevelFactionData GetLevelData()
    {
        return _newData.GetLevelFactionData(GetCardElementType());
    }

    protected override int GetStartProgress()
    {
        LevelFactionData data = _oldData.GetLevelFactionData(GetCardElementType());
        if(data != null)
        {
            return data.AccExp;
        }
        return 0;
    }

    protected override int GetEndProgress()
    {
        LevelFactionData data = _newData.GetLevelFactionData(GetCardElementType());
        if (data != null)
        {
            return data.AccExp;
        }
        return 0;
    }
}