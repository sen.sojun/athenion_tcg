﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using DG.Tweening;
using TMPro;

public class ProgrssionUIData
{
    #region Public Properties
    public List<ProgressionTable> ProgressionTableList { private set; get; }
    #endregion

    #region Constructors
    public ProgrssionUIData()
    {
        ProgressionTableList = new List<ProgressionTable>();
    }

    public ProgrssionUIData(ProgressionData data)
    {
        ProgressionTableList = new List<ProgressionTable>(data.GetProgression());
    }
    #endregion

    #region Methods
    #endregion
}

public class ProgressionCell : MonoBehaviour
{
    #region Inspectors
    [Header("UI")]
    [SerializeField] private TextMeshProUGUI _TEXT_title;
    [SerializeField] private TextMeshProUGUI _TEXT_startLv;
    [SerializeField] private TextMeshProUGUI _TEXT_endLv;
    [SerializeField] private Slider _SliderEXP;
    [SerializeField] private TextMeshProUGUI _TEXT_expPt;
    [SerializeField] private GameObject _VFX_LevelUp;
    #endregion

    #region Private Properties
    private static float _fillRate = 0.2f; //value per second
    private static float _animateDuration = 2.0f; //value per second

    private Sequence _sq;

    private int _expSoundIndex;
    private ProgrssionUIData _data;
    private List<ProgressionTable> _table;
    #endregion

    #region Methods
    private void OnDisable()
    {
        StopSound();
    }

    public void SetupData(ProgrssionUIData data)
    {
        _data = data;
        _table = _data.ProgressionTableList;

        // Set First Time
        float realStartValue = _table[0].StartValue - _table[0].MinValue;
        float realEndValue = _table[0].EndValue - _table[0].MinValue;
        float capValue = _table[0].MaxValue - _table[0].MinValue;

        // Get Slider Value
        float startSliderValue = realStartValue / capValue;
        float endSliderValue = realEndValue / capValue;

        // Init Data
        _SliderEXP.value = startSliderValue;
        UpdateTextExp(_table[0].MinValue, _table[0].MaxValue);

        // Update Text Exp
        _TEXT_startLv.text = "LV." + _table[0].Index.ToString();
        _TEXT_endLv.text = "LV." + (_table[0].Index + 1).ToString();
    }

    public void Show(UnityAction onComplete)
    {
        GameHelper.UITransition_FadeIn(this.gameObject, onComplete);
    }

    public void Hide()
    {
        GameHelper.UITransition_FadeOut(this.gameObject);
    }

    public void StartProgressionLoop(UnityAction onComplete)
    {
        StartCoroutine(IEProgressionLoop(_table, onComplete));
    }

    private IEnumerator IEProgressionLoop(List<ProgressionTable> table, UnityAction onComplete)
    {
        foreach(ProgressionTable item in table)
        {
            bool isFinished = false;
            PlayProgressionOnce(item, () => { isFinished = true; });
            yield return new WaitUntil(() => isFinished == true);
        }
        onComplete?.Invoke();
    }

    // Animate per round
    private void PlayProgressionOnce(ProgressionTable tableData, UnityAction onComplete)
    {
        float realStartValue = tableData.StartValue - tableData.MinValue;
        float realEndValue = tableData.EndValue - tableData.MinValue;
        float capValue = tableData.MaxValue - tableData.MinValue;

        // Get Slider Value
        float startSliderValue = realStartValue / capValue;
        float endSliderValue = realEndValue / capValue;
        if(capValue == 0)
        {
            startSliderValue = 1;
            endSliderValue = 1;
        }

        // Init Data
        _SliderEXP.value = startSliderValue;
        UpdateTextExp(tableData.MinValue, tableData.MaxValue);

        // Update Text Exp
        _TEXT_startLv.text = "LV." + tableData.Index.ToString();
        _TEXT_endLv.text = "LV." + (tableData.Index + 1).ToString();
        _SliderEXP.onValueChanged.RemoveAllListeners();
        _SliderEXP.onValueChanged.AddListener(
            delegate
            {
                _TEXT_startLv.text = "LV." + tableData.Index.ToString();
                _TEXT_endLv.text = "LV." + (tableData.Index + 1).ToString();
                UpdateTextExp(tableData.MinValue, tableData.MaxValue);
            }
        );

        // Animation Slider Bar
        float duration = (endSliderValue - startSliderValue) / _fillRate;
        duration = _animateDuration;
        _expSoundIndex = SoundManager.PlaySFX(SoundManager.SFX.Exp_Increasing_Loop, true);
        _sq = DOTween.Sequence();
        _sq.Append(_SliderEXP.DOValue(endSliderValue, duration));
        _sq.OnComplete(
            delegate () 
            {
                // Levelup Check
                if (_SliderEXP.value == 1.0f
                    && realEndValue > realStartValue)
                {
                    SoundManager.PlaySFX(SoundManager.SFX.Exp_Level_Up);
                    _VFX_LevelUp.SetActive(true);
                }
                _SliderEXP.value = endSliderValue;
                UpdateTextExp(tableData.MinValue, tableData.MaxValue);

                StopSound();

                onComplete?.Invoke();
            }
        );
    }

    private void StopSound()
    {
        SoundManager.StopSFX(_expSoundIndex, 0.0f);
    }

    private void UpdateTextExp(float minCap, float maxCap)
    {
        int intMinCap = Mathf.CeilToInt(minCap);
        int intMaxCap = Mathf.CeilToInt(maxCap);
        int realMaxCap = intMaxCap - intMinCap;
        _TEXT_expPt.text = (intMinCap == intMaxCap) ? "Max" : string.Format("{0} / {1}", Mathf.CeilToInt(_SliderEXP.value * realMaxCap), realMaxCap);
    }
    #endregion
}