﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class PopupAttachment : MonoBehaviour
{
    #region Enum
    public enum PopupType
    {
        Normal
        , Warning
        , FullDetail
    }
    #endregion
    public bool EnablePopup = true;
    public PopupType Type = PopupType.Normal;
    public Button BTN_Lock;

    [Header("Localize Key")]
    public string TitleKey = "";
    [TextArea] public string MessageKey = "";

    private void OnEnable()
    {
        if (BTN_Lock != null)
        {
            BTN_Lock.onClick.RemoveAllListeners();
            BTN_Lock.onClick.AddListener(ShowPopup);
        }
    }

    private void OnDisable()
    {
        if (BTN_Lock != null)
        {
            BTN_Lock.onClick.RemoveAllListeners();
        }
    }

    public void SetEnable(bool isEnable)
    {
        if (BTN_Lock != null)
        {
            BTN_Lock.gameObject.SetActive(isEnable);
        }

    }

    private void ShowPopup()
    {
        if (EnablePopup == false) return;

        string head = LocalizationManager.Instance.GetText(TitleKey);
        string message = LocalizationManager.Instance.GetText(MessageKey);

        switch (Type)
        {
            case PopupType.Normal:
            {
                PopupUIManager.Instance.ShowPopup_ConfirmOnly(head, message);
                break;
            }
            case PopupType.Warning:
            {
                PopupUIManager.Instance.ShowPopup_ConfirmOnlyWithWarning(head, message);
                break;
            }
            case PopupType.FullDetail:
            {
                PopupUIManager.Instance.ShowPopup_DetailWindow(head, message);
                break;
            }
        }
    }
}
