﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[ExecuteInEditMode]
public class TextMeshPro_LimitMaxLine : MonoBehaviour
{
    public GameObject Button_Close;

    private void OnEnable()
    {
        TextMeshProUGUI tm = GetComponent<TextMeshProUGUI>();
        if (tm != null) {
            Button_Close.SetActive(tm.preferredHeight > 1200);
        }
    }

}
