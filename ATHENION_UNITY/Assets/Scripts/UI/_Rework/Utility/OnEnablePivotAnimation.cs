﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class OnEnablePivotAnimation : MonoBehaviour
{
    [Header("Start Pivot")]
    public Vector2 StartPivot = new Vector2(0.0f,0.0f);

    private Sequence _sq;

    private void OnEnable()
    {
        if (_sq != null)
        {
            _sq.Kill();
        }

        _sq = DOTween.Sequence();
        if (StartPivot.x == 0 && StartPivot.y == 0)
        {
            _sq = GameHelper.UITransition_FadeIn(this.gameObject);
        }
        else
        {
            _sq = GameHelper.UITransition_FadeMoveIn(this.gameObject, this.gameObject, StartPivot.x, StartPivot.y);
        }
       

    }

}
