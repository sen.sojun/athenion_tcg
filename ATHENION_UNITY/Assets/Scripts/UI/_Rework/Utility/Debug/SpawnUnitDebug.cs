﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Karamucho.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class SpawnUnitDebug : MonoBehaviour
{
    #region Inspector
    [Header("Controller")]
    [SerializeField] private LightingController LightingController;

    [Header("Input")]
    [SerializeField] private TMP_InputField INPUT_Spawn;
    [SerializeField] private TMP_InputField INPUT_Attack;
    [SerializeField] private TMP_InputField INPUT_Death;

    [Header("UI")]
    [SerializeField] private Button BTN_Play;
    [SerializeField] private GameObject HandPosition;
    [SerializeField] private GameObject SlotPosition;

    [Header("Prefab")]
    [SerializeField] private GameObject Prefab_Card;
    #endregion

    #region Private Properties
    private static readonly string Default_SpawnEFXKey = "VFX_Spawn_Default";
    private static readonly string Default_AttackEFXKey = "VFX_Attack_Default";
    private static readonly string Default_DeathEFXKey = "VFX_Death_Default";

    private Coroutine _task = null;
    private CardUI _cardUI = null;
    private string _spawnEFXKey = "";
    private string _attackEFXKey = "";
    private string _deathEFXKey = "";
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        BTN_Play.onClick.RemoveAllListeners();
        BTN_Play.onClick.AddListener(OnClickPlay);
    }

    private void OnClickPlay()
    {
        _spawnEFXKey = INPUT_Spawn.text;
        _attackEFXKey = INPUT_Attack.text;
        _deathEFXKey = INPUT_Death.text;

        // check null
        if (_spawnEFXKey == null || string.IsNullOrEmpty(_spawnEFXKey))
        {
            _spawnEFXKey = Default_SpawnEFXKey;
        }
        if (_attackEFXKey == null || string.IsNullOrEmpty(_attackEFXKey))
        {
            _attackEFXKey = Default_AttackEFXKey;
        }
        if (_deathEFXKey == null || string.IsNullOrEmpty(_deathEFXKey))
        {
            _deathEFXKey = Default_DeathEFXKey;
        }

        if (_task != null)
        {
            StopCoroutine(_task);
            _task = null;
        }

        if (_cardUI != null)
        {
            Destroy(_cardUI.gameObject);
            _cardUI = null;
        }

        _task = StartCoroutine(OnShowEFX());
    }

    public void GoToMenu()
    {
        PopupUIManager.Instance.Show_MidSoftLoad(true);

        GameHelper.LoadSceneAsync(
              GameHelper.NavigatorSceneName
            , LoadSceneMode.Single
            , delegate () { PopupUIManager.Instance.Show_MidSoftLoad(false); }
            , null
        );
    }

    private IEnumerator OnShowEFX()
    {
        bool isFinish = false;

        // create token
        _cardUI = CreateCardToken(HandPosition.transform.position);
        Vector3 slotPosition = SlotPosition.transform.position;

        // Play spawn animation


        // Spawn EFX
        {
            isFinish = false;
            PlaySpawnEFX(_spawnEFXKey, _cardUI, slotPosition, () => { isFinish = true; });
            yield return new WaitUntil(() => isFinish == true);
        }

        yield return new WaitForSeconds(2.0f);

        // Attack EFX
        {
            foreach (CardDirectionType direction in System.Enum.GetValues(typeof(CardDirectionType)))
            {
                yield return IEPlayAttackEFX(_attackEFXKey, _cardUI, direction, slotPosition);
            }
        }

        yield return new WaitForSeconds(2.0f);

        // Death EFX
        {
            isFinish = false;
            _cardUI.SetTriggerDiscard();
            PlayDeathEFX(_deathEFXKey, _cardUI, slotPosition, () => { isFinish = true; });
            yield return new WaitUntil(() => isFinish == true);
        }

        Destroy(_cardUI.gameObject);
        _cardUI = null;
    }

    public CardUI CreateCardToken(Vector3 position)
    {
        GameObject objCard = Instantiate(Prefab_Card);
        objCard.transform.position = position;

        CardUI card = objCard.GetComponent<CardUI>();

        card.IsBackCard = false;
        card.InitDummyCard(CardUI.CardMode.TOKEN, CardUI.UIMode.OBJECT);
        card.AnimationToToken();

        return card;
    }

    public void PlaySpawnAnimation(CardUI card)
    {
        //PlaySpawnEFX(_spawnEFXKey);
    }

    public void PlaySpawnEFX(string spawnVFXKey, CardUI card, Vector3 slotPosition, UnityAction onComplete)
    {
        UIEffectManager.Instance.SpawnSummonVFX(
              spawnVFXKey
            , card
            , slotPosition
            , delegate ()
            {
                // FAILSAFE: Reset card to correct rotation
                card.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 0));
            }
            , onComplete
            , true
        );
    }

    public void PlayAttackEFX(string vfxKey, CardUI card, CardDirectionType direction, Vector3 slotPosition, UnityAction onComplete)
    {
        Vector3 dir = Vector3.forward;
        switch (direction)
        {
            case CardDirectionType.N:   dir = new Vector3(0, 0, 1).normalized; break;
            case CardDirectionType.NE:  dir = new Vector3(1, 0, 1).normalized; break;
            case CardDirectionType.E:   dir = new Vector3(1, 0, 0).normalized; break;
            case CardDirectionType.SE:  dir = new Vector3(1, 0, -1).normalized; break;
            case CardDirectionType.S:   dir = new Vector3(0, 0, -1).normalized; break;
            case CardDirectionType.SW:  dir = new Vector3(-1, 0, -1).normalized; break;
            case CardDirectionType.W:   dir = new Vector3(-1, 0, 0).normalized; break;
            case CardDirectionType.NW:  dir = new Vector3(-1, 0, 1).normalized; break;
        }
        float distance = 2.0f;
        Vector3 attackOffset = card.transform.position + (dir * distance);

        UIEffectManager.Instance.SpawnAttackVFX(
              vfxKey
            , attackOffset
            , onComplete
            , dir
        );
    }

    private IEnumerator IEPlayAttackEFX(string vfxKey, CardUI card, CardDirectionType direction, Vector3 slotPosition)
    {
        bool isFinish = false;
        PlayAttackEFX(vfxKey, card, direction, slotPosition, () => { isFinish = true; });
        yield return new WaitUntil(() => isFinish == true);
    }
    
    public void PlayDeathEFX(string vfxKey, CardUI card, Vector3 slotPosition, UnityAction onComplete)
    {
        UIEffectManager.Instance.SpawnDeathVFX(
              vfxKey
            , card
            , slotPosition
            , onComplete
        );
    }
}
