﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugHideInProduction : MonoBehaviour
{
    private void Start()
    {
        gameObject.SetActive(GameHelper.IsATNVersion_RealDevelopment);
    }

}
