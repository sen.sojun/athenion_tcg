﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Karamucho.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class SoulAttackDebug : MonoBehaviour
{
    #region Inspector
    [Header("Controller")]
    [SerializeField] private LightingController LightingController;

    [Header("Input")]
    [SerializeField] private TMP_InputField INPUT_Start;
    [SerializeField] private TMP_InputField INPUT_Projectile;
    [SerializeField] private TMP_InputField INPUT_End;
    [SerializeField] private TMP_InputField INPUT_Damage;
    [SerializeField] private Toggle INPUT_ToggleIsLocal;

    [Header("Target")]
    [SerializeField] private Transform TARGET_Local;
    [SerializeField] private Transform TARGET_Oppo;

    [Header("UI")]
    [SerializeField] private Button BTN_Fire;
    [SerializeField] private Button BTN_Magic;
    [SerializeField] private Button BTN_SoulOption;
    [SerializeField] private GameObject Panel;
    #endregion

    private void Start()
    {
        BTN_Fire.onClick.RemoveAllListeners();
        BTN_Fire.onClick.AddListener(ActionSoulAttack);

        BTN_Magic.onClick.RemoveAllListeners();
        BTN_Magic.onClick.AddListener(ActionUnitAttack);

        BTN_SoulOption.onClick.RemoveAllListeners();
        BTN_SoulOption.onClick.AddListener(delegate
        {
            Panel.SetActive(!Panel.activeSelf);
        });
    }

    #region Methods
    public void ActionSoulAttack()
    {
        string startKey = INPUT_Start.text;
        string projectileKey = INPUT_Projectile.text;
        string endKey = INPUT_End.text;

        Transform target, attacker;
        if (INPUT_ToggleIsLocal.isOn)
        {
            target = TARGET_Local;
            attacker = TARGET_Oppo;
        }
        else
        {
            target = TARGET_Oppo;
            attacker = TARGET_Local;
        }

        #region Action VFX
        UnityAction actionOnImpact = null;
        UnityAction actionOnProjectile = null;
        // Set onImpact
        actionOnImpact = delegate
        {
            UIEffectManager.Instance.HeroImpactEffect(endKey, target.position, null);
        };

        // Set Projectile
        actionOnProjectile = delegate
        {
            UIEffectManager.Instance.HeroProjectileEffect(projectileKey, attacker.position, target.position, actionOnImpact, 0.5f);
        };

        // Casting
        UIEffectManager.Instance.HeroImpactEffect(startKey, attacker.position, actionOnProjectile);
        #endregion
    }

    public void ActionUnitAttack()
    {
        string startKey = INPUT_Start.text;
        string projectileKey = INPUT_Projectile.text;
        string endKey = INPUT_End.text;

        Transform target, attacker;
        if (INPUT_ToggleIsLocal.isOn)
        {
            target = TARGET_Local;
            attacker = TARGET_Oppo;
        }
        else
        {
            target = TARGET_Oppo;
            attacker = TARGET_Local;
        }

        #region Action VFX
        UnityAction actionOnImpact = null;
        UnityAction actionOnProjectile = null;
        // Set onImpact
        actionOnImpact = delegate
        {
            UIEffectManager.Instance.AbilityGroupEffect(endKey, target.position, null);
        };

        // Set Projectile
        actionOnProjectile = delegate
        {
            UIEffectManager.Instance.AbilityEffect(projectileKey, attacker.position, target.position, actionOnImpact, 0.5f);
        };

        // Casting
        UIEffectManager.Instance.AbilityGroupEffect(startKey, attacker.position, actionOnProjectile);
        #endregion
    }

    public void GoToMenu()
    {
        PopupUIManager.Instance.Show_MidSoftLoad(true);

        GameHelper.LoadSceneAsync(
              GameHelper.NavigatorSceneName
            , LoadSceneMode.Single
            , delegate () { PopupUIManager.Instance.Show_MidSoftLoad(false); }
            , null
        );
    }
    #endregion

    #region Lighting

    public void FadeBlackIn()
    {
        LightingController.AdjustSunLightIntensity(0);
    }

    public void FadeBlackOut()
    {
        LightingController.AdjustSunLightIntensity();
    }

    public void SpotLight_ShowHeroTop(bool isShow, UnityAction onComplete = null)
    {
        LightingController.ShowTopSpotLight(isShow, onComplete);
    }

    public void SpotLight_ShowHeroBot(bool isShow, UnityAction onComplete = null)
    {
        LightingController.ShowBotSpotLight(isShow, onComplete);
    }

    public void SpotLight_ShowMid(bool isShow, UnityAction onComplete = null)
    {
        LightingController.ShowMidSpotLight(isShow, onComplete);
    }
    #endregion

}
