﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class PopupAttachment_Detail : MonoBehaviour
{
    #region Enum
    public enum PopupType
    {
        Normal
        , Warning
        , FullDetail
    }
    #endregion
    public PopupType Type = PopupType.Normal;
    public Button BTN_Main;

    [Header("Localize Key")]
    public string TitleKey = "";
    [TextArea] public string MessageKey = "";

    private void OnEnable()
    {
        if (BTN_Main != null)
        {
            BTN_Main.onClick.RemoveAllListeners();
            BTN_Main.onClick.AddListener(ShowPopup);
        }
    }

    private void OnDisable()
    {
        if (BTN_Main != null)
        {
            BTN_Main.onClick.RemoveAllListeners();
        }
    }

    private void ShowPopup()
    {
        string head = LocalizationManager.Instance.GetText(TitleKey);
        string message = LocalizationManager.Instance.GetText(MessageKey);

        switch (Type)
        {
            case PopupType.Normal:
            {
                PopupUIManager.Instance.ShowPopup_ConfirmOnly(head, message);
                break;
            }
            case PopupType.Warning:
            {
                PopupUIManager.Instance.ShowPopup_ConfirmOnlyWithWarning(head, message);
                break;
            }
            case PopupType.FullDetail:
            {
                PopupUIManager.Instance.ShowPopup_DetailWindow(head, message);
                break;
            }
        }
    }
}
