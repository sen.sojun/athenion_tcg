﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UITransitionData : MonoBehaviour
{
    public Vector2 OriginalPosition { get; private set; }

    public void Init()
    {
        RectTransform rectTransform = gameObject.GetComponent<RectTransform>();
        OriginalPosition = new Vector2(rectTransform.anchoredPosition.x, rectTransform.anchoredPosition.y);
    }
}
