﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenNotchOffset : MonoBehaviour
{
    private const float OFFSET_TOP = 68.0f;
    private const float OFFSET_BOT = 0.0f;

    private void OnEnable()
    {
        RectTransform rect = GetComponent<RectTransform>();
        if (GameHelper.IsNotchScreen())
        {
            rect.RectTransformSetTop(OFFSET_TOP);
            rect.RectTransformSetBottom(OFFSET_BOT);
        }
        else
        {
            rect.RectTransformSetTop(0.0f);
            rect.RectTransformSetBottom(0.0f);
        }

    }
}
