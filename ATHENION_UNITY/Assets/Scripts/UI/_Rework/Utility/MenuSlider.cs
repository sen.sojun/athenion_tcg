﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using DG.Tweening;

public class MenuSlider : MonoBehaviour
{
    #region Inspector Properties
    [Header("UI")]
    [SerializeField] private Transform IMG_Highlight;
    [SerializeField] private List<Button> ButtonList = new List<Button>();
    #endregion

    #region Animation Option
    [SerializeField] private const float MOVE_DURATION = 0.5f;
    [SerializeField] private const Ease EASE_ANIMATION = Ease.OutCirc;
    #endregion

    #region Private Properties
    private int _currentStepIndex = 0;
    private UnityAction<int> OnClickIndex = null;
    #endregion

    private void Awake()
    {
        InitUI();
    }

    /*
    private void Start()
    {
    }
    */

    #region Methods
    public void InitUI()
    {
        if (ButtonList != null)
        {
            for (int index = 0; index < ButtonList.Count; ++index)
            {
                int buttonIndex = index;
                ButtonList[buttonIndex].onClick.RemoveAllListeners();
                ButtonList[buttonIndex].onClick.AddListener(
                    delegate ()
                    {
                        SetIndex(buttonIndex);
                    }
                );
            }
        }
    }

    public void SetIndex(int index)
    {
        _currentStepIndex = index;
        OnTriggerClickIndex(_currentStepIndex);
    }

    public void SetOnClickIndexCallback(UnityAction<int> callback)
    {
        OnClickIndex = callback;
    }

    public void SetInteractable(int index, bool isInteractable)
    {
        if (ButtonList != null && ButtonList.Count > index)
        {
            ButtonList[index].interactable = isInteractable;
        }
    }

    public void MoveHighlightIndex(int index)
    {
        _currentStepIndex = index;
        Vector3 target = ButtonList[_currentStepIndex].transform.localPosition;
        IMG_Highlight.DOLocalMove(target, MOVE_DURATION).SetEase(EASE_ANIMATION);
    }
    #endregion

    #region Events
    private void OnTriggerClickIndex(int index)
    {
        Debug.LogFormat("OnTriggerClickIndex: {0}", index);

        OnClickIndex?.Invoke(index);
    }
    #endregion

}
