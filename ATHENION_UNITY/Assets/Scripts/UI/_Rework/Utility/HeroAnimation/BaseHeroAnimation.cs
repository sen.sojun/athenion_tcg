﻿using Spine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class BaseHeroAnimation : MonoBehaviour
{
    #region enum
    public enum HeroAnimationState
    {
        IDLE

        , GREETING
        , COMPLIMENT
        , MISTAKE
        , SORRY
        , THANK
        , THREATEN
        , HIT
            
        , BEGIN
        , WIN
        , LOSE
    }
    #endregion

    [System.Serializable]
    public class EventEffectData
    {
        [Tooltip("Example: Vfx_Threaten_01, Vfx_Threaten_02, etc.")]
        public string EventKey;
        [Tooltip("Object of Event")]
        public ParticleSystem VFX_Object;
    }

    #region Inspector
    [SerializeField] private EventEffectData[] _VFXData;
    #endregion

    #region Private
    // Animation Key
    protected const string _idle = "idle";
    protected const string _greeting = "greeting";
    protected const string _compliment = "compliment";
    protected const string _mistake = "mistake";
    protected const string _sorry = "sorry";
    protected const string _thank = "thank";
    protected const string _threaten = "threaten";
    protected const string _hit = "hit";
    protected const string _begin = "begin";
    protected const string _win = "win";
    protected const string _lose = "lose";

    // Animation Voice
    protected const string _event_idle = "";
    protected const string _event_greeting = "Voice_Greeting";
    protected const string _event_compliment = "Voice_Compliment";
    protected const string _event_mistake = "Voice_Mistake";
    protected const string _event_sorry = "Voice_Sorry";
    protected const string _event_thank = "Voice_Thank";
    protected const string _event_threaten = "Voice_Threaten";
    protected const string _event_hit = "Voice_Hit";
    protected const string _event_begin = "Voice_Begin";
    protected const string _event_win = "Voice_Win";
    protected const string _event_lose = "Voice_Lose";

    // parameter
    protected UnityAction _onComplete;
    protected string _heroID;
    protected bool _isPlaying = false;
    protected bool _isEventEnable = false;

    protected SoundHeroData _soundHeroData = null;
    #endregion

    #region Virtual Methods
    public virtual void SetHeroID(string heroID)
    {
        _heroID = heroID;
    }

    public virtual void PlayAnimation(SoundHeroData soundHeroData, HeroAnimationState state, UnityAction onComplete = null)
    {
        _soundHeroData = soundHeroData;
        _onComplete = onComplete;

        // Check state
        string stateKey = GetAnimationStateKey(state);

        // Check Animation FailSafe
        if (CheckAnimationState(stateKey))
        {
            // Add Event
            _isPlaying = true;
            AddEvent(stateKey);
        }

        // Check Sound FailSafe
        if (CheckAnimationEvent(GetEventAnimationStateKey(state)) == false)
        {
            _isPlaying = true;
            PlayHeroVoice(delegate
            {
                _isPlaying = false;
                _onComplete?.Invoke();
            });
        }
    }
    #endregion

    #region Abstract Methods
    protected abstract void AddEvent(string stateKey);

    protected abstract void DeInitEvent();

    protected abstract void AnimationState_Complete(Spine.TrackEntry trackEntry);

    #region Check FailSafe

    protected abstract bool CheckAnimationEvent(string eventName);

    protected abstract bool CheckAnimationState(string eventName);
    #endregion

    #endregion

    #region Methods
    protected void PlayHeroVoice(UnityAction onComplete = null)
    {
        if(_soundHeroData == null)
        {
            onComplete?.Invoke();
            return;
        }

        // play hero sound.
        SoundManager.PlayHeroVoice(_soundHeroData, onComplete);
    }

    protected void EventHandle(TrackEntry trackEntry, Spine.Event e)
    {
        // Voice Event
        TriggerEvent(_event_idle, e, null);
        TriggerEvent(_event_greeting, e, PlayHeroVoice);
        TriggerEvent(_event_compliment, e, PlayHeroVoice);
        TriggerEvent(_event_mistake, e, PlayHeroVoice);
        TriggerEvent(_event_sorry, e, PlayHeroVoice);
        TriggerEvent(_event_thank, e, PlayHeroVoice);
        TriggerEvent(_event_threaten, e, PlayHeroVoice);
        TriggerEvent(_event_begin, e, PlayHeroVoice);
        TriggerEvent(_event_hit, e, null);
        TriggerEvent(_event_win, e, null);
        TriggerEvent(_event_lose, e, PlayHeroVoice);

        // VFX Event
        foreach (EventEffectData item in _VFXData)
        {
            TriggerEvent(item.EventKey, e, delegate
            {
                if (item.VFX_Object != null) ShowVFX(item.VFX_Object.gameObject);
            });
        }
        //TriggerEvent(_event_vfx_1, e, delegate 
        //{
        //    if (_VFX_1 != null) ShowVFX(_VFX_1);       
        //});
        //TriggerEvent(_event_vfx_2, e, delegate
        //{
        //    if (_VFX_2 != null) ShowVFX(_VFX_2);
        //});
    }

    protected void TriggerEvent(string eventName, Spine.Event e, UnityAction<UnityAction> callback)
    {
        if (e.Data.Name == eventName)
        {
            callback?.Invoke(null);
        }
    }

    protected void ShowVFX(GameObject vfx)
    {
        vfx?.SetActive(true);
    }
    #endregion

    #region Get Methods
    public bool GetIsPlaying()
    {
        return _isPlaying;
    }

    protected string GetAnimationStateKey(HeroAnimationState state)
    {
        // Check state
        string stateKey = _idle;
        switch (state)
        {
            case HeroAnimationState.IDLE:
            {
                stateKey = _idle;
                break;
            }
            case HeroAnimationState.GREETING:
            {
                stateKey = _greeting;
                break;
            }
            case HeroAnimationState.COMPLIMENT:
            {
                stateKey = _compliment;
                break;
            }
            case HeroAnimationState.MISTAKE:
            {
                stateKey = _mistake;
                break;
            }
            case HeroAnimationState.SORRY:
            {
                stateKey = _sorry;
                break;
            }
            case HeroAnimationState.THANK:
            {
                stateKey = _thank;
                break;
            }
            case HeroAnimationState.THREATEN:
            {
                stateKey = _threaten;
                break;
            }
            case HeroAnimationState.HIT:
            {
                stateKey = _hit;
                break;
            }
            case HeroAnimationState.BEGIN:
            {
                stateKey = _begin;
                break;
            }
            case HeroAnimationState.WIN:
            {
                stateKey = _win;
                break;
            }
            case HeroAnimationState.LOSE:
            {
                stateKey = _lose;
                break;
            }
        }

        return stateKey;
    }

    public static string GetEventAnimationStateKey(HeroAnimationState state)
    {
        // Check state
        switch (state)
        {
            case HeroAnimationState.IDLE:
            {
                return _event_idle;
            }
            case HeroAnimationState.GREETING:
            {
                return _event_greeting;
            }
            case HeroAnimationState.COMPLIMENT:
            {
                return _event_compliment;
            }
            case HeroAnimationState.MISTAKE:
            {
                return _event_mistake;
            }
            case HeroAnimationState.SORRY:
            {
                return _event_sorry;
            }
            case HeroAnimationState.THANK:
            {
                return _event_thank;
            }
            case HeroAnimationState.THREATEN:
            {
                return _event_threaten;
            }
            case HeroAnimationState.HIT:
            {
                return _event_hit;
            }
            case HeroAnimationState.BEGIN:
            {
                return _event_begin;
            }
            case HeroAnimationState.WIN:
            {
                return _event_win;
            }
            case HeroAnimationState.LOSE:
            {
                return _event_lose;
            }
            default:
            {
                return "";
            }
        }
    }

    public static HeroVoice GetVoiceIndexFromAnimationState(HeroAnimationState state)
    {
        // Check state
        switch (state)
        {
            case HeroAnimationState.IDLE:
            {
                return HeroVoice.None;
            }
            case HeroAnimationState.GREETING:
            {
                return HeroVoice.Greeting;
            }
            case HeroAnimationState.COMPLIMENT:
            {
                return HeroVoice.GoodMove;
            }
            case HeroAnimationState.MISTAKE:
            {
                return HeroVoice.Mistake;
            }
            case HeroAnimationState.SORRY:
            {
                return HeroVoice.Sorry;
            }
            case HeroAnimationState.THANK:
            {
                return HeroVoice.Thank;
            }
            case HeroAnimationState.THREATEN:
            {
                return HeroVoice.Threaten;
            }
            case HeroAnimationState.HIT:
            {
                return HeroVoice.None;
            }
            case HeroAnimationState.BEGIN:
            {
                return HeroVoice.Begin;
            }
            case HeroAnimationState.WIN:
            {
                return HeroVoice.None;
            }
            case HeroAnimationState.LOSE:
            {
                return HeroVoice.Lose;
            }
            default:
            {
                return HeroVoice.None;
            }
        }
    }

    public static HeroAnimationState GetStateFromVoiceIndex(HeroVoice voiceState)
    {
        // Check state
        switch (voiceState)
        {
            case HeroVoice.None: return HeroAnimationState.IDLE;
            case HeroVoice.Begin: return HeroAnimationState.BEGIN;
            case HeroVoice.Lose: return HeroAnimationState.LOSE;
            case HeroVoice.Greeting: return HeroAnimationState.GREETING;
            case HeroVoice.Threaten: return HeroAnimationState.THREATEN;
            case HeroVoice.GoodMove: return HeroAnimationState.COMPLIMENT;
            case HeroVoice.Mistake: return HeroAnimationState.MISTAKE;
            case HeroVoice.Thank: return HeroAnimationState.THANK;
            case HeroVoice.Sorry: return HeroAnimationState.SORRY;
            default: return HeroAnimationState.IDLE;
        }
    }
    #endregion


}
