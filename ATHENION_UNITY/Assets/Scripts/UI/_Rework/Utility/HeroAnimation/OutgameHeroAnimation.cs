﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Spine.Unity;
using Spine;

public class OutgameHeroAnimation : BaseHeroAnimation
{
    #region Inspector
    [Header("Skeleton")]
    [SerializeField] private SkeletonGraphic _HeroSkeleton;
    #endregion

    #region Private Propereties
    #endregion

    public override void PlayAnimation(SoundHeroData soundHeroData, HeroAnimationState state, UnityAction onComplete = null)
    {
        // Check if assign skeleton animation !
        if (_HeroSkeleton == null)
        {
            Debug.LogWarning("No SkeletonAnimation:" + gameObject.name);
            return;
        }

        base.PlayAnimation(soundHeroData, state, onComplete);

    }

    protected override void AddEvent(string stateKey)
    {
        _HeroSkeleton.AnimationState.SetAnimation(0, stateKey, false);
        _HeroSkeleton.AnimationState.Complete += AnimationState_Complete;
    }

    protected override void AnimationState_Complete(TrackEntry trackEntry)
    {
        _HeroSkeleton.AnimationState.SetAnimation(0, _idle, true);
        _HeroSkeleton.AnimationState.Complete -= AnimationState_Complete;
        _isPlaying = false;
        _onComplete?.Invoke();

    }

    public override void SetHeroID(string heroID)
    {
        base.SetHeroID(heroID);
        if (_HeroSkeleton != null)
        {
            DataManager.Instance.StartCoroutine(WaitForSetup());
            //_HeroSkeleton.AnimationState.Event += EventHandle;
        }

    }

    IEnumerator WaitForSetup()
    {
        while (_HeroSkeleton.AnimationState == null)
        {
            yield return null;
        }
        _HeroSkeleton.AnimationState.Event += EventHandle;
    }

    protected override void DeInitEvent()
    {
        _HeroSkeleton.AnimationState.Event -= EventHandle;
    }

    protected override bool CheckAnimationEvent(string eventName)
    {
        Spine.EventData eventData = _HeroSkeleton.SkeletonDataAsset.GetAnimationStateData().SkeletonData.FindEvent(eventName);
        if (eventData != null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    protected override bool CheckAnimationState(string animationName)
    {
        Spine.Animation animationData = _HeroSkeleton.AnimationState.Data.SkeletonData.FindAnimation(animationName);
        if (animationData != null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}
