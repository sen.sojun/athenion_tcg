﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Karamucho;

public class PlayboardController : MonoBehaviour
{
    #region Inspector Properties
    [SerializeField] private SpriteRenderer _Playboard;

    #endregion

    #region Private Properties
    private const string _playBoardRootPath = "Images/Playboards/";
    private const string _playBoardPrefix = "BOARD_";
    #endregion

    #region Methods
    public void Setup(string boardID)
    {
        string boardKey = (_playBoardPrefix + boardID).ToUpper();
        Sprite sprite = ResourceManager.Load<Sprite>(_playBoardRootPath + boardKey);

        if (sprite != null)
        {
            _Playboard.sprite = sprite;
        }
        else
        {
            Debug.LogWarning("Cannot find :" + boardKey + "| Use default board.");

            boardKey = _playBoardPrefix + "DEFAULT";
            Sprite defaultSprite = ResourceManager.Load<Sprite>(_playBoardRootPath + boardKey);
            _Playboard.sprite = defaultSprite;
        }

    }
    #endregion
}
