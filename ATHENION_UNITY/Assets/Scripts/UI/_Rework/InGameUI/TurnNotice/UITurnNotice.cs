﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class UITurnNotice : MonoBehaviour
{
    #region Inspector
    [Header("Animator")]
    [SerializeField] private Animator _TurnAnimatorController;
    #endregion

    #region Private Properties
    private const string _KEY_ISLOCAL = "Player";
    private const string _KEY_ISENEMY = "Enemy";

    private UnityAction _onComplete;
    #endregion

    #region Methods
    public void Show(bool isLocal, UnityAction onComplete)
    {
        _onComplete = onComplete;

        if (_TurnAnimatorController != null)
        {
            _TurnAnimatorController.SetTrigger(isLocal ? _KEY_ISLOCAL : _KEY_ISENEMY);
        }
        else
        {
            Debug.LogWarning("No Animator Controller in UITurn, Plz fix !");
            OnAnimationComplete();
        }

    }

    public void OnAnimationComplete()
    {
        if (_onComplete != null)
        {
            _onComplete.Invoke();
        }
    }
    #endregion
}
