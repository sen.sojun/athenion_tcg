﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class FloatTextObject : MonoBehaviour
{
    #region Inspector
    [SerializeField] private TextMeshProUGUI TEXT_Message;
    #endregion

    #region Private 
    #endregion

    #region Methods
    public void CreateFloatingAP(string message, Vector2 startPosition, float distance, float duration, bool isMoveUp = true)
    {
        TEXT_Message.color = GameHelper.GetColorByHtmlCode("#09E4CDFF");
        CreateFloating(message, startPosition, distance, duration, isMoveUp);
    }

    public void CreateFloatingSoul(string message, Vector2 startPosition, float distance, float duration, bool isMoveUp = true)
    {
        TEXT_Message.color = GameHelper.GetColorByHtmlCode("#FFE36DFF");
        CreateFloating(message, startPosition, distance, duration, isMoveUp);
    }

    public void CreateFloating(Color color, string message, Vector2 startPosition, float distance, float duration, bool isMoveUp = true)
    {
        TEXT_Message.color = color;
        CreateFloating(message, startPosition, distance, duration, isMoveUp);
    }

    public void CreateFloatingDefault(string message, Vector2 startPosition, float distance, float duration, bool isMoveUp = true)
    {
        CreateFloating(message, startPosition, distance, duration, isMoveUp);
    }

    private void CreateFloating(string message, Vector2 startPosition, float distance, float duration, bool isMoveUp = true)
    {
        TEXT_Message.text = message;

        if (!isMoveUp)
        {
            distance *= -1; // convert to negative
        }

        GameHelper.UITransition_ZoomFadeIn(gameObject);
        gameObject.transform.position = startPosition;
        float targetDistanceY = gameObject.GetComponent<RectTransform>().anchoredPosition.y + distance;
        gameObject.GetComponent<RectTransform>().DOAnchorPosY(targetDistanceY, duration).SetEase(Ease.OutExpo).OnComplete(delegate
        {
            GameHelper.UITransition_ZoomFadeOut(gameObject);
        });
    }
    #endregion


}
