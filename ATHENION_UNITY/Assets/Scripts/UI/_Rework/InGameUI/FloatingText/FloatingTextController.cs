﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingTextController : MonoBehaviour
{
    #region Public
    public float Distance = 100;
    #endregion

    #region Private
    [SerializeField] private FloatTextObject _prefab;
    [Header("Custom")]
    [SerializeField] private FloatTextObject _prefabAP;
    [SerializeField] private FloatTextObject _prefabSOUL;
    [SerializeField] private FloatTextObject _prefabHP;



    private List<FloatTextObject> _floatPool = new List<FloatTextObject>();
    #endregion

    #region Methods
    private void Update()
    {
        //if (Input.GetKeyDown(KeyCode.F))
        //{
        //    Vector2 targetPosition = Camera.main.WorldToScreenPoint(UIManager.Instance.UIPlayerBottom.GetAPPosition());
        //    AddFloatTextAP("+99", targetPosition, 2.0f);

        //    Vector2 targetPosition1 = Camera.main.WorldToScreenPoint(UIManager.Instance.UIPlayerBottom.GetSoulPosition());
        //    AddFloatTextAP("+99", targetPosition1, 2.0f);
        //}
    }

    public void AddFloatTextSoul(string message, Vector2 startPosition, float duration, bool isMoveUp = true)
    {
        foreach (FloatTextObject item in _floatPool)
        {
            if (!item.gameObject.activeSelf)
            {
                item.CreateFloatingDefault(message, startPosition, Distance, duration, isMoveUp);
                return;
            }
        }

        FloatTextObject cell = CreateCellSOUL();
        cell.CreateFloatingDefault(message, startPosition, Distance, duration, isMoveUp);
    }

    public void AddFloatTextAP(string message, Vector2 startPosition, float duration, bool isMoveUp = true)
    {
        foreach (FloatTextObject item in _floatPool)
        {
            if (!item.gameObject.activeSelf)
            {
                item.CreateFloatingDefault(message, startPosition, Distance, duration, isMoveUp);
                return;
            }
        }

        FloatTextObject cell = CreateCellAP();
        cell.CreateFloatingDefault(message, startPosition, Distance, duration, isMoveUp);

    }

    public void AddFloatTextCustom(Color color, string message, Vector2 startPosition, float duration, bool isMoveUp = true)
    {
        foreach (FloatTextObject item in _floatPool)
        {
            if (!item.gameObject.activeSelf)
            {
                item.CreateFloating(color, message, startPosition, Distance, duration, isMoveUp);
                return;
            }
        }

        FloatTextObject cell = CreateCell();
        cell.CreateFloating(color, message, startPosition, Distance, duration, isMoveUp);
    }

    #endregion

    #region Generator
    private FloatTextObject CreateCell()
    {
        GameObject obj = Instantiate(_prefab.gameObject, this.transform);
        FloatTextObject cell = obj.GetComponent<FloatTextObject>();
        obj.transform.localScale = Vector3.one;
        _floatPool.Add(cell);
        return cell;
    }

    private FloatTextObject CreateCellAP()
    {
        GameObject obj = Instantiate(_prefabAP.gameObject, this.transform);
        FloatTextObject cell = obj.GetComponent<FloatTextObject>();
        obj.transform.localScale = Vector3.one;
        _floatPool.Add(cell);
        return cell;
    }

    private FloatTextObject CreateCellSOUL()
    {
        GameObject obj = Instantiate(_prefabSOUL.gameObject, this.transform);
        FloatTextObject cell = obj.GetComponent<FloatTextObject>();
        obj.transform.localScale = Vector3.one;
        _floatPool.Add(cell);
        return cell;
    }

    private FloatTextObject CreateCellHP()
    {
        GameObject obj = Instantiate(_prefabHP.gameObject, this.transform);
        FloatTextObject cell = obj.GetComponent<FloatTextObject>();
        obj.transform.localScale = Vector3.one;
        _floatPool.Add(cell);
        return cell;
    }
    #endregion


}
