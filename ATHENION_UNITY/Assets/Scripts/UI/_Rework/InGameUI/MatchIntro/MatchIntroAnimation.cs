﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using Karamucho.UI;
using UnityEngine.Events;

public class MatchIntroAnimation : MonoBehaviour
{
    #region Inspector
    [Header("Animator")]
    [SerializeField] private Animator AnimtionController;

    [Header("Opponent")]
    [SerializeField] private TextMeshProUGUI TEXT_Oppo_Name;
    [SerializeField] private Image IMG_Oppo_Avatar;
    [SerializeField] private Image IMG_Oppo_Title;
    [SerializeField] private GameObject Hero_Oppo_AnimatedGroup;
    [SerializeField] private GameObject Hero_Oppo;
    [SerializeField] private GameObject Hero_Oppo_SpriteGroup;
    [SerializeField] private Image Hero_Oppo_Sprite;

    [Header("Player")]
    [SerializeField] private TextMeshProUGUI TEXT_Player_Name;
    [SerializeField] private Image IMG_Player_Avatar;
    [SerializeField] private Image IMG_Player_Title;
    [SerializeField] private GameObject Hero_Player_AnimatedGroup;
    [SerializeField] private GameObject Hero_Player;
    [SerializeField] private GameObject Hero_Player_SpriteGroup;
    [SerializeField] private Image Hero_Player_Sprite;

    [Header("Turn")]
    [SerializeField] private TextMeshProUGUI TEXT_TurnStart;
    #endregion

    #region Private
    private bool _isLocalFirst = false;
    private UnityAction _onComplete;
    #endregion

    #region Method
    public void InitAnimation(bool isLocalFirst, UnityAction onComplete)
    {
        gameObject.SetActive(true);
        _onComplete = onComplete;
        _isLocalFirst = isLocalFirst;

        SetFirstStartAnimation();
    }

    private void SetFirstStartAnimation()
    {
        AnimtionController.SetBool("First", _isLocalFirst);
        string first = LocalizationManager.Instance.GetText("BATTLE_PLAY_FIRST");
        string second = LocalizationManager.Instance.GetText("BATTLE_PLAY_SECOND");
        TEXT_TurnStart.text = _isLocalFirst ? first : second;
    }

    public void SetTopProfile(BattlePlayerData data)
    {
        TEXT_Oppo_Name.text = data.PlayerInfo.DisplayName;
        IMG_Oppo_Avatar.sprite = LoadAvatar(data.PlayerInfo.AvatarID);
        IMG_Oppo_Title.sprite = LoadTitle(data.PlayerInfo.TitleID);
        bool isAnimated = LoadHeroSpine(data.Hero.ID, Hero_Oppo.transform, Hero_Oppo_Sprite);
        Hero_Oppo_AnimatedGroup.gameObject.SetActive(isAnimated);
        Hero_Oppo_SpriteGroup.gameObject.SetActive(!isAnimated);
    }

    public void SetBotProfile(BattlePlayerData data)
    {
        TEXT_Player_Name.text = data.PlayerInfo.DisplayName;
        IMG_Player_Avatar.sprite = LoadAvatar(data.PlayerInfo.AvatarID);
        IMG_Player_Title.sprite = LoadTitle(data.PlayerInfo.TitleID);
        bool isAnimated = LoadHeroSpine(data.Hero.ID, Hero_Player.transform, Hero_Player_Sprite);
        Hero_Player_AnimatedGroup.gameObject.SetActive(isAnimated);
        Hero_Player_SpriteGroup.gameObject.SetActive(!isAnimated);
    }

    #endregion

    #region Profile Setup
    private Sprite LoadAvatar(string id)
    {
        Sprite sprite = SpriteResourceHelper.LoadSprite(id, SpriteResourceHelper.SpriteType.ICON_Avatar);
        return sprite;
    }

    private Sprite LoadTitle(string id)
    {
        Sprite sprite = SpriteResourceHelper.LoadSprite(id, SpriteResourceHelper.SpriteType.ICON_Title);
        return sprite;
    }

    /// <summary>
    /// Return isAnimated
    /// </summary>
    /// <param name="id"></param>
    /// <param name="parent"></param>
    /// <param name="image"></param>
    /// <returns></returns>
    private bool LoadHeroSpine(string id, Transform parent, Image image)
    {
        GameObject obj = null;
        if (CardResourceManager.LoadUIHero(id, out obj))
        {         
            Instantiate(obj, parent).transform.localPosition = Vector3.zero;
            return true;
        }
        else
        {
            image.sprite = SpriteResourceHelper.LoadSprite(id, SpriteResourceHelper.SpriteType.SKIN_Hero);
            return false;
        }

    }
    #endregion

    #region Events
    public void OnComplete()
    {
        _onComplete?.Invoke();
        gameObject.SetActive(false);

        // DeInit
        Destroy(gameObject);
        
    }
    #endregion


}
