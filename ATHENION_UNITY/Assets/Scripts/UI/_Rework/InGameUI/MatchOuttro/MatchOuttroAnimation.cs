﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using Karamucho.UI;
using UnityEngine.Events;

public class MatchOuttroAnimation : MonoBehaviour
{
    #region Inspector
    [Header("Animator")]
    [SerializeField] private Animator AnimtionController;

    [Header("Player")]
    [SerializeField] private GameObject Hero_Player_AnimatedGroup;
    [SerializeField] private GameObject Hero_Player;
    [SerializeField] private GameObject Hero_Player_SpriteGroup;
    [SerializeField] private Image Hero_Player_Sprite;

    [Header("Turn")]
    [SerializeField] private TextMeshProUGUI TEXT_Result;
    #endregion

    #region Private
    private bool _isVictory = false;
    private UnityAction _onComplete;
    #endregion

    #region Method
    public void InitAnimation(bool isVictory, UnityAction onComplete)
    {
        gameObject.SetActive(true);
        _onComplete = onComplete;
        _isVictory = isVictory;

        // Sound
        if (_isVictory)
            SoundManager.PlaySFX(SoundManager.SFX.Match_Victory);
        else
            SoundManager.PlaySFX(SoundManager.SFX.Match_Lose);


        SetVictory();
    }

    private void SetVictory()
    {
        string victoryKey = LocalizationManager.Instance.GetText("BATTLE_RESULT_VICTORY");
        string defeatKey = LocalizationManager.Instance.GetText("BATTLE_RESULT_DEFEAT");
        TEXT_Result.text = _isVictory ? victoryKey : defeatKey;
    }

    public void SetProfile(BattlePlayerData data)
    {
        bool isAnimated = LoadHeroSpine(data.Hero.ID, Hero_Player.transform, Hero_Player_Sprite);
        Hero_Player_AnimatedGroup.gameObject.SetActive(isAnimated);
        Hero_Player_SpriteGroup.gameObject.SetActive(!isAnimated);
    }

    #endregion

    #region Profile Setup
    private Sprite LoadAvatar(string id)
    {
        Sprite sprite = SpriteResourceHelper.LoadSprite(id, SpriteResourceHelper.SpriteType.ICON_Avatar);
        return sprite;
    }

    private Sprite LoadTitle(string id)
    {
        Sprite sprite = SpriteResourceHelper.LoadSprite(id, SpriteResourceHelper.SpriteType.ICON_Title);
        return sprite;
    }

    /// <summary>
    /// Return isAnimated
    /// </summary>
    /// <param name="id"></param>
    /// <param name="parent"></param>
    /// <param name="image"></param>
    /// <returns></returns>
    private bool LoadHeroSpine(string id, Transform parent, Image image)
    {
        GameObject obj = null;
        if (CardResourceManager.LoadUIHero(id, out obj))
        {
            Instantiate(obj, parent).transform.localPosition = Vector3.zero;
            return true;
        }
        else
        {
            image.sprite = SpriteResourceHelper.LoadSprite(id, SpriteResourceHelper.SpriteType.SKIN_Hero);
            return false;
        }

    }
    #endregion

    #region Events
    public void OnComplete()
    {
        _onComplete?.Invoke();

    }
    #endregion
}
