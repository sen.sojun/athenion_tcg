﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using DG.Tweening;

public class UIPopupRankingUpdate : MonoBehaviour
{
    #region Serialize Properties
    [SerializeField] private TextMeshProUGUI _TEXT_Title;

    [Header("Reward Cell")]
    [SerializeField] private RewardCell _Prefab_RewardCell;
    [SerializeField] private Transform _Container;
    [SerializeField] private TextMeshProUGUI _TEXT_NoReward;

    [Header("Previous Rank")]
    [SerializeField] private TextMeshProUGUI _PreviousRankingText;
    [SerializeField] private Image _PreviousRankImage;
    [SerializeField] private Image _PreviousAvatarImage;

    [Header("Current Rank")]
    [SerializeField] private TextMeshProUGUI _CurrentRankingText;
    [SerializeField] private Image _CurrentRankImage;
    [SerializeField] private Image _CurrentAvatarImage;

    [Header("BTN")]
    [SerializeField] private Button _BTN_Ok;
    [SerializeField] private Button _BTN_Skip;
    #endregion

    #region Public Properties
    #endregion

    #region Private Properties
    private int _previousRankIndex;
    private int _currentRankIndex;
    private string _previousRankID;
    private string _currentRankID;
    private Animator _animatorController;
    private bool _isSummary;

    private List<RewardCell> _rewardCellList = new List<RewardCell>();
    #endregion

    private void Start()
    {
        _animatorController = GetComponent<Animator>();
    }

    public void ShowUI(UnityAction onClickOK, int previousRankIndex, int currentRankIndex, List<ItemData> itemList)
    {
        _previousRankIndex = previousRankIndex;
        _currentRankIndex = currentRankIndex;

        _previousRankID = GameHelper.GetRankID(_previousRankIndex);
        _currentRankID = GameHelper.GetRankID(_currentRankIndex);
         
        SetRankUI(_previousRankID, _PreviousRankingText, _PreviousRankImage, _PreviousAvatarImage);
        SetRankUI(_currentRankID, _CurrentRankingText, _CurrentRankImage, _CurrentAvatarImage);

        _BTN_Ok.onClick.RemoveAllListeners();
        _BTN_Ok.onClick.AddListener(delegate
        {
            StopSound_Ambient();
            onClickOK?.Invoke();
        });

        _BTN_Skip.onClick.RemoveAllListeners();
        _BTN_Skip.onClick.AddListener(SkipAnimation);

        int endedSeason = DataManager.Instance.GetCurrentSeasonIndex() - 1;
        _TEXT_Title.text = string.Format(LocalizationManager.Instance.GetText("TEXT_SEASON_ENDED"), endedSeason);
        SetPreviousReward(itemList);

        GameHelper.UITransition_FadeIn(this.gameObject);
        PlaySound_Ambient();
    }

    private void SetRankUI(string rankID, TextMeshProUGUI rankText, Image rankImage, Image avatarImage)
    {
        string rankName = GameHelper.GetRankName(rankID);

        rankImage.sprite = SpriteResourceHelper.LoadSprite(rankID, SpriteResourceHelper.SpriteType.ICON_Rank);
        rankText.text = rankName;
        avatarImage.sprite = SpriteResourceHelper.LoadSprite(DataManager.Instance.PlayerInfo.AvatarID, SpriteResourceHelper.SpriteType.Avatar);
    }

    private void SetPreviousReward(List<ItemData> itemList)
    {
        //Reset
        ClearReward();

        // Create & add reward
        foreach (ItemData item in itemList)
        {
            _rewardCellList.Add(GenerateReward(item.ItemID, item.Amount));
        }

        // Show no reward?
        _TEXT_NoReward.gameObject.SetActive(_rewardCellList.Count == 0);
    }

    private void ClearReward()
    {
        foreach (RewardCell item in _rewardCellList)
        {
            Destroy(item.gameObject);
        }
        _rewardCellList.Clear();
    }

    private RewardCell GenerateReward(string imgKey, int count)
    {
        GameObject obj = Instantiate(_Prefab_RewardCell.gameObject, _Container);
        RewardCell cell = obj.GetComponent<RewardCell>();
        cell.InitData(imgKey, count);
        return cell;
    }

    public void HideUI()
    {
        if (_animatorController != null)
        {
            CloseAnimation();
        }
        else
        {
            GameHelper.UITransition_FadeOut(this.gameObject);
        }

    }

    public void ClosePopup()
    {
        gameObject.SetActive(false);
    }

    #region Animation
    private void RestartAnimation()
    {
        _animatorController.SetTrigger("Restart");
    }

    private void SkipAnimation()
    {
        if (!_isSummary)
        {
            _isSummary = true;
            _animatorController.SetTrigger("Skip");
        }

    }

    private void CloseAnimation()
    {
        _animatorController.SetTrigger("Close");
    }

    public void Flag_IsSummary()
    {
        _isSummary = true;
    }
    #endregion

    #region Sound
    public void PlaySound_Ambient()
    {
        SoundManager.PlaySFX(SoundManager.SFX.RankUpdate_Ambient, true);
    }

    public void StopSound_Ambient()
    {
        SoundManager.StopSFX(0, 0.5f);
    }

    public void PlaySound_Show()
    {
        SoundManager.PlaySFX(SoundManager.SFX.RankUpdate_Show);
    }
    #endregion



}
