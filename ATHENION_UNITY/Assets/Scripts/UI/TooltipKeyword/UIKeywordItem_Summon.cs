﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Karamucho.UI
{
    public class UIKeywordItem_Summon : UIKeywordItem
    {
        #region Public Properties
        [Header("Reference")]
        public TextMeshProUGUI Atk;
        public TextMeshProUGUI Hp;
        public TextMeshProUGUI Spirit;
        public RectTransform ArtGroup;
        public Image Frame;
        public Image AttackImage;
        public Image HpImage;
        public Image RarityImage;

        [Header("Passive Icon")]
        public Image PassiveIcon;

        [Header("Arrow Materials")]
        public Sprite ArrowBlue;
        public Sprite ArrowRed;

        [Header("Arrow")]
        public List<GameObject> ArrowDir = new List<GameObject>();
        #endregion

        #region Methods

        protected override void Init()
        {
            HeadText.text = LocalizationManager.Instance.GetText(Localize_Head);
        }

        public void SetToken(string key, CardUIData ownerCard)
        {
            CardData cardData = CardData.CreateCard(key);
            CardUIData data = new CardUIData(0, cardData, ownerCard.CardBackID);
            Color color = new Color();

            string abilityText;
            //LocalizationManager.Instance.GetText("INFO_KEYWORD_SUMMON_SUB", out formatText);
            abilityText = LocalizationManager.Instance.GetText(data.Description);
            if (string.IsNullOrEmpty(abilityText))
            {
                abilityText = LocalizationManager.Instance.GetText("TEXT_NO_ABILITY");
            }
            SubText.text = string.Format(abilityText, data.Name);

            SetAtk(data.ATKDefault, data);
            SetHp(data.HPDefault, data);
            SetSpirit(data.SpiritDefault);
            ShowDirection(data.CardDir);
            SetFrame(data);
            SetImage(data);
            ShowPassiveIcon(data);

            // Set color
            if (GameHelper.IsInBattleScene())
            {
                if (GameManager.Instance.IsLocalPlayer(ownerCard.Owner))
                {
                    color = GameHelper.GetColor_PlayerBlue();
                }
                else
                {
                    color = GameHelper.GetColor_PlayerRed();
                }
                SetColorPlayer(color, GameManager.Instance.IsLocalPlayer(ownerCard.Owner) ? ArrowBlue : ArrowRed);
            }
            else
            {
                color = GameHelper.GetColor_PlayerBlue();
                SetColorPlayer(color, ArrowBlue);
            }


            if (data.Type == CardType.Minion || data.Type == CardType.Minion_NotInDeck)
            {
                AttackImage.gameObject.SetActive(true);
                HpImage.gameObject.SetActive(true);
                Atk.gameObject.SetActive(true);
                Hp.gameObject.SetActive(true);
            }
            else if (data.Type == CardType.Spell)
            {
                AttackImage.gameObject.SetActive(false);
                HpImage.gameObject.SetActive(false);
                Atk.gameObject.SetActive(false);
                Hp.gameObject.SetActive(false);
                PassiveIcon.gameObject.SetActive(false);
            }
        }

        private void SetAtk(int value, CardUIData data)
        {
            Color color = new Color();
            if(value < data.ATKBase)
            {
                color = Color.red;
            }
            else
            {
                if (data.ATKBase > data.ATKDefault)
                {
                    color = Color.green;
                }
                else
                {
                    color = Color.white;
                }
            }

            Atk.text = value.ToString();
            Atk.color = color;
        }

        private void SetHp(int value, CardUIData data)
        {
            Color color = new Color();
            if (value < data.HPBase)
            {
                color = Color.red;
            }
            else
            {
                if (data.HPBase > data.HPDefault)
                {
                    color = Color.green;
                }
                else
                {
                    color = Color.white;
                }
            }

            Hp.text = value.ToString();
            Hp.color = color;
        }

        private void SetSpirit(int value)
        {
            Spirit.text = value.ToString();
            //Spirit.text = GameHelper.GetIntToRomanNumber(value).ToString();
        }

        private void SetColorPlayer(Color color, Sprite mat)
        {
            foreach (GameObject item in ArrowDir)
            {
                item.GetComponent<Image>().sprite = mat;
            }
        }

        private void SetFrame(CardUIData data)
        {
            if (data.Type == CardType.Minion || data.Type == CardType.Minion_NotInDeck || data.Type == CardType.Spell)
            {
                Sprite tempSprite;
                CardResourceManager.LoadTokenFrame(data.Rarity, out tempSprite);
                Frame.sprite = tempSprite;
            }
        }

        private void ShowDirection(CardDirection dirValue)
        {
            foreach (CardDirectionType item in System.Enum.GetValues(typeof(CardDirectionType)))
            {
                bool isShow = dirValue.IsDirection(item);
                ArrowDir[(int)item].SetActive(isShow);
            }
        }

        private void SetImage(CardUIData data)
        {
            // Clear previous image
            {
                foreach (Transform child in ArtGroup.transform)
                {
                    Destroy(child.gameObject);
                }
            }

            GameObject obj;
            if (CardResourceManager.Create(CardResourceManager.ImageType.InfoToken, data.ImageKey, ArtGroup.transform, false, out obj))
            {
                obj.transform.localPosition = Vector3.zero;
                obj.transform.localRotation = Quaternion.identity;
            }
        }

        public void ShowPassiveIcon(CardUIData cardUIData)
        {
            if (cardUIData.FeedbackList != null && cardUIData.FeedbackList.Count > 0)
            {
                PassiveIcon.gameObject.SetActive(true);
                foreach (AbilityFeedback icon in cardUIData.FeedbackList)
                {                  
                    switch (icon)
                    {
                        case AbilityFeedback.Untargetable:
                        case AbilityFeedback.Trigger:
                        case AbilityFeedback.Backstab:
                        case AbilityFeedback.Lastwish:
                        case AbilityFeedback.Link:
                        case AbilityFeedback.Berserk:
                        {
                            Sprite sprite = SpriteResourceHelper.LoadSprite(icon.ToString().ToUpper(), SpriteResourceHelper.SpriteType.ICON_Keyword);
                            PassiveIcon.sprite = sprite;
                            return;
                        }
                        default:
                        {
                            PassiveIcon.gameObject.SetActive(false);
                            return;
                        }
                    }
                }
            }
            else
            {
                PassiveIcon.gameObject.SetActive(false);
            }
        }
        #endregion
    }
}
