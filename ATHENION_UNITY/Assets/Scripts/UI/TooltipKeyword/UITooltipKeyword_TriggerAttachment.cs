﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UITooltipKeyword_TriggerAttachment : UIInputEvent
{
    [SerializeField] UITooltipKeywordGroup UITooltipKeywordGroup;
    bool _isHaveKeyword = false;
    bool _isShow = false;

    private void OnDisable()
    {
        HideTooltip();
    }

    #region Methods
    public void ShowTooltip()
    {
        _isShow = true;
        UITooltipKeywordGroup.ShowTooltip();
    }

    public void HideTooltip()
    {
        _isShow = false;
        UITooltipKeywordGroup.HideAllKeywordIcon();
    }

    public void SetKeyword(int value)
    {
        if (value != 0)
        {
            _isHaveKeyword = true;
            if (!gameObject.activeSelf)
            {
                gameObject.SetActive(true);
            }
        }
        else
        {
            _isHaveKeyword = false;
            if (gameObject.activeSelf)
            {
                gameObject.SetActive(false);
            }
        }
        
    }

    #endregion

    #region Events
    public override void OnEventClick(PointerEventData eventData)
    {
        base.OnEventClick(eventData);
        if (_isHaveKeyword)
        {
            if (!_isShow)
            {
                ShowTooltip();
            }
            else
            {
                HideTooltip();
            }

        }

    }

    public override void OnEventBeginHold(PointerEventData eventData)
    {
        base.OnEventBeginHold(eventData);
        //Debug.Log("Hold");
        //if(_isHaveKeyword) ShowTooltip();
    }

    public override void OnEventEndHold(PointerEventData eventData)
    {
        base.OnEventEndHold(eventData);
        //Debug.Log("StopHold");
        //HideTooltip();
    }
    #endregion

}
