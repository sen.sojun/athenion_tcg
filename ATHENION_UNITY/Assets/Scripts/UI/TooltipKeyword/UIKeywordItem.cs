﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;
using UnityEngine.UI;

public class UIKeywordItem : MonoBehaviour {

    public Image Panel;
    public TextMeshProUGUI HeadText;
    public TextMeshProUGUI SubText;
    [Header("Localize")]
    public string Localize_Head;
    public string Localize_Sub;

    #region Methods
    private void OnEnable()
    {
        Init();
    }

    protected virtual void Init()
    {
        HeadText.text = LocalizationManager.Instance.GetText(Localize_Head);
        SubText.text = LocalizationManager.Instance.GetText(Localize_Sub);
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }
    #endregion
}
