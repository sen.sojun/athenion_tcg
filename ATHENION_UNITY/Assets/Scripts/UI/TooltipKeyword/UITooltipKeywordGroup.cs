﻿using Karamucho.UI;
using UnityEngine;

public class UITooltipKeywordGroup : MonoBehaviour {

    #region Public Properties

    [Header("Ability Keyword")]
    public UIKeywordItem Trigger;
    public UIKeywordItem Awaken;
    public UIKeywordItem Stealth;
    public UIKeywordItem Sentinel;
    public UIKeywordItem Rally;
    public UIKeywordItem Lastwish;
    public UIKeywordItem Backstab;
    public UIKeywordItem Absorb;
    public UIKeywordItem Taunt;
    public UIKeywordItem Freeze;
    public UIKeywordItem Silence;
    public UIKeywordItem Lock;
    public UIKeywordItem Move;
    public UIKeywordItem Fear;
    public UIKeywordItem Link;
    public UIKeywordItem Draw;
    public UIKeywordItem Aura;
    public UIKeywordItem Berserk;
    public UIKeywordItem_Summon Summon;
    public UIKeywordItem_Summon Summon2;
    public UIKeywordItem DarkPower;
    public UIKeywordItem DarkMatter;
    public UIKeywordItem Reset;
    public UIKeywordItem Untargetable;
    public UIKeywordItem Chain;
    public UIKeywordItem Piercing;

    [Header("Status Keyword")]
    public UIKeywordItem Linked;
    public UIKeywordItem Silenced;
    public UIKeywordItem Frozen;
    public UIKeywordItem Feared;
    public UIKeywordItem Taunted;
    public UIKeywordItem DarkPowered;
    public UIKeywordItem Invincibled;
    public UIKeywordItem Untargetabled;
    public UIKeywordItem BePiercing;
    #endregion

    private CardUIData _data;

    #region Method
    public void SetData(CardUIData data)
    {
        _data = data;
    }

    public void ShowTooltip()
    {
        GameHelper.UITransition_FadeIn(this.gameObject);
        HideAllKeywordIcon();

        if (_data == null)
        {
            Debug.LogWarning("tooltip keyword error:cardUI data is missing");
            return;
        }

        // Ability
        if (_data.IsAbilityKeyword(AbilityKeyword.Trigger))
        {
            //Trigger.Show();
        }
        if (_data.IsAbilityKeyword(AbilityKeyword.Awaken))
        {
            Awaken.Show();
        }
        if (_data.IsAbilityKeyword(AbilityKeyword.Stealth))
        {
            Stealth.Show();
        }
        if (_data.IsAbilityKeyword(AbilityKeyword.Lastwish))
        {
            Lastwish.Show();
        }
        if (_data.IsAbilityKeyword(AbilityKeyword.Backstab))
        {
            Backstab.Show();
        }
        if (_data.IsAbilityKeyword(AbilityKeyword.Absorb))
        {
            Absorb.Show();
        }
        if (_data.IsAbilityKeyword(AbilityKeyword.Taunt))
        {
            Taunt.Show();
        }
        if (_data.IsAbilityKeyword(AbilityKeyword.Silence))
        {
            Silence.Show();
        }
        if (_data.IsAbilityKeyword(AbilityKeyword.Freeze))
        {
            Freeze.Show();
        }
        if (_data.IsAbilityKeyword(AbilityKeyword.Fear))
        {
            Fear.Show();
        }
        if (_data.IsAbilityKeyword(AbilityKeyword.Link))
        {
            Link.Show();
        }
        if (_data.IsAbilityKeyword(AbilityKeyword.Lock))
        {
            Lock.Show();
        }
        if (_data.IsAbilityKeyword(AbilityKeyword.Move))
        {
            Move.Show();
        }
        if (_data.IsAbilityKeyword(AbilityKeyword.Draw))
        {
            Draw.Show();
        }
        if (_data.IsAbilityKeyword(AbilityKeyword.Aura))
        {
            Aura.Show();
        }
        if (_data.IsAbilityKeyword(AbilityKeyword.Berserk))
        {
            Berserk.Show();
        }
        if (_data.IsAbilityKeyword(AbilityKeyword.DarkPower))
        {
            DarkPower.Show();
        }
        if (_data.IsAbilityKeyword(AbilityKeyword.DarkMatter))
        {
            DarkMatter.Show();
        }
        if (_data.IsAbilityKeyword(AbilityKeyword.Reset))
        {
            Reset.Show();
        }
        if (_data.IsAbilityKeyword(AbilityKeyword.Untargetable))
        {
            Untargetable.Show();
        }
        if (_data.IsAbilityKeyword(AbilityKeyword.Chain))
        {
            Chain.Show();
        }
        if (_data.IsAbilityKeyword(AbilityKeyword.Piercing))
        {
            Piercing.Show();
        }

        if (_data.InfoScriptKey != "" && _data.InfoScriptKey != "-")
        {
            string[] keylist = _data.InfoScriptKey.Split(',');
            if (keylist.Length == 2)
            {
                Summon.SetToken(keylist[0], _data);
                Summon.Show();
                Summon2.SetToken(keylist[1], _data);
                Summon2.Show();
            }
            else
            {
                Summon.SetToken(_data.InfoScriptKey, _data);
                Summon.Show();
                Summon2.Hide();
            }

        }

        // Status
        if (_data.IsMinionStatus(CardStatusType.Be_Silence))
        {
            Silenced.Show();
        }
        if (_data.IsMinionStatus(CardStatusType.Be_Freeze))
        {
            Frozen.Show();
        }
        if (_data.IsMinionStatus(CardStatusType.Be_Fear))
        {
            Feared.Show();
        }
        if (_data.IsMinionStatus(CardStatusType.Be_Taunt))
        {
            Taunted.Show();
        }
        if(_data.IsMinionStatus(CardStatusType.Be_Stealth))
        {
            Stealth.Show();
        }
        if (_data.IsMinionStatus(CardStatusType.Be_Link))
        {
            Linked.Show();
        }
        if (_data.IsMinionStatus(CardStatusType.Be_DarkMatter))
        {
            DarkPowered.Show();
        }
        if (_data.IsMinionStatus(CardStatusType.Be_Invincible))
        {
            Invincibled.Show();
        }
        if (_data.IsMinionStatus(CardStatusType.Be_Untargetable))
        {
            Untargetabled.Show();
        }
        if (_data.IsMinionStatus(CardStatusType.Be_Piercing))
        {
            BePiercing.Show();
        }

        /*
        // น่าจะไม่ต้องแสดง Tooltip เรื่องนี้
        if (data.IsMinionStatus(CardStatusType.TauntActive)) // Taunt ของมิเนียนนี้กำลังทำงาน
        {
            // เปิด Tooltip ของ Taunt กำลังทำงาน
        }
        if (data.IsMinionStatus(CardStatusType.AuraActive)) // Aura ของมิเนียนนี้กำลังทำงาน
        {
            // เปิด Tooltip ของ Aura กำลังทำงาน
        }
        if (data.IsMinionStatus(CardStatusType.BerserkActive)) // Berserk ของมิเนียนนี้กำลังทำงาน
        {
            // เปิด Tooltip ของ Berserk กำลังทำงาน
        }
        */
    }

    public void HideAllKeywordIcon()
    {
        // Ability
        Trigger.Hide();
        Awaken.Hide();
        Stealth.Hide();
        Sentinel.Hide();
        Rally.Hide();
        Lastwish.Hide();
        Backstab.Hide();
        Absorb.Hide();
        Taunt.Hide();
        Silence.Hide();
        Freeze.Hide();
        Fear.Hide();
        Link.Hide();
        Lock.Hide();
        Move.Hide();
        Draw.Hide();
        Aura.Hide();
        Berserk.Hide();
        Summon.Hide();
        Summon2.Hide();
        DarkPower.Hide();
        DarkMatter.Hide();
        Reset.Hide();
        Untargetable.Hide();
        Chain.Hide();
        Piercing.Hide();

        // Status
        Silenced.Hide();
        Frozen.Hide();
        Feared.Hide();
        Taunted.Hide();
        Linked.Hide();
        DarkPowered.Hide();
        Invincibled.Hide();
        Untargetabled.Hide();
        BePiercing.Hide();
    }
    #endregion
}
