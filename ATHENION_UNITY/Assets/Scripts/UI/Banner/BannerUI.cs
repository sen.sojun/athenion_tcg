﻿using Karamucho;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class BannerUI : MonoBehaviour
{
    private Image image;
    private Button button;

    private BannerData _data;

    public void SetData(BannerData data, UnityAction<BannerData> onClick)
    {
        this._data = data;
        image = GetComponent<Image>();
        image.sprite = ResourceManager.Load<Sprite>(data.ImagePath);

        button = GetComponent<Button>();
        button.onClick.RemoveAllListeners();
        button.onClick.AddListener(() => onClick?.Invoke(_data));
    }
}
