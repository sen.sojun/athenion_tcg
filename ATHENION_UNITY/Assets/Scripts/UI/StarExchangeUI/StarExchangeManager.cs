﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class StarExchangeManager : OutGameUIBase
{
    public static event Action OnInit;
    public static event Action<string> OnOpenPage;
    public static event Action<StarExchangeItem> OnPurchased;
    public static event Action OnRefresh;

    public override string SceneName { get { return GameHelper.StarExchangeSceneName; } }

    public static StarExchangeManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<StarExchangeManager>();
                DontDestroyOnLoad(_instance.gameObject);
            }
            return _instance;
        }
    }
    private static StarExchangeManager _instance;

    public StarExchangeData Data { get; private set; }

    public override bool IsCanUnloadResourceAfterUnloadScene => false;

    public StarExchangeUIManager UIManager;

    private Coroutine _autoRefreshTask;
    private PlayerVIPData playerVIP;

    private const int _maximumItemPerRotationPool = 12;

    public static readonly int RefreshPoolPrice = 20;
    public static readonly int RefreshPoolPrice_VIP = 10;

    public override void OnInitialize(UnityAction onComplete)
    {
        StartCoroutine(OnInitUI(onComplete));
    }

    private IEnumerator OnInitUI(UnityAction onComplete)
    {
        PopupUIManager.Instance.Show_MidSoftLoad(true);
        bool isFinish = false;

        playerVIP = DataManager.Instance.InventoryData.VIPData;
        UIManager.InitUI();

        isFinish = false;
        RefreshData(() => { isFinish = true; });
        yield return new WaitUntil(() => isFinish == true);

        yield return new WaitForEndOfFrame();

        PopupUIManager.Instance.Show_MidSoftLoad(false);

        OnInit?.Invoke();
        OnOpenPage?.Invoke("StarMarket");
        onComplete?.Invoke();

        yield break;
    }

    public override void OnExit()
    {
        if (_autoRefreshTask != null)
        {
            StopCoroutine(_autoRefreshTask);
        }
    }

    public void RefreshData(UnityAction onComplete)
    {
        StartCoroutine(OnRefreshData(onComplete));
    }

    public IEnumerator OnRefreshData(UnityAction onComplete)
    {
        this.Data = DataManager.Instance.GetStarExchangeData();
        UIManager.SetVirtualCurrency(DataManager.Instance.InventoryData.GetAllPlayerVirtualCurrency());

        UIManager.ClearAllCell();
        UIManager.SetNewArrivalData(Data.NewArrival.ItemList);
        UIManager.SetRotationPoolData(Data.RotationPool.ItemList);

        int refreshPrice = playerVIP.IsVIP() ? RefreshPoolPrice_VIP : RefreshPoolPrice;
        UIManager.SetRotationPrice(refreshPrice);

        bool activeRefreshBtn = Data.RotationPool.ItemList.Count >= _maximumItemPerRotationPool;
        UIManager.SetActiveRotationPriceBtn(activeRefreshBtn);

        UIManager.UpdateNewArrivalTime(GetDateTime(Data.NewArrival.EndDate, DateTimeData.GetDateTimeUTC()));
        UIManager.UpdateRotationPoolTime(GetDateTime(Data.EndDate, DateTimeData.GetDateTimeUTC()));

        //UIManager.UpdateLayout();
        yield return null;

        if (_autoRefreshTask != null)
        {
            StopCoroutine(_autoRefreshTask);
        }

        _autoRefreshTask = StartCoroutine(AutoRefresh());

        onComplete?.Invoke();
    }

    private IEnumerator AutoRefresh()
    {
        while (true)
        {
            yield return new WaitForSeconds(1f);
            UIManager.UpdateNewArrivalTime(GetDateTime(Data.NewArrival.EndDate, DateTimeData.GetDateTimeUTC()));
            UIManager.UpdateRotationPoolTime(GetDateTime(Data.EndDate, DateTimeData.GetDateTimeUTC()));

            yield return CheckUpdateData();
        }
    }

    private string GetDateTime(DateTime endDate, DateTime currentDate)
    {
        TimeSpan time = endDate - currentDate;

        if (time.Days > 0)
        {
            return String.Format(LocalizationManager.Instance.GetText("TEXT_DAY"), time.Days);
        }
        else if (time.Seconds >= 0)
        {
            return time.ToString(@"hh\:mm\:ss");
        }
        else
        {
            return "00:00:00";
        }
    }

    private IEnumerator CheckUpdateData()
    {
        if (this == null)
            yield break;

        if (DateTimeData.GetDateTimeUTC() > Data.EndDate)
        {
            bool isFinish = false;
            PopupUIManager.Instance.Show_SoftLoad(true);

            isFinish = false;
            DataManager.Instance.LoadStarExchangeData(() => isFinish = true, null);
            yield return new WaitUntil(() => isFinish == true);

            isFinish = false;
            RefreshData(() => { isFinish = true; });
            yield return new WaitUntil(() => isFinish == true);

            PopupUIManager.Instance.Show_SoftLoad(false);
        }

        yield break;
    }

    public void PurchaseStarExchangeItem(StarExchangeItem item)
    {
        PopupUIManager.Instance.Show_MidSoftLoad(true);
        UnityAction onComplete = delegate
        {
            DataManager.Instance.LoadInventory(
                () =>
                {
                    RefreshData(delegate ()
                    {
                        PopupUIManager.Instance.Show_MidSoftLoad(false);
                        OnPurchased?.Invoke(item);
                    });
                    //UI.Refresh
                }
                , null
            );
        };
        UnityAction<string> onFail = delegate (string err)
        {
            PopupUIManager.Instance.Show_MidSoftLoad(false);
            string title = LocalizationManager.Instance.GetText("STAR_EXCHANGE_TITLE");
            string message = LocalizationManager.Instance.GetText(err);
            PopupUIManager.Instance.ShowPopup_Error(title, message);
        };

        // DI convert to VC_DI
        string currencyCode = "VC_" + item.Currency.ToString();
        DataManager.Instance.PurchaseStarExchange(item.ItemID, currencyCode, onComplete, onFail);
    }

    public void RefreshRotaionPool()
    {
        PopupUIManager.Instance.Show_MidSoftLoad(true);
        UnityAction onComplete = delegate
        {
            DataManager.Instance.LoadInventory(
                () =>
                {
                    RefreshData(delegate ()
                    {
                        PopupUIManager.Instance.Show_MidSoftLoad(false);
                        OnRefresh?.Invoke();
                    });
                    //UI.Refresh
                }
                , null
            );
        };
        UnityAction<string> onFail = delegate (string err)
        {
            PopupUIManager.Instance.Show_MidSoftLoad(false);
            string title = LocalizationManager.Instance.GetText("STAR_EXCHANGE_TITLE");
            string message = LocalizationManager.Instance.GetText(err);
            PopupUIManager.Instance.ShowPopup_Error(title, message);
        };

        DataManager.Instance.RefreshRotaionPool(onComplete, onFail);
    }

}
