﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Newtonsoft.Json;
using System;

public class StarExchangeUIManager : MonoBehaviour
{
    #region Inspector
    [Header("Ref")]
    [SerializeField] private Canvas Canvas;

    [Header("VC")]
    [SerializeField] private StarVC StarVC_SC;
    [SerializeField] private StarVC StarVC_SS;
    [SerializeField] private StarVC StarVC_S1;

    [Header("Time")]
    [SerializeField] private TextMeshProUGUI TEXT_Time_New;
    [SerializeField] private TextMeshProUGUI TEXT_Time_Rotate;

    [Header("Prafab Cell")]
    [SerializeField] private StarStoreCell Prefab_StoreCell_new;
    [SerializeField] private StarStoreCell Prefab_StoreCell_rotate;

    [Header("Content")]
    [SerializeField] private Transform GROUP_New;
    [SerializeField] private Transform GROUP_Rotate;
    [SerializeField] private Transform GROUP_NoContent;

    [Header("BTN")]
    [SerializeField] private Button BTN_Refresh;
    [SerializeField] private TextMeshProUGUI TEXT_RefreshPrice;
    #endregion

    #region Private Properties
    private int _vc_sc;
    private int _vc_ss;
    private int _vc_s1;

    private int _refreshPrice = 0;

    private List<StarStoreCell> _cellNewList = new List<StarStoreCell>();
    private List<StarStoreCell> _cellRotateList = new List<StarStoreCell>();
    #endregion

    #region Methods
    public void InitUI()
    {
        InitBTN();
    }

    public void SetActiveRotationPriceBtn(bool activeRefreshBtn)
    {
        BTN_Refresh.gameObject.SetActive(activeRefreshBtn);
    }

    private void InitBTN()
    {
        BTN_Refresh.onClick.RemoveAllListeners();
        BTN_Refresh.onClick.AddListener(RefreshRotation);
    }

    public void UpdateLayout()
    {
        if (Canvas != null)
        {
            Canvas.ForceUpdateCanvases();
        }
    }

    public void SetVirtualCurrency(Dictionary<VirtualCurrency, int> playerCurrency)
    {
        playerCurrency.TryGetValue(VirtualCurrency.SC, out _vc_sc);
        playerCurrency.TryGetValue(VirtualCurrency.SS, out _vc_ss);
        playerCurrency.TryGetValue(VirtualCurrency.S1, out _vc_s1);

        StarVC_SC.UpdateUI(VirtualCurrency.SC, _vc_sc);
        StarVC_SS.UpdateUI(VirtualCurrency.SS, _vc_ss);
        StarVC_S1.UpdateUI(VirtualCurrency.S1, _vc_s1);
    }

    public void SetRotationPrice(int refreshPrice)
    {
        //TODO: Set Rotation Refresh Price.
        _refreshPrice = refreshPrice;
        string key = LocalizationManager.Instance.GetText("BUTTON_REFRESH_STAR_ROTATION");
        string message = string.Format(key, refreshPrice.ToString());
        TEXT_RefreshPrice.text = message;
    }

    public void ClearAllCell()
    {
        ClearStoreCell(_cellNewList);
        ClearStoreCell(_cellRotateList);
    }

    public void SetNewArrivalData(List<StarExchangeItem> itemList)
    {
        //NewArrivalText.text = JsonConvert.SerializeObject(itemList);
        ClearStoreCell(_cellNewList);
        CreateStoreCell(Prefab_StoreCell_new.gameObject, GROUP_New, itemList, _cellNewList);
        GameHelper.ForceRebuildAllLayoutImmediate(GROUP_New.GetComponent<RectTransform>());
    }

    public void SetRotationPoolData(List<StarExchangeItem> itemList)
    {
        //RotationPoolText.text = JsonConvert.SerializeObject(itemList);
        GROUP_NoContent.gameObject.SetActive(itemList.Count == 0);
        ClearStoreCell(_cellRotateList);
        CreateStoreCell(Prefab_StoreCell_rotate.gameObject, GROUP_Rotate, itemList, _cellRotateList);
        GameHelper.ForceRebuildAllLayoutImmediate(GROUP_Rotate.GetComponent<RectTransform>());
    }

    public void UpdateNewArrivalTime(string newArrivalTime)
    {
        TEXT_Time_New.text = newArrivalTime;
    }

    public void UpdateRotationPoolTime(string rotationTime)
    {
        TEXT_Time_Rotate.text = rotationTime;
    }

    #endregion

    #region Cell Generator
    private void CreateStoreCell(GameObject prefab, Transform targetParent, List<StarExchangeItem> itemList, List<StarStoreCell> cellList)
    {
        if (cellList.Count == 0)
        {
            foreach (StarExchangeItem item in itemList)
            {
                GameObject obj = Instantiate(prefab, targetParent);
                StarStoreCell cell = obj.GetComponent<StarStoreCell>();
                cell.SetupData(item, ShowDetail, PuchaseItem);
                cellList.Add(cell);
            }
        }
        else
        {
            for (int i = 0; i < itemList.Count; i++)
            {
                cellList[i].gameObject.SetActive(true);
                cellList[i].SetupData(itemList[i], ShowDetail, PuchaseItem);
            }
        }
    }

    private void ClearStoreCell(List<StarStoreCell> list)
    {
        foreach (StarStoreCell item in list)
        {
            item.gameObject.SetActive(false);
        }
    }
    #endregion

    #region Info Popup
    public void ShowDetail(StarExchangeItem data)
    {
        string head = LocalizationManager.Instance.GetText("ITEM_NAME_" + data.ItemID);
        string message = LocalizationManager.Instance.GetText("ITEM_DESCRIPTION_" + data.ItemID);
        PopupUIManager.Instance.ShowPopup_ConfirmOnlyWithItemDetail(head, message, data.ItemID);
    }

    public void HideDetail()
    {
        PopupUIManager.Instance.ClearPopup_SectionMid();
    }
    #endregion

    public void RefreshRotation()
    {
        string head = LocalizationManager.Instance.GetText("TEXT_REFRESH_STAR_ROTATION");
        string key = LocalizationManager.Instance.GetText("TEXT_REFRESH_STAR_ROTATION_DESCRIPTION");
        string message = string.Format(key, _refreshPrice);

        PopupUIManager.Instance.ShowPopup_SureCheck(head, message, delegate
        {
            StarExchangeManager.Instance.RefreshRotaionPool();
        }, null);

    }

    public void PuchaseItem(StarExchangeItem item)
    {
        string head = LocalizationManager.Instance.GetText("TEXT_REDEEM_ITEM");
        string key = LocalizationManager.Instance.GetText("TEXT_REDEEM_ITEM_DESCRIPTION");
        string itemName = LocalizationManager.Instance.GetText("ITEM_NAME_" + item.ItemID);
        string vc = string.Format("<sprite name={0}>", "S_" + item.Currency.ToString().ToUpper());
        string price = item.Price.ToString("N0");
        string message = string.Format(key, itemName, vc, price);

        PopupUIManager.Instance.ShowPopup_SureCheck(head, message, delegate
        {
            StarExchangeManager.Instance.PurchaseStarExchangeItem(item);
        }, null);
    }
}
