﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;

public class StarStoreCell : MonoBehaviour
{
    #region Inspector
    [Header("Ref")]
    [SerializeField] private Image IMG_IconItem;
    [SerializeField] private Image IMG_VC;
    [SerializeField] private TextMeshProUGUI TEXT_Price;
    [SerializeField] private GameObject GROUP_Owned;

    [Header("BTN")]
    [SerializeField] private Button BTN_Info;
    [SerializeField] private Button BTN_Buy;
    #endregion

    #region Private
    private StarExchangeItem _data;
    private UnityAction<StarExchangeItem> _onClickInfo;
    private UnityAction<StarExchangeItem> _onClickBuy;
    #endregion

    #region Methods
    public void SetupData(StarExchangeItem data, UnityAction<StarExchangeItem> onClickInfo, UnityAction<StarExchangeItem> onClickBuy)
    {
        _data = data;
        _onClickBuy = onClickBuy;
        _onClickInfo = onClickInfo;

        SetImage(_data.ItemID);
        SetPrice(_data.Currency, _data.Price);
        SetOwned(_data.IsBuyable);
        SetupBTN();
    }

    private void SetupBTN()
    {
        BTN_Info.onClick.RemoveAllListeners();
        BTN_Info.onClick.AddListener(() =>_onClickInfo(_data));

        BTN_Buy.onClick.RemoveAllListeners();
        BTN_Buy.onClick.AddListener(() => _onClickBuy(_data));
    }

    private void SetImage(string id)
    {
        IMG_IconItem.sprite = SpriteResourceHelper.LoadSprite(id, SpriteResourceHelper.SpriteType.ICON_Item);
    }

    private void SetPrice(VirtualCurrency vc, int price)
    {
        IMG_VC.sprite = SpriteResourceHelper.LoadSprite("VC_" + vc.ToString(), SpriteResourceHelper.SpriteType.ICON_Item);
        TEXT_Price.text = price.ToString("N0");
    }

    private void SetOwned(bool isBuyable)
    {
        GROUP_Owned.SetActive(!isBuyable);
    }

    #endregion
}
