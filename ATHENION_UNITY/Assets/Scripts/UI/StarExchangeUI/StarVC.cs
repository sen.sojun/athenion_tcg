﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class StarVC : MonoBehaviour
{
    #region Inspector
    [SerializeField] private Image IMG_VC;
    [SerializeField] private TextMeshProUGUI TEXT_Amount;
    #endregion

    #region Private Properties
    private const string _vcprefix = "VC_";
    #endregion

    public void UpdateUI(VirtualCurrency vcType, int amount)
    {
        IMG_VC.sprite = SpriteResourceHelper.LoadSprite(_vcprefix + vcType.ToString(), SpriteResourceHelper.SpriteType.ICON_Item);
        TEXT_Amount.text = amount.ToString("N0");
    }
}
