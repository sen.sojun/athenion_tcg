﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UITapFeedbackController : MonoBehaviour
{
    [Header("Tap Effect")]
    [SerializeField] private GameObject TAP_effect;
    //[SerializeField] private float DestroyTime = 0.7f;
    FlexiblePooling effectPooling;
    private void Awake()
    {
        effectPooling = new FlexiblePooling(this.transform, TAP_effect, 5);
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            CreateTap(Input.mousePosition);
        }
    }

    private void CreateTap(Vector3 screenPos)
    {
        GameObject obj = effectPooling.GetObject();
        //GameObject obj = Instantiate(TAP_effect, this.transform);
        Vector3 currentPosition = screenPos;
        obj.GetComponent<RectTransform>().anchoredPosition = currentPosition;
        obj.SetActive(true);
        //Destroy(obj, DestroyTime);
    }

    private void ShowTap()
    {
        TAP_effect.SetActive(true);
    }

    private void HideTap()
    {
        TAP_effect.SetActive(false);
    }


}
