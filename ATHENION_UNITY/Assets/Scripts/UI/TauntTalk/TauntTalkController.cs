﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TauntTalkController : MonoBehaviour
{
    #region Public Properties
    public Button BTN_TauntTalk;
    public Button TauntBlockBG;
    public GameObject GROUP_TauntWarning;

    [Header("Dialog")]
    public TauntTalkDialog TopDialog;
    public TauntTalkDialog BotDialog;

    [Header ("Choice Ref")]
    public List<BTN_TauntChoice> TauntList;
    #endregion

    #region Private Properties
    // Delay timer
    private const float _DELAY_RESET_TIME = 10.0f;
    private const float _DELAY_TIME = 2.0f;
    private const float _DELAY_START_TIME = 3.0f;
    private float _currentDeleyTime = _DELAY_START_TIME;

    private bool _isReady = true;
    private bool _isOpen = false;
    private IEnumerator _tauntTimerCoroutine;
    #endregion

    #region Methods
    private void Start()
    {
        TauntBlockBG.gameObject.SetActive(false);
        SetClickToOpen();
        SetBGToClose();
        _tauntTimerCoroutine = TauntTimer();
    }

    public void ShowDialog(PlayerIndex playerIndex, string dialog, float duration = 3.0f)
    {
        if (GameManager.Instance.IsLocalPlayer(playerIndex))
        {
            BotDialog.ShowDialog(dialog, duration);
        }
        else
        {
            TopDialog.ShowDialog(dialog, duration);
        }
    }

    public void EnableBTN(bool isEnable)
    {
        BTN_TauntTalk.gameObject.SetActive(isEnable);
    }

    private void SetBGToClose()
    {
        TauntBlockBG.onClick.RemoveAllListeners();
        TauntBlockBG.onClick.AddListener(delegate {
            HideChoices();
            SetClickToOpen();
        });
    }

    public void SetClickToOpen()
    {
        BTN_TauntTalk.onClick.RemoveAllListeners();
        BTN_TauntTalk.onClick.AddListener(delegate
        {
            if (GameManager.Instance.IsStoryMode()) return;

            if (UIManager.Instance.TauntTalkController.BotDialog.IsPlaying ||
            GameManager.Instance.Phase <= GamePhase.ReHand)
            {
                return;
            }

            // Show Emote Menu
            if (!_isOpen)
            {
                if (_isReady)
                {
                    SoundManager.PlaySFX(SoundManager.SFX.Cancel_Click);
                    ShowChoices();
                    SetClickToClose();
                }
                else
                {
                    if (!GROUP_TauntWarning.activeSelf)
                    {
                        SoundManager.PlaySFX(SoundManager.SFX.Graveyard_Click);
                        GameHelper.UITransition_ZoomFadeIn(GROUP_TauntWarning);
                    }
                    //Debug.LogWarning("<b>Show warning taunt is on cooldown !!!</b>");
                }
            }

        });
    }

    private void SetClickToClose()
    {
        BTN_TauntTalk.onClick.RemoveAllListeners();
        BTN_TauntTalk.onClick.AddListener(delegate
        {
            if (_isOpen)
            {
                HideChoices();
                SetClickToOpen();
            }

        });
    }

    public void ShowChoices()
    {
        if (!_isOpen)
        {
            TauntBlockBG.gameObject.SetActive(true);
            foreach (BTN_TauntChoice item in TauntList)
            {
                item.Show(OnTauntPlay);
            }
            _isOpen = true;
        }

    }

    public void HideChoices()
    {
        if (_isOpen)
        {
            TauntBlockBG.gameObject.SetActive(false);
            foreach (BTN_TauntChoice item in TauntList)
            {
                item.Hide();
            }
            _isOpen = false;
        }
    }
    #endregion

    #region Events
    private void OnTauntPlay()
    {        
        StopCoroutine(_tauntTimerCoroutine);

        _tauntTimerCoroutine = TauntTimer();
        StartCoroutine(_tauntTimerCoroutine);
    }

    private IEnumerator TauntTimer()
    {
        // Start delay
        //Debug.LogWarning("Taunt delay :" + _currentDeleyTime);
        _isReady = false;
        yield return new WaitForSecondsRealtime(_currentDeleyTime);

        // increase delay
        _currentDeleyTime += _DELAY_TIME;
        _isReady = true;
        //Debug.LogWarning("Taunt Ready ");

        // Fully reset taunt timer
        yield return new WaitForSecondsRealtime(_DELAY_RESET_TIME);
        _currentDeleyTime = _DELAY_START_TIME;
        //Debug.LogWarning("<color=red>Timer Reset:</color>" + _currentDeleyTime);
    }
    #endregion
}
