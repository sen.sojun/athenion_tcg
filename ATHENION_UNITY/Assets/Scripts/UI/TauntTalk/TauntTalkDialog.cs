﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using DG.Tweening;

public class TauntTalkDialog : MonoBehaviour
{
    #region Static Properties
    public readonly static float ScaleTime = 0.3f;
    #endregion

    #region Properties
    public Image Box;
    public TextMeshProUGUI DialogText;
    public bool IsPlaying { get; private set; }

    private float _duration;
    private Sequence _sq;
    #endregion

    private void Start()
    {
        Box.gameObject.SetActive(false);
        Box.transform.localScale = Vector3.zero;
    }

    public void ShowDialog(string dialog, float duration = 3.0f)
    {
#if TEST_DEBUG
        _duration = duration;
        StartCoroutine(Show(dialog));
#else
        _duration = duration;

        if (_duration >= (2.0f * ScaleTime))
        {
            StartCoroutine(Show(dialog));
        }
#endif
    }

    private IEnumerator Show(string dialog)
    {
        Box.gameObject.SetActive(true);
        Box.transform.localScale = Vector3.zero;
        DialogText.text = dialog;

        float duration = _duration - (2.0f * ScaleTime); // ลบ Scale Time ออก

        IsPlaying = true;
        _sq = DOTween.Sequence();

        _sq.Append(Box.transform.DOScale(1.0f, ScaleTime).SetEase(Ease.OutBack));

        yield return new WaitForSeconds(duration);

        _sq.Append(Box.transform.DOScale(0.0f, ScaleTime).SetEase(Ease.OutExpo));
        IsPlaying = false;
    }

}
