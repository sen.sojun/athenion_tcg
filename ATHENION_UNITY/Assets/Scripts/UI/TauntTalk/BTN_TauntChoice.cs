﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;
using UnityEngine.Events;


public class BTN_TauntChoice : MonoBehaviour, IPointerClickHandler
{
    private Sequence _sq;
    private bool _isPlaying = false;
    private UnityAction _onClick;

    public HeroVoice HeroVoiceIndex;

    #region Methods
    private void Start()
    {
        gameObject.SetActive(false);
    }

    public void Show(UnityAction onClick)
    {
        _onClick = onClick;
        _sq = DOTween.Sequence();
        gameObject.SetActive(true);
        _isPlaying = true;
        transform.localScale = Vector3.zero;
        _sq.Append(transform.DOScale(1.0f, 0.3f).SetEase(Ease.OutBack)).OnComplete(delegate
        {
            _isPlaying = false;
        });
    }

    public void Hide()
    {
        _sq.Kill();
        _isPlaying = true;
        _sq.Append(transform.DOScale(0.0f, 0.3f).SetEase(Ease.OutExpo).OnComplete(delegate
        {
            //gameObject.SetActive(false);
            _isPlaying = false;
        }));
    }

    #endregion

    #region Event
    public void OnPointerClick(PointerEventData eventData)
    {
        _onClick?.Invoke();

        UIManager.Instance.HideTauntChoices();
        GameManager.Instance.RequestLocalSendMessage(HeroVoiceIndex);
        UIManager.Instance.EnableTauntBTN(true);
    }
    #endregion
}
