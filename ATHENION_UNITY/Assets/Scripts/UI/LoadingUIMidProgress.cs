﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class LoadingUIMidProgress : MonoBehaviour
{
    [SerializeField]
    private Image progressImage;

    private float currentProgress = 0;
    private float targetProgress = 0;

    Coroutine lerpValueRoutine;

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private void SetProgress(float value)
    {
        progressImage.fillAmount = value;
    }

    public void SetProgress(int current, int max)
    {
        targetProgress = Convert.ToSingle(current) / Convert.ToSingle(max); 
        //
        if (current == max)
        {
            if (lerpValueRoutine != null)
                StopCoroutine(lerpValueRoutine);
        }
        else
        {
            if (lerpValueRoutine != null)
                StopCoroutine(lerpValueRoutine);
            lerpValueRoutine = StartCoroutine(UpdateProgress());
        }
    }

    public void ShowUI()
    {
        currentProgress = 0;
        SetProgress(0);
        gameObject.SetActive(true);
    }

    public void HideUI()
    {
        gameObject.SetActive(false);
    }

    IEnumerator UpdateProgress()
    {
        float duration = 0f;
        while (currentProgress < targetProgress)
        {
            duration += 0.5f * Time.deltaTime;
            currentProgress = Mathf.Lerp(currentProgress, targetProgress, duration / 0.5f); 
            SetProgress(currentProgress);
            yield return null;
        }
    }
}
