﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UITooltip : MonoBehaviour {

    public TextMeshProUGUI TextTooltip;
    private string _realMessage;

    public void Show(string message, Vector3 position)
    {
        if (!gameObject.activeSelf)
        {
            LocalizationManager.Instance.GetText(message, out _realMessage);
            TextTooltip.text = _realMessage;
            SetPosition(position);
            gameObject.SetActive(true);
        }
    }

    public void Hide()
    {
        if (gameObject.activeSelf)
        {
            gameObject.SetActive(false);
        }
    }

    private void SetPosition(Vector3 position)
    {
        Vector3 targetLocation = Camera.main.WorldToScreenPoint(position);
        Vector3 offset = new Vector3(0, Screen.height * 0.05f, 0);
        //offset += new Vector3(0, Screen.height * 0.05f, 0);

        if (targetLocation.x > Screen.width * 0.8f)
        {
            offset += new Vector3(-Screen.width * 0.1f, 0, 0);
        }
        else if (targetLocation.x < Screen.width * 0.2f)
        {
            offset += new Vector3(Screen.width * 0.1f, 0, 0);
        }

        gameObject.transform.position = targetLocation + offset;
    }

}
