﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.Linq;
using Karamucho.UI;

namespace Karamucho.Collection
{
    public class DetailCard_Collection : DetailCard_Craftable
    {
        #region Inspector
        public GameObject P_FR;
        public GameObject P_RF;
        public GameObject P_S1;

        [Header("Player Currency")]
        public TextMeshProUGUI PlayerFragmentCount;
        public TextMeshProUGUI PlayerRainbowCount;
        public TextMeshProUGUI PlayerNovaCount;

        [Header("Prefab Price")]
        [SerializeField] private CraftingPriceCell Prefab_PriceCell;

        [Header("Cost Recycle")]
        [SerializeField] private Transform RecycleContent;

        [Header("Cost Craft")]
        [SerializeField] private Transform CraftContent;

        [Header("Cost Enchant")]
        [SerializeField] private Transform EnchantContent;

        #endregion

        #region Private Properties
        private int _currencyFR = 0;
        private int _currencyRF = 0;
        private int _currencyS1 = 0;
        private Dictionary<VirtualCurrency, int> _currentVC = new Dictionary<VirtualCurrency, int>();

        private Dictionary<VirtualCurrency, int> _craftPriceDict = new Dictionary<VirtualCurrency, int>();
        private Dictionary<VirtualCurrency, int> _recyclePriceDict = new Dictionary<VirtualCurrency, int>();
        private Dictionary<VirtualCurrency, int> _enchantPriceDict = new Dictionary<VirtualCurrency, int>();

        private List<CraftingPriceCell> _craftCells = new List<CraftingPriceCell>();
        private List<CraftingPriceCell> _recycleCells = new List<CraftingPriceCell>();
        private List<CraftingPriceCell> _enchantCells = new List<CraftingPriceCell>();
        #endregion

        private void UpdateCurrency()
        {
            _currentVC = DataManager.Instance.InventoryData.GetAllPlayerVirtualCurrency();
            _currencyFR = _currentVC[VirtualCurrency.FR];
            _currencyRF = _currentVC[VirtualCurrency.RF];
            _currencyS1 = _currentVC[VirtualCurrency.S1];

            _craftPriceDict = _currentCatalogCardData.CraftPrice;
            _recyclePriceDict = _currentCatalogCardData.RecyclePrice;
            _enchantPriceDict = _currentCatalogCardData.TransformPrice;
        }

        #region Override Method
        public override void Show()
        {
            base.Show();

            if (PlayerSeenListManager.Instance.IsNewCard(_data.FullID))
            {
                PlayerSeenListManager.Instance.AddSeenListData(SeenItemTypes.Card, true, _data.FullID);
            }
        }

        protected override void SetCurrency()
        {
            UpdateCurrency(); 

            SetPlayer_Fragment(_currencyFR);
            SetPlayer_Rainbow(_currencyRF);
            SetPlayer_Nova(_currencyS1);
            SetPrice();  

            base.SetCurrency();
        }

        protected override void RecycleCard()
        {
            base.RecycleCard();

            SetEffect(_recyclePriceDict);
        }

        protected override void SetBTNAvailable(bool isCraftable)
        {
            bool isCanCraftFeature = DataManager.Instance.IsFeatureEnable(FeatureUnlock.CardCrafting);
            // Is Not Craftable
            BTN_Craft.interactable = false;
            BTN_Recycle.interactable = false;
            BTN_Enchant.interactable = false;

            BTN_LockCraft.gameObject.SetActive(!isCanCraftFeature);
            BTN_LockRecycle.gameObject.SetActive(!isCanCraftFeature);
            BTN_LockEnchant.gameObject.SetActive(!isCanCraftFeature);

            if (isCraftable)
            {
                // Check Recycle Amounts
                bool isCanRecycle = _currentAmount > 0;
                BTN_Recycle.interactable = isCanRecycle && isCanCraftFeature;

                // Check Craft
                bool isCanCraft = _currentAmount < _maximumCard;
                foreach (KeyValuePair<VirtualCurrency, int> item in _craftPriceDict)
                {
                    if (_currentVC[item.Key] < item.Value)
                    {
                        isCanCraft = false;
                        break;
                    }
                }
                BTN_Craft.interactable = isCanCraft && isCanCraftFeature;

                // Check Craft
                bool isCanEnchant = _currentAmount > 0;
                foreach (KeyValuePair<VirtualCurrency, int> item in _enchantPriceDict)
                {
                    if (_currentVC[item.Key] < item.Value)
                    {
                        isCanEnchant = false;
                        break;
                    }
                }
                BTN_Enchant.interactable = isCanEnchant && isCanCraftFeature;

                // Check if have available
                BTN_Craft.gameObject.SetActive(_craftPriceDict != null && _craftPriceDict.Count > 0);
                BTN_Recycle.gameObject.SetActive(_recyclePriceDict != null && _recyclePriceDict.Count > 0);
                BTN_Enchant.gameObject.SetActive(_enchantPriceDict != null && _enchantPriceDict.Count > 0);
            }
            else
            {
                BTN_Craft.gameObject.SetActive(false);
                BTN_Recycle.gameObject.SetActive(false);
                BTN_Enchant.gameObject.SetActive(false);
            }
        }
        #endregion

        private void SetEffect(Dictionary<VirtualCurrency, int> recyclePrice)
        {
            P_FR.SetActive(recyclePrice.ContainsKey(VirtualCurrency.FR));
            P_RF.SetActive(recyclePrice.ContainsKey(VirtualCurrency.RF));
            P_S1.SetActive(recyclePrice.ContainsKey(VirtualCurrency.S1));
        }

        #region Currency Setup
        private void SetPlayer_Fragment(int value)
        {
            PlayerFragmentCount.text = value.ToString("N0");
        }

        private void SetPlayer_Rainbow(int value)
        {
            PlayerRainbowCount.text = value.ToString("N0");
        }

        private void SetPlayer_Nova(int value)
        {
            PlayerNovaCount.text = value.ToString("N0");
        }

        private void SetPrice()
        {
            SetupPriceCell(_craftPriceDict, Prefab_PriceCell.gameObject, CraftContent, _craftCells, false);
            SetupPriceCell(_recyclePriceDict, Prefab_PriceCell.gameObject, RecycleContent, _recycleCells, true);
            SetupPriceCell(_enchantPriceDict, Prefab_PriceCell.gameObject, EnchantContent, _enchantCells, false);
        }

        private void SetupPriceCell(Dictionary<VirtualCurrency,int> priceList, GameObject prefab, Transform Group, List<CraftingPriceCell> cellList, bool isPositive)
        {
            // Clear Cell
            foreach (CraftingPriceCell item in cellList)
            {
                item.gameObject.SetActive(false);
            }

            // Setup Cell
            List<VirtualCurrency> keyList = priceList.Keys.ToList();
            for (int i = 0; i < keyList.Count; i++)
            {
                if (i >= cellList.Count)
                {
                    GameObject obj = Instantiate(prefab, Group);
                    CraftingPriceCell cell = obj.GetComponent<CraftingPriceCell>();
                    cellList.Add(cell);
                }

                cellList[i].gameObject.SetActive(true);
                cellList[i].Setup(keyList[i], priceList[keyList[i]], isPositive);
            }
        }
        #endregion

        #region Sound
        public void PlaySound_Craft()
        {
            SoundManager.PlaySFX(SoundManager.SFX.Collection_Craft);
        }

        public void PlaySound_Recycle()
        {
            SoundManager.PlaySFX(SoundManager.SFX.Collection_Destroy);
        }
        #endregion
    }

}

