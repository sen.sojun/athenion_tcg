﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CraftingPriceCell : MonoBehaviour
{
    #region Inspector Properties
    [Header("Ref")]
    [SerializeField] private Image IMG_Icon;
    [SerializeField] private TextMeshProUGUI TEXT_Price;
    #endregion

    #region Methods
    public void Setup(VirtualCurrency currency, int amount, bool isPositive = true)
    {
        gameObject.SetActive(amount > 0);

        string key = "VC_" + currency.ToString();
        Sprite icon = SpriteResourceHelper.LoadSprite(key, SpriteResourceHelper.SpriteType.ICON_Item);

        // Negateive Symbol
        Color textColor = Color.white;
        string symbol = "";
        if (!isPositive)
        {
            symbol = "-";
            textColor = GameHelper.GetColor_Number_Decrease();
        }
        else
        {
            symbol = "+";
            textColor = GameHelper.GetColor_Number_Increase();
        }

        IMG_Icon.sprite = icon;
        TEXT_Price.text = symbol + amount.ToString("N0");
        TEXT_Price.color = textColor;
    }
    #endregion
}
