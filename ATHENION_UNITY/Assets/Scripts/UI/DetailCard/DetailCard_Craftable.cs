﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

namespace Karamucho.UI
{
    public class DetailCard_Craftable : DetailCard
    {
        #region Enums
        private enum CraftActionTypes
        {
            None = 0
            , Craft
            , Recycle
            , Transform
        }
        #endregion

        #region Public Properties
        [Header("Crafting Animation")]
        public Animator CraftAnimator;

        [Header("BTN")]
        public Button BTN_Craft;
        public Button BTN_Recycle;
        public Button BTN_Enchant;
        public Button BTN_LockCraft;
        public Button BTN_LockRecycle;
        public Button BTN_LockEnchant;

        [Header("Hint")]
        public GameObject HintGroup;
        public TextMeshProUGUI TEXT_Hint;
        public TextMeshProUGUI TEXT_RecycleCount;

        [Header("UI")]
        public GameObject BlockingMask;
        #endregion

        #region Protected Properties
        protected bool _isLoading;
        protected CatalogCardData _cardCatalogData;
        #endregion

        #region Private Properties
        private CraftActionTypes _actionType;
        #endregion

        #region Events
        public event Action<string> OnCraftCard;
        public event Action<string> OnRecycleCard;
        public event Action<string> OnTransformCard;
        #endregion

        #region Method

        public override void UpdateCard(CardUIData data, int amount, CatalogCardData catalogCardData)
        {
            _data = data;
            _currentAmount = amount;
            _cardCatalogData = catalogCardData;

            if (catalogCardData != null)
            {
                _currentCatalogCardData = new CatalogCardData(catalogCardData);
            }
            else
            {
                Debug.LogError("No Catalog Data");
            }

            SetBTN();
            SetArtistName(_data);
            SetHint();
            CollectionCardUI.SetData(_data);
            CollectionCardUI.SetCardAmount(amount);
            UITooltipKeywordGroup.SetData(_data);
            UITooltipKeywordGroup_Trigger.SetKeyword(_data.GetAbilityKeywordCount());

            SetCurrency();

            SetBTNAvailable(DataManager.Instance.IsItemCatalogTagListContain(_data.FullID, "craftable"));
        }

        protected override void SetBTN()
        {
            base.SetBTN();

            // OnCraft
            BTN_Craft.onClick.RemoveAllListeners();
            BTN_Craft.onClick.AddListener(delegate
            {
                if (_isLoading)
                {
                    return;
                }

                PopupUIManager.Instance.ShowPopup_SureCheck(
                    LocalizationManager.Instance.GetText("TEXT_CRAFT_CARD")
                    , LocalizationManager.Instance.GetText("TEXT_CRAFT_CARD_DESCRIPTION")
                    , CraftCard
                    , () => ShowBlockingMask(false)
                );
            });

            // On Recycle
            BTN_Recycle.onClick.RemoveAllListeners();
            BTN_Recycle.onClick.AddListener(delegate
            {
                if (_isLoading)
                {
                    return;
                }

                PopupUIManager.Instance.ShowPopup_SureCheck(
                    LocalizationManager.Instance.GetText("TEXT_RECYCLE_CARD")
                    , LocalizationManager.Instance.GetText("TEXT_RECYCLE_CARD_DESCRIPTION")
                    , RecycleCard
                    , () => ShowBlockingMask(false)
                );
            });

            // On Enchant
            BTN_Enchant.onClick.RemoveAllListeners();
            BTN_Enchant.onClick.AddListener(delegate
            {
                if (_isLoading)
                {
                    return;
                }

                PopupUIManager.Instance.ShowPopup_SureCheck(
                      LocalizationManager.Instance.GetText("TEXT_ENCHANT_CARD")
                    , LocalizationManager.Instance.GetText("TEXT_ENCHANT_CARD_DESCRIPTION")
                    , TransformCard
                    , () => ShowBlockingMask(false)
                );
            });
        }

        protected virtual void SetBTNAvailable(bool isCraftable)
        {
            if (!DataManager.Instance.IsFeatureEnable(FeatureUnlock.CardCrafting))
            {
                isCraftable = false;
            }

            if (isCraftable)
            {
                // Is Craftable
                // Check Recycle Amounts
                if (_currentAmount > 0)
                {
                    BTN_Recycle.interactable = true;
                }
                else
                {
                    BTN_Recycle.interactable = false;
                    BTN_Enchant.interactable = false;
                }
            }
            else
            {
                // Is Not Craftable
                BTN_Craft.interactable = false;
                BTN_Recycle.interactable = false;
                BTN_Enchant.interactable = false;
            }
        }

        protected void SetHint()
        {
            HintGroup.SetActive(_cardCatalogData.IsPriceOverride);

            string priceOverrideMessage = LocalizationManager.Instance.GetText("TEXT_CARD_PRICE_CHANGE_HINT");
            string quotaAmount = LocalizationManager.Instance.GetText("TEXT_CARD_PRICE_CHANGE_HINT_COUNT");
        
            TEXT_Hint.text = string.Format(priceOverrideMessage, _cardCatalogData.OverridePriceEndDateTime.ToString("dd-MM-yyyy"));

            string quotaText = string.Empty;
            if(_cardCatalogData.IsLimited)
            {
                quotaText = string.Format(quotaAmount, _cardCatalogData.QuotaAmount);
            }
            TEXT_RecycleCount.text = quotaText;
        }

        #region Craft Card
        protected void CraftCard()
        {
            _actionType = CraftActionTypes.Craft;
            _isLoading = true;
            ShowBlockingMask(true);
            OnCraftCard?.Invoke(_cardCatalogData.ItemCatalogID);
        }

        protected virtual void RecycleCard()
        {
            _actionType = CraftActionTypes.Recycle;
            _isLoading = true;
            ShowBlockingMask(true);
            OnRecycleCard?.Invoke(_cardCatalogData.ItemCatalogID);
        }

        protected void TransformCard()
        {
            _actionType = CraftActionTypes.Transform;
            _isLoading = true;
            ShowBlockingMask(true);
            OnTransformCard?.Invoke(_cardCatalogData.ItemCatalogID);
        }

        public void OnCraftComplete(CardUIData data, int amount, CatalogCardData catalogData)
        {
            AnimationCraft();
            SetCurrency();
            UpdateCard(data, amount, catalogData);
            _actionType = CraftActionTypes.None;

            _isLoading = false;
            ShowBlockingMask(false);
        }

        public void OnCraftFail()
        {
            _actionType = CraftActionTypes.None;

            _isLoading = false;
            ShowBlockingMask(false);
        }
        #endregion

        protected virtual void SetCurrency()
        {
            // Disable Blocking 
            ShowBlockingMask(false);
        }

        #endregion

        #region UI Animation Methods
        private void AnimationRestart()
        {
            CraftAnimator.SetTrigger("Restart");
        }

        private void AnimationCraft()
        {
            AnimationRestart();
            switch (_actionType)
            {
                case CraftActionTypes.Craft:
                {
                    CraftAnimator.SetTrigger("Craft");
                }
                break;
                
                case CraftActionTypes.Recycle:
                {
                    CraftAnimator.SetTrigger("Recycle");
                }
                break;

                case CraftActionTypes.Transform:
                {
                    CraftAnimator.SetTrigger("Craft");
                }
                break;
            }
        }

        private void ShowBlockingMask(bool isShow)
        {
            BlockingMask.SetActive(isShow);
        }

        public void ShowOwned()
        {
            CollectionCardUI.SetOwned(true);
        }

        public void HideOwned()
        {
            if (_currentAmount == 0)
            {
                CollectionCardUI.SetOwned(false);
            }

        }
        #endregion
    }
}
