﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace Karamucho.UI
{
    public class DetailCard_StarChamber : DetailCard_Craftable
    {
        #region Inspector
        [Header("Prefab")]
        [SerializeField] private CraftingPriceCell PrefabPriceCell;
        [SerializeField] private StarShardCell PrefabStarShardCell;

        [Header("Group")]
        [SerializeField] private Transform GROUP_Currency_Normal;
        [SerializeField] private Transform GROUP_Currency_Super;
        [SerializeField] private Transform GROUP_Craft;
        [SerializeField] private Transform GROUP_Recycle;
        [SerializeField] private Transform GROUP_Enchant;

        #endregion

        #region Private Properties
        private Dictionary<VirtualCurrency, int> _currencyDict = new Dictionary<VirtualCurrency, int>();
        private Dictionary<VirtualCurrency, int> _craftDict = new Dictionary<VirtualCurrency, int>();
        private Dictionary<VirtualCurrency, int> _recycleDict = new Dictionary<VirtualCurrency, int>();
        private Dictionary<VirtualCurrency, int> _enchantDict = new Dictionary<VirtualCurrency, int>();

        private List<StarShardCell> _starShardList = new List<StarShardCell>();
        private List<CraftingPriceCell> _craftingPriceList = new List<CraftingPriceCell>();
        private List<CraftingPriceCell> _recyclePriceList = new List<CraftingPriceCell>();
        private List<CraftingPriceCell> _enchantPriceList = new List<CraftingPriceCell>();
        #endregion

        private void UpdateCurrency()
        {
            // Get Currency
            _currencyDict = DataManager.Instance.InventoryData.GetAllPlayerVirtualCurrency();

            // Clear
            _craftDict.Clear();
            _recycleDict.Clear();
            _enchantDict.Clear();

            // Get Prices
            foreach (KeyValuePair<VirtualCurrency, int> item in _currentCatalogCardData.CraftPrice)
            {
                _craftDict.Add(item.Key, item.Value);
            }
            foreach (KeyValuePair<VirtualCurrency, int> item in _currentCatalogCardData.RecyclePrice)
            {
                _recycleDict.Add(item.Key, item.Value);
            }
            foreach (KeyValuePair<VirtualCurrency, int> item in _currentCatalogCardData.TransformPrice)
            {
                _enchantDict.Add(item.Key, item.Value);
            }
        }

        #region Override Method
        protected override void SetCurrency()
        {
            UpdateCurrency();
            SetPlayerAndPriceCurrency();

            base.SetCurrency();
        }

        protected override void SetBTNAvailable(bool isCraftable)
        {
            if (isCraftable)
            {
                // Is Craftable
                // Check Recycle Amounts
                if (_currentAmount > 0)
                {
                    BTN_Recycle.interactable = true;
                }
                else
                {
                    BTN_Recycle.interactable = false;
                }

                // Check Craft Cost
                if (_currentAmount < _maximumCard && CheckCanPayPrice(_currencyDict, _craftDict))
                {
                    BTN_Craft.interactable = true;
                }
                else
                {
                    BTN_Craft.interactable = false;
                }

                // Check Enchant Cost
                if (CheckCanPayPrice(_currencyDict, _enchantDict))
                {
                    BTN_Enchant.interactable = true;
                }
                else
                {
                    BTN_Enchant.interactable = false;
                }

                // Check if have available
                BTN_Craft.gameObject.SetActive(_craftDict.Count > 0);
                BTN_Recycle.gameObject.SetActive(_recycleDict.Count > 0);
                BTN_Enchant.gameObject.SetActive(_enchantDict.Count > 0);
            }
            else
            {
                // Is Not Craftable
                BTN_Craft.interactable = false;
                BTN_Recycle.interactable = false;
            }

        }
        #endregion

        #region Methods
        private void ClearAll()
        {
            foreach (StarShardCell item in _starShardList)
            {
                Destroy(item.gameObject);
            }
            foreach (CraftingPriceCell item in _craftingPriceList)
            {
                Destroy(item.gameObject);
            }
            foreach (CraftingPriceCell item in _recyclePriceList)
            {
                Destroy(item.gameObject);
            }
            foreach (CraftingPriceCell item in _enchantPriceList)
            {
                Destroy(item.gameObject);
            }

            _starShardList.Clear();
            _craftingPriceList.Clear();
            _recyclePriceList.Clear();
            _enchantPriceList.Clear();
        }

        private void SetPlayerAndPriceCurrency()
        {
            ClearAll();

            // Player Currency
            foreach (KeyValuePair<VirtualCurrency, int> item in _currencyDict)
            {
                if (item.Key < VirtualCurrency.S1 && item.Key > VirtualCurrency.DI)
                {
                    _starShardList.Add(CreateStarShard(item.Key, item.Value, StarShardPanel._maxNormalSignAmount, GROUP_Currency_Normal));
                }
                else if(item.Key == VirtualCurrency.S1) // Only Nova
                {
                    _starShardList.Add(CreateStarShard(item.Key, item.Value, StarShardPanel._maxSuperSignAmount, GROUP_Currency_Super));
                }
            }

            // Price Craft
            foreach (KeyValuePair<VirtualCurrency, int> item in _craftDict)
            {
                _craftingPriceList.Add(CreatePrice(item.Key, item.Value, GROUP_Craft, false));
            }
            // Price Recycle
            foreach (KeyValuePair<VirtualCurrency, int> item in _recycleDict)
            {
                _recyclePriceList.Add(CreatePrice(item.Key, item.Value, GROUP_Recycle, true));
            }
            // Price Enchant
            foreach (KeyValuePair<VirtualCurrency, int> item in _enchantDict)
            {
                _enchantPriceList.Add(CreatePrice(item.Key, item.Value, GROUP_Enchant, false));
            }

        }

        private bool CheckCanPayPrice(Dictionary<VirtualCurrency, int> currentMoney, Dictionary<VirtualCurrency, int> price)
        {
            foreach (KeyValuePair<VirtualCurrency, int> itemPrice in price)
            {
                int current = 0;
                if(currentMoney.TryGetValue(itemPrice.Key, out current))
                {
                    if (current < itemPrice.Value) return false;
                }
                else
                {
                    return false;
                }
            }
            return true;
        }
        #endregion

        #region Price Generate
        private StarShardCell CreateStarShard(VirtualCurrency currency, int amount, int maxAmount, Transform parent, bool isPositive = true)
        {
            GameObject obj = Instantiate(PrefabStarShardCell.gameObject, parent);
            StarShardCell cell = obj.GetComponent<StarShardCell>();
            cell.SetData(currency, amount, maxAmount);

            return cell;
        }

        private CraftingPriceCell CreatePrice(VirtualCurrency currency, int amount, Transform parent, bool isPositive = true)
        {
            GameObject obj = Instantiate(PrefabPriceCell.gameObject, parent);
            CraftingPriceCell cell = obj.GetComponent<CraftingPriceCell>();
            cell.Setup(currency, amount, isPositive);

            return cell;
        }
        #endregion
    }
}
