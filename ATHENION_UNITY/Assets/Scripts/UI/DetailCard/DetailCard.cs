﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Karamucho.Collection;
using System;

namespace Karamucho.UI
{
    public class DetailCard : MonoBehaviour
    {
        #region Inspector Properties
        [Header("Base Detail Card")]
        [SerializeField] protected CardUI_Collection CollectionCardUI;
        [SerializeField] protected TextMeshProUGUI ArtistName;

        [Header("Tooltip Keyword")]
        [SerializeField] protected UITooltipKeywordGroup UITooltipKeywordGroup;
        [SerializeField] protected UITooltipKeyword_TriggerAttachment UITooltipKeywordGroup_Trigger;

        [Header("BTN Voice")]
        [SerializeField] protected Button BTN_PlayVoice;

        [Header("BTN Close")]
        [SerializeField] protected Button BTN_Close;
        #endregion

        #region Public Properties
        public CardUIData Data { get { return _data; } }
        #endregion

        #region Protected Properties
        protected CardUIData _data;
        protected CatalogCardData _currentCatalogCardData;
        protected int _currentAmount;
        protected const int _maximumCard = 3;
        #endregion

        #region Private Properties
        private bool _isPlayVoice = false;
        #endregion

        #region Events
        public event Action OnHideUI;
        #endregion

        #region Base Method
        public virtual void Show()
        {
            GameHelper.UITransition_FadeIn(this.gameObject);
        }

        public virtual void Hide()
        {
            OnHideUI?.Invoke();
            GameHelper.UITransition_FadeOut(this.gameObject);
        }

        protected virtual void SetBTN()
        {
            BTN_Close.onClick.RemoveAllListeners();
            BTN_Close.onClick.AddListener(Hide);

            BTN_PlayVoice.onClick.RemoveAllListeners();
            BTN_PlayVoice.onClick.AddListener(PlayVoice);
        }

        public virtual void UpdateCard(CardUIData data, int amount, CatalogCardData catalogCardData = null)
        {
            _data = data;
            _currentAmount = amount;

            if (catalogCardData != null)
            {
                _currentCatalogCardData = new CatalogCardData(catalogCardData);
            }
            else
            {
                Debug.LogWarning("No Catalog Data: " + _data.FullID);
            }

            SetBTN();
            SetArtistName(_data);
            CollectionCardUI.SetData(_data);
            CollectionCardUI.SetCardAmount(_currentAmount);
            UITooltipKeywordGroup.SetData(_data);
            UITooltipKeywordGroup_Trigger.SetKeyword(_data.GetAbilityKeywordCount());
        }

        protected void PlayVoice()
        {
            if (!_isPlayVoice)
            {
                _isPlayVoice = true;
                SoundManager.PlayMinionSummonVoice(_data.BaseID, () => _isPlayVoice = false);
            }
          
        }

        protected void SetArtistName(CardUIData data)
        {
            ImageDBData imageDBData;
            if (ImageDB.Instance.GetData(data.ImageKey, out imageDBData))
            {
                if (LocalizationManager.Instance.CurrentLanguage == Language.Thai)
                {
                    ArtistName.text = "ศิลปิน\n" + imageDBData.ArtistName_TH;
                }
                else
                {
                    ArtistName.text = "Illustrator\n" + imageDBData.ArtistName_EN;
                }
            }
            else
            {
                if (GameHelper.IsATNVersion_RealDevelopment)
                {
                    ArtistName.text = "<color=red>พี่ปั้ม ภาพนี้ยังไม่ได้ใส่ชื่อศิลปินครับ</color>";
                }
                else
                {
                    ArtistName.text = "";
                }
            }
        }
        #endregion
    }
}
