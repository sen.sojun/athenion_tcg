﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;

public class ProfileEditorRankingCell : MonoBehaviour
{
    #region Public Properties
    public RewardCell Prefab_RewardCell;
    public Image IMG_RankIcon;
    public Image IMG_PlayerAvatar;
    public GameObject IMG_Base;
    public GameObject IMG_Active;
    public TextMeshProUGUI TEXT_RankName;
    public Transform Container_reward;

    [Header("Color (Not used yet)")]
    public Color ActiveTextColor;
    public Color NormalTextColor;
    #endregion

    #region Private Properties
    private int _rankIndex;
    private string _rankId;
    private string _rankName;
    private bool _isActive;

    private List<RewardCell> _rewardList = new List<RewardCell>();
    #endregion

    #region Methods
    public void InitData(int rankIndex, string rankId, string rankName, bool isActive)
    {
        _rankIndex = rankIndex;
        _rankId = rankId;
        _rankName = rankName;
        _isActive = isActive;

        SetActiveStatus(_isActive);
        SetText();
        SetImage();
        SetReward();

        //Set Avatar
        string key = DataManager.Instance.PlayerInfo.AvatarID;
        Sprite avatar = SpriteResourceHelper.LoadSprite(key, SpriteResourceHelper.SpriteType.Avatar);
        SetAvatar(avatar);
    }

    public void SetAvatar(Sprite avatarSpr)
    {
        if (IMG_PlayerAvatar != null)
        {
            IMG_PlayerAvatar.sprite = avatarSpr;
        }
        else
        {
            Debug.Log("ProfileEditorRankingCell.IMG_PlayerAvatar is null; Did you forget to set object reference?");
        }
    }

    public void SetActiveStatus(bool isActive)
    {
        //IMG_Base.SetActive(!isActive);
        IMG_Active.SetActive(isActive);
    }

    private void SetReward()
    {
        if (_rewardList != null && _rewardList.Count > 0)
        {
            while (_rewardList.Count > 0)
            {
                RewardCell cell = _rewardList[0];
                _rewardList.RemoveAt(0);
                Destroy(cell.gameObject);
            }
        }

        List<ItemData> itemDataList = new List<ItemData>();
        SeasonRankRewardDBData data;
        if (SeasonRankRewardDB.Instance.GetData(DataManager.Instance.GetCurrentSeasonIndex().ToString(), out data))
        {
            itemDataList = data.GetRewardItemList(_rankIndex);
        }

        foreach (ItemData item in itemDataList)
        {
            _rewardList.Add(CreateReward(item.ItemID, item.Amount));
        }

    }

    private void SetText()
    {
        TEXT_RankName.text = _rankName;
    }

    private void SetImage()
    {
        Sprite sprite = SpriteResourceHelper.LoadSprite(_rankId, SpriteResourceHelper.SpriteType.ICON_Rank);
        IMG_RankIcon.sprite = sprite;
    }

    #endregion

    private string GetRewardText()
    {
        string text = "";
        foreach (RewardCell item in _rewardList)
        {
            text += string.Format("{0} x {1}\n", item.GetRewardID().ToString(), item.GetRewardCount().ToString());
        }

        return text;
    }

    #region Cell Generate
    private RewardCell CreateReward(string rewardKey, int count)
    {
        GameObject obj = Instantiate(Prefab_RewardCell.gameObject, Container_reward);
        obj.SetActive(true);
        RewardCell cell = obj.GetComponent<RewardCell>();
        cell.InitData(rewardKey, count);

        return cell;
    }
    #endregion
}
