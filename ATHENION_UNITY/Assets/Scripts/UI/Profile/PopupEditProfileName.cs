﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

public class PopupEditProfileName : MonoBehaviour
{
    public TMP_InputField NameField;
    public TextMeshProUGUI ErrorText;
    public Button Confirm;
    public Button Cancel;
    [Header("PriceInfo")]
    public int Amount;

    public void SetErrorText(string text)
    {
        ErrorText.text = text;
    }

    public void ShowUI(UnityAction<EditNameInfo> onConfirm, UnityAction onCancel)
    {
        bool isRenameFree = DataManager.Instance.GetPlayerRenameStatus() == false;
        Confirm.onClick.RemoveAllListeners();
        Confirm.onClick.AddListener(() =>
        {
            EditNameInfo info = new EditNameInfo() { NewName = NameField.text, Price = Amount, Currency = VirtualCurrency.DI };
            if (onConfirm != null)
                onConfirm.Invoke(info);
        }
        );

        Cancel.onClick.RemoveAllListeners();
        Cancel.onClick.AddListener(onCancel);

        SetErrorText("");

        gameObject.SetActive(true);
    }

    public void HideUI()
    {
        gameObject.SetActive(false);
    }
}

public struct EditNameInfo
{
    public string NewName;
    public VirtualCurrency Currency;
    public int Price;
}
