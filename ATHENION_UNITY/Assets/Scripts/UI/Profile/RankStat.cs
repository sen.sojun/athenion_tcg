﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RankStat : MonoBehaviour
{
    public Image IMG_Rank;
    public TextMeshProUGUI TEXT_Rank;

    private void OnEnable()
    {
        string rankId = GameHelper.GetRankID(DataManager.Instance.GetPlayerRankIndex());
        string rankName = GameHelper.GetRankName(rankId);

        IMG_Rank.sprite = SpriteResourceHelper.LoadSprite(rankId, SpriteResourceHelper.SpriteType.ICON_Rank);
        TEXT_Rank.text = rankName;
    }
}
