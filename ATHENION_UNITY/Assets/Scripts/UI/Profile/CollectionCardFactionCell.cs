﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class CollectionCardFactionCell : MonoBehaviour
{
    #region Public Properties
    public Slider SLIDER_Progression;
    public TextMeshProUGUI TEXT_CardCount;
    #endregion

    #region Private Properties
    private int _currentCard;
    private int _maxCard;
    #endregion

    #region Methods
    public void SetData(int current, int max)
    {
        _currentCard = current;
        _maxCard = max;

        SetProgress(true);
    }

    private void SetProgress(bool isAnimate = false)
    {
        TEXT_CardCount.text = string.Format("{0}/{1}", _currentCard, _maxCard);

        if (SLIDER_Progression != null)
        {
            float progressValue = (float)_currentCard / (float)_maxCard;
            if (isAnimate)
            {
                SLIDER_Progression.value = 0;
                SLIDER_Progression.DOValue(progressValue, 1.5f).SetEase(Ease.OutExpo);
            }
            else
            {
                SLIDER_Progression.value = progressValue;
            }
        }

    }

    #endregion


}
