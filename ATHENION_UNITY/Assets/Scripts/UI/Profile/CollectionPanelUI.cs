﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CollectionPanelUI : MonoBehaviour
{
    #region Public Propertues
    [Header("All Card")]
    public TextMeshProUGUI TEXT_AllCard;

    [Header("Faction Cell")]
    public CollectionCardFactionCell Cell_Fire;
    public CollectionCardFactionCell Cell_Water;
    public CollectionCardFactionCell Cell_Earth;
    public CollectionCardFactionCell Cell_Air;
    public CollectionCardFactionCell Cell_Holy;
    public CollectionCardFactionCell Cell_Dark;
    public CollectionCardFactionCell Cell_Neutral;

    [Header("Pack Cell")]
    public CollectionCardFactionCell Cell_WOE;
    #endregion

    #region Private Properties
    private const string _seriesKey_None = "None";
    private const string _seriesKey_Basic = "Basic";
    private const string _seriesKey_WOE = "WOE";
    private const string _seriesKey_StarChamber = "StarChamber";

    private int _cardCount_Fire;
    private int _cardCount_Water;
    private int _cardCount_Earth;
    private int _cardCount_Air;
    private int _cardCount_Holy;
    private int _cardCount_Dark;
    private int _cardCount_Neutral;
    private int _cardCountMax_Fire;
    private int _cardCountMax_Water;
    private int _cardCountMax_Earth;
    private int _cardCountMax_Air;
    private int _cardCountMax_Holy;
    private int _cardCountMax_Dark;
    private int _cardCountMax_Neutral;

    private int _cardCount_WOE;
    private int _cardCountMax_WOE;

    private bool isInit = false;
    private List<string> _cardBaseInventoryList = new List<string>();
    private List<string> _cardBaseMaxList = new List<string>();
    private List<string> _cardBaseInventoryList_WOE = new List<string>();
    private List<string> _cardBaseMaxList_WOE = new List<string>();
    #endregion

    #region Methods
    // Start is called before the first frame update
    private void OnEnable()
    {
        if (!isInit)
        {
            isInit = true;
            // Count Inventory
            CardData cardData;
            CardListData playerInventory = DataManager.Instance.InventoryData.CardInventoryList;
            foreach (KeyValuePair<string, int> item in playerInventory)
            {
                if (CardData.GetCardData(item.Key, out cardData))
                {
                    // Exception
                    if (cardData.Type == CardType.Minion_NotInDeck) continue;
                    if (cardData.SeriesKey == _seriesKey_StarChamber) continue;

                    CountCardByFaction(cardData);
                    CountCardByPack(cardData, _seriesKey_WOE);
                }
            }

            // Count Max
            CardData catalogData;
            CardListData catalogDataStack = new CardListData();        // Card Catalog Stack
            List<CatalogCardData> catalogCardDataList = DataManager.Instance.GetCardCatalog();
            foreach (CatalogCardData catalogCardData in catalogCardDataList)
            {
                CardData cardCatalogData = CardData.CreateCard(catalogCardData.ItemCatalogID);
                if (cardCatalogData != null)
                {
                    catalogDataStack.AddCard(catalogCardData.ItemCatalogID, 1);
                }
            }
            foreach (KeyValuePair<string, int> item in catalogDataStack)
            {
                if (CardData.GetCardData(item.Key, out catalogData))
                {
                    // Exception
                    if (catalogData.Type == CardType.Minion_NotInDeck) continue;
                    if (catalogData.SeriesKey == _seriesKey_StarChamber) continue;

                    CountCardMaxByFaction(catalogData);
                    CountCardMaxByPack(catalogData, _seriesKey_WOE);
                }
            }
        }

        InitData();
        //DebugReport();


    }

    private void InitData()
    {
        TEXT_AllCard.text = string.Format("{0}/{1}",GetAllCardCount(true), GetAllCardCount(false));

        Cell_Fire.SetData(_cardCount_Fire, _cardCountMax_Fire);
        Cell_Water.SetData(_cardCount_Water, _cardCountMax_Water);
        Cell_Earth.SetData(_cardCount_Earth, _cardCountMax_Earth);
        Cell_Air.SetData(_cardCount_Air, _cardCountMax_Air);
        Cell_Holy.SetData(_cardCount_Holy, _cardCountMax_Holy);
        Cell_Dark.SetData(_cardCount_Dark, _cardCountMax_Dark);
        Cell_Neutral.SetData(_cardCount_Neutral, _cardCountMax_Neutral);

        Cell_WOE.SetData(_cardCount_WOE, _cardCountMax_WOE);
    }

    public int GetCardCountByFaction(CardElementType element, bool isInventory)
    {
        int count = 0;
        switch (element)
        {
            case CardElementType.FIRE:
            {
                count = isInventory ? _cardCount_Fire : _cardCountMax_Fire;
                break;
            }
            case CardElementType.WATER:
            {
                count = isInventory ? _cardCount_Water : _cardCountMax_Water;
                break;
            }
            case CardElementType.EARTH:
            {
                count = isInventory ? _cardCount_Earth : _cardCountMax_Earth;
                break;
            }
            case CardElementType.AIR:
            {
                count = isInventory ? _cardCount_Air : _cardCountMax_Air;
                break;
            }
            case CardElementType.HOLY:
            {
                count = isInventory ? _cardCount_Holy : _cardCountMax_Holy;
                break;
            }
            case CardElementType.DARK:
            {
                count = isInventory ? _cardCount_Dark : _cardCountMax_Dark ;
                break;
            }
            case CardElementType.NEUTRAL:
            {
                count = isInventory ? _cardCount_Neutral : _cardCountMax_Neutral;
                break;
            }
        }
        return count;
    }

    private void CountCardByFaction(CardData cardData)
    {
        if (_cardBaseInventoryList.Contains(cardData.BaseID)) return;
        _cardBaseInventoryList.Add(cardData.BaseID);

        if (cardData.CardElement.IsElement(CardElementType.NEUTRAL))
        {
            _cardCount_Neutral++;
        }
        else if (cardData.CardElement.IsElement(CardElementType.FIRE))
        {
            _cardCount_Fire++;
        }
        else if (cardData.CardElement.IsElement(CardElementType.WATER))
        {
            _cardCount_Water++;
        }
        else if (cardData.CardElement.IsElement(CardElementType.EARTH))
        {
            _cardCount_Earth++;
        }
        else if (cardData.CardElement.IsElement(CardElementType.AIR))
        {
            _cardCount_Air++;
        }
        else if (cardData.CardElement.IsElement(CardElementType.HOLY))
        {
            _cardCount_Holy++;
        }
        else if (cardData.CardElement.IsElement(CardElementType.DARK))
        {
            _cardCount_Dark++;
        }
    }

    private void CountCardMaxByFaction(CardData cardData)
    {
        if (_cardBaseMaxList.Contains(cardData.BaseID)) return;
        _cardBaseMaxList.Add(cardData.BaseID);

        if (cardData.CardElement.IsElement(CardElementType.NEUTRAL))
        {
            _cardCountMax_Neutral++;
        }
        else if (cardData.CardElement.IsElement(CardElementType.FIRE))
        {
            _cardCountMax_Fire++;
        }
        else if (cardData.CardElement.IsElement(CardElementType.WATER))
        {
            _cardCountMax_Water++;
        }
        else if (cardData.CardElement.IsElement(CardElementType.EARTH))
        {
            _cardCountMax_Earth++;
        }
        else if (cardData.CardElement.IsElement(CardElementType.AIR))
        {
            _cardCountMax_Air++;
        }
        else if (cardData.CardElement.IsElement(CardElementType.HOLY))
        {
            _cardCountMax_Holy++;
        }
        else if (cardData.CardElement.IsElement(CardElementType.DARK))
        {
            _cardCountMax_Dark++;
        }
    }

    private void CountCardByPack(CardData cardData, string packName)
    {
        if (cardData.SeriesKey == packName)
        {
            if (_cardBaseInventoryList_WOE.Contains(cardData.BaseID)) return;
            _cardBaseInventoryList_WOE.Add(cardData.BaseID);
            _cardCount_WOE++;
        }
    }

    private void CountCardMaxByPack(CardData cardData, string packName)
    {
        if (cardData.SeriesKey == packName)
        {
            if (_cardBaseMaxList_WOE.Contains(cardData.BaseID)) return;
            _cardBaseMaxList_WOE.Add(cardData.BaseID);
            _cardCountMax_WOE++;
        }
    }

    private int GetAllCardCount(bool isInventory)
    {
        int count = 0;
        if (isInventory)
        {
            count += _cardCount_Fire;
            count += _cardCount_Water;
            count += _cardCount_Earth;
            count += _cardCount_Air;
            count += _cardCount_Holy;
            count += _cardCount_Dark;
            count += _cardCount_Neutral;
        }
        else
        {
            count += _cardCountMax_Fire;
            count += _cardCountMax_Water;
            count += _cardCountMax_Earth;
            count += _cardCountMax_Air;
            count += _cardCountMax_Holy;
            count += _cardCountMax_Dark;
            count += _cardCountMax_Neutral;
        }

        return count;
    }

    private int GetPackCardCount_WOE(bool isInventory)
    {
        int count = 0;
        if (isInventory)
        {
            count += _cardCount_WOE;
        }
        else
        {
            count += _cardCountMax_WOE;
        }

        return count;
    }

    public void DebugReport()
    {
        Debug.Log(string.Format("All: {0} / {1}", GetAllCardCount(true), GetAllCardCount(false)));
        Debug.Log(string.Format("NEUTRAL: {0} / {1}", GetCardCountByFaction(CardElementType.NEUTRAL, true), GetCardCountByFaction(CardElementType.NEUTRAL, false)));
        Debug.Log(string.Format("FIRE: {0} / {1}", GetCardCountByFaction(CardElementType.FIRE, true), GetCardCountByFaction(CardElementType.FIRE, false)));
        Debug.Log(string.Format("WATER: {0} / {1}", GetCardCountByFaction(CardElementType.WATER, true), GetCardCountByFaction(CardElementType.WATER, false)));
        Debug.Log(string.Format("EARTH: {0} / {1}", GetCardCountByFaction(CardElementType.EARTH, true), GetCardCountByFaction(CardElementType.EARTH, false)));
        Debug.Log(string.Format("AIR: {0} / {1}", GetCardCountByFaction(CardElementType.AIR, true), GetCardCountByFaction(CardElementType.AIR, false)));
        Debug.Log(string.Format("HOLY: {0} / {1}", GetCardCountByFaction(CardElementType.HOLY, true), GetCardCountByFaction(CardElementType.HOLY, false)));
        Debug.Log(string.Format("DARK: {0} / {1}", GetCardCountByFaction(CardElementType.DARK, true), GetCardCountByFaction(CardElementType.DARK, false)));
        Debug.Log(string.Format("WOE: {0} / {1}", GetPackCardCount_WOE(true), GetPackCardCount_WOE(false)));
    }
    #endregion
}
