﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;
using Karamucho.UI;
using System;

public class TitleEditorPanelUI : MonoBehaviour
{
    #region Event

    public static event Action OnActive;

    #endregion

    #region Public Properties
    public Button BTN_Done;
    public SkinSlotCell Prefab_SkinCell;
    public TitleDetailPanel Popup_TitleDetailPanel;

    [Header("Avatar")]
    public Transform Container_Title;
    #endregion

    #region Private Properties
    private Dictionary<string, SkinSlotCell> _titleCellList = new Dictionary<string, SkinSlotCell>();

    private string _currentTitle;
    private string _nextTitle;
    private UnityAction _onComplete;
    #endregion

    #region Methods
    public void Show(UnityAction onComplete)
    {
        _onComplete = onComplete;

        //Cached current avatar ID
        _currentTitle = DataManager.Instance.PlayerInfo.TitleID;
        _nextTitle = DataManager.Instance.PlayerInfo.TitleID;

        if (BTN_Done != null)
        {
            BTN_Done.onClick.RemoveAllListeners();
            BTN_Done.onClick.AddListener(delegate
            {
                if (_currentTitle != _nextTitle)
                {
                    PopupUIManager.Instance.Show_MidSoftLoad(true);
                    DataManager.Instance.UpdatePlayerTitleID
                    (
                        _nextTitle,
                        delegate ()
                        {
                            PopupUIManager.Instance.Show_MidSoftLoad(false);
                            Hide();
                        },
                        null
                    );
                }
                else
                {
                    Hide();
                }
            });
        }

        gameObject.SetActive(true);

        InitSkin();
        SelectTitle(DataManager.Instance.PlayerInfo.TitleID);

        OnActive?.Invoke();
    }

    public void Hide()
    {
        GameHelper.UITransition_FadeOut(gameObject);
        _onComplete?.Invoke();
    }

    public void InitSkin()
    {
        ClearAll();

        // Title
        List<TitleDBData> titleDataList = new List<TitleDBData>();
        TitleDB.Instance.GetAllData(out titleDataList);
        List<string> titleIdList = DataManager.Instance.InventoryData.TitleIDList;

        //Init Avatar Slots
        foreach (TitleDBData item in titleDataList)
        {
            bool isAvailable = titleIdList.Contains(item.TitleID);
            //Check Listing
            if (isAvailable || item.IsListing)
            {
                SkinSlotCell newCell = CreateSkinSlot(
                    item.TitleID,
                    SkinSlotCell.SkinType.Title,
                    isAvailable,
                    Container_Title,
                    delegate ()
                    {
                        Popup_TitleDetailPanel.Show(item, isAvailable,
                        delegate
                        {
                            SelectTitle(item.TitleID);
                        }
                        );
                    }
                );

                newCell.ShowGlow(_currentTitle == item.TitleID);

                _titleCellList.Add(item.TitleID, newCell);
            }
        }
    }

    public void SelectTitle(string id)
    {
        Debug.Log("Select: " + id);

        _titleCellList[_currentTitle].ShowGlow(false);
        _titleCellList[_nextTitle].ShowGlow(false);
        _nextTitle = id;
        _titleCellList[_nextTitle].ShowGlow(true);
    }
    #endregion


    #region Cell Generate
    public SkinSlotCell CreateSkinSlot(string id, SkinSlotCell.SkinType skinType, bool isAvailable, Transform parent, UnityAction onClick)
    {
        GameObject obj = Instantiate(Prefab_SkinCell.gameObject, parent);
        SkinSlotCell cell = obj.GetComponent<SkinSlotCell>();
        cell.InitData(id, skinType, isAvailable, onClick);
        return cell;
    }

    public void ClearAll()
    {
        // Clear Hero Skin
        foreach (KeyValuePair<string, SkinSlotCell> item in _titleCellList)
        {
            Destroy(item.Value.gameObject);
        }
        _titleCellList.Clear();
    }
    #endregion
}
