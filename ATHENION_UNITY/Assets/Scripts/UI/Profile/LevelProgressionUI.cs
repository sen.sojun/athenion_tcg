﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;
using DG.Tweening;

public class LevelProgressionUI : MonoBehaviour
{
    #region Public Properties
    [Header("Prefabs")]
    public GameObject Prefab_Passed;
    public GameObject Prefab_Current;
    public GameObject Prefab_Next;

    [Header("Container")]
    public Transform Container;
    #endregion

    #region Private Properties
    private List<PlayerExpDBData> _levelData = new List<PlayerExpDBData>();
    private PlayerLevelData _playerLevelData = new PlayerLevelData();
    private int _maxLevelCount = 0;

    private int _startCurrentExp = 0;
    private int _endCurrentExp = 0;

    private List<LevelCell> _cellList = new List<LevelCell>();
    private Coroutine _initTask = null;
    private LevelCell _currentLevelCell;
    private bool _isAnimating;
    #endregion

    private void OnDestroy()
    {
        if (_initTask != null)
        {
            StopCoroutine(_initTask);
            _initTask = null;
        }
    }

    private void OnEnable()
    {
        AnimationMoveToLevel();
    }

    #region Methods
    public void InitData(UnityAction onComplete)
    {
        List<PlayerExpDBData> data;
        if (PlayerExpDB.Instance.GetAllData(out data))
        {
            _levelData = new List<PlayerExpDBData>(data);
        }

        _maxLevelCount = _levelData.Count;
        _playerLevelData = DataManager.Instance.PlayerInfo.LevelData;

        _startCurrentExp = _playerLevelData.CurrentLevelAccExp;
        _endCurrentExp = _playerLevelData.NextLevelAccExp;

        if (_initTask != null)
        {
            StopCoroutine(_initTask);
            _initTask = null;
        }

        _initTask = StartCoroutine(InitCellList(onComplete));
    }

    private IEnumerator InitCellList(UnityAction onComplete)
    {
        GameObject cell;
        int indexLevel = 0;
        LevelCell levelCell;

        Dictionary<string, int> rewardData = new Dictionary<string, int>();
        string rewardKey = "";
        string rewardText = "";
        int rewardCount = 0;
        int count = 0;

        foreach (PlayerExpDBData item in _levelData)
        {
            indexLevel = item.Level;
            rewardKey = "";
            rewardText = "";
            rewardCount = 0;

            List<ItemData> itemDataList = new List<ItemData>();
            LevelRewardDBData data;
            if(LevelRewardDB.Instance.GetData(item.LevelStr, out data))
            {
                itemDataList = data.RewardItemList;
            }

            foreach (ItemData itemData in itemDataList)
            {
                rewardKey = itemData.ItemID;
                rewardCount = itemData.Amount;
                rewardText = string.Format(LocalizationManager.Instance.GetText(rewardKey), rewardCount);
                break;
            }

            if (indexLevel < _playerLevelData.Level)
            {
                cell = Instantiate(Prefab_Passed, Container);
                levelCell = cell.GetComponent<LevelCell>();

                levelCell.InitData("Lv." + indexLevel.ToString("N0"), rewardText, rewardKey, rewardCount, LevelCell.LevelStatus.Passed);
            }
            else if (indexLevel == _playerLevelData.Level)
            {
                cell = Instantiate(Prefab_Current, Container);
                levelCell = cell.GetComponent<LevelCell>();

                // Calculate EXP
                float progression = (float)(_playerLevelData.AccExp - _startCurrentExp) / (float)(_endCurrentExp - _startCurrentExp);

                levelCell.InitData("Lv." + indexLevel.ToString("N0"), rewardText, rewardKey, rewardCount, LevelCell.LevelStatus.Current, progression);
                _currentLevelCell = levelCell;
            }
            else
            {
                cell = Instantiate(Prefab_Next, Container);
                levelCell = cell.GetComponent<LevelCell>();

                levelCell.InitData("Lv." + indexLevel.ToString("N0"), rewardText, rewardKey, rewardCount, LevelCell.LevelStatus.Next);
            }

            cell.transform.SetAsLastSibling();
            _cellList.Add(levelCell);
            count++;

            if (count % 4 == 0)
            {
                yield return new WaitForEndOfFrame();
            }
        }

        if (onComplete != null) 
        {
            onComplete.Invoke();
        }

        // Play Animaiton After Generate
        AnimationMoveToLevel();

        _initTask = null;
        yield break;
    }

    #endregion

    #region Animation
    private void AnimationMoveToLevel()
    {
        if (_currentLevelCell != null)
        {
            float offset = 450.0f;
            //offset = 0;
            float targetPosY = (_currentLevelCell.GetComponent<RectTransform>().anchoredPosition.y + offset) * (-1);

            if (Container.localPosition.y != targetPosY)
            {
               // Debug.Log(targetPosY);
                if (targetPosY > 16035)
                {
                    targetPosY = 16035;
                }
                if (!_isAnimating)
                {
                    _isAnimating = true;
                    Container.DOLocalMoveY(targetPosY, 1.5f).SetEase(Ease.OutExpo).OnComplete(delegate
                    {
                        _isAnimating = false;
                    });
                }

            }

        }
    }
    #endregion
}
