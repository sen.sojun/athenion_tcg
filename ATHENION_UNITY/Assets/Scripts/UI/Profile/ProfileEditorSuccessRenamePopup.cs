﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

public class ProfileEditorSuccessRenamePopup : MonoBehaviour
{
    public TextMeshProUGUI NameText;

    public Button BTN_Close;

    public void ShowUI(string newName, UnityAction onCloseClick)
    {
        GameHelper.UITransition_FadeIn(gameObject);

        NameText.text = newName;

        BTN_Close.onClick.RemoveAllListeners();
        BTN_Close.onClick.AddListener(delegate
        {
            onCloseClick?.Invoke();
        });
    }
}
