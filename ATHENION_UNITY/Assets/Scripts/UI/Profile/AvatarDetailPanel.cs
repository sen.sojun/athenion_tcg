﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;
using Karamucho.UI;

public class AvatarDetailPanel : MonoBehaviour
{
    #region Public Properties
    public Button BTN_Okay;
    public Button BTN_Cancel;
    public SkinSlotCell OBJ_DetailSkin;
    public TextMeshProUGUI TEXT_Name;
    public TextMeshProUGUI TEXT_Detail;
    #endregion

    #region Private Properties
    private AvatarDBData _data;
    private bool _isAvailable;
    #endregion

    #region Methods
    public void Show(AvatarDBData data, bool isAvailable, UnityAction onOkay)
    {
        _data = data;
        _isAvailable = isAvailable;

        SetText();
        SetSkin();
        InitBTN(onOkay);
        gameObject.SetActive(true);

    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    private void SetSkin()
    {
        OBJ_DetailSkin.InitData(_data.AvatarID, SkinSlotCell.SkinType.Avatar, _isAvailable, null);
    }

    private void SetText()
    {
        TEXT_Name.text = LocalizationManager.Instance.GetItemName(_data.AvatarID);
        TEXT_Detail.text = LocalizationManager.Instance.GetItemDescription(_data.AvatarID);
    }

    private void InitBTN(UnityAction onOkay)
    {
        BTN_Cancel.onClick.RemoveAllListeners();
        BTN_Cancel.onClick.AddListener(Hide);

        BTN_Okay.onClick.RemoveAllListeners();
        BTN_Okay.onClick.AddListener(delegate
        {
            if (onOkay != null)
            {
                onOkay.Invoke();
            }
            Hide();
        });

        BTN_Okay.interactable = _isAvailable;
    }

    #endregion
}
