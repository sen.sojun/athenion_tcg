﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using DG.Tweening;

public class FactionLevelRewardUI : MonoBehaviour
{
    #region Public Properties
    [Header("Prefabs")]
    public GameObject Prefab_LevelCell;

    [Header("Container")]
    public Transform Container;

    public Dictionary<int, LevelProgressionCell> RewardCells { get { return _rewardDict; } }
    #endregion

    #region Private Properties
    private Dictionary<int, LevelProgressionCell> _rewardDict = new Dictionary<int, LevelProgressionCell>();
    #endregion

    #region Method
    public void InitData(CardElementType faction, int playerLevel, out int activeChildCount)
    {
        activeChildCount = 0;

        PlayerExpFactionDB.Instance.GetAllData(out List<PlayerExpFactionDBData> dataList);
        LevelFactionRewardDB.Instance.GetData(faction.ToString(), out LevelFactionRewardDBData rewData);

        int wantedDataCount = dataList.Count - 1; //Ignore level 1

        //Create new cell or update existing cell according to the data;
        int loopCount = Mathf.Max(_rewardDict.Count, wantedDataCount);
        for (int i = 1; i <= loopCount; i++)
        {
            PlayerExpFactionDBData levelData = dataList[i];
            List<ItemData> itemData = null;
            if (rewData != null) itemData = rewData.GetRewardItemList(levelData.Level);

            if (i > _rewardDict.Count)
            {
                LevelProgressionCell newCell = CreateLevelCell(levelData.Level, itemData);
                _rewardDict.Add(levelData.Level, newCell);
                activeChildCount++;
            }
            else if (i > wantedDataCount)
            {
                _rewardDict[levelData.Level].gameObject.SetActive(false);
            }
            else
            {
                _rewardDict[levelData.Level].gameObject.SetActive(true);
                _rewardDict[levelData.Level].InitData(levelData.Level.ToString("N0"), itemData);
                activeChildCount++;
            }

            if (DataManager.Instance.PlayerLevelFactionRewardRecord.IsRewardClaimed(faction, levelData.Level))
            {
                _rewardDict[levelData.Level].ShowCheckMark(true);
                _rewardDict[levelData.Level].BTN_Claimed.gameObject.SetActive(true);
                _rewardDict[levelData.Level].BTN_Get.gameObject.SetActive(false);
                _rewardDict[levelData.Level].BTN_Go.gameObject.SetActive(false);
            }
            else
            {
                _rewardDict[levelData.Level].ShowCheckMark(false);
                _rewardDict[levelData.Level].BTN_Claimed.gameObject.SetActive(false);

                if(playerLevel >= levelData.Level)
                {
                    _rewardDict[levelData.Level].BTN_Get.gameObject.SetActive(true);
                    _rewardDict[levelData.Level].BTN_Get.onClick.RemoveAllListeners();
                    _rewardDict[levelData.Level].BTN_Get.onClick.AddListener(
                        delegate 
                        {
                            //onClaim?.Invoke();
                            ProfileEditorFactionLevel.OnRewardClaimClick
                            (
                                levelData.Level,
                                delegate
                                {
                                    _rewardDict[levelData.Level].ShowCheckMark(true);
                                    _rewardDict[levelData.Level].BTN_Claimed.gameObject.SetActive(true);
                                    _rewardDict[levelData.Level].BTN_Get.gameObject.SetActive(false);
                                    _rewardDict[levelData.Level].BTN_Go.gameObject.SetActive(false);
                                }
                            );
                        }
                        );

                    _rewardDict[levelData.Level].BTN_Go.gameObject.SetActive(false);
                }
                else
                {
                    _rewardDict[levelData.Level].BTN_Get.gameObject.SetActive(false);

                    _rewardDict[levelData.Level].BTN_Go.gameObject.SetActive(true);
                    _rewardDict[levelData.Level].BTN_Go.onClick.RemoveAllListeners();
                    _rewardDict[levelData.Level].BTN_Go.onClick.AddListener(
                        delegate 
                        {
                            ProfileEditorManager.OnBTNGoToPlayClick();
                        }
                        );
                }
            }

        }

        if (playerLevel > 1 && _rewardDict.ContainsKey(playerLevel))
        {
            _rewardDict[playerLevel].ShowCellHighlight(true);
        }
    }

    public LevelProgressionCell GetLastCellInList()
    {
        int maxKey = 0;
        foreach(KeyValuePair<int, LevelProgressionCell> cell in _rewardDict)
        {
            maxKey = Mathf.Max(maxKey, cell.Key);
        }
        return _rewardDict[maxKey];
    }
    #endregion

    #region Cell Gen
    private LevelProgressionCell CreateLevelCell(int level, List<ItemData> rewardList)
    {
        GameObject obj = Instantiate(Prefab_LevelCell, Container);
        obj.SetActive(true);
        LevelProgressionCell levelCell = obj.GetComponent<LevelProgressionCell>();
        levelCell.InitData(level.ToString("N0"), rewardList);

        return levelCell;
    }
    #endregion
}
