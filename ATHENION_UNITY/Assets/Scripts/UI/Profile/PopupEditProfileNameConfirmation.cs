﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

public class PopupEditProfileNameConfirmation : MonoBehaviour
{
    public TextMeshProUGUI Text;
    public TextMeshProUGUI CurrentDiamondText;

    public TextMeshProUGUI PriceText;

    public Button Confirm;
    public Button Cancel;

    public void ShowUI(EditNameInfo info, UnityAction onConfirm, UnityAction onCancel)
    {
        Text.text = string.Format(LocalizationManager.Instance.GetText("PROFILE_DETAIL_EDIT_NAME_CONFIRMATION"), info.NewName);
        CurrentDiamondText.text = string.Format(LocalizationManager.Instance.GetText("PROFILE_EDIT_NAME_CURRENT_DIAMOND"), DataManager.Instance.InventoryData.GetVirtualCurrency(VirtualCurrency.DI));

        bool isFree = DataManager.Instance.GetPlayerRenameStatus() == false;
        PriceText.text = isFree ? LocalizationManager.Instance.GetText("BUTTON_FREE") : string.Format("{0} {1}", info.Price, GameHelper.TMPro_GetEmojiCurrency(info.Currency));

        Confirm.onClick.RemoveAllListeners();
        Confirm.onClick.AddListener(onConfirm);

        Cancel.onClick.RemoveAllListeners();
        Cancel.onClick.AddListener(onCancel);

        gameObject.SetActive(true);
    }

    public string GetPriceText(int price, VirtualCurrency currency)
    {
        return string.Format("{0} {1}", price, GameHelper.GetLocalizeCurrencyText(currency));
    }

    public void HideUI()
    {
        gameObject.SetActive(false);
    }
}
