﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using System;
using PlayFab;
using PlayFab.ClientModels;
using DG.Tweening;

public class ProfileEditorManager : MonoBehaviour
{
    #region Enums
    public enum TabType
    {
        LevelProgression
        , Ranking
        , FactionLevel
    }
    #endregion

    #region Event
    public static event Action OnActive;
    public static event Action<EditNameInfo> OnChangeName;
    public static UnityAction OnBTNGoToPlayClick;
    #endregion

    #region Public Properties
    [Header("EditName")]
    public ProfileEditorRenameConfirmation PopupEditProfileNameConfirmation;
    public PopupEditProfileName PopupEditName;
    public ProfileEditorSuccessRenamePopup PopupSuccessNameEdit;

    [Header("Ref")]
    public ProfileEditorPanel ProfilePanel;
    public ProfileEditorLevelProgression LevelProgressionUI;
    public ProfileEditorRankingPanel RankingPanelUI;
    public ProfileEditorFactionLevel FactionLevelPanelUi;
    public TitleEditorPanelUI TitlePanelUI;
    public AvatarEditorPanelUI AvatarPanelUI;
    public GameObject PopupAlphaMask;
    public Image ToggleSlider;
    public GameObject PlayerSettingPanel;

    [Header("Navigation Group")]
    public Button BTN_Level;
    public Button BTN_FactionLevel;
    public Button BTN_Ranking;
    public RectTransform IMG_MovingImage;

    [Header("BTN")]
    public Button BTN_EditTitle;
    public Button BTN_EditAvatar;
    public Button BTN_EditName;
    public Button BTN_Close;

    public bool UI_IsLevelShow { get; private set; }
    public bool UI_IsRankingShow { get; private set; }
    public bool UI_IsFactionLvlShow { get; private set; }

    #endregion

    #region Private Properties
    private TabType _currentTab = TabType.LevelProgression;
    private UnityAction _onClosePanel;
    private Tweener _navGroupImageTweener;
    private bool _initialized = false;
    #endregion

    #region BTN EVENT
    private void InitBTN()
    {
        BTN_EditName.onClick.RemoveAllListeners();
        BTN_EditName.onClick.AddListener(ShowNameEditor);
        if (AuthenticationManager.IsBeGuest())
        {
            //Guest cannot change name;
            BTN_EditName.interactable = false;
        }
        else
        {
            BTN_EditName.interactable = true;
        }

        BTN_EditTitle.onClick.RemoveAllListeners();
        if (DataManager.Instance.IsFeatureEnable(FeatureUnlock.EditTitle))
        {
            BTN_EditTitle.interactable = true;
            BTN_EditTitle.onClick.AddListener(ShowTitleEditor);
        }
        else
        {
            BTN_EditTitle.interactable = false;
        }

        BTN_EditAvatar.onClick.RemoveAllListeners();
        if (DataManager.Instance.IsFeatureEnable(FeatureUnlock.EditAvatar))
        {
            BTN_EditAvatar.interactable = true;
            BTN_EditAvatar.onClick.AddListener(ShowAvatarEditor);
        }
        else
        {
            BTN_EditAvatar.interactable = false;
        }

        BTN_Close.onClick.RemoveAllListeners();
        BTN_Close.onClick.AddListener(Close);

        BTN_Level.onClick.RemoveAllListeners();
        BTN_Level.onClick.AddListener(delegate
        {
            if (!UI_IsLevelShow)
            {
                ChangePage(TabType.LevelProgression);
            }
        });

        BTN_FactionLevel.onClick.RemoveAllListeners();
        BTN_FactionLevel.onClick.AddListener(delegate
        {
            if (!UI_IsFactionLvlShow)
            {
                ChangePage(TabType.FactionLevel);
            }
        });
        BTN_FactionLevel.interactable = DataManager.Instance.IsFeatureEnable(FeatureUnlock.FactionLevel);

        BTN_Ranking.onClick.RemoveAllListeners();
        BTN_Ranking.onClick.AddListener(delegate
        {
            if (!UI_IsRankingShow)
            {
                ChangePage(TabType.Ranking);
            }
        });
    }

    private void UpdateFeatureLockIcon()
    {
        BTN_EditTitle.interactable = DataManager.Instance.IsFeatureEnable(FeatureUnlock.EditTitle);
        BTN_EditAvatar.interactable = DataManager.Instance.IsFeatureEnable(FeatureUnlock.EditAvatar);
        BTN_FactionLevel.interactable = DataManager.Instance.IsFeatureEnable(FeatureUnlock.FactionLevel);
    }

    private void ChangePage(TabType tab)
    {
        Transform parent = null;

        switch (tab)
        {
            case TabType.LevelProgression:
                {
                    UI_IsLevelShow = true;
                    UI_IsFactionLvlShow = false;
                    UI_IsRankingShow = false;
                    parent = BTN_Level.transform;
                }
                break;

            case TabType.FactionLevel:
                {
                    UI_IsLevelShow = false;
                    UI_IsFactionLvlShow = true;
                    UI_IsRankingShow = false;
                    parent = BTN_FactionLevel.transform;
                }
                break;

            case TabType.Ranking:
                {
                    UI_IsLevelShow = false;
                    UI_IsFactionLvlShow = false;
                    UI_IsRankingShow = true;
                    parent = BTN_Ranking.transform;
                }
                break;

            default: return;
        }

        LevelProgressionUI.gameObject.SetActive(UI_IsLevelShow);
        RankingPanelUI.gameObject.SetActive(UI_IsRankingShow);
        FactionLevelPanelUi.gameObject.SetActive(UI_IsFactionLvlShow);

        if (_navGroupImageTweener != null)
        {
            _navGroupImageTweener.Kill();
        }

        IMG_MovingImage.transform.SetParent(parent);
        IMG_MovingImage.SetAsFirstSibling();
        _navGroupImageTweener = IMG_MovingImage.DOLocalMove(new Vector2(0f, 0f), 0.5f).SetEase(Ease.OutExpo);
    }

    private void Close()
    {
        _onClosePanel?.Invoke();
        GameHelper.UITransition_FadeOut(gameObject);
    }

    private void HideAlphaMask()
    {
        GameHelper.UITransition_FadeOut(PopupAlphaMask);
    }

    private void ShowAvatarEditor()
    {
        if (DataManager.Instance.IsFeatureEnable(FeatureUnlock.EditAvatar))
        {
            GameHelper.UITransition_FadeIn(PopupAlphaMask);
            GameHelper.UITransition_FadeIn(AvatarPanelUI.gameObject);
            AvatarPanelUI.Show(delegate () { HideAlphaMask(); UpdateProfile(); });
        }
    }

    private void ShowTitleEditor()
    {
        if (DataManager.Instance.IsFeatureEnable(FeatureUnlock.EditTitle))
        {
            GameHelper.UITransition_FadeIn(PopupAlphaMask);
            GameHelper.UITransition_FadeIn(TitlePanelUI.gameObject);
            TitlePanelUI.Show(delegate () { HideAlphaMask(); UpdateProfile(); });
        }
    }

    private void ShowNameEditor()
    {
        GameHelper.UITransition_FadeIn(PopupAlphaMask);
        GameHelper.UITransition_FadeIn(PopupEditName.gameObject);

        PopupEditName.NameField.text = "";
        PopupEditName.ShowUI(
            delegate (EditNameInfo data)
            {
                NameValidCase ValidCase;
                if (GameHelper.CheckValidNameContains(data.NewName, out ValidCase, NameValidCase.Valid) == false)
                {
                    SetErrorText(ValidCase);
                    return;
                }

                PopupEditProfileNameConfirmation.ShowUI(
                    data,
                    delegate
                    {
                        OnConfirmEditName(data);
                    },
                    delegate
                    {
                        PopupEditProfileNameConfirmation.HideUI();
                        PopupEditProfileNameConfirmation.Confirm.onClick.RemoveAllListeners();
                        PopupEditProfileNameConfirmation.Cancel.onClick.RemoveAllListeners();
                        PopupEditProfileNameConfirmation.HideUI();
                        HideAlphaMask();
                    }
                    );

                PopupEditName.HideUI();
            },
            delegate
            {
                PopupEditName.Confirm.onClick.RemoveAllListeners();
                PopupEditName.Cancel.onClick.RemoveAllListeners();
                PopupEditName.HideUI();
                HideAlphaMask();
            }
        );
    }
    #endregion
    #region Method
    public void UpdateProfile()
    {
        ProfilePanel.InitData();

        RankingPanelUI.ResetAllCellAvatar();
    }

    public void SetStarterTab(TabType tab)
    {
        _currentTab = tab;
    }

    public void SetOnCloseEvent(UnityAction onClose)
    {
        _onClosePanel = onClose;
    }

    public void ExitProfile()
    {
        gameObject.SetActive(false);
    }

    private void Awake()
    {
        InitializeProfileEditor();
    }

    private void OnEnable()
    {
        if (BTN_Level != null)
        {
            IMG_MovingImage.transform.SetParent(BTN_Level.transform);
            IMG_MovingImage.transform.localPosition = new Vector2(0f, 0f);
        }

        ChangePage(_currentTab);

        OnActive?.Invoke();
    }

    //private void GoToShop()
    //{
    //    PopupUIManager.Instance.Show_MidSoftLoad(true);
    //    MenuManager.Instance.OnClickDiamondButton();
    //    ExitProfile();
    //}

    private void InitializeProfileEditor()
    {
        PopupUIManager.Instance.Show_MidSoftLoad(true);

        InitBTN();
        UpdateFeatureLockIcon();

        UnityAction onCompleteTabInit = () =>
        {
            if (!_initialized)
            {
                OnBTNGoToPlayClick = () =>
                {
                    NavigatorController.Instance.ShowHomePage("lobby");
                    Close();
                };

                _initialized = true;
                PopupUIManager.Instance.Show_MidSoftLoad(false);
            }

        };

        ProfileEditorLevelProgression.OnFinishedInit += onCompleteTabInit;
        ProfileEditorRankingPanel.OnFinishedInit += onCompleteTabInit;
        ProfileEditorFactionLevel.OnFinishedInit += onCompleteTabInit;
    }

    private void SetErrorText(NameValidCase validCase)
    {
        switch (validCase)
        {
            case NameValidCase.Valid:
                break;
            case NameValidCase.SpecialName:
                PopupEditName.SetErrorText(LocalizationManager.Instance.GetText("PROFILE_EDIT_NAME_ERROR_SPECIAL_NAME"));
                break;
            case NameValidCase.SameCurrent:
                PopupEditName.SetErrorText(LocalizationManager.Instance.GetText("PROFILE_EDIT_NAME_ERROR_SAME_CURRENT_NAME"));
                break;
            case NameValidCase.Empty:
                break;
            case NameValidCase.ProfanityWords:
                PopupEditName.SetErrorText(LocalizationManager.Instance.GetText("PROFILE_EDIT_NAME_ERROR_USE_PROFANITY_WORDS"));
                break;
            case NameValidCase.SpecialAlphabet:
                PopupEditName.SetErrorText(LocalizationManager.Instance.GetText("PROFILE_EDIT_NAME_ERROR_USE_SPECIAL_ALPHABET"));
                break;
            default:
                break;
        }
    }

    private void OnConfirmEditName(EditNameInfo info)
    {
        int currentAmount = DataManager.Instance.InventoryData.GetVirtualCurrency(info.Currency);

        bool isRenameFree = DataManager.Instance.GetPlayerRenameStatus() == false;
        bool isNotEnoughMoney = !isRenameFree && currentAmount < info.Price;

        if (CheckIsBotName(info.NewName) == true)
        {
            PlayFabError error = new PlayFabError();
            error.Error = PlayFabErrorCode.NameNotAvailable;
            error.ErrorMessage = "Name not available";

            PopupUIManager.Instance.Show_MidSoftLoad(true);
            StartCoroutine(GameHelper.Delay(UnityEngine.Random.Range(.85f, 1.5f), () =>
            {
                PopupUIManager.Instance.ShowPopup_Error(error.Error.ToString(), error.ErrorMessage);
                //FeedbackUIController.ShowUI(error, FeedbackUIController.HideUI);
                PopupUIManager.Instance.Show_MidSoftLoad(false);
            })
           );
        }
        else if (isNotEnoughMoney)
        {

            EditNameError error = new EditNameError() { isNotEnoughMoney = isNotEnoughMoney, info = info };
            string title = LocalizationManager.Instance.GetText("TITLE_ERROR");
            string message = "Error : \n" + string.Format(
                      LocalizationManager.Instance.GetText("SHOP_PURCHASE_FAIL_NO_MONEY")
                    , GameHelper.GetLocalizeCurrencyText(error.info.Currency));

            string textBuy = LocalizationManager.Instance.GetText("BUTTON_BUY");
            string textCancel = LocalizationManager.Instance.GetText("BUTTON_CANCEL");
            PopupUIManager.Instance.ShowPopup_TwoChoices(title, message, textBuy, textCancel,
            delegate
            {
                NavigatorController.Instance.GotoShopPage(ShopTypes.Diamond);
            },
            delegate
            {
                PopupEditName.HideUI();
                PopupAlphaMask.gameObject.SetActive(false);
            });
            //FeedbackUIController.ShowUI(FeedbackUI.Status.Fail, title, message, FeedbackUIController.HideUI, shopButton);
        }
        else // Start rename process.
        {
            StartCoroutine(EditNameProcess(info));
        }
        PopupEditProfileNameConfirmation.HideUI();
    }

    private bool CheckIsBotName(string newName)
    {
        List<BotDBData> botList = null;
        if (BotDB.Instance.GetAllData(out botList))
        {
            BotDBData data = botList.Find(bot => bot.BotName == newName);
            if (data != null)
            {
                return true;
            }
        }
        return false;
    }

    private IEnumerator EditNameProcess(EditNameInfo info)
    {
        PopupUIManager.Instance.Show_MidSoftLoad(true);

        bool isFinish = false;
        bool isEditNameSuccess = false;
        PlayFabManager.Instance.UpdateUserTitleDisplayName(
            info.NewName,
            delegate (UpdateUserTitleDisplayNameResult result)
            {
                isFinish = true;
                isEditNameSuccess = true;
            },
            delegate (PlayFabError error)
            {
                PopupUIManager.Instance.ShowPopup_Error(error.Error.ToString(), error.ErrorMessage, () =>
                {
                    isFinish = true;
                    PopupAlphaMask.gameObject.SetActive(false);
                });
                //FeedbackUIController.ShowUI(error, () => FeedbackUIController.HideUI());
            }
        );
        yield return new WaitUntil(() => isFinish);

        if (isEditNameSuccess)
        {
            OnChangeName?.Invoke(info);
            yield return UpdateAndPurchaseRenameFeature();
            yield return UpdateProfileUIAndRefreshInventory(info);

            PopupUIManager.Instance.Show_MidSoftLoad(false);

            isFinish = false;

            /*
            string title = LocalizationManager.Instance.GetText("TITLE_SUCCESS");
            string message = string.Format(LocalizationManager.Instance.GetText("PROFILE_EDIT_NAME_DETAIL_SUCCESS"), info.NewName);
            PopupUIManager.Instance.ShowPopup_ConfirmOnlyWithCorrect(title, message,
                delegate ()
                {
                    isFinish = true;
                }
            );
            */
            PopupSuccessNameEdit.ShowUI(
                 info.NewName,
                 delegate ()
                 {
                     isFinish = true;
                     GameHelper.UITransition_FadeOut(PopupSuccessNameEdit.gameObject);
                 }
                );

            yield return new WaitUntil(() => isFinish);

            HideAlphaMask();
        }
        else
        {
            PopupUIManager.Instance.Show_MidSoftLoad(false);
        }
    }

    private IEnumerator UpdateAndPurchaseRenameFeature()
    {
        bool isFinish = false;
        DataManager.Instance.UpdateAndPurchaseRenameFeature(
            delegate ()
            {
                isFinish = true;
            },
            delegate (string error)
            {
                isFinish = true;
            }
        );
        yield return new WaitUntil(() => isFinish);
    }

    private IEnumerator UpdateProfileUIAndRefreshInventory(EditNameInfo info)
    {
        bool isLoadPlayerRenameStatusComplete = false;
        bool isLoadInventoryComplete = false;

        DataManager.Instance.LoadPlayerRenameStatus(() => isLoadPlayerRenameStatusComplete = true);
        DataManager.Instance.LoadInventory(() => isLoadInventoryComplete = true, (string errorCode) => isLoadInventoryComplete = true);
        yield return new WaitUntil(() => isLoadInventoryComplete && isLoadPlayerRenameStatusComplete);

        DataManager.Instance.UpdatePlayerDisplayName(info.NewName
            , delegate ()
            {
                UpdateProfile();
            }
            , null
        );
    }
    #endregion
}


public class EditNameError
{
    public bool isNotEnoughMoney;
    public bool isSameCurrentName;
    public EditNameInfo info;
    public PlayFabError PlayfabError;
    public bool HasError()
    {
        return isNotEnoughMoney || isSameCurrentName || PlayfabError.ErrorMessage != "";
    }
}
