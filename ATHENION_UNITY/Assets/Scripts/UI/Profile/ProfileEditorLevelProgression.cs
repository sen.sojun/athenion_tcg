﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;
using DG.Tweening;
using System;

public class ProfileEditorLevelProgression : MonoBehaviour
{
    #region Public Properties
    [Header("Prefabs")]
    public GameObject PrefabLevelcell;

    [Header("Container")]
    public Transform Container;
    public ScrollRect ScrollRect;
    public RectTransform Rect_Viewport;
    #endregion

    #region Private Properties
    private List<PlayerExpDBData> _levelData = new List<PlayerExpDBData>();
    private int _playerLevel = 0;
    private int _maxLevelCount = 0;

    public static UnityAction OnFinishedInit;
    private Dictionary<int, LevelProgressionCell> _cellDict = new Dictionary<int, LevelProgressionCell>();
    private Coroutine _initTask = null;
    private GameObject _currentLevelCell;
    private bool _isInit = false;
    private bool _isAnimating;
    #endregion

    #region Static Properties
    public static event Action OnCompleteClaimReward;
    #endregion

    private void OnDestroy()
    {
        if (_initTask != null)
        {
            StopCoroutine(_initTask);
            _initTask = null;
        }
    }

    private void OnEnable()
    {
        if (_isInit)
        {
            UpdateData(
                delegate ()
                {
                    AnimationMoveToLevel();
                }
            );
        }
        else
        {
            InitData(OnFinishedInit);
        }
    }

    #region Methods
    public void UpdateData(UnityAction onComplete)
    {
        if (_isInit)
        {
            int indexLevel = 0;
            LevelProgressionCell levelCell = null;

            Dictionary<string, int> rewardData = new Dictionary<string, int>();

            foreach (PlayerExpDBData item in _levelData)
            {
                indexLevel = item.Level;

                List<ItemData> itemDataList = new List<ItemData>();
                LevelRewardDBData data;
                if (LevelRewardDB.Instance.GetData(item.LevelStr, out data))
                {
                    itemDataList = data.RewardItemList;
                }

                bool isCurrentLvl = indexLevel == _playerLevel;

                levelCell = _cellDict[indexLevel];
                levelCell.InitData(indexLevel.ToString("N0"), itemDataList);
                levelCell.ShowCellHighlight(isCurrentLvl);
            }
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void InitData(UnityAction onComplete)
    {
        List<PlayerExpDBData> data;
        if (PlayerExpDB.Instance.GetAllData(out data))
        {
            _levelData = new List<PlayerExpDBData>(data);
        }

        _maxLevelCount = _levelData.Count;
        _playerLevel = DataManager.Instance.PlayerInfo.LevelData.Level;

        if (_initTask != null)
        {
            StopCoroutine(_initTask);
            _initTask = null;
        }

        _initTask = StartCoroutine(InitCellList(onComplete));
    }

    private IEnumerator InitCellList(UnityAction onComplete)
    {
        if (_cellDict != null && _cellDict.Count > 0)
        {
            foreach (KeyValuePair<int, LevelProgressionCell> item in _cellDict)
            {
                Destroy(item.Value.gameObject);
            }
            _cellDict.Clear();
        }
        
        int count = 0;

        foreach (PlayerExpDBData item in _levelData)
        {
            List<ItemData> itemDataList = new List<ItemData>();
            LevelRewardDBData data;
            if (LevelRewardDB.Instance.GetData(item.LevelStr, out data))
            {
                itemDataList = data.RewardItemList;
            }

            int indexLevel = item.Level;
            bool isCurrentLvl = indexLevel == _playerLevel;

            GameObject cell = Instantiate(PrefabLevelcell, Container);
            LevelProgressionCell levelCell = cell.GetComponent<LevelProgressionCell>();
            levelCell.InitData(indexLevel.ToString("N0"), itemDataList);
            levelCell.ShowCellHighlight(isCurrentLvl);
            levelCell.ShowCheckMark(_playerLevel >= indexLevel);

            if (itemDataList.Count <= 0)
            {
                levelCell.BTN_Claimed.gameObject.SetActive(false);
                levelCell.BTN_Get.gameObject.SetActive(false);
                levelCell.BTN_Go.gameObject.SetActive(false);
            }
            else
            {
                if (DataManager.Instance.PlayerLevelRewardRecord.IsRewardClaimed(indexLevel))
                {
                    levelCell.ShowCheckMark(true);
                    levelCell.BTN_Claimed.gameObject.SetActive(true);
                    levelCell.BTN_Get.gameObject.SetActive(false);
                    levelCell.BTN_Go.gameObject.SetActive(false);
                }
                else
                {
                    levelCell.ShowCheckMark(false);
                    levelCell.BTN_Claimed.gameObject.SetActive(false);

                    if (_playerLevel >= indexLevel)
                    {
                        levelCell.BTN_Get.gameObject.SetActive(true);
                        levelCell.BTN_Get.onClick.RemoveAllListeners();
                        levelCell.BTN_Get.onClick.AddListener(
                            delegate
                            {
                                ClaimLevelReward(
                                    indexLevel,
                                    delegate
                                    {
                                        levelCell.ShowCheckMark(true);
                                        levelCell.BTN_Claimed.gameObject.SetActive(true);
                                        levelCell.BTN_Get.gameObject.SetActive(false);
                                        levelCell.BTN_Go.gameObject.SetActive(false);
                                    }
                                    );
                            }
                            );

                        levelCell.BTN_Go.gameObject.SetActive(false);
                    }
                    else
                    {
                        levelCell.BTN_Get.gameObject.SetActive(false);

                        levelCell.BTN_Go.gameObject.SetActive(true);
                        levelCell.BTN_Go.onClick.RemoveAllListeners();
                        levelCell.BTN_Go.onClick.AddListener(
                            delegate
                            {
                                ProfileEditorManager.OnBTNGoToPlayClick();
                            }
                            );
                    }
                }
            }

            cell.transform.SetAsLastSibling();
            _cellDict.Add(indexLevel, levelCell);

            count++;

            if (count % 4 == 0)
            {
                yield return new WaitForEndOfFrame();
            }
        }

        _isInit = true;
        if (onComplete != null)
        {
            onComplete.Invoke();
        }

        // Play Animaiton After Generate
        AnimationMoveToLevel();
        
        ScrollRect.onValueChanged.AddListener(OnScrolling);

        _initTask = null;
        yield break;
    }
    
    private void ClaimLevelReward(int level, UnityAction onComplete = null)
    {
        List<ItemData> itemDataList = new List<ItemData>();
        LevelRewardDBData data;
        if (LevelRewardDB.Instance.GetData(level.ToString(), out data))
        {
            itemDataList = data.RewardItemList;
        }
        else
        {
            return;
        }

        if(itemDataList.Count == 0)
        {
            return;
        }

        PopupUIManager.Instance.Show_MidSoftLoad(true);
        DataManager.Instance.CheckServerLevelReward(
            level,
            //onComplete
            delegate
            {
                PopupUIManager.Instance.Show_MidSoftLoad(false);
                PopupUIManager.Instance.ShowPopup_GotReward(LocalizationManager.Instance.GetText("TEXT_GOTREWARDTITLE"),
                                                            LocalizationManager.Instance.GetText("TEXT_YOUVEGOTREWARD")
                                                            , itemDataList);

                OnCompleteClaimReward?.Invoke();
                onComplete?.Invoke();
                DataManager.Instance.LoadInventory(null, null);
            },
            //onFail
            delegate (string failMsg)
            {
                PopupUIManager.Instance.Show_MidSoftLoad(false);
                PopupUIManager.Instance.ShowPopup_Error("ERROR", failMsg);
            }
            );
    }

    private void ToggleVisibleCell()
    {
        RectTransform rect_cell = null;

        foreach (KeyValuePair<int, LevelProgressionCell> cell in _cellDict)
        {
            rect_cell = cell.Value.GetComponent<RectTransform>();
            if (GameHelper.IsRectTouchRect(rect_cell, Rect_Viewport))
            {
                cell.Value.gameObject.SetActive(true);
            }
            else
            {
                cell.Value.gameObject.SetActive(false);
            }
        }
    }
    #endregion

    #region Events
    private void OnScrolling(Vector2 value)
    {
        Container.GetComponent<ContentSizeFitter>().enabled = false;
        Container.GetComponent<GridLayoutGroup>().enabled = false;

        ToggleVisibleCell();
    }
    #endregion

    #region Animation
    private void AnimationMoveToLevel()
    {
        if (_currentLevelCell != null)
        {
            float offset = 450.0f;
            //offset = 0;
            float targetPosY = (_currentLevelCell.GetComponent<RectTransform>().anchoredPosition.y + offset) * (-1);

            if (Container.localPosition.y != targetPosY)
            {
                // Debug.Log(targetPosY);
                if (targetPosY > 16035)
                {
                    targetPosY = 16035;
                }
                if (!_isAnimating)
                {
                    _isAnimating = true;
                    Container.DOLocalMoveY(targetPosY, 1.5f).SetEase(Ease.OutExpo).OnComplete(delegate
                    {
                        _isAnimating = false;
                    });
                }

            }

        }
    }
    #endregion
}
