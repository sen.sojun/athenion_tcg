﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LevelProgressionCell_MultiReward : MonoBehaviour
{
    #region Public Properties
    [Header("Public Properties")]
    public Color TextColorNormal;
    public Color TextColorCurrent;

    [Header("Message")]
    public TextMeshProUGUI TEXT_Level;
    public TextMeshProUGUI LBL_Level;

    [Header("Ref")]
    public RewardCell Prefab_RewardCell;
    public RectTransform Container_reward;
    public Image CurrentHighlightBorder;
    #endregion

    #region Private Properties
    private List<RewardCell> _rewardList;
    #endregion

    #region Method
    public void InitData(string level, List<ItemData> itemDataList, bool isCurrent)
    {
        //Set Level
        if (TEXT_Level != null)
        {
            TEXT_Level.text = level;
        }

        //Set Reward Detail    
        if (_rewardList != null && itemDataList.Count > 0)
        {
            int loopCount = Mathf.Max(_rewardList.Count, itemDataList.Count);
            for(int i = 0; i < loopCount; i++)
            {
                if (i >= _rewardList.Count)
                {
                    _rewardList.Add(CreateReward(itemDataList[i].ItemID, itemDataList[i].Amount, isCurrent));
                }
                else if (i >= itemDataList.Count)
                {
                    _rewardList[i].gameObject.SetActive(false);
                }
                else
                {
                    UpdateReward(_rewardList[i], itemDataList[i].ItemID, itemDataList[i].Amount, isCurrent);
                    _rewardList[i].gameObject.SetActive(true);
                }
            }
        }
        else
        {
            _rewardList = new List<RewardCell>();
            if (itemDataList != null)
            {
                foreach (ItemData item in itemDataList)
                {
                    _rewardList.Add(CreateReward(item.ItemID, item.Amount, isCurrent));
                }
            }
        }
        
        SetCurrentHighlight(isCurrent);
    }

    public void SetCurrentHighlight(bool isCurrent)
    {
        if (isCurrent)
        {
            LBL_Level.color = TextColorCurrent;
            TEXT_Level.color = TextColorCurrent;
        }
        else
        {
            LBL_Level.color = TextColorNormal;
            TEXT_Level.color = TextColorNormal;
        }

        CurrentHighlightBorder.gameObject.SetActive(isCurrent);
        
        //Set all reward text color
        foreach (RewardCell item in _rewardList)
        {
            item.TEXT_RewardCount.color = isCurrent ? TextColorCurrent : TextColorNormal;
        }
    }

    public void ShowClaimedMask(bool show)
    {
        foreach (RewardCell item in _rewardList)
        {
            item.ShowClaimedMask(show);
        }
    }
    #endregion

    #region Cell Generate
    private RewardCell CreateReward(string rewardKey, int count, bool isCurrent)
    {
        GameObject obj = Instantiate(Prefab_RewardCell.gameObject, Container_reward);
        obj.SetActive(true);
        RewardCell cell = obj.GetComponent<RewardCell>();
        cell.InitData(rewardKey, count);
        cell.TEXT_RewardCount.color = isCurrent ? TextColorCurrent : TextColorNormal;
        return cell;
    }
    private void UpdateReward(RewardCell cell, string rewardKey, int count, bool isCurrent)
    {
        cell.InitData(rewardKey, count);
        cell.TEXT_RewardCount.color = isCurrent ? TextColorCurrent : TextColorNormal;
    }
    #endregion

}
