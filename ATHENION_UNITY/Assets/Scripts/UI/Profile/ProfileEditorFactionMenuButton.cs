﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using DG.Tweening;

public class ProfileEditorFactionMenuButton : MonoBehaviour
{
    #region Public Properties
    [Header("Public Properties")]
    public CardElementType ElementFaction;

    [Header("Ref")]
    public TextMeshProUGUI TEXT_Level;
    public TextMeshProUGUI TEXT_FactionExp;
    public Slider Slider_XPBar;
    public Image IMG_FlagGlow;
    public Button BTN_SelfClickable;
    #endregion

    #region Private Properties
    [SerializeField] private ProfileEditorFactionLevel _factionController;
    private Sequence _glowAnimation = null;
    #endregion

    public void UpdateData()
    {
        LevelFactionData flData = DataManager.Instance.PlayerInfo.LevelFactionData.GetLevelFactionData(ElementFaction);
        if (flData == null)
        {
            flData = new LevelFactionData(0);
        }

        TEXT_Level.text = flData.Level.ToString();

        float playerExp = (flData.AccExp - flData.CurrentLevelAccExp);
        float requiredExpForThisLvl = (flData.NextLevelAccExp - flData.CurrentLevelAccExp);
        if (requiredExpForThisLvl == 0 || flData.NextLevelAccExp == 0)
        {
            Slider_XPBar.value = 1f;
            TEXT_FactionExp.text = LocalizationManager.Instance.GetText("PROFILE_FACTION_LEVEL_MAX");
        }
        else
        {
            Slider_XPBar.value = playerExp / requiredExpForThisLvl;
            TEXT_FactionExp.text = string.Format("{0}/{1}", playerExp, requiredExpForThisLvl);
        }

        BTN_SelfClickable.onClick.RemoveAllListeners();
        BTN_SelfClickable.onClick.AddListener
            (
                delegate 
                {
                    _factionController.ToggleLevelReward(ElementFaction, out bool active);
                    ShowFlagGlow(active);
                }
            );
    }

    public void ShowFlagGlow(bool show)
    {
        if(_glowAnimation != null)
        {
            _glowAnimation.Kill();
            _glowAnimation = null;
        }

        if(show)
        {
            _glowAnimation = GameHelper.UITransition_FadeIn(IMG_FlagGlow.gameObject);
        }
        else
        {
            _glowAnimation = GameHelper.UITransition_FadeOut(IMG_FlagGlow.gameObject);
        }
    }

}
