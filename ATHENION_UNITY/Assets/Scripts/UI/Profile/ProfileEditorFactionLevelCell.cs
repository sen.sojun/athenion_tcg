﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using DG.Tweening;

public class ProfileEditorFactionLevelCell : MonoBehaviour
{
    #region Public Properties
    [Header("Message")]
    public TextMeshProUGUI TEXT_FactionName;
    public TextMeshProUGUI TEXT_Level;
    public TextMeshProUGUI TEXT_NextLevel;
    public TextMeshProUGUI TEXT_WinCount;

    [Header("Ref")]
    public Image IMG_FactionFlag;
    public RewardCell RewardCell_NextReward;
    public Slider SLIDER_Exp;
    public GameObject CompletedMask;
    public Button ClickableButton;
    public Button BTN_Go;
    public Button BTN_Get;
    #endregion

    #region Method
    public void InitData(CardElementType faction, LevelFactionData fLevel, bool isClaimable, bool isCompleted, Sprite flag = null
                    , UnityAction onCellClick = null, UnityAction onGetClick = null)
    {
        string factName = LocalizationManager.Instance.GetText("FACTION_" + faction.ToString());
        TEXT_FactionName.text = factName;

        TEXT_Level.text = fLevel.Level.ToString();
        TEXT_NextLevel.text = fLevel.NextLevel.ToString();

        int totalWinCount = 0;
        List<GameMode> includedModes = new List<GameMode> { GameMode.Casual, GameMode.Rank, GameMode.Arena};
        foreach(GameMode mode in includedModes)
        {
            DataManager.Instance.PlayerInfo.GetPlayerPlayScore(mode, faction, out int playCount, out int winCount, out float winRate);
            totalWinCount += winCount;
        }
        TEXT_WinCount.text = string.Format(LocalizationManager.Instance.GetText("PROFILE_FACTION_LEVEL_WINCOUNT"), totalWinCount.ToString());

        if (fLevel.AccExp != 0)
        {
            float playerExp = (fLevel.AccExp - fLevel.CurrentLevelAccExp);
            float requiredExpForThisLvl = (fLevel.NextLevelAccExp - fLevel.CurrentLevelAccExp);
            SLIDER_Exp.value = playerExp / requiredExpForThisLvl;
        }
        else
        {
            SLIDER_Exp.value = 0;
        }

        if(flag != null) IMG_FactionFlag.sprite = flag;
        
        if (LevelFactionRewardDB.Instance.GetData(faction.ToString(), out LevelFactionRewardDBData rewData))
        {
            List<ItemData> itemData = rewData.GetRewardItemList(fLevel.Level + 1);
            if (itemData.Count > 0)
            {
                RewardCell_NextReward.InitData(itemData[0].ItemID, itemData[0].Amount);
                RewardCell_NextReward.gameObject.SetActive(true);
            }
            else
            {
                RewardCell_NextReward.gameObject.SetActive(false);
            }
        }
        else
        {
            RewardCell_NextReward.gameObject.SetActive(false);
        }

        if (onCellClick != null)
        {
            ClickableButton.onClick.RemoveAllListeners();
            ClickableButton.onClick.AddListener(onCellClick);
        }

        if (onGetClick != null)
        {
            BTN_Get.onClick.RemoveAllListeners();
            BTN_Get.onClick.AddListener(onGetClick);
        }

        BTN_Go.onClick.RemoveAllListeners();
        BTN_Go.onClick.AddListener(GoToPlay);

        UpdateButton(isClaimable);

        CompletedMask.SetActive(!isClaimable && isCompleted);
    }

    public void UpdateButton(bool claimable)
    {
        BTN_Get.gameObject.SetActive(claimable);
        BTN_Go.gameObject.SetActive(!claimable);
    }

    private void GoToPlay()
    {
        ProfileEditorManager.OnBTNGoToPlayClick?.Invoke();
    }
    #endregion

}
