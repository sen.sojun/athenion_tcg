﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LevelCell : MonoBehaviour
{
    #region Enum 
    public enum LevelStatus
    {
        Passed
        , Current
        , Next
    }
    #endregion

    #region Public Properties
    [Header ("Message")]
    public TextMeshProUGUI TEXT_Level;
    public TextMeshProUGUI TEXT_Detail;
    public TextMeshProUGUI TEXT_RewardCount;

    [Header("Ref")]
    public GameObject Icon_Group;
    public Image RewardImage;
    public Slider SLIDER_Progression;
    public TooltipAttachment Tooltip;
    #endregion

    #region Method
    public void InitData(string level, string detailKey,string imageKey, int rewardCount, LevelStatus levelStatus,  float progression = 1.0f)
    {
        switch (levelStatus)
        {
            case LevelStatus.Passed:
            {
                SetProgress(1.0f);
                break;
            }

            case LevelStatus.Current:
            {
                SetProgress(progression);
                break;
            }

            case LevelStatus.Next:
            {
                SetProgress(0.0f);
                break;
            }
        }

        SetLevel(level);
        SetDetail(detailKey);
        SetRewardCount(imageKey ,rewardCount);  
    }

    private void SetLevel(string level)
    {
        if (TEXT_Level != null) TEXT_Level.text = level;
    }

    private void SetDetail(string detailKey)
    {
        string prefix = "";
        if (detailKey != "")
        {
            prefix = "ITEM_NAME_";
        }
        if (TEXT_Detail != null) TEXT_Detail.text = LocalizationManager.Instance.GetText(prefix + detailKey);
    }

    private void SetRewardCount(string imageKey ,int count)
    {
        Icon_Group.SetActive(count > 0);
        if (count > 0)
        {
            if (TEXT_RewardCount != null) TEXT_RewardCount.text = count.ToString("N0");
            SetImage(imageKey);
        }
    }

    private void SetProgress(float value)
    {
        if (SLIDER_Progression != null) SLIDER_Progression.value = value;
    }

    private void SetImage(string imageKey)
    {
        Sprite sprite = SpriteResourceHelper.LoadSprite(imageKey, SpriteResourceHelper.SpriteType.ICON_Item);
        if (RewardImage != null) RewardImage.sprite = sprite;

        Tooltip.Setup(imageKey);
    }
    #endregion

}
