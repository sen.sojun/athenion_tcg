﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ProfilePanel : MonoBehaviour
{
    #region Public Properties
    [Header("Ref")]
    public TextMeshProUGUI TEXT_PlayerName;
    public TextMeshProUGUI TEXT_RankName;
    public TextMeshProUGUI TEXT_VIPExpire;
    public Button EditNameButton;
    public Image IMG_Avatar;
    public Image IMG_RankFrame;
    public Image IMG_Title;
    public GameObject Group_VIP;

    private float _timer = 0.0f;
    #endregion

    private void Update()
    {
        UpdateVIPExpire();
    }

    private void OnEnable()
    {
        DataManager.Instance.AddListenerOnVIPUpdate(OnVIPUpdate);
    }

    private void OnDisable()
    {
        DataManager.Instance.RemoveListenerOnVIPUpdate(OnVIPUpdate);
    }

    #region Methods
    public void InitData()
    {
        SetPlayerName();
        SetAvatar();
        SetRank();
        SetTitle();

        UpdateVIPExpire();
    }

    private void SetPlayerName()
    {
        string message = DataManager.Instance.PlayerInfo.DisplayName;
        TEXT_PlayerName.text = message;
    }

    private void SetRank()
    {
        string rankId = GameHelper.GetRankID(DataManager.Instance.GetPlayerRankIndex());
        string rankName = GameHelper.GetRankName(rankId);

        IMG_RankFrame.sprite = SpriteResourceHelper.LoadSprite(rankId, SpriteResourceHelper.SpriteType.ICON_Rank);
        TEXT_RankName.text = rankName;
    }

    private void SetAvatar()
    {
        string key = DataManager.Instance.PlayerInfo.AvatarID;
        Sprite sprite = SpriteResourceHelper.LoadSprite(key, SpriteResourceHelper.SpriteType.Avatar);
        IMG_Avatar.sprite = sprite;
    }

    private void SetTitle()
    {
        string key = DataManager.Instance.PlayerInfo.TitleID;
        Sprite sprite = SpriteResourceHelper.LoadSprite(key, SpriteResourceHelper.SpriteType.Title);
        IMG_Title.sprite = sprite;
    }

    private void UpdateVIPExpire()
    {
        if (DataManager.Instance.InventoryData.VIPData.IsVIP())
        {
            DateTime vipExpire = DataManager.Instance.InventoryData.VIPData.ExpiredDateTime;
            TimeSpan remainingValue = vipExpire.Subtract(DateTimeData.GetDateTimeUTC());

            string vipText = "";
            if (remainingValue.Days > 0)
            {
                vipText = string.Format(
                      LocalizationManager.Instance.GetText("PROFILE_VIP_EXPIRE_DATE_TIME")
                    , remainingValue.Days
                    , (remainingValue.Days > 1) ? "s" : ""
                    , remainingValue.Hours
                    , remainingValue.Minutes
                    , remainingValue.Seconds
                );
            }
            else
            {
                vipText = string.Format(
                      LocalizationManager.Instance.GetText("PROFILE_VIP_EXPIRE_TIME")
                    , remainingValue.Hours
                    , remainingValue.Minutes
                    , remainingValue.Seconds
                );
            }

            TEXT_VIPExpire.text = vipText;
            Group_VIP.gameObject.SetActive(true);
            TEXT_VIPExpire.gameObject.SetActive(true);
        }
        else
        {
            Group_VIP.gameObject.SetActive(false);
            TEXT_VIPExpire.gameObject.SetActive(false);
        }
    }
    #endregion

    #region Event Methods
    private void OnVIPUpdate()
    {
        UpdateVIPExpire();
    }
    #endregion
}
