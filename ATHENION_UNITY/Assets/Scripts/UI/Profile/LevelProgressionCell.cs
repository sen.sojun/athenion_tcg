﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;

public class LevelProgressionCell : MonoBehaviour
{
    #region Public Properties
    [Header("Public Properties")]
    public GameObject Prefab_RewardBox;

    [Header("Message")]
    public TextMeshProUGUI LABEL_Level;
    public TextMeshProUGUI TEXT_Level;

    [Header("Ref")]
    public GameObject Icon_Group;
    public Transform Container;
    public Image RewardImage;
    public Image CurrentHighlightBorder;
    public TooltipAttachment Tooltip;

    public Button BTN_Get;
    public Button BTN_Go;
    public Button BTN_Claimed;

    public UnityAction OnEnableEvent;
    public UnityAction OnDisableEvent;
    #endregion

    #region Private Properties
    private List<RewardCell_SeasonPass> _rewardList = new List<RewardCell_SeasonPass>();
    #endregion

    #region Events
    public void OnEnable()
    {
        OnEnableEvent?.Invoke();
    }

    public void OnDisable()
    {
        OnDisableEvent?.Invoke();
    }
    #endregion

    #region Method
    public void InitData(string level, List<ItemData> itemDataList)
    {
        //Set Level
        if (TEXT_Level != null)
        {
            TEXT_Level.text = level;
        }

        //Set Image
        SetRewardData(itemDataList);

        ShowCellHighlight(false);
    }

    public void ShowCheckMark(bool isCheck)
    {
        foreach (RewardCell_SeasonPass item in _rewardList)
        {
            item.ShowCheckIcon(isCheck);
            //item.ShowBigCheckIcon(isCheck);
        }
    }

    public void ShowCellHighlight(bool isCurrent)
    {
        CurrentHighlightBorder.gameObject.SetActive(isCurrent);
    }

    private void SetRewardData(List<ItemData> itemDataList)
    {
        int loopCount = Mathf.Max(_rewardList.Count, itemDataList.Count);
        for (int i = 0; i < loopCount; i++)
        {
            if (i >= _rewardList.Count)
            {
                _rewardList.Add(CreateReward(itemDataList[i].ItemID, itemDataList[i].Amount, false));
            }
            else if (i >= itemDataList.Count)
            {
                _rewardList[i].gameObject.SetActive(false);
            }
            else
            {
                _rewardList[i].InitData(itemDataList[i].ItemID, itemDataList[i].Amount);
                _rewardList[i].gameObject.SetActive(true);
            }
        }

    }
    #endregion

    #region Cell Gen
    private RewardCell_SeasonPass CreateReward(string rewardKey, int count, bool isCurrent)
    {
        GameObject obj = Instantiate(Prefab_RewardBox.gameObject, Container);
        obj.SetActive(true);
        RewardCell_SeasonPass cell = obj.GetComponent<RewardCell_SeasonPass>();
        cell.InitData(rewardKey, count);
        return cell;
    }
    #endregion

}
