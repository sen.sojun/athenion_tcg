﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProfileAvatar : MonoBehaviour
{
    private void OnEnable()
    {
        string key = DataManager.Instance.PlayerInfo.AvatarID;
        GetComponent<Image>().sprite = SpriteResourceHelper.LoadSprite(key, SpriteResourceHelper.SpriteType.Avatar);
    }
}
