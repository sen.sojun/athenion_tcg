﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;
using DG.Tweening;

//Faction Level Panel Manager
public class ProfileEditorFactionLevel : MonoBehaviour
{
    #region Public Properties
    [Header("Prefabs")]
    public GameObject Prefab_LevelFactionCell;

    [Header("Container")]
    public Transform Container;

    [Header("Ref")]
    public FactionLevelRewardUI FactionRewardUI;
    public RectTransform BTN_ToLastLevelNav;
    public RectTransform Rect_ViewPort;
    public RectTransform Rect_Container;
    public ScrollRect ScrollRect_Scroll;
    public List<ProfileEditorFactionMenuButton> FactionMenuButton;

    [Header("Show Order")]
    public List<CardElementType> FactionShowOrder;
    [Header("Flag Sprite (Should be ordered according to Show Order)")]
    public List<Sprite> FactionSprite;
    #endregion

    #region Private Properties
    private List<PlayerExpDBData> _levelData = new List<PlayerExpDBData>();
    private Dictionary<CardElementType, ProfileEditorFactionLevelCell> _factionCellDict = new Dictionary<CardElementType, ProfileEditorFactionLevelCell>();
    private bool _isAnimating;
    private bool _isInitialized = false;
    private CardElementType _currentShownReward;
    private Tweener _factionTween = null;
    private LevelProgressionCell _lastCellCache = null;
    private Sequence _lastNavAnimSeq = null;
    #endregion

    public static UnityAction OnFinishedInit;
    public static UnityAction OnCompletedRecieveReward;
    public static UnityAction<int, UnityAction> OnRewardClaimClick;

    #region Method
    public void ToggleLevelReward(CardElementType faction, out bool active)
    {
        active = true;

        RectTransform factionRewardRect = FactionRewardUI.GetComponent<RectTransform>();

        if (FactionRewardUI.gameObject.activeSelf)
        {
            if(_currentShownReward == faction)
            {
                if (_factionTween != null)
                {
                    _factionTween.Kill();
                }
                _factionTween = factionRewardRect.DOSizeDelta(new Vector2(factionRewardRect.sizeDelta.x, 0f), 0.5f).SetEase(Ease.OutCirc);
                _factionTween.OnComplete(
                    delegate
                    {
                        FactionRewardUI.gameObject.SetActive(false);
                        LayoutRebuilder.ForceRebuildLayoutImmediate(factionRewardRect);
                    }
                    );
                
                DisableLastLevelNav();

                active = false;

                return;
            }
        }
        _currentShownReward = faction;

        FactionRewardUI.gameObject.SetActive(true);

        OnRewardClaimClick = (int level, UnityAction onComplete) =>
        {
            ClaimFactionReward(faction, level, onComplete);
        };

        //Reset Button Highlight
        foreach (ProfileEditorFactionMenuButton btn in FactionMenuButton)
        {
            btn.ShowFlagGlow(btn.ElementFaction == _currentShownReward);
        }

        //init or reset Data
        LevelFactionData flData = DataManager.Instance.PlayerInfo.LevelFactionData.GetLevelFactionData(faction);
        if (flData == null)
        {
            flData = new LevelFactionData(0);
        };
        FactionRewardUI.InitData(faction, flData.Level,  out int activeChild);
        //Return last cell level
        if(_lastCellCache != null)
        {
            _lastCellCache.OnDisableEvent -= OnLastCellDisable;
            _lastCellCache.OnEnableEvent -= OnLastCellEnable;
            _lastCellCache = null;
        }
        //_lastCellCache = FactionRewardUI.RewardCells[activeChild];

        //reset anim
        RectTransform cellRect = FactionRewardUI.Prefab_LevelCell.GetComponent<RectTransform>();
        
        if (_factionTween != null)
        {
            _factionTween.Kill();
        }

        ScrollRect_Scroll.verticalNormalizedPosition = 1f;
        factionRewardRect.sizeDelta = new Vector2(factionRewardRect.sizeDelta.x, 0f);
        _factionTween = factionRewardRect.DOSizeDelta(new Vector2(factionRewardRect.sizeDelta.x, activeChild * cellRect.sizeDelta.y), 0.5f);
        _factionTween.OnComplete
            (
                delegate 
                {
                    LayoutRebuilder.ForceRebuildLayoutImmediate(factionRewardRect);

                    UpdateLastCellNav();
                    EnableLastLevelNav(true);

                    _lastCellCache = FactionRewardUI.GetLastCellInList();//Cache the last cell so we will always know which cell we subcribed;
                    _lastCellCache.OnDisableEvent += OnLastCellDisable;
                    _lastCellCache.OnEnableEvent += OnLastCellEnable;
                }
            );
    }

    private void SetupData()
    {
        if (DataManager.Instance.IsFeatureEnable(FeatureUnlock.FactionLevel))
        {
            if (!_isInitialized)
            {
                _isInitialized = true;
                OnFinishedInit?.Invoke();

                Button btnLastPrize = BTN_ToLastLevelNav.GetComponent<Button>();
                if (btnLastPrize != null)
                {
                    btnLastPrize.onClick.RemoveAllListeners();
                    btnLastPrize.onClick.AddListener(OnLastPrizeClick);
                }

                ScrollRect_Scroll.onValueChanged.AddListener(OnScrolling);
            }
            foreach (ProfileEditorFactionMenuButton btn in FactionMenuButton)
            {
                btn.UpdateData();
            }
            //Default Page
            if(!FactionRewardUI.gameObject.activeInHierarchy)
            {
                ToggleLevelReward(FactionMenuButton[0].ElementFaction, out bool active);
                FactionMenuButton[0].ShowFlagGlow(true);
            }
        }

    }

    private bool IsClaimableReward(CardElementType faction, out List<ItemData> rewardList)
    {
        rewardList = new List<ItemData>();

        LevelFactionData flData = DataManager.Instance.PlayerInfo.LevelFactionData.GetLevelFactionData(faction);
        if (flData == null) return false; //Null data means never play this faction and also means no reward sorry bro :D
        
        LevelFactionRewardDB.Instance.GetData(faction.ToString(), out LevelFactionRewardDBData rewardData);

        for (int i = 1; i <= flData.Level; i++)
        {
            List<ItemData> itemData = null;
            if (rewardData != null)
            {
                itemData = rewardData.GetRewardItemList(i);

                if (DataManager.Instance.PlayerLevelFactionRewardRecord.IsRewardClaimed(faction, i))
                {
                    //Claimed
                    continue;
                }
                else
                {
                    //Claimable
                    rewardList.AddRange(itemData);
                }
            }
        }

        return rewardList.Count > 0;
    }

    private bool IsClaimableRewardByLevel(CardElementType faction, int level, out List<ItemData> rewardList)
    {
        bool claimable = false;

        rewardList = new List<ItemData>();

        LevelFactionData flData = DataManager.Instance.PlayerInfo.LevelFactionData.GetLevelFactionData(faction);
        if (flData == null) return false; //Null data means never play this faction and also means no reward sorry bro :D

        LevelFactionRewardDB.Instance.GetData(faction.ToString(), out LevelFactionRewardDBData rewData);
        
        if (!DataManager.Instance.PlayerLevelFactionRewardRecord.IsRewardClaimed(faction, level))
        {
            rewardList = rewData.GetRewardItemList(level);
            claimable = true;
        }

        return claimable && rewardList.Count > 0;
    }

    private void ClaimAllClaimableFactionReward(CardElementType faction)
    {
        if(IsClaimableReward(faction, out List<ItemData> allRewardList))
        {
            PopupUIManager.Instance.Show_MidSoftLoad(true);
            PlayerLevelFactionRewardRecordData cacheData = new PlayerLevelFactionRewardRecordData(DataManager.Instance.PlayerLevelFactionRewardRecord);
            DataManager.Instance.CheckServerLevelFactionReward(
                faction.ToString(),
                //onComplete
                delegate(List<ItemData> reward)
                {
                    LevelFactionData flData = DataManager.Instance.PlayerInfo.LevelFactionData.GetLevelFactionData(faction);
                    if (flData != null)
                    {
                        for (int i = 1; i <= flData.Level; i++)
                        {
                            if (cacheData.IsRewardClaimed(faction, i) == false
                                && DataManager.Instance.PlayerLevelFactionRewardRecord.IsRewardClaimed(faction, i) == true)
                            {
                                FactionRewardUI.InitData(faction, i, out int activeChildCount);
                            }
                        }
                    }

                    PopupUIManager.Instance.Show_MidSoftLoad(false);
                    PopupUIManager.Instance.ShowPopup_GotReward(LocalizationManager.Instance.GetText("TEXT_GOTREWARDTITLE"), 
                                                                LocalizationManager.Instance.GetText("TEXT_YOUVEGOTREWARD")
                                                                , reward);

                    _factionCellDict[faction].UpdateButton(false);
                    
                    OnCompletedRecieveReward?.Invoke();
                    DataManager.Instance.LoadInventory(null, null);
                },
                //onFail
                delegate(string failMsg)
                {
                    PopupUIManager.Instance.Show_MidSoftLoad(false);
                    PopupUIManager.Instance.ShowPopup_Error("ERROR", failMsg);
                }
                );
        }
    }
    
    private void ClaimFactionReward(CardElementType faction, int level, UnityAction onComplete = null)
    {
        if (IsClaimableRewardByLevel(faction, level, out List<ItemData> rewardList))
        {
            PopupUIManager.Instance.Show_MidSoftLoad(true);
            DataManager.Instance.CheckServerLevelFactionReward(
                faction.ToString(),
                level,
                //onComplete
                delegate
                {
                    PopupUIManager.Instance.Show_MidSoftLoad(false);
                    PopupUIManager.Instance.ShowPopup_GotReward(LocalizationManager.Instance.GetText("TEXT_GOTREWARDTITLE"),
                                                                LocalizationManager.Instance.GetText("TEXT_YOUVEGOTREWARD")
                                                                , rewardList);
                    
                    OnCompletedRecieveReward?.Invoke();
                    onComplete?.Invoke();
                    DataManager.Instance.LoadInventory(null, null);
                },
                //onFail
                delegate (string failMsg)
                {
                    PopupUIManager.Instance.Show_MidSoftLoad(false);
                    PopupUIManager.Instance.ShowPopup_Error("ERROR", failMsg);
                }
                );
        }
    }

    private void DisableLastLevelNav(bool playTrans = false)
    {
        if(_lastNavAnimSeq != null)
        {
            _lastNavAnimSeq.Pause();
            _lastNavAnimSeq.Kill();
        }

        if (playTrans)
        {
            BTN_ToLastLevelNav.DOAnchorPosY(-248, 0.1f).OnComplete(delegate { BTN_ToLastLevelNav.gameObject.SetActive(false); UpdateLastCellNav(); });
        }
        else
        {
            BTN_ToLastLevelNav.anchoredPosition = new Vector3(0f, -248, 0f);
            BTN_ToLastLevelNav.gameObject.SetActive(false);
            UpdateLastCellNav();
        }
    }

    private void EnableLastLevelNav(bool playTrans = false)
    {
        if (_lastNavAnimSeq != null)
        {
            _lastNavAnimSeq.Pause();
            _lastNavAnimSeq.Kill();
        }

        if (playTrans)
        {
            BTN_ToLastLevelNav.gameObject.SetActive(true);
            BTN_ToLastLevelNav.DOAnchorPosY(0f, 0.1f);
        }
        else
        {
            BTN_ToLastLevelNav.gameObject.SetActive(true);
            BTN_ToLastLevelNav.anchoredPosition = new Vector3(0f, 0f, 0f);
        }
    }

    private void ToggleVisibleCell()
    {
        RectTransform rect_cell = null;

        foreach (KeyValuePair<int, LevelProgressionCell> cell in FactionRewardUI.RewardCells)
        {
            rect_cell = cell.Value.GetComponent<RectTransform>();
            if (GameHelper.IsRectTouchRect(rect_cell, Rect_ViewPort))
            {
                cell.Value.gameObject.SetActive(true);
            }
            else
            {
                cell.Value.gameObject.SetActive(false);
            }
        }
    }

    private void UpdateLastCellNav()
    {
        Sprite btnImage = null;
        string imageName = string.Format("{0}_Last", _currentShownReward.ToString());
        btnImage = SpriteResourceHelper.LoadSprite(imageName, SpriteResourceHelper.SpriteType.FactLvl_CustomImage, true);
        Image BTNImageComp = BTN_ToLastLevelNav.GetComponent<Image>();
        if(BTNImageComp != null && btnImage != null)
        {
            BTNImageComp.sprite = btnImage;
        }
    }
    #endregion

    #region Event
    private void OnScrolling(Vector2 value)
    {
        ContentSizeFitter csfComp = Rect_Container.GetComponent<ContentSizeFitter>();
        VerticalLayoutGroup vlgComp = Rect_Container.GetComponent<VerticalLayoutGroup>();

        if (csfComp != null) csfComp.enabled = false;
        if (vlgComp != null) vlgComp.enabled = false;

        ToggleVisibleCell();
    }

    private void OnLastCellEnable()
    {
        DisableLastLevelNav(true);
    }

    private void OnLastCellDisable()
    {
        EnableLastLevelNav(true);
    }
    
    private void OnLastPrizeClick()
    {
        ScrollRect_Scroll.DOVerticalNormalizedPos(0f, 0.35f);
        DisableLastLevelNav(true);
    }

    private void OnEnable()
    {
        SetupData();
    }

    private void OnDisable()
    {

    }
    #endregion
}
