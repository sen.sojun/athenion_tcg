﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

public class ProfileEditorRenameConfirmation : MonoBehaviour
{
    public TextMeshProUGUI Text;
    public TextMeshProUGUI CurrentDiamondText;

    public TextMeshProUGUI PriceText;

    public Button Confirm;
    public Button Cancel;

    public void ShowUI(EditNameInfo info, UnityAction onConfirm, UnityAction onCancel)
    {
        GameHelper.UITransition_FadeIn(gameObject);

        //Text.text = string.Format(LocalizationManager.Instance.GetText("PROFILE_DETAIL_EDIT_NAME_CONFIRMATION"), info.NewName);
        Text.text = info.NewName;
        CurrentDiamondText.text = string.Format(LocalizationManager.Instance.GetText("PROFILE_EDIT_NAME_CURRENT_DIAMOND"), GameHelper.TMPro_GetEmojiCurrency(VirtualCurrency.DI) + " " + DataManager.Instance.InventoryData.GetVirtualCurrency(VirtualCurrency.DI));

        bool isFree = DataManager.Instance.GetPlayerRenameStatus() == false;
        PriceText.text = isFree ? LocalizationManager.Instance.GetText("BUTTON_FREE") : string.Format("{0} {1}", info.Price, GameHelper.TMPro_GetEmojiCurrency(info.Currency));

        Confirm.onClick.RemoveAllListeners();
        Confirm.onClick.AddListener(onConfirm);

        Cancel.onClick.RemoveAllListeners();
        Cancel.onClick.AddListener(onCancel);
        
    }

    public string GetPriceText(int price, VirtualCurrency currency)
    {
        return string.Format("{0} {1}", price, GameHelper.GetLocalizeCurrencyText(currency));
    }

    public void HideUI()
    {
        Confirm.onClick.RemoveAllListeners();
        Cancel.onClick.RemoveAllListeners();
        GameHelper.UITransition_FadeOut(gameObject);
    }
}
