﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;
using Karamucho.UI;
using DG.Tweening;

public class PopupRewardDetail : MonoBehaviour
{
    #region Public Properties
    public Button BTN_Okay;
    public Button BTN_Cancel;

    [Header("TEXT")]
    public TextMeshProUGUI TEXT_Title;
    public TextMeshProUGUI TEXT_Sub;
    public TextMeshProUGUI TEXT_Detail;
    #endregion

    #region Public Methods
    public void Show(string head, string sub, string detail)
    {
        gameObject.SetActive(true);
        SetBTN();
        SetTEXT(head, sub, detail);

        gameObject.GetComponent<CanvasGroup>().alpha = 0;
        gameObject.GetComponent<CanvasGroup>().DOFade(1.0f, 0.3f);
    }

    public void Hide()
    {
        gameObject.GetComponent<CanvasGroup>().DOFade(0.0f, 0.3f).OnComplete(delegate
        {
            gameObject.SetActive(false);
        });

    }
    #endregion

    #region Private Method
    private void SetTEXT(string head, string sub, string detail)
    {
        TEXT_Title.text = head;
        TEXT_Sub.text = sub;
        TEXT_Detail.text = detail;
    }

    private void SetBTN()
    {
        BTN_Okay.onClick.RemoveAllListeners();
        BTN_Okay.onClick.AddListener(Hide);
    }
    #endregion
}
