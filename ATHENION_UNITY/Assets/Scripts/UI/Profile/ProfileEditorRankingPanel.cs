﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Linq;
using TMPro;

public class ProfileEditorRankingPanel : MonoBehaviour
{
    #region Public Properties
    [Header("Prefabs")]
    public ProfileEditorRankingCell Prefab_RankCell;

    [Header("Ref")]
    public Transform Container;
    public TextMeshProUGUI TEXT_RankHead;
    #endregion

    #region Private Properties
    private Dictionary<int, ProfileEditorRankingCell> _rankCellDict = new Dictionary<int, ProfileEditorRankingCell>();
    private int _cachedRankIndex = -1;
    #endregion

    public static UnityAction OnFinishedInit;

    #region Method
    private void OnEnable()
    {
        // Check if CurrentRank is in range
        RankDB.Instance.GetAllData(out List<RankDBData> RankDBList);
        RankDB.Instance.GetRank(DataManager.Instance.GetPlayerRankIndex(), out RankDBData playerRankData);

        if (_rankCellDict.Count > 0)
        {
            if(_cachedRankIndex != playerRankData.RankIndex)
            {
                _rankCellDict[_cachedRankIndex].SetActiveStatus(false);
                _rankCellDict[playerRankData.RankIndex].SetActiveStatus(true);
                _cachedRankIndex = playerRankData.RankIndex;
            }

            // Loop Update All Rank
            {
                int cRankGroup = -5;
                foreach (RankDBData item in RankDBList)
                {
                    if (cRankGroup != item.GroupIndex)
                    {
                        _rankCellDict[item.GroupIndex].InitData(
                              item.RankIndex
                            , item.RankID
                            , GameHelper.GetRankName(item.RankID)
                            , item.GroupIndex == playerRankData.GroupIndex
                        );
                        cRankGroup = item.GroupIndex;
                    }
                }
            }
            return;
        }
        
        _cachedRankIndex = playerRankData.RankIndex;

        // Loop All Rank
        {
            int cRankGroup = -5;
            foreach (RankDBData item in RankDBList)
            {
                if (cRankGroup != item.GroupIndex)
                {
                    _rankCellDict.Add(item.GroupIndex, CreateCell(item.RankIndex, item.GroupIndex == playerRankData.GroupIndex));
                    cRankGroup = item.GroupIndex;
                }
            }
        }

        SetText();

        OnFinishedInit?.Invoke();
    }

    private void SetText()
    {
        string message = LocalizationManager.Instance.GetText("TEXT_RANK_SEASON");
        if (TEXT_RankHead != null) TEXT_RankHead.text = message + " " + DataManager.Instance.GetCurrentSeasonIndex().ToString();
    }

    public void ResetAllCellAvatar()
    {
        string key = DataManager.Instance.PlayerInfo.AvatarID;
        Sprite avatar = SpriteResourceHelper.LoadSprite(key, SpriteResourceHelper.SpriteType.Avatar);

        foreach (KeyValuePair<int, ProfileEditorRankingCell> item in _rankCellDict)
        {
            item.Value.SetAvatar(avatar);
        }
    }
    #endregion

    #region Generate Cell
    private ProfileEditorRankingCell CreateCell(int rankIndex, bool isActive)
    {
        GameObject obj = Instantiate(Prefab_RankCell.gameObject, Container);
        obj.SetActive(true);
        ProfileEditorRankingCell cell = obj.GetComponent<ProfileEditorRankingCell>();

        string rankId = GameHelper.GetRankID(rankIndex);
        string rankName = GameHelper.GetRankName(rankId);

        cell.InitData(rankIndex, rankId, rankName, isActive);

        return cell;
    }
    #endregion

}
