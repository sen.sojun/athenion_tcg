﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;
using Karamucho.UI;
using System;

public class AvatarEditorPanelUI : MonoBehaviour
{
    #region Event

    public static event Action OnActive;

    #endregion

    #region Public Properties
    public Button BTN_Done;
    public SkinSlotCell Prefab_SkinCell;
    public AvatarDetailPanel Popup_AvatarDetailPanel;

    [Header("Avatar")]
    public Transform Container_Avatar;
    #endregion

    #region Private Properties
    private Dictionary<string,SkinSlotCell> _avatarCellList = new Dictionary<string, SkinSlotCell>();

    private string _currentAvatar;
    private string _nextAvatar;
    private UnityAction _onComplete;
    #endregion

    #region Methods
    public void Show(UnityAction onComplete)
    {
        _onComplete = onComplete;

        //Cached current avatar ID
        _currentAvatar = DataManager.Instance.PlayerInfo.AvatarID;
        _nextAvatar = DataManager.Instance.PlayerInfo.AvatarID;

        if (BTN_Done != null)
        {
            BTN_Done.onClick.RemoveAllListeners();
            BTN_Done.onClick.AddListener(delegate
            {
                if (_currentAvatar != _nextAvatar)
                {
                    PopupUIManager.Instance.Show_MidSoftLoad(true);
                    DataManager.Instance.UpdatePlayerAvatarID
                    (
                        _nextAvatar,
                        delegate ()
                        {
                            PopupUIManager.Instance.Show_MidSoftLoad(false);
                            Hide();
                        },
                        null
                    );
                }
                else
                {
                    Hide();
                }
            });
        }

        gameObject.SetActive(true);

        InitSkin();
        SelectAvatar(DataManager.Instance.PlayerInfo.AvatarID);

        OnActive?.Invoke();
    }

    public void Hide()
    {
        GameHelper.UITransition_FadeOut(gameObject);
        _onComplete?.Invoke();
    }

    public void InitSkin()
    {
        ClearAll();

        // Title
        List<AvatarDBData> avatarDataList = new List<AvatarDBData>();
        AvatarDB.Instance.GetAllData(out avatarDataList);
        List<string> avatarIdList = DataManager.Instance.InventoryData.AvatarIDList;
        
        //Init Avatar Slots
        foreach (AvatarDBData item in avatarDataList)
        {
            bool isAvailable = avatarIdList.Contains(item.AvatarID);
            //Check Listing
            if (isAvailable || item.IsListing)
            {
                SkinSlotCell newCell = CreateSkinSlot(
                    item.AvatarID, 
                    SkinSlotCell.SkinType.Avatar,
                    isAvailable, 
                    Container_Avatar, 
                    delegate()
                    {
                        Popup_AvatarDetailPanel.Show(item, isAvailable, 
                        delegate
                        {
                            SelectAvatar(item.AvatarID);
                        }
                        );
                    }
                );

                newCell.ShowGlow(_currentAvatar == item.AvatarID);

                _avatarCellList.Add(item.AvatarID, newCell);
            }
        }
    }

    public void SelectAvatar(string id)
    {
        Debug.Log("Select: " + id);
        
        _avatarCellList[_currentAvatar].ShowGlow(false);
        _avatarCellList[_nextAvatar].ShowGlow(false);
        _nextAvatar = id;
        _avatarCellList[_nextAvatar].ShowGlow(true);
    }
    #endregion


    #region Cell Generate
    public SkinSlotCell CreateSkinSlot(string id, SkinSlotCell.SkinType skinType, bool isAvailable, Transform parent, UnityAction onClick)
    {
        GameObject obj = Instantiate(Prefab_SkinCell.gameObject, parent);
        SkinSlotCell cell = obj.GetComponent<SkinSlotCell>();
        cell.InitData(id, skinType, isAvailable, onClick);
        return cell;
    }

    public void ClearAll()
    {
        // Clear Hero Skin
        foreach (KeyValuePair<string, SkinSlotCell> item in _avatarCellList)
        {
            Destroy(item.Value.gameObject);
        }
        _avatarCellList.Clear();
    }
    #endregion
}
