﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;

public class UIPopupBox : MonoBehaviour
{
    #region Public Properties
    public TextMeshProUGUI MessageText;
    public Button OkButton;
    public TextMeshProUGUI OkButtonText;
    public Button CloseButton;
    #endregion

    #region Private Properties
    private UnityAction _onClickOK = null;
    #endregion

    /*
    // Start is called before the first frame update
    void Start()
    {        
    }

    // Update is called once per frame
    void Update()
    {        
    }
    */

    public void ShowUI(string message, string buttonText, UnityAction onClickOK, bool isEnableClose = true)
    {
        MessageText.text = message;
        OkButtonText.text = buttonText;
        _onClickOK = onClickOK;

        OkButton.onClick.RemoveAllListeners();
        OkButton.onClick.AddListener(OnClickOK);
        OkButton.SendMessage("OnEnable", SendMessageOptions.DontRequireReceiver);

        CloseButton.gameObject.SetActive(isEnableClose);
        GameHelper.UITransition_FadeIn(this.gameObject);
    }

    public void HideUI()
    {
        GameHelper.UITransition_FadeOut(this.gameObject);
    }

    public void OnClickOK()
    {
        if (_onClickOK != null)
        {
            _onClickOK.Invoke();
        }
    }
}
