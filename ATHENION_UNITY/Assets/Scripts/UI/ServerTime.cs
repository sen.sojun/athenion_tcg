﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ServerTime : MonoBehaviour
{
    private void OnEnable()
    {
        string text = "Server Time";

        Text unityText = GetComponent<Text>();
        if (unityText)
        {
            unityText.text = text;
        }

        TextMeshProUGUI textMeshProUGUIText = GetComponent<TextMeshProUGUI>();
        if (textMeshProUGUIText)
        {
            textMeshProUGUIText.text = text;
        }

        TextMeshPro TextMeshProText = GetComponent<TextMeshPro>();
        if (TextMeshProText)
        {
            TextMeshProText.text = text;
        }
    }
}
