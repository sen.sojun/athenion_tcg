﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;
using System;

public class MainBanner : MonoBehaviour, IBeginDragHandler, IDragHandler, IDropHandler, IEndDragHandler
{
    public static event Action<BannerData> OnClickBannerEvent;

    #region Public Properties
    public GridLayoutGroup GridGroup;
    public GridLayoutGroup DotGroup;
    public DotPage PrefabDot;
    public GameObject BannerPrefab;
    #endregion

    #region Private Properties
    private ScrollRect _scrollRect;
    private Vector2 _cellSize;
    private int _bannerCount;
    private float _currentPosition;
    private int _currentIndex;

    private Sequence _sq;
    private float _moveDuration = 0.8f;
    private float _delayCycle = 5.0f;
    private float _time = 0;

    private float _startX;
    private bool _isDrag = false;
    private bool _isDraged = false;

    private List<BannerUI> bannerList = new List<BannerUI>();
    private List<DotPage> DotList = new List<DotPage>();
    #endregion

    #region Methods
    private void Start()
    {
        SetupData();
        _scrollRect = GetComponent<ScrollRect>();
        _sq = DOTween.Sequence();
        _cellSize = GridGroup.cellSize;
        _currentIndex = 0;


        SetBannerPosition_x(_currentIndex);
        InitDot();
    }

    public void SetupData()
    {
        CreateBanner(GetBannerData());
    }

    public virtual List<BannerData> GetBannerData()
    {
        return DataManager.Instance.GetMainBannerDataList();
    }

    private void CreateBanner(List<BannerData> list)
    {
        for (int i = 0; i < list.Count; i++)
        {
            if (list[i].IsValidDate == false)
                continue;
            BannerUI banner = Instantiate(BannerPrefab, GridGroup.transform).GetComponent<BannerUI>();
            banner.SetData(list[i], OnClickBanner);
            bannerList.Add(banner);
        }
    }

    public void OnClickBanner(BannerData bannerData)
    {
        if (bannerData.CustomData == null)
        {
            NavigatorController.Instance.SendMessage(bannerData.FunctionName, SendMessageOptions.DontRequireReceiver);
        }
        else
        {
            NavigatorController.Instance.SendMessage(bannerData.FunctionName, bannerData.CustomData, SendMessageOptions.DontRequireReceiver);
        }
        OnClickBannerEvent?.Invoke(bannerData);
    }

    private void Update()
    {
        AutoScroll();
        UpdateDot();
    }

    private void UpdateDot()
    {
        if (DotGroup.gameObject.activeSelf == false)
            return;
        foreach (DotPage item in DotList)
        {
            if (item.Index == _currentIndex)
            {
                item.ShowProgress(_time, _delayCycle);
            }
            else
            {
                item.HideProgress();
            }
        }
    }

    private void InitDot()
    {
        _bannerCount = GridGroup.transform.childCount;
        for (int i = 0; i < _bannerCount; i++)
        {
            GameObject dot = Instantiate(PrefabDot.gameObject, DotGroup.transform);
            DotPage dotPage = dot.GetComponent<DotPage>();
            dotPage.Setup(i);
            DotList.Add(dotPage);
        }
        if (_bannerCount <= 1)
        {
            DotGroup.gameObject.SetActive(false);
        }
    }

    private void AutoScroll()
    {
        if (!_isDrag)
        {
            if (_time < _delayCycle)
            {
                _time += Time.deltaTime;
            }
            else
            {
                ResetTime();
                NextPage();

                SetBannerPosition_x(_currentIndex);
            }
        }
    }

    private void ResetTime()
    {
        _time = 0.0f;
    }

    private void SetBannerPosition_x(int index, bool isAnimate = true)
    {
        float posX = 0;
        posX = index * (-_cellSize.x);

        if (isAnimate)
        {
            _sq.Kill();
            _sq = DOTween.Sequence();
            _sq.Append(GridGroup.transform.DOLocalMoveX(posX, _moveDuration).SetEase(Ease.OutExpo));
        }
        else
        {
            GridGroup.transform.localPosition = new Vector2(posX, 0);
        }
    }

    public void NextPage()
    {
        if (_currentIndex < _bannerCount - 1)
        {
            _currentIndex++;
        }
        else
        {
            _currentIndex = 0;
        }
    }

    public void PreviousPage()
    {
        if (_currentIndex > 0)
        {
            _currentIndex--;
        }
        else
        {
            _currentIndex = _bannerCount - 1;
        }
    }

    #endregion

    #region Events
    public void OnBeginDrag(PointerEventData eventData)
    {
        _isDrag = true;
        ResetTime();
        _startX = eventData.position.x;
        //Debug.Log("Begin Drag");
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (!_isDrag)
        {
            return;
        }

        if (eventData.position.x < _startX)
        {
            //next
            if (_currentIndex == _bannerCount - 1)
            {
                return;
            }
            else
            {
                _isDrag = false;
                NextPage();
                SetBannerPosition_x(_currentIndex);
                _scrollRect.velocity = Vector2.zero;
            }

        }
        else if (eventData.position.x > _startX)
        {
            //previous
            if (_currentIndex == 0)
            {
                return;
            }
            else
            {
                _isDrag = false;
                PreviousPage();
                SetBannerPosition_x(_currentIndex);
                _scrollRect.velocity = Vector2.zero;
            }

        }


        //Debug.Log("Drag");
    }

    public void OnDrop(PointerEventData eventData)
    {
        //Debug.Log("Drop");
        _isDrag = false;
        _scrollRect.velocity = Vector2.zero;


    }

    public void OnEndDrag(PointerEventData eventData)
    {
        _isDrag = false;
        _scrollRect.velocity = Vector2.zero;
    }


    #endregion
}
