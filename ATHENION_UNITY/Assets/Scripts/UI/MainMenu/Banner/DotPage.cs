﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DotPage : MonoBehaviour
{
    public int Index { get; private set; }
    public Image DOT_Progress;
    public Image DOT_Status;

    public Sprite Sprite_Active;
    public Sprite Sprite_Disable;

    private void Start()
    {
        HideProgress();
    }

    public void Setup(int index)
    {
        Index = index;
    }

    public void ShowProgress(float current, float max)
    {
        DOT_Progress.fillAmount = current / max;
        DOT_Status.sprite = Sprite_Active;
    }

    public void HideProgress()
    {
        DOT_Progress.fillAmount = 0;
        DOT_Status.sprite = Sprite_Disable;
    }
}
