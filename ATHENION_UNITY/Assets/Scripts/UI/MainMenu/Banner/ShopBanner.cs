﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopBanner : MainBanner
{
    public override List<BannerData> GetBannerData()
    {
        return DataManager.Instance.GetShopBannerDataList();
    }
}
