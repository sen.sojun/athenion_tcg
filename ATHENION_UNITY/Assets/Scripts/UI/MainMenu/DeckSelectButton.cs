﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;
using Karamucho;

[RequireComponent(typeof(Button))]
public class DeckSelectButton : MonoBehaviour 
{
    public Image HeroImage;
    public TextMeshProUGUI ButtonText;
    public Image Glow;
    public GameObject ERROR;
    public int Index { get { return _index; } }
    public string DeckName { get; private set; }
    public string HeroID { get; private set; }

    private int _index;
    private UnityAction _onClick;

    /*
	// Use this for initialization
	void Start () 
    {		
	}
    */

    /*
	// Update is called once per frame
	void Update () 
    {		
	}
    */

    public void OnSelected(bool isSelected)
    {
        Glow.gameObject.SetActive(isSelected);
    }

    /*
    private void OnEnable()
    {
        Button button = GetComponent<Button>();
        button.onClick.AddListener(OnClickSelect);
    }

    private void OnDisable()
    {
        Button button = GetComponent<Button>();
        button.onClick.RemoveListener(OnClickSelect);
    }
    */

    public void Setup(int index, string text, string heroID, bool isValid, UnityAction onClick)
    {
        _index = index;

        if (HeroImage != null)
        {
            string key = heroID;
            Sprite sprite = SpriteResourceHelper.LoadSprite(key, SpriteResourceHelper.SpriteType.Hero_Square);
            HeroImage.sprite = sprite;
        }
        if (ButtonText != null)
        {
            ButtonText.text = text;
            DeckName = text;
            HeroID = heroID;
        }        

        _onClick = onClick;

        Button button = GetComponent<Button>();
        button.onClick.RemoveAllListeners();
        button.onClick.AddListener(OnClickSelect);

        // Deck Valid
        ERROR.gameObject.SetActive(!isValid);
    }

    private void OnClickSelect()
    {
        SoundManager.PlaySFX(SoundManager.SFX.End_Game_Button_Click);

        if (_onClick != null)
        {
            _onClick.Invoke();
        }
    }
}
