﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationMenuController : MonoBehaviour
{
    #region Event 
    public delegate void AnimationMenuControllerHandler();
    public static event AnimationMenuControllerHandler OnClickOnlineButton;
    public static event AnimationMenuControllerHandler OnClickSoloButton;
    #endregion

    public enum MenuState
    {
        MAIN,
        PLAY_ONLINE,
        PLAY_SOLO,
        OPTION,
        SHOP
    }
    #region Public Properties
    public Animator MainMenuAnimator;
    #endregion

    #region Private Properties
    private MenuState _menuState;
    #endregion

    #region Methods
    private void Start()
    {
        _menuState = MenuState.MAIN;
    }

    private void Update()
    {
        KeyInputBack();
    }

    private void KeyInputBack()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            switch (_menuState)
            {
                case MenuState.PLAY_ONLINE:
                    {
                        Goto_Main();
                        break;
                    }
                case MenuState.PLAY_SOLO:
                    {
                        Goto_Main();
                        break;
                    }
            }
        }

    }

    public void Goto_Main()
    {
        MainMenuAnimator.SetBool("IsOnline", false);
        MainMenuAnimator.SetBool("IsSolo", false);

        _menuState = MenuState.MAIN;
    }

    public void Goto_OnlinePlay()
    {
        if (_menuState != MenuState.PLAY_ONLINE)
        {
            MainMenuAnimator.SetBool("IsOnline", true);
            MainMenuAnimator.SetBool("IsSolo", false);
            _menuState = MenuState.PLAY_ONLINE;
            OnClickOnlineButton?.Invoke();
        }
        else
        {
            Goto_Main();
        }
    }

    public void Goto_SoloPlay()
    {
        if (_menuState != MenuState.PLAY_SOLO)
        {
            MainMenuAnimator.SetBool("IsSolo", true);
            MainMenuAnimator.SetBool("IsOnline", false);
            _menuState = MenuState.PLAY_SOLO;
            OnClickSoloButton?.Invoke();
        }
        else
        {
            Goto_Main();
        }
    }
    #endregion
}
