﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FriendMatchStatus : MonoBehaviour
{
    public GameObject STATUS_Ready;
    public GameObject STATUS_NotReady;

    public void SetReady(bool isReady)
    {
        STATUS_Ready.SetActive(isReady);
        STATUS_NotReady.SetActive(!isReady);
    }
}
