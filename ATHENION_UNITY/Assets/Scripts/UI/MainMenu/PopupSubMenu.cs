﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class PopupSubMenu : MonoBehaviour
{
    #region Inspector
    [SerializeField] private Button BTN_Close;
    [SerializeField] private GridLayoutGroup LAYOUT_Grid;
    #endregion

    public void Show()
    {
        InitBTN();
        GameHelper.UITransition_FadeIn(this.gameObject);
        StartCoroutine(SpacingAnimation());
    }

    public void Hide()
    {
        StopAllCoroutines();
        GameHelper.UITransition_FadeOut(this.gameObject);
    }

    #region Methods
    private void InitBTN()
    {
        BTN_Close.onClick.RemoveAllListeners();
        BTN_Close.onClick.AddListener(Hide);
    }
    #endregion

    #region Animation
    private IEnumerator SpacingAnimation()
    {
        // Setup
        Vector2 defaultSpacing = new Vector2(LAYOUT_Grid.cellSize.x, 0);
        float frameCount = 8.0f;
        float speedPerFrame = defaultSpacing.x/ frameCount;
        LAYOUT_Grid.spacing = new Vector2(-defaultSpacing.x, 0);

        // Animate
        while (LAYOUT_Grid.spacing.x < 0)
        {
            LAYOUT_Grid.spacing += new Vector2(speedPerFrame, 0);
            yield return new WaitForEndOfFrame();
        }

        // Reset
        LAYOUT_Grid.spacing = new Vector2(0, 0);
    }
    #endregion
}
