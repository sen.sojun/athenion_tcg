﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;
using Karamucho.Collection;

public class UIPopupCardChange : MonoBehaviour
{
    #region Inspector

    [Header("Card")]
    [SerializeField] private GameObject GROUP_Card;
    [SerializeField] private TextMeshProUGUI TEXT_Description;
    [SerializeField] private CardUI_Collection CardOld;
    [SerializeField] private CardUI_Collection CardCurrent;

    [Header("Hilight")]
    [SerializeField] private CollectionCardChangeHilight CardHilight;

    [Header("BTN")]
    [SerializeField] private Button BTN_Back;
    [SerializeField] private Button BTN_Next;
    [SerializeField] private Button BTN_Confirm;

    [Header("Dot Group")]
    [SerializeField] private GameObject DotPrefab;
    [SerializeField] private Transform GROUP_Dot;
    [SerializeField] private Color COLOR_Normal;
    [SerializeField] private Color COLOR_Active;
    [SerializeField] private TextMeshProUGUI TEXT_Page;
    #endregion

    #region Private Properties
    private List<CardUIData> _oldCardList = new List<CardUIData>();
    private List<CardUIData> _currentCardList = new List<CardUIData>();
    private int _currentPage = 0;
    private int _maxPage = 0;
    private List<Image> _dotList = new List<Image>();
    private UnityAction _onCloseAction;
    #endregion

    #region Methods
    public void ShowUI(CardChangeLogDetail logDetail, List<CardUIData> oldCardList, List<CardUIData> currentCardList, UnityAction onShowComplete = null)
    {
        //GameHelper.UITransition_FadeIn(this.gameObject, onShowComplete);
        TEXT_Description.text = string.Format(LocalizationManager.Instance.GetText("TEXT_CARD_CHANGE_DESCRIPTION"), logDetail.EndDate.ToString(GameHelper.DateTimeFormatSeason));
        GameHelper.UITransition_FadeIn(this.gameObject);
        InitData(oldCardList, currentCardList);
        InitBTN();
        InitPageType();

        RefreshPage();

        _onCloseAction = onShowComplete;
    }

    public void HideUI()
    {
        GameHelper.UITransition_FadeOut(this.gameObject, _onCloseAction);
        _onCloseAction = null;
    }

    private void InitBTN()
    {
        BTN_Back.onClick.RemoveAllListeners();
        BTN_Back.onClick.AddListener(PreviousPage);

        BTN_Next.onClick.RemoveAllListeners();
        BTN_Next.onClick.AddListener(NextPage);

        BTN_Confirm.onClick.RemoveAllListeners();
        BTN_Confirm.onClick.AddListener(HideUI);
    }

    private void InitData(List<CardUIData> oldCardList, List<CardUIData> currentCardList)
    {
        _oldCardList = oldCardList;
        _currentCardList = currentCardList;
        _maxPage = _currentCardList.Count;
        _currentPage = 0;
    }

    private void UpdateCard()
    {
        GameHelper.UITransition_FadeIn(GROUP_Card);

        CardOld.SetData(_oldCardList[_currentPage]);
        CardOld.SetCardAmount(1);
        CardCurrent.SetData(_currentCardList[_currentPage]);
        CardCurrent.SetCardAmount(1);

        CardChangeTracker tracker = new CardChangeTracker(_oldCardList[_currentPage], _currentCardList[_currentPage]);
        CardHilight.SetHilightCardChange(tracker);

        StopAllCoroutines();
        StartCoroutine(UpdateHilightBox());
    }

    private IEnumerator UpdateHilightBox()
    {
        yield return new WaitForEndOfFrame();

        float height;
        height = CardCurrent.GetDescriptionHeight();
        CardHilight.SetDescriptionBoxSize(height);
    }
    #endregion

    #region Navigator
    private void RefreshPage()
    {
        RefreshDotPages();
        RefreshPageNumber();
        UpdateCard();
    }

    private void RefreshDotPages()
    {
        BTN_Back.gameObject.SetActive(_currentPage != 0);
        BTN_Next.gameObject.SetActive(_currentPage != _maxPage - 1);

        // Create Dot
        while (_dotList.Count < _maxPage)
        {
            CreateDotPage();
        }

        // Change Dot Color
        for (int i = 0; i < _dotList.Count; i++)
        {
            if (i == _currentPage)
            {
                _dotList[i].color = COLOR_Active;
            }
            else
            {
                _dotList[i].color = COLOR_Normal;
            }
        }
    }

    private void RefreshPageNumber()
    {
        TEXT_Page.text = string.Format("{0} / {1}", _currentPage + 1, _maxPage);
    }

    private void InitPageType()
    {
        int maximumPage = 13;

        GROUP_Dot.gameObject.SetActive(_maxPage > maximumPage == false);
        TEXT_Page.gameObject.SetActive(_maxPage > maximumPage == true);

    }

    private void CreateDotPage()
    {
        GameObject obj = Instantiate(DotPrefab, GROUP_Dot);
        _dotList.Add(obj.GetComponent<Image>());
    }

    private void NextPage()
    {
        if (_currentPage < _maxPage)
        {
            _currentPage++;
        }

        RefreshPage();
    }

    private void PreviousPage()
    {
        if (_currentPage > 0)
        {
            _currentPage--;
        }

        RefreshPage();
    }

    #endregion

}
