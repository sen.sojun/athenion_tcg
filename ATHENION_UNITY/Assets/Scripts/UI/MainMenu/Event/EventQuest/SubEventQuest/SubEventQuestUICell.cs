﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;

public class SubEventQuestUICell : BaseGameObjectiveUICell
{
    [SerializeField] protected GameObject _bTN_Progress;
    [SerializeField] protected GameObject _collectColor;
    [SerializeField] protected GameObject _doneColor;

    #region Methods
    public override void InitData(BaseGameObjectiveUIData UIData, UnityAction onClaimReward)
    {
        _data = UIData;

        SetStateButton();

        //Set Description
        TEXT_Head.text = _data.Title;
        TEXT_Detail.text = _data.Description;
        RewardImage.sprite = SpriteResourceHelper.LoadSprite(_data.ImageKey, SpriteResourceHelper.SpriteType.ICON_Item);
        

        //SET BTN
        BTN_Reward.onClick.RemoveAllListeners();
        BTN_Reward.onClick.AddListener(onClaimReward);

        SetSort(_data.IsClaimable, _data.IsCleared);
    }

    public void SetStateButton()
    {
        //BLOCK_BlackLayer.SetActive(_data.IsCleared);

        TEXT_Progression.transform.localPosition = new Vector2(0, -20);
        if (_data.IsCompleted)
        {
            if (_data.IsClaimable)
            {
                TEXT_Progression.text = LocalizationManager.Instance.GetText("BUTTON_COLLECT");
            }
            else
            {
                TEXT_Progression.text = LocalizationManager.Instance.GetText("BUTTON_DONE");
                TEXT_Progression.transform.localPosition = new Vector2(0, 0);
            }
        }
        else
        {
            float currentProgress = (float)_data.ProgressCurrent / (float)_data.ProgressMax;
            TEXT_Progression.text = _data.ProgressCurrent + "/" + _data.ProgressMax;
        }

        BTN_Reward.enabled = _data.IsClaimable && _data.IsCompleted;
        _collectColor.SetActive(_data.IsClaimable && _data.IsCompleted);
        _doneColor.SetActive(!_data.IsClaimable && _data.IsCleared);
        RewardImage.enabled = !_data.IsCleared;
    }

    #endregion
}
