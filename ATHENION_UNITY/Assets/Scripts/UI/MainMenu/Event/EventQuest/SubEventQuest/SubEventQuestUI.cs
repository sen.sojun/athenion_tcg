﻿using Karamucho;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

public class SubEventQuestUI : MonoBehaviour
{
    #region Public Properties
    public TextMeshProUGUI TXT_Duration;
    public GameObject GROUP_Header;
    public GameObject GROUP_NoQuest;
    #endregion
    
    #region Private Properties
    private Dictionary<string, BaseGameObjectiveUICell> _gameEventUIList = new Dictionary<string, BaseGameObjectiveUICell>();
    #endregion

    [SerializeField]
    private Transform _CellParent;
    [SerializeField]
    private GameObject _QuestCellPrefab;

    public void SetData(Dictionary<string, QuestData> questDataDict, string eventKey, UnityAction<string, string> onClaimEvent)
    {
        if(_gameEventUIList.Count <= 0)
        {
            CreateQuestList(questDataDict, eventKey, onClaimEvent);
        }
        else
        {
            foreach (KeyValuePair<string, QuestData> questData in questDataDict)
            {
                UpdateEventQuestCell(questData.Value, eventKey, onClaimEvent);
            }
        }
    }

    private void CreateQuestList(Dictionary<string, QuestData> questDataDict, string eventKey, UnityAction<string, string> onClaimEvent)
    {
        foreach(KeyValuePair<string, QuestData> questData in questDataDict)
        {
            GameObject obj = Instantiate(_QuestCellPrefab, _CellParent);
            BaseGameObjectiveUICell cell = obj.GetComponent<BaseGameObjectiveUICell>();

            EventQuestUIData data = new EventQuestUIData(questData.Value);
            cell.InitData(data, () => onClaimEvent(data.ID, eventKey));

            _gameEventUIList.Add(questData.Key, cell);
        }

        SortQuestCell(questDataDict);
    }

    public void SortQuestCell(Dictionary<string, QuestData> questDataDict)
    {
        foreach (KeyValuePair<string, QuestData> questData in questDataDict)
        {
            EventQuestUIData data = new EventQuestUIData(questData.Value);
            _gameEventUIList[data.ID].SetSort(data.IsClaimable, data.IsCleared);
        }
    }

    public void UpdateEventQuestCell(QuestData questData, string eventKey, UnityAction<string, string> onClaimEvent)
    {
        if (_gameEventUIList.TryGetValue(questData.ID, out BaseGameObjectiveUICell cell))
        {
            EventQuestUIData data = new EventQuestUIData(questData);
            cell.InitData(data, () => onClaimEvent(questData.ID, eventKey));
        }
    }

    public void ShowUI()
    {
        GameHelper.UITransition_FadeIn(gameObject);
    }

    public void HideUI(UnityAction onComplete = null)
    {
        GameHelper.UITransition_FadeOut(gameObject, () => onComplete?.Invoke());
    }
}
