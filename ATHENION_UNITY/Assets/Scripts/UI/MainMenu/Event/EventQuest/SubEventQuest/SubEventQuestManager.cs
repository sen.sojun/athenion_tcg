﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

public class SubEventQuestManager : MonoBehaviour
{
    #region Public Properties
    public SubEventQuestUI Ref_EventQuestUI;
    #endregion

    #region Private Properties
    private BaseGameEventTemplate _baseGameEventTemplate = null;
    private Dictionary<string, GameEventInfo> _gameEventQuestList = new Dictionary<string, GameEventInfo>();
    private GameEventInfo _currentEventInfo = null;
    private PlayerSubEventQuestData _subEventQuestData = new PlayerSubEventQuestData();
    private Dictionary<string, QuestData> _questDict = new Dictionary<string, QuestData>();
    private bool _hasInit = false;
    private Coroutine _crTimer = null;
    #endregion
    
    public async Task<bool> SetupData(BaseGameEventTemplate baseGameEventTemplate)
    {
        if (!_hasInit)
        {
            _baseGameEventTemplate = baseGameEventTemplate;
            await TaskInvokeRefreshSubEventQuest();

            _hasInit = true;
        }

        _gameEventQuestList.TryGetValue(_baseGameEventTemplate.EventKey, out _currentEventInfo);
        _questDict = new Dictionary<string, QuestData>();
        List<QuestData> questList = new List<QuestData>(_subEventQuestData.GetQuestDataList());
        foreach (QuestData qData in questList)
        {
            _questDict.Add(qData.ID, qData);
        }

        Ref_EventQuestUI.SetData(_questDict, _currentEventInfo.EventKey, ClaimReward);
        return true;
    }

    public void ShowUI()
    {
        GameHelper.UITransition_FadeIn(Ref_EventQuestUI.gameObject);
    }

    public void HideUI()
    {
        GameHelper.UITransition_FadeOut(Ref_EventQuestUI.gameObject);
    }

    private void OnEnable()
    {
        _crTimer = StartCoroutine(CountTimer());
    }

    private void OnDisable()
    {
        if (_crTimer != null)
        {
            StopCoroutine(_crTimer);
            _crTimer = null;
        }
    }

    private void CreateGameSubEventQuestList(string eventQuestListJson)
    {
        Dictionary<string, object> rawGameEventData = JsonConvert.DeserializeObject<Dictionary<string, object>>(eventQuestListJson);
        foreach (KeyValuePair<string, object> data in rawGameEventData)
        {
            string json = data.Value.ToString();
            _gameEventQuestList.Add(data.Key, new GameEventInfo(data.Key, json));
        }
    }
    
    private IEnumerator CountTimer()
    {
        string headerDescription = LocalizationManager.Instance.GetText("TEXT_GENERAL_HEADER_DESC");
        string durationText = LocalizationManager.Instance.GetText("TEXT_GENERAL_EVENT_QUEST_DURATION");
        string headerText = string.Format("{0}{1}{2}", headerDescription, Environment.NewLine, durationText);

        Ref_EventQuestUI.GROUP_Header.SetActive(true);

        while (true)
        {
            if (_currentEventInfo != null)
            {
                string timeText;

                System.TimeSpan time = _currentEventInfo.EndDateTimeUTC - DateTimeData.GetDateTimeUTC();
                if (time.Days > 0)
                {
                    string duration = string.Format(LocalizationManager.Instance.GetText("TEXT_DAY"), time.Days);
                    timeText = duration;
                }
                else
                {
                    timeText = time.ToString(@"hh\:mm\:ss");
                }

                Ref_EventQuestUI.TXT_Duration.text = timeText;

                yield return new WaitForSeconds(1);
            }
            else
            {
                Ref_EventQuestUI.GROUP_Header.SetActive(false);
                Ref_EventQuestUI.GROUP_NoQuest.SetActive(true);

                break;
            }
        }
    }

    private void ClaimReward(string questID, string eventKey)
    {
        _questDict.TryGetValue(questID, out QuestData quest);
        if (quest != null)
        {
            PopupUIManager.Instance.Show_MidSoftLoad(true);
            DataManager.Instance.CheckServerSubEventQuest(
                questID,
                eventKey,
                async delegate (string dataQuest)
                {
                    //RefreshQuest();
                    string head = LocalizationManager.Instance.GetText("TEXT_EVENT_QUEST_COMPLETE");
                    string detail = LocalizationManager.Instance.GetText("TEXT_GOT_REWARD"); ;
                    PopupUIManager.Instance.ShowPopup_GotReward(head, detail, quest.ItemDataList);
                    _subEventQuestData.SetData(dataQuest);
                    await SetupData(_baseGameEventTemplate);
                    PopupUIManager.Instance.Show_MidSoftLoad(false);
                },
                delegate (string err)
                {
                    PopupUIManager.Instance.Show_MidSoftLoad(false);
                }
            );
        }

    }

    private void ShowReadMore()
    {
        string title = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventDetailHeaderKey);
        string message = LocalizationManager.Instance.GetText("EVENT_MOREDETAIL_" + _currentEventInfo.EventKey.ToUpper());

        PopupUIManager.Instance.ShowPopup_DetailWindow(title, message);
    }

    private Task<bool> TaskInvokeRefreshSubEventQuest()
    {
        var t = new TaskCompletionSource<bool>();
        try
        {
            InvokeRefreshSubEventQuest(() => t.TrySetResult(true), () => t.TrySetResult(false));
        }
        catch (Exception e)
        {
            //t.TrySetException(e); //For thowing exception
            t.TrySetResult(false);
        }

        return t.Task;
    }

    private void InvokeRefreshSubEventQuest(UnityAction onComplete = null, UnityAction onFail = null)
    {
        //PopupUIManager.Instance.Show_MidSoftLoad(true);
        PlayFabManager.Instance.ExecuteCloudScript(
            "RefreshSubEventQuest"
            , new { EventKey = _baseGameEventTemplate.EventKey }
            , (result) =>
            {
                //PopupUIManager.Instance.Show_MidSoftLoad(false);
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (!resultProcess.IsSuccess)
                {
                    onFail?.Invoke();
                }

                // Data Initialize.
                CreateGameSubEventQuestList(resultProcess.CustomData["sub_event_quest"]);
                _subEventQuestData.SetData(resultProcess.CustomData["quest_data"]);
                onComplete?.Invoke();
            }
            , null
        );
    }
}