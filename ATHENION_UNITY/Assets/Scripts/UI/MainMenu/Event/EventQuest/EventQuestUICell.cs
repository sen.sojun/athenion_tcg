﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;

public class EventQuestUICell : BaseGameObjectiveUICell
{
    [SerializeField] protected GameObject _BTN_DisabledGet;
    [SerializeField] protected RewardCell _RewardCell;

    #region Methods
    public override void InitData(BaseGameObjectiveUIData UIData, UnityAction onClaimReward)
    {
        _data = UIData;

        SetStateButton();

        //Set Description
        TEXT_Detail.text = _data.Description;

        //Set Reward
        /*
        TEXT_RewardCount.text = _data.RewardCount.ToString("N0");
        Sprite sprite = SpriteResourceHelper.LoadSprite(_data.ImageKey, SpriteResourceHelper.SpriteType.ICON_Item);
        RewardImage.sprite = sprite;
        */
        _RewardCell.InitData(_data.ImageKey, _data.RewardCount);
        //Tooltip.Setup(_data.ImageKey);

        //Set Progress
        float currentProgress = (float)_data.ProgressCurrent / (float)_data.ProgressMax;
        SLIDER_Progression.value = currentProgress;
        TEXT_Progression.text = _data.ProgressCurrent + "/" + _data.ProgressMax;

        //SET BTN
        BTN_Reward.onClick.RemoveAllListeners();
        BTN_Reward.onClick.AddListener(onClaimReward);

        SetSort(_data.IsClaimable, _data.IsCleared);
    }

    public void SetStateButton()
    {
        BLOCK_BlackLayer.SetActive(_data.IsCleared);

        _BTN_DisabledGet.SetActive(!_data.IsCompleted || _data.IsCleared);
        BTN_Reward.gameObject.SetActive(_data.IsClaimable);
    }

    #endregion
}
