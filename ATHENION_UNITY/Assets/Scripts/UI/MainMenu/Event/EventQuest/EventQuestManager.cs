﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace GameEvent
{
    public class EventQuestManager : MonoBehaviour, IPageable
    {
        #region Public Properties
        public EventQuestUI Ref_EventQuestUI;
        #endregion

        #region Private Properties
        private Dictionary<string, QuestData> _questDict = new Dictionary<string, QuestData>();
        private bool _firstTimeInit = false;
        private Coroutine _crTimer = null;
        private GameEventInfo _currentEventInfo;
        #endregion

        private void OnEnable()
        {

        }

        private void OnDisable()
        {
            if (_crTimer != null)
            {
                StopCoroutine(_crTimer);
                _crTimer = null;
            }
        }

        private void SetupData()
        {
            if (!_firstTimeInit)
            {
                _currentEventInfo = DataManager.Instance.GetGameEventDB().GetActiveGameEventQuest();
                _firstTimeInit = true;
            }

            _questDict = new Dictionary<string, QuestData>();
            List<QuestData> questList = DataManager.Instance.PlayerEventQuest.GetQuestDataList();
            //Convert to Dict
            foreach (QuestData qData in questList)
            {
                _questDict.Add(qData.ID, qData);
            }

            // Count Event Duration
            _crTimer = StartCoroutine(CountTimer());

            Ref_EventQuestUI.SetData(_questDict, ClaimReward);

        }

        private IEnumerator CountTimer()
        {
            string headerDescription = LocalizationManager.Instance.GetText("TEXT_GENERAL_HEADER_DESC");
            string durationText = LocalizationManager.Instance.GetText("TEXT_GENERAL_EVENT_QUEST_DURATION");
            string headerText = string.Format("{0}{1}{2}", headerDescription, Environment.NewLine, durationText);

            Ref_EventQuestUI.GROUP_Header.SetActive(true);

            while (true)
            {
                if (_currentEventInfo != null)
                {
                    string timeText;

                    System.TimeSpan time = _currentEventInfo.EndDateTimeUTC - DateTimeData.GetDateTimeUTC();
                    if (time.Days > 0)
                    {
                        string duration = string.Format(LocalizationManager.Instance.GetText("TEXT_DAY"), time.Days);
                        timeText = duration;
                    }
                    else
                    {
                        timeText = time.ToString(@"hh\:mm\:ss");
                    }

                    Ref_EventQuestUI.TXT_Duration.text = string.Format(headerText, timeText);

                    yield return new WaitForSeconds(1);
                }
                else
                {
                    Ref_EventQuestUI.GROUP_Header.SetActive(false);
                    Ref_EventQuestUI.GROUP_NoQuest.SetActive(true);

                    break;
                }
            }
        }

        void IPageable.Show(UnityAction onComplete)
        {
            ShowUI();

            onComplete?.Invoke();
        }

        void IPageable.Hide(UnityAction onComplete)
        {
            HideUI();

            onComplete?.Invoke();
        }

        public void ShowUI()
        {
            GameHelper.UITransition_FadeIn(Ref_EventQuestUI.gameObject);
            SetupData();
        }

        public void HideUI()
        {
            GameHelper.UITransition_FadeOut(Ref_EventQuestUI.gameObject);
        }

        private void ClaimReward(string questID)
        {
            _questDict.TryGetValue(questID, out QuestData quest);
            if (quest != null)
            {
                PopupUIManager.Instance.Show_MidSoftLoad(true);
                DataManager.Instance.CheckServerEventQuest(questID,
                    delegate
                    {
                        //RefreshQuest();
                        string head = LocalizationManager.Instance.GetText("TEXT_EVENT_QUEST_COMPLETE");
                        string detail = LocalizationManager.Instance.GetText("TEXT_GOT_REWARD"); ;
                        PopupUIManager.Instance.ShowPopup_GotReward(head, detail, quest.ItemDataList);
                        PopupUIManager.Instance.Show_MidSoftLoad(false);

                        SetupData(); //Update
                    },
                    delegate (string err)
                    {
                        PopupUIManager.Instance.Show_MidSoftLoad(false);
                    }
                );
            }

        }

        private void ShowReadMore()
        {
            string title = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventDetailHeaderKey);
            string message = LocalizationManager.Instance.GetText("EVENT_MOREDETAIL_" + _currentEventInfo.EventKey.ToUpper());

            PopupUIManager.Instance.ShowPopup_DetailWindow(title, message);
        }

    }
}
