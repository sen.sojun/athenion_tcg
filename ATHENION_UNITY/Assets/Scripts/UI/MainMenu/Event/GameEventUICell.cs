﻿using Karamucho;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

public class GameEventUICell : MonoBehaviour
{
    [SerializeField]
    private Button _Button;
    [SerializeField]
    private Image _Image;

    [SerializeField]
    private TextMeshProUGUI _EventNameText;
    [SerializeField]
    private TextMeshProUGUI _DateText;

    private GameEventInfo _eventInfo;

    private void OnEnable()
    {
        StartCoroutine(CountTimer());
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    public void SetData(GameEventInfo eventInfo, UnityAction<string> onSelectEvent)
    {
        this._eventInfo = eventInfo;
        string eventKey = eventInfo.EventKey;
        _Image.sprite = SpriteResourceHelper.LoadSprite(eventKey, SpriteResourceHelper.SpriteType.GAME_Event);
        _Button.onClick.AddListener(() => onSelectEvent?.Invoke(eventKey));

        _EventNameText.text = LocalizationManager.Instance.GetText("EVENT_HEADER_" + eventKey.ToUpper());

    }

    private IEnumerator CountTimer()
    {
        yield return new WaitUntil(() => _eventInfo != null);
        while (true)
        {
            TimeSpan time = _eventInfo.EndDateTimeUTC - DateTimeData.GetDateTimeUTC();
            if (time.Days > 0)
            {
                _DateText.text = String.Format(LocalizationManager.Instance.GetText("TEXT_DAY"), time.Days);
            }
            else
            {
                _DateText.text = time.ToString(@"hh\:mm\:ss");
            }

            yield return new WaitForSeconds(1);
        }
    }
}
