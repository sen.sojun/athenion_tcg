﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace GameEvent
{
    public class GameEventThemeUI : MonoBehaviour
    {
        [SerializeField]
        private Image _BGImage;
        [SerializeField]
        private TextMeshProUGUI _ECText;

        private GameEventDB _eventDB;

        public GameEventDB EventDB
        {
            get
            {
                if (_eventDB == null)
                    _eventDB = DataManager.Instance.GetGameEventDB();
                return _eventDB;
            }
        }

        private void OnEnable()
        {
            ShopEventManager.OnExchangeComplete += UpdateCurrencyUI;
            PlayerInventoryData.OnUpdateEventCurrency += UpdateCurrencyUI;
        }

        private void OnDisable()
        {
            ShopEventManager.OnExchangeComplete -= UpdateCurrencyUI;
            PlayerInventoryData.OnUpdateEventCurrency -= UpdateCurrencyUI;
        }

        public void Initialize()
        {
            GameEventThemeInfo themeInfo = EventDB.GetCurrentTheme();
            if (themeInfo != null)
            {
                _BGImage.sprite = SpriteResourceHelper.LoadSprite(themeInfo.Theme, SpriteResourceHelper.SpriteType.GAME_Event);
            }

            GameEventActiveECInfo currencyInfo = EventDB.GetActiveCurrencyInfo();
            if (currencyInfo != null)
            {
                UpdateCurrencyUI();
            }
        }

        private void UpdateCurrencyUI()
        {
            GameEventActiveECInfo currencyInfo = EventDB.GetActiveCurrencyInfo();
            if (currencyInfo != null)
            {
                //TODO: Rework Event Currency (Finish).

                int currentEventCurrency = DataManager.Instance.InventoryData.GetAmountByItemID(currencyInfo.Currency);
                _ECText.text = $"{GameHelper.TMPro_GetEmojiCurrency(currencyInfo.Currency)}x {currentEventCurrency}";
                //PlayerEventCurrency eventCurrencyData = DataManager.Instance.InventoryData.GetEventCurrency(currencyInfo.Currency);
                //_ECText.text = string.Format("{0}{1}", GameHelper.TMPro_GetEmojiCurrency(currencyInfo.Currency), "x " + eventCurrencyData.Amount.ToString()); ;
            }
        }
    }
}