﻿using System.Collections;
using System.Collections.Generic;
using Karamucho;
using UnityEngine;
using UnityEngine.Events;

public class MiniGameEventManager : MonoBehaviour, IPageable
{
    #region Static Values

    private static readonly string EventTemplatePath = "Events/Templates/";

    #endregion

    #region Public Properties
    [SerializeField] private MiniGameEventUIManager _UIManager;
    public static MiniGameEventManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<MiniGameEventManager>();
            return _instance;
        }
    }
    #endregion

    #region Private Properties

    private static MiniGameEventManager _instance;

    #endregion

    #region Methods

    #region UI Methods

    public void Show(UnityAction onComplete)
    {
        ShowUI();
        onComplete?.Invoke();
    }

    public void Hide(UnityAction onComplete)
    {
        HideUI();
        onComplete?.Invoke();
    }

    private void ShowUI()
    {
        _UIManager.ShowUI();
    }

    private void HideUI()
    {
        _UIManager.HideUI();
    }

    #endregion

    #region Mini Game Event Methods

    public void CreateMiniGamePanel(string eventKey, UnityAction onComplete, UnityAction onFail)
    {
        Dictionary<string, GameEventInfo> data = DataManager.Instance.GetGameEventDB().GetGameEventInfo();
        if (IsTimeValid(data[eventKey]))
        {
            string templatePath = EventTemplatePath + data[eventKey].Template;
            PopupUIManager.Instance.Show_MidSoftLoad(true);

            _UIManager.LoadTemplate(eventKey, templatePath, onComplete, onFail);
        }
        else
        {
            onFail?.Invoke();
        }
    }

    public bool IsTimeValid(GameEventInfo gameEventInfo)
    {
        return DateTimeData.GetDateTimeUTC() > gameEventInfo.ReleaseDateTimeUTC && DateTimeData.GetDateTimeUTC() < gameEventInfo.EndDateTimeUTC;
    }

    #endregion

    #endregion
}
