﻿using System.Collections;
using System.Collections.Generic;
using GameEvent;
using Karamucho;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class MiniGameEventUIManager : MonoBehaviour
{
    #region Private Properties

    //[SerializeField] private Button BTN_Close;
    private BaseGameEventTemplate _currentTemplate;
    //private bool _isInit;

    #endregion

    //private void OnEnable()
    //{
    //    if (!_isInit)
    //    {
    //        SetBtn();
    //        _isInit = true;
    //    }
    //}

    #region Methods

    public void ShowUI()
    {
        GameHelper.UITransition_FadeIn(gameObject);
    }

    public void HideUI(UnityAction onComplete = null)
    {
        GameHelper.UITransition_FadeOut(gameObject);
        onComplete?.Invoke();
        ClearTemplate();
    }

    public void LoadTemplate(string eventKey, string path, UnityAction onComplete, UnityAction onFail)
    {
        GameObject eventPanel = ResourceManager.Load(path) as GameObject;

        if (eventPanel != null)
        {
            _currentTemplate = Instantiate(eventPanel, this.transform).GetComponent<BaseGameEventTemplate>();

            if (_currentTemplate != null)
            {
                _currentTemplate.Init(
                    eventKey
                    , string.Empty
                    , () =>
                    {
                        EventManager.Instance.SetCloseBthEvent(CloseMiniGameEvent);
                        onComplete?.Invoke();
                    }
                    , () =>
                    {
                        Destroy(_currentTemplate);
                        _currentTemplate = null;
                        onFail?.Invoke();
                    }
                );
            }
            else
            {
                onFail?.Invoke();
            }
        }
        else
        {
            onFail?.Invoke();
        }
    }

    private void ClearTemplate()
    {
        _currentTemplate.DestroySelf();
        _currentTemplate = null;
        EventManager.Instance.SetCloseBthEvent();
    }

    //private void SetBtn()
    //{
    //    BTN_Close.onClick.RemoveAllListeners();
    //    BTN_Close.onClick.AddListener(CloseMiniGameEvent);
    //}

    private void CloseMiniGameEvent()
    {
        PopupUIManager.Instance.Show_MidSoftLoad(true);
        EventManager.Instance.ShowPage(EventPageType.GameEvent);
    }
    #endregion
}
