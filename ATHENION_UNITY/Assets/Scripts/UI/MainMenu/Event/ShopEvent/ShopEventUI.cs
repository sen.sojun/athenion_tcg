﻿using Karamucho;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ShopEventUI : MonoBehaviour
{
    private const string EVENT_PROMOTE_IMG = "Events/EventImages/EVENT_PROMOTION";
    [SerializeField] private Image _PromotionImage;
    [Header("Prefab")]
    [SerializeField]
    private GameObject _ShortShopEventUIPrefab;
    [SerializeField]
    private Transform _ItemSpawnpoint;

    private UnityAction<ShopEventItemData> _onExchange;
    private ShopEventData _data;

    private List<ShopEventUICell> _itemUICellList = new List<ShopEventUICell>();

    public void SetEvent(UnityAction<ShopEventItemData> onExchange)
    {
        this._onExchange = onExchange;
    }

    public void ShowUI(ShopEventData shopEventInfo)
    {
        this._data = shopEventInfo;
        _PromotionImage.sprite = ResourceManager.Load<Sprite>(EVENT_PROMOTE_IMG);
        GameHelper.UITransition_FadeIn(gameObject);

        ClearContent();
        CreateUIElement(shopEventInfo);
        RebuildUILayoutGroup();

        Debug.Log(shopEventInfo.GetDebugText());
    }

    private void CreateUIElement(ShopEventData shopEventInfo)
    {
        List<ShopEventItemData> itemList = shopEventInfo.itemList;
        for (int i = 0; i < itemList.Count; i++)
        {
            AddUICell(itemList[i]);
        }
    }

    private void AddUICell(ShopEventItemData data)
    {
        GameObject prefab = _ShortShopEventUIPrefab;
        ShopEventUICell cell = Instantiate(prefab, _ItemSpawnpoint).GetComponent<ShopEventUICell>();
        cell.SetData(data, _onExchange);
        _itemUICellList.Add(cell);
    }

    private void ClearContent()
    {
        foreach (Transform child in _ItemSpawnpoint)
        {
            GameObject.Destroy(child.gameObject);
        }
        _itemUICellList.Clear();
    }

    private void RebuildUILayoutGroup()
    {
        LayoutRebuilder.ForceRebuildLayoutImmediate(_ItemSpawnpoint.GetComponent<RectTransform>());
    }

    public void HideUI()
    {
        GameHelper.UITransition_FadeOut(gameObject);
    }
}
