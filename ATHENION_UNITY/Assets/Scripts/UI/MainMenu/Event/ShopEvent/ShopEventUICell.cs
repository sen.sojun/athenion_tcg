﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;
using System;

public class ShopEventUICell : MonoBehaviour
{
    [SerializeField]
    private Button _Button;
    [SerializeField]
    private Image _Image;

    [SerializeField]
    private TextMeshProUGUI _ActivePriceText;
    [SerializeField]
    private TextMeshProUGUI _DeActivePriceText;
    [SerializeField]
    private TextMeshProUGUI _ItemName;

    public ShopEventItemData ItemData { get; private set; }

    public void SetData(ShopEventItemData data, UnityAction<ShopEventItemData> onClick)
    {
        ItemData = data;
        _Image.sprite = SpriteResourceHelper.LoadSprite(data.ImageKey, SpriteResourceHelper.SpriteType.GAME_Event);
        _ItemName.text = LocalizationManager.Instance.GetText(data.LocalizeKey);

        bool isActiveBTN = data.Buyable == true;
        SetPriceButton(isActiveBTN, data);
        SetInteractableButton(isActiveBTN);
        AddButtonEvent(onClick, isActiveBTN);
    }

    private void SetPriceButton(bool isActiveBTN, ShopEventItemData data)
    {
        string currency = data.Currency;
        string price = string.Format("{0}  {1}", data.Price, GameHelper.TMPro_GetEmojiCurrency(currency));
        _ActivePriceText.text = price;

        _DeActivePriceText.gameObject.SetActive(!isActiveBTN);
        _ActivePriceText.gameObject.SetActive(isActiveBTN);
    }

    private void AddButtonEvent(UnityAction<ShopEventItemData> onClick, bool isActiveBTN)
    {
        if (isActiveBTN)
        {
            _Button.onClick.RemoveAllListeners();
            _Button.onClick.AddListener(() => onClick?.Invoke(this.ItemData));
        }
    }

    private void SetInteractableButton(bool isActiveBTN)
    {
        _Button.interactable = isActiveBTN;
    }
}
