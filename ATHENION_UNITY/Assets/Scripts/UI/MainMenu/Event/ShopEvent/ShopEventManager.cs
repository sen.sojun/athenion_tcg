﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace GameEvent
{
    public class ShopEventManager : MonoBehaviour, IPageable
    {
        #region Event
        public delegate void ShopEventHandler();
        public static event ShopEventHandler OnExchangeComplete;
        #endregion

        public ShopEventUI ShopEventUI;
        public ShopEventData ShopEventInfo { get; private set; }

        void IPageable.Show(UnityAction onComplete)
        {
            this.ShopEventInfo = DataManager.Instance.GetShopEventInfo();
            ShowUI();

            onComplete?.Invoke();
        }

        void IPageable.Hide(UnityAction onComplete)
        {
            HideUI();

            onComplete?.Invoke();
        }

        public void ShowUI()
        {
            ShopEventUI.SetEvent(OnExchangeBTNClicked);

            CalculateShopEventInfo();
            ShopEventUI.ShowUI(ShopEventInfo);
        }

        public void HideUI()
        {
            ShopEventUI.HideUI();
        }

        private void OnExchangeBTNClicked(ShopEventItemData data)
        {
            string title = LocalizationManager.Instance.GetText("SHOP_EVENT_TITLE");
            string itemName = LocalizationManager.Instance.GetText(data.LocalizeKey);
            Debug.Log(itemName);
            string message = string.Format(LocalizationManager.Instance.GetText("SHOP_EVENT_PURCHASE_DETAIL"), itemName);
            string okText = string.Format("{0} {1}", data.Price, GameHelper.TMPro_GetEmojiCurrency(data.Currency));
            string cancelText = LocalizationManager.Instance.GetText("BUTTON_CANCEL");

            PopupUIManager.Instance.ShowPopup_TwoChoices(title, message, okText, cancelText
                , delegate
                {
                    Debug.Log("Exchange " + data.BundleID);
                    StartCoroutine(RequestPurchaseProcess(data.BundleID));
                }
                , null
            );
        }

        public int CalculateTotolPrice(List<ShopEventItemData> itemList)
        {
            int total = 0;
            for (int i = 0; i < itemList.Count; i++)
            {
                total += itemList[i].Price;
            }
            return total;
        }

        private IEnumerator RequestPurchaseProcess(string itemID)
        {
            PopupUIManager.Instance.Show_MidSoftLoad(true);
            string errorText = "";
            yield return PurchaseShopEvent(itemID, (error) => { errorText = error; });
            if (string.IsNullOrEmpty(errorText))
            {
                yield return DataManager.Instance.IELoadInventory(null);

                CalculateShopEventInfo();
                ShopEventUI.ShowUI(ShopEventInfo);
            }
            else
            {
                string title = LocalizationManager.Instance.GetText("SHOP_EVENT_TITLE");
                string error = LocalizationManager.Instance.GetText(errorText);
                PopupUIManager.Instance.ShowPopup_Error(title, error);
            }
            PopupUIManager.Instance.Show_MidSoftLoad(false);
            OnExchangeComplete?.Invoke();
            yield break;
        }

        private IEnumerator PurchaseShopEvent(string itemID, UnityAction<string> onFail)
        {
            bool isFinish = false;
            DataManager.Instance.PurchaseShopEventItem(
                itemID,
                delegate ()
                {
                    isFinish = true;
                },
                delegate (string error)
                {
                    onFail?.Invoke(error);
                    isFinish = true;
                }
            );
            yield return new WaitUntil(() => isFinish == true);
        }

        private void CalculateShopEventInfo()
        {
            for (int i = 0; i < ShopEventInfo.itemList.Count; i++)
            {
                ShopEventItemData item = ShopEventInfo.itemList[i];
                string itemID = item.BundleID;

                bool isContainItem = DataManager.Instance.InventoryData.IsContainItem(itemID);
                if (isContainItem == false)
                    continue;
                int itemAmount = DataManager.Instance.InventoryData.GetAmountByItemID(itemID);
                if (item.MaxCount != 0 && itemAmount >= item.MaxCount)
                {
                    item.SetBuyable(false);
                }
            }
        }

    }
}