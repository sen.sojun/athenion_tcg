using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;
using UnityEngine.Events;

public class GameEventTemplateEventQuest : BaseGameEventTemplate
{
    #region Public Properties
    #endregion

    #region Inspector Properties
    [Header("Prefab")]
    public GameObject EventQuestcell;

    [Header("Image")]
    public Image IMG_BG;
    public Image IMG_Header;

    public TextMeshProUGUI TEXT_Header;
    public TextMeshProUGUI TEXT_Objective;
    public TextMeshProUGUI TEXT_Duration;
    public TextMeshProUGUI TEXT_DetailEvent;

    [Header("BTN")]
    public Button BTN_ReadMore;
    public Button BTN_ReadMore_Close;
    public Button BTN_Back;

    [Header("Ref")]
    public RectTransform Container;
    public GameObject EventDetail;

    [Header("Ref Group")]
    public RectTransform Head_EventQuest;
    #endregion

    #region Private Properties
    private List<QuestData> _questList = new List<QuestData>();
    private List<EventQuestCell> _questCellList = new List<EventQuestCell>();
    private GameEventInfo _eventInfo;
    #endregion

    #region Contructors
    #endregion

    #region Methods
    public override void Init(string eventKey, string sceneName, UnityAction onComplete = null, UnityAction onFail = null)
    {
        PopupUIManager.Instance.Show_SoftLoad(true);

        base.Init(eventKey, sceneName);
        _questList = DataManager.Instance.PlayerEventQuest.GetQuestDataList();
        DataManager.Instance.GetGameEventDB().GetGameEventInfo().TryGetValue(eventKey, out _eventInfo);

        // UI
        InitUI();
        InitBTN();
        CreateQuestList(_questList, Head_EventQuest, EventQuestcell);
        SortQuest();

        // Count Event Duration
        StartCoroutine(CountTimer());

        GameHelper.UITransition_FadeIn(FadeIn.gameObject, () => PopupUIManager.Instance.Show_SoftLoad(false));
    }

    public void InitUI()
    {
        IMG_BG.sprite = SpriteResourceHelper.LoadSprite(_eventThemeInfo.Theme, SpriteResourceHelper.SpriteType.GAME_Event);
        IMG_Header.sprite = SpriteResourceHelper.LoadSprite(_eventThemeInfo.Theme + "_BANNER", SpriteResourceHelper.SpriteType.GAME_Event);

        TEXT_Header.text = LocalizationManager.Instance.GetText("EVENT_HEADER_" + _eventKey.ToUpper());
        TEXT_Objective.text = LocalizationManager.Instance.GetText("EVENT_OBJECTIVE_" + _eventKey.ToUpper());
        TEXT_Duration.text = LocalizationManager.Instance.GetText("EVENT_DURATION_" + _eventKey.ToUpper());
        TEXT_DetailEvent.text = LocalizationManager.Instance.GetText("EVENT_MOREDETAIL_" + _eventKey.ToUpper());

    }

    public void InitBTN()
    {
        BTN_Back.onClick.RemoveAllListeners();
        BTN_Back.onClick.AddListener(Close);

        BTN_ReadMore.onClick.RemoveAllListeners();
        BTN_ReadMore.onClick.AddListener(ShowReadMore);

        BTN_ReadMore_Close.onClick.RemoveAllListeners();
        BTN_ReadMore_Close.onClick.AddListener(HideReadMore);
    }

    private IEnumerator CountTimer()
    {
        yield return new WaitUntil(() => _eventInfo != null);
        while (true)
        {
            System.TimeSpan time = _eventInfo.EndDateTimeUTC - DateTimeData.GetDateTimeUTC();
            if (time.Days > 0)
            {
                string key = LocalizationManager.Instance.GetText("EVENT_DURATION_" + _eventKey.ToUpper());
                string duration = string.Format(LocalizationManager.Instance.GetText("TEXT_DAY"), time.Days);
                TEXT_Duration.text = string.Format(key, duration);
            }
            else
            {
                TEXT_Duration.text = time.ToString(@"hh\:mm\:ss");
            }

            yield return new WaitForSeconds(1);
        }
    }

    #endregion

    #region Read More & Close
    public void ShowReadMore()
    {
        string title = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventDetailHeaderKey);
        string message = LocalizationManager.Instance.GetText("EVENT_MOREDETAIL_" + _eventKey.ToUpper());

        PopupUIManager.Instance.ShowPopup_DetailWindow(title, message);
        //EventDetail.gameObject.SetActive(true);
    }

    public void HideReadMore()
    {
        EventDetail.gameObject.SetActive(false);
    }

    public override void Close()
    {
        PopupUIManager.Instance.Show_SoftLoad(true);

        GameHelper.UITransition_FadeOut(FadeOut.gameObject, () =>
        {
            base.Close();
            PopupUIManager.Instance.Show_SoftLoad(false);
        });

    }
    #endregion

    #region Quest Generator
    private void CreateQuestList(List<QuestData> questDataList, Transform order, GameObject prefab)
    {
        for (int i = 0; i < questDataList.Count; i++)
        {
            GameObject obj = Instantiate(prefab, Container);
            EventQuestCell cell = obj.GetComponent<EventQuestCell>();

            _questCellList.Add(cell);
            cell.transform.SetAsLastSibling();
            EventQuestUIData data = new EventQuestUIData(questDataList[i]);
            cell.InitData(data, () => ClaimReward(data));
        }
    }

    private void RefreshQuest()
    {
        _questList = DataManager.Instance.PlayerEventQuest.GetQuestDataList();

        for (int i = 0; i < _questCellList.Count; i++)
        {
            EventQuestUIData data = new EventQuestUIData(_questList[i]);
            _questCellList[i].InitData(data, () => ClaimReward(data));
        }

        SortQuest();
    }

    private void SortQuest()
    {
        for (int i = 0; i < _questCellList.Count; i++)
        {
            EventQuestUIData data = (EventQuestUIData)_questCellList[i].Data;
            _questCellList[i].SetSort(data.IsClaimable, data.IsCleared);
        }
    }

    private void ClaimReward(EventQuestUIData data)
    {
        QuestData quest = _questList.Find(x => x.ID == data.ID);
        if (quest != null)
        {
            PopupUIManager.Instance.Show_MidSoftLoad(true);
            DataManager.Instance.CheckServerEventQuest(data.ID,
                delegate
                {
                    RefreshQuest();
                    string head = LocalizationManager.Instance.GetText("TEXT_EVENT_QUEST_COMPLETE");
                    string detail = LocalizationManager.Instance.GetText("TEXT_GOT_REWARD"); ;
                    PopupUIManager.Instance.ShowPopup_GotReward(head, detail, quest.ItemDataList);
                    PopupUIManager.Instance.Show_MidSoftLoad(false);

                },
                delegate (string err)
                {
                    PopupUIManager.Instance.Show_MidSoftLoad(false);
                }
            );
        }

    }
    #endregion
}