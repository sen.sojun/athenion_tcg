﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameEventFindCoupleSelectFriendCell : BaseGameEventSelectFriendCell
{
    public enum FindCoupleFriendStatus
    {
        YouPropose,
        ProposeYou
    }
    
    [Header("Find Couple Status Image Object")]
    [SerializeField] private GameObject YouProposeStatusImage;
    [SerializeField] private GameObject ProposeYouStatusImage;
    
    public void SetFindCoupleStatusImage(FindCoupleFriendStatus status)
    {
        YouProposeStatusImage.SetActive(false);
        ProposeYouStatusImage.SetActive(false);
        switch (status)
        {
            case FindCoupleFriendStatus.YouPropose:
                YouProposeStatusImage.SetActive(true);
                break;
            case FindCoupleFriendStatus.ProposeYou:
                ProposeYouStatusImage.SetActive(true);
                break;
            default:
                break;
        }
    }
}
