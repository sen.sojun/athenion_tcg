﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameEventFindCoupleUI : MonoBehaviour
{
    public Image CurrentMainEventImage;
    public Sprite[] MainEventImageSpriteList;
    public GameObject SelectFriendPanel;
    public Slider PresentsReceivedProgress;

    [Header("Prefabs")]
    public GameEventThresholdReward FindCoupleThresholdRewardPrefab;
    public GameEventFindCoupleSelectFriendCell GameEventFindCoupleSelectFriendCellPrefab;

    [Header("Transforms")]
    public Transform EventRewardTransform;
    public Transform FriendCellTransform;

    [Header("Texts")]
    public TMP_Text SelectFriendButtonText;
    public TMP_Text RemainingPlayTimeText;
    public TMP_Text SendText;
    public TMP_Text RewardsReceivedText;
    public TMP_Text NoFriendInListText;

    [Header("Buttons")]
    public Button SelectFriendButton;
    public Button SelectFriendFillBGButton;
    public Button SendButton;
    public Button ReadMoreButton;
    
    private GameEventFindCouple _gameEventFindCouple;
    private Coroutine _countdownCoroutine;
    private List<PlayerInfoData> _friendList;

    private void OnEnable()
    {
        _countdownCoroutine = StartCoroutine(Counting());
    }

    private void OnDisable()
    {
        if (_countdownCoroutine != null)
        {
            StopCoroutine(_countdownCoroutine);
        }
    }

    public Task<bool> TaskInit()
    {
        var t = new TaskCompletionSource<bool>();
        try
        {
            Init(() => t.TrySetResult(true));
        }
        catch (Exception e)
        {
            //t.TrySetException(e); //For thowing exception
            t.TrySetResult(false);
        }

        return t.Task;
    }

    private void Init(UnityAction onComplete = null)
    {
        _gameEventFindCouple = GetComponent<GameEventFindCouple>();
        _friendList = DataManager.Instance.GetFriendList(FriendStatus.Friend);

        #region Initialize Buttons
        ReadMoreButton.onClick.AddListener(() => ShowReadMore());
        SelectFriendFillBGButton.onClick.AddListener(() => CloseSelectFriendPanel());
        #endregion

        UpdateUI();
        onComplete?.Invoke();
    }

    public void UpdateUI()
    {
        FindCoupleEvent_CSPlayerData playerEventData = _gameEventFindCouple.PlayerEventData;
        FindCoupleEvent_CSEventData findCoupleEventData = _gameEventFindCouple.FindCoupleEventData;

        #region Main Event Sprite
        int spriteIndex = 0;
        for (int i = 0; i < findCoupleEventData.reward_thresholds.Length; i++)
        {
            if (playerEventData.action_points < findCoupleEventData.reward_thresholds[i])
            {
                spriteIndex = i;
                break;
            }
            else if (i == findCoupleEventData.reward_thresholds.Length - 1) // >= max threshold
            {
                spriteIndex = i + 1;
            }
        }
        CurrentMainEventImage.sprite = MainEventImageSpriteList[spriteIndex];
        #endregion

        #region Threshold Rewards
        foreach (GameEventThresholdReward item in EventRewardTransform.GetComponentsInChildren<GameEventThresholdReward>())
        {
            Destroy(item.gameObject);
        }
        for (int i = 0; i < findCoupleEventData.reward_thresholds.Length; i++)
        {
            int threshold = findCoupleEventData.reward_thresholds[i];
            GameEventThresholdReward giftingReward = Instantiate(FindCoupleThresholdRewardPrefab, EventRewardTransform);

            int rewardIndex = i;
            giftingReward.Init(
                new ItemData(findCoupleEventData.threshold_rewards[i].Keys.First(), findCoupleEventData.threshold_rewards[i].Values.First()),
                threshold,
                playerEventData.claimed_reward_index_list.ContainsKey(i.ToString()),
                _gameEventFindCouple.CheckCanClaimThresholdReward(i),
                () => _gameEventFindCouple.CheckThresholdRewardDetail(rewardIndex)
                );
        }
        #endregion

        RewardsReceivedText.text = playerEventData.action_points.ToString();
        PresentsReceivedProgress.value = Mathf.Clamp((float)playerEventData.action_points / findCoupleEventData.reward_thresholds[findCoupleEventData.reward_thresholds.Length - 1], 0.06f, 1f);
        SelectFriendButton.onClick.RemoveAllListeners();
        SendButton.onClick.RemoveAllListeners();
        SendButton.gameObject.SetActive(false);

        //On breakup cooldown
        if (playerEventData.breakup_timestamp.HasValue && !GameHelper.IsNewDayUTC(playerEventData.breakup_timestamp.Value))
        {
            SelectFriendButtonText.text = LocalizationManager.Instance.GetText(_gameEventFindCouple.LocalizationKeyPrefix + "TITLE_YOU_HAVE_BREAKUP_COOLDOWN");
            SelectFriendButton.onClick.AddListener(() =>
                PopupUIManager.Instance.ShowPopup_ConfirmOnly
                (
                    LocalizationManager.Instance.GetText(_gameEventFindCouple.LocalizationKeyPrefix + "TITLE_YOU_HAVE_BREAKUP_COOLDOWN")
                    , LocalizationManager.Instance.GetText(_gameEventFindCouple.LocalizationKeyPrefix + "BODY_YOU_HAVE_BREAKUP_COOLDOWN")
                )
            );
        }
        //Have couple
        else if (!string.IsNullOrEmpty(playerEventData.current_couple_id))
        {
            PlayerInfoData coupleInfo = _friendList.Find((player) => player.PlayerID == playerEventData.current_couple_id);
            SelectFriendButtonText.text = coupleInfo.DisplayName;
            SendButton.gameObject.SetActive(true);
            SendButton.onClick.AddListener(async () => 
            {
                bool isSuccess = await _gameEventFindCouple.TaskInvokeFindCoupleAction(coupleInfo.PlayerID, GameEventFindCouple.FindCoupleEventAction.action_point);
                if (isSuccess)
                {
                    StartCoroutine(_gameEventFindCouple.ActionRewardGenerate(SendButton.transform, new List<ItemData> { new ItemData(findCoupleEventData.action_rewards.First()) }, false));
                    await _gameEventFindCouple.TryClaimEachThresholdRewardsAndUpdateUI();
                }
                else
                {
                    _gameEventFindCouple.RefreshDataAndClaimThresholdReward();
                }
            });

            RemainingPlayTimeText.text = string.Format("{0}/{1}", playerEventData.remain_playtime, findCoupleEventData.default_playtime);
            SelectFriendButton.onClick.AddListener(() => ShowBreakup(coupleInfo.PlayerID));
            //Noti
            if (!string.IsNullOrWhiteSpace(playerEventData.accept_couple_id_to_notify))
            {
                PlayerInfoData newCoupleInfo = _friendList.Find(data => data.PlayerID == playerEventData.accept_couple_id_to_notify);
                PopupUIManager.Instance.ShowPopup_ConfirmOnly(
                    LocalizationManager.Instance.GetText(_gameEventFindCouple.LocalizationKeyPrefix + "POPUP_TITLE_NOTIFY_ACCEPT")
                    , string.Format(LocalizationManager.Instance.GetText(_gameEventFindCouple.LocalizationKeyPrefix + "POPUP_BODY_NOTIFY_ACCEPT"), newCoupleInfo.DisplayName));
            }
        }
        //Not have couple
        else
        {
            SelectFriendButtonText.text = LocalizationManager.Instance.GetText(_gameEventFindCouple.LocalizationKeyPrefix + "TITLE_SELECT_FRIEND");
            SelectFriendButton.onClick.AddListener(() => ShowSelectFriendPanel());
            //Noti
            if (!string.IsNullOrWhiteSpace(playerEventData.breakup_couple_id_to_notify))
            {
                PlayerInfoData brokeupCoupleInfo = _friendList.Find(data => data.PlayerID == playerEventData.breakup_couple_id_to_notify);
                PopupUIManager.Instance.ShowPopup_ConfirmOnly(
                    LocalizationManager.Instance.GetText(_gameEventFindCouple.LocalizationKeyPrefix + "POPUP_TITLE_NOTIFY_BREAKUP")
                    , string.Format(LocalizationManager.Instance.GetText(_gameEventFindCouple.LocalizationKeyPrefix + "POPUP_BODY_NOTIFY_BREAKUP"), brokeupCoupleInfo.DisplayName));
            }
        }
    }

    private IEnumerator Counting()
    {
        while (true)
        {
            if (SendText.gameObject.activeSelf)
            {
                if (!_gameEventFindCouple.PlayerEventData.action_timestamp.HasValue)
                {
                    SendText.text = LocalizationManager.Instance.GetText(_gameEventFindCouple.LocalizationKeyPrefix + "BUTTON_SEND");
                }
                else
                {
                    float cooldownTime = Convert.ToSingle(_gameEventFindCouple.GetActionCooldownTime().TotalSeconds);
                    if (cooldownTime <= 0)
                    {
                        SendText.text = LocalizationManager.Instance.GetText(_gameEventFindCouple.LocalizationKeyPrefix + "BUTTON_SEND");
                    }
                    else
                    {
                        //SendText.text = GameHelper.GetCountdownText(cooldownTime); //With Hour, use this instead
                        int h, m, s;
                        h = Mathf.FloorToInt(cooldownTime / 3600);
                        m = Mathf.FloorToInt((cooldownTime - h * 3600) / 60);
                        s = Mathf.FloorToInt(cooldownTime % 60);

                        if (h > 0)
                        {
                            SendText.text = string.Format("{0}:{1}:{2}", h.ToString("D2"), m.ToString("D2"), s.ToString("D2"));
                        }
                        else
                        {
                            SendText.text = string.Format("{0}:{1}", m.ToString("D2"), s.ToString("D2"));
                        }
                    }
                }
            }
           
            yield return new WaitForSeconds(1f);
        }
    }

    private void ShowReadMore()
    {
        string title = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventDetailHeaderKey);
        string message = LocalizationManager.Instance.GetText("EVENT_MOREDETAIL_" + _gameEventFindCouple.EventKey.ToUpper());

        PopupUIManager.Instance.ShowPopup_DetailWindow(title, message);
    }

    private void ShowBreakup(string playerID)
    {
        PopupUIManager.Instance.ShowPopup_TwoChoices(
            LocalizationManager.Instance.GetText(_gameEventFindCouple.LocalizationKeyPrefix + "POPUP_CHOICE_TITLE_BREAKUP")
            , LocalizationManager.Instance.GetText(_gameEventFindCouple.LocalizationKeyPrefix + "POPUP_CHOICE_BODY_BREAKUP")
            , LocalizationManager.Instance.GetText(_gameEventFindCouple.LocalizationKeyPrefix + "POPUP_CHOICE_YES_BREAKUP")
            , LocalizationManager.Instance.GetText(_gameEventFindCouple.LocalizationKeyPrefix + "POPUP_CHOICE_NO_BREAKUP")
            , async () => 
            {
                bool isSuccess = await _gameEventFindCouple.TaskInvokeFindCoupleAction(playerID, GameEventFindCouple.FindCoupleEventAction.breakup);
                if(isSuccess)
                {
                    _gameEventFindCouple.PlayerEventData.breakup_couple_id_to_notify = playerID;
                    await _gameEventFindCouple.TryClaimEachThresholdRewardsAndUpdateUI();
                }
                else
                {
                    _gameEventFindCouple.RefreshDataAndClaimThresholdReward();
                }
            }, null);
    }

    private void ShowSelectFriendPanel()
    {
        foreach (GameEventFindCoupleSelectFriendCell cell in FriendCellTransform.GetComponentsInChildren<GameEventFindCoupleSelectFriendCell>())
        {
            Destroy(cell.gameObject);
        }

        //Update in case of your friend unfriended you
        _friendList = DataManager.Instance.GetFriendList(FriendStatus.Friend);
        NoFriendInListText.gameObject.SetActive(_friendList == null || _friendList.Count == 0);
        foreach (PlayerInfoData item in _friendList)
        {
            PlayerInfoUIData infoData = new PlayerInfoUIData(item);
            var coupleStatus = _gameEventFindCouple.PlayerEventData.friend_status_list.ContainsKey(item.PlayerID) ? _gameEventFindCouple.PlayerEventData.friend_status_list[item.PlayerID] : new FindCoupleEvent_CSFriendStatus();
            UnityAction selectFriendButtonAction = null;
            if (coupleStatus.you_propose)
            {
                selectFriendButtonAction = null;
            }
            else if (coupleStatus.propose_you)
            {
                selectFriendButtonAction = () =>
                {
                    PopupUIManager.Instance.ShowPopup_TwoChoices(
                    LocalizationManager.Instance.GetText(_gameEventFindCouple.LocalizationKeyPrefix + "POPUP_CHOICE_TITLE_ACCEPT")
                    , LocalizationManager.Instance.GetText(_gameEventFindCouple.LocalizationKeyPrefix + "POPUP_CHOICE_BODY_ACCEPT")
                    , LocalizationManager.Instance.GetText(_gameEventFindCouple.LocalizationKeyPrefix + "POPUP_CHOICE_YES_ACCEPT")
                    , LocalizationManager.Instance.GetText(_gameEventFindCouple.LocalizationKeyPrefix + "POPUP_CHOICE_NO_ACCEPT")
                    , async () => 
                    {
                        bool isSuccess = await _gameEventFindCouple.TaskInvokeFindCoupleAction(item.PlayerID, GameEventFindCouple.FindCoupleEventAction.accept);
                        CloseSelectFriendPanel();
                        if (isSuccess)
                        {
                             _gameEventFindCouple.PlayerEventData.accept_couple_id_to_notify = item.PlayerID;
                             await _gameEventFindCouple.TryClaimEachThresholdRewardsAndUpdateUI();
                        }
                        else
                        {
                            _gameEventFindCouple.RefreshDataAndClaimThresholdReward();
                        }
                    }
                    , null);
                    
                };
            }
            else
            {
                selectFriendButtonAction = () =>
                {
                    PopupUIManager.Instance.ShowPopup_TwoChoices(
                    LocalizationManager.Instance.GetText(_gameEventFindCouple.LocalizationKeyPrefix + "POPUP_CHOICE_TITLE_PROPOSE")
                    , LocalizationManager.Instance.GetText(_gameEventFindCouple.LocalizationKeyPrefix + "POPUP_CHOICE_BODY_PROPOSE")
                    , LocalizationManager.Instance.GetText(_gameEventFindCouple.LocalizationKeyPrefix + "POPUP_CHOICE_YES_PROPOSE")
                    , LocalizationManager.Instance.GetText(_gameEventFindCouple.LocalizationKeyPrefix + "POPUP_CHOICE_NO_PROPOSE")
                    , async () => 
                    {
                        bool isSuccess = await _gameEventFindCouple.TaskInvokeFindCoupleAction(item.PlayerID, GameEventFindCouple.FindCoupleEventAction.propose);
                        CloseSelectFriendPanel();
                        if (isSuccess)
                        {
                            await _gameEventFindCouple.TryClaimEachThresholdRewardsAndUpdateUI();
                        }
                        else
                        {
                            _gameEventFindCouple.RefreshDataAndClaimThresholdReward();
                        }
                    }
                    , null);

                };
            }

            GenerateFindCoupleSelectFriendCell
            (
                GameEventFindCoupleSelectFriendCellPrefab.gameObject
                , infoData
                , coupleStatus
                , FriendCellTransform
                , selectFriendButtonAction
            );
        }

        GameHelper.UITransition_FadeIn(SelectFriendPanel.gameObject);
    }

    private void CloseSelectFriendPanel()
    {
        GameHelper.UITransition_FadeOut(SelectFriendPanel.gameObject);
    }

    private GameEventFindCoupleSelectFriendCell GenerateFindCoupleSelectFriendCell(
          GameObject prefab, PlayerInfoUIData data, FindCoupleEvent_CSFriendStatus findCoupleStatus, Transform target
        , UnityAction onClickRequest
    )
    {
        GameObject obj = Instantiate(prefab, target);
        GameEventFindCoupleSelectFriendCell cell = obj.GetComponent<GameEventFindCoupleSelectFriendCell>();

        cell.SetAvatar(data.AvatarID);
        cell.SetRank(data.RankID);
        cell.SetTitle(data.TitleID);
        cell.SetName(data.DisplayName);
        if (findCoupleStatus.propose_you)
        {
            cell.SetFindCoupleStatusImage(GameEventFindCoupleSelectFriendCell.FindCoupleFriendStatus.ProposeYou);
        }
        else if (findCoupleStatus.you_propose)
        {
            cell.SetFindCoupleStatusImage(GameEventFindCoupleSelectFriendCell.FindCoupleFriendStatus.YouPropose);
        }

        if(onClickRequest != null)
        {
            obj.GetComponent<Button>().onClick.AddListener(onClickRequest);
        }
        return cell;
    }
}
