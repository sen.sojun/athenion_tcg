﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(GameEventFindCoupleUI))]
public class GameEventFindCouple : BaseGameEventTemplate
{
    public readonly string LocalizationKeyPrefix = "EVENT_BE_MY_COUPLE_2020_";

    public enum FindCoupleEventAction
    {
        accept
        , propose
        , breakup
        , action_point
    }

    public FindCoupleEvent_CSEventData FindCoupleEventData { get; protected set; }
    public FindCoupleEvent_CSPlayerData PlayerEventData { get; protected set; }

    private GameEventFindCoupleUI _gameEventFindCoupleUI;

    public override async void Init(string eventKey, string sceneName, UnityAction onComplete = null, UnityAction onFail = null)
    {
        base.Init(eventKey, sceneName);
        _gameEventFindCoupleUI = GetComponent<GameEventFindCoupleUI>();
        bool isSuccess = await TaskInvokeFindCoupleRefreshData();
        if (!isSuccess)
        {
            onFail?.Invoke();
            return;
        }

        isSuccess = await _gameEventFindCoupleUI.TaskInit();
        if (!isSuccess)
        {
            onFail?.Invoke();
            return;
        }

        onComplete?.Invoke(); //Call before claim threshold reward to show event page first
        await TryClaimEachThresholdRewardsAndUpdateUI();
    }

    public async void RefreshDataAndClaimThresholdReward(UnityAction onComplete = null, UnityAction onFail = null)
    {
        bool isSuccess = await TaskInvokeFindCoupleRefreshData();
        if (!isSuccess)
        {
            onFail?.Invoke();
            return;
        }

        isSuccess = await TryClaimEachThresholdRewardsAndUpdateUI();
        if (!isSuccess)
        {
            onFail?.Invoke();
            return;
        }

        onComplete?.Invoke();
    }

    public TimeSpan GetActionCooldownTime()
    {
        if (!PlayerEventData.action_timestamp.HasValue)
        {
            return TimeSpan.Zero;
        }

        return PlayerEventData.action_timestamp.Value.AddMilliseconds(FindCoupleEventData.default_cooldown) - DateTimeData.GetDateTimeUTC();
    }

    public bool CheckCanClaimThresholdReward(int rewardIndex)
    {
        if (PlayerEventData.claimed_reward_index_list.ContainsKey(rewardIndex.ToString()))
        {
            return false;
        }

        int minimumThreshold = FindCoupleEventData.reward_thresholds[rewardIndex];
        return PlayerEventData.action_points >= minimumThreshold;
    }

    public void CheckThresholdRewardDetail(int selectingIndex)
    {
        PopupUIManager.Instance.ShowPopup_PreviewReward(
            LocalizationManager.Instance.GetText(LocalizationKeyPrefix + "POPUP_TITLE_REWARD_DETAIL"),
            string.Format(LocalizationManager.Instance.GetText(LocalizationKeyPrefix + "POPUP_BODY_REWARD_DETAIL"), FindCoupleEventData.reward_thresholds[selectingIndex].ToString()),
            new List<ItemData> { new ItemData(FindCoupleEventData.threshold_rewards[selectingIndex].Keys.First(), FindCoupleEventData.threshold_rewards[selectingIndex].Values.First()) });
    }

    public async Task<bool> TryClaimEachThresholdRewardsAndUpdateUI()
    {
        bool isSuccess = false;
        for (int thresholdIndex = 0; thresholdIndex < FindCoupleEventData.threshold_rewards.Length; thresholdIndex++)
        {
            isSuccess = await TaskInvokeClaimThresholdReward(thresholdIndex);
            if (!isSuccess)
            {
                return isSuccess;
            }
        }

        _gameEventFindCoupleUI.UpdateUI();
        return isSuccess;
    }

    public Task<bool> TaskInvokeFindCoupleAction(string targetPlayerID, FindCoupleEventAction action)
    {
        var t = new TaskCompletionSource<bool>();
        try
        {
            InvokeFindCoupleAction(targetPlayerID, action, () => t.TrySetResult(true), () => t.TrySetResult(false));
        }
        catch (Exception e)
        {
            //t.TrySetException(e); //For thowing exception
            t.TrySetResult(false);
        }

        return t.Task;
    }

    //Called automatically in refresh method
    private Task<bool> TaskInvokeFindCoupleRefreshData()
    {
        var t = new TaskCompletionSource<bool>();
        try
        {
            InvokeFindCoupleRefreshData(() => t.TrySetResult(true), () => t.TrySetResult(false));
        }
        catch (Exception e)
        {
            //t.TrySetException(e); //For thowing exception
            t.TrySetResult(false);
        }

        return t.Task;
    }

    //Called automatically in refresh method
    private Task<bool> TaskInvokeClaimThresholdReward(int selectingIndex)
    {
        var t = new TaskCompletionSource<bool>();
        try
        {
            InvokeClaimThresholdReward(selectingIndex, () => t.TrySetResult(true));
        }
        catch (Exception e)
        {
            //t.TrySetException(e); //For thowing exception
            t.TrySetResult(false);
        }

        return t.Task;
    }

    private void InvokeFindCoupleAction(string targetPlayerID, FindCoupleEventAction action, UnityAction onComplete = null, UnityAction onFail = null)
    {
        if (action == FindCoupleEventAction.action_point)
        {
            if (PlayerEventData.remain_playtime <= 0)
            {
                return;
            }
            else if (GetActionCooldownTime() > TimeSpan.Zero)
            {
                PopupUIManager.Instance.ShowPopup_ConfirmOnlyWithWarning(
                    LocalizationManager.Instance.GetText("EVENT_COOLDOWN"),
                    LocalizationManager.Instance.GetText("EVENT_ONCOOLDOWN"));
                return;
            }
        }

        PopupUIManager.Instance.Show_SoftLoad(true);
        PlayFabManager.Instance.ExecuteCloudScript(
            "FindCoupleEventCoupleAction"
            , new
            {
                EventKey = _eventKey,
                ReceiverID = targetPlayerID,
                Action = action.ToString()
            }, (result) =>
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (!resultProcess.IsSuccess)
                {
                    PopupUIManager.Instance.ShowPopup_Error(
                        LocalizationManager.Instance.GetText("ERROR_POPUP_HEADER"),
                        LocalizationManager.Instance.GetText(resultProcess.MessageDetail));

                    PopupUIManager.Instance.Show_SoftLoad(false);
                    onFail?.Invoke();
                    return;
                }

                bool isLoadDataSuccess = true;
                isLoadDataSuccess = GameHelper.TryDeserializeJSONStr(resultProcess.CustomData["player_data"], out FindCoupleEvent_CSPlayerData playerEventDataResponse);
                if (isLoadDataSuccess)
                {
                    PlayerEventData = playerEventDataResponse;

                    if (action == FindCoupleEventAction.action_point)
                    {
                        List<ItemData> receivedItems = new List<ItemData> { new ItemData(FindCoupleEventData.action_rewards.First()) };
                        DataManager.Instance.InventoryData.GrantItemByItemList(receivedItems);
                    }
                    else if (action == FindCoupleEventAction.propose)
                    {
                        PopupUIManager.Instance.ShowPopup_ConfirmOnly
                        (
                            LocalizationManager.Instance.GetText(LocalizationKeyPrefix + "POPUP_TITLE_PROPOSE_SUCCESS")
                            , LocalizationManager.Instance.GetText(LocalizationKeyPrefix + "POPUP_BODY_PROPOSE_SUCCESS")
                        );
                    }

                    if (!string.IsNullOrWhiteSpace(resultProcess.CustomData["special_popup_message"]))
                    {
                        string specialPopupMessage = resultProcess.CustomData["special_popup_message"];
                        PopupUIManager.Instance.ShowPopup_ConfirmOnly
                        (
                            LocalizationManager.Instance.GetText("POPUP_TITLE_" + specialPopupMessage)
                            , LocalizationManager.Instance.GetText("POPUP_BODY_" + specialPopupMessage)
                        );
                    }

                    PopupUIManager.Instance.Show_SoftLoad(false);
                    onComplete?.Invoke();
                }
                else
                {
                    PopupUIManager.Instance.Show_SoftLoad(false);
                }
            }
            , null
        );
    }

    private void InvokeFindCoupleRefreshData(UnityAction onComplete = null, UnityAction onFail = null)
    {
        PopupUIManager.Instance.Show_MidSoftLoad(true);
        PlayFabManager.Instance.ExecuteCloudScript(
            "FindCoupleEventRefreshData"
            , new { EventKey = _eventKey }
            , (result) =>
            {
                PopupUIManager.Instance.Show_MidSoftLoad(false);
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (!resultProcess.IsSuccess)
                {
                    onFail?.Invoke();
                }

                // Data Initialize.
                bool isLoadDataSuccess = true;
                isLoadDataSuccess &= GameHelper.TryDeserializeJSONStr(resultProcess.CustomData["event_data"], out FindCoupleEvent_CSEventData eventDataResponse);
                isLoadDataSuccess &= GameHelper.TryDeserializeJSONStr(resultProcess.CustomData["player_data"], out FindCoupleEvent_CSPlayerData playerEventDataResponse);
                if (isLoadDataSuccess)
                {
                    FindCoupleEventData = eventDataResponse;
                    PlayerEventData = playerEventDataResponse;
                    onComplete?.Invoke();
                }
                else
                {
                    onFail?.Invoke();
                }
            }
            , null
        );
    }
    
    private void InvokeClaimThresholdReward(int selectingIndex, UnityAction onComplete = null)
    {
        if (!CheckCanClaimThresholdReward(selectingIndex))
        {
            onComplete?.Invoke();
            return;
        }

        PopupUIManager.Instance.Show_SoftLoad(true);
        PlayFabManager.Instance.ExecuteCloudScript(
            "FindCoupleEventClaimThresholdReward"
            , new
            {
                EventKey = _eventKey,
                ClaimingRewardIndex = selectingIndex.ToString()
            }, (result) =>
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (!resultProcess.IsSuccess)
                {
                    PopupUIManager.Instance.ShowPopup_Error(
                        LocalizationManager.Instance.GetText("ERROR_POPUP_HEADER"),
                        LocalizationManager.Instance.GetText(resultProcess.MessageDetail));

                    PopupUIManager.Instance.Show_SoftLoad(false);
                    return;
                }

                bool isLoadDataSuccess = true;
                isLoadDataSuccess = GameHelper.TryDeserializeJSONStr(resultProcess.CustomData["player_data"], out FindCoupleEvent_CSPlayerData playerEventDataResponse);
                if (isLoadDataSuccess)
                {
                    PlayerEventData = playerEventDataResponse;

                    List<ItemData> receivedItems = new List<ItemData> { new ItemData(FindCoupleEventData.threshold_rewards[selectingIndex].Keys.First(), FindCoupleEventData.threshold_rewards[selectingIndex].Values.First()) };
                    OnReceiveItem(receivedItems); //Analytics
                    if (PlayerEventData.claimed_reward_index_list.Count == FindCoupleEventData.threshold_rewards.Length)
                    {
                        OnEventComplete(); //Analytics
                    }
                    
                    DataManager.Instance.InventoryData.GrantItemByItemList(receivedItems);

                    PopupUIManager.Instance.Show_SoftLoad(false);
                    PopupUIManager.Instance.ShowPopup_GotRewardAnimated(
                        LocalizationManager.Instance.GetText("EVENT_CONGRATS"),
                        LocalizationManager.Instance.GetText("EVENT_YOUVEGOT"), receivedItems, () => 
                        {
                            onComplete?.Invoke();
                        });
                }
                else
                {
                    PopupUIManager.Instance.Show_SoftLoad(false);
                }
            }
            , null
        );
    }

}

public class FindCoupleEvent_CSEventData
{
    public string play_trigger;
    public string[] require_ticket;
    public int default_playtime;
    public int default_cooldown;
    public bool daily_repeatable;
    public Dictionary<string, int> action_rewards;
    public Dictionary<string, int>[] threshold_rewards;
    public int[] reward_thresholds;
}

public class FindCoupleEvent_CSPlayerData
{
    public DateTime? daily_timestamp;
    public DateTime? action_timestamp;
    public int remain_playtime;
    public int action_points;
    public string current_couple_id;
    public Dictionary<string, FindCoupleEvent_CSFriendStatus> friend_status_list;
    public DateTime? breakup_timestamp;
    public string accept_couple_id_to_notify;
    public string breakup_couple_id_to_notify;
    public Dictionary<string, bool> claimed_reward_index_list;
}

public class FindCoupleEvent_CSFriendStatus
{
    public bool you_propose;
    public bool propose_you;
}
