﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class EventRPSUIController : MonoBehaviour
{
    #region UI Properties

    // Images which is assigned value only one time at the beginning.
    [Header("Static Image UI Element List")]
    [SerializeField] private List<Image> _staticImageList;
    [SerializeField] private Image _eventDetailImage;
    [SerializeField] private Image _eventMainBgImage;
    [SerializeField] private Image _eventHeaderBgImage;
    [SerializeField] private Image _eventReadMoreImage;
    [SerializeField] private Image _eventActionImage;
    [SerializeField] private Image _eventCloseImage;
    [SerializeField] private Image _eventTicketImage;
    [SerializeField] private Image _eventTicketBgImage;

    // Images which is assigned value multiple time during runtime.
    [Header("Dynamic Image UI Element List")]
    [SerializeField] private Image _opponentActionImage;
    [SerializeField] private Image _playerActionImage;
    [SerializeField] private Image _opponentBalloonImage;
    [SerializeField] private Image _playerBalloonImage;
    [SerializeField] private Image _resultImage;
    [SerializeField] private Image _resultTextImage;

    // Text
    [Header("Text UI Elements")]
    [SerializeField] private TextMeshProUGUI _eventHeaderText;
    [SerializeField] private TextMeshProUGUI _eventObjectiveText;
    [SerializeField] private TextMeshProUGUI _eventDetailText;
    [SerializeField] private TextMeshProUGUI _eventDurationText;
    [SerializeField] private TextMeshProUGUI _eventCooldownText;
    [SerializeField] private TextMeshProUGUI _resultText;
    [SerializeField] private TextMeshProUGUI _eventTicketText;

    // Other UI.
    [Header("Other UI Elements")]
    [SerializeField] private RectTransform _eventRewardParent;
    [SerializeField] private Button _closeButton;
    [SerializeField] private Button _readMoreButton;
    [SerializeField] private Slider _rewardProgressSlider;

    // Animator.
    [Header("Animator Elements")]
    [SerializeField] private Animator _characterAnimator;
    #endregion
}
