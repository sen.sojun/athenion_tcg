﻿using System.Collections;
using System.Collections.Generic;
using Karamucho;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class EventRPSRewardItem : MonoBehaviour
{
    public Image RewardImage;
    public Image AcceptRewardSignImage;
    public TextMeshProUGUI RewardText;
    public TextMeshProUGUI WinRoundText;

    /// <summary>
    /// Call this method to initialize this instance.
    /// </summary>
    /// <param name="sprite">Reward's sprite.</param>
    /// <param name="totalReward">Total reward string.</param>
    /// <param name="winRound">Win round string.</param>
    /// <param name="isRewardAccept">Reward</param>
    public void Init(Sprite sprite, string totalReward, string winRound, bool isRewardAccept)
    {
        AcceptRewardSignImage.gameObject.SetActive(isRewardAccept);

        RewardImage.sprite = sprite;
        RewardText.text = totalReward;
        WinRoundText.text = winRound;
    }

    public void UpdateItemState(bool isRewardAccept)
    {
        AcceptRewardSignImage.gameObject.SetActive(isRewardAccept);
    }
}
