﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using Karamucho;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class GameEventTemplateRPS : BaseGameEventTemplate
{
    #region Static Value

    #region Cloud Script Key

    private static readonly string RPSResultString = "result";

    private static readonly string PlayerEventCurrencyString = "event_currency";
    private static readonly string PlayerRPSResultString = "rps_result";
    private static readonly string PlayerDataWinDayKey = "win_days";
    private static readonly string PlayerDataRewardCountString = "reward_count";

    private static readonly string EventActionListKey = "action_img_index";

    #region Reward Key
    private static readonly string DailyRewardKey = "daily_rewards";                        // Daily reward object key.
    private static readonly string WinRewardListKey = "win_rewards";                        // Win reward list key.
    private static readonly string ThresholdRewardIndexKey = "threshold_index";             // Threshold reward index key.
    private static readonly string ThresholdRewardKey = "threshold_rewards";                // Threshold reward object key.
    #endregion

    #endregion

    #region Localize Key
    private static readonly string WinLocalizedKey = "EVENT_WIN_";
    private static readonly string DrawLocalizedKey = "EVENT_DRAW_";
    private static readonly string LoseLocalizedKey = "EVENT_LOSE_";
    #endregion

    #region Prefab Key

    private static readonly string EventRPSRewardPrefabName = "EventRPSRewardItem";

    #endregion

    #region Resource Key

    // Resource path.
    private static readonly string EventAnimatorResourcePath = "Events/{0}/Animations/EventPlayerAvatar";

    // Prefix key.
    private static readonly string ActionEnableSpriteString = "action_enable_";
    private static readonly string ActionDisableSpriteString = "action_disable_";
    private static readonly string PlayerAvatarSpriteString = "player_avatar";
    private static readonly string OpponentAvatarSpriteString = "opponent_avatar";
    
    // New
    private static readonly string ActionPrefixKey = "ACTION_";
    
    
    // Suffix key.
    private static readonly string WinKey = "_WIN";
    private static readonly string LoseKey = "_LOSE";
    private static readonly string NormalKey = "_NORMAL";
    private static readonly string WinText = "Mtext_VICTORY";
    private static readonly string LoseText = "Mtext_DEFEATED";
    private static readonly string DrawText = "Mtext_DRAW";

    // Full key
    private static readonly string ActionPrefabKey = "EventRPSAction";

    #endregion

    #region Animator Key

    private static readonly string AvatarIdleStateKey = "Avatar_Idle_State";
    private static readonly string AvatarLoadingStateKey = "Avatar_Loading_State";
    private static readonly string AvatarWinStateKey = "Avatar_Win_State";
    private static readonly string AvatarLoseStateKey = "Avatar_Lose_State";
    private static readonly string AvatarDrawStateKey = "Avatar_Draw_State";

    #endregion

    #region Game Logic Rule

    #region Rock-Paper-Scissors Rule

    private static readonly List<string> RpsCountDownList = new List<string>
    {
        "BB_0", "BB_1", "BB_2"
    };
    private static readonly List<string> RpsActionName = new List<string>
    {
        "BB_ACT_0", "BB_ACT_1", "BB_ACT_2"
    };
    private static readonly Dictionary<string, Dictionary<int, string>> RpsRule = new Dictionary<string, Dictionary<int, string>>
    {
        { "win", new Dictionary<int, string>
            {
                {0, RpsActionName[2]}
                ,{1, RpsActionName[0]}
                ,{2, RpsActionName[1]}
            }
        }
        , { "lose", new Dictionary<int, string>
            {
                {0, RpsActionName[1]}
                ,{1, RpsActionName[2]}
                ,{2, RpsActionName[0]}
            }
        }
        , { "draw", new Dictionary<int, string>
            {
                {0, RpsActionName[0]}
                ,{1, RpsActionName[1]}
                ,{2, RpsActionName[2]}
            }
        }
    };

    private static readonly string DrawString = "draw";
    private static readonly string WinString = "win";
    private static readonly string LoseString = "lose";

    #endregion

    #endregion

    #region Magic Number

    private const float EndGaugePercentage = 0.85f; 
    private const float StartGaugePercentage = 1.80f;

    #endregion

    #endregion

    #region Properties

    #region UI Properties

    // This one is for loading images those are specific to this event and won't be changed in any situation. 
    [Header("Static Image UI Element List")]
    [SerializeField] private List<Image> StaticImageList;
    [SerializeField] private Image EventDetailImage;
    [SerializeField] private Image EventMainBgImage;
    [SerializeField] private Image EventHeaderBgImage;
    [SerializeField] private Image EventReadMoreImage;
    [SerializeField] private Image EventActionImage;
    [SerializeField] private Image EventCloseImage;
    [SerializeField] private Image EventTicketImage;
    [SerializeField] private Image EventTicketBgImage;

    // This one is for loading images which can be changed in runtime.
    [Header("Dynamic Image UI Element List")]
    [SerializeField] private Image OpponentActionImage;
    [SerializeField] private Image PlayerActionImage;
    [SerializeField] private Image OpponentBalloonImage;
    [SerializeField] private Image PlayerBalloonImage;
    [SerializeField] private Image ResultImage;
    [SerializeField] private Image ResultTextImage;

    [Header("Text UI Elements")]
    [SerializeField] private TextMeshProUGUI EventHeaderText;
    [SerializeField] private TextMeshProUGUI EventObjectiveText;
    [SerializeField] private TextMeshProUGUI EventDetailText;
    [SerializeField] private TextMeshProUGUI EventDurationText;
    [SerializeField] private TextMeshProUGUI EventCooldownText;
    [SerializeField] private TextMeshProUGUI ResultText;
    [SerializeField] private TextMeshProUGUI EventTicketText;

    [Header("Other UI Elements")]
    [SerializeField] private RectTransform EventRewardParent;
    [SerializeField] private Button CloseButton;
    [SerializeField] private Button ReadMoreButton;
    [SerializeField] private Slider RewardProgressSlider;

    [Header("Animator Elements")]
    [SerializeField] private Animator CharacterAnimator;

    [Header("Template Pre-setting")]
    [SerializeField] private RPSType GameType;
    [SerializeField] private RPSRandomMode _randomMode;
    [SerializeField] private RPSActionFunction _actionFunction;
    [SerializeField] private string CloudScriptActionFunctionName;

    #endregion

    #region Event Properties

    public delegate void GameEventAction();
    public static event GameEventAction OnDisableAction;
    public static event GameEventAction OnEnableAction;

    #endregion

    #region Private Properties

    private Vector3 _resultDefaultPos;

    private int _rewardCount;
    private int _winDayCount;
    private float _rewardOneSliderProgress;
    private List<float> _rewardSliderProgressList;

    private List<EventRPSRewardItem> _rewardItemList;
    private string _rpsResult;

    private string _winLocalizedText;
    private string _drawLocalizedText;
    private string _loseLocalizedText;
    
    private Dictionary<string, object> _playerData;
    private GameObject _rewardPrefab;
    private int _limitPlayTime;
    private int _playTimeLeft;
    private List<EventRPSAction> _actionList;
    private List<int> _actionImgIndexList;

    // Static value from game event database.
    private int _maxWin;
    private Dictionary<int, ItemData> _thresholdRewardDict;
    private List<ItemData> _dailyRewardList;
    private List<ItemData> _winRewardList;

    // Temp value for this game event template.
    private DateTime _dailyRewardTimeStamp;
    private DateTime _actionTimeStamp;
    private int _remainPlaytime;
    private bool _validDate;
    private ItemData _ticket;

    #endregion

    #endregion

    #region Methods

    #region Overrided Methods
    /// <summary>
    /// Call this method to initialize this game event template.
    /// </summary>
    /// <param name="eventKey"></param>
    /// <param name="sceneName"></param>
    public override void Init(string eventKey, string sceneName, UnityAction onComplete = null, UnityAction onFail = null)
    {
        base.Init(eventKey, sceneName);

        PopupUIManager.Instance.Show_SoftLoad(true);

        DataManager.Instance.ExcecuteEventCloudScript(
            "GetRPSPlayerData"
            , new { EventKey = _eventKey }
            , (result) =>
            {
                DataManager.Instance.LoadInventory(null, null);

                bool isSuccess = true;

                // Data Initialize.
                isSuccess &= InitData(result);          

                if (isSuccess)
                {
                    // UI Initialize.
                    InitStaticUI();                     
                    InitPastResult();                   
                    GenerateRewards();                  
                    GenerateAction();                   

                    GameHelper.UITransition_FadeIn(FadeIn.gameObject, () => PopupUIManager.Instance.Show_SoftLoad(false));
                }
                else
                {
                    string errMsg = "GameEventTemplateRPS/Init: Data from cloud-script is not found.";
                    OnLoadSceneFailed(errMsg);
                }
                
            }
            , OnLoadSceneFailed
        );
    }

    /// <summary>
    /// Call this method when load scene is failed.
    /// </summary>
    /// <param name="errorString"></param>
    protected override void OnLoadSceneFailed(string errorString)
    {
        Debug.LogError(errorString);

        PopupUIManager.Instance.Show_SoftLoad(false);
        base.OnLoadSceneFailed(errorString);
    }

    #endregion

    #region Unity Callback Methods
    public void OnDisable()
    {
        OnDisableAction = null;
        OnEnableAction = null;
    }
    #endregion

    #region Initialize Methods

    #region Initial Data

    /// <summary>
    /// Call this method to initialize the data of this event template.
    /// </summary>
    /// <param name="eventData">Event Data.</param>
    /// <returns></returns>
    protected override bool InitData(CloudScriptResultProcess eventData)
    {
        bool isSuccess = true;

        #region Base Event Data

        isSuccess &= base.InitData(eventData);

        if (!isSuccess)
            return false;

        #endregion

        #region Custom Event Data

        isSuccess &= eventData.CustomData.ContainsKey(EventActionListKey);
        if (!isSuccess) return false;

        isSuccess &= GameHelper.TryDeserializeJSONStr(eventData.CustomData[EventActionListKey], out _actionImgIndexList);
        if (!isSuccess) return false;

        #endregion

        #region Threshold Reward Data

        _thresholdRewardDict = new Dictionary<int, ItemData>();

        // Threshold Reward Data
        isSuccess &= GameHelper.TryDeserializeJSONStr(EventDataDict[ThresholdRewardKey].ToString(), out List<object> thresholdRewardDictionary);
        if (isSuccess)
        {
            _rewardOneSliderProgress = 1 / (float) (thresholdRewardDictionary.Count - 1);
            _rewardSliderProgressList = new List<float>();

            for (int i = 0; i < thresholdRewardDictionary.Count; i++)
            {
                isSuccess &= GameHelper.TryDeserializeJSONStr(thresholdRewardDictionary[i].ToString(),
                    out Dictionary<string, object> itemDict);

                if (isSuccess)
                {
                    _thresholdRewardDict.Add(int.Parse(itemDict[ThresholdRewardIndexKey].ToString()),
                        new ItemData(
                            itemDict[GameEventTemplateKeys.ItemIDKey].ToString()
                            , int.Parse(itemDict[GameEventTemplateKeys.ItemAmountKey].ToString())
                        )
                    );

                    if (i == 0)
                    {
                        _rewardSliderProgressList.Add(0);
                    }
                    else
                    {
                        int day = _thresholdRewardDict.Keys.ElementAt(i) - _thresholdRewardDict.Keys.ElementAt(i - 1);
                        float progressSize = _rewardOneSliderProgress / day;
                        _rewardSliderProgressList.Add(progressSize);
                    }

                    if (i == thresholdRewardDictionary.Count - 1)
                        _maxWin = int.Parse(itemDict[ThresholdRewardIndexKey].ToString());
                }
                else return false;
            }
        }
        else return false;

        #endregion

        #region Daily Reward Data

        _dailyRewardList = new List<ItemData>();

        // Daily Reward Data
        isSuccess &= GameHelper.TryDeserializeJSONStr(EventDataDict[DailyRewardKey].ToString(), out List<object> dailyRewardDictionary);
        if (isSuccess)
        {
            foreach (object item in dailyRewardDictionary)
            {
                isSuccess &= GameHelper.TryDeserializeJSONStr(item.ToString(), out Dictionary<string, object> itemDict);

                if (isSuccess)
                    _dailyRewardList.Add(new ItemData(
                            itemDict[GameEventTemplateKeys.ItemIDKey].ToString()
                            , int.Parse(itemDict[GameEventTemplateKeys.ItemAmountKey].ToString())
                        )
                    );
                else return false;
            }
        }
        else return false;
        #endregion

        #region Win Reward Data
        _winRewardList = new List<ItemData>();

        // Win Reward Data
        isSuccess &= GameHelper.TryDeserializeJSONStr(EventDataDict[WinRewardListKey].ToString(), out List<object> winRewardDictionary);
        if (isSuccess)
        {
            foreach (object item in winRewardDictionary)
            {
                isSuccess &= GameHelper.TryDeserializeJSONStr(item.ToString(), out Dictionary<string, object> itemDict);
                if (isSuccess)
                    _winRewardList.Add(new ItemData(
                            itemDict[GameEventTemplateKeys.ItemIDKey].ToString()
                            , int.Parse(itemDict[GameEventTemplateKeys.ItemAmountKey].ToString())
                        )
                    );
                else return false;
            }
        }
        else return false;
        #endregion

        #region Player Event Data

        // Get all player data dictionary.
        Dictionary<string, object> playerDataDict;
        isSuccess &= eventData.CustomData.ContainsKey(GameEventTemplateKeys.PlayerDataKey);
        if (isSuccess) GameHelper.TryDeserializeJSONStr(eventData.CustomData[GameEventTemplateKeys.PlayerDataKey], out playerDataDict);
        else return false;

        // Check valid data.

        isSuccess &= playerDataDict.ContainsKey(GameEventTemplateKeys.PlayerDailyTimestampKey);
        isSuccess &= playerDataDict.ContainsKey(GameEventTemplateKeys.PlayerActionTimestampKey);
        isSuccess &= playerDataDict.ContainsKey(GameEventTemplateKeys.PlayerRemainPlaytimeKey);
        isSuccess &= playerDataDict.ContainsKey(PlayerDataWinDayKey);
        isSuccess &= playerDataDict.ContainsKey(PlayerDataRewardCountString);
        isSuccess &= playerDataDict.ContainsKey(PlayerRPSResultString);
        if (!isSuccess) return false;

        isSuccess &= DateTime.TryParse(playerDataDict[GameEventTemplateKeys.PlayerDailyTimestampKey].ToString(), out _dailyRewardTimeStamp);
        isSuccess &= DateTime.TryParse(playerDataDict[GameEventTemplateKeys.PlayerActionTimestampKey].ToString(), out _actionTimeStamp);
        isSuccess &= int.TryParse(playerDataDict[GameEventTemplateKeys.PlayerRemainPlaytimeKey].ToString(), out _remainPlaytime);
        isSuccess &= int.TryParse(playerDataDict[PlayerDataWinDayKey].ToString(), out _winDayCount);
        isSuccess &= int.TryParse(playerDataDict[PlayerDataRewardCountString].ToString(), out _rewardCount);
        isSuccess &= playerDataDict.ContainsKey(PlayerRPSResultString);
        _rpsResult = playerDataDict[PlayerRPSResultString].ToString();
        if (!isSuccess) return false;

        isSuccess &= eventData.CustomData.ContainsKey(GameEventTemplateKeys.PlayerValidDateKey);
        if (!isSuccess) return false;
        isSuccess &= GameHelper.TryDeserializeJSONStr(eventData.CustomData[GameEventTemplateKeys.PlayerValidDateKey], out _validDate);
        if (!isSuccess) return false;

        #endregion

        #region Ticket Data

        UpdateTicket();

        #endregion

        #region Others Data

        if (ResultImage != null)
            _resultDefaultPos = ResultImage.rectTransform.localPosition;
        else return false;

        #endregion

        return true;
    }

    #endregion

    #region Initial UI Elements

    /// <summary>
    /// Call this method to assign value into UI elements.
    /// </summary>
    private void InitStaticUI()
    {
        #region Text UI

        EventHeaderText.text = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventHeaderKey + _eventKey);
        EventObjectiveText.text = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventObjectiveKey + _eventKey);
        EventDetailText.text = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventDetailKey + _eventKey);
        EventDurationText.text = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventDurationKey + _eventKey);

        #endregion

        #region Image UI

        EventMainBgImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, _eventThemeInfo.Theme + GameEventTemplateKeys.EventMainBackgroundSuffix, true) != null
            ? SpriteResourceHelper.LoadSubEventImage(_eventKey, _eventThemeInfo.Theme + GameEventTemplateKeys.EventMainBackgroundSuffix)
            : SpriteResourceHelper.LoadSprite(_eventThemeInfo.Theme, SpriteResourceHelper.SpriteType.GAME_Event);
        EventHeaderBgImage.sprite = SpriteResourceHelper.LoadSprite(_eventThemeInfo.Theme + GameEventTemplateKeys.EventHeaderBannerSuffix, SpriteResourceHelper.SpriteType.GAME_Event);

        EventMainBgImage.enabled = true;
        EventHeaderBgImage.enabled = true;
        
        EventReadMoreImage.gameObject.SetActive(true);
        EventCloseImage.gameObject.SetActive(true);

        if (StaticImageList != null)
        {
            for (int i = 0; i < StaticImageList.Count; i++)
            {
                StaticImageList[i].sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, GameEventTemplateKeys.StaticNativeImagePrefixKey + i);
                StaticImageList[i].SetNativeSize();
                StaticImageList[i].enabled = true;
            }
        }

        #endregion

        #region Button UI

        CloseButton.onClick.RemoveAllListeners();
        CloseButton.onClick.AddListener(Close);

        ReadMoreButton.onClick.RemoveAllListeners();
        ReadMoreButton.onClick.AddListener(EventDetail);

        #endregion

        // If this game event template requires Ticket system.
        switch (PlayTriggerType)
        {
            case GameEventStartType.Ticket: 
                InitTicketUI();
                break;
            case GameEventStartType.Cooldown:
                InitCooldownUI();
                break;
            case GameEventStartType.CooldownTicket:
                // No Implementation.
                break;
        }
        
    }

    /// <summary>
    /// Call this method to assign ticket related value into UI elements.
    /// </summary>
    private void InitTicketUI()
    {
        // Text UI
        EventTicketText.text = $"{_ticket.Amount}/{RequireTicket[0]}";//_ticket.Amount + "/" + RequireTicket[0];

        // Image UI
        EventTicketImage.sprite = SpriteResourceHelper.LoadSprite(GameEventTemplateKeys.TicketIconImagePrefixKey + _eventThemeInfo.Theme, SpriteResourceHelper.SpriteType.ICON_Item);
        EventTicketBgImage.sprite = SpriteResourceHelper.LoadSprite(GameEventTemplateKeys.TicketBgImagePrefixKey + _eventThemeInfo.Theme, SpriteResourceHelper.SpriteType.GAME_Event);
        EventTicketImage.enabled = true;
        EventTicketBgImage.enabled = true;
    }

    /// <summary>
    /// Call this method to assign cooldown related value into UI elements.
    /// </summary>
    private void InitCooldownUI()
    {
        if (EventCooldownText != null)
        {
            StartCoroutine(CooldownTimer(_actionTimeStamp
                , DefaultCooldown
                , EventCooldownText
                , delegate { EventCooldownText.text = "Ready!"; })
            );
        }
    }

    /// <summary>
    /// Call this method to set previous result.
    /// </summary>
    private void InitPastResult()
    {
        // Find animator controller.

        string eventAnimatorResourcePath = string.Format(EventAnimatorResourcePath, _eventKey);

        if (ResourceManager.Load<RuntimeAnimatorController>(eventAnimatorResourcePath) != null)
        {
            CharacterAnimator.runtimeAnimatorController = ResourceManager.Load<RuntimeAnimatorController>(eventAnimatorResourcePath);
            CharacterAnimator.gameObject.SetActive(true);
            CharacterAnimator.Play(EventCharacterAnimationState.Idle.ToString());
        }

        // Declare effect sound variable.
        SoundManager.SFX effectSound = SoundManager.SFX.__________Event_____________;

        // If it's new day, show default UI and return this method.
        if (_validDate)
        {
            return;
        }

        // Set the result gameObject to visible.
        ResultText.gameObject.SetActive(true);
        ResultImage.gameObject.SetActive(true);

        // Set when previous result is lose.
        if (_rpsResult == LoseString)
        {
            CharacterAnimator.Play(EventCharacterAnimationState.Failed.ToString());
            ResultImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, LoseText);
            ResultText.text = LocalizationManager.Instance.GetText(LoseLocalizedKey + _eventKey);
            effectSound = SoundManager.SFX.SFX_RPS_DEFEATED;
        }
        // Set when previous result is win.
        else if (_rpsResult == WinString)
        {
            CharacterAnimator.Play(EventCharacterAnimationState.Success.ToString());
            ResultImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, WinText);
            ResultText.text = LocalizationManager.Instance.GetText(WinLocalizedKey + _eventKey);
            effectSound = SoundManager.SFX.SFX_RPS_VICTORY;
        }

        // Play sound.
        SoundManager.PlaySFX(effectSound);

        ResultImage.rectTransform.localScale = Vector3.one * 1.2f;

        // Animate result image and text, enable close button after finish it.
        //ResultImage.rectTransform.DOLocalMoveZ(-95, 0.5f).SetEase(Ease.OutElastic).onComplete += () =>
        //{
        //    ResultTextImage.gameObject.SetActive(true);
        //    CloseButton.enabled = true;
        //    ReadMoreButton.enabled = true;
        //};
        ResultImage.rectTransform.DOScale(Vector3.one, 0.5f).SetEase(Ease.OutElastic).onComplete += () =>
        {
            ResultTextImage.gameObject.SetActive(true);
            CloseButton.enabled = true;
            ReadMoreButton.enabled = true;
        };
    }

    #endregion

    #region Generate UI

    /// <summary>
    /// Call this method to generate the player action of this event template.
    /// </summary>
    private void GenerateAction()
    {
        _actionList = new List<EventRPSAction>();
        GameObject prefab = ResourceManager.Load(GameEventTemplateKeys.EventPrefabResourcePath + ActionPrefabKey) as GameObject;

        if (_actionImgIndexList == null || _actionImgIndexList.Count <= 0)
        {
            int index = 0;
            while (true)
            {
                Sprite sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, ActionPrefixKey + index, true);

                if (sprite == null)
                    break;

                EventRPSAction script = Instantiate(prefab, EventActionImage.rectTransform).GetComponent<EventRPSAction>();
                script.Init(sprite, index, this);
                _actionList.Add(script);
                index++;
            }
        }
        else
        {
            for (int i = 0; i < _actionImgIndexList.Count; i++)
            {
                Sprite sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, ActionPrefixKey + _actionImgIndexList[i]);
                EventRPSAction script = Instantiate(prefab, EventActionImage.rectTransform).GetComponent<EventRPSAction>();
                script.Init(sprite, i, this);
                _actionList.Add(script);
            }
        }

        // Check if it's a new date.
        // If it is not, disable all button action.
        if (!_validDate)
            OnDisableAction?.Invoke();
    }

    /// <summary>
    /// Call this method to initial the win reward of this event template.
    /// </summary>
    /// <param name="eventData">Event data.</param>
    private void GenerateRewards()
    {
        _rewardItemList = new List<EventRPSRewardItem>();
        
        RewardProgressSlider.gameObject.SetActive(true);
        //RewardProgressSlider.value = _rewardSliderProgress * (_rewardCount - 1);

        float slideProgress = _rewardOneSliderProgress * (_rewardCount - 1);
        if (!_thresholdRewardDict.ContainsKey(_winDayCount) && _winDayCount != 0)
        {
            //slideProgress += (_rewardSliderProgressList[_rewardCount]);
            //if (_winDayCount == _thresholdRewardDict.Keys.ElementAt(_rewardCount) - 1)
            //    slideProgress +=
            //        (_rewardSliderProgressList[_rewardCount] *
            //         (_winDayCount - _thresholdRewardDict.Keys.ElementAt(_rewardCount - 1))) * EndGaugePercentage;
            //else if(_winDayCount == _thresholdRewardDict.Keys.ElementAt(_rewardCount - 1) + 1)
            //    slideProgress +=
            //        (_rewardSliderProgressList[_rewardCount] *
            //         (_winDayCount - _thresholdRewardDict.Keys.ElementAt(_rewardCount - 1))) * StartGaugePercentage;
            //else
                slideProgress += (_rewardSliderProgressList[_rewardCount - 1] *
                                  (_winDayCount - _thresholdRewardDict.Keys.ElementAt(_rewardCount - 1)));
        }
        RewardProgressSlider.value = slideProgress;

        if (ResourceManager.Load(GameEventTemplateKeys.EventPrefabResourcePath + EventRPSRewardPrefabName) != null)
            _rewardPrefab = ResourceManager.Load(GameEventTemplateKeys.EventPrefabResourcePath + EventRPSRewardPrefabName) as GameObject;

        for (int index = 0; index < _thresholdRewardDict.Count; index++)
        {
            EventRPSRewardItem script = Instantiate(_rewardPrefab, EventRewardParent).GetComponent<EventRPSRewardItem>();
            Sprite spriteFile = SpriteResourceHelper.LoadSprite(_thresholdRewardDict.Values.ElementAt(index).ItemID, SpriteResourceHelper.SpriteType.ICON_Item);

            string winRoundText = _thresholdRewardDict.Keys.ElementAt(index) + "win(s)";
            bool rewardAcceptStatus = index < _rewardCount;

            script.Init(
                spriteFile
                , "x" + _thresholdRewardDict.Values.ElementAt(index).Amount
                , winRoundText
                , rewardAcceptStatus
            );

            _rewardItemList.Add(script);
        }
    }

    #endregion

    #endregion

    #region UI Management Methods

    /// <summary>
    /// Call this method to show read more UI.
    /// </summary>
    public void ShowReadMore()
    {
        GameHelper.UITransition_FadeIn(EventDetailImage.gameObject);
    }

    /// <summary>
    /// Call this method to hide read more UI.
    /// </summary>
    public void HideReadMore()
    {
        GameHelper.UITransition_FadeOut(EventDetailImage.gameObject);
    }

    /// <summary>
    /// Call this method to update ticket UI.
    /// </summary>
    public void UpdateTicket()
    {
        if (PlayTriggerType == GameEventStartType.Ticket)
        {
            string ticketID = GameEventTemplateKeys.TicketIDPrefixKey + _eventThemeInfo.Theme;
            int ticketAmount = DataManager.Instance.InventoryData.GetAmountByItemID(ticketID);

            _ticket = new ItemData(ticketID, ticketAmount);
            EventTicketText.text = $"{_ticket.Amount}/{RequireTicket[0]}"; //_ticket.Amount + "/" + RequireTicket[0];
        }
    }

    #endregion

    #region Player Action Methods

    /// <summary>
    /// Call this method when the player select one action.
    /// </summary>
    /// <param name="caller">The class object who calls this method.</param>
    /// <param name="actionIndex">Action index.</param>
    public void OnSelectAction(object caller, int actionIndex)
    {
        //SoundManager.PlaySFX(SoundManager.SFX.Coin_Flick);

        OnDisableAction?.Invoke();

        if (caller.GetType() != typeof(EventRPSAction))
        {
            Debug.Log("Caller is not from " + caller.GetType());
            return;
        }

        EventRPSAction action = (EventRPSAction)caller;
        RectTransform rect = action.gameObject.GetComponent<RectTransform>();

        if (PlayTriggerType == GameEventStartType.Ticket && _ticket.Amount < RequireTicket[0])
        {
            string title = LocalizationManager.Instance.GetText(GameEventTemplateKeys.NoTicketTitleKey);
            string message = LocalizationManager.Instance.GetText(GameEventTemplateKeys.NoTicketMessageKey);
            PopupUIManager.Instance.ShowPopup_Error(title, message);
            OnEnableAction?.Invoke();
            return;
        }

        // If this is valid date, give participate reward.
        if (GameHelper.IsNewDayUTC(_dailyRewardTimeStamp))
        {
            StartCoroutine(DailyRewardGenerate(rect));
            _dailyRewardTimeStamp = DateTimeData.GetDateTimeUTC();
        }

        CloseButton.enabled = false;
        ReadMoreButton.enabled = false;

        if (GameType == RPSType.RPS)
        {
            OpponentActionImage.gameObject.SetActive(false);
            PlayerActionImage.gameObject.SetActive(false);
        }
        
        ResultText.gameObject.SetActive(false);
        ResultImage.gameObject.SetActive(false);
        ResultTextImage.gameObject.SetActive(false);
        ResultImage.rectTransform.localPosition = _resultDefaultPos;

        StartCoroutine(TakeAction(actionIndex));
        StartCoroutine(ActionCountDown(actionIndex));
    }
    #endregion

    #region Enumerator Methods

    private IEnumerator TakeAction(int actionIndex, UnityAction onComplete = null)
    {
        PopupUIManager.Instance.Show_SoftLoad(true);

        yield return ExecuteTakePlayerAction(actionIndex);
        yield return DataManager.Instance.IELoadInventory(null);

        PopupUIManager.Instance.Show_SoftLoad(false);
        onComplete?.Invoke();
    }

    private IEnumerator ExecuteTakePlayerAction(int actionIndex)
    {
        bool isFinish = false;

        string ticketId = PlayTriggerType == GameEventStartType.Ticket ? _ticket.ItemID : string.Empty;

        DataManager.Instance.ExcecuteEventCloudScript(
            //"RPSTakeAction"
            //CloudScriptActionFunctionName
            _actionFunction.ToString()
            , new
            {
                EventKey = _eventKey
                //, RandomMode = "Normal"
                , RandomMode = _randomMode.ToString()
                , TicketID = ticketId
                , ActionIndex = actionIndex
            }
            , delegate (CloudScriptResultProcess eventData)
            {
                GameHelper.TryDeserializeJSONStr(eventData.CustomData[RPSResultString], out _rpsResult);
                //if rewards have EC
                //string ec_json = eventData.CustomData[PlayerEventCurrencyString];
                //Debug.Log(ec_json);
                //DataManager.Instance.InventoryData.SetEventCurrency(ec_json);

                // TODO: Event currency and Item Data.

                bool isSuccess = GameHelper.TryDeserializeJSONStr(eventData.CustomData["item_data"], out Dictionary<string, int> ItemDataDict);
                if (isSuccess)
                {
                    List<ItemData> itemDataList = DataManager.ConvertDictionaryToItemDataList(ItemDataDict);
                    foreach (ItemData itemData in itemDataList)
                    {
                        DataManager.Instance.InventoryData.GrantItemByItemID(itemData.ItemID, itemData.Amount);
                    }
                }

                isFinish = true;
            }
            , (error) =>
             {
                 Debug.Log(error);
                 isFinish = true;
                 PopupUIManager.Instance.Show_SoftLoad(false);
             });
        yield return new WaitUntil(() => isFinish);
    }

    /// <summary>
    /// Call this enumerator method to generate the daily or any reward that need to be obtain for once in a day.
    /// </summary>
    /// <param name="parentTransform">Parent's rect-transform.</param>
    /// <returns></returns>
    private IEnumerator DailyRewardGenerate(RectTransform parentTransform)
    {
        int index = 0;

        GameObject eventDailyRewardGameObject = ResourceManager.Load(GameEventTemplateKeys.EventActionRewardPrefabPath) as GameObject;

        while (index < _dailyRewardList.Count)
        {
            Sprite sprite = SpriteResourceHelper.LoadSprite(_dailyRewardList[index].ItemID, SpriteResourceHelper.SpriteType.ICON_Item);
            EventActionReward item = Instantiate(eventDailyRewardGameObject, parentTransform).GetComponent<EventActionReward>();
            item.Init(sprite, _dailyRewardList[index]);

            yield return new WaitForSeconds(1);

            index++;
        }
    }

    /// <summary>
    /// Call this enumerator method to manage the RPS action countdown.
    /// </summary>
    /// <param name="actionIndex">Selected action.</param>
    /// <returns></returns>
    private IEnumerator ActionCountDown(int actionIndex)
    {
        int index = 0;

        if (GameType == RPSType.RPS)
        {
            OpponentBalloonImage.gameObject.SetActive(true);
            PlayerBalloonImage.gameObject.SetActive(true);
            OpponentActionImage.gameObject.SetActive(true);
            PlayerActionImage.gameObject.SetActive(true);

            while (index < RpsCountDownList.Count)
            {
                OpponentActionImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, RpsCountDownList[index]);
                PlayerActionImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, RpsCountDownList[index]);
                SoundManager.PlaySFX(SoundManager.SFX.SFX_RPS_BUBBLE_SPAWN);
                index++;
                yield return new WaitForSeconds(1);
            }
        }
        else if(GameType == RPSType.Other)
        {
            CharacterAnimator.Play(EventCharacterAnimationState.Loading.ToString());
            yield return new WaitForSeconds(2);
        }

        // Waiting loop.
        while (string.IsNullOrWhiteSpace(_rpsResult))
        {
            yield return null;
        }

        // Update Ticket use.
        DataManager.Instance.LoadInventory(UpdateTicket, null);

        string opponentRpsActionName = RpsRule[_rpsResult][actionIndex];

        // Show action.
        if (GameType == RPSType.RPS)
        {
            OpponentActionImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, opponentRpsActionName);
            PlayerActionImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, RpsActionName[actionIndex]);
            SoundManager.PlaySFX(SoundManager.SFX.Intro_HeroOut);
            yield return new WaitForSeconds(1);
        }

        // Set result UI element active.
        ResultText.gameObject.SetActive(true);
        ResultImage.gameObject.SetActive(true);

        // Draw result.
        if (_rpsResult == DrawString)
        {
            ResultImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, DrawText);
            ResultText.text = LocalizationManager.Instance.GetText(DrawLocalizedKey + _eventKey);

            ResultImage.rectTransform.DOLocalMoveZ(-95, 0.5f).SetEase(Ease.OutElastic).onComplete += () =>
            {
                ResultTextImage.gameObject.SetActive(true);
                OnEnableAction?.Invoke();
                ReadMoreButton.enabled = true;
                CloseButton.enabled = true;
            };
        }
        // Win result.
        else if (_rpsResult == WinString)
        {
            CharacterAnimator.Play(EventCharacterAnimationState.Success.ToString());

            ResultImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, WinText);
            ResultText.text = LocalizationManager.Instance.GetText(WinLocalizedKey + _eventKey);

            SoundManager.PlaySFX(SoundManager.SFX.SFX_RPS_VICTORY);
            ResultImage.rectTransform.DOLocalMoveZ(-95, 0.5f).SetEase(Ease.OutElastic).onComplete += () =>
            {
                ResultTextImage.gameObject.SetActive(true);
                StartCoroutine(UpdateProgress(delegate
                {
                    CloseButton.enabled = true;
                    ReadMoreButton.enabled = true;
                }));
            };
        }
        else
        {
            CharacterAnimator.Play(EventCharacterAnimationState.Failed.ToString());

            ResultImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, LoseText);
            ResultText.text = LocalizationManager.Instance.GetText(LoseLocalizedKey + _eventKey);

            SoundManager.PlaySFX(SoundManager.SFX.SFX_RPS_DEFEATED);
            ResultImage.rectTransform.DOLocalMoveZ(-95, 0.5f).SetEase(Ease.OutElastic).onComplete += () =>
            {
                ResultTextImage.gameObject.SetActive(true);
                CloseButton.enabled = true;
                ReadMoreButton.enabled = true;
            };
        }
        _rpsResult = string.Empty;
    }

    /// <summary>
    /// Call this enumerator method to manage the update reward progression.
    /// </summary>
    /// <returns></returns>
    private IEnumerator UpdateProgress(UnityAction onComplete)
    {
        string title = LocalizationManager.Instance.GetText(GameEventTemplateKeys.NotiRewardTitleKey);
        string message = LocalizationManager.Instance.GetText(GameEventTemplateKeys.NotiRewardMessageKey);

        if (_rewardCount > 3)
        {
            if (_winRewardList.Count > 0)
            {
                PopupUIManager.Instance.ShowPopup_GotRewardAnimated(
                        title
                        , message
                        , _winRewardList
                        , delegate
                        {
                            OnReceiveItem(_winRewardList);
                            onComplete?.Invoke();
                        }
                    ).SetAnimationEase(Ease.OutExpo)
                    .SetDelay(0.1f)
                    .SetAnimationDuration(0.25f)
                    .SetMaxScale(8f);
            }
            else
            {
                onComplete?.Invoke();
            }
            yield break;
        }

        if (_rewardCount < 1)
        {
            _rewardCount++;
            _winDayCount++;

            yield return new WaitForSeconds(2);

            PopupUIManager.Instance.ShowPopup_GotRewardAnimated(
                title
                , message
                , SumRewardData(new List<ItemData> { _thresholdRewardDict[_winDayCount] }, _winRewardList)
                , delegate
                {
                    OnReceiveItem(SumRewardData(new List<ItemData> { _thresholdRewardDict[_winDayCount] }, _winRewardList));
                    _rewardItemList[_rewardCount - 1].UpdateItemState(true);
                    onComplete?.Invoke();
                }
                
            ).SetAnimationEase(Ease.OutExpo)
                .SetDelay(0.1f)
                .SetAnimationDuration(0.25f)
                .SetMaxScale(8f);

            DataManager.Instance.LoadInventory(null, null);

            yield break;
        }
        
        _winDayCount++;
        float target = 0;
        if (!_thresholdRewardDict.ContainsKey(_winDayCount) && _winDayCount != 0)
        {
            //target = _rewardOneSliderProgress * (_rewardCount - 1) + _rewardSliderProgressList[_rewardCount];
            //target = _rewardOneSliderProgress * (_rewardCount - 1) +
            //         (_rewardSliderProgressList[_rewardCount] *
            //          (_winDayCount - _thresholdRewardDict.Keys.ElementAt(_rewardCount - 1)));

            //if (_winDayCount == _thresholdRewardDict.Keys.ElementAt(_rewardCount) - 1)
            //{
            //    target = _rewardOneSliderProgress * (_rewardCount - 1) +
            //             (_rewardSliderProgressList[_rewardCount] *
            //              (_winDayCount - _thresholdRewardDict.Keys.ElementAt(_rewardCount - 1)) * EndGaugePercentage) ;
            //}
            //else if (_winDayCount == _thresholdRewardDict.Keys.ElementAt(_rewardCount - 1) + 1)
            //{
            //    target = _rewardOneSliderProgress * (_rewardCount - 1) +
            //             (_rewardSliderProgressList[_rewardCount] *
            //              (_winDayCount - _thresholdRewardDict.Keys.ElementAt(_rewardCount - 1)) * StartGaugePercentage);
            //}
            //else
            //{
                target = _rewardOneSliderProgress * (_rewardCount - 1) +
                         (_rewardSliderProgressList[_rewardCount] *
                          (_winDayCount - _thresholdRewardDict.Keys.ElementAt(_rewardCount - 1)));
            //}
        }
        else
        {
            _rewardCount++;
            target = _rewardOneSliderProgress * (_rewardCount - 1);
        }

        while (RewardProgressSlider.value < target)
        {
            RewardProgressSlider.value += Time.deltaTime * 0.25f;
            yield return null;
        }

        if (_thresholdRewardDict.ContainsKey(_winDayCount))
        {
            // Show reward.
            PopupUIManager.Instance.ShowPopup_GotRewardAnimated(
                    title
                    , message
                    , SumRewardData(new List<ItemData> { _thresholdRewardDict[_winDayCount] }, _winRewardList)
                    , delegate
                    {
                        OnReceiveItem(SumRewardData(new List<ItemData> { _thresholdRewardDict[_winDayCount] }, _winRewardList));
                        _rewardItemList[_rewardCount - 1].UpdateItemState(true);
                        onComplete?.Invoke();
                    }
                ).SetAnimationEase(Ease.OutExpo)
                .SetDelay(0.1f)
                .SetAnimationDuration(0.25f)
                .SetMaxScale(8f);
        }
        else
        {
            if (_winRewardList.Count > 0)
            {
                PopupUIManager.Instance.ShowPopup_GotRewardAnimated(
                        title
                        , message
                        , _winRewardList
                        , delegate
                        {
                            OnReceiveItem(_winRewardList);
                            onComplete?.Invoke();
                        }
                    ).SetAnimationEase(Ease.OutExpo)
                    .SetDelay(0.1f)
                    .SetAnimationDuration(0.25f)
                    .SetMaxScale(8f);
            }
            else
            {
                onComplete?.Invoke();
            }
        }

        if (_rewardCount == _thresholdRewardDict.Count)
            OnEventComplete();
    }
    #endregion

    #region Other Methods

    public override void Close()
    {
        PopupUIManager.Instance.Show_SoftLoad(true);

        GameHelper.UITransition_FadeOut(FadeOut.gameObject, () =>
        {
            base.Close();
            PopupUIManager.Instance.Show_SoftLoad(false);
        });
        
    }

    public void EventDetail()
    {
        string title = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventDetailHeaderKey);
        string message = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventDetailKey + _eventKey);

        PopupUIManager.Instance.ShowPopup_DetailWindow(title, message);
    }

    #endregion

    #endregion
}

public enum RPSType
{
    RPS = 0
    , Other
}

public enum RPSActionFunction
{
    RPSTakeRandomAction
    , RPSTakeAction
}

public enum RPSRandomMode
{
    None
    , Normal
    , Days
}