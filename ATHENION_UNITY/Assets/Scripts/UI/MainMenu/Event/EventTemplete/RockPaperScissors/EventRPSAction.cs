﻿using System.Collections.Generic;
using Karamucho;
using UnityEngine;
using UnityEngine.UI;

public enum EventRPSButton
{
    Enable
    , Disable
}

public class EventRPSAction : MonoBehaviour
{
    #region Properties

    public Image ActionIconImage;                               // Image for RPS action icon.
    public Material GrayScaleMaterial;                          // Greyscale material.

    private Button _RPSButton;                                  // Button component of RPS action button.
    #endregion

    /// <summary>
    /// Call this method to initialize the event RPS button.
    /// </summary>
    /// <param name="actionSprite">Action's sprite.</param>
    /// <param name="actionIndex">Action index.</param>
    /// <param name="manager">Event RPS manager.</param>
    public void Init(Sprite actionSprite, int actionIndex, GameEventTemplateRPS manager)
    {
        ActionIconImage.sprite = actionSprite;

        _RPSButton = GetComponent<Button>();
        _RPSButton.onClick.AddListener(() => manager?.OnSelectAction(this, actionIndex));

        GameEventTemplateRPS.OnDisableAction += DisableAction;
        GameEventTemplateRPS.OnEnableAction += EnableAction;
    }

    /// <summary>
    /// Call this method to disable this RPS button.
    /// </summary>
    public void DisableAction()
    {
        ActionIconImage.material = GrayScaleMaterial;
        _RPSButton.enabled = false;
    }

    /// <summary>
    /// Call this method to enable this RPS button.
    /// </summary>
    public void EnableAction()
    {
        ActionIconImage.material = null;
        _RPSButton.enabled = true;
    }
}
