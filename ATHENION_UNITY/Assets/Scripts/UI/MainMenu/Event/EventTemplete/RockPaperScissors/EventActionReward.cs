﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class EventActionReward : MonoBehaviour
{
    public Image RewardIconImage;
    public Image RewardBgImage;
    public TextMeshProUGUI RewardAmountText;
    public RectTransform rect;

    public void Init(Sprite sprite, ItemData itemData, bool isDefaultDiagonal = true, UnityAction onComplete = null)
    {
        SoundManager.PlaySFX(SoundManager.SFX.SFX_RPS_BUBBLE_SPAWN);

        RewardIconImage.sprite = sprite;
        RewardAmountText.text = itemData.Amount.ToString();

        if (!isDefaultDiagonal)
        {
            rect.anchoredPosition = Vector2.right * rect.sizeDelta.x;
            RewardBgImage.rectTransform.localScale = Vector3.one;
        }
        else
        {
            RewardBgImage.rectTransform.localScale = new Vector3(-1,1,1);
        }

        transform.DOMoveY(transform.position.y + 50, 0.5f).SetEase(Ease.Linear).onComplete += delegate
        {
            Destroy(gameObject, 1);
            onComplete?.Invoke();
        };
    }
}
