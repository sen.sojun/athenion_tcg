﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class TileFlipController : MonoBehaviour
{
    #region Static Value

    private static readonly string OpenTileStateName = "OPEN_TILE";
    private static readonly string FoundRewardStateName = "FOUND_REWARD";
    private static readonly string EmptyTileStateName = "EMPTY_TILE";
    private static readonly string WinRewardKey = "Win";
    private static readonly string NormalRewardKey = "Normal";
    private static readonly string CloseKey = "Close";
    private static readonly Color _defaultColor = new Color(1,1,1,1);

    #endregion

    #region Inspector Properties

    [Header("UI Elements")]
    [SerializeField] private Image _EmptyImage;
    [SerializeField] private Image _FoundRewardImage;
    [SerializeField] private Image _LeftPointer;
    [SerializeField] private Image _RightPointer;
    [SerializeField] private Button _tileButton;

    #endregion

    #region Private Properties

    private int _tileIndex;
    private GameEventTileFlipUIManager _uiManager;

    #endregion

    //public void Init(string state)
    //{
    //    _tileButton.gameObject.SetActive(true);

    //    if (state == WinRewardKey)
    //    {
    //        _EmptyImage.gameObject.SetActive(true);
    //        _EmptyImage.color = _defaultColor;
    //        _FoundRewardImage.gameObject.SetActive(true);
    //        _FoundRewardImage.color = _defaultColor;
    //    }
    //    else if (state == NormalRewardKey)
    //    {
    //        _EmptyImage.gameObject.SetActive(true);
    //        _EmptyImage.color = _defaultColor;
    //        _FoundRewardImage.gameObject.SetActive(false);
    //    }
    //    else if (state == CloseKey)
    //    {
    //        _EmptyImage.gameObject.SetActive(false);
    //        _FoundRewardImage.gameObject.SetActive(false);
    //    }
    //}

    public void Init(int tileIndex, GameEventTileFlipUIManager uiManager, TileFlipEventData eventInfoData, TileFlipPlayerData playerData)
    {
        _tileIndex = tileIndex;
        _uiManager = uiManager;
        string eventKey = eventInfoData.EventKey;

        if (playerData.OpenedTileDict.ContainsKey(_tileIndex) == false ||
            playerData.OpenedTileDict[_tileIndex] == CloseKey)
        {
            if (_EmptyImage != null) _EmptyImage.gameObject.SetActive(false);
            if (_FoundRewardImage != null) _FoundRewardImage.gameObject.SetActive(false);

            if (_uiManager.IsPointerEnable)
            {
                InitFlipPointer(Convert.ToBoolean(_tileIndex % 2));
            }
        }
        else if (playerData.OpenedTileDict.ContainsKey(_tileIndex) && playerData.OpenedTileDict[_tileIndex] != CloseKey)
        {
            if (_EmptyImage != null)
            {
                _EmptyImage.sprite = SpriteResourceHelper.LoadSubEventImage(eventKey, EmptyTileStateName, true);
                _EmptyImage.gameObject.SetActive(_EmptyImage.sprite != null);
            }

            if (_FoundRewardImage != null)
            {
                _FoundRewardImage.sprite = SpriteResourceHelper.LoadSubEventImage(eventKey, playerData.OpenedTileDict[_tileIndex], true);
                _FoundRewardImage.gameObject.SetActive(_FoundRewardImage.sprite != null);
            }

            if (_uiManager.IsPointerEnable)
            {
                if(_LeftPointer != null) _LeftPointer.gameObject.SetActive(false);
                if(_RightPointer != null) _RightPointer.gameObject.SetActive(false);
            }
        }
    }

    public void SetButtonAction(UnityAction onClickAction)
    {
        _tileButton.onClick.RemoveAllListeners();
        _tileButton.onClick.AddListener(onClickAction);
    }

    public void PlayOpenTile()
    {
        if (_EmptyImage != null)
        {
            _EmptyImage.gameObject.SetActive(true);
            _EmptyImage.DOFade(1, 1f).SetEase(Ease.Linear);
        }
    }

    public void PlayFoundRewardAnimation(TileFlipEventData eventInfoData, TileFlipPlayerData playerData, UnityAction onComplete)
    {
        if (_FoundRewardImage != null)
        {
            string eventKey = eventInfoData.EventKey;

            _FoundRewardImage.sprite = SpriteResourceHelper.LoadSubEventImage(eventKey, playerData.OpenedTileDict[_tileIndex]);
            SoundManager.PlaySFX(SoundManager.SFX.SFX_RPS_VICTORY);

            _FoundRewardImage.transform.localScale = Vector3.one * 1.35f;
            _FoundRewardImage.gameObject.SetActive(true);
            _FoundRewardImage.DOFade(1, 0.25f).SetEase(Ease.Linear);
            _FoundRewardImage.transform.DOScale(Vector3.one * 1.5f, 0.5f).SetEase(Ease.Linear).onComplete += delegate
            {
                _FoundRewardImage.transform.DOScale(Vector3.one, 0.25f).SetEase(Ease.Linear);
            };
            StartCoroutine(GameEventTileFlipManager.Instance.DelayActionTime(2, onComplete));
        }
    }

    public void SetButtonState(bool value)
    {
        _tileButton.enabled = value;
    }

    private void InitFlipPointer(bool value)
    {
        if (value)
        {
            if (_LeftPointer != null) _LeftPointer.gameObject.SetActive(false);
            if (_RightPointer != null) _RightPointer.gameObject.SetActive(true);
        }
        else
        {
            if (_LeftPointer != null) _LeftPointer.gameObject.SetActive(true);
            if (_RightPointer != null) _RightPointer.gameObject.SetActive(false);
        }
    }
}
