﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TileFlipButton : MonoBehaviour
{
    #region Public Properties
    public GameEventTemplateTile RefEventManager;
    public TileFlipButtonImage ButtonImage;
    public GameObject Aura;
    #endregion
    #region Private Properties
    int _buttonNo;
    #endregion
    public int ButtonNo { get { return _buttonNo; } }
    public void InitGridButton(int bNo, bool enableImg)
    {
        _buttonNo = bNo;
        ButtonImage.gameObject.SetActive(enableImg);
    }

    public void OnGridClick()
    {
        RefEventManager.OnGridClicked(_buttonNo);
    }

    public void ChangeResultImage(Sprite spr)
    {
        ChangeResultImage(spr, null);
    }

    public void ChangeResultImage(Sprite spr, Sprite spr2)
    {
        ButtonImage.gameObject.SetActive(true);
        ButtonImage.SetSprite(spr, spr2);
    }

    public void PlayChildPopAnim()
    {
        ButtonImage.PlayPopAnim();
    }
}
