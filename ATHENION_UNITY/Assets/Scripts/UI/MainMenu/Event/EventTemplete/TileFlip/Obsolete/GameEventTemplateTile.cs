﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;
using System.Linq;
using TMPro;
using UnityEngine.SceneManagement;
using DG.Tweening;
using System;
using UnityEngine.Events;
using Random = UnityEngine.Random;

public enum Asd
{
    _Close,
    _Open
}
public enum TileFlipImage
{
    BackgroundImage,
    GridImage_2x3,
    Box_Open,
    Box_Close,
    SaltyPrize,
    GridGround,
    DetailBoxImage,
    CooldownBoxImage,
    Linebreak_game1,

    None
}
public enum TileFlipStatus
{
    Idle,
    Processing,
    Renewing
}

public class GameEventTemplateTile : BaseGameEventTemplate
{
    #region Public Properties
    public TextMeshProUGUI EventNameText;
    public TextMeshProUGUI EventDescriptionText;
    public TextMeshProUGUI EventDurationText;
    public TextMeshProUGUI FlipCooldownText;
    public TextMeshProUGUI EventDetailText;
    public TextMeshProUGUI EventDetailHeader;
    public TextMeshProUGUI TextReward;
    public TextMeshProUGUI TextReadMore;
    public TextMeshProUGUI TextCooldown;
    public TileFlipButton ButtonPrefab;
    public Image BackgroundImage;
    public Image GridBackgroundImage;
    public Image GridGroundImage;
    public Image DetailButtonImage;
    public Image CooldownImage;
    public Image ItemLineBreak;
    public GameObject EventDetailWindow;
    public EventRewardCell UIRewardCell;
    public GameObject EventCanvas;
    #endregion
    #region Private Properties
    TileFlipStatus status;
    Dictionary<int, TileFlipButton> gridButtonDict;
    Dictionary<string, List<ItemData>> rewardList;
    List<GameObject> _rewardUiList;
    Dictionary<string, string> rewardTextHeader;
    Coroutine _cooldownTimer;
    Coroutine _initCoroutine;
    Coroutine _boardRefresher;
    DateTime _lastOpenStamp;
    List<ItemData> _rewardedItemList;
    TileFlipButton _targetButton;
    GameEventInfo _eventInfo;
    string _rewardTier;
    string _textColor;
    string _openSfx;
    int _cooldown;
    bool _eventDataInitialized = false;
    bool _refreshOnComplete = false;
    bool _refreshOnNewDay = false;
    float _connectionTimeOut = 30f;
    #region Assets Path
    #endregion

    #endregion
    private void Start()
    {
        EventCanvas.SetActive(false);
        PopupUIManager.Instance.Show_SoftLoad(true);
        _initCoroutine = StartCoroutine("DataInit");
        status = TileFlipStatus.Renewing;
    }

    private void ActiveEventUI()
    {
        EventCanvas.SetActive(true);
        PopupUIManager.Instance.Show_SoftLoad(false);

        status = TileFlipStatus.Idle;
        _cooldownTimer = StartCoroutine("CooldownTimer");

        if (_eventKey == null || _eventKey == "") _eventKey = "treasure_hunter_2019";
    }

    public override void Init(string eventKey, string sceneName, UnityAction onComplete = null, UnityAction onFail = null)
    {
        base.Init(eventKey, sceneName);
        //LoadDataJson(delegate (string result)
        //{
        //    // TODO: Deserialize Json.
        //    // - Event Image Background.
        //    // - Event Name Key.
        //    // - Event Detail/Rule Header Key.
        //    // - Event Detail/Rule Key.
        //    // - Price List Header Key.
        //    // - Reward Icon Image List.
        //    // - Reward Text Key List.
        //    // - Reward Amount List.

        //    // TODO: ResourceManager event content.

        //}
        //    , delegate (string errorMsg)
        //    {
        //        // TODO: Print error.
        //    }
        //);

        GameEventDB gameEventDB = DataManager.Instance.GetGameEventDB();
        _eventInfo = gameEventDB.GetGameEventInfo()[_eventKey];
        DataManager.Instance.ExcecuteEventCloudScript("GetTileFlipPlayerData", new { EventKey = _eventKey }, OnGetTileFlipPlayerData, (error) => Debug.Log(error));

        if (BackgroundImage != null) BackgroundImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, TileFlipImage.BackgroundImage.ToString());

    }

    private void OnGetTileFlipPlayerData(CloudScriptResultProcess result)
    {
        Debug.Log(result.CustomData["data"]);

        Dictionary<string, object> rawPlayerData = JsonConvert.DeserializeObject<Dictionary<string, object>>(result.CustomData["data"]);
        _lastOpenStamp = (DateTime)rawPlayerData["timestamp"];
        List<string> slots = JsonConvert.DeserializeObject<List<string>>(rawPlayerData["slot"].ToString());
        int gridPattern = Convert.ToInt32(rawPlayerData["pattern"].ToString());
        int answer = Convert.ToInt32(rawPlayerData["answer"].ToString());
        _cooldown = Convert.ToInt32(result.CustomData["flipCooldown"].ToString());
        _textColor = JsonConvert.DeserializeObject<string>(result.CustomData["textColor"]);
        _openSfx = JsonConvert.DeserializeObject<string>(result.CustomData["openSfx"]);

        InitUI();

        //Get reward data
        Dictionary<string, object> rawRewardData = JsonConvert.DeserializeObject<Dictionary<string, object>>(result.CustomData["rewards"]);
        List<ItemData> godList = GetItemListFromJson(rawRewardData["God"].ToString());
        List<ItemData> saltList = GetItemListFromJson(rawRewardData["Salt"].ToString());
        rewardList = new Dictionary<string, List<ItemData>>();
        rewardList.Add("God", godList); 
        rewardList.Add("Salt", saltList);
        rewardTextHeader = new Dictionary<string, string>();
        rewardTextHeader.Add("God", LocalizationManager.Instance.GetText("EVENT_LREWARD_" + _eventKey.ToUpper()));
        rewardTextHeader.Add("Salt", LocalizationManager.Instance.GetText("EVENT_SREWARD_" + _eventKey.ToUpper()));
        //Init reward cell
        if(_rewardUiList != null)
        {
            foreach(GameObject obj in _rewardUiList)
            {
                Destroy(obj);
            }
        }
        _rewardUiList = new List<GameObject>();
        int c = 0;
        foreach (KeyValuePair<string, List<ItemData>> listItem in rewardList)
        {
            c++;
            GameObject rewardCellObj = Instantiate(UIRewardCell.gameObject, UIRewardCell.transform.parent);
            rewardCellObj.SetActive(true);
            EventRewardCell rewardCellComp = rewardCellObj.GetComponent<EventRewardCell>();
            rewardCellComp.InitData(c + ". " + rewardTextHeader[listItem.Key], CodeToColor(_textColor, 255), listItem.Value);
            _rewardUiList.Add(rewardCellObj);
        }

        //Init grid data
        gridButtonDict = new Dictionary<int, TileFlipButton>();

        int buttonNo = 0;
        foreach (string gridBtn in slots)
        {
            GameObject newBtn = Instantiate(ButtonPrefab.gameObject, ButtonPrefab.transform.parent);
            newBtn.SetActive(true);
            TileFlipButton gridBtnComp = newBtn.GetComponent<TileFlipButton>();
            if (gridBtn == "None")
            {
                gridBtnComp.InitGridButton(buttonNo, false);
                gridButtonDict.Add(buttonNo, gridBtnComp);
            }
            else
            {
                gridBtnComp.InitGridButton(buttonNo, true);
                if (buttonNo == answer)
                {
                    gridBtnComp.ChangeResultImage(SpriteResourceHelper.LoadSubEventImage(_eventKey, TileFlipImage.Box_Close.ToString()),
                                                  SpriteResourceHelper.LoadSubEventImage(_eventKey, TileFlipImage.Box_Open.ToString()));

                    gridBtnComp.Aura.SetActive(true);
                }
                else
                {
                    gridBtnComp.ChangeResultImage(SpriteResourceHelper.LoadSubEventImage(_eventKey, TileFlipImage.SaltyPrize.ToString()));
                }
            }

            buttonNo++;
        }

        Sprite boardSprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, TileFlipImage.GridImage_2x3.ToString()+"_"+gridPattern.ToString("D2"));
        GridBackgroundImage.sprite = boardSprite;

        _refreshOnComplete = JsonConvert.DeserializeObject<string>(result.CustomData["resetOnComplete"]) == "Y";
        _refreshOnNewDay = JsonConvert.DeserializeObject<string>(result.CustomData["resetOnNewDay"]) == "Y";
        _boardRefresher = StartCoroutine("Refresher");

        _eventDataInitialized = true;
    }

    private void DestroyAllItems()
    {
        TileFlipButton[] allTileFlipButton = FindObjectsOfType<TileFlipButton>();
        foreach (TileFlipButton item in allTileFlipButton)
        {
            Destroy(item.gameObject);
        }
    }

    private void RenewBoard()
    {
        DestroyAllItems();

        PopupUIManager.Instance.Show_SoftLoad(true);
        _initCoroutine = StartCoroutine("DataInit");
        status = TileFlipStatus.Renewing;
    }

    private List<ItemData> GetItemListFromJson(string json)
    {
        List<ItemData> result = new List<ItemData>();
        List<object> objectList = JsonConvert.DeserializeObject<List<object>>(json);
        for (int i = 0; i < objectList.Count; i++)
        {
            Dictionary<string, object> item = JsonConvert.DeserializeObject<Dictionary<string, object>>(objectList[i].ToString());
            string itemID = item["itemID"].ToString();
            int amount = Convert.ToInt32(item["amount"]);
            ItemData itemData = new ItemData(itemID, amount);
            result.Add(itemData);
        }
        return result;
    }

    public override void Close()
    {
        if (status != TileFlipStatus.Idle) return;

        UnloadEventScene();
    }

    public void UnloadEventScene()
    {
        SceneManager.UnloadSceneAsync(SceneName);
    }

    public void OnGridClicked(int btnNo)
    {
        if (status != TileFlipStatus.Idle) return;

        Debug.Log(string.Format("Grid Button {0} has been clicked;", btnNo));
        if (RemainingCooldown() > 0)
        {
            PopupUIManager.Instance.ShowPopup_Error(LocalizationManager.Instance.GetText("EVENT_ERROR"), LocalizationManager.Instance.GetText("EVENT_ONCOOLDOWN"));
        }
        else
        {
            if (gridButtonDict.ContainsKey(btnNo))
            {
                PopupUIManager.Instance.ShowPopup_SureCheck(
                    LocalizationManager.Instance.GetText("EVENT_CONFIRMATION"),
                    LocalizationManager.Instance.GetText("EVENT_CONFIRM_"+_eventKey.ToUpper()),
                    delegate ()
                    {
                        OpenSlot(btnNo);
                    },
                    null
                    );
            }

        }
    }

    public void ToggleEventDetail(bool active)
    {
        if (status != TileFlipStatus.Idle) return;

        if (active)
        {
            GameHelper.UITransition_FadeIn(EventDetailWindow);
        }
        else
        {
            GameHelper.UITransition_FadeOut(EventDetailWindow);
        }
    }

    void InitUI()
    {
        SetEventText(EventNameText, "EVENT_HEADER_" + _eventKey.ToUpper(), "000000", 255);
        //SetEventText(EventDetailHeader, "EVENT_HEADER_" + _eventKey.ToUpper(), _textColor, 255);
        SetEventText(EventDescriptionText, "EVENT_DESC_" + _eventKey.ToUpper(), _textColor, 255);
        SetEventText(EventDurationText, "", _textColor, 255);
        SetEventText(EventDetailText, "EVENT_MOREDETAIL_" + _eventKey.ToUpper(), "ffffff", 255);
        SetEventText(EventDetailHeader, "EVENT_MOREDETAIL_HEADER", "000000", 255);
        SetEventText(TextCooldown, "EVENT_COOLDOWN", _textColor, 255);
        SetEventText(TextReward, "EVENT_REWARDS", _textColor, 255);
        SetEventText(TextReadMore, "EVENT_READMORE", "ffffff", 255);
        
        if (GridGroundImage != null) GridGroundImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, TileFlipImage.GridGround.ToString());
        if (DetailButtonImage != null) DetailButtonImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, TileFlipImage.DetailBoxImage.ToString());
        if (CooldownImage != null) CooldownImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, TileFlipImage.CooldownBoxImage.ToString());
        if (ItemLineBreak != null) ItemLineBreak.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, TileFlipImage.Linebreak_game1.ToString());
    }

    void InitGridButton()
    {
        gridButtonDict = new Dictionary<int, TileFlipButton>();
        Vector2Int gridSize = new Vector2Int(3, 2);

        int buttonNo = 0;
        for (int y = 0; y < gridSize.y; y++)
        {
            for (int x = 0; x < gridSize.x; x++)
            {
                GameObject newBtn = Instantiate(ButtonPrefab.gameObject, ButtonPrefab.transform.parent);
                newBtn.SetActive(true);
                TileFlipButton gridBtnComp = newBtn.GetComponent<TileFlipButton>();
                gridButtonDict.Add(buttonNo, gridBtnComp);
                gridBtnComp.InitGridButton(buttonNo, false);

                buttonNo++;
            }
        }
    }

    void OpenSlot(int btnNo)
    {
        if (!gridButtonDict.ContainsKey(btnNo)) return;

        PopupUIManager.Instance.Show_SoftLoad(true);

        _targetButton = gridButtonDict[btnNo];

        status = TileFlipStatus.Processing;
        DataManager.Instance.ExcecuteEventCloudScript("OpenTileFlipBlock", new { EventKey = _eventKey, Index = btnNo }, OnOpenedSlot, (error) => Debug.Log(error));

        GameHelper.StrToEnum(_openSfx.ToUpper(), out SoundManager.SFX sfx);
        SoundManager.PlaySFX(sfx);
    }

    private void OnOpenedSlot(CloudScriptResultProcess result)
    {
        PopupUIManager.Instance.Show_SoftLoad(false);

        _rewardedItemList = GetItemListFromJson(result.CustomData["rewardList"].ToString());
        _rewardTier = JsonConvert.DeserializeObject<string>(result.CustomData["rewardTier"].ToString());
        if (_rewardTier == "God")
        {
            _targetButton.ChangeResultImage(SpriteResourceHelper.LoadSubEventImage(_eventKey, TileFlipImage.Box_Close.ToString()),
                                          SpriteResourceHelper.LoadSubEventImage(_eventKey, TileFlipImage.Box_Open.ToString()));
            _targetButton.PlayChildPopAnim();
        }
        else
        {
            _targetButton.ChangeResultImage(SpriteResourceHelper.LoadSubEventImage(_eventKey, TileFlipImage.SaltyPrize.ToString()));
        }
        _lastOpenStamp = DateTimeData.GetDateTimeUTC();
        Invoke("SuccessOpenBlock", 1.5f);

        //Update Player's Item;
        //DataManager.Instance.InventoryData.SetEventCurrency(result.CustomData["event_currency"]);
        //DataManager.Instance.LoadInventory(null,null);

        // TODO: Event currency and Item Data.

        bool isSuccess = GameHelper.TryDeserializeJSONStr(result.CustomData["item_data"], out Dictionary<string, int> ItemDataDict);
        if (isSuccess)
        {
            List<ItemData> itemDataList = DataManager.ConvertDictionaryToItemDataList(ItemDataDict);
            foreach (ItemData itemData in itemDataList)
            {
                DataManager.Instance.InventoryData.GrantItemByItemID(itemData.ItemID, itemData.Amount);
            }
        }

        gridButtonDict.Remove(_targetButton.ButtonNo);
    }

    void SuccessOpenBlock()
    {
        ShowRewardPopUp();
        if(_rewardTier == "God") _targetButton.Aura.SetActive(true);
        //if (_rewardTier == "God") _targetButton.ChangeResultImage(SpriteResourceHelper.LoadSubEventImage(_eventKey, TileFlipImage.Box_Close.ToString()));
    }

    void ShowRewardPopUp()
    {
        status = TileFlipStatus.Idle;
        //NotificationManager.Instance.ShowPopup_GotReward("CONGRATULATIONS!", "You've got", rewardedItemList);
        PopupUIManager.Instance.ShowPopup_GotRewardAnimated(LocalizationManager.Instance.GetText("EVENT_CONGRATS"), LocalizationManager.Instance.GetText("EVENT_YOUVEGOT"), _rewardedItemList)
            .SetAnimationEase(Ease.OutExpo)
            .SetDelay(0.1f)
            .SetAnimationDuration(0.25f)
            .SetMaxScale(8f)
            ;
    }

    void SetEventText(TextMeshProUGUI txt, string localizeCode, string coloCode, int alpha)
    {
        if (txt == null) return;

        txt.text = LocalizationManager.Instance.GetText(localizeCode);
        txt.color = CodeToColor(coloCode, alpha);
    }

    float RemainingCooldown()
    {
        DateTime curDate = DateTimeData.GetDateTimeUTC();
        float cooltimeRemain = (float)(_lastOpenStamp - curDate).TotalSeconds + _cooldown;
        return cooltimeRemain;
    }

    IEnumerator CooldownTimer()
    {
        while (true)
        {
            float remainingCooldown = RemainingCooldown();
            if (remainingCooldown > 0)
            {
                int h, m, s;
                h = Mathf.FloorToInt(remainingCooldown / 3600);
                m = Mathf.FloorToInt((remainingCooldown - h * 3600) / 60);
                s = Mathf.FloorToInt(remainingCooldown % 60);
                if (h > 0)
                {
                    FlipCooldownText.text = string.Format("{0}:{1}:{2}", h.ToString("D2"), m.ToString("D2"), s.ToString("D2"));
                }
                else
                {
                    FlipCooldownText.text = string.Format("{0}:{1}", m.ToString("D2"), s.ToString("D2"));
                }
            }
            else
            {
                FlipCooldownText.text = "Ready!";
            }

            yield return new WaitForEndOfFrame();
        }
    }

    IEnumerator Refresher()
    {
        while (true)
        {
            if (status == TileFlipStatus.Idle)
            {
                if(_refreshOnComplete && gridButtonDict.Count <= 0)
                {
                    RenewBoard();
                    DataManager.Instance.ExcecuteEventCloudScript("GetTileFlipPlayerData", new { EventKey = _eventKey }, OnGetTileFlipPlayerData, (error) => Debug.Log(error));
                    StopCoroutine(_boardRefresher);
                }
            }
            yield return new WaitForEndOfFrame();
        }
    }

    IEnumerator DataInit()
    {
        float timeCounter = 0;
        while (timeCounter <= _connectionTimeOut)
        {
            timeCounter += Time.unscaledDeltaTime;
            if (_eventDataInitialized)
            {
                ActiveEventUI();
                StopCoroutine(_initCoroutine);
            }
            yield return new WaitForEndOfFrame();
        }
        PopupUIManager.Instance.ShowPopup_Error("Connection Timeout!", "Error! Can not retrive event data!");
        UnloadEventScene();
        yield return null;
    }

    Color CodeToColor(string colorCode, int alpha)
    {
        int red = Convert.ToInt32(colorCode.Substring(0,2), 16);
        int blue = Convert.ToInt32(colorCode.Substring(2, 2), 16);
        int green = Convert.ToInt32(colorCode.Substring(4, 2), 16);
        Color c = new Color32(Convert.ToByte(red), Convert.ToByte(blue), Convert.ToByte(green),255);
        return c;
    }
}