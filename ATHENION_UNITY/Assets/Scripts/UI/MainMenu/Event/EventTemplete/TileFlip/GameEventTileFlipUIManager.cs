﻿using System;
using System.Collections;
using System.Collections.Generic;
using GameEvent;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class GameEventTileFlipUIManager : MonoBehaviour
{
    #region Static Value
    // Localization Key
    private static readonly string EventCooldownEndPrefixKey = "EVENT_COOLDOWN_END_";
    private static readonly string EventRewardsKey = "EVENT_REWARDS";
    private static readonly string EventBigRewardLabelPrefixKey = "EVENT_LREWARD_";
    private static readonly string EventSmallRewardLabelPrefixKey = "EVENT_SREWARD_";
    private static readonly string EventGridBgPrefixKey = "GRID_";
    #endregion

    #region Public Properties

    public bool IsCooldown => _isCooldown;
    public bool IsPointerEnable => _pointerEnable;
    public List<TileFlipController> TileControllerList => _tileButtonList;

    #endregion

    #region Private Properties
    private bool _isCooldown;
    private GameEventTileFlipManager _manager;
    #endregion

    #region Inspector Properties
    [Header("Text UI Elements")]
    [SerializeField] private TextMeshProUGUI _eventHeaderText;
    [SerializeField] private TextMeshProUGUI _eventObjectiveText;
    [SerializeField] private TextMeshProUGUI _eventCooldownLabelText;
    [SerializeField] private TextMeshProUGUI _eventCooldownValueText;
    [SerializeField] private TextMeshProUGUI _eventRewardLabelText;
    [SerializeField] private TextMeshProUGUI _eventRewardBigTierText;
    [SerializeField] private TextMeshProUGUI _eventRewardSmallTierText;
    [SerializeField] private List<TextMeshProUGUI> _eventBigRewardAmountTextList;
    [SerializeField] private List<TextMeshProUGUI> _eventSmallRewardAmountTextList;

    [Header("Image UI Elements")]
    [SerializeField] private List<Image> _eventBigRewardImageList;
    [SerializeField] private List<Image> _eventSmallRewardImageList;
    [SerializeField] private Image _eventHeaderBgImage;
    [SerializeField] private Image _gridBackgroundImage;

    [Header("Button UI Elements")]
    [SerializeField] private Button _eventInfoButton;
    [SerializeField] private List<TileFlipController> _tileButtonList;

    [Header("Other Setting")]
    [SerializeField] private RectTransform _tileParent;
    [SerializeField] private bool _pointerEnable;
    #endregion

    #region Methods

    #region Init Methods

    /// <summary>
    /// Call this method to init UI.
    /// </summary>
    /// <param name="manager">Game event template manager.</param>
    public void InitUi(GameEventTileFlipManager manager)
    {
        #region Prepare Data

        _manager = manager; 
        var eventInfoData = _manager.EventInfoData;
        var playerData = _manager.PlayerData;

        #endregion

        #region Text UI

        if (_eventHeaderText != null) _eventHeaderText.text = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventHeaderKey + eventInfoData.EventKey);
        if (_eventObjectiveText != null) _eventObjectiveText.text = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventObjectiveKey + eventInfoData.EventKey);
        if (_eventRewardLabelText != null) _eventRewardLabelText.text = LocalizationManager.Instance.GetText(EventRewardsKey);
        if (_eventRewardBigTierText != null) _eventRewardBigTierText.text = LocalizationManager.Instance.GetText(EventBigRewardLabelPrefixKey + eventInfoData.EventKey);
        if (_eventRewardSmallTierText != null) _eventRewardSmallTierText.text = LocalizationManager.Instance.GetText(EventSmallRewardLabelPrefixKey + eventInfoData.EventKey);

        RunCooldown();

        #endregion

        #region Image UI

        _eventHeaderBgImage.sprite = SpriteResourceHelper.LoadSprite(eventInfoData.EventThemeInfo.Theme + GameEventTemplateKeys.EventHeaderBannerSuffix, SpriteResourceHelper.SpriteType.GAME_Event);
        _gridBackgroundImage.sprite = SpriteResourceHelper.LoadSubEventImage(eventInfoData.EventKey, EventGridBgPrefixKey + playerData.PatternTile);

        #endregion

        #region Reward UI

        // Reward Images
        if (_eventBigRewardImageList != null)
        {
            for (int i = 0; i < _eventBigRewardImageList.Count; i++)
            {
                _eventBigRewardImageList[i].sprite = SpriteResourceHelper.LoadSprite(eventInfoData.WinRewardDataList[i].ItemID, SpriteResourceHelper.SpriteType.ICON_Item);
            }
        }

        if (_eventSmallRewardImageList != null)
        {
            for (int i = 0; i < _eventSmallRewardImageList.Count; i++)
            {
                _eventSmallRewardImageList[i].sprite = SpriteResourceHelper.LoadSprite(eventInfoData.NormalRewardDataList[i].ItemID, SpriteResourceHelper.SpriteType.ICON_Item);
            }
        }

        // Reward Amount Texts
        if (_eventBigRewardAmountTextList != null)
        {
            for (int i = 0; i < _eventBigRewardAmountTextList.Count; i++)
            {
                _eventBigRewardAmountTextList[i].text = eventInfoData.WinRewardDataList[i].Amount.ToString();
            }
        }

        if (_eventSmallRewardAmountTextList != null)
        {
            for (int i = 0; i < _eventSmallRewardAmountTextList.Count; i++)
            {
                _eventSmallRewardAmountTextList[i].text = eventInfoData.NormalRewardDataList[i].Amount.ToString();
            }
        }

        #endregion

        #region Generate Button

        GenerateTileButton();

        #endregion
    }

    /// <summary>
    /// Call this method to set tile action.
    /// </summary>
    /// <param name="tileAction"></param>
    public void SetTileButton(UnityAction<int> tileAction)
    {
        for (int i = 0; i < _tileButtonList.Count; i++)
        {
            var index = i;
            _tileButtonList[i].SetButtonAction(() => tileAction?.Invoke(index));
        }
    }

    /// <summary>
    /// Call this method to set event info button.
    /// </summary>
    /// <param name="action">Button delegate method action.</param>
    public void SetEventInfoBtn(UnityAction action)
    {
        _eventInfoButton.onClick.RemoveAllListeners();
        _eventInfoButton.onClick.AddListener(action);
    }

    /// <summary>
    /// Call this method to generate tile button.
    /// </summary>
    private void GenerateTileButton()
    {
        #region Prepare Data

        //var eventInfoData = _manager.EventInfoData;
        var playerData = _manager.PlayerData;
        //var buttonCount = eventInfoData.TileHeight * eventInfoData.TileWidth;
        //_tileControllerList = new List<TileFlipController>();

        for (int i = 0; i < _tileButtonList.Count; i++)
        {
            //GameObject item = Instantiate(_tileButtonPrefab.gameObject, _tileParent);
            //var tileScript = item.GetComponent<TileFlipController>();
            //_tileButtonList[i].Init(playerData.OpenedTileDict[i]);

            //if (_pointerEnable)
            //{
            //    _tileButtonList[i].InitFlipPointer(Convert.ToBoolean(i % 2));
            //}

            //_tileControllerList.Add(tileScript);

            _tileButtonList[i].Init(i, this, _manager.EventInfoData, _manager.PlayerData);
        }

        #endregion
    }

    /// <summary>
    /// Call this method to run cooldown.
    /// </summary>
    public void RunCooldown()
    {
        var eventInfoData = _manager.EventInfoData;
        var playerData = _manager.PlayerData;

        if (_eventCooldownValueText.text != null)
        {
            if (eventInfoData.PlayTriggerType == GameEventStartType.Cooldown && eventInfoData.DefaultCooldown > 0)
            {
                if (!_isCooldown)
                {
                    _isCooldown = true;
                    GameEventManager.Instance.RunRoutineTask(
                        _manager.CooldownTimer(
                            playerData.LastPlayDateTime
                            , eventInfoData.DefaultCooldown
                            , _eventCooldownValueText
                            , CoolDownEnd
                        )
                    );
                }
            }
        }
    }

    #endregion

    public void OnTileActionStart(int tileIndex)
    {
        //_tileControllerList[tileIndex].PlayOpenTile();
        _tileButtonList[tileIndex].PlayOpenTile();
    }

    public void OnTileActionEnd(int tileIndex, UnityAction onComplete)
    {
        if (_manager.PlayerData.OpenedTileDict[tileIndex] != "None")
        {
            _tileButtonList[tileIndex].PlayFoundRewardAnimation(_manager.EventInfoData, _manager.PlayerData, onComplete);
        }
        else
        {
            onComplete?.Invoke();
        }
    }
    
    /// <summary>
    /// Call this method when cooldown is end.
    /// </summary>
    private void CoolDownEnd()
    {
        var eventInfoData = _manager.EventInfoData;
        _eventCooldownValueText.text = LocalizationManager.Instance.GetText(EventCooldownEndPrefixKey + eventInfoData.EventKey);
        _isCooldown = false;
    }

    public void SetButtonsState(bool value)
    {
        _eventInfoButton.enabled = value;
        foreach (var tile in _tileButtonList)
        { 
            tile.SetButtonState(value);
        }
    }

    #endregion
}
