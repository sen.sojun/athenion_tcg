﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;

public class GameEventTileFlipManager : BaseGameEventTemplate
{
    #region Static Value

    private static readonly string _actionErrorTitleString = "ERROR_ACTION_TITLE_";
    private static readonly string _actionErrorMessageString = "ERROR_ACTION_MESSAGE_";

    private static readonly string _actionOpenedTitleKey = "EVENT_OPENED_TITLE_";
    private static readonly string _actionOpenedMessageKey = "EVENT_OPENED_MESSAGE_";

    private static readonly string _actionResultString = "is_win_reward";

    private static readonly string _bigRewardKey = "Win";
    private static readonly string _smallRewardKey = "Normal";

    #endregion

    #region Inspector Properties

    [Header("UI Controller")]
    [SerializeField] private GameEventTileFlipUIManager _uiController;

    [Header("CloudScript Function ")]
    [SerializeField] private string _initCloudScriptFunctionName;
    [SerializeField] private string _actionCloudScriptFunctionName;

    #endregion

    #region Public Properties
    public TileFlipEventData EventInfoData { get; protected set; }
    public TileFlipPlayerData PlayerData { get; protected set; }
    #endregion

    #region Private Properties
    #endregion

    #region Methods

    #region Override Methods

    public override void Init(string eventKey, string sceneName, UnityAction onComplete = null, UnityAction onFail = null)
    {
        base.Init(eventKey, sceneName, onComplete, onFail);

        DataManager.Instance.ExcecuteEventCloudScript(
            _initCloudScriptFunctionName
            , new {EventKey = eventKey}
            , result =>
            {
                if (result.IsSuccess == false)
                {
                    onFail?.Invoke();
                    return;
                }

                EventInfoData = new TileFlipEventData(eventKey, result);
                PlayerData = new TileFlipPlayerData(result);

                if (EventInfoData.IsLoadedSuccess && PlayerData.IsLoadedSuccess)
                {
                    InitUI();
                    onComplete?.Invoke();
                }
                else
                {
                    onFail?.Invoke();
                }
            }
            , errMsh =>
            {
                onFail?.Invoke();
            }
        );
    }

    #endregion

    #region Init Methods

    private void InitUI()
    {
        _uiController.InitUi(this);
        _uiController.SetTileButton(OnSelectAction);
        _uiController.SetEventInfoBtn(OnShowEventInfo);
    }

    #endregion

    #region Btn OnClick Methods

    /// <summary>
    /// Call this method to do open tile action.
    /// </summary>
    /// <param name="tileIndex">Tile index.</param>
    private void OnSelectAction(int tileIndex)
    {
        PopupUIManager.Instance.Show_SoftLoad(true);
        _uiController.SetButtonsState(false);

        // Check button controller valid.
        if (!CheckButtonControllerValid(tileIndex))
        {
            return;
        }

        // Check action valid.
        if (!CheckActionValid(tileIndex))
        {
            return;
        }

        // Call generate participate reward.
        if(EventInfoData.ParticipateRewardDataList != null && EventInfoData.ParticipateRewardDataList.Count > 0)
        {
            StartCoroutine(ActionRewardGenerate(
                _uiController.TileControllerList[tileIndex].transform
                , EventInfoData.ParticipateRewardDataList)
            );
        }

        // Start opening tile.
        StartOpenTile(tileIndex);
    }

    /// <summary>
    /// Call this method to show event info.
    /// </summary>
    private void OnShowEventInfo()
    {
        //EventDetailImage.gameObject.SetActive(true);
        string title = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventDetailHeaderKey);
        string message = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventDetailKey + EventInfoData.EventKey);

        PopupUIManager.Instance.ShowPopup_DetailWindow(title, message);
    }

    #endregion

    #region Checking Methods

    /// <summary>
    /// Call this method to check if the action is valid or not.
    /// </summary>
    /// <returns></returns>
    private bool CheckActionValid(int tileIndex)
    {
        switch (EventInfoData.PlayTriggerType)
        {
            case GameEventStartType.Cooldown when _uiController.IsCooldown:
            {
                string title = LocalizationManager.Instance.GetText(GameEventTemplateKeys.CooldownTitleKey);
                string message = LocalizationManager.Instance.GetText(GameEventTemplateKeys.CooldownMessageKey);
                PopupUIManager.Instance.ShowPopup_Error(
                    title
                    , message
                    , delegate
                    {
                        PopupUIManager.Instance.Show_SoftLoad(false);
                        _uiController.SetButtonsState(true);
                    }
                );
                return false;
            }

            case GameEventStartType.Cooldown when PlayerData.OpenedTileDict[tileIndex] == _bigRewardKey || PlayerData.OpenedTileDict[tileIndex] == _smallRewardKey:
            {
                string title = LocalizationManager.Instance.GetText(_actionOpenedTitleKey + EventInfoData.EventKey);
                string message = LocalizationManager.Instance.GetText(_actionOpenedMessageKey + EventInfoData.EventKey);
                PopupUIManager.Instance.ShowPopup_Error(
                    title
                    , message
                    , delegate
                    {
                        PopupUIManager.Instance.Show_SoftLoad(false);
                        _uiController.SetButtonsState(true);
                    }
                );
                    return false;
            }
        }

        return true;
    }

    /// <summary>
    /// Call this method to check button controller valid.
    /// </summary>
    /// <param name="actionIndex"></param>
    /// <returns></returns>
    private bool CheckButtonControllerValid(int actionIndex)
    {
        if (_uiController.TileControllerList.Count < actionIndex)
        {
            string title = LocalizationManager.Instance.GetText(_actionErrorTitleString + EventInfoData.EventKey);
            string message = LocalizationManager.Instance.GetText(_actionErrorMessageString + EventInfoData.EventKey);
            PopupUIManager.Instance.ShowPopup_Error(
                title
                , message
                , delegate
                {
                    PopupUIManager.Instance.Show_SoftLoad(false);
                    _uiController.SetButtonsState(true);
                }
            );
            return false;
        }

        return true;
    }

    #endregion

    #region Action Methods

    private void StartOpenTile(int tileIndex)
    {
        bool isSuccess = true;

        _uiController.OnTileActionStart(tileIndex);

        DataManager.Instance.ExcecuteEventCloudScript(
            _actionCloudScriptFunctionName
            , new
            {
                EventKey = EventInfoData.EventKey
                , TileIndex = tileIndex
            }
            , result =>
            {
                isSuccess &= result.IsSuccess;

                if (!isSuccess)
                {
                    PopupUIManager.Instance.Show_SoftLoad(false);
                    _uiController.SetButtonsState(true);

                    PopupUIManager.Instance.ShowPopup_Error(
                            LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventActionErrorTitleKey)
                        , LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventActionErrorMessageKey)
                        , delegate
                        {
                            PopupUIManager.Instance.Show_SoftLoad(false);
                            _uiController.SetButtonsState(true);
                        }
                    );

                    return;
                }

                PlayerData.UpdatePlayerData(result);

                //string ecJson = result.CustomData[GameEventTemplateKeys.EventCurrencyKey];
                //DataManager.Instance.InventoryData.SetEventCurrency(ecJson);
                //DataManager.Instance.LoadInventory(null, null);

                isSuccess = GameHelper.TryDeserializeJSONStr(result.CustomData[GameEventTemplateKeys.PlayerReceivedItemKey], out Dictionary<string, int> itemDict);
                if (isSuccess)
                {
                    List<ItemData> itemDataList = DataManager.ConvertDictionaryToItemDataList(itemDict);
                    DataManager.Instance.InventoryData.GrantItemByItemList(itemDataList);
                    OnReceiveItem(itemDataList);
                }

                _uiController.RunCooldown();

                _uiController.OnTileActionEnd(
                    tileIndex
                    , () =>
                    {
                        var rewardList = new List<ItemData>();

                        if (PlayerData.OpenedTileDict[tileIndex] == _bigRewardKey)
                        {
                            rewardList = EventInfoData.WinRewardDataList;
                        }
                        else if (PlayerData.OpenedTileDict[tileIndex] == _smallRewardKey)
                        {
                            rewardList = EventInfoData.NormalRewardDataList;
                        }

                        PopupUIManager.Instance.Show_SoftLoad(false);
                        _uiController.SetButtonsState(true);

                        PopupUIManager.Instance.ShowPopup_GotRewardAnimated(
                            LocalizationManager.Instance.GetText(GameEventTemplateKeys.NotiRewardTitleKey)
                            , LocalizationManager.Instance.GetText(GameEventTemplateKeys.NotiRewardMessageKey)
                            , rewardList
                            , null

                        ).SetAnimationEase(Ease.OutExpo)
                        .SetDelay(0.1f)
                        .SetAnimationDuration(0.25f)
                        .SetMaxScale(8f);
                    }
                );
            }
            , Debug.LogError
        );
    }

    #endregion

    #endregion
}

public class TileFlipEventData : BaseGameEventData
{
    #region CloudScript Key

    private static readonly string TileWidthKey = "tile_width";
    private static readonly string TileHeightKey = "tile_height";
    private static readonly string WinRewardKey = "win_reward";
    private static readonly string NormalRewardKey = "normal_reward";

    #endregion

    #region Public Properties

    public List<ItemData> WinRewardDataList { get; private set; }
    public List<ItemData> NormalRewardDataList { get; private set; }
    public List<ItemData> ParticipateRewardDataList { get; private set; }
    public int TileWidth => _tileWidth;
    public int TileHeight => _tileHeight;
    #endregion

    #region Private Properties

    private int _tileWidth;
    private int _tileHeight;

    #endregion

    #region Constructor

    public TileFlipEventData(string eventKey, CloudScriptResultProcess eventData) : base (eventKey, eventData) 
    {
        #region Init Data Instance

        WinRewardDataList = new List<ItemData>();
        NormalRewardDataList = new List<ItemData>();
        ParticipateRewardDataList = new List<ItemData>();

        _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(eventData.CustomData[GameEventTemplateKeys.EventDataKey], out Dictionary<string, object> eventDataDict);

        #endregion

        #region Rewards Data

        _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(eventDataDict[WinRewardKey].ToString(), out Dictionary<string, int> winRewardDict);
        _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(eventDataDict[NormalRewardKey].ToString(), out Dictionary<string, int> normalRewardDict);
        _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(eventDataDict[GameEventTemplateKeys.ParticipateRewardListKey].ToString(), out Dictionary<string, int> participateRewardDict);

        if (!_isLoadedSuccess) return;
        WinRewardDataList = DataManager.ConvertDictionaryToItemDataList(winRewardDict);
        NormalRewardDataList = DataManager.ConvertDictionaryToItemDataList(normalRewardDict);
        if(participateRewardDict != null && participateRewardDict.Count > 0)
            ParticipateRewardDataList = DataManager.ConvertDictionaryToItemDataList(participateRewardDict);
        else
            ParticipateRewardDataList = new List<ItemData>();
    
        #endregion

        #region Tile Data

        _isLoadedSuccess &= int.TryParse(eventDataDict[TileWidthKey].ToString(), out _tileWidth);
        _isLoadedSuccess &= int.TryParse(eventDataDict[TileHeightKey].ToString(), out _tileHeight);

        #endregion
    }

    #endregion
}

public class TileFlipPlayerData
{
    #region CloudScript Key
    private static readonly string OpenedTileListKey = "opened_tile";
    private static readonly string PatternTileKey = "pattern_tile";
    #endregion

    #region Public Properties
    public bool IsLoadedSuccess => _isLoadedSuccess;
    public DateTime LastPlayDateTime => _lastPlayDateTime;
    public Dictionary<int, string> OpenedTileDict => _openedTileDict;
    public int PatternTile => _patternTile;
    #endregion

    #region Private Properties
    private bool _isLoadedSuccess = true;
    private DateTime _lastPlayDateTime;
    private Dictionary<int, string> _openedTileDict;
    private int _patternTile;
    #endregion

    #region Constructor

    public TileFlipPlayerData(CloudScriptResultProcess playerData)
    {
        _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(playerData.CustomData[GameEventTemplateKeys.PlayerDataKey], out Dictionary<string, object> playerDataDictionary);
        if (!_isLoadedSuccess) return;

        _isLoadedSuccess &= DateTime.TryParse(playerDataDictionary[GameEventTemplateKeys.PlayerActionTimestampKey].ToString(), out _lastPlayDateTime);
        _isLoadedSuccess &= int.TryParse(playerDataDictionary[PatternTileKey].ToString(), out _patternTile);
        _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(playerDataDictionary[OpenedTileListKey].ToString(), out _openedTileDict);

    }

    #endregion

    #region Methods

    public void UpdatePlayerData(CloudScriptResultProcess playerData)
    {
        _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(playerData.CustomData[GameEventTemplateKeys.PlayerDataKey], out Dictionary<string, object> playerDataDictionary);
        if (!_isLoadedSuccess) return;

        _isLoadedSuccess &= DateTime.TryParse(playerDataDictionary[GameEventTemplateKeys.PlayerActionTimestampKey].ToString(), out _lastPlayDateTime);
        _isLoadedSuccess &= int.TryParse(playerDataDictionary[PatternTileKey].ToString(), out _patternTile);
        _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(playerDataDictionary[OpenedTileListKey].ToString(), out _openedTileDict);

    }

    #endregion
}
