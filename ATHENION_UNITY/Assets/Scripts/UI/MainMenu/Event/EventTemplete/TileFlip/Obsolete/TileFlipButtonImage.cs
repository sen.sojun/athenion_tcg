﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TileFlipButtonImage : MonoBehaviour
{
    Sprite _openSprite;
    public Image MainImage;
    public Animator MainAnimator;

    private void Awake()
    {
        MainImage = GetComponent<Image>();
        MainAnimator = GetComponent<Animator>();
    }

    public void SetSprite(Sprite spr, Sprite nextSpr)
    {
        MainImage.sprite = spr;
        if(nextSpr != null) _openSprite = nextSpr;
    }

    public void PlayPopAnim()
    {
        MainAnimator.SetTrigger("Open");
    }

    public void ChangeSpriteToOpen()
    {
        if (_openSprite != null)
        {
            MainImage.sprite = _openSprite;
        }
    }
}
