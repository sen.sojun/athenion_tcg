﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;
using System.Linq;
using TMPro;
using UnityEngine.SceneManagement;
using DG.Tweening;
using System;
using System.Security.Cryptography;
using GameEvent;
using Spine;
using Spine.Unity;
using Karamucho;
using UnityEngine.Events;
using Random = UnityEngine.Random;

public class GameEventTemplatePrepareGift : BaseGameEventTemplate
{
    #region enums
    public enum AssetName
    {
        None,
        GiftButtonReady,
        GiftButtonNotReady,
        EventBackground,
        BTNGift,
        TextBar,
        GoalBox,
        HeaderBanner,
        CharacterStand,
        MainCharacter,
        Ticket,
        TicketBox,
        Gauge_Bar,
        Gauge_Fill,
        RewardBox,

        SD_Sample,
        SD_Idle,
        SD_Ready,
        SD_Done,
        SD_Happy,

        Sub_SD_Idle,
        Sub_SD_Ready,
        Sub_SD_Done,

        Last
    }
    public enum PrepareGiftStatus
    {
        Idle,
        Processing,
        Renewing
    }
    #endregion
    #region Static Value

    private static readonly string AnimationIdleString = "IDLE";
    private static readonly string AnimationActionString = "ACTION";
    private static readonly string AnimationEndString = "END";

    #endregion
    #region Public Properties
    [Header("UI Elements.")]
    public TextMeshProUGUI TXTGiftButton;
    public TextMeshProUGUI TXTEventName;
    public TextMeshProUGUI TXTEventObjective;
    public TextMeshProUGUI TXTEventPeriod;
    public TextMeshProUGUI TXTEventDetail;
    public TextMeshProUGUI TXTEventDetailHeader;
    public TextMeshProUGUI TXTEventLimit;
    public TextMeshProUGUI TXTDailyChanceLeft;
    public TextMeshProUGUI TXTGave;
    public TextMeshProUGUI TXTShowTicket;
    public Image GiftButtonCooldown;
    public RewardCell AttempRewardObj;
    public EventGoalRewardPackage CumulativeRewardPrefab;
    public Slider ProgressBar;
    public GameObject UIGroup;
    public GameObject TicketUIGroup;
    public Transform VFXParent;
    [SerializeField] private Button _readMoreButton;
    [SerializeField] private Button _closeButton;
    [SerializeField] private List<Animator> _animatorList;
    [Header("Image UI Ref")]
    public Image BackgroundImage;
    public Image CharacterBGImage;
    public Image GiftButtonImage;
    public Image TextBarImage;
    public Image HeaderBanner;
    public Image CharacterStand;
    public Image SR_Background;
    public Image SR_MainCharacter;
    public Image GaugeBar;
    public Image GaugeFill;
    public Image IMGTicket;
    public Image IMGTicketBox;
    public Image IMGGoalBox;
    public Image IMGSDChar;
    [SerializeField] private Image _IMGSubSDChar;
    #endregion
    #region Private Properties
    PrepareGiftStatus _status;
    private Dictionary<string, List<ItemData>> _rewardList;
    private Dictionary<string, PrepareGiftCumulativeData> _cumulativeGiftData;
    private Dictionary<int, EventGoalRewardPackage> _cumulativeRewardUI;
    private GameEventInfo _eventInfo;
    private Coroutine _cdTimer;
    private DateTime _playerLastTimeStamp;
    private Vector3 _mainCharPivot;
    private Vector3 _attempRewardOriP;
    private Vector3 _attempRewardTargP;
    private SkeletonGraphic _characterSpineObj;
    private float _cooldown;
    private int _completeScore;
    private int _dailyLimit;
    private int _playerDailyCount;
    private int _attempRewardIndex;
    private int _playerCumulativeCount;
    private int _ticketRequire;
    private int _playerTicket;
    private string _ticketID;
    private string _textColor;
    private string _attempRewardKey;
    private bool _resetAfterComplete;
    private bool _isOnCooldown;
    private bool _playableAfterFinished;

    private static readonly string PrefabResourcePath = "Events/{0}/Prefabs/";
    private static readonly string ParticleKey = "ParticlePrefab";
    #endregion

    private void OnEnable()
    {
        if (_isOnCooldown)
        {
            foreach (var animator in _animatorList)
            {
                animator.Play(AnimationEndString);
            }
        }
        else
        {
            if (_playerDailyCount <= 0)
            {
                foreach (var animator in _animatorList)
                {
                    animator.Play(AnimationEndString);
                }
            }
            else
            {
                foreach (var animator in _animatorList)
                {
                    animator.Play(AnimationIdleString);
                }
            }
        }
    }

    public override void Init(string eventKey, string sceneName, UnityAction onComplete = null, UnityAction onFail = null)
    {
        base.Init(eventKey, sceneName);

        GameEventDB gameEventDB = DataManager.Instance.GetGameEventDB();
        _eventInfo = gameEventDB.GetGameEventInfo()[_eventKey];
        DataManager.Instance.ExcecuteEventCloudScript(
            "GetPrepareGiftData"
            , new { EventKey = _eventKey }
            , result =>
            {
                if (result.IsSuccess)
                {
                    OnSuccessGetData(result, onComplete);
                }
                else
                {
                    onFail?.Invoke();
                }
            }
            , err =>
            {
                onFail?.Invoke();
                OnCommonError(err);
            }
        );
        //PopupUIManager.Instance.Show_SoftLoad(true);

        _eventKey = eventKey;

        _attempRewardOriP = AttempRewardObj.transform.position;
        _attempRewardTargP = new Vector3(0f, 40f, 0f);

        UIGroup.SetActive(false);
    }

    public void GiveGift()
    {
        SoundManager.PlaySFX(SoundManager.SFX.SFX_RPS_BUBBLE_SPAWN);

        if (_status != PrepareGiftStatus.Idle)
        {
            return;
        }
        if (!_playableAfterFinished)
        {
            int maxScore = 0;

            foreach (KeyValuePair<string, PrepareGiftCumulativeData> item in _cumulativeGiftData)
            {
                int.TryParse(item.Key, out int score);
                if (score > maxScore)
                {
                    maxScore = score;
                }
            }

            if (_playerCumulativeCount >= maxScore)
            {
                PopupUIManager.Instance.ShowPopup_Error(LocalizationManager.Instance.GetText("EVENT_ERROR"),
                                          LocalizationManager.Instance.GetText("EVENT_FINISHED_" + _eventKey.ToUpper()));
                _status = PrepareGiftStatus.Idle;
                return;
            }
        }
        if (_isOnCooldown)
        {
            CooldownError();
            return;
        }
        if (_dailyLimit > 0 && _playerDailyCount <= 0)
        {
            LimitError();
            return;
        }

        if (IsRequireTicket)
        {
            if (RequireTicket[0] > _playerTicket)
            {
                //NOT ENOUGH TICKET
                PopupUIManager.Instance.ShowPopup_Error(LocalizationManager.Instance.GetText("EVENT_ERROR"),
                                                        LocalizationManager.Instance.GetText("EVENT_NO_TICKET_MESSAGE"));
                return;
            }
            else
            {
                _playerTicket -= RequireTicket[0];
            }
        }

        foreach (var animator in _animatorList)
        {
            animator.Play(AnimationActionString);
        }

        DataManager.Instance.LoadInventory(null, null);
        PopupUIManager.Instance.Show_SoftLoad(true);

        ShowAttempReward();

        _status = PrepareGiftStatus.Processing;
        DataManager.Instance.ExcecuteEventCloudScript("GiveAGift", new { EventKey = _eventKey , TicketID = _ticketID}, OnSuccessGift, OnCommonError);

        //if (IMGSDChar != null)
        //{
        //    IMGSDChar.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, AssetName.SD_Ready.ToString());
        //}

        //if (_IMGSubSDChar != null)
        //{
        //    _IMGSubSDChar.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, AssetName.Sub_SD_Ready.ToString());
        //}

        //GameHelper.StrToEnum("SFX_OPEN_" + _eventKey.ToUpper(), out SoundManager.SFX sfx);
        //SoundManager.PlaySFX(sfx);
    }

    public override void Close()
    {
        PopupUIManager.Instance.Show_SoftLoad(true);

        GameHelper.UITransition_FadeOut(FadeOut.gameObject, () =>
        {
            UnloadEventScene();
            PopupUIManager.Instance.Show_SoftLoad(false);
        });
    }

    public void UnloadEventScene()
    {
        SceneManager.UnloadSceneAsync(SceneName);
    }

    protected override void OnDestroy()
    {
        _cdTimer = null;

        base.OnDestroy();
    }

    private void StartCooldownTimer()
    {
        if (!_isOnCooldown)
        {
            _isOnCooldown = true;

            foreach (var animator in _animatorList)
            {
                animator.Play(AnimationEndString);
            }

            _cdTimer = GameEventManager.Instance.RunRoutineTask(CooldownTimer(
                _playerLastTimeStamp
                , DefaultCooldown
                , TXTGiftButton
                , () =>
                {
                    _isOnCooldown = false;
                    _cdTimer = null;
                    SetEventText(TXTGiftButton, LocalizationManager.Instance.GetText("EVENT_GIVE_BUTTON_" + _eventKey.ToUpper()), _textColor, 255);

                    foreach (var animator in _animatorList)
                    {
                        animator.Play(AnimationIdleString);
                    }
                }));
        }
        else
        {
            Debug.LogWarningFormat("StartCooldownTimer: already start coroutine.");
        }
    }

    private void ShowAttempReward()
    {
        if (_rewardList.ContainsKey(_attempRewardKey) && _rewardList[_attempRewardKey].Count > 0)
        {
            AttempRewardObj.gameObject.SetActive(true);
            _attempRewardIndex = 0;
            RepeatableAttempRewardPopup();
        }
    }

    private void RepeatableAttempRewardPopup()
    {
        ItemData attempReward = _rewardList[_attempRewardKey][_attempRewardIndex];
        AttempRewardObj.InitData(attempReward.ItemID, attempReward.Amount);
        AttempRewardObj.transform.position = _attempRewardOriP;
        AttempRewardObj.transform.DOMove(_attempRewardOriP+_attempRewardTargP, 0.65f).OnComplete
            (
                delegate ()
                {
                    _attempRewardIndex++;
                    if (_attempRewardIndex < _rewardList[_attempRewardKey].Count)
                    {
                        RepeatableAttempRewardPopup();
                    }
                    else
                    {
                        AttempRewardObj.gameObject.SetActive(false);
                        AttempRewardObj.transform.position = _attempRewardOriP;
                    }
                }
            );
    }

    private void OnSuccessGift(CloudScriptResultProcess result)
    {
        PopupUIManager.Instance.Show_SoftLoad(false);
        _status = PrepareGiftStatus.Idle;

        if (result.MessageValue.StartsWith("0"))
        {
            OnCommonError("", delegate () { Close(); });
            return;
        }

        if (result.CustomData.ContainsKey("playerData"))
        {
            Dictionary<string, object> rawPlayerData = JsonConvert.DeserializeObject<Dictionary<string, object>>(result.CustomData["playerData"]);

            _playerLastTimeStamp = (DateTime)rawPlayerData["timestamp"];
            int.TryParse(rawPlayerData["cumulativecount"].ToString(), out _playerCumulativeCount);
            int.TryParse(rawPlayerData["chances"].ToString(), out _playerDailyCount);

            if (_playerCumulativeCount == _completeScore) OnEventComplete();
        }

        List<ItemData> rewardList = new List<ItemData>();

        if (result.CustomData.ContainsKey("cumulativeReward"))
        {
            List<Dictionary<string, object>> items = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(result.CustomData["cumulativeReward"].ToString());
            foreach (Dictionary<string, object> item in items)
            {
                string itemID = item["itemID"].ToString();
                int amount = Convert.ToInt32(item["amount"]);
                rewardList.Add(new ItemData(itemID, amount));
            }

            PopupUIManager.Instance.ShowPopup_GotRewardAnimated(LocalizationManager.Instance.GetText("EVENT_CONGRATS"), LocalizationManager.Instance.GetText("EVENT_YOUVEGOT"), rewardList);
            OnReceiveItem(rewardList);
        }
        OnReceiveItem(_rewardList[_attempRewardKey]);

        UpdateUI(rewardList);
        //DataManager.Instance.InventoryData.SetEventCurrency(result.CustomData["event_currency"]);
        //DataManager.Instance.LoadInventory(null, null);

        // TODO: Event currency and Item Data.

        bool isSuccess = GameHelper.TryDeserializeJSONStr(result.CustomData["item_data"], out Dictionary<string, int> ItemDataDict);
        if (isSuccess)
        {
            List<ItemData> itemDataList = DataManager.ConvertDictionaryToItemDataList(ItemDataDict);
            foreach (ItemData itemData in itemDataList)
            {
                DataManager.Instance.InventoryData.GrantItemByItemID(itemData.ItemID, itemData.Amount);
            }
        }

        //if (IMGSDChar != null)
        //{
        //    IMGSDChar.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, AssetName.SD_Done.ToString());
        //}

        //if (_IMGSubSDChar != null)
        //{
        //    _IMGSubSDChar.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, AssetName.Sub_SD_Done.ToString());
        //}

        //StartCoroutine(DelayActionTime(2, delegate
        //{
        //    if (IMGSDChar != null)
        //    {
        //        IMGSDChar.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, AssetName.SD_Idle.ToString());
        //    }

        //    if (_IMGSubSDChar != null)
        //    {
        //        _IMGSubSDChar.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, AssetName.Sub_SD_Idle.ToString());
        //    }
        //}));
    }

    private void OnSuccessGetData(CloudScriptResultProcess result, UnityAction onComplete = null)
    {
        base.InitData(result);

        //Debug.Log(result.CustomData["playerData"]);
        //Debug.Log(result.CustomData["eventData"]);
        Dictionary<string, object> rawPlayerData = JsonConvert.DeserializeObject<Dictionary<string, object>>(result.CustomData["player_data"]);
        Dictionary<string, object> rawEventData = JsonConvert.DeserializeObject<Dictionary<string, object>>(result.CustomData["event_data"]);

        //init event data
        _ticketID = GameEventTemplateKeys.TicketIDPrefixKey + _eventThemeInfo.Theme;
        //_ticketID = GameEventTemplateKeys.TicketIDPrefixKey + "HP19";

        float.TryParse(rawEventData["default_cooldown"].ToString(), out _cooldown);
        float.TryParse(rawEventData["main_char_pivot_x"].ToString(), out float pivotX);
        float.TryParse(rawEventData["main_char_pivot_y"].ToString(), out float pivotY);
        int.TryParse(rawEventData["default_playtime"].ToString(), out _dailyLimit);
        _playableAfterFinished = rawEventData["playable_after_finished"].ToString() == "Y";
        _textColor = rawEventData["textcolor"].ToString();
        _attempRewardKey = rawEventData["attempreward"].ToString();
        _resetAfterComplete = rawEventData["resetaftercomplete"].ToString() == "Y";

        //_mainCharPivot = new Vector3(pivotX, pivotY);

        Dictionary<string, object> rawRewardList = JsonConvert.DeserializeObject<Dictionary<string, object>>(rawEventData["rewards"].ToString());
        Dictionary<string, object> rawCumulativeList = JsonConvert.DeserializeObject<Dictionary<string, object>>(rawEventData["cumulativegiftdata"].ToString());

        _rewardList = new Dictionary<string, List<ItemData>>();
        foreach(KeyValuePair<string, object> rawReward in rawRewardList)
        {
            List<Dictionary<string, object>> items = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(rawReward.Value.ToString());
            List<ItemData> itemDataList = new List<ItemData>();
            foreach (Dictionary<string, object> item in items)
            {
                string itemID = item["itemID"].ToString();
                int amount = Convert.ToInt32(item["amount"]);
                ItemData itemData = new ItemData(itemID, amount);
                itemDataList.Add(itemData);
            }
            _rewardList.Add(rawReward.Key, itemDataList);
        }
        _cumulativeGiftData = new Dictionary<string, PrepareGiftCumulativeData>();
        foreach (KeyValuePair<string, object> rawCumulativeData in rawCumulativeList)
        {
            Dictionary<string, object> item = JsonConvert.DeserializeObject<Dictionary<string, object>>(rawCumulativeData.Value.ToString());
            PrepareGiftCumulativeData newData = new PrepareGiftCumulativeData(  item["reward"].ToString(), 
                                                                                item["animation_prefix"].ToString(),
                                                                                item["character_stand"].ToString(),
                                                                                item["vfx"].ToString());
            _cumulativeGiftData.Add(rawCumulativeData.Key, newData);
            int.TryParse(rawCumulativeData.Key, out int cCount);
            _completeScore = _completeScore < cCount ? cCount : _completeScore;
        }

        //init player data
        _playerLastTimeStamp = (DateTime)rawPlayerData["timestamp"];
        int.TryParse(rawPlayerData["cumulativecount"].ToString(), out _playerCumulativeCount);
        int.TryParse(rawPlayerData["chances"].ToString(), out _playerDailyCount);
        _playerTicket = DataManager.Instance.InventoryData.GetAmountByItemID(_ticketID);

        //_cdTimer = StartCoroutine("CooldownTimer");
        //StartCooldownTimer();

        InitUI();
        UpdateUI(null);
        
        //if (IMGSDChar != null)
        //{
        //    IMGSDChar.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, AssetName.SD_Idle.ToString());
        //}

        #region Button UI

        //_closeButton.onClick.RemoveAllListeners();
        //_closeButton.onClick.AddListener(Close);

        _readMoreButton.onClick.RemoveAllListeners();
        _readMoreButton.onClick.AddListener(EventDetail);

        #endregion

        onComplete?.Invoke();
        //GameHelper.UITransition_FadeIn(FadeIn.gameObject, () => PopupUIManager.Instance.Show_SoftLoad(false));
    }

    private void InitUI()
    {
        //Create cumulative gift data UI;
        Sprite cr_RewardBG = SpriteResourceHelper.LoadSubEventImage(_eventKey, AssetName.RewardBox.ToString());
        Sprite cr_ReqBG = SpriteResourceHelper.LoadSubEventImage(_eventKey, AssetName.GoalBox.ToString());

        _cumulativeRewardUI = new Dictionary<int, EventGoalRewardPackage>();
        foreach (KeyValuePair<string, PrepareGiftCumulativeData> data in _cumulativeGiftData)
        {
            if (data.Key == "0") continue; //0 means default.

            GameObject obj = Instantiate(CumulativeRewardPrefab.gameObject, CumulativeRewardPrefab.transform.parent);
            obj.SetActive(true);
            EventGoalRewardPackage cellComp = obj.GetComponent<EventGoalRewardPackage>();
            cellComp.InitData(string.Format(LocalizationManager.Instance.GetText("EVENT_TIMES_" + _eventKey.ToUpper()), data.Key), 
                                                                                _rewardList[data.Value.RewardKey], 
                                                                                false, 
                                                                                true, 
                                                                                cr_RewardBG, 
                                                                                cr_ReqBG
                                                                                );
            cellComp.SetPopupMessage(string.Format(LocalizationManager.Instance.GetText("EVENT_POPUP_HEADER_" + _eventKey.ToUpper()), data.Key),
                                                   LocalizationManager.Instance.GetText("EVENT_POPUP_DETAIL_" + _eventKey.ToUpper()));
            _cumulativeRewardUI.Add(int.Parse(data.Key), cellComp);
        }

        SetEventText(TXTEventName, LocalizationManager.Instance.GetText("EVENT_HEADER_" + _eventKey.ToUpper()), _textColor, 255);
        SetEventText(TXTEventObjective, LocalizationManager.Instance.GetText("EVENT_OBJECTIVE_" + _eventKey.ToUpper()), _textColor, 255);
        SetEventText(TXTEventPeriod, string.Format(LocalizationManager.Instance.GetText("EVENT_PERIOD_" + _eventKey.ToUpper()), ""),
                                    _textColor, 255, true);
        
        SetEventText(TXTEventDetail, LocalizationManager.Instance.GetText("EVENT_MOREDETAIL_" + _eventKey.ToUpper()), "ffffff", 255);
        SetEventText(TXTEventDetailHeader, LocalizationManager.Instance.GetText("EVENT_MOREDETAIL_HEADER"), "000000", 255);

        SetEventText(TXTEventLimit, LocalizationManager.Instance.GetText("EVENT_GIVE_LIMIT_" + _eventKey.ToUpper()), _textColor, 255);

        if (BackgroundImage != null) BackgroundImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, AssetName.EventBackground.ToString());
        if (HeaderBanner != null) HeaderBanner.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, AssetName.HeaderBanner.ToString());
        if (GiftButtonImage != null)
        {
            GiftButtonImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, AssetName.BTNGift.ToString());
            GiftButtonImage.SetNativeSize();

            //GiftButtonCooldown.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, AssetName.BTNGift.ToString());
            //GiftButtonCooldown.SetNativeSize();
        }
        if (TextBarImage != null)
        {
            Sprite spr = SpriteResourceHelper.LoadSubEventImage(_eventKey, AssetName.TextBar.ToString());
            if (spr != null)
            {
                TextBarImage.sprite = spr;
            }
            else
            {
                TextBarImage.gameObject.SetActive(false);
            }
        }
        if (CharacterStand != null)
        {
            Sprite spr = SpriteResourceHelper.LoadSubEventImage(_eventKey, AssetName.CharacterStand.ToString(), true);
            if (spr != null)
            {
                CharacterStand.sprite = spr;
                CharacterStand.enabled = true;
            }
            else
            {
                CharacterStand.enabled = false;
            }

            //Instantiate character
            
            string targetPath = string.Format("Events/{0}/Prefabs/", _eventKey) + AssetName.MainCharacter.ToString();
            GameObject obj = ResourceManager.Load(targetPath) as GameObject;

            if (obj != null)
            {
                GameObject newlyCreatedObj = Instantiate(obj, CharacterStand.transform);
                _characterSpineObj = newlyCreatedObj.GetComponent<SkeletonGraphic>();
            }
            else
            //If spine object is null, use SpriteRenderer instead.
            {
                SR_Background.gameObject.SetActive(true);

                //Adjust its position
                //SR_MainCharacter.GetComponent<RectTransform>().pivot = _mainCharPivot;
                //SR_MainCharacter.GetComponent<RectTransform>().transform.localPosition = new Vector3(); //Reset its position after new pivot is set.

                CharacterStand.gameObject.SetActive(false);
            }
        }
        if (GaugeBar != null) GaugeBar.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, AssetName.Gauge_Bar.ToString());
        if (GaugeFill != null) GaugeFill.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, AssetName.Gauge_Fill.ToString());

        //if (_IMGSubSDChar != null) _IMGSubSDChar.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, AssetName.Sub_SD_Idle.ToString());

        if (PlayTriggerType == GameEventStartType.Ticket)
        {
            TXTEventLimit.text = "";

            TicketUIGroup.SetActive(true);
            IMGTicket.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, AssetName.Ticket.ToString());
            IMGTicketBox.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, AssetName.TicketBox.ToString());

            if (_dailyLimit > 0)
            {
                TXTDailyChanceLeft.gameObject.SetActive(true);
                string text = $"{_playerDailyCount}/{_dailyLimit}";
                SetEventText(TXTDailyChanceLeft, text, _textColor, 255, true);
            }
            else
                TXTDailyChanceLeft.gameObject.SetActive(false);
        }
        else if (PlayTriggerType == GameEventStartType.Cooldown)
        {
            if (_dailyLimit > 0)
            {
                TXTDailyChanceLeft.gameObject.SetActive(true);
                string text = $"{_playerDailyCount}/{_dailyLimit}";
                SetEventText(TXTDailyChanceLeft, text, _textColor, 255, true);
            }
            else
                TXTDailyChanceLeft.gameObject.SetActive(false);
        }

        UIGroup.SetActive(true);
    }

    private void UpdateUI(List<ItemData> rewardList)
    {   
        foreach (KeyValuePair<int, EventGoalRewardPackage> data in _cumulativeRewardUI)
        {
            data.Value.ActiveClaimedTick(_playerCumulativeCount >= data.Key);
        }

        //Update progress bar
        float barLength = 0f;
        float rangeBetweenGoal = 1f / (_cumulativeGiftData.Count - 1); //-1 = ignore default;
        int calculatingCount = _playerCumulativeCount;
        int previousTarget = 0;
        int currentTarget = 0;
        foreach(KeyValuePair<string, PrepareGiftCumulativeData> data in _cumulativeGiftData)
        {
            if (data.Key == "0") continue; //skip 0 coz its default value;

            if(int.TryParse(data.Key, out currentTarget))
            {
                int targetLength = currentTarget - previousTarget;
                if (calculatingCount >= targetLength)
                {
                    barLength += rangeBetweenGoal;
                }
                else
                {
                    barLength += rangeBetweenGoal * ((float)calculatingCount / (currentTarget - previousTarget));
                }
                calculatingCount -= targetLength;

                if (calculatingCount <= 0) break;

                previousTarget = currentTarget;
            }
        }
        ProgressBar.value = barLength;

        if(_dailyLimit > 0 && _playerDailyCount <= 0)
        {
            if(_cdTimer != null)
                StopCoroutine(_cdTimer);
            SetEventText(TXTGiftButton, "EVENT_LIMIT_REACHED_SHORT_" + _eventKey.ToUpper(), _textColor, 255);
            string text = $"{_playerDailyCount}/{_dailyLimit}";
            SetEventText(TXTDailyChanceLeft, text, _textColor, 255, true);
        }
        else if (_dailyLimit > 0 && _playerDailyCount > 0)
        {
            string text = $"{_playerDailyCount}/{_dailyLimit}";
            SetEventText(TXTDailyChanceLeft, text, _textColor, 255, true);
        }

        if (IsRequireTicket)
        {
            SetTicketAmount(_playerTicket, RequireTicket[0]);
            if (_dailyLimit > 0)
            {
                string text = $"{_playerDailyCount}/{_dailyLimit}";
                SetEventText(TXTDailyChanceLeft, text, _textColor, 255, true);
            }
        }

        SetEventText(TXTGave,
                     string.Format(LocalizationManager.Instance.GetText("EVENT_ALREADY_GIFT_" + _eventKey.ToUpper()), _playerCumulativeCount),
                     _textColor, 255);

        //Change Character Stand and play VFX
        PrepareGiftCumulativeData matchedData = null;
        foreach (KeyValuePair<string, PrepareGiftCumulativeData> list in _cumulativeGiftData)
        {
            int.TryParse(list.Key.ToString(), out int currentLvl);
            if (_playerCumulativeCount >= currentLvl)
            {
                matchedData = list.Value;
            }
        }

        if (matchedData != null)
        {
            string standImage = matchedData.CharacterStand;
            Sprite spr = SpriteResourceHelper.LoadSubEventImage(_eventKey, standImage, true);
            if (spr != null && CharacterStand != null)
            {
                CharacterStand.sprite = spr;
                CharacterStand.enabled = true;
            }

            string vfxName = matchedData.OnReachVFX;
            //========================================================
            //play at VFXParent transform
            //========================================================

            UnityAction showPopUp = delegate
            {

            if (rewardList != null && rewardList.Count > 0)
            {
                PopupUIManager.Instance
                    .ShowPopup_GotRewardAnimated(LocalizationManager.Instance.GetText("EVENT_CONGRATS"),
                        LocalizationManager.Instance.GetText("EVENT_YOUVEGOT"), 
                        rewardList
                        ,delegate () 
                        {
                            //if (IMGSDChar != null)
                            //{
                            //    IMGSDChar.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, AssetName.SD_Idle.ToString());
                            //}
                        })
                        .SetAnimationEase(Ease.OutExpo)
                        .SetDelay(0.1f)
                        .SetAnimationDuration(0.25f)
                        .SetMaxScale(8f)
                        ;
                }
            };
            
            if (rewardList != null && rewardList.Count > 0)
            {
                // add particle
                GameObject particleItem = ResourceManager.Load(string.Format(PrefabResourcePath, _eventKey) + ParticleKey) as GameObject;
                if (particleItem != null)
                {
                    GameObject objLeft = Instantiate(particleItem, VFXParent);
                    Destroy(objLeft, 2);
                }
            }
            
            //========================================================
            //Play animation on success gift
            //If _characterSpineObj is a spine animation

            if (_characterSpineObj != null)
            {
                if (rewardList == null)
                {
                    _characterSpineObj.AnimationState.SetAnimation(0, matchedData.AnimationPrefix + "_" + "idle01", true);
                }
                else
                {
                    SoundManager.PlaySFX(SoundManager.SFX.SFX_RPS_VICTORY);
                    _characterSpineObj.AnimationState.SetAnimation(0, matchedData.AnimationPrefix + "_" + "hap01", false).Complete +=
                        delegate (TrackEntry entry)
                        {
                            showPopUp?.Invoke();
                        };
                    _characterSpineObj.AnimationState.AddAnimation(0, matchedData.AnimationPrefix + "_" + "idle01", true, 0f);
                }
            }
            else
            {
                SR_Background.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, matchedData.CharacterStand, true);
                SR_MainCharacter.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, matchedData.AnimationPrefix, true);

                //Adjust its position
                //SR_MainCharacter.GetComponent<RectTransform>().pivot = _mainCharPivot;
                //SR_MainCharacter.GetComponent<RectTransform>().transform.localPosition = new Vector3(); //Reset its position after new pivot is set.
            }
        }

        StartCooldownTimer();

    }

    private void OnCommonError(string arg0)
    {
        PopupUIManager.Instance.Show_SoftLoad(false);
        PopupUIManager.Instance.ShowPopup_Error(LocalizationManager.Instance.GetText("EVENT_ERROR"),
                                                LocalizationManager.Instance.GetText("EVENT_GENERAL_ERROR"));
        _status = PrepareGiftStatus.Idle;
    }
    private void OnCommonError(string arg0, UnityAction onComplete)
    {
        PopupUIManager.Instance.Show_SoftLoad(false);
        PopupUIManager.Instance.ShowPopup_Error(LocalizationManager.Instance.GetText("EVENT_ERROR"),
                                                LocalizationManager.Instance.GetText("EVENT_GENERAL_ERROR"), onComplete);
        _status = PrepareGiftStatus.Idle;
    }

    private void CooldownError()
    {
        PopupUIManager.Instance.ShowPopup_Error(LocalizationManager.Instance.GetText("EVENT_ERROR"),
                                                LocalizationManager.Instance.GetText("EVENT_COOLDOWN_NOTREADY_" + _eventKey.ToUpper()));
        _status = PrepareGiftStatus.Idle;
    }

    private void LimitError()
    {
        PopupUIManager.Instance.ShowPopup_Error(LocalizationManager.Instance.GetText("EVENT_ERROR"),
                                                LocalizationManager.Instance.GetText("EVENT_DAILY_LIMIT_REACHED_" + _eventKey.ToUpper()));
        _status = PrepareGiftStatus.Idle;
    }

    private void SetTicketAmount(int currentTicket, int requireTicket)
    {
        //TXTShowTicket.text = string.Format("{0}/{1}", currentTicket, requireTicket);
        SetEventText(TXTShowTicket, string.Format("{0}/{1}", currentTicket, requireTicket), _textColor, 255);
    }

    void SetEventText(TextMeshProUGUI txt, string txtString, string colorCode, int alpha, bool completeString = false)
    {
        if (txt == null) return;

        if(completeString)
            txt.text = txtString;
        else
            txt.text = LocalizationManager.Instance.GetText(txtString);

        //txt.color = CodeToColor(colorCode, alpha);
    }

    Color CodeToColor(string colorCode, int alpha)
    {
        int red = Convert.ToInt32(colorCode.Substring(0, 2), 16);
        int blue = Convert.ToInt32(colorCode.Substring(2, 2), 16);
        int green = Convert.ToInt32(colorCode.Substring(4, 2), 16);
        Color c = new Color32(Convert.ToByte(red), Convert.ToByte(blue), Convert.ToByte(green), 255);
        return c;
    }

    public void EventDetail()
    {
        string title = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventDetailHeaderKey);
        string message = LocalizationManager.Instance.GetText("EVENT_DETAIL_" + _eventKey);

        PopupUIManager.Instance.ShowPopup_DetailWindow(title, message);
    }
}

public class PrepareGiftCumulativeData
{
    public string RewardKey;
    public string AnimationPrefix;
    public string CharacterStand;
    public string OnReachVFX;

    public PrepareGiftCumulativeData()
    {

    }

    public PrepareGiftCumulativeData(string rewardKey, string animPrefix = "", string standImage = "", string onReachVFX = "")
    {
        RewardKey = rewardKey;
        AnimationPrefix = animPrefix;
        CharacterStand = standImage;
        OnReachVFX = onReachVFX;
    }
}
