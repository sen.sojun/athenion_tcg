﻿using System.Collections;
using System.Collections.Generic;
using GameEvent;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class GameEventPrepareGiftUIManager : MonoBehaviour
{
    #region Static Value

    private static readonly string _eventStartButtonPrefixKey = "EVENT_START_BUTTON_TEXT_";
    private static readonly string _eventProgressionPrefixKey = "PROGRESS_";

    private static readonly string _eventCharIdleStateKey = "IDLE";
    private static readonly string _eventCharCooldownStateKey = "COOLDOWN";
    private static readonly string _eventCharActionStateKey = "ACTION";
    private static readonly string _eventTransitionEnd = "END";

    #endregion

    #region Inspector Properties

    [Header("Image UI Elements")]
    [SerializeField] private Image _EventThemeHeaderImage;
    [SerializeField] private Image _EventThemeBgImage;
    [SerializeField] private Image _EventActionImage;
    [SerializeField] private Image _EventBannerBgImage;
    [SerializeField] private Image _EventGiftImage;
    [SerializeField] private Image _EventProgressionImage;

    [Header("Text UI Elements")]
    [SerializeField] private TextMeshProUGUI _EventHeaderText;
    [SerializeField] private TextMeshProUGUI _EventObjectiveText;
    [SerializeField] private TextMeshProUGUI _EventActionText;
    [SerializeField] private TextMeshProUGUI _EventPlayTimeText;

    [Header("Button UI Elements")]
    [SerializeField] private Button _EventInfoButton;
    [SerializeField] private Button _EventActionButton;

    [Header("Animator Elements")]
    [SerializeField] private List<Animator> _EventCharacterAnimatorList;
    [SerializeField] private Animator _EventPrepareGiftTransitionAnimator;

    [Header("Reward Elements")]
    [SerializeField] private List<PrepareGiftRewardController> _EventRewardControllerList;

    [Header("Other Setting")]
    [SerializeField] private Material _GreyScaleMaterial;

    #endregion

    #region Public Properties

    public bool IsCoolDown => _isCoolDown;

    #endregion

    #region Private Properties

    private bool _isCoolDown;
    private GameEventPrepareGiftManager _manager;

    private float _rewardOneSliderProgress;
    private List<float> _rewardSliderProgressList;

    private int _totalReceivedReward;
    #endregion

    #region Methods

    #region Init Methods

    public void InitUi(GameEventPrepareGiftManager manager)
    {
        #region Init Data

        _manager = manager;
        PrepareGiftEventData eventInfoData = _manager.EventInfoData;
        PrepareGiftPlayerData playerData = _manager.PlayerData;
        string eventKey = eventInfoData.EventKey;
        _totalReceivedReward = playerData.ReceivedRewardDict.Count;

        _rewardOneSliderProgress = 1 / (float)(eventInfoData.RewardList.Count);
        _rewardSliderProgressList = new List<float>();

        for (int i = 0; i < eventInfoData.RewardList.Count; i++)
        {
            int day;
            if (i == 0)
            {
                day = eventInfoData.GiftProgressionCondition[i];
            }
            else
            {
                day = eventInfoData.GiftProgressionCondition[i] - eventInfoData.GiftProgressionCondition[i - 1];
            }

            float progressSize = _rewardOneSliderProgress / day;
            _rewardSliderProgressList.Add(progressSize);
        }

        #endregion

        #region Text UI

        if (_EventHeaderText != null) _EventHeaderText.text = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventHeaderKey + eventKey);
        if (_EventObjectiveText != null) _EventObjectiveText.text = string.Format(LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventObjectiveKey + eventKey), playerData.GiftProgression);
        if (_EventPlayTimeText != null) _EventPlayTimeText.text = $"{playerData.RemainPlayTime}/{eventInfoData.DefaultPlaytime}";
        

        #endregion

        #region Image UI

        if (_EventThemeHeaderImage != null) _EventThemeHeaderImage.sprite = SpriteResourceHelper.LoadSprite(eventInfoData.Theme + GameEventTemplateKeys.EventThemeHeaderImgSuffix, SpriteResourceHelper.SpriteType.GAME_Event);
        if (_EventThemeBgImage != null) _EventThemeBgImage.sprite = SpriteResourceHelper.LoadSprite(eventInfoData.Theme + GameEventTemplateKeys.EventThemeBgImgSuffix, SpriteResourceHelper.SpriteType.GAME_Event);
        if (_EventBannerBgImage != null) _EventBannerBgImage.sprite = SpriteResourceHelper.LoadSprite(eventInfoData.Theme + GameEventTemplateKeys.EventHeaderBannerSuffix, SpriteResourceHelper.SpriteType.GAME_Event);
        if (_EventGiftImage != null) _EventGiftImage.sprite = SpriteResourceHelper.LoadSubEventImage(eventKey, _eventProgressionPrefixKey + _totalReceivedReward);

        UpdateProgressBar();

        #endregion

        #region Other UI

        for (int i = 0; i < _EventRewardControllerList.Count; i++)
        {
            _EventRewardControllerList[i].Init(i, _manager.EventInfoData, _manager.PlayerData);
        }

        #endregion
    }

    public void SetActionBtn(UnityAction action)
    {
        if (_EventActionButton == null) return;

        _EventActionButton.onClick.RemoveAllListeners();
        _EventActionButton.onClick.AddListener(action);
    }

    public void SetEventInfoBtn(UnityAction action)
    {
        if (_EventInfoButton == null) return;
        _EventInfoButton.onClick.RemoveAllListeners();
        _EventInfoButton.onClick.AddListener(action);
    }

    private void OnEnable()
    {
        RunCooldown();
    }

    #endregion

    #region Update Methods

    public void RunCooldown()
    {
        var eventInfoData = _manager.EventInfoData;
        var playerData = _manager.PlayerData;

        if (eventInfoData.PlayTriggerType == GameEventStartType.Cooldown && eventInfoData.DefaultCooldown > 0)
        {
            if (!_isCoolDown)
            {
                _isCoolDown = true;
                UpdateCharacterAnimation(_eventCharCooldownStateKey);
                GameEventManager.Instance.RunRoutineTask(
                    _manager.CooldownTimer(
                        playerData.LastPlayDateTime
                        , eventInfoData.DefaultCooldown
                        , _EventActionText
                        , CoolDownEnd
                    )
                );
            }
        }
    }

    private void CoolDownEnd()
    {
        string eventKey = _manager.EventInfoData.EventKey;

        _isCoolDown = false;
        if (_EventActionText != null) _EventActionText.text = LocalizationManager.Instance.GetText(_eventStartButtonPrefixKey + eventKey);
        if (_EventActionButton != null) _EventActionButton.image.material = null;

        UpdateCharacterAnimation(_eventCharIdleStateKey);
    }

    public void OnActionStart()
    {
        StartCoroutine(_manager.ActionRewardGenerate(
            _EventActionButton.transform
            , _manager.EventInfoData.ParticipateRewardList)
        );
        UpdateCharacterAnimation(_eventCharActionStateKey);
        _EventPrepareGiftTransitionAnimator.Play(_eventCharActionStateKey);
    }

    public void OnActionEnd(UnityAction onComplete)
    {
        UpdateProgressBar();
        RunCooldown();

        foreach (var rewardController in _EventRewardControllerList)
        {
            rewardController.UpdateController(_manager.EventInfoData, _manager.PlayerData);
        }

        if (_EventPlayTimeText != null) _EventPlayTimeText.text = $"{_manager.PlayerData.RemainPlayTime}/{_manager.EventInfoData.DefaultPlaytime}";
        if (_EventObjectiveText != null) _EventObjectiveText.text = string.Format(LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventObjectiveKey + _manager.EventInfoData.EventKey), _manager.PlayerData.GiftProgression);

        StartCoroutine(_manager.DelayActionTime(1.5f, onComplete));

    }

    private void UpdateProgressBar()
    {
        var eventInfoData = _manager.EventInfoData;
        var playerData = _manager.PlayerData;

        if (_EventProgressionImage != null)
        {
            int index = -1;
            foreach (int condition in eventInfoData.GiftProgressionCondition)
            {
                index++;
                if (playerData.GiftProgression <= condition)
                    break;
            }

            if (index > -1 && index < eventInfoData.GiftProgressionCondition.Count)
            {
                _EventProgressionImage.fillAmount = _rewardOneSliderProgress * index;
                if (index == 0)
                    _EventProgressionImage.fillAmount += _rewardSliderProgressList[index] * playerData.GiftProgression;
                else if (index > 0)
                    _EventProgressionImage.fillAmount += _rewardSliderProgressList[index] * (playerData.GiftProgression - eventInfoData.GiftProgressionCondition[index - 1]);
            }
            else if (index >= eventInfoData.GiftProgressionCondition.Count)
            {
                _EventProgressionImage.fillAmount = 1;
            }
        }

        if (_totalReceivedReward != playerData.ReceivedRewardDict.Count)
        {
            _totalReceivedReward = playerData.ReceivedRewardDict.Count;
            _EventGiftImage.sprite = SpriteResourceHelper.LoadSubEventImage(eventInfoData.EventKey,
                _eventProgressionPrefixKey + _totalReceivedReward);
        }

        _EventPrepareGiftTransitionAnimator.Play(_eventTransitionEnd);
    }

    private void UpdateCharacterAnimation(string animationState)
    {
        foreach (var animator in _EventCharacterAnimatorList)
        {
            animator.Play(animationState);
        }
    }

    #endregion

    #region Setting Methods

    public void SetAllBtnState(bool value)
    {
        SetEventBtnState(value);
        SetActionBtnState(value);
        SetRewardBtnState(value);
    }

    private void SetEventBtnState(bool value)
    {
        _EventInfoButton.enabled = value;
    }

    private void SetActionBtnState(bool value)
    {
        _EventActionButton.enabled = value;
    }

    private void SetRewardBtnState(bool value)
    {
        foreach (var rewardButton in _EventRewardControllerList)
        {
            rewardButton.SetBtnState(value);
        }
    }

    #endregion

    #endregion
}
