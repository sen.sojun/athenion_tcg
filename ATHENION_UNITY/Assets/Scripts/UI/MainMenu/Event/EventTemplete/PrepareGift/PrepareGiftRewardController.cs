﻿using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class PrepareGiftRewardController : MonoBehaviour
{
    #region Static Value

    private const string PrepareGiftRewardTitlePrefix = "EVENT_PREPARE_GIFT_REWARD_TITLE_";
    private const string PrepareGiftRewardMessagePrefix = "EVENT_PREPARE_GIFT_REWARD_MESSAGE_";

    private const string RewardIconPrefix = "REWARD_";

    #endregion

    #region Private Static

    private int _rewardIndex;
    private int _progression;
    private string _eventKey;
    private List<ItemData> _itemData;

    #endregion

    #region Inspector Properties

    [Header("UI Elements")]

    [SerializeField] private Image _RewardIconImage;
    [SerializeField] private Image _ReceivedIconImage;
    [SerializeField] private Button _RewardButton;

    #endregion

    #region Methods

    #region Init Methods

    public void Init(int rewardIndex, PrepareGiftEventData eventData, PrepareGiftPlayerData playerData)
    {
        #region Data

        _rewardIndex = rewardIndex;
        _eventKey = eventData.EventKey;
        _itemData = eventData.RewardList[_rewardIndex];
        _progression = eventData.GiftProgressionCondition[_rewardIndex];

        #endregion

        #region Image

        if (_RewardIconImage != null) _RewardIconImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, RewardIconPrefix + _rewardIndex);
        if (_ReceivedIconImage != null && playerData.ReceivedRewardDict.ContainsKey(_rewardIndex)) _ReceivedIconImage.gameObject.SetActive(true);

        #endregion

        #region Button

        _RewardButton.onClick.RemoveAllListeners();
        _RewardButton.onClick.AddListener(ShowItemPackDetail);

        #endregion
    }

    #endregion

    #region Update Methods

    public void UpdateController(PrepareGiftEventData eventData, PrepareGiftPlayerData playerData)
    {
        if (_ReceivedIconImage != null && playerData.ReceivedRewardDict.ContainsKey(_rewardIndex))
        {
            if (_ReceivedIconImage.gameObject.activeInHierarchy == false)
            {
                _ReceivedIconImage.gameObject.SetActive(true);

                PopupUIManager.Instance.ShowPopup_GotRewardAnimated(
                        LocalizationManager.Instance.GetText(GameEventTemplateKeys.NotiRewardTitleKey)
                        , LocalizationManager.Instance.GetText(GameEventTemplateKeys.NotiRewardMessageKey)
                        , _itemData
                        , null
                    ).SetAnimationEase(Ease.OutExpo)
                    .SetDelay(0.1f)
                    .SetAnimationDuration(0.25f)
                    .SetMaxScale(8f);
            }
        }
    }

    #endregion

    #region Other Methods

    private void ShowItemPackDetail()
    {
        PopupUIManager.Instance.ShowPopup_PreviewReward(
            string.Format(LocalizationManager.Instance.GetText(PrepareGiftRewardTitlePrefix + _eventKey), (_rewardIndex + 1))
            , string.Format(LocalizationManager.Instance.GetText(PrepareGiftRewardMessagePrefix + _eventKey), (_progression))
            , _itemData
        );
    }

    #endregion

    #region Setting Methods

    public void SetBtnState(bool value)
    {
        _RewardButton.enabled = value;
    }

    #endregion

    #endregion
}
