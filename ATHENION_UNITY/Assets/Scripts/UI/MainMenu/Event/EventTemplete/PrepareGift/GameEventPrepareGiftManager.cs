﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameEventPrepareGiftManager : BaseGameEventTemplate
{
    #region Static Value

    

    #endregion

    #region Public Properties

    public PrepareGiftEventData EventInfoData { get; private set; }
    public PrepareGiftPlayerData PlayerData { get; private set; }

    #endregion

    #region Private Properties



    #endregion

    #region Inspector Properties

    [Header("UI Controller")] [SerializeField]
    private GameEventPrepareGiftUIManager _UiController;

    [Header("CloudScript Function")]
    [SerializeField] private string _InitCloudScriptFunctionName;
    [SerializeField] private string _ActionCloudScriptFunctionName;

    #endregion

    #region Methods

    #region Override Methods

    public override void Init(string eventKey, string sceneName, UnityAction onComplete = null, UnityAction onFail = null)
    {
        base.Init(eventKey, sceneName, onComplete, onFail);

        DataManager.Instance.ExcecuteEventCloudScript(
            _InitCloudScriptFunctionName
            , new {EventKey = eventKey}
            , result =>
            {
                if (!result.IsSuccess)
                {
                    onFail?.Invoke();
                    return;
                }

                EventInfoData = new PrepareGiftEventData(eventKey, result);
                PlayerData = new PrepareGiftPlayerData(result);

                if (EventInfoData.IsLoadedSuccess && PlayerData.IsLoadedSuccess)
                {
                    InitUI();
                    onComplete?.Invoke();
                }
                else
                {
                    onFail?.Invoke();
                }
            }
            , errMsg =>
            {
                onFail?.Invoke();
            }
        );
    }

    #endregion

    #region Init Methods

    private void InitUI()
    {
        _UiController.InitUi(this);
        _UiController.SetActionBtn(OnSelectAction);
        _UiController.SetEventInfoBtn(OnShowEventDetail);
    }

    #endregion

    #region Btn OnClick Methods

    private void OnSelectAction()
    {
        _UiController.SetAllBtnState(true);

        if (CheckActionValid() == false)
        {
            _UiController.SetAllBtnState(true);
            return;
        }

        _UiController.OnActionStart();

        PopupUIManager.Instance.Show_SoftLoad(true);

        DataManager.Instance.ExcecuteEventCloudScript(
            _ActionCloudScriptFunctionName
            , new
            {
                EventKey = EventInfoData.EventKey
            }
            , result =>
            {
                if (result.IsSuccess == false)
                {
                    PopupUIManager.Instance.Show_SoftLoad(false);
                    _UiController.SetAllBtnState(true);

                    PopupUIManager.Instance.ShowPopup_Error(
                        LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventActionErrorTitleKey)
                        , LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventActionErrorMessageKey)
                        , null
                    );

                    return;
                }

                PlayerData.UpdatePlayerData(result);
                _UiController.OnActionEnd(() =>
                {
                    PopupUIManager.Instance.Show_SoftLoad(false);
                    _UiController.SetAllBtnState(true);
                });

                bool isSuccess = GameHelper.TryDeserializeJSONStr(result.CustomData[GameEventTemplateKeys.PlayerReceivedItemKey], out Dictionary<string, int> itemDict);
                if (isSuccess)
                {
                    List<ItemData> itemDataList = DataManager.ConvertDictionaryToItemDataList(itemDict);
                    DataManager.Instance.InventoryData.GrantItemByItemList(itemDataList);
                    OnReceiveItem(itemDataList);

                    if (PlayerData.ReceivedRewardDict.Count >= EventInfoData.RewardList.Count)
                        OnEventComplete();
                }
            }
            , Debug.Log
        );
    }

    #endregion

    #region Checking Methods

    private bool CheckActionValid()
    {
        switch (EventInfoData.PlayTriggerType)
        {
            case GameEventStartType.Cooldown when _UiController.IsCoolDown:
            {
                string title = LocalizationManager.Instance.GetText(GameEventTemplateKeys.CooldownTitleKey);
                string message = LocalizationManager.Instance.GetText(GameEventTemplateKeys.CooldownMessageKey);
                PopupUIManager.Instance.ShowPopup_Error(
                    title
                    , message
                    , delegate
                    {
                        PopupUIManager.Instance.Show_SoftLoad(false);
                        _UiController.SetAllBtnState(true);
                    }
                );
            }
            return false;
        }

        return true;
    }

    #endregion

    #endregion
}

public class PrepareGiftEventData : BaseGameEventData
{
    #region CloudScript Key

    private static readonly string _participateRewardKey = "participate_reward";
    private static readonly string _giftProgressionConditionKey = "gift_progression_condition";

    #endregion

    #region Public Properties

    public List<List<ItemData>> RewardList { get; private set; }
    public List<ItemData> ParticipateRewardList { get; private set; }
    public List<int> GiftProgressionCondition => _giftProgressionCondition;

    #endregion

    #region Private Properties

    private List<int> _giftProgressionCondition;

    #endregion

    #region Constructor

    public PrepareGiftEventData(string eventKey, CloudScriptResultProcess eventData) : base(eventKey, eventData)
    {
        #region Init Data Instance

        RewardList = new List<List<ItemData>>();
        _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(eventData.CustomData[GameEventTemplateKeys.EventDataKey], out Dictionary<string, object> eventDataDict);

        #endregion

        #region Participate Reward Data

        _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(eventDataDict[_participateRewardKey].ToString(), out Dictionary<string, int> participateRewardDict);
        if (!_isLoadedSuccess) return;

        ParticipateRewardList = DataManager.ConvertDictionaryToItemDataList(participateRewardDict);

        #endregion

        #region Tier Reward Data

        _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(eventDataDict[GameEventTemplateKeys.RewardListKey].ToString(), out List<object> rewardObjList);
        if (!_isLoadedSuccess) return;

        for (int i = 0; i < rewardObjList.Count; i++)
        {
            _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(rewardObjList[i].ToString(), out Dictionary<string, int> rewardDictionary);
            if (!_isLoadedSuccess) return;

            RewardList.Add(DataManager.ConvertDictionaryToItemDataList(rewardDictionary));
        }

        #endregion

        #region Gift Progression Condition Key

        _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(eventDataDict[_giftProgressionConditionKey].ToString(), out _giftProgressionCondition);

        #endregion
    }

    #endregion
}

public class PrepareGiftPlayerData
{
    #region CloudScript Key

    private static readonly string _giftProgressionKey = "gift_progression";
    private static readonly string _receivedRewardKey = "received_reward";

    #endregion

    #region Public Properties

    public bool IsLoadedSuccess => _isLoadedSuccess;
    public int RemainPlayTime => _remainPlayTime;
    public DateTime LastPlayDateTime => _lastPlayDateTime;
    public int GiftProgression => _giftProgression;
    public Dictionary<int, string> ReceivedRewardDict => _receivedRewardDict;

    #endregion

    #region Private Properties

    private bool _isLoadedSuccess = true;
    private int _remainPlayTime;
    private DateTime _lastPlayDateTime;
    private int _giftProgression;
    private Dictionary<int, string> _receivedRewardDict;

    #endregion

    #region Constructor

    public PrepareGiftPlayerData(CloudScriptResultProcess eventData)
    {
        _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(eventData.CustomData[GameEventTemplateKeys.PlayerDataKey],
            out Dictionary<string, object> playerDataDictionary);

        if (!_isLoadedSuccess) return;

        _isLoadedSuccess &= int.TryParse(playerDataDictionary[GameEventTemplateKeys.PlayerRemainPlaytimeKey].ToString(), out _remainPlayTime);
        _isLoadedSuccess &= DateTime.TryParse(playerDataDictionary[GameEventTemplateKeys.PlayerActionTimestampKey].ToString(), out _lastPlayDateTime);
        _isLoadedSuccess &= int.TryParse(playerDataDictionary[_giftProgressionKey].ToString(), out _giftProgression);
        _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(playerDataDictionary[_receivedRewardKey].ToString(), out _receivedRewardDict);
    }

    #endregion

    #region Methods

    public void UpdatePlayerData(CloudScriptResultProcess eventData)
    {
        _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(eventData.CustomData[GameEventTemplateKeys.PlayerDataKey],
            out Dictionary<string, object> playerDataDictionary);

        if (!_isLoadedSuccess) return;

        _isLoadedSuccess &= int.TryParse(playerDataDictionary[GameEventTemplateKeys.PlayerRemainPlaytimeKey].ToString(), out _remainPlayTime);
        _isLoadedSuccess &= DateTime.TryParse(playerDataDictionary[GameEventTemplateKeys.PlayerActionTimestampKey].ToString(), out _lastPlayDateTime);
        _isLoadedSuccess &= int.TryParse(playerDataDictionary[_giftProgressionKey].ToString(), out _giftProgression);
        _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(playerDataDictionary[_receivedRewardKey].ToString(), out _receivedRewardDict);
    }

    #endregion
}