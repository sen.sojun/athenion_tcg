﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;

public class EventRewardCell : MonoBehaviour
{
    #region Public Properties
    public RewardCell Prefab_RewardCell;
    public Image IMG_PrizeRank;
    public GameObject IMG_CellBackground;
    public TextMeshProUGUI TXT_PrizeRank;
    #endregion

    #region Private Properties
    private List<RewardCell> _rewardList = new List<RewardCell>();
    #endregion

    #region Methods
    public void InitData(string rankTxt, Color c, List<ItemData> rewardList)
    {
        SetText(rankTxt, c);
        //SetImage();
        SetReward(rewardList);
    }

    private void SetReward(List<ItemData> rewardList)
    {
        foreach (ItemData item in rewardList)
        {
            _rewardList.Add(CreateReward(item.ItemID, item.Amount));
        }
       
    }

    private void SetText(string rankTxt, Color c)
    {
        TXT_PrizeRank.gameObject.SetActive(true);
        TXT_PrizeRank.text = rankTxt;
        TXT_PrizeRank.color = c;
    }

    private void SetImage()
    {
        Sprite sprite = SpriteResourceHelper.LoadSprite("", SpriteResourceHelper.SpriteType.ICON_Rank);
        IMG_PrizeRank.sprite = sprite;
    }

    #endregion

    private string GetRewardText()
    {
        string text = "";
        foreach (RewardCell item in _rewardList)
        {
            text += string.Format("{0} x {1}\n", item.GetRewardID().ToString(), item.GetRewardCount().ToString());
        }

        return text;
    }

    #region Cell Generate
    private RewardCell CreateReward(string rewardKey, int count)
    {
        GameObject obj = Instantiate(Prefab_RewardCell.gameObject, Prefab_RewardCell.transform.parent);
        obj.SetActive(true);
        RewardCell cell = obj.GetComponent<RewardCell>();
        cell.InitData(rewardKey, count);

        return cell;
    }
    #endregion
}
