﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGameEventUIOptional
{
    void Init(BaseGameEventTemplate manager);
    void Update(BaseGameEventTemplate manager);
}
