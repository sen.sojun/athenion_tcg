﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;

public class GameEventDailyRewardManager : BaseGameEventTemplate
{
    #region Static Value

    #region Localization Key

    private static readonly string ActionNotValidTitle = "EVENT_ACTION_NOT_VALID_TITLE_";
    private static readonly string ActionNotValidMessage = "EVENT_ACTION_NOT_VALID_MESSAGE_";

    private static readonly string ReceivedAllRewardTitle = "EVENT_DAILY_REWARD_ALL_RECEIVED_TITLE";
    private static readonly string ReceivedAllRewardMessage = "EVENT_DAILY_REWARD_ALL_RECEIVED_MESSAGE";

    #endregion

    #endregion

    #region Public Properties

    public DailyRewardEventData EventInfoData { get; private set; }
    public DailyRewardPlayerData PlayerData { get; private set; }

    #endregion

    #region Inspector Properties

    [Header("UI Controller")]
    [SerializeField] private GameEventDailyRewardUIManager _UiController;

    [Header("CloudScript Function")]
    [SerializeField] private string _InitCloudScriptFunctionName;
    [SerializeField] private string _GetRewardCloudScriptFunctionName;

    #endregion

    #region Methods

    #region Override Methods

    public override void Init(string eventKey, string sceneName, UnityAction onComplete = null, UnityAction onFail = null)
    {
        base.Init(eventKey, sceneName, onComplete, onFail);

        DataManager.Instance.ExcecuteEventCloudScript(
            _InitCloudScriptFunctionName
            , new { EventKey = eventKey }
            , result =>
            {
                if (!result.IsSuccess)
                {
                    onFail?.Invoke();
                    return;
                }

                EventInfoData = new DailyRewardEventData(eventKey, result);
                PlayerData = new DailyRewardPlayerData(result);

                if (EventInfoData.IsLoadedSuccess && PlayerData.IsLoadedSuccess)
                {
                    InitUI();
                    onComplete?.Invoke();
                }
                else
                {
                    onFail?.Invoke();
                }
            }
            , errMsh =>
            {
                onFail?.Invoke();
            });
    }

    #endregion

    #region Init Methods

    private void InitUI()
    {
        _UiController.InitUi(this);
        _UiController.SetActionButton(OnSelectAction);
        _UiController.SetEventInfoBtn(OnShowEventDetail);
    }

    #endregion

    #region Btn OnClick Methods

    private void OnSelectAction()
    {
        _UiController.SetAllBtnState(false);
        PopupUIManager.Instance.Show_SoftLoad(true);

        if (!CheckActionValid())
            return;

        _UiController.RequestSubResultActionStart();

        bool isSuccess = true;

        DataManager.Instance.ExcecuteEventCloudScript(
            _GetRewardCloudScriptFunctionName
            , new { EventKey = EventInfoData.EventKey}
            , result =>
            {
                isSuccess &= result.IsSuccess;

                if (!isSuccess)
                {
                    PopupUIManager.Instance.Show_SoftLoad(false);
                    _UiController.SetAllBtnState(true);
                    _UiController.RequestSubResultActionCancel();

                    PopupUIManager.Instance.ShowPopup_Error(
                        LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventActionErrorTitleKey)
                        , LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventActionErrorMessageKey)
                    );

                    return;
                }

                PlayerData.UpdatePlayerData(result);

                int receivedIndex = PlayerData.ReceivedRewardDict.Count - 1;
                List<ItemData> itemDataList = EventInfoData.RewardDataList[receivedIndex];
                foreach (ItemData itemData in itemDataList)
                {
                    DataManager.Instance.InventoryData.GrantItemByItemID(itemData.ItemID, itemData.Amount);
                }
                OnReceiveItem(itemDataList);

                if (PlayerData.ReceivedRewardDict.Count == EventInfoData.RewardDataList.Count)
                {
                    OnEventComplete();
                }

                PopupUIManager.Instance.Show_SoftLoad(false);
                _UiController.SetAllBtnState(true);

                _UiController.UpdateDailyRewardUI(null);
                _UiController.RequestSubResultActionEnd();

            }
            , Debug.LogError);
    }

    #endregion

    #region Checking Methods

    private bool CheckActionValid()
    {
        switch (EventInfoData.PlayTriggerType)
        {
            case GameEventStartType.Daily when PlayerData.IsActionValid == false:
            {
                PopupUIManager.Instance.ShowPopup_Error(
                    LocalizationManager.Instance.GetText(ActionNotValidTitle + EventInfoData.EventKey)
                    , LocalizationManager.Instance.GetText(ActionNotValidMessage + EventInfoData.EventKey)
                    , () =>
                    {
                        PopupUIManager.Instance.Show_SoftLoad(false);
                        _UiController.SetAllBtnState(true);
                    }
                );
                return false;
            }

            case GameEventStartType.Daily
                when PlayerData.ReceivedRewardDict.Count >= EventInfoData.RewardDataList.Count:
            {
                PopupUIManager.Instance.ShowPopup_Error(
                    LocalizationManager.Instance.GetText(ReceivedAllRewardTitle)
                    , LocalizationManager.Instance.GetText(ReceivedAllRewardMessage)
                    , () =>
                    {
                        PopupUIManager.Instance.Show_SoftLoad(false);
                        _UiController.SetAllBtnState(true);
                    }
                );
                    return false;
            }
        }

        return true;
    }

    #endregion

    #endregion
}

public class DailyRewardEventData : BaseGameEventData
{
    #region CloudScript Key

    private static readonly string _winRewardKey = "win_rewards";

    #endregion

    #region Properties

    public List<List<ItemData>> RewardDataList { get; private set; }

    #endregion

    #region Constructor

    public DailyRewardEventData(string eventKey, CloudScriptResultProcess eventData) : base(eventKey, eventData)
    {
        #region Init Data Instance

        RewardDataList = new List<List<ItemData>>();
        _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(eventData.CustomData[GameEventTemplateKeys.EventDataKey], out Dictionary<string, object> eventDataDict);

        #endregion

        #region Win Rewards Data

        _isLoadedSuccess &= eventDataDict.ContainsKey(_winRewardKey);
        if (!_isLoadedSuccess) return;

        _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(eventDataDict[_winRewardKey].ToString(), out List<object> listOfRewardList);
        if (!_isLoadedSuccess) return;
        foreach (object item in listOfRewardList)
        {
            _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(item.ToString(), out Dictionary<string, int> winRewardDict);
            if (!_isLoadedSuccess) return;
            RewardDataList.Add(DataManager.ConvertDictionaryToItemDataList(winRewardDict));
        }

        #endregion
    }

    #endregion
}

public class DailyRewardPlayerData
{
    #region CloudScript Key
    private static readonly string ReceivedRewardKey = "received_reward";
    private static readonly string ValidActionString = "valid_action";
    #endregion

    #region Properties
    public bool IsLoadedSuccess => _isLoadedSuccess;
    private bool _isLoadedSuccess = true;

    public Dictionary<string, object> ReceivedRewardDict => _receivedRewardDict;
    private Dictionary<string, object> _receivedRewardDict;
    public bool IsActionValid => _isActionValid;
    private bool _isActionValid;
    #endregion

    #region Constructor

    public DailyRewardPlayerData(CloudScriptResultProcess eventData)
    {
        _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(eventData.CustomData[GameEventTemplateKeys.PlayerDataKey], out Dictionary<string, object> playerDataDictionary);
        if (!_isLoadedSuccess) return;

        _isLoadedSuccess &= playerDataDictionary.ContainsKey(ReceivedRewardKey);
        _isLoadedSuccess &= eventData.CustomData.ContainsKey(ValidActionString);
        if (!_isLoadedSuccess) return;

        _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(playerDataDictionary[ReceivedRewardKey].ToString(), out _receivedRewardDict);
        _isLoadedSuccess &= bool.TryParse(eventData.CustomData[ValidActionString], out _isActionValid);
    }
    #endregion

    #region Methods

    public void UpdatePlayerData(CloudScriptResultProcess eventData)
    {
        _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(eventData.CustomData[GameEventTemplateKeys.PlayerDataKey], out Dictionary<string, object> playerDataDictionary);
        if (!_isLoadedSuccess) return;

        _isLoadedSuccess &= playerDataDictionary.ContainsKey(ReceivedRewardKey);
        _isLoadedSuccess &= eventData.CustomData.ContainsKey(ValidActionString);
        if (!_isLoadedSuccess) return;

        _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(playerDataDictionary[ReceivedRewardKey].ToString(), out _receivedRewardDict);
        _isLoadedSuccess &= bool.TryParse(eventData.CustomData[ValidActionString], out _isActionValid);
    }


    #endregion
}
