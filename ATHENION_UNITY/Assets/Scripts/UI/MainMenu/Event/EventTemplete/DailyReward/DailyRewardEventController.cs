﻿using System.Collections;
using System.Collections.Generic;
using Coffee.UIExtensions;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public enum DailyRewardStatus
{
    None
    , Received
    , Missed
}

public class DailyRewardEventController : MonoBehaviour
{
    #region Static Value
    private const string RewardBgKey = "REWARD_BG";
    private const string RewardReceivedKey = "REWARD_RECEIVED";
    private const string RewardMissedKey = "REWARD_MISSED";
    private const string RewardTextLabelKey = "REWARD_LABEL";
    private const string RewardIconKey = "REWARD_ICON_";

    private const string RewardTitleKey = "EVENT_CONGRATS";
    private const string RewardMessageKey = "EVENT_YOUVEGOT";
    private const string RewardDetailTitleKey = "EVENT_REWARD_TITLE_";
    private const string RewardDetailMessageKey = "EVENT_REWARD_MESSAGE_";
    #endregion

    #region Public Properties
    public List<ItemData> ItemData => _itemData;
    public UIShiny ShinyController => _uiShinyController;
    public int RewardIndex => _rewardIndex;
    #endregion

    #region Private Properties
    private string _eventKey;
    private List<ItemData> _itemData;
    private UIShiny _uiShinyController;
    private int _rewardIndex;
    #endregion

    #region Inspector Properties
    [Header("UI Elements.")]
    [SerializeField] private Image _RewardBgImage;
    [SerializeField] private Image _RewardIconImage;
    [SerializeField] private Image _RewardStatusImage;
    [SerializeField] private Image _RewardTextLabelImage;
    [SerializeField] private Button _RewardButton;

    [Header("Other Setting.")]
    [SerializeField] private SoundManager.SFX _receiveSound;
    #endregion

    #region Methods

    #region Init Methods

    public void Init(string eventKey, List<ItemData> itemData, int rewardIndex, DailyRewardStatus status)
    {
        #region Set Properties
        _eventKey = eventKey;
        _itemData = itemData;
        _uiShinyController = GetComponent<UIShiny>();
        _rewardIndex = rewardIndex;
        #endregion

        #region Image
        if (_RewardBgImage != null) _RewardBgImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, RewardBgKey);
        if (_RewardTextLabelImage != null) _RewardTextLabelImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, RewardTextLabelKey);
        if (_RewardIconImage != null)
        {
            _RewardIconImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, RewardIconKey + _rewardIndex);
            _RewardIconImage.gameObject.SetActive(true);
        }
        SetRewardStatusImage(status);
        #endregion

        #region Button

        if (_RewardButton != null)
        {
            _RewardButton.onClick.RemoveAllListeners();
            _RewardButton.onClick.AddListener(ShowItemPackDetail);
        }

        #endregion
    }

    #endregion

    #region Other Methods

    private void ShowItemPackDetail()
    {
        PopupUIManager.Instance.ShowPopup_PreviewReward(
            string.Format(LocalizationManager.Instance.GetText(RewardDetailTitleKey + _eventKey), (RewardIndex + 1))
            , string.Format(LocalizationManager.Instance.GetText(RewardDetailMessageKey + _eventKey), (RewardIndex + 1))
            , _itemData
        );
    }

    public void ClaimRewardStatusUpdate(UnityAction onComplete)
    {
        _uiShinyController.Stop();
        SetRewardStatusImage(DailyRewardStatus.Received);
        _RewardStatusImage.color = new Color(1, 1, 1, 0);
        _RewardStatusImage.rectTransform.localScale = Vector3.one * 1.5f;

        _RewardStatusImage.DOFade(1, 0.3f).SetEase(Ease.Linear);
        _RewardStatusImage.rectTransform.DOScale(Vector3.one * 1.75f, 0.25f).SetEase(Ease.Linear).onComplete += () =>
        {
            _RewardStatusImage.rectTransform.DOScale(Vector3.one, 0.15f).SetEase(Ease.Linear).onComplete += () =>
            {
                SoundManager.PlaySFX(_receiveSound);
                _uiShinyController.loop = false;
                _uiShinyController.Play();

                StartCoroutine(WaitForSec(1, () =>
                {
                    PopupUIManager.Instance.ShowPopup_GotRewardAnimated(
                            LocalizationManager.Instance.GetText(RewardTitleKey)
                            , LocalizationManager.Instance.GetText(RewardMessageKey)
                            , _itemData
                            , onComplete
                        ).SetAnimationEase(Ease.OutExpo)
                        .SetDelay(0.1f)
                        .SetAnimationDuration(0.25f)
                        .SetMaxScale(8f);
                }));
            };
        };
    }

    private IEnumerator WaitForSec(float second, UnityAction onComplete)
    {
        yield return new WaitForSeconds(second);
        onComplete?.Invoke();
    }

    private void SetRewardStatusImage(DailyRewardStatus status)
    {
        if (_RewardStatusImage == null) return;
        switch (status)
        {
            case DailyRewardStatus.None:
                _RewardStatusImage.sprite = null;
                //_RewardStatusImage.enabled = false;
                _RewardStatusImage.gameObject.SetActive(false);
                break;
            case DailyRewardStatus.Received:
                _RewardStatusImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, RewardReceivedKey);
                //_RewardStatusImage.enabled = true;
                _RewardStatusImage.gameObject.SetActive(true);
                break;
            case DailyRewardStatus.Missed:
                _RewardStatusImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, RewardMissedKey);
                //_RewardStatusImage.enabled = true;
                _RewardStatusImage.gameObject.SetActive(true);
                break;
        }
    }

    public void SetBackgroundImageActive(Color color)
    {
        _RewardBgImage.color = color;
    }

    #endregion

    #endregion
}
