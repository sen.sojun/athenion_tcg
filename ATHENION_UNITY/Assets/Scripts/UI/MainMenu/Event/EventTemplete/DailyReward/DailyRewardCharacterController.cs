﻿using UnityEngine;
using UnityEngine.UI;

public class DailyRewardCharacterController : BaseDailyRewardSubResultController
{
    #region Static Value

    private static readonly string _charIdleString = "CHR_IDLE";
    private static readonly string _charWinString = "CHR_WIN";

    #endregion

    #region Inspector Properties

    [Header("UI Elements")]
    [SerializeField] private Image _CharacterImage;

    #endregion

    #region Private Properties

    private string _eventKey;

    #endregion

    #region Methods

    #region Unity Callback Methods

    private void OnEnable()
    {
        GameEventDailyRewardUIManager.OnActionStart += GameEventDailyRewardUiManager_OnActionStart;
        GameEventDailyRewardUIManager.OnActionEnd += GameEventDailyRewardUiManager_OnActionEnd;
        GameEventDailyRewardUIManager.OnActionCancel += GameEventDailyRewardUiManager_OnActionCancel;
    }

    private void OnDisable()
    {
        GameEventDailyRewardUIManager.OnActionStart -= GameEventDailyRewardUiManager_OnActionStart;
        GameEventDailyRewardUIManager.OnActionEnd -= GameEventDailyRewardUiManager_OnActionEnd;
        GameEventDailyRewardUIManager.OnActionCancel -= GameEventDailyRewardUiManager_OnActionCancel;
    }

    #endregion

    private void GameEventDailyRewardUiManager_OnActionStart()
    {
        if (_CharacterImage != null)
            _CharacterImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, _charWinString);
    }

    private void GameEventDailyRewardUiManager_OnActionEnd(DailyRewardEventData eventInfoData, DailyRewardPlayerData playerData)
    {
        if (_CharacterImage != null)
            _CharacterImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, _charIdleString);
    }

    private void GameEventDailyRewardUiManager_OnActionCancel()
    {
        if (_CharacterImage != null)
            _CharacterImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, _charIdleString);
    }

    #endregion

    public override void Init(DailyRewardEventData eventInfoData, DailyRewardPlayerData playerData)
    {
        if (eventInfoData == null || playerData == null) return;
            _eventKey = eventInfoData.EventKey;

        if (_CharacterImage != null)
            _CharacterImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, _charIdleString);
    }
}
