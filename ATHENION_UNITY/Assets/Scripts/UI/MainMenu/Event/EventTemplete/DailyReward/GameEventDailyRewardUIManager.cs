﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class GameEventDailyRewardUIManager : MonoBehaviour
{
    #region Static Value

    private const string ActionTextPrefixKey = "EVENT_ACTION_TEXT_";

    #endregion

    #region Inspector Properties

    [Header("Image UI Elements")]
    [SerializeField] private Image _EventHeaderBgImage;
    [SerializeField] private Image _EventActionButtonImage;

    [Header("Text UI Elements")]
    [SerializeField] private TextMeshProUGUI _EventHeaderText;
    [SerializeField] private TextMeshProUGUI _EventObjectiveText;
    [SerializeField] private TextMeshProUGUI _EventActionText;

    [Header("Button UI Elements")]
    [SerializeField] private Button _EventInfoButton;
    [SerializeField] private Button _EventActionButton;

    [Header("Reference UI Elements")]
    [SerializeField] private List<DailyRewardEventController> _rewardScriptList;

    [Header("Other Setting")]
    [SerializeField] private Material _GrayScaleMaterial;
    [SerializeField] private Color _BackgroundImageColor;

    [Header("Sub Result Controller")]
    [SerializeField] private List<BaseDailyRewardSubResultController> _subResultControllers;

    #endregion

    #region Public Properties
    #endregion

    #region Private Properties

    private GameEventDailyRewardManager _manager;

    #endregion

    #region Event

    public static event Action<DailyRewardEventData, DailyRewardPlayerData> OnInit;
    public static event Action OnActionStart;
    public static event Action<DailyRewardEventData, DailyRewardPlayerData> OnActionEnd;
    public static event Action OnActionCancel;

    #endregion

    #region Methods

    #region Init Methods

    /// <summary>
    /// Call this method to initialize UI elements.
    /// </summary>
    /// <param name="manager">Game event manager </param>
    public void InitUi(GameEventDailyRewardManager manager)
    {
        #region Set Data

        _manager = manager;
        var eventInfoData = manager.EventInfoData;
        var eventThemeInfo = eventInfoData.EventThemeInfo;
        var eventKey = eventInfoData.EventKey;
        var playerData = manager.PlayerData;

        #endregion

        #region Text UI

        if (_EventHeaderText != null) _EventHeaderText.text = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventHeaderKey + eventKey);
        if (_EventObjectiveText != null) _EventObjectiveText.text = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventObjectiveKey + eventKey);
        if (_EventActionText != null) _EventActionText.text = LocalizationManager.Instance.GetText(ActionTextPrefixKey + eventKey);

        #endregion

        #region Image UI

        _EventHeaderBgImage.sprite = SpriteResourceHelper.LoadSprite(eventThemeInfo.Theme + GameEventTemplateKeys.EventHeaderBannerSuffix, SpriteResourceHelper.SpriteType.GAME_Event);

        if (playerData.IsActionValid == false && _EventActionButtonImage != null)
        {
            _EventActionButtonImage.material = _GrayScaleMaterial;
        }

        #endregion

        #region Generate UI

        GenerateRewardItem();

        #endregion

        #region Other UI

        if (_subResultControllers != null && _subResultControllers.Count > 0)
        {
            foreach (var controller in _subResultControllers)
            {
                controller.Init(eventInfoData, playerData);
            }
        }

        if (playerData.ReceivedRewardDict.Count == eventInfoData.RewardDataList.Count)
        {
            SetActionBtnState(false);
        }
        #endregion
    }

    /// <summary>
    /// Call this method to set event action.
    /// </summary>
    /// <param name="action">Delegate action for this event.</param>
    public void SetActionButton(UnityAction action)
    {
        _EventActionButton.onClick.RemoveAllListeners();
        _EventActionButton.onClick.AddListener(action);
    }

    /// <summary>
    /// Call this method to set showing event detail action.
    /// </summary>
    /// <param name="action">Delegate action for showing detail action.</param>
    public void SetEventInfoBtn(UnityAction action)
    {
        _EventInfoButton.onClick.RemoveAllListeners();
        _EventInfoButton.onClick.AddListener(action);
    }

    /// <summary>
    /// Call this method to generate daily reward item UI.
    /// </summary>
    public void GenerateRewardItem()
    {
        DailyRewardEventData eventInfoData = _manager.EventInfoData;
        DailyRewardPlayerData playerData = _manager.PlayerData;

        int currentRewardIndex = 0;
        if (playerData.IsActionValid)
            currentRewardIndex = playerData.ReceivedRewardDict.Count;

        for (int i = 0; i < _rewardScriptList.Count; i++)
        {
            DailyRewardStatus status;
            if (i >= playerData.ReceivedRewardDict.Count)
            {
                status = DailyRewardStatus.None;
            }
            else
            {
                status = playerData.ReceivedRewardDict[i.ToString()].ToString() == DailyRewardStatus.Missed.ToString()
                    ? DailyRewardStatus.Missed
                    : DailyRewardStatus.Received;
            }

            _rewardScriptList[i].Init(eventInfoData.EventKey, eventInfoData.RewardDataList[i], i, status);
            if (i == currentRewardIndex && playerData.IsActionValid && status == DailyRewardStatus.None)
                _rewardScriptList[i].ShinyController.Play();

            if (i > currentRewardIndex)
                _rewardScriptList[i].SetBackgroundImageActive(_BackgroundImageColor);
        }

    }

    #endregion

    #region Setting Methods

    public void SetAllBtnState(bool value)
    {
        SetActionBtnState(value);
        _EventInfoButton.enabled = value;
    }

    public void SetActionBtnState(bool value)
    {
        _EventActionButton.enabled = value;
        _EventActionButton.image.material = value ? null : _GrayScaleMaterial;
    }

    #endregion

    #region Update Methods

    public void UpdateDailyRewardUI(UnityAction onComplete)
    {
        DailyRewardPlayerData playerData = _manager.PlayerData;

        int index = playerData.ReceivedRewardDict.Count - 1;
        _rewardScriptList[index].ClaimRewardStatusUpdate(onComplete);
    }

    #endregion

    #region Event Request Methods

    public void RequestSubResultActionStart()
    {
        OnActionStart?.Invoke();
    }

    public void RequestSubResultActionEnd()
    {
        OnActionEnd?.Invoke(_manager.EventInfoData, _manager.PlayerData);
    }

    public void RequestSubResultActionCancel()
    {
        OnActionCancel?.Invoke();
    }

    #endregion

    #endregion
}
