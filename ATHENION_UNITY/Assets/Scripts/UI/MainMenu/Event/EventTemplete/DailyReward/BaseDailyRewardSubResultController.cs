﻿using UnityEngine;

[System.Serializable]
public abstract class BaseDailyRewardSubResultController : MonoBehaviour
{
    public abstract void Init(DailyRewardEventData eventInfoData, DailyRewardPlayerData playerData);
}
