﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using GameEvent;
using UnityEngine;
using UnityEngine.Events;

public class GameEventSlotMachineManager : BaseGameEventTemplate
{
    #region Static Value

    // Localization Key
    private static readonly string EventStartButtonPrefixKey = "EVENT_START_BUTTON_TEXT_";

    // Cloud Script Key
    private static readonly string ResultRewardIndexString = "result";
    #endregion

    #region Public Properties

    public SlotMachineEventData EventInfoData { get; private set; }
    public SlotMachinePlayerData PlayerData { get; private set; }

    #endregion

    #region Private Properties

    [Header("UI Controller")]
    [SerializeField] private GameEventSlotMachineUIManager _uiController;

    [Header("CloudScript Function ")]
    [SerializeField] private string _initCloudScriptFunctionName;
    [SerializeField] private string _getRewardCloudScriptFunctionName;

    [Header("Other Setting")]
    [SerializeField] private SoundManager.SFX _resultSoundEffect;

    private int _resultRewardIndex;
    #endregion

    #region Methods

    #region Override Methods

    public override void Init(string eventKey, string sceneName, UnityAction onComplete = null, UnityAction onFail = null)
    {
        base.Init(eventKey, sceneName, null);

        DataManager.Instance.ExcecuteEventCloudScript(
            _initCloudScriptFunctionName
            , new {EventKey = eventKey }
            , result =>
            {
                if (!result.IsSuccess)
                {
                    onFail?.Invoke();
                    return;
                }

                DataManager.Instance.LoadInventory(null,null);

                EventInfoData = new SlotMachineEventData(eventKey, result);
                PlayerData = new SlotMachinePlayerData(result);

                if (EventInfoData.IsLoadedSuccess && PlayerData.IsLoadedSuccess)
                {
                    InitUI();
                    onComplete?.Invoke();
                }
                else
                {
                    onFail?.Invoke();
                }
            }
            , errMsh =>
            {
                onFail?.Invoke();
            }
        );
    }

    #endregion

    #region Init Methods

    private void InitUI()
    {
        _uiController.InitUi(this);
        _uiController.SetActionBtn(OnSelectAction);
        _uiController.SetEventInfoBtn(OnShowEventInfo);
    }

    #endregion

    #region Btn OnClick Methods

    /// <summary>
    /// Call this method when the player click on action button.
    /// </summary>
    private void OnSelectAction()
    {
        _uiController.SetBtnState(false);
        PopupUIManager.Instance.Show_SoftLoad(true);

        switch (EventInfoData.PlayTriggerType)
        {
            case GameEventStartType.Cooldown when _uiController.IsCoolDown:
            {

                string noTicketTitle = LocalizationManager.Instance.GetText(GameEventTemplateKeys.CooldownTitleKey);
                string noTicketMessage = LocalizationManager.Instance.GetText(GameEventTemplateKeys.CooldownMessageKey);
                PopupUIManager.Instance.ShowPopup_Error(
                    noTicketTitle
                    , noTicketMessage
                    , delegate
                    {
                        _uiController.SetAnimation(EventCharacterAnimationState.Idle);
                        _uiController.SetBtnState(true);
                        PopupUIManager.Instance.Show_SoftLoad(false);
                    }
                );
                return;
            }

            case GameEventStartType.Cooldown when PlayerData.RemainPlayTime <= 0 && EventInfoData.DefaultPlaytime > 0:
            {

                string noTicketTitle = LocalizationManager.Instance.GetText(GameEventTemplateKeys.NoRemainTryTitleKey);
                string noTicketMessage = LocalizationManager.Instance.GetText(GameEventTemplateKeys.NoRemainTryMessageKey);
                PopupUIManager.Instance.ShowPopup_Error(
                    noTicketTitle
                    , noTicketMessage
                    , delegate
                    {
                        _uiController.SetAnimation(EventCharacterAnimationState.Idle);
                        _uiController.SetBtnState(true);
                        PopupUIManager.Instance.Show_SoftLoad(false);
                    }
                );
                return;
            }

            case GameEventStartType.Daily when !GameHelper.IsNewDayUTC(PlayerData.LastPlayDateTime):
            {
                

                string title = LocalizationManager.Instance.GetText(GameEventTemplateKeys.DailyAlreadyDoneTitleKey);
                string message = LocalizationManager.Instance.GetText(GameEventTemplateKeys.DailyAlreadyDoneMessageKey);
                PopupUIManager.Instance.ShowPopup_Error(
                    title
                    , message
                    , delegate
                    {
                        _uiController.SetAnimation(EventCharacterAnimationState.Idle);
                        _uiController.SetBtnState(true);
                        PopupUIManager.Instance.Show_SoftLoad(false);
                    }
                );
                    return;
            }
        }

        _uiController.SetAnimation(EventCharacterAnimationState.Loading);

        // Call generate participate reward.
        StartCoroutine(ActionRewardGenerate(
            _uiController.PlaySlotButton.transform
            , EventInfoData.ParticipateRewardList)
        );

        StartRandomGambleAction();
        
    }

    private void OnShowEventInfo()
    {
        //EventDetailImage.gameObject.SetActive(true);
        string title = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventDetailHeaderKey);
        string message = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventDetailKey + _eventKey);

        PopupUIManager.Instance.ShowPopup_DetailWindow(title, message);
    }

    #endregion

    #region Action Methods

    /// <summary>
    /// Call this method if the gamble is the normal one.
    /// </summary>
    private void StartRandomGambleAction()
    {
        _uiController.StartGambleResult(this);

        StartCoroutine(GetRandomGambleRoutine(
            indexResult => 
            {
                _uiController.SetAnimation(EventCharacterAnimationState.Success);
                SoundManager.PlaySFX(_resultSoundEffect);
                _uiController.SetResultImage(indexResult);
                _uiController.EndGambleResult(this, EndAction);
            },
            delegate
            {
                string title = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventActionErrorTitleKey);
                string message = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventActionErrorMessageKey);

                PopupUIManager.Instance.ShowPopup_Error(
                    title
                    , message
                    , delegate
                    {
                        _uiController.SetBtnState(true);
                        _uiController.SetAnimation(EventCharacterAnimationState.Idle);
                        _uiController.ResetGambleResult(this);
                        PopupUIManager.Instance.Show_SoftLoad(false);
                    }
                );
            })
        );
    }

    /// <summary>
    /// Call this method when the action is end.
    /// </summary>
    public void EndAction()
    {
        string title = LocalizationManager.Instance.GetText(GameEventTemplateKeys.NotiRewardTitleKey);
        string message = LocalizationManager.Instance.GetText(GameEventTemplateKeys.NotiRewardMessageKey);

        OnReceiveItem(EventInfoData.RewardDataList[_resultRewardIndex]);
        if (EventInfoData.ParticipateRewardList.Count > 0 && EventInfoData.ParticipateRewardList != null)
            OnReceiveItem(EventInfoData.ParticipateRewardList);

        PopupUIManager.Instance.ShowPopup_GotRewardAnimated(
                title
                , message
                , EventInfoData.RewardDataList[_resultRewardIndex]
                , delegate
                {
                    _uiController.SetBtnState(true);
                    _uiController.SetAnimation(EventCharacterAnimationState.Idle);
                    _uiController.ResetGambleResult(this);
                }

            ).SetAnimationEase(Ease.OutExpo)
            .SetDelay(0.1f)
            .SetAnimationDuration(0.25f)
            .SetMaxScale(8f);

        _uiController.UpdateUi(EventInfoData, PlayerData);
        PopupUIManager.Instance.Show_SoftLoad(false);
    }

    #endregion

    #region Co-routine Methods

    /// <summary>
    /// Call this co-routine method to get the random gamble result.
    /// </summary>
    /// <param name="onComplete">Delegate method when this action is complete.</param>
    /// <param name="onFail">Delegate method when this action is fail to complete.</param>
    /// <returns></returns>
    private IEnumerator GetRandomGambleRoutine(UnityAction<int> onComplete = null, UnityAction onFail = null)
    {
        yield return new WaitForSeconds(2);

        bool isSuccess = true;

        DataManager.Instance.ExcecuteEventCloudScript(
            _getRewardCloudScriptFunctionName
            , new { EventKey = _eventKey }
            , delegate (CloudScriptResultProcess result)
            {
                isSuccess &= result.IsSuccess;
                isSuccess &= result.CustomData.ContainsKey(ResultRewardIndexString);
                
                if (!isSuccess)
                {
                    onFail?.Invoke();
                    return;
                }

                isSuccess &= int.TryParse(result.CustomData[ResultRewardIndexString], out _resultRewardIndex);

                if (!isSuccess)
                {
                    onFail?.Invoke();
                    return;
                }

                PlayerData.UpdatePlayerData(result);

                //string ecJson = result.CustomData[GameEventTemplateKeys.EventCurrencyKey];
                //DataManager.Instance.InventoryData.SetEventCurrency(ecJson);
                //DataManager.Instance.LoadInventory(null, null);
                
                isSuccess = GameHelper.TryDeserializeJSONStr(result.CustomData["received_item_data"], out Dictionary<string, int> ItemDataDict);
                if (isSuccess)
                {
                    List<ItemData> itemDataList = DataManager.ConvertDictionaryToItemDataList(ItemDataDict);
                    foreach (ItemData itemData in itemDataList)
                    {
                        DataManager.Instance.InventoryData.GrantItemByItemID(itemData.ItemID, itemData.Amount);
                    }
                }


                _uiController.OnCooldown();

                onComplete?.Invoke(_resultRewardIndex);
            }
            , Debug.LogError
        );
    }

    #endregion

    #endregion
}

public class SlotMachineEventData : BaseGameEventData
{
    #region CloudScript Key

    private static readonly string WinRewardString = "win_rewards";
    private static readonly string DefaultPlayTimeString = "default_playtime";
    private static readonly string DefaultCooldownString = "default_cooldown";
    private static readonly string ResultRewardIndexString = "result";

    #endregion

    #region Public Properties

    public List<List<ItemData>> RewardDataList { get; private set; }
    public List<ItemData> ParticipateRewardList { get; private set; }

    #endregion

    #region Constructor

    public SlotMachineEventData(string eventKey, CloudScriptResultProcess eventData) : base(eventKey, eventData)
    {
        #region Init Data Instance

        RewardDataList = new List<List<ItemData>>();
        _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(eventData.CustomData[GameEventTemplateKeys.EventDataKey], out Dictionary<string, object> eventDataDict);
        
        #endregion

        #region Win Rewards Data

        _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(eventDataDict[WinRewardString].ToString(), out List<object> listOfRewardList);
        if (!_isLoadedSuccess) return;
        foreach (object item in listOfRewardList)
        {
            _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(item.ToString(), out Dictionary<string, int> winRewardDict);
            if (!_isLoadedSuccess) return;
            RewardDataList.Add(DataManager.ConvertDictionaryToItemDataList(winRewardDict));
        }

        #endregion

        #region Participate Reward Data

        _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(eventDataDict[GameEventTemplateKeys.ParticipateRewardListKey].ToString(), out Dictionary<string, int> participateRewardDict);
        if (!_isLoadedSuccess) return;
        ParticipateRewardList = DataManager.ConvertDictionaryToItemDataList(participateRewardDict);

        #endregion
    }

    #endregion
}

public class SlotMachinePlayerData
{
    #region CloudScript Key



    #endregion

    #region Public Properties

    public bool IsLoadedSuccess => _isLoadedSuccess;
    public int RemainPlayTime => _remainPlayTime;
    public DateTime LastPlayDateTime => _lastPlayDateTime;

    #endregion

    #region Private Properties

    private bool _isLoadedSuccess = true;
    private int _remainPlayTime;
    private DateTime _lastPlayDateTime;

    #endregion

    #region Constructor

    public SlotMachinePlayerData(CloudScriptResultProcess eventData)
    {
        _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(eventData.CustomData[GameEventTemplateKeys.PlayerDataKey], out Dictionary<string, object> playerDataDictionary);
        if (!_isLoadedSuccess) return;

        _isLoadedSuccess &= int.TryParse(playerDataDictionary[GameEventTemplateKeys.PlayerRemainPlaytimeKey].ToString(), out _remainPlayTime);
        _isLoadedSuccess &= DateTime.TryParse(playerDataDictionary[GameEventTemplateKeys.PlayerActionTimestampKey].ToString(), out _lastPlayDateTime);
    }

    #endregion

    #region Methods

    public void UpdatePlayerData(CloudScriptResultProcess playerData)
    {
        _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(playerData.CustomData[GameEventTemplateKeys.PlayerDataKey], out Dictionary<string, object> playerDataDictionary);
        if (!_isLoadedSuccess) return;

        _isLoadedSuccess &= int.TryParse(playerDataDictionary[GameEventTemplateKeys.PlayerRemainPlaytimeKey].ToString(), out _remainPlayTime);
        _isLoadedSuccess &= DateTime.TryParse(playerDataDictionary[GameEventTemplateKeys.PlayerActionTimestampKey].ToString(), out _lastPlayDateTime);
    }

    #endregion
}