﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public interface IGameEventResult
{
    void StartResult();
    void EndResult(UnityAction onComplete);
    void ResetResult();
}
