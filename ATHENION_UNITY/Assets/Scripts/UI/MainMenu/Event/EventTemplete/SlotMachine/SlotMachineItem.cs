﻿using System.Collections.Generic;
using Karamucho;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public enum SlotMachineImageEnum
{
    SlotBg = 0
    , Slot
}

public class SlotMachineItem : MonoBehaviour
{
    #region Static Value

    private static readonly string ImageResourcePath = "Images/Items/";
    private static readonly string GrayScaleMaterialPath = "Events/Materials/UIImageGrayscale";
    
    #endregion

    #region Public Properties

    [Header("Normal UI Elements")]
    public Image SlotBgImage;
    public Image SlotImage;
    public Image RewardImage;
    public TextMeshProUGUI RewardAmountText;

    [Header("HighLight UI Elements")]
    public Image HighlightSlotBgImage;
    public Image HighlightSlotImage;
    public Image HighlightRewardImage;
    public TextMeshProUGUI HighlightRewardAmountText;

    #endregion

    #region Private Properties
    private ItemData _itemData;
    #endregion

    public void Init(Dictionary<string, string> imageResourcePathDictionary, ItemData itemData)
    {
        if (ResourceManager.Load<Sprite>(imageResourcePathDictionary[SlotMachineImageEnum.SlotBg.ToString()]) != null)
        {
            SlotBgImage.sprite = ResourceManager.Load<Sprite>(imageResourcePathDictionary[SlotMachineImageEnum.SlotBg.ToString()]);
            HighlightSlotBgImage.sprite = ResourceManager.Load<Sprite>(imageResourcePathDictionary[SlotMachineImageEnum.SlotBg.ToString()]);
        }

        if (ResourceManager.Load<Sprite>(imageResourcePathDictionary[SlotMachineImageEnum.Slot.ToString()]) != null)
        {
            SlotImage.sprite = ResourceManager.Load<Sprite>(imageResourcePathDictionary[SlotMachineImageEnum.Slot.ToString()]);
            HighlightSlotImage.sprite = ResourceManager.Load<Sprite>(imageResourcePathDictionary[SlotMachineImageEnum.Slot.ToString()]);
        }

        string rewardImagePath = ImageResourcePath + itemData.ItemID;
        if (ResourceManager.Load<Sprite>(rewardImagePath) != null)
        {
            RewardImage.sprite = ResourceManager.Load<Sprite>(rewardImagePath);
            HighlightRewardImage.sprite = ResourceManager.Load<Sprite>(rewardImagePath);
        }

        RewardAmountText.text = itemData.Amount.ToString();
        HighlightRewardAmountText.text = itemData.Amount.ToString();

        _itemData = itemData;
        
        SetGrayScale(true);
    }

    public ItemData GetItemData()
    {
        return _itemData;
    }

    /// <summary>
    /// Call this method to set gray scale on the images.
    /// </summary>
    /// <param name="value"></param>
    public void SetGrayScale(bool value)
    {
        if (value)
        {
            SlotBgImage.material = ResourceManager.Load<Material>(GrayScaleMaterialPath);
            SlotImage.material = ResourceManager.Load<Material>(GrayScaleMaterialPath);
            RewardImage.material = ResourceManager.Load<Material>(GrayScaleMaterialPath);
        }
        else
        {
            SlotBgImage.material = null;
            SlotImage.material = null;
            RewardImage.material = null;
        }
    }

    /// <summary>
    /// Call this method to set highlight state of this slot.
    /// </summary>
    /// <param name="value"></param>
    public void SetImageHighlightState(bool value)
    {
        if (value)
        {
            SlotBgImage.gameObject.SetActive(false);
            HighlightSlotBgImage.gameObject.SetActive(true);
        }
        else
        {
            SlotBgImage.gameObject.SetActive(true);
            HighlightSlotBgImage.gameObject.SetActive(false);
        }
    }
}
