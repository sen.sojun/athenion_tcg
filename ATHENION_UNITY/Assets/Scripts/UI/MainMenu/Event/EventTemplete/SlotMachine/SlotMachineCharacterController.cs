﻿using System.Collections;
using System.Collections.Generic;
using Spine.Unity;
using UnityEngine;

public class SlotMachineCharacterController : MonoBehaviour
{
    [SpineAnimation] public string IdleAnimation;
    [SpineAnimation] public string HappyAnimation;

    public SkeletonGraphic AnimatorScript;

    public void PlayIdleAnimation(bool isLoop = true)
    {
        AnimatorScript.AnimationState.SetAnimation(0, IdleAnimation, isLoop);
    }

    public void PlaySuccessfulAnimation(bool isLoop = true)
    {
        AnimatorScript.AnimationState.SetAnimation(0, HappyAnimation, isLoop);
    }
}
