﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using DG.Tweening;
using GameEvent;
using Photon.Realtime;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

public class GameEventSlotMachineUIManager : MonoBehaviour
{
    #region Static Value

    private static readonly string EventStartButtonPrefixKey = "EVENT_START_BUTTON_TEXT_";
    private static readonly string EventResultImageIndexPrefix = "RESULT_";

    #endregion

    #region Inspector Properties

    [Header("Static Image UI Elements")]
    [SerializeField] private List<Image> _staticNativeSizeImageList;
    [SerializeField] private List<Image> _staticNonNativeSizeImageList;
    [SerializeField] private Image _slotArrowImage;
    [SerializeField] private Image _eventHeaderImage;
    [SerializeField] private Image _eventStartButtonImage;
    [SerializeField] private Image _eventStartButtonAdditionalImage;

    [Header("Dynamic Image UI Elements")]
    [SerializeField] private Image _eventCharacterImage;
    [SerializeField] private Image _eventResultImage;
    [SerializeField] private Image _eventResultEffectImage;

    [Header("Text UI Elements")]
    [SerializeField] private TextMeshProUGUI _eventHeaderText;
    [SerializeField] private TextMeshProUGUI _eventObjectiveText;
    [SerializeField] private TextMeshProUGUI _eventDescriptionText;
    [SerializeField] private TextMeshProUGUI _eventTimeText;
    [SerializeField] private TextMeshProUGUI _eventCooldownButtonText;
    [SerializeField] private TextMeshProUGUI _eventRemainTryText;

    [Header("Button UI Elements")]
    [SerializeField] private Button _playSlotButton;
    [SerializeField] private Button _eventInfoButton;

    [Header("Script Components")]
    [SerializeField] private SlotMachineController _slotMachineController;
    [SerializeField] private UI_InfiniteScroll _infiniteScrollComponent;
    [SerializeField] private Animator _mainCharacterAnimator;
    [SerializeField] private SlotMachineCharacterController _characterSpineController;

    [Header("Other Setting")]
    [SerializeField] private bool _slotMachineEnable;
    [SerializeField] private Material _grayScaleMaterial;
    [SerializeField] private GameObject _gameEventResultControllerGameObject;
    private IGameEventResult _gameEventResultController;
    #endregion

    #region Public Properties

    public bool IsCoolDown => _isCoolDown;
    public Button PlaySlotButton => _playSlotButton;

    #endregion

    #region Private Properties

    private bool _isCoolDown;
    private GameEventSlotMachineManager _manager;

    #endregion

    #region Methods

    #region Init Methods

    public void InitUi(GameEventSlotMachineManager manager)
    {
        _manager = manager;
        var eventInfoData = _manager.EventInfoData;
        var eventKey = eventInfoData.EventKey;
        var evenThemeInfo = eventInfoData.EventThemeInfo;
        var playerData = _manager.PlayerData;

        _gameEventResultController = _gameEventResultControllerGameObject.GetComponent<IGameEventResult>();

        #region Text UI

        if (_eventHeaderText != null) _eventHeaderText.text = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventHeaderKey + eventKey);
        if (_eventObjectiveText != null) _eventObjectiveText.text = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventObjectiveKey + eventKey);
        if (_eventDescriptionText != null) _eventDescriptionText.text = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventDescriptionKey + eventKey);
        if (_eventTimeText != null) _eventTimeText.text = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventTimeKey + eventKey);
        if (_eventRemainTryText != null) _eventRemainTryText.text = $"{playerData.RemainPlayTime}/{eventInfoData.DefaultPlaytime}";
        if (_eventCooldownButtonText != null) _eventCooldownButtonText.text = LocalizationManager.Instance.GetText(EventStartButtonPrefixKey + eventKey);

        OnCooldown();

        #endregion

        #region Static Image UI

        //_eventHeaderImage.sprite = SpriteResourceHelper.LoadSprite(evenThemeInfo.Theme + GameEventTemplateKeys.EventHeaderBannerSuffix, SpriteResourceHelper.SpriteType.GAME_Event);

        if (_slotMachineEnable)
            _slotArrowImage.gameObject.SetActive(true);

        if (_staticNativeSizeImageList != null && _staticNativeSizeImageList.Count > 0)
        {
            for (int i = 0; i < _staticNativeSizeImageList.Count; i++)
            {
                _staticNativeSizeImageList[i].sprite = SpriteResourceHelper.LoadSubEventImage(eventKey, GameEventTemplateKeys.StaticNativeImagePrefixKey + i);
                _staticNativeSizeImageList[i].SetNativeSize();
                _staticNativeSizeImageList[i].enabled = true;
            }
        }

        if (_staticNonNativeSizeImageList != null && _staticNonNativeSizeImageList.Count > 0)
        {
            for (int i = 0; i < _staticNonNativeSizeImageList.Count; i++)
            {
                _staticNonNativeSizeImageList[i].sprite = SpriteResourceHelper.LoadSubEventImage(eventKey, GameEventTemplateKeys.StaticNonNativeImagePrefixKey + i);
                _staticNonNativeSizeImageList[i].enabled = true;
            }
        }

        if (manager.EventInfoData.PlayTriggerType.Equals(GameEventStartType.Daily) && !GameHelper.IsNewDayUTC(playerData.LastPlayDateTime))
        {
            if (_eventStartButtonImage != null) _eventStartButtonImage.gameObject.SetActive(false);
            if (_eventStartButtonAdditionalImage != null) _eventStartButtonAdditionalImage.gameObject.SetActive(false);

            //action text
            if (_eventCooldownButtonText != null) _eventCooldownButtonText.text = LocalizationManager.Instance.GetText("EVENT_DAILY_LIMIT_REACHED_" + eventKey);
        }
        else if (_isCoolDown || !GameHelper.IsNewDayUTC(playerData.LastPlayDateTime))
        {
            if (_eventStartButtonImage != null) _eventStartButtonImage.material = _grayScaleMaterial;
            if (_eventStartButtonAdditionalImage != null) _eventStartButtonAdditionalImage.material = _grayScaleMaterial;
        }
        
        #endregion
    }

    private void OnEnable()
    {
        #region Dynamic Image UI

        SetAnimation(EventCharacterAnimationState.Idle);

        #endregion
    }

    #endregion

    #region Update Methods

    public void UpdateUi(SlotMachineEventData eventInfoData, SlotMachinePlayerData playerData)
    {
        if (_eventRemainTryText != null) _eventRemainTryText.text = $"{playerData.RemainPlayTime}/{eventInfoData.DefaultPlaytime}";
        if (eventInfoData.PlayTriggerType == GameEventStartType.Cooldown && eventInfoData.DefaultCooldown > 0)
        {
            if (!_isCoolDown)
            {
                _isCoolDown = true;
                GameEventManager.Instance.RunRoutineTask(
                    GameEventSlotMachineManager.Instance.CooldownTimer(
                        playerData.LastPlayDateTime
                        , eventInfoData.DefaultCooldown
                        , _eventCooldownButtonText
                        , CoolDownEnd
                    )
                );
            }
        }

        if (_isCoolDown || !GameHelper.IsNewDayUTC(playerData.LastPlayDateTime))
        {
            if (_eventStartButtonImage != null) _eventStartButtonImage.material = _grayScaleMaterial;
            if (_eventStartButtonAdditionalImage != null) _eventStartButtonAdditionalImage.material = _grayScaleMaterial;
        }
    }

    public void OnCooldown()
    {
        var eventInfoData = _manager.EventInfoData;
        var playerData = _manager.PlayerData;

        if (eventInfoData.PlayTriggerType == GameEventStartType.Cooldown && eventInfoData.DefaultCooldown > 0)
        {
            if (!_isCoolDown)
            {
                _isCoolDown = true;
                GameEventManager.Instance.RunRoutineTask(
                    _manager.CooldownTimer(
                        playerData.LastPlayDateTime
                        , eventInfoData.DefaultCooldown
                        , _eventCooldownButtonText
                        , CoolDownEnd
                    )
                );
            }
        }
        else if (eventInfoData.PlayTriggerType == GameEventStartType.Cooldown 
                 && (int)eventInfoData.DefaultCooldown == -1 
                 && GameHelper.IsNewDayUTC(playerData.LastPlayDateTime) == false)
        {
            if (!_isCoolDown)
            {
                _isCoolDown = true;
                GameEventManager.Instance.RunRoutineTask(
                    _manager.NewDayTimer(
                        _eventCooldownButtonText
                        , CoolDownEnd
                    )
                );
            }
        }
    }

    #endregion

    #region Setting Methods

    public void SetActionBtn(UnityAction action)
    {
        _playSlotButton.onClick.RemoveAllListeners();
        _playSlotButton.onClick.AddListener(action);
    }

    public void SetEventInfoBtn(UnityAction action)
    {
        _eventInfoButton.onClick.RemoveAllListeners();
        _eventInfoButton.onClick.AddListener(action);
    }

    public void SetBtnState(bool value)
    {
        _playSlotButton.enabled = value;
        _eventInfoButton.enabled = value;
    }

    public void SetAnimation(EventCharacterAnimationState animationState)
    {
        if (_mainCharacterAnimator != null) _mainCharacterAnimator.Play(animationState.ToString());
    }

    public void SetResultImage(int index)
    {
        var eventKey = _manager.EventInfoData.EventKey;

        if (_eventResultImage != null)
            _eventResultImage.sprite = SpriteResourceHelper.LoadSubEventImage(eventKey, EventResultImageIndexPrefix + index);
    }

    #endregion

    #region Result Methods

    public void StartGambleResult(object caller)
    {
        if(caller.GetType() == typeof(GameEventSlotMachineManager))
            _gameEventResultController.StartResult();
    }

    /// <summary>
    /// Call this method to show the result of normal gamble.
    /// </summary>
    /// <param name="caller">Caller Instance.</param>
    /// <param name="onComplete">Delegate methods when this action is complete.</param>
    public void EndGambleResult(object caller, UnityAction onComplete)
    {
        if(caller.GetType() == typeof(GameEventSlotMachineManager))
            _gameEventResultController.EndResult(onComplete);
    }

    /// <summary>
    /// Call this method to reset the result.
    /// </summary>
    public void ResetGambleResult(object caller)
    {
        if(caller.GetType() == typeof(GameEventSlotMachineManager))
        {
            _gameEventResultController.ResetResult();
        }
    }

    #endregion

    /// <summary>
    /// Call this method when cooldown is end.
    /// </summary>
    private void CoolDownEnd()
    {
        var eventKey = _manager.EventInfoData.EventKey;

        _eventCooldownButtonText.text = LocalizationManager.Instance.GetText(EventStartButtonPrefixKey + eventKey);
        _eventStartButtonAdditionalImage.material = null;
        _eventStartButtonImage.material = null;
        _isCoolDown = false;
    }

    #endregion
}
