﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Karamucho;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

public class GameEventTemplateVisitStall : BaseGameEventTemplate
{
    #region Enums

    public enum RandomMode
    {
        Normal
        , Weight
    }

    public enum GambleType
    {
        Normal,
        SlotMachine
    }

    #endregion

    #region Static Value

    #region Cloud Script Key

    private static readonly string WinRewardString = "win_rewards";
    private static readonly string DefaultPlayTimeString = "default_playtime";
    private static readonly string DefaultCooldownString = "default_cooldown";
    private static readonly string ResultRewardIndexString = "result";

    #endregion

    #region Localize Key
    private static readonly string EventStartButtonPrefixKey = "EVENT_START_BUTTON_TEXT_";

    
    #endregion

    #region Cloud Script Static Function Name

    private static readonly string GetSlotMachineResultName = "GetSlotMachineResult";

    #endregion

    #region Prefab Name

    private static readonly string EventCharacterPrefabName = "EventCharacterAvatar";
    #endregion

    #region Resource Key

    // Resource path
    private static readonly string EventPrefabResourcePath = "Events/{0}/Prefabs/";

    // Prefix Key
    private static readonly string EventResultImageIndexPrefix = "RESULT_";

    // Suffix Key
    private static readonly string EventHeaderBannerSuffix = "_BANNER";

    // Full Key
    private static readonly string EventCharacterIdleKey = "CHAR_IDLE";
    private static readonly string EventCharacterActionKey = "CHAR_ACTION";
    private static readonly string EventCharacterFinishKey = "CHAR_FINISH";

    #endregion

    #endregion

    #region Inspector Setting Properties

    [Header("UI RecTransform Element")]
    [SerializeField] private RectTransform _eventCharacterParentRectTransform;
    
    [Header("Static Image UI Elements")]
    [SerializeField] private List<Image> _staticNativeSizeImageList;
    [SerializeField] private List<Image> _staticNonNativeSizeImageList;
    [SerializeField] private Image _slotArrowImage;
    [SerializeField] private Image _eventDetailImage;
    [SerializeField] private Image _eventHeaderImage;
    [SerializeField] private Image _eventMainBgImage;
    [SerializeField] private Image _eventReadMoreButtonImage;
    [SerializeField] private Image _eventCloseButtonImage;
    [SerializeField] private Image _eventStartButtonImage;
    [SerializeField] private Image _eventStartButtonAdditionalImage;
    [SerializeField] private Image _eventTicketImage;
    [SerializeField] private Image _eventTicketBgImage;

    [Header("Dynamic Image UI Element List")]
    [SerializeField] private Image _eventCharacterImage;
    [SerializeField] private Image _eventResultImage;
    [SerializeField] private Image _eventResultEffectImage;

    [Header("UI Text Elements")]
    [SerializeField] private TextMeshProUGUI _eventHeaderText;
    [SerializeField] private TextMeshProUGUI _eventObjectiveText;
    [SerializeField] private TextMeshProUGUI _eventDescriptionText;
    [SerializeField] private TextMeshProUGUI _eventDurationText;
    [SerializeField] private TextMeshProUGUI _eventTimeText;
    [SerializeField] private TextMeshProUGUI _eventCooldownButtonText;
    [SerializeField] private TextMeshProUGUI _eventRemainTryText;
    [SerializeField] private TextMeshProUGUI _eventDetailText;
    [SerializeField] private TextMeshProUGUI _eventTicketText;

    [Header("UI Button Elements")]
    [SerializeField] private Button _playSlotButton;
    [SerializeField] private Button _backButton;
    [SerializeField] private Button _eventDetailButton;

    [Header("Script Components")]
    [SerializeField] private SlotMachineController _slotMachineController;
    [SerializeField] private UI_InfiniteScroll _infiniteScrollComponent;
    [SerializeField] private Animator _mainCharacterAnimator;
    [SerializeField] private SlotMachineCharacterController _characterSpineController;

    [Header("Scene Setting")]
    [SerializeField] private bool _slotMachineEnable;
    [SerializeField] private RandomMode _randomMode;
    [SerializeField] private GambleType _gambleType;
    [SerializeField] private EventCharacterAnimationScript _characterAnimator;
    [SerializeField] private Material _grayScaleMaterial;
    [SerializeField] private SoundManager.SFX _resultSoundEffect;

    [Header("Event Slot Machine String")]
    [SerializeField] private string _getPlayerDataFunctionString;
    [SerializeField] private string _takeActionFunctionString;

    #endregion

    #region Private Property
    private List<List<ItemData>> _rewardDataList;
    private List<ItemData> _participateRewardList;
    private DateTime _lastPlayDateTime;
    private int _remainPlayTime;
    private int _resultRewardIndex;
    private int _totalDaysLeft;

    private DateTime _releaseDataTime;
    private DateTime _endDateTime;

    private ItemData _ticketData = new ItemData(string.Empty, -1);

    #endregion

    #region Methods

    #region Override Methods

    /// <summary>
    /// Call this method to initialize this game event template.
    /// </summary>
    /// <param name="eventKey">Event key.</param>
    /// <param name="sceneName">Event scene name.</param>
    public override void Init(string eventKey, string sceneName, UnityAction onComplete = null, UnityAction onFail = null)
    {
        base.Init(eventKey, sceneName);

        PopupUIManager.Instance.Show_SoftLoad(true);

        DataManager.Instance.ExcecuteEventCloudScript(
            _getPlayerDataFunctionString
            , new {EventKey = _eventKey }
            , result =>
            {
                bool isSuccess = true;

                isSuccess &= InitData(result);

                if (isSuccess)
                {
                    // UI Initialize.
                    InitUI();
                    GenerateSlots();
                    //GenerateCharacter();
                    GameHelper.UITransition_FadeIn(FadeIn.gameObject, () => PopupUIManager.Instance.Show_SoftLoad(false));
                }
                else
                {
                    string errMsg = "GameEventTemplateVisitStall/Init: Data from cloud-script is not found.";
                    OnLoadSceneFailed(errMsg);
                }
            }
            , OnLoadSceneFailed
        );
    }

    /// <summary>
    /// Call this method when load scene is failed.
    /// </summary>
    /// <param name="errorString"></param>
    protected override void OnLoadSceneFailed(string errorString)
    {
        Debug.LogError(errorString);

        PopupUIManager.Instance.Show_SoftLoad(false);
        base.OnLoadSceneFailed(errorString);
    }

    protected override bool InitData(CloudScriptResultProcess eventData)
    {
        bool isSuccess = true;

        #region Base Event Data

        isSuccess &= base.InitData(eventData);

        if (!isSuccess)
            return false;

        #endregion

        #region Win Rewards Data

        isSuccess &= EventDataDict.ContainsKey(WinRewardString);
        if (!isSuccess) return false;

        isSuccess &= GameHelper.TryDeserializeJSONStr(EventDataDict[WinRewardString].ToString(), out List<object> listOfRewardList);
        if (!isSuccess) return false;

        _rewardDataList = new List<List<ItemData>>();

        foreach (object item in listOfRewardList)
        {
            isSuccess &= GameHelper.TryDeserializeJSONStr(item.ToString(), out Dictionary<string, object> winRewardDict);
            if (!isSuccess) return false;

            isSuccess &= winRewardDict.ContainsKey(GameEventTemplateKeys.RewardListKey);
            if (!isSuccess) return false;

            isSuccess &= GameHelper.TryDeserializeJSONStr(winRewardDict[GameEventTemplateKeys.RewardListKey].ToString(), out List<object> rewardList);
            if (!isSuccess) return false;

            List<ItemData> itemList = new List<ItemData>();

            foreach (object rewardItem in rewardList)
            {
                isSuccess &= GameHelper.TryDeserializeJSONStr(rewardItem.ToString(), out Dictionary<string, object> rewardDict);
                isSuccess &= rewardDict.TryGetValue(GameEventTemplateKeys.ItemIDKey, out object rewardItemID);
                isSuccess &= rewardDict.TryGetValue(GameEventTemplateKeys.ItemAmountKey, out object rewardAmount);
                if (!isSuccess) return false;

                isSuccess &= int.TryParse(rewardAmount.ToString(), out int amount);
                if (!isSuccess) return false;

                itemList.Add(new ItemData(rewardItemID.ToString(), amount));
            }

            _rewardDataList.Add(itemList);
        }

        #endregion

        #region Participate Reward Data

        _participateRewardList = new List<ItemData>();

        isSuccess &= GameHelper.TryDeserializeJSONStr(EventDataDict[GameEventTemplateKeys.ParticipateRewardListKey].ToString(), out List<object> participateRewardList);
        if (isSuccess)
        {
            for (int i = 0; i < participateRewardList.Count; i++)
            {
                isSuccess &= GameHelper.TryDeserializeJSONStr(participateRewardList[i].ToString(), out Dictionary<string, object> rewardDictionary);
                if (!isSuccess) return false;

                isSuccess &= rewardDictionary.ContainsKey(GameEventTemplateKeys.ItemIDKey);
                isSuccess &= rewardDictionary.ContainsKey(GameEventTemplateKeys.ItemAmountKey);

                if (!isSuccess) return false;

                _participateRewardList.Add(new ItemData(
                    rewardDictionary[GameEventTemplateKeys.ItemIDKey].ToString()
                    , int.Parse(rewardDictionary[GameEventTemplateKeys.ItemAmountKey].ToString()))
                );
            }
        }

        #endregion

        #region Event Date Data

        isSuccess &= GameHelper.TryDeserializeJSONStr(eventData.CustomData[GameEventTemplateKeys.EventReleaseDateKey], out _releaseDataTime);
        isSuccess &= GameHelper.TryDeserializeJSONStr(eventData.CustomData[GameEventTemplateKeys.EventEndDateKey], out _endDateTime);

        if (!isSuccess) return false;

        _totalDaysLeft = (int) (_endDateTime - DateTimeData.GetDateTimeUTC()).TotalDays;
        if (_totalDaysLeft <= 0)
            _totalDaysLeft = 0;

        #endregion

        #region Player Data

        isSuccess &= GameHelper.TryDeserializeJSONStr(eventData.CustomData[GameEventTemplateKeys.PlayerDataKey], out Dictionary<string, object> playerDataObjects);

        if (!isSuccess) return false;

        isSuccess &= DateTime.TryParse(playerDataObjects[GameEventTemplateKeys.PlayerActionTimestampKey].ToString(), out _lastPlayDateTime);
        isSuccess &= int.TryParse(playerDataObjects[GameEventTemplateKeys.PlayerRemainPlaytimeKey].ToString(), out _remainPlayTime);

        if (!isSuccess) return false;

        #endregion

        #region Trigger Data

        if (PlayTriggerType == GameEventStartType.Ticket)
        {
            UpdateTicketData();
        }

        #endregion

        return true;
    }

    #endregion

    #region Initialize Methods

    /// <summary>
    /// Call this method to generate festival stalls.
    /// </summary>
    private void GenerateSlots()
    {
        if (!_slotMachineEnable) return;
        _slotMachineController.Init(_eventKey, _rewardDataList);
        _infiniteScrollComponent.Init();
    }

    /// <summary>
    /// Call this method to initialize UI.
    /// </summary>
    private void InitUI()
    {
        #region Text UI
        _eventHeaderText.text = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventHeaderKey + _eventKey);
        _eventObjectiveText.text = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventObjectiveKey + _eventKey);
        _eventDescriptionText.text = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventDescriptionKey + _eventKey);
        _eventTimeText.text = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventTimeKey + _eventKey);
        _eventDetailText.text = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventDetailKey + _eventKey);
        _eventDurationText.text = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventDurationKey + _eventKey, out string resultText) ? string.Format(resultText, _totalDaysLeft) : GameEventTemplateKeys.EventDurationKey + _eventKey;
        _eventRemainTryText.text = $"{_remainPlayTime}/{DefaultPlaytime}";

        if (PlayTriggerType == GameEventStartType.Ticket)
            _eventTicketText.text = $"{_ticketData.Amount}/{RequireTicket[0]}";
        #endregion

        #region Static Image UI

        _eventMainBgImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, _eventThemeInfo.Theme + GameEventTemplateKeys.EventMainBackgroundSuffix, true) != null
            ? SpriteResourceHelper.LoadSubEventImage(_eventKey, _eventThemeInfo.Theme + GameEventTemplateKeys.EventMainBackgroundSuffix)
            : SpriteResourceHelper.LoadSprite(_eventThemeInfo.Theme, SpriteResourceHelper.SpriteType.GAME_Event);

        _eventHeaderImage.sprite = SpriteResourceHelper.LoadSprite(_eventThemeInfo.Theme + GameEventTemplateKeys.EventHeaderBannerSuffix, SpriteResourceHelper.SpriteType.GAME_Event);

        _eventMainBgImage.enabled = true;
        _eventHeaderImage.enabled = true;

        _eventCloseButtonImage.gameObject.SetActive(true);
        _eventReadMoreButtonImage.gameObject.SetActive(true);
        _eventStartButtonImage.gameObject.SetActive(true);
        _eventStartButtonAdditionalImage.gameObject.SetActive(true);
        _eventStartButtonImage.material = _grayScaleMaterial;
        _eventStartButtonAdditionalImage.material = _grayScaleMaterial;

        if (_slotMachineEnable)
            _slotArrowImage.gameObject.SetActive(true);

        if (_staticNativeSizeImageList != null && _staticNativeSizeImageList.Count > 0)
        {
            for (int i = 0; i < _staticNativeSizeImageList.Count; i++)
            {
                _staticNativeSizeImageList[i].sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, GameEventTemplateKeys.StaticNativeImagePrefixKey + i);
                _staticNativeSizeImageList[i].SetNativeSize();
                _staticNativeSizeImageList[i].enabled = true;
            }
        }

        if (_staticNonNativeSizeImageList != null && _staticNonNativeSizeImageList.Count > 0)
        {
            for (int i = 0; i < _staticNonNativeSizeImageList.Count; i++)
            {
                _staticNonNativeSizeImageList[i].sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, GameEventTemplateKeys.StaticNonNativeImagePrefixKey + i);
                _staticNonNativeSizeImageList[i].enabled = true;
            }
        }

        #endregion

        #region Dynamic Image UI

        if (_eventCharacterImage.gameObject.activeInHierarchy)
        {
            _eventCharacterImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, EventCharacterIdleKey);
            _eventCharacterImage.enabled = true;
        }

        switch (_characterAnimator)
        {
            case EventCharacterAnimationScript.Native:
                if (_eventCharacterImage.gameObject.activeInHierarchy)
                {
                    _eventCharacterImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, EventCharacterIdleKey);
                    _eventCharacterImage.enabled = true;
                    PlayCharacterAnimationState(EventCharacterAnimationState.Idle);
                }
                break;
            case EventCharacterAnimationScript.Spine:
                if (_characterSpineController != null)
                    PlayCharacterAnimationState(EventCharacterAnimationState.Idle);
                break;
        }

        PlayCharacterAnimationState(EventCharacterAnimationState.Idle);

        #endregion

        #region Ticket UI

        if (PlayTriggerType == GameEventStartType.Ticket)
        {
            // Text UI
            _eventTicketText.text = $"{_ticketData.Amount}/{RequireTicket[0]}";
            
            _eventTicketImage.sprite = SpriteResourceHelper.LoadSprite(GameEventTemplateKeys.TicketIconImagePrefixKey + _eventThemeInfo.Theme, SpriteResourceHelper.SpriteType.ICON_Item);
            _eventTicketBgImage.sprite = SpriteResourceHelper.LoadSprite(GameEventTemplateKeys.TicketBgImagePrefixKey + _eventThemeInfo.Theme, SpriteResourceHelper.SpriteType.GAME_Event);

            _eventTicketImage.enabled = true;
            _eventTicketBgImage.enabled = true;
        }

        #endregion

        #region Button UI

        _backButton.onClick.RemoveAllListeners();
        _backButton.onClick.AddListener(Close);

        _eventDetailButton.onClick.RemoveAllListeners();
        _eventDetailButton.onClick.AddListener(EventDetail);
        #endregion

        if (DefaultCooldown > 0)
            StartCoroutine(CooldownTimer(_lastPlayDateTime, DefaultCooldown, _eventCooldownButtonText, CoolDownEnd));
    }

    /// <summary>
    /// Call this method to generate spine character of this event.
    /// </summary>
    private void GenerateCharacter()
    {
        string characterPrefabPath = string.Format(EventPrefabResourcePath, _eventKey) + EventCharacterPrefabName;
        GameObject characterGameObject = ResourceManager.Load(characterPrefabPath) as GameObject;

        _characterSpineController = Instantiate(characterGameObject, _eventCharacterParentRectTransform).GetComponent<SlotMachineCharacterController>();
        PlayCharacterAnimationState(EventCharacterAnimationState.Idle);
    }

    #endregion

    #region Action Methods

    /// <summary>
    /// Call this method to show the read more.
    /// </summary>
    public void ShowReadMore()
    {
        _eventDetailImage.gameObject.SetActive(true);
    }

    /// <summary>
    /// Call this method to hide read more.
    /// </summary>
    public void HideReadMore()
    {
        _eventDetailImage.gameObject.SetActive(false);
    }

    /// <summary>
    /// Call this method to set button state.
    /// </summary>
    /// <param name="value">button state.</param>
    public void SetEnableButtonState(bool value)
    {
        _backButton.enabled = value;
        _playSlotButton.enabled = value;
    }

    /// <summary>
    /// Call this method if the gamble use slot machine logic and display.
    /// </summary>
    public void StartSlotMachine()
    {
        StartCoroutine(GetSlotRoutine());
        _slotMachineController.PlaySlotMachine(EndAction
            , delegate { PlayCharacterAnimationState(EventCharacterAnimationState.Success); }
        );
    }

    /// <summary>
    /// Call this method if the gamble is the normal one.
    /// </summary>
    public void StartRandomGambleAction()
    {
        StartCoroutine(GetRandomGambleRoutine(
            delegate
            {
                PlayCharacterAnimationState(EventCharacterAnimationState.Success);
                SoundManager.PlaySFX(_resultSoundEffect);
                PlayGambleResult(EndAction);
            },
            delegate
            {
                string title = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventActionErrorTitleKey);
                string message = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventActionErrorMessageKey);

                PopupUIManager.Instance.ShowPopup_Error(
                    title
                    , message
                    , delegate
                    {
                        SetEnableButtonState(true);
                        PlayCharacterAnimationState(EventCharacterAnimationState.Idle);
                    }
                );
            })
        );
    }

    /// <summary>
    /// Call this method to show the result of normal gamble.
    /// </summary>
    /// <param name="onComplete"></param>
    public void PlayGambleResult(UnityAction onComplete)
    {
        _eventResultEffectImage.DOFade(1, 0.5f).SetEase(Ease.Linear);
        _eventResultImage.DOFade(1, 0.5f).SetEase(Ease.Linear);

        Vector3 currentPos = _eventResultEffectImage.transform.position;
        _eventResultEffectImage.transform.DOMove(currentPos + Vector3.up * 10, 0.5f).SetEase(Ease.Linear).onComplete += () =>
        {
            _eventResultEffectImage.transform.DOMove(currentPos, 0.75f).SetEase(Ease.Linear).onComplete += () =>
            {
                StartCoroutine(DelayActionTime(1, onComplete));
            };
        };
    }

    /// <summary>
    /// Call this method to reset the result.
    /// </summary>
    public void ResetGambleResult()
    {
        _eventResultEffectImage.color = new Color(1, 1, 1, 0);
        _eventResultImage.color = new Color(1, 1, 1, 0);
    }

    /// <summary>
    /// Call this method when the action is end.
    /// </summary>
    public void EndAction()
    {
        string title = LocalizationManager.Instance.GetText(GameEventTemplateKeys.NotiRewardTitleKey);
        string message = LocalizationManager.Instance.GetText(GameEventTemplateKeys.NotiRewardMessageKey);

        OnReceiveItem(_rewardDataList[_resultRewardIndex]);
        if (_participateRewardList.Count > 0 && _participateRewardList != null)
            OnReceiveItem(_participateRewardList);

        PopupUIManager.Instance.ShowPopup_GotRewardAnimated(
                title
                , message
                , _rewardDataList[_resultRewardIndex]
                , delegate
                {
                    DataManager.Instance.LoadInventory(delegate
                        {
                            if (PlayTriggerType == GameEventStartType.Ticket)
                            {
                                UpdateTicketData(delegate
                                {
                                    _eventTicketText.text = $"{_ticketData.Amount}/{RequireTicket[0]}";
                                });
                            }
                        }
                        , null);
                    PlayCharacterAnimationState(EventCharacterAnimationState.Idle);
                    ResetGambleResult();
                }

            ).SetAnimationEase(Ease.OutExpo)
            .SetDelay(0.1f)
            .SetAnimationDuration(0.25f)
            .SetMaxScale(8f);

        _remainPlayTime--;
        _eventRemainTryText.text = $"{_remainPlayTime}/{DefaultPlaytime}";
        _lastPlayDateTime = DateTimeData.GetDateTimeUTC();

        SetEnableButtonState(true);
        StartCoroutine(CooldownTimer(_lastPlayDateTime, DefaultCooldown, _eventCooldownButtonText, CoolDownEnd));
    }

    /// <summary>
    /// Call this method to get remain cooldown of action.
    /// </summary>
    /// <returns></returns>
    public float GetRemainCoolDown()
    {
        DateTime currentDateTime = DateTimeData.GetDateTimeUTC();
        float cooldown = (float)(_lastPlayDateTime - currentDateTime).TotalSeconds + DefaultCooldown;
        return cooldown;
    }

    /// <summary>
    /// Call this method when the play trigger game event.
    /// </summary>
    public void OnSelectAction()
    {
        SetEnableButtonState(false);
        PlayCharacterAnimationState(EventCharacterAnimationState.Loading);

        // Check if the action is valid.
        switch (PlayTriggerType)
        {
            case GameEventStartType.Ticket when DefaultCooldown > 0 && _ticketData.Amount <= 0:
            {
                string noTicketTitle = LocalizationManager.Instance.GetText(GameEventTemplateKeys.NoTicketTitleKey);
                string noTicketMessage = LocalizationManager.Instance.GetText(GameEventTemplateKeys.NoTicketMessageKey);

                PopupUIManager.Instance.ShowPopup_Error(
                    noTicketTitle
                    , noTicketMessage
                    , delegate
                    {
                        SetEnableButtonState(true);
                        PlayCharacterAnimationState(EventCharacterAnimationState.Idle);
                    }
                );
                return;
            }

            case GameEventStartType.Ticket when _ticketData.Amount > 0 && GetRemainCoolDown() > 0:
            {
                string noTicketTitle = LocalizationManager.Instance.GetText(GameEventTemplateKeys.CooldownTitleKey);
                string noTicketMessage = LocalizationManager.Instance.GetText(GameEventTemplateKeys.CooldownMessageKey);
                PopupUIManager.Instance.ShowPopup_Error(
                    noTicketTitle
                    , noTicketMessage
                    , delegate
                    {
                        SetEnableButtonState(true);
                        PlayCharacterAnimationState(EventCharacterAnimationState.Idle);
                    }
                );
                return;
            }

            case GameEventStartType.Ticket when _remainPlayTime <= 0:
            {
                string noTicketTitle = LocalizationManager.Instance.GetText(GameEventTemplateKeys.NoRemainTryTitleKey);
                string noTicketMessage = LocalizationManager.Instance.GetText(GameEventTemplateKeys.NoRemainTryMessageKey);
                PopupUIManager.Instance.ShowPopup_Error(
                    noTicketTitle
                    , noTicketMessage
                    , delegate
                    {
                        SetEnableButtonState(true);
                        PlayCharacterAnimationState(EventCharacterAnimationState.Idle);
                    }
                );
                return;
            }

            case GameEventStartType.Cooldown when GetRemainCoolDown() > 0:
            {
                string noTicketTitle = LocalizationManager.Instance.GetText(GameEventTemplateKeys.CooldownTitleKey);
                string noTicketMessage = LocalizationManager.Instance.GetText(GameEventTemplateKeys.CooldownMessageKey);
                PopupUIManager.Instance.ShowPopup_Error(
                    noTicketTitle
                    , noTicketMessage
                    , delegate
                    {
                        SetEnableButtonState(true);
                        PlayCharacterAnimationState(EventCharacterAnimationState.Idle);
                    }
                );
                return;
            }

            case GameEventStartType.Cooldown when _remainPlayTime <= 0:
            {
                string noTicketTitle = LocalizationManager.Instance.GetText(GameEventTemplateKeys.NoRemainTryTitleKey);
                string noTicketMessage = LocalizationManager.Instance.GetText(GameEventTemplateKeys.NoRemainTryMessageKey);
                PopupUIManager.Instance.ShowPopup_Error(
                    noTicketTitle
                    , noTicketMessage
                    , delegate
                    {
                        SetEnableButtonState(true);
                        PlayCharacterAnimationState(EventCharacterAnimationState.Idle);
                    }
                );
                return;
            }
        }

        // Call generate participate reward.
        StartCoroutine(ActionRewardGenerate(
            _playSlotButton.transform
            , _participateRewardList)
        );

        switch (_gambleType)
        {
            case GambleType.SlotMachine:
                StartSlotMachine();
                break;
            case GambleType.Normal:
                StartRandomGambleAction();
                break;
        }
    }

    /// <summary>
    /// Call this method to play specific character animation.
    /// </summary>
    /// <param name="animationState">Animation's state.</param>
    public void PlayCharacterAnimationState(EventCharacterAnimationState animationState)
    {
        switch (_characterAnimator)
        {
            case EventCharacterAnimationScript.Native:
                _mainCharacterAnimator.Play(animationState.ToString());
                break;
            case EventCharacterAnimationScript.Spine:
                switch (animationState)
                {
                    case EventCharacterAnimationState.Idle:
                        _characterSpineController.PlayIdleAnimation();
                        break;
                    case EventCharacterAnimationState.Loading:
                        // None for now.
                        break;
                    case EventCharacterAnimationState.Success:
                        _characterSpineController.PlaySuccessfulAnimation();
                        break;
                    case EventCharacterAnimationState.Failed:
                        // None for now.
                        break;
                }
                break;
        }
    }

    /// <summary>
    /// Call this method when cooldown is end.
    /// </summary>
    public void CoolDownEnd()
    {
        _eventCooldownButtonText.text = LocalizationManager.Instance.GetText(EventStartButtonPrefixKey + _eventKey);
        _eventStartButtonAdditionalImage.material = null;
        _eventStartButtonImage.material = null;
    }

    /// <summary>
    /// Call this method to update ticket data.
    /// </summary>
    public void UpdateTicketData(UnityAction onComplete = null)
    {
        string ticketID = GameEventTemplateKeys.TicketIDPrefixKey + _eventThemeInfo.Theme;
        int ticketAmount = DataManager.Instance.InventoryData.GetAmountByItemID(ticketID);

        _ticketData = new ItemData(ticketID, ticketAmount);

        onComplete?.Invoke();
    }

    #endregion

    #region Co-routine Methods

    /// <summary>
    /// Call this co-routine method to get slot machine result.
    /// </summary>
    /// <returns></returns>
    private IEnumerator GetSlotRoutine()
    {
        yield return new WaitForSeconds(2);

        DataManager.Instance.ExcecuteEventCloudScript(
            _takeActionFunctionString
            , new
            {
                EventKey = _eventKey
                , RandomMode = _randomMode.ToString()
                , TicketID = _ticketData.ItemID
            }
            , delegate(CloudScriptResultProcess result)
            {
                _resultRewardIndex = int.Parse(result.CustomData[ResultRewardIndexString]);

                //string ecJson = result.CustomData[GameEventTemplateKeys.EventCurrencyKey];
                //DataManager.Instance.InventoryData.SetEventCurrency(ecJson);

                // TODO: Event currency and Item Data.

                bool isSuccess = GameHelper.TryDeserializeJSONStr(result.CustomData["item_data"], out Dictionary<string, int> ItemDataDict);
                if (isSuccess)
                {
                    List<ItemData> itemDataList = DataManager.ConvertDictionaryToItemDataList(ItemDataDict);
                    foreach (ItemData itemData in itemDataList)
                    {
                        DataManager.Instance.InventoryData.GrantItemByItemID(itemData.ItemID, itemData.Amount);
                    }
                }

                // Need to be checked again later.
                _slotMachineController.ReceiveResult(_rewardDataList[_resultRewardIndex][0]);
            }
            , Debug.Log
        );

        yield return null;
    }

    /// <summary>
    /// Call this co-routine method to get the random gamble result.
    /// </summary>
    /// <param name="onComplete">Delegate method when this action is complete.</param>
    /// <param name="onFail">Delegate method when this action is fail to complete.</param>
    /// <returns></returns>
    private IEnumerator GetRandomGambleRoutine(UnityAction onComplete = null, UnityAction onFail = null)
    {
        yield return new WaitForSeconds(2);

        bool isSuccess = true;

        DataManager.Instance.ExcecuteEventCloudScript(
            _takeActionFunctionString
            , new
            {
                EventKey = _eventKey
                , RandomMode = _randomMode.ToString()
                , TicketID = _ticketData.ItemID
            }
            , delegate (CloudScriptResultProcess result)
            {
                isSuccess &= result.IsSuccess;
                isSuccess &= result.CustomData.ContainsKey(ResultRewardIndexString);
                Debug.Log(result.CustomData[ResultRewardIndexString]);
                Debug.Log(result.CustomData.ContainsKey(ResultRewardIndexString));
                if (!isSuccess)
                {
                    onFail?.Invoke();
                    return;
                }

                isSuccess &= int.TryParse(result.CustomData[ResultRewardIndexString], out _resultRewardIndex);

                if (!isSuccess)
                {
                    onFail?.Invoke();
                    return;
                }

                //string ecJson = result.CustomData[GameEventTemplateKeys.EventCurrencyKey];
                //DataManager.Instance.InventoryData.SetEventCurrency(ecJson);
                //DataManager.Instance.LoadInventory(null, null);

                // TODO: Event currency and Item Data.

                isSuccess = GameHelper.TryDeserializeJSONStr(result.CustomData["item_data"], out Dictionary<string, int> ItemDataDict);
                if (isSuccess)
                {
                    List<ItemData> itemDataList = DataManager.ConvertDictionaryToItemDataList(ItemDataDict);
                    foreach (ItemData itemData in itemDataList)
                    {
                        DataManager.Instance.InventoryData.GrantItemByItemID(itemData.ItemID, itemData.Amount);
                    }
                }


                if (_eventResultImage != null)
                    _eventResultImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, EventResultImageIndexPrefix + _resultRewardIndex);
                onComplete?.Invoke();
            }
            , Debug.LogError
        );
    }

    #endregion

    #region Other Methods

    public override void Close()
    {
        PopupUIManager.Instance.Show_SoftLoad(true);

        GameHelper.UITransition_FadeOut(FadeOut.gameObject, () =>
        {
            base.Close();
            PopupUIManager.Instance.Show_SoftLoad(false);
        });

    }

    public void EventDetail()
    {
        string title = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventDetailHeaderKey);
        string message = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventDetailKey + _eventKey);

        PopupUIManager.Instance.ShowPopup_DetailWindow(title, message);
    }

    #endregion

    #endregion
}

