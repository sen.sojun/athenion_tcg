﻿using DG.Tweening;
using GameEvent;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class EsiimsiResultController : MonoBehaviour, IGameEventResult
{
    [SerializeField] private Image _eventResultEffectImage;
    [SerializeField] private Image _eventResultImageBG;
    [SerializeField] private Image _eventResultImage;

    public void StartResult()
    {
        // Nothing.
    }

    public void EndResult(UnityAction onComplete)
    {
        _eventResultEffectImage.DOFade(0.5f, 0.25f).SetEase(Ease.Linear);
        _eventResultImageBG.DOFade(1f, 0.5f).SetEase(Ease.Linear);
        _eventResultImage.DOFade(1, 0.5f).SetEase(Ease.Linear);

        Vector3 currentPos = _eventResultEffectImage.transform.position;
        _eventResultImage.transform.DOMove(currentPos + Vector3.up * 10, 0.5f).SetEase(Ease.Linear).onComplete += () =>
        {
            _eventResultImage.transform.DOMove(currentPos, 0.75f).SetEase(Ease.Linear).onComplete += () =>
            {
                StartCoroutine(GameEventManager.Instance.DelayActionTime(1, onComplete));
            };
        };
    }

    public void ResetResult()
    {
        if (_eventResultEffectImage != null) _eventResultEffectImage.color = new Color(1, 1, 1, 0);
        if (_eventResultImageBG != null) _eventResultImageBG.color = new Color(1, 1, 1, 0);
        if (_eventResultImage) _eventResultImage.color = new Color(1, 1, 1, 0);
    }
}
