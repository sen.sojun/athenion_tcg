﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Karamucho;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

public class SlotMachineController : MonoBehaviour
{
    #region Static Value

    private static readonly string ItemResourcePath = "Images/Items/";
    private static readonly string EventSpriteResourcePath = "Events/{0}/Sprites/";
    private static readonly string EventPrefabResourcePath = "Events/{0}/Prefabs/SlotPrefab";
    private static readonly string SlotImagePrefix = "SLOT_";

    private static readonly string SlotBgKey = "SLOT_BG";

    private static readonly int DelayWaitTime = 2;

    #endregion

    #region Public Propeties

    [Header("Slot Static Value")]
    public float MinSlotSpeed;              // Max speed for slot machine.
    public float MaxSlotSpeed;              // Min speed for slot machine.

    [Header("UI Elements")]
    public ScrollRect SlotMachineScrollRect;     // Scroll view of slot machine.
    public RectTransform SlotContent;

    #endregion

    #region Private Propeties

    private List<SlotMachineItem> _stallList;
    private bool _isArrowEnd;
    private Tween _arrowTween;
    private float _currentSpeed;
    private bool _isReceiveResult;
    private bool _isStopSlot;

    private ItemData _resultItemData;

    #endregion

    #region Methods

    #region Unity Call-back Methods
    private void OnTriggerEnter(Collider other)
    {
        SlotMachineItem script = other.GetComponent<SlotMachineItem>();

        //Debug.Log("Slot enter.");

        if (script == null) return;

        if(!_isStopSlot)
            SoundManager.PlaySFX(SoundManager.SFX.SFX_RPS_BUBBLE_SPAWN);

        if (_isReceiveResult && _currentSpeed <= MinSlotSpeed)
        {
            //Debug.Log(script.GetItemData().ItemID);

            if (_resultItemData.ItemID == script.GetItemData().ItemID)
            {
                //script.SetGrayScale(false);
                script.SetImageHighlightState(true);
                _isStopSlot = true;
                SoundManager.PlaySFX(SoundManager.SFX.SFX_RPS_VICTORY);
            }
        }

    }
    #endregion

    #region Intiailize Methods

    // TODO: May need to be checked later on.
    public void Init(string eventKey, List<List<ItemData>> rewardItemDataList)
    {
        _stallList = new List<SlotMachineItem>();
        _isStopSlot = true;

        string slotPrefabPath = string.Format(EventPrefabResourcePath, eventKey);
        string slotImagePrefixPath = string.Format(EventSpriteResourcePath, eventKey);
        GameObject slotPrefab = ResourceManager.Load(slotPrefabPath) as GameObject;

        Dictionary<string, string> ImagePathDict = new Dictionary<string, string>
        {
            {SlotMachineImageEnum.SlotBg.ToString(), string.Empty }
            , {SlotMachineImageEnum.Slot.ToString(), string.Empty }
        };

        ImagePathDict[SlotMachineImageEnum.SlotBg.ToString()] = string.Format(EventSpriteResourcePath, eventKey) + SlotBgKey;

        // Note: Generate 2 set of each slots.
        for (int i = 0; i < 2; i++)
        {
            for (int j = 0; j < rewardItemDataList.Count; j++)
            {
                SlotMachineItem script = Instantiate(slotPrefab, SlotContent).GetComponent<SlotMachineItem>();

                ImagePathDict[SlotMachineImageEnum.Slot.ToString()] = string.Format(EventSpriteResourcePath, eventKey) + SlotImagePrefix + j;

                string slotImagePath = slotImagePrefixPath + SlotImagePrefix + j;
                script.Init(ImagePathDict
                    , rewardItemDataList[j][0]
                );

                _stallList.Add(script);
            }
        }
    }

    public void ResetSlot()
    {
        foreach (SlotMachineItem machineItem in _stallList)
        {
            machineItem.SetImageHighlightState(false);
        }
    }

    #endregion

    #region Action Methods

    public void PlaySlotMachine(UnityAction onComplete, UnityAction onCharacterAction)
    {
        ResetSlot();
        _currentSpeed = 0;
        _isReceiveResult = false;
        _isStopSlot = false;

        StartCoroutine(SlotRoutine(onComplete, onCharacterAction));
    }

    public void ReceiveResult(ItemData resultItemData)
    {
        _isReceiveResult = true;
        _resultItemData = resultItemData;
    }

    #endregion

    #region Co-routine Methods

    private IEnumerator SlotRoutine(UnityAction onComplete, UnityAction onCharacterAction)
    {
        while (!_isStopSlot)
        {
            // TODO: Check
            if (!_isReceiveResult)
            {
                _currentSpeed += (Time.deltaTime);

                if (_currentSpeed > MaxSlotSpeed)
                    _currentSpeed = MaxSlotSpeed;

                //Debug.Log(_currentSpeed);
                SlotMachineScrollRect.verticalNormalizedPosition += _currentSpeed * Time.deltaTime;
            }
            else
            {
                _currentSpeed -= (Time.deltaTime);

                SlotMachineScrollRect.verticalNormalizedPosition += _currentSpeed * Time.deltaTime;

                if (_currentSpeed < MinSlotSpeed)
                    _currentSpeed = MinSlotSpeed;
            }

            yield return null;
        }

        onCharacterAction?.Invoke();

        yield return new WaitForSeconds(DelayWaitTime);

        onComplete?.Invoke();
    }

    #endregion

    #endregion
}
