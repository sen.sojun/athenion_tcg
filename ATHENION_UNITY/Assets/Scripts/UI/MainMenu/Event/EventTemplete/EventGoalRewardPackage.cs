﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class EventGoalRewardPackage : MonoBehaviour
{
    #region Public Properties
    public Image IMG_Preview;
    public Image IMG_RewardBG;
    public Image IMG_ReqBG;
    public GameObject IMG_Claimed;
    public GameObject GoalPanel;
    public TextMeshProUGUI TXT_GoalText;
    public TextMeshProUGUI TXT_RewardAmount;
    #endregion

    #region Private Properties
    List<ItemData> _itemList;
    string _popupHeader;
    string _popupDetail;
    #endregion

    public void InitData(string goalTxt, List<ItemData> itemList, bool claimed, bool showGoalTxt, Sprite rewardBG = null, Sprite goalBG = null)
    {
        _itemList = itemList;
        GoalPanel.SetActive(showGoalTxt);
        if (showGoalTxt)
        {
            TXT_GoalText.text = goalTxt;
        }

        if(IMG_RewardBG != null && rewardBG != null)
        {
            IMG_RewardBG.sprite = rewardBG;
        }
        if(IMG_ReqBG != null && goalBG != null)
        {
            IMG_ReqBG.sprite = goalBG;
        }

        if(_itemList.Count <= 1)
        {
            //IMG_Preview
            Sprite sprite = SpriteResourceHelper.LoadSprite(_itemList[0].ItemID, SpriteResourceHelper.SpriteType.ICON_Item);
            IMG_Preview.sprite = sprite;
            TXT_RewardAmount.text = "x"+_itemList[0].Amount.ToString();
        }
        else
        {
            TXT_RewardAmount.gameObject.SetActive(false);
        }
    }

    public void SetPopupMessage(string header, string detail)
    {
        _popupHeader = header;
        _popupDetail = detail;
    }

    public void ActiveClaimedTick(bool active)
    {
        IMG_Claimed.SetActive(active);
    }

    public void ShowDetail()
    {
        /*
        PopupUIManager.Instance.ShowPopup_PackDetail_Short("69 Gifts Rewards",
            "Get the following rewards when requirement is met",
            _itemList);
        */
        PopupUIManager.Instance.ShowPopup_PreviewReward(_popupHeader, _popupDetail,_itemList);
    }
}
