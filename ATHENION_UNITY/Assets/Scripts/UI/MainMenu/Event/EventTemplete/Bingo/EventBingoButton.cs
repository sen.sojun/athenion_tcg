﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;
using System.Linq;
using TMPro;
using UnityEngine.SceneManagement;
using DG.Tweening;
using System;
using System.Security.Cryptography;
using Spine;
using Spine.Unity;
using Karamucho;
using UnityEngine.Events;
using Random = UnityEngine.Random;

public class EventBingoButton : MonoBehaviour
{
    #region Public Properties
    [Header("Child Ref")]
    public Image ButtonImage;
    public Image BackgroundImage;
    public Image IMGTick;
    public TextMeshProUGUI TXTRewardAmount;
    public string ButtonKey { get => _buttonKey; private set => _buttonKey = value; }
    #endregion

    #region Private Properties
    private string _buttonKey;
    private Tweener cachedTweener = null;
    #endregion

    public void InitData(string buttonKey, Sprite bgSpr, Sprite stampSpr, Sprite rewardSpr, string rewAmt, bool tick)
    {
        ButtonKey = buttonKey;

        SetImage(ButtonImage, rewardSpr);
        SetImage(BackgroundImage, bgSpr);
        SetImage(IMGTick, stampSpr);

        TXTRewardAmount.text = rewAmt;
        TXTRewardAmount.gameObject.SetActive(rewAmt.Length > 0);

        IMGTick.gameObject.SetActive(tick);
    }

    private void SetImage(Image imgComp, Sprite spr)
    {
        imgComp.gameObject.SetActive((spr != null));
        imgComp.sprite = spr;
    }

    public void SetAsHidden()
    {
        ButtonImage.gameObject.SetActive(false);
        BackgroundImage.gameObject.SetActive(false);
        IMGTick.gameObject.SetActive(false);
        KaramuchoButtonUI btnComp = GetComponent<KaramuchoButtonUI>();
        btnComp?.onClick.RemoveAllListeners();
    }

    public void SetClaimed()
    {
        IMGTick.gameObject.SetActive(true);
    }

    public void PlayStampAnimation()
    {
        if (cachedTweener != null)
        {
            cachedTweener.Kill();
            ResetAnim();
            cachedTweener = null;
        }

        float startSize = 5f;
        float animTime = 0.20f;
        IMGTick.transform.localScale = new Vector3(startSize, startSize, IMGTick.transform.localScale.z);

        cachedTweener = IMGTick.transform.DOScale(1f, animTime).OnComplete(delegate() { cachedTweener = null; }); ;
    }

    private void ResetAnim()
    {
        float defaultSize = 1f;
        IMGTick.transform.localScale = new Vector3(defaultSize, defaultSize, IMGTick.transform.localScale.z);
    }
}
