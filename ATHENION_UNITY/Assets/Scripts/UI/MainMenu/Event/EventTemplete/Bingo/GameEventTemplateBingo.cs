﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;
using System.Linq;
using TMPro;
using UnityEngine.SceneManagement;
using DG.Tweening;
using System;
using System.Security.Cryptography;
using Spine;
using Spine.Unity;
using Karamucho;
using UnityEngine.Events;
using Random = UnityEngine.Random;

public class GameEventTemplateBingo : BaseGameEventTemplate
{
    #region enums
    public enum AssetName
    {
        None,
        EventBackground,
        HeaderBanner,
        Ticket,
        TicketBox,
        Stamp,
        ButtonBackground,
        GridBackground,
        ClearRewardClaimButton,
        ClearRewardImage,
        Bingo,
        
        SD_PreStamp,
        SD_Stamp1,
        SD_Stamp2,
        SD_Bingo,

        Last
    }
    public enum BingoStatus
    {
        Idle,
        Processing
    }
    public enum BingoButtonType
    {
        BingoButton,
        RewardButton
    }
    #endregion
    #region Public Properties
    public GameObject UICanvas;

    [Header("UI Ref")]
    public Image IMGBackgroundImage;
    public Image IMGHeaderBanner;
    public Image IMGClearReward;
    public Image IMGBTNClearReward;
    public Image IMGBingo;
    public Image IMGClearRewardClaimed;
    public Image IMGTicket;
    public Image IMGTicketBox;
    public Image IMGGrid;
    public Image IMGClearRewardStamp;
    public Image IMGSDCharacter;
    public TextMeshProUGUI CooldownText;
    public TextMeshProUGUI TXTMoreDetail;
    public TextMeshProUGUI TXTMoreDetailHeader;
    public TextMeshProUGUI TXTEventName;
    public TextMeshProUGUI TXTClaimClearReward;
    public TextMeshProUGUI TXTObjective;
    public GameObject BingoButtonPrefab;
    public Button BTNReadMore;
    public Button BTNClose;
    #endregion
    #region Private Properties
    private static readonly string PrefabResourcePath = "Events/{0}/Prefabs/";
    private int _ticketRequired;
    private int _dailyChance;
    private int _playerTicket;
    private int[] _colKey = new int[] { 0, 1, 2, 3, 4 };
    private string _textColor;
    private string _ticketID;
    private string[] _rowKey = new string[] { "R", "A", "B", "C", "D" };
    private bool _isOnCooldown;
    private List<string> _openedSlot;
    private List<string> _claimedSlots;
    private Dictionary<string, List<ItemData>> _rewardList;
    private Dictionary<string, EventBingoButton> _bingoBtnList;
    private Dictionary<string, List<string>> _slotRewardCond;
    private List<GameObject> _boardButtonList;
    private ItemData _requireTicket;
    private GameEventInfo _eventInfo;
    private Vector2 _boardSize;
    private DateTime _playerLastTimeStamp;
    private BingoStatus _status;
    private Coroutine _cdTimer;

    private string _clearRewardID = "clear_reward";
    private string _participateRewardID = "participation";
    private int _remainPlaytime;
    #endregion

    public override void Init(string eventKey, string sceneName, UnityAction onComplete = null, UnityAction onFail = null)
    {
        base.Init(eventKey, sceneName);

        GameEventDB gameEventDB = DataManager.Instance.GetGameEventDB();
        _eventInfo = gameEventDB.GetGameEventInfo()[_eventKey];
        DataManager.Instance.ExcecuteEventCloudScript("GetBingoData", new { EventKey = _eventKey }, OnSuccessGetData, OnCommonError);
        PopupUIManager.Instance.Show_SoftLoad(true);
        _status = BingoStatus.Processing;

        UICanvas.SetActive(false);
        _eventKey = eventKey;
    }

    public override void Close()
    {
        PopupUIManager.Instance.Show_SoftLoad(true);
        GameHelper.UITransition_FadeOut(FadeOut.gameObject, () =>
        {
            UnloadEventScene();
            PopupUIManager.Instance.Show_SoftLoad(false);
        });
    }

    public void ShowClearReward()
    {
        PopupUIManager.Instance.ShowPopup_PreviewReward(
            LocalizationManager.Instance.GetText("EVENT_REWARD_TITLE_" + _eventKey)
            , LocalizationManager.Instance.GetText("EVENT_CLEAR_REWARD_DETAIL_" + _eventKey)
            , _rewardList[_clearRewardID]
        );
    }

    protected override void OnDestroy()
    {
        _cdTimer = null;

        base.OnDestroy();
    }

    public void UnloadEventScene()
    {
        SceneManager.UnloadSceneAsync(SceneName);
    }

    public void ClaimBingoClearReward()
    {
        if(_claimedSlots.Contains(_clearRewardID))
        {
            PopupUIManager.Instance.ShowPopup_Error(LocalizationManager.Instance.GetText("EVENT_ALREADY_CLAIMED_TITLE_" + _eventKey),
                                        LocalizationManager.Instance.GetText("EVENT_ALREADY_CLAIMED_DETAIL_" + _eventKey));
            return;
        }
        
        foreach(KeyValuePair<string, List<string>> cList in _slotRewardCond)
        {
            if (cList.Key == _participateRewardID) continue;

            if (!_claimedSlots.Contains(cList.Key))
            {
                PopupUIManager.Instance.ShowPopup_Error(LocalizationManager.Instance.GetText("EVENT_ERROR"), 
                                                    LocalizationManager.Instance.GetText("EVENT_CLEAR_REQUIRED_DETAIL_" + _eventKey));
                return;
            }
        }

        PopupUIManager.Instance.Show_SoftLoad(true);
        _status = BingoStatus.Processing;

        DataManager.Instance.ExcecuteEventCloudScript("ClaimBingoReward", 
                                                        new { EventKey = _eventKey, Index = _clearRewardID }, 
                                                        delegate
                                                        {
                                                            PopupUIManager.Instance.Show_SoftLoad(false);
                                                            _status = BingoStatus.Idle;
                                                            ShowRewardPopup(_rewardList[_clearRewardID]);
                                                            _claimedSlots.Add(_clearRewardID);
                                                            OnEventComplete();
                                                            OnReceiveItem(_rewardList[_clearRewardID]);
                                                            UpdateUI();
                                                        }, 
                                                        OnCommonError);
        
    }

    private void OnCommonError(string arg0)
    {
        PopupUIManager.Instance.Show_SoftLoad(false);
        _status = BingoStatus.Idle;

        PopupUIManager.Instance.ShowPopup_Error(LocalizationManager.Instance.GetText("EVENT_ERROR"),
                                                string.Format("{0} ({1})",LocalizationManager.Instance.GetText("EVENT_GENERAL_ERROR"), arg0), 
                                                Close);
    }

    private void OnSuccessGetData(CloudScriptResultProcess result)
    {
        base.InitData(result);

        Dictionary<string, object> rawPlayerData = JsonConvert.DeserializeObject<Dictionary<string, object>>(result.CustomData["player_data"]);
        Dictionary<string, object> rawEventData = JsonConvert.DeserializeObject<Dictionary<string, object>>(result.CustomData["event_data"]);

        UpdatePlayerData(rawPlayerData);

        Dictionary<string, object> rawRewardsData = JsonConvert.DeserializeObject<Dictionary<string, object>>(rawEventData["rewards"].ToString());
        _rewardList = new Dictionary<string, List<ItemData>>();
        foreach (KeyValuePair<string, object> rawRewardData in rawRewardsData)
        {
            List<Dictionary<string, object>> rawData = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(rawRewardData.Value.ToString());
            string rewardKey = rawRewardData.Key;
            List<ItemData> itemList = new List<ItemData>();

            foreach (Dictionary<string, object> data in rawData)
            {
                string rewardID = data["itemID"].ToString();
                int.TryParse(data["amount"].ToString(), out int rewardAmt);
                itemList.Add(new ItemData(rewardID, rewardAmt));
            }

            _rewardList.Add(rewardKey, itemList);
        }

        Dictionary<string, object> rawRewardCondition = JsonConvert.DeserializeObject<Dictionary<string, object>>(rawEventData["slot_reward"].ToString());
        _slotRewardCond = new Dictionary<string, List<string>>();
        foreach (KeyValuePair<string, object> rawConditionData in rawRewardCondition)
        {
            List<string> rawData = JsonConvert.DeserializeObject<List<string>>(rawConditionData.Value.ToString());
            string condKey = rawConditionData.Key;
            List<string> slotList = new List<string>();
            foreach (string data in rawData)
            {
                slotList.Add(data);
            }

            _slotRewardCond.Add(condKey, slotList);
        }
        
        _textColor = rawEventData["textcolor"].ToString();
        if(PlayTriggerType == GameEventStartType.Ticket)
            _ticketID = GameEventTemplateKeys.TicketIDPrefixKey + _eventThemeInfo.Theme;

        InitUI();
        UpdateUI();

        if (BTNReadMore != null)
        {
            BTNReadMore.onClick.RemoveAllListeners();
            BTNReadMore.onClick.AddListener(ShowEventDetail);
        }

        if (BTNClose != null)
        {
            BTNClose.onClick.RemoveAllListeners();
            BTNClose.onClick.AddListener(Close);
        }

        _status = BingoStatus.Idle;

        GameHelper.UITransition_FadeIn(FadeIn.gameObject, () => PopupUIManager.Instance.Show_SoftLoad(false));
    }

    private void OnBingoButtonClick(string key, BingoButtonType type)
    {
        if (type == BingoButtonType.RewardButton)
        {
            if (_rewardList.ContainsKey(key))
            {
                PopupUIManager.Instance.ShowPopup_PreviewReward(
                    LocalizationManager.Instance.GetText("EVENT_REWARD_TITLE_" + _eventKey)
                    , LocalizationManager.Instance.GetText("EVENT_REWARD_DETAIL_" + _eventKey)
                    , _rewardList[key]
                );
            }
        }
        else
        {
            if (!_isOnCooldown)
            {
                //TO DO: Check ticket.
                if (PlayTriggerType == GameEventStartType.Ticket)
                {
                    if(_playerTicket < RequireTicket[0])
                        PopupUIManager.Instance.ShowPopup_Error(LocalizationManager.Instance.GetText("EVENT_ERROR"),
                                                         LocalizationManager.Instance.GetText("EVENT_NO_TICKET_DETAIL_" + _eventKey));
                    return;
                }

                if (_claimedSlots.Contains(key))
                {
                    PopupUIManager.Instance.ShowPopup_Error(LocalizationManager.Instance.GetText("EVENT_ALREADY_CLAIMED_TITLE_" + _eventKey),
                                                            LocalizationManager.Instance.GetText("EVENT_ALREADY_CLAIMED_DETAIL_" + _eventKey));
                    return;
                }
                if (_remainPlaytime <= 0)
                {
                    PopupUIManager.Instance.ShowPopup_Error(LocalizationManager.Instance.GetText(GameEventTemplateKeys.NoRemainTryTitleKey),
                        LocalizationManager.Instance.GetText(GameEventTemplateKeys.NoRemainTryMessageKey));
                    return;
                }
                            
                PopupUIManager.Instance.ShowPopup_SureCheck(LocalizationManager.Instance.GetText("EVENT_OPEN_CONF_TITLE_" + _eventKey),
                                                            LocalizationManager.Instance.GetText("EVENT_OPEN_CONF_DETAIL_" + _eventKey),
                                                            delegate ()
                                                            {
                                                                OpenBingoSlot(key);
                                                            },
                                                            null);
                
            }
            else
            {
                PopupUIManager.Instance.ShowPopup_Error(LocalizationManager.Instance.GetText("EVENT_COOLDOWN"),
                                                        LocalizationManager.Instance.GetText("EVENT_ONCOOLDOWN"));
            }
        }
    }

    private void OpenBingoSlot(string key)
    {
        if (PlayTriggerType == GameEventStartType.Ticket)
        {
            if (_playerTicket >= RequireTicket[0])
            {
                PopupUIManager.Instance.Show_SoftLoad(true);

                _bingoBtnList[key].SetClaimed();
                _bingoBtnList[key].PlayStampAnimation();

                _playerTicket -= RequireTicket[0];
                SetTicketAmount(_playerTicket, RequireTicket[0]);

                DataManager.Instance.ExcecuteEventCloudScript("OpenBingoSlot", new { EventKey = _eventKey, Index = key, TicketID = _ticketID }, OnSuccessOpenSlot, OnCommonError);

                SetUIImage(IMGSDCharacter, AssetName.SD_Stamp1.ToString());

                _status = BingoStatus.Processing;
                _claimedSlots.Add(key);
            }
        }
        else
        {
            PopupUIManager.Instance.Show_SoftLoad(true);
            _bingoBtnList[key].SetClaimed();
            _bingoBtnList[key].PlayStampAnimation();

            DataManager.Instance.ExcecuteEventCloudScript("OpenBingoSlot", new { EventKey = _eventKey, Index = key, TicketID = string.Empty }, OnSuccessOpenSlot, OnCommonError);

            SetUIImage(IMGSDCharacter, AssetName.SD_Stamp1.ToString());

            _status = BingoStatus.Processing;
            _claimedSlots.Add(key);
        }
    }

    private void OnSuccessOpenSlot(CloudScriptResultProcess csResult)
    {
        bool bingo = false;
        List<ItemData> combinedList = new List<ItemData>();

        combinedList.AddRange(_rewardList[_participateRewardID]); //Participation reward will always granted to player whenever s/he open bingo slot;
        foreach (KeyValuePair<string, List<ItemData>> item in _rewardList)
        {
            if (!_claimedSlots.Contains(item.Key))
            {
                if (_slotRewardCond.ContainsKey(item.Key))
                {
                    bool allCompleted = true;

                    foreach (string cond in _slotRewardCond[item.Key])
                    {
                        if (!_claimedSlots.Contains(cond))
                        {
                            allCompleted = false;
                            break;
                        }
                    }

                    if (allCompleted)
                    {
                        bingo = true;

                        combinedList.AddRange(_rewardList[item.Key]);
                        _claimedSlots.Add(item.Key);

                        _bingoBtnList[item.Key].PlayStampAnimation();
                    }
                }
            }
        }

        UnityAction onComplete = delegate ()
        {
            _remainPlaytime--;
            _status = BingoStatus.Idle;
            PopupUIManager.Instance.Show_SoftLoad(false);
            
            ShowRewardPopup(combinedList);

            Dictionary<string, object> rawPlayerData = JsonConvert.DeserializeObject<Dictionary<string, object>>(csResult.CustomData["player_data"]);
            UpdatePlayerData(rawPlayerData);
            
            SetUIImage(IMGSDCharacter, AssetName.SD_Bingo.ToString());

            UpdateUI();
        };

        UnityAction onCompleteWithBingo = delegate ()
        {
            SetUIImage(IMGSDCharacter, AssetName.SD_Bingo.ToString());
            PopupUIManager.Instance.Show_SoftLoad(false);
            BingoAnim_FadeIn(onComplete);
        };

        if (csResult.CustomData.ContainsKey("messageValue"))
        {
            string resultMsg = csResult.CustomData["messageValue"].ToString();
            if (resultMsg.StartsWith("0"))
            {
                onComplete?.Invoke();
                OnCommonError(resultMsg);
                return;
            }
        }

        if (csResult.CustomData.ContainsKey("event_currency"))
        {
            //DataManager.Instance.InventoryData.SetEventCurrency(csResult.CustomData["event_currency"]);

            // TODO: Event currency and Item Data.

            bool isSuccess = GameHelper.TryDeserializeJSONStr(csResult.CustomData["item_data"], out Dictionary<string, int> ItemDataDict);
            if (isSuccess)
            {
                List<ItemData> itemDataList = DataManager.ConvertDictionaryToItemDataList(ItemDataDict);
                foreach (ItemData itemData in itemDataList)
                {
                    DataManager.Instance.InventoryData.GrantItemByItemID(itemData.ItemID, itemData.Amount);
                }
            }
        }
        if (!bingo)
        {
            DataManager.Instance.LoadInventory(onComplete, delegate (string errorMsg) { onComplete?.Invoke(); });
        }
        else
        {
            DataManager.Instance.LoadInventory(onCompleteWithBingo, delegate (string errorMsg) { onComplete?.Invoke(); });
        }

        //ANALYTIC
        bool allClear = true;
        foreach (KeyValuePair<string, List<string>> cList in _slotRewardCond)
        {
            if (cList.Key == _participateRewardID) continue;

            if (!_claimedSlots.Contains(cList.Key))
            {
                allClear = false;
                break;
            }
        }
        if(allClear)
        {
            OnEventComplete();
        }
    }

    private void ShowRewardPopup(List<ItemData> itemLists)
    {
        PopupUIManager.Instance.ShowPopup_GotRewardAnimated(LocalizationManager.Instance.GetText("EVENT_CONGRATS"),
                                                            LocalizationManager.Instance.GetText("EVENT_YOUVEGOT"), 
                                                            itemLists, delegate ()
                                                            {
                                                                SetUIImage(IMGSDCharacter, AssetName.SD_PreStamp.ToString());
                                                            });
        OnReceiveItem(itemLists);
    }

    private void BingoAnim_FadeIn(UnityAction onComplete)
    {
        float time = 0.5f;
        float delay = 0.75f;
        SoundManager.PlaySFX(SoundManager.SFX.SFX_RPS_VICTORY);
        IMGBingo.gameObject.SetActive(true);
        IMGBingo.transform.localScale = new Vector3(0f, 0f, 0f);
        IMGBingo.transform.DOScale(new Vector3(1.2f, 1.2f, 1.2f), time).OnComplete
            (
            delegate ()
            {
                //Just use DOScale to delay the onComplete func.
                IMGBingo.transform.DOScale(IMGBingo.transform.localScale, delay).OnComplete(delegate () { BingoAnim_FadeOut(onComplete); });
            }
            );
        
    }
    private void BingoAnim_FadeOut(UnityAction onComplete)
    {
        //Fade out code deleted due to designer's suggestion; (But I'm too lazy to change func name :P)
        //Only deactive gameObject code left.
        IMGBingo.gameObject.SetActive(false);
        onComplete?.Invoke();
    }

    private void InitUI()
    {
        UICanvas.SetActive(true);
        _boardButtonList = new List<GameObject>();
        _bingoBtnList = new Dictionary<string, EventBingoButton>();
        
        Sprite buttonBgSprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, AssetName.ButtonBackground.ToString()); ;
        Sprite stampSprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, AssetName.Stamp.ToString()); ;
        for (int x = 0; x < _colKey.Length; x++)
        {
            for (int y = 0; y < _rowKey.Length; y++)
            {
                string buttonKey = string.Format("{0}{1}", _colKey[x], _rowKey[y]);
                string rewAmt = "";
                BingoButtonType type;
                Sprite rewardSprite = null;
                if (x == 0 || y == 0)
                {
                    type = BingoButtonType.RewardButton;
                    if (_rewardList.ContainsKey(buttonKey))
                    {
                        List<ItemData> reward = _rewardList[buttonKey];
                        if (reward.Count > 0)
                        {
                            if (reward.Count == 1)
                            {
                                rewardSprite = SpriteResourceHelper.LoadSprite(reward[0].ItemID, SpriteResourceHelper.SpriteType.ICON_Item);
                                rewAmt = reward[0].Amount.ToString();
                            }
                            else
                            {
                                rewardSprite = SpriteResourceHelper.LoadSprite("IB_GIFT", SpriteResourceHelper.SpriteType.ICON_Item);
                            }
                        }
                    }
                }
                else
                {
                    type = BingoButtonType.BingoButton;
                }
                Debug.Log(string.Format("Creating Bingo Button : {0},{1} type of {2}", _colKey[x], _rowKey[y], type.ToString()));

                GameObject newButton = Instantiate(BingoButtonPrefab, BingoButtonPrefab.transform.parent);
                newButton.SetActive(true);
                newButton.name = "Button " + buttonKey;
                KaramuchoButtonUI btnComp = newButton.GetComponent<KaramuchoButtonUI>();
                btnComp.onClick.RemoveAllListeners();
                btnComp.onClick.AddListener(delegate() { OnBingoButtonClick(buttonKey, type); });
                EventBingoButton btnDataComp = newButton.GetComponent<EventBingoButton>();
                btnDataComp.InitData(buttonKey, type == BingoButtonType.RewardButton ? buttonBgSprite : null, stampSprite, rewardSprite, rewAmt, _claimedSlots.Contains(buttonKey));
                if (x == 0 && y == 0) btnDataComp.SetAsHidden(); //Hide the first button
                _bingoBtnList.Add(buttonKey, btnDataComp);
            }
        }

        SetEventText(TXTMoreDetail, LocalizationManager.Instance.GetText("EVENT_MORE_DETAIL_"+ _eventKey), _textColor, 255, true);
        SetEventText(TXTMoreDetailHeader, LocalizationManager.Instance.GetText("EVENT_DETAIL_HEADER"), "000000", 255, true);
        SetEventText(TXTEventName, LocalizationManager.Instance.GetText("EVENT_HEADER_" + _eventKey), _textColor, 255, true);
        SetEventText(TXTClaimClearReward, LocalizationManager.Instance.GetText("EVENT_CLAIM_" + _eventKey), _textColor, 255, true);
        SetEventText(TXTObjective, LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventObjectiveKey + _eventKey), "32746C", 255, true);

        SetUIImage(IMGBackgroundImage, AssetName.EventBackground.ToString());
        SetUIImage(IMGHeaderBanner, AssetName.HeaderBanner.ToString());
        SetUIImage(IMGGrid, AssetName.GridBackground.ToString());
        SetUIImage(IMGBTNClearReward, AssetName.ClearRewardClaimButton.ToString());
        SetUIImage(IMGClearReward, AssetName.ClearRewardImage.ToString());
        SetUIImage(IMGBingo, AssetName.Bingo.ToString());
        SetUIImage(IMGClearRewardStamp, AssetName.Stamp.ToString());
        SetUIImage(IMGSDCharacter, AssetName.SD_PreStamp.ToString());

        if (PlayTriggerType == GameEventStartType.Ticket)
        {
            SetUIImage(IMGTicket, AssetName.Ticket.ToString());
            SetUIImage(IMGTicketBox, AssetName.TicketBox.ToString());
        }

        StartCooldownTimer();
    }

    private void UpdateUI()
    {
        foreach (string key in _claimedSlots)
        {
            if (_bingoBtnList.ContainsKey(key))
            {
                _bingoBtnList[key].SetClaimed();
            }
        }

        if(_claimedSlots.Contains(_clearRewardID))
        {
            IMGClearRewardStamp.gameObject.SetActive(true);
            SetEventText(TXTClaimClearReward, LocalizationManager.Instance.GetText("EVENT_CLAIMED_" + _eventKey), _textColor, 255, true);
        }

        if(PlayTriggerType == GameEventStartType.Ticket)
            _playerTicket = DataManager.Instance.InventoryData.GetAmountByItemID(_ticketID);
        StartCooldownTimer();
    }

    private void StartCooldownTimer()
    {
        if (!_isOnCooldown)
        {
            _isOnCooldown = true;

            _cdTimer = StartCoroutine(CooldownTimer(
                  _playerLastTimeStamp
                , DefaultCooldown
                , CooldownText
                , delegate ()
                {
                    _isOnCooldown = false;
                    _cdTimer = null;
                    if(PlayTriggerType == GameEventStartType.Ticket)
                        SetTicketAmount(_playerTicket, RequireTicket[0]);
                }
            ));
        }
        else
        {
            Debug.LogWarningFormat("StartCooldownTimer: already start coroutine.");
        }
    }

    private void SetTicketAmount(int currentTicket, int requireTicket)
    {
        CooldownText.text = string.Format("{0}/{1}", currentTicket, requireTicket);
    }

    private void SetUIImage(Image uiImage, string assetName)
    {
        Sprite spr = SpriteResourceHelper.LoadSubEventImage(_eventKey, assetName, true);

        uiImage.enabled = (spr != null);
        uiImage.sprite = spr;
    }

    private void SetEventText(TextMeshProUGUI txt, string txtString, string coloCode, int alpha, bool completeString = false)
    {
        if (txt == null) return;

        if (completeString)
            txt.text = txtString;
        else
            txt.text = LocalizationManager.Instance.GetText(txtString);

        txt.color = CodeToColor(coloCode, alpha);
    }

    Color CodeToColor(string colorCode, int alpha)
    {
        int red = Convert.ToInt32(colorCode.Substring(0, 2), 16);
        int blue = Convert.ToInt32(colorCode.Substring(2, 2), 16);
        int green = Convert.ToInt32(colorCode.Substring(4, 2), 16);
        Color c = new Color32(Convert.ToByte(red), Convert.ToByte(blue), Convert.ToByte(green), 255);
        return c;
    }

    private void UpdatePlayerData(Dictionary<string, object> rawData)
    {
        //Init player data;
        List<object> rawClaimedSlot = JsonConvert.DeserializeObject<List<object>>(rawData["claimed_slots"].ToString());
        _claimedSlots = new List<string>();
        foreach (object cSlot in rawClaimedSlot)
        {
            _claimedSlots.Add(cSlot.ToString());
        }

        _playerLastTimeStamp = (DateTime)rawData["timestamp"];
        _remainPlaytime = int.Parse(rawData["remain_playtime"].ToString());
        //_playerDailyTimestamp = (DateTime)rawData["daily_timestamp"];
    }

    private void ShowEventDetail()
    {
        string title = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventDetailHeaderKey);
        string message = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventDetailKey + _eventKey);

        PopupUIManager.Instance.ShowPopup_DetailWindow(title, message);
    }
}
