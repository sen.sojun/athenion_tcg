﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using Karamucho;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[Obsolete]
public class GameEventTemplatePuzzleMap : BaseGameEventTemplate
{
    #region Static Value

    #region Cloud Script Key

    private static readonly string LoadEventDataFunctionName = "GetPuzzleMapPlayerData";                    // CloudScript function for getting player data.
    private static readonly string UpdateEventDataFunctionName = "OpenPuzzleMap";                           // CloudScript function for opening puzzle map.

    private static readonly string GridDataKey = "grid_data";
    private static readonly string TotalCooldownTicketKey = "total_cooldown_tickets";
    private static readonly string UsedCooldownTicketKey = "used_cooldown_tickets";
    private static readonly string OpenedGridKey = "opened_grids";

    private static readonly string RewardsKey = "rewards";
    private static readonly string RewardKeyKey = "reward_key";
    private static readonly string EventPuzzleMapsKey = "puzzle_maps";
    private static readonly string EventPuzzleIDKey = "puzzle_ID";
    private static readonly string EventPuzzleReleaseDate = "release_date";
    private static readonly string EventPuzzleEndDate = "end_date";

    private static readonly string EventPlayerCurrentPuzzleID = "current_puzzle_ID";
    private static readonly string EventMapStartDate = "map_start_date";
    private static readonly string EventMapEndDate = "map_end_date";
    #endregion

    #region Localize Key

    private static readonly string EventGridHasClickedTitleKey = "EVENT_GRID_IS_CLICKED_TITLE";
    private static readonly string EventGridHasClickedMessageKey = "EVENT_GRID_IS_CLICKED_MESSAGE";
    private static readonly string EventConfirmOpenGridTitleKey = "EVENT_CONFIRM_OPEN_GRID_TITLE";
    private static readonly string EventConfirmOpenGridMessageKey = "EVENT_CONFIRM_OPEN_GRID_MESSAGE";

    private static readonly string EventErrorPuzzleMapIDTitleKey = "ERROR_PUZZLE_ID_INVALID_TITLE";
    private static readonly string EventErrorPuzzleMapIDMessageKey = "ERROR_PUZZLE_ID_INVALID_MESSAGE";

    #endregion

    #region Resource Key

    //Resource path
    private static readonly string EventSpriteResourcePath = "Events/{0}/Sprites/";

    //Prefix Key
    private static readonly string EventIconPrefix = "ICON_";
    private static readonly string GridSubBgPrefix = "GRID_";
    private static readonly string GridBgPrefix = "GRID_MAP_";
    private static readonly string GridSheet = "GRID_MAP_SHEET";

    //Full Key
    private static readonly string EventIconBgKey = "ICON_BG";
    private static readonly string EventRewardBgKey = "REWARD_BOX";
    private static readonly string EventRewardKey = "REWARD_IMG";
    private static readonly string EventPuzzleMapKey = "MAPBOX";
    private static readonly string EventPuzzleGridMapKey = "GRID_MAP";
    private static readonly string EventIconHighlightKey = "ICON_HIGHLIGHT";
    private static readonly string EventGridHighlightKey = "GRID_HIGHLIGHT";
    private static readonly string EventCharacterIdleKey = "CHAR_IDLE";
    private static readonly string EventCharacterFoundKey = "CHAR_FOUND";

    //Prefab
    private static readonly string ButtonPrefabName = "PuzzleButton";
    private static readonly string IconPrefabName = "IconGameObject";
    private static readonly string GridBackgroundPrefabName = "GridBackground";

    #endregion

    #endregion

    #region UI Properties

    [Header("Rect Transform UI Elements")]
    [SerializeField] private RectTransform GridBackgroundParentTransform;
    [SerializeField] private RectTransform ButtonParentTransform;
    [SerializeField] private RectTransform IconParentTransform;

    [Header("Static Image UI Element List")]
    [SerializeField] private List<Image> StaticNativeSizeImageList;
    [SerializeField] private List<Image> StaticNonNativeSizeImageList;
    [SerializeField] private Image EventMainBgImage;
    [SerializeField] private Image EventDetailImage;
    [SerializeField] private Image EventTicketImage;
    [SerializeField] private Image EventTicketBgImage;
    [SerializeField] private Image EventMapImage;
    [SerializeField] private Image EventGridImage;
    [SerializeField] private Image EventHeaderBgImage;
    [SerializeField] private Image EventReadMoreImage;
    [SerializeField] private Image EventCloseImage;

    [Header("Dynamic Image UI Element List")]
    [SerializeField] private Image EventCharacterImage;

    [Header("Text UI Elements")]
    [SerializeField] private TextMeshProUGUI EventHeaderText;
    [SerializeField] private TextMeshProUGUI EventObjectiveText;
    [SerializeField] private TextMeshProUGUI EventDetailText;
    [SerializeField] private TextMeshProUGUI EventTicketText;
    [SerializeField] private TextMeshProUGUI EventDurationText;
    [SerializeField] private TextMeshProUGUI EventPlayTimeText;
    [SerializeField] private TextMeshProUGUI EventCooldownText;
    [SerializeField] private List<TextDictionary> EventFoundIconTextList;

    public delegate void GameEventAction();
    public static event GameEventAction OnDisableAction;
    public static event GameEventAction OnEnableAction;

    [Header("Button UI Elements")]
    [SerializeField] private Button EventCloseButton;
    [SerializeField] private Button ReadMoreButton;

    [Header("Sound Setting")]
    [SerializeField] private SoundManager.SFX RewardDefaultSound;
    [SerializeField] private SoundManager.SFX NoneRewardDefaultSound;

    [Header("Scene Setting")]
    [SerializeField] private bool IsButtonGrayScale;
    [SerializeField] private bool IsButtonBgGrayScale;
    [SerializeField] private Transform _participateParentTransform;

    #endregion

    #region Private Properties

    private int _cooldownTicketCount;
    private int _currentTotalDays;
    private int _totalDaysLeft;
    private int _remainPlayTime;

    private DateTime _eventReleaseDateTime;
    private DateTime _eventEndDateTime;
    private DateTime _lastActionTimeStamp;

    private List<int> _openedGrid;

    private List<string> _gridIconList;
    private List<string> _foundGridIconList;

    private ItemData _ticket;                                                       // Ticket.
    private List<ItemData> _participateRewardList;                                  // Participate reward.
    private Dictionary<string, List<ItemData>> _sumRewardDictionary;
    private Dictionary<string, List<ItemData>> _rewardsDictionary;                  // Reward dictionary.

    private Dictionary<string, PuzzleEventIconItem> _iconList;                      // Icon list.
    private Dictionary<string, int> _gridTotalIconCountDictionary;
    private Dictionary<string, int> _gridFoundIconCountDictionary;

    private List<PuzzleGridBackgroundItem> _puzzleGridBackgroundItemList;

    private bool _isGameReady;

    #endregion

    #region Methods

    #region Unity Callback Methods

    public void OnDisable()
    {
        OnDisableAction = null;
        OnEnableAction = null;
    }

    #endregion

    #region Override Methods

    public override void Init(string eventKey, string sceneName, UnityAction onComplete = null, UnityAction onFail = null)
    {
        PopupUIManager.Instance.Show_SoftLoad(true);

        base.Init(eventKey, sceneName);

        DataManager.Instance.ExcecuteEventCloudScript(
            LoadEventDataFunctionName
            , new
            {
                EventKey = _eventKey
            }
            , (result) =>
            {
                bool isSuccess = result.IsSuccess;

                if (!isSuccess)
                {
                    InvalidCheck(result.MessageDetail);
                    return;
                }

                // Data Initialize.
                isSuccess &= InitData(result);

                if (isSuccess)
                {
                    // UI Initialize.
                    InitUI();
                    GenerateButtonUI();
                    GenerateIconList();
                }

                PopupUIManager.Instance.Show_SoftLoad(false);
            }
            , delegate(string error)
            {
                PopupUIManager.Instance.Show_SoftLoad(false);
            }
        );
    }

    #endregion

    #region Initialize Methods

    /// <summary>
    /// Call this method to initialize data from cloud script.
    /// </summary>
    /// <param name="eventData">Event data.</param>
    protected override bool InitData(CloudScriptResultProcess eventData)
    {
        bool isSuccess = true;

        #region Base Event Data

        isSuccess &= base.InitData(eventData);

        if (!isSuccess)
            return false;

        #endregion

        #region Event Date Data

        if (PlayTriggerType == GameEventStartType.CooldownTicket)
        {
            isSuccess &= GameHelper.TryDeserializeJSONStr(eventData.CustomData[GameEventTemplateKeys.EventReleaseDateKey], out _eventReleaseDateTime);
            isSuccess &= GameHelper.TryDeserializeJSONStr(eventData.CustomData[GameEventTemplateKeys.EventEndDateKey], out _eventEndDateTime);

            if (!isSuccess) return false;

            _currentTotalDays = (int)(DateTimeData.GetDateTimeUTC() - _eventReleaseDateTime).TotalDays + 1;
            _totalDaysLeft = (int)(_eventEndDateTime - DateTimeData.GetDateTimeUTC()).TotalDays;

            if (_totalDaysLeft <= 0)
                _totalDaysLeft = 0;
        }
        else if (PlayTriggerType == GameEventStartType.Ticket)
        {
            isSuccess &= GameHelper.TryDeserializeJSONStr(eventData.CustomData[EventMapStartDate], out _eventReleaseDateTime);
            isSuccess &= GameHelper.TryDeserializeJSONStr(eventData.CustomData[EventMapEndDate], out _eventEndDateTime);

            if (!isSuccess) return false;
        }

        #endregion

        #region Reward Data

        _rewardsDictionary = new Dictionary<string, List<ItemData>>();

        isSuccess &= GameHelper.TryDeserializeJSONStr(EventDataDict[RewardsKey].ToString(), out List<object> rewardsList);
        if (isSuccess)
        {
            for (int i = 0; i < rewardsList.Count; i++)
            {
                isSuccess &= GameHelper.TryDeserializeJSONStr(rewardsList[i].ToString(), out Dictionary<string, object> rewardDictionary);

                if (isSuccess)
                {
                    isSuccess &= rewardDictionary.ContainsKey(RewardKeyKey);
                    isSuccess &= rewardDictionary.ContainsKey(GameEventTemplateKeys.RewardListKey);
                    if (!isSuccess) return false;

                    isSuccess &= GameHelper.TryDeserializeJSONStr(rewardDictionary[GameEventTemplateKeys.RewardListKey].ToString(),
                        out List<object> itemList);
                    if (!isSuccess) return false;

                    List<ItemData> itemDataList = new List<ItemData>();
                    for (int j = 0; j < itemList.Count; j++)
                    {
                        isSuccess &= GameHelper.TryDeserializeJSONStr(itemList[j].ToString(),
                            out Dictionary<string, object> item);
                        if (!isSuccess) return false;

                        itemDataList.Add(new ItemData(
                            item[GameEventTemplateKeys.ItemIDKey].ToString()
                            , int.Parse(item[GameEventTemplateKeys.ItemAmountKey].ToString()))
                        );
                    }

                    _rewardsDictionary.Add(rewardDictionary[RewardKeyKey].ToString(), itemDataList);
                }
                else return false;
            }
        }
        else return false;

        _participateRewardList = new List<ItemData>();

        isSuccess &= GameHelper.TryDeserializeJSONStr(EventDataDict[GameEventTemplateKeys.ParticipateRewardListKey].ToString(), out List<object> participateRewardList);
        if (isSuccess)
        {
            for (int i = 0; i < participateRewardList.Count; i++)
            {
                isSuccess &= GameHelper.TryDeserializeJSONStr(participateRewardList[i].ToString(), out Dictionary<string, object> rewardDictionary);
                if (!isSuccess) return false;

                isSuccess &= rewardDictionary.ContainsKey(GameEventTemplateKeys.ItemIDKey);
                isSuccess &= rewardDictionary.ContainsKey(GameEventTemplateKeys.ItemAmountKey);

                if (!isSuccess) return false;

                _participateRewardList.Add(new ItemData(
                    rewardDictionary[GameEventTemplateKeys.ItemIDKey].ToString()
                    , int.Parse(rewardDictionary[GameEventTemplateKeys.ItemAmountKey].ToString()))
                );
            }
        }

        _sumRewardDictionary = new Dictionary<string, List<ItemData>>();
        foreach (var reward in _rewardsDictionary)
        {
            _sumRewardDictionary.Add(reward.Key, SumRewardData(reward.Value, _participateRewardList));
        }
        #endregion

        #region Grid Data

        isSuccess &= GameHelper.TryDeserializeJSONStr(eventData.CustomData[GridDataKey], out _gridIconList);
        if (!isSuccess) return false;

        _gridTotalIconCountDictionary = new Dictionary<string, int>();

        foreach (string iconName in _gridIconList)
        {
            if (!_gridTotalIconCountDictionary.ContainsKey(iconName))
                _gridTotalIconCountDictionary.Add(iconName, 1);
            else
                _gridTotalIconCountDictionary[iconName]++;
        }

        #endregion

        #region Player and Additional Event Data

        // All player data.
        isSuccess &= GameHelper.TryDeserializeJSONStr(eventData.CustomData[GameEventTemplateKeys.PlayerDataKey], out Dictionary<string, object> playerDataDictionary);
        if (!isSuccess) return false;

        // Player grid data.
        isSuccess &= GameHelper.TryDeserializeJSONStr(playerDataDictionary[OpenedGridKey].ToString(), out _openedGrid);
        if (isSuccess && (_openedGrid.Count == 0 || _openedGrid == null))
            _openedGrid = new List<int>();

        _gridFoundIconCountDictionary = new Dictionary<string, int>();

        foreach (var key in _gridTotalIconCountDictionary.Keys)
        {
            _gridFoundIconCountDictionary.Add(key, 0);
        }

        foreach (int gridIndex in _openedGrid)
        {
            string iconName = _gridIconList[gridIndex];
            _gridFoundIconCountDictionary[iconName]++;
        }

        // Player remain playtime.
        isSuccess &= playerDataDictionary.ContainsKey(GameEventTemplateKeys.PlayerRemainPlaytimeKey);
        if (!isSuccess) return false;

        isSuccess &= int.TryParse(playerDataDictionary[GameEventTemplateKeys.PlayerRemainPlaytimeKey].ToString(), out _remainPlayTime);
        if (!isSuccess) return false;

        // Player previous action timestamp.
        isSuccess &= playerDataDictionary.ContainsKey(GameEventTemplateKeys.PlayerActionTimestampKey);
        if (!isSuccess) return false;

        isSuccess &= DateTime.TryParse(playerDataDictionary[GameEventTemplateKeys.PlayerActionTimestampKey].ToString(), out _lastActionTimeStamp);
        if (!isSuccess) return false;

        switch (PlayTriggerType)
        {
            case GameEventStartType.CooldownTicket:
                isSuccess &= GameHelper.TryDeserializeJSONStr(playerDataDictionary[TotalCooldownTicketKey].ToString(), out int totalCooldownTicket);
                isSuccess &= GameHelper.TryDeserializeJSONStr(playerDataDictionary[UsedCooldownTicketKey].ToString(), out int usedCooldownTicket);

                if (!isSuccess) return false;
                _cooldownTicketCount = totalCooldownTicket - usedCooldownTicket;
                break;
            case GameEventStartType.Ticket:
            {
                string ticketID = GameEventTemplateKeys.TicketIDPrefixKey + _eventThemeInfo.Theme;
                int ticketAmount = DataManager.Instance.InventoryData.GetAmountByItemID(ticketID);

                _ticket = new ItemData(ticketID, ticketAmount);
                break;
            }
            case GameEventStartType.Cooldown:
                _isGameReady = false;
                break;
            
        }

        #endregion

        return true;
    }

    /// <summary>
    /// Call this method to initialize all game event graphic display (such as sprite).
    /// </summary>
    /// <param name="eventData">Event data.</param>
    private void InitUI()
    {
        #region Text UI
        
        EventHeaderText.text = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventHeaderKey + _eventKey);
        EventObjectiveText.text = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventObjectiveKey + _eventKey);
        EventDetailText.text = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventDetailKey + _eventKey);
        //EventDurationText.text = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventDurationKey + _eventKey, out string resultText) ? string.Format(resultText, _totalDaysLeft) : GameEventTemplateKeys.EventDurationKey + _eventKey;

        if (EventFoundIconTextList != null && EventFoundIconTextList.Count > 0)
        {
            foreach (TextDictionary textDictionary in EventFoundIconTextList)
            {
                if (_gridTotalIconCountDictionary.ContainsKey(textDictionary.Key))
                    textDictionary.Text.text = $"{_gridFoundIconCountDictionary[textDictionary.Key]}/{_gridTotalIconCountDictionary[textDictionary.Key]}";
            }
        }

        StartCoroutine(CountTimer());

        #endregion

        #region Static Image UI
        
        EventMainBgImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, _eventThemeInfo.Theme + GameEventTemplateKeys.EventMainBackgroundSuffix, true) != null 
            ? SpriteResourceHelper.LoadSubEventImage(_eventKey, _eventThemeInfo.Theme + GameEventTemplateKeys.EventMainBackgroundSuffix) 
            : SpriteResourceHelper.LoadSprite(_eventThemeInfo.Theme, SpriteResourceHelper.SpriteType.GAME_Event);

        EventMapImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, EventPuzzleMapKey);
        EventGridImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, EventPuzzleGridMapKey);
        EventHeaderBgImage.sprite = SpriteResourceHelper.LoadSprite(_eventThemeInfo.Theme + GameEventTemplateKeys.EventHeaderBannerSuffix, SpriteResourceHelper.SpriteType.GAME_Event);

        EventMainBgImage.enabled = true;
        EventMapImage.enabled = true;
        EventGridImage.enabled = true;
        EventHeaderBgImage.enabled = true;

        EventReadMoreImage.gameObject.SetActive(true);
        EventCloseImage.gameObject.SetActive(true);

        if (StaticNativeSizeImageList != null)
        {
            for (int i = 0; i < StaticNativeSizeImageList.Count; i++)
            {
                StaticNativeSizeImageList[i].sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, GameEventTemplateKeys.StaticNativeImagePrefixKey + i);
                StaticNativeSizeImageList[i].SetNativeSize();
                StaticNativeSizeImageList[i].enabled = true;
            }
        }

        if (StaticNonNativeSizeImageList != null)
        {
            for (int i = 0; i < StaticNonNativeSizeImageList.Count; i++)
            {
                StaticNonNativeSizeImageList[i].sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, GameEventTemplateKeys.StaticNonNativeImagePrefixKey + i);
                StaticNonNativeSizeImageList[i].enabled = true;
            }
        }

        #endregion

        #region Dynamic Image UI

        if (EventCharacterImage.gameObject.activeInHierarchy)
        {
            EventCharacterImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, EventCharacterIdleKey);
            EventCharacterImage.enabled = true;
        }

        #endregion

        #region Other UI

        switch (PlayTriggerType)
        {
            case GameEventStartType.Ticket:
                InitTicketUI();
                break;
            case GameEventStartType.CooldownTicket:
                InitCooldownTicketUI();
                break;
            case GameEventStartType.Cooldown:
                InitCooldownUI();
                break;
            case GameEventStartType.Daily:
                InitDailyUI();
                break;
        }

        #endregion
    }

    /// <summary>
    /// Call this method to assign cooldown ticket related value into UI elements.
    /// </summary>
    private void InitCooldownTicketUI()
    {
        EventTicketText.text = _cooldownTicketCount.ToString();

        EventTicketImage.sprite = SpriteResourceHelper.LoadSprite(GameEventTemplateKeys.TicketCooldownImagePrefixKey + _eventThemeInfo.Theme, SpriteResourceHelper.SpriteType.GAME_Event);
        EventTicketBgImage.sprite = SpriteResourceHelper.LoadSprite(GameEventTemplateKeys.TicketCooldownBgImagePrefixKey + _eventThemeInfo.Theme, SpriteResourceHelper.SpriteType.GAME_Event);
        EventTicketImage.enabled = true;
        EventTicketBgImage.enabled = true;
    }

    /// <summary>
    /// Call this method to assign ticket related value into UI elements.
    /// </summary>
    private void InitTicketUI()
    {
        // Text UI
        EventTicketText.text = _ticket.Amount + "/" + RequireTicket[0];

        // Image UI
        EventTicketImage.sprite = SpriteResourceHelper.LoadSprite(GameEventTemplateKeys.TicketIconImagePrefixKey + _eventThemeInfo.Theme, SpriteResourceHelper.SpriteType.ICON_Item);
        EventTicketBgImage.sprite = SpriteResourceHelper.LoadSprite(GameEventTemplateKeys.TicketBgImagePrefixKey + _eventThemeInfo.Theme, SpriteResourceHelper.SpriteType.GAME_Event);
        EventTicketImage.enabled = true;
        EventTicketBgImage.enabled = true;
    }

    /// <summary>
    /// Call this method to assign cooldown related value into UI elements.
    /// </summary>
    private void InitCooldownUI()
    {
        // Text UI
        EventPlayTimeText.text = string.Empty;

        StartCoroutine(CooldownTimer(_lastActionTimeStamp, DefaultCooldown, EventCooldownText, delegate
            {
                EventCooldownText.text = string.Empty;
                EventPlayTimeText.text = $"{_remainPlayTime}/{DefaultPlaytime}";
                _isGameReady = true;
            })
        );
    }

    /// <summary>
    /// Call this method to assign daily related value into UI elements
    /// </summary>
    private void InitDailyUI()
    {
        // Text UI
        EventCooldownText.text = string.Empty;
        EventPlayTimeText.text = $"{_remainPlayTime}/{DefaultPlaytime}";

        if (_remainPlayTime <= 0)
            _isGameReady = false;
    }

    /// <summary>
    /// Call this method to generate all action buttons.
    /// </summary>
    /// <param name="eventKey">Event key.</param>
    private void GenerateButtonUI()
    {
        _puzzleGridBackgroundItemList = new List<PuzzleGridBackgroundItem>();

        // Create dictionary of image.
        Dictionary<string, Sprite> imageResourcePathDic = new Dictionary<string, Sprite>
        {
            {PuzzleButtonItemEnum.GridImage.ToString(), null}
            , {PuzzleButtonItemEnum.IconImage.ToString(), null}
            , {PuzzleButtonItemEnum.IconHighlightImage.ToString(), null}
            , {PuzzleButtonItemEnum.GridHighlightImage.ToString(), SpriteResourceHelper.LoadSubEventImage(_eventKey, EventGridHighlightKey)}
        };

        // Get button and grid prefab.
        GameObject buttonPrefab = ResourceManager.Load(GameEventTemplateKeys.EventPrefabResourcePath + ButtonPrefabName) as GameObject;
        GameObject gridBackGroundPrefab = ResourceManager.Load(GameEventTemplateKeys.EventPrefabResourcePath + GridBackgroundPrefabName) as GameObject;

        // Load sprite sheet.
        Sprite[] gridBgSprites = ResourceManager.LoadAll<Sprite>(string.Format(EventSpriteResourcePath, _eventKey) + GridSheet);
        Sprite iconHighlightSprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, EventIconHighlightKey, true);

        for (int i = 0; i < _gridIconList.Count; i++)
        {
            GameObject spawnButtonPrefab = Instantiate(buttonPrefab, ButtonParentTransform);
            GameObject spawnGridBackgroundPrefab = Instantiate(gridBackGroundPrefab, GridBackgroundParentTransform);

            // Assign Button Prefab and Script.
            PuzzleButtonItem buttonScript = spawnButtonPrefab.GetComponent<PuzzleButtonItem>();

            if (buttonScript != null)
            {
                // Declare sound variable. 
                SoundManager.SFX selectedSound;

                if (_gridIconList[i].ToUpper() != "NONE" && !string.IsNullOrEmpty(_gridIconList[i]))
                {
                    imageResourcePathDic[PuzzleButtonItemEnum.IconHighlightImage.ToString()] = iconHighlightSprite;
                    selectedSound = RewardDefaultSound;
                }
                else
                {
                    imageResourcePathDic[PuzzleButtonItemEnum.IconHighlightImage.ToString()] = null;
                    selectedSound = NoneRewardDefaultSound;
                }

                imageResourcePathDic[PuzzleButtonItemEnum.GridImage.ToString()] = SpriteResourceHelper.LoadSubEventImage(_eventKey, GridSubBgPrefix + i, true);
                imageResourcePathDic[PuzzleButtonItemEnum.IconImage.ToString()] = SpriteResourceHelper.LoadSubEventImage(_eventKey, _gridIconList[i], true);

                buttonScript.Init(imageResourcePathDic, i, this, selectedSound, _openedGrid.Contains(i), IsButtonGrayScale);
            }

            // Assign Grid Background Prefab and Script.
            PuzzleGridBackgroundItem gridBgScript = spawnGridBackgroundPrefab.GetComponent<PuzzleGridBackgroundItem>();

            if (gridBgScript != null)
            {
                gridBgScript.Init(
                    gridBgSprites.Length <= 0
                        ? SpriteResourceHelper.LoadSubEventImage(_eventKey, "")
                        : gridBgSprites[i]
                    , _openedGrid.Contains(i)
                    , IsButtonBgGrayScale
                );
                _puzzleGridBackgroundItemList.Add(gridBgScript);
            }
        }
    }

    /// <summary>
    /// Call this method to generate all icon game object.
    /// </summary>
    /// <param name="eventKey"></param>
    private void GenerateIconList()
    {
        _iconList = new Dictionary<string, PuzzleEventIconItem>();

        if (!IconParentTransform.gameObject.activeInHierarchy)
            return;

        string prefabPath = GameEventTemplateKeys.EventPrefabResourcePath + IconPrefabName;
        GameObject item = ResourceManager.Load(prefabPath) as GameObject;

        for (int i = 0; i < _rewardsDictionary.Count; i++)
        {
            PuzzleEventIconItem script = Instantiate(item, IconParentTransform).GetComponent<PuzzleEventIconItem>();
            script.SetIconImage(
                SpriteResourceHelper.LoadSubEventImage(_eventKey, EventIconPrefix + i)                          
                , SpriteResourceHelper.LoadSubEventImage(_eventKey, EventIconBgKey)
                , _foundGridIconList.Contains(_rewardsDictionary.Keys.ElementAt(i))
            );
            _iconList.Add(_rewardsDictionary.Keys.ElementAt(i), script);
        }
    }

    #endregion

    #region Action Methods

    /// <summary>
    /// Call this method when player click on the grid button.
    /// </summary>
    /// <param name="caller">Caller's object.</param>
    /// <param name="actionIndex">Grid button index.</param>
    public void OnSelectAction(object caller, int actionIndex)
    {
        if (caller.GetType() != typeof(PuzzleButtonItem))
            return;

        PuzzleButtonItem buttonScript = (PuzzleButtonItem)caller;
        OnDisableAction?.Invoke();

        switch (PlayTriggerType)
        {
            // Cooldown ticket condition.
            case GameEventStartType.CooldownTicket when _cooldownTicketCount <= 0:
            {
                string noTicketTitle = LocalizationManager.Instance.GetText(GameEventTemplateKeys.NoTicketTitleKey);
                string noTicketMessage = LocalizationManager.Instance.GetText(GameEventTemplateKeys.NoTicketMessageKey);
                PopupUIManager.Instance.ShowPopup_Error(
                    noTicketTitle
                    , noTicketMessage
                    , delegate
                    {
                        OnEnableAction?.Invoke();
                        buttonScript.DeselectButton();
                    }
                );
                return;
            }

            // Event quest ticket condition.
            case GameEventStartType.Ticket when _ticket.Amount <= 0:
            {
                string noTicketTitle = LocalizationManager.Instance.GetText(GameEventTemplateKeys.NoTicketTitleKey);
                string noTicketMessage = LocalizationManager.Instance.GetText(GameEventTemplateKeys.NoTicketMessageKey);
                PopupUIManager.Instance.ShowPopup_Error(
                    noTicketTitle
                    , noTicketMessage
                    , delegate
                    {
                        OnEnableAction?.Invoke();
                        buttonScript.DeselectButton();
                    }
                );
                return;
            }

            // Cooldown condition.
            case GameEventStartType.Cooldown when !_isGameReady:
            {
                string noTicketTitle = LocalizationManager.Instance.GetText(GameEventTemplateKeys.CooldownTitleKey);
                string noTicketMessage = LocalizationManager.Instance.GetText(GameEventTemplateKeys.CooldownMessageKey);
                PopupUIManager.Instance.ShowPopup_Error(
                    noTicketTitle
                    , noTicketMessage
                    , delegate
                    {
                        OnEnableAction?.Invoke();
                        buttonScript.DeselectButton();
                    }
                );
                return;
            }

            case GameEventStartType.Cooldown when _remainPlayTime <= 0:
            {
                string noTicketTitle = LocalizationManager.Instance.GetText(GameEventTemplateKeys.NoRemainTryTitleKey);
                string noTicketMessage = LocalizationManager.Instance.GetText(GameEventTemplateKeys.NoRemainTryMessageKey);
                PopupUIManager.Instance.ShowPopup_Error(
                    noTicketTitle
                    , noTicketMessage
                    , delegate
                    {
                        OnEnableAction?.Invoke();
                        buttonScript.DeselectButton();
                    }
                );
                return;
            }

            case GameEventStartType.Daily when _remainPlayTime <= 0:
            {
                string noTicketTitle = LocalizationManager.Instance.GetText(GameEventTemplateKeys.NoRemainTryTitleKey);
                string noTicketMessage = LocalizationManager.Instance.GetText(GameEventTemplateKeys.NoRemainTryMessageKey);
                PopupUIManager.Instance.ShowPopup_Error(
                    noTicketTitle
                    , noTicketMessage
                    , delegate
                    {
                        OnEnableAction?.Invoke();
                        buttonScript.DeselectButton();
                    }
                );
                return;
            }
        }


        // Opened grid condition.
        if (_openedGrid.Contains(actionIndex))
        {
            string hasClickedTitle = LocalizationManager.Instance.GetText(EventGridHasClickedTitleKey);
            string hasClickedMessage = LocalizationManager.Instance.GetText(EventGridHasClickedMessageKey);
            PopupUIManager.Instance.ShowPopup_Error(
                hasClickedTitle
                , hasClickedMessage
                , delegate
                {
                    OnEnableAction?.Invoke();
                    buttonScript.DeselectButton();
                }
            );
            return;
        }

        string confirmGridOpenTitle = LocalizationManager.Instance.GetText(EventConfirmOpenGridTitleKey);
        string confirmGridOpenMessage = LocalizationManager.Instance.GetText(EventConfirmOpenGridMessageKey);
        string confirmYes = LocalizationManager.Instance.GetText("BUTTON_YES");
        string confirmNo = LocalizationManager.Instance.GetText("BUTTON_NO");
        bool isConfirmed = false;
        PopupUIManager.Instance.ShowPopup_TwoChoices(
            confirmGridOpenTitle
            , confirmGridOpenMessage
            , confirmYes
            , confirmNo
            , delegate
            {
                
                PopupUIManager.Instance.Show_SoftLoad(true);

                EventCloseButton.enabled = false;
                ReadMoreButton.enabled = false;

                if (isConfirmed)
                    return;
                isConfirmed = true;

                SoundManager.PlaySFX(SoundManager.SFX.SFX_RPS_BUBBLE_SPAWN);

                _puzzleGridBackgroundItemList[actionIndex].OnGridIsClicked();

                // TODO: Rework in progress
                buttonScript.OnSelectedButtonAction(delegate
                    {
                        // Icon Reward.
                        _openedGrid.Add(actionIndex);

                        // If Icon list is active in scene, update the list.
                        if (IconParentTransform.gameObject.activeInHierarchy)
                        {
                            if(_foundGridIconList == null)
                                _foundGridIconList = new List<string>();
                            _foundGridIconList.Add(_gridIconList[actionIndex]);
                            _iconList[_gridIconList[actionIndex]].OnFoundIcon();
                        }

                        // If trigger is ticket, update ticket id.
                        string ticketID = PlayTriggerType == GameEventStartType.Ticket ? _ticket.ItemID : string.Empty;

                        // Change character sprite.
                        if (EventCharacterImage.gameObject.activeInHierarchy)
                        {
                            if (_gridIconList[actionIndex] != "None")
                            {
                                EventCharacterImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, EventCharacterFoundKey);
                            }
                        }

                        DataManager.Instance.ExcecuteEventCloudScript(
                            UpdateEventDataFunctionName
                            , new
                            {
                                EventKey = _eventKey
                                ,GridIndex = actionIndex
                                ,TicketID = ticketID
                            }
                            , delegate (CloudScriptResultProcess eventData)
                            {
                                PopupUIManager.Instance.Show_SoftLoad(false);

                                if (!eventData.IsSuccess)
                                {
                                    switch (eventData.MessageDetail)
                                    {
                                        case "ERROR_PUZZLE_ID_INVALID":
                                            string puzzleIDInvalidTitle = LocalizationManager.Instance.GetText(EventErrorPuzzleMapIDTitleKey);
                                            string puzzleIDInvalidMessage = LocalizationManager.Instance.GetText(EventErrorPuzzleMapIDMessageKey);
                                            PopupUIManager.Instance.ShowPopup_ConfirmOnly(
                                                puzzleIDInvalidTitle
                                                , puzzleIDInvalidMessage
                                                , Close
                                            );
                                            return;
                                    }
                                }

                                //if (eventData.CustomData.ContainsKey(GameEventTemplateKeys.EventCurrencyKey))
                                //{
                                //    string ecJson = eventData.CustomData[GameEventTemplateKeys.EventCurrencyKey];
                                //    DataManager.Instance.InventoryData.SetEventCurrency(ecJson);
                                //}
                                //DataManager.Instance.LoadInventory(null, null);

                                // TODO: Event currency and Item Data.

                                bool isSuccess = GameHelper.TryDeserializeJSONStr(eventData.CustomData["item_data"], out Dictionary<string, int> ItemDataDict);
                                if (isSuccess)
                                {
                                    List<ItemData> itemDataList = DataManager.ConvertDictionaryToItemDataList(ItemDataDict);
                                    foreach (ItemData itemData in itemDataList)
                                    {
                                        DataManager.Instance.InventoryData.GrantItemByItemID(itemData.ItemID, itemData.Amount);
                                    }
                                }


                                //StartCoroutine(ActionRewardGenerate(
                                //    _participateParentTransform
                                //    , _participateRewardList
                                //    , false
                                //    , delegate
                                //    {

                                //    })
                                //);

                                if (_rewardsDictionary[_gridIconList[actionIndex]].Count > 0)
                                {
                                    string rewardTitle = LocalizationManager.Instance.GetText(GameEventTemplateKeys.NotiRewardTitleKey);
                                    string rewardMessage = LocalizationManager.Instance.GetText(GameEventTemplateKeys.NotiRewardMessageKey);

                                    PopupUIManager.Instance.ShowPopup_GotRewardAnimated(rewardTitle
                                            , rewardMessage
                                            , _sumRewardDictionary[_gridIconList[actionIndex]]
                                            , delegate
                                            {
                                                if (EventCharacterImage.gameObject.activeInHierarchy)
                                                    EventCharacterImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, EventCharacterIdleKey);
                                            }).SetAnimationEase(Ease.OutExpo)
                                                .SetDelay(0.1f)
                                                .SetAnimationDuration(0.25f)
                                                .SetMaxScale(8f);
                                }
                                else if (_rewardsDictionary[_gridIconList[actionIndex]].Count <= 0 &&
                                         _participateRewardList.Count > 0)
                                {
                                    string rewardTitle = LocalizationManager.Instance.GetText(GameEventTemplateKeys.NotiSmallRewardTitleKey);
                                    string rewardMessage = LocalizationManager.Instance.GetText(GameEventTemplateKeys.NotiSmallRewardMessageKey);

                                    PopupUIManager.Instance.ShowPopup_GotRewardAnimated(rewardTitle
                                            , rewardMessage
                                            , _participateRewardList
                                            , delegate
                                            {
                                                if (EventCharacterImage.gameObject.activeInHierarchy)
                                                    EventCharacterImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, EventCharacterIdleKey);
                                            }).SetAnimationEase(Ease.OutExpo)
                                        .SetDelay(0.1f)
                                        .SetAnimationDuration(0.25f)
                                        .SetMaxScale(8f);
                                }
                                else
                                {
                                    string noRewardTitle = LocalizationManager.Instance.GetText(GameEventTemplateKeys.NoRewardTitleKey);
                                    string noRewardMessage = LocalizationManager.Instance.GetText(GameEventTemplateKeys.NoRewardMessageKey);
                                    PopupUIManager.Instance.ShowPopup_Error(noRewardTitle, noRewardMessage);
                                }

                                OnPostActionSuccess(buttonScript, actionIndex);
                            },
                            delegate(string error)
                            {
                                EventCloseButton.enabled = true;
                                ReadMoreButton.enabled = true;
                                OnEnableAction?.Invoke();
                                buttonScript.DeselectButton();
                                Debug.LogError(error);
                                PopupUIManager.Instance.Show_SoftLoad(false);
                            });
                    }
                );

                //if (PlayTriggerType == GameEventStartType.CooldownTicket)
                //{
                //    _cooldownTicketCount--;
                //    EventTicketText.text = _cooldownTicketCount.ToString();
                //    if (_currentTotalDays >= _gridIconList.Count && _cooldownTicketCount == 0)
                //        OnEventComplete();
                //}
                //else if (PlayTriggerType == GameEventStartType.Ticket)
                //{
                //    _ticket = new ItemData(_ticket.ItemID, _ticket.Amount - RequireTicket[0]);
                //    EventTicketText.text = _ticket.Amount + "/" + RequireTicket[0];

                //    if (_openedGrid.Count == _gridIconList.Count)
                //        OnEventComplete();
                //}

            }
            , delegate
            {
                OnEnableAction?.Invoke();
                buttonScript.DeselectButton();
            }
        );
    }

    /// <summary>
    /// Call this method when the action is success.
    /// </summary>
    /// <param name="buttonScript">Puzzle button controller.</param>
    /// <param name="actionIndex">Puzzle button index.</param>
    public void OnPostActionSuccess(PuzzleButtonItem buttonScript, int actionIndex)
    {
        EventCloseButton.enabled = true;
        ReadMoreButton.enabled = true;

        OnEnableAction?.Invoke();
        OnReceiveItem(_rewardsDictionary[_gridIconList[actionIndex]]);
        buttonScript.DeselectButton();

        string iconName = _gridIconList[actionIndex];
        _gridFoundIconCountDictionary[iconName]++;

        if (EventFoundIconTextList != null && EventFoundIconTextList.Count > 0)
        {
            foreach (TextDictionary textDictionary in EventFoundIconTextList)
            {
                if (_gridTotalIconCountDictionary.ContainsKey(textDictionary.Key))
                    textDictionary.Text.text = $"{_gridFoundIconCountDictionary[textDictionary.Key]}/{_gridTotalIconCountDictionary[textDictionary.Key]}";
            }
        }

        switch (PlayTriggerType)
        {
            case GameEventStartType.Cooldown:
            {
                _remainPlayTime--;
                _lastActionTimeStamp = DateTimeData.GetDateTimeUTC();
                EventPlayTimeText.text = string.Empty;
                _isGameReady = false;
                InitCooldownUI();
            }
                break;
            case GameEventStartType.Ticket:
            {
                _ticket = new ItemData(_ticket.ItemID, _ticket.Amount - RequireTicket[0]);
                EventTicketText.text = _ticket.Amount + "/" + RequireTicket[0];

                if (_openedGrid.Count == _gridIconList.Count)
                    OnEventComplete();
            }
                break;
            case GameEventStartType.CooldownTicket:
            {
                _cooldownTicketCount--;
                EventTicketText.text = _cooldownTicketCount.ToString();
                if (_currentTotalDays >= _gridIconList.Count && _cooldownTicketCount == 0)
                    OnEventComplete();
                break;
            }
            case GameEventStartType.Daily:
            {
                _remainPlayTime--;
                _lastActionTimeStamp = DateTimeData.GetDateTimeUTC();
                InitDailyUI();
                break;
            }
        }
    }

    private IEnumerator CountTimer()
    {
        while (true)
        {
            TimeSpan time = _eventEndDateTime - DateTimeData.GetDateTimeUTC();
            if (time.Days > 0)
            {
                EventDurationText.text = string.Format(LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventDurationKey + _eventKey), time.Days, LocalizationManager.Instance.GetText("DAILY_DAY"));
            }
            else
            {
                EventDurationText.text = string.Format(LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventDurationKey + _eventKey), time.ToString(@"hh\:mm\:ss"), string.Empty);
            }

            yield return new WaitForSeconds(1);
        }
    }
    #endregion

    #region Invalid Methods

    private void InvalidCheck(string invalidMessage)
    {
        string errorTitle = string.Empty;
        string errorMessage = string.Empty;

        switch (invalidMessage)
        {
            case "ERROR_PUZZLE_MAP_TIME_INVALID":
                errorTitle = LocalizationManager.Instance.GetText("ERROR_PUZZLE_MAP_TIME_INVALID_TITLE");
                errorMessage = LocalizationManager.Instance.GetText("ERROR_PUZZLE_MAP_TIME_INVALID_MESSAGE");
                break;
            case "ERROR_GAME_EVENT_TIME_INVALID":
                errorTitle = LocalizationManager.Instance.GetText("");
                errorMessage = LocalizationManager.Instance.GetText("");
                break;
        }

        PopupUIManager.Instance.ShowPopup_Error(
            errorTitle
            , errorMessage
            , delegate
            {
                PopupUIManager.Instance.Show_SoftLoad(false);
                Close();
            }
        );
    }

    #endregion

    #region Other Methods

    /// <summary>
    /// Call this method to show read more.
    /// </summary>
    public void ShowReadMore()
    {
        EventDetailImage.gameObject.SetActive(true);
    }

    /// <summary>
    /// Call this method to hide read more
    /// </summary>
    public void HideReadMore()
    {
        EventDetailImage.gameObject.SetActive(false);
    }

    #endregion

    #endregion
}
