﻿using System;
using System.Collections;
using System.Collections.Generic;
using GameEvent;
using Karamucho;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class GameEventPuzzleMapUIManager : MonoBehaviour
{
    #region Static Value

    //Full Key
    private static readonly string EventGridHighlightKey = "GRID_HIGHLIGHT";
    private static readonly string EventPuzzleMapKey = "MAPBOX";
    private static readonly string EventPuzzleGridMapKey = "GRID_MAP";

    #endregion

    #region Inspector Properties

    [Header("Image UI Elements")]
    [SerializeField] private Image _EventHeaderBgImage;
    [SerializeField] private Image _EventMapImage;
    [SerializeField] private Image _EventGridImage;

    [Header("Text UI Elements")]
    [SerializeField] private TextMeshProUGUI _EventHeaderText;
    [SerializeField] private TextMeshProUGUI _EventObjectiveText;
    [SerializeField] private TextMeshProUGUI _EventPlayTimeText;
    [SerializeField] private TextMeshProUGUI _EventCooldownText;

    [Header("Button UI Elements")]
    [SerializeField] private Button _EventInfoButton;
    [SerializeField] private List<BasePuzzleMapButtonController> _PuzzleButtonList;

    [Header("Other UI Elements")]
    [SerializeField] private BasePuzzleSubResultController _PuzzleSubResultController;

    [Header("Sound Setting")]
    [SerializeField] private SoundManager.SFX _RewardDefaultSound;
    [SerializeField] private SoundManager.SFX _NoneRewardDefaultSound;

    #endregion

    #region Public Properties
    public bool IsCoolDown => _isCoolDown;
    public List<BasePuzzleMapButtonController> PuzzleButtonList => _PuzzleButtonList;
    public SoundManager.SFX RewardDefaultSound => _RewardDefaultSound;
    public SoundManager.SFX NoneRewardDefaultSound => _NoneRewardDefaultSound;
    #endregion

    #region Private Properties

    private List<PuzzleGridBackgroundItem> _puzzleGridBackgroundItemList;
    private GameEventPuzzleMapManager _manager;
    private Dictionary<string, PuzzleEventIconItem> _iconList;
    private bool _isCoolDown;

    #endregion

    #region Events

    public static event Action OnEnableAction;
    public static event Action OnDisableAction; 

    #endregion

    #region Methods

    #region Init Methods

    public void InitUi(GameEventPuzzleMapManager manager)
    {
        _manager = manager;
        PuzzleMapEventData eventInfoData = _manager.EventInfoData;
        PuzzleMapPlayerData playerData = _manager.PlayerData;
        string eventKey = eventInfoData.EventKey;

        #region Text UI

        if (_EventHeaderText != null) _EventHeaderText.text = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventHeaderKey + eventKey);
        if (_EventObjectiveText != null) _EventObjectiveText.text = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventObjectiveKey + eventKey);

        //if (_EventFoundIconTextList != null && _EventFoundIconTextList.Count > 0)
        //{
        //    foreach (TextDictionary textDictionary in _EventFoundIconTextList)
        //    {
        //        if (eventInfoData.GridTotalIconCountDictionary.ContainsKey(textDictionary.Key))
        //            textDictionary.Text.text = $"{playerData.GridFoundIconCountDict[textDictionary.Key]}/{eventInfoData.GridTotalIconCountDictionary[textDictionary.Key]}";
        //    }
        //}

        if (_EventCooldownText != null) RunCooldown();

        #endregion

        #region Image UI

        if (_EventHeaderBgImage != null) _EventHeaderBgImage.sprite = SpriteResourceHelper.LoadSprite(eventInfoData.EventThemeInfo.Theme + GameEventTemplateKeys.EventHeaderBannerSuffix, SpriteResourceHelper.SpriteType.GAME_Event);
        if (_EventMapImage != null) _EventMapImage.sprite = SpriteResourceHelper.LoadSubEventImage(eventKey, EventPuzzleMapKey);
        if (_EventGridImage != null) _EventGridImage.sprite = SpriteResourceHelper.LoadSubEventImage(eventKey, EventPuzzleGridMapKey);

        #endregion

        #region Button UI

        InitButtonUI();

        #endregion

        #region Other UI

        InitSubResult();

        #endregion
    }

    public void SetActionBtn(UnityAction<int> action)
    {
        if (_PuzzleButtonList == null || _PuzzleButtonList.Count <= 0) return;

        for (int i = 0; i < _PuzzleButtonList.Count; i++)
        {
            int index = i;
            _PuzzleButtonList[i].SetButtonAction(() =>
            {
                action?.Invoke(index);
            });
        }
    }

    public void SetEventInfoBtn(UnityAction action)
    {
        if (_EventInfoButton == null) return;
        _EventInfoButton.onClick.RemoveAllListeners();
        _EventInfoButton.onClick.AddListener(action);
    }

    private void InitButtonUI()
    {
        PuzzleMapEventData eventInfoData = _manager.EventInfoData;
        PuzzleMapPlayerData playerData = _manager.PlayerData;

        for (int i = 0; i < _PuzzleButtonList.Count; i++)
        {
            _PuzzleButtonList[i].Init(i, eventInfoData, playerData);
        }
    }

    private void InitSubResult()
    {
        PuzzleMapEventData eventInfoData = _manager.EventInfoData;
        PuzzleMapPlayerData playerData = _manager.PlayerData;

        if (_PuzzleSubResultController == null) return;
        _PuzzleSubResultController.Init(eventInfoData, playerData);
    }

    #endregion

    #region Built-in Methods

    private void OnDisable()
    {
        OnEnableAction = null;
        OnDisableAction = null;
    }

    #endregion

    #region Update Methods

    public void RunCooldown()
    {
        var eventInfoData = _manager.EventInfoData;
        var playerData = _manager.PlayerData;

        if (eventInfoData.PlayTriggerType == GameEventStartType.Cooldown && eventInfoData.DefaultCooldown > 0)
        {
            if (!_isCoolDown)
            {
                _isCoolDown = true;
                GameEventManager.Instance.RunRoutineTask(
                    _manager.CooldownTimer(
                        playerData.LastPlayDateTime
                        , eventInfoData.DefaultCooldown
                        , _EventCooldownText
                        , CoolDownEnd
                    )
                );
            }
        }
        else if (eventInfoData.PlayTriggerType == GameEventStartType.Cooldown
                 && (int)eventInfoData.DefaultCooldown == -1
                 && GameHelper.IsNewDayUTC(playerData.LastPlayDateTime) == false)
        {
            if (!_isCoolDown)
            {
                _isCoolDown = true;
                GameEventManager.Instance.RunRoutineTask(
                    _manager.NewDayTimer(
                        _EventCooldownText
                        , CoolDownEnd
                    )
                );
            }
        }
    }

    public void SetButtonsState(bool value)
    {
        if (value == true)
        {
            OnEnableAction?.Invoke();
            _EventInfoButton.enabled = true;
        }
        else
        {
            OnDisableAction?.Invoke();
            _EventInfoButton.enabled = false;
        }
    }

    public void OnActionEnd(int actionIndex, UnityAction onComplete)
    {
        PuzzleMapEventData eventInfoData = _manager.EventInfoData;
        PuzzleMapPlayerData playerData = _manager.PlayerData;

        if(_PuzzleSubResultController != null)
            _PuzzleSubResultController.UpdateResult(eventInfoData, playerData);

        _PuzzleButtonList[actionIndex].ActionEnd(eventInfoData, playerData, onComplete);
    }

    #endregion

    #region Result Methods

    private void CoolDownEnd()
    {
        // TODO: Implement when cooldown is done.
    }

    #endregion

    #endregion
}
