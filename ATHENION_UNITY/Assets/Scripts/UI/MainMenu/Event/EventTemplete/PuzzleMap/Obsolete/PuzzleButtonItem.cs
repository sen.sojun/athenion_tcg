﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using DG.Tweening;
using Karamucho;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public enum PuzzleButtonItemEnum
{
    GridImage = 0
    , IconImage
    , IconHighlightImage
    , GridHighlightImage
}


public class PuzzleButtonItem : MonoBehaviour
{
    #region Public Properties

    [Header("UI Elements")]
    public Image GridImage;
    public Image GridGrayScaleImage;
    public Image IconHighlightImage;
    public Image IconImage;
    public Image IconStaticImage;
    public Image GridHighlightImage;

    #endregion

    #region Private Properties

    private Button _button;
    private bool _isGrayScale;
    private SoundManager.SFX _finishSound;

    #endregion

    #region Methods

    /// <summary>
    /// Call this method to initialize this button.
    /// </summary>
    /// <param name="imageResourcePathDict">Dictionary of image resource path.</param>
    /// <param name="buttonIndex">Button index.</param>
    /// <param name="manager">Game event template of puzzle map.</param>
    /// <param name="finishSound"></param>
    /// <param name="isClicked">Is clicked status.</param>
    /// <param name="isGrayScale">Is greyscale active status.</param>
    public void Init(Dictionary<string, Sprite> imageResourcePathDict, int buttonIndex, GameEventTemplatePuzzleMap manager, SoundManager.SFX finishSound, bool isClicked, bool isGrayScale = true)
    {
        _isGrayScale = isGrayScale;
        _finishSound = finishSound;

        if (imageResourcePathDict[PuzzleButtonItemEnum.GridImage.ToString()] == null)
        {
            GridImage.enabled = false;
            GridGrayScaleImage.enabled = false;
        }
        else
        {
            GridImage.sprite = imageResourcePathDict[PuzzleButtonItemEnum.GridImage.ToString()];
            GridGrayScaleImage.sprite = imageResourcePathDict[PuzzleButtonItemEnum.GridImage.ToString()];
            GridImage.SetNativeSize();
            GridGrayScaleImage.SetNativeSize();

            if (!isGrayScale)
            {
                GridGrayScaleImage.enabled = false;
            }
        }

        if (imageResourcePathDict[PuzzleButtonItemEnum.IconImage.ToString()] == null)
        {
            IconImage.enabled = false;
            IconStaticImage.enabled = false;
        }
        else
        {
            IconImage.sprite = imageResourcePathDict[PuzzleButtonItemEnum.IconImage.ToString()];
            IconStaticImage.sprite = imageResourcePathDict[PuzzleButtonItemEnum.IconImage.ToString()];
            IconImage.SetNativeSize();
            IconStaticImage.SetNativeSize();
        }

        if (imageResourcePathDict[PuzzleButtonItemEnum.IconHighlightImage.ToString()] == null)
        {
            IconHighlightImage.enabled = false;
        }
        else
        {
            IconHighlightImage.sprite = imageResourcePathDict[PuzzleButtonItemEnum.IconHighlightImage.ToString()];
            IconHighlightImage.enabled = true;
            IconHighlightImage.gameObject.SetActive(false);
        }

        if (imageResourcePathDict[PuzzleButtonItemEnum.GridHighlightImage.ToString()] == null)
        {
            GridHighlightImage.sprite = null;
            GridHighlightImage.enabled = false;
        }
        else
        {
            GridHighlightImage.sprite = imageResourcePathDict[PuzzleButtonItemEnum.GridHighlightImage.ToString()];
            GridHighlightImage.enabled = true;
            //GridHighlightImage.SetNativeSize();
            GridHighlightImage.rectTransform.sizeDelta = GridImage.rectTransform.sizeDelta;
            GridHighlightImage.gameObject.SetActive(false);
        }

        _button = GetComponent<Button>();
        _button.onClick.AddListener(delegate
            {
                SoundManager.PlaySFX(SoundManager.SFX.SFX_RPS_BUBBLE_SPAWN);
                GridHighlightImage.gameObject.SetActive(true);
                manager?.OnSelectAction(this, buttonIndex);
            }
        );
        

        if (isClicked)
        {
            GridGrayScaleImage.gameObject.SetActive(false);

            if (IconStaticImage.sprite != null)
            {
                IconStaticImage.gameObject.SetActive(true);
                if (IconHighlightImage.sprite != null)
                    IconHighlightImage.gameObject.SetActive(true);
            }
        }

        GameEventTemplatePuzzleMap.OnEnableAction += EnableAction;
        GameEventTemplatePuzzleMap.OnDisableAction += DisableAction;
    }

    /// <summary>
    /// Call this method to disable this RPS button.
    /// </summary>
    public void DisableAction()
    {
        _button.enabled = false;
    }

    /// <summary>
    /// Call this method to enable this RPS button.
    /// </summary>
    public void EnableAction()
    {
        _button.enabled = true;
    }

    /// <summary>
    /// Call this method to execute this button action.
    /// </summary>
    public void OnSelectedButtonAction(UnityAction onComplete)
    {
        if (_isGrayScale)
        {
            GridGrayScaleImage.DOFade(0, 1).SetEase(Ease.Linear).onComplete += () =>
            {
                if (IconImage.sprite != null)
                {
                    IconImage.gameObject.SetActive(true);
                    IconImage.rectTransform.DOScale(Vector3.one, 0.2f).SetEase(Ease.Linear).onComplete += () =>
                    {
                        if (IconHighlightImage.sprite != null)
                            IconHighlightImage.gameObject.SetActive(true);

                        SoundManager.PlaySFX(_finishSound);
                        StartCoroutine(DelayInvoke(1, onComplete));
                    };
                }
                else
                {
                    SoundManager.PlaySFX(_finishSound);
                    onComplete?.Invoke();
                }
            };
        }
        else
        {
            StartCoroutine(DelayInvoke(1, delegate
            {
                if (IconImage.sprite != null)
                {
                    IconImage.gameObject.SetActive(true);
                    IconImage.rectTransform.DOScale(Vector3.one, 0.2f).SetEase(Ease.Linear).onComplete += () =>
                    {
                        if (IconHighlightImage.sprite != null)
                            IconHighlightImage.gameObject.SetActive(true);

                        SoundManager.PlaySFX(_finishSound);
                        StartCoroutine(DelayInvoke(0.5f, onComplete));
                    };
                }
                else
                {
                    SoundManager.PlaySFX(_finishSound);
                    onComplete?.Invoke();
                }
            }));
        }

        
    }

    /// <summary>
    /// Call this method to deselect highlight
    /// </summary>
    public void DeselectButton()
    {
        if(GridHighlightImage.sprite != null)
            GridHighlightImage.gameObject.SetActive(false);
    }

    private IEnumerator DelayInvoke(float time, UnityAction onComplete)
    {
        yield return new WaitForSeconds(time);
        onComplete?.Invoke();
    }

    #endregion
}
