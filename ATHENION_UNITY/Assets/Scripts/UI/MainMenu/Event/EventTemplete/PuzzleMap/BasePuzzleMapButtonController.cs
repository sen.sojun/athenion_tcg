﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[System.Serializable]
public abstract class BasePuzzleMapButtonController : MonoBehaviour
{
    #region Inspector Properties

    [Header("Base UI Setting")]
    [SerializeField] protected Button _PuzzleMapButton;

    #endregion

    #region Protected Properties

    protected int _buttonIndex;

    #endregion

    #region Methods

    #region Abstract Methods

    public abstract void Init(int buttonIndex, PuzzleMapEventData eventData, PuzzleMapPlayerData playerData);
    public abstract void ActionStart(UnityAction onComplete = null);
    public abstract void ActionEnd(PuzzleMapEventData eventData, PuzzleMapPlayerData playerData, UnityAction onComplete = null);
    public abstract void ActionCancel(UnityAction onComplete = null);

    #endregion

    #region Unity Callback Methods

    protected virtual void OnEnable()
    {
        GameEventPuzzleMapUIManager.OnEnableAction += GameEventPuzzleMapUiManager_OnEnableAction;
        GameEventPuzzleMapUIManager.OnDisableAction += GameEventPuzzleMapUiManager_OnDisableAction;
    }

    protected virtual void OnDisable()
    {
        GameEventPuzzleMapUIManager.OnEnableAction -= GameEventPuzzleMapUiManager_OnEnableAction;
        GameEventPuzzleMapUIManager.OnDisableAction -= GameEventPuzzleMapUiManager_OnDisableAction;
    }

    #endregion

    #region Event Methods

    protected virtual void GameEventPuzzleMapUiManager_OnEnableAction()
    {
        _PuzzleMapButton.enabled = true;
    }

    protected virtual void GameEventPuzzleMapUiManager_OnDisableAction()
    {
        _PuzzleMapButton.enabled = false;
    }

    #endregion

    #region Setting Methods

    public void SetButtonAction(UnityAction action)
    {
        if (_PuzzleMapButton == null) return;
        _PuzzleMapButton.onClick.RemoveAllListeners();
        _PuzzleMapButton.onClick.AddListener(action);
    }

    #endregion

    #endregion
}
