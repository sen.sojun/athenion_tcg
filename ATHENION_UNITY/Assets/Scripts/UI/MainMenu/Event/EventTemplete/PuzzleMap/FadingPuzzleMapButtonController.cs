﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class FadingPuzzleMapButtonController : BasePuzzleMapButtonController
{
    #region Static Value

    private static readonly string _iconPrefixKey = "ICON_";
    private static readonly string _fadingImagePrefixKey = "GRID_";
    private static readonly string _highlightResultImageKey = "HIGHLIGHT_GRID";
    private static readonly Vector3 _iconStartSize = Vector3.one * 1.35f;

    #endregion

    #region Inspector Properties

    [Header("UI Elements")]
    [SerializeField] private Image _FadingImage;
    [SerializeField] private Image _ResultImage;
    [SerializeField] private Image _HighlightResultImage;

    #endregion

    #region Methods

    #region Init Methods

    public override void Init(int buttonIndex, PuzzleMapEventData eventData, PuzzleMapPlayerData playerData)
    {
        _buttonIndex = buttonIndex;

        if (_FadingImage != null && playerData.IsGridOpened(_buttonIndex))
        {
            _FadingImage.gameObject.SetActive(false);
        }
        else if (_FadingImage != null && playerData.IsGridOpened(_buttonIndex) == false)
        {
            _FadingImage.sprite = SpriteResourceHelper.LoadSubEventImage(eventData.EventKey, _fadingImagePrefixKey + _buttonIndex);
        }

        if (_ResultImage != null && playerData.IsGridOpened(_buttonIndex))
        {
            Sprite resultSprite = SpriteResourceHelper.LoadSubEventImage(eventData.EventKey, _iconPrefixKey + playerData.GetGridDataByIndex(_buttonIndex).GridIcon, true);
            if (resultSprite == null)
            {
                _ResultImage.gameObject.SetActive(false);
            }
            else
            {
                _ResultImage.sprite = resultSprite;
                _ResultImage.gameObject.SetActive(true);

                if (_HighlightResultImage != null)
                {
                    Sprite highlightSprite = SpriteResourceHelper.LoadSubEventImage(eventData.EventKey, _highlightResultImageKey, true);
                    if (highlightSprite == null)
                    {
                        _HighlightResultImage.gameObject.SetActive(false);
                    }
                    else
                    {
                        _HighlightResultImage.sprite = highlightSprite;
                    }
                }
            }
        }
        else if(_ResultImage != null && playerData.IsGridOpened(_buttonIndex) == false)
        {
            _ResultImage.gameObject.SetActive(false);
            if (_HighlightResultImage != null)
                _HighlightResultImage.gameObject.SetActive(false);
        }
    }

    #endregion

    #region Action Methods

    public override void ActionStart(UnityAction onComplete = null)
    {
        if (_FadingImage != null)
        {
            _FadingImage.DOFade(0, 0.5f).SetEase(Ease.Linear).onComplete += () =>
            {
                StartCoroutine(GameEventPuzzleMapManager.Instance.DelayActionTime(0.5f, onComplete));
            };
        }
    }

    public override void ActionEnd(PuzzleMapEventData eventData, PuzzleMapPlayerData playerData, UnityAction onComplete = null)
    {
        if (_ResultImage != null && playerData.IsGridOpened(_buttonIndex))
        {
            Sprite resultSprite = SpriteResourceHelper.LoadSubEventImage(eventData.EventKey, _iconPrefixKey + playerData.GetGridDataByIndex(_buttonIndex).GridIcon, true);
            if (resultSprite != null)
            {
                _ResultImage.sprite = resultSprite;
                //_ResultImage.rectTransform.localScale = ;
                _ResultImage.gameObject.SetActive(true);
                _ResultImage.rectTransform.DOScale(_iconStartSize, 0.25f).SetEase(Ease.Linear).onComplete += () =>
                {
                    _ResultImage.rectTransform.DOScale(Vector3.one, 0.15f).SetEase(Ease.Linear).onComplete += () =>
                    {
                        StartCoroutine(GameEventPuzzleMapManager.Instance.DelayActionTime(1, onComplete));
                    };
                };

                if (_HighlightResultImage != null)
                {
                    Sprite highlightSprite = SpriteResourceHelper.LoadSubEventImage(eventData.EventKey, _highlightResultImageKey, true);
                    if (highlightSprite != null)
                    {
                        _HighlightResultImage.sprite = highlightSprite;
                        //_HighlightResultImage.rectTransform.localScale = _iconStartSize;
                        _HighlightResultImage.gameObject.SetActive(true);
                        _HighlightResultImage.rectTransform.DOScale(_iconStartSize, 1).SetEase(Ease.Linear).onComplete +=
                            () =>
                            {
                                _HighlightResultImage.rectTransform.DOScale(Vector3.one, 0.15f).SetEase(Ease.Linear);
                            };
                    }
                }
            }
            else
            {
                onComplete?.Invoke();
            }
        }
    }

    public override void ActionCancel(UnityAction onComplete = null)
    {
        _FadingImage.color = new Color(1,1,1,1);
        _FadingImage.gameObject.SetActive(true);
        
        if(_ResultImage != null)
            _ResultImage.gameObject.SetActive(false);

        if (_HighlightResultImage != null)
            _HighlightResultImage.gameObject.SetActive(false);

        onComplete?.Invoke();
    }

    #endregion

    #endregion
}
