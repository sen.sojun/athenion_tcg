﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class PuzzleGridBackgroundItem : MonoBehaviour
{
    [Header("Image UI Elements")]
    public Image GridBackgroundImage;
    public Image GridBackgroundGrayScaleImage;

    private bool _isGrayScale;

    /// <summary>
    /// Call this method to initialize the grid background image data.
    /// </summary>
    /// <param name="imageSprite">Grid background image sprite.</param>
    /// <param name="isClicked">Grid's "is clicked" state.</param>
    /// <param name="isGrayScale">Grid's "is greyscale" state.</param>
    public void Init(Sprite imageSprite, bool isClicked, bool isGrayScale = true)
    {
        _isGrayScale = isGrayScale;

        if (imageSprite != null)
        {
            GridBackgroundImage.sprite = imageSprite;
            GridBackgroundGrayScaleImage.sprite = imageSprite;
            GridBackgroundImage.SetNativeSize();
            GridBackgroundGrayScaleImage.SetNativeSize();
        }

        if (!_isGrayScale)
        {
            GridBackgroundGrayScaleImage.gameObject.SetActive(false);
        }

        if (isClicked)
        {
            GridBackgroundGrayScaleImage.gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Call this method to fade the grid gray scale background when this grid is clicked.
    /// </summary>
    public void OnGridIsClicked()
    {
        if(_isGrayScale)
            GridBackgroundGrayScaleImage.DOFade(0, 1).SetEase(Ease.Linear);
    }
}
