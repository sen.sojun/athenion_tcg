﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;

public class GameEventPuzzleMapManager : BaseGameEventTemplate
{
    #region Static Value

    private static readonly string _actionOpenedGridTitleKey = "EVENT_GRID_IS_CLICKED_TITLE";
    private static readonly string _actionOpenedGridMessageKey = "EVENT_GRID_IS_CLICKED_MESSAGE";

    private static readonly string _actionConfirmOpenGridTitleKey = "EVENT_CONFIRM_OPEN_GRID_TITLE";
    private static readonly string _actionConfirmOpenGridMessageKey = "EVENT_CONFIRM_OPEN_GRID_MESSAGE";

    private static readonly string _actionErrorTitleString = "ERROR_ACTION_TITLE_";
    private static readonly string _actionErrorMessageString = "ERROR_ACTION_MESSAGE_";

    private static readonly string _participateRewardKey = "Participate";

    #endregion

    #region Public Properties

    public PuzzleMapEventData EventInfoData { get; private set; }
    public PuzzleMapPlayerData PlayerData { get; private set; }

    #endregion

    #region Private Properties

    #endregion

    #region Inspector Properties

    [Header("UI Controller")]
    [SerializeField] private GameEventPuzzleMapUIManager _UiController;

    [Header("CloudScript Function")]
    [SerializeField] private string _InitCloudScriptFunctionName;
    [SerializeField] private string _ActionCloudScriptFunctionName;

    #endregion

    #region Methods

    #region Override Methods

    public override void Init(string eventKey, string sceneName, UnityAction onComplete = null, UnityAction onFail = null)
    {
        base.Init(eventKey, sceneName, onComplete, onFail);

        DataManager.Instance.ExcecuteEventCloudScript(
            _InitCloudScriptFunctionName
            , new {EventKey = eventKey}
            , result =>
            {
                if (!result.IsSuccess)
                {
                    onFail?.Invoke();
                    return;
                }

                EventInfoData = new PuzzleMapEventData(eventKey, result);
                PlayerData = new PuzzleMapPlayerData(result);

                if (EventInfoData.IsLoadedSuccess && PlayerData.IsLoadedSuccess)
                {
                    InitUI();
                    onComplete?.Invoke();
                }
                else
                {
                    onFail?.Invoke();
                }
            }
            , errMsg =>
            {
                onFail?.Invoke();
            });
    }

    #endregion

    #region Init Methods

    private void InitUI()
    {
        _UiController.InitUi(this);
        _UiController.SetActionBtn(OnSelectAction);
        _UiController.SetEventInfoBtn(OnShowEventDetail);
    }

    #endregion

    #region Btn OnClick Methods

    private void OnSelectAction(int actionIndex)
    {
        _UiController.SetButtonsState(false);

        if (CheckButtonControllerValid(actionIndex) == false) return;
        if (CheckActionValid(actionIndex) == false) return;

        PopupUIManager.Instance.ShowPopup_TwoChoices(
            LocalizationManager.Instance.GetText(_actionConfirmOpenGridTitleKey)
            , LocalizationManager.Instance.GetText(_actionConfirmOpenGridMessageKey)
            , LocalizationManager.Instance.GetText("BUTTON_YES")
            , LocalizationManager.Instance.GetText("BUTTON_NO")
            , delegate
            {
                if (EventInfoData.RewardDictionary.ContainsKey(_participateRewardKey))
                {
                    if (EventInfoData.RewardDictionary[_participateRewardKey].Count > 0)
                    {
                        StartCoroutine(ActionRewardGenerate(
                            _UiController.PuzzleButtonList[actionIndex].transform
                            , EventInfoData.RewardDictionary[_participateRewardKey])
                        );
                    }
                }

                StartOpenGrid(actionIndex);
            }
            , delegate
            {
                _UiController.SetButtonsState(true);
                PopupUIManager.Instance.Show_SoftLoad(false);
            }
        );
    }

    #endregion

    #region Check Action Valid Methods

    private bool CheckActionValid(int buttonIndex)
    {
        switch (EventInfoData.PlayTriggerType)
        {
            case GameEventStartType.Cooldown when _UiController.IsCoolDown:
            {
                string title = LocalizationManager.Instance.GetText(GameEventTemplateKeys.CooldownTitleKey);
                string message = LocalizationManager.Instance.GetText(GameEventTemplateKeys.CooldownMessageKey);
                PopupUIManager.Instance.ShowPopup_Error(
                    title
                    , message
                    , delegate
                    {
                        PopupUIManager.Instance.Show_SoftLoad(false);
                        _UiController.SetButtonsState(true);
                    }
                );
                return false;
            }

            case GameEventStartType.Cooldown when PlayerData.IsGridOpened(buttonIndex):
            {
                string title = LocalizationManager.Instance.GetText(_actionOpenedGridTitleKey);
                string message = LocalizationManager.Instance.GetText(_actionOpenedGridMessageKey);
                PopupUIManager.Instance.ShowPopup_Error(
                    title
                    , message
                    , delegate
                    {
                        PopupUIManager.Instance.Show_SoftLoad(false);
                        _UiController.SetButtonsState(true);
                    }
                );
                return false;
            }

            case GameEventStartType.Cooldown when PlayerData.RemainPlayTime <= 0:
            {
                string title = LocalizationManager.Instance.GetText(GameEventTemplateKeys.NoRemainTryTitleKey);
                string message = LocalizationManager.Instance.GetText(GameEventTemplateKeys.NoRemainTryMessageKey);
                PopupUIManager.Instance.ShowPopup_Error(
                    title
                    , message
                    , delegate
                    {
                        PopupUIManager.Instance.Show_SoftLoad(false);
                        _UiController.SetButtonsState(true);
                    }
                );
                return false;
            }

            case GameEventStartType.Daily when PlayerData.IsGridOpened(buttonIndex):
            {
                string title = LocalizationManager.Instance.GetText(_actionOpenedGridTitleKey);
                string message = LocalizationManager.Instance.GetText(_actionOpenedGridMessageKey);
                PopupUIManager.Instance.ShowPopup_Error(
                    title
                    , message
                    , delegate
                    {
                        PopupUIManager.Instance.Show_SoftLoad(false);
                        _UiController.SetButtonsState(true);
                    }
                );
                return false;
            }

            case GameEventStartType.Daily when GameHelper.IsNewDayUTC(PlayerData.LastPlayDateTime) == false:
            {
                string title = LocalizationManager.Instance.GetText(GameEventTemplateKeys.DailyAlreadyDoneTitleKey);
                string message = LocalizationManager.Instance.GetText(GameEventTemplateKeys.DailyAlreadyDoneMessageKey);
                PopupUIManager.Instance.ShowPopup_Error(
                    title
                    , message
                    , delegate
                    {
                        PopupUIManager.Instance.Show_SoftLoad(false);
                        _UiController.SetButtonsState(true);
                    }
                );
                return false;
            }
        }

        return true;
    }

    private bool CheckButtonControllerValid(int buttonIndex)
    {
        if (_UiController.PuzzleButtonList.Count < buttonIndex)
        {
            string title = LocalizationManager.Instance.GetText(_actionErrorTitleString + EventInfoData.EventKey);
            string message = LocalizationManager.Instance.GetText(_actionErrorMessageString + EventInfoData.EventKey);
            PopupUIManager.Instance.ShowPopup_Error(
                title
                , message
                , delegate
                {
                    PopupUIManager.Instance.Show_SoftLoad(false);
                    _UiController.SetButtonsState(true);
                }
            );
            return false;
        }

        return true;
    }

    #endregion

    #region Action Methods

    private void StartOpenGrid(int actionIndex)
    {
        bool isSuccess = true;
        PopupUIManager.Instance.Show_SoftLoad(true);

        _UiController.PuzzleButtonList[actionIndex].ActionStart();

        DataManager.Instance.ExcecuteEventCloudScript(
            _ActionCloudScriptFunctionName
            , new
            {
                EventKey = EventInfoData.EventKey
                , GridIndex = actionIndex
            }
            , result =>
            {
                isSuccess &= result.IsSuccess;

                if (!isSuccess)
                {
                    PopupUIManager.Instance.Show_SoftLoad(false);
                    _UiController.SetButtonsState(true);

                    PopupUIManager.Instance.ShowPopup_Error(
                        LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventActionErrorTitleKey)
                        , LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventActionErrorMessageKey)
                        , delegate
                        {
                            PopupUIManager.Instance.Show_SoftLoad(false);
                            _UiController.SetButtonsState(true);
                            _UiController.PuzzleButtonList[actionIndex].ActionCancel();
                        }
                    );

                    return;
                }

                PlayerData.UpdatePlayerData(result);

                isSuccess = GameHelper.TryDeserializeJSONStr(result.CustomData[GameEventTemplateKeys.PlayerReceivedItemKey], out Dictionary<string, int> itemDict);
                if (isSuccess)
                {
                    List<ItemData> itemDataList = DataManager.ConvertDictionaryToItemDataList(itemDict);
                    DataManager.Instance.InventoryData.GrantItemByItemList(itemDataList);
                    OnReceiveItem(itemDataList);
                }

                _UiController.RunCooldown();

                SoundManager.PlaySFX(PlayerData.GetGridDataByIndex(actionIndex).GridIcon != "None"
                    ? _UiController.RewardDefaultSound
                    : _UiController.NoneRewardDefaultSound);

                _UiController.OnActionEnd(
                    actionIndex
                    , () =>
                    {
                        PopupUIManager.Instance.Show_SoftLoad(false);
                        _UiController.SetButtonsState(true);

                        int totalOpenGrid = PlayerData.OpenedGrid.Count;
                        string rewardKey = PlayerData.GetLatestOpenedGrid().GridIcon;
                        List<ItemData> rewardList = EventInfoData.RewardDictionary[rewardKey];

                        PopupUIManager.Instance.ShowPopup_GotRewardAnimated(
                                LocalizationManager.Instance.GetText(GameEventTemplateKeys.NotiRewardTitleKey)
                                , LocalizationManager.Instance.GetText(GameEventTemplateKeys.NotiRewardMessageKey)
                                , rewardList
                                , null

                            ).SetAnimationEase(Ease.OutExpo)
                            .SetDelay(0.1f)
                            .SetAnimationDuration(0.25f)
                            .SetMaxScale(8f);
                    }
                );
            }
            , Debug.LogError
        );
    }

    #endregion

    #endregion
}

public class PuzzleMapEventData : BaseGameEventData
{
    #region CloudScript Key

    private static readonly string _rewardsKey = "rewards";
    private static readonly string _puzzleMapWidthKey = "width";
    private static readonly string _puzzleMapHeightKey = "height";
    private static readonly string _iconTotalKey = "icon_count";
    private static readonly string _iconKeyListKey = "icon_keys";

    #endregion

    #region Public Properties

    public Dictionary<string, List<ItemData>> RewardDictionary { get; private set; }
    public List<string> IconKeyList => _iconKeyList;
    //public Dictionary<string, int> GridTotalIconCountDictionary => _gridTotalIconCountDictionary;

    #endregion

    #region Private Properties

    //private Dictionary<string, int> _gridTotalIconCountDictionary;
    private readonly List<string> _iconKeyList;

    #endregion

    #region Constructor

    public PuzzleMapEventData(string eventKey, CloudScriptResultProcess eventData) : base(eventKey, eventData)
    {
        #region Init Data Instance

        RewardDictionary = new Dictionary<string, List<ItemData>>();
        _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(eventData.CustomData[GameEventTemplateKeys.EventDataKey], out Dictionary<string, object> eventDataDict);

        #endregion

        #region Reward Data

        _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(eventDataDict[_rewardsKey].ToString(), out Dictionary<string, object> rewardDictionary);
        if (!_isLoadedSuccess) return;

        foreach (KeyValuePair<string, object> pair in rewardDictionary)
        {
            _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(pair.Value.ToString(), out Dictionary<string, int> rewardData);
            if (!_isLoadedSuccess) return;

            List<ItemData> itemList = DataManager.ConvertDictionaryToItemDataList(rewardData);
            RewardDictionary.Add(pair.Key, itemList);
        }

        #endregion

        #region Puzzle Map Data

        _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(eventDataDict[_iconKeyListKey].ToString(), out _iconKeyList);

        #endregion
    }

    #endregion
}

public class PuzzleMapPlayerData
{
    #region CloudScript Key

    private static readonly string _openedGridKey = "opened_grids";
    private static readonly string _iconTotalKey = "icon_count";
    private static readonly string _gridIdKey = "grid_id";
    private static readonly string _gridIconKey = "grid_icon";

    #endregion

    #region Public Properties

    public bool IsLoadedSuccess => _isLoadedSuccess;
    public int RemainPlayTime => _remainPlayTime;
    public DateTime LastPlayDateTime => _lastPlayDateTime;
    public List<PuzzleMapGridData> OpenedGrid => _openedGrid;
    
    #endregion

    #region Private Properties

    private bool _isLoadedSuccess = true;
    private int _remainPlayTime;
    private DateTime _lastPlayDateTime;
    private List<PuzzleMapGridData> _openedGrid;

    #endregion

    #region Constructor

    public PuzzleMapPlayerData(CloudScriptResultProcess eventData)
    {
        _openedGrid = new List<PuzzleMapGridData>();

        _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(eventData.CustomData[GameEventTemplateKeys.PlayerDataKey], out Dictionary<string, object> playerDataDictionary);
        if (!_isLoadedSuccess) return;

        _isLoadedSuccess &= int.TryParse(playerDataDictionary[GameEventTemplateKeys.PlayerRemainPlaytimeKey].ToString(), out _remainPlayTime);
        _isLoadedSuccess &= DateTime.TryParse(playerDataDictionary[GameEventTemplateKeys.PlayerActionTimestampKey].ToString(), out _lastPlayDateTime);
        _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(playerDataDictionary[_openedGridKey].ToString(), out List<object> gridObjectList);
        if (!_isLoadedSuccess) return;

        foreach (object gridObject in gridObjectList)
        {
            _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(gridObject.ToString(), out Dictionary<string, string> gridDictionary);
            if (!_isLoadedSuccess) return;

            _openedGrid.Add(new PuzzleMapGridData(int.Parse(gridDictionary[_gridIdKey]), gridDictionary[_gridIconKey]));
        }
    }

    #endregion

    #region Methods

    public void UpdatePlayerData(CloudScriptResultProcess eventData)
    {
        _openedGrid.Clear();

        _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(eventData.CustomData[GameEventTemplateKeys.PlayerDataKey], out Dictionary<string, object> playerDataDictionary);
        if (!_isLoadedSuccess) return;

        _isLoadedSuccess &= int.TryParse(playerDataDictionary[GameEventTemplateKeys.PlayerRemainPlaytimeKey].ToString(), out _remainPlayTime);
        _isLoadedSuccess &= DateTime.TryParse(playerDataDictionary[GameEventTemplateKeys.PlayerActionTimestampKey].ToString(), out _lastPlayDateTime);
        _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(playerDataDictionary[_openedGridKey].ToString(), out List<object> gridObjectList);
        if (!_isLoadedSuccess) return;

        foreach (object gridObject in gridObjectList)
        {
            _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(gridObject.ToString(), out Dictionary<string, string> gridDictionary);
            if (!_isLoadedSuccess) return;

            _openedGrid.Add(new PuzzleMapGridData(int.Parse(gridDictionary[_gridIdKey]), gridDictionary[_gridIconKey]));
        }
    }

    public bool IsGridOpened(int gridIndex)
    {
        foreach (PuzzleMapGridData gridData in _openedGrid)
        {
            if (gridData.GridIndex == gridIndex)
                return true;
        }

        return false;
    }

    public bool IsIconValid(string gridIcon)
    {
        foreach (PuzzleMapGridData gridData in _openedGrid)
        {
            if (gridData.GridIcon == gridIcon)
                return true;
        }

        return false;
    }

    public PuzzleMapGridData GetGridDataByIndex(int gridIndex)
    {
        foreach (PuzzleMapGridData gridData in _openedGrid)
        {
            if (gridData.GridIndex == gridIndex)
                return gridData;
        }

        return null;
    }

    public PuzzleMapGridData GetLatestOpenedGrid()
    {
        return _openedGrid[_openedGrid.Count - 1];
    }

    #endregion
}

public class PuzzleMapGridData
{
    #region Public Properties

    public int GridIndex { get; private set; }
    public string GridIcon { get; private set; }

    #endregion

    public PuzzleMapGridData(int gridIndex, string gridIcon)
    {
        GridIndex = gridIndex;
        GridIcon = gridIcon;
    }
}