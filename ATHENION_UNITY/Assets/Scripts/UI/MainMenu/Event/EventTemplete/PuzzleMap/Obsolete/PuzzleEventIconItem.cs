﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PuzzleEventIconItem : MonoBehaviour
{
    public Image IconImage;
    public Image IconGrayScaleImage;
    public Image IconBgImage;

    public void SetIconImage(Sprite iconSprite, Sprite iconBgSprite, bool isFound = false)
    {
        IconImage.sprite = iconSprite;
        IconGrayScaleImage.sprite = iconSprite;
        IconBgImage.sprite = iconBgSprite;

        IconImage.SetNativeSize();
        IconGrayScaleImage.SetNativeSize();
        IconBgImage.SetNativeSize();

        if(isFound)
            IconGrayScaleImage.gameObject.SetActive(false);
    }

    public void OnFoundIcon()
    {
        IconGrayScaleImage.gameObject.SetActive(false);
    }
}
