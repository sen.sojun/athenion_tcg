﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class IconSpotPuzzleMapController : BasePuzzleSubResultController
{
    #region Static Value

    private static readonly string _iconPrefixKey = "ICON_";
    private static readonly string _iconHighlightKey = "ICON_HIGHLIGHT";

    #endregion

    #region Inspector Properties

    [Header("UI Elements")]
    [SerializeField] private List<Image> _IconImageList;
    [SerializeField] private List<Image> _IconHighlightImageList;
    [SerializeField] private Material _GreyscaleMaterial;

    #endregion

    #region Methods

    public override void Init(PuzzleMapEventData eventInfoData, PuzzleMapPlayerData playerData)
    {
        for (int i = 0; i < _IconImageList.Count; i++)
        {
            _IconImageList[i].sprite = SpriteResourceHelper.LoadSubEventImage(
                eventInfoData.EventKey
                , _iconPrefixKey + eventInfoData.IconKeyList[i]
            );

            if(_IconHighlightImageList.Count > 0 && _IconHighlightImageList != null)
            {
                _IconHighlightImageList[i].sprite = SpriteResourceHelper.LoadSubEventImage(
                    eventInfoData.EventKey
                    , _iconHighlightKey
                );
            }

            if (playerData.IsIconValid(eventInfoData.IconKeyList[i]))
            {
                if (_IconHighlightImageList.Count > 0)
                    _IconHighlightImageList?[i].gameObject.SetActive(true);
            }
            else
            {
                _IconImageList[i].material = _GreyscaleMaterial;
                if (_IconHighlightImageList.Count > 0)
                    _IconHighlightImageList?[i].gameObject.SetActive(false);
            }
        }
    }

    public override void UpdateResult(PuzzleMapEventData eventInfoData, PuzzleMapPlayerData playerData)
    {
        int index = eventInfoData.IconKeyList.IndexOf(playerData.GetLatestOpenedGrid().GridIcon);

        if (index < eventInfoData.IconKeyList.Count && index >= 0)
        {
            _IconImageList[index].material = null;
            if (_IconHighlightImageList.Count > 0)
                _IconHighlightImageList[index].gameObject.SetActive(true);
        }
    }

    #endregion
}
