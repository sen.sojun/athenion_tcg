﻿using UnityEngine;

[System.Serializable]
public abstract class BasePuzzleSubResultController : MonoBehaviour
{
    #region Methods

    public abstract void Init(PuzzleMapEventData eventInfoData, PuzzleMapPlayerData playerData);
    public abstract void UpdateResult(PuzzleMapEventData eventInfoData, PuzzleMapPlayerData playerData);

    #endregion
}
