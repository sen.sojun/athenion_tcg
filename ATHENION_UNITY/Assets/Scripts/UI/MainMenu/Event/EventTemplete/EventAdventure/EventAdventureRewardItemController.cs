﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class EventAdventureRewardItemController : MonoBehaviour
{
    #region Static Value

    private static readonly string _highlightImageKey = "HIGHLIGHT_IMAGE";
    private static readonly string _rewardPrefixKey = "REWARD_";

    private static readonly string _EventAdventureRewardTitle = "EVENT_ADVENTURE_REWARD_TITLE";
    private static readonly string _EventAdventureRewardMessage = "EVENT_ADVENTURE_REWARD_MESSAGE";

    #endregion

    #region Inspector Properties

    [Header("UI Elements")]
    [SerializeField] private Image _RewardIconImage;
    [SerializeField] private Image _RewardCompleteImage;
    [SerializeField] private TextMeshProUGUI _RewardValueText;
    [SerializeField] private Button _RewardButton;
    #endregion

    #region Private Properties

    private int _rewardIndex;
    private List<ItemData> _rewardList;
    private string _eventKey;
    #endregion

    #region Methods

    #region Init Methods

    public void Init(int rewardIndex, EventAdventureEventData eventInfoData, EventAdventurePlayerData playerData)
    {
        _rewardIndex = rewardIndex;
        _rewardList = eventInfoData.RewardList[_rewardIndex];
        _eventKey = eventInfoData.EventKey;
        int receiveRewardCount = eventInfoData.RewardPoolList[_rewardIndex] - playerData.RemainRewardPoolList[_rewardIndex];

        if (_RewardIconImage != null) _RewardIconImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, _rewardPrefixKey + _rewardIndex);
        if (_RewardCompleteImage != null) _RewardCompleteImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, _highlightImageKey);
        if (_RewardValueText != null) _RewardValueText.text = $"{receiveRewardCount}/{eventInfoData.RewardPoolList[_rewardIndex]}";

        if (_RewardCompleteImage != null) _RewardCompleteImage.gameObject.SetActive(receiveRewardCount >= eventInfoData.RewardPoolList[_rewardIndex]);

        if (_RewardButton != null)
        {
            _RewardButton.onClick.RemoveAllListeners();
            _RewardButton.onClick.AddListener(ShowItemPackDetail);
        }
    }

    #endregion

    public void UpdateProgress(EventAdventureEventData eventInfoData, EventAdventurePlayerData playerData)
    {
        int receiveRewardCount = eventInfoData.RewardPoolList[_rewardIndex] - playerData.RemainRewardPoolList[_rewardIndex];
        if (_RewardCompleteImage != null) _RewardCompleteImage.gameObject.SetActive(receiveRewardCount >= eventInfoData.RewardPoolList[_rewardIndex]);
        if (_RewardValueText != null) _RewardValueText.text = $"{receiveRewardCount}/{eventInfoData.RewardPoolList[_rewardIndex]}";
    }

    private void ShowItemPackDetail()
    {
        PopupUIManager.Instance.ShowPopup_PreviewReward(
            LocalizationManager.Instance.GetText(_EventAdventureRewardTitle)
            , LocalizationManager.Instance.GetText(_EventAdventureRewardMessage)
            , _rewardList
        );
    }

    #endregion
}
