﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class BackgroundUIScrollingController : MonoBehaviour
{
    #region Inspector Properties

    [Header("UI Image Elements")]
    [SerializeField] private List<Image> _BackgroundList;

    [Header("Offset Setting")]
    [SerializeField] private float _EndOffset;
    [SerializeField] private float _StartOffSet;
    [SerializeField] private Vector2 _MoveDirection;
    [SerializeField] private float _Speed;

    #endregion

    #region Private Properties

    private bool _isStart = false;

    #endregion

    #region Methods

    #region Unity Callback Methods

    private void Update()
    {
        if (_isStart)
        {
            foreach (Image image in _BackgroundList)
            {
                image.rectTransform.Translate(_MoveDirection * _Speed * Time.deltaTime);
                if (image.rectTransform.anchoredPosition.x <= _EndOffset)
                {
                    image.rectTransform.anchoredPosition = new Vector2(_StartOffSet, 0);
                }
            }
        }
    }

    private void OnEnable()
    {
        GameEventAdventureUIManager.OnAdventureStart += GameEventAdventureUiManager_OnAdventureStart;
        GameEventAdventureUIManager.OnAdventureEnd += GameEventAdventureUiManager_OnAdventureEnd;
    }

    private void OnDisable()
    {
        GameEventAdventureUIManager.OnAdventureStart -= GameEventAdventureUiManager_OnAdventureStart;
        GameEventAdventureUIManager.OnAdventureEnd -= GameEventAdventureUiManager_OnAdventureEnd;
    }

    #endregion

    #region Init Methods

    public void Init(bool isActive)
    {
        _isStart = isActive;
    }

    #endregion

    #region Event Methods

    private void GameEventAdventureUiManager_OnAdventureStart()
    {
        _isStart = true;
    }

    private void GameEventAdventureUiManager_OnAdventureEnd()
    {
        _isStart = false;
    }

    #endregion

    #endregion
}
