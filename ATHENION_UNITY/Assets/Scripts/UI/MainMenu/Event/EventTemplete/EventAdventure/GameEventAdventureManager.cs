﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;

public class GameEventAdventureManager : BaseGameEventTemplate
{
    #region Static Value

    private static readonly string _eventActionStartSuccessTitlePrefixKey = "EVENT_START_ADVENTURE_SUCCESS_TITLE_";
    private static readonly string _eventActionStartSuccessMessagePrefixKey = "EVENT_START_ADVENTURE_SUCCESS_MESSAGE_";

    private static readonly string _eventRewardIsAllReceivedTitlePrefixKey = "EVENT_ALL_REWARD_RECEIVED_TITLE_";
    private static readonly string _eventRewardIsAllReceivedMessagePrefixKey = "EVENT_ALL_REWARD_RECEIVED_MESSAGE_";

    #endregion

    #region Public Properties

    public EventAdventureEventData EventInfoData { get; private set; }
    public EventAdventurePlayerData PlayerData { get; private set; }

    #endregion

    #region Private Properties



    #endregion

    #region Inspector Properties

    [Header("UI Controller")]
    [SerializeField] private GameEventAdventureUIManager _UiController;

    [Header("CloudScript Function")]
    [SerializeField] private string _InitCloudScriptFunctionName;
    [SerializeField] private string _StartActionCloudScriptFunctionName;
    [SerializeField] private string _EndActionCloudScriptFunctionName;

    #endregion

    #region Methods

    #region Override Methods

    public override void Init(string eventKey, string sceneName, UnityAction onComplete = null, UnityAction onFail = null)
    {
        base.Init(eventKey, sceneName, onComplete, onFail);

        DataManager.Instance.ExcecuteEventCloudScript(
            _InitCloudScriptFunctionName
            , new {EventKey = eventKey}
            , result =>
            {
                if (!result.IsSuccess)
                {
                    onFail?.Invoke();
                    return;
                }

                EventInfoData = new EventAdventureEventData(eventKey, result);
                PlayerData = new EventAdventurePlayerData(result);

                if (EventInfoData.IsLoadedSuccess && PlayerData.IsLoadedSuccess)
                {
                    InitUI();
                    onComplete?.Invoke();
                }
                else
                {
                    onFail?.Invoke();
                }
            }
            , errMsg =>
            {
                onFail?.Invoke();
            }
        );
    }

    #endregion

    #region Init Methods

    private void InitUI()
    {
        _UiController.InitUi(this);
        _UiController.SetActionBtn(OnSelectAction);
        _UiController.SetEventInfo(OnShowEventDetail);
        _UiController.SetReceiveRewardActionBtn(OnReceiveRewardAction);
    }

    #endregion

    #region Btn OnClick Methods

    private void OnSelectAction()
    {
        _UiController.SetButtonsState(false);

        if (CheckActionValid() == false)
        {
            _UiController.SetButtonsState(true);
            return;
        }

        PopupUIManager.Instance.Show_SoftLoad(true);

        DataManager.Instance.ExcecuteEventCloudScript(
            _StartActionCloudScriptFunctionName
            , new
            {
                EventKey = EventInfoData.EventKey
            }
            , result =>
            {
                if (result.IsSuccess == false)
                {
                    PopupUIManager.Instance.Show_SoftLoad(false);
                    _UiController.SetButtonsState(true);

                    PopupUIManager.Instance.ShowPopup_Error(
                        LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventActionErrorTitleKey)
                        , LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventActionErrorMessageKey)
                        , null
                    );

                    return;
                }

                PlayerData.UpdatePlayerData(result);
                _UiController.OnActionStart();

                PopupUIManager.Instance.Show_SoftLoad(false);
                _UiController.SetButtonsState(true);

                PopupUIManager.Instance.ShowPopup_ConfirmOnly(
                    LocalizationManager.Instance.GetText(_eventActionStartSuccessTitlePrefixKey + EventInfoData.EventKey)
                    , LocalizationManager.Instance.GetText(_eventActionStartSuccessMessagePrefixKey + EventInfoData.EventKey)
                );
            }
            , Debug.Log
        );
        
    }

    private void OnReceiveRewardAction()
    {
        PopupUIManager.Instance.Show_SoftLoad(true);
        _UiController.SetButtonsState(false);

        DataManager.Instance.ExcecuteEventCloudScript(
            _EndActionCloudScriptFunctionName
            , new
            {
                EventKey = EventInfoData.EventKey
            }
            , result =>
            {
                if (result.IsSuccess == false)
                {
                    PopupUIManager.Instance.Show_SoftLoad(false);
                    _UiController.SetButtonsState(true);

                    PopupUIManager.Instance.ShowPopup_Error(
                        LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventActionErrorTitleKey)
                        , LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventActionErrorMessageKey)
                        , null
                    );

                    return;
                }

                PlayerData.UpdatePlayerData(result);

                bool isSuccess = GameHelper.TryDeserializeJSONStr(result.CustomData[GameEventTemplateKeys.PlayerReceivedItemKey], out Dictionary<string, int> itemDict);
                if (isSuccess)
                {
                    List<ItemData> itemDataList = DataManager.ConvertDictionaryToItemDataList(itemDict);
                    DataManager.Instance.InventoryData.GrantItemByItemList(itemDataList);
                    OnReceiveItem(itemDataList);

                    int countPool = 0;
                    foreach (int count in PlayerData.RemainRewardPoolList)
                    {
                        countPool += count;
                    }

                    if(countPool <= 0)
                        OnEventComplete();

                    _UiController.OnReceivedRewardStart(() =>
                    {
                        PopupUIManager.Instance.Show_SoftLoad(false);
                        _UiController.SetButtonsState(true);

                        PopupUIManager.Instance.ShowPopup_GotRewardAnimated(
                                LocalizationManager.Instance.GetText(GameEventTemplateKeys.NotiRewardTitleKey)
                                , LocalizationManager.Instance.GetText(GameEventTemplateKeys.NotiRewardMessageKey)
                                , itemDataList
                                , () => _UiController.OnReceivedRewardEnd()
                            ).SetAnimationEase(Ease.OutExpo)
                            .SetDelay(0.1f)
                            .SetAnimationDuration(0.25f)
                            .SetMaxScale(8f);
                    });
                }
            }
            , 
            Debug.Log
        );
    }

    #endregion

    #region Check Action Valid Methods

    private bool CheckActionValid()
    {
        int countPool = 0;
        foreach (int count in PlayerData.RemainRewardPoolList)
        {
            countPool += count;
        }

        switch (EventInfoData.PlayTriggerType)
        {
            case GameEventStartType.Cooldown when PlayerData.RemainPlayTime <= 0:
            {
                string title = LocalizationManager.Instance.GetText(GameEventTemplateKeys.NoRemainTryTitleKey);
                string message = LocalizationManager.Instance.GetText(GameEventTemplateKeys.NoRemainTryMessageKey);
                PopupUIManager.Instance.ShowPopup_Error(
                    title
                    , message
                    , delegate
                    {
                        PopupUIManager.Instance.Show_SoftLoad(false);
                        _UiController.SetButtonsState(true);
                    }
                );
                return false;
            }

            case GameEventStartType.Cooldown when _UiController.IsCoolDown:
            {
                string title = LocalizationManager.Instance.GetText(GameEventTemplateKeys.CooldownTitleKey);
                string message = LocalizationManager.Instance.GetText(GameEventTemplateKeys.CooldownMessageKey);
                PopupUIManager.Instance.ShowPopup_Error(
                    title
                    , message
                    , delegate
                    {
                        PopupUIManager.Instance.Show_SoftLoad(false);
                        _UiController.SetButtonsState(true);
                    }
                );
                return false;
            }

            case GameEventStartType.Cooldown when countPool <= 0:
            {
                string title = LocalizationManager.Instance.GetText(_eventRewardIsAllReceivedTitlePrefixKey + EventInfoData.EventKey);
                string message = LocalizationManager.Instance.GetText(_eventRewardIsAllReceivedMessagePrefixKey + EventInfoData.EventKey);
                PopupUIManager.Instance.ShowPopup_Error(
                    title
                    , message
                    , delegate
                    {
                        PopupUIManager.Instance.Show_SoftLoad(false);
                        _UiController.SetButtonsState(true);
                    }
                );
                return false;
            }
        }

        return true;
    }

    #endregion

    #endregion
}

public class EventAdventureEventData : BaseGameEventData
{
    #region CloudScript Key

    private static readonly string _rewardListKey = "reward_list";
    private static readonly string _rewardPoolKey = "reward_pool";

    #endregion

    #region Public Properties

    public List<List<ItemData>> RewardList { get; private set; }
    public List<int> RewardPoolList => _rewardPoolList;

    #endregion

    #region Private Properties

    private List<int> _rewardPoolList;

    #endregion

    #region Constructor

    public EventAdventureEventData(string eventKey, CloudScriptResultProcess eventData) : base(eventKey, eventData)
    {
        #region Init Data Instance

        RewardList = new List<List<ItemData>>();
        _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(eventData.CustomData[GameEventTemplateKeys.EventDataKey], out Dictionary<string, object> eventDataDict);
        if (!_isLoadedSuccess) return;

        #endregion

        #region Reward Data

        _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(eventDataDict[_rewardListKey].ToString(), out List<object> rewardObjectList);
        if (!_isLoadedSuccess) return;

        foreach (object rewardObject in rewardObjectList)
        {
            _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(rewardObject.ToString(), out Dictionary<string, int> rewardDict);
            if (!_isLoadedSuccess) return;

            List<ItemData> itemList = DataManager.ConvertDictionaryToItemDataList(rewardDict);
            RewardList.Add(itemList);
        }

        #endregion

        #region Reward Pool Data

        _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(eventDataDict[_rewardPoolKey].ToString(), out _rewardPoolList);

        #endregion
    }

    #endregion
}

public class EventAdventurePlayerData
{
    #region CloudScript Key

    private static readonly string _remainRewardPoolKey = "remain_reward_pool";
    private static readonly string _adventureStateKey = "adventure_state";
    private static readonly string _adventureDoneKey = "adventure_done";

    #endregion

    #region Public Properties

    public bool IsLoadedSuccess => _isLoadedSuccess;
    public int RemainPlayTime => _remainPlayTime;
    public DateTime LastPlayDateTime => _lastPlayDateTime;
    public List<int> RemainRewardPoolList => _remainRewardPoolList;
    public bool AdventureState => _adventureState;
    public bool AdventureDone => _adventureDone;

    #endregion

    #region Private Properties

    private bool _isLoadedSuccess = true;
    private int _remainPlayTime;
    private DateTime _lastPlayDateTime;
    private List<int> _remainRewardPoolList;
    private bool _adventureState;
    private bool _adventureDone;

    #endregion

    #region Constructor

    public EventAdventurePlayerData(CloudScriptResultProcess eventData)
    {
        _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(eventData.CustomData[GameEventTemplateKeys.PlayerDataKey], out Dictionary<string, object> playerDataDictionary);
        if (!_isLoadedSuccess) return;

        _isLoadedSuccess &= int.TryParse(playerDataDictionary[GameEventTemplateKeys.PlayerRemainPlaytimeKey].ToString(), out _remainPlayTime);
        _isLoadedSuccess &= DateTime.TryParse(playerDataDictionary[GameEventTemplateKeys.PlayerActionTimestampKey].ToString(), out _lastPlayDateTime);
        _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(playerDataDictionary[_remainRewardPoolKey].ToString(), out _remainRewardPoolList);
        _isLoadedSuccess &= bool.TryParse(playerDataDictionary[_adventureStateKey].ToString(), out _adventureState);
        _isLoadedSuccess &= bool.TryParse(playerDataDictionary[_adventureDoneKey].ToString(), out _adventureDone);
    }

    #endregion

    #region Methods

    public void UpdatePlayerData(CloudScriptResultProcess eventData)
    {
        _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(eventData.CustomData[GameEventTemplateKeys.PlayerDataKey], out Dictionary<string, object> playerDataDictionary);
        if (!_isLoadedSuccess) return;

        _isLoadedSuccess &= int.TryParse(playerDataDictionary[GameEventTemplateKeys.PlayerRemainPlaytimeKey].ToString(), out _remainPlayTime);
        _isLoadedSuccess &= DateTime.TryParse(playerDataDictionary[GameEventTemplateKeys.PlayerActionTimestampKey].ToString(), out _lastPlayDateTime);
        _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(playerDataDictionary[_remainRewardPoolKey].ToString(), out _remainRewardPoolList);
        _isLoadedSuccess &= bool.TryParse(playerDataDictionary[_adventureStateKey].ToString(), out _adventureState);
        _isLoadedSuccess &= bool.TryParse(playerDataDictionary[_adventureDoneKey].ToString(), out _adventureDone);
    }

    #endregion
}