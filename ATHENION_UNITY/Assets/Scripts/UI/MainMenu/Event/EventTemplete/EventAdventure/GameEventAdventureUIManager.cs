﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using GameEvent;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class GameEventAdventureUIManager : MonoBehaviour
{
    #region Static Value

    private static readonly string _charIdleStateKey = "IDLE";
    private static readonly string _charWalkingStateKey = "WALKING";
    private static readonly string _charHappyStateKey = "HAPPY";

    private static readonly string _cooldownWaitingKey = "EVENT_ADVENTURE_COOLDOWN_WAITING";
    private static readonly string _eventChestTextPrefixKey = "EVENT_ADVENTURE_TREASURE_";

    private static readonly Color _alphaColor = new Color(1,1,1,0);

    #endregion

    #region Inspector Properties

    [Header("Image UI Elements")]
    [SerializeField] private Image _EventHeaderBgImage;
    [SerializeField] private Image _EventActionButtonImage;
    [SerializeField] private Image _EventRewardPanelImage;
    [SerializeField] private Image _EventRewardReceiveButtonImage;

    [Header("Text UI Elements")]
    [SerializeField] private TextMeshProUGUI _EventHeaderText;
    [SerializeField] private TextMeshProUGUI _EventCooldownLabelText;
    [SerializeField] private TextMeshProUGUI _EventCooldownValueText;
    [SerializeField] private TextMeshProUGUI _EventChestLabelText;

    [Header("Button UI Elements")]
    [SerializeField] private Button _EventInfoButton;
    [SerializeField] private Button _EventStartButton;
    [SerializeField] private Button _EventRewardOpenPanelButton;
    [SerializeField] private Button _EventRewardReceiveButton;
    [SerializeField] private Button _EventRewardClosePanelButton;

    [Header("Animator Controller")]
    [SerializeField] private List<Animator> _CharAnimatorList;

    [Header("Sub Controller")]
    [SerializeField] private List<BackgroundUIScrollingController> _BackgroundUiList;
    [SerializeField] private List<EventAdventureRewardItemController> _RewardControllerList;

    [Header("Other Setting")]
    [SerializeField] private SoundManager.SFX _RewardDefaultSound;
    [SerializeField] private Material grayScaleMaterial;

    #endregion

    #region Public Properties

    public bool IsCoolDown => _isCoolDown;

    #endregion

    #region Private Properties

    private GameEventAdventureManager _manager;
    private bool _isCoolDown;
    #endregion

    #region Events

    public static event Action OnAdventureStart;
    public static event Action OnAdventureEnd;

    #endregion

    #region Methods

    #region Unity Callback Methods

    private void OnEnable()
    {
        // Character animation.
        OnPlayCharacterAnimation(_isCoolDown ? _charWalkingStateKey : _charIdleStateKey);
    }

    private void OnDisable()
    {
        // Nothing.
    }

    #endregion

    #region Init Methods

    public void InitUi(GameEventAdventureManager manager)
    {
        _manager = manager;
        EventAdventureEventData eventInfoData = _manager.EventInfoData;
        EventAdventurePlayerData playerData = _manager.PlayerData;
        string eventKey = eventInfoData.EventKey;

        #region Image UI

        if (_EventHeaderBgImage != null) _EventHeaderBgImage.sprite = SpriteResourceHelper.LoadSprite(eventInfoData.EventThemeInfo.Theme + GameEventTemplateKeys.EventHeaderBannerSuffix, SpriteResourceHelper.SpriteType.GAME_Event);

        #endregion

        #region Text UI

        if (_EventHeaderText != null) _EventHeaderText.text = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventHeaderKey + eventKey);
        if (_EventCooldownLabelText != null) _EventCooldownLabelText.text = LocalizationManager.Instance.GetText(_cooldownWaitingKey);
        if (_EventChestLabelText != null) _EventChestLabelText.text = LocalizationManager.Instance.GetText(_eventChestTextPrefixKey + eventKey);

        OnCooldown();

        #endregion

        #region Button UI

        _EventRewardOpenPanelButton.onClick.RemoveAllListeners();
        _EventRewardOpenPanelButton.onClick.AddListener(() =>
        {
            PopupUIManager.Instance.Show_SoftLoad(true);
            GameHelper.UITransition_FadeIn(_EventRewardPanelImage.gameObject
                , () => PopupUIManager.Instance.Show_SoftLoad(false)
            );
        });

        _EventRewardClosePanelButton.onClick.RemoveAllListeners();
        _EventRewardClosePanelButton.onClick.AddListener(() =>
        {
            PopupUIManager.Instance.Show_SoftLoad(true);
            GameHelper.UITransition_FadeOut(_EventRewardPanelImage.gameObject
                , () => PopupUIManager.Instance.Show_SoftLoad(false)
            );
        });

        _EventRewardReceiveButton.gameObject.SetActive(playerData.AdventureDone);
        _EventRewardReceiveButton.enabled = playerData.AdventureDone;
        if (!playerData.AdventureDone) _EventRewardReceiveButtonImage.color = _alphaColor;

        #endregion

        #region Other UI

            // Reward item in reward panel.
        for (int i = 0; i < _RewardControllerList.Count; i++)
        {
            _RewardControllerList[i].Init(i, eventInfoData, playerData);
        }

        // Init background. 
        foreach (var controller in _BackgroundUiList)
        {
            controller.Init(_isCoolDown);
        }

        _EventActionButtonImage.material = !_isCoolDown && playerData.RemainPlayTime > 0 ? null : grayScaleMaterial;

        #endregion
    }

    #endregion

    #region Update Methods

    private void OnCooldown()
    {
        var eventInfoData = _manager.EventInfoData;
        var playerData = _manager.PlayerData;

        _EventActionButtonImage.material = grayScaleMaterial;

        if (eventInfoData.PlayTriggerType == GameEventStartType.Cooldown && eventInfoData.DefaultCooldown > 0)
        {
            if (!_isCoolDown)
            {
                _isCoolDown = true;
                GameEventManager.Instance.RunRoutineTask(
                    _manager.CooldownTimer(
                        playerData.LastPlayDateTime
                        , eventInfoData.DefaultCooldown
                        , _EventCooldownValueText
                        , CoolDownEnd
                    )
                );
            }
        }
        else if (eventInfoData.PlayTriggerType == GameEventStartType.Cooldown
                 && (int)eventInfoData.DefaultCooldown == -1
                 && GameHelper.IsNewDayUTC(playerData.LastPlayDateTime) == false)
        {
            if (!_isCoolDown)
            {
                _isCoolDown = true;
                GameEventManager.Instance.RunRoutineTask(
                    _manager.NewDayTimer(
                        _EventCooldownValueText
                        , CoolDownEnd
                    )
                );
            }
        }
    }

    /// <summary>
    /// Call this method when cooldown is end.
    /// </summary>
    private void CoolDownEnd()
    {
        EventAdventurePlayerData playerData = _manager.PlayerData;

        _EventCooldownValueText.gameObject.SetActive(false);
        _EventCooldownLabelText.gameObject.SetActive(false);
        _isCoolDown = false;

        OnPlayCharacterAnimation(_charIdleStateKey);
        OnAdventureEnd?.Invoke();

        _EventRewardReceiveButton.gameObject.SetActive(true);
        _EventRewardReceiveButtonImage.DOFade(1, 0.5f).SetEase(Ease.Linear).onComplete += () =>
        {
            _EventRewardReceiveButton.enabled = true;
            _EventActionButtonImage.material = playerData.RemainPlayTime > 0 ? null : grayScaleMaterial;
        };
        
    }

    public void OnActionStart()
    {
        OnCooldown();
        OnPlayCharacterAnimation(_charWalkingStateKey);
        OnAdventureStart?.Invoke();

        _EventActionButtonImage.material = grayScaleMaterial;
        if(_EventCooldownLabelText != null) _EventCooldownLabelText.gameObject.SetActive(true);
        if(_EventCooldownValueText != null) _EventCooldownValueText.gameObject.SetActive(true);
    }

    public void OnReceivedRewardStart(UnityAction onComplete)
    {
        SoundManager.PlaySFX(_RewardDefaultSound);
        OnPlayCharacterAnimation(_charHappyStateKey);

        StartCoroutine(GameEventAdventureManager.Instance.DelayActionTime(1f, onComplete));
    }

    public void OnReceivedRewardEnd()
    {
        EventAdventureEventData eventInfoData = _manager.EventInfoData;
        EventAdventurePlayerData playerData = _manager.PlayerData;

        OnPlayCharacterAnimation(_charIdleStateKey);

        _EventRewardReceiveButton.gameObject.SetActive(false);
        _EventRewardReceiveButton.enabled = false;
        _EventRewardReceiveButtonImage.color = _alphaColor;

        foreach (EventAdventureRewardItemController rewardController in _RewardControllerList)
        {
            rewardController.UpdateProgress(eventInfoData, playerData);
        }
    }

    public void OnPlayCharacterAnimation(string state)
    {
        foreach (var animator in _CharAnimatorList)
        {
            animator.Play(state);
        }
    }

    #endregion

    #region Setting Methods

    public void SetActionBtn(UnityAction action)
    {
        _EventStartButton.onClick.RemoveAllListeners();
        _EventStartButton.onClick.AddListener(action);
    }

    public void SetEventInfo(UnityAction action)
    {
        _EventInfoButton.onClick.RemoveAllListeners();
        _EventInfoButton.onClick.AddListener(action);
    }

    public void SetReceiveRewardActionBtn(UnityAction action)
    {
        _EventRewardReceiveButton.onClick.RemoveAllListeners();
        _EventRewardReceiveButton.onClick.AddListener(action);
    }

    public void SetButtonsState(bool value)
    {
        _EventInfoButton.enabled = value;
        _EventStartButton.enabled = value;
        _EventRewardOpenPanelButton.enabled = value;
        _EventRewardReceiveButton.enabled = value;
        _EventRewardClosePanelButton.enabled = value;
    }

    #endregion

    #endregion
}
