﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(GameEventGiftSendingUI))]
public class GameEventGiftSending : BaseGameEventTemplate
{
    public readonly string LocalizationKeyPrefix = "EVENT_FLOWER_FORWARD_2020_";

    public GiftSendingEvent_CSEventData GiftSendingEventData { get; protected set; }
    public GiftSendingEvent_CSPlayerData PlayerEventData { get; protected set; }
    public string SelectingFriendID;

    private GameEventGiftSendingUI _gameEventGiftSendingUI;

    public override async void Init(string eventKey, string sceneName, UnityAction onComplete = null, UnityAction onFail = null)
    {
        base.Init(eventKey, sceneName);
        _gameEventGiftSendingUI = GetComponent<GameEventGiftSendingUI>();
        bool isSuccess = await TaskInvokeRefreshData();
        if (!isSuccess)
        {
            onFail?.Invoke();
            return;
        }

        isSuccess = await _gameEventGiftSendingUI.TaskInit();
        if (!isSuccess)
        {
            onFail?.Invoke();
            return;
        }

        onComplete?.Invoke(); //Call before claim threshold reward to show event page first
        await TryClaimEachThresholdRewardsAndUpdateUI();
    }

    public TimeSpan GetActionCooldownTime()
    {
        if (!PlayerEventData.action_timestamp.HasValue)
        {
            return TimeSpan.Zero;
        }

        return PlayerEventData.action_timestamp.Value.AddMilliseconds(GiftSendingEventData.default_cooldown) - DateTimeData.GetDateTimeUTC();
    }

    public bool CheckCanClaimReward(int rewardIndex)
    {
        if (PlayerEventData.claimed_reward_index_list.ContainsKey(rewardIndex.ToString()))
        {
            return false;
        }

        int minimumThreshold = GiftSendingEventData.presents_thresholds[rewardIndex];
        return PlayerEventData.presents_received >= minimumThreshold;
    }

    public void CheckThresholdRewardDetail(int rewardIndex)
    {
        PopupUIManager.Instance.ShowPopup_PreviewReward(
        LocalizationManager.Instance.GetText(LocalizationKeyPrefix + "POPUP_TITLE_REWARD_DETAIL"),
        string.Format(LocalizationManager.Instance.GetText(LocalizationKeyPrefix + "POPUP_BODY_REWARD_DETAIL"), GiftSendingEventData.presents_thresholds[rewardIndex].ToString()),
        new List<ItemData> { new ItemData(GiftSendingEventData.win_rewards[rewardIndex].Keys.First(), GiftSendingEventData.win_rewards[rewardIndex].Values.First()) });
    }

    private Task<bool> TaskInvokeRefreshData()
    {
        var t = new TaskCompletionSource<bool>();
        try
        {
            InvokeRefreshData(() => t.TrySetResult(true), () => t.TrySetResult(false));
        }
        catch (Exception e)
        {
            //t.TrySetException(e); //For thowing exception
            t.TrySetResult(false);
        }

        return t.Task;
    }

    private void InvokeRefreshData(UnityAction onComplete, UnityAction onFail)
    {
        PlayFabManager.Instance.ExecuteCloudScript(
            "GiftSendingEventRefreshData"
            , new { EventKey = _eventKey }
            , (result) =>
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (!resultProcess.IsSuccess)
                {
                    onFail?.Invoke();
                    return;
                }

                // Data Initialize.
                bool isLoadDataSuccess = true;
                isLoadDataSuccess &= GameHelper.TryDeserializeJSONStr(resultProcess.CustomData["event_data"], out GiftSendingEvent_CSEventData eventDataResponse);
                isLoadDataSuccess &= GameHelper.TryDeserializeJSONStr(resultProcess.CustomData["player_data"], out GiftSendingEvent_CSPlayerData playerEventDataResponse);
                if (isLoadDataSuccess)
                {
                    GiftSendingEventData = eventDataResponse;
                    PlayerEventData = playerEventDataResponse;
                    onComplete?.Invoke();
                }
                else
                {
                    onFail?.Invoke();
                }
            }
            , null
        );
    }

    private async Task<bool> TryClaimEachThresholdRewardsAndUpdateUI()
    {
        bool isSuccess = false;
        for (int thresholdIndex = 0; thresholdIndex < GiftSendingEventData.win_rewards.Length; thresholdIndex++)
        {
            isSuccess = await TaskInvokeClaimThresholdReward(thresholdIndex);
            if (!isSuccess)
            {
                return isSuccess;
            }
        }

        _gameEventGiftSendingUI.UpdateUI();
        return isSuccess;
    }

    private Task<bool> TaskInvokeClaimThresholdReward(int selectingIndex)
    {
        var t = new TaskCompletionSource<bool>();
        try
        {
            InvokeClaimThresholdReward(selectingIndex, () => t.TrySetResult(true));
        }
        catch (Exception e)
        {
            //t.TrySetException(e); //For thowing exception
            t.TrySetResult(false);
        }

        return t.Task;
    }

    private void InvokeClaimThresholdReward(int rewardIndex, UnityAction onComplete = null)
    {
        if (!CheckCanClaimReward(rewardIndex))
        {
            onComplete?.Invoke();
            return;
        }

        PopupUIManager.Instance.Show_SoftLoad(true);
        PlayFabManager.Instance.ExecuteCloudScript(
            "GiftSendingEventClaimReward"
            , new
            {
                EventKey = _eventKey,
                ClaimingRewardIndex = rewardIndex.ToString()
            }, (result) =>
            {
                PopupUIManager.Instance.Show_SoftLoad(false);
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (!resultProcess.IsSuccess)
                {
                    PopupUIManager.Instance.ShowPopup_Error(
                        LocalizationManager.Instance.GetText("ERROR_POPUP_HEADER"), 
                        LocalizationManager.Instance.GetText(resultProcess.MessageDetail));
                    return;
                }

                bool isLoadDataSuccess = true;
                isLoadDataSuccess = GameHelper.TryDeserializeJSONStr(resultProcess.CustomData["player_data"], out GiftSendingEvent_CSPlayerData playerEventDataResponse);
                if (isLoadDataSuccess)
                {
                    PlayerEventData = playerEventDataResponse;

                    List<ItemData> receivedItems = new List<ItemData> { new ItemData(GiftSendingEventData.win_rewards[rewardIndex].Keys.First(), GiftSendingEventData.win_rewards[rewardIndex].Values.First()) };
                    OnReceiveItem(receivedItems); //Analytics
                    if (PlayerEventData.claimed_reward_index_list.Count == GiftSendingEventData.win_rewards.Length)
                    {
                        OnEventComplete(); //Analytics
                    }

                    DataManager.Instance.InventoryData.GrantItemByItemList(receivedItems);
                    PopupUIManager.Instance.ShowPopup_GotRewardAnimated(
                        LocalizationManager.Instance.GetText("EVENT_CONGRATS"),
                        LocalizationManager.Instance.GetText("EVENT_YOUVEGOT"), receivedItems, () =>
                        {
                            onComplete?.Invoke();
                        });
                }
                else
                {
                    onComplete?.Invoke();
                }
            }
            , null
        );
    }

    public void InvokeSendGift(Transform buttonTransform, UnityAction onComplete = null)
    {
        if (PlayerEventData.remain_playtime <= 0)
        {
            PopupUIManager.Instance.ShowPopup_ConfirmOnlyWithWarning(
                LocalizationManager.Instance.GetText(LocalizationKeyPrefix + "POPUP_TITLE_SEND_LIMIT_REACH_TODAY"),
                LocalizationManager.Instance.GetText(LocalizationKeyPrefix + "POPUP_BODY_SEND_LIMIT_REACH_TODAY"));
            return;
        }
        else if (GetActionCooldownTime().TotalMilliseconds > 0)
        {
            PopupUIManager.Instance.ShowPopup_ConfirmOnlyWithWarning(
                LocalizationManager.Instance.GetText(LocalizationKeyPrefix + "POPUP_TITLE_SEND_COOLDOWN"),
                LocalizationManager.Instance.GetText(LocalizationKeyPrefix + "POPUP_BODY_SEND_COOLDOWN"));
            return;
        }
        else if (string.IsNullOrWhiteSpace(SelectingFriendID))
        {
            PopupUIManager.Instance.ShowPopup_ConfirmOnlyWithWarning(
                LocalizationManager.Instance.GetText(LocalizationKeyPrefix + "POPUP_TITLE_SELECT_FRIEND_FIRST"),
                LocalizationManager.Instance.GetText(LocalizationKeyPrefix + "POPUP_BODY_SELECT_FRIEND_FIRST"));
            return;
        }

        PopupUIManager.Instance.Show_SoftLoad(true);
        PlayFabManager.Instance.ExecuteCloudScript(
            "GiftSendingEventSendGiftToFriend"
            , new
            {
                EventKey = EventKey,
                ReceiverID = SelectingFriendID
            }, (result) =>
            {
                PopupUIManager.Instance.Show_SoftLoad(false);
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (!resultProcess.IsSuccess)
                {
                    PopupUIManager.Instance.ShowPopup_Error(
                        LocalizationManager.Instance.GetText("ERROR_POPUP_HEADER"),
                        LocalizationManager.Instance.GetText(resultProcess.MessageDetail));
                    return;
                }

                bool isLoadDataSuccess = true;
                isLoadDataSuccess = GameHelper.TryDeserializeJSONStr(resultProcess.CustomData["player_data"], out GiftSendingEvent_CSPlayerData playerEventDataResponse);
                if (isLoadDataSuccess)
                {
                    PlayerEventData = playerEventDataResponse;
                    List<ItemData> receivedItems = new List<ItemData> { new ItemData(GiftSendingEventData.action_rewards.Keys.First(), GiftSendingEventData.action_rewards.Values.First()) };
                    OnReceiveItem(receivedItems); //Analytics
                    DataManager.Instance.InventoryData.GrantItemByItemList(receivedItems);
                    StartCoroutine(ActionRewardGenerate(buttonTransform, receivedItems, false));
                    SelectingFriendID = "";
                    onComplete?.Invoke();
                }
                else
                {
                    onComplete?.Invoke();
                }
            }
            , null
        );
    }
}

public class GiftSendingEvent_CSEventData
{
    public string play_trigger;
    public string[] require_ticket;
    public int default_playtime;
    public int default_cooldown;
    public bool daily_repeatable;
    public Dictionary<string, int> action_rewards;
    public Dictionary<string, int>[] win_rewards;
    public int[] presents_thresholds;
}

public class GiftSendingEvent_CSPlayerData
{
    public DateTime? daily_timestamp;
    public DateTime? action_timestamp;
    public int remain_playtime;
    public int presents_received;
    public Dictionary<string, bool> sent_player_id_list;
    public Dictionary<string, bool> claimed_reward_index_list;
}
