﻿using UnityEngine.UI;

public class GiftSendingSelectFriendCell : BaseGameEventSelectFriendCell
{
    public Image SentGiftImage;

    public void ShowSentGift(bool isShow)
    {
        SentGiftImage.gameObject.SetActive(isShow);
    }
}
