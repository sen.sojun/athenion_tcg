﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[RequireComponent(typeof(GameEventGiftSending))]
public class GameEventGiftSendingUI : MonoBehaviour
{
    public CanvasGroup MainEventCanvas;
    public GameObject SelectFriendPanel;
    public Slider PresentsReceivedProgress;

    [Header("Prefabs")]
    public GameEventThresholdReward GiftingEventRewardItemPrefab;
    public GiftSendingSelectFriendCell GiftSendingSelectFriendCellPrefab;

    [Header("Transforms")]
    public Transform EventRewardTransform;
    public Transform FriendCellTransform;

    [Header("Texts")]
    public TMP_Text SelectFriendButtonText;
    public TMP_Text RemainingPlayTimeText;
    public TMP_Text SendText;
    public TMP_Text RewardsReceivedText;
    public TMP_Text NoFriendNoticeText;

    [Header("Buttons")]
    public Button SelectFriendButton;
    public Button SelectFriendFillBGButton;
    public Button SendGiftButton;
    public Button ReadMoreButton;

    [Header("Image UIs")]
    public Image CurrentPresentImage;
    public List<Sprite> PresentSpriteList = new List<Sprite>();

    private GameEventGiftSending _gameEventGiftSending;
    private Coroutine _countdownCoroutine;

    private void OnEnable()
    {
        _countdownCoroutine = StartCoroutine(Counting());
    }

    private void OnDisable()
    {
        if (_countdownCoroutine != null)
        {
            StopCoroutine(_countdownCoroutine);
        }
    }

    public Task<bool> TaskInit()
    {
        var t = new TaskCompletionSource<bool>();
        try
        {
            Init(() => t.TrySetResult(true));
        }
        catch (Exception e)
        {
            //t.TrySetException(e); //For thowing exception
            t.TrySetResult(false);
        }

        return t.Task;
    }

    public void Init(UnityAction onComplete = null)
    {
        _gameEventGiftSending = GetComponent<GameEventGiftSending>();
        
        #region Initialize Buttons
        ReadMoreButton.onClick.AddListener(() => ShowReadMore());
        SelectFriendButton.onClick.AddListener(() => ShowSelectFriendPanel());
        SelectFriendFillBGButton.onClick.AddListener(() => CloseSelectFriendPanel());
        SendGiftButton.onClick.AddListener(() => _gameEventGiftSending.InvokeSendGift(SendGiftButton.transform, () => UpdateUI()));
        #endregion

        UpdateUI();
        onComplete?.Invoke();
    }

    public void UpdateUI()
    {
        #region Main Present Sprite
        int presentSpriteIndex = 0;
        for (int i = 0; i < _gameEventGiftSending.GiftSendingEventData.presents_thresholds.Length; i++)
        {
            if (_gameEventGiftSending.PlayerEventData.presents_received < _gameEventGiftSending.GiftSendingEventData.presents_thresholds[i])
            {
                presentSpriteIndex = i;
                break;
            }
            else if (i == _gameEventGiftSending.GiftSendingEventData.presents_thresholds.Length - 1) // >= max threshold
            {
                presentSpriteIndex = i + 1;
            }
        }
        CurrentPresentImage.sprite = PresentSpriteList[presentSpriteIndex];
        #endregion

        #region Threshold Rewards
        foreach (GameEventThresholdReward item in EventRewardTransform.GetComponentsInChildren<GameEventThresholdReward>())
        {
            Destroy(item.gameObject);
        }
        for (int i = 0; i < _gameEventGiftSending.GiftSendingEventData.presents_thresholds.Length; i++)
        {
            int threshold = _gameEventGiftSending.GiftSendingEventData.presents_thresholds[i];
            GameEventThresholdReward giftingReward = Instantiate(GiftingEventRewardItemPrefab, EventRewardTransform);

            int rewardIndex = i;
            giftingReward.Init(
                new ItemData(_gameEventGiftSending.GiftSendingEventData.win_rewards[i].Keys.First(), _gameEventGiftSending.GiftSendingEventData.win_rewards[i].Values.First()),
                threshold,
                _gameEventGiftSending.PlayerEventData.claimed_reward_index_list.ContainsKey(i.ToString()),
                _gameEventGiftSending.CheckCanClaimReward(i),
                () => _gameEventGiftSending.CheckThresholdRewardDetail(rewardIndex)
                );
        }
        #endregion

        RewardsReceivedText.text = _gameEventGiftSending.PlayerEventData.presents_received.ToString();
        PresentsReceivedProgress.value = Mathf.Clamp((float)_gameEventGiftSending.PlayerEventData.presents_received / _gameEventGiftSending.GiftSendingEventData.presents_thresholds[_gameEventGiftSending.GiftSendingEventData.presents_thresholds.Length - 1], 0.025f, 0.90f);
        SelectFriendButtonText.text = string.IsNullOrWhiteSpace(_gameEventGiftSending.SelectingFriendID) ? LocalizationManager.Instance.GetText(_gameEventGiftSending.LocalizationKeyPrefix + "SELECT_FRIEND_BUTTON") : SelectFriendButtonText.text;
        RemainingPlayTimeText.text = string.Format("{0}/{1}", _gameEventGiftSending.PlayerEventData.remain_playtime, _gameEventGiftSending.GiftSendingEventData.default_playtime);
    }

    private IEnumerator Counting()
    {
        while (true)
        {
            if (!_gameEventGiftSending.PlayerEventData.action_timestamp.HasValue)
            {
                SendText.text = LocalizationManager.Instance.GetText("BUTTON_SEND");
            }
            else
            {
                TimeSpan cooldownTime = _gameEventGiftSending.GetActionCooldownTime();
                if (cooldownTime.TotalMilliseconds <= 0)
                {
                    SendText.text = LocalizationManager.Instance.GetText("BUTTON_SEND");
                }
                else
                {
                    SendText.text = GameHelper.GetCountdownText(cooldownTime);
                }
            }

            yield return new WaitForSeconds(1f);
        }
    }

    private void ShowReadMore()
    {
        string title = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventDetailHeaderKey);
        string message = LocalizationManager.Instance.GetText("EVENT_MOREDETAIL_" + _gameEventGiftSending.EventKey.ToUpper());

        PopupUIManager.Instance.ShowPopup_DetailWindow(title, message);
    }

    private void ShowSelectFriendPanel()
    {
        List<PlayerInfoData> friendList = DataManager.Instance.GetFriendList(FriendStatus.Friend);
        foreach(GiftSendingSelectFriendCell cell in FriendCellTransform.GetComponentsInChildren<GiftSendingSelectFriendCell>())
        {
            Destroy(cell.gameObject);
        }

        NoFriendNoticeText.gameObject.SetActive(friendList == null || friendList.Count == 0);
        foreach (PlayerInfoData item in friendList)
        {
            PlayerInfoUIData infoData = new PlayerInfoUIData(item);
            bool isSentGiftToday = _gameEventGiftSending.PlayerEventData.sent_player_id_list.ContainsKey(infoData.PlayerID)? _gameEventGiftSending.PlayerEventData.sent_player_id_list[infoData.PlayerID] : false;
            GenerateGiftSendingSelectFriendCell(
                GiftSendingSelectFriendCellPrefab.gameObject
                , infoData
                , isSentGiftToday
                , FriendCellTransform
                , () =>
                    {
                        if (isSentGiftToday)
                        {
                            return;
                        }

                        _gameEventGiftSending.SelectingFriendID = infoData.PlayerID;
                        SelectFriendButtonText.text = infoData.DisplayName;
                        CloseSelectFriendPanel();
                    }
                );
        }
        
        GameHelper.UITransition_FadeIn(SelectFriendPanel.gameObject);
    }

    private void CloseSelectFriendPanel()
    {
        GameHelper.UITransition_FadeOut(SelectFriendPanel.gameObject);
    }

    private GiftSendingSelectFriendCell GenerateGiftSendingSelectFriendCell(
          GameObject prefab, PlayerInfoUIData data, bool isSentGift, Transform target
        , UnityAction onClickRequest
    )
    {
        GameObject obj = Instantiate(prefab, target);
        GiftSendingSelectFriendCell cell = obj.GetComponent<GiftSendingSelectFriendCell>();

        cell.SetAvatar(data.AvatarID);
        cell.SetRank(data.RankID);
        cell.SetTitle(data.TitleID);
        cell.SetName(data.DisplayName);
        cell.ShowHighlight(_gameEventGiftSending.SelectingFriendID == data.PlayerID);
        cell.ShowSentGift(isSentGift);
        if (!isSentGift)
        {
            obj.GetComponent<Button>().onClick.AddListener(onClickRequest);
        }

        return cell;
    }
}
