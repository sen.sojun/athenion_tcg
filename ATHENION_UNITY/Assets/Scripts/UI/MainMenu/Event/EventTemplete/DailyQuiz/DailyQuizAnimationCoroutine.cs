﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class DailyQuizAnimationCoroutine : MiniGameAnimationCoroutine
{
    public Image ChibiSpriteToAnimate;
    public Sprite[] SpriteSequence;

    public override IEnumerator AnimationWinEventCoroutine(UnityAction onComplete)
    {
        ChibiSpriteToAnimate.sprite = SpriteSequence[0];
        yield return new WaitForSeconds(0.2f);
        ChibiSpriteToAnimate.sprite = SpriteSequence[1];
        yield return new WaitForSeconds(0.2f);
        ChibiSpriteToAnimate.sprite = SpriteSequence[2];

        onComplete?.Invoke();
    }
}
