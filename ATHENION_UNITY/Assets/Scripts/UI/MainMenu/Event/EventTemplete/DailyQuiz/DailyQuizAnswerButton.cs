﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Button))]
public class DailyQuizAnswerButton : MonoBehaviour, IPointerClickHandler
{
    #region Public Properties
    public TextMeshProUGUI TXTAnswer;
    public GameEventTemplateDailyQuiz EventManager;
    public Image ButtonImage;
    public Image CorrectSymbol;
    public Image WrongSymbol;
    #endregion

    #region Private Properties
    private int _answer_index;
    #endregion

    public void InitButton(int index, string text, Sprite imgSpr)
    {
        TXTAnswer.text = text;
        _answer_index = index;
        //ButtonImage.sprite = imgSpr;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        Button btn = GetComponent<Button>();
        if (btn != null)
        {
            if (btn.interactable)
            {
                EventManager.ConfirmAnswer(_answer_index);
            }
        }
    }

    public void SetAnswered(bool isCorrect)
    {
        CorrectSymbol.gameObject.SetActive(isCorrect);
        WrongSymbol.gameObject.SetActive(!isCorrect);

        //Gray out
        if (!isCorrect)
        {
            Color c = ButtonImage.color;
            c.r *= 0.5f;
            c.g *= 0.5f;
            c.b *= 0.5f;
            ButtonImage.color = c;
        }
    }
}
