﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;
using System.Linq;
using TMPro;
using UnityEngine.SceneManagement;
using DG.Tweening;
using System;
using System.Security.Cryptography;
using Spine;
using Spine.Unity;
using Karamucho;
using UnityEngine.Events;
using Random = UnityEngine.Random;

public class GameEventTemplateDailyQuiz : BaseGameEventTemplate
{
    #region enums
    public enum AssetName
    {
        None,
        BackgroundImage,
        HeaderBanner,
        QuestionBoard,
        CooldownBorder,
        BTMDecorMid,
        BTMDecorLeft,
        BTMDecorRight,
        ChoiceButton,
        CheckIcon,
        ZoomZoom,


        Last
    }
    public enum DailyQuizStatus
    {
        Idle,
        Processing,
        Renewing
    }
    #endregion
    #region Public Properties
    public GameObject UICanvas;
    public GameObject PrefabAnswerBTN;
    public GameObject CumulativeRewardsBar;
    public EventGoalRewardPackage CumulativeRewardPrefab;
    public Slider ProgressBar;
    public MiniGameAnimationCoroutine MiniGameAnimationCoroutine;

    [Header("UI Ref")]
    public Button BTNHeaderBanner;
    public Button BTNHint;
    public Image BackgroundImage;
    public Image HeaderBanner;
    public Image BlackBoardImage;
    public Image QuestionImage;
    public Image BTMDecor_Mid;
    public Image BTMDecor_Left;
    public Image BTMDecor_Right;
    public Image CorrectRewardImage;
    public Image CooldownBorder;
    public Image IMGZoomUpImage;
    public Image IMGZoomZoomIcon;
    public DailyQuizAnswerButton[] StaticAnswerButtons;
    public TextMeshProUGUI TXTCorrectRewardAmount;
    public TextMeshProUGUI TXTHeaderBanner;
    public TextMeshProUGUI TXTObjective;
    public TextMeshProUGUI TXTQuestion;
    public TextMeshProUGUI TXTCooldown;
    public TextMeshProUGUI TXTDetailHeader;
    public TextMeshProUGUI TXTDetailBody;
    #endregion
    #region Private Properties
    private DailyQuizStatus _status;
    private Dictionary<int, QuizQuestionData> _questionList;
    private Dictionary<string, EventGoalRewardPackage> _uiCumulativeRewList;
    private List<ItemData> _currentCorrectReward;
    private Dictionary<string, List<ItemData>> _cumulativeRewardList;
    private List<GameObject> _ansButtonList;
    private Dictionary<int, int> _playerClearedQuestionAndAnswerDict;
    private List<int> _playerAnswers;
    private List<ItemData> _currentShowReward;
    private GameEventInfo _eventInfo;
    private DateTime _playerTimestamp;
    private DateTime _playerDailyTimestamp;
    private Coroutine _coroutineCDTimer;
    private int _playerQuestionIndex;
    private int _playerAnswerIndex;
    private int _playerDailyCount;
    private int _eventCooldown;
    private int _dailyQuestionLimit;
    private string _textColor;
    private string _rewardButtonPic;
    private static readonly string PrefabResourcePath = "Events/{0}/Prefabs/";
    #endregion

    public override void Init(string eventKey, string sceneName, UnityAction onComplete = null, UnityAction onFail = null)
    {
        base.Init(eventKey, sceneName);

        GameEventDB gameEventDB = DataManager.Instance.GetGameEventDB();
        _eventInfo = gameEventDB.GetGameEventInfo()[_eventKey];
        DataManager.Instance.ExcecuteEventCloudScript("GetDailyQuizData", new { EventKey = _eventKey }, 
            (result) => 
            {
                OnSuccessGetData(result);
                onComplete?.Invoke();
            }
            , OnCommonError
        );

        _eventKey = eventKey;
    }

    private void OnCommonError(string arg0)
    {
        PopupUIManager.Instance.Show_SoftLoad(false);
        PopupUIManager.Instance.ShowPopup_Error(LocalizationManager.Instance.GetText("EVENT_ERROR"),
                                                LocalizationManager.Instance.GetText("EVENT_GENERAL_ERROR"));
        _status = DailyQuizStatus.Idle;
    }

    private void OnSuccessGetData(CloudScriptResultProcess result)
    {
        Dictionary<string, object> rawPlayerData = JsonConvert.DeserializeObject<Dictionary<string, object>>(result.CustomData["playerData"]);
        Dictionary<string, object> rawEventData = JsonConvert.DeserializeObject<Dictionary<string, object>>(result.CustomData["eventData"]);

        //Init player data;
        UpdatePlayerData(rawPlayerData);

        //Init event data;
        int.TryParse(rawEventData["ans_cooldown"].ToString(), out _eventCooldown);
        int.TryParse(rawEventData["daily_chance"].ToString(), out _dailyQuestionLimit);
        _textColor = rawEventData["textcolor"].ToString();
        Dictionary<string, object> rawQuestionList = JsonConvert.DeserializeObject<Dictionary<string, object>>(rawEventData["question_list"].ToString());
        _questionList = new Dictionary<int, QuizQuestionData>();
        foreach (KeyValuePair<string, object> rawQuestion in rawQuestionList)
        {
            Dictionary<string, object> items = JsonConvert.DeserializeObject<Dictionary<string, object>>(rawQuestion.Value.ToString());
            List<string> answerList = new List<string>();
            int.TryParse(rawQuestion.Key, out int index);
            string question = LocalizationManager.Instance.GetText((items["q_prefix"].ToString()+"_"+_eventKey).ToUpper());
            List<object> rawAnswerList = JsonConvert.DeserializeObject<List<object>>(items["ans_list"].ToString());
            foreach(object answer in rawAnswerList)
            {
                string answerLocalizedText = LocalizationManager.Instance.GetText((items["q_prefix"].ToString() + "_" + answer.ToString() + "_" + _eventKey).ToUpper());
                answerList.Add(answerLocalizedText);
            }

            string sprName = items["q_pic"].ToString();
            QuizQuestionData qData = new QuizQuestionData(question, answerList, sprName, null);
            _questionList.Add(index, qData);
        }

        Dictionary<string, object> rawCorrectRewards = JsonConvert.DeserializeObject<Dictionary<string, object>>(rawEventData["correct_rewards"].ToString());
        _currentCorrectReward = new List<ItemData>();
        foreach (KeyValuePair<string, object> rawData in rawCorrectRewards)
        {
            Dictionary<string, object> rewardDataList = JsonConvert.DeserializeObject<Dictionary<string, object>>(rawData.Value.ToString());

            DateTime effectiveDate = (DateTime)rewardDataList["EffectiveDate"];
            DateTime expireDate = (DateTime)rewardDataList["ExpireDate"];
            if (_playerDailyTimestamp >= effectiveDate && _playerDailyTimestamp < expireDate)
            {
                List<ItemData> rewardData = new List<ItemData>();
                List<Dictionary<string, object>> rawItemList = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(rewardDataList["RewardList"].ToString());
                foreach (Dictionary<string, object> data in rawItemList)
                {
                    string rewardID = data["itemID"].ToString();
                    int.TryParse(data["amount"].ToString(), out int rewardAmt);
                    rewardData.Add(new ItemData(rewardID, rewardAmt));
                }
                _currentCorrectReward = rewardData;
                _rewardButtonPic = rawData.Key;
                break;
            }
        }
        
        Dictionary<string, object> rawCumulativeRewardList = JsonConvert.DeserializeObject<Dictionary<string, object>>(rawEventData["cumulative_rewards"].ToString());
        _cumulativeRewardList = new Dictionary<string, List<ItemData>>();
        foreach (KeyValuePair<string, object> rawCRewardData in rawCumulativeRewardList)
        {
            List<Dictionary<string, object>> cRewardDataList = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(rawCRewardData.Value.ToString());

            List<ItemData> cRewardData = new List<ItemData>();
            foreach (Dictionary<string, object> data in cRewardDataList)
            {
                string rewardID = data["itemID"].ToString();
                int.TryParse(data["amount"].ToString(), out int rewardAmt);
                cRewardData.Add(new ItemData(rewardID, rewardAmt));
            }

            _cumulativeRewardList.Add(rawCRewardData.Key, cRewardData);
        }

        InitUI();
        UpdateUI();

        _status = DailyQuizStatus.Idle;
        PopupUIManager.Instance.Show_SoftLoad(false);
    }

    public void ConfirmAnswer(int index)
    {
        if (_status != DailyQuizStatus.Idle || _playerClearedQuestionAndAnswerDict.ContainsKey(_playerQuestionIndex)) return;

        /*if (_playerDailyCount >= _dailyQuestionLimit)
        {
            
            PopupUIManager.Instance.ShowPopup_Error(LocalizationManager.Instance.GetText("EVENT_ERROR"),
                                        LocalizationManager.Instance.GetText("EVENT_DAILY_LIMIT_REACHED_" + _eventKey));
            
        }
        else*/ if (RemainingCooldown() > 0)
        {
            PopupUIManager.Instance.ShowPopup_ConfirmOnlyWithWarning(LocalizationManager.Instance.GetText("EVENT_COOLDOWN"),
                                        LocalizationManager.Instance.GetText("EVENT_ONCOOLDOWN"));
        }
        /*else if (_playerClearedQuestion.Contains(_playerQuestionIndex))
        {
            
            PopupUIManager.Instance.ShowPopup_Error(LocalizationManager.Instance.GetText("EVENT_ERROR"),
                                        LocalizationManager.Instance.GetText("EVENT_ALRDY_CLEARED_" + _eventKey));
            
        }*/
        else if (_playerAnswers.Contains(index))
        {
            PopupUIManager.Instance.ShowPopup_Error(LocalizationManager.Instance.GetText("EVENT_ERROR"),
                                        LocalizationManager.Instance.GetText("EVENT_ALRDY_ANS_DETAIL_" + _eventKey.ToUpper()));
        }
        else
        {
            PopupUIManager.Instance.ShowPopup_SureCheck(
                        LocalizationManager.Instance.GetText("EVENT_CONFIRM_ANS_TITLE_" + _eventKey.ToUpper()),
                        LocalizationManager.Instance.GetText("EVENT_CONFIRM_ANS_DETAIL_" + _eventKey.ToUpper())
                        ,
                        delegate ()
                        {
                            SendAnswerQuestion(index);
                        },
                        null
                        );
        }
    }

    public void SendAnswerQuestion(int index)
    {
        PopupUIManager.Instance.Show_SoftLoad(true);
        _status = DailyQuizStatus.Processing;
        DataManager.Instance.ExcecuteEventCloudScript("AnswerDailyQuiz", new { EventKey = _eventKey, Index = index }, OnSuccessAnswer, OnCommonError);
        _playerAnswerIndex = index;
    }

    private void OnSuccessAnswer(CloudScriptResultProcess result)
    {
        _status = DailyQuizStatus.Idle;
        Dictionary<string, object> rawPlayerData = JsonConvert.DeserializeObject<Dictionary<string, object>>(result.CustomData["playerData"]);
        UpdatePlayerData(rawPlayerData);

        //Show Result
        if (_playerClearedQuestionAndAnswerDict.ContainsKey(_playerQuestionIndex) && _playerClearedQuestionAndAnswerDict[_playerQuestionIndex] == _playerAnswerIndex)
        {
            List<ItemData> allReward = _currentCorrectReward;
            if (_cumulativeRewardList.ContainsKey(_playerClearedQuestionAndAnswerDict.Count.ToString()))
            {
                allReward.AddRange(_cumulativeRewardList[_playerClearedQuestionAndAnswerDict.Count.ToString()]);
            }
           
            OnReceiveItem(allReward);
            OnEventComplete();
            foreach (ItemData itemData in _currentCorrectReward)
            {
                DataManager.Instance.InventoryData.GrantItemByItemID(itemData.ItemID, itemData.Amount);
            }

            SoundManager.PlaySFX(SoundManager.SFX.SFX_RPS_VICTORY);
            if (MiniGameAnimationCoroutine != null)
            {
                StartCoroutine(MiniGameAnimationCoroutine.AnimationWinEventCoroutine(() =>
                {
                    PopupUIManager.Instance.ShowPopup_GotRewardAnimated(LocalizationManager.Instance.GetText("EVENT_CONGRATS")
                                                                  , LocalizationManager.Instance.GetText("EVENT_YOUVEGOT")
                                                                  , allReward)
                                                                  .SetAnimationEase(Ease.OutExpo)
                                                                  .SetDelay(0.1f)
                                                                  .SetAnimationDuration(0.25f)
                                                                  .SetMaxScale(8f)
                                                                  ;
                }));
            }
            else
            {
                PopupUIManager.Instance.ShowPopup_GotRewardAnimated(LocalizationManager.Instance.GetText("EVENT_CONGRATS")
                                                                  , LocalizationManager.Instance.GetText("EVENT_YOUVEGOT")
                                                                  , allReward)
                                                                  .SetAnimationEase(Ease.OutExpo)
                                                                  .SetDelay(0.1f)
                                                                  .SetAnimationDuration(0.25f)
                                                                  .SetMaxScale(8f)
                                                                  ;
            }
        }
        else
        {
            if(MiniGameAnimationCoroutine != null)
            {
                StartCoroutine(MiniGameAnimationCoroutine.AnimationLoseEventCoroutine(() => 
                {
                    PopupUIManager.Instance.ShowPopup_Error(LocalizationManager.Instance.GetText("EVENT_ERROR"),
                            LocalizationManager.Instance.GetText("EVENT_WRONG_ANS_DETAIL_" + _eventKey.ToUpper()));
                }));
            }
            else
            {
                PopupUIManager.Instance.ShowPopup_Error(LocalizationManager.Instance.GetText("EVENT_ERROR"),
                            LocalizationManager.Instance.GetText("EVENT_WRONG_ANS_DETAIL_" + _eventKey.ToUpper()));
            }
        }
        
        UpdateUI();
        PopupUIManager.Instance.Show_SoftLoad(false);
    }

    private void OnEnable()
    {
        _coroutineCDTimer = StartCoroutine(CooldownTimer());
    }

    private void OnDisable()
    {
        if(_coroutineCDTimer != null)
        {
            StopCoroutine(_coroutineCDTimer);
        }
    }

    private void InitUI()
    {
        BTNHeaderBanner.onClick.AddListener(() => ShowReadMore());
        BTNHint.onClick.AddListener(() => ShowFacebookGroupLinkPopup());
    }

    private void UpdateUI()
    {
        QuizQuestionData currentQ;
        if (_playerQuestionIndex < _questionList.Count)
        {
            currentQ = _questionList[_playerQuestionIndex];
        }
        else
        {
            currentQ = _questionList.Values.Last();
        }

        TXTQuestion.text = _playerClearedQuestionAndAnswerDict.ContainsKey(_playerQuestionIndex) ? LocalizationManager.Instance.GetText("EVENT_WAIT_NEXT_DAY_FOR_NEW_Q_" + _eventKey.ToUpper()) : currentQ.Question;
        //QuestionImage.sprite = SpriteResourceHelper.LoadSprite(currentQ.QuestionImage, SpriteResourceHelper.SpriteType.ICON_Avatar, true); //For match of the day FEB2020
        //if(QuestionImage.IsActive())
        //{
        //    SetUIImage(IMGZoomZoomIcon, AssetName.ZoomZoom.ToString());
        //}
        //SetUIImage(IMGZoomUpImage, currentQ.QuestionImage);
        
        int btnIndex = 0;
        foreach (string choice in currentQ.AnswerList)
        {
            //For match of the day FEB2020
            //Sprite spr = SpriteResourceHelper.LoadSprite(choice, SpriteResourceHelper.SpriteType.ICON_Avatar, true);

            StaticAnswerButtons[btnIndex].InitButton(btnIndex, LocalizationManager.Instance.GetText(choice.ToUpper()), null);
            if (_playerClearedQuestionAndAnswerDict.ContainsKey(_playerQuestionIndex) && _playerClearedQuestionAndAnswerDict[_playerQuestionIndex] == btnIndex)
            {
                StaticAnswerButtons[btnIndex].SetAnswered(true);
            }
            else if (_playerAnswers.Contains(btnIndex))
            {
                StaticAnswerButtons[btnIndex].SetAnswered(false);
            }

            btnIndex++;
        }
    }

    private void SetUIImage(Image uiImage, string assetName)
    {
        if(uiImage == null)
        {
            return;
        }

        Sprite spr = SpriteResourceHelper.LoadSubEventImage(_eventKey, assetName, true);

        uiImage.enabled = (spr != null);
        uiImage.sprite = spr;
    }

    private void SetEventText(TextMeshProUGUI txt, string txtString, string coloCode, int alpha, bool completeString = false)
    {
        if (txt == null) return;

        if (completeString)
            txt.text = txtString;
        else
            txt.text = LocalizationManager.Instance.GetText(txtString);

        txt.color = CodeToColor(coloCode, alpha);
    }

    Color CodeToColor(string colorCode, int alpha)
    {
        int red = Convert.ToInt32(colorCode.Substring(0, 2), 16);
        int blue = Convert.ToInt32(colorCode.Substring(2, 2), 16);
        int green = Convert.ToInt32(colorCode.Substring(4, 2), 16);
        Color c = new Color32(Convert.ToByte(red), Convert.ToByte(blue), Convert.ToByte(green), 255);
        return c;
    }

    private void UpdatePlayerData(Dictionary<string, object> rawData)
    {
        //Init player data;
        int.TryParse(rawData["question_index"].ToString(), out _playerQuestionIndex);

        _playerClearedQuestionAndAnswerDict = new Dictionary<int, int>();
        List<object> rawClearedQuestion = JsonConvert.DeserializeObject<List<object>>(rawData["clearedquestion"].ToString());
        foreach (object cleared in rawClearedQuestion)
        {
            Dictionary<string, int> clearedQInfo = JsonConvert.DeserializeObject<Dictionary<string, int>>(cleared.ToString());
            _playerClearedQuestionAndAnswerDict[clearedQInfo["qIndex"]] = clearedQInfo["ansIndex"];
        }
        _playerAnswers = new List<int>();
        List<object> rawAnswers = JsonConvert.DeserializeObject<List<object>>(rawData["answered"].ToString());
        foreach (object answer in rawAnswers)
        {
            int.TryParse(answer.ToString(), out int answeredNo);
            _playerAnswers.Add(answeredNo);
        }
        int.TryParse(rawData["daily_count"].ToString(), out _playerDailyCount);
        _playerTimestamp = (DateTime)rawData["timestamp"]; 
        _playerDailyTimestamp = (DateTime)rawData["daily_timestamp"]; 
    }

    private float RemainingCooldown()
    {
        DateTime curDate = DateTimeData.GetDateTimeUTC();
        return (float)(_playerTimestamp - curDate).TotalSeconds + _eventCooldown;
    }

    IEnumerator CooldownTimer()
    {
        while (true)
        {
            if (_playerClearedQuestionAndAnswerDict.ContainsKey(_playerQuestionIndex))
            {
                SetEventText(TXTCooldown, "", _textColor, 255);
                break;
            }

            float remainingCooldown = RemainingCooldown();
            if (remainingCooldown > 0)
            {
                int h, m, s;
                h = Mathf.FloorToInt(remainingCooldown / 3600);
                m = Mathf.FloorToInt((remainingCooldown - h * 3600) / 60);
                s = Mathf.FloorToInt(remainingCooldown % 60);

                if (h > 0)
                {
                    SetEventText(TXTCooldown, string.Format("{0}:{1}:{2}", h.ToString("D2"), m.ToString("D2"), s.ToString("D2")), _textColor, 255);
                }
                else
                {
                    SetEventText(TXTCooldown, string.Format("{0}:{1}", m.ToString("D2"), s.ToString("D2")), _textColor, 255);
                }
            }
            else
            {
                SetEventText(TXTCooldown, "READY!", _textColor, 255);
            }

            yield return new WaitForEndOfFrame();
        }
    }

    public override void Close()
    {
        UnloadEventScene();
    }

    public void UnloadEventScene()
    {
        SceneManager.UnloadSceneAsync(SceneName);
    }

    public void ShowCorrectRewardDetail()
    {
        List<ItemData> itemList = _currentCorrectReward;

        string title = LocalizationManager.Instance.GetText(("EVENT_CORRECT_REWARD_TITLE_" + _eventKey).ToUpper());
        string detail = LocalizationManager.Instance.GetText(("EVENT_CORRECT_REWARD_DETAIL_" + _eventKey).ToUpper());
        PopupUIManager.Instance.ShowPopup_PackDetail_Short(title, detail, itemList);
    }

    private void ShowReadMore()
    {
        string title = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventDetailHeaderKey);
        string message = LocalizationManager.Instance.GetText("EVENT_MOREDETAIL_" + _eventKey.ToUpper());

        PopupUIManager.Instance.ShowPopup_DetailWindow(title, message);
    }

    private void ShowFacebookGroupLinkPopup()
    {
        PopupUIManager.Instance.ShowPopup_TwoChoices_Tap_BlankArea_Cancel(
            LocalizationManager.Instance.GetText("EVENT_TEXT_TITLE_FB_GROUP_GLOBAL_CELEBRATION_QUIZ_2020"),
            LocalizationManager.Instance.GetText("EVENT_TEXT_BODY_FB_GROUP_GLOBAL_CELEBRATION_QUIZ_2020"),
            LocalizationManager.Instance.GetText("BUTTON_FACEBOOK_TH"),
            LocalizationManager.Instance.GetText("BUTTON_FACEBOOK_GLOBAL")
            , delegate ()
            {
                Application.OpenURL(GameHelper.ATNFacebookTHLink);
            }
            , delegate ()
            {
                Application.OpenURL(GameHelper.ATNFacebookGlobalLink);
            }
            , false
            , false
            );
    }
}

public class QuizQuestionData
{
    public string Question;
    public List<string> AnswerList;
    public string QuestionImage;
    public List<int> CorrectAnswer;

    public QuizQuestionData()
    {

    }

    public QuizQuestionData(string question, List<string> answers, string img, List<int> correctAns)
    {
        Question = question;
        AnswerList = answers;
        QuestionImage = img;
        CorrectAnswer = correctAns;
    }

}
