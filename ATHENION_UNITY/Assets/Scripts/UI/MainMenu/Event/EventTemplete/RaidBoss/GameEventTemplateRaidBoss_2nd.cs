﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using GameEvent;
using Karamucho;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class GameEventTemplateRaidBoss_2nd : BaseGameEventTemplate
{
    #region Static Values
    #region CloudScript Key
    private static readonly string AttackDamageKey = "attack_damage";
    private static readonly string ParticipateRewardKey = "participate_reward";
    private static readonly string TierDataKey = "tier_data";
    private static readonly string RaidBossHPKey = "raid_boss_hp";
    private static readonly string BossHPKey = "boss_hp";
    private static readonly string RankKey = "rank";
    private static readonly string ReceivedRewardKey = "received_reward";
    #endregion

    #region Localization Key
    private static readonly string EventCurrentDamageDescKey = "EVENT_CURRENT_DAMAGE_DESC_";
    private static readonly string EventActionDescKey = "EVENT_ACTION_DESC_";
    private static readonly string EventCurrentDamageUnitKey = "EVENT_BOSS_DAMAGE_UNIT_";
    private static readonly string EventRewardTitle = "EVENT_RAID_BOSS_REWARD_TITLE_";
    private static readonly string EventRewardMessage = "EVENT_RAID_BOSS_REWARD_MESSAGE_";
    private static readonly string EventBossHPDisplayKey = "EVENT_RAID_BOSS_HP_";
    #endregion

    #region Resource Key
    // Prefix Key.
    private static readonly string EventRankPrefixKey = "RANK_";
    private static readonly string EventBossImagePrefixKey = "BOSS_RANK_";
    private static readonly string EventRewardBoxPrefixKey = "REWARD_BOX_";

    // Full Key.
    private static readonly string EventRaidBossImageKey = "BOSS_IMG";
    private static readonly string EventRewardBoxHighlight = "REWARD_BOX_HIGHLIGHT";

    // Prefab Key
    private static readonly string EventRaidBossChestBox = "EventRaidBossRewardChest";
    #endregion
    #endregion

    #region UI Properties
    [Header("Static Image UI Element List")]
    [SerializeField] private List<Image> _staticNativeSizeImageList;
    [SerializeField] private List<Image> _staticNonNativeSizeImageList;
    [SerializeField] private List<Image> _eventTicketImageList;
    [SerializeField] private Image _eventMainBgImage;
    [SerializeField] private Image _eventHeaderBgImage;

    [Header("Dynamic Image UI Element List")]
    [SerializeField] private Image _eventRankImage;
    [SerializeField] private Image _eventBossImage;

    [Header("Text UI Elements")]
    [SerializeField] private TextMeshProUGUI _eventHeaderText;
    [SerializeField] private TextMeshProUGUI _eventTicketText;
    [SerializeField] private TextMeshProUGUI _eventCurrentDamageText;
    [SerializeField] private List<TextMeshProUGUI> _eventActionDescriptionTextList;
    [SerializeField] private List<TextMeshProUGUI> _eventActionDamageTextList;
    [SerializeField] private List<TextMeshProUGUI> _eventActionDamageUnitTextList;
    [SerializeField] private List<TextMeshProUGUI> _eventTicketTextList;

    [Header("Button UI Elements")]
    [SerializeField] private Button _eventCloseButton;
    [SerializeField] private Button _eventReadMoreButton;
    [SerializeField] private List<EventRaidBossAction> _actionButtonList;

    [Header("Slider UI Elements")]
    [SerializeField] private List<Slider> _eventBossHpSliderList;

    [Header("Other UI Elements")]
    [SerializeField] private RectTransform _eventRewardBoxParent;

    [Header("Other Setting")]
    [SerializeField] private List<bool> ActionDefaultDiagonalList;

    [Header("Particle Setting")]
    [SerializeField] private RaidAttackEffect _attackEffect;
    [SerializeField] private List<GameObject> _movingParticleList;
    [SerializeField] private List<GameObject> _burstParticleList;
    [SerializeField] private List<GameObject> _slashParticleList;
    [SerializeField] private List<Transform> _referencePosition;
    [SerializeField] private bool _activateParticle;
    [SerializeField] private SoundManager.SFX _effectStart;
    [SerializeField] private SoundManager.SFX _effectEnd;
    #endregion

    #region Public Properties
    [Header("CloudScript Function Name")]
    [SerializeField] private string _initCloudScriptFunctionName;
    [SerializeField] private string _getRewardCloudScriptFunctionName;
    [SerializeField] private string _actionCloudScriptFunctionName;
    #endregion

    #region Private Properties

    private List<List<ItemData>> _tierRewardList;                                       // Reward list in each damage rank of current boss.
    private List<int> _tierBossHpList;                                                  // Boss Hp list in each rank of current boss.
    private List<string> _rankList;                                                     // Rank list, ex. {D,C,B,A,S}, etc.

    private List<List<ItemData>> _participateRewardList;
    private List<int> _actionDamageValueList;
    private int _currentRaidBossHP;
    private ItemData _ticket;

    private List<EventRaidBossRewardChest> _rewardChestButtonScripts;
    private int _rewardClearedCount;
    private bool _isActionTaken;

    private bool _participateDone;

    private List<Vector3> _particleDefaultPosList;
    private List<Vector3> _wayPointReferenceList;

    private Coroutine _coroutine;
    private Vector3 _defaultCameraPosition;

    private Image _selectedDefeatImage;

    private Dictionary<string, object> _receivedRewardDictionary;
    #endregion

    #region Methods
    #region Override Methods
    public override void Init(string eventKey, string sceneName, UnityAction onComplete = null, UnityAction onFail = null)
    {
        base.Init(eventKey, sceneName);

        //PopupUIManager.Instance.Show_SoftLoad(true);

        DataManager.Instance.ExcecuteEventCloudScript(
            _initCloudScriptFunctionName
            , new { EventKey = _eventKey }
            , result =>
            {
                DataManager.Instance.LoadInventory(null, null);

                bool isSuccess = true;

                // Data Initialize.
                isSuccess &= InitData(result);

                if (isSuccess)
                {
                    // UI Initialize.
                    InitUI();
                    GenerateUI();
                }
                else
                {
                    string errMsg = "GameEventTemplateRaidBoss_2nd/Init: Data from cloud-script is not found.";
                    OnLoadSceneFailed(errMsg);
                }

                //GameHelper.UITransition_FadeIn(FadeIn.gameObject, () => PopupUIManager.Instance.Show_SoftLoad(false));
                onComplete?.Invoke();
            }
            , errMsg =>
            {
                PopupUIManager.Instance.Show_SoftLoad(false);
                OnLoadSceneFailed(errMsg);
            }
        );
    }
    #endregion

    #region Init Methods

    protected override bool InitData(CloudScriptResultProcess eventData)
    {
        bool isSuccess = true;

        #region Base Data

        isSuccess &= base.InitData(eventData);
        if (!isSuccess) return false;

        #endregion

        #region Boss Tier Data

        // Create new data instance for boss data.
        _tierRewardList = new List<List<ItemData>>();
        _tierBossHpList = new List<int>();
        _rankList = new List<string>();

        // Try to deserialize boss tier JSON data for this event. 
        isSuccess &= GameHelper.TryDeserializeJSONStr(EventDataDict[TierDataKey].ToString(), out List<object> tierDataList);
        if (!isSuccess) return false;

        // Add boss data.
        foreach (object tierItem in tierDataList)
        {
            // Check data accessible.
            isSuccess &= GameHelper.TryDeserializeJSONStr(tierItem.ToString(), out Dictionary<string, object> tierDictionary);
            isSuccess &= int.TryParse(tierDictionary[BossHPKey].ToString(), out int bossHp);
            isSuccess &= GameHelper.TryDeserializeJSONStr(tierDictionary[GameEventTemplateKeys.RewardListKey].ToString(), out List<object> rewardObjectList);
            isSuccess &= tierDictionary.ContainsKey(RankKey);
            if (!isSuccess) return false;

            // Set data value to variable.
            _tierBossHpList.Add(bossHp);
            _rankList.Add(tierDictionary[RankKey].ToString());

            List<ItemData> rewardList = new List<ItemData>();
            foreach (object rewardItem in rewardObjectList)
            {
                isSuccess &= GameHelper.TryDeserializeJSONStr(rewardItem.ToString(), out Dictionary<string, object> rewardObjectDictionary);
                isSuccess &= rewardObjectDictionary.ContainsKey(GameEventTemplateKeys.ItemIDKey);
                isSuccess &= rewardObjectDictionary.ContainsKey(GameEventTemplateKeys.ItemAmountKey);
                if (!isSuccess) return false;

                isSuccess &= int.TryParse(rewardObjectDictionary[GameEventTemplateKeys.ItemAmountKey].ToString(), out int rewardAmount);
                if (!isSuccess) return false;

                rewardList.Add(new ItemData(
                    rewardObjectDictionary[GameEventTemplateKeys.ItemIDKey].ToString()
                    , rewardAmount)
                );
            }
            _tierRewardList.Add(rewardList);
        }
        #endregion

        #region Participate Reward Data

        isSuccess &= GameHelper.TryDeserializeJSONStr(EventDataDict[ParticipateRewardKey].ToString(), out List<object> participateRewardList);
        if (!isSuccess) return false;

        _participateRewardList = new List<List<ItemData>>();

        foreach (object item in participateRewardList)
        {
            isSuccess &= GameHelper.TryDeserializeJSONStr(item.ToString(), out Dictionary<string, object> participateRewardDict);
            if (!isSuccess) return false;

            isSuccess &= participateRewardDict.ContainsKey(GameEventTemplateKeys.RewardListKey);
            if (!isSuccess) return false;

            isSuccess &= GameHelper.TryDeserializeJSONStr(participateRewardDict[GameEventTemplateKeys.RewardListKey].ToString(), out List<object> rewardList);
            if (!isSuccess) return false;

            List<ItemData> itemList = new List<ItemData>();

            foreach (object reward in rewardList)
            {
                isSuccess &= GameHelper.TryDeserializeJSONStr(reward.ToString(), out Dictionary<string, object> rewardDictionary);
                isSuccess &= rewardDictionary.ContainsKey(GameEventTemplateKeys.ItemIDKey);
                isSuccess &= rewardDictionary.ContainsKey(GameEventTemplateKeys.ItemAmountKey);
                if (!isSuccess) return false;

                isSuccess &= int.TryParse(rewardDictionary[GameEventTemplateKeys.ItemAmountKey].ToString(), out int itemAmount);
                if (!isSuccess) return false;

                itemList.Add(new ItemData(rewardDictionary[GameEventTemplateKeys.ItemIDKey].ToString(), itemAmount));
            }

            _participateRewardList.Add(itemList);
        }

        #endregion

        #region Action Damage and Current Raid Boss HP Data

        isSuccess &= GameHelper.TryDeserializeJSONStr(EventDataDict[AttackDamageKey].ToString(), out _actionDamageValueList);
        isSuccess &= int.TryParse(EventDataDict[RaidBossHPKey].ToString(), out _currentRaidBossHP);
        if (!isSuccess) return false;

        #endregion

        #region Ticket Event Data
        switch (PlayTriggerType)
        {
            case GameEventStartType.Ticket:
                {
                    string ticketId = GameEventTemplateKeys.TicketIDPrefixKey + _eventThemeInfo.Theme;
                    int ticketAmount = DataManager.Instance.InventoryData.GetAmountByItemID(ticketId);

                    _ticket = new ItemData(ticketId, ticketAmount);
                    break;
                }
        }
        #endregion

        #region Rank Data

        for (int i = 0; i < _tierBossHpList.Count; i++)
        {
            if (_currentRaidBossHP <= _tierBossHpList[i])
                _rewardClearedCount = i;
            else
                break;
        }

        #endregion

        #region Player Data

        isSuccess &= GameHelper.TryDeserializeJSONStr(eventData.CustomData[GameEventTemplateKeys.PlayerDataKey], out Dictionary<string, object> playerDataDictionary);
        if (!isSuccess) return false;

        isSuccess &= GameHelper.TryDeserializeJSONStr(playerDataDictionary[ReceivedRewardKey].ToString(), out _receivedRewardDictionary);
        if (!isSuccess) return false;
        #endregion

        #region Other Data

        // Default Particle position.
        _particleDefaultPosList = new List<Vector3>();
        foreach (GameObject particleItem in _movingParticleList)
        {
            _particleDefaultPosList.Add(particleItem.transform.position);
        }

        // Way Point for particle.

        _wayPointReferenceList = new List<Vector3>();
        foreach (Transform transform1 in _referencePosition)
        {
            _wayPointReferenceList.Add(transform1.position);
        }
        #endregion

        return true;
    }

    /// <summary>
    /// Call this method to initialize event UI.
    /// </summary>
    private void InitUI()
    {
        #region Text UI

        // Base Text.
        if (_eventHeaderText != null) _eventHeaderText.text = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventHeaderKey + _eventKey);

        // Custom Text.
        if (_eventCurrentDamageText != null) _eventCurrentDamageText.text = (_tierBossHpList[0] - _currentRaidBossHP).ToString("n0") + " " + LocalizationManager.Instance.GetText(EventCurrentDamageUnitKey + _eventKey);

        for (int i = 0; i < _eventActionDescriptionTextList.Count; i++)
        {
            _eventActionDescriptionTextList[i].text = LocalizationManager.Instance.GetText(EventActionDescKey + i + "_" + _eventKey);
        }

        for (int i = 0; i < _eventActionDamageTextList.Count; i++)
        {
            _eventActionDamageTextList[i].text = _actionDamageValueList[i].ToString();
        }

        // Text Unit (Damage, Pts, etc.)
        foreach (TextMeshProUGUI text in _eventActionDamageUnitTextList)
        {
            text.text = LocalizationManager.Instance.GetText(EventCurrentDamageUnitKey + _eventKey);
        }
        #endregion

        #region Static and Dynamic Image UI

        //_eventMainBgImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, _eventThemeInfo.Theme + GameEventTemplateKeys.EventMainBackgroundSuffix, true) == null ? SpriteResourceHelper.LoadSprite(_eventThemeInfo.Theme, SpriteResourceHelper.SpriteType.GAME_Event) : SpriteResourceHelper.LoadSubEventImage(_eventKey, _eventThemeInfo.Theme + GameEventTemplateKeys.EventMainBackgroundSuffix);

        _eventHeaderBgImage.sprite = SpriteResourceHelper.LoadSprite(_eventThemeInfo.Theme + GameEventTemplateKeys.EventHeaderBannerSuffix, SpriteResourceHelper.SpriteType.GAME_Event);

        _eventBossImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, EventBossImagePrefixKey + _rankList[_rewardClearedCount]);
        _eventRankImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, EventRankPrefixKey + _rankList[_rewardClearedCount]);

        //_eventMainBgImage.enabled = true;
        _eventHeaderBgImage.enabled = true;
        _eventBossImage.enabled = true;
        _eventRankImage.enabled = true;

        if (_staticNativeSizeImageList != null)
        {
            for (int i = 0; i < _staticNativeSizeImageList.Count; i++)
            {
                _staticNativeSizeImageList[i].sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, GameEventTemplateKeys.StaticNativeImagePrefixKey + i);
                _staticNativeSizeImageList[i].SetNativeSize();
                _staticNativeSizeImageList[i].enabled = true;
            }
        }

        if (_staticNonNativeSizeImageList != null)
        {
            for (int i = 0; i < _staticNonNativeSizeImageList.Count; i++)
            {
                _staticNonNativeSizeImageList[i].sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, GameEventTemplateKeys.StaticNonNativeImagePrefixKey + i);
                _staticNonNativeSizeImageList[i].enabled = true;
            }
        }

        #endregion

        #region Button UI

        InitBtn();

        #endregion

        #region Other UI

        // HP Slider
        UpdateBossHPGauge();

        // Ticket
        switch (PlayTriggerType)
        {
            case GameEventStartType.Ticket:
                InitTicketUI();
                break;
        }

        // Set Action Button State.
        UpdateAllActionState();

        #endregion
    }

    /// <summary>
    /// Call this method to initialize button onClick action.
    /// </summary>
    private void InitBtn()
    {
        //_eventCloseButton.onClick.RemoveAllListeners();
        _eventReadMoreButton.onClick.RemoveAllListeners();

        //_eventCloseButton.onClick.AddListener(Close);
        _eventReadMoreButton.onClick.AddListener(ShowReadMore);

        //_eventCloseButton.gameObject.SetActive(true);
        _eventReadMoreButton.gameObject.SetActive(true);
    }

    /// <summary>
    /// Call this method to set the ticket value into UI.
    /// </summary>
    private void InitTicketUI()
    {
        // Text UI
        _eventTicketText.text = $"x{_ticket.Amount}";

        for (int i = 0; i < _eventTicketTextList.Count; i++)
        {
            _eventTicketTextList[i].text = $"x{RequireTicket[i]}";
        }

        foreach (Image image in _eventTicketImageList)
        {
            image.sprite = SpriteResourceHelper.LoadSprite(GameEventTemplateKeys.TicketIconImagePrefixKey + _eventThemeInfo.Theme, SpriteResourceHelper.SpriteType.ICON_Item);
            image.enabled = true;
        }
    }

    /// <summary>
    /// Call this method to generate all necessary UI.
    /// </summary>
    private void GenerateUI()
    {
        // Generate Reward Box UI.

        GameObject rewardBoxPrefab = ResourceManager.Load(GameEventTemplateKeys.EventPrefabResourcePath + EventRaidBossChestBox) as GameObject;

        _rewardChestButtonScripts = new List<EventRaidBossRewardChest>();

        if (rewardBoxPrefab == null)
            return;

        for (int i = 1; i < _tierBossHpList.Count; i++)
        {
            GameObject rewardBox = Instantiate(rewardBoxPrefab, _eventRewardBoxParent);
            EventRaidBossRewardChest rewardBoxScript = rewardBox.GetComponent<EventRaidBossRewardChest>();

            List<Sprite> spriteList = new List<Sprite>
            {
                i == _tierBossHpList.Count - 1
                    ? SpriteResourceHelper.LoadSubEventImage(_eventKey, EventRewardBoxPrefixKey + 1)
                    : SpriteResourceHelper.LoadSubEventImage(_eventKey, EventRewardBoxPrefixKey + 0)
                , SpriteResourceHelper.LoadSubEventImage(_eventKey, EventRankPrefixKey + _rankList[i])
                , SpriteResourceHelper.LoadSubEventImage(_eventKey, EventRewardBoxHighlight)
            };

            rewardBoxScript.Init(
                spriteList
                , i <= _rewardClearedCount
                , _receivedRewardDictionary.ContainsKey(i.ToString())
                , i
                , null
                , this
            );

            _rewardChestButtonScripts.Add(rewardBoxScript);
        }
    }

    #endregion

    #region Update Methods
    /// <summary>
    /// Call this method to update all general buttons state.
    /// </summary>
    /// <param name="buttonsState">Buttons' state.</param>
    private void UpdateGeneralButtonsState(bool buttonsState)
    {
        //_eventCloseButton.enabled = buttonsState;
        _eventReadMoreButton.enabled = buttonsState;
    }

    /// <summary>
    /// Call this method to update action buttons state.
    /// </summary>
    /// <param name="currentState">Action buttons' state.</param>
    private void UpdateAllActionState(bool currentState)
    {
        foreach (EventRaidBossAction action in _actionButtonList)
        {
            action.UpdateActionState(currentState);
        }
    }

    /// <summary>
    /// Call this method to update ticket UI
    /// </summary>
    private void UpdateTicketUI(UnityAction onComplete)
    {
        DataManager.Instance.LoadInventory(delegate
        {
            string ticketID = GameEventTemplateKeys.TicketIDPrefixKey + _eventThemeInfo.Theme;
            int ticketAmount = DataManager.Instance.InventoryData.GetAmountByItemID(ticketID);

            _ticket = new ItemData(ticketID, ticketAmount);
            _eventTicketText.text = $"x{_ticket.Amount}";

            onComplete?.Invoke();

        }, null);
    }

    /// <summary>
    /// Call this method to update boss HP gauge.
    /// </summary>
    private void UpdateBossHPGauge()
    {
        bool isSubtract = _tierBossHpList[0] != 0;

        for (int i = 0; i < _eventBossHpSliderList.Count; i++)
        {
            if (i < _rewardClearedCount)
            {
                _eventBossHpSliderList[i].value = isSubtract ? 0 : 1;
            }
            else
            {
                _eventBossHpSliderList[i].value = isSubtract ? 1 : 0;
            }
            _eventBossHpSliderList[i].gameObject.SetActive(true);
        }

        if (_rewardClearedCount < _eventBossHpSliderList.Count)
        {
            int currentMaxHp = Mathf.Abs(_tierBossHpList[_rewardClearedCount] - _tierBossHpList[_rewardClearedCount + 1]);
            int currentBossHp = Mathf.Abs(_currentRaidBossHP - _tierBossHpList[_rewardClearedCount]);

            if (_rewardClearedCount == 0 && !isSubtract)
            {
                currentBossHp = Mathf.Abs(_currentRaidBossHP);
            }

            _eventBossHpSliderList[_rewardClearedCount].value = (float)currentBossHp / currentMaxHp;
            _eventBossHpSliderList[_rewardClearedCount].gameObject.SetActive(true);
        }

    }

    /// <summary>
    /// Call this method to update action button state respect to current amount of event ticket.
    /// </summary>
    private void UpdateAllActionState()
    {
        for (int i = 0; i < RequireTicket.Count; i++)
        {
            _actionButtonList[i].Init(i, null, this);
            if (_ticket.Amount < RequireTicket[i])
                _actionButtonList[i].UpdateActionState(false);
            else
                _actionButtonList[i].UpdateActionState(true);
        }
    }

    /// <summary>
    /// Call this method to update raid rank status.
    /// </summary>
    private void UpdateRaidRank(UnityAction onComplete = null)
    {
        int rankIndex = 0;

        for (int i = 0; i < _tierBossHpList.Count; i++)
        {
            if (_currentRaidBossHP <= _tierBossHpList[i])
                rankIndex = i;
            else
                break;
        }

        if (rankIndex > _rewardClearedCount)
        {
            _rewardClearedCount = rankIndex;

            // Update rank sprite.

            _eventRankImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, EventRankPrefixKey + _rankList[_rewardClearedCount]);
            _eventRankImage.rectTransform.localScale = new Vector3(1.5f, 1.5f, 1.5f);

            _eventBossImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, EventBossImagePrefixKey + _rankList[_rewardClearedCount]);
            _eventBossImage.rectTransform.localScale = new Vector3(1.2f, 1.2f, 1.2f);

            _eventBossImage.rectTransform.DOScale(Vector3.one, 0.5f).SetEase(Ease.Linear);
            _eventRankImage.rectTransform.DOScale(Vector3.one, 0.5f).SetEase(Ease.Linear).onComplete += delegate
            {
                // Update chest.
                _rewardChestButtonScripts[_rewardClearedCount - 1].SetRewardCleared();
                onComplete?.Invoke();
            };
        }
        else
        {
            onComplete?.Invoke();
        }
    }
    #endregion

    #region Action Methods
    /// <summary>
    /// Call this method when the action is selected.
    /// </summary>
    /// <param name="caller">Action button instance.</param>
    /// <param name="actionIndex">Action index.</param>
    public void OnSelectAction(object caller, int actionIndex)
    {
        // Check caller type.
        // If it doesn't match, return this method.
        if (caller.GetType() != typeof(EventRaidBossAction))
            return;

        UpdateAllActionState(false);
        UpdateGeneralButtonsState(false);

        // Event quest ticket condition.
        if (PlayTriggerType == GameEventStartType.Ticket && _ticket.Amount < RequireTicket[actionIndex])
        {
            string noTicketTitle = LocalizationManager.Instance.GetText(GameEventTemplateKeys.NoTicketTitleKey);
            string noTicketMessage = LocalizationManager.Instance.GetText(GameEventTemplateKeys.NoTicketMessageKey);
            PopupUIManager.Instance.ShowPopup_Error(
                noTicketTitle
                , noTicketMessage
                , delegate
                {
                    UpdateAllActionState();
                    UpdateGeneralButtonsState(true);
                }
            );
            return;
        }

        OnExecuteAction(actionIndex);
    }

    /// <summary>
    /// Call this method to resolve the raid boss attack action.
    /// </summary>
    /// <param name="actionIndex">Action index.</param>
    private void OnExecuteAction(int actionIndex)
    {
        if (_isActionTaken)
            return;
        _isActionTaken = true;

        int currentTicketAmount = _ticket.Amount - RequireTicket[actionIndex];
        _eventTicketText.text = $"x{currentTicketAmount}";

        PopupUIManager.Instance.Show_SoftLoad(true);

        DataManager.Instance.ExcecuteEventCloudScript(
            _actionCloudScriptFunctionName
            , new
            {
                EventKey = _eventKey
                ,
                ActionIndex = actionIndex
                ,
                TicketID = _ticket.ItemID
            }
            , delegate (CloudScriptResultProcess result)
            {
                if (result.IsSuccess)
                {
                    //if rewards have EC
                    //string ec_json = result.CustomData[GameEventTemplateKeys.EventCurrencyKey];
                    //Debug.Log(ec_json);
                    //DataManager.Instance.InventoryData.SetEventCurrency(ec_json);

                    // TODO: Event currency and Item Data.

                    bool isSuccess = GameHelper.TryDeserializeJSONStr(result.CustomData["item_data"], out Dictionary<string, int> ItemDataDict);
                    if (isSuccess)
                    {
                        List<ItemData> itemDataList = DataManager.ConvertDictionaryToItemDataList(ItemDataDict);
                        foreach (ItemData itemData in itemDataList)
                        {
                            DataManager.Instance.InventoryData.GrantItemByItemID(itemData.ItemID, itemData.Amount);
                        }
                    }

                    _coroutine = StartCoroutine(ActionRewardGenerate(
                        _actionButtonList[actionIndex].transform
                        , _participateRewardList[actionIndex]
                        , ActionDefaultDiagonalList[actionIndex]
                        , delegate
                        {
                            StopCoroutine(_coroutine);
                            OnReceiveItem(_participateRewardList[actionIndex]);

                            _movingParticleList[actionIndex].SetActive(true);
                            OnPlayActionEffect(
                                actionIndex
                                , delegate
                                {
                                    if (GameHelper.TryDeserializeJSONStr(result.CustomData[RaidBossHPKey], out _currentRaidBossHP))
                                    {
                                        //EventBossHealthText.text = $"HP: {Mathf.Clamp(_currentRaidBossHP, 0, _tierBossHpList[0]):n0} / {_tierBossHpList[0]:n0}";
                                        _eventCurrentDamageText.text = (_tierBossHpList[0] - _currentRaidBossHP).ToString("n0") + " " + LocalizationManager.Instance.GetText(EventCurrentDamageUnitKey + _eventKey);
                                        UpdateRaidRank(delegate
                                        {
                                            UpdateBossHPGauge();
                                            UpdateTicketUI(
                                                delegate
                                                {
                                                    _isActionTaken = false;
                                                    UpdateAllActionState();
                                                    UpdateGeneralButtonsState(true);
                                                    PopupUIManager.Instance.Show_SoftLoad(false);
                                                }
                                            );
                                        });
                                    }
                                    else
                                    {
                                        UpdateTicketUI(
                                            delegate
                                            {
                                                _isActionTaken = false;
                                                UpdateAllActionState();
                                                UpdateGeneralButtonsState(true);
                                                PopupUIManager.Instance.Show_SoftLoad(false);
                                            }
                                        );
                                    }
                                }
                            );
                        })
                    );

                }
                else
                {
                    UpdateTicketUI(
                        delegate
                        {
                            _isActionTaken = false;
                            UpdateAllActionState();
                            UpdateGeneralButtonsState(true);
                            PopupUIManager.Instance.Show_SoftLoad(false);
                        }
                    );
                }
            }
            , delegate (string error)
            {
                Debug.LogError(error);

                UpdateTicketUI(
                    delegate
                    {
                        _isActionTaken = false;
                        UpdateAllActionState(true);
                        UpdateGeneralButtonsState(true);
                        PopupUIManager.Instance.Show_SoftLoad(false);
                    }
                );
            }
        );
    }

    /// <summary>
    /// Call this method to show the action effect.
    /// </summary>
    /// <param name="actionIndex">Action index.</param>
    /// <param name="onComplete">Delegate method when this method is finish.</param>
    private void OnPlayActionEffect(int actionIndex, UnityAction onComplete)
    {
        switch (_attackEffect)
        {
            case RaidAttackEffect.Projectile:

                SoundManager.PlaySFX(_effectStart);
                _movingParticleList[actionIndex].transform.DOPath(
                    _wayPointReferenceList.ToArray()
                    , 0.75f
                    , PathType.CatmullRom).SetEase(Ease.InOutExpo).onComplete += delegate
                    {
                        _movingParticleList[actionIndex].SetActive(false);
                        _movingParticleList[actionIndex].transform.position = _particleDefaultPosList[actionIndex];

                        _burstParticleList[actionIndex].SetActive(true);

                        SoundManager.PlaySFX(_effectEnd);
                        StartCoroutine(DelayActionTime(0.5f, delegate
                        {
                            _burstParticleList[actionIndex].SetActive(false);
                            onComplete?.Invoke();
                        }));
                    };

                break;
            case RaidAttackEffect.Sword:
                break;
        }
    }

    /// <summary>
    /// Call this method to receive chest reward.
    /// </summary>
    /// <param name="caller">Action button instance.</param>
    /// <param name="actionIndex">Action index.</param>
    public void OnReceiveReward(object caller, int actionIndex)
    {
        // Check caller type.
        // If it doesn't match, return this method.
        if (caller.GetType() != typeof(EventRaidBossRewardChest))
            return;

        PopupUIManager.Instance.Show_SoftLoad(true);

        DataManager.Instance.ExcecuteEventCloudScript(
            _getRewardCloudScriptFunctionName
            , new
            {
                EventKey = _eventKey,
                RewardIndex = actionIndex
            }
            , delegate (CloudScriptResultProcess result)
            {
                PopupUIManager.Instance.Show_SoftLoad(false);

                if (result.IsSuccess)
                {
                    string title = LocalizationManager.Instance.GetText(GameEventTemplateKeys.NotiRewardTitleKey);
                    string message = LocalizationManager.Instance.GetText(GameEventTemplateKeys.NotiRewardMessageKey);
                    PopupUIManager.Instance.ShowPopup_GotRewardAnimated(
                        title
                        , message
                        , _tierRewardList[actionIndex]
                        , delegate
                        {
                            _rewardChestButtonScripts[actionIndex - 1].SetRewardReceived();
                            DataManager.Instance.LoadInventory(null, null);
                            OnReceiveItem(_tierRewardList[actionIndex]);
                        }
                    );
                }
            }
            ,
            delegate (string error)
            {
                Debug.LogError(error);
                PopupUIManager.Instance.Show_SoftLoad(false);
            }
        );
    }

    /// <summary>
    /// Call this method to preview reward in chest.
    /// </summary>
    /// <param name="caller"></param>
    /// <param name="actionIndex"></param>
    public void OnPreviewReward(object caller, int actionIndex)
    {
        if (LocalizationManager.Instance.GetText(EventRewardTitle + _eventKey, out string title))
            title = string.Format(title, _rankList[actionIndex]);
        if (LocalizationManager.Instance.GetText(EventRewardMessage + _eventKey, out string message))
            message = string.Format(message, (_tierBossHpList[0] - _tierBossHpList[actionIndex]).ToString("n0"));

        PopupUIManager.Instance.ShowPopup_PreviewReward(
            title
            , message
            , _tierRewardList[actionIndex]
        );
    }
    #endregion

    #region IEnumurator Method

    private IEnumerator TimeCountDown(float time, UnityAction onComplete)
    {
        yield return new WaitForSeconds(time);
        onComplete?.Invoke();
    }
    #endregion

    #region Other Methods

    /// <summary>
    /// Call this method to show read more.
    /// </summary>
    public void ShowReadMore()
    {
        //EventDetailImage.gameObject.SetActive(true);
        string title = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventDetailHeaderKey);
        string message = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventDetailKey + _eventKey);

        PopupUIManager.Instance.ShowPopup_DetailWindow(title, message);
    }

    public override void Close()
    {
        PopupUIManager.Instance.Show_SoftLoad(true);

        GameHelper.UITransition_FadeOut(FadeOut.gameObject, () =>
        {
            DestroySelf();
            PopupUIManager.Instance.Show_SoftLoad(false);
            EventManager.Instance.ShowPage(EventPageType.GameEvent);
        });
    }
    #endregion
    #endregion
}
