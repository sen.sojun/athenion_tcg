﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class EventRaidBossRewardChest : MonoBehaviour
{
    #region Properties

    [Header("Image UI Elements")]
    [SerializeField] private Image PreviewRewardChestImage;             // Main GameObject 1.
    [SerializeField] private Image PreviewClearedCheckImage;
    [SerializeField] private Image PreviewRankImage;

    [SerializeField] private Image ReceiveRewardChestImage;
    [SerializeField] private Image ReceiveGlowingImage;                 // Main GameObject 2.
    [SerializeField] private Image ReceiveRankImage;

    [Header("Button UI Elements")]
    [SerializeField] private Button PreviewRewardButton;
    [SerializeField] private Button ReceiveRewardButton;

    #endregion

    #region Methods

    public void Init(List<Sprite> imageSpriteList, bool isClear, bool isReceivedReward, int rewardIndex, GameEventTemplateRaidBoss eventTemplate = null, GameEventTemplateRaidBoss_2nd eventTemplate2 = null)
    {
        // Set image.
        PreviewRewardChestImage.sprite = imageSpriteList[0];
        PreviewRankImage.sprite = imageSpriteList[1];

        ReceiveRewardChestImage.sprite = imageSpriteList[0];
        ReceiveRankImage.sprite = imageSpriteList[1];

        ReceiveGlowingImage.sprite = imageSpriteList[2];

        //// Assign Button Listener.
        //PreviewRewardButton.onClick.AddListener(delegate
        //{
        //    eventTemplate?.OnPreviewReward(
        //        this
        //        , rewardIndex
        //    );
        //    eventTemplate2?.OnPreviewReward(
        //        this
        //        , rewardIndex
        //    );
        //});

        //ReceiveRewardButton.onClick.AddListener(delegate
        //{
        //    eventTemplate?.OnReceiveReward(
        //        this
        //        , rewardIndex
        //    );
        //    eventTemplate2?.OnReceiveReward(
        //        this
        //        , rewardIndex
        //    );
        //});

        // Set Button.
        if (isClear && isReceivedReward)
        {
            ReceiveGlowingImage.gameObject.SetActive(false);
            PreviewRewardChestImage.gameObject.SetActive(true);
            PreviewClearedCheckImage.enabled = true;
        }

        if (isClear && !isReceivedReward)
        {
            ReceiveGlowingImage.gameObject.SetActive(true);
            PreviewRewardChestImage.gameObject.SetActive(false);
            PreviewClearedCheckImage.enabled = false;
        }

        if (!isClear && !isReceivedReward)
        {
            ReceiveGlowingImage.gameObject.SetActive(false);
            PreviewRewardChestImage.gameObject.SetActive(true);
            PreviewClearedCheckImage.enabled = false;
        }
    }

    public void SetReceiveRewardBtn(UnityAction action)
    {
        ReceiveRewardButton.onClick.RemoveAllListeners();
        ReceiveRewardButton.onClick.AddListener(action);
    }

    public void SetPreviewRewardBtn(UnityAction action)
    {
        PreviewRewardButton.onClick.RemoveAllListeners();
        PreviewRewardButton.onClick.AddListener(action);
    }

    public void SetRewardCleared()
    {
        ReceiveGlowingImage.gameObject.SetActive(true);
        PreviewRewardChestImage.gameObject.SetActive(false);
        PreviewClearedCheckImage.enabled = false;
    }

    public void SetRewardReceived()
    {
        ReceiveGlowingImage.gameObject.SetActive(false);
        PreviewRewardChestImage.gameObject.SetActive(true);
        PreviewClearedCheckImage.enabled = true;
    }

    #endregion
}
