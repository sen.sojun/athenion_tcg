﻿using System;
using System.Collections;
using System.Collections.Generic;
using Karamucho;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class GameEventRaidBossUIManager : MonoBehaviour
{
    #region Static Value

    #region Resource Key
    // Prefix Key.
    private static readonly string EventRankPrefixKey = "RANK_";
    private static readonly string EventBossImagePrefixKey = "BOSS_RANK_";
    private static readonly string EventRewardBoxPrefixKey = "REWARD_BOX_";

    // Full Key.
    private static readonly string EventRaidBossImageKey = "BOSS_IMG";
    private static readonly string EventRewardBoxHighlight = "REWARD_BOX_HIGHLIGHT";

    // Prefab Key
    private static readonly string EventRaidBossChestBox = "EventRaidBossRewardChest";
    #endregion

    #region Localization

    private static readonly string EventCurrentDamageUnitKey = "EVENT_CURRENT_DAMAGE_DESC_";
    private static readonly string EventBossNameKey = "EVENT_BOSS_NAME_";
    #endregion

    #endregion

    #region Private Properties

    private List<EventRaidBossRewardChest> _rewardChestButtonScripts;

    #endregion

    #region Inspector Properties

    [Header("Static Image UI Element List")]
    [SerializeField] private List<Image> _staticNativeSizeImageList;
    [SerializeField] private List<Image> _staticNonNativeSizeImageList;
    [SerializeField] private Image _eventHeaderBgImage;

    [Header("Dynamic Image UI Element List")]
    [SerializeField] private Image _eventRankImage;
    [SerializeField] private Image _eventBossImage;

    [Header("Text UI Elements")]
    [SerializeField] private TextMeshProUGUI _eventHeaderText;
    [SerializeField] private TextMeshProUGUI _eventCurrentDamageText;
    [SerializeField] private TextMeshProUGUI _eventBossNameText;
    [SerializeField] private TextMeshProUGUI _eventBossHpText;

    [Header("Button UI Elements")]
    [SerializeField] private Button _eventInfoButton;

    [Header("Progression UI Elements")]
    [SerializeField] private Image _eventBossHpGaugeImage;

    [Header("Other UI Elements")]
    [SerializeField] private RectTransform _eventRewardParent;
    [SerializeField] private bool _isBossImageStatic;

    #endregion

    #region Methods
    public void InitUi(RaidBossEventData eventInfoData, RaidBossPlayerData playerData)
    {
        string eventKey = eventInfoData.EventKey;
        var eventThemeInfo = eventInfoData.EventThemeInfo;
        string bossWaveIndex = GetBossWaveIndex(eventInfoData);

        #region Text UI

        if (_eventHeaderText != null) _eventHeaderText.text = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventHeaderKey + eventKey);
        if (_eventCurrentDamageText != null) _eventCurrentDamageText.text = string.Format(LocalizationManager.Instance.GetText(EventCurrentDamageUnitKey + eventKey), (eventInfoData.TierBossHpList[0] - eventInfoData.CurrentRaidBossHp).ToString("n0"));
        if (_eventBossNameText != null) _eventBossNameText.text = LocalizationManager.Instance.GetText(EventBossNameKey + bossWaveIndex + "_" + eventKey);
        #endregion

        #region Image UI

        //_eventHeaderBgImage.sprite = SpriteResourceHelper.LoadSprite(eventThemeInfo.Theme + GameEventTemplateKeys.EventHeaderBannerSuffix, SpriteResourceHelper.SpriteType.GAME_Event);
        _eventRankImage.sprite = SpriteResourceHelper.LoadSubEventImage(eventKey, EventRankPrefixKey + eventInfoData.RankList[eventInfoData.RewardClearedCount]);
        _eventBossImage.sprite = _isBossImageStatic
            ? SpriteResourceHelper.LoadSubEventImage(eventKey, EventRaidBossImageKey + "_" + bossWaveIndex)
            : SpriteResourceHelper.LoadSubEventImage(eventKey, EventBossImagePrefixKey + eventInfoData.RankList[eventInfoData.RewardClearedCount]);

        if (_staticNativeSizeImageList != null)
        {
            for (int i = 0; i < _staticNativeSizeImageList.Count; i++)
            {
                _staticNativeSizeImageList[i].sprite = SpriteResourceHelper.LoadSubEventImage(eventKey, GameEventTemplateKeys.StaticNativeImagePrefixKey + i);
                _staticNativeSizeImageList[i].SetNativeSize();
            }
        }

        if (_staticNonNativeSizeImageList != null)
        {
            for (int i = 0; i < _staticNonNativeSizeImageList.Count; i++)
            {
                _staticNonNativeSizeImageList[i].sprite = SpriteResourceHelper.LoadSubEventImage(eventKey, GameEventTemplateKeys.StaticNonNativeImagePrefixKey + i);
            }
        }

        #endregion

        #region Boss Hp UI

        int count = eventInfoData.TierBossHpList.Count;
        int currentBossHp = Mathf.Abs(eventInfoData.CurrentRaidBossHp);
        int maxBossHp = Mathf.Abs(eventInfoData.TierBossHpList[count - 1] - eventInfoData.TierBossHpList[0]);

        if (eventInfoData.TierBossHpList[eventInfoData.TierBossHpList.Count - 1] <= 0 && eventInfoData.CurrentRaidBossHp <= 0)
        {
            _eventBossHpGaugeImage.fillAmount = 0;
            if (_eventBossHpText != null) _eventBossHpText.text = $"HP: {0}/{maxBossHp}";
        }
        else
        {
            _eventBossHpGaugeImage.fillAmount = (float)currentBossHp / maxBossHp;
            if (_eventBossHpText != null) _eventBossHpText.text = $"HP: {currentBossHp}/{maxBossHp}";
        }

        

        #endregion

        GenerateUI(eventInfoData, playerData);
    }

    private string GetBossWaveIndex(RaidBossEventData eventInfoData)
    {
        foreach (var item in eventInfoData.BossWaveDetailDict)
        {
            if (item.Value.IsValidDate())
            {
                return item.Key;
            }
        }

        return "0";
    }

    public void SetReceiveRewardBtn(UnityAction<int, UnityAction> action)
    {
        for (int i = 0; i < _rewardChestButtonScripts.Count; i++)
        {
            int index = i + 1;
            _rewardChestButtonScripts[i].SetReceiveRewardBtn(() =>
            {
                action?.Invoke(
                    index
                    , () =>
                    {
                        _rewardChestButtonScripts[index - 1].SetRewardReceived();
                    }
                );
            });
        }
    }

    public void SetPreviewRewardBtn(UnityAction<int> action)
    {
        for (int i = 0; i < _rewardChestButtonScripts.Count; i++)
        {
            int index = i + 1;
            _rewardChestButtonScripts[i].SetPreviewRewardBtn(() =>
            {
                action?.Invoke(index);
            });
        }
    }

    public void SetEventInfoBtn(UnityAction action)
    {
        _eventInfoButton.onClick.RemoveAllListeners();
        _eventInfoButton.onClick.AddListener(action);
    }

    private void GenerateUI(RaidBossEventData eventInfoData, RaidBossPlayerData playerData)
    {
        // Generate Reward Box UI.

        GameObject rewardBoxPrefab = ResourceManager.Load(GameEventTemplateKeys.EventPrefabResourcePath + EventRaidBossChestBox) as GameObject;
        string eventKey = eventInfoData.EventKey;

        _rewardChestButtonScripts = new List<EventRaidBossRewardChest>();

        if (rewardBoxPrefab == null)
            return;

        for (int i = 1; i < eventInfoData.TierBossHpList.Count; i++)
        {
            GameObject rewardBox = Instantiate(rewardBoxPrefab, _eventRewardParent);
            EventRaidBossRewardChest rewardBoxScript = rewardBox.GetComponent<EventRaidBossRewardChest>();

            List<Sprite> spriteList = new List<Sprite>
            {
                i == eventInfoData.TierBossHpList.Count - 1
                    ? SpriteResourceHelper.LoadSubEventImage(eventKey, EventRewardBoxPrefixKey + 1)
                    : SpriteResourceHelper.LoadSubEventImage(eventKey, EventRewardBoxPrefixKey + 0)
                , SpriteResourceHelper.LoadSubEventImage(eventKey, EventRankPrefixKey + eventInfoData.RankList[i])
                , SpriteResourceHelper.LoadSubEventImage(eventKey, EventRewardBoxHighlight)
            };

            rewardBoxScript.Init(
                spriteList
                , i <= eventInfoData.RewardClearedCount
                , playerData.ReceivedRewardDictionary.ContainsKey(i.ToString())
                , i
            );

            _rewardChestButtonScripts.Add(rewardBoxScript);
        }
    }
    #endregion
}
