﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Karamucho;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class GameEventTemplateRaidBoss : BaseGameEventTemplate
{
    #region Static Values

    #region CloudScript Key

    private static readonly string AttackDamageKey = "attack_damage";
    private static readonly string ParticipateRewardKey = "participate_reward";
    private static readonly string TierDataKey = "tier_data";
    private static readonly string RaidBossHPKey = "raid_boss_hp";
    private static readonly string BossHPKey = "boss_hp";
    private static readonly string RankKey = "rank";
    private static readonly string ReceivedRewardKey = "received_reward";

    #endregion

    #region Localization Key

    private static readonly string EventCurrentDamageDescKey = "EVENT_CURRENT_DAMAGE_DESC_";
    private static readonly string EventActionDescKey = "EVENT_ACTION_DESC_";
    private static readonly string EventCurrentDamageUnitKey = "EVENT_BOSS_DAMAGE_UNIT_";
    private static readonly string EventRewardTitle = "EVENT_RAID_BOSS_REWARD_TITLE";
    private static readonly string EventRewardMessage = "EVENT_RAID_BOSS_REWARD_MESSAGE";
    private static readonly string EventBossHPDisplayKey = "EVENT_RAID_BOSS_HP_";

    #endregion

    #region Resource Key

    // Prefix Key.
    private static readonly string EventRankPrefixKey = "RANK_";
    private static readonly string EventRewardBoxPrefixKey = "REWARD_BOX_";

    // Full Key.
    private static readonly string EventRaidBossImageKey = "BOSS_IMG";
    private static readonly string EventRewardBoxHighlight = "REWARD_BOX_HIGHLIGHT";

    // Prefab Key
    private static readonly string EventRaidBossChestBox = "EventRaidBossRewardChest";

    #endregion

    #endregion

    #region UI Properties

    [Header("Static Image UI Element List")]
    [SerializeField] private List<Image> StaticNativeSizeImageList;
    [SerializeField] private List<Image> StaticNonNativeSizeImageList;
    [SerializeField] private List<Image> EventTicketImageList;
    [SerializeField] private Image EventMainBgImage;
    [SerializeField] private Image EventDetailImage;
    [SerializeField] private Image EventHeaderBgImage;
    [SerializeField] private Image EventReadMoreImage;
    [SerializeField] private Image EventCloseImage;
    [SerializeField] private Image EventBossImage;
    [SerializeField] private Image EventBossGrayScaleImage;
    [SerializeField] private Image EventDefeatTHImage;
    [SerializeField] private Image EventDefeatENImage;
    [SerializeField] private Image EventDefeatJPImage;

    [Header("Dynamic Image UI Element List")]
    [SerializeField] private Image EventRankImage;

    [Header("Text UI Elements")]
    [SerializeField] private TextMeshProUGUI EventHeaderText;
    [SerializeField] private TextMeshProUGUI EventObjectiveText;
    [SerializeField] private TextMeshProUGUI EventDetailText;
    [SerializeField] private TextMeshProUGUI EventTicketText;
    [SerializeField] private TextMeshProUGUI EventDurationText;
    [SerializeField] private TextMeshProUGUI EventCurrentDamageDescriptionText;
    [SerializeField] private TextMeshProUGUI EventCurrentDamageText;
    [SerializeField] private TextMeshProUGUI EventBossHealthText;
    [SerializeField] private List<TextMeshProUGUI> EventActionDescriptionTextList;
    [SerializeField] private List<TextMeshProUGUI> EventActionDamageTextList;
    [SerializeField] private List<TextMeshProUGUI> EventActionDamageUnitTextList;
    [SerializeField] private List<TextMeshProUGUI> EventTicketTextList;

    [Header("Button UI Elements")]
    [SerializeField] private Button EventCloseButton;
    [SerializeField] private Button ReadMoreButton;
    [SerializeField] private List<EventRaidBossAction> ActionButtonList;

    [Header("Slider UI Elements")]
    [SerializeField] private Slider EventBossHpSlider;

    [Header("Other UI Elements")]
    [SerializeField] private RectTransform EventRewardBoxParent;

    [Header("Other Setting")]
    [SerializeField] private List<bool> ActionDefaultDiagonalList;

    [Header("Particle Setting")]
    [SerializeField] private RaidAttackEffect _attackEffect;
    [SerializeField] private List<GameObject> _movingParticleList;
    [SerializeField] private List<GameObject> _burstParticleList;
    [SerializeField] private List<GameObject> _slashParticleList;
    [SerializeField] private List<Transform> _referencePosition;
    [SerializeField] private bool _activateParticle;

    [Header("Camera Setting")]
    [SerializeField] private Camera _mainCamera;
    #endregion

    #region Public Properties
    [Header("CloudScript Function Name")]
    [SerializeField] private string _initCloudScriptFunctionName;
    [SerializeField] private string _getRewardCloudScriptFunctionName;
    [SerializeField] private string _actionCloudScriptFunctionName;

    #endregion

    #region Private Properties

    private List<List<ItemData>> _tierRewardList;
    private List<int> _tierBossHpList;
    private List<string> _rankList;

    private List<List<ItemData>> _participateRewardList;
    private List<int> _actionDamageValueList;
    private int _currentRaidBossHP;
    private ItemData _ticket;

    private List<EventRaidBossRewardChest> _rewardChestButtonScripts;
    private int _rewardClearedCount;
    private bool _isActionTaken;

    private bool _participateDone;

    private List<Vector3> _particleDefaultPosList;
    private List<Vector3> _wayPointReferenceList;

    private Coroutine _coroutine;
    private Vector3 _defaultCameraPosition;

    private Image _selectedDefeatImage;

    private Dictionary<string, object> _receivedRewardDictionary;
    #endregion

    #region Methods

    #region Initialize Methods

    /// <summary>
    /// Call this method to initialize event to this scene.
    /// </summary>
    /// <param name="eventKey">Current event key.</param>
    /// <param name="sceneName">Current scene name.</param>
    public override void Init(string eventKey, string sceneName, UnityAction onComplete = null, UnityAction onFail = null)
    {
        base.Init(eventKey, sceneName);

        PopupUIManager.Instance.Show_SoftLoad(true);

        DataManager.Instance.ExcecuteEventCloudScript(
            _initCloudScriptFunctionName
            , new { EventKey = _eventKey }
            , (result) =>
            {
                DataManager.Instance.LoadInventory(null, null);

                bool isSuccess = true;

                // Data Initialize.
                isSuccess &= InitData(result);

                if (isSuccess)
                {
                    // UI Initialize.
                    InitUI();
                    GenerateUI();
                    UpdateBossDefeat();
                }
                else
                {
                    string errMsg = "GameEventTemplateRaidBoss/Init: Data from cloud-script is not found.";
                    OnLoadSceneFailed(errMsg);
                }

                PopupUIManager.Instance.Show_SoftLoad(false);
            }
            , delegate(string errMsg)
            {
                PopupUIManager.Instance.Show_SoftLoad(false);
                OnLoadSceneFailed(errMsg);
            }
        );
    }

    /// <summary>
    /// Call this method to initialize event data of this event template.
    /// </summary>
    /// <param name="eventData">Event data.</param>
    /// <returns></returns>
    protected override bool InitData(CloudScriptResultProcess eventData)
    {
        bool isSuccess = true;

        #region Base Data
        
        isSuccess &= base.InitData(eventData);
        if (!isSuccess) return false;

        #endregion

        #region Tier Data

        _tierRewardList = new List<List<ItemData>>();
        _tierBossHpList = new List<int>();
        _rankList = new List<string>();

        isSuccess &= GameHelper.TryDeserializeJSONStr(EventDataDict[TierDataKey].ToString(), out List<object> tierDataList);
        if (!isSuccess) return false;

        foreach (object tierItem in tierDataList)
        {
            // Check data accessible.
            isSuccess &= GameHelper.TryDeserializeJSONStr(tierItem.ToString(), out Dictionary<string, object> tierDictionary);
            isSuccess &= int.TryParse(tierDictionary[BossHPKey].ToString(), out int bossHP);
            isSuccess &= GameHelper.TryDeserializeJSONStr(tierDictionary[GameEventTemplateKeys.RewardListKey].ToString(), out List<object> rewardObjectList);
            isSuccess &= tierDictionary.ContainsKey(RankKey);
            if (!isSuccess) return false;

            // Set data value to variable.
            _tierBossHpList.Add(bossHP);
            _rankList.Add(tierDictionary[RankKey].ToString());

            List<ItemData> rewardList = new List<ItemData>();
            foreach (object rewardItem in rewardObjectList)
            {
                isSuccess &= GameHelper.TryDeserializeJSONStr(rewardItem.ToString(), out Dictionary<string, object> rewardObjectDictionary);
                isSuccess &= rewardObjectDictionary.ContainsKey(GameEventTemplateKeys.ItemIDKey);
                isSuccess &= rewardObjectDictionary.ContainsKey(GameEventTemplateKeys.ItemAmountKey);
                if (!isSuccess) return false;

                isSuccess &= int.TryParse(rewardObjectDictionary[GameEventTemplateKeys.ItemAmountKey].ToString(), out int rewardAmount);
                if (!isSuccess) return false;

                rewardList.Add(new ItemData(
                    rewardObjectDictionary[GameEventTemplateKeys.ItemIDKey].ToString()
                    , rewardAmount)
                );
            }
            _tierRewardList.Add(rewardList);
        }

        #endregion

        #region Participate Reward Data

        isSuccess &= GameHelper.TryDeserializeJSONStr(EventDataDict[ParticipateRewardKey].ToString(), out List<object> participateRewardList);
        if (!isSuccess) return false;

        _participateRewardList = new List<List<ItemData>>();

        foreach (object item in participateRewardList)
        {
            isSuccess &= GameHelper.TryDeserializeJSONStr(item.ToString(), out Dictionary<string, object> participateRewardDict);
            if (!isSuccess) return false;

            isSuccess &= participateRewardDict.ContainsKey(GameEventTemplateKeys.RewardListKey);
            if (!isSuccess) return false;

            isSuccess &= GameHelper.TryDeserializeJSONStr(participateRewardDict[GameEventTemplateKeys.RewardListKey].ToString(), out List<object> rewardList);
            if (!isSuccess) return false;

            List<ItemData> itemList = new List<ItemData>();

            foreach (object reward in rewardList)
            {
                isSuccess &= GameHelper.TryDeserializeJSONStr(reward.ToString(), out Dictionary<string, object> rewardDictionary);
                isSuccess &= rewardDictionary.ContainsKey(GameEventTemplateKeys.ItemIDKey);
                isSuccess &= rewardDictionary.ContainsKey(GameEventTemplateKeys.ItemAmountKey);
                if (!isSuccess) return false;

                isSuccess &= int.TryParse(rewardDictionary[GameEventTemplateKeys.ItemAmountKey].ToString(), out int itemAmount);
                if (!isSuccess) return false;

                itemList.Add(new ItemData(rewardDictionary[GameEventTemplateKeys.ItemIDKey].ToString(), itemAmount));
            }

            _participateRewardList.Add(itemList);
        }

        #endregion

        #region Action Damage and Current Raid Boss HP Data

        isSuccess &= GameHelper.TryDeserializeJSONStr(EventDataDict[AttackDamageKey].ToString(), out _actionDamageValueList);
        isSuccess &= int.TryParse(EventDataDict[RaidBossHPKey].ToString(), out _currentRaidBossHP);
        if (!isSuccess) return false;

        #endregion

        #region Ticket Event Data

        switch (PlayTriggerType)
        {
            case GameEventStartType.Ticket:
            {
                string ticketID = GameEventTemplateKeys.TicketIDPrefixKey + _eventThemeInfo.Theme;
                int ticketAmount = DataManager.Instance.InventoryData.GetAmountByItemID(ticketID);

                _ticket = new ItemData(ticketID, ticketAmount);
                break;
            }
        }

        #endregion

        #region Rank Data

        for (int i = 0; i < _tierBossHpList.Count; i++)
        {
            if (_currentRaidBossHP <= _tierBossHpList[i])
                _rewardClearedCount = i;
            else
                break;
        }

        #endregion

        #region Player Data

        isSuccess &= GameHelper.TryDeserializeJSONStr(eventData.CustomData[GameEventTemplateKeys.PlayerDataKey], out Dictionary<string, object> playerDataDictionary);
        if (!isSuccess) return false;

        isSuccess &= GameHelper.TryDeserializeJSONStr(playerDataDictionary[ReceivedRewardKey].ToString(), out _receivedRewardDictionary);
        if (!isSuccess) return false;
        #endregion

        #region Other Data

        // Default Particle position.
        _particleDefaultPosList = new List<Vector3>();
        foreach (GameObject particleItem in _movingParticleList)
        {
            _particleDefaultPosList.Add(particleItem.transform.position);
        }

        // Way Point for particle.

        _wayPointReferenceList = new List<Vector3>();
        foreach (Transform transform1 in _referencePosition)
        {
            _wayPointReferenceList.Add(transform1.position);
        }

        // Camera Position.
        _defaultCameraPosition = _mainCamera.transform.localPosition;
        #endregion

        return true;
    }

    /// <summary>
    /// Call this method to initialize event UI.
    /// </summary>
    private void InitUI()
    {
        #region Text UI

        // Base Text.
        if (EventHeaderText != null) EventHeaderText.text = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventHeaderKey + _eventKey);
        if (EventObjectiveText != null) EventObjectiveText.text = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventObjectiveKey + _eventKey);

        // Custom Text.
        if (EventCurrentDamageDescriptionText != null) EventCurrentDamageDescriptionText.text = LocalizationManager.Instance.GetText(EventCurrentDamageDescKey + _eventKey);
        if (EventCurrentDamageText != null) EventCurrentDamageText.text = (_tierBossHpList[0] - _currentRaidBossHP).ToString("n0") + " " + LocalizationManager.Instance.GetText(EventCurrentDamageUnitKey + _eventKey);
        //if (EventBossHealthText != null) EventBossHealthText.text = $"HP: {Mathf.Clamp(_currentRaidBossHP, 0, _tierBossHpList[0]):n0} / {_tierBossHpList[0]:n0}";
        if (EventBossHealthText != null)
        {
            if (LocalizationManager.Instance.GetText(EventBossHPDisplayKey + _eventKey, out string resultText))
            {
                int hp;
                int maxHp = _tierBossHpList[0] - _tierBossHpList[_tierBossHpList.Count - 1];
                if (_tierBossHpList[_tierBossHpList.Count - 1] < 0)
                {
                    hp = _tierBossHpList[0] - _currentRaidBossHP;
                }
                else
                {
                    hp = Mathf.Clamp(_currentRaidBossHP, 0, _tierBossHpList[0]);
                }
                EventBossHealthText.text = string.Format(resultText, hp.ToString("n0"), maxHp.ToString("n0"));
            }
            else
            {
                EventBossHealthText.text = EventBossHPDisplayKey + _eventKey;
            }
        }

        for (int i = 0; i < EventActionDescriptionTextList.Count; i++)
        {
            EventActionDescriptionTextList[i].text = LocalizationManager.Instance.GetText(EventActionDescKey + i + "_" + _eventKey);
        }

        for (int i = 0; i < EventActionDamageTextList.Count; i++)
        {
            EventActionDamageTextList[i].text = _actionDamageValueList[i].ToString();
        }

        // Text Unit (Damage, Pts, etc.)
        foreach (TextMeshProUGUI text in EventActionDamageUnitTextList)
        {
            text.text = LocalizationManager.Instance.GetText(EventCurrentDamageUnitKey);
        }
        #endregion

        #region Static Image UI

        if (SpriteResourceHelper.LoadSubEventImage(_eventKey, _eventThemeInfo.Theme + GameEventTemplateKeys.EventMainBackgroundSuffix, true) == null)
            EventMainBgImage.sprite = SpriteResourceHelper.LoadSprite(_eventThemeInfo.Theme, SpriteResourceHelper.SpriteType.GAME_Event);
        else
            EventMainBgImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, _eventThemeInfo.Theme + GameEventTemplateKeys.EventMainBackgroundSuffix);

        EventHeaderBgImage.sprite = SpriteResourceHelper.LoadSprite(_eventThemeInfo.Theme + GameEventTemplateKeys.EventHeaderBannerSuffix, SpriteResourceHelper.SpriteType.GAME_Event);
        EventBossImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, EventRaidBossImageKey);
        EventBossGrayScaleImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, EventRaidBossImageKey);
        EventRankImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, EventRankPrefixKey + _rankList[_rewardClearedCount]);

        switch (LocalizationManager.Instance.CurrentLanguage)
        {
            case Language.English:
                _selectedDefeatImage = EventDefeatENImage;
                break;
            case Language.Thai:
                _selectedDefeatImage = EventDefeatTHImage;
                break;
            case Language.Japanese:
                _selectedDefeatImage = EventDefeatJPImage;
                break;
        }

        EventMainBgImage.enabled = true;
        EventHeaderBgImage.enabled = true;
        EventBossImage.enabled = true;
        EventBossGrayScaleImage.enabled = true;
        EventRankImage.enabled = true;

        EventReadMoreImage.gameObject.SetActive(true);
        EventCloseImage.gameObject.SetActive(true);

        if (StaticNativeSizeImageList != null)
        {
            for (int i = 0; i < StaticNativeSizeImageList.Count; i++)
            {
                StaticNativeSizeImageList[i].sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, GameEventTemplateKeys.StaticNativeImagePrefixKey + i);
                StaticNativeSizeImageList[i].SetNativeSize();
                StaticNativeSizeImageList[i].enabled = true;
            }
        }

        if (StaticNonNativeSizeImageList != null)
        {
            for (int i = 0; i < StaticNonNativeSizeImageList.Count; i++)
            {
                StaticNonNativeSizeImageList[i].sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, GameEventTemplateKeys.StaticNonNativeImagePrefixKey + i);
                StaticNonNativeSizeImageList[i].enabled = true;
            }
        }

        #endregion

        #region Button UI

        InitBtn();

        #endregion

        #region Other UI

        // HP Slider
        UpdateBossHPGauge();

        // Ticket
        switch (PlayTriggerType)
        {
            case GameEventStartType.Ticket:
                InitTicketUI();
                break;
        }

        // Set Action Button State.
        UpdateAllActionState();

        #endregion
    }

    /// <summary>
    /// Call this method to set the ticket value into UI.
    /// </summary>
    private void InitTicketUI()
    {
        // Text UI
        EventTicketText.text = $"x{_ticket.Amount}";

        for (int i = 0; i < EventTicketTextList.Count; i++)
        {
            EventTicketTextList[i].text = $"x{RequireTicket[i]}";
        }

        foreach (Image image in EventTicketImageList)
        {
            image.sprite = SpriteResourceHelper.LoadSprite(GameEventTemplateKeys.TicketIconImagePrefixKey + _eventThemeInfo.Theme, SpriteResourceHelper.SpriteType.ICON_Item);
            image.enabled = true;
        }
    }

    /// <summary>
    /// Call this method to generate all necessary UI.
    /// </summary>
    private void GenerateUI()
    {
        // Generate Reward Box UI.

        GameObject rewardBoxPrefab = ResourceManager.Load(GameEventTemplateKeys.EventPrefabResourcePath + EventRaidBossChestBox) as GameObject;
        
        _rewardChestButtonScripts = new List<EventRaidBossRewardChest>();

        if (rewardBoxPrefab == null)
            return;

        for (int i = 1; i < _tierBossHpList.Count; i++)
        {
            GameObject rewardBox = Instantiate(rewardBoxPrefab, EventRewardBoxParent);
            EventRaidBossRewardChest rewardBoxScript = rewardBox.GetComponent<EventRaidBossRewardChest>();

            List<Sprite> spriteList = new List<Sprite>
            {
                i == _tierBossHpList.Count - 1
                    ? SpriteResourceHelper.LoadSubEventImage(_eventKey, EventRewardBoxPrefixKey + 1)
                    : SpriteResourceHelper.LoadSubEventImage(_eventKey, EventRewardBoxPrefixKey + 0)
                , SpriteResourceHelper.LoadSubEventImage(_eventKey, EventRankPrefixKey + _rankList[i])
                , SpriteResourceHelper.LoadSubEventImage(_eventKey, EventRewardBoxHighlight)
            };

            rewardBoxScript.Init(
                    spriteList
                    , i <= _rewardClearedCount
                    , _receivedRewardDictionary.ContainsKey(i.ToString())
                    , i
                    , this
            );

            _rewardChestButtonScripts.Add(rewardBoxScript);
        }
    }

    /// <summary>
    /// Call this method to initialize button onClick action.
    /// </summary>
    private void InitBtn()
    {
        EventCloseButton.onClick.RemoveAllListeners();
        ReadMoreButton.onClick.RemoveAllListeners();

        EventCloseButton.onClick.AddListener(Close);
        ReadMoreButton.onClick.AddListener(ShowReadMore);

        EventCloseButton.gameObject.SetActive(true);
        ReadMoreButton.gameObject.SetActive(true);
    }

    #endregion

    #region Update Methods
    /// <summary>
    /// Call this method to update all general buttons state.
    /// </summary>
    /// <param name="buttonsState">Buttons' state.</param>
    private void UpdateGeneralButtonsState(bool buttonsState)
    {
        EventCloseButton.enabled = buttonsState;
        ReadMoreButton.enabled = buttonsState;
    }

    /// <summary>
    /// Call this method to update action buttons state.
    /// </summary>
    /// <param name="currentState">Action buttons' state.</param>
    private void UpdateAllActionState(bool currentState)
    {
        foreach (EventRaidBossAction action in ActionButtonList)
        {
            action.UpdateActionState(currentState);
        }
    }

    /// <summary>
    /// Call this method to update action button state respect to current amount of event ticket.
    /// </summary>
    private void UpdateAllActionState()
    {
        for (int i = 0; i < RequireTicket.Count; i++)
        {
            ActionButtonList[i].Init(i, this);
            if (_ticket.Amount < RequireTicket[i])
                ActionButtonList[i].UpdateActionState(false);
            else
                ActionButtonList[i].UpdateActionState(true);
        }
    }

    /// <summary>
    /// Call this method to update ticket UI
    /// </summary>
    private void UpdateTicketUI(UnityAction onComplete)
    {
        DataManager.Instance.LoadInventory(delegate
        {
            string ticketID = GameEventTemplateKeys.TicketIDPrefixKey + _eventThemeInfo.Theme;
            int ticketAmount = DataManager.Instance.InventoryData.GetAmountByItemID(ticketID);
            
            _ticket = new ItemData(ticketID, ticketAmount);
            EventTicketText.text = $"x{_ticket.Amount}";

            onComplete?.Invoke();

        }, null );
    }

    /// <summary>
    /// Call this method to update boss HP gauge.
    /// </summary>
    private void UpdateBossHPGauge()
    {
        EventBossHpSlider.value = (float)_currentRaidBossHP / _tierBossHpList[0];
        EventBossHpSlider.gameObject.SetActive(true);
    }

    /// <summary>
    /// Call this method to update raid rank status.
    /// </summary>
    private void UpdateRaidRank(UnityAction onComplete = null)
    {
        int rankIndex = 0;

        for (int i = 0; i < _tierBossHpList.Count; i++)
        {
            if (_currentRaidBossHP <= _tierBossHpList[i])
                rankIndex = i;
            else
                break;
        }

        if (rankIndex > _rewardClearedCount)
        {
            _rewardClearedCount = rankIndex;

            // Update rank sprite.

            EventRankImage.sprite = SpriteResourceHelper.LoadSubEventImage(_eventKey, EventRankPrefixKey + _rankList[_rewardClearedCount]);
            EventRankImage.rectTransform.localScale = new Vector3(1.5f, 1.5f, 1.5f);

            EventRankImage.rectTransform.DOScale(Vector3.one, 0.5f).SetEase(Ease.Linear).onComplete += delegate
            {
                // Update chest.
                _rewardChestButtonScripts[_rewardClearedCount - 1].SetRewardCleared();

                if (_rewardClearedCount == _tierBossHpList.Count - 1)
                    UpdateBossDefeat(onComplete);
                else
                    onComplete?.Invoke();
            };
        }
        else
        {
            onComplete?.Invoke();
        }
    }

    /// <summary>
    /// Call this method to update boss defeat status.
    /// </summary>
    /// <param name="onComplete"></param>
    private void UpdateBossDefeat(UnityAction onComplete = null)
    {
        if (_rewardClearedCount != _tierBossHpList.Count - 1)
            return;

        EventBossGrayScaleImage.DOFade(1, 1f).SetEase(Ease.Linear).onComplete += delegate
        {
            _selectedDefeatImage.SetNativeSize();
            _selectedDefeatImage.rectTransform.localScale = Vector3.one * 1.5f;
            _selectedDefeatImage.gameObject.SetActive(true);
            _selectedDefeatImage.rectTransform.DOScale(Vector3.one, 0.5f).SetEase(Ease.OutExpo).onComplete += delegate
            {
                SoundManager.PlaySFX(SoundManager.SFX.Soul_Attack_Hit_Holy);
                onComplete?.Invoke();
            };
        };
    }
    #endregion

    #region Action Methods

    /// <summary>
    /// Call this method when the action is selected.
    /// </summary>
    /// <param name="caller">Action button instance.</param>
    /// <param name="actionIndex">Action index.</param>
    public void OnSelectAction(object caller, int actionIndex)
    {
        // Check caller type.
        // If it doesn't match, return this method.
        if (caller.GetType() != typeof(EventRaidBossAction))
            return;

        UpdateAllActionState(false);
        UpdateGeneralButtonsState(false);

        // Event quest ticket condition.
        if (PlayTriggerType == GameEventStartType.Ticket && _ticket.Amount < RequireTicket[actionIndex])
        {
            string noTicketTitle = LocalizationManager.Instance.GetText(GameEventTemplateKeys.NoTicketTitleKey);
            string noTicketMessage = LocalizationManager.Instance.GetText(GameEventTemplateKeys.NoTicketMessageKey);
            PopupUIManager.Instance.ShowPopup_Error(
                noTicketTitle
                , noTicketMessage
                , delegate
                {
                    UpdateAllActionState();
                    UpdateGeneralButtonsState(true);
                }
            );
            return;
        }

        OnExecuteAction(actionIndex);
    }

    /// <summary>
    /// Call this method to resolve the raid boss attack action.
    /// </summary>
    /// <param name="actionIndex">Action index.</param>
    private void OnExecuteAction(int actionIndex)
    {
        if(_isActionTaken)
            return;
        _isActionTaken = true;

        int currentTicketAmount = _ticket.Amount - RequireTicket[actionIndex];
        EventTicketText.text = $"x{currentTicketAmount}";

        PopupUIManager.Instance.Show_SoftLoad(true);

        DataManager.Instance.ExcecuteEventCloudScript(
            _actionCloudScriptFunctionName
            , new
            {
                EventKey = _eventKey
                , ActionIndex = actionIndex
                , TicketID = _ticket.ItemID
            }
            , delegate (CloudScriptResultProcess result)
            {
                if (result.IsSuccess)
                {
                    _coroutine = StartCoroutine(ActionRewardGenerate(
                        ActionButtonList[actionIndex].transform
                        , _participateRewardList[actionIndex]
                        , ActionDefaultDiagonalList[actionIndex]
                        , delegate
                        {
                            StopCoroutine(_coroutine);

                            _movingParticleList[actionIndex].SetActive(true);
                            OnPlayActionEffect(
                                actionIndex
                                , delegate
                                {
                                    if (GameHelper.TryDeserializeJSONStr(result.CustomData[RaidBossHPKey], out _currentRaidBossHP))
                                    {
                                        EventBossHealthText.text = $"HP: {Mathf.Clamp(_currentRaidBossHP, 0, _tierBossHpList[0]):n0} / {_tierBossHpList[0]:n0}";
                                        EventCurrentDamageText.text = (_tierBossHpList[0] - _currentRaidBossHP).ToString("n0") + " " + LocalizationManager.Instance.GetText(EventCurrentDamageUnitKey);
                                        UpdateRaidRank(delegate
                                        {
                                            UpdateBossHPGauge();
                                            UpdateTicketUI(
                                                delegate
                                                {
                                                    _isActionTaken = false;
                                                    UpdateAllActionState();
                                                    UpdateGeneralButtonsState(true);
                                                    PopupUIManager.Instance.Show_SoftLoad(false);
                                                }
                                            );
                                        });
                                    }
                                    else
                                    {
                                        UpdateTicketUI(
                                            delegate
                                            {
                                                _isActionTaken = false;
                                                UpdateAllActionState();
                                                UpdateGeneralButtonsState(true);
                                                PopupUIManager.Instance.Show_SoftLoad(false);
                                            }
                                        );
                                    }
                                }
                            );
                        })
                    );

                }
                else
                {
                    UpdateTicketUI(
                        delegate
                        {
                            _isActionTaken = false;
                            UpdateAllActionState();
                            UpdateGeneralButtonsState(true);
                            PopupUIManager.Instance.Show_SoftLoad(false);
                        }
                    );
                }
            }
            , delegate (string error)
            {
                Debug.LogError(error);

                UpdateTicketUI(
                    delegate
                    {
                        _isActionTaken = false;
                        UpdateAllActionState(true);
                        UpdateGeneralButtonsState(true);
                        PopupUIManager.Instance.Show_SoftLoad(false);
                    }
                );
            }
        );
    }

    /// <summary>
    /// Call this method to show the action effect.
    /// </summary>
    /// <param name="actionIndex">Action index.</param>
    /// <param name="onComplete">Delegate method when this method is finish.</param>
    private void OnPlayActionEffect(int actionIndex, UnityAction onComplete)
    {
        switch (_attackEffect)
        {
            case RaidAttackEffect.Projectile:

                SoundManager.PlaySFX(SoundManager.SFX.Soul_Attack_Start_Holy);
                _movingParticleList[actionIndex].transform.DOPath(
                    _wayPointReferenceList.ToArray()
                    , 0.75f
                    , PathType.CatmullRom).SetEase(Ease.InOutExpo).onComplete += delegate
                {
                    _movingParticleList[actionIndex].SetActive(false);
                    _movingParticleList[actionIndex].transform.position = _particleDefaultPosList[actionIndex];
                    
                    _burstParticleList[actionIndex].SetActive(true);

                    SoundManager.PlaySFX(SoundManager.SFX.Soul_Attack_Hit_Holy);
                    CameraScreenShake(25 *_actionDamageValueList[actionIndex]);
                    StartCoroutine(TimeCountDown(0.5f, delegate
                    {
                        _burstParticleList[actionIndex].SetActive(false);
                        onComplete?.Invoke();
                    }));
                };

                break;
            case RaidAttackEffect.Sword:
                break;
        }
    }

    /// <summary>
    /// Call this method to receive chest reward.
    /// </summary>
    /// <param name="caller">Action button instance.</param>
    /// <param name="actionIndex">Action index.</param>
    public void OnReceiveReward(object caller, int actionIndex)
    {
        // Check caller type.
        // If it doesn't match, return this method.
        if (caller.GetType() != typeof(EventRaidBossRewardChest))
            return;

        PopupUIManager.Instance.Show_SoftLoad(true);

        DataManager.Instance.ExcecuteEventCloudScript(
            _getRewardCloudScriptFunctionName
            , new
            {
                EventKey = _eventKey, RewardIndex = actionIndex
            }
            , delegate(CloudScriptResultProcess result)
            {
                PopupUIManager.Instance.Show_SoftLoad(false);

                if (result.IsSuccess)
                {
                    string title = LocalizationManager.Instance.GetText(GameEventTemplateKeys.NotiRewardTitleKey);
                    string message = LocalizationManager.Instance.GetText(GameEventTemplateKeys.NotiRewardMessageKey);
                    PopupUIManager.Instance.ShowPopup_GotRewardAnimated(
                        title
                        , message
                        , _tierRewardList[actionIndex]
                        , delegate
                        {
                            _rewardChestButtonScripts[actionIndex - 1].SetRewardReceived();
                            DataManager.Instance.LoadInventory(null, null);
                            OnReceiveItem(_tierRewardList[actionIndex]);
                        }
                    );
                }
            }
            ,
            delegate(string error)
            {
                Debug.LogError(error);
                PopupUIManager.Instance.Show_SoftLoad(false);
            }
        );
    }

    /// <summary>
    /// Call this method to preview reward in chest.
    /// </summary>
    /// <param name="caller"></param>
    /// <param name="actionIndex"></param>
    public void OnPreviewReward(object caller, int actionIndex)
    {
        if (LocalizationManager.Instance.GetText(EventRewardTitle, out string title))
            title = string.Format(title, _rankList[actionIndex]);
        if (LocalizationManager.Instance.GetText(EventRewardMessage, out string message))
            message = string.Format(message, (_tierBossHpList[0] - _tierBossHpList[actionIndex]).ToString("n0"));

        //PopupUIManager.Instance.ShowPopup_PackDetail_Short(
        //    title
        //    , message
        //    , _tierRewardList[actionIndex]
        //    , null
        //);

        PopupUIManager.Instance.ShowPopup_PreviewReward(
            title
            , message
            , _tierRewardList[actionIndex]
        );
    }

    #endregion

    #region IEnumurator Method

    private IEnumerator TimeCountDown(float time, UnityAction onComplete)
    {
        yield return new WaitForSeconds(time);
        onComplete?.Invoke();
    }

    #endregion

    #region Other Methods

    /// <summary>
    /// Call this method to show read more.
    /// </summary>
    public void ShowReadMore()
    {
        //EventDetailImage.gameObject.SetActive(true);
        string title = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventDetailHeaderKey);
        string message = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventDetailKey + _eventKey);

        PopupUIManager.Instance.ShowPopup_DetailWindow(title, message);
    }

    /// <summary>
    /// Call this method to hide read more.
    /// </summary>
    public void HideReadMore()
    {
        EventDetailImage.gameObject.SetActive(false);
    }

    /// <summary>
    /// Call this method to shake main camera.
    /// </summary>
    /// <param name="strength"></param>
    public void CameraScreenShake(float strength = 5)
    {
        _mainCamera.DOShakePosition(0.15f, 0.02f * strength, 1, 2).SetEase(Ease.OutElastic).OnComplete(delegate
        {
            _mainCamera.transform.localPosition = _defaultCameraPosition;
        });
    }
    #endregion

    #endregion
}

public enum RaidAttackEffect
{
    Projectile = 0
    , Sword
}