﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameEventRaidBossManager : BaseGameEventTemplate
{
    #region Static Value

    // Localization Key
    private static readonly string EventRewardTitle = "EVENT_RAID_BOSS_REWARD_TITLE_";
    private static readonly string EventRewardMessage = "EVENT_RAID_BOSS_REWARD_MESSAGE_";

    #endregion

    #region Public Properties
    public RaidBossEventData EventInfoData { get; protected set; }
    public RaidBossPlayerData PlayerData { get; protected set; }
    #endregion

    #region Private Properties

    [Header("CloudScript Function Name")]
    [SerializeField] private string _initCloudScriptFunctionName;
    [SerializeField] private string _getRewardCloudScriptFunctionName;

    [Header("UI Controller")]
    [SerializeField] private GameEventRaidBossUIManager _uiController;
    [SerializeField] private SubEventQuestManager _subEventQuestManager;

    #endregion

    #region Methods

    public override void Init(string eventKey, string sceneName, UnityAction onComplete = null, UnityAction onFail = null)
    {
        base.Init(eventKey, sceneName, null);

        //await _subEventQuestManager.SetupData(this);
        DataManager.Instance.ExcecuteEventCloudScript(
            _initCloudScriptFunctionName
            , new { EventKey = eventKey }
            , result =>
            {
                if (!result.IsSuccess)
                {
                    onFail?.Invoke();
                    return;
                }

                // DataManager.Instance.LoadInventory(null, null);

                EventInfoData = new RaidBossEventData(eventKey, result);
                PlayerData = new RaidBossPlayerData(result);

                if (EventInfoData.IsLoadedSuccess && PlayerData.IsLoadedSuccess)
                {
                    InitUI();
                    onComplete?.Invoke();
                }
                else
                {
                    onFail?.Invoke();
                }
            }
            , errMsg =>
            {
                onFail?.Invoke();
            }
        );
    }

    private void InitUI()
    {
        _uiController.InitUi(EventInfoData, PlayerData);
        _uiController.SetReceiveRewardBtn(OnReceiveReward);
        _uiController.SetPreviewRewardBtn(OnPreviewReward);
        _uiController.SetEventInfoBtn(OnShowEventInfo);
    }

    private void OnReceiveReward(int rewardIndex, UnityAction onComplete)
    {
        PopupUIManager.Instance.Show_SoftLoad(true);

        DataManager.Instance.ExcecuteEventCloudScript(
            _getRewardCloudScriptFunctionName
            , new
            {
                EventKey = _eventKey,
                RewardIndex = rewardIndex
            }
            , result =>
            {
                PopupUIManager.Instance.Show_SoftLoad(false);

                if (result.IsSuccess)
                {
                    string title = LocalizationManager.Instance.GetText(GameEventTemplateKeys.NotiRewardTitleKey);
                    string message = LocalizationManager.Instance.GetText(GameEventTemplateKeys.NotiRewardMessageKey);
                    PopupUIManager.Instance.ShowPopup_GotRewardAnimated(
                        title
                        , message
                        , EventInfoData.TierRewardList[rewardIndex]
                        , delegate
                        {
                            onComplete?.Invoke();
                            DataManager.Instance.LoadInventory(null, null);
                            OnReceiveItem(EventInfoData.TierRewardList[rewardIndex]);
                        }
                    );

                    DataManager.Instance.InventoryData.GrantItemByItemList(EventInfoData.TierRewardList[rewardIndex]);
                    //string ecJson = result.CustomData[GameEventTemplateKeys.EventCurrencyKey];
                    //DataManager.Instance.InventoryData.SetEventCurrency(ecJson);
                }
            }
            , err =>
            {
                Debug.LogError(err);
                PopupUIManager.Instance.Show_SoftLoad(false);
            }
        );
    }

    private void OnPreviewReward(int rewardIndex)
    {
        if (LocalizationManager.Instance.GetText(EventRewardTitle + EventInfoData.EventKey, out string title))
            title = string.Format(title, EventInfoData.RankList[rewardIndex]);
        else
            title = EventRewardTitle + EventInfoData.EventKey;

        if (LocalizationManager.Instance.GetText(EventRewardMessage + EventInfoData.EventKey, out string message))
            message = string.Format(message, (EventInfoData.TierBossHpList[0] - EventInfoData.TierBossHpList[rewardIndex]).ToString("n0"));
        else
            message = EventRewardMessage + EventInfoData.EventKey;

        PopupUIManager.Instance.ShowPopup_PreviewReward(
            title
            , message
            , EventInfoData.TierRewardList[rewardIndex]
        );
    }

    private void OnShowEventInfo()
    {
        //EventDetailImage.gameObject.SetActive(true);
        string title = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventDetailHeaderKey);
        string message = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventDetailKey + _eventKey);

        PopupUIManager.Instance.ShowPopup_DetailWindow(title, message);
    }

    #endregion
}

public class RaidBossEventData : BaseGameEventData
{
    public struct BossWaveDetail
    {
        public DateTime StartDate;
        public DateTime EndDate;

        public BossWaveDetail(DateTime startDate, DateTime endDate)
        {
            StartDate = startDate;
            EndDate = endDate;
        }

        public bool IsValidDate()
        {
            DateTime now = DateTimeData.GetDateTimeUTC();
            return now > StartDate && now < EndDate;
        }
    }

    #region CloudScript Key
    private static readonly string TierDataKey = "tier_data";
    private static readonly string RaidBossHPKey = "raid_boss_hp";
    private static readonly string BossHPKey = "boss_hp";
    private static readonly string RankKey = "rank";
    private static readonly string BossWaveDetailKey = "boss_wave_detail";
    #endregion

    #region Public Properties

    public List<int> TierBossHpList;
    public List<List<ItemData>> TierRewardList;
    public List<string> RankList;
    public int CurrentRaidBossHp;
    public int RewardClearedCount;
    public int BossImageChangeInterval;
    public Dictionary<string, BossWaveDetail> BossWaveDetailDict = new Dictionary<string, BossWaveDetail>();
    #endregion

    #region Constructor

    public RaidBossEventData(string eventKey, CloudScriptResultProcess eventData) : base(eventKey, eventData)
    {
        // Create new data instance for boss data.
        TierRewardList = new List<List<ItemData>>();
        TierBossHpList = new List<int>();
        RankList = new List<string>();

        _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(eventData.CustomData[GameEventTemplateKeys.EventDataKey], out Dictionary<string, object> eventDataDict);
        _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(eventDataDict[TierDataKey].ToString(), out List<object> tierDataList);

        // Add boss data.
        foreach (object tierItem in tierDataList)
        {
            // Check data accessible.
            _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(tierItem.ToString(), out Dictionary<string, object> tierDictionary);
            _isLoadedSuccess &= int.TryParse(tierDictionary[BossHPKey].ToString(), out int bossHp);
            _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(tierDictionary[GameEventTemplateKeys.RewardListKey].ToString(), out List<object> rewardObjectList);
            _isLoadedSuccess &= tierDictionary.ContainsKey(RankKey);
            if (!_isLoadedSuccess) return;

            // Set data value to variable.
            TierBossHpList.Add(bossHp);
            RankList.Add(tierDictionary[RankKey].ToString());

            List<ItemData> rewardList = new List<ItemData>();
            foreach (object rewardItem in rewardObjectList)
            {
                _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(rewardItem.ToString(), out Dictionary<string, object> rewardObjectDictionary);
                _isLoadedSuccess &= rewardObjectDictionary.ContainsKey(GameEventTemplateKeys.ItemIDKey);
                _isLoadedSuccess &= rewardObjectDictionary.ContainsKey(GameEventTemplateKeys.ItemAmountKey);
                if (!_isLoadedSuccess) return;

                _isLoadedSuccess &= int.TryParse(rewardObjectDictionary[GameEventTemplateKeys.ItemAmountKey].ToString(), out int rewardAmount);
                if (!_isLoadedSuccess) return;

                rewardList.Add(new ItemData(
                    rewardObjectDictionary[GameEventTemplateKeys.ItemIDKey].ToString()
                    , rewardAmount)
                );
            }
            TierRewardList.Add(rewardList);
        }

        // Boss Hp data.
        _isLoadedSuccess &= int.TryParse(eventDataDict[RaidBossHPKey].ToString(), out CurrentRaidBossHp);
        
        GameHelper.TryDeserializeJSONStr(eventDataDict[BossWaveDetailKey].ToString(), out Dictionary<string, object> rawBossWaveDetailDict);
        foreach (var item in rawBossWaveDetailDict)
        {
            string key = item.Key;
            GameHelper.TryDeserializeJSONStr(item.Value.ToString(), out Dictionary<string, object> rawData);
            DateTime startDate = Convert.ToDateTime(rawData["start_date"].ToString());
            DateTime endDate = Convert.ToDateTime(rawData["end_date"].ToString());
            BossWaveDetailDict.Add(key, new BossWaveDetail(startDate, endDate));
        }

        // Rank Data
        for (int i = 0; i < TierBossHpList.Count; i++)
        {
            if (CurrentRaidBossHp <= TierBossHpList[i])
                RewardClearedCount = i;
            else
                break;
        }
    }

    #endregion
}

public class RaidBossPlayerData
{
    #region CloudScript Key
    private static readonly string ReceivedRewardKey = "received_reward";
    #endregion

    #region Public Properties

    public Dictionary<string, object> ReceivedRewardDictionary;
    public bool IsLoadedSuccess => _isLoadedSuccess;

    #endregion

    #region Protected Properties

    protected bool _isLoadedSuccess = true;

    #endregion

    #region Constructor

    public RaidBossPlayerData(CloudScriptResultProcess eventData)
    {
        _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(eventData.CustomData[GameEventTemplateKeys.PlayerDataKey], out Dictionary<string, object> playerDataDictionary);
        if (!_isLoadedSuccess) return;

        _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(playerDataDictionary[ReceivedRewardKey].ToString(), out ReceivedRewardDictionary);
    }

    #endregion
}
