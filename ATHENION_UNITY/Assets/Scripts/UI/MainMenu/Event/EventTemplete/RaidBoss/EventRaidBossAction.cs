﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class EventRaidBossAction : MonoBehaviour
{
    #region Properties

    public List<Image> ImageList;
    public TextMeshProUGUI DamageText;
    public Material GrayScaleMaterial;

    private Button _attackBossButton;
    private Color _textColor;

    #endregion

    #region Methods

    /// <summary>
    /// Call this method to initialize this action button.
    /// </summary>
    /// <param name="actionIndex">Action index.</param>
    /// <param name="manager">The raid boss event template.</param>
    public void Init(int actionIndex, GameEventTemplateRaidBoss manager = null, GameEventTemplateRaidBoss_2nd manager2 = null)
    {
        _attackBossButton = GetComponent<Button>();
        _attackBossButton.onClick.AddListener(delegate
        {
            manager?.OnSelectAction(this, actionIndex);
            manager2.OnSelectAction(this, actionIndex);
        });

        _textColor = DamageText.color;
    }

    /// <summary>
    /// Call this method to update action button state.
    /// </summary>
    /// <param name="actionState">Current action state.</param>
    public void UpdateActionState(bool actionState)
    {
        if (actionState)
        {
            foreach (Image image in ImageList)
            {
                image.material = null;
            }

            DamageText.color = _textColor;

            _attackBossButton.enabled = true;
        }
        else
        {
            foreach (Image image in ImageList)
            {
                image.material = GrayScaleMaterial;
            }

            DamageText.color = Color.white;

            _attackBossButton.enabled = false;
        }
    }
    #endregion
}
