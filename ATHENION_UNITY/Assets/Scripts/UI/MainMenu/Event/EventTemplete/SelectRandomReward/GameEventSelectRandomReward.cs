﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(GameEventSelectRandomRewardUI))]
public class GameEventSelectRandomReward : BaseGameEventTemplate
{
    public readonly string LocalizationKeyPrefix = "EVENT_RED_ENVELOPES_2020_";

    public SelectRandomRewardEvent_CSEventData SelectRandomRewardEventData { get; protected set; }
    public SelectRandomRewardEvent_CSPlayerData PlayerEventData { get; protected set; }

    private GameEventSelectRandomRewardUI _gameEventSelectRandomReward;

    public override void Init(string eventKey, string sceneName, UnityAction onComplete = null, UnityAction onFail = null)
    {
        base.Init(eventKey, sceneName);
        _gameEventSelectRandomReward = GetComponent<GameEventSelectRandomRewardUI>();
        
        PlayFabManager.Instance.ExecuteCloudScript(
            "SelectRandomRewardEventRefreshData"
            , new { EventKey = _eventKey }
            , (result) =>
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (!resultProcess.IsSuccess)
                {
                    onFail?.Invoke();
                }

                // Data Initialize.
                bool isLoadDataSuccess = true;
                isLoadDataSuccess &= GameHelper.TryDeserializeJSONStr(resultProcess.CustomData["event_data"], out SelectRandomRewardEvent_CSEventData eventDataResponse);
                isLoadDataSuccess &= GameHelper.TryDeserializeJSONStr(resultProcess.CustomData["player_data"], out SelectRandomRewardEvent_CSPlayerData playerEventDataResponse);
                if (isLoadDataSuccess)
                {
                    SelectRandomRewardEventData = eventDataResponse;
                    PlayerEventData = playerEventDataResponse;
                    _gameEventSelectRandomReward.Init(() => 
                    {
                        onComplete?.Invoke();
                    });
                }
                else
                {
                    onFail?.Invoke();
                }
            }
            , null
        );
    }

    public TimeSpan GetActionCooldownTime()
    {
        if (!PlayerEventData.action_timestamp.HasValue)
        {
            return TimeSpan.Zero;
        }

        return PlayerEventData.action_timestamp.Value.AddMilliseconds(SelectRandomRewardEventData.default_cooldown) - DateTimeData.GetDateTimeUTC();
    }
    
    public void InvokeSelectReward(int selectingIndex, UnityAction<List<ItemData>, string, string> onComplete = null)
    {
        if (PlayerEventData.claimed_reward_index_list.ContainsKey(selectingIndex.ToString()))
        {
            return;
        }
        else if (PlayerEventData.remain_playtime <= 0)
        {
            return;
        }
        else if (GetActionCooldownTime() > TimeSpan.Zero)
        {
            PopupUIManager.Instance.ShowPopup_ConfirmOnlyWithWarning(
                LocalizationManager.Instance.GetText("EVENT_COOLDOWN"),
                LocalizationManager.Instance.GetText("EVENT_ONCOOLDOWN"));
            return;
        }

        PopupUIManager.Instance.Show_SoftLoad(true);
        PlayFabManager.Instance.ExecuteCloudScript(
            "SelectRandomRewardEventTakeAction"
            , new
            {
                EventKey = _eventKey,
                ActionIndex = selectingIndex
            }, (result) =>
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (!resultProcess.IsSuccess)
                {
                    PopupUIManager.Instance.ShowPopup_Error(
                        LocalizationManager.Instance.GetText("ERROR_POPUP_HEADER"),
                        LocalizationManager.Instance.GetText(resultProcess.MessageDetail));

                    PopupUIManager.Instance.Show_SoftLoad(false);
                    return;
                }

                bool isLoadDataSuccess = true;
                isLoadDataSuccess = GameHelper.TryDeserializeJSONStr(resultProcess.CustomData["player_data"], out SelectRandomRewardEvent_CSPlayerData playerEventDataResponse);
                if (isLoadDataSuccess)
                {
                    PlayerEventData = playerEventDataResponse;

                    GameHelper.TryDeserializeJSONStr(resultProcess.CustomData["selected_rewards"], out Dictionary<string, int> receivedItemsDict);
                    List<ItemData> receivedItems = new List<ItemData>();
                    foreach (KeyValuePair<string, int> item in receivedItemsDict)
                    {
                        receivedItems.Add(new ItemData(item));
                    }

                    OnReceiveItem(receivedItems); //Analytics
                    PopupUIManager.Instance.Show_SoftLoad(false);
                    onComplete?.Invoke(receivedItems, resultProcess.CustomData["reward_rarity"], resultProcess.CustomData["event_currency"]);
                }
                else
                {
                    PopupUIManager.Instance.Show_SoftLoad(false);
                }
            }
            , null
        );
    }
}

public class SelectRandomRewardEvent_CSEventData
{
    public string play_trigger;
    public string[] require_ticket;
    public int default_playtime;
    public int default_cooldown;
    public bool daily_repeatable;
    public Dictionary<string, int> action_rewards;
}

public class SelectRandomRewardEvent_CSPlayerData
{
    public DateTime? daily_timestamp;
    public DateTime? action_timestamp;
    public int remain_playtime;
    public Dictionary<string, bool> claimed_reward_index_list;
}

