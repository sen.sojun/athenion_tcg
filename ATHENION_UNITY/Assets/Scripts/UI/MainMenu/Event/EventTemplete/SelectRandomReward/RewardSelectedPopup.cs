﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(Animator))]
public class RewardSelectedPopup : MonoBehaviour
{
    [SerializeField] private GameObject _animationUIGroup;
    [SerializeField] private Animator _animator;
    [SerializeField] private Image _mainImage;
    [SerializeField] private Sprite _defaultMainImage;

    private UnityEvent _onAnimationComplete = new UnityEvent();
    private List<Sprite> _imagesToSwap = new List<Sprite>();

    public void PlayRewardSelectedAnimation(List<Sprite> imagesToSwap, UnityAction onComplete)
    {
        _mainImage.sprite = _defaultMainImage;
        _imagesToSwap = imagesToSwap;
        _onAnimationComplete.AddListener(onComplete);
        _animator.SetTrigger("RewardSelected");
    }

    public void SwapMainImage(int spriteIndex)
    {
        _mainImage.sprite = _imagesToSwap[spriteIndex];
    }

    public void InvokeOnAnimationCompleteEvent()
    {
        _onAnimationComplete?.Invoke();
        _onAnimationComplete?.RemoveAllListeners();
        _imagesToSwap.Clear();
    }
    
    public void PlaySFX(SoundManager.SFX sound)
    {
        SoundManager.PlaySFX(sound);
    }
}
