﻿using Coffee.UIExtensions;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(GameEventSelectRandomReward))]
public class GameEventSelectRandomRewardUI : MonoBehaviour
{
    [Header("Sprites")]
    public Sprite ClosedSelectionSprite;
    public Sprite OpenedSelectionSprite;
    public Sprite OpenedSelectionSpriteCommon;
    public Sprite OpenedSelectionSpriteUncommon;
    public Sprite OpenedSelectionSpriteRare;

    [SerializeField] private RewardSelectedPopup _rewardSelectedPopup;
    [SerializeField] private GameObject[] _selectionHighlights;

    [Header("Texts")]
    [SerializeField] private TMP_Text _mainEventText;

    [Header("Buttons")]
    [SerializeField] private Button _readMoreButton;
    [SerializeField] private Button[] _selectRewardButtons;

    [Header("Image UIs")]
    [SerializeField] private Image _eventBanner;
    [SerializeField] private Image _subBG;
    [SerializeField] private Image[] _selectRewardImages;

    private GameEventSelectRandomReward _gameEventSelectRandomReward;
    private Coroutine _countdownCoroutine;

    public void Init(UnityAction onComplete = null)
    {
        _gameEventSelectRandomReward = GetComponent<GameEventSelectRandomReward>();

        #region Initialize Buttons
        _readMoreButton.onClick.AddListener(() => ShowReadMore());
        for (int i = 0; i < _selectRewardButtons.Length; i++)
        {
            int selectingIndex = i;
            _selectRewardButtons[i].onClick.AddListener(() => 
            {
                _gameEventSelectRandomReward.InvokeSelectReward(selectingIndex, (selectedRewards, rewardRarity, updatedEventCurrency) => 
                {
                    ShowSelectedReward(selectedRewards, rewardRarity, () => 
                    {
                        // TODO: Reword event currency update system.
                        //DataManager.Instance.InventoryData.SetEventCurrency(updatedEventCurrency);
                        UpdateUI();
                    });
                });
            });
        }
        #endregion

        UpdateUI();
        onComplete?.Invoke();
    }

    public void UpdateUI()
    {
        for(int i=0; i< _selectRewardImages.Length; i++)
        {
            if (_gameEventSelectRandomReward.PlayerEventData.claimed_reward_index_list.ContainsKey(i.ToString()))
            {
                _selectRewardImages[i].sprite = OpenedSelectionSprite;
                _selectRewardImages[i].GetComponent<UIShiny>().enabled = false;
                _selectRewardButtons[i].enabled = false;
                _selectRewardButtons[i].GetComponent<ButtonSound>().enabled = false;
                _selectionHighlights[i].SetActive(false);
            }
            else
            {
                _selectRewardImages[i].sprite = ClosedSelectionSprite;
                if(_gameEventSelectRandomReward.PlayerEventData.remain_playtime <= 0)
                {
                    _selectRewardImages[i].GetComponent<UIShiny>().enabled = false;
                    _selectRewardButtons[i].enabled = true;
                    _selectRewardButtons[i].GetComponent<ButtonSound>().enabled = false;
                    _selectionHighlights[i].SetActive(false);
                }
                else
                {
                    _selectRewardImages[i].GetComponent<UIShiny>().enabled = true;
                    _selectRewardButtons[i].enabled = true;
                    _selectRewardButtons[i].GetComponent<ButtonSound>().enabled = true;
                    _selectionHighlights[i].SetActive(true);
                }
            }
        }
    }

    private void OnEnable()
    {
        _countdownCoroutine = StartCoroutine(Counting());
    }

    private void OnDisable()
    {
        if (_countdownCoroutine != null)
        {
            StopCoroutine(_countdownCoroutine);
        }
    }

    private void ShowSelectedReward(List<ItemData> claimedRewards, string rewardRarity, UnityAction onComplete = null)
    {
        PopupUIManager.Instance.Show_SoftLoad(true);
        Sprite envelopeSpriteToShow = GetSpriteByRewardRarity(rewardRarity);
        _rewardSelectedPopup.PlayRewardSelectedAnimation(new List<Sprite> { envelopeSpriteToShow }, () =>
        {
            PopupUIManager.Instance.ShowPopup_GotRewardAnimated(
                LocalizationManager.Instance.GetText(_gameEventSelectRandomReward.LocalizationKeyPrefix + "POPUP_TITLE_REWARDED"),
                LocalizationManager.Instance.GetText(_gameEventSelectRandomReward.LocalizationKeyPrefix + "POPUP_BODY_REWARDED"),
                claimedRewards);

            PopupUIManager.Instance.Show_SoftLoad(false);
            onComplete?.Invoke();
        });
    }

    private Sprite GetSpriteByRewardRarity(string rarity)
    {
        switch (rarity)
        {
            case "common": return OpenedSelectionSpriteCommon;
            case "uncommon": return OpenedSelectionSpriteUncommon;
            case "rare": return OpenedSelectionSpriteRare;
            default : return OpenedSelectionSpriteCommon;
        }
    }

    private IEnumerator Counting()
    {
        while (true)
        {
            if (!_gameEventSelectRandomReward.PlayerEventData.action_timestamp.HasValue)
            {
                _mainEventText.text = LocalizationManager.Instance.GetText(_gameEventSelectRandomReward.LocalizationKeyPrefix + "MAIN_TEXT_HAS_PLAYTIME");
            }
            else
            {
                TimeSpan cooldownTime = _gameEventSelectRandomReward.GetActionCooldownTime();
                if (cooldownTime.TotalMilliseconds <= 0)
                {
                    if(_gameEventSelectRandomReward.PlayerEventData.remain_playtime > 0)
                    {
                        _mainEventText.text = LocalizationManager.Instance.GetText(_gameEventSelectRandomReward.LocalizationKeyPrefix + "MAIN_TEXT_HAS_PLAYTIME");
                    }
                    else
                    {
                        _mainEventText.text = LocalizationManager.Instance.GetText(_gameEventSelectRandomReward.LocalizationKeyPrefix + "MAIN_TEXT_NO_PLAYTIME");
                    }
                }
                else
                {
                    _mainEventText.text = GameHelper.GetCountdownText(cooldownTime);
                }
            }

            yield return new WaitForSeconds(1f);
        }
    }

    private void ShowReadMore()
    {
        string title = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventDetailHeaderKey);
        string message = LocalizationManager.Instance.GetText("EVENT_MOREDETAIL_" + _gameEventSelectRandomReward.EventKey.ToUpper());

        PopupUIManager.Instance.ShowPopup_DetailWindow(title, message);
    }
}
