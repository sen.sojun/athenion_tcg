﻿using System;
using System.Collections;
using System.Collections.Generic;
using GameEvent;
using Karamucho;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public abstract class BaseGameEventTemplate : MonoSingleton<BaseGameEventTemplate>
{
    #region Properties

    public string SceneName { get; protected set; }
    public string EventKey
    {
        get
        {
            return _eventKey;
        }
    }
    public GameEventInfo EventData
    {
        get
        {
            return _eventData;
        }
    }
    public GameEventThemeInfo EventThemeInfo
    {
        get
        {
            return _eventThemeInfo;
        }
    }

    protected string _eventKey;
    protected GameEventInfo _eventData;
    protected GameEventThemeInfo _eventThemeInfo;

    protected GameEventStartType PlayTriggerType;
    protected List<int> RequireTicket;
    protected int DefaultPlaytime;
    protected float DefaultCooldown;
    protected bool IsRepeatable;
    protected Dictionary<string, object> EventDataDict;

    protected bool IsRequireTicket { get { return PlayTriggerType == GameEventStartType.Ticket || PlayTriggerType == GameEventStartType.Ticket_Cooldown; } }

    #region Fading Setting

    [Header("Fading Setting")]
    [SerializeField] protected CanvasGroup FadeIn;
    [SerializeField] protected CanvasGroup FadeOut;
    #endregion
    #endregion

    #region Method

    /// <summary>
    /// Call this method to initialize the event template.
    /// </summary>
    /// <param name="eventKey">Event key.</param>
    /// <param name="sceneName">Scene's name.</param>
    /// <param name="onComplete"></param>
    /// <param name="onFail"></param>
    public virtual void Init(string eventKey, string sceneName, UnityAction onComplete = null, UnityAction onFail = null)
    {
        // Initialize template.

        _eventKey = eventKey;
        SceneName = sceneName;

        GameEventDB eventDB = DataManager.Instance.GetGameEventDB();
        _eventData = eventDB.GetGameEventInfo()[_eventKey];
        _eventThemeInfo = eventDB.GetCurrentTheme();
        // Do something

        //onComplete?.Invoke();
    }

    /// <summary>
    /// Virtual method of initial event data from "PlayFab".
    /// </summary>
    /// <param name="eventData">Event data.</param>
    /// <returns></returns>
    protected virtual bool InitData(CloudScriptResultProcess eventData)
    {
        bool isSuccess = true;

        #region Base Event Data

        isSuccess &= GameHelper.TryDeserializeJSONStr(eventData.CustomData[GameEventTemplateKeys.EventDataKey], out EventDataDict);
        isSuccess &= GameHelper.StrToEnum(EventDataDict[GameEventTemplateKeys.EventPlayTriggerKey].ToString(), out PlayTriggerType);
        isSuccess &= GameHelper.TryDeserializeJSONStr(EventDataDict[GameEventTemplateKeys.EventRequireTicketKey].ToString(), out RequireTicket);
        isSuccess &= int.TryParse(EventDataDict[GameEventTemplateKeys.EventDefaultPlaytimeKey].ToString(), out DefaultPlaytime);
        isSuccess &= float.TryParse(EventDataDict[GameEventTemplateKeys.EventDefaultCooldownKey].ToString(), out DefaultCooldown);
        isSuccess &= bool.TryParse(EventDataDict[GameEventTemplateKeys.EventDailyRepeatableKey].ToString(), out IsRepeatable);

        if (!isSuccess)
            return false;

        return true;

        #endregion
    }

    protected virtual void OnLoadSceneFailed(string errorString)
    {
        Close();
    }

    protected override void OnSceneUnloaded(Scene scene)
    {
        if (scene.name == SceneName)
        {
            this.DestroySelf();
        }
    }

    protected void OnEventComplete()
    {
        GameEventManager.Instance.OnEventComplete(_eventKey);
    }

    protected void OnReceiveItem(List<ItemData> items)
    {
        GameEventManager.Instance.OnReceiveEventReward(_eventKey, items);
    }

    /// <summary>
    /// Call this method to set event detail action.
    /// </summary>
    protected void OnShowEventDetail()
    {
        string title = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventDetailHeaderKey);
        string message = LocalizationManager.Instance.GetText(GameEventTemplateKeys.EventDetailKey + _eventKey);

        PopupUIManager.Instance.ShowPopup_DetailWindow(title, message);
    }

    /// <summary>
    /// Call this method to unload (close) current event template scene.
    /// </summary>
    public virtual void Close()
    {
        SceneManager.UnloadSceneAsync(SceneName);
    }

    /// <summary>
    /// Call this method to manage cooldown of current event template.
    /// </summary>
    /// <param name="lastPlayDateTime"></param>
    /// <param name="defaultCooldown"></param>
    /// <param name="cooldownText">Text UI Element</param>
    /// <param name="onComplete"></param>
    /// <returns></returns>
    public IEnumerator CooldownTimer(DateTime lastPlayDateTime, float defaultCooldown, TextMeshProUGUI cooldownText = null, UnityAction onComplete = null)
    {
        while (true)
        {
            DateTime currentDateTime = DateTimeData.GetDateTimeUTC();
            float cooldown = (float)(lastPlayDateTime - currentDateTime).TotalSeconds + defaultCooldown;

            if (cooldown > 0)
            {
                int h, m, s;
                h = Mathf.FloorToInt(cooldown / 3600);
                m = Mathf.FloorToInt((cooldown - h * 3600) / 60);
                s = Mathf.FloorToInt(cooldown % 60);
                if (h > 0)
                {
                    if(cooldownText != null)
                        cooldownText.text = $"{h:D2}:{m:D2}:{s:D2}";
                }
                else
                {
                    if (cooldownText != null)
                        cooldownText.text = $"{m:D2}:{s:D2}";
                }
            }
            else
            {
                onComplete?.Invoke();
                yield break;
            }

            yield return new WaitForEndOfFrame();
        }
    }

    public IEnumerator NewDayTimer(TextMeshProUGUI cooldownText = null, UnityAction onComplete = null)
    {
        while (true)
        {
            var currentDateTime = DateTimeData.GetDateTimeUTC();
            var newDayDateTime = GameHelper.GetNextDayUTC(currentDateTime);

            float cooldown = (float)(newDayDateTime - currentDateTime).TotalSeconds;

            if (cooldown > 0)
            {
                int h, m, s;
                h = Mathf.FloorToInt(cooldown / 3600);
                m = Mathf.FloorToInt((cooldown - h * 3600) / 60);
                s = Mathf.FloorToInt(cooldown % 60);
                if (h > 0)
                {
                    if (cooldownText != null)
                        cooldownText.text = $"{h:D2}:{m:D2}:{s:D2}";
                }
                else
                {
                    if (cooldownText != null)
                        cooldownText.text = $"{m:D2}:{s:D2}";
                }
            }
            else
            {
                onComplete?.Invoke();
                yield break;
            }

            yield return new WaitForEndOfFrame();
        }
    }

    /// <summary>
    /// Call this method to generate action reward.
    /// </summary>
    /// <param name="parentTransform">Start position parent.</param>
    /// <param name="rewardList">Reward's list.</param>
    /// <param name="onComplete"></param>
    /// <returns></returns>
    public IEnumerator ActionRewardGenerate(Transform parentTransform, List<ItemData> rewardList, bool isDefaultDiagonal = true, UnityAction onComplete = null)
    {
        int index = 0;

        GameObject eventDailyRewardGameObject = ResourceManager.Load(GameEventTemplateKeys.EventActionRewardPrefabPath) as GameObject;

        while (index < rewardList.Count)
        {
            Sprite sprite = SpriteResourceHelper.LoadSprite(rewardList[index].ItemID, SpriteResourceHelper.SpriteType.ICON_Item);
            EventActionReward item = Instantiate(eventDailyRewardGameObject, parentTransform).GetComponent<EventActionReward>();

            item.Init(sprite, rewardList[index], isDefaultDiagonal);

            if (index == rewardList.Count - 1)
            {
                yield return new WaitForSeconds(1.75f);
                onComplete?.Invoke();
                yield break;
            }
            else
            {
                yield return new WaitForSeconds(1);
            }

            index++;
        }

    }

    /// <summary>
    /// Call this method to delay action time by specific time.
    /// </summary>
    /// <param name="second"></param>
    /// <param name="onComplete"></param>
    /// <returns></returns>
    public IEnumerator DelayActionTime(float second, UnityAction onComplete)
    {
        yield return new WaitForSeconds(second);
        onComplete?.Invoke();
    }

    /// <summary>
    /// Call this method to 
    /// </summary>
    /// <param name="itemDataList"></param>
    /// <returns></returns>
    protected List<ItemData> SumRewardData(params List<ItemData>[] itemDataList)
    {
        List<ItemData> resultList = new List<ItemData>();

        Dictionary<string, int> itemDataDictionary = new Dictionary<string, int>();

        foreach (List<ItemData> itemData in itemDataList)
        {
            foreach (ItemData data in itemData)
            {
                if (itemDataDictionary.ContainsKey(data.ItemID))
                    itemDataDictionary[data.ItemID] += data.Amount;
                else
                    itemDataDictionary.Add(data.ItemID, data.Amount);
            }
        }

        foreach (KeyValuePair<string, int> keyValuePair in itemDataDictionary)
        {
            resultList.Add(new ItemData(
                keyValuePair.Key
                , keyValuePair.Value)
            );
        }

        return resultList;
    }

    #endregion
}

public class BaseGameEventData
{
    #region Public Properties

    public GameEventStartType PlayTriggerType => _playTriggerType;
    public List<int> RequireTicket => _requireTicket;
    public int DefaultPlaytime => _defaultPlaytime;
    public float DefaultCooldown => _defaultCooldown;
    public bool IsRepeatable => _isRepeatable;
    public string Theme;

    public string EventKey => _eventKey;
    public bool IsLoadedSuccess => _isLoadedSuccess;
    public GameEventThemeInfo EventThemeInfo => _eventThemeInfo;

    #endregion

    #region Protected Properties

    protected bool _isLoadedSuccess = true;
    protected string _eventKey;
    protected GameEventThemeInfo _eventThemeInfo;

    #endregion

    #region Private Properties

    private readonly GameEventStartType _playTriggerType;
    private List<int> _requireTicket;
    private int _defaultPlaytime;
    private float _defaultCooldown;
    private bool _isRepeatable;

    #endregion

    public BaseGameEventData()
    {
        
    }

    public BaseGameEventData(string eventKey, CloudScriptResultProcess eventData)
    {
        _eventKey = eventKey;
        _eventThemeInfo = DataManager.Instance.GetGameEventDB().GetCurrentTheme();
        _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(eventData.CustomData[GameEventTemplateKeys.EventDataKey], out Dictionary<string, object> EventDataDict);
        _isLoadedSuccess &= GameHelper.StrToEnum(EventDataDict[GameEventTemplateKeys.EventPlayTriggerKey].ToString(), out _playTriggerType);
        _isLoadedSuccess &= GameHelper.TryDeserializeJSONStr(EventDataDict[GameEventTemplateKeys.EventRequireTicketKey].ToString(), out _requireTicket);
        _isLoadedSuccess &= int.TryParse(EventDataDict[GameEventTemplateKeys.EventDefaultPlaytimeKey].ToString(), out _defaultPlaytime);
        _isLoadedSuccess &= float.TryParse(EventDataDict[GameEventTemplateKeys.EventDefaultCooldownKey].ToString(), out _defaultCooldown);
        _isLoadedSuccess &= bool.TryParse(EventDataDict[GameEventTemplateKeys.EventDailyRepeatableKey].ToString(), out _isRepeatable);
        _isLoadedSuccess &= EventDataDict.ContainsKey(GameEventTemplateKeys.EventMainThemeKey);
        if(_isLoadedSuccess) Theme = EventDataDict[GameEventTemplateKeys.EventMainThemeKey].ToString();
    }
}

public class GameEventTemplateKeys
{
    // Base Event Key
    public static readonly string EventDataKey = "event_data";
    public static readonly string EventPlayTriggerKey = "play_trigger";
    public static readonly string EventRequireTicketKey = "require_ticket";
    public static readonly string EventDefaultPlaytimeKey = "default_playtime";
    public static readonly string EventDefaultCooldownKey = "default_cooldown";
    public static readonly string EventDailyRepeatableKey = "daily_repeatable";
    public static readonly string EventMainThemeKey = "event_theme";

    // Event Theme Info
    public static readonly string EventReleaseDateKey = "release_date";
    public static readonly string EventEndDateKey = "end_date";
    public static readonly string EventCurrencyKey = "event_currency";

    // Player Event Key
    public static readonly string PlayerDataKey = "player_data";
    public static readonly string PlayerDailyTimestampKey = "daily_timestamp";
    public static readonly string PlayerActionTimestampKey = "action_timestamp";
    public static readonly string PlayerRemainPlaytimeKey = "remain_playtime";
    public static readonly string PlayerValidDateKey = "valid_date";
    public static readonly string PlayerReceivedItemKey = "received_item_data";

    // Static Image Key
    public static readonly string StaticNativeImagePrefixKey = "IMG_";
    public static readonly string StaticNonNativeImagePrefixKey = "N_IMG_";

    // Ticket Data Key
    public static readonly string TicketIDPrefixKey = "EVENT_TICKET_";
    public static readonly string TicketIconImagePrefixKey = "EVENT_TICKET_";
    public static readonly string TicketBgImagePrefixKey = "TICKET_BG_";
    public static readonly string TicketCooldownImagePrefixKey = "TICKET_COOLDOWN_ICON_";
    public static readonly string TicketCooldownBgImagePrefixKey = "TICKET_COOLDOWN_BG_";

    // Item Data Key
    public static readonly string ItemIDKey = "itemID";
    public static readonly string ItemAmountKey = "amount";

    // Reward Data Key
    public static readonly string RewardListKey = "reward_list";
    public static readonly string ParticipateRewardListKey = "participate_reward";

    // Resource Path.
    public static readonly string EventPrefabResourcePath = "Events/Prefabs/";
    public static readonly string EventActionRewardPrefabPath = "Events/Prefabs/EventActionReward";

    // Suffix Key.
    public static readonly string EventHeaderBannerSuffix = "_BANNER";
    public static readonly string EventMainBackgroundSuffix = "_INGAME";
    public static readonly string EventThemeHeaderImgSuffix = "_HEADER";
    public static readonly string EventThemeBgImgSuffix = "_BACKGROUND";

    #region Localize Key
    public static readonly string EventHeaderKey = "EVENT_HEADER_";
    public static readonly string EventObjectiveKey = "EVENT_OBJECTIVE_";
    public static readonly string EventDurationKey = "EVENT_DURATION_";
    public static readonly string EventDescriptionKey = "EVENT_DESC_";
    public static readonly string EventTimeKey = "EVENT_TIME_";
    public static readonly string EventDetailKey = "EVENT_DETAIL_";
    public static readonly string EventDetailHeaderKey = "EVENT_DETAIL_HEADER";

    public static readonly string NotiRewardTitleKey = "EVENT_CONGRATS";
    public static readonly string NotiRewardMessageKey = "EVENT_YOUVEGOT";
    public static readonly string NotiSmallRewardTitleKey = "EVENT_SMALL_REWARD_TITLE";
    public static readonly string NotiSmallRewardMessageKey = "EVENT_SMALL_REWARD_MESSAGE";
    public static readonly string NoRewardTitleKey = "EVENT_NO_REWARD_TITLE";
    public static readonly string NoRewardMessageKey = "EVENT_NO_REWARD_MESSAGE";
    public static readonly string NoRemainTryTitleKey = "EVENT_NO_REMAIN_TRY_TITLE";
    public static readonly string NoRemainTryMessageKey = "EVENT_NO_REMAIN_TRY_MESSAGE";
    public static readonly string NoTicketTitleKey = "EVENT_NO_TICKET_TITLE";
    public static readonly string NoTicketMessageKey = "EVENT_NO_TICKET_MESSAGE";
    public static readonly string DailyAlreadyDoneTitleKey = "EVENT_DAILY_ALREADY_DONE_TITLE";
    public static readonly string DailyAlreadyDoneMessageKey = "EVENT_DAILY_ALREADY_DONE_MESSAGE";

    public static readonly string CooldownTitleKey = "EVENT_COOLDOWN";
    public static readonly string CooldownMessageKey = "EVENT_ONCOOLDOWN";

    public static readonly string EventActionErrorTitleKey = "EVENT_ACTION_ERROR_TITLE";
    public static readonly string EventActionErrorMessageKey = "EVENT_ACTION_ERROR_MESSAGE";

    #endregion

}

public enum GameEventStartType
{
    None = 0
    , Cooldown              // Using Cooldown to be a rule for enable playing.
    , Ticket                // Using Ticket to be a rule for enable playing.
    , CooldownTicket        // Using Ticket which is generated after the cooldown time is meet the requirement.
    , Ticket_Cooldown       // Using both of Ticket and Cooldown time to be a rule for enable playing.
    , Daily                 // Using Daily to be a rule for enable playing. (If the result is draw, player can keep playing until get either "win" or "lose" result)
}

public enum EventCharacterAnimationScript
{
    None
    , Native
    , Spine
}

public enum EventCharacterAnimationState
{
    Idle
    , Loading
    , Success
    , Failed
}

[Serializable]
public struct TextDictionary
{
    public string Key;
    public TextMeshProUGUI Text;
}