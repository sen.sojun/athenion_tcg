﻿using Coffee.UIExtensions;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class GameEventThresholdReward : MonoBehaviour
{
    [Header("Image From Asset Bundle")]
    public Image RewardBGImage;
    public Image HighlightRewardImage;
    public Image PresentReceiveTextBG;

    [SerializeField] private Image RewardImage;
    [SerializeField] private Image ClaimedRewardImage;
    [SerializeField] private TMP_Text RewardAmountText;
    [SerializeField] private TMP_Text RequiredThresholdText;
    [SerializeField] private UIShiny[] ShinyBackgrounds;
    [SerializeField] private Button ClaimRewardButton;
    [SerializeField] GameObject RewardHilight;

    public void Init(ItemData itemData, int requiredThreshold, bool isRewardClaimed, bool isCanClaimReward, UnityAction onClickEvent)
    {
        RewardImage.sprite = SpriteResourceHelper.LoadSprite(itemData.ItemID, SpriteResourceHelper.SpriteType.ICON_Item);
        RewardAmountText.text = "x " + itemData.Amount.ToString();
        RequiredThresholdText.text = requiredThreshold.ToString();
        ClaimRewardButton.onClick.AddListener(onClickEvent);
        UpdateRewardState(isRewardClaimed, isCanClaimReward);
    }

    public void UpdateRewardState(bool isRewardClaimed, bool isCanClaimReward)
    {
        ClaimedRewardImage.gameObject.SetActive(isRewardClaimed);
        RewardHilight.SetActive(isCanClaimReward & !isRewardClaimed);
        foreach (UIShiny shiny in ShinyBackgrounds)
        {
            shiny.enabled = isCanClaimReward & !isRewardClaimed;
        }
    }
}
