﻿using UnityEngine;
using UnityEngine.Events;

public interface IPageable
{
    GameObject gameObject { get; }
    void Show(UnityAction onComplete);
    void Hide(UnityAction onComplete);
}
