﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MiniGameAnimationCoroutine : MonoBehaviour
{
    public virtual IEnumerator AnimationWinEventCoroutine(UnityAction onComplete)
    {
        yield return null;
        onComplete?.Invoke();
    }

    public virtual IEnumerator AnimationLoseEventCoroutine(UnityAction onComplete)
    {
        yield return null;
        onComplete?.Invoke();
    }
}
