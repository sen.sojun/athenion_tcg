﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace GameEvent
{
    public enum EventPageType { Shop, GameEvent, EventQuest, MiniGameEvent }
    [Serializable]
    public class EventPage
    {
        public EventPageType PageType;
        public GameObject GameObject;

        public IPageable Page
        {
            get
            {
                if (_page == null)
                    _page = GameObject.GetComponent<IPageable>();
                return _page;
            }
        }
        private IPageable _page;

    }
    public class EventManager : MonoBehaviour
    {
        #region Events

        public static event Action OnInit;
        public static event Action OnQuit;
        public static event Action<string> OnOpenPage;
        public static event Action<EventPageType> OnOpenTab;

        #endregion

        public List<EventPage> EventPage = new List<EventPage>();
        public EventUIManager EventUIManager;

        public EventPage CurrentPage => _currentPage;
        private EventPage _currentPage;

        private EventPageType _currentPageType;
        private bool _isInit;


        public static EventManager Instance
        {
            get
            {
                if (_instance == null)
                    _instance = GameObject.FindObjectOfType<EventManager>();
                return _instance;
            }
        }
        private static EventManager _instance;

        private void Awake()
        {
            _instance = this;
        }

        public void Initialize()
        {
            EventUIManager.Initialize();
            UnityAction onClickShop = delegate ()
            {
                ShowPage(EventPageType.Shop);
            };
            UnityAction onClickGameEvent = delegate ()
            {
                ShowPage(EventPageType.GameEvent);
            };
            UnityAction onClickEventQuest = delegate ()
            {
                ShowPage(EventPageType.EventQuest);
            };

            EventUIManager.SetEvent(onClickShop, onClickGameEvent, onClickEventQuest);
            SetCloseBthEvent();

            // Set Seen 
            PlayerSeenListManager.Instance.SetSeenListData(SeenItemTypes.Event, true, DataManager.Instance.GetGameEventDB().GetValidGameEventInfoKeyList());

            OnInit?.Invoke();
            OnOpenPage?.Invoke("Event");
            SoundManager.PlayEventBGM(3.0f);

        }

        public void Close()
        {
            OnQuit?.Invoke();
            SoundManager.PlayBGM(SoundManager.BGM.Menu, 1.5f);
            GameHelper.UITransition_FadeOut(this.gameObject, delegate { Destroy(this.gameObject); });
        }

        public void ShowPage(EventPageType pageType, UnityAction onComplete = null)
        {
            if (_currentPage != null && _currentPage.PageType == pageType)
            {
                onComplete?.Invoke();
                return;
            }

            EventUIManager.ChangeTab(pageType);
            PopupUIManager.Instance.Show_MidSoftLoad(true);
            StartCoroutine(IEShowPage(pageType, () =>
            {
                PopupUIManager.Instance.Show_MidSoftLoad(false);
                OnOpenTab?.Invoke(pageType);
                onComplete?.Invoke();
            }));
        }

        public void CloseCurrentPage(UnityAction onComplete = null)
        {
            if (_currentPage != null)
            {
                PopupUIManager.Instance.Show_MidSoftLoad(true);
                StartCoroutine(IECloseCurrentPage(() =>
                {
                    PopupUIManager.Instance.Show_MidSoftLoad(false);
                    _currentPage = null;
                    onComplete?.Invoke();
                }));
            }
        }

        private IEnumerator IEShowPage(EventPageType pageType, UnityAction onComplete = null)
        {
            EventUIManager.GetMainRegion().gameObject.SetActive(true);
            if (_currentPage != null)
            {
                bool isFinish = false;
                _currentPage.Page.Hide(() => isFinish = true);
                yield return new WaitUntil(() => isFinish == true);
            }
            EventPage eventPage = EventPage.Find((page) => page.PageType == pageType);
            if (eventPage == null)
            {
                EventUIManager.GetMainRegion().gameObject.SetActive(false);
                _currentPage = null;
            }
            else
            {
                _currentPage = eventPage;

                bool isFinish = false;
                _currentPage.Page.Show(() => isFinish = true);
                yield return new WaitUntil(() => isFinish == true);
            }
            onComplete?.Invoke();
        }

        private IEnumerator IECloseCurrentPage(UnityAction onComplete = null)
        {
            EventUIManager.GetMainRegion().gameObject.SetActive(true);
            if (_currentPage != null)
            {
                bool isFinish = false;
                _currentPage.Page.Hide(() => isFinish = true);
                yield return new WaitUntil(() => isFinish == true);
            }
            onComplete?.Invoke();
        }

        public void SetCloseBthEvent(UnityAction closeAction = null)
        {
            EventUIManager.SetCloseBtnEvent(closeAction ?? Close);
        }
    }
}
