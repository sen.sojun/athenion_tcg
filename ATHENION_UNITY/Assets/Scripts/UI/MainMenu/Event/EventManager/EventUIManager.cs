﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace GameEvent
{
    public class EventUIManager : MonoBehaviour
    {
        [Serializable]
        public class EventTitleButton
        {
            public Button Button;
            public TMPro.TextMeshProUGUI Text;

            public void ClearTextToDefault()
            {
                Text.color = Color.white;
            }
        }

        [SerializeField] private EventTitleButton _ShopButton;
        [SerializeField] private EventTitleButton _GameEventButton;
        [SerializeField] private EventTitleButton _EventQusetButton;
        [SerializeField] private Button _CloseButton;

        [SerializeField] private GameEventThemeUI _GameEventThemeUI;
        [SerializeField] private EventManager _EventManager;

        [SerializeField] private RectTransform _MainRegion;

        [Header("TitleBar")]
        [SerializeField] private Sprite _UnSelectSprite;
        [SerializeField] private Sprite _SelectedSprite;

        public void Initialize()
        {
            _GameEventThemeUI.Initialize();
        }

        public void SetEvent(UnityAction onClickShop, UnityAction onClickGameEvent, UnityAction onClickEventQuest)
        {
            _ShopButton.Button.onClick.RemoveAllListeners();
            _GameEventButton.Button.onClick.RemoveAllListeners();
            _EventQusetButton.Button.onClick.RemoveAllListeners();

            _ShopButton.Button.onClick.AddListener(onClickShop);
            _GameEventButton.Button.onClick.AddListener(onClickGameEvent);
            _EventQusetButton.Button.onClick.AddListener(onClickEventQuest);
        }

        public void SetCloseBtnEvent(UnityAction onClick)
        {
            _CloseButton.onClick.RemoveAllListeners();
            _CloseButton.onClick.AddListener(onClick);
        }

        public RectTransform GetMainRegion()
        {
            return _MainRegion;
        }

        public void ChangeTab(EventPageType pageType)
        {
            if (pageType == EventPageType.MiniGameEvent)
                return;
            SetTitleButtonSpriteToDefault();

            EventTitleButton btn = null;
            switch (pageType)
            {
                case EventPageType.Shop:
                    btn = _ShopButton;
                    break;
                case EventPageType.GameEvent:
                    btn = _GameEventButton;
                    break;
                case EventPageType.EventQuest:
                    btn = _EventQusetButton;
                    break;
                default:
                    break;
            }
            if (btn != null)
            {
                btn.Button.image.sprite = _SelectedSprite;
                btn.Text.color = Color.black;
            }
        }

        private void SetTitleButtonSpriteToDefault()
        {
            _ShopButton.Button.image.sprite = _UnSelectSprite;
            _ShopButton.ClearTextToDefault();

            _GameEventButton.Button.image.sprite = _UnSelectSprite;
            _GameEventButton.ClearTextToDefault();

            _EventQusetButton.Button.image.sprite = _UnSelectSprite;
            _EventQusetButton.ClearTextToDefault();
        }
    }
}