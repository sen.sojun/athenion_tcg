﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace GameEvent
{
    public class GameEventUI : MonoBehaviour
    {
        #region Private Properties
        private List<GameEventUICell> _gameEventUIList = new List<GameEventUICell>();
        #endregion
        
        [SerializeField]
        private Transform _SpawnPoint;
        [SerializeField]
        private GameEventUICell _EventUIPrefab;

        public void SetData(Dictionary<string, GameEventInfo> eventData, UnityAction<string> onSelectEvent)
        {
            ClearUIList();
            GenerateUIElement(eventData, onSelectEvent);
        }

        private void GenerateUIElement(Dictionary<string, GameEventInfo> eventData, UnityAction<string> onSelectEvent)
        {
            foreach (KeyValuePair<string, GameEventInfo> data in eventData)
            {
                string eventKey = data.Key;
                GameEventInfo eventInfo = data.Value;

                if (GameEventManager.Instance.IsTimeValid(eventInfo))
                {
                    GameEventUICell element = Instantiate(_EventUIPrefab.gameObject, _SpawnPoint).GetComponent<GameEventUICell>();
                    element.SetData(eventInfo, onSelectEvent);
                    _gameEventUIList.Add(element);
                }
            }
        }

        private void ClearUIList()
        {
            foreach (Transform child in _SpawnPoint)
            {
                GameObject.Destroy(child.gameObject);
            }
            _gameEventUIList.Clear();
        }

        public void ShowUI()
        {
            GameHelper.UITransition_FadeIn(gameObject);
            //gameObject.SetActive(true);
        }

        public void HideUI(UnityAction onComplete = null)
        {
            GameHelper.UITransition_FadeOut(gameObject);
            //gameObject.SetActive(false);
            onComplete?.Invoke();
        }
    }
}