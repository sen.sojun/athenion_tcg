﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using Karamucho;
using Karamucho.GameEvent;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Events;

namespace GameEvent
{
    public class GameEventManager : MonoBehaviour, IPageable
    {
        public static readonly string EventTemplatePath = "Events/Templates/";

        public static event Action<string> OnSelectEvent;
        public static event Action<string> OnCompleteEvent;
        public static event Action<string, List<ItemData>> OnReceiveRewards;

        public static GameEventManager Instance
        {
            get
            {
                if (_instance == null)
                    _instance = GameObject.FindObjectOfType<GameEventManager>();
                return _instance;
            }
        }

        private static GameEventManager _instance;

        [SerializeField]
        private GameEventUI _GameEventUI;

        [SerializeField] private RectTransform _EventParent;

        #region Public Properties

        #endregion

        #region Private Properties 
        #endregion

        void IPageable.Show(UnityAction onComplete)
        {
            ShowUI();
            onComplete?.Invoke();
        }

        void IPageable.Hide(UnityAction onComplete)
        {
            HideUI();
            onComplete?.Invoke();
        }

        public void ShowUI()
        {
            RefrestData();
            _GameEventUI.ShowUI();
        }

        public void HideUI()
        {
            _GameEventUI.HideUI();
        }

        #region Game Event Methods

        private void RefrestData()
        {
            Dictionary<string, GameEventInfo> data = DataManager.Instance.GetGameEventDB().GetGameEventInfo();
            _GameEventUI.SetData(data, OnClickEvent);
        }

        private void OnClickEvent(string eventKey)
        {
            //EventManager.Instance.CloseCurrentPage(() =>
            //{
            //    Dictionary<string, GameEventInfo> data = DataManager.Instance.GetGameEventDB().GetGameEventInfo();
            //    if (IsTimeValid(data[eventKey]))
            //    {
            //        //string json = JsonConvert.SerializeObject(data[eventKey]);
            //        //string title = LocalizationManager.Instance.GetText("EVENT_TITLE") + " " + eventKey;

            //        string template = data[eventKey].Template;
            //        PopupUIManager.Instance.Show_MidSoftLoad(true);

            //        GameObject eventPanel = ResourceManager.Load(EventTemplatePath + template) as GameObject;

            //        if (eventPanel != null)
            //        {
            //            var controller = Instantiate(eventPanel, _EventParent).GetComponent<BaseGameEventTemplate>();
            //            if (controller != null)
            //            {
            //                controller.Init(eventKey, string.Empty, () => GameHelper.UITransition_FadeIn(controller.gameObject));
            //                OnSelectEvent?.Invoke(eventKey);
            //            }
            //        }
            //        else
            //        {
            //            string errTitle = LocalizationManager.Instance.GetText("EVENT_TITLE") + " " + eventKey;
            //            string errDetail = LocalizationManager.Instance.GetText("ERROR_GAME_EVENT_NOT_AVAILABLE");

            //            PopupUIManager.Instance.ShowPopup_Error(errTitle, errDetail, () => EventManager.Instance.ShowPage(EventPageType.GameEvent));
            //        }
            //    }
            //    else
            //    {
            //        string title = LocalizationManager.Instance.GetText("EVENT_TITLE") + " " + eventKey;
            //        string detail = LocalizationManager.Instance.GetText("ERROR_GAME_EVENT_INVALID_TIME");

            //        PopupUIManager.Instance.ShowPopup_Error(title, detail, () => EventManager.Instance.ShowPage(EventPageType.GameEvent));
            //    }
            //});
            MiniGameEventManager.Instance.CreateMiniGamePanel(
                eventKey
                , () =>
                {
                    EventManager.Instance.ShowPage(EventPageType.MiniGameEvent, () =>
                    {
                        PopupUIManager.Instance.Show_MidSoftLoad(false);
                        OnSelectEvent?.Invoke(eventKey);
                    });
                }
                , () =>
                {
                    PopupUIManager.Instance.Show_MidSoftLoad(false);

                    string errTitle = LocalizationManager.Instance.GetText("EVENT_TITLE") + " " + eventKey;
                    string errDetail = LocalizationManager.Instance.GetText("ERROR_GAME_EVENT_NOT_AVAILABLE");

                    PopupUIManager.Instance.ShowPopup_Error(errTitle, errDetail);
                }
            );
        }

        public bool IsTimeValid(GameEventInfo gameEventInfo)
        {
            return DateTimeData.GetDateTimeUTC() > gameEventInfo.ReleaseDateTimeUTC && DateTimeData.GetDateTimeUTC() < gameEventInfo.EndDateTimeUTC;
        }

        /// <summary>
        /// Call when complete event.
        /// </summary>
        /// <param name="eventKey"></param>
        public void OnEventComplete(string eventKey)
        {
            OnCompleteEvent?.Invoke(eventKey);
        }

        /// <summary>
        /// Call when receive event rewards
        /// </summary>
        /// <param name="items"></param>
        public void OnReceiveEventReward(string eventKey, List<ItemData> items)
        {
            OnReceiveRewards?.Invoke(eventKey, items);
        }

        public Coroutine RunRoutineTask(IEnumerator action)
        {
            return StartCoroutine(action);
        }

        /// <summary>
        /// Call this method to delay action time by specific time.
        /// </summary>
        /// <param name="second"></param>
        /// <param name="onComplete"></param>
        /// <returns></returns>
        public IEnumerator DelayActionTime(float second, UnityAction onComplete)
        {
            yield return new WaitForSeconds(second);
            onComplete?.Invoke();
        }
        #endregion

    }

}