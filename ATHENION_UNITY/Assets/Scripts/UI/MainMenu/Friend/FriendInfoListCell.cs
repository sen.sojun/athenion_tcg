﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Events;

public class FriendInfoListCell : MonoBehaviour
{
    #region Private Properties
    [Header("BTN")]
    [SerializeField] private Button BTN_Info;
    [SerializeField] private Button BTN_Chat;
    [SerializeField] private Button BTN_Match;
    [SerializeField] private Button BTN_Remove;

    [Header("Group")]
    [SerializeField] private GameObject Group_Info;
    [SerializeField] private GameObject Group_Button;

    private PlayerInfoUIData _data;
    private bool _isShowInfo;

    private UnityAction<PlayerInfoUIData> _onClickChat;
    private UnityAction<PlayerInfoUIData> _onClickMatch;
    private UnityAction<PlayerInfoUIData> _onClickRemove;
    #endregion

    #region Method
    public void SetEvent(
          PlayerInfoUIData data
        , UnityAction<PlayerInfoUIData> onChat
        , UnityAction<PlayerInfoUIData> onMatch
        , UnityAction<PlayerInfoUIData> onRemove
    )
    {
        _data = data;
        _onClickChat = onChat;
        _onClickMatch = onMatch;
        _onClickRemove = onRemove;

        bool isOnline = _data.ActiveStatus == PlayerActiveStatus.Online || _data.ActiveStatus == PlayerActiveStatus.Busy;
        BTN_Match?.gameObject.SetActive(isOnline);
        BTN_Chat?.gameObject.SetActive(true);
        BTN_Remove?.gameObject.SetActive(true);

        if (BTN_Info != null)
        {
            BTN_Info.onClick.RemoveAllListeners();
            BTN_Info.onClick.AddListener(OnClickInfo);
        }

        if (BTN_Chat != null)
        {
            BTN_Chat.onClick.RemoveAllListeners();
            BTN_Chat.onClick.AddListener(OnClickChat);
        }

        if (BTN_Match != null)
        {
            BTN_Match.onClick.RemoveAllListeners();
            BTN_Match.onClick.AddListener(OnClickMatch);
        }

        if (BTN_Remove != null)
        {
            BTN_Remove.onClick.RemoveAllListeners();
            BTN_Remove.onClick.AddListener(OnClickRemove);
        }
    }

    public void ToggleDetail()
    {
        if (_isShowInfo)
        {
            ShowGroupButton();
        }
        else
        {
            ShowGroupInfo();
        }
    }

    public void ShowGroupInfo()
    {
        if (Group_Info != null)
        {
            Group_Info.SetActive(true);
        }

        if (Group_Button != null)
        {
            Group_Button.SetActive(false);
        }

        _isShowInfo = true;
    }

    public void ShowGroupButton()
    {
        if (Group_Info != null)
        {
            Group_Info.SetActive(false);
        }

        if (Group_Button != null)
        {
            Group_Button.SetActive(true);
        }

        _isShowInfo = false;
    }

    private void OnClickInfo()
    {
        ToggleDetail();
    }

    private void OnClickChat()
    {
        _onClickChat?.Invoke(_data);
    }

    private void OnClickMatch()
    {
        _onClickMatch?.Invoke(_data);
    }

    private void OnClickRemove()
    {
        _onClickRemove?.Invoke(_data);
    }
    #endregion
}
