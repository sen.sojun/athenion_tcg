﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Events;

public class FriendLastPlayedCell : MonoBehaviour
{
    #region Public Properties
    #endregion

    #region Private Properties
    [Header("Text")]
    [SerializeField] private TextMeshProUGUI TEXT_Sent;

    [Header("BTN")]
    [SerializeField] private Button BTN_Request;

    private UnityAction _onClickRequest;
    #endregion

    #region Method
    public void SetEvent(UnityAction onRequest)
    {
        _onClickRequest = onRequest;

        if (BTN_Request != null)
        {
            BTN_Request.onClick.RemoveAllListeners();
            BTN_Request.onClick.AddListener(delegate
            {
                if (_onClickRequest != null)
                {
                    _onClickRequest.Invoke();
                }

            });
        }
    }

    public void ShowSent(bool isShow)
    {
        if (TEXT_Sent != null)      TEXT_Sent.gameObject.SetActive(isShow);
        if (BTN_Request != null)    BTN_Request.gameObject.SetActive(!isShow);
    }
    #endregion
}
