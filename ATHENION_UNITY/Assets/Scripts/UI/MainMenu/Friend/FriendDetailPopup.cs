﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Events;

public class FriendDetailPopup : MonoBehaviour
{
    #region Serialize
    [Header("BTN")]
    [SerializeField] private Button _BTN_Close;
    [SerializeField] private Button _BTN_FriendlyMatch;
    [SerializeField] private Button _BTN_Delete;
    [SerializeField] private Button _BTN_Chat;
    #endregion

    #region Private Properties
    private PlayerInfoUIData _data;
    private UnityAction<PlayerInfoUIData> _onDelete;
    private UnityAction<PlayerInfoUIData> _onFriendlyMatch;
    #endregion

    #region Method
    private void Init()
    {
        SetPlayerData(_data);
        InitBTN();
    }

    private void InitBTN()
    {

        _BTN_Close.onClick.RemoveAllListeners();
        _BTN_Close.onClick.AddListener(HideUI);

        _BTN_FriendlyMatch.interactable = _data.ActiveStatus == PlayerActiveStatus.Online ||
                                          _data.ActiveStatus == PlayerActiveStatus.Busy;

        _BTN_FriendlyMatch.onClick.RemoveAllListeners();
        _BTN_FriendlyMatch.onClick.AddListener(SendFriendlyMatch);

        _BTN_Delete.onClick.RemoveAllListeners();
        _BTN_Delete.onClick.AddListener(DeleteFriend);

        _BTN_Chat.onClick.RemoveAllListeners();
        _BTN_Chat.onClick.AddListener(SendChat);
    }

    private void SetPlayerData(PlayerInfoUIData data)
    {
        GetComponent<FriendInfoUI>().SetData(data);
    }
    #endregion

    #region Event
    private void DeleteFriend()
    {
        _onDelete?.Invoke(_data);
        HideUI();
    }

    private void SendChat()
    {
        KTPlayManager.Instance.ShowChatWithPlayerID(_data.PlayerID);
    }

    private void SendFriendlyMatch()
    {
        if (DataManager.Instance.IsHasDeckAvailable())
        {
            _onFriendlyMatch?.Invoke(_data);
        }
        else
        {
            string text = LocalizationManager.Instance.GetText("ERROR_FIND_DECK_NOT_FOUND");
            PopupUIManager.Instance.ShowPopup_Error("", text);
        }


        HideUI();
    }
    #endregion

    #region Show/Hide
    public void ShowUI(PlayerInfoUIData data, UnityAction<PlayerInfoUIData> onDelete, UnityAction<PlayerInfoUIData> onFriendlyMatch)
    {
        _data = data;
        _onDelete = onDelete;
        _onFriendlyMatch = onFriendlyMatch;

        Init();

        GameHelper.UITransition_FadeIn(this.gameObject);

    }

    public void HideUI()
    {
        GameHelper.UITransition_FadeOut(this.gameObject);
    }
    #endregion
}
