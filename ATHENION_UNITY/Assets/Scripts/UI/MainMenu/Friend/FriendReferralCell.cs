﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class FriendReferralCell : MonoBehaviour
{
    #region Public Properties
    #endregion

    #region Private Properties
    [Header("DECO Level")]
    [SerializeField] private GameObject DECO_Level;

    [Header("Level")]
    [SerializeField] private TextMeshProUGUI TEXT_Level;

    [Header("Text")]
    [SerializeField] private TextMeshProUGUI TEXT_Claimed;

    [Header("BTN")]
    [SerializeField] private Button BTN_Claim;

    private UnityAction _onClickClaim;
    #endregion

    #region Method
    public void SetEvent(UnityAction onClick)
    {
        _onClickClaim = onClick;

        if (BTN_Claim != null)
        {
            BTN_Claim.onClick.RemoveAllListeners();
            BTN_Claim.onClick.AddListener(delegate
            {
                if (_onClickClaim != null)
                {
                    _onClickClaim.Invoke();
                }

            });
        }
    }

    public void ShowLevel(int level)
    {
        if (DECO_Level != null) DECO_Level.gameObject.SetActive(true);
        if (TEXT_Level != null) TEXT_Level.text = level.ToString();
        if (BTN_Claim != null) BTN_Claim.gameObject.SetActive(false);
        if (TEXT_Claimed != null) TEXT_Claimed.gameObject.SetActive(false);
    }

    public void ShowClaim()
    {
        if (DECO_Level != null) DECO_Level.gameObject.SetActive(false);
        if (BTN_Claim != null) BTN_Claim.gameObject.SetActive(true);
        if (TEXT_Claimed != null) TEXT_Claimed.gameObject.SetActive(false);
    }

    public void ShowClaimed()
    {
        if (DECO_Level != null) DECO_Level.gameObject.SetActive(false);
        if (BTN_Claim != null) BTN_Claim.gameObject.SetActive(false);
        if (TEXT_Claimed != null) TEXT_Claimed.gameObject.SetActive(true);
    }
    #endregion
}
