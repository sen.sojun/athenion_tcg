﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInfoUIData
{
    #region Public Properties
    public string PlayerID { get; private set; }
    public string DisplayName { get; private set; }
    public string AvatarID { get; private set; }
    public string TitleID { get; private set; }
    public PlayerActiveStatus ActiveStatus { get; private set; }

    public PlayerLevelData LevelData { get; private set; }
    public int CurrentLevel { get; private set; }

    public int PlayerScore { get; private set; }

    public int RankIndex { get; private set; }
    public string RankID { get; private set; }
    public string RankName { get; private set; }
    public DateTime LastLogin { get; private set; }
    public PlayerInfoData PlayerData => _playerData;
    #endregion

    #region Private Properties

    private PlayerInfoData _playerData;

    #endregion

    public PlayerInfoUIData(PlayerInfoData data)
    {
        PlayerID = data.PlayerID;
        DisplayName = data.DisplayName;
        AvatarID = data.AvatarID;
        TitleID = data.TitleID;
        ActiveStatus = data.ActiveStatus;

        LevelData = data.LevelData;
        CurrentLevel = LevelData.Level;

        RankIndex = data.GetPlayerCurrentRank();
        RankID = GameHelper.GetRankID(RankIndex);
        RankName = GameHelper.GetRankName(RankIndex);

        LastLogin = data.LastLogin;

        _playerData = data;
    }
}
