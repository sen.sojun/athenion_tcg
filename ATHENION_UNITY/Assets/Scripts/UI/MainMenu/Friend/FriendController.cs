﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using TMPro;
using DG.Tweening;
using System;

public enum AddFriendType
{
    Recent
    , Name
}

public class FriendController : MonoBehaviour
{
    #region Public Properties
    [Header("Prefab")]
    public FriendInfoListCell FriendOnline_cell;
    public FriendInfoListCell FriendOffline_cell;
    public FriendRequestCell FriendRequest_cell;
    public FriendLastPlayedCell FriendLastPlayed_cell;
    public FriendReferralCell FriendReferral_cell;

    [Header("Add Friend")]
    public TMP_InputField INPUT_FriendName;
    public Button BTN_AddFriend;

    [Header("BTN")]
    public Button BTN_Close;

    [Header("Ref Group")]
    public GameObject Group_Friend;
    public GameObject Group_Request;
    public GameObject Group_Add;
    public GameObject Group_Referal;

    [Header("List Section")]
    public Transform Section_FriendList;
    public Transform Section_Online;
    public Transform Section_Offline;
    public Transform Section_Request;
    public Transform Section_LastPlayed;
    public Transform Section_Referral;

    [Header("Section Controller")]
    public FriendReferralUI ReferralUIController;

    [Header("Toggle Navigation")]
    [SerializeField] private MenuSlider MenuButtonSlider;
    [SerializeField] private Button BTN_FriendList;
    [SerializeField] private Button BTN_Request;
    [SerializeField] private Button BTN_Lastplayed;
    [SerializeField] private Button BTN_Referal;

    public bool IsFriendShow { private set; get; }
    public bool IsRequestShow { private set; get; }
    public bool IsLastPlayedShow { private set; get; }
    public bool IsReferalShow { private set; get; }

    [Header("TEXT")]
    public TextMeshProUGUI TEXT_PlayerName;
    #endregion

    #region Serialize
    [Header("Popup")]
    [SerializeField] private FriendDetailPopup Popup_FriendDetail;
    #endregion

    #region Private Properties
    // Pool
    private List<FriendInfoListCell> _playerOnlineFriendCellPool = new List<FriendInfoListCell>();
    private List<FriendInfoListCell> _playerOfflineFriendCellPool = new List<FriendInfoListCell>();
    private List<FriendRequestCell> _playerRequestCellPool = new List<FriendRequestCell>();
    private List<FriendLastPlayedCell> _playerLastPlayedCellPool = new List<FriendLastPlayedCell>();
    private List<FriendReferralCell> _playerReferralCellPool = new List<FriendReferralCell>();

    private bool _isInited = false;
    private int _maxLastMatch = 3;
    #endregion

    #region Static Event Properties
    public static event Action OnOpenFriendList;
    public static event Action OnTapFriendList;
    public static event Action OnTapFriendRequest;
    public static event Action OnTapAddFriend;
    public static event Action OnTapChat;
    public static event Action<AddFriendType, string, string> OnAddFriend;
    public static event Action<PlayerInfoData> OnAcceptFriend;
    public static event Action<PlayerInfoData> OnDeleteFriend;
    public static event Action OnRefreshFriendList;
    #endregion

    #region Method

    #region LoopIntervalChecking

    #region FriendRefresh
    public IEnumerator RefreshFriendListInterval()
    {
        bool isFinish = false;
        TaskLoader<string> taskLoader = new TaskLoader<string>();
        DataManager.Instance.RefreshAllFriendAndLastMatchPlayerDataAndReferreeList(
            delegate ()
            {
                isFinish = true;
                if (this != null)
                {
                    if (!_isInited)
                    {
                        Init();
                    }
                    else
                    {
                        RefreshData();
                    }
                }
            }
            , delegate (string error) { isFinish = true; }
        );
        yield return new WaitUntil(() => isFinish == true);
    }

    #endregion

    #region FriendMatch
    public IEnumerator RefreshFriendMatchInterval()
    {
        bool isFinish = false;
        TaskLoader<string> taskLoader = new TaskLoader<string>();
        DataManager.Instance.RefreshFriendMatchRequest(
              delegate ()
              {
                  isFinish = true;
                  if (this != null)
                  {
                      OnRefreshFriendMatchComplete();
                  }
              }
            , delegate (string error) { isFinish = true; }
        );
        yield return new WaitUntil(() => isFinish == true);
    }

    #region RefreshFriendMatchInverval Callback

    private void OnRefreshFriendMatchComplete()
    {
        if (IsCanRefreshMatchRequest() == false)
        {
            return;
        }
        List<FriendMatchData> receiverFriendMatchList = DataManager.Instance.GetFriendMatchDataList(FriendMatchData.StatusType.Receiver);
        receiverFriendMatchList.Sort((a, b) => a.TimeStampUTC.CompareTo(b.TimeStampUTC));
        if (IsReceiverNotAvailable(receiverFriendMatchList))
        {
            UnityAction action = delegate ()
            {
                PopupUIManager.Instance.ShowPopup_Error(
                      LocalizationManager.Instance.GetText("FRIENDLY_MATCH_TITLE")
                    , LocalizationManager.Instance.GetText("ERROR_FRIENDLY_MATCH_RECIEVER_IS_PLAYING")
                    , delegate ()
                    {
                        HomeManager.Instance.HideSelectDeck();
                    }
                );
            };

            DataManager.Instance.DeclineFriendMatchRequest(receiverFriendMatchList[0].PlayerID, action, (error) => { action?.Invoke(); });
            return;
        }

        if (DataManager.Instance.PlayerInfo.Activity == PlayerActivity.In_MainMenu)
        {
            List<FriendMatchData> senderFriendMatchList = DataManager.Instance.GetFriendMatchDataList(FriendMatchData.StatusType.Sender);
            senderFriendMatchList.Sort((a, b) => a.TimeStampUTC.CompareTo(b.TimeStampUTC));
            if (senderFriendMatchList != null && senderFriendMatchList.Count > 0)
            {
                FriendMatchData inviter = senderFriendMatchList[0];
                ShowFriendMatchNotification(inviter);
            }
        }
        else if (DataManager.Instance.PlayerInfo.Activity == PlayerActivity.In_Lobby_Friendly)
        {
            Karamucho.Network.MatchingManager matchingManager = FindObjectOfType<Karamucho.Network.MatchingManager>();
            if (!IsMatchValid(matchingManager, receiverFriendMatchList))
            {
                string title = "";
                string detail = "";
                if (string.IsNullOrEmpty(matchingManager.Setting.FriendPlayfabID))
                {
                    // receiver
                    title = LocalizationManager.Instance.GetText("FRIENDLY_MATCH_TITLE");
                    detail = LocalizationManager.Instance.GetText("ERROR_FRIENDLY_MATCH_SENDER_IS_DECLINE");
                }
                else
                {
                    // sender
                    title = LocalizationManager.Instance.GetText("FRIENDLY_MATCH_TITLE");
                    detail = LocalizationManager.Instance.GetText("ERROR_FRIENDLY_MATCH_RECEIVER_IS_DECLINE");
                }

                PopupUIManager.Instance.ShowPopup_Error(
                      title
                    , detail
                    , delegate ()
                    {
                        HomeManager.Instance.HideSelectDeck();
                    }
                );

                matchingManager.ExitFriendMatching();
            }
        }
    }

    private bool IsReceiverNotAvailable(List<FriendMatchData> receiverFriendMatchList)
    {
        if (receiverFriendMatchList != null && receiverFriendMatchList.Count > 0)
        {
            List<PlayerInfoData> friend = DataManager.Instance.GetFriendList(FriendStatus.Friend);
            PlayerInfoData receiverData = friend.Find(player => player.PlayerID == receiverFriendMatchList[0].PlayerID);
            if (receiverData != null && receiverData.ActiveStatus == PlayerActiveStatus.Playing)
            {
                return true;
            }
        }
        return false;
    }

    private void ShowFriendMatchNotification(FriendMatchData inviter)
    {
        List<PlayerInfoData> friendList = DataManager.Instance.GetFriendList(FriendStatus.Friend);
        PlayerInfoData inviterPlayerInfo = friendList.Find(x => x.PlayerID == inviter.PlayerID);
        if (inviterPlayerInfo == null)
        {
            return;
        }
        PlayerInfoUIData playerInfoUIData = new PlayerInfoUIData(inviterPlayerInfo);

        PopupUIManager.Instance.ShowInviteMatch(
              LocalizationManager.Instance.GetText("FRIENDLY_MATCH_TITLE")
            , string.Format(LocalizationManager.Instance.GetText("FRIENDLY_MATCH_RECEIVED"), playerInfoUIData.DisplayName)
            , playerInfoUIData
            , delegate ()
            {
                if (DataManager.Instance.IsHasDeckAvailable())
                {
                    if (DataManager.Instance.PlayerInfo.ActiveStatus == PlayerActiveStatus.Online)
                    {
                        HomeManager.Instance.JoinFriendlyMatch(inviter.Region, inviter.RoomName);
                    }
                    else
                    {
                        DataManager.Instance.DeclineFriendMatchRequest(
                       inviter.PlayerID
                           , delegate ()
                           {
                               // call this again. to get next request
                               OnRefreshFriendMatchComplete();
                           }
                           , delegate (string errorMsg)
                           {
                               Debug.Log(errorMsg);
                           }
                       );
                    }
                }
                else
                {

                    DataManager.Instance.DeclineFriendMatchRequest(
                        inviter.PlayerID
                        , delegate ()
                        {
                            // call this again. to get next request
                            OnRefreshFriendMatchComplete();
                            string text = LocalizationManager.Instance.GetText("ERROR_FIND_DECK_NOT_FOUND");
                            PopupUIManager.Instance.ShowPopup_Error("", text);
                        }
                        , delegate (string errorMsg)
                        {
                            Debug.Log(errorMsg);
                        }
                    );
                }

                if (DataManager.Instance.PlayerInfo.ActiveStatus == PlayerActiveStatus.Online)
                {
                    HomeManager.Instance.HideFriends();
                }
            }
            , delegate ()
            {
                DataManager.Instance.DeclineFriendMatchRequest(
                        inviter.PlayerID
                    , delegate ()
                    {
                        // call this again. to get next request
                        OnRefreshFriendMatchComplete();
                    }
                    , delegate (string errorMsg)
                    {
                        Debug.Log(errorMsg);
                    }
                );
            }
        );
    }

    private bool IsMatchValid(Karamucho.Network.MatchingManager matchingManager, List<FriendMatchData> receiverFriendMatchList)
    {
        bool isValid = false;
        if (matchingManager.IsHost())
        {
            // I am host
            if (matchingManager.IsJoiningToRoom() || matchingManager.GetRoomPlayerCount() >= 2)
            {
                // 2 players already in room.
                isValid = true;
            }
            else if (receiverFriendMatchList != null && receiverFriendMatchList.Count > 0)
            {
                FriendMatchData invitee = receiverFriendMatchList[0];
                isValid = matchingManager.CheckFriendRoomValid(invitee);
            }
        }
        else
        {
            // I am not host
            if (matchingManager.IsJoiningToRoom() || matchingManager.GetRoomPlayerCount() >= 2)
            {
                isValid = true;
            }
        }

        return isValid;
    }

    private bool IsCanRefreshMatchRequest()
    {
        bool canRefresh;
        switch (DataManager.Instance.PlayerInfo.Activity)
        {
            case PlayerActivity.In_MainMenu:
            case PlayerActivity.In_Lobby_Friendly:
                {
                    // allow to do this task.
                    canRefresh = true;
                }
                break;

            default:
                canRefresh = false;
                break;
        }

        return canRefresh;
    }
    #endregion

    #endregion

    #endregion

    private void Init()
    {
        if (!_isInited)
        {
            _isInited = true;
            InitNavigation();
            InitData();
            InitBTN();
            SetPlayerName();
        }
    }

    private void SetPlayerName()
    {
        TEXT_PlayerName.text = DataManager.Instance.PlayerInfo.DisplayName;
    }

    private void InitData()
    {
        List<PlayerInfoData> friendList = DataManager.Instance.GetFriendList(FriendStatus.Friend);
        List<PlayerInfoData> requestList = DataManager.Instance.GetFriendList(FriendStatus.Request_Sender);
        List<PlayerInfoData> sentList = DataManager.Instance.GetFriendList(FriendStatus.Request_Receiver);
        List<PlayerInfoData> lastMatchData = DataManager.Instance.GetLastMatchPlayerList();
        List<ReferreeInfoData> referreeList = DataManager.Instance.GetReferreeList();
        // Lastest player is last order in the list.
        //lastMatchData.Reverse();

        // Generate Friend
        Debug.Log(gameObject);
        Section_Online.gameObject.SetActive(false);
        Section_Offline.gameObject.SetActive(false);
        foreach (PlayerInfoData item in friendList)
        {
            PlayerInfoUIData infoData = new PlayerInfoUIData(item);
            if (IsPlayerActiveStatusOnline(infoData.ActiveStatus))
            {
                FriendInfoListCell cell = GenerateFriendCell(
                      FriendOnline_cell.gameObject
                    , infoData
                    , Section_Online
                    , SendChat
                    , SendFriendlyMatch
                    , DeleteMyFriend

                );
                _playerOnlineFriendCellPool.Add(cell);
                cell.transform.SetSiblingIndex(Section_Online.GetSiblingIndex());
                Section_Online.gameObject.SetActive(true);
            }
            else
            {
                FriendInfoListCell cell = GenerateFriendCell(
                      FriendOffline_cell.gameObject
                    , infoData
                    , Section_Offline
                    , SendChat
                    , SendFriendlyMatch
                    , DeleteMyFriend
                );
                _playerOfflineFriendCellPool.Add(cell);
                cell.transform.SetSiblingIndex(Section_Offline.GetSiblingIndex());
                Section_Offline.gameObject.SetActive(true);
            }
        }

        RebuildUILayout();

        // Generate Request
        foreach (PlayerInfoData item in requestList)
        {
            PlayerInfoUIData infoData = new PlayerInfoUIData(item);
            FriendRequestCell cell = GenerateFriendRequestCell(
                  FriendRequest_cell.gameObject
                , infoData
                , Section_Request
                , delegate ()
                {
                    AcceptFriend(infoData);
                }
                , delegate ()
                {
                    DeleteFriendRequest(infoData);
                }
            );
            _playerRequestCellPool.Add(cell);
        }

        // Generate Last Match Player
        int k = 0;
        foreach (PlayerInfoData item in lastMatchData)
        {
            if (friendList.Find(player => player.PlayerID == item.PlayerID) != null)
                continue;
            if (k >= _maxLastMatch) break;

            bool isSent = false;
            foreach (PlayerInfoData requestSentPlayer in sentList)
            {
                if (item.PlayerID == requestSentPlayer.PlayerID)
                {
                    isSent = true;
                    break;
                }
            }

            PlayerInfoUIData infoData = new PlayerInfoUIData(item);
            FriendLastPlayedCell cell = GenerateLastPlayedCell(
                  FriendLastPlayed_cell.gameObject
                , infoData
                , isSent
                , Section_LastPlayed
                , delegate
                {
                    AddFriendByID(infoData.PlayerID);
                }
            );
            _playerLastPlayedCellPool.Add(cell);
            k++;
        }

        // Generate Referral Player
        foreach (ReferreeInfoData referree in referreeList)
        {
            PlayerInfoUIData infoData = new PlayerInfoUIData(referree);
            FriendReferralCell cell = GenerateReferreeCell(
                  FriendReferral_cell.gameObject
                , infoData
                , referree
                , Section_Referral
            );
            _playerReferralCellPool.Add(cell);
        }
        ReferralUIController.SetShowNoReferreesText(_playerReferralCellPool.Count == 0);
    }

    private void RefreshData()
    {
        List<PlayerInfoData> friendList = DataManager.Instance.GetFriendList(FriendStatus.Friend);
        List<PlayerInfoData> requestList = DataManager.Instance.GetFriendList(FriendStatus.Request_Sender);
        List<PlayerInfoData> sentList = DataManager.Instance.GetFriendList(FriendStatus.Request_Receiver);
        List<PlayerInfoData> lastMatchData = DataManager.Instance.GetLastMatchPlayerList();
        List<ReferreeInfoData> referreeList = DataManager.Instance.GetReferreeList();

        // Lastest player is last order in the list.
        //lastMatchData.Reverse();

        // Reset
        Debug.Log(gameObject);
        Section_Online.gameObject.SetActive(false);
        Section_Offline.gameObject.SetActive(false);
        foreach (FriendInfoListCell item in _playerOnlineFriendCellPool)
        {
            item.gameObject.SetActive(false);
        }
        foreach (FriendInfoListCell item in _playerOfflineFriendCellPool)
        {
            item.gameObject.SetActive(false);
        }
        foreach (FriendRequestCell item in _playerRequestCellPool)
        {
            item.gameObject.SetActive(false);
        }
        foreach (FriendLastPlayedCell item in _playerLastPlayedCellPool)
        {
            item.gameObject.SetActive(false);
        }
        foreach (var item in _playerReferralCellPool)
        {
            Destroy(item.gameObject);
        }
        _playerReferralCellPool.Clear();

        // Refresh Friend
        int i = 0;
        foreach (PlayerInfoData item in friendList)
        {
            PlayerInfoUIData infoData = new PlayerInfoUIData(item);
            if (IsPlayerActiveStatusOnline(infoData.ActiveStatus))
            {
                RefreshCell_FriendList(
                    i, _playerOnlineFriendCellPool, infoData, FriendOnline_cell.gameObject, Section_Online
                    , SendChat
                    , SendFriendlyMatch
                    , DeleteMyFriend
                );
                Section_Online.gameObject.SetActive(true);
            }
            else
            {
                RefreshCell_FriendList(
                    i, _playerOfflineFriendCellPool, infoData, FriendOffline_cell.gameObject, Section_Offline
                    , SendChat
                    , SendFriendlyMatch
                    , DeleteMyFriend
                );
                Section_Offline.gameObject.SetActive(true);
            }

            // next loop
            i += 1;
        }
        RebuildUILayout();

        // Refresh Request
        int j = 0;
        foreach (PlayerInfoData item in requestList)
        {
            PlayerInfoUIData infoData = new PlayerInfoUIData(item);
            RefreshCell_Request(j, _playerRequestCellPool, infoData, FriendRequest_cell.gameObject, Section_Request
                , delegate ()
                {
                    AcceptFriend(infoData);
                }
                , delegate ()
                {
                    DeleteFriendRequest(infoData);
                }
            );
            j++;
        }

        // Refresh Last Played
        int k = 0;
        foreach (PlayerInfoData item in lastMatchData)
        {
            if (friendList.Find(player => player.PlayerID == item.PlayerID) != null)
                continue;

            if (k >= _maxLastMatch) break;

            PlayerInfoUIData infoData = new PlayerInfoUIData(item);

            bool isSent = false;
            foreach (PlayerInfoData requestSentPlayer in sentList)
            {
                if (item.PlayerID == requestSentPlayer.PlayerID)
                {
                    isSent = true;
                    break;
                }
            }

            RefreshCell_LastPlayed(k, _playerLastPlayedCellPool, infoData, isSent, FriendLastPlayed_cell.gameObject, Section_LastPlayed
                , delegate
                {
                    AddFriendByID(infoData.PlayerID);
                }
            );
            k++;
        }

        // Refresh Referral
        foreach (ReferreeInfoData referree in referreeList)
        {
            PlayerInfoUIData infoData = new PlayerInfoUIData(referree);
            FriendReferralCell cell = GenerateReferreeCell(
                  FriendReferral_cell.gameObject
                , infoData
                , referree
                , Section_Referral
            );
            _playerReferralCellPool.Add(cell);
        }
        ReferralUIController.SetShowNoReferreesText(_playerReferralCellPool.Count == 0);

        OnRefreshFriendList?.Invoke();
    }

    private void RebuildUILayout()
    {
        LayoutRebuilder.ForceRebuildLayoutImmediate(Section_Online.GetComponent<RectTransform>());
        LayoutRebuilder.ForceRebuildLayoutImmediate(Section_Offline.GetComponent<RectTransform>());
        LayoutRebuilder.ForceRebuildLayoutImmediate(Section_FriendList.GetComponent<RectTransform>());
    }

    private void RefreshCell_FriendList(
          int index, List<FriendInfoListCell> listPool, PlayerInfoUIData data, GameObject prefab, Transform parent
        , UnityAction<PlayerInfoUIData> onChat
        , UnityAction<PlayerInfoUIData> onMatch
        , UnityAction<PlayerInfoUIData> onRemove
    )
    {
        if (index <= listPool.Count - 1)
        {
            FriendInfoListCell clickCell = listPool[index];
            FriendInfoUI cell = clickCell.gameObject.GetComponent<FriendInfoUI>();

            clickCell.gameObject.SetActive(true);
            cell.SetData(data);
            clickCell.SetEvent(data, onChat, onMatch, onRemove);
        }
        else
        {
            FriendInfoListCell cell = GenerateFriendCell(prefab, data, parent, onChat, onMatch, onRemove);
            listPool.Add(cell);
            cell.transform.SetAsLastSibling();
        }
    }

    private void RefreshCell_Request(int index, List<FriendRequestCell> listPool, PlayerInfoUIData data, GameObject prefab, Transform parent, UnityAction onClickYes, UnityAction onClickNo)
    {
        if (index <= listPool.Count - 1)
        {
            listPool[index].gameObject.SetActive(true);
            listPool[index].GetComponent<FriendInfoUI>().SetData(data);
            FriendRequestCell clickCell = listPool[index].GetComponent<FriendRequestCell>();
            clickCell.SetEvent(onClickYes, onClickNo);
        }
        else
        {
            FriendRequestCell cell = GenerateFriendRequestCell(prefab, data, parent, onClickYes, onClickNo);
            listPool.Add(cell);
            cell.transform.SetAsLastSibling();
        }
    }

    private void RefreshCell_LastPlayed(int index, List<FriendLastPlayedCell> listPool, PlayerInfoUIData data, bool isSent, GameObject prefab, Transform parent, UnityAction onClickRequest)
    {
        if (index <= listPool.Count - 1)
        {
            listPool[index].gameObject.SetActive(true);
            listPool[index].GetComponent<FriendInfoUI>().SetData(data);
            FriendLastPlayedCell clickCell = listPool[index].GetComponent<FriendLastPlayedCell>();
            clickCell.ShowSent(isSent);
            clickCell.SetEvent(onClickRequest);
        }
        else
        {
            FriendLastPlayedCell cell = GenerateLastPlayedCell(prefab, data, isSent, parent, onClickRequest);
            listPool.Add(cell);
            cell.transform.SetAsLastSibling();
        }
    }

    // Recent Add Friend.
    private void AddFriendByID(string id)
    {
        PopupUIManager.Instance.Show_MidSoftLoad(true);
        DataManager.Instance.SearchAndAddFriendRequestByPlayerID(
            id
            , () => OnAddFriendSuccess(AddFriendType.Recent, id, string.Empty)
            , OnAddFriendFail
        );
        Debug.Log("Trying Add Friend by ID : " + id);
    }

    private void AddFriendByName(string name)
    {
        if (GameHelper.CheckValidNameContains(name, NameValidCase.Empty)) return;

        PopupUIManager.Instance.Show_MidSoftLoad(true);
        DataManager.Instance.SearchAndAddFriendRequestByDisplayName(
            name
            , () => OnAddFriendSuccess(AddFriendType.Name, string.Empty, name)
            , OnAddFriendFail
        );

        Debug.Log("Trying Add Friend by name : " + name);
    }

    private void OnAddFriendSuccess(AddFriendType type, string playerID, string playerName)
    {
        OnAddFriend?.Invoke(type, playerID, playerName);

        PopupUIManager.Instance.Show_MidSoftLoad(false);

        string title = LocalizationManager.Instance.GetText("FRIEND_REQUEST_TITLE");
        string message = LocalizationManager.Instance.GetText("FRIEND_REQUEST_SENT");
        PopupUIManager.Instance.ShowPopup_ConfirmOnlyWithCorrect(title, message);
        RefreshData();
    }

    private void OnAddFriendFail(string error)
    {
        PopupUIManager.Instance.Show_MidSoftLoad(false);
        string title = LocalizationManager.Instance.GetText("FRIEND_REQUEST_TITLE");
        string errorText = LocalizationManager.Instance.GetText(error);
        PopupUIManager.Instance.ShowPopup_Error(title, errorText);
    }

    private void OnClickButton(int index)
    {
        switch (index)
        {
            case 0:
                {
                    ShowFriendList(true);
                }
                break;

            case 1:
                {
                    ShowRequestList(true);
                }
                break;

            case 2:
                {
                    ShowLastPlayedList(true);
                }
                break;

            case 3:
                {
                    ShowReferalList(true);
                }
                break;
        }

        MenuButtonSlider.MoveHighlightIndex(index);
    }

    private void AcceptFriend(PlayerInfoUIData data)
    {
        PopupUIManager.Instance.Show_MidSoftLoad(true);
        DataManager.Instance.AcceptFriendRequest(
            data.PlayerID
            , delegate
            {
                OnAcceptFriend?.Invoke(data.PlayerData);
                PopupUIManager.Instance.Show_MidSoftLoad(false);
                RefreshData();
            }
            , delegate (string onFail)
            {
                PopupUIManager.Instance.Show_MidSoftLoad(false);
                string title = LocalizationManager.Instance.GetText("FRIEND_REQUEST_TITLE");
                string message = LocalizationManager.Instance.GetText(onFail);
                PopupUIManager.Instance.ShowPopup_ConfirmOnlyWithCorrect(title, message);
            }
        );
    }

    private void DeleteMyFriend(PlayerInfoUIData data)
    {
        DeleteFriend(data, true);
    }

    private void DeleteFriendRequest(PlayerInfoUIData data)
    {
        DeleteFriend(data, false);
    }

    private void DeleteFriend(PlayerInfoUIData data, bool isBeFriend)
    {
        string title = "";
        string detail = "";

        if (isBeFriend)
        {
            title = LocalizationManager.Instance.GetText("TEXT_DELETE_FRIEND");
            detail = string.Format(LocalizationManager.Instance.GetText("FRIEND_DELETE"), data.DisplayName);
        }
        else
        {
            title = LocalizationManager.Instance.GetText("TEXT_DELETE_FRIEND_REQUEST");
            detail = string.Format(LocalizationManager.Instance.GetText("FRIEND_DELETE_REQUEST"), data.DisplayName);
        }

        PopupUIManager.Instance.ShowPopup_SureCheck(
              title
            , detail
            , delegate ()
            {
                PopupUIManager.Instance.Show_MidSoftLoad(true);

                DataManager.Instance.RemoveFriend(
                      data.PlayerID
                    , delegate ()
                    {
                        OnDeleteFriend?.Invoke(data.PlayerData);
                        PopupUIManager.Instance.Show_MidSoftLoad(false);
                        RefreshData();
                    }
                    , delegate (string err)
                    {
                        Debug.Log(err);
                        PopupUIManager.Instance.Show_MidSoftLoad(false);
                    }
                );
            }
            , null
            );
    }

    private void SendChat(PlayerInfoUIData data)
    {
        KTPlayManager.Instance.ShowChatWithPlayerID(data.PlayerID);
        OnTapChat?.Invoke();
    }

    private void SendFriendlyMatch(PlayerInfoUIData data)
    {
        if (DataManager.Instance.IsHasDeckAvailable())
        {
            HomeManager.Instance.StartFriendlyMatch(data.PlayerID);
        }
        else
        {
            string text = LocalizationManager.Instance.GetText("ERROR_FIND_DECK_NOT_FOUND");
            PopupUIManager.Instance.ShowPopup_Error("", text);
        }
    }

    private bool IsPlayerActiveStatusOnline(PlayerActiveStatus activeStatus)
    {
        return (activeStatus == PlayerActiveStatus.Online
               || activeStatus == PlayerActiveStatus.Playing
               || activeStatus == PlayerActiveStatus.Busy);
    }
    #endregion

    #region BTN
    private void InitBTN()
    {
        BTN_Close.onClick.RemoveAllListeners();
        BTN_Close.onClick.AddListener(Hide);

        BTN_AddFriend.onClick.RemoveAllListeners();
        BTN_AddFriend.onClick.AddListener(delegate
        {
            string input = INPUT_FriendName.text;
            if (!string.IsNullOrEmpty(input))
            {
                AddFriendByName(input);
                INPUT_FriendName.text = string.Empty;
            }
        });

        MenuButtonSlider.SetOnClickIndexCallback(OnClickButton);
    }
    #endregion

    #region Generate Cell
    public FriendInfoListCell GenerateFriendCell(
          GameObject prefab, PlayerInfoUIData data, Transform target
        , UnityAction<PlayerInfoUIData> onChat
        , UnityAction<PlayerInfoUIData> onMatch
        , UnityAction<PlayerInfoUIData> onRemove
    )
    {
        GameObject obj = Instantiate(prefab, target);
        FriendInfoUI cell = obj.GetComponent<FriendInfoUI>();
        FriendInfoListCell clickCell = obj.GetComponent<FriendInfoListCell>();

        cell.SetData(data);
        clickCell.ShowGroupInfo();
        clickCell.SetEvent(data, onChat, onMatch, onRemove);

        return clickCell;
    }

    public FriendRequestCell GenerateFriendRequestCell(
          GameObject prefab, PlayerInfoUIData data, Transform target
        , UnityAction onClickYes
        , UnityAction onClickNo
    )
    {
        GameObject obj = Instantiate(prefab, target);
        FriendInfoUI cell = obj.GetComponent<FriendInfoUI>();
        FriendRequestCell clickCell = obj.GetComponent<FriendRequestCell>();

        cell.SetData(data);
        clickCell.SetEvent(onClickYes, onClickNo);

        return clickCell;
    }

    public FriendLastPlayedCell GenerateLastPlayedCell(
          GameObject prefab, PlayerInfoUIData data, bool isSent, Transform target
        , UnityAction onClickRequest
    )
    {
        GameObject obj = Instantiate(prefab, target);
        FriendInfoUI cell = obj.GetComponent<FriendInfoUI>();
        FriendLastPlayedCell clickCell = obj.GetComponent<FriendLastPlayedCell>();

        cell.SetData(data);
        clickCell.SetEvent(onClickRequest);
        clickCell.ShowSent(isSent);

        return clickCell;
    }

    public FriendReferralCell GenerateReferreeCell(
          GameObject prefab, PlayerInfoUIData data, ReferreeInfoData referreeInfo, Transform target
    )
    {
        GameObject obj = Instantiate(prefab, target);
        FriendInfoUI cell = obj.GetComponent<FriendInfoUI>();
        FriendReferralCell referreeCell = obj.GetComponent<FriendReferralCell>();

        cell.SetData(data);

        if (referreeInfo.LevelData.Level >= 7)
        {
            if (referreeInfo.ClaimedRewardLevel7)
            {
                referreeCell.ShowClaimed();
            }
            else
            {
                referreeCell.SetEvent(delegate
                {
                    referreeCell.ShowClaimed();
                    DataManager.Instance.RequestClaimReferreeLevel7Reward(referreeInfo.PlayerID,
                        (rewardClaimedList) =>
                        {
                            if (rewardClaimedList != null && rewardClaimedList.Count > 0)
                            {
                                DataManager.Instance.LoadInventory(() =>
                                {
                                    PopupUIManager.Instance.ShowPopup_GotReward(
                                                    LocalizationManager.Instance.GetText("POPUP_REFERRAL_TITLE_CLAIM_REFERRER_REWARD"),
                                                    LocalizationManager.Instance.GetText("POPUP_REFERRAL_BODY_CLAIM_REFERRER_REWARD"),
                                                    rewardClaimedList);
                                },
                                null);
                            }
                            else
                            {
                                PopupUIManager.Instance.ShowPopup_ConfirmOnly(
                                                    LocalizationManager.Instance.GetText("POPUP_REFERRAL_TITLE_CLAIM_REFERRER_REWARD"),
                                                    LocalizationManager.Instance.GetText("POPUP_REFERRAL_BODY_CLAIM_REFERRER_REWARD"));
                            }
                        },
                        (failMessage) =>
                        {
                            PopupUIManager.Instance.ShowPopup_Error(
                                        LocalizationManager.Instance.GetText("POPUP_REFERRAL_TITLE_ERROR"),
                                        LocalizationManager.Instance.GetText(failMessage));
                        });
                });
                referreeCell.ShowClaim();
            }
        }
        else
        {
            referreeCell.ShowLevel(referreeInfo.LevelData.Level);
        }

        return referreeCell;
    }
    #endregion

    #region Show/Hide
    public void Show()
    {
        TopToolbar.AddSetting(true, true, Hide);
        PopupUIManager.Instance.Show_SoftLoad(true);
        GameHelper.UITransition_FadeIn(this.gameObject, () => PopupUIManager.Instance.Show_SoftLoad(false));
        Init();
        OnClickButton(0); // Show Friend List
        RebuildUILayout();

        HomeManager.Instance.HideMainCanvas();

        OnOpenFriendList?.Invoke();
    }

    public void Hide()
    {
        TopToolbar.RemoveLatestSetting();
        HomeManager.Instance.ShowMainCanvas();
        GameHelper.UITransition_FadeOut(this.gameObject);
    }
    #endregion

    #region Navigation
    public void ShowFriendList(bool isShow)
    {
        HideAll();
        if (IsFriendShow != isShow)
        {
            IsFriendShow = isShow;
        }
        ChangeTab();
    }

    public void ShowRequestList(bool isShow)
    {
        HideAll();
        if (IsRequestShow != isShow)
        {
            IsRequestShow = isShow;
        }
        ChangeTab();
    }

    public void ShowLastPlayedList(bool isShow)
    {
        HideAll();
        if (IsLastPlayedShow != isShow)
        {
            IsLastPlayedShow = isShow;
        }
        ChangeTab();
    }

    public void ShowReferalList(bool isShow)
    {
        HideAll();
        if (IsReferalShow != isShow)
        {
            IsReferalShow = isShow;
        }
        ChangeTab();
    }

    private void HideAll()
    {
        IsFriendShow = false;
        IsRequestShow = false;
        IsLastPlayedShow = false;
        IsReferalShow = false;
    }

    /*
    private void ShowFriendDetail(PlayerInfoUIData data)
    {
        Popup_FriendDetail.ShowUI(
              data
            , DeleteFriend
            , delegate (PlayerInfoUIData playerInfoUIData)
            {
                HomeManager.Instance.StartFriendlyMatch(playerInfoUIData.PlayerID);
            }
        );
    }
    */

    public void InitNavigation()
    {
        IsFriendShow = true;
        IsRequestShow = false;
        IsLastPlayedShow = false;
        IsReferalShow = false;

        ChangeTab();
    }

    private void ShowTab(GameObject target, bool isShow)
    {
        PopupUIManager.Instance.Show_SoftLoad(true);
        if (isShow)
        {
            GameHelper.UITransition_FadeIn(target, () => PopupUIManager.Instance.Show_SoftLoad(false));
        }
        else
        {
            GameHelper.UITransition_FadeOut(target, () => PopupUIManager.Instance.Show_SoftLoad(false));
        }
    }

    public void ChangeTab()
    {
        ShowTab(Group_Friend, IsFriendShow);
        ShowTab(Group_Request, IsRequestShow);
        ShowTab(Group_Add, IsLastPlayedShow);
        ShowTab(Group_Referal, IsReferalShow);

        if (_isInited)
        {
            if (IsFriendShow) OnTapFriendList?.Invoke();
            if (IsRequestShow) OnTapFriendRequest?.Invoke();
            if (IsLastPlayedShow) OnTapAddFriend?.Invoke();
        }
    }
    #endregion

}
