﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FriendReferralUI : MonoBehaviour
{
    public Button CopyReferralCodeButton;
    public Button ApplyReferralCodeButton;
    public Button BannerButton;
    public TMP_Text ApplyReferralCodeButtonText;
    public TMP_InputField ReferralCodeInputField;
    public TMP_Text MyReferralCode;
    public TMP_Text NoReferreesText;
    public GameObject ReferralCodeInputUIGroup;

    public void SetShowNoReferreesText(bool doShow)
    {
        //New requirement: always show
        //NoReferreesText.gameObject.SetActive(doShow);
    }

    private void OnEnable()
    {
        UpdateUIState();
    }
    
    private void UpdateUIState()
    {
        if (DataManager.Instance.PlayerReferredInfoData.RegisteredReferralCode)
        {
            ReferralCodeInputField.text = DataManager.Instance.PlayerReferredInfoData.ReferrerPlayerID;
            ReferralCodeInputField.enabled = false;
            ApplyReferralCodeButtonText.text = LocalizationManager.Instance.GetText("BUTTON_REFERRAL_APPLIED");
            ApplyReferralCodeButton.onClick.RemoveAllListeners();
            ApplyReferralCodeButton.GetComponent<Image>().enabled = false;
            ReferralCodeInputUIGroup.SetActive(true);
        }
        else if(DataManager.Instance.PlayerInfo.LevelData.Level > 3)
        {
            ReferralCodeInputUIGroup.SetActive(false);
        }
        else
        {
            ReferralCodeInputField.text = "";
            ReferralCodeInputField.enabled = true;
            ApplyReferralCodeButtonText.text = LocalizationManager.Instance.GetText("BUTTON_REFERRAL_APPLY");
            ApplyReferralCodeButton.onClick.RemoveAllListeners();
            ApplyReferralCodeButton.onClick.AddListener(() => RegisterFriendReferralCode(ReferralCodeInputField.text));
            ApplyReferralCodeButton.GetComponent<Image>().enabled = true;
            ReferralCodeInputUIGroup.SetActive(true);
        }

        MyReferralCode.text = DataManager.Instance.GetCurrentPlayerReferralCode();
        CopyReferralCodeButton.onClick.RemoveAllListeners();
        CopyReferralCodeButton.onClick.AddListener(() => CopyReferralCodeToClipboard(DataManager.Instance.GetCurrentPlayerReferralCode()));
        BannerButton.onClick.RemoveAllListeners();
        BannerButton.onClick.AddListener
        (() => 
            PopupUIManager.Instance.ShowPopup_DetailWindow(LocalizationManager.Instance.GetText("POPUP_REFERRAL_BANNER_TITLE"), LocalizationManager.Instance.GetText("POPUP_REFERRAL_BANNER_BODY")) 
        );
    }

    private void CopyReferralCodeToClipboard(string code)
    {
        GameHelper.CopyToClipboard(code);
        PopupUIManager.Instance.ShowPopup_ConfirmOnly(
                               LocalizationManager.Instance.GetText("POPUP_REFERRAL_TITLE_CODE_COPIED"),
                               LocalizationManager.Instance.GetText("POPUP_REFERRAL_BODY_CODE_COPIED"));
    }

    private void RegisterFriendReferralCode(string code)
    {
        if (string.IsNullOrWhiteSpace(code))
        {
            return;
        }

        DataManager.Instance.RegisterFriendReferralCode(code, 
           (rewardClaimedList) => {
               if (rewardClaimedList.Count > 0)
               {
                   PopupUIManager.Instance.ShowPopup_GotReward(
                                       LocalizationManager.Instance.GetText("POPUP_REFERRAL_TITLE_REGISTERED"),
                                       LocalizationManager.Instance.GetText("POPUP_REFERRAL_BODY_REGISTERED_WITH_REWARD"),
                                       rewardClaimedList);
               }
               else
               {
                   PopupUIManager.Instance.ShowPopup_ConfirmOnly(
                                       LocalizationManager.Instance.GetText("POPUP_REFERRAL_TITLE_REGISTERED"),
                                       LocalizationManager.Instance.GetText("POPUP_REFERRAL_BODY_REGISTERED_NO_REWARD"));
               }
               
               UpdateUIState();
            },
            (failMessage) => {
                PopupUIManager.Instance.ShowPopup_Error(
                LocalizationManager.Instance.GetText("POPUP_REFERRAL_TITLE_ERROR"),
                LocalizationManager.Instance.GetText(failMessage));
            });
    }

}
