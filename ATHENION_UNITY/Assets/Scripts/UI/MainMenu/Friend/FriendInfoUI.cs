﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;
using DG.Tweening;

public class FriendInfoUI : MonoBehaviour
{
    #region Public Properties
    [Header("Image")]
    [SerializeField] private Image IMG_Avatar;
    [SerializeField] private Image IMG_RankFrame;
    [SerializeField] private Image IMG_Title;
    [SerializeField] private Image IMG_StatusIcon;

    [Header("TEXT")]
    [SerializeField] private TextMeshProUGUI TEXT_Name;
    [SerializeField] private TextMeshProUGUI TEXT_Status;
    [SerializeField] private TextMeshProUGUI TEXT_Level;
    [SerializeField] private TextMeshProUGUI TEXT_RankName;
    [SerializeField] private TextMeshProUGUI TEXT_LastLogin;
    [SerializeField] private TextMeshProUGUI TEXT_LastLoginTime;

    [Header("Status Color")]
    [SerializeField] private Color ColorOnline;
    [SerializeField] private Color ColorBusy;
    [SerializeField] private Color ColorPlaying;
    [SerializeField] private Color ColorOffline;
    #endregion

    #region Private Properties
    private static readonly string TimeFormat_Day   = "TEXT_LAST_LOGIN_TIME_DAY";
    private static readonly string TimeFormat_Hour  = "TEXT_LAST_LOGIN_TIME_HOUR";
    private static readonly string TimeFormat_Min   = "TEXT_LAST_LOGIN_TIME_MIN";
    private static readonly string TimeFormat_Sec   = "TEXT_LAST_LOGIN_TIME_SEC";

    private PlayerInfoUIData _data;
    #endregion

    #region Method
    public void SetData(PlayerInfoUIData data)
    {
        _data = data;

        SetAvatar(_data.AvatarID);
        SetRank(_data.RankID);
        SetTitle(_data.TitleID);
        SetName(_data.DisplayName);
        SetLevel(_data.CurrentLevel);
        SetStatus(_data.ActiveStatus);
        SetLastLogin(_data.ActiveStatus, _data.LastLogin);
    }

    public void SetAvatar(string key)
    {
        IMG_Avatar.sprite = SpriteResourceHelper.LoadSprite(key, SpriteResourceHelper.SpriteType.Avatar);
    }

    public void SetRank(string key)
    {
        IMG_RankFrame.sprite = SpriteResourceHelper.LoadSprite(key, SpriteResourceHelper.SpriteType.ICON_Rank);

        if (TEXT_RankName != null)
        {
            TEXT_RankName.text = GameHelper.GetRankName(key);
        }
    }

    public void SetTitle(string key)
    {
        IMG_Title.sprite = SpriteResourceHelper.LoadSprite(key, SpriteResourceHelper.SpriteType.Title);
    }

    public void SetName(string text)
    {
        TEXT_Name.text = text;
    }

    public void SetStatus(PlayerActiveStatus status)
    {
        Color statusColor;
        string statusText = "";

        switch (status)
        {
            case PlayerActiveStatus.Offline:
            {
                statusColor = ColorOffline;
                statusText = "STATUS_OFFLINE";
            }
            break;

            case PlayerActiveStatus.Online:
            {
                statusColor = ColorOnline;
                statusText = "STATUS_ONLINE";
            }
            break;

            case PlayerActiveStatus.Busy:
            {
                statusColor = ColorBusy;
                statusText = "STATUS_BUSY";
            }
            break;


            case PlayerActiveStatus.Playing:
            default:
            {
                statusColor = ColorPlaying;
                statusText = "STATUS_PLAYING";
            }
            break;
        }

        if (IMG_StatusIcon != null) IMG_StatusIcon.color = statusColor;
        if (TEXT_Status != null)    TEXT_Status.color = statusColor;
        if (TEXT_Status != null)    TEXT_Status.text = LocalizationManager.Instance.GetText(statusText);
    }

    public void SetLevel(int text)
    {
        TEXT_Level.text = "Lv." + text.ToString();
    }

    public void SetLastLogin(PlayerActiveStatus status, DateTime lastLogin)
    {
        if (TEXT_LastLogin != null && TEXT_LastLoginTime != null)
        {
            if (status == PlayerActiveStatus.Offline)
            {
                TEXT_LastLogin.gameObject.SetActive(true);

                DateTime serverTime = DateTimeData.GetDateTimeUTC();
                TimeSpan span = (serverTime - lastLogin);

                if (span.Days > 0)
                {
                    TEXT_LastLoginTime.text = string.Format(LocalizationManager.Instance.GetText(TimeFormat_Day), span.Days);
                }
                else if (span.Hours > 0)
                {
                    TEXT_LastLoginTime.text = string.Format(LocalizationManager.Instance.GetText(TimeFormat_Hour), span.Hours);
                }
                else if (span.Minutes > 0)
                {
                    TEXT_LastLoginTime.text = string.Format(LocalizationManager.Instance.GetText(TimeFormat_Min), span.Minutes);
                }
                else
                {
                    TEXT_LastLoginTime.text = string.Format(LocalizationManager.Instance.GetText(TimeFormat_Sec), span.Seconds);
                }
            }
            else
            {
                TEXT_LastLogin.gameObject.SetActive(false);
                TEXT_LastLoginTime.text = "";
            }
        }
    }
    #endregion
}
