﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Events;

public class FriendRequestCell : MonoBehaviour
{
    #region Public Properties
    #endregion

    #region Private Properties
    [Header("BTN")]
    [SerializeField] private Button BTN_Yes;
    [SerializeField] private Button BTN_No;

    private UnityAction _onClickYes;
    private UnityAction _onClickNo;
    #endregion

    #region Method
    public void SetEvent(UnityAction onYes, UnityAction onNo)
    {
        _onClickYes = onYes;
        _onClickNo = onNo;

        if (BTN_Yes != null)
        {
            BTN_Yes.onClick.RemoveAllListeners();
            BTN_Yes.onClick.AddListener(delegate
            {
                if (_onClickYes != null)
                {
                    _onClickYes.Invoke();
                }

            });
        }

        if (BTN_No != null)
        {
            BTN_No.onClick.RemoveAllListeners();
            BTN_No.onClick.AddListener(delegate
            {
                if (_onClickNo != null)
                {
                    _onClickNo.Invoke();
                }

            });
        }
    }
    #endregion
}
