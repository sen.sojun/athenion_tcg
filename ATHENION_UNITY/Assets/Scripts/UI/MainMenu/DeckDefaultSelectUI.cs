﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class DeckDefaultSelectUI : MonoBehaviour 
{
    [SerializeField]
    private GameObject _container;
    private List<DeckDefaultSelectButton> _buttonList;

    private UnityAction<int> _onSelectDeck = null;
     
    public void ShowUI(UnityAction<int> onSelectDeck)
    {
        _onSelectDeck = onSelectDeck;
        _buttonList = new List<DeckDefaultSelectButton>();

        if (_container != null)
        {
            // get all child
            int index = (int)CardElementType.FIRE;
            foreach (Transform t in _container.transform)
            {
                ElementDBData element;
                ElementDB.Instance.GetData(((CardElementType)index).ToString().ToUpper(), out element);
                DeckDefaultSelectButton button = t.GetComponent<DeckDefaultSelectButton>();
                button.Setup(
                      index
                    , element.Name
                    , OnSelect
                );
                _buttonList.Add(button);

                ++index;
            }
        }

        gameObject.SetActive(true);
    }

    public void HideUI()
    {
        gameObject.SetActive(false);
    }

    public void OnSelect(int index)
    {
        if (_onSelectDeck != null)
        {
            _onSelectDeck.Invoke(index);
        }
    }
}
