﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;
using Karamucho.UI;

public class PlayerCoinTopBarUI : MonoBehaviour
{
    #region Public Properties
    public TextMeshProUGUI AmountText;
    public Button ClickButton;
    #endregion

    #region Private Properties
    private UnityAction _onClick;
    #endregion
    
    public void OnEnable()
    {
        ClickButton.onClick.RemoveAllListeners();
        ClickButton.onClick.AddListener(OnClick);
        DataManager.OnUpdateInventory += OnDataManagerUpdateInventory;

        UpdateUI();
    }

    private void OnDisable()
    {
        DataManager.OnUpdateInventory -= OnDataManagerUpdateInventory;
        _onClick = null;
    }
     
    #region Metods
    private void OnDataManagerUpdateInventory()
    {
        UpdateUI();
    }

    public void UpdateUI()
    {
        UpdateAmount();
    }

    public void SetOnClick(UnityAction onClick)
    {
        _onClick = onClick;
    }

    private void UpdateAmount()
    {
        int coin = DataManager.Instance.InventoryData.GetVirtualCurrency(VirtualCurrency.CO);
        AmountText.text = string.Format("{0:n0}", coin);
    }

    private void OnClick()
    {
        Debug.Log("PlayerCoinTopBarUI/OnClick: Clicked!");

        if (_onClick != null)
        {
            _onClick.Invoke();
        }
    }
    #endregion
}
