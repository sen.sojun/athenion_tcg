﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MainMenuUI : MonoBehaviour
{
    public TextMeshProUGUI LanguageButtonText;
    public TextMeshProUGUI DebugButtonText;

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void ShowUI()
    {
        LanguageButtonText.text = LocalizationManager.Instance.CurrentLanguage.ToString();
        UpdateShowDebugLog();

        gameObject.SetActive(true);
    }

    public void HideUI()
    {
        gameObject.SetActive(false);
    }

    public void ToggleLanguage()
    {
        LocalizationManager.Instance.ToggleLanguage();
        LanguageButtonText.text = LocalizationManager.Instance.CurrentLanguage.ToString();
    }

    public void ToggleShowDebugLog()
    {
        if (DataManager.Instance.GetShowDebugLog())
        {
            DataManager.Instance.SetShowDebugLog(false);
        }
        else
        {
            DataManager.Instance.SetShowDebugLog(true);
        }

        UpdateShowDebugLog();
    }

    private void UpdateShowDebugLog()
    {
        if (DataManager.Instance.GetShowDebugLog())
        {
            DebugButtonText.text = "<color=green>ENABLE</color> DEBUG LOG";
        }
        else
        {
            DebugButtonText.text = "<color=red>DISABLE</color> DEBUG LOG";
        }
    }
}
