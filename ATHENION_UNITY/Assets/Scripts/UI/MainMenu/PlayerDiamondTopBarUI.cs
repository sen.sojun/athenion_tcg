﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;
using Karamucho.UI;
using System;

public class PlayerDiamondTopBarUI : MonoBehaviour
{
    #region Public Properties
    public TextMeshProUGUI AmountText;
    public Button TopupButton;
    #endregion

    #region Private Properties
    private UnityAction _onClick;
    #endregion

    public void OnEnable()
    {
        TopupButton.onClick.RemoveAllListeners();
        TopupButton.onClick.AddListener(OnClickTopup);

        DataManager.OnUpdateInventory += OnDataManagerUpdateInventory;

        UpdateUI();
    }

    private void OnDisable()
    {
        DataManager.OnUpdateInventory -= OnDataManagerUpdateInventory;
    }

    #region Metods

    private void OnDataManagerUpdateInventory()
    {
        UpdateUI();
    }

    public void UpdateUI()
    {
        UpdateAmount();
    }

    private void UpdateAmount()
    {
        int diamond = DataManager.Instance.InventoryData.GetVirtualCurrency(VirtualCurrency.DI);
        AmountText.text = string.Format("{0:n0}", diamond);
    }

    private void OnClickTopup()
    {
        Debug.Log("PlayerDiamondTopBarUI/OnClick: Clicked!");
        if (NavigatorController.Instance.CurrentPage.SceneName == GameHelper.HomeSceneName)
        {
            HomeManager.Instance.OnClickDiamond();
        }
        NavigatorController.Instance.ShowShopPage(ShopTypes.Diamond.ToString());
    }
    #endregion
}
