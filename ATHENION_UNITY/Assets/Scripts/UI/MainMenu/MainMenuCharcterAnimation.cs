﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Karamucho;

public class MainMenuCharcterAnimation : MonoBehaviour
{
    #region Private Properties
    private readonly string _animtedHeroPath = "Prefabs/AnimatedHero/";
    private readonly string _prefix = "GROUP_";

    private string _recentHeroID = "";
    private GameObject _currentAnimatedHero = null;
    private OutgameHeroAnimation _hero;
    private Coroutine _task = null;
    #endregion

    #region MonoBehaviour Event
    private void OnEnable()
    {
        StartCreateAnimatedHero();
    }

    private void OnDisable()
    {
        StopCreateAnimatedHero();
    }
    #endregion

    #region Methods
    public void StartCreateAnimatedHero()
    {
        StopCreateAnimatedHero();

        string deckID = DataManager.Instance.GetRecentDeckSelection();
        _recentHeroID = DataManager.Instance.GetDeck(deckID).HeroID;

        _task = StartCoroutine(OnCreateAnimatedHero());
    }

    public void StopCreateAnimatedHero()
    {
        if (_task != null)
        {
            StopCoroutine(_task);
            _task = null;
        }
    }

    private IEnumerator OnCreateAnimatedHero()
    {
        if (_currentAnimatedHero != null)
        {
            Destroy(_currentAnimatedHero);
        }

        // Delay 2 frames
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();

        string path = string.Format("{0}{1}{2}", _animtedHeroPath, _prefix, _recentHeroID);

        ResourceManager.LoadAsync<GameObject>(
              path
            , delegate (GameObject prefab)
            {
                if (prefab != null)
                {
                    _currentAnimatedHero = Instantiate(prefab, transform);
                    _currentAnimatedHero.transform.localPosition = Vector3.zero;
                    _hero = _currentAnimatedHero.GetComponent<OutgameHeroAnimation>();
                    _hero.SetHeroID(_recentHeroID);
                }
                else
                {
                    Debug.LogWarningFormat("Cannot find prefab: {0}", path);

                    // Try default prefab
                    path = string.Format("{0}{1}{2}", _animtedHeroPath, _prefix, "H0001");
                    ResourceManager.LoadAsync<GameObject>(
                          path
                        , delegate (GameObject defaultPrefab)
                        {
                            if (defaultPrefab != null)
                            {
                                _currentAnimatedHero = Instantiate(prefab, transform);
                                _currentAnimatedHero.transform.localPosition = Vector3.zero;
                            }
                            else
                            {
                                Debug.LogWarningFormat("Cannot find default prefab: {0}", path);
                            }
                        }
                    );
                }
            }
        );

        yield break;
    }

    public void PlayRandomAnimation()
    {
        // Disable Animation
        return;

        // Random Value
        int max = System.Enum.GetValues(typeof(BaseHeroAnimation.HeroAnimationState)).Length;
        int random = Random.Range((int)BaseHeroAnimation.HeroAnimationState.GREETING, (int)BaseHeroAnimation.HeroAnimationState.THREATEN + 1);
        BaseHeroAnimation.HeroAnimationState state = (BaseHeroAnimation.HeroAnimationState)random;

        // Create Sound
        SoundHeroData soundHero = new SoundHeroData(_recentHeroID, state);
        PlayHeroAnimation(soundHero, state);
    }

    public void PlayHeroAnimation(SoundHeroData soundHero, BaseHeroAnimation.HeroAnimationState state)
    { 
        if (_hero != null)
        {
            if (_hero.GetIsPlaying()) return;

            _hero.PlayAnimation(soundHero, state);
        }
    }
    #endregion
}
