﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;
using Karamucho.UI;

public class PlayerProfileTopBarUI : MonoBehaviour
{
    #region Public Properties
    public Image IMG_Avatar;
    public Image IMG_Title;
    public Image IMG_RankFrame;
    public Image IMG_FactionLevelFlag;
    public TextMeshProUGUI PlayerNameText;
    public TextMeshProUGUI PlayerLevelText;
    public TextMeshProUGUI TXT_LastPlayedFactionLevel;
    public Slider Slider_FactionLevelBar;

    [Header("Flag Sprite Ref")]
    public List<Sprite> FlagSpriteList;
    #endregion

    public void OnEnable()
    {
        UpdateUI();
    }

    private void OnDisable()
    {

    }

    #region Methods

    public void UpdateUI()
    {
        UpdatePlayerImage();
        UpdatePlayerName();
        UpdatePlayerLevel();
        UpdatePlayerRank();
        UpdatePlayerTitle();
        UpdatePlayerLastPlayedFaction();
    }

    private void UpdatePlayerImage()
    {
        // Set player image.
        IMG_Avatar.sprite = SpriteResourceHelper.LoadSprite(DataManager.Instance.PlayerInfo.AvatarID, SpriteResourceHelper.SpriteType.Avatar);
    }

    private void UpdatePlayerTitle()
    {
        //Set Player Title
        IMG_Title.sprite = SpriteResourceHelper.LoadSprite(DataManager.Instance.PlayerInfo.TitleID, SpriteResourceHelper.SpriteType.Title);
    }

    private void UpdatePlayerName()
    {
        // Set player name.
        PlayerNameText.text = DataManager.Instance.PlayerInfo.DisplayName;
    }

    private void UpdatePlayerRank()
    {
        string rankId = GameHelper.GetRankID(DataManager.Instance.GetPlayerRankIndex());
        string rankName = GameHelper.GetRankName(rankId);
        IMG_RankFrame.sprite = SpriteResourceHelper.LoadSprite(rankId, SpriteResourceHelper.SpriteType.ICON_Rank);
    }

    private void UpdatePlayerLevel()
    {
        // Set player level.
        PlayerLevelData currentPlayerLevelData = DataManager.Instance.PlayerInfo.LevelData;
        PlayerLevelText.text = currentPlayerLevelData.Level.ToString();
    }

    private void UpdatePlayerLastPlayedFaction()
    {
        string deckID = DataManager.Instance.GetRecentDeckSelection();
        DataManager.Instance.GetDeck(deckID).GetElementType(out CardElementType elementFaction);

        LevelFactionData flData = DataManager.Instance.PlayerInfo.LevelFactionData.GetLevelFactionData(elementFaction);
        if (flData == null)
        {
            flData = new LevelFactionData(0);
        }

        TXT_LastPlayedFactionLevel.text = string.Format("Lv.{0}",flData.Level);

        float playerExp = (flData.AccExp - flData.CurrentLevelAccExp);
        float requiredExpForThisLvl = (flData.NextLevelAccExp - flData.CurrentLevelAccExp);
        if (requiredExpForThisLvl == 0 || flData.NextLevelAccExp == 0)
        {
            Slider_FactionLevelBar.value = 1f;
        }
        else
        {
            Slider_FactionLevelBar.value = playerExp / requiredExpForThisLvl;
        }

        //Set Flag Img
        IMG_FactionLevelFlag.sprite = FlagSpriteList[(int)elementFaction];
    }

    #endregion
}
