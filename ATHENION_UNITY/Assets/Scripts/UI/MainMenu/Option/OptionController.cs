﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using DG.Tweening;
using System;

public class OptionController : MonoBehaviour
{
    #region Public Propereties
    [Header("Language")]
    public Toggle TOG_lang_thai;
    public Toggle TOG_lang_eng;

    [Header("Graphic")]
    public Toggle TOG_fps_30;
    public Toggle TOG_fps_60;
    public Toggle TOG_fps_120;
    public Toggle TOG_quaility_low;
    public Toggle TOG_quaility_med;
    public Toggle TOG_quaility_high;

    [Header("Audio")]
    public Toggle TOG_sound_off;
    public Toggle TOG_sound_on;
    public Toggle TOG_bgm_off;
    public Toggle TOG_bgm_on;

    [Header("BTN")]
    public Button BTN_OptionConfirm;
    public Button Tutorial;
    #endregion

    #region Private Propereties
    private Language _settingLanguage;
    private int _settingFPS;
    private ScreenQuality _settingQuality;
    private bool _settingSound;
    private bool _settingMusic;
    #endregion

    #region Events
    public static event Action OnShowUI;
    public static event Action OnHideUI;
    #endregion

    #region Method
    private void Awake()
    {
        LoadSaveSetting();
        UpdateUISetting();

        BTN_OptionConfirm.onClick.RemoveAllListeners();
        BTN_OptionConfirm.onClick.AddListener(delegate
        {
            SubmitSetting();
            UpdateUISetting();
            HideUI();
        });
    }

    public void SubmitSetting()
    {
        SetLanguage();
        SetFPS();
        SetQuality();
        SetSound();
        SetMusic();
    }

    public void CancelSetting()
    {
        HideUI();
    }

    public virtual void ShowUI()
    {
        OnShowUI?.Invoke();
        UpdateUISetting();
        GameHelper.UITransition_FadeIn(this.gameObject);
    }

    public virtual void HideUI()
    {
        OnHideUI?.Invoke();
        GameHelper.UITransition_FadeOut(this.gameObject);
    }

    private void UpdateUISetting()
    {
        UpdateLanguage();
        UpdateFPS();
        UpdateQuality();
        UpdateSound();
        UpdateBGM();
    }

    private void UpdateLanguage()
    {
        if (_settingLanguage == Language.Thai)
        {
            TOG_lang_thai.isOn = true;
            TOG_lang_eng.isOn = false;
        }
        else if (_settingLanguage == Language.English)
        {
            TOG_lang_thai.isOn = false;
            TOG_lang_eng.isOn = true;
        }
    }

    private void UpdateFPS()
    {
        switch (_settingFPS)
        {
            case 30:
                {
                    TOG_fps_30.isOn = true;
                    TOG_fps_60.isOn = false;
                    TOG_fps_120.isOn = false;
                    break;
                }
            case 60:
                {
                    TOG_fps_30.isOn = false;
                    TOG_fps_60.isOn = true;
                    TOG_fps_120.isOn = false;
                    break;
                }
            case 120:
                {
                    TOG_fps_30.isOn = false;
                    TOG_fps_60.isOn = false;
                    TOG_fps_120.isOn = true;
                    break;
                }
        }
    }

    private void UpdateQuality()
    {
        switch (_settingQuality)
        {
            case ScreenQuality.Low:
                {
                    TOG_quaility_low.isOn = true;
                    TOG_quaility_med.isOn = false;
                    TOG_quaility_high.isOn = false;
                    break;
                }
            case ScreenQuality.Med:
                {
                    TOG_quaility_low.isOn = false;
                    TOG_quaility_med.isOn = true;
                    TOG_quaility_high.isOn = false;
                    break;
                }
            case ScreenQuality.High:
                {
                    TOG_quaility_low.isOn = false;
                    TOG_quaility_med.isOn = false;
                    TOG_quaility_high.isOn = true;
                    break;
                }
        }
    }

    private void UpdateSound()
    {
        if (_settingSound)
        {
            TOG_sound_on.isOn = true;
            TOG_sound_off.isOn = false;
        }
        else
        {
            TOG_sound_on.isOn = false;
            TOG_sound_off.isOn = true;
        }
    }

    private void UpdateBGM()
    {
        if (_settingMusic)
        {
            TOG_bgm_on.isOn = true;
            TOG_bgm_off.isOn = false;
        }
        else
        {
            TOG_bgm_on.isOn = false;
            TOG_bgm_off.isOn = true;
        }
    }

    private void LoadSaveSetting()
    {
        _settingLanguage = LocalizationManager.Instance.CurrentLanguage;
        _settingFPS = DataManager.Instance.GetFPSSetting();
        _settingQuality = DataManager.Instance.GetScreenQuality();
        _settingSound = DataManager.Instance.GetSFXOn();
        _settingMusic = DataManager.Instance.GetBGMOn();
    }
    #endregion

    #region Setting Method
    private void SetLanguage()
    {
        if (TOG_lang_thai.isOn)
        {
            _settingLanguage = Language.Thai;

        }
        else if (TOG_lang_eng.isOn)
        {
            _settingLanguage = Language.English;
        }

        LocalizationManager.Instance.SetLanguage(_settingLanguage);
    }

    private void SetFPS()
    {
        if (TOG_fps_30.isOn)
        {
            _settingFPS = 30;
        }
        else if (TOG_fps_60.isOn)
        {
            _settingFPS = 60;
        }
        else if (TOG_fps_120.isOn)
        {
            _settingFPS = 120;
        }

        DataManager.Instance.SetFPSSetting(_settingFPS);
        Application.targetFrameRate = _settingFPS;
    }

    private void SetQuality()
    {
        if (TOG_quaility_low.isOn)
        {
            _settingQuality = ScreenQuality.Low;
        }
        else if (TOG_quaility_med.isOn)
        {
            _settingQuality = ScreenQuality.Med;
        }
        else if (TOG_quaility_high.isOn)
        {
            _settingQuality = ScreenQuality.High;
        }

        DataManager.Instance.SetScreenQuality(_settingQuality);
        switch (_settingQuality)
        {
            case ScreenQuality.Low:
                {
                    ScreenManager.SetScreenSize(GameHelper.QualityLowValue);
                    break;
                }
            case ScreenQuality.Med:
                {
                    ScreenManager.SetScreenSize(GameHelper.QualityMedValue);
                    break;
                }
            case ScreenQuality.High:
                {
                    ScreenManager.SetScreenSize(GameHelper.QualityHighValue);
                    break;
                }
        }
    }

    private void SetSound()
    {
        if (TOG_sound_on.isOn)
        {
            _settingSound = true;
        }
        else if (TOG_sound_off.isOn)
        {
            _settingSound = false;
        }

        DataManager.Instance.SetSFXOn(_settingSound);
        DataManager.Instance.SetVoiceOn(_settingSound);

        SoundManager.SetSFXOn(_settingSound);
        SoundManager.SetVoiceOn(_settingSound);
    }

    private void SetMusic()
    {
        if (TOG_bgm_on.isOn)
        {
            _settingMusic = true;
        }
        else if (TOG_bgm_off.isOn)
        {
            _settingMusic = false;
        }

        DataManager.Instance.SetBGMOn(_settingMusic);
        SoundManager.SetBGMOn(_settingMusic);
    }
    #endregion
}
