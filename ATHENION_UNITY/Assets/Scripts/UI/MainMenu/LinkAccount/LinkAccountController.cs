﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;

#if UNITY_ANDROID
using GooglePlayGames;
#endif

public class LinkAccountController : MonoBehaviour
{
    #region Public Properties
    public UnityAction OnCompleteLink;
    #endregion

    #region Private Properties
    private string _inputEmailAddress = string.Empty;
    private string _inputPassword = string.Empty;
    private string _inputPasswordConfirm = string.Empty;
    #endregion

    #region Private UI Properties
    [Header("Input Register")]
    [SerializeField] private TMP_InputField _InputEmail;
    [SerializeField] private TMP_InputField _InputPassword;
    [SerializeField] private TMP_InputField _InputPasswordConfirm;

    [Header("BTN")]
    [SerializeField] private Button _ButtonRegister;
    [SerializeField] private Button _ButtonLinkFacebook;
    [SerializeField] private Button _ButtonLinkGoogle;
    [SerializeField] private Button _BTN_Close;

    [Header("TEXT")]
    [SerializeField] private TextMeshProUGUI TEXT_EmailID;
    [SerializeField] private TextMeshProUGUI TEXT_FacebookID;
    [SerializeField] private TextMeshProUGUI TEXT_GoogleID;

    [Header("Ref")]
    [SerializeField] private DisplayNameSettingManager _DisplayNameSettingPanel;
    [SerializeField] private GameObject GROUP_LinkedAccount;
    [SerializeField] private GameObject GROUP_Register;
    #endregion

    #region Methods
    public void Show()
    {
        GameHelper.UITransition_FadeIn(this.gameObject);
        RefreshUI();
        InitBTN();
    }

    public void Hide()
    {
        GameHelper.UITransition_FadeOut(this.gameObject);
    }

    private void RefreshUI()
    {
        PlayerAccountInfoData accountData = DataManager.Instance.PlayerAccountInfo;

        // Linked Email
        Debug.LogWarning(accountData.IsLinkedEmail + "   " + accountData.GetLinkedPlayFabEmail());
        GROUP_LinkedAccount.SetActive(accountData.IsLinkedEmail);
        GROUP_Register.SetActive(!accountData.IsLinkedEmail);

        // Set Account Email
        if (accountData.IsLinkedEmail) TEXT_EmailID.text = accountData.GetLinkedPlayFabEmail();

        _ButtonLinkFacebook.interactable = !accountData.IsLinkedFacebook;
        if (accountData.IsLinkedFacebook) TEXT_FacebookID.text = accountData.GetLinkedFacebookFullName();

        _ButtonLinkGoogle.interactable = !accountData.IsLinkedGoogle;
        if (accountData.IsLinkedGoogle) TEXT_GoogleID.text = accountData.GetLinkedGoogleEmail();

        // Reset
        _InputEmail.text = "";
        _InputPassword.text = "";
        _InputPasswordConfirm.text = "";
    }

    private void InitBTN()
    {
        _ButtonRegister.onClick.RemoveAllListeners();
        _ButtonRegister.onClick.AddListener(DoRegisterEmail);

        _ButtonLinkFacebook.onClick.RemoveAllListeners();
        _ButtonLinkFacebook.onClick.AddListener(DoLinkFacebookAccount);

        _ButtonLinkGoogle.onClick.RemoveAllListeners();
        _ButtonLinkGoogle.onClick.AddListener(DoLinkGooglePlayGames);

        _BTN_Close.onClick.RemoveAllListeners();
        _BTN_Close.onClick.AddListener(Hide);
    }

    private void CheckDisplayNameValid()
    {
        if(DataManager.Instance.IsDisplayNameValid())
        {
            OnLinkAccountComplete();
        }
        else
        {
            _DisplayNameSettingPanel.SetCallbacks(OnLinkAccountComplete, null);
            _DisplayNameSettingPanel.ShowUI();
        }
    }

    private void OnLinkAccountComplete()
    {
        // EVERYTHING JUST FIND
        PopupUIManager.Instance.Show_MidSoftLoad(false);
        RefreshUI();

        OnCompleteLink?.Invoke();
    }

    #region LinkFacebook
    /// <summary>
    /// Link with Facebook Account
    /// </summary>
    private void DoLinkFacebookAccount()
    {
        AuthenticationManager.LinkAccountFacebook(OnFacebookLinkComplete, OnFacebookLinkFail);
    }

    private void OnFacebookLinkComplete()
    {
        PopupUIManager.Instance.Show_MidSoftLoad(false);
        CheckDisplayNameValid();
    }

    private void OnFacebookLinkFail(string error)
    {
        PopupUIManager.Instance.Show_MidSoftLoad(false);
        PopupInvalid(error);
    }
    #endregion

    #region LinkGooglePlayGames
    /// <summary>
    /// Link with GooglePlayGames Account
    /// </summary>
    private void DoLinkGooglePlayGames()
    {
        #if UNITY_ANDROID
        AuthenticationManager.LinkAccountGooglePlayGames(OnGoogleLinkComplete, OnGoogleLinkFail);
        #endif
    }

    private void OnGoogleLinkComplete()
    {
        CheckDisplayNameValid();
    }

    private void OnGoogleLinkFail(string error)
    {
        StartCoroutine(IEGoogleSignOut(
                delegate()
                {
                    PopupUIManager.Instance.Show_MidSoftLoad(false);
                    PopupInvalid(error);
                }
            )
        );      
    }

    private IEnumerator IEGoogleSignOut(UnityAction onComplete)
    {
        #if UNITY_ANDROID
        PlayGamesPlatform.Instance.SignOut();
        while (PlayGamesPlatform.Instance.IsAuthenticated())
        {
            yield return null;
        }
        #endif

        onComplete?.Invoke();

        yield break;
    }

    #endregion

    #region RegisterEmail
    private void DoRegisterEmail()
    {
        GetInputText();

        if (string.IsNullOrEmpty(_inputEmailAddress) || string.IsNullOrEmpty(_inputPassword) || string.IsNullOrEmpty(_inputPasswordConfirm))
        {
            // INVALID INFORMATION
            PopupInvalid();
            return;
        }
        if (_inputPassword != _inputPasswordConfirm && _inputPassword.Length < 6)
        {
            PopupInvalid();
            return;
        }


        PopupUIManager.Instance.Show_MidSoftLoad(true);
        AuthenticationManager.RegisterEmailPassword(
            _inputEmailAddress
            , _inputPassword
            , onRegisterEmailComplete
            , onRegisterEmailFail
        );
    }

    private void GetInputText()
    {
        _inputEmailAddress = _InputEmail.text;
        _inputPassword = _InputPassword.text;
        _inputPasswordConfirm = _InputPasswordConfirm.text;
    }

    private void PopupInvalid(string error = "", UnityAction onClick = null)
    {
        string title = LocalizationManager.Instance.GetText("TITLE_ERROR");
        string message = LocalizationManager.Instance.GetText("ERROR_REGISTER_MESSAGE");
        if (!string.IsNullOrEmpty(error))
        {
            message = LocalizationManager.Instance.GetText(string.Format("ERROR_PLAYFAB_{0}", error));
        }
        PopupUIManager.Instance.ShowPopup_Error(title, message, onClick);
    }

    #region Register Email Methods
    private void onRegisterEmailComplete()
    {
        DataManager.Instance.UpdateContactEmail(
            _inputEmailAddress
           , delegate ()
           {
               PopupUIManager.Instance.Show_MidSoftLoad(false);
               CheckDisplayNameValid();
           }
           , onRegisterEmailFail
       );
    }

    private void onRegisterEmailFail(string error)
    {
        PopupUIManager.Instance.Show_MidSoftLoad(false);
        PopupInvalid(error);
    }
    #endregion

    #endregion

    #endregion
}