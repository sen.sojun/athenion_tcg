﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Spine;
using Spine.Unity;

public class MainMenuCharacterUI : MonoBehaviour
{
    #region Enums
    public enum HeroAnimationKey
    {
          action01
        , action02
        , action03
        , idle
        , idle01
        , idle02
        , idle03
    }

    public enum AnimationState
    {
          Idle      = 0
        , Action    = 1
    }
    #endregion

    #region Public Properties
    public GameObject HeroSkeleton;
    public Button ClickButton;

    public Material HeroSkeletonMaterial;
    public SkeletonDataAsset HeroData_Alex;
    public SkeletonDataAsset HeroData_Tear;
    public SkeletonDataAsset HeroData_Brundo;
    public SkeletonDataAsset HeroData_Ralana;
    public SkeletonDataAsset HeroData_Sadia;
    public SkeletonDataAsset HeroData_Tartarus;
    public SkeletonDataAsset HeroData_Marry;
    public SkeletonDataAsset HeroData_Nyx;
    public SkeletonDataAsset HeroData_Zebon;
    #endregion

    #region Private Properties
    private SkeletonGraphic _skeleton;
    private HeroDBData _heroData;
    private AnimationState _state = AnimationState.Idle;
    private bool _isCanClick = true;
    private int _heroIndex = 0;
    #endregion

    private void Awake()
    {
        Init();
    }

    /*
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    */

    public void OnEnable()
    {
        Init();
    }

    public void OnDisable()
    {
        UnsubscribeAnimation();
    }

    #region Methods
    public void Init()
    {
        ClickButton.onClick.RemoveAllListeners();
        ClickButton.onClick.AddListener(OnClick);

        _skeleton = HeroSkeleton.GetComponent<SkeletonGraphic>();
        UpdateUI();
    }

    public void UpdateUI()
    {
        RandomHeroAnimate();
    }

    public void RandomHeroAnimate()
    {
        List<string> heroIDList = new List<string>()
        {
            // "H0001" // Alex
              "H0002" // Tear
            , "H0003" // Brundo
            //, "H0004" // Ralana
            , "H0005" // Sadia
            , "H0006" // Tartarrus
            //, "H0007" // Mary
            //, "H0008" // Nyx
            //, "H0009" // Zabon
        };

        //string heroID = heroIDList[Random.Range(0, heroIDList.Count)];
        if(_heroIndex > heroIDList.Count - 1)
        {
            _heroIndex = 0;
        }
        string heroID = heroIDList[_heroIndex++];

        HeroDBData heroData;
        if (HeroDB.Instance.GetData(heroID, out heroData))
        {
            _heroData = heroData;
            //Debug.LogFormat("RandomHeroAnimate: {0}", heroData.ID);
            UpdateHeroAnimate();
        }

        _isCanClick = true;
    }

    private void UpdateHeroAnimate()
    {
        if (_heroData != null)
        {
            SkeletonDataAsset skeletonDataAsset;

            switch (_heroData.ID)
            {
                case "H0001": skeletonDataAsset = HeroData_Alex; break;
                case "H0002": skeletonDataAsset = HeroData_Tear; break;
                case "H0003": skeletonDataAsset = HeroData_Brundo; break;
                case "H0004": skeletonDataAsset = HeroData_Ralana; break;
                case "H0005": skeletonDataAsset = HeroData_Sadia; break;
                case "H0006": skeletonDataAsset = HeroData_Tartarus; break;
                case "H0007": skeletonDataAsset = HeroData_Marry; break;
                case "H0008": skeletonDataAsset = HeroData_Nyx; break;
                case "H0009": skeletonDataAsset = HeroData_Zebon; break;

                default:
                {
                    skeletonDataAsset = HeroData_Sadia; break;
                }
            }

            if (skeletonDataAsset != null)
            {
                SetSkeletonDataAsset(skeletonDataAsset);
                return;
            }
        }

        HeroSkeleton.gameObject.SetActive(false);
    }

    private void SetSkeletonDataAsset(SkeletonDataAsset skeletonDataAsset)
    {
        if (skeletonDataAsset != null)
        {
            /*
            if (HeroSkeleton.GetComponent<SkeletonGraphic>() != null)
            {
                DestroyImmediate(HeroSkeleton.GetComponent<SkeletonGraphic>());
                //Destroy(_skeleton.gameObject);
            }

            _skeleton = SkeletonGraphic.AddSkeletonGraphicComponent(HeroSkeleton, skeletonDataAsset);
            _skeleton.material = HeroSkeletonMaterial;
            
            _skeleton.startingAnimation = "";
            _skeleton.AnimationState.SetEmptyAnimations(0.0f);
            _skeleton.startingLoop = false;
            */

            _skeleton.skeletonDataAsset = skeletonDataAsset;
            //_skeleton.Clear();
            _skeleton.Initialize(true);
            

            SubscribeAnimation();

            _state = AnimationState.Idle;
            HeroSkeleton.gameObject.SetActive(true);
            PlayAnimationIdle();
        }
    }

    private void SubscribeAnimation()
    {
        _skeleton.AnimationState.Start += OnAnimationStart;
        _skeleton.AnimationState.Complete += OnAnimationComplete;
        _skeleton.AnimationState.End += OnAnimationEnd;
    }

    private void UnsubscribeAnimation()
    {
        _skeleton.AnimationState.Start -= OnAnimationStart;
        _skeleton.AnimationState.Complete -= OnAnimationComplete;
        _skeleton.AnimationState.End -= OnAnimationEnd;
    }
    #endregion

    #region Event Methods
    private void OnClick()
    {
        return;

        Debug.Log("MainMenuCharacterUI/OnClick: Clicked!");

        if (_heroData != null && _isCanClick)
        {
            _isCanClick = false;
            PlayHeroVoice(_heroData.ID);
        }

        if (_state == AnimationState.Idle)
        {
            //PlayAnimationAction();
        }
    }

    private void PlayAnimationIdle()
    {
        HeroAnimationKey key = HeroAnimationKey.idle;      

        _state = AnimationState.Idle;
        TrackEntry te = _skeleton.AnimationState.SetAnimation(0, key.ToString(), false);
        //te.EventThreshold = 0.5f;
    }

    private void PlayHeroVoice(string heroID)
    {
        SoundHeroData soundHeroData = new SoundHeroData(heroID, HeroVoice.Begin);
        SoundManager.PlayHeroVoice(soundHeroData, OnClickComplete);
    }

    private void OnClickComplete()
    {
        _isCanClick = true;
    }

    private void OnAnimationStart(TrackEntry trackEntry)
    {
        //Debug.Log("OnAnimationStart : " + trackEntry.TrackIndex);
    }

    private void OnAnimationComplete(TrackEntry trackEntry)
    {
        //Debug.Log("OnAnimationComplete : " + trackEntry.TrackIndex);

        switch (_state)
        {
            case AnimationState.Idle:
            case AnimationState.Action:
            {
                //PlayAnimationIdle();
                UpdateUI();
            }
            break;
        }
    }

    private void OnAnimationEnd(TrackEntry trackEntry)
    {
        //Debug.Log("OnAnimationEnd : " + trackEntry.TrackIndex);
    }
    #endregion
}
