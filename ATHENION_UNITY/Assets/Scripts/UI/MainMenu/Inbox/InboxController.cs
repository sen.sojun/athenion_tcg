﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using Newtonsoft.Json;
using DG.Tweening;

public class InboxController : MonoBehaviour
{
    #region Inspector
    [Header("Inspector")]
    [SerializeField] private ScrollRect ScrollRect;
    [SerializeField] private RectTransform Viewport;

    [SerializeField] private Button BTN_DeleteAll;
    [SerializeField] private Button BTN_ReceiveAll;
    [SerializeField] private Button BTN_Close;
    #endregion

    #region Public Properties
    [Header("Prefab Cell")]
    public MessageInboxCell PrefabMessageCell;

    [Header("Ref")]
    public Transform Container;

    [Header("Popup")]
    public MessageInboxPopup InboxMessagePopup;
    #endregion

    #region Private Properties
    private List<MessageInboxCell> _inboxMessageCellList = new List<MessageInboxCell>();
    private bool _isInboxMessagePopupShow;
    private bool _isInit = false;
    private Coroutine _refreshTask = null;

    private List<MessageInboxUIData> _messageInboxUIDataList = new List<MessageInboxUIData>();
    #endregion

    #region Static Event Properties
    public static event Action OnActive;
    public static event Action OnOpenMessage;
    public static event Action<MessageInboxData> OnReceiveReward;
    public static event Action<int, List<ItemData>> OnReceiveAllReward;
    public static event Action<MessageInboxData> OnDeleteMessage;
    public static event Action<int> OnDeleteAllMessage;
    public static event Action OnRefreshInbox;
    #endregion

    #region Methods
    public void InitData()
    {
        _isInit = true;
        _messageInboxUIDataList = GetAllMessageInboxUIData();

        SetEnableLayoutComponent(true);
        ClearAllMessage();

        CreateMessage(_messageInboxUIDataList);

        InitBTN();

        OnRefreshInbox?.Invoke();
    }

    public void InitBTN()
    {
        BTN_DeleteAll.onClick.RemoveAllListeners();
        BTN_DeleteAll.onClick.AddListener(DeleteAllEmptyMessage);
        BTN_DeleteAll.gameObject.SetActive(_messageInboxUIDataList.Count > 0);

        BTN_ReceiveAll.onClick.RemoveAllListeners();
        BTN_ReceiveAll.onClick.AddListener(GetAllMessageReward);
        BTN_ReceiveAll.gameObject.SetActive(IsCanClaimAll());

        BTN_Close.onClick.RemoveAllListeners();
        BTN_Close.onClick.AddListener(Hide);
    }

    public void Show()
    {
        OnActive?.Invoke();

        TopToolbar.AddSetting(true, true, Hide);
        InitData();
        ScrollRect.onValueChanged.AddListener(OnScrolling);

        GameHelper.UITransition_FadeIn(this.gameObject, delegate ()
        {
            HomeManager.Instance.HideMainCanvas();
        });
    }

    public void Hide()
    {
        HomeManager.Instance.ShowMainCanvas();
        ScrollRect.onValueChanged.RemoveListener(OnScrolling);
        TopToolbar.RemoveLatestSetting();
        GameHelper.UITransition_FadeOut(this.gameObject);
    }

    private void ClearAllMessage()
    {
        foreach (MessageInboxCell item in _inboxMessageCellList)
        {
            DestroyImmediate(item.gameObject);
        }

        _inboxMessageCellList.Clear();
    }

    private void DeleteAllEmptyMessage()
    {
        PopupUIManager.Instance.ShowPopup_SureCheck(
              LocalizationManager.Instance.GetText("TEXT_DELETE_ALL_MESSAGE_CONFIRMATION_TITLE")
            , LocalizationManager.Instance.GetText("TEXT_DELETE_ALL_MESSAGE_CONFIRMATION_DESCRIPTION")
            , delegate ()
            {
                PopupUIManager.Instance.Show_MidSoftLoad(true);

                int messageAmount = GetMessageAmountCanDelete();
                DataManager.Instance.DeleteAllMessageInbox(
                    delegate ()
                    {
                        OnDeleteAllMessage?.Invoke(messageAmount);
                        RefreshMessage();

                        PopupUIManager.Instance.ShowPopup_ConfirmOnlyWithCorrect(
                              LocalizationManager.Instance.GetText("TEXT_DELETE_ALL_MESSAGE_SUCCESS_TITLE")
                            , LocalizationManager.Instance.GetText("TEXT_DELETE_ALL_MESSAGE_SUCCESS_DESCRIPTION")
                        );

                        PopupUIManager.Instance.Show_MidSoftLoad(false);
                    },
                    delegate (string errorMsg)
                    {
                        Debug.LogErrorFormat("DeleteAllEmptyMessage: Failed Delete all message {0}", errorMsg);

                        PopupUIManager.Instance.ShowPopup_Error(
                              LocalizationManager.Instance.GetText("ERROR_POPUP_HEADER")
                            , LocalizationManager.Instance.GetText("TEXT_DELETE_ALL_MESSAGE_FAIL_DESCRIPTION")
                        );

                        PopupUIManager.Instance.Show_MidSoftLoad(false);
                    }
                );
            }
            , null
        );
    }

    private void GetAllMessageReward()
    {
        // Check if still have reward message
        if (!GetIsHaveRewardToClaim()) return;

        int messageAmount = GetMessageAmountCanClaime();
        PopupUIManager.Instance.Show_MidSoftLoad(true);
        DataManager.Instance.ClaimAllMessageInboxItem(
            delegate (List<ItemData> itemDataList)
            {
                OnReceiveAllReward?.Invoke(messageAmount, itemDataList);
                RefreshMessage();
                PopupUIManager.Instance.Show_MidSoftLoad(false);
                string title = LocalizationManager.Instance.GetText("TEXT_CLAIM_ALL_REWARD");
                string message = LocalizationManager.Instance.GetText("TEXT_GOT_REWARD"); ;
                PopupUIManager.Instance.ShowPopup_GotReward(title, message, itemDataList);
            },
            delegate
            {
                PopupUIManager.Instance.Show_MidSoftLoad(false);
                Debug.LogWarning("Receive All message FAIL !");
            }
        );
    }

    private void CreateMessage(List<MessageInboxUIData> dataList)
    {
        int index = -1;
        for (int i = 0; i < dataList.Count; i++)
        {
            if (i >= _inboxMessageCellList.Count)
            {
                GameObject obj = Instantiate(PrefabMessageCell.gameObject, Container);
                _inboxMessageCellList.Add(obj.GetComponent<MessageInboxCell>());
            }

            UpdateMessage(_inboxMessageCellList[i], dataList[i]);
            index = i;
        }

        for (int i = index + 1; i < _inboxMessageCellList.Count; i++)
        {
            GameObject obj = _inboxMessageCellList[i].gameObject;
            _inboxMessageCellList.RemoveAt(i);
            Destroy(obj);

            i--;
        }
    }

    private void UpdateMessage(MessageInboxCell cell, MessageInboxUIData data)
    {
        cell.InitData(
            data
            , delegate () // onClick
            {
                ShowMessagePopup(data);
            }
            , delegate () // onDelete
            {
                DeleteMessageInbox(data);
            }
        );
    }

    private void DeleteMessageInbox(MessageInboxUIData data)
    {
        PopupUIManager.Instance.ShowPopup_SureCheck(
            LocalizationManager.Instance.GetText("TEXT_DELETE_MESSAGE_CONFIRMATION_TITLE")
            , LocalizationManager.Instance.GetText("TEXT_DELETE_MESSAGE_CONFIRMATION_DESCRIPTION")
            , delegate ()
            {
                PopupUIManager.Instance.Show_SoftLoad(true);

                MessageInboxData message = new MessageInboxData(data.Data);
                DataManager.Instance.DeleteMessageInbox(
                    data.ID
                    , delegate () // onComplete
                    {
                        OnDeleteMessage?.Invoke(message);

                        RefreshMessage();

                        PopupUIManager.Instance.ShowPopup_ConfirmOnlyWithCorrect(
                            LocalizationManager.Instance.GetText("TEXT_DELETE_MESSAGE_SUCCESS_TITLE")
                            , LocalizationManager.Instance.GetText("TEXT_DELETE_MESSAGE_SUCCESS_DESCRIPTION")
                        );

                        PopupUIManager.Instance.Show_SoftLoad(false);
                    }
                    , delegate (string errorMsg) // onFail
                    {
                        RefreshMessage();

                        PopupUIManager.Instance.ShowPopup_Error(
                            LocalizationManager.Instance.GetText("ERROR_POPUP_HEADER")
                            , LocalizationManager.Instance.GetText("TEXT_DELETE_MESSAGE_FAIL_DESCRIPTION")
                        );

                        PopupUIManager.Instance.Show_SoftLoad(false);
                    }
                );
            }
            , null
        );
    }

    public void RefreshMessage()
    {
        if (_refreshTask != null)
        {
            StopCoroutine(_refreshTask);
            _refreshTask = null;
        }

        if (gameObject.activeSelf)
        {
            _refreshTask = StartCoroutine(OnRefreshMessage());
        }
    }

    private IEnumerator OnRefreshMessage()
    {
        _messageInboxUIDataList = GetAllMessageInboxUIData();

        if (_isInit == false)
        {
            _refreshTask = null;
            yield break;
        }

        PopupUIManager.Instance.Show_MidSoftLoad(true);

        _isInit = false;

        SetEnableAllLayout(true);
        //ClearAllMessage();

        yield return new WaitForEndOfFrame();

        CreateMessage(_messageInboxUIDataList);

        BTN_DeleteAll.gameObject.SetActive(_messageInboxUIDataList.Count > 0);
        BTN_ReceiveAll.gameObject.SetActive(IsCanClaimAll());

        yield return new WaitForEndOfFrame();
        _isInit = true;

        PopupUIManager.Instance.Show_MidSoftLoad(false);

        OnRefreshInbox?.Invoke();
        _refreshTask = null;
    }

    private void ShowMessagePopup(MessageInboxUIData data)
    {
        _isInboxMessagePopupShow = true;

        OnOpenMessage?.Invoke();

        InboxMessagePopup.ShowUI(
              data
            , delegate () // onClick
            {
                PopupUIManager.Instance.Show_SoftLoad(true);

                DataManager.Instance.ClaimMessageInboxItem(
                      data.ID
                    , delegate () // onComplete
                    {
                        OnReceiveReward?.Invoke(data.Data);

                        RefreshMessage();
                        HideMessagePopup();

                        PopupUIManager.Instance.ShowPopup_GotReward(
                              LocalizationManager.Instance.GetText("TEXT_CLAIM_MESSAGE_SUCCESS_TITLE")
                            , LocalizationManager.Instance.GetText("TEXT_CLAIM_MESSAGE_SUCCESS_DESCRIPTION")
                            , data.ItemDataList
                        );

                        PopupUIManager.Instance.Show_SoftLoad(false);
                    }
                    , delegate (string errorMsg) // onFail
                    {
                        RefreshMessage();
                        HideMessagePopup();

                        Debug.LogErrorFormat("ClaimMessageInboxItem: Failed! {0}", errorMsg);
                        PopupUIManager.Instance.ShowPopup_Error(
                              LocalizationManager.Instance.GetText("ERROR_POPUP_HEADER")
                            , LocalizationManager.Instance.GetText("TEXT_CLAIM_MESSAGE_FAIL_DESCRIPTION")
                        );

                        PopupUIManager.Instance.Show_SoftLoad(false);
                    }
                );
            }
            , delegate () // onClose
            {
                RefreshMessage();
                HideMessagePopup();
            }
        );
    }

    private void HideMessagePopup()
    {
        _isInboxMessagePopupShow = false;
        InboxMessagePopup.HideUI();
    }

    private List<MessageInboxUIData> GetAllMessageInboxUIData()
    {
        List<MessageInboxUIData> messageDataList = new List<MessageInboxUIData>();
        List<MessageInboxData> allMessageInboxData = DataManager.Instance.GetAllMessageInboxData();

        foreach (MessageInboxData item in allMessageInboxData)
        {
            if (CheckMessageInboxValid(item))
            {
                messageDataList.Add(new MessageInboxUIData(item));
            }
        }

        return messageDataList;
    }

    private bool GetIsHaveRewardToClaim()
    {
        return GetMessageAmountCanClaime() > 0;
    }

    private int GetMessageAmountCanClaime()
    {
        int result = 0;
        List<MessageInboxData> allMessageInboxData = DataManager.Instance.GetAllMessageInboxData();
        foreach (MessageInboxData item in allMessageInboxData)
        {
            if (IsCanClaim(item))
            {
                result++;
            }
        }

        return result;
    }

    private int GetMessageAmountCanDelete()
    {
        int result = 0;
        List<MessageInboxData> allMessageInboxData = DataManager.Instance.GetAllMessageInboxData();
        foreach (MessageInboxData item in allMessageInboxData)
        {
            if (item.IsRead)
            {
                if (item.IsCanClaim())
                {
                    if (item.IsClaimed == false)
                    {
                        continue;
                    }
                }
                result++;
            }
        }

        return result;
    }

    private bool IsCanClaim(MessageInboxData item)
    {
        return CheckMessageInboxValid(item) && item.IsClaimed == false;
    }

    private bool IsCanClaimAll()
    {
        foreach (MessageInboxUIData item in _messageInboxUIDataList)
        {
            if (item.IsCanClaim())
            {
                return true;
            }
        }

        return false;
    }

    private bool CheckMessageInboxValid(MessageInboxData messageData)
    {
        if (!messageData.IsExpired
            && messageData.IsReceived
            && !messageData.IsDeleted)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    protected virtual void OnScrolling(Vector2 value)
    {
        if (_isInit && ScrollRect != null && Viewport != null && ScrollRect.content.transform.childCount > 0)
        {
            SetEnableLayoutComponent(false);

            // Show Only Visible Child.
            RectTransform child_rect = null;
            foreach (Transform child in ScrollRect.content.transform)
            {
                child_rect = child.GetComponent<RectTransform>();
                if (child_rect != null)
                {
                    if (GameHelper.IsRectTouchRect(child_rect, Viewport))
                    {
                        child.gameObject.SetActive(true);
                    }
                    else
                    {
                        child.gameObject.SetActive(false);
                    }
                }
            }
        }
    }

    protected void SetEnableAllLayout(bool isEnable)
    {
        if (isEnable)
        {
            foreach (Transform child in ScrollRect.content.transform)
            {
                child.gameObject.SetActive(true);
            }
        }

        SetEnableLayoutComponent(isEnable);
    }

    protected void SetEnableLayoutComponent(bool isEnable)
    {
        RectTransform content = ScrollRect.content;

        // Layout Component
        if (content.GetComponent<ContentSizeFitter>() != null)
        {
            content.GetComponent<ContentSizeFitter>().enabled = isEnable;
        }

        // All Layout Group
        LayoutGroup[] layoutGroups = content.GetComponents<LayoutGroup>();
        if (layoutGroups != null && layoutGroups.Length > 0)
        {
            foreach (LayoutGroup layoutGroup in layoutGroups)
            {
                layoutGroup.enabled = isEnable;
            }
        }
    }

    private void UpdateLayout()
    {
        SetEnableAllLayout(true);

        LayoutRebuilder.ForceRebuildLayoutImmediate(ScrollRect.content);
    }
    #endregion

    #region Refresh
    public IEnumerator RefreshInboxListInterval()
    {
        bool isFinish = false;
        DataManager.Instance.LoadAllMessageInbox(
            delegate ()
            {
                if (this == null || this.gameObject == null)
                {
                    return;
                }

                isFinish = true;
                if (NavigatorController.Instance.CurrentPage.SceneName == GameHelper.HomeSceneName)
                {
                    if (!_isInit)
                    {
                        //InitData();
                    }
                    else
                    {
                        RefreshMessage();
                    }
                }

            }
            , delegate (string error) { isFinish = true; }
        );
        yield return new WaitUntil(() => isFinish == true);
    }
    #endregion
}