﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;
using DG.Tweening;

public class MessageInboxPopup : MonoBehaviour
{
    #region Inspector Properties
    [Header("MainWindow")]
    [SerializeField] private GameObject MainWindow;

    [Header("TEXT")]
    [SerializeField] private TextMeshProUGUI TEXT_Title;
    [SerializeField] private TextMeshProUGUI TEXT_Description;
    [SerializeField] private TextMeshProUGUI TEXT_RemainTime;

    [Header("BTN")]
    [SerializeField] private Button BTN_Claim;
    [SerializeField] private Button BTN_Close;

    [Header("Prefab Reward")]
    [SerializeField] private RewardCell Prefab_RewardCell;

    [Header("Reward Container")]
    [SerializeField] private Transform RewardContainer;
    #endregion

    #region Private Properties
    private static readonly string TimeLeftFormat_Day = "TEXT_INBOX_TIME_LEFT_DAY";
    private static readonly string TimeLeftFormat_Hour = "TEXT_INBOX_TIME_LEFT_HOUR";
    private static readonly string TimeLeftFormat_Min = "TEXT_INBOX_TIME_LEFT_MIN";
    private static readonly string TimeLeftFormat_Sec = "TEXT_INBOX_TIME_LEFT_SEC";
    private static readonly string TimeLeftFormat_Expired = "TEXT_INBOX_TIME_LEFT_EXPIRED";

    private MessageInboxUIData _data = null;
    private UnityAction _onClick;
    private UnityAction _onClose;

    private List<RewardCell> _listRewardCell = new List<RewardCell>();
    #endregion

    #region Methods
    public void ShowUI(MessageInboxUIData data, UnityAction onClick, UnityAction onClose)
    {
        _data = data;
        _onClick = onClick;
        _onClose = onClose;

        DataManager.Instance.ReadMessageInbox(_data.ID, null, null);

        SetBTN();
        UpdateText();
        UpdateReward();

        GameHelper.UITransition_FadeIn(this.gameObject);
    }

    public void HideUI()
    {
        GameHelper.UITransition_FadeOut(this.gameObject);
    }

    private void SetBTN()
    {
        if (_data.ItemDataList.Count > 0 && !_data.IsClaimed)
        {
            BTN_Claim.interactable = true;
        }
        else
        {
            BTN_Claim.interactable = false;
        }

        BTN_Claim.onClick.RemoveAllListeners();
        BTN_Claim.onClick.AddListener(delegate
        {
            if (_onClick != null)
            {
                _onClick.Invoke();
            }
        });

        BTN_Close.onClick.RemoveAllListeners();
        BTN_Close.onClick.AddListener(delegate
        {
            if (_onClose != null)
            {
                _onClose.Invoke();
            }
        });
    }

    private void UpdateText()
    {
        TEXT_Title.text = _data.Title;
        TEXT_Description.text = _data.Message;

        // Calculate remaining time
        DateTime serverTime = DateTimeData.GetDateTimeUTC();
        TimeSpan span = (_data.ExpiredDateTimeUTC - serverTime);

        if (span.Milliseconds > 0)
        {
            if (span.Days >= 365 * 10) // 10 years is permanent message.
            {
                TEXT_RemainTime.text = "";
            }
            else if(span.Days > 0)
            {
                TEXT_RemainTime.text = string.Format(LocalizationManager.Instance.GetText(TimeLeftFormat_Day), span.Days);
            }
            else if (span.Hours > 0)
            {
                TEXT_RemainTime.text = string.Format(LocalizationManager.Instance.GetText(TimeLeftFormat_Hour), span.Hours);
            }
            else if (span.Minutes > 0)
            {
                TEXT_RemainTime.text = string.Format(LocalizationManager.Instance.GetText(TimeLeftFormat_Min), span.Minutes);
            }
            else
            {
                TEXT_RemainTime.text = string.Format(LocalizationManager.Instance.GetText(TimeLeftFormat_Sec), span.Seconds);
            }
        }
        else
        {
            TEXT_RemainTime.text = LocalizationManager.Instance.GetText(TimeLeftFormat_Expired);
        }
    }

    private void ClearReward()
    {
        foreach (RewardCell item in _listRewardCell)
        {
            item.gameObject.SetActive(false);
        }
    }

    private void CreateReward(string rewardKey, int amount)
    {
        GameObject obj = Instantiate(Prefab_RewardCell.gameObject, RewardContainer);
        RewardCell cell = obj.GetComponent<RewardCell>();
        cell.InitData(rewardKey, amount);

        _listRewardCell.Add(cell);
    }

    private void UpdateReward()
    {
        ClearReward();

        // Generate Reward Cell
        for (int i = 0; i < _data.ItemDataList.Count; i++)
        {
            ItemData reward = _data.ItemDataList[i];
            if (i < _listRewardCell.Count)
            {
                _listRewardCell[i].InitData(reward.ItemID, reward.Amount);
                _listRewardCell[i].gameObject.SetActive(true);
            }
            else
            {
                CreateReward(reward.ItemID, reward.Amount);
            }
        }
    }
    #endregion
}
