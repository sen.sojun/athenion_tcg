﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;

public class MessageInboxUIData
{
    #region Public Properties
    public string ID { get; private set; }
    public string Title { get { return GetTitle(LocalizationManager.Instance.CurrentLanguage); } }
    public string Message { get { return GetMessage(LocalizationManager.Instance.CurrentLanguage); } }

    public Dictionary<string, string> TitleData { get; private set; }
    public Dictionary<string, string> MessageData { get; private set; }

    public List<ItemData> ItemDataList { get; private set; }
    public bool IsRead { get; private set; }
    public bool IsClaimed { get; private set; }
    public bool IsDeleted { get; private set; }
    public DateTime CreatedDateTimeUTC { get; private set; }
    public DateTime ReceivedDateTimeUTC { get; private set; }
    public DateTime ExpiredDateTimeUTC { get; private set; }

    public MessageInboxData Data { get { return _data; } }
    #endregion

    #region Private Properties
    private MessageInboxData _data;
    #endregion

    #region Constructors
    public MessageInboxUIData(MessageInboxData data)
    {
        _data = data;
        ID = data.ID;
        TitleData = new Dictionary<string, string>(data.TitleData);
        MessageData = new Dictionary<string, string>(data.MessageData);

        IsRead = data.IsRead;
        IsClaimed = data.IsClaimed;
        IsDeleted = data.IsDeleted;

        ItemDataList = new List<ItemData>(data.ItemDataList);

        CreatedDateTimeUTC = data.CreatedDateTimeUTC;
        ReceivedDateTimeUTC = data.ReceivedDateTimeUTC;
        ExpiredDateTimeUTC = data.ExpiredDateTimeUTC;
    }

    public MessageInboxUIData(MessageInboxUIData data)
    {
        ID = data.ID;
        TitleData = new Dictionary<string, string>(data.TitleData);
        MessageData = new Dictionary<string, string>(data.MessageData);

        IsRead = data.IsRead;
        IsClaimed = data.IsClaimed;
        IsDeleted = data.IsDeleted;

        ItemDataList = new List<ItemData>(data.ItemDataList);

        CreatedDateTimeUTC = data.CreatedDateTimeUTC;
        ReceivedDateTimeUTC = data.ReceivedDateTimeUTC;
        ExpiredDateTimeUTC = data.ExpiredDateTimeUTC;
    }
    #endregion

    #region Methods
    private string GetTitle(Language language)
    {
        string lang = language.ToString();
        if (TitleData.ContainsKey(lang))
        {
            return TitleData[lang];
        }

        return "";
    }

    private string GetMessage(Language language)
    {
        string lang = language.ToString();
        if (MessageData.ContainsKey(lang))
        {
            return MessageData[lang];
        }

        return "";
    }

    public bool IsCanClaim()
    {
        if (ItemDataList != null && ItemDataList.Count > 0 && !IsClaimed)
        {
            return true;
        }

        return false;
    }
    #endregion
}

public class MessageInboxCell : MonoBehaviour
{
    #region Public Properties
    [Header("ICON")]
    [SerializeField] private Image ICON_Unread;
    [SerializeField] private Image ICON_Read;
    [SerializeField] private Image ICON_CanClaim;

    [Header("TEXT")]
    [SerializeField] private TextMeshProUGUI TEXT_Title;
    [SerializeField] private TextMeshProUGUI TEXT_Description;
    [SerializeField] private TextMeshProUGUI TEXT_RemainTime;

    [Header("BTN")]
    [SerializeField] private Button BTN_Open;
    [SerializeField] private Button BTN_Delete;
    #endregion

    #region Private Properties
    private static readonly string TimeLeftFormat_Day = "TEXT_INBOX_TIME_LEFT_DAY";
    private static readonly string TimeLeftFormat_Hour = "TEXT_INBOX_TIME_LEFT_HOUR";
    private static readonly string TimeLeftFormat_Min = "TEXT_INBOX_TIME_LEFT_MIN";
    private static readonly string TimeLeftFormat_Sec = "TEXT_INBOX_TIME_LEFT_SEC";
    private static readonly string TimeLeftFormat_Expired = "TEXT_INBOX_TIME_LEFT_EXPIRED";

    private MessageInboxUIData _data;
    private UnityAction _onClick;
    private UnityAction _onDelete;
    #endregion

    public void InitData(MessageInboxUIData data, UnityAction onClick, UnityAction onDelete)
    {
        _data = data;
        _onClick = onClick;
        _onDelete = onDelete;

        SetBTN();
        UpdateText();
        UpdateStatus();
    }

    #region Methods
    private void SetBTN()
    {
        BTN_Open.onClick.RemoveAllListeners();
        BTN_Open.onClick.AddListener(_onClick);

        BTN_Delete.onClick.RemoveAllListeners();
        BTN_Delete.onClick.AddListener(_onDelete);
    }

    private void UpdateText()
    {
        TEXT_Title.text = _data.Title;
        TEXT_Description.text = _data.Message;

        // Calculate remaining time
        DateTime serverTime = DateTimeData.GetDateTimeUTC();
        TimeSpan span = (_data.ExpiredDateTimeUTC - serverTime);

        if (span.Milliseconds > 0)
        {
            if(span.Days >= 365 * 10) // 10 years is permanent message.
            {
                TEXT_RemainTime.text = "";
            }
            else if (span.Days > 0)
            {
                TEXT_RemainTime.text = string.Format(LocalizationManager.Instance.GetText(TimeLeftFormat_Day), span.Days);
            }
            else if (span.Hours > 0)
            {
                TEXT_RemainTime.text = string.Format(LocalizationManager.Instance.GetText(TimeLeftFormat_Hour), span.Hours);
            }
            else if (span.Minutes > 0)
            {
                TEXT_RemainTime.text = string.Format(LocalizationManager.Instance.GetText(TimeLeftFormat_Min), span.Minutes);
            }
            else
            {
                TEXT_RemainTime.text = string.Format(LocalizationManager.Instance.GetText(TimeLeftFormat_Sec), span.Seconds);
            }
        }
        else
        {
            TEXT_RemainTime.text = LocalizationManager.Instance.GetText(TimeLeftFormat_Expired);
        }
    }

    private void UpdateStatus()
    {
        // Read Status
        bool isRead = (_data.IsRead || _data.IsClaimed);
        ICON_Unread.gameObject.SetActive(!isRead);
        ICON_Read.gameObject.SetActive(isRead);

        // Claim Status
        bool isCanClaim = false;
        if (_data.ItemDataList != null && _data.ItemDataList.Count > 0)
        {
            if (!_data.IsClaimed)
            {
                isCanClaim = true;
            }
        }
        ICON_CanClaim.gameObject.SetActive(isCanClaim);
    }
    #endregion
}
