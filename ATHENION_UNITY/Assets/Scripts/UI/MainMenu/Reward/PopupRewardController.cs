﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;
using UnityEngine.UI;

public class PopupRewardController : MonoBehaviour
{
    #region Public Properties
    public Button BTN_Confirm;

    [Header("UI")]
    public TextMeshProUGUI Title;
    public TextMeshProUGUI Detail;
    public Image ImageDetail;
    #endregion

    #region Methods
    private void Start()
    {
        //gameObject.SetActive(false);
    }

    public void ShowPopup(string rewardDetail, UnityAction onComplete)
    {
        gameObject.SetActive(true);
        SetData(rewardDetail);

        BTN_Confirm.onClick.RemoveAllListeners();
        BTN_Confirm.onClick.AddListener(delegate {
            gameObject.SetActive(false);
            if (onComplete != null)
            {
                onComplete.Invoke();
            }
            });
    }

    private void SetData(string detail)
    {
        Detail.text = detail;
    }
    #endregion

}
