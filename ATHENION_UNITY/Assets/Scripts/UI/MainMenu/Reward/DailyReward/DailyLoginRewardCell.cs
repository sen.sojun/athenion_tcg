﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using Karamucho;
using DG.Tweening;

public class DailyLoginRewardCell : MonoBehaviour
{
    [SerializeField]
    private GameObject _next;
    [SerializeField]
    private TextMeshProUGUI _DayText;

    public Image Image;
    public GameObject ClaimSymbol;
    public GameObject Highlight;
    public TextMeshProUGUI Text;
    public TooltipAttachment Tooltip;

    private DailyLoginRewardItem _data;
    public void SetData(DailyLoginRewardItem data)
    {
        if (this._data != data)
        {
            // Set Data
            this._data = data;
            SetImage(data);
        }
        SetText(data);
    }

    private void SetImage(DailyLoginRewardItem data)
    {
        string key = data.itemID.ToUpper();
        Sprite sprite = SpriteResourceHelper.LoadSprite(key, SpriteResourceHelper.SpriteType.ICON_Item);
        Image.sprite = sprite;

        //Debug.Log(key);
        Tooltip.Setup(key);
    }

    private void SetText(DailyLoginRewardItem data)
    {
        _DayText.text = data.day.ToString();
        Text.text = data.amount.ToString();
    }

    public void SetHighlight(bool isCurrentDay)
    {
        if (Highlight == null) return;
        Highlight.SetActive(isCurrentDay);
    }

    public void SetIsClaimReward(bool isClaimReward)
    {
        if (ClaimSymbol == null) return;

        ClaimSymbol.SetActive(isClaimReward);
    }

    public string GetNameByItemID(string itemID)
    {
        if (GameHelper.IsVirtualCurrency(itemID))
        {
            string virtualCurrencyText = itemID.Split('_')[1];
            VirtualCurrency currencyType;
            if (GameHelper.StrToEnum(virtualCurrencyText, out currencyType))
            {
                return GameHelper.GetLocalizeCurrencyText(currencyType);
            }
            else
            {
                return "";
            }
        }
        else
        {
            return LocalizationManager.Instance.GetItemName(itemID);
        }
    }

    public IEnumerator AnimateClaimReward()
    {
        ClaimSymbol.gameObject.SetActive(true);
        ClaimSymbol.transform.localScale = Vector3.zero;

        bool isFinish = false;
        ClaimSymbol.transform.DOScale(1, 0.25f).SetEase(Ease.OutBack).OnComplete(() => isFinish = true);
        yield return new WaitUntil(() => isFinish);
    }

    public void SetActiveNext(bool active)
    {
        _next.SetActive(active);
    }
}
