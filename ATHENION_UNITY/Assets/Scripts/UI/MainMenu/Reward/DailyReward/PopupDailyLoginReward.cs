﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class PopupDailyLoginReward : MonoBehaviour
{
    [SerializeField]
    private Button _confirmButton;
    [SerializeField]
    private TextMeshProUGUI _dayText;
    [SerializeField]
    private List<DailyLoginRewardCell> rewardUIList = new List<DailyLoginRewardCell>();

    private UnityAction _onClick;
    private DailyLoginRewardData _data;

    private Coroutine animTask;
    private bool isAnimateFinish;

    public void SetData(DailyLoginRewardData data)
    {
        _data = data;
        for (int i = 0; i < data.rewardTable.Length; i++)
        {
            rewardUIList[i].SetData(data.rewardTable[i]);

            bool isClaimReward = i < data.rewardIndex;
            rewardUIList[i].SetIsClaimReward(isClaimReward);

            rewardUIList[i].SetActiveNext(false);
            rewardUIList[i].SetHighlight(false);
        }
        rewardUIList[data.rewardIndex].SetHighlight(true);
        _dayText.text = data.rewardItem.day.ToString();
    }

    public void ShowUI(UnityAction onClick)
    {
        _confirmButton.onClick.AddListener(OnClickConfirmButton);

        _onClick = onClick;
        GameHelper.UITransition_FadeIn(this.gameObject);
    }

    public void HideUI()
    {
        isAnimateFinish = true;
        _confirmButton.onClick.RemoveAllListeners();

        GameHelper.UITransition_FadeOut(this.gameObject);
    }

    public void SetIsClaimReward(bool isClaim)
    {
        rewardUIList[_data.rewardIndex + 1].SetActiveNext(isClaim);
        rewardUIList[_data.rewardIndex + 1].SetHighlight(isClaim);

        rewardUIList[_data.rewardIndex].SetHighlight(!isClaim);
        rewardUIList[_data.rewardIndex].SetIsClaimReward(isClaim);
    }

    private void OnClickConfirmButton()
    {
        if (isAnimateFinish)
        {
            if (_onClick != null)
                _onClick.Invoke();
        }
        else if (isAnimateFinish == false && animTask == null)
        {
            animTask = StartCoroutine(ExcecuteAnimation());
        }
    }

    IEnumerator ExcecuteAnimation()
    {
        SoundManager.PlaySFX(SoundManager.SFX.Coin_Flick);
        rewardUIList[_data.rewardIndex + 1].SetActiveNext(true);
        rewardUIList[_data.rewardIndex + 1].SetHighlight(true);
        rewardUIList[_data.rewardIndex].SetHighlight(false);
        yield return rewardUIList[_data.rewardIndex].AnimateClaimReward();
        yield return new WaitForSeconds(0.2f);
        if (_onClick != null)
            _onClick.Invoke();
        isAnimateFinish = true;
    }


}
