﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialLoginData : DailyLoginData
{
    public bool IsActionValid { get; private set; }

    public SpecialLoginData(SpecialLoginRewardData rewardData, string json) : base(rewardData, json)
    {
        IsActionValid = true;
        IsActionValid &= rewardData.isNewDay;
        IsActionValid &= TotalDays <= rewardData.DayLimit;
    }
}

[SerializeField]
public class SpecialLoginRewardData : DailyLoginRewardData
{
    public int DayLimit;
}