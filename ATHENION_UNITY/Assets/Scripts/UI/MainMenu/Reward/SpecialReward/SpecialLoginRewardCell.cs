﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Experimental.UIElements;

public class SpecialLoginRewardCell : MonoBehaviour
{
    #region Inspector Properties

    [SerializeField] private Image _RewardIcon;
    [SerializeField] private Image _ClaimIcon;
    [SerializeField] private Image _HighlightIcon;
    [SerializeField] private TextMeshProUGUI _DayText;

    #endregion

    #region Private Properties

    private DailyLoginRewardItem _data;

    #endregion

    #region Methods

    public void SetData(DailyLoginRewardItem data)
    {
        
    }

    #endregion
}
