﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class SpecialLoginController : MonoBehaviour
{
    [SerializeField] private Button _ConfirmButton;
    [SerializeField] private List<DailyLoginRewardCell> _RewardUIList;

    private UnityAction _onClick;
    private SpecialLoginRewardData _data;

    private Coroutine animTask;
    private bool isAnimateFinish;

    public void SetData(SpecialLoginRewardData data)
    {
        _data = data;
        for (int i = 0; i < data.rewardTable.Length; i++)
        {
            _RewardUIList[i].SetData(data.rewardTable[i]);

            bool isClaimReward = i < data.rewardIndex;
            _RewardUIList[i].SetIsClaimReward(isClaimReward);

            _RewardUIList[i].SetActiveNext(false);
            //_RewardUIList[i].SetHighlight(false);
        }
        //_RewardUIList[data.rewardIndex].SetHighlight(true);
    }

    public void ShowUI(UnityAction onClick)
    {
        _ConfirmButton.onClick.AddListener(OnClickConfirmButton);

        _onClick = onClick;
        GameHelper.UITransition_FadeIn(this.gameObject);
    }

    public void HideUI()
    {
        isAnimateFinish = true;
        _ConfirmButton.onClick.RemoveAllListeners();

        GameHelper.UITransition_FadeOut(this.gameObject);
    }

    public void SetIsClaimReward(bool isClaim)
    {
        _RewardUIList[_data.rewardIndex + 1].SetActiveNext(isClaim);
        //_RewardUIList[_data.rewardIndex + 1].SetHighlight(isClaim);

        //_RewardUIList[_data.rewardIndex].SetHighlight(!isClaim);
        _RewardUIList[_data.rewardIndex].SetIsClaimReward(isClaim);
    }

    private void OnClickConfirmButton()
    {
        if (isAnimateFinish)
        {
            if (_onClick != null)
                _onClick.Invoke();
        }
        else if (isAnimateFinish == false && animTask == null)
        {
            animTask = StartCoroutine(ExecuteAnimation());
        }
    }

    IEnumerator ExecuteAnimation()
    {
        SoundManager.PlaySFX(SoundManager.SFX.Coin_Flick);
        //_RewardUIList[_data.rewardIndex + 1].SetActiveNext(true);
        //_RewardUIList[_data.rewardIndex + 1].SetHighlight(true);
        //_RewardUIList[_data.rewardIndex].SetHighlight(false);
        yield return _RewardUIList[_data.rewardIndex].AnimateClaimReward();
        yield return new WaitForSeconds(0.2f);
        if (_onClick != null)
            _onClick.Invoke();
        isAnimateFinish = true;
    }
}

