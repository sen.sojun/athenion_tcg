﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;


[RequireComponent(typeof(Button))]
public class DeckDefaultSelectButton : MonoBehaviour 
{
    public TextMeshProUGUI ButtonText;

    private int _index;
    private UnityAction<int> _onClick;
    
    private void OnEnable()
    {
        Button button = GetComponent<Button>();
        button.onClick.AddListener(OnClickSelect);
    }

    private void OnDisable()
    {
        Button button = GetComponent<Button>();
        button.onClick.RemoveListener(OnClickSelect);
    }

    public void Setup(int index, string text, UnityAction<int> onClick)
    {
        _index = index;
        if (ButtonText != null)
        {
            ButtonText.text = text;
        }

        _onClick = onClick;
    }

    private void OnClickSelect()
    {
        if (_onClick != null)
        {
            _onClick.Invoke(_index);
        }
    }
}
