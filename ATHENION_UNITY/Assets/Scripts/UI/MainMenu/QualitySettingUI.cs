﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class QualitySettingUI : MonoBehaviour 
{
    public TextMeshProUGUI CurrentFPSText;
    public TextMeshProUGUI SliderValueText;
    public TextMeshProUGUI CurrentQualityText;

    public Button FPS30Button;
    public Button FPS60Button;
    public Slider QualitySlider;
    public Button SubmitQualityButton;

    /*
    // Use this for initialization
    void Start () 
    {		
	}
    */
	
    /*
	// Update is called once per frame
	void Update () 
    {		
	}
    */

    public void ShowUI()
    {
        QualitySlider.onValueChanged.RemoveAllListeners();
        QualitySlider.onValueChanged.AddListener(OnSliderValueChanged);

        UpdateFPSText();
        UpdateQualityText();

        QualitySlider.value = QualitySlider.maxValue * DataManager.Instance.GetScreenRatio();

        gameObject.SetActive(true);
    }

    public void HideUI()
    {
        gameObject.SetActive(false);
    }

    public void OnSliderValueChanged(float value)
    {
        float percent = (value / QualitySlider.maxValue);

        SliderValueText.text = string.Format("{0}%", percent * 100.0f);
    }

    public void SubmitQuality()
    {
        float percent = (QualitySlider.value / QualitySlider.maxValue);

        SetScreenRatio(percent);
    }

    public void SetFPS(int fps)
    {
        DataManager.Instance.SetFPSSetting(fps);
        Application.targetFrameRate = fps;

        UpdateFPSText();
    }

    private void SetScreenRatio(float percent)
    {
        ScreenManager.SetScreenSize(percent);
        UpdateQualityText();
    }

    private void UpdateFPSText()
    {
        CurrentFPSText.text = string.Format("Current : {0} FPS", DataManager.Instance.GetFPSSetting());
    }

    private void UpdateQualityText()
    {
        CurrentQualityText.text = string.Format("{0}%", DataManager.Instance.GetScreenRatio() * 100.0f);
        SliderValueText.text = string.Format("{0}%", DataManager.Instance.GetScreenRatio() * 100.0f);
    }
}
