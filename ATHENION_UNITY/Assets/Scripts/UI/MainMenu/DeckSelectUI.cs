﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;
using Karamucho;
using Karamucho.UI;
using System;
using DG.Tweening;
using System.Linq;

public class DeckSelectUI : MonoBehaviour
{

    #region Public Properties
    [Header("Ref")]
    public Button BTN_Play;
    public Image Icon_Faction;
    public GameObject GROUP_BotLevel;
    public Transform GROUP_Character;
    public GameObject GROUP_FactionLevel;

    [Header("SIDE")]
    public GameObject GROUP_Rank;
    public FriendMatchStatus FriendStatus;
    public GameObject Group_WeeklyRankReward;
    [Obsolete]public GameObject GROUP_Leader;

    [Header("BTN")]
    [Obsolete]
    public Button BTN_Chat;

    [Header("TOGGLE")]
    public GameObject ToggleGroup;
    public GameObject GROUP_BOTMODE;
    public Button BTN_Casual;
    public Button BTN_Rank;
    public GameObject Glow_Casual;
    public GameObject Glow_Rank;

    [Header("TEXT")]
    public TextMeshProUGUI TEXT_Rank;
    public GameObject Group_SeasonDate;
    public TextMeshProUGUI TEXT_SeasonStatus;
    public TextMeshProUGUI TEXT_Mode;

    [Header("Season End")]
    public GameObject GROUP_SeasonEnd;
    public TextMeshProUGUI TEXT_SeasonEnded;

    [Header("Faction Level")]
    public TextMeshProUGUI TEXT_LevelFaction;
    public Slider SLIDER_FactionLevel;
    public GameObject GROUP_FactionReward;
    public Image IMG_Reward;
    public TextMeshProUGUI TEXT_RewardCount;
    public TooltipAttachment Reward_Tooltip;
    public GameObject Item_Max;
    public GameObject TIP_FactionLevelFirstTime;

    [Header("Faction Flag")]
    public Sprite Flag_Fire;
    public Sprite Flag_Water;
    public Sprite Flag_Air;
    public Sprite Flag_Earth;
    public Sprite Flag_Holy;
    public Sprite Flag_Dark;
    public Sprite Flag_Neutral;

    [Header("Faction Effect")]
    public GameObject E_Fire;
    public GameObject E_Water;
    public GameObject E_Air;
    public GameObject E_Earth;
    public GameObject E_Holy;
    public GameObject E_Dark;
    public GameObject E_Neutral;
    #endregion

    #region Private Properties
    private GameMode _currentMode;

    [Header("Private Ref")]
    [SerializeField] private GameObject buttonPrefab;
    [SerializeField] private Button _exitBTN;

    [SerializeField]
    private GameObject _container;

    private List<DeckSelectButton> _buttonList = new List<DeckSelectButton>();

    private UnityAction<int> _onSelectDeck = null;
    private UnityAction<GameMode> _onPlay = null;

    private int _currentDeck = -1;
    private List<DeckData> _deckList = new List<DeckData>();

    private UnityAction<string, BotLevel> _onSelectBot = null;
    private string _heroID;
    private BotLevel _botLevel = BotLevel.Easy;
    private List<string> _botHeroIDList = new List<string>();
    private bool _isSelectingBot = false;
    private GameObject _character;

    #endregion

    #region Methods
    public void ShowUI(GameMode gameMode, List<string> elementFilterList = null)
    {
        bool isEnable = true;

        switch (gameMode)
        {
            case GameMode.Casual:   isEnable = DataManager.Instance.IsFeatureEnable(FeatureUnlock.CasualMode); break;
            case GameMode.Rank:     isEnable = DataManager.Instance.IsFeatureEnable(FeatureUnlock.RankMode); break;
            case GameMode.Friendly: isEnable = DataManager.Instance.IsFeatureEnable(FeatureUnlock.FriendMode); break;
            case GameMode.Practice: isEnable = DataManager.Instance.IsFeatureEnable(FeatureUnlock.PracticeMode); break;
            case GameMode.Event:    isEnable = DataManager.Instance.IsFeatureEnable(FeatureUnlock.EventBattleMode); break;
        }

        SetModeText(gameMode);
        GROUP_BOTMODE.gameObject.SetActive(gameMode == GameMode.Practice);

        // Rank Season Ended
        if (gameMode == GameMode.Rank && DataManager.Instance.CanPlayRankMode == false)
        {
            GROUP_SeasonEnd.gameObject.SetActive(true);
            BTN_Play.gameObject.SetActive(false);
        }
        else
        {
            GROUP_SeasonEnd.gameObject.SetActive(false);
            BTN_Play.gameObject.SetActive(true);
        }
       
        TEXT_SeasonEnded.text = string.Format(LocalizationManager.Instance.GetText("TEXT_SEASON_ENDED"),DataManager.Instance.GetCurrentSeasonIndex());

        if (isEnable)
        {
            if (elementFilterList == null)
            {
                elementFilterList = new List<string>(); //If input is null -> add all element to list to show all decks
                foreach(CardElementType type in Enum.GetValues(typeof(CardElementType)))
                {
                    elementFilterList.Add(type.ToString());
                }
            }

            List<DeckData> allDeckList = DataManager.Instance.GetAllDeck();
            if(allDeckList.Exists(deck => elementFilterList.Contains(deck.ElementID)))
            {
                ShowUI(
                      gameMode
                    , allDeckList
                    , DataManager.Instance.GetRecentDeckSelection()
                    , elementFilterList
                );
            }
            else
            {
                string title = LocalizationManager.Instance.GetText("ERROR_FIND_DECK_NOT_FOUND");
                string message = LocalizationManager.Instance.GetText("");
                PopupUIManager.Instance.ShowPopup_ConfirmOnlyWithWarning(title, message, () => HomeManager.Instance.HideSelectDeck());
            }
        }
        else
        {
            HomeManager.Instance.HideSelectDeck();
        }
    }

    private void SetModeText(GameMode gameMode)
    {
        string mode = "";
        switch (gameMode)
        {
            case GameMode.Casual: mode = LocalizationManager.Instance.GetText("MENU_MATCHING_MODE_CASUAL"); break;
            case GameMode.Rank: mode = LocalizationManager.Instance.GetText("MENU_MATCHING_MODE_RANK"); break;
            case GameMode.Friendly: mode = LocalizationManager.Instance.GetText("MENU_MATCHING_MODE_FRIENDLY"); break;
            case GameMode.Practice: mode = LocalizationManager.Instance.GetText("MENU_MATCHING_MODE_PRACTICE"); break;
            case GameMode.Event: mode = LocalizationManager.Instance.GetText("MENU_MATCHING_MODE_EVENT"); break;
        }
        TEXT_Mode.text = mode;
    }

    public void SetEvent(UnityAction<GameMode> onPlay, UnityAction<int> onSelectDeck)
    {
        _onPlay = onPlay;
        _onSelectDeck = onSelectDeck;
    }

    //Must be called from public void ShowUI(GameMode, List<string>) or the filter logic might bug out
    private void ShowUI(GameMode gameMode, List<DeckData> deckList, string recentDeckID, List<string> elementFilterList)
    {
        gameObject.SetActive(true);
        ClearButton();

        _isSelectingBot = false;
        _currentMode = gameMode;

        SetActiveTogglePanel(gameMode);

        //_onSelectDeck = onSelectDeck;
        _onSelectBot = null;

        int selectedIndex = 0;

        WeeklyRankRewardData weeklyRankRewardData = DataManager.Instance.GetWeeklyRankRewardData();
        Group_WeeklyRankReward.gameObject.SetActive(_currentMode == GameMode.Rank && DataManager.Instance.PlayerInfo.GetPlayerCurrentRank() >= weeklyRankRewardData.GetMinimumRankForClaimingWeeklyReward());
        Group_SeasonDate.gameObject.SetActive(_currentMode == GameMode.Rank);
        GROUP_Leader.gameObject.SetActive(_currentMode == GameMode.Rank);
        GROUP_FactionLevel.gameObject.SetActive(_currentMode == GameMode.Rank || _currentMode == GameMode.Casual);
        ShowFactionLevelFirstTime(_currentMode);
 
        // Generate Deck
        #region Generate Deck
        _buttonList = new List<DeckSelectButton>();
        _deckList = new List<DeckData>(deckList);
        if(_deckList.Count > 0 && !_deckList.Any(deck => deck.DeckID == recentDeckID))
        {
            recentDeckID = _deckList[0].DeckID; //Special case where player delete recent deck in editor -> use first deck as default
        }
        bool recentDeckGotFiltered = false;
        Dictionary<int, bool> deckShowedStatus = new Dictionary<int, bool>();
        for (int index = 0; index < _deckList.Count; ++index)
        {
            int tempIndex = index;
            bool isValid = DataManager.Instance.IsLocalDeckValid(_deckList[index].DeckID);
            GameObject obj = Instantiate(buttonPrefab, _container.transform) as GameObject;
            DeckSelectButton button = obj.GetComponent<DeckSelectButton>();
            button.Setup(
                  index
                , _deckList[index].DeckName
                , _deckList[index].HeroID
                , isValid
                , delegate
                {
                    SelectedDeck(tempIndex);
                    ShowGlow();
                }
            );
            _buttonList.Add(button);

            //Set recent deck
            if (_deckList[index].DeckID == recentDeckID)
            {
                if (isValid)
                {
                    selectedIndex = index;
                }
                else
                {
                    BTN_Play.interactable = false;
                }
            }

            //Filter
            CardElementType deckElem = (CardElementType)Enum.Parse(typeof(CardElementType), _deckList[index].ElementID);
            if (elementFilterList.Contains(deckElem.ToString()))
            {
                deckShowedStatus.Add(index, true);
            }
            else
            {
                deckShowedStatus.Add(index, false);
                obj.SetActive(false);
                if (_deckList[index].DeckID == recentDeckID)
                {
                    recentDeckGotFiltered = true;
                }
            }
        }

        //Set recent deck after filter
        if (recentDeckGotFiltered && deckShowedStatus.Any(doShow => doShow.Value == true))
        {
            KeyValuePair<int, bool> firstFilteredDeckIndex = deckShowedStatus.Where(doShow => doShow.Value == true).First();
            recentDeckID = _deckList[firstFilteredDeckIndex.Key].DeckID;
            if (DataManager.Instance.IsLocalDeckValid(recentDeckID))
            {
                selectedIndex = firstFilteredDeckIndex.Key;
            }
            else
            {
                BTN_Play.interactable = false;
            }
        }
        else if (recentDeckGotFiltered)
        {
            BTN_Play.interactable = false;
        }
        #endregion

        InitBTN();

        //SetChatEnable();
        //SetChat(); // sent player id (Friendly)
        SetFriendStatus();
        SetRank();
        if(_buttonList.Count > 0) SelectedDeck(selectedIndex);
        ShowGlow();
        SetHeader();
        SetBotLevel();
    }
    
    private void SetActiveTogglePanel(GameMode gameMode)
    {
        if (gameMode == GameMode.Casual || gameMode == GameMode.Rank)
        {
            ToggleGroup.gameObject.SetActive(true);
        }
        else
        {
            ToggleGroup.gameObject.SetActive(false);
            return;
        }

        bool isCanPlayCasual = DataManager.Instance.IsFeatureEnable(FeatureUnlock.CasualMode);
        bool isCanPlayRank = DataManager.Instance.IsFeatureEnable(FeatureUnlock.RankMode);

        BTN_Casual.interactable = isCanPlayCasual;
        BTN_Rank.interactable = isCanPlayRank;
        Glow_Casual.SetActive(gameMode == GameMode.Casual && isCanPlayCasual);
        Glow_Rank.SetActive(gameMode == GameMode.Rank && isCanPlayRank);
    }

    private void InitBTN()
    {
        _exitBTN.onClick.RemoveAllListeners();
        _exitBTN.onClick.AddListener(() =>
        {
            HomeManager.Instance.HideSelectDeck();
        });

        BTN_Casual.onClick.RemoveAllListeners();
        BTN_Casual.onClick.AddListener(() =>
        {
            ShowUI(GameMode.Casual);
        });

        BTN_Rank.onClick.RemoveAllListeners();
        BTN_Rank.onClick.AddListener(() =>
        {
            ShowUI(GameMode.Rank);
        });

        BTN_Play.onClick.RemoveAllListeners();
        BTN_Play.onClick.AddListener(delegate
        {
            _onSelectDeck?.Invoke(_currentDeck);
            _onPlay?.Invoke(_currentMode);
        });
    }

    public void ShowSelectBotUI(UnityAction<string, BotLevel> onSelectBot)
    {
        gameObject.SetActive(true);
        ClearButton();

        _isSelectingBot = true;
        _currentMode = GameMode.Practice;

        _onSelectDeck = null;
        _onSelectBot = onSelectBot;

        _buttonList = new List<DeckSelectButton>();
        int selectedheroIndex = 0;

        List<BotProfileDBData> botProfileDBDataList;
        BotProfileDB.Instance.GetAllData(out botProfileDBDataList);

        _botHeroIDList = new List<string>();
        foreach (BotProfileDBData rawData in botProfileDBDataList)
        {
            if (!_botHeroIDList.Contains(rawData.HeroID))
            {
                _botHeroIDList.Add(rawData.HeroID);
            }
        }
        _deckList = null;

        // Generate Deck
        #region Generate Deck
        int index = 0;
        foreach (string botHeroID in _botHeroIDList)
        {
            int botIndex = index;
            string heroID = botHeroID;
            HeroData heroData = HeroData.CreateHero(heroID, PlayerIndex.One);

            GameObject obj = Instantiate(buttonPrefab, _container.transform) as GameObject;
            DeckSelectButton button = obj.GetComponent<DeckSelectButton>();
            button.Setup(
                  botIndex
                , heroData.GetName()
                , heroData.ID
                , true
                , delegate
                {
                    SelectedBot(botIndex);
                    ShowGlow();
                    BTN_Play.interactable = true;
                }
            );
            _buttonList.Add(button);
            ++index;
        }
        #endregion

        BTN_Play.onClick.RemoveAllListeners();
        BTN_Play.onClick.AddListener(delegate
        {
            OnSelectBot(_heroID, _botLevel);
        });

        SetRank();
        SelectedBot(selectedheroIndex);
        ShowGlow();
        SetHeader();
        SetBotLevel();
    }

    private void SetHeader()
    {
        //TEXT_Header.text = LocalizationManager.Instance.GetText(string.Format(
        //      "BUTTON_MENU_MATCHING_MODE_{0}"
        //    , _currentMode.ToString().ToUpper()
        //));
    }

    public void SetFriendStatus(bool isReady = false)
    {
        bool isFriendMode = _currentMode == GameMode.Friendly;

        FriendStatus.gameObject.SetActive(isFriendMode);
        if (isFriendMode)
        {
            FriendStatus.SetReady(isReady);
        }
    }

    #region Chat

    [Obsolete]
    public void SetChatEnable()
    {
        return;
        //bool isFriendMode = _currentMode == GameMode.Friendly;
        //BTN_Chat.gameObject.SetActive(isFriendMode);
    }

    [Obsolete]
    public void SetChat(string playerID = "")
    {
        return;
        //string friendID = playerID;
        //Debug.Log("SetChat: " + friendID);
        //BTN_Chat.onClick.RemoveAllListeners();
        //BTN_Chat.onClick.AddListener(delegate
        //{
        //    KTPlayManager.Instance.ShowChatWithPlayerID(playerID);
        //});
    }

    #endregion

    private void SetRank()
    {
        bool isRank = _currentMode == GameMode.Rank;

        GROUP_Rank.SetActive(isRank);
    }

    private void SetBotLevel()
    {
        GROUP_BotLevel.SetActive(_isSelectingBot);
    }

    private void ShowGlow()
    {
        if (_buttonList.Count > 0)
        {
            foreach (DeckSelectButton item in _buttonList)
            {
                if (item.Index == _currentDeck)
                {
                    item.OnSelected(true);
                }
                else
                {
                    item.OnSelected(false);
                }
            }
        }
    }

    public void SelectedDeck(int index)
    {
        if (_currentDeck == index) return;

        _currentDeck = index;

        if (_deckList.Count > 0)
        {
            string deckID = _deckList[_currentDeck].DeckID;

            _heroID = _deckList[_currentDeck].HeroID;
            DataManager.Instance.SetRecentDeckSelection(deckID);
            BTN_Play.interactable = DataManager.Instance.IsLocalDeckValid(deckID);
        }
        else
        {
            BTN_Play.interactable = false;
        }

        UpdateInfo();

    }

    public void SelectedDifficulty(int botLevelIndex)
    {
        SelectedDifficulty((BotLevel)botLevelIndex);
    }

    public void SelectedDifficulty(BotLevel botLevel)
    {
        _botLevel = botLevel;
        UpdateInfo();
    }

    public void SelectedBot(int index)
    {
        _currentDeck = index;
        _heroID = _botHeroIDList[_currentDeck];

        UpdateInfo();
    }

    public void UpdateInfo()
    {
        HeroData data = HeroData.CreateHero(_heroID, PlayerIndex.One);

        SetFaction(data.ElementType);
        string key = _buttonList[_currentDeck].HeroID;

        if (GROUP_Character.childCount > 0)
        {
            List<Transform> destroyList = new List<Transform>();
            foreach (Transform item in GROUP_Character.transform)
            {
                destroyList.Add(item);
            }
        }

        // Select Hero
        if (_character != null) Destroy(_character);

        GameObject prefab = null;
        CardResourceManager.LoadUIHero(key, out prefab);

        GameHelper.UITransition_FadeIn(GROUP_Character.gameObject);
        _character = Instantiate(prefab, GROUP_Character);
        _character.transform.localPosition = Vector3.zero;
        _character.transform.localEulerAngles = Vector3.zero;

    }

    public void SetFaction(CardElementType cardElementType)
    {
        switch (cardElementType)
        {
            case CardElementType.FIRE:
                {
                    Icon_Faction.sprite = Flag_Fire;
                }
                break;

            case CardElementType.WATER:
                {
                    Icon_Faction.sprite = Flag_Water;
                }
                break;

            case CardElementType.AIR:
                {
                    Icon_Faction.sprite = Flag_Air;
                }
                break;

            case CardElementType.EARTH:
                {
                    Icon_Faction.sprite = Flag_Earth;
                }
                break;

            case CardElementType.HOLY:
                {
                    Icon_Faction.sprite = Flag_Holy;
                }
                break;

            case CardElementType.DARK:
                {
                    Icon_Faction.sprite = Flag_Dark;
                }
                break;

            default:
                {
                    Icon_Faction.sprite = Flag_Neutral;
                }
                break;
        }

        E_Fire.SetActive(cardElementType == CardElementType.FIRE);
        E_Water.SetActive(cardElementType == CardElementType.WATER);
        E_Earth.SetActive(cardElementType == CardElementType.EARTH);
        E_Air.SetActive(cardElementType == CardElementType.AIR);
        E_Holy.SetActive(cardElementType == CardElementType.HOLY);
        E_Dark.SetActive(cardElementType == CardElementType.DARK);
        E_Neutral.SetActive(cardElementType == CardElementType.NEUTRAL);

        // Level Faction
        int level = 0;
        float value = 0;
        LevelFactionData levelFaction = DataManager.Instance.PlayerInfo.LevelFactionData.GetLevelFactionData(cardElementType);
        LevelFactionRewardDB.Instance.GetData(cardElementType.ToString(), out LevelFactionRewardDBData rewardData);
        if (levelFaction == null)
        {
            level = 1;
        }
        else
        {
            level = levelFaction.Level;
            value = (float)levelFaction.CurrentLevelAccExp / (float)levelFaction.NextLevelAccExp;
        }

        TEXT_LevelFaction.text = "Lv." + level.ToString();
        SLIDER_FactionLevel.DOValue(value, 0.2f);

        // Faction Reward
        List<ItemData> itemData = rewardData.GetRewardItemList(level+1);
        if (itemData != null && itemData.Count > 0)
        {
            ItemData item = itemData[0];
            IMG_Reward.sprite = SpriteResourceHelper.LoadSprite(item.ItemID, SpriteResourceHelper.SpriteType.ICON_Item);
            TEXT_RewardCount.text = item.Amount.ToString();
            Reward_Tooltip.Setup(item.ItemID);
            Reward_Tooltip.gameObject.SetActive(true);
            Item_Max.SetActive(false);
        }
        else
        {
            Reward_Tooltip.gameObject.SetActive(false);
            Item_Max.SetActive(true);
        }




    }

    public void HideUI()
    {
        ClearButton();

        gameObject.SetActive(false);
    }

    private void OnSelectBot(string heroID, BotLevel botLevel)
    {
        if (_onSelectBot != null)
        {
            _onSelectBot.Invoke(heroID, botLevel);
        }
    }

    private void ClearButton()
    {
        // destroy all child
        //for (int i = 0; i < _buttonList.Count; i++)
        //{
        //    DestroyImmediate(_buttonList[i].gameObject);
        //}
        foreach (Transform child in _container.transform)
        {
            Destroy(child.gameObject);
        }

        _buttonList.Clear();
    }

    private void ShowFactionLevelFirstTime(GameMode mode)
    {
        if (mode == GameMode.Casual || mode == GameMode.Rank)
        {
            if (DataManager.Instance.IsFeatureEnable(FeatureUnlock.FactionLevel))
            {
                int factionLvStat = DataManager.Instance.PlayerInfo.GetPlayerStat(StatKeys.FL_SEEN.ToString());
                if (factionLvStat == 0)
                {
                    TIP_FactionLevelFirstTime.gameObject.SetActive(true);
                    PlayerStatManager.Instance.CountFactionLevelSeenStat(null, null);
                }
                else
                {
                    TIP_FactionLevelFirstTime.gameObject.SetActive(false);
                }
            }
        }
    }
    #endregion
}
