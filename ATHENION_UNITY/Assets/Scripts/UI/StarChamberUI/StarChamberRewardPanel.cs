﻿using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarChamberRewardPanel : NoticePopup_Confirm
{
    #region Public Properties

    #endregion

    #region Serialize Properties
    [Header("Prefab")]
    [SerializeField] private RewardCell_StarChamber _PrefabRewardCell_Star;

    [Header("Ref")]
    [SerializeField] private Transform _Container;
    public GameObject _AnimatorGraphic_Box1;
    public GameObject _AnimatorGraphic_Box2;
    public GameObject _AnimatorGraphic_Box3;
    [SerializeField] private GameObject _PUI_Flare;

    #endregion

    #region Private Properties
    private const string _anim_progress = "progess";
    private const string _anim_open = "open";
    private const string _anim_openIdle = "openDone_idle";
    private const string _anim_close = "close";

    private SkeletonGraphic _box1;
    private SkeletonGraphic _box2;
    private SkeletonGraphic _box3;

    private List<RewardCell_StarChamber> _rewardCellList = new List<RewardCell_StarChamber>();
    #endregion

    #region Method
    private void Awake()
    {
        _box1 = _AnimatorGraphic_Box1.GetComponent<SkeletonGraphic>();
        if (_box1 == null)
        {
            PopupUIManager.Instance.ShowPopup_Error("Error", "Box1 is null");
        }

        _box2 = _AnimatorGraphic_Box2.GetComponent<SkeletonGraphic>();
        if (_box2 == null)
        {
            PopupUIManager.Instance.ShowPopup_Error("Error", "Box2 is null");
        }

        _box3 = _AnimatorGraphic_Box3.GetComponent<SkeletonGraphic>();
        if (_box3 == null)
        {
            PopupUIManager.Instance.ShowPopup_Error("Error", "Box3 is null");
        }

    }

    public void Setup(List<StarCubeItemData> itemList)
    {

        // Reset
        HideAll();
        ClearAllCell();

        // Set Base Properties
        string title = LocalizationManager.Instance.GetText("TEXT_OPEN_STARCUBE");
        string message = LocalizationManager.Instance.GetText("TEXT_OPEN_STARCUBE_DESCRIPTION");
        PopupUIData noticeData = new PopupUIData(title, message, PopupUITypes.Confirm);
        SetData(noticeData);

        // Create Reward
        foreach (StarCubeItemData item in itemList)
        {
            _rewardCellList.Add(CreateReward(item));
        }


    }

    public void SetAnimationBoxOpen(int boxIndex)
    {
        // Box Animation
        switch (boxIndex)
        {
            case 0:
            _AnimatorGraphic_Box1.gameObject.SetActive(true);
            PlayAnim_Open(_box1);
            break;

            case 1:
            _AnimatorGraphic_Box2.gameObject.SetActive(true);
            PlayAnim_Open(_box2);
            break;

            case 2:
            _AnimatorGraphic_Box3.gameObject.SetActive(true);
            PlayAnim_Open(_box3);
            break;
        }

        _PUI_Flare.gameObject.SetActive(true);
    }

    public override void Hide()
    {
        if (_box1.gameObject.activeSelf)
        {
            ResetAnimation(_box1);
        }
        if (_box2.gameObject.activeSelf)
        {
            ResetAnimation(_box2);
        }
        if (_box3.gameObject.activeSelf)
        {
            ResetAnimation(_box3);
        }
        base.Hide();
    }

    private void HideAll()
    {
        _AnimatorGraphic_Box1.gameObject.SetActive(false);
        _AnimatorGraphic_Box2.gameObject.SetActive(false);
        _AnimatorGraphic_Box3.gameObject.SetActive(false);
        _PUI_Flare.gameObject.SetActive(false);
    }
    #endregion

    #region Reward Generate
    private RewardCell_StarChamber CreateReward(StarCubeItemData itemData)
    {
        GameObject obj = Instantiate(_PrefabRewardCell_Star.gameObject, _Container);
        RewardCell_StarChamber cell = obj.GetComponent<RewardCell_StarChamber>();
        if (itemData.IsDuplicate)
        {
            cell.InitData(itemData.NewID, itemData.Amount);
        }
        else
        {
            cell.InitData(itemData.ItemID, itemData.Amount);
        }

        cell.SetStarCubeData(itemData);

        return cell;
    }

    private void ClearAllCell()
    {
        foreach (RewardCell_StarChamber item in _rewardCellList)
        {
            Destroy(item.gameObject);
        }

        _rewardCellList.Clear();
    }
    #endregion


    #region Animation
    private void PlayAnim_Progress(SkeletonGraphic target)
    {
        SetAnimation(target, _anim_progress, true);
    }

    private void PlayAnim_Open(SkeletonGraphic target)
    {
        SetAnimation(target, _anim_open, false);
    }

    private void PlayAnim_OpenDone(SkeletonGraphic target)
    {
        SetAnimation(target, _anim_openIdle, true);
    }

    private void PlayAnim_Close(SkeletonGraphic target)
    {
        SetAnimation(target, _anim_close, false);
    }

    private void SetAnimation(SkeletonGraphic target, string animName, bool loop)
    {
        target.AnimationState.SetAnimation(0, animName, loop);
    }

    private void ResetAnimation(SkeletonGraphic target)
    {
        PlayAnim_Close(target);
    }
    #endregion
}
