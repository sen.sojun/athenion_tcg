﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;
using Karamucho.Collection;

namespace Karamucho.UI
{
    public class StarChamberNewArrivalUI : MonoBehaviour
    {
        #region Static Properties
        private static readonly int _cardPerPage = 3;
        #endregion

        #region Inspector Properties
        [SerializeField] private TextMeshProUGUI _timerText;
        [SerializeField] private Button _nextPageButton;
        [SerializeField] private Button _prevPageButton;
        [SerializeField] private List<CollectionCardUI> _cardUIList;
        [SerializeField] private GameObject _Text_GotAll;
        #endregion

        #region Private Properties
        private bool _isInit = false;
        private List<ItemData> _cardList;
        private DateTime _endDate;
        private int _pageIndex = 0;
        private int _maxPage
        {
            get
            {
                return (_cardList.Count / _cardPerPage) - 1;
            }
        }

        private UnityAction<string> _onClickCard;

        private bool IsFirstPage { get { return (_pageIndex == 0); } }
        private bool IsLastPage
        {
            get
            {
                if (_cardList != null && _cardList.Count > 0)
                {
                    return (_pageIndex == ((_cardList.Count / _cardPerPage) - 1));
                }
                else
                {
                    return (_pageIndex == 0);
                }
            }
        }
        #endregion

        // Update is called once per frame
        void Update()
        {
            if (_isInit)
            {
                UpdateTimer();
            }
        }

        #region Methods
        public void SetupData(List<ItemData> cardList, DateTime endDate, UnityAction<string> onClickCard)
        {
            _cardList = new List<ItemData>(cardList);
            _endDate = endDate;

            _Text_GotAll.SetActive(cardList.Count == 0);

            _onClickCard = onClickCard;

            if (_nextPageButton != null)
            {
                _nextPageButton.onClick.RemoveListener(OnClickNextPage);
                _nextPageButton.onClick.AddListener(OnClickNextPage);
            }

            if (_prevPageButton != null)
            {
                _prevPageButton.onClick.RemoveListener(OnClickPrevPage);
                _prevPageButton.onClick.AddListener(OnClickPrevPage);
            }

            ShowPage(0);

            _isInit = true;
        }

        private void ShowPage(int pageIndex)
        {
            _pageIndex = pageIndex;

            CardData cardData;
            CardUIData cardUIData;
            for (int index = 0; index < _cardPerPage; ++index)
            {
                int cardIndex = index + (_pageIndex * _cardPerPage);
                if (cardIndex < _cardList.Count)
                {
                    cardData = CardData.CreateCard(_cardList[cardIndex].ItemID);
                    cardUIData = new CardUIData(index, cardData, DataManager.Instance.DefaultCardBackIDList[0]);

                    _cardUIList[index].SetData(cardUIData);
                    _cardUIList[index].SetCardAmount(_cardList[cardIndex].Amount);
                    _cardUIList[index].SetOnClickEvent(OnClickCard);

                    _cardUIList[index].gameObject.SetActive(true);
                }
                else
                {
                    _cardUIList[index].gameObject.SetActive(false);
                }
            }

            //Debug.Log("MAX: " + _maxPage);
            if (_maxPage > 0)
            {
                if (_nextPageButton != null)
                {
                    _nextPageButton.gameObject.SetActive(IsFirstPage);
                }

                if (_prevPageButton != null)
                {
                    _prevPageButton.gameObject.SetActive(IsLastPage);
                }
            }
            else
            {
                if (_nextPageButton != null)
                {
                    _nextPageButton.gameObject.SetActive(false);
                }

                if (_prevPageButton != null)
                {
                    _prevPageButton.gameObject.SetActive(false);
                }
            }



        }

        private void UpdateTimer()
        {
            TimeSpan duration = _endDate - DateTimeData.GetDateTimeUTC();
            if (duration.TotalMilliseconds >= 0)
            {
                if (duration.TotalDays >= 1.0)
                {
                    string timerFormat = LocalizationManager.Instance.GetText("TEXT_DAY_FORMAT");
                    string timerText = string.Format(
                        timerFormat
                        , duration.Days
                        , (duration.Days > 1 ? "s" : "")
                    );
                    _timerText.text = timerText;
                }
                else
                {
                    _timerText.text = duration.ToString(@"hh\:mm\:ss");
                }
            }
            else
            {
                _timerText.text = "00:00:00";
            }
        }
        #endregion

        #region Event Methods
        private void OnClickCard(CollectionCardUI card)
        {
            _onClickCard?.Invoke(card.Data.FullID);
        }

        private void OnClickNextPage()
        {
            _pageIndex++;

            int maxIndex = 0;
            if (_cardList != null && _cardList.Count > 0)
            {
                maxIndex = ((_cardList.Count / _cardPerPage) - 1);
            }
            else
            {
                maxIndex = 0;
            }

            _pageIndex = Mathf.Min(maxIndex, _pageIndex); // Clamp max.

            ShowPage(_pageIndex);
        }

        private void OnClickPrevPage()
        {
            _pageIndex--;
            _pageIndex = Mathf.Max(0, _pageIndex); // Clamp zero.

            ShowPage(_pageIndex);
        }
        #endregion
    }
}
