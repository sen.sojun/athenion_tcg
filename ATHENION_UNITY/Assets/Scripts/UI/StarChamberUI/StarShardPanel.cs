﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarShardPanel : MonoBehaviour
{
    #region Serialize Properties
    [Header("Currency")]
    [SerializeField] private List<StarShardCell> _StarShardList = new List<StarShardCell>();
    #endregion

    #region Private Properties
    public static readonly int _maxNormalSignAmount = 999;
    public static readonly int _maxSuperSignAmount = 10;

    private Dictionary<VirtualCurrency, int> _vcList = new Dictionary<VirtualCurrency, int>();
    #endregion

    #region Methods
    public void UpdateCurrency()
    {
        _vcList = new Dictionary<VirtualCurrency, int>();

        _vcList.Add(VirtualCurrency.SC, DataManager.Instance.InventoryData.GetVirtualCurrency(VirtualCurrency.SC));
        _vcList.Add(VirtualCurrency.ST, DataManager.Instance.InventoryData.GetVirtualCurrency(VirtualCurrency.ST));
        _vcList.Add(VirtualCurrency.SS, DataManager.Instance.InventoryData.GetVirtualCurrency(VirtualCurrency.SS));
        _vcList.Add(VirtualCurrency.S1, DataManager.Instance.InventoryData.GetVirtualCurrency(VirtualCurrency.S1));
        _vcList.Add(VirtualCurrency.S2, DataManager.Instance.InventoryData.GetVirtualCurrency(VirtualCurrency.S2));
        _vcList.Add(VirtualCurrency.S3, DataManager.Instance.InventoryData.GetVirtualCurrency(VirtualCurrency.S3));
        _vcList.Add(VirtualCurrency.S4, DataManager.Instance.InventoryData.GetVirtualCurrency(VirtualCurrency.S4));
        _vcList.Add(VirtualCurrency.S5, DataManager.Instance.InventoryData.GetVirtualCurrency(VirtualCurrency.S5));

        if (_StarShardList != null && _StarShardList.Count > 0)
        {
            for (int index = 0; index < _StarShardList.Count; ++index)
            {
                if (_vcList != null && _vcList.ContainsKey(_StarShardList[index].Currency))
                {
                    int amount = _vcList[_StarShardList[index].Currency];

                    switch (_StarShardList[index].Currency)
                    {
                        case VirtualCurrency.S1: // Nova
                        case VirtualCurrency.S2: // Umbra
                        case VirtualCurrency.S3: // Quasar
                        case VirtualCurrency.S4: // Solar
                        case VirtualCurrency.S5: // Lunar
                        {
                            _StarShardList[index].SetData(amount, _maxSuperSignAmount);
                        }
                        break;

                        case VirtualCurrency.SC:
                        case VirtualCurrency.ST:
                        case VirtualCurrency.SS:
                        {
                            _StarShardList[index].SetData(amount, _maxNormalSignAmount);
                        }
                        break;
                    }
                }
            }
        }
    }
    #endregion
}
