﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class StarShardCell : MonoBehaviour
{
    #region Serialize Properties
    [SerializeField] public VirtualCurrency Currency;
    [SerializeField] private Image IMG_Icon;
    [SerializeField] private TextMeshProUGUI _TEXT_Current;
    [SerializeField] private TextMeshProUGUI _TEXT_Max;
    #endregion

    #region Public Properties
    #endregion

    #region Private Properties
    private int _current = 0;
    private int _max = 0;
    #endregion

    public void SetData(int current, int max)
    {
        _current = current;
        _max = max;

        SetCurrent();
        SetMax();
    }

    public void SetData(VirtualCurrency currency, int current, int max)
    {
        _current = current;
        _max = max;

        SetCurrent();
        SetMax();
        SetImage(currency);
        SetTooltip("VC_" + currency.ToString());
    }

    private void SetTooltip(string key)
    {
        GetComponent<TooltipAttachment>().Setup(key);
    }

    private void SetCurrent()
    {
        _TEXT_Current.text = _current.ToString();
    }

    private void SetMax()
    {
        _TEXT_Max.text = "/ " + _max.ToString();
    }

    private void SetImage(VirtualCurrency currency)
    {
        string key = "VC_" + currency.ToString(); ;
        Sprite icon = SpriteResourceHelper.LoadSprite(key, SpriteResourceHelper.SpriteType.ICON_Item);
        IMG_Icon.sprite = icon;
    }
}
