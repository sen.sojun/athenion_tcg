﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class AutoRotate : MonoBehaviour
{
    public Vector3 MaxAngle;
    public Ease Easing;
    public float Time;
    public float Delay;
    
    private Vector3 _originalAngle;
    private Sequence _sq;

    private void Awake()
    {
        _originalAngle = new Vector3(transform.localRotation.x, transform.localRotation.y, transform.localRotation.z);
    }

    private void OnEnable()
    {
        if (_sq != null)
        {
            _sq.Kill();
        }

        _sq = DOTween.Sequence();
        _sq.Append(transform.DOLocalRotate(MaxAngle, Time).SetEase(Easing));
        _sq.Append(transform.DOLocalRotate(_originalAngle, Time).SetEase(Easing));
        _sq.PrependInterval(Delay);
        _sq.SetLoops(-1);
    }

    private void OnDisable()
    {
        if (_sq != null)
        {
            _sq.Kill();
            _sq = null;
        }

    }

}
