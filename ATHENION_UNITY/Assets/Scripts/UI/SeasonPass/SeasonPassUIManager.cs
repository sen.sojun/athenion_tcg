﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using Random = UnityEngine.Random;

public class SeasonPassUIManager : MonoSingleton<SeasonPassUIManager>
{
    #region Enums
    public enum TabType
    {
        Reward
        , Quest
        , Challenge
    }
    #endregion

    #region Public Properties
    public bool UI_IsShowReward { get; private set; }
    public bool UI_IsShowMainQuest { get; private set; }
    public bool UI_IsShowChallenge { get; private set; }

    [Header("Ref")]
    public GameObject Popup_BuyPremium;
    public SeasonPassGetTier Popup_GetTier;
    public SeasonPassRewardPanel Group_Rewards;
    public SeasonPassMainQuestPanel Group_MainQuest;
    public SeasonPassChallengePanel Group_ChallengeQuest;
    public TextMeshProUGUI TEXT_PlayerTierLevel;
    public TextMeshProUGUI TEXT_PlayerCurExp;
    public TextMeshProUGUI TEXT_DaysRemaining;
    public Image IMG_NavSlider;
    public Slider Slider_XPBar;
    public RectTransform Rect_PanelInfo;
    public CanvasGroup IMGFX_Flare;
    public CanvasGroup IMGFX_GoldBar;

    [Header("Button")]
    public Button BTN_TapToClose;
    public Button BTN_NavRewards;
    public Button BTN_NavMainQ;
    public Button BTN_NavChallenge;
    public Button BTN_GetTierTop;
    public Button BTN_BuyPremiumPass;
    #endregion

    #region Private Properties
    private SeasonPassUIManager.TabType _currentTab = TabType.Reward;
    private bool _isXPBarAnimating = false;
    private bool _isCheckingUnclaimedReward = false;
    private bool _initialized = false;
    private bool _isTabInitialized = false;
    private List<ItemData> _firstRewardData = new List<ItemData>();
    private Coroutine _initCuroutine = null;
    private Coroutine _timeCounter = null;
    private Coroutine _popupTimer = null;
    private Tweener _navGroupImageTweener;
    private Sequence _xpBarAnimation;
    private int _sfxIndex = -1;
    #endregion

    #region Static Properties
    public static event Action OnCompletedQuest;
    #endregion

    #region Events

    public static event Action OnActive;
    public static event Action OnReady;
    public static event Action OnClose;

    private void OnEnable()
    {
        _initCuroutine = StartCoroutine(InitializeData());
    }

    private void OnDisable()
    {
        if (_timeCounter != null) StopCoroutine(_timeCounter);

        if (_xpBarAnimation != null)
        {
            _xpBarAnimation.onComplete?.Invoke();
            _xpBarAnimation.Kill();
        }

        if (_sfxIndex >= 0)
        {
            SoundManager.StopSFX(_sfxIndex, 0.05f);
            _sfxIndex = -1;
        }
    }
    #endregion

    #region Methods
    public void UpdateUI()
    {
        //Update Player Data
        PlayerSeasonPassData playerData = SeasonPassManager.Instance.GetPlayerSeasonPassData();
        int playerTier = playerData.GetCurrentSeasonPassTier();
        bool isPlayerPremium = SeasonPassManager.Instance.IsSeasonPassBePremium();

        SeasonPassTierDB.Instance.GetData(playerTier.ToString(), out SeasonPassTierDBData curTierData);
        SeasonPassTierDB.Instance.GetData((playerTier + 1).ToString(), out SeasonPassTierDBData nextTierData);

        UpdateLevelText(playerTier);

        if (nextTierData != null)
        {
            BTN_GetTierTop.interactable = true;

            int curExessiveScore = playerData.GetCurrentTierExcessiveScore();
            int requiredScore = nextTierData.TierAccScore - curTierData.TierAccScore;
            TEXT_PlayerCurExp.text = string.Format("{0}/{1}", curExessiveScore, requiredScore);
            Slider_XPBar.value = (float)curExessiveScore / (float)requiredScore;
        }
        else
        {
            BTN_GetTierTop.interactable = false;

            TEXT_PlayerCurExp.text = LocalizationManager.Instance.GetText("TEXT_SSPASS_MAXLEVEL");
            Slider_XPBar.value = 1f;
        }

        BTN_BuyPremiumPass.gameObject.SetActive(!isPlayerPremium);

        Group_Rewards.UpdateData();
        Group_MainQuest.UpdateAllCells();
        Group_ChallengeQuest.UpdateAllCells();
    }

    public void SetStarterTab(SeasonPassUIManager.TabType tab)
    {
        _currentTab = tab;
    }

    public void UpdateLevelText(int level)
    {
        TEXT_PlayerTierLevel.text = level.ToString();
    }

    public void PlayBarAnimation(int levelCount, float finalBarValue, UnityAction onComplete = null)
    {
        _isXPBarAnimating = true;

        if (_xpBarAnimation != null)
        {
            _xpBarAnimation.Kill();
        }

        float animSpd = 1f;
        if(levelCount >= 20)
        {
            animSpd = 0.2f;
        }
        else if (levelCount >= 10)
        {
            animSpd = 0.35f;
        }
        else if (levelCount >= 3)
        {
            animSpd = 0.5f;
        }

        _xpBarAnimation = DOTween.Sequence();
        _sfxIndex = SoundManager.PlaySFX(SoundManager.SFX.Exp_Increasing_Loop, true);
        for (int i = 0; i < levelCount; i++)
        {
            _xpBarAnimation.Append(Slider_XPBar.DOValue(1f, 0.3f * animSpd).OnComplete
                (
                    delegate 
                    {
                        //setup Glow VFX
                        IMGFX_GoldBar.gameObject.SetActive(true);
                        IMGFX_GoldBar.alpha = 0f;

                        IMGFX_Flare.gameObject.SetActive(true);
                        IMGFX_Flare.alpha = 0f;
                        IMGFX_Flare.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);

                        //Stop jik jik jik sfx
                        if (_sfxIndex >= 0)
                        {
                            SoundManager.StopSFX(_sfxIndex, 0.05f);
                            _sfxIndex = -1;
                        }
                        SoundManager.PlaySFX(SoundManager.SFX.Exp_Level_Up);
                    }
                ));

            //start Glow VFX
            //Fade in
            _xpBarAnimation.Append(IMGFX_Flare.transform.DOScale(2, 0.5f * animSpd).OnComplete
                (
                    delegate
                    {
                        Slider_XPBar.value = 0f;
                        IncreaseLevelText();
                    }
                )
                );
            _xpBarAnimation.Join(IMGFX_Flare.DOFade(1, 0.5f * animSpd));
            _xpBarAnimation.Join(IMGFX_GoldBar.DOFade(1, 0.5f * animSpd));
            //Fade out
            _xpBarAnimation.Append(IMGFX_Flare.transform.DOScale(2.65f, 0.5f * animSpd));
            _xpBarAnimation.Join(IMGFX_Flare.DOFade(0, 0.5f * animSpd));

            Tweener fadeoutTweener = IMGFX_GoldBar.DOFade(0, 0.5f * animSpd);
            if (finalBarValue > 0 || (finalBarValue == 0 && i != (levelCount-1)))
            {
                fadeoutTweener.OnComplete( delegate { _sfxIndex = SoundManager.PlaySFX(SoundManager.SFX.Exp_Increasing_Loop, true); });
            }
            _xpBarAnimation.Join(fadeoutTweener);
        }
        _xpBarAnimation.Append(Slider_XPBar.DOValue(finalBarValue, 0.3f * animSpd).SetEase(Ease.OutCirc));
        _xpBarAnimation.OnComplete
        (
            delegate 
            {
                _isXPBarAnimating = false;
                string title = LocalizationManager.Instance.GetText("TEXT_CONGRATULATIONS");
                string msg = string.Format(LocalizationManager.Instance.GetText("TEXT_SSPASS_REWARD_DETAIL"), TEXT_PlayerTierLevel.text);
                if(levelCount > 0)
                {
                    PopupUIManager.Instance.ShowPopup_ConfirmOnly(title, msg, onComplete);
                }
                else
                {
                    onComplete?.Invoke();
                }

                if (_sfxIndex >= 0)
                {
                    SoundManager.StopSFX(_sfxIndex, 0.15f);
                    _sfxIndex = -1;
                }
            }
        );
        _xpBarAnimation.Play();
    }

    public void OnClickBuyPremiumPass()
    {
        UnityAction OnClickPurchase = () =>
        {
            PopupUIManager.Instance.Show_MidSoftLoad(true);
            SeasonPassManager.Instance.PurchaseSeasonPassPremium(
                (rewardList) =>
                {
                    string title = LocalizationManager.Instance.GetText("SHOP_PURCHASE_SUCCESS_HEADER");
                    string message = string.Format(LocalizationManager.Instance.GetText("TEXT_SSPASS_UNLOCKPREMIUMPASS"));

                    PopupUIManager.Instance.ShowPopup_GotReward(title, message, rewardList);
                    PopupUIManager.Instance.Show_MidSoftLoad(false);
                    DataManager.Instance.LoadInventory(null, null);

                    //TO DO:Refresh UI;
                    UpdateUI();
                }
                , (error) =>
                {
                    PopupUIManager.Instance.Show_MidSoftLoad(false);
                    string title = LocalizationManager.Instance.GetText("ERROR_POPUP_HEADER");
                    string msg = LocalizationManager.Instance.GetText(error);
                    PopupUIManager.Instance.ShowPopup_Error(title, msg);
                }
            );
        };
        PriceData priceData = SeasonPassManager.Instance.GetSeasonPassPremiumPrice();
        string price_sprite = "S_DI"; //just set S_DI as default value;
        switch(priceData.CurrencyType)
        {
            case VirtualCurrency.DI:
                price_sprite = "S_DI";
                break;
                
            case VirtualCurrency.CO:
                price_sprite = "S_CO";
                break;
        }

        string confirmTitle = LocalizationManager.Instance.GetText("TEXT_PURCHASE_SEASONPASS_TITLE");
        string confirmMsg = string.Format(LocalizationManager.Instance.GetText("TEXT_PURCHASE_SEASONPASS_DETAIL"),string.Format("<sprite name={0}>", price_sprite), priceData.Amount);
        PopupUIManager.Instance.ShowPopup_SureCheck(LocalizationManager.Instance.GetText("TEXT_PURCHASE_SEASONPASS_TITLE"),
                                                        confirmMsg,
                                                        OnClickPurchase, null);
    }

    public void CompleteQuest()
    {
        CompleteQuest("ALLQUEST");
    }

    public void CompleteQuest(string qID, bool isMainQuest = true)
    {
        if (IsProcessing()) return;

        //CheckSeasonPassAllTierReward

        PlayerSeasonPassData playerData = SeasonPassManager.Instance.GetPlayerSeasonPassData();
        int cachedLevel = playerData.GetCurrentSeasonPassTier();

        UnityAction PlayBarAnimAction = () =>
        {
            int increasedLevel = CheckLevelUp(cachedLevel);
            float finalBar = 0f;

            PlayerSeasonPassData newPlayerData = SeasonPassManager.Instance.GetPlayerSeasonPassData();
            int newLevel = playerData.GetCurrentSeasonPassTier();

            SeasonPassTierDB.Instance.GetData(newLevel.ToString(), out SeasonPassTierDBData curTierData);
            SeasonPassTierDB.Instance.GetData((newLevel + 1).ToString(), out SeasonPassTierDBData nextTierData);

            if (nextTierData != null)
            {
                int curExessiveScore = playerData.GetCurrentTierExcessiveScore();
                int requiredScore = nextTierData.TierAccScore - curTierData.TierAccScore;
                TEXT_PlayerCurExp.text = string.Format("{0}/{1}", curExessiveScore, requiredScore);
                finalBar = (float)curExessiveScore / (float)requiredScore;
            }
            else
            {
                TEXT_PlayerCurExp.text = LocalizationManager.Instance.GetText("TEXT_SSPASS_MAXLEVEL");
                finalBar = 1f;
            }

            List<ItemData> gotRewardList = new List<ItemData>();
            SeasonPassTierRewardDBData ssRewardData = SeasonPassManager.Instance.GetCurrentSeasonPassTierRewardData();
            for (int i = cachedLevel; i <= newLevel; i++)
            {
                //newPlayerData.IsBePremium
                List<SeasonPassTierRewardData> rewDataList = ssRewardData.GetSeasonPassTierRewardDataList(i);
                foreach (SeasonPassTierRewardData rewData in rewDataList)
                {
                    if (!rewData.IsPremium || (rewData.IsPremium && playerData.IsBePremium()))
                    {
                        gotRewardList.AddRange(rewData.ItemDataList);
                    }
                }
            }

            PlayBarAnimation(increasedLevel, finalBar, CheckUnclaimedReward);
        };

        UnityAction<List<ItemData>> OnComplete = (List<ItemData> rewardList) =>
        {
            Debug.Log("OnComplete is called!");

            PopupUIManager.Instance.Show_MidSoftLoad(false);

            OnCompletedQuest?.Invoke();

            if (rewardList.Count > 0)
            {
                if (rewardList.Count == 1 && rewardList[0].ItemID == "SCORE")
                {
                    PopupUIManager.Instance.ShowPopup_GotRewardAuto(rewardList, PlayBarAnimAction);
                }
                else
                {
                    string title = LocalizationManager.Instance.GetText("TEXT_SSPASS_QUESTCOMPLETED");
                    string message = LocalizationManager.Instance.GetText("TEXT_YOUVEGOTREWARD");

                    PopupUIManager.Instance.ShowPopup_GotReward(title, message, rewardList, PlayBarAnimAction);
                }

                DataManager.Instance.LoadInventory(null, null);
            }
            else
            {
                PlayBarAnimAction?.Invoke();
            }

            if (isMainQuest)
            {
                //Group_MainQuest.UpdateSpecificCell(qID);
                Group_MainQuest.UpdateAllCells();
            }
            else
            {
                //Group_ChallengeQuest.UpdateSpecificCell(qID);
                Group_ChallengeQuest.SetupData();
            }
        };

        PopupUIManager.Instance.Show_MidSoftLoad(true);

        if (qID == "ALLQUEST")
        {
            SeasonPassManager.Instance.CheckSeasonPassAllQuests(
                OnComplete,
                delegate (string error)
                {
                    PopupUIManager.Instance.Show_MidSoftLoad(false);
                    string title = LocalizationManager.Instance.GetText("ERROR_POPUP_HEADER");
                    string msg = error;
                    PopupUIManager.Instance.ShowPopup_Error(title, msg);
                }
                );
        }
        else
        {
            SeasonPassManager.Instance.CheckSeasonPassQuest(qID,
                OnComplete,
                delegate (string error)
                {
                    PopupUIManager.Instance.Show_MidSoftLoad(false);
                    string title = LocalizationManager.Instance.GetText("ERROR_POPUP_HEADER");
                    string msg = error;
                    PopupUIManager.Instance.ShowPopup_Error(title, msg);
                }
                );
        }
    }

    private void ResetUIFlow()
    {
        BTN_NavRewards.onClick.Invoke();
    }

    private IEnumerator _extraProcess;

    public void SetExtraProcess(IEnumerator process)
    {
        _extraProcess = process;
    }

    private IEnumerator InitializeData()
    {
        OnActive?.Invoke();
        TopToolbar.AddSetting(true, true, delegate { ClosePopup(); });

        if (!_initialized)
        {
            _initialized = true;

            InitButton();

            UI_IsShowReward = true;
            UI_IsShowMainQuest = false;
            UI_IsShowChallenge = false;

            _xpBarAnimation = null;
            /*
            Group_MainQuest.SetUIManagerRef(this);
            Group_ChallengeQuest.SetUIManagerRef(this);
            Group_Rewards.SetUIManagerRef(this);
            Popup_GetTier.SetUIManagerRef(this);
            */
            ChangePage(_currentTab);

            SeasonPassRewardPanel.OnFinishedInit += OnFinishedTabInit;
            SeasonPassMainQuestPanel.OnFinishedInit += OnFinishedTabInit;
            SeasonPassChallengePanel.OnFinishedInit += OnFinishedTabInit;
        }
        else
        {
            ChangePage(_currentTab);
        }

        if (_timeCounter != null) StopCoroutine(_timeCounter);
        SeasonPassDBData dbData = SeasonPassManager.Instance.GetCurrentSeasonPassDBData();
        DateTime expDate = dbData.EndDateTime;
        _timeCounter = StartCoroutine(EndTimer(expDate, TEXT_DaysRemaining, null));

        yield return new WaitUntil(() => _isTabInitialized == true);

        UpdateUI();

        OnReady?.Invoke();

        SetStarterTab(TabType.Reward);

        if (_extraProcess != null)
            yield return _extraProcess;

        //TODO: Show Grant Reward.
        CheckUnclaimedReward();
    }

    private void OnFinishedTabInit()
    {
        _isTabInitialized = true;
    }

    private void ChangePage(SeasonPassUIManager.TabType tab)
    {
        GameObject currentTabObj = null;
        Transform parent = null;

        switch (tab)
        {
            case TabType.Reward:
                {
                    UI_IsShowReward = true;
                    UI_IsShowMainQuest = false;
                    UI_IsShowChallenge = false;
                    currentTabObj = Group_Rewards.gameObject;
                    parent = BTN_NavRewards.transform;
                }
                break;

            case TabType.Quest:
                {
                    UI_IsShowReward = false;
                    UI_IsShowMainQuest = true;
                    UI_IsShowChallenge = false;
                    currentTabObj = Group_MainQuest.gameObject;
                    parent = BTN_NavMainQ.transform;
                }
                break;

            case TabType.Challenge:
                {
                    UI_IsShowReward = false;
                    UI_IsShowMainQuest = false;
                    UI_IsShowChallenge = true;
                    currentTabObj = Group_ChallengeQuest.gameObject;
                    parent = BTN_NavChallenge.transform;
                }
                break;

            default: return;
        }

        Group_Rewards.gameObject.SetActive(UI_IsShowReward);
        Group_MainQuest.gameObject.SetActive(UI_IsShowMainQuest);
        Group_ChallengeQuest.gameObject.SetActive(UI_IsShowChallenge);

        GameHelper.UITransition_FadeIn(currentTabObj.gameObject);
        MoveSliderImage(parent);
    }

    private void InitButton()
    {
        BTN_TapToClose.onClick.RemoveAllListeners();
        BTN_TapToClose.onClick.AddListener
        (
            delegate
            {
                ClosePopup();
            }
        );

        BTN_NavRewards.onClick.RemoveAllListeners();
        BTN_NavRewards.onClick.AddListener
        (
            delegate
            {
                if (!UI_IsShowReward && !IsProcessing())
                {
                    ChangePage(TabType.Reward);
                }
            }
        );

        BTN_NavMainQ.onClick.RemoveAllListeners();
        BTN_NavMainQ.onClick.AddListener
        (
            delegate
            {
                if (!UI_IsShowMainQuest && !IsProcessing())
                {
                    ChangePage(TabType.Quest);
                }
            }
        );

        BTN_NavChallenge.onClick.RemoveAllListeners();
        BTN_NavChallenge.onClick.AddListener
        (
            delegate
            {
                if (!UI_IsShowChallenge && !IsProcessing())
                {
                    ChangePage(TabType.Challenge);
                }
            }
        );

        BTN_BuyPremiumPass.onClick.RemoveAllListeners();
        BTN_BuyPremiumPass.onClick.AddListener
        (
            delegate
            {
                OnClickBuyPremiumPass();
            }
        );

        BTN_GetTierTop.onClick.RemoveAllListeners();
        BTN_GetTierTop.onClick.AddListener
        (
            delegate
            {
                OnGetTierClick();
            }
        );
    }

    private void IncreaseLevelText()
    {
        if (int.TryParse(TEXT_PlayerTierLevel.text, out int nextLevel))
        {
            nextLevel++;
            UpdateLevelText(nextLevel);
        }
    }

    private int CheckLevelUp(int checkLevel)
    {
        PlayerSeasonPassData playerData = SeasonPassManager.Instance.GetPlayerSeasonPassData();
        int newLevel = playerData.GetCurrentSeasonPassTier();
        return (newLevel - checkLevel);
    }

    private void MoveSliderImage(Transform targetTrans)
    {
        if (_navGroupImageTweener != null)
        {
            _navGroupImageTweener.Kill();
        }
        IMG_NavSlider.transform.SetParent(targetTrans);
        IMG_NavSlider.transform.SetAsFirstSibling();
        _navGroupImageTweener = IMG_NavSlider.transform.DOLocalMove(new Vector2(0f, 0f), 0.5f).SetEase(Ease.OutExpo);
    }

    public void ClosePopup(bool isForceClose = false)
    {
        if (!IsProcessing() || isForceClose)
        {
            TopToolbar.RemoveLatestSetting();
            GameHelper.UITransition_FadeOut(gameObject);
            HomeManager.Instance.ShowMainCanvas();

            OnClose?.Invoke();
        }
    }

    private void OnGetTierClick()
    {
        if (!IsProcessing())
        {
            Popup_GetTier.gameObject.SetActive(true);
        }
    }

    private void CheckUnclaimedReward()
    {
        _isCheckingUnclaimedReward = true;
        PopupUIManager.Instance.Show_SoftLoad(true);
        SeasonPassManager.Instance.CheckSeasonPassTierReward(
            1,
            delegate (List<ItemData> rewardData)
            {
                _isCheckingUnclaimedReward = false;
                DataManager.Instance.LoadInventory(null, null);
                PopupUIManager.Instance.Show_SoftLoad(false);
                if (rewardData.Count > 0)
                {
                    string firstTimeHeader = LocalizationManager.Instance.GetText("TEXT_MISSION_YOUVEGOTREWARDTITLE");
                    string firstTimeText = LocalizationManager.Instance.GetText("TEXT_MISSION_YOUVEGOTREWARD");
                    PopupUIManager.Instance.ShowPopup_GotReward(
                        firstTimeHeader,
                        firstTimeText,
                        rewardData);
                }
            },
            delegate (string errorMsg)
            {
                _isCheckingUnclaimedReward = false;
                PopupUIManager.Instance.ShowPopup_Error("Check Unclaimed Reward Error", 
                                                        string.Format("An error occured while checking unclaimed reward, please try again later.\r\nError Code : {0}", errorMsg)
                                                        );
                PopupUIManager.Instance.Show_SoftLoad(false);
            });
    }


    public bool IsProcessing()
    {
        return _isXPBarAnimating || _isCheckingUnclaimedReward;
    }
    
    IEnumerator EndTimer(DateTime finishDate, TextMeshProUGUI cooldownText = null, UnityAction onComplete = null)
    {
        while (true)
        {
            DateTime currentDateTime = DateTimeData.GetDateTimeUTC();
            float remainingSec = (float)(finishDate - currentDateTime).TotalSeconds;

            if(remainingSec <= 0)
            {
                PopupUIManager.Instance.ShowPopup_Error
                (
                    LocalizationManager.Instance.GetText("TEXT_SSPASS_INVALIDDATE"),
                    LocalizationManager.Instance.GetText("TEXT_SSPASS_INVALIDDATE_DETAIL"),
                    delegate
                    {
                        onComplete?.Invoke();
                        ClosePopup(true);
                    }
                );
                yield break;
            }
            else if (remainingSec > 0)
            {
                int d, h, m, s;
                d = Mathf.FloorToInt(remainingSec / 86400);
                h = Mathf.FloorToInt(remainingSec / 3600);
                m = Mathf.FloorToInt((remainingSec - h * 3600) / 60);
                s = Mathf.FloorToInt(remainingSec % 60);

                string timeText = "";
                if (d > 0)
                {
                    timeText = string.Format(LocalizationManager.Instance.GetText("TEXT_XDAYS"), d);
                }
                else if (h > 0)
                {
                    timeText = string.Format(LocalizationManager.Instance.GetText("TEXT_XHOURS"), h);
                }
                else if (m > 0)
                {
                    timeText = string.Format(LocalizationManager.Instance.GetText("TEXT_XMINUTES"), m);
                }
                else
                {
                    timeText = string.Format(LocalizationManager.Instance.GetText("TEXT_XSECONDS"), s);
                }

                if (cooldownText != null)
                {
                    cooldownText.text = timeText;
                }
            }

            yield return new WaitForEndOfFrame();
        }
    }
    
    public void PageNavigate(string funcName, string funcParam)
    {
        if (IsProcessing()) return;

        ClosePopup(true);

        NavigatorController.Instance.SendMessage(
              funcName
            , funcParam
            , SendMessageOptions.DontRequireReceiver
        );
    }

    IEnumerator RewardPopupTimer(float time, UnityAction onComplete = null)
    {
        float timeCounter = 0f;

        while(timeCounter < time)
        {
            timeCounter += Time.deltaTime;

            yield return new WaitForEndOfFrame();
        }

        onComplete.Invoke();
    }
    #endregion

}
