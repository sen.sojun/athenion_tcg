﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;
using DG.Tweening;
using UnityEngine.Events;
using System.Linq;

public class SeasonPassGetTier : MonoBehaviour
{
    #region Public Properties
    [Header("Prefabs")]
    public GameObject Prefab_RewardCell;

    [Header("Ref")]
    public GameObject Mask_GameObject;
    public Transform Container_Rewards;
    public ScrollRect ScrollRect_Rewards;
    public TextMeshProUGUI TEXT_TierAmount;
    public TextMeshProUGUI TEXT_BuyTierTo;
    public TextMeshProUGUI TEXT_PriceText;
    public Button Button_Plus;
    public Button Button_Minus;
    public Button Button_PlusTen;
    public Button Button_Cancel;
    public Button Button_Purchase;
    #endregion

    #region Private Properties
    //private SeasonPassUIManager _seasonPassUIManager;
    private bool _initialized = false;
    private int _tierAmount;
    private int _currentPlayerTier;
    private int _maxLevel;
    private int _vc_DI_Cost;
    private List<RewardCell_SeasonPass> _rewardCellList = new List<RewardCell_SeasonPass>();
    #endregion

    #region Methods

    private void InitUI()
    {
        if(!_initialized)
        {
            _initialized = true;

            InitBTNEvent();
            SeasonPassTierDB.Instance.GetAllData(out List<SeasonPassTierDBData> dataList);
            _maxLevel = dataList.Count;

            PriceData priceData = SeasonPassManager.Instance.GetSeasonPassTierPrice();
            _vc_DI_Cost = priceData.Amount;
        }

        _tierAmount = 1;
        
        _currentPlayerTier = SeasonPassManager.Instance.GetPlayerSeasonPassData().GetCurrentSeasonPassTier();

        if (_rewardCellList != null && _rewardCellList.Count > 0)
        {
            foreach(RewardCell_SeasonPass cell in _rewardCellList)
            {
                cell.gameObject.SetActive(false);
            }
        }
        UpdateUI();
    }

    private void UpdateUI()
    {
        bool isPlayerPremium = SeasonPassManager.Instance.IsSeasonPassBePremium();

        UpdateHeaderText();
        UpdateTotalPriceText();
        UpdateTierAmount();

        //Update Reward
        SeasonPassTierRewardDBData ssRewardData = SeasonPassManager.Instance.GetCurrentSeasonPassTierRewardData();
        List<ItemData> additionalRewards = new List<ItemData>();

        bool isSum = true; //"VC_" will always sum;
        
        for(int i = 1; i <= _tierAmount; i++)
        {
            List<SeasonPassTierRewardData> rewardDataList = ssRewardData.GetSeasonPassTierRewardDataList(_currentPlayerTier+i);
            foreach (SeasonPassTierRewardData rewardData in rewardDataList)
            {
                if (rewardData.IsPremium && !isPlayerPremium) continue;

                if (rewardData != null && rewardData.ItemDataList.Count > 0)
                {
                    foreach (ItemData tierReward in rewardData.ItemDataList)
                    {
                        if (isSum || tierReward.ItemID.StartsWith("VC"))
                        {
                            bool found = false;
                            foreach (ItemData existingReward in additionalRewards)
                            {
                                if (existingReward.ItemID == tierReward.ItemID)
                                {
                                    found = true;
                                    existingReward.ModifyAmount(tierReward.Amount);
                                    break;
                                }
                            }

                            if (!found)
                            {
                                additionalRewards.Add(new ItemData(tierReward.ItemID, tierReward.Amount));
                            }
                        }
                        else
                        {
                            additionalRewards.Add(new ItemData(tierReward.ItemID, tierReward.Amount));
                        }
                    }
                }
            }
        }
        
        /*
        //Latest Level Reward
        List<string> latestList = new List<string>();
        List<SeasonPassTierRewardData> latestReward = ssRewardData.GetSeasonPassTierRewardDataList(_currentPlayerTier + _tierAmount);
        foreach (SeasonPassTierRewardData rewardData in latestReward)
        {
            foreach (ItemData rewData in rewardData.ItemDataList)
            {
                latestList.Add(rewData.ItemID);
            }
        }
        */

        int loopCount = Mathf.Max(_rewardCellList.Count, additionalRewards.Count);
        for (int i = 0; i < loopCount; i++)
        {
            //if UI is more than data, hide ui;
            if (i >= additionalRewards.Count)
            {
                _rewardCellList[i].gameObject.SetActive(false);
            }
            //if UI is less than data, create new ui;
            else if (i >= _rewardCellList.Count)
            {
                RewardCell_SeasonPass newCell = CreateReward(additionalRewards[i].ItemID, additionalRewards[i].Amount, Container_Rewards);
                _rewardCellList.Add(newCell);

                newCell.OnBeginDragEvent += OnRewardItem_BeginDrag;
                newCell.OnDragEvent += OnRewardItem_Drag;
                newCell.OnEndDragEvent += OnRewardItem_EndDrag;
            }
            //Update UI;
            else
            {
                _rewardCellList[i].InitData(additionalRewards[i].ItemID, additionalRewards[i].Amount);
                _rewardCellList[i].gameObject.SetActive(true);
                /*
                if(latestList.Contains(additionalRewards[i].ItemID))
                {
                //    _rewardCellList[i].PlayerAmountPopAnimation();
                }*/
            }
        }

    }

    private void UpdateTotalPriceText()
    {
        TEXT_PriceText.text = string.Format(LocalizationManager.Instance.GetText("TEXT_SSPASS_COST"), "<sprite name=S_DI>", _tierAmount * _vc_DI_Cost);
    }

    private void UpdateHeaderText()
    {
        TEXT_BuyTierTo.text = string.Format(LocalizationManager.Instance.GetText("TEXT_BUYTIERTO"), _currentPlayerTier + _tierAmount);
    }

    private void UpdateTierAmount()
    {
        TEXT_TierAmount.text = _tierAmount.ToString();
    }

    private void InitBTNEvent()
    {
        Button_Plus.onClick.RemoveAllListeners();
        Button_Plus.onClick.AddListener( delegate { OnPlusClick(); } );

        Button_Minus.onClick.RemoveAllListeners();
        Button_Minus.onClick.AddListener(delegate { OnMinusClick(); });

        Button_PlusTen.onClick.RemoveAllListeners();
        Button_PlusTen.onClick.AddListener(delegate { OnPlusTenClick(); });

        Button_Cancel.onClick.RemoveAllListeners();
        Button_Cancel.onClick.AddListener(delegate { Mask_GameObject.SetActive(false); });

        Button_Purchase.onClick.RemoveAllListeners();
        Button_Purchase.onClick.AddListener(delegate { OnGetTierNowClick(); });
    }
    #endregion

    #region Events
    private void OnEnable()
    {
        InitUI();
    }
    private void OnDisable()
    {

    }

    private void OnPlusClick()
    {
        if (_currentPlayerTier + _tierAmount + 1 > _maxLevel) return;

        _tierAmount++;
        UpdateUI();
    }
    private void OnMinusClick()
    {
        if (_tierAmount <= 1) return;
        _tierAmount--;
        UpdateUI();
    }
    private void OnPlusTenClick()
    {
        if (_currentPlayerTier + _tierAmount + 10 > _maxLevel)
        {
            _tierAmount += _maxLevel - (_currentPlayerTier + _tierAmount);
        }
        else
        {
            _tierAmount += 10;
        }
        UpdateUI();
    }
    private void OnGetTierNowClick()
    {
        UnityAction OnConfirmedPurchase = () =>
        {
            PopupUIManager.Instance.Show_MidSoftLoad(true);
            Mask_GameObject.SetActive(false);
            SeasonPassManager.Instance.PurchaseSeasonPassTier(
                _tierAmount,
                (rewardList) =>
                {
                    string title = LocalizationManager.Instance.GetText("TEXT_CONGRATULATIONS");
                    string msg = string.Format(LocalizationManager.Instance.GetText("TEXT_SSPASS_REWARD_DETAIL"), _currentPlayerTier + _tierAmount);
                    PopupUIManager.Instance.ShowPopup_GotReward(title, msg, rewardList);
                    SeasonPassUIManager.Instance.UpdateUI();
                    
                    PopupUIManager.Instance.Show_MidSoftLoad(false);
                    DataManager.Instance.LoadInventory(null,null);
                }
                , (error) =>
                {
                    PopupUIManager.Instance.Show_MidSoftLoad(false);
                    string title = LocalizationManager.Instance.GetText("ERROR_POPUP_HEADER");
                    string msg = error;
                    PopupUIManager.Instance.ShowPopup_Error(title, string.Format("{0} ({1}{2})",LocalizationManager.Instance.GetText(msg), "<sprite name=S_DI>", _tierAmount * _vc_DI_Cost) );
                }
            );
        };

        PopupUIManager.Instance.ShowPopup_SureCheck(LocalizationManager.Instance.GetText("TEXT_CONFIRMATION_TITLE"), 
                                                    string.Format(LocalizationManager.Instance.GetText("TEXT_SSPASS_BUYTIERCONFIRMATION"), 
                                                    _tierAmount,"<sprite name=S_DI>", _tierAmount* _vc_DI_Cost), OnConfirmedPurchase, null);

    }
    #endregion

    #region Cell Gen    
    private RewardCell_SeasonPass CreateReward(string rewardKey, int count, Transform container)
    {
        GameObject obj = Instantiate(Prefab_RewardCell, container);
        obj.SetActive(true);
        RewardCell_SeasonPass cell = obj.GetComponent<RewardCell_SeasonPass>();
        cell.InitData(rewardKey, count);
        return cell;
    }
    #endregion

    #region Event
    
    public void OnRewardItem_BeginDrag(PointerEventData eventData)
    {
        ScrollRect_Rewards.OnBeginDrag(eventData);
    }

    public void OnRewardItem_Drag(PointerEventData eventData)
    {
        ScrollRect_Rewards.OnDrag(eventData);
    }

    public void OnRewardItem_EndDrag(PointerEventData eventData)
    {
        ScrollRect_Rewards.OnEndDrag(eventData);
    }
    
    #endregion
}
