﻿using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;
using DG.Tweening;
using UnityEngine.Events;
using System.Linq;
using DG.Tweening;
using System;

public class SeasonPassMainQuestPanel : MonoBehaviour
{
    #region Public Properties
    [Header("Ref")]
    public GameObject Prefab_QuestCell;
    public GameObject Group_CellGroup;
    
    public Transform Container_AllGroup;
    public Transform Container_Weekly;
    public RectTransform Viewport_AllGroup;
    public ScrollRect ScrollRect_AllGroup;

    public TextMeshProUGUI Text_CurrentGroup;
    public TextMeshProUGUI Text_QuestCount;

    public Button Button_PrevPage;
    public Button Button_NextPage;
    public Button Button_ClaimAll;
    #endregion

    #region Private Properties
    private bool _isInitialized = false;
    private int _weekCount;
    private int _currentPage;
    private Transform _groupResetable;
    private Dictionary<int, List<string>> _allQuestIDList = new Dictionary<int, List<string>>();
    private Dictionary<string, SeasonPassQuestCell> _questCellDict = new Dictionary<string, SeasonPassQuestCell>();
    private Dictionary<int, Transform> _groupWeekly = new Dictionary<int, Transform>();
    private Sequence _changePageSeq;
    private List<GameObject> _permanantDisableList = new List<GameObject>();
    #endregion

    #region Events
    public static event Action OnFinishedInit;
    public static event Action OnClose;

    private void OnEnable()
    {
        PopupUIManager.Instance.Show_MidSoftLoad(true);

        Init(
            delegate()
            {
                UpdateAllCells();
                ResetUIFlow();

                PopupUIManager.Instance.Show_MidSoftLoad(false);

                //Reset Container
                ToggleVisibleCell();

                OnFinishedInit?.Invoke();
            }
        );
    }

    private void OnDisable()
    {
        PlayerSeenListManager.Instance.SyncUpdateSeenListData();
        OnClose?.Invoke();
    }

    private void OnScrolling(Vector2 value)
    {
        if (_isInitialized)
        {
            Container_AllGroup.GetComponent<ContentSizeFitter>().enabled = false;
            Container_AllGroup.GetComponent<VerticalLayoutGroup>().enabled = false;

            _groupResetable.GetComponent<ContentSizeFitter>().enabled = false;
            _groupResetable.GetComponent<VerticalLayoutGroup>().enabled = false;
            foreach (KeyValuePair<int, Transform> group in _groupWeekly)
            {
                group.Value.GetComponent<ContentSizeFitter>().enabled = false;
                group.Value.GetComponent<VerticalLayoutGroup>().enabled = false;
            }

            ToggleVisibleCell();
        }
    }
    #endregion

    #region Methods
    public void Init(UnityAction onComplete)
    {
        StartCoroutine(InitializeData(onComplete));
    }

    private IEnumerator InitializeData(UnityAction onComplete)
    {
        if(!_isInitialized)
        {
            _allQuestIDList = new Dictionary<int, List<string>>();
            if(_questCellDict.Count > 0)
            {
                //Clear unfinished cell
                foreach(KeyValuePair<string, SeasonPassQuestCell> data in _questCellDict)
                {
                    Destroy(data.Value.gameObject);
                }
            }
            _questCellDict = new Dictionary<string, SeasonPassQuestCell>();

            yield return InitializeResetableQuest();
            yield return InitializeWeeklyQuest();

            InitButton();

            LayoutRebuilder.ForceRebuildLayoutImmediate(ScrollRect_AllGroup.GetComponent<RectTransform>());

            yield return new WaitForEndOfFrame();

            ScrollRect_AllGroup.onValueChanged.AddListener(OnScrolling);

            yield return new WaitForEndOfFrame();

            _isInitialized = true;
        }

        onComplete?.Invoke();
    }

    public void UpdateAllCells()
    {
        Dictionary<int, Transform> tempGroup = new Dictionary<int, Transform>();
        //Container All
        if (Container_AllGroup.gameObject.activeSelf)
        {
            foreach (KeyValuePair<string, SeasonPassQuestCell> questCell in _questCellDict)
            {
                UpdateSpecificCell(questCell.Key);
            }

            tempGroup = new Dictionary<int, Transform>(_groupWeekly);
        }
        //Container WEEKLY
        else if (Container_Weekly.gameObject.activeSelf)
        {
            foreach (SeasonPassQuestCell cell in Container_Weekly.GetComponentsInChildren<SeasonPassQuestCell>())
            {
                cell.UpdateData();
            }
            tempGroup.Add(-1, Container_Weekly);
        }

        bool anyClaimable = false;

        //First check daily/weekly quest
        if (_allQuestIDList != null && _allQuestIDList.Count > 0) //If this is not null
        {
            foreach (string qID in _allQuestIDList[0])
            {
                SeasonPassQuestCell qCell;
                if (_questCellDict.TryGetValue(qID, out qCell))
                {
                    if (qCell.QuestData.IsCompleted && !qCell.QuestData.IsCleared && qCell.IsQuestAvailable)
                    {
                        //completed and ready to claim
                        anyClaimable = true;
                    }
                }
            }
        }

        //Update each group's children;
        //Weekly Quest
        foreach (KeyValuePair<int, Transform> group in tempGroup)
        {
            string headerText = "";
            //if (group.Key == 0)
            //{
            //    headerText = LocalizationManager.Instance.GetText("TEXT_SEASONPASS_DAILY");
            //}
            //else if(group.Key > 0)
            if (group.Key > 0)
            {
                headerText = string.Format(LocalizationManager.Instance.GetText("TEXT_WEEKNO"), group.Key);
            }

            //Sorting
            //Get position list
            DataTable sortingTable = new DataTable();
            sortingTable.Columns.Add("QID", typeof(string));
            sortingTable.Columns.Add("DATACELL", typeof(SeasonPassQuestCell));
            sortingTable.Columns.Add("QSTATE", typeof(string));

            List<Vector2> positionList = new List<Vector2>();
            for(int i = 0; i < group.Value.transform.childCount; i++)
            {
                Transform child = group.Value.transform.GetChild(i);
                SeasonPassQuestCell childCell = child.GetComponent<SeasonPassQuestCell>();
                if (childCell == null) continue;

                positionList.Add(childCell.gameObject.transform.localPosition);

                int questState = 0;

                if (childCell.QuestData.IsCompleted && !childCell.QuestData.IsCleared && childCell.IsQuestAvailable)
                {
                    //completed and ready to claim
                    anyClaimable = true;
                }
                else if (childCell.QuestData.IsCleared)
                {
                    //completely cleared
                    questState = 2;
                }
                else
                {
                    //in progress
                    questState = 1;
                }

                DataRow newRow = sortingTable.NewRow();
                newRow["QID"] = childCell.QuestData.ID.ToString();
                newRow["DATACELL"] = childCell;
                newRow["QSTATE"] = questState.ToString();
                sortingTable.Rows.Add(newRow);
            }
            sortingTable.DefaultView.Sort = "QSTATE,QID ASC"; //Sort
            sortingTable = sortingTable.DefaultView.ToTable(); //Convert to sorted table

            //Set each sibling's local position according to order sorted by datatable;
            for (int i = 0; i < sortingTable.Rows.Count; i++)
            {
                SeasonPassQuestCell cellObj = (SeasonPassQuestCell)sortingTable.Rows[i]["DATACELL"];
                cellObj.transform.SetSiblingIndex(i);
                cellObj.transform.localPosition = positionList[i];

                //Show / Hide Header and set linebreak sprite;
                if (i == 0 && headerText != "") //First  children
                {
                    cellObj.ShowHeaderTab(true);
                    //cellObj.Text_Type.text = headerText;
                    cellObj.ShowLinebreakImage(true);
                }
                else if (i == (sortingTable.Rows.Count - 1)) //Last children
                {
                    cellObj.ShowHeaderTab(false);
                    //cellObj.Text_Type.text = headerText;
                    cellObj.ShowLinebreakImage(false);
                }
                else
                {
                    cellObj.ShowHeaderTab(false);
                    cellObj.ShowLinebreakImage(true);
                }
            }
            
        }
        
        Button_ClaimAll.gameObject.SetActive(anyClaimable);

        ToggleVisibleCell();
        UpdateHeaderText();
    }

    public void UpdateSpecificCell(string qID)
    {
        _questCellDict[qID].UpdateData();
    }

    public void ShowNextWeek()
    {
        _currentPage++;
        UpdateHeaderText();
        UpdateNavigationArrow();

        ChangeQuestGroup();
    }

    public void ShowPrevWeek()
    {
        _currentPage--;
        UpdateHeaderText();
        UpdateNavigationArrow();

        ChangeQuestGroup();
    }

    private void ChangeQuestGroup()
    {
        if(_changePageSeq != null)
        {
            _changePageSeq.Kill();
            _changePageSeq = null;
        }

        if (_currentPage == 0)
        {
            _changePageSeq = DOTween.Sequence();
            _changePageSeq.Pause();
            _changePageSeq.Append(GameHelper.UITransition_FadeOut(Container_Weekly.gameObject, 
                delegate 
                {
                    ScrollRect_AllGroup.verticalNormalizedPosition = 1;
                    ScrollRect_AllGroup.content = Container_AllGroup.GetComponent<RectTransform>();

                    UpdateAllCells();

                    //Reset Container
                    ToggleVisibleCell();
                }
                ));
            _changePageSeq.Append(GameHelper.UITransition_FadeIn(Container_AllGroup.gameObject, delegate { UpdateAllCells(); ToggleVisibleCell(); }));
            _changePageSeq.Play();
        }
        else
        {
            //[New] Change Page
            List<SeasonPassQuestList> weeklyQuestDataList = SeasonPassManager.Instance.GetAllMainWeekQuestDataList(); //All quest data;
            SeasonPassQuestList weekQuestData = weeklyQuestDataList[_currentPage-1];

            _changePageSeq = DOTween.Sequence();
            _changePageSeq.Pause();
            if (Container_AllGroup.gameObject.activeSelf) _changePageSeq.Append(GameHelper.UITransition_FadeOut(Container_AllGroup.gameObject));
            _changePageSeq.Append(GameHelper.UITransition_FadeOut(Container_Weekly.gameObject,
                delegate
                {
                    UpdateGroupChildren(weekQuestData, Container_Weekly.gameObject);
                    ScrollRect_AllGroup.verticalNormalizedPosition = 1;
                    ScrollRect_AllGroup.content = Container_Weekly.GetComponent<RectTransform>();
                }
                ));
            _changePageSeq.Play();
            _changePageSeq.OnComplete(delegate { GameHelper.UITransition_FadeIn(Container_Weekly.gameObject); UpdateAllCells(); ToggleVisibleCell(); });
        }
    }
    
    private void UpdateGroupChildren(SeasonPassQuestList weekQuestData, GameObject groupContainer)
    {
        List<List<QuestData>> playerWeekQuestList = SeasonPassManager.Instance.GetMainWeekQuestDataList();
        bool isWeekStarted = _currentPage <= playerWeekQuestList.Count;
        DateTime startDate = weekQuestData.StartDateTimeUTC;

        int childCount = groupContainer.transform.childCount;
        int loopCount;
        if (isWeekStarted)
        {
            loopCount = Mathf.Max(childCount, playerWeekQuestList[_currentPage - 1].Count);
        }
        else
        {
            loopCount = Mathf.Max(childCount, weekQuestData.QuestList.Count);
        }
        
        List<QuestData> questDataList;
        if (isWeekStarted) // if not yet unlocked
        {
            questDataList = playerWeekQuestList[_currentPage - 1];
        }
        else
        {
            questDataList = weekQuestData.GetQuestDataList();
        }

        for (int i = 0; i < loopCount; i++)
        {
            //if UI is more than data, hide ui;
            if (i >= questDataList.Count)
            {
                if (!_permanantDisableList.Contains(groupContainer.transform.GetChild(i).gameObject))
                {
                    _permanantDisableList.Add(groupContainer.transform.GetChild(i).gameObject);
                }
                groupContainer.transform.GetChild(i).gameObject.SetActive(false);
            }
            //if UI is less than data, create new ui;
            else if (i >= groupContainer.transform.childCount)
            {
                SeasonPassQuestUIData questUIData = new SeasonPassQuestUIData(questDataList[i]);
                SeasonPassQuestCell questCell = CreateReward(questUIData, false, groupContainer.transform);
                questCell.Button_Go.gameObject.SetActive(false);
                questCell.Button_Get.gameObject.SetActive(false);
                questCell.SetPremiumOnlyEvent(SeasonPassUIManager.Instance.OnClickBuyPremiumPass);
                questCell.Text_UnlockDate.text = string.Format(LocalizationManager.Instance.GetText("TEXT_SSPASS_TASKUNLOCK"), weekQuestData.StartDateTimeUTC.ToString("dd/MM/yyyy"));
                questCell.IsQuestAvailable = isWeekStarted;
                if (!isWeekStarted)
                {
                    questCell.ToggleComingSoon(true);
                    questCell.Button_Go.gameObject.SetActive(false);
                    questCell.Button_Get.gameObject.SetActive(false);
                }
                else
                {
                    questCell.ToggleComingSoon(false);
                    questCell.Button_Go.gameObject.SetActive(!questCell.QuestData.IsCompleted && !questCell.QuestData.IsCleared && isWeekStarted);
                    questCell.Button_Get.gameObject.SetActive(questCell.QuestData.IsCompleted && !questCell.QuestData.IsCleared && isWeekStarted);
                }

                questCell.SetGetEvent(
                    delegate {
                        SeasonPassUIManager.Instance.CompleteQuest(questUIData.ID, true);
                    });
                questCell.SetGoEvent(delegate { SeasonPassUIManager.Instance.PageNavigate(questUIData.LocalData.GoFuncName, questUIData.LocalData.GoFuncParam); });
                questCell.ShowLinebreakImage(true);
            }
            //Update UI;
            else
            {
                SeasonPassQuestUIData questUIData = new SeasonPassQuestUIData(questDataList[i]);
                GameObject cellObj = groupContainer.transform.GetChild(i).gameObject;
                SeasonPassQuestCell questCell = cellObj.GetComponent<SeasonPassQuestCell>();

                if (questCell != null)
                {
                    questCell.InitializeData(questUIData, false);
                    questCell.Button_Go.gameObject.SetActive(false);
                    questCell.Button_Get.gameObject.SetActive(false);
                    questCell.SetPremiumOnlyEvent(SeasonPassUIManager.Instance.OnClickBuyPremiumPass);
                    questCell.Text_UnlockDate.text = string.Format(LocalizationManager.Instance.GetText("TEXT_SSPASS_TASKUNLOCK"), weekQuestData.StartDateTimeUTC.ToString("dd/MM/yyyy"));
                    questCell.IsQuestAvailable = isWeekStarted;
                    if (!isWeekStarted)
                    {
                        questCell.ToggleComingSoon(true);
                        questCell.Button_Go.gameObject.SetActive(false);
                        questCell.Button_Get.gameObject.SetActive(false);
                    }
                    else
                    {
                        questCell.ToggleComingSoon(false);
                        questCell.Button_Go.gameObject.SetActive(!questCell.QuestData.IsCompleted && !questCell.QuestData.IsCleared && isWeekStarted);
                        questCell.Button_Get.gameObject.SetActive(questCell.QuestData.IsCompleted && !questCell.QuestData.IsCleared && isWeekStarted);
                    }

                    questCell.SetGetEvent(
                        delegate {
                            SeasonPassUIManager.Instance.CompleteQuest(questUIData.ID, true);
                        });
                }

                if (_permanantDisableList.Contains(cellObj))
                {
                    _permanantDisableList.Remove(cellObj);
                }
                cellObj.SetActive(true);
            }
        }
    }

    private void InitButton()
    {
        Button_PrevPage.onClick.RemoveAllListeners();
        Button_PrevPage.onClick.AddListener(delegate { ShowPrevWeek(); });

        Button_NextPage.onClick.RemoveAllListeners();
        Button_NextPage.onClick.AddListener(delegate { ShowNextWeek(); });

        Button_ClaimAll.onClick.RemoveAllListeners();
        Button_ClaimAll.onClick.AddListener(delegate { SeasonPassUIManager.Instance.CompleteQuest(); });
    }

    private void ResetUIFlow()
    {
        _currentPage = 0;
        UpdateHeaderText();
        UpdateNavigationArrow();
        ChangeQuestGroup();
    }

    private void UpdateNavigationArrow()
    {
        Button_PrevPage.gameObject.SetActive(_currentPage > 0);
        Button_NextPage.gameObject.SetActive(_currentPage < _weekCount);
    }

    private void UpdateHeaderText()
    {
        int questCount = 0;
        int finishedQuest = 0;

        if (_currentPage <= 0)
        {
            //ALL QUESTS
            Text_CurrentGroup.text = LocalizationManager.Instance.GetText("TEXT_ALLQUEST");
            foreach(KeyValuePair<int, List<string>> questIDList in _allQuestIDList)
            {
                foreach(string qID in questIDList.Value)
                {
                    questCount++;
                    if (_questCellDict[qID].QuestData.IsCleared) finishedQuest++;
                }
            }
        }
        else
        {
            //SPECIFIC WEEK QUEST
            Text_CurrentGroup.text = string.Format(LocalizationManager.Instance.GetText("TEXT_WEEKNO"), _currentPage);
            foreach (string qID in _allQuestIDList[_currentPage])
            {
                questCount++;
                if (_questCellDict[qID].QuestData.IsCleared) finishedQuest++;
            }
        }

        Text_QuestCount.text = string.Format("{0}/{1}", finishedQuest, questCount);
    }

    private void ToggleVisibleCell()
    {
        RectTransform rect_cell = null;

        foreach (KeyValuePair<string, SeasonPassQuestCell> qCell in _questCellDict)
        {
            rect_cell = qCell.Value.GetComponent<RectTransform>();
            if (GameHelper.IsRectTouchRect(rect_cell, Viewport_AllGroup))
            {
                if (!_permanantDisableList.Contains(qCell.Value.gameObject))
                {
                    qCell.Value.gameObject.SetActive(true);

                    // add seen list
                    PlayerSeenListManager.Instance.AddSeenListData(SeenItemTypes.SeasonPassQuest, false, qCell.Value.QuestData.ID);
                }
            }
            else
            {
                qCell.Value.gameObject.SetActive(false);
            }
        }
    }

    private IEnumerator InitializeResetableQuest()
    {
        GameObject newResetableGroup = Instantiate(Group_CellGroup, Container_AllGroup);
        newResetableGroup.name = "Group_Resetable";
        newResetableGroup.SetActive(true);
        _groupResetable = newResetableGroup.transform;

        List<QuestData> combinedQuestList = new List<QuestData>();
        List<QuestData> dailyQuestList = SeasonPassManager.Instance.GetMainDailyQuestDataList();
        List<QuestData> weeklyQuestList = SeasonPassManager.Instance.GetMainWeeklyQuestDataList();
        combinedQuestList.AddRange(dailyQuestList);
        combinedQuestList.AddRange(weeklyQuestList);

        List<string> questIDList = new List<string>();
        foreach (QuestData qData in combinedQuestList)
        {
            //DailyQuest
            SeasonPassQuestUIData data = new SeasonPassQuestUIData(qData);
            SeasonPassQuestCell questCell = CreateReward(data, true, _groupResetable.transform);

            questCell.SetPremiumOnlyEvent(SeasonPassUIManager.Instance.OnClickBuyPremiumPass);
            questCell.SetGetEvent(delegate { SeasonPassUIManager.Instance.CompleteQuest(qData.ID, true); });
            questCell.SetGoEvent(delegate { SeasonPassUIManager.Instance.PageNavigate(data.LocalData.GoFuncName, data.LocalData.GoFuncParam); });

            //Show Soft Linebreak
            questCell.ShowLinebreakImage(true);

            _questCellDict.Add(qData.ID, questCell);

            questIDList.Add(qData.ID);

            yield return new WaitForEndOfFrame();
        }
        //GROUP/WEEK 0 is resetable quest group
        _allQuestIDList.Add(0, questIDList);

        //Categotize daily/weekly quest
        DateTime currentDT = DateTimeData.GetDateTimeUTC();
        //DAILY
        //Show Header Text on first daily cell
        if (dailyQuestList.Count > 0)
        {
            string firstDailyQuestID = dailyQuestList[0].ID;
            SeasonPassQuestCell firstDailyCell = _questCellDict[firstDailyQuestID];
            firstDailyCell.ShowHeaderTab(true);
            firstDailyCell.StartTimer(GameHelper.GetNextDayUTC(currentDT));
            firstDailyCell.Text_Type.text = LocalizationManager.Instance.GetText("TEXT_SEASONPASS_DAILY");
            //Show hard linebreak on last cell
            string lastDailyQuestID = dailyQuestList[dailyQuestList.Count - 1].ID;
            SeasonPassQuestCell lastDailyCell = _questCellDict[lastDailyQuestID];
            lastDailyCell.ShowLinebreakImage(false);
        }
        //WEEKLY
        //Show Header Text on first daily cell
        if (weeklyQuestList.Count > 0)
        {
            string firstWeeklyQuestID = weeklyQuestList[0].ID;
            SeasonPassQuestCell firstWeeklyCell = _questCellDict[firstWeeklyQuestID];
            firstWeeklyCell.ShowHeaderTab(true);
            firstWeeklyCell.StartTimer(GameHelper.GetNextWeekDate(currentDT));
            firstWeeklyCell.Text_Type.text = LocalizationManager.Instance.GetText("TEXT_WEEKLY_QUEST");
            //Show hard linebreak on last cell
            string lastWeeklyQuestID = weeklyQuestList[weeklyQuestList.Count - 1].ID;
            SeasonPassQuestCell lastWeeklyCell = _questCellDict[lastWeeklyQuestID];
            lastWeeklyCell.ShowLinebreakImage(false);
        }

        LayoutRebuilder.ForceRebuildLayoutImmediate(_groupResetable.GetComponent<RectTransform>());
    }

    private IEnumerator InitializeWeeklyQuest()
    {
        List<List<QuestData>> playerWeekQuestList = SeasonPassManager.Instance.GetMainWeekQuestDataList();
        List<SeasonPassQuestList> weeklyQuestDataList = SeasonPassManager.Instance.GetAllMainWeekQuestDataList(); //All quest data;

        //==============================================================================
        int weekIndex = 1;
        foreach (SeasonPassQuestList weeklyQuestData in weeklyQuestDataList)
        {
            List<QuestData> weeklyQuestList = weeklyQuestData.GetQuestDataList();
            if (weeklyQuestList.Count > 0)
            {
                List<QuestData> questDataList;

                DateTime weekStartdate = weeklyQuestData.StartDateTimeUTC;
                string weekHeaderText = string.Format(LocalizationManager.Instance.GetText("TEXT_WEEKNO"), weekIndex);
                bool questAvailable = weekIndex <= playerWeekQuestList.Count;
                if (questAvailable) // if not yet unlocked
                {
                    questDataList = playerWeekQuestList[weekIndex-1];
                }
                else
                {
                    questDataList = weeklyQuestList;
                    weekHeaderText += string.Format("<color=#8D6F3E>         {0}", weekStartdate.ToString("dd/MM/yyyy"));
                }

                GameObject weekGroup = Instantiate(Group_CellGroup, Container_AllGroup);
                weekGroup.name = string.Format("Group_Week_{0}", weekIndex);
                weekGroup.SetActive(true);
                _groupWeekly.Add(weekIndex, weekGroup.transform);
                List<string> weekQuestIDList = new List<string>();
                foreach (QuestData qData in questDataList)
                {
                    SeasonPassQuestUIData data = new SeasonPassQuestUIData(qData);
                    SeasonPassQuestCell questCell = CreateReward(data, false, weekGroup.transform);
                    questCell.SetPremiumOnlyEvent(SeasonPassUIManager.Instance.OnClickBuyPremiumPass);
                    //questCell.SetGetEvent(delegate { SeasonPassManager.Instance.CheckSeasonPassQuest(quest.ID, OnCheckSeasonPassQuestComplete, OnCheckSeasonPassQuestFail); });
                    questCell.SetGetEvent(delegate { SeasonPassUIManager.Instance.CompleteQuest(qData.ID, true); });
                    questCell.SetGoEvent(delegate { SeasonPassUIManager.Instance.PageNavigate(data.LocalData.GoFuncName, data.LocalData.GoFuncParam); });
                    questCell.ShowLinebreakImage(true);
                    questCell.Text_Type.text = weekHeaderText;
                    questCell.Text_UnlockDate.text = string.Format(LocalizationManager.Instance.GetText("TEXT_SSPASS_TASKUNLOCK"), weekStartdate.ToString("dd/MM/yyyy"));
                    questCell.IsQuestAvailable = questAvailable;
                    if(!questAvailable)
                    {
                        questCell.ToggleComingSoon(true);
                        questCell.Button_Go.gameObject.SetActive(false);
                        questCell.Button_Get.gameObject.SetActive(false);
                    }

                    _questCellDict.Add(data.ID, questCell);
                    weekQuestIDList.Add(data.ID);

                    yield return new WaitForEndOfFrame();
                }
                _allQuestIDList.Add(weekIndex, weekQuestIDList);

                Transform firstChild = weekGroup.transform.GetChild(0);
                SeasonPassQuestCell firstCell = firstChild.GetComponent<SeasonPassQuestCell>();
                firstCell.ShowHeaderTab(true);

                LayoutRebuilder.ForceRebuildLayoutImmediate(weekGroup.GetComponent<RectTransform>());
            }

            weekIndex++;
        }
        //==============================================================================
        /*
        weekIndex = 1;
        foreach (List<QuestData> weeklyQuestList in playerWeekQuestList)
        {
            if (weeklyQuestList.Count > 0)
            {
                GameObject weekGroup = Instantiate(Group_CellGroup, Container_AllGroup);
                weekGroup.name = string.Format("Group_Week_{0}", weekIndex);
                weekGroup.SetActive(true);
                _groupWeekly.Add(weekIndex, weekGroup.transform);
                List<string> weekQuestIDList = new List<string>();
                foreach (QuestData qData in weeklyQuestList)
                {
                    SeasonPassQuestUIData data = new SeasonPassQuestUIData(qData);
                    SeasonPassQuestCell questCell = CreateReward(data, false, weekGroup.transform);
                    questCell.SetPremiumOnlyEvent(SeasonPassUIManager.Instance.OnClickBuyPremiumPass);
                    //questCell.SetGetEvent(delegate { SeasonPassManager.Instance.CheckSeasonPassQuest(quest.ID, OnCheckSeasonPassQuestComplete, OnCheckSeasonPassQuestFail); });
                    questCell.SetGetEvent(delegate { SeasonPassUIManager.Instance.CompleteQuest(qData.ID, true); });
                    questCell.SetGoEvent(delegate { SeasonPassUIManager.Instance.PageNavigate(data.LocalData.GoFuncName, data.LocalData.GoFuncParam); });
                    questCell.ShowLinebreakImage(true);
                    
                    _questCellDict.Add(data.ID, questCell);
                    weekQuestIDList.Add(data.ID);

                    yield return new WaitForEndOfFrame();
                }
                _allQuestIDList.Add(weekIndex, weekQuestIDList);

                Transform firstChild = weekGroup.transform.GetChild(0);
                SeasonPassQuestCell firstCell = firstChild.GetComponent<SeasonPassQuestCell>();
                firstCell.ShowHeaderTab(true);
                string weekText = string.Format(LocalizationManager.Instance.GetText("TEXT_WEEKNO"), weekIndex);
                if (false) // if not yet unlock
                {
                    weekText += string.Format("<color=#8D6F3E> {0}", "");
                }
                firstCell.Text_Type.text = weekText;

                LayoutRebuilder.ForceRebuildLayoutImmediate(weekGroup.GetComponent<RectTransform>());
            }

            weekIndex++;
        }
        */
        _weekCount = _groupWeekly.Count;
    }

    #endregion

    #region Cell Gen
    private SeasonPassQuestCell CreateReward(SeasonPassQuestUIData data, bool showTimer, Transform parent)
    {
        GameObject obj = Instantiate(Prefab_QuestCell, parent);
        obj.SetActive(true);
        SeasonPassQuestCell cell = obj.GetComponent<SeasonPassQuestCell>();
        cell.InitializeData(data, showTimer);
        return cell;
    }
    #endregion
}
