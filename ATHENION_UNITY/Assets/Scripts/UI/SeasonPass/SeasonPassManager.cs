﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

#region Global Structs
public struct PriceData
{
    #region Public Properties
    public VirtualCurrency CurrencyType { get { return _currencyType; } }
    public int Amount { get { return _amount; } }
    #endregion

    #region Private Properties
    private VirtualCurrency _currencyType;
    private int _amount;
    #endregion

    public PriceData(VirtualCurrency virtualCurrency, int amount)
    {
        _currencyType = virtualCurrency;
        _amount = amount;
    }
}
#endregion

public class SeasonPassManager : MonoSingleton<SeasonPassManager>
{
    #region Event
    public static event Action OnPurchasePremium;
    /// <summary>
    /// Callback On purchase tier by amount.
    /// </summary>
    public static event Action<int> OnPurchaseSeasonPassTier;
    public static event Action<int, List<ItemData>> OnCompleteSeasonPassTier;
    public static event Action<string> OnCompleteQuest;
    #endregion

    #region Public Properties
    #endregion

    #region Private Properties
    #endregion

    #region Methods
    public List<QuestData> GetMainDailyQuestDataList()
    {
        return DataManager.Instance.PlayerSeasonPassQuest.GetMainDailyQuestDataList();
    }

    public List<QuestData> GetMainWeeklyQuestDataList()
    {
        return DataManager.Instance.PlayerSeasonPassQuest.GetMainWeeklyQuestDataList();
    }

    public List<SeasonPassQuestList> GetAllMainWeekQuestDataList()
    {
        SeasonPassQuestListDBData data = QuestListDB.Instance.SeasonPassQuestList;
        List<SeasonPassQuestList> allQuestList = data.GetSeasonPassQuestListData_AllWeek(DataManager.Instance.GetCurrentSeasonPassIndex());
        return allQuestList;
    }

    public List<List<QuestData>> GetMainWeekQuestDataList()
    {
        return DataManager.Instance.PlayerSeasonPassQuest.GetMainWeekQuestDataList();
    }

    public List<QuestData> GetChallengeQuestDataList()
    {
        return DataManager.Instance.PlayerSeasonPassQuest.GetChallengeQuestDataList();
    }

    public PriceData GetSeasonPassPremiumPrice()
    {
        return new PriceData(VirtualCurrency.DI, SeasonPassPriceDB.Instance.GetSeasonPassPrice(SeasonPassPriceTypes.Premium, VirtualCurrency.DI));
    }

    public PriceData GetSeasonPassTierPrice()
    {
        return new PriceData(VirtualCurrency.DI, SeasonPassPriceDB.Instance.GetSeasonPassPrice(SeasonPassPriceTypes.Tier, VirtualCurrency.DI));
    }

    public bool IsSeasonPassPremiumCanPurchase()
    {
        return DataManager.Instance.PlayerSeasonPass.IsBePremium() == false;
    }

    public bool IsSeasonPassBePremium()
    {
        return DataManager.Instance.PlayerSeasonPass.IsBePremium();
    }

    public void CheckSeasonPassQuest(string questID, UnityAction<List<ItemData>> onComplete, UnityAction<string> onFail)
    {
        DataManager.Instance.CheckServerSeasonPassQuest(
            questID
            , (result) =>
            {
                OnCompleteQuest?.Invoke(questID);
                onComplete?.Invoke(result);
            }
            , onFail
        );
    }

    public void CheckSeasonPassAllQuests(UnityAction<List<ItemData>> onComplete, UnityAction<string> onFail)
    {
        DataManager.Instance.CheckServerSeasonPassAllQuests(
            (resultQuestID) =>
            {
                foreach(string questID in resultQuestID)
                {
                    OnCompleteQuest?.Invoke(questID);
                }
            }
            , onComplete
            , onFail
        );
    }

    public void CheckSeasonPassAllTierReward(UnityAction<List<ItemData>> onComplete, UnityAction<string> onFail)
    {
        int currentTier = DataManager.Instance.PlayerSeasonPass.GetCurrentSeasonPassTier();
        DataManager.Instance.CheckSeasonPassAllTierReward(
            (result) =>
            {
                OnCompleteSeasonPassTier?.Invoke(currentTier, result);
                onComplete?.Invoke(result);
            }
            , onFail
        );
    }

    public void CheckSeasonPassTierReward(int tier, UnityAction<List<ItemData>> onComplete, UnityAction<string> onFail)
    {
        int currentTier = DataManager.Instance.PlayerSeasonPass.GetCurrentSeasonPassTier();
        PlayerSeasonPassTierRewardRecordData recordData = DataManager.Instance.PlayerSeasonPassRewardRecord;
        if (recordData.IsRewardClaimed(tier))
        {
            onComplete?.Invoke(new List<ItemData>());
            return;
        }

        CheckSeasonPassAllTierReward((result) =>
        {
            OnCompleteSeasonPassTier?.Invoke(currentTier, result);
            onComplete?.Invoke(result);
        }, onFail);
    }

    public void PurchaseSeasonPassPremium(UnityAction<List<ItemData>> onComplete, UnityAction<string> onFail)
    {
        DataManager.Instance.PurchaseCurrentSeasonPassPremium(
            (result) =>
            {
                OnPurchasePremium?.Invoke();
                onComplete?.Invoke(result);
            }
            , onFail
        );
    }

    public void PurchaseSeasonPassTier(int tierAmount, UnityAction<List<ItemData>> onComplete, UnityAction<string> onFail)
    {
        DataManager.Instance.PurchaseCurrentSeasonPassTier(
            tierAmount
            , (result) =>
            {
                onComplete?.Invoke(result);
                OnPurchaseSeasonPassTier?.Invoke(tierAmount);
            }
            , onFail
        );
    }

    public SeasonPassTierRewardDBData GetCurrentSeasonPassTierRewardData()
    {
        SeasonPassTierRewardDB.Instance.GetData(DataManager.Instance.GetCurrentSeasonPassIndex().ToString(), out SeasonPassTierRewardDBData data);
        return data;
    }

    public PlayerSeasonPassData GetPlayerSeasonPassData()
    {
        return DataManager.Instance.PlayerSeasonPass;
    }

    public SeasonPassDBData GetCurrentSeasonPassDBData()
    {
        return SeasonPassDB.Instance.GetCurrentSeasonPassDBData();
    }
    #endregion
}