﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class SeasonPassQuestUIData : BaseGameObjectiveUIData
{
    #region Public Properties
    public bool IsRequiredPremium { get { return _isPremium; } }
    public QuestData LocalData { get { return _localData; } }
    #endregion

    #region Private Properties
    private bool _isPremium = false;
    private QuestData _localData;
    #endregion
    
    #region Constructor
    public SeasonPassQuestUIData()
    {

    }

    public SeasonPassQuestUIData(QuestData data)
    {
        SetupData(data);
        _localData = data;
        _isPremium = data.IsPremium;
    }
    #endregion

    #region Methods
    protected override string GetTitle()
    {
        return LocalizationManager.Instance.GetQuestTitle(ID);
    }

    protected override string GetDescription()
    {
        return LocalizationManager.Instance.GetQuestDescription(ID);
    }
    #endregion
}

public class SeasonPassQuestCell : MonoBehaviour
{
    #region Public Properties
    public bool IsQuestAvailable = true;
    [Header("Ref")]
    public GameObject Group_Header;
    public GameObject Group_Detail;
    public GameObject Group_RewardMulti;
    public GameObject Group_ProgressBar;
    public GameObject Group_ComingSoon;
    public GameObject MaskLayer_Completed;

    public GameObject Image_SoftLineBreak;
    public GameObject Image_HardLineBreak;
    public GameObject Image_PremiumBG;

    public TextMeshProUGUI Text_Type;
    public TextMeshProUGUI Text_TimeCounter;
    public TextMeshProUGUI Text_Description;
    public TextMeshProUGUI Text_Progression;
    public TextMeshProUGUI Text_UnlockDate;

    public RewardCell_SeasonPass Cell_SingleReward;
    public List<RewardCell_SeasonPass> Cell_DoubleRewardList;

    public GameObject Notice_New;

    public Slider Slider_ProgressionBar;

    public Button Button_Go;
    public Button Button_Get;
    public Button Button_PremiumOnly;
    
    public SeasonPassQuestUIData QuestData { get { return _data; } }
    #endregion

    #region Private Properties
    private bool _isInitialized = false;
    private bool _isShowTimer;
    private SeasonPassQuestUIData _data;
    private Coroutine _timer = null;
    private UnityAction _onGoClick;
    private UnityAction _onGetClick;
    private UnityAction _onPremiumOnlyClick;
    private List<ItemData> _itemDataList;
    private DateTime _countdownTime = DateTime.MinValue;
    #endregion

    #region Events
    private void OnEnable()
    {
        if(_timer != null)
        {
            StopCoroutine(_timer);
            _timer = null;
        }

        if(_isShowTimer) _timer = StartCoroutine(Timer(Text_TimeCounter));
    }

    private void OnDisable()
    {
        if (_timer != null)
        {
            StopCoroutine(_timer);
            _timer = null;
        }
    }
    #endregion

    #region Methods
    public void InitializeData(SeasonPassQuestUIData questUIData, bool isDaily)
    {
        _data = questUIData;
        _isShowTimer = isDaily;

        UpdateData();
    }

    public void InitializeData(SeasonPassQuestUIData questUIData)
    {
        InitializeData(questUIData, false);
    }

    public void UpdateData()
    {
        if (Text_TimeCounter != null) Text_TimeCounter.text = "";
        if (Text_Description != null) Text_Description.text = _data.Description;
        if (_isShowTimer && gameObject.activeSelf && gameObject.activeInHierarchy)
        {
            _timer = StartCoroutine(Timer(Text_TimeCounter));
        }
        
        if (Text_Progression != null) Text_Progression.text = string.Format("{0}/{1}", _data.ProgressCurrent, _data.ProgressMax);
        float barVal = (float)_data.ProgressCurrent / (float)_data.ProgressMax;
        if (Slider_ProgressionBar != null) Slider_ProgressionBar.value = barVal;

        //Update button
        bool isRequiredPremium = _data.IsRequiredPremium;
        bool isPlayable = false;
        if(isRequiredPremium)
        {
            if (Image_PremiumBG != null) Image_PremiumBG.SetActive(true);

            bool isPlayerPremium = SeasonPassManager.Instance.IsSeasonPassBePremium();
            if (isPlayerPremium && Button_PremiumOnly != null)
            {
                Button_PremiumOnly.gameObject.SetActive(false);
                isPlayable = true;
            }
            else
            {
                Button_PremiumOnly.gameObject.SetActive(true);
            }
        }
        else
        {
            if (Image_PremiumBG != null) Image_PremiumBG.SetActive(false);
            if (Button_PremiumOnly != null) Button_PremiumOnly.gameObject.SetActive(false);

            isPlayable = true;
        }

        if (isPlayable)
        {
            if (Button_Go != null) Button_Go.gameObject.SetActive(!_data.IsCompleted && !_data.IsCleared && IsQuestAvailable);
            if (Button_Get != null) Button_Get.gameObject.SetActive(_data.IsCompleted && !_data.IsCleared && IsQuestAvailable);

            //Completed Mask
            if (MaskLayer_Completed != null) MaskLayer_Completed.SetActive(_data.IsCompleted && _data.IsCleared);
        }
        else
        {
            //Completed Mask
            if (MaskLayer_Completed != null)
            {
                MaskLayer_Completed.SetActive(false);
            }
        }
        /*
        if (_data.IsCleared)
        {
            transform.SetAsLastSibling();
        }
        else if (_data.IsCompleted)
        {
            transform.SetAsFirstSibling();
        }
        */
        _itemDataList = _data.ItemDataList;
        if (_itemDataList.Count > 1)
        {
            //Show multiple reward. (max 2)
            Group_RewardMulti.SetActive(true);
            Cell_SingleReward.gameObject.SetActive(false);

            for(int i = 0; i < 2; i++ )
            {
                Cell_DoubleRewardList[i].gameObject.SetActive(true);
                Cell_DoubleRewardList[i].InitData(_itemDataList[i].ItemID, _itemDataList[i].Amount);
            }
        }
        else
        {
            //Show single reward.
            Group_RewardMulti.SetActive(false);
            if (_itemDataList.Count > 0)
            {
                Cell_SingleReward.gameObject.SetActive(true);
                Cell_SingleReward.InitData(_itemDataList[0].ItemID, _itemDataList[0].Amount);
            }
            else
            {
                Cell_SingleReward.gameObject.SetActive(false);
            }
        }
        //if (Button_Go != null) Button_Go.gameObject.SetActive(false);

        SetNewNotice();
    }

    public void ShowCompletedMask(bool show)
    {
        if (MaskLayer_Completed != null) MaskLayer_Completed.SetActive(show);
    }
    public void ShowPremiumBG(bool show)
    {
        if (Image_PremiumBG != null) Image_PremiumBG.SetActive(show);
    }
    public void ShowHeaderTab(bool show)
    {
        if (Group_Header != null) Group_Header.SetActive(show);
    }
    public void ShowGoButton(bool show)
    {
        if (Button_Go != null) Button_Go.gameObject.SetActive(show);
    }
    public void ShowGetButton(bool show)
    {
        if (Button_Get != null) Button_Get.gameObject.SetActive(show);
    }
    public void ShowPremiumButton(bool show)
    {
        if (Button_PremiumOnly != null) Button_PremiumOnly.gameObject.SetActive(show);
    }
    public void ShowLinebreakImage(bool showSoftLinebreak)
    {
        if (Image_SoftLineBreak != null) Image_SoftLineBreak.SetActive(showSoftLinebreak);
        if (Image_HardLineBreak != null) Image_HardLineBreak.SetActive(!showSoftLinebreak);
    }
    public void ToggleComingSoon(bool isComingSoon)
    {
        Group_ComingSoon.SetActive(isComingSoon);
        Group_ProgressBar.SetActive(!isComingSoon);
        Text_Description.gameObject.SetActive(!isComingSoon);
    }
    public void SetGoEvent(UnityAction action)
    {
        if (Button_Go != null)
        {
            Button_Go.onClick.RemoveAllListeners();
            Button_Go.onClick.AddListener(action);
        }
    }
    public void SetGetEvent(UnityAction action)
    {
        if (Button_Get != null)
        {
            Button_Get.onClick.RemoveAllListeners();
            Button_Get.onClick.AddListener(action);
        }
    }
    public void SetPremiumOnlyEvent(UnityAction action)
    {
        if (Button_PremiumOnly != null)
        {
            Button_PremiumOnly.onClick.RemoveAllListeners();
            Button_PremiumOnly.onClick.AddListener(action);
        }
    }
    public void StartTimer(DateTime countdownTime)
    {
        _countdownTime = countdownTime;

        if (_isShowTimer) _timer = StartCoroutine(Timer(Text_TimeCounter));
    }

    public void SetNewNotice()
    {
        if(Notice_New == null)
        {
            return;
        }

        if (_data.LocalData.Type == QuestTypes.SeasonPassMainWeek)
        {
            Notice_New.SetActive(_data.LocalData.IsNew);
        }
        else
        {
            Notice_New.SetActive(false);
        }
    }
    #endregion

    #region Coroutine
    protected IEnumerator Timer(TextMeshProUGUI cooldownText = null, UnityAction onComplete = null)
    {
        while (true)
        {
            if (_countdownTime != DateTime.MinValue)
            {
                DateTime currentDateTime = DateTimeData.GetDateTimeUTC();
                //DateTime nextDateTime = GameHelper.GetNextDayUTC(currentDateTime);
                int daysRemain = (_countdownTime - currentDateTime).Days;
                float secondsUntilNextDay = (float)(_countdownTime - currentDateTime).TotalSeconds;

                if(daysRemain > 0)
                {
                    string time = string.Format(LocalizationManager.Instance.GetText("TEXT_DAY"), daysRemain);
                    cooldownText.text = string.Format(LocalizationManager.Instance.GetText("TEXT_SSPASS_DAILYTIMER"), time);
                }
                else if (secondsUntilNextDay > 0)
                {
                    string time = "";
                    int h, m, s;
                    h = Mathf.FloorToInt(secondsUntilNextDay / 3600);
                    m = Mathf.FloorToInt((secondsUntilNextDay - h * 3600) / 60);
                    s = Mathf.FloorToInt(secondsUntilNextDay % 60);
                    if (h > 0)
                    {
                        if (cooldownText != null)
                            time = $"{h:D2}:{m:D2}:{s:D2}";
                    }
                    else
                    {
                        if (cooldownText != null)
                            time = $"{m:D2}:{s:D2}";
                    }

                    cooldownText.text = string.Format(LocalizationManager.Instance.GetText("TEXT_SSPASS_DAILYTIMER"), time);
                }
                else
                {
                    onComplete?.Invoke();
                    yield break;
                }
            }

            yield return new WaitForSeconds(1f);
        }
    }
    #endregion
}
