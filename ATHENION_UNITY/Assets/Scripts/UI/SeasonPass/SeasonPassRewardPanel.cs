﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;
using DG.Tweening;
using UnityEngine.Events;
using System.Linq;
using System;

public class SeasonPassRewardPanel : MonoBehaviour
{
    #region Public Properties
    [Header("Prefabs")]
    public GameObject Prefab_LevelCell;

    [Header("Ref")]
    public SeasonPassLevelCell Cell_NextBigReward;
    public ScrollRect ScrollRect_Level;
    public Scrollbar ScrollBar_Level;
    public RectTransform Rect_ViewPort;
    public Transform Container_Level;
    public Button Button_ToNextReward;
    public Image Image_TierPrizeImage;
    #endregion

    #region Private Properties
    private bool _isInitialized = false;
    private Dictionary<int,SeasonPassLevelCell> _rewardCellDict = new Dictionary<int, SeasonPassLevelCell>();
    private Dictionary<int, SeasonPassRewardNavigator> _rewardNavigatorDict = new Dictionary<int, SeasonPassRewardNavigator>();
    private int _magicSeqNo = 10;
    private int _currentlyShowReward = 0;
    private int _nextBigPrizeLevel = 0;
    private List<int> _showList = new List<int>();
    private Coroutine _initCoroutine;
    private Tweener _scrollRect_Tweener = null;
    #endregion

    #region Events
    public static event Action OnFinishedInit;
    #endregion

    #region Methods
    private void InitializeData()
    {
        if (!_isInitialized)
        {
            UnityAction onCompleteInit = () =>
            {
                _isInitialized = true;

                Button_ToNextReward.onClick.RemoveAllListeners();
                Button_ToNextReward.onClick.AddListener(delegate { MoveToTarget(_nextBigPrizeLevel/*int.Parse(Cell_NextBigReward.Text_Level.text)*/); });

                UpdateData();

                OnFinishedInit?.Invoke();
            };

            _initCoroutine = StartCoroutine(OnInit(onCompleteInit));
        }
        else
        {
            UpdateData();
        }
    }

    public void UpdateData()
    {
        //Update reward cells
        for(int i = 1; i <= _rewardCellDict.Count; i++)
        {
            UpdateRewardCell(i);
        }
    }

    private void UpdateNextReward(int cellLevel)
    {
        float range = (float)cellLevel / (float)_magicSeqNo;
        int showRange = Mathf.CeilToInt(range) * _magicSeqNo;

        if (_rewardCellDict.ContainsKey(showRange))
        {
            ShowNextReward(showRange);
        }
    }

    private void ShowNextReward(int cellLevel)
    {
        _currentlyShowReward = cellLevel;
        /*
        if (!Cell_NextBigReward.gameObject.activeSelf)
        {
            Cell_NextBigReward.gameObject.SetActive(true);
        }
        SeasonPassLevelCell copyCell = _rewardCellDict[cellLevel];
        Cell_NextBigReward.SetupData(cellLevel);
        Cell_NextBigReward.UpdateReward(true, copyCell.PremiumRewardData);
        Cell_NextBigReward.UpdateReward(false, copyCell.FreeRewardData);
        */
        //Hide old method.
        //Cell_NextBigReward.gameObject.SetActive(false);
        _nextBigPrizeLevel = cellLevel;

        Sprite spr = SpriteResourceHelper.LoadSprite(string.Format("SSPass_Tier{0}", cellLevel), SpriteResourceHelper.SpriteType.SSPass_CustomImage, true);
        if (spr == null)
        {
            Button_ToNextReward.gameObject.SetActive(false);
        }
        else
        {
            Button_ToNextReward.gameObject.SetActive(true);
            if (!Image_TierPrizeImage.gameObject.activeSelf)
            {
                Image_TierPrizeImage.gameObject.SetActive(true);
            }
            Image_TierPrizeImage.sprite = spr;
        }
    }

    private void MoveToTarget(int level)
    {
        RectTransform contRect = Container_Level.GetComponent<RectTransform>();
        RectTransform cellRect = _rewardCellDict[level].GetComponent<RectTransform>();
        VerticalLayoutGroup layout_v = Container_Level.GetComponent<VerticalLayoutGroup>();

        float pos_y = Mathf.Abs(cellRect.transform.localPosition.y);
        float cell_height = cellRect.rect.height;
        float viewport_height = Rect_ViewPort.rect.height;
        float max_offset_y = contRect.rect.height - viewport_height;
        float center_offset = (cell_height * 0.5f) - (viewport_height * 0.5f);

        float container_y = pos_y + center_offset;
        container_y = Mathf.Clamp(container_y, 0.0f, max_offset_y);

        //Container_Level.transform.localPosition = new Vector3(
        //      Container_Level.transform.localPosition.x
        //    , container_Y
        //    , Container_Level.transform.localPosition.z
        //);

        ScrollRect_Level.velocity = Vector2.zero; // stop all movement.

        if (_scrollRect_Tweener != null)
        {
            _scrollRect_Tweener.Kill();
            _scrollRect_Tweener = null;
        }

        _scrollRect_Tweener = Container_Level.DOLocalMoveY(container_y, 0.45f).SetEase(Ease.OutExpo);
    }
    #endregion

    #region Events
    private void OnEnable()
    {
        if (!_isInitialized)
        {
            InitializeData();
        }
        else
        {
            MoveToTarget(SeasonPassManager.Instance.GetPlayerSeasonPassData().GetCurrentSeasonPassTier());
        }
    }

    private void OnDisable()
    {
        if (_scrollRect_Tweener != null)
        {
            _scrollRect_Tweener.Kill();
            _scrollRect_Tweener = null;
        }
    }

    public void OnCellDisabled(int cellLevel)
    {
        if (_showList.Contains(cellLevel)) _showList.Remove(cellLevel);
    }

    public void OnCellEnabled(int cellLevel)
    {
        if (!_showList.Contains(cellLevel)) _showList.Add(cellLevel);
        UpdateNextReward(_showList.Max());

        UpdateRewardCell(cellLevel);
    }

    private void UpdateRewardCell(int cellLevel)
    {
        PlayerSeasonPassData playerData = SeasonPassManager.Instance.GetPlayerSeasonPassData();
        int playerTier = playerData.GetCurrentSeasonPassTier();

        if (playerTier <= 0) return; //No Data
        if (!_rewardCellDict.ContainsKey(cellLevel)) { Debug.LogFormat("Dictionary is not containing cell level {0}", cellLevel); return; }

        if (playerTier < cellLevel)
        {
            _rewardCellDict[cellLevel].ShowDisableMask(true);
            _rewardCellDict[cellLevel].ShowHighlightMask(false);
            foreach (RewardCell_SeasonPass cell in _rewardCellDict[cellLevel].FreeRewardUI)
            {
                cell.ShowCheckIcon(false);
                cell.ShowLockIcon(true);
            }
            foreach (RewardCell_SeasonPass cell in _rewardCellDict[cellLevel].PremiumRewardUI)
            {
                cell.ShowCheckIcon(false);
                cell.ShowLockIcon(true);
            }
        }
        else if (playerTier > cellLevel)
        {
            _rewardCellDict[cellLevel].ShowDisableMask(false);
            foreach (RewardCell_SeasonPass cell in _rewardCellDict[cellLevel].FreeRewardUI)
            {
                cell.ShowCheckIcon(true);
            }
            foreach (RewardCell_SeasonPass cell in _rewardCellDict[cellLevel].PremiumRewardUI)
            {
                if (playerData.IsBePremium())
                {
                    cell.ShowCheckIcon(true);
                }
                else
                {
                    cell.ShowLockIcon(true);
                    cell.ShowCheckIcon(false);
                }
            }
            _rewardCellDict[cellLevel].ShowHighlightMask(false);
        }
        else
        {
            foreach (RewardCell_SeasonPass cell in _rewardCellDict[cellLevel].FreeRewardUI)
            {
                cell.ShowCheckIcon(true);
            }
            foreach (RewardCell_SeasonPass cell in _rewardCellDict[cellLevel].PremiumRewardUI)
            {
                if (playerData.IsBePremium())
                { 
                    cell.ShowCheckIcon(true);
                }
                else
                {
                    cell.ShowCheckIcon(false);
                    cell.ShowLockIcon(true);
                }
            }
            _rewardCellDict[cellLevel].ShowDisableMask(false);
            _rewardCellDict[cellLevel].ShowHighlightMask(true);
        }
    }
    #endregion

    #region Cell Gen & Update
    private IEnumerator OnInit(UnityAction onComplete)
    {
        PopupUIManager.Instance.Show_MidSoftLoad(true);

        int maxLevel;
        SeasonPassTierDB.Instance.GetAllData(out List<SeasonPassTierDBData> dataList);
        maxLevel = dataList.Count;
        for (int i = 1; i <= maxLevel; i++)
        {
            if (_rewardCellDict.ContainsKey(i))
            {
                _rewardCellDict[i].SetupData(i, null);
            }
            else
            {
                _rewardCellDict.Add(i, CreateReward(i, null));
            }

            _showList.Add(i);

            if (i % 3 == 0) //Create 3 items every frame;
            {
                yield return new WaitForEndOfFrame();
            }
        }

        SeasonPassTierRewardDBData ssRewardData = SeasonPassManager.Instance.GetCurrentSeasonPassTierRewardData();
        List<SeasonPassTierRewardData> rewList = ssRewardData.GetAllSeasonPassTierRewardData();
        foreach(SeasonPassTierRewardData rewardData in rewList)
        {
            _rewardCellDict[rewardData.TierLevel].UpdateReward(rewardData);
        }

        ScrollRect_Level.onValueChanged.AddListener(OnScrolling);

        ShowNextReward(_magicSeqNo);
        
        LayoutRebuilder.ForceRebuildLayoutImmediate(Container_Level.GetComponent<RectTransform>());

        onComplete?.Invoke();

        yield return new WaitForEndOfFrame();

        PopupUIManager.Instance.Show_MidSoftLoad(false);

        MoveToTarget(SeasonPassManager.Instance.GetPlayerSeasonPassData().GetCurrentSeasonPassTier());
    }

    private SeasonPassLevelCell CreateReward(int lvl, UnityAction onCellClick)
    {
        int level = lvl;
        GameObject obj = Instantiate(Prefab_LevelCell, Container_Level);
        obj.SetActive(true);
        SeasonPassLevelCell cell = obj.GetComponent<SeasonPassLevelCell>();
        cell.SetupData(lvl, onCellClick);

        // subscribe event
        cell.OnBeginDragEvent += OnLevelCell_BeginDrag;
        cell.OnDragEvent += OnLevelCell_Drag;
        cell.OnEndDragEvent += OnLevelCell_EndDrag;

        return cell;
    }

    private void UpdateReward(SeasonPassLevelCell cell, int lvl, SeasonPassTierRewardData reward)
    {
        cell.UpdateReward(reward);
    }

    private void OnScrolling(Vector2 value)
    {
        Container_Level.GetComponent<ContentSizeFitter>().enabled = false;
        Container_Level.GetComponent<VerticalLayoutGroup>().enabled = false;

        ToggleVisibleCell();
    }

    private void ToggleVisibleCell()
    {
        RectTransform rect_cell = null;

        foreach (KeyValuePair<int, SeasonPassLevelCell> cell in _rewardCellDict)
        {
            rect_cell = cell.Value.GetComponent<RectTransform>();
            if (GameHelper.IsRectTouchRect(rect_cell, Rect_ViewPort))
            {
                cell.Value.gameObject.SetActive(true);
            }
            else
            {
                cell.Value.gameObject.SetActive(false);
            }
        }
    }
    #endregion

    #region Event ScrollDeck
    public void OnLevelCell_BeginDrag(PointerEventData eventData)
    {
        ScrollRect_Level.OnBeginDrag(eventData);
    }

    public void OnLevelCell_Drag(PointerEventData eventData)
    {
        ScrollRect_Level.OnDrag(eventData);
    }

    public void OnLevelCell_EndDrag(PointerEventData eventData)
    {
        ScrollRect_Level.OnEndDrag(eventData);
    }
    #endregion
}
