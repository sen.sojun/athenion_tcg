﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;
using DG.Tweening;
using UnityEngine.Events;
using System.Linq;
using System;

public class SeasonPassRewardNavigator : MonoBehaviour
{
    #region Public Properties
    [Header("Ref")]
    public Image IMG_ShowImage;
    public Transform Container_PrizePreviewer;
    public int RewardTier { get { return _rewardTier; } private set { _rewardTier = value; } }
    #endregion

    #region Private Properties
    private int _rewardTier;
    private GameObject _previewObject;
    private Button _clickableButton;
    #endregion

    public void SetData(int rewardTier, Sprite img, GameObject preview, UnityAction onClick = null)
    {
        RewardTier = rewardTier;
        IMG_ShowImage.sprite = img;

        if(preview != null)
        {
            _previewObject = Instantiate(preview, Container_PrizePreviewer);
        }

        _clickableButton.onClick.RemoveAllListeners();
        if (onClick != null)
        {
            _clickableButton.onClick.AddListener(onClick);
        }
    }
}
