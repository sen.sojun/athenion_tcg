﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;
using UnityEngine.EventSystems;


public class SeasonPassHomeButton : MonoBehaviour
{
    #region Public Properties
    [Header("Ref")]
    public RewardCell_SeasonPass Prefab_RewardCell;
    public TextMeshProUGUI Text_Level;
    public TextMeshProUGUI Text_Exp;
    public TextMeshProUGUI Text_Header;
    public Slider Slider_Bar;
    public GameObject MaskLayer_Lock;
    #endregion

    #region Private Properties
    private bool _firstInit = false;
    private Button _buttonClick;
    #endregion

    #region Events
    private void OnEnable()
    {
        if(!_firstInit)
        {
            _firstInit = true;
            _buttonClick = GetComponent<Button>();
            Prefab_RewardCell.OnClickEvent += OnRewardCellClick;
            LocalizationManager.Instance.BindToggleLanguageEvent(UpdateData);
        }

        UpdateData();
    }

    private void OnDisable()
    {
        
    }

    private void OnDestroy()
    {
        Prefab_RewardCell.OnClickEvent -= OnRewardCellClick;
        LocalizationManager.Instance.UnbindToggleLanguageEvent(UpdateData);
    }
    #endregion

    #region Method
    public void UpdateData()
    {
        //MaskLayer_Lock.SetActive(DataManager.Instance.IsFeatureEnable(FeatureUnlock.SeasonPass));

        SeasonPassTierRewardDBData ssRewardData = SeasonPassManager.Instance.GetCurrentSeasonPassTierRewardData();

        if(ssRewardData == null)
        {
            _buttonClick.interactable = false;
            Prefab_RewardCell.gameObject.SetActive(false);
            Slider_Bar.gameObject.SetActive(false);
            Text_Exp.gameObject.SetActive(true);
            Text_Exp.text = LocalizationManager.Instance.GetText("TEXT_SSPASS_STARTINGSOON");
            Text_Header.text = LocalizationManager.Instance.GetText("TEXT_SEASONPASS");
            Text_Level.text = "1";
            return;
        }
        _buttonClick.interactable = true;

        PlayerSeasonPassData playerData = SeasonPassManager.Instance.GetPlayerSeasonPassData();

        int playerTier = playerData.GetCurrentSeasonPassTier();
        bool isPlayerPremium = SeasonPassManager.Instance.IsSeasonPassBePremium();

        SeasonPassTierDB.Instance.GetData(playerTier.ToString(), out SeasonPassTierDBData curTierData);
        SeasonPassTierDB.Instance.GetData((playerTier + 1).ToString(), out SeasonPassTierDBData nextTierData);

        Text_Level.text = playerTier.ToString();

        if (nextTierData != null)
        {
            Prefab_RewardCell.gameObject.SetActive(true);
            Slider_Bar.gameObject.SetActive(true);
            Text_Exp.gameObject.SetActive(false);

            int curExessiveScore = playerData.GetCurrentTierExcessiveScore();
            int requiredScore = nextTierData.TierAccScore - curTierData.TierAccScore;
            //Text_Exp.text = string.Format("{0}/{1}", curExessiveScore, requiredScore);
            Slider_Bar.value = (float)curExessiveScore / (float)requiredScore;

            ItemData shownItem = null;
            List<SeasonPassTierRewardData> rewDataList = ssRewardData.GetSeasonPassTierRewardDataList(playerTier + 1);
            foreach (SeasonPassTierRewardData rewData in rewDataList)
            {
                if (isPlayerPremium == rewData.IsPremium)
                {
                    if (rewData.ItemDataList.Count > 0)
                    {
                        shownItem = rewData.ItemDataList[0];
                        break;
                    }
                }
            }
            
            //Init Reward Icon
            if (shownItem != null)
            {
                Prefab_RewardCell.InitData(shownItem.ItemID, shownItem.Amount);
            }
            else
            {
                Prefab_RewardCell.SetAsEmptySlot();
            }
        }
        else //No next tier = max tier archieved
        {
            Prefab_RewardCell.gameObject.SetActive(false);
            Slider_Bar.gameObject.SetActive(false);
            Text_Exp.gameObject.SetActive(true);
            Text_Exp.text = LocalizationManager.Instance.GetText("TEXT_SSPASS_MAXLEVEL");
        }

        int ssIndex = SeasonPassManager.Instance.GetCurrentSeasonPassDBData() == null ? 0 : SeasonPassManager.Instance.GetCurrentSeasonPassDBData().SeasonPassIndex;
        string headerText = string.Format(LocalizationManager.Instance.GetText("TEXT_SSPASS_BUTTONHEADER"), ssIndex);
        Text_Header.text = headerText;

    }

    private void OnRewardCellClick(PointerEventData eventData)
    {
        _buttonClick.onClick?.Invoke();
    }
    #endregion
}
