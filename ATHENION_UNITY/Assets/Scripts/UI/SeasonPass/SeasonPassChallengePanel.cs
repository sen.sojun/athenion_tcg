﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;
using DG.Tweening;
using UnityEngine.Events;
using System.Linq;
using System;
using System.Data;

public class SeasonPassChallengePanel : MonoBehaviour
{
    #region Public Properties
    [Header("Prefabs")]
    public GameObject Prefab_ChallengeCell;

    [Header("Ref")]
    public RectTransform Rect_ViewPort;
    public ScrollRect ScrollRect_QuestGroup;
    public Transform Container_QuestList;
    #endregion

    #region Private Properties
    private bool _isInitialized = false;
    private Dictionary<string, SeasonPassQuestCell> _challengeCellDict = new Dictionary<string, SeasonPassQuestCell>();
    private Coroutine _initCoroutine = null;
    #endregion

    #region Event
    public static event Action OnFinishedInit;

    private void OnEnable()
    {
        SetupData();
    }

    private void OnDisable()
    {
        //OnFinishedInit -= AfterFinishInit;
    }

    private void OnScrolling(Vector2 value)
    {
        if (_isInitialized)
        {
            Container_QuestList.GetComponent<ContentSizeFitter>().enabled = false;
            Container_QuestList.GetComponent<VerticalLayoutGroup>().enabled = false;

            ToggleVisibleCell();
        }
    }
    #endregion

    #region Method
    public void SetupData()
    {
        if (!_isInitialized)
        {
            PopupUIManager.Instance.Show_MidSoftLoad(true);
            OnFinishedInit += AfterFinishInit;

            if (_initCoroutine != null)
            {
                StopCoroutine(_initCoroutine);
            }
            _initCoroutine = StartCoroutine(InitData());
        }
        else
        {
            UpdateData();
            ToggleVisibleCell();
        }

    }

    private void AfterFinishInit()
    {
        UpdateData();
        ScrollRect_QuestGroup.onValueChanged.AddListener(OnScrolling);
        PopupUIManager.Instance.Show_MidSoftLoad(false);
        ScrollRect_QuestGroup.verticalNormalizedPosition = 1.1f;
        OnFinishedInit -= AfterFinishInit;
    }

    private IEnumerator InitData()
    {
        List<QuestData> challengeQuestData = SeasonPassManager.Instance.GetChallengeQuestDataList();

        int i = 0; //frame count
        foreach(QuestData qData in challengeQuestData)
        {
            if (_challengeCellDict.ContainsKey(qData.ID))
            {
                _challengeCellDict[qData.ID].UpdateData();
            }
            else
            {
                SeasonPassQuestUIData data = new SeasonPassQuestUIData(qData);
                SeasonPassQuestCell questCell = CreateReward(data, true, Container_QuestList.transform);

                questCell.SetPremiumOnlyEvent(null);
                questCell.SetGetEvent(delegate { SeasonPassUIManager.Instance.CompleteQuest(qData.ID, false); });
                questCell.SetGoEvent(delegate { SeasonPassUIManager.Instance.PageNavigate(data.LocalData.GoFuncName, data.LocalData.GoFuncParam); });

                //Show Soft Linebreak
                questCell.ShowLinebreakImage(true);

                _challengeCellDict.Add(qData.ID, questCell);

                //Only wait when instantiate
                i++;
                if (i % 4 == 0)
                {
                    yield return new WaitForEndOfFrame();
                }
            }

        }
        Debug.LogFormat("Initialized Challenge Tab: finished in {0} frames",i);
        
        //Wait 3 frames
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();

        //Finished init
        _isInitialized = true;
        OnFinishedInit?.Invoke();
    }

    public void UpdateData()
    {
        UpdateAllCells();
        
        //Sort
        //Get position list
        DataTable sortingTable = new DataTable();
        sortingTable.Columns.Add("QID", typeof(string));
        sortingTable.Columns.Add("DATACELL", typeof(SeasonPassQuestCell));
        sortingTable.Columns.Add("QSTATE", typeof(string));

        List<Vector2> positionList = new List<Vector2>();
        for (int i = 0; i < Container_QuestList.transform.childCount; i++)
        {
            Transform child = Container_QuestList.transform.GetChild(i);
            SeasonPassQuestCell childCell = child.GetComponent<SeasonPassQuestCell>();
            if (childCell == null) continue;

            positionList.Add(childCell.gameObject.transform.localPosition);

            float questState = 0;

            if (childCell.QuestData.IsCompleted && !childCell.QuestData.IsCleared)
            {
                //completed and ready to claim
            }
            else if (childCell.QuestData.IsCleared)
            {
                //completely cleared
                questState = 2f;
            }
            else
            {
                //in progress
                float missionVal = childCell.QuestData.ProgressCurrent / childCell.QuestData.ProgressMax;
                questState = 1.01f - missionVal;
            }

            DataRow newRow = sortingTable.NewRow();
            newRow["QID"] = childCell.QuestData.ID.ToString();
            newRow["DATACELL"] = childCell;
            newRow["QSTATE"] = questState.ToString();
            sortingTable.Rows.Add(newRow);
        }
        sortingTable.DefaultView.Sort = "QSTATE,QID ASC"; //Sort
        sortingTable = sortingTable.DefaultView.ToTable(); //Convert to sorted table

        //Set each sibling's local position according to order sorted by datatable;
        for (int i = 0; i < sortingTable.Rows.Count; i++)
        {
            SeasonPassQuestCell cellObj = (SeasonPassQuestCell)sortingTable.Rows[i]["DATACELL"];
            cellObj.transform.SetSiblingIndex(i);
            cellObj.transform.localPosition = positionList[i];
        }

    }

    public void UpdateAllCells()
    {
        foreach (KeyValuePair<string, SeasonPassQuestCell> questCell in _challengeCellDict)
        {
            UpdateSpecificCell(questCell.Key);
        }
    }

    public void UpdateSpecificCell(string qID)
    {
        _challengeCellDict[qID].UpdateData();
    }

    private void ToggleVisibleCell()
    {
        RectTransform rect_cell = null;

        foreach (KeyValuePair<string, SeasonPassQuestCell> qCell in _challengeCellDict)
        {
            rect_cell = qCell.Value.GetComponent<RectTransform>();
            if (GameHelper.IsRectTouchRect(rect_cell, Rect_ViewPort))
            {
                qCell.Value.gameObject.SetActive(true);
            }
            else
            {
                qCell.Value.gameObject.SetActive(false);
            }
        }
    }
    #endregion

    #region Cell Gen
    private SeasonPassQuestCell CreateReward(SeasonPassQuestUIData data, bool isDaily, Transform parent)
    {
        GameObject obj = Instantiate(Prefab_ChallengeCell, parent);
        obj.SetActive(true);
        SeasonPassQuestCell cell = obj.GetComponent<SeasonPassQuestCell>();
        cell.InitializeData(data);
        return cell;
    }
    #endregion
}
