﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;
using UnityEngine.EventSystems;

//UIData
public class SeasonPassLevelCell : UIInputEvent
{
    #region Public Properties
    [Header("Prefabs")]
    public bool IsBigPrize = false;
    public GameObject Prefab_LevelRewardCell;

    [Header("Ref")]
    public TextMeshProUGUI Text_Level;
    public Transform Container_Premium;
    public Transform Container_Free;
    public GameObject Mask_Disabled;
    public GameObject Mask_Hilight;

    public SeasonPassTierRewardData PremiumRewardData { get { return _premRewardData; } private set { _premRewardData = value; } }
    public SeasonPassTierRewardData FreeRewardData { get { return _freeRewardData; } private set { _freeRewardData = value; } }
    public List<RewardCell_SeasonPass> PremiumRewardUI { get { return _premRewardUIList; } }
    public List<RewardCell_SeasonPass> FreeRewardUI { get { return _freeRewardUIList; } }

    #endregion

    #region Private Properties
    private bool _initialized = false;
    private SeasonPassTierRewardData _premRewardData;
    private SeasonPassTierRewardData _freeRewardData;
    private List<RewardCell_SeasonPass> _premRewardUIList = new List<RewardCell_SeasonPass>();
    private List<RewardCell_SeasonPass> _freeRewardUIList = new List<RewardCell_SeasonPass>();
    private Button _btn_CellButton;
    private UnityAction _onCellClick;
    private string _premiumRewardID;
    private string _freeRewardID;
    private int _cellLevel;
    #endregion

    #region Event
    private void OnEnable()
    {
        if(_cellLevel > 0) SeasonPassUIManager.Instance.Group_Rewards.OnCellEnabled(_cellLevel);
    }

    private void OnDisable()
    {
        if (_cellLevel > 0) SeasonPassUIManager.Instance.Group_Rewards.OnCellDisabled(_cellLevel);
    }
    #endregion

    #region Method
    public void SetupData(int level, UnityAction onCellClick = null)
    {
        if(!_initialized)
        {
            _initialized = true;
            InitializeData();
        }

        if (onCellClick != null) _onCellClick = onCellClick;

        Text_Level.text = level.ToString();

        _cellLevel = level;

        gameObject.name = "Reward Tier " + level.ToString();
    }

    public void UpdateReward(SeasonPassTierRewardData reward)
    {
        UpdateReward(reward.IsPremium, reward);
    }

    public void UpdateReward(bool isPremium, SeasonPassTierRewardData reward)
    {
        if (reward != null && reward.ItemDataList != null && reward.ItemDataList.Count > 0)
        {
            int loopCount;
            List<RewardCell_SeasonPass> rewGroupUIList;
            Transform container;

            if (isPremium)
            {
                _premiumRewardID = reward.ID;
                _premRewardData = reward;
                loopCount = Mathf.Max(_premRewardUIList.Count, reward.ItemDataList.Count);
                rewGroupUIList = _premRewardUIList;
                container = Container_Premium;
            }
            else
            {
                _freeRewardID = reward.ID;
                _freeRewardData = reward;
                loopCount = Mathf.Max(_freeRewardUIList.Count, reward.ItemDataList.Count);
                rewGroupUIList = _freeRewardUIList;
                container = Container_Free;
            }

            for (int i = 0; i < loopCount; i++)
            {
                if (i >= reward.ItemDataList.Count)
                {
                    rewGroupUIList[i].gameObject.SetActive(false);
                }
                else if (i >= rewGroupUIList.Count)
                {
                    rewGroupUIList.Add(CreateReward(reward.ItemDataList[i].ItemID, reward.ItemDataList[i].Amount, container));
                }
                else
                {
                    UpdateReward(rewGroupUIList[i], reward.ItemDataList[i].ItemID, reward.ItemDataList[i].Amount);
                    rewGroupUIList[i].gameObject.SetActive(true);
                }
            }
        }
        else
        {
            List<RewardCell_SeasonPass> rewGroupList;

            if (isPremium)
            {
                rewGroupList = _premRewardUIList;
            }
            else
            {
                rewGroupList = _freeRewardUIList;
            }

            for (int i = 0; i < rewGroupList.Count; i++)
            {
                rewGroupList[i].gameObject.SetActive(false);  
            }
        }
    }

    public void InitializeData()
    {
        _btn_CellButton = GetComponent<Button>();
    }

    public void ShowDisableMask(bool show)
    {
        if (!IsBigPrize && Mask_Disabled != null) Mask_Disabled.SetActive(show);
    }

    public void ShowHighlightMask(bool show)
    {
        if(!IsBigPrize && Mask_Hilight != null) Mask_Hilight.SetActive(show);
    }

    public void SetCompleted(bool completed)
    {
        _btn_CellButton.enabled = false;

        if(_premRewardUIList.Count > 0)
        {
            foreach(RewardCell_SeasonPass cell in _premRewardUIList)
            {
                cell.ShowCheckIcon(true);
            }
        }
        if (_freeRewardUIList.Count > 0)
        {
            foreach (RewardCell_SeasonPass cell in _freeRewardUIList)
            {
                cell.ShowCheckIcon(true);
            }
        }
    }
    #endregion

    #region Cell Gen
    private RewardCell_SeasonPass CreateReward(string rewardKey, int count, Transform container)
    {
        GameObject obj = Instantiate(Prefab_LevelRewardCell, container);
        obj.SetActive(true);
        RewardCell_SeasonPass cell = obj.GetComponent<RewardCell_SeasonPass>();
        cell.InitData(rewardKey, count);
        return cell;
    }
    private void UpdateReward(RewardCell_SeasonPass cell, string rewardKey, int count)
    {
        cell.InitData(rewardKey, count);
    }
    #endregion

}
