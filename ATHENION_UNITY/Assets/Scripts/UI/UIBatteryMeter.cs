﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIBatteryMeter : MonoBehaviour
{
    #region Inspector
    [SerializeField] private TextMeshProUGUI TEXT_Percent;
    [SerializeField] private Image IMG_BatteryLevel;

    [Header("Charge")]
    [SerializeField] private Sprite _sprite_full_charge;
    [SerializeField] private Sprite _sprite_half_charge;
    [SerializeField] private Sprite _sprite_min_charge;

    [Header("Normal")]
    [SerializeField] private Sprite _sprite_full;
    [SerializeField] private Sprite _sprite_half;
    [SerializeField] private Sprite _sprite_min;
    #endregion

    #region Private Properties
    private const float _batteryValueFull = 55.0f;
    private const float _batteryValueHalf = 30.0f;
    #endregion

    private void OnEnable()
    {
        StartCoroutine(UpdateBattery());
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private IEnumerator UpdateBattery()
    {
        while (true)
        {
            SetBattery();
            yield return new WaitForSecondsRealtime(5.0f);
        }
    }

    private void SetBattery()
    {
        float battery = Mathf.Abs(SystemInfo.batteryLevel);
        int percent = (int)(battery * 100.0f);

        if (SystemInfo.batteryStatus == BatteryStatus.Charging)
        {
            if (percent > _batteryValueFull)
            {
                IMG_BatteryLevel.sprite = _sprite_full_charge;
            }
            else if (percent > _batteryValueHalf)
            {
                IMG_BatteryLevel.sprite = _sprite_half_charge;
            }
            else
            {
                IMG_BatteryLevel.sprite = _sprite_min_charge;
            }

        }
        else
        {
            if (percent > _batteryValueFull)
            {
                IMG_BatteryLevel.sprite = _sprite_full;
            }
            else if (percent > _batteryValueHalf)
            {
                IMG_BatteryLevel.sprite = _sprite_half;
            }
            else
            {
                IMG_BatteryLevel.sprite = _sprite_min;
            }
        }

        TEXT_Percent.text = percent.ToString() + "%";
        //IMG_BatteryLevel.fillAmount = battery;
    }
}
