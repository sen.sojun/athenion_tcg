﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SeasonStatusText : MonoBehaviour
{
    #region Properties
    public TextMeshProUGUI Text;

    private Coroutine _update = null;
    #endregion

    #region MonoBehaviour Methods
    private void OnEnable()
    {
        StartUpdateCountdown();
    }

    private void OnDisable()
    {
        StopUpdateCountdown();
    }

    private void OnDestroy()
    {
        StopUpdateCountdown();
    }
    #endregion

    #region Methods
    IEnumerator CountDown()
    {
        while (true)
        {
            UpdateUI();

            yield return new WaitForSeconds(1f);
        }
    }

    public void UpdateUI()
    {
        Text.text = DataManager.Instance.GetSeasonStatus();
    }

    private void StartUpdateCountdown()
    {
        StopUpdateCountdown();

        _update = StartCoroutine(CountDown());
    }

    private void StopUpdateCountdown()
    {
        if (_update != null)
        {
            StopCoroutine(_update);
            _update = null;
        }
    }
    #endregion
}
