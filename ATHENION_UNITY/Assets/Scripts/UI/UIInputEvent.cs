﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIInputEvent : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    #region Properties
    public readonly float DoubleTapTimeout = 0.2f;
    public readonly float HoldTime = 0.2f;

    private bool _isClickable = true;

    private bool _isInputDown = false;
    private bool _isInputHold = false;
    private bool _isInputDrag = false;

    private int _touchId = 0;
    private int _tapCount = 0;
    private PointerEventData _downEventData;

    private float _lastTapTime = 0.0f;
    private float _holdTimer = 0.0f;

    #region Events
    public event Action<PointerEventData> OnClickEvent;
    public event Action<PointerEventData> OnDoubleClickEvent;
    public event Action<PointerEventData> OnBeginHoldEvent;
    public event Action<PointerEventData> OnEndHoldEvent;
    public event Action<PointerEventData> OnBeginDragEvent;
    public event Action<PointerEventData> OnDragEvent;
    public event Action<PointerEventData> OnEndDragEvent;
    #endregion

    #endregion

    // Start is called before the first frame update
    void Start()
    {        
    }

    // Update is called once per frame
    protected void Update()
    {
        if (_isInputDown)
        {
            _holdTimer += Time.deltaTime;
        }

        if (_tapCount == 1)
        {
            // wait for 2nd tap.
            float waitTime = (Time.time - _lastTapTime);

            if (!_isInputDown && waitTime >= DoubleTapTimeout)
            {
                // 2nd tap not coming in time.

                // Call Click.
                _isInputDown = false;
                _tapCount = 0;
                OnEventClick(_downEventData);
            }
            else if (_holdTimer >= HoldTime)
            {
                // Hold 

                // Call Hold Start.
                _isInputDown = false;
                _isInputHold = true;
                _tapCount = 0;
                OnEventBeginHold(_downEventData); 
            }
        }
    }

    #region Event Methods
    public void OnPointerDown(PointerEventData eventData)
    {
        OnEventUnityPointerDown(eventData);

        if (!_isInputDown && !_isInputHold) // Prevent multiple finger tap.
        {
            _isInputDown = true;
            _holdTimer = 0.0f;
            _lastTapTime = Time.time;
            _tapCount++;
            _touchId = eventData.pointerId;
            _downEventData = eventData;

            if (_tapCount == 2)
            {
                // tap 2nd time.

                // Call Double Click.
                _isInputDown = false;
                _tapCount = 0;
                OnEventDoubleClick(_downEventData);
            }
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        OnEventUnityPointerUp(eventData);

        if (_touchId != eventData.pointerId)
        {
            // reject other finger up event.
            return;
        }

        if (_isInputDown)
        {
            _isInputDown = false;
        }
        else
        {
            if (_isInputHold)
            {
                // Call Hold End.
                _isInputHold = false;
                _isInputDrag = false;
                _holdTimer = 0.0f;
                OnEventEndHold(eventData);
            }
        }
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        OnEventUnityBeginDrag(eventData);

        if (_touchId != eventData.pointerId)
        {
            // reject other finger up event.
            return;
        }

        if (_isInputHold)
        {
            // Call Hold End.
            _isInputHold = false;
            OnEventEndHold(eventData);
        }

        // Cancel click
        _isInputDown = false;
        _tapCount = 0;

        _isInputDrag = true;
        _touchId = eventData.pointerId;
        OnEventBeginDrag(eventData);
    }

    public void OnDrag(PointerEventData eventData)
    {
        OnEventUnityDrag(eventData);

        if (_touchId != eventData.pointerId)
        {
            // reject other finger up event.
            return;
        }

        _isInputDrag = true;
        _touchId = eventData.pointerId;
        OnEventDrag(eventData);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        OnEventUnityEndDrag(eventData);

        if (_touchId != eventData.pointerId)
        {
            // reject other finger up event.
            return;
        }

        _isInputDrag = false;
        _touchId = eventData.pointerId;
        OnEventEndDrag(eventData);
    }

    public void SetClickable(bool clickable)
    {
        _isClickable = clickable;
    }
    #endregion

    #region UI Event Methods

    // ========================== Unity Event ==========================

    public virtual void OnEventUnityPointerDown(PointerEventData eventData)
    {
       // Debug.Log("OnEventUnityPointerDown " + eventData.position);
    }

    public virtual void OnEventUnityPointerUp(PointerEventData eventData)
    {
       // Debug.Log("OnEventUnityPointerUp " + eventData.position);
    }

    public virtual void OnEventUnityBeginDrag(PointerEventData eventData)
    {
        //Debug.Log("OnEventUnityBeginDrag " + eventData.position);
    }

    public virtual void OnEventUnityDrag(PointerEventData eventData)
    {
        //Debug.Log("OnEventUnityDrag " + eventData.position);
    }

    public virtual void OnEventUnityEndDrag(PointerEventData eventData)
    {
        //Debug.Log("OnEventUnityEndDrag " + eventData.position);
    }


    // ========================== Custom Event ==========================

    public virtual void OnEventClick(PointerEventData eventData)
    {
        if (!_isClickable) return;
        //Debug.Log("OnEventClick");
        OnClickEvent?.Invoke(eventData);
    }

    public virtual void OnEventDoubleClick(PointerEventData eventData)
    {
        //Debug.Log("OnEventDoubleClick");
        OnDoubleClickEvent?.Invoke(eventData);
    }

    public virtual void OnEventBeginHold(PointerEventData eventData)
    {
        //Debug.Log("OnEventStartHold" + eventData.position);
        OnBeginHoldEvent?.Invoke(eventData);
    }

    public virtual void OnEventEndHold(PointerEventData eventData)
    {
        //Debug.Log("OnEventEndHold" + eventData.position);
        OnEndHoldEvent?.Invoke(eventData);
    }

    public virtual void OnEventBeginDrag(PointerEventData eventData)
    {
        //Debug.Log("OnEventStartDrag " + eventData.position);
        OnBeginDragEvent?.Invoke(eventData);
    }

    public virtual void OnEventDrag(PointerEventData eventData)
    {
        //Debug.Log("OnEventDrag " + eventData.position);
        OnDragEvent?.Invoke(eventData);
    }

    public virtual void OnEventEndDrag(PointerEventData eventData)
    {
        //Debug.Log("OnEventEndDrag " + eventData.position);
        OnEndDragEvent?.Invoke(eventData);
    }
    #endregion
}
