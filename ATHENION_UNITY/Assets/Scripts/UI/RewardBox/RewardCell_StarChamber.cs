﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;

public class RewardCell_StarChamber : RewardCell
{
    #region Inspector Properties
    [Header("Rarity")]
    [SerializeField] private GameObject Rarity_Common;
    [SerializeField] private GameObject Rarity_Rare;
    [SerializeField] private GameObject Rarity_Epic;
    [SerializeField] private GameObject Rarity_Legend;

    [Header("Duplicate")]
    [SerializeField] private TextMeshProUGUI TEXT_Duplicate;
    #endregion

    private StarCubeItemData _data;

    #region Method
    public void SetStarCubeData(StarCubeItemData data)
    {
        _data = data;

        SetDuplicate(false);
        SetRarity();
    }

    private void SetRarity()
    {
        Rarity_Common.SetActive(_data.Rarity == CardRarity.Common);
        Rarity_Rare.SetActive(_data.Rarity == CardRarity.Rare);
        Rarity_Epic.SetActive(_data.Rarity == CardRarity.Epic);
        Rarity_Legend.SetActive(_data.Rarity == CardRarity.Legend);
    }

    private void SetDuplicate(bool isAnimate)
    {
        if (isAnimate)
        {
            StartCoroutine(SetAnimationDuplicate());
        }
        else
        {
            TEXT_Duplicate.gameObject.SetActive(_data.IsDuplicate);

            if (_data.IsDuplicate)
            {
                float duration = 0.75f;
                TEXT_Duplicate.alpha = 0.0f;

                TEXT_Duplicate.transform.localScale = new Vector3(1.3f, 1.3f, 1.3f);
                TEXT_Duplicate.transform.DOScale(1.0f, duration).SetEase(Ease.OutBack);
                TEXT_Duplicate.DOFade(1.0f, duration);
            }
        }
        
    }

    

    private IEnumerator SetAnimationDuplicate()
    {
        TEXT_Duplicate.gameObject.SetActive(_data.IsDuplicate);

        if (_data.IsDuplicate)
        {
            float duration = 0.75f;
            TEXT_Duplicate.alpha = 0.0f;

            yield return new WaitForSeconds(1.5f);
            TEXT_Duplicate.transform.localScale = new Vector3(1.3f, 1.3f, 1.3f);
            TEXT_Duplicate.transform.DOScale(1.0f, duration).SetEase(Ease.OutBack);
            TEXT_Duplicate.DOFade(1.0f, duration);
        }

        yield return null;
    }
    #endregion
}
