﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class RewardCell : UIInputEvent
{
    #region Public Properties
    public Image IMG_RewardIcon;
    public TextMeshProUGUI TEXT_RewardCount;
    public GameObject ClaimedMask;
    #endregion

    #region Private Properties
    protected string _imgKey;
    protected int _rewardCount;
    protected UIInputEvent _inputParent;
    protected bool _isEmpty;
    #endregion

    #region Methods
    public void InitData(string imgKey, int rewardCount)
    {
        _imgKey = imgKey;
        _rewardCount = rewardCount;

        SetImage();
        SetRewardCount();
        _isEmpty = false;
    }

    protected void SetImage()
    {
        Sprite sprite = SpriteResourceHelper.LoadSprite(_imgKey, SpriteResourceHelper.SpriteType.ICON_Item);
        IMG_RewardIcon.sprite = sprite;
        IMG_RewardIcon.gameObject.SetActive(true);
    }

    protected void SetRewardCount()
    {
        TEXT_RewardCount.text = _rewardCount.ToString();
        TEXT_RewardCount.gameObject.SetActive(true);
    }

    public int GetRewardCount()
    {
        return _rewardCount;
    }

    public string GetRewardID()
    {
        return _imgKey;
    }

    public void ShowClaimedMask(bool show)
    {
        if(ClaimedMask != null) ClaimedMask.SetActive(show);
    }

    public void SetAsEmptySlot()
    {
        IMG_RewardIcon.gameObject.SetActive(false);
        TEXT_RewardCount.gameObject.SetActive(false);
        _isEmpty = true;
    }

    #endregion

    #region Event
    public override void OnEventClick(PointerEventData eventData)
    {
        if (_isEmpty) return;
        //Debug.Log("Start Hold" + _imgKey);
        TooltipManager.Instance.ShowTooltip_Full(_imgKey, eventData.position);
    }

    public override void OnEventUnityPointerDown(PointerEventData eventData)
    {
        //if (_isEmpty) return;
        ////Debug.Log("Start Hold" + _imgKey);
        //TooltipManager.Instance.ShowTooltip_Full(_imgKey, eventData.position);
    }

    public override void OnEventBeginHold(PointerEventData eventData)
    {
        //if (_isEmpty) return;
        ////Debug.Log("Start Hold" + _imgKey);
        //TooltipManager.Instance.ShowTooltip_Full(_imgKey, eventData.position);
    }

    public override void OnEventEndHold(PointerEventData eventData)
    {
        //if (_isEmpty) return;
        ////Debug.Log("Stop Hold" + _imgKey);
        //TooltipManager.Instance.HideTooltip_Full();
    }

    public override void OnEventBeginDrag(PointerEventData eventData)
    {
        _inputParent = gameObject.transform.parent.GetComponentInParent<UIInputEvent>();
        if (_inputParent != null && _inputParent != this)
        {
            _inputParent.OnEventBeginDrag(eventData);
        }
        else
        {
            base.OnEventBeginDrag(eventData);
        }
    }

    public override void OnEventDrag(PointerEventData eventData)
    {
        _inputParent = gameObject.transform.parent.GetComponentInParent<UIInputEvent>();
        if (_inputParent != null && _inputParent != this)
        {
            _inputParent.OnEventDrag(eventData);
        }
        else
        {
            base.OnEventDrag(eventData);
        }
    }

    public override void OnEventEndDrag(PointerEventData eventData)
    {
        _inputParent = gameObject.transform.parent.GetComponentInParent<UIInputEvent>();
        if (_inputParent != null && _inputParent != this)
        {
            _inputParent.OnEventEndDrag(eventData);
        }
        else
        {
            base.OnEventEndDrag(eventData);
        }
    }
    #endregion
}
