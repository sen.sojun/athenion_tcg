﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;
using DG.Tweening;

public class RewardCell_SeasonPass : RewardCell
{
    #region Public Properties
    public Image IMG_RewardBG1;
    public Image IMG_RewardBG2;
    public Image IMG_RewardBG3;
    public Image IMG_LockIcon;
    public Image IMG_CheckIcon;
    public Image IMG_BigCheckIcon;

    public string ItemKey { get { return _imgKey; } }
    public int ItemAmount { get { return _rewardCount; } }
    #endregion

    #region Private Properties
    private Tweener _PopAnim = null;
    #endregion
    
    public void UpdateRewardBackground(int bgIndex)
    {
        IMG_RewardBG3.gameObject.SetActive(bgIndex == 3);
        IMG_RewardBG2.gameObject.SetActive(bgIndex == 2);

        //If bgIndex == 1 or bg2 and 3 are inactived, show normal bg.
        IMG_RewardBG1.gameObject.SetActive(bgIndex == 1 
                                            || (!IMG_RewardBG3.gameObject.activeSelf 
                                            && !IMG_RewardBG1.gameObject.activeSelf)
                                            );
    }

    public void PlayerAmountPopAnimation()
    {
        if(_PopAnim != null)
        {
            _PopAnim.onComplete?.Invoke();
            _PopAnim.Kill();
            _PopAnim = null;
        }
        Vector3 oriLocalScale = new Vector3(TEXT_RewardCount.transform.localScale.x, TEXT_RewardCount.transform.localScale.y, TEXT_RewardCount.transform.localScale.z);
        TEXT_RewardCount.transform.localScale = new Vector3(1.25f, 1.25f, TEXT_RewardCount.transform.localScale.z);
        _PopAnim = TEXT_RewardCount.transform.DOScale(oriLocalScale, 0.2f);
        _PopAnim.OnComplete(delegate { TEXT_RewardCount.transform.localScale = oriLocalScale; } );
    }

    public void ShowLockIcon(bool show)
    {
        IMG_LockIcon.gameObject.SetActive(show);
    }

    public void ShowCheckIcon(bool show)
    {
        IMG_CheckIcon.gameObject.SetActive(show);
    }

    public void ShowBigCheckIcon(bool show)
    {
        IMG_BigCheckIcon.gameObject.SetActive(show);
    }

    private void OnDisable()
    {
        if (_PopAnim != null)
        {
            _PopAnim.onComplete?.Invoke();
            _PopAnim.Kill();
            _PopAnim = null;
        }
    }

}
