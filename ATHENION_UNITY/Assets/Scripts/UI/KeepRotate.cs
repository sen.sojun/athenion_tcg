﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeepRotate : MonoBehaviour 
{
    public Vector3 RotateSpeed;

	// Use this for initialization
	void Start () 
    {		
	}
	
	// Update is called once per frame
	void Update () 
    {
        transform.Rotate(RotateSpeed * Time.deltaTime);
    }
}
