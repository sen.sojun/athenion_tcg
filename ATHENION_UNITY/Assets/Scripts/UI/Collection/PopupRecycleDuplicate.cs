﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;
using Karamucho.UI;
using System;

public class PopupRecycleDuplicate : MonoBehaviour
{
    #region Inspector
    [Header("Rarity")]
    [SerializeField] private RecycleCell Cell_Common;
    [SerializeField] private RecycleCell Cell_Rare;
    [SerializeField] private RecycleCell Cell_Epic;
    [SerializeField] private RecycleCell Cell_Legend;

    [Header("Result")]
    [SerializeField] private TextMeshProUGUI TEXT_Result;
    [SerializeField] private Color Color_SurPlus;

    [Header("FR")]
    [SerializeField] private TextMeshProUGUI TEXT_FR_current;
    [SerializeField] private TextMeshProUGUI TEXT_FR_new;

    [Header("RF")]
    [SerializeField] private TextMeshProUGUI TEXT_RF_current;
    [SerializeField] private TextMeshProUGUI TEXT_RF_new;

    [Header("S1")]
    [SerializeField] private TextMeshProUGUI TEXT_S1_current;
    [SerializeField] private TextMeshProUGUI TEXT_S1_new;

    [Header("BTN")]
    [SerializeField] private Button BTN_Cancel;
    [SerializeField] private Button BTN_Recycle;
    #endregion

    #region Private Properties
    private UnityAction _onClick;
    private List<ItemData> _cardList = new List<ItemData>();
    private Dictionary<CardRarity, RecycleCellData> _dictCardRarity = new Dictionary<CardRarity, RecycleCellData>();
    #endregion

    #region Methods
    public void Show(Dictionary<CardRarity, RecycleCellData> itemList, UnityAction onClick)
    {
        _onClick = onClick;
        _dictCardRarity = itemList;

        InitBTN();
        ResetPriceUI();
        SetupPrice();

        GameHelper.UITransition_FadeIn(this.gameObject);

    }

    public void Hide()
    {
        GameHelper.UITransition_FadeOut(this.gameObject);
    }

    private void ResetPriceUI()
    {
        Cell_Common.Setup(new RecycleCellData());
        Cell_Rare.Setup(new RecycleCellData());
        Cell_Epic.Setup(new RecycleCellData());
        Cell_Legend.Setup(new RecycleCellData());
    }

    private void SetupPrice()
    {
        RecycleCellData recyclePriceSum = new RecycleCellData();
        foreach (KeyValuePair<CardRarity, RecycleCellData> item in _dictCardRarity)
        {
            recyclePriceSum.AddCardAmount(item.Value.CardAmount);
            recyclePriceSum.AddRecyclePrice(VirtualCurrency.FR, item.Value.GetRecyclePrice(VirtualCurrency.FR));
            recyclePriceSum.AddRecyclePrice(VirtualCurrency.RF, item.Value.GetRecyclePrice(VirtualCurrency.RF));
            recyclePriceSum.AddRecyclePrice(VirtualCurrency.S1, item.Value.GetRecyclePrice(VirtualCurrency.S1));

            switch (item.Key)
            {
                case CardRarity.Common:
                {
                    Cell_Common.Setup(item.Value);
                    break;
                }
                case CardRarity.Rare:
                {
                    Cell_Rare.Setup(item.Value);
                    break;
                }
                case CardRarity.Epic:
                {
                    Cell_Epic.Setup(item.Value);
                    break;
                }
                case CardRarity.Legend:
                {
                    Cell_Legend.Setup(item.Value);
                    break;
                }
                default:
                {
                    Debug.LogWarning("something went wrong !!!");
                    break;
                }
            }
        }

        int currentFR = DataManager.Instance.InventoryData.GetVirtualCurrency(VirtualCurrency.FR);
        int currentRF = DataManager.Instance.InventoryData.GetVirtualCurrency(VirtualCurrency.RF);
        int currentS1 = DataManager.Instance.InventoryData.GetVirtualCurrency(VirtualCurrency.S1);
        int newFR = currentFR + recyclePriceSum.GetRecyclePrice(VirtualCurrency.FR);
        int newRF = currentRF + recyclePriceSum.GetRecyclePrice(VirtualCurrency.RF);
        int newS1 = currentS1 + recyclePriceSum.GetRecyclePrice(VirtualCurrency.S1);

        TEXT_FR_current.text = "<sprite name=S_FR> " + currentFR.ToString("N0");
        TEXT_RF_current.text = "<sprite name=S_RF> " + currentRF.ToString("N0");
        TEXT_S1_current.text = "<sprite name=S_S1> " + currentS1.ToString("N0");

        TEXT_FR_new.text = newFR.ToString("N0");
        TEXT_RF_new.text = newRF.ToString("N0");
        TEXT_S1_new.text = newS1.ToString("N0");

        // Check Color
        TEXT_FR_new.color = newFR > currentFR ? Color_SurPlus : Color.white;
        TEXT_RF_new.color = newRF > currentRF ? Color_SurPlus : Color.white;
        TEXT_S1_new.color = newS1 > currentS1 ? Color_SurPlus : Color.white;

        TEXT_Result.text = string.Format(LocalizationManager.Instance.GetText("TEXT_RECYCLE_RESULT")
            , recyclePriceSum.GetRecyclePrice(VirtualCurrency.FR).ToString("N0")
            , recyclePriceSum.GetRecyclePrice(VirtualCurrency.RF).ToString("N0")
            , recyclePriceSum.GetRecyclePrice(VirtualCurrency.S1).ToString("N0"));
    }
   
    private void InitBTN()
    {
        BTN_Recycle.interactable = ((_dictCardRarity.Count > 0) && DataManager.Instance.IsFeatureEnable(FeatureUnlock.CardCrafting));

        BTN_Cancel.onClick.RemoveAllListeners();
        BTN_Cancel.onClick.AddListener(Hide);

        BTN_Recycle.onClick.RemoveAllListeners();
        BTN_Recycle.onClick.AddListener(delegate
        {
            Hide();         
            _onClick?.Invoke();
        });
    }
    #endregion
}