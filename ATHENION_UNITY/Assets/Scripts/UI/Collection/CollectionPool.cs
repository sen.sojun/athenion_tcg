﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using Karamucho;

public abstract class CollectionItem : UIInputEventPoolItem
{
    #region Public Properties
    public string PrefabPath { get; protected set; }
    #endregion

    #region Methods
    public void SetPrefabPath(string path)
    {
        PrefabPath = path;
    }
    #endregion
}

public class CollectionPool : PrefabPool<CollectionPool, CollectionItem>
{
    private static readonly string _rootCollectionPath = "Prefabs/Collections/";
    private static readonly string _rootCollectionCardPath = _rootCollectionPath + "_CardUI_Collection";
    private static readonly string _rootCollectionCardBarPath = _rootCollectionPath + "CardBar";
    private static readonly string _rootCollectionDeckPath = _rootCollectionPath + "DeckSlot";
    private static readonly string _rootCollectionMarkerPath = _rootCollectionPath + "Marker";

    public CollectionItem CreateCollectionCard()
    {
        return CreateItem(_rootCollectionCardPath);
    }

    public CollectionItem CreateCollectionCardBar()
    {
        return CreateItem(_rootCollectionCardBarPath);
    }

    public CollectionItem CreateCollectionDeck()
    {
        return CreateItem(_rootCollectionDeckPath);
    }

    public CollectionItem CreateCollectionMarker()
    {
        return CreateItem(_rootCollectionMarkerPath);
    }

    protected CollectionItem CreateItem(string prefabPath)
    {
        if (!IsInit) Init();

        for (int i = 0; i < _pool.Count; i++)
        {
            CollectionItem item = (CollectionItem)_pool[i];
            if (item != null && !item.IsLock && item.PrefabPath == prefabPath)
            {
                item.SetLock(true);
                item.GetGameObject().SetActive(true);
                return _pool[i];
            }
        }

        //Debug.Log("PrefabPath : " + prefabPath);
        GameObject go = MonoBehaviour.Instantiate(ResourceManager.Load(prefabPath)) as GameObject;
        CollectionItem component = go.GetComponent<CollectionItem>();
        component.SetPrefabPath(prefabPath);

        _pool.Add(component);
        component.SetLock(true);

        return component;
    }
}
