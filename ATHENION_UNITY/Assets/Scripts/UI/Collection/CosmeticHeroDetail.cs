﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;
using Karamucho;
using UnityEngine.UI;
using DG.Tweening;

public class CosmeticHeroDetail : MonoBehaviour
{
    #region Public Properties
    public Image IMG_Art_Main;
    public Image IMG_Art_Disable;
    public Button BTN_Close;
    public Button BTN_Story;
    public TextMeshProUGUI TEXT_Name;
    public TextMeshProUGUI TEXT_Detail;
    public TextMeshProUGUI TEXT_Story;
    public CanvasGroup Group_Story;

    public bool IsAvailable { get { return _isAvailable; } }
    #endregion

    #region Private Properties
    private string _id;
    private string _skinName;
    private bool _isAvailable;
    private bool _isStoryShow;
    #endregion

    #region Method
    public void Show(string id, string name, bool isAvailable)
    {
        GameHelper.UITransition_FadeIn(this.gameObject);
        InitData(id, name, isAvailable);
    }

    public void Hide()
    {
        GameHelper.UITransition_FadeOut(this.gameObject);
    }

    public void InitData(string id, string name, bool isAvailable)
    {
        _id = id;
        _skinName = name;
        _isAvailable = isAvailable;

        SetImage();
        SetName();
        SetBTN();
    }

    private void SetImage()
    {
        Sprite sprite = SpriteResourceHelper.LoadSprite(_id, SpriteResourceHelper.SpriteType.SKIN_Hero);

        IMG_Art_Main.sprite = sprite;
        IMG_Art_Disable.sprite = sprite;

        IMG_Art_Main.gameObject.SetActive(_isAvailable);
        IMG_Art_Disable.gameObject.SetActive(!_isAvailable);
    }

    private void SetName()
    {
        string story = LocalizationManager.Instance.GetText("HERO_BIO_" + _id);

        TEXT_Name.text = _skinName;
        TEXT_Detail.text = story;
      
        if(TEXT_Story != null)
            TEXT_Story.text = story;
    }

    private void SetBTN()
    {
        BTN_Close.onClick.RemoveAllListeners();
        BTN_Close.onClick.AddListener(Hide);

        if (BTN_Story != null)
        {
            BTN_Story.onClick.RemoveAllListeners();
            BTN_Story.onClick.AddListener(ToggleStory);
        }
    }

    private void ToggleStory()
    {
        _isStoryShow = !_isStoryShow;

        if (_isStoryShow)
        {
            Group_Story.gameObject.SetActive(_isStoryShow);
            Group_Story.alpha = 0;
            Group_Story.DOFade(1.0f, 0.3f);
        }
        else
        {
            Group_Story.DOFade(0.0f, 0.3f).OnComplete(delegate
            {
                Group_Story.gameObject.SetActive(_isStoryShow);
            });
        }
    }
    #endregion
}
