﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;
using Karamucho.UI;
using System;

public class PopupCraftMissingCard : MonoBehaviour
{
    #region Inspector
    [Header("Rarity")]
    [SerializeField] private CraftCell Cell_Common;
    [SerializeField] private CraftCell Cell_Rare;
    [SerializeField] private CraftCell Cell_Epic;
    [SerializeField] private CraftCell Cell_Legend;

    [Header("Result")]
    [SerializeField] private TextMeshProUGUI TEXT_AllCard;
    [SerializeField] private TextMeshProUGUI TEXT_Result;
    [SerializeField] private Color Color_Minus;

    [Header("FR")]
    [SerializeField] private TextMeshProUGUI TEXT_FR_current;
    [SerializeField] private TextMeshProUGUI TEXT_FR_new;

    [Header("RF")]
    [SerializeField] private TextMeshProUGUI TEXT_RF_current;
    [SerializeField] private TextMeshProUGUI TEXT_RF_new;

    [Header("S1")]
    [SerializeField] private TextMeshProUGUI TEXT_S1_current;
    [SerializeField] private TextMeshProUGUI TEXT_S1_new;

    [Header("BTN")]
    [SerializeField] private Button BTN_Cancel;
    [SerializeField] private Button BTN_CraftAll;
    [SerializeField] private GameObject Lock_Craft;
    #endregion

    #region Private Properties
    private UnityAction _onClick;
    private UnityAction _onCancel;
    private List<ItemData> _cardList = new List<ItemData>();
    private Dictionary<CardRarity, CraftCellData> _dictCardRarity = new Dictionary<CardRarity, CraftCellData>();
    #endregion

    #region Methods
    public void Show(Dictionary<CardRarity, CraftCellData> itemList, UnityAction onClick, UnityAction onCancel)
    {
        _onClick = onClick;
        _onCancel = onCancel;
        _dictCardRarity = itemList;

        bool isValid = false;

        ResetPriceUI();
        SetupPrice(out isValid);
        InitBTN(isValid);

        GameHelper.UITransition_ZoomFadeIn(this.gameObject);
    }

    public void Hide()
    {
        GameHelper.UITransition_ZoomFadeOut(this.gameObject);
    }

    private void CancelCraftAllMissing()
    {
        _onCancel?.Invoke();
        Hide();
    }

    private void ResetPriceUI()
    {
        Cell_Common.Setup(new CraftCellData());
        Cell_Rare.Setup(new CraftCellData());
        Cell_Epic.Setup(new CraftCellData());
        Cell_Legend.Setup(new CraftCellData());
    }

    private void SetupPrice(out bool isValid)
    {
        int cardSum = 0;
        CraftCellData craftPriceSum = new CraftCellData();
        foreach (KeyValuePair<CardRarity, CraftCellData> item in _dictCardRarity)
        {
            cardSum += item.Value.CardAmount;
            craftPriceSum.AddCardAmount(item.Value.CardAmount);
            craftPriceSum.AddCraftPrice(VirtualCurrency.FR, item.Value.GetCraftPrice(VirtualCurrency.FR));
            craftPriceSum.AddCraftPrice(VirtualCurrency.RF, item.Value.GetCraftPrice(VirtualCurrency.RF));
            craftPriceSum.AddCraftPrice(VirtualCurrency.S1, item.Value.GetCraftPrice(VirtualCurrency.S1));

            switch (item.Key)
            {
                case CardRarity.Common:
                {
                    Cell_Common.Setup(item.Value);
                    break;
                }
                case CardRarity.Rare:
                {
                    Cell_Rare.Setup(item.Value);
                    break;
                }
                case CardRarity.Epic:
                {
                    Cell_Epic.Setup(item.Value);
                    break;
                }
                case CardRarity.Legend:
                {
                    Cell_Legend.Setup(item.Value);
                    break;
                }
                default:
                {
                    Debug.LogWarning("something went wrong !!!");
                    break;
                }
            }
        }

        int currentFR = DataManager.Instance.InventoryData.GetVirtualCurrency(VirtualCurrency.FR);
        int currentRF = DataManager.Instance.InventoryData.GetVirtualCurrency(VirtualCurrency.RF);
        int currentS1 = DataManager.Instance.InventoryData.GetVirtualCurrency(VirtualCurrency.S1);
        int newFR = currentFR - craftPriceSum.GetCraftPrice(VirtualCurrency.FR);
        int newRF = currentRF - craftPriceSum.GetCraftPrice(VirtualCurrency.RF);
        int newS1 = currentS1 - craftPriceSum.GetCraftPrice(VirtualCurrency.S1);

        // Check fragment valid to craft.
        if (newFR < 0 || newRF < 0 || newS1 < 0)
        {
            isValid = false;
        }
        else
        {
            isValid = true;
        }

        TEXT_FR_current.text = "<sprite name=S_FR> " + currentFR.ToString("N0");
        TEXT_RF_current.text = "<sprite name=S_RF> " + currentRF.ToString("N0");
        TEXT_S1_current.text = "<sprite name=S_S1> " + currentS1.ToString("N0");

        TEXT_FR_new.text = newFR.ToString("N0");
        TEXT_RF_new.text = newRF.ToString("N0");
        TEXT_S1_new.text = newS1.ToString("N0");

        // Check Color
        TEXT_FR_new.color = newFR < currentFR ? Color_Minus : Color.white;
        TEXT_RF_new.color = newRF < currentRF ? Color_Minus : Color.white;
        TEXT_S1_new.color = newS1 < currentS1 ? Color_Minus : Color.white;

        TEXT_Result.text = string.Format(LocalizationManager.Instance.GetText("TEXT_CRAFT_ALL_RESULT")
            , craftPriceSum.GetCraftPrice(VirtualCurrency.FR).ToString("N0")
            , craftPriceSum.GetCraftPrice(VirtualCurrency.RF).ToString("N0")
            , craftPriceSum.GetCraftPrice(VirtualCurrency.S1).ToString("N0"));

        TEXT_AllCard.text = cardSum + " " + (cardSum > 1 ? LocalizationManager.Instance.GetText("TEXT_CARDS"): LocalizationManager.Instance.GetText("TEXT_CARD"));
    }
   
    private void InitBTN(bool craftValid)
    {
        bool craftUnlock = DataManager.Instance.IsFeatureEnable(FeatureUnlock.CardCrafting);
        Lock_Craft.SetActive(!craftUnlock);

        BTN_CraftAll.interactable = (_dictCardRarity.Count > 0) && craftValid && craftUnlock;

        BTN_Cancel.onClick.RemoveAllListeners();
        BTN_Cancel.onClick.AddListener(CancelCraftAllMissing);

        BTN_CraftAll.onClick.RemoveAllListeners();
        BTN_CraftAll.onClick.AddListener(delegate
        {
            Hide();         
            _onClick?.Invoke();
        });
    }
    #endregion
}