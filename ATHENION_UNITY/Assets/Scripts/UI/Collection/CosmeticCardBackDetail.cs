﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;
using Karamucho;
using UnityEngine.UI;

public class CosmeticCardBackDetail : MonoBehaviour
{
    #region Public Properties
    public Image IMG_Art_Main;
    public Image IMG_Art_Disable;
    public Button BTN_Close;
    public TextMeshProUGUI TEXT_Name;
    public TextMeshProUGUI TEXT_Detail;

    public bool IsAvailable { get { return _isAvailable; } }
    #endregion

    #region Private Properties
    private string _id;
    private string _skinName;
    private bool _isAvailable;
    #endregion

    #region Method
    public void Show(string id, string name, bool isAvailable)
    {
        GameHelper.UITransition_FadeIn(this.gameObject);
        InitData(id, name, isAvailable);
    }

    public void Hide()
    {
        GameHelper.UITransition_FadeOut(this.gameObject);
    }

    public void InitData(string id, string name, bool isAvailable)
    {
        _id = id;
        _skinName = name;
        _isAvailable = isAvailable;

        SetImage();
        SetName();
        SetBTN();
    }

    private void SetImage()
    {
        Sprite sprite = SpriteResourceHelper.LoadSprite(_id, SpriteResourceHelper.SpriteType.CardBack_IMG);

        IMG_Art_Main.sprite = sprite;
        IMG_Art_Disable.sprite = sprite;

        IMG_Art_Main.gameObject.SetActive(_isAvailable);
        IMG_Art_Disable.gameObject.SetActive(!_isAvailable);
    }

    private void SetName()
    {
        TEXT_Name.text = LocalizationManager.Instance.GetItemName(_id);
        TEXT_Detail.text = LocalizationManager.Instance.GetItemDescription(_id);
    }

    private void SetBTN()
    {
        BTN_Close.onClick.RemoveAllListeners();
        BTN_Close.onClick.AddListener(Hide);
    }
    #endregion
}
