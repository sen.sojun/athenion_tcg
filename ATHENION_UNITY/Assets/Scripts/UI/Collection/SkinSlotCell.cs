﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using Karamucho;
using Coffee.UIExtensions;

public class SkinSlotCell : MonoBehaviour
{
    #region Enum
    public enum SkinType
    {
        Hero
        , CardBack
        , HUD
        , Avatar
        , Title
    }
    #endregion

    #region Public Properties
    public Image IMG_Art_Main;
    public Image IMG_Art_Disable;
    public GameObject GlowSelected;

    public string ID { get { return _itemId; } }
    public bool IsAvailble { get { return _isAvailable; } }
    #endregion

    #region Private Propertie
    private string _itemId;
    private bool _isSelected;
    private bool _isAvailable;
    #endregion

    #region Methods
    public void InitData(string itemID, SkinType skinType, bool isAvailable, UnityAction onClick)
    {
        _itemId = itemID;
        _isAvailable = isAvailable;
        SetupBTN(onClick);
        SetImage(skinType);
        ShowAvailable(isAvailable);
    }

    private void SetImage(SkinType type)
    {
        Sprite sprite = null;
        switch (type)
        {
            case SkinType.Avatar:
            {
                sprite = SpriteResourceHelper.LoadSprite(_itemId, SpriteResourceHelper.SpriteType.Avatar);
                break;
            }

            case SkinType.Hero:
            {
                sprite = SpriteResourceHelper.LoadSprite(_itemId, SpriteResourceHelper.SpriteType.Hero_Square);
                break;
            }

            case SkinType.CardBack:
            {
                sprite = SpriteResourceHelper.LoadSprite(_itemId, SpriteResourceHelper.SpriteType.ICON_CardBack);
                break;
            }

            case SkinType.HUD:
            {
                sprite = SpriteResourceHelper.LoadSprite(_itemId, SpriteResourceHelper.SpriteType.ICON_HUD);
                break;
            }

            case SkinType.Title:
            {
                sprite = SpriteResourceHelper.LoadSprite(_itemId, SpriteResourceHelper.SpriteType.Title);
                break;
            }
        }

        IMG_Art_Main.sprite = sprite;
        IMG_Art_Disable.sprite = sprite;
    }

    #endregion

    #region Event
    private void SetupBTN(UnityAction onClick)
    {
        GetComponent<Button>().onClick.RemoveAllListeners();
        GetComponent<Button>().onClick.AddListener(delegate
        {
            if (onClick != null)
            {
                onClick.Invoke();
            }
        });
    }

    public void ShowGlow(bool isShow)
    {
        GlowSelected.SetActive(isShow);
    }

    private void ShowAvailable(bool isAvailable)
    {
        IMG_Art_Main.gameObject.SetActive(isAvailable);
        IMG_Art_Disable.gameObject.SetActive(!isAvailable);
    }
    #endregion
}
