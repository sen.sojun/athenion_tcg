﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using DG.Tweening;
using TMPro;
using UnityEngine.EventSystems;
using Karamucho.UI;

namespace Karamucho.Collection
{
    public class CollectionCardUI : CollectionItem
    {
        #region Public Properties
        public GameObject Group;

        [Header("Card Stat TEXT")]
        [SerializeField] private TextMeshProUGUI _TEXT_CardName;
        [SerializeField] private TextMeshProUGUI _TEXT_Description;
        [SerializeField] private TextMeshProUGUI _TEXT_Attack;
        [SerializeField] private TextMeshProUGUI _TEXT_HP;
        [SerializeField] private TextMeshProUGUI _TEXT_Soul;
        [SerializeField] private TextMeshProUGUI _TEXT_Armor;

        [Header("Card Stat Icon")]
        public Image PackIconImage;
        public Image AttackImage;
        public Image HpImage;
        public Image Frame;
        public Image FrameDescription;
        public Image ArmorImage;
        public GameObject ArtworkGroup;
        public Image Shadow;
        public GameObject CardAmountGroup;
        public TextMeshProUGUI CardAmount;
        public GameObject Effect_Rainbow;

        [Header("Arrow")]
        public List<GameObject> ArrowDir = new List<GameObject>();

        [Header("Toggle Object")]
        public GameObject GreyMask;
        public GameObject NewNotification;

        public CardUIData Data { get { return _data; } }
        #endregion

        #region Private Properties
        // Dynamic Scale reference parameter For card description
        private const float _DEFAULT_CARD_WIDTH = 804.0f;
        private const float _DEFAULT_FONT_SIZE = 36.0f;
        private Vector4 _DEFAULT_FONT_MARGIN = new Vector4(70, 50, 70, 110);
        private float _ratioDeference = 1.0f;

        private Vector3 _defaultScale = new Vector3(0.85f, 0.85f, 0.85f);
        private Vector3 _showOffset = new Vector3(0, 0, 0);
        private CardUIData _data;
        private Sequence _sq;
        private float _showTimer;

        private bool _tooltipIsShow = false;
        private bool _isCardSnap = false;
        private int _tempCardAmount;

        private UnityAction<CollectionCardUI> _onClickEvent = null;

        private PointerEventDataEvent _onUnityBeginDrag = null;
        private PointerEventDataEvent _onUnityDrag = null;
        private PointerEventDataEvent _onUnityEndDrag = null;
        #endregion


        #region Events
        public event Action<CollectionCardUI> OnCardBeginDragEvent;
        public event Action<CollectionCardUI> OnCardEndDragEvent;
        #endregion

        #region Methods
        protected new void Update()
        {
            base.Update();
        }

        public bool IsHaveObject(Type type)
        {
            //return true;

            return GameHelper.IsHaveObject(type);
        }

        public void SetData(CardUIData data)
        {
            if (_data != null)
            {
                if (_data.FullID != data.FullID)
                {
                    _data = data;
                }
            }
            else
            {
                _data = data;
            }

            SetName(_data.Name);
            SetAtk(_data.ATKCurrent, _data);
            SetHp(_data.HPCurrent, _data);
            SetSpirit(_data.SpiritCurrent);
            SetArmor(_data.ArmorCurrent, _data);
            ShowDirection(_data.CardDir);
            SetDescription(_data.Description);
            SetFrame(_data);
            SetImage(_data);
            SetPackIcon(_data);
            SetFoil();
            SetNewNotification(false);

            //if (IsHaveObject(typeof(CollectionManager_OLD)))
            //{
            //    // Check Is New Card
            //    if (CollectionManager_OLD.Instance.UI_IsCraftingShow)
            //    {
            //        SetNewNotification(false);
            //    }
            //    else
            //    {
            //        SetNewNotification(DataManager.Instance.InventoryData.IsNewCard(_data.FullID));
            //    }
            //}

            if (_data.Type == CardType.Minion || _data.Type == CardType.Minion_NotInDeck)
            {
                AttackImage.gameObject.SetActive(true);
                HpImage.gameObject.SetActive(true);
                _TEXT_Attack.gameObject.SetActive(true);
                _TEXT_HP.gameObject.SetActive(true);
                _TEXT_Soul.gameObject.SetActive(true);
            }
            else if (_data.Type == CardType.Spell || _data.Type == CardType.Spell_NotInDeck || _data.Type == CardType.Hero)
            {
                AttackImage.gameObject.SetActive(false);
                HpImage.gameObject.SetActive(false);
                _TEXT_Attack.gameObject.SetActive(false);
                _TEXT_HP.gameObject.SetActive(false);
                _TEXT_Soul.gameObject.SetActive(false);
            }
        }

        public void SetCardAmount(int value, bool isMaxCard = false)
        {
            //if (IsHaveObject(typeof(CollectionManager_OLD)))
            //{
            //    if (_tempCardAmount != value && CollectionManager_OLD.Instance.UI_IsCraftingShow)
            //    {
            //        _tempCardAmount = value;

            //        // Shake Animation
            //        Sequence sq = DOTween.Sequence();
            //        CardAmount.transform.localScale = new Vector3(1, 1, 1);
            //        sq.Append(CardAmount.transform.DOScale(1.5f, 0.3f));
            //        sq.Append(CardAmount.transform.DOScale(1.0f, 0.3f));
            //    }
            //}

            CardAmount.text = "x" + value;
            Color colorBrown = GameHelper.GetColorByHtmlCode("#5E3530");
            if (value <= 0)
            {
                CardAmountGroup.SetActive(false);
                CardAmount.color = Color.red;
                SetOwned(false);
            }
            else if (value == 1)
            {
                CardAmountGroup.SetActive(false);
                CardAmount.color = colorBrown;
                if (isMaxCard)
                {
                    SetOwned(false);
                }
                else
                {
                    SetOwned(true);
                }

            }
            else
            {
                CardAmountGroup.SetActive(true);
                CardAmount.color = colorBrown;
                if (isMaxCard)
                {
                    SetOwned(false);
                }
                else
                {
                    SetOwned(true);
                }

            }

            // Force Disable
            //CardAmountGroup.SetActive(false);
        }

        public void SetName(string message)
        {
            _TEXT_CardName.text = message;
        }

        public void SetFoil()
        {
            Effect_Rainbow.SetActive(_data.IsFoil);
        }

        public void SetArmor(int value, CardUIData data)
        {
            if (data.ArmorDefault <= 0)
            {
                _TEXT_Armor.gameObject.SetActive(false);
                ArmorImage.gameObject.SetActive(false);
            }
            else
            {
                _TEXT_Armor.gameObject.SetActive(true);
                ArmorImage.gameObject.SetActive(true);
            }

            Color color = new Color();
            if (value < data.ArmorDefault)
            {
                color = Color.yellow;
            }
            else if (value > data.ArmorDefault)
            {
                color = Color.green;
            }
            else
            {
                color = Color.white;
            }
            _TEXT_Armor.text = value.ToString();
            _TEXT_Armor.color = color;
        }

        public void SetAtk(int value, CardUIData data)
        {
            Color color = new Color();
            if (value < data.ATKDefault)
            {
                color = Color.red;
            }
            else if (value > data.ATKDefault)
            {
                color = Color.green;
            }
            else
            {
                color = Color.white;
            }
            _TEXT_Attack.text = value.ToString();
            _TEXT_Attack.color = color;
        }

        public void SetHp(int value, CardUIData data)
        {
            Color color = new Color();
            if (value < data.HPDefault)
            {
                color = Color.red;
            }
            else if (value > data.HPDefault)
            {
                color = Color.green;
            }
            else
            {
                color = Color.white;
            }
            _TEXT_HP.text = value.ToString();
            _TEXT_HP.color = color;
        }

        public void SetSpirit(int value)
        {
            _TEXT_Soul.text = value.ToString();
            //Spirit.text = GameHelper.GetIntToRomanNumber(value).ToString();
        }

        public void SetDescription(string message)
        {
            if (string.IsNullOrEmpty(message) || string.IsNullOrWhiteSpace(message))
            {
                _TEXT_Description.text = "\n\n\n";
            }
            else
            {
                _TEXT_Description.text = message;
            }

            SetDynamicFont();
        }

        private void SetDynamicFont()
        {
            if (transform.parent.GetComponent<GridLayoutGroup>())
            {
                // Get cell size
                float cellSize = transform.parent.GetComponent<GridLayoutGroup>().cellSize.x;
                _ratioDeference = (float)(cellSize / _DEFAULT_CARD_WIDTH);

                // Calculate Margin
                _TEXT_Description.fontSize = _DEFAULT_FONT_SIZE * _ratioDeference;
                _TEXT_Description.margin = new Vector4(
                    _DEFAULT_FONT_MARGIN.x * _ratioDeference,
                    _DEFAULT_FONT_MARGIN.y * _ratioDeference,
                    _DEFAULT_FONT_MARGIN.z * _ratioDeference,
                    _DEFAULT_FONT_MARGIN.w * _ratioDeference
                    );
            }

        }

        private void SetFrame(CardUIData data)
        {
            if (data.Type == CardType.Minion || data.Type == CardType.Minion_NotInDeck || data.Type == CardType.Spell)
            {
                Sprite tempSprite;
                Sprite boxSprite;
                CardResourceManager.LoadCardFrame(data.Rarity, data.Element.ToCardElementType(), out tempSprite);
                CardResourceManager.LoadCardFrameBox(data.Rarity, out boxSprite);
                Frame.sprite = tempSprite;
                FrameDescription.sprite = boxSprite;
            }
        }

        public void ShowDirection(CardDirection dirValue)
        {
            foreach (CardDirectionType item in System.Enum.GetValues(typeof(CardDirectionType)))
            {
                bool isShow = dirValue.IsDirection(item);
                ArrowDir[(int)item].SetActive(isShow);
            }
        }

        private void SetImage(CardUIData data)
        {
            //Debug.LogFormat("SetImage {0}", data.ImageKey);

            if (ArtworkGroup == null)
            {
                Debug.LogError("CollectionCardUI/SetImage: ArtworkGroup is null.");
                return;
            }

            // Clear previous image
            {
                foreach (Transform child in ArtworkGroup.transform)
                {
                    Destroy(child.gameObject);
                }
            }

            CardResourceManager.CreateAsync(
                CardResourceManager.ImageType.InfoCard
                , data.ImageKey
                , ArtworkGroup.transform
                , false
                , delegate(GameObject obj) 
                {
                    if (obj != null)
                    {
                        obj.transform.localPosition = Vector3.zero;
                        obj.transform.localRotation = Quaternion.identity;
                    }
                    SetMask(data);
                }
            );
        }

        private void SetMask(CardUIData data)
        {
            foreach (Transform child in ArtworkGroup.transform)
            {
                if (data.Type == CardType.Minion || data.Type == CardType.Minion_NotInDeck || data.Type == CardType.Spell)
                {
                    Sprite tempSprite;
                    CardResourceManager.LoadCardMaskFrame(data.Rarity, data.Element.ToCardElementType(), out tempSprite);
                    child.GetComponent<Image>().sprite = tempSprite;
                }
            }
        }

        private void SetPackIcon(CardUIData data)
        {
            Sprite sprite;
            CardResourceManager.LoadSeriesIcon(data.SeriesKey, out sprite);

            PackIconImage.sprite = sprite;
        }

        public void ShowShadow(bool isShow)
        {
            Shadow.gameObject.SetActive(isShow);
        }

        public void SetOwned(bool isShow)
        {
            GreyMask.SetActive(!isShow);
        }

        public void SetNewNotification(bool isShow)
        {
            if (NewNotification != null)
            {
                NewNotification.SetActive(isShow);
            }
        }

        public CardUIData GetCardData()
        {
            return _data;
        }

        public void SetOnClickEvent(UnityAction<CollectionCardUI> onClick)
        {
            _onClickEvent = onClick;
        }

        public void AddListenerOnUnityBeginDrag(UnityAction<PointerEventData> onUnityBeginDrag)
        {
            if (_onUnityBeginDrag == null)
            {
                _onUnityBeginDrag = new PointerEventDataEvent(); 
            }

            _onUnityBeginDrag.AddListener(onUnityBeginDrag);
        }

        public void AddListenerOnUnityDrag(UnityAction<PointerEventData> onUnityDrag)
        {
            if (_onUnityDrag == null)
            {
                _onUnityDrag = new PointerEventDataEvent();
            }

            _onUnityDrag.AddListener(onUnityDrag);
        }

        public void AddListenerOnUnityEndDrag(UnityAction<PointerEventData> onUnityEndDrag)
        {
            if (_onUnityEndDrag == null)
            {
                _onUnityEndDrag = new PointerEventDataEvent();
            }

            _onUnityEndDrag.AddListener(onUnityEndDrag);
        }

        public void RemoveAllListenerOnUnityBeginDrag()
        {
            if (_onUnityBeginDrag != null)
            {
                _onUnityBeginDrag.RemoveAllListeners();
            }
        }

        public void RemoveAllListenerOnUnityDrag()
        {
            if (_onUnityDrag != null)
            {
                _onUnityDrag.RemoveAllListeners();
            }
        }

        public void RemoveAllListenerOnUnityEndDrag()
        {
            if (_onUnityEndDrag != null)
            {
                _onUnityEndDrag.RemoveAllListeners();
            }
        }
        #endregion

        #region Input Event
        public override void OnEventUnityPointerDown(PointerEventData eventData)
        {
            base.OnEventUnityPointerDown(eventData);

            //if (IsHaveObject(typeof(CollectionManager_OLD)))
            //{
            //    if (CollectionManager_OLD.Instance.UI_IsCraftingShow) return;

            //    ShowShadow(true);
            //}
            //else if (IsHaveObject(typeof(StarChamberManager)))
            //{
            //    if (StarChamberManager.Instance.UI_IsCraftingShow) return;

            //    ShowShadow(true);
            //}
            //else if (IsHaveObject(typeof(EventBattleManager)))
            //{
            //    if (EventBattleManager.Instance.Is_UIDetailCardShow) return;

            //    ShowShadow(true);
            //}
            //else
            //{
            //    //ShowShadow(true);
            //}
        }

        public override void OnEventUnityPointerUp(PointerEventData eventData)
        {
            base.OnEventUnityPointerUp(eventData);

            //if (IsHaveObject(typeof(CollectionManager_OLD)))
            //{
            //    if (CollectionManager_OLD.Instance.UI_IsCraftingShow) return;

            //    ShowShadow(false);
            //}
            //else if (IsHaveObject(typeof(StarChamberManager)))
            //{
            //    if (StarChamberManager.Instance.UI_IsCraftingShow) return;

            //    ShowShadow(false);
            //}
            //else if (IsHaveObject(typeof(EventBattleManager)))
            //{
            //    if (EventBattleManager.Instance.Is_UIDetailCardShow) return;

            //    ShowShadow(false);
            //}
            //else
            //{
            //    //ShowShadow(false);
            //}
        }

        public override void OnEventUnityBeginDrag(PointerEventData eventData)
        {
            base.OnEventUnityBeginDrag(eventData);

            if (_onUnityBeginDrag != null)
            {
                _onUnityBeginDrag.Invoke(eventData);
            }
        }

        public override void OnEventUnityDrag(PointerEventData eventData)
        {
            base.OnEventUnityDrag(eventData);

            if (_onUnityDrag != null)
            {
                _onUnityDrag.Invoke(eventData);
            }
        }

        public override void OnEventUnityEndDrag(PointerEventData eventData)
        {
            base.OnEventUnityEndDrag(eventData);

            if (_onUnityEndDrag != null)
            {
                _onUnityEndDrag.Invoke(eventData);
            }
        }

        public override void OnEventClick(PointerEventData eventData)
        {
            base.OnEventClick(eventData);

            //if (IsHaveObject(typeof(CollectionManager_OLD)))
            //{
            //    if (CollectionManager_OLD.Instance.UI_IsCraftingShow) return;

            //    CollectionManager_OLD.Instance.OnCard_Click(this);
            //}
            //else if (IsHaveObject(typeof(StarChamberManager)))
            //{
            //    if (StarChamberManager.Instance.UI_IsCraftingShow) return;

            //    StarChamberManager.Instance.OnCard_Click(this);
            //}

            if (_onClickEvent != null)
            {
                _onClickEvent.Invoke(this);
            }
        }

        public override void OnEventDoubleClick(PointerEventData eventData)
        {
            base.OnEventDoubleClick(eventData);

            //if (IsHaveObject(typeof(CollectionManager_OLD)))
            //{                
            //    CollectionManager_OLD.Instance.OnCard_DoubleClick(this);
            //}
        }

        public override void OnEventBeginDrag(PointerEventData eventData)
        {
            base.OnEventBeginDrag(eventData);

            OnCardBeginDragEvent?.Invoke(this);
            //if (IsHaveObject(typeof(CollectionManager_OLD)))
            //{                
            //    CollectionManager_OLD.Instance.OnCard_StartDrag(this);
            //    //CollectionManager.Instance.CollectionSwipeZone.OnBeginDrag(eventData);
            //}
        }

        public override void OnEventDrag(PointerEventData eventData)
        {
            base.OnEventDrag(eventData);

            //if (IsHaveObject(typeof(CollectionManager_OLD)))
            //{                
            //    //if (_isCardSnap)
            //    {
            //        CollectionManager_OLD.Instance.OnCard_Drag(this);
            //    }
            //    //else
            //    {
            //        //CollectionManager.Instance.CollectionSwipeZone.OnDrag(eventData);
            //    }
            //}
        }

        public override void OnEventEndDrag(PointerEventData eventData)
        {
            base.OnEventEndDrag(eventData);

            OnCardEndDragEvent?.Invoke(this);
            //if (IsHaveObject(typeof(CollectionManager_OLD)))
            //{
            //    //_isCardSnap = false;
            //    CollectionManager_OLD.Instance.OnCard_EndDrag(this);
            //}
        }

        public override void OnEventBeginHold(PointerEventData eventData)
        {
            base.OnEventBeginHold(eventData);

            //if (IsHaveObject(typeof(CollectionManager_OLD)))
            //{
            //    if (CollectionManager_OLD.Instance.UI_IsCraftingShow) return;

            //    //_isCardSnap = true;
            //    CollectionManager_OLD.Instance.OnCard_StartHold(this);
            //}
        }

        public override void OnEventEndHold(PointerEventData eventData)
        {
            base.OnEventEndHold(eventData);

            //if (IsHaveObject(typeof(CollectionManager_OLD)))
            //{
            //    if (CollectionManager_OLD.Instance.UI_IsCraftingShow) return;

            //    CollectionManager_OLD.Instance.OnCard_EndHold(this);
            //}
        }
        #endregion
    }
}
