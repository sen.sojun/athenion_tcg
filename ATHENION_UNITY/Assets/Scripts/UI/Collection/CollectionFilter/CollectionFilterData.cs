﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#region Base Class
public abstract class CollectionFilterData
{
    #region Protected Properties
    protected List<string> _paramData = new List<string>();
    #endregion

    #region Methods
    public abstract List<CardData> GetFilterResult(List<CardData> cardDataList, CollectionFilterParamList filterParams);
    #endregion
}
#endregion

#region Filter Soul
public class CollectionFilterData_Soul : CollectionFilterData
{
    public override List<CardData> GetFilterResult(List<CardData> cardDataList, CollectionFilterParamList filterParams)
    {
        _paramData = filterParams.GetFilterParamData(CollectionFilterTypes.Soul);
        if (_paramData.Count == 0)
        {
            return cardDataList;
        }

        List<CardData> result = new List<CardData>();
        foreach (CardData item in cardDataList)
        {
            if(_paramData.Contains(item.Spirit.ToString()))
            {
                result.Add(item);
            }
        }

        return result;
    }
}
#endregion

#region Filter Faction
public class CollectionFilterData_Faction : CollectionFilterData
{
    public override List<CardData> GetFilterResult(List<CardData> cardDataList, CollectionFilterParamList filterParams)
    {
        _paramData = filterParams.GetFilterParamData(CollectionFilterTypes.Faction);
        if (_paramData.Count == 0)
        {
            return cardDataList;
        }

        List<CardData> result = new List<CardData>();
        foreach (CardData item in cardDataList)
        {
            if (_paramData.Contains(item.CardElement.ToCardElementType().ToString()))
            {
                result.Add(item);
            }
        }
        return result;
    }
}
#endregion

#region Filter Rarity
public class CollectionFilterData_Rarity : CollectionFilterData
{
    public override List<CardData> GetFilterResult(List<CardData> cardDataList, CollectionFilterParamList filterParams)
    {
        _paramData = filterParams.GetFilterParamData(CollectionFilterTypes.Rarity);
        if (_paramData.Count == 0)
        {
            return cardDataList;
        }

        List<CardData> result = new List<CardData>();
        foreach (CardData item in cardDataList)
        {
            if (_paramData.Contains(item.Rarity.ToString()))
            {
                result.Add(item);
            }
        }
        return result;
    }
}
#endregion

#region Filter Word
public class CollectionFilterData_Word : CollectionFilterData
{
    public override List<CardData> GetFilterResult(List<CardData> cardDataList, CollectionFilterParamList filterParams)
    {
        _paramData = filterParams.GetFilterParamData(CollectionFilterTypes.Word);
        if (_paramData.Count == 0)
        {
            return cardDataList;
        }

        string paramStr = filterParams.GetFilterParamString(CollectionFilterTypes.Word);
        List<CardData> result = new List<CardData>();
        foreach (CardData item in cardDataList)
        {
            string cardName = item.Name.ToLower();
            string cardDes = item.Description.ToLower();

            if (cardName.Contains(paramStr.ToLower()) || cardDes.Contains(paramStr.ToLower()))
            {
                result.Add(item);
            }
        }

        return result;
    }
}
#endregion

#region Filter Owned
public class CollectionFilterData_Owned : CollectionFilterData
{
    public override List<CardData> GetFilterResult(List<CardData> cardDataList, CollectionFilterParamList filterParams)
    {
        _paramData = filterParams.GetFilterParamData(CollectionFilterTypes.Owned);
        if (_paramData.Count == 0)
        {
            return cardDataList;
        }

        OwnedFilterTypes filterParam;
        List<CardData> result = new List<CardData>();
        CardListData allCardData = DataManager.Instance.GetCardListCatalog();
        CardListData inventoryCardData = DataManager.Instance.InventoryData.CardInventoryList;

        foreach (string param in _paramData)
        {
            if (GameHelper.StrToEnum(param, out filterParam))
            {
                int count = 0;
                if (filterParam == OwnedFilterTypes.All)
                {
                    foreach (CardData item in cardDataList)
                    {
                        if (allCardData.TryGetValue(item.FullID, out count))
                        {
                            result.Add(item);
                        }
                    }
                }
                else if (filterParam == OwnedFilterTypes.Owned)
                {
                    foreach (CardData item in cardDataList)
                    {
                        if (inventoryCardData.TryGetValue(item.FullID, out count))
                        {
                            if(count > 0)
                            {
                                result.Add(item);
                            }
                        }
                    }
                }
            }
        }

        return result;
    }
}
#endregion

#region Filter Craftable
public class CollectionFilterData_Craftabled : CollectionFilterData
{
    public override List<CardData> GetFilterResult(List<CardData> cardDataList, CollectionFilterParamList filterParams)
    {
        _paramData = filterParams.GetFilterParamData(CollectionFilterTypes.Craftable);
        if (_paramData.Count == 0)
        {
            return cardDataList;
        }

        CraftableFilterTypes filterParam;
        List<CardData> result = new List<CardData>();
        foreach (string param in _paramData)
        {
            if (GameHelper.StrToEnum(param, out filterParam))
            {
                foreach (CardData item in cardDataList)
                {
                    switch (filterParam)
                    {
                        case CraftableFilterTypes.Craftable:
                        {
                            if (DataManager.Instance.IsItemCatalogTagListContain(item.FullID, "craftable"))
                            {
                                result.Add(item);
                            }
                        }
                        break;

                        case CraftableFilterTypes.All:
                        {
                            result.Add(item);
                        }
                        break;
                    }
                }
            }
        }

        return result;
    }
}
#endregion

#region Filter Series
public class CollectionFilterData_Series : CollectionFilterData
{
    public override List<CardData> GetFilterResult(List<CardData> cardDataList, CollectionFilterParamList filterParams)
    {
        _paramData = filterParams.GetFilterParamData(CollectionFilterTypes.Series);
        if (_paramData.Count == 0)
        {
            return cardDataList;
        }

        List<CardData> result = new List<CardData>();
        foreach (CardData item in cardDataList)
        {
            if (_paramData.Contains(item.SeriesKey.ToUpper()))
            {
                result.Add(item);
            }
        }

        return result;
    }
}
#endregion

#region Filter Foil
public class CollectionFilterData_Foil : CollectionFilterData
{
    public override List<CardData> GetFilterResult(List<CardData> cardDataList, CollectionFilterParamList filterParams)
    {
        _paramData = filterParams.GetFilterParamData(CollectionFilterTypes.Foil);
        if (_paramData.Count == 0)
        {
            return cardDataList;
        }

        List<CardData> result = new List<CardData>();
        foreach (CardData item in cardDataList)
        {
            if (item.IsFoil)
            {
                if(_paramData.Contains(FoilFilterTypes.Foil.ToString()))
                {
                    result.Add(item);
                }
            }
            else
            {
                if (_paramData.Contains(FoilFilterTypes.Standard.ToString()))
                {
                    result.Add(item);
                }
            }
        }

        return result;
    }
}
#endregion