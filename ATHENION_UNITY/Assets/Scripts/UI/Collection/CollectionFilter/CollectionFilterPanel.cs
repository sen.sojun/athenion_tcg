using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;

namespace Karamucho.Collection {
    public class CollectionFilterPanel : MonoBehaviour {

        #region Inpector Properties
        [Header("Header")]
        [SerializeField] private Button _BTN_Okay;
        [SerializeField] private Button _BTN_Close;

        [SerializeField] private Transform _GROUP_Panel;
        [SerializeField] private List<CollectionFilterNode> _collectionFilterNodeList;
        #endregion

        #region Methods
        private void Start()
        {
            _BTN_Okay.onClick.RemoveAllListeners();
            _BTN_Okay.onClick.AddListener(HideUI);

            _BTN_Close.onClick.RemoveAllListeners();
            _BTN_Close.onClick.AddListener(HideUI);
        }

        public void ShowUI()
        {
            GameHelper.UITransition_FadeIn(this.gameObject);
        }

        public void HideUI()
        {
            GameHelper.UITransition_FadeOut(this.gameObject);
        }

        public void SyncFilterParam()
        {
            foreach (CollectionFilterNode node in _collectionFilterNodeList)
            {
                node.SyncFilterParam();
            }
        }
        #endregion
    }
}