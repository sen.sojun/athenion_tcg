using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;
using System;

public class CollectionFilterManager : MonoSingleton<CollectionFilterManager>
{
    #region Inspector
    [SerializeField] private CollectionFilterNode_Faction _FilterNode_Faction;
    #endregion

    #region Public Properties
    public CollectionFilterParamList FilterParamDataList { get { return _filterParamDataList; } }
    #endregion

    #region Private Properties
    private List<CardData> _filterCardList = new List<CardData>();
    private CollectionFilterParamList _defaultFilterParamDataList = new CollectionFilterParamList();
    private CollectionFilterParamList _filterParamDataList = new CollectionFilterParamList();

    private List<CollectionFilterData> _collectionFilterDataList = new List<CollectionFilterData>();
    private CardListData _cardDataStack = new CardListData();
    private CardListData _inventoryDataStack = new CardListData();

    private bool _isInit = false;
    #endregion

    #region Methods

    #region Init
    private void InitFilterData()
    {
        if(_isInit)
        {
            return;
        }

        // init all filter data
        _collectionFilterDataList.Add(new CollectionFilterData_Soul());
        _collectionFilterDataList.Add(new CollectionFilterData_Faction());
        _collectionFilterDataList.Add(new CollectionFilterData_Rarity());
        _collectionFilterDataList.Add(new CollectionFilterData_Word());
        _collectionFilterDataList.Add(new CollectionFilterData_Owned());
        _collectionFilterDataList.Add(new CollectionFilterData_Craftabled());
        _collectionFilterDataList.Add(new CollectionFilterData_Series());
        _collectionFilterDataList.Add(new CollectionFilterData_Foil());

        _isInit = true;
    }
    #endregion

    #region Set
    public void SetFilterNodeFaction(string id)
    {
        CardElementType element;
        GameHelper.StrToEnum(id, out element);
        _FilterNode_Faction.SetupFilterNode(element);
        _FilterNode_Faction.SyncFilterParam();
    }

    public void ResetFilterNodeFaction()
    {
        _FilterNode_Faction.SetupFilterNode();
        _FilterNode_Faction.SyncFilterParam();
    }
    #endregion

    #region Get
    public List<CardData> GetCalculatedFilterData(List<CardData> baseCardList)
    {
        // init
        InitFilterData();

        // get calculated result
        _filterCardList = new List<CardData>(baseCardList);
        foreach (CollectionFilterData item in _collectionFilterDataList)
        {
            _filterCardList = item.GetFilterResult(_filterCardList, _defaultFilterParamDataList);
        }

        foreach (CollectionFilterData item in _collectionFilterDataList)
        {
            _filterCardList = item.GetFilterResult(_filterCardList, _filterParamDataList);
        }

        return _filterCardList;
    }
    #endregion

    #region Reset Filter
    public void ResetFilter()
    {
        _filterParamDataList.ClearAllFilterParam();
    }

    public void ResetFilter(CollectionFilterTypes type)
    {
        _filterParamDataList.ClearFilterParam(type);
    }

    public void ResetDefaultFilter()
    {
        _defaultFilterParamDataList.ClearAllFilterParam();
    }

    public void ResetDefaultFilter(CollectionFilterTypes type)
    {
        _defaultFilterParamDataList.ClearFilterParam(type);
    }
    #endregion

    #region Add Filter
    public void AddDefaultFilterParam(CollectionFilterTypes type, string paramStr)
    {
        _defaultFilterParamDataList.AddFilterParam(type, paramStr);
    }

    public void AddFilterParam(CollectionFilterTypes type, string paramStr)
    {
        _filterParamDataList.AddFilterParam(type, paramStr);
    }

    public void AddFilterParam(CollectionFilterTypes type, CardRarity rarity)
    {
        AddFilterParam(type, rarity.ToString());
    }

    public void AddFilterParam(CollectionFilterTypes type, CardElementType faction)
    {
        AddFilterParam(type, faction.ToString());
    }

    public void AddFilterParam(CollectionFilterTypes type, SeriesKeyFilterTypes series)
    {
        AddFilterParam(type, series.ToString());
    }

    public void AddFilterParam(CollectionFilterTypes type, OwnedFilterTypes owner)
    {
        AddFilterParam(type, owner.ToString());
    }

    public void AddFilterParam(CollectionFilterTypes type, int soul)
    {
        AddFilterParam(type, soul.ToString());
    }

    public void AddFilterParam(CollectionFilterTypes type, CraftableFilterTypes craftable)
    {
        AddFilterParam(type, craftable.ToString());
    }
    #endregion

    #region Remove Filter
    public void RemoveDefaultFilterParam(CollectionFilterTypes type, string paramStr)
    {
        _defaultFilterParamDataList.RemoveFilterParam(type, paramStr);
    }

    public void RemoveFilterParam(CollectionFilterTypes type, List<string> paramStrList)
    {
        foreach (string param in paramStrList)
        {
            _filterParamDataList.RemoveFilterParam(type, param);
        }
    }

    public void RemoveFilterParam(CollectionFilterTypes type, string paramStr)
    {
        _filterParamDataList.RemoveFilterParam(type, paramStr);
    }

    public void RemoveFilterParam(CollectionFilterTypes type, CardRarity rarity)
    {
        RemoveFilterParam(type, rarity.ToString());
    }

    public void RemoveFilterParam(CollectionFilterTypes type, CardElementType faction)
    {
        RemoveFilterParam(type, faction.ToString());
    }

    public void RemoveFilterParam(CollectionFilterTypes type, SeriesKeyFilterTypes series)
    {
        RemoveFilterParam(type, series.ToString());
    }

    public void RemoveFilterParam(CollectionFilterTypes type, OwnedFilterTypes owner)
    {
        RemoveFilterParam(type, owner.ToString());
    }

    public void RemoveFilterParam(CollectionFilterTypes type, int soul)
    {
        RemoveFilterParam(type, soul.ToString());
    }

    public void RemoveFilterParam(CollectionFilterTypes type, CraftableFilterTypes craftable)
    {
        RemoveFilterParam(type, craftable.ToString());
    }
    #endregion

    #endregion
}