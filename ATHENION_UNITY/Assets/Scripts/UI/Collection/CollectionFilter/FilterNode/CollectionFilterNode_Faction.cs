﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectionFilterNode_Faction : CollectionFilterNode
{
    #region Inpector Properties
    [SerializeField] private Toggle _Toggle_Fire;
    [SerializeField] private Toggle _Toggle_Water;
    [SerializeField] private Toggle _Toggle_Earth;
    [SerializeField] private Toggle _Toggle_Air;
    [SerializeField] private Toggle _Toggle_Holy;
    [SerializeField] private Toggle _Toggle_Dark;
    [SerializeField] private Toggle _Toggle_Nautral;
    #endregion

    protected override CollectionFilterTypes _filterType => CollectionFilterTypes.Faction;

    #region Methods
    private void OnEnable()
    {
        SyncFilterParam();
    }

    protected override void Setup()
    {
        _Toggle_Fire.onValueChanged.RemoveAllListeners();
        _Toggle_Fire.onValueChanged.AddListener(
            (isToggle) =>
            {
                AddFilterParam(isToggle, CardElementType.FIRE);
            }
        );

        _Toggle_Water.onValueChanged.RemoveAllListeners();
        _Toggle_Water.onValueChanged.AddListener(
            (isToggle) =>
            {
                AddFilterParam(isToggle, CardElementType.WATER);
            }
        );

        _Toggle_Earth.onValueChanged.RemoveAllListeners();
        _Toggle_Earth.onValueChanged.AddListener(
            (isToggle) =>
            {
                AddFilterParam(isToggle, CardElementType.EARTH);
            }
        );

        _Toggle_Air.onValueChanged.RemoveAllListeners();
        _Toggle_Air.onValueChanged.AddListener(
            (isToggle) =>
            {
                AddFilterParam(isToggle, CardElementType.AIR);
            }
        );

        _Toggle_Holy.onValueChanged.RemoveAllListeners();
        _Toggle_Holy.onValueChanged.AddListener(
            (isToggle) =>
            {
                AddFilterParam(isToggle, CardElementType.HOLY);
            }
        );

        _Toggle_Dark.onValueChanged.RemoveAllListeners();
        _Toggle_Dark.onValueChanged.AddListener(
            (isToggle) =>
            {
                AddFilterParam(isToggle, CardElementType.DARK);
            }
        );

        _Toggle_Nautral.onValueChanged.RemoveAllListeners();
        _Toggle_Nautral.onValueChanged.AddListener(
            (isToggle) =>
            {
                AddFilterParam(isToggle, CardElementType.NEUTRAL);
            }
        );
    }

    public void SetupFilterNode(CardElementType faction)
    {
        _Toggle_Fire.gameObject.SetActive(faction == CardElementType.FIRE);
        _Toggle_Water.gameObject.SetActive(faction == CardElementType.WATER);
        _Toggle_Earth.gameObject.SetActive(faction == CardElementType.EARTH);
        _Toggle_Air.gameObject.SetActive(faction == CardElementType.AIR);
        _Toggle_Holy.gameObject.SetActive(faction == CardElementType.HOLY);
        _Toggle_Dark.gameObject.SetActive(faction == CardElementType.DARK);
    }

    public void SetupFilterNode()
    {
        _Toggle_Fire.gameObject.SetActive(true);
        _Toggle_Water.gameObject.SetActive(true);
        _Toggle_Earth.gameObject.SetActive(true);
        _Toggle_Air.gameObject.SetActive(true);
        _Toggle_Holy.gameObject.SetActive(true);
        _Toggle_Dark.gameObject.SetActive(true);
        _Toggle_Nautral.gameObject.SetActive(true);
    }

    public override void SyncFilterParam()
    {
        List<string> paramData = CollectionFilterManager.Instance.FilterParamDataList.GetFilterParamData(_filterType);
        _Toggle_Fire.isOn = paramData.Contains(CardElementType.FIRE.ToString());
        _Toggle_Water.isOn = paramData.Contains(CardElementType.WATER.ToString());
        _Toggle_Earth.isOn = paramData.Contains(CardElementType.EARTH.ToString());
        _Toggle_Air.isOn = paramData.Contains(CardElementType.AIR.ToString());
        _Toggle_Holy.isOn = paramData.Contains(CardElementType.HOLY.ToString());
        _Toggle_Dark.isOn = paramData.Contains(CardElementType.DARK.ToString());
        _Toggle_Nautral.isOn = paramData.Contains(CardElementType.NEUTRAL.ToString());
    }

    private void AddFilterParam(bool isAdded, CardElementType faction)
    {
        AddFilterParam(isAdded, faction.ToString());
    }
    #endregion
}