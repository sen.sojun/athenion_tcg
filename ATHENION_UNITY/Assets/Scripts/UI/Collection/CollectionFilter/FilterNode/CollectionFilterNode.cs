﻿using Karamucho.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CollectionFilterNode : MonoBehaviour
{
    #region Protected Properties
    protected List<string> _paramList = new List<string>();
    protected abstract CollectionFilterTypes _filterType { get; }
    #endregion

    #region Events
    public static event Action OnFilterNodeUpdate;
    #endregion

    #region Methods
    protected void Start()
    {
        Setup();
    }

    protected abstract void Setup();

    public abstract void SyncFilterParam();

    public virtual void AddFilterParam(bool isAdded, string paramStr)
    {
        if (isAdded)
        {
            _paramList.Add(paramStr);
            CollectionFilterManager.Instance.AddFilterParam(_filterType, paramStr);
        }
        else
        {
            _paramList.Remove(paramStr);
            CollectionFilterManager.Instance.RemoveFilterParam(_filterType, paramStr);
        }

        InvokeFilterNodeUpdate();
    }

    protected void InvokeFilterNodeUpdate()
    {
        OnFilterNodeUpdate?.Invoke();
    }
    #endregion
}