﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectionFilterNode_Series : CollectionFilterNode
{
    #region Inpector Properties
    [SerializeField] private Toggle _Toggle_Basic;
    [SerializeField] private Toggle _Toggle_Woe;
    [SerializeField] private Toggle _Toggle_GM;
    [SerializeField] private Toggle _Toggle_Star;
    [SerializeField] private Toggle _Toggle_Faction;
    #endregion

    protected override CollectionFilterTypes _filterType => CollectionFilterTypes.Series;

    #region Methods
    protected override void Setup()
    {
        _Toggle_Basic.onValueChanged.RemoveAllListeners();
        _Toggle_Basic.onValueChanged.AddListener(
            (isToggle) =>
            {
                AddFilterParam(isToggle, SeriesKeyFilterTypes.BASIC);
            }
        );

        _Toggle_Woe.onValueChanged.RemoveAllListeners();
        _Toggle_Woe.onValueChanged.AddListener(
            (isToggle) =>
            {
                AddFilterParam(isToggle, SeriesKeyFilterTypes.WOE);
            }
        );

        _Toggle_GM.onValueChanged.RemoveAllListeners();
        _Toggle_GM.onValueChanged.AddListener(
            (isToggle) =>
            {
                AddFilterParam(isToggle, SeriesKeyFilterTypes.GM);
            }
        );

        _Toggle_Star.onValueChanged.RemoveAllListeners();
        _Toggle_Star.onValueChanged.AddListener(
            (isToggle) =>
            {
                AddFilterParam(isToggle, SeriesKeyFilterTypes.STAR);
            }
        );
        _Toggle_Faction.onValueChanged.RemoveAllListeners();
        _Toggle_Faction.onValueChanged.AddListener(
            (isToggle) =>
            {
                AddFilterParam(isToggle, SeriesKeyFilterTypes.FACTION);
            }
        );
    }

    public override void SyncFilterParam()
    {
        List<string> paramData = CollectionFilterManager.Instance.FilterParamDataList.GetFilterParamData(_filterType);
        _Toggle_Basic.isOn = paramData.Contains(SeriesKeyFilterTypes.BASIC.ToString());
        _Toggle_Woe.isOn = paramData.Contains(SeriesKeyFilterTypes.WOE.ToString());
        _Toggle_GM.isOn = paramData.Contains(SeriesKeyFilterTypes.GM.ToString());
        _Toggle_Star.isOn = paramData.Contains(SeriesKeyFilterTypes.STAR.ToString());
        _Toggle_Faction.isOn = paramData.Contains(SeriesKeyFilterTypes.FACTION.ToString());
    }

    private void AddFilterParam(bool isAdded, SeriesKeyFilterTypes series)
    {
        AddFilterParam(isAdded, series.ToString());
    }
    #endregion
}
