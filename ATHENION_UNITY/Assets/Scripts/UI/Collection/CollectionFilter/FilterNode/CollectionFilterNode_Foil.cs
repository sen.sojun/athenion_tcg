﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectionFilterNode_Foil : CollectionFilterNode
{
    #region Inpector Properties
    [SerializeField] private Toggle _Toggle_IsStandard;
    [SerializeField] private Toggle _Toggle_IsFoil;
    #endregion

    protected override CollectionFilterTypes _filterType => CollectionFilterTypes.Foil;

    #region Methods
    protected override void Setup()
    {
        _Toggle_IsStandard.onValueChanged.RemoveAllListeners();
        _Toggle_IsStandard.onValueChanged.AddListener(
            (isToggle)=> 
            {
                AddFilterParam(isToggle, FoilFilterTypes.Standard);
            }
        );

        _Toggle_IsFoil.onValueChanged.RemoveAllListeners();
        _Toggle_IsFoil.onValueChanged.AddListener(
            (isToggle) =>
            {
                AddFilterParam(isToggle, FoilFilterTypes.Foil);
            }
        );
    }

    public override void SyncFilterParam()
    {
        List<string> paramData = CollectionFilterManager.Instance.FilterParamDataList.GetFilterParamData(_filterType);
        _Toggle_IsStandard.isOn = paramData.Contains(FoilFilterTypes.Standard.ToString());
        _Toggle_IsFoil.isOn = paramData.Contains(FoilFilterTypes.Foil.ToString());
    }

    private void AddFilterParam(bool isAdded, FoilFilterTypes foilType)
    {
        AddFilterParam(isAdded, foilType.ToString());
    }
    #endregion
}