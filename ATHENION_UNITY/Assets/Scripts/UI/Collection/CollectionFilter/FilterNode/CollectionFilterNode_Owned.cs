﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectionFilterNode_Owned : CollectionFilterNode
{
    #region Inpector Properties
    [SerializeField] private Button _BTN_IsOwned;
    [SerializeField] private GameObject _Glow;
    #endregion

    protected override CollectionFilterTypes _filterType => CollectionFilterTypes.Owned;

    #region Private Properties
    private bool _isAll = false;
    #endregion

    #region Methods
    private void OnEnable()
    {
        SyncFilterParam();
    }

    private void ResetFilter()
    {
        _isAll = false;
        CollectionFilterManager.Instance.RemoveFilterParam(_filterType, OwnedFilterTypes.All.ToString());
        CollectionFilterManager.Instance.AddFilterParam(_filterType, OwnedFilterTypes.Owned.ToString());

        SyncFilterParam();
        InvokeFilterNodeUpdate();
    }

    protected override void Setup()
    {
        _BTN_IsOwned.onClick.RemoveAllListeners();
        _BTN_IsOwned.onClick.AddListener(AddFilterParam);
    }

    public override void SyncFilterParam()
    {
        List<string> paramData = CollectionFilterManager.Instance.FilterParamDataList.GetFilterParamData(_filterType);
        _isAll = paramData.Contains(OwnedFilterTypes.All.ToString());
        _Glow.SetActive(_isAll);
    }

    private void AddFilterParam()
    {
        _isAll = !_isAll;
        if (_isAll)
        {
            CollectionFilterManager.Instance.RemoveFilterParam(_filterType, OwnedFilterTypes.Owned.ToString());
            CollectionFilterManager.Instance.AddFilterParam(_filterType, OwnedFilterTypes.All.ToString());
        }
        else
        {
            CollectionFilterManager.Instance.RemoveFilterParam(_filterType, OwnedFilterTypes.All.ToString());
            CollectionFilterManager.Instance.AddFilterParam(_filterType, OwnedFilterTypes.Owned.ToString());
        }

        SyncFilterParam();
        InvokeFilterNodeUpdate();
    }
    #endregion
}