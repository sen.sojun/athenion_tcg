﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectionFilterNode_Soul : CollectionFilterNode
{
    #region Inpector Properties
    [SerializeField] private Toggle _Toggle_Soul_1;
    [SerializeField] private Toggle _Toggle_Soul_2;
    [SerializeField] private Toggle _Toggle_Soul_3;
    #endregion

    protected override CollectionFilterTypes _filterType => CollectionFilterTypes.Soul;

    #region Methods
    protected override void Setup()
    {
        _Toggle_Soul_1.onValueChanged.RemoveAllListeners();
        _Toggle_Soul_1.onValueChanged.AddListener(
            (isToggle) =>
            {
                AddFilterParam(isToggle, 1);
            }
        );

        _Toggle_Soul_2.onValueChanged.RemoveAllListeners();
        _Toggle_Soul_2.onValueChanged.AddListener(
            (isToggle) =>
            {
                AddFilterParam(isToggle, 2);
            }
        );

        _Toggle_Soul_3.onValueChanged.RemoveAllListeners();
        _Toggle_Soul_3.onValueChanged.AddListener(
            (isToggle) =>
            {
                AddFilterParam(isToggle, 3);
            }
        );
    }

    public override void SyncFilterParam()
    {
        List<string> paramData = CollectionFilterManager.Instance.FilterParamDataList.GetFilterParamData(_filterType);
        _Toggle_Soul_1.isOn = paramData.Contains("1");
        _Toggle_Soul_2.isOn = paramData.Contains("2");
        _Toggle_Soul_3.isOn = paramData.Contains("3");
    }

    private void AddFilterParam(bool isAdded, int soul)
    {
        AddFilterParam(isAdded, soul.ToString());
    }
    #endregion
}
