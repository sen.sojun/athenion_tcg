﻿using Karamucho.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectionFilterNode_Rarity : CollectionFilterNode
{
    #region Inpector Properties
    [SerializeField] private Toggle _Toggle_Common;
    [SerializeField] private Toggle _Toggle_Rare;
    [SerializeField] private Toggle _Toggle_Epic;
    [SerializeField] private Toggle _Toggle_Legendary;
    #endregion

    protected override CollectionFilterTypes _filterType => CollectionFilterTypes.Rarity;

    #region Methods
    protected override void Setup()
    {
        _Toggle_Common.onValueChanged.RemoveAllListeners();
        _Toggle_Common.onValueChanged.AddListener(
            (isToggle) =>
            {
                AddFilterParam(isToggle, CardRarity.Common);
            }
        );

        _Toggle_Rare.onValueChanged.RemoveAllListeners();
        _Toggle_Rare.onValueChanged.AddListener(
            (isToggle) =>
            {
                AddFilterParam(isToggle, CardRarity.Rare);
            }
        );

        _Toggle_Epic.onValueChanged.RemoveAllListeners();
        _Toggle_Epic.onValueChanged.AddListener(
            (isToggle) =>
            {
                AddFilterParam(isToggle, CardRarity.Epic);
            }
        );

        _Toggle_Legendary.onValueChanged.RemoveAllListeners();
        _Toggle_Legendary.onValueChanged.AddListener(
            (isToggle) =>
            {
                AddFilterParam(isToggle, CardRarity.Legend);
            }
        );

    }

    public override void SyncFilterParam()
    {
        List<string> paramData = CollectionFilterManager.Instance.FilterParamDataList.GetFilterParamData(_filterType);
        _Toggle_Common.isOn = paramData.Contains(CardRarity.Common.ToString());
        _Toggle_Rare.isOn = paramData.Contains(CardRarity.Rare.ToString());
        _Toggle_Epic.isOn = paramData.Contains(CardRarity.Epic.ToString());
        _Toggle_Legendary.isOn = paramData.Contains(CardRarity.Legend.ToString());
    }

    private void AddFilterParam(bool isAdded, CardRarity rarity)
    {
        AddFilterParam(isAdded, rarity.ToString());
    }
    #endregion
}
