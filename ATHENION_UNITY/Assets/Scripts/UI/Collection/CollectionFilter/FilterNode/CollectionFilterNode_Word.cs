﻿using Karamucho.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CollectionFilterNode_Word : CollectionFilterNode
{
    #region Inpector Properties
    [SerializeField] private TMP_InputField _Input_Word;
    #endregion

    #region Private Properties
    private string _prevWord = string.Empty;
    #endregion

    protected override CollectionFilterTypes _filterType => CollectionFilterTypes.Word;

    #region Methods
    protected override void Setup()
    {
        _Input_Word.onEndEdit.RemoveAllListeners();
        _Input_Word.onEndEdit.AddListener(
            delegate
            {
                AddFilterParam(true, _Input_Word.text);
            }
        );

    }

    public override void SyncFilterParam()
    {
        string paramData = CollectionFilterManager.Instance.FilterParamDataList.GetFilterParamString(_filterType);
        _Input_Word.text = paramData;

    }

    public override void AddFilterParam(bool isAdded, string paramStr)
    {
        CollectionFilterManager.Instance.RemoveFilterParam(CollectionFilterTypes.Word, _prevWord);
        CollectionFilterManager.Instance.AddFilterParam(CollectionFilterTypes.Word, paramStr);
        _prevWord = paramStr;

        InvokeFilterNodeUpdate();
    }

    #endregion
}
