﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#region Global Enum
public enum CollectionFilterTypes
{
    None = 0
    , Soul
    , Faction
    , Rarity
    , Word
    , Owned
    , Craftable
    , Series
    , Foil
}

public enum OwnedFilterTypes
{
    All = 0
    , Owned = 1
}

public enum SeriesKeyFilterTypes
{
    BASIC = 0
    , WOE = 1
    , STAR = 2
    , GM = 3
    , FACTION = 4
}

public enum CraftableFilterTypes
{
    All = 0
    , Craftable
}

public enum FoilFilterTypes
{
    Foil
    , Standard
}
#endregion

public class CollectionFilterParamList : IEnumerable<KeyValuePair<CollectionFilterTypes, List<string>>>
{
    #region Private Properties
    private Dictionary<CollectionFilterTypes, List<string>> _paramDict = new Dictionary<CollectionFilterTypes, List<string>>();
    #endregion

    #region Contructors
    public CollectionFilterParamList()
    {
    }
    #endregion

    #region Methods
    public List<string> GetFilterParamData(CollectionFilterTypes type)
    {
        if(_paramDict.ContainsKey(type))
        {
            return _paramDict[type];
        }

        return new List<string>();
    }

    public string GetFilterParamString(CollectionFilterTypes type)
    {
        string result = string.Empty;
        if (_paramDict.ContainsKey(type))
        {
            foreach (string param in _paramDict[type])
            {
                result += param;
            }
        }

        return result;
    }

    public void AddFilterParam(CollectionFilterTypes type, string paramValue)
    {
        if(_paramDict.ContainsKey(type) == false)
        {
            _paramDict.Add(type, new List<string>());
        }

        if (_paramDict[type].Contains(paramValue) == false)
        {
            _paramDict[type].Add(paramValue);
        }
    }

    public void RemoveFilterParam(CollectionFilterTypes type, string paramValue)
    {
        if(_paramDict.ContainsKey(type) == false)
        {
            return;
        }

        if(_paramDict[type].Contains(paramValue))
        {
            _paramDict[type].Remove(paramValue);

            if(_paramDict[type].Count == 0)
            {
                _paramDict.Remove(type);
            }
        }
    }

    public void ClearFilterParam(CollectionFilterTypes type)
    {
        if(_paramDict.ContainsKey(type))
        {
            _paramDict[type] = new List<string>();
        }
    }

    public void ClearAllFilterParam()
    {
        _paramDict.Clear();
    }

    public IEnumerator<KeyValuePair<CollectionFilterTypes, List<string>>> GetEnumerator()
    {
        return _paramDict.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }
    #endregion
}