﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Karamucho.UI;
using TMPro;
using DG.Tweening;
using UnityEngine.UI.Extensions;

public class PopupDeckSummary : MonoBehaviour
{
    #region Inspector
    [Header("Arrow")]
    [SerializeField] private List<Image> IMG_Arrow;
    [SerializeField] private List<TextMeshProUGUI> TEXT_Dir;

    #endregion

    #region Private Properties
    private const float _minimumWidth = 0;
    private const float _maximumWidth = 15;
    private const float _minimumHeight = 0;
    private const float _maximumHeight = 75;

    private int _maxCard = 0;
    private Dictionary<CardDirectionType, int> _directionDict = new Dictionary<CardDirectionType, int>();
    #endregion




    #region Methods
    public void InitData(CardListData tempDeck)
    {

        CalculateDirection(tempDeck);
        UpdateArrow();

        //DebugDeckDirection();
    }

    private void CalculateDirection(CardListData cardList)
    {
        ClearList();

        foreach (KeyValuePair<string, int> item in cardList)
        {
            CardData data = CardData.CreateCard(item.Key);
            foreach (CardDirectionType dir in System.Enum.GetValues(typeof(CardDirectionType)))
            {
                if (data.CardDir.IsDirection(dir)) AddDirection(dir, item.Value);
            }
        }
    }

    private void UpdateArrow()
    {
        int maxCard = 0;
        foreach (CardDirectionType item in System.Enum.GetValues(typeof(CardDirectionType)))
        {
            // Update Number
            if (_directionDict[item] == 0)
            {
                TEXT_Dir[(int)item].text = "";
            }
            else
            {
                TEXT_Dir[(int)item].text = _directionDict[item].ToString();
            }

            if (_directionDict[item] > maxCard)
            {
                maxCard = _directionDict[item];
            }
           
        }

        foreach (CardDirectionType item in System.Enum.GetValues(typeof(CardDirectionType)))
        {
            // Update Arrow
            float value = (_directionDict[item] * _maximumHeight) / maxCard; // or maxCardPerDeck
            //value = (float)_directionDict[item] / (float)maxCard; // or maxCardPerDeck
            if (maxCard == 0) value = 0;

            //IMG_Arrow[(int)item].GetComponent<RectTransform>().sizeDelta = new Vector2(_maximumWidth, 0);
            //IMG_Arrow[(int)item].GetComponent<RectTransform>().sizeDelta = new Vector2(_maximumWidth, value);
            IMG_Arrow[(int)item].GetComponent<RectTransform>().DOSizeDelta(new Vector2(_maximumWidth, value), 0.75f).SetEase(Ease.OutCirc);

            //IMG_Arrow[(int)item].fillAmount = 0;
            //IMG_Arrow[(int)item].DOFillAmount(value, 0.75f).SetEase(Ease.OutCirc);

        }
    }

    private void DebugDeckDirection()
    {
        Debug.Log("COUNT:" + _directionDict.Count);
        foreach (KeyValuePair<CardDirectionType,int> item in _directionDict)
        {
            Debug.Log(string.Format("<b>Dir:{0} = {1}</b>", item.Key, item.Value));
        }
    }
    #endregion

    #region Operator
    private void ClearList()
    {
        _directionDict.Clear();

        foreach (CardDirectionType item in System.Enum.GetValues(typeof(CardDirectionType)))
        {
            AddDirection(item, 0);
        }

    }

    private void AddDirection(CardDirectionType dir, int value = 1)
    {
        if (_directionDict.ContainsKey(dir))
        {
            _directionDict[dir] += value;
        }
        else
        {
            _directionDict.Add(dir, value);
        }
    }
    #endregion
}
