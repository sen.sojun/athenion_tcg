﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct CardChangeTracker
{
    public bool SoulAndArrow;
    public bool CardName;
    public bool Faction;
    public bool Atk;
    public bool Hp;
    public bool Armor;
    public bool Skill;

    public CardChangeTracker(CardUIData cardDataOld, CardUIData cardDataCurrent)
    {
        SoulAndArrow = (cardDataOld.SpiritDefault != cardDataCurrent.SpiritDefault) || (cardDataOld.CardDir != cardDataCurrent.CardDir);
        CardName = cardDataOld.Name != cardDataCurrent.Name;
        Faction = cardDataOld.Element != cardDataCurrent.Element;
        Atk = cardDataOld.ATKDefault != cardDataCurrent.ATKDefault;
        Hp = cardDataOld.HPDefault != cardDataCurrent.HPDefault;
        Armor = cardDataOld.ArmorDefault != cardDataCurrent.ArmorDefault;
        Skill = cardDataOld.Description != cardDataCurrent.Description;
    }
}

public class CollectionCardChangeHilight : MonoBehaviour
{
    #region Inspector
    [SerializeField] private GameObject Hilight_Soul;
    [SerializeField] private GameObject Hilight_CardName;
    [SerializeField] private GameObject Hilight_Faction;
    [SerializeField] private GameObject Hilight_Atk;
    [SerializeField] private GameObject Hilight_Hp;
    [SerializeField] private GameObject Hilight_Armor;
    [SerializeField] private GameObject Hilight_Skill;
    #endregion

    public void SetHilightCardChange(CardChangeTracker tracker)
    {
        Hilight_Soul.SetActive(tracker.SoulAndArrow);
        Hilight_CardName.SetActive(tracker.CardName);
        Hilight_Faction.SetActive(tracker.Faction);
        Hilight_Atk.SetActive(tracker.Atk);
        Hilight_Hp.SetActive(tracker.Hp);
        Hilight_Armor.SetActive(tracker.Armor);
        Hilight_Skill.SetActive(tracker.Skill);
    }

    public void SetDescriptionBoxSize(float value)
    {
        Debug.Log("BoxSize: " + value);
        float margin = 10.0f;
        RectTransform rect = Hilight_Skill.GetComponent<RectTransform>();
        rect.sizeDelta = new Vector2(rect.sizeDelta.x, value + margin);
    }
}
