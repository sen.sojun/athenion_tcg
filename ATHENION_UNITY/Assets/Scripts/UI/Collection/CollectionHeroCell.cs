﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Karamucho.UI {
    public class CollectionHeroCell : MonoBehaviour, IPointerClickHandler
    {
        #region Public Properties
        public Image Glow;
        public Image ART_Hero;
        public Image ART_Hero_Gray;
        public bool IsAvailble { get { return _isAvailable; } }
        #endregion

        #region Private Peoperties
        private string _heroID = "";
        private UnityAction _onClickAction;
        private HeroData _data;
        private bool _isAvailable;
        #endregion

        #region Methods
        public void InitData(string heroID,bool isAvailable, UnityAction action)
        {
            _isAvailable = isAvailable;
            SetHeroID(heroID);
            SetOnClick(action);
            SetImage();
        }

        public string GetHeroID()
        {
            return _heroID;
        }

        public HeroData GetHeroData()
        {
            return _data;
        }

        public void ShowFlare()
        {
            Glow.gameObject.SetActive(true);
        }

        public void HideFlare()
        {
            Glow.gameObject.SetActive(false);
        }

        private void SetImage()
        {
            Sprite sprite = SpriteResourceHelper.LoadSprite(_data.ID, SpriteResourceHelper.SpriteType.Hero_Square);
            ART_Hero.sprite = sprite;
            ART_Hero_Gray.sprite = sprite;

            ART_Hero.gameObject.SetActive(_isAvailable);
            ART_Hero_Gray.gameObject.SetActive(!_isAvailable);
        }

        private void SetHeroID(string heroID)
        {
            _heroID = heroID;
            _data = HeroData.CreateHero(_heroID, PlayerIndex.One);
        }

        private void SetOnClick(UnityAction action)
        {
            _onClickAction = action;
        }
        #endregion

        #region Events
        public void OnPointerClick(PointerEventData eventData)
        {
            SoundManager.PlaySFX(SoundManager.SFX.Button_Click);

            if (_onClickAction != null)
            {
                _onClickAction.Invoke();
            }
        }
        #endregion
    }
}