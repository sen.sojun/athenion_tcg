﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Karamucho.Collection
{
    #region Global Enums
    public enum DropZoneName
    {
        None = 0
        , CollectionCard
        , CollectionDeckDetail
    }
    #endregion

    public class CollectionUIDropZone : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        #region Inspector Properties
        [Header("Name")]
        [SerializeField] private DropZoneName _zoneName;
        #endregion

        #region Public Properties
        public bool IsPointerEnter { get { return _isPointerEnter; } }
        #endregion

        #region Private Properties
        private bool _isPointerEnter = false;
        #endregion

        #region Events
        public event Action<DropZoneName> OnPointerEnterEvent;
        public event Action<DropZoneName> OnPointerExitEvent;
        #endregion

        public void OnPointerEnter(PointerEventData eventData)
        {
            OnPointerEnterEvent?.Invoke(_zoneName);
            _isPointerEnter = true;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            OnPointerExitEvent?.Invoke(_zoneName);
            _isPointerEnter = false;
        }
    }
}
