﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PopupWarning : MonoBehaviour {

    public TextMeshProUGUI MessageText;
    public Animator Animator;
    public GameObject bg;

    public void ShowWarning(string message)
    {
        MessageText.text = message;
        Animator.SetTrigger("Show");
    }
}
