﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;
using Karamucho;

public class CollectionCardBackCell : MonoBehaviour
{
    #region Public Properties
    public Image IMG_Art_Main;
    public Image IMG_Art_Disable;
    public Button BTN_Select;
    public TextMeshProUGUI TEXT_Name;

    public bool IsAvailable { get { return _isAvailable; } }
    #endregion

    #region Private Properties
    private string _id;
    private string _skinName;
    private UnityAction _onClick;
    private bool _isAvailable;
    #endregion

    public void InitData(string id,string name, bool isAvailable, UnityAction onClick)
    {
        _id = id;
        _skinName = name;
        _isAvailable = isAvailable;
        _onClick = onClick;

        SetName();
        SetImage();
        SetBTN();
    }

    #region Methods
    private void SetImage()
    {
        Sprite sprite = SpriteResourceHelper.LoadSprite(_id, SpriteResourceHelper.SpriteType.CardBack_IMG);

        IMG_Art_Main.sprite = sprite;
        IMG_Art_Disable.sprite = sprite;

        IMG_Art_Main.gameObject.SetActive(_isAvailable);
        IMG_Art_Disable.gameObject.SetActive(!_isAvailable);

    }

    private void SetBTN()
    {
        BTN_Select.onClick.RemoveAllListeners();
        BTN_Select.onClick.AddListener(delegate
        {
            if (_onClick != null)
            {
                _onClick.Invoke();
            }
        });
    }

    private void SetName()
    {
        TEXT_Name.text = _skinName;
    }
    #endregion
}
