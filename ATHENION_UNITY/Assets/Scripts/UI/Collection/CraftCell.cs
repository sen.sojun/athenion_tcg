﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CraftCellData
{
    #region Public Properties
    public int CardAmount { get { return _cardAmount; } }
    #endregion

    #region Private Properties
    private int _cardAmount = 0;
    private Dictionary<VirtualCurrency, int> _craftPrice = new Dictionary<VirtualCurrency, int>();
    #endregion

    #region Contructors
    #endregion

    #region Methods
    public void AddCardAmount(int value)
    {
        _cardAmount += value;
    }

    public void AddCraftPrice(VirtualCurrency vc, int amount)
    {
        if (!_craftPrice.ContainsKey(vc))
        {
            _craftPrice.Add(vc, 0);
        }
        _craftPrice[vc] += amount;
    }

    public int GetCraftPrice(VirtualCurrency vc)
    {
        if (_craftPrice.ContainsKey(vc))
        {
            return _craftPrice[vc];
        }

        return 0;
    }
    #endregion
}

public class CraftCell : MonoBehaviour
{
    #region Inspector
    [SerializeField] private TextMeshProUGUI TEXT_CardCount;
    [SerializeField] private TextMeshProUGUI TEXT_FR;
    [SerializeField] private TextMeshProUGUI TEXT_RF;
    [SerializeField] private TextMeshProUGUI TEXT_S1;
    #endregion

    #region Methods
    public void Setup(CraftCellData craftCellData)
    {
        TEXT_CardCount.text = craftCellData.CardAmount.ToString("N0");
        TEXT_FR.text = "<sprite name=S_FR> " + craftCellData.GetCraftPrice(VirtualCurrency.FR).ToString("N0");
        TEXT_RF.text = "<sprite name=S_RF> " + craftCellData.GetCraftPrice(VirtualCurrency.RF).ToString("N0");
        TEXT_S1.text = "<sprite name=S_S1> " + craftCellData.GetCraftPrice(VirtualCurrency.S1).ToString("N0");
    }
    #endregion
}