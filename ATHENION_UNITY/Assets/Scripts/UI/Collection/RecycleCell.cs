﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class RecycleCellData
{
    #region Public Properties
    public int CardAmount { get { return _cardAmount; } }
    #endregion

    #region Private Properties
    private int _cardAmount = 0;
    private Dictionary<VirtualCurrency, int> _recyclePrice = new Dictionary<VirtualCurrency, int>();
    #endregion

    #region Contructors
    #endregion

    #region Methods
    public void AddCardAmount(int value)
    {
        _cardAmount += value;
    }

    public void AddRecyclePrice(VirtualCurrency vc, int amount)
    {
        if (!_recyclePrice.ContainsKey(vc))
        {
            _recyclePrice.Add(vc, 0);
        }
        _recyclePrice[vc] += amount;
    }

    public int GetRecyclePrice(VirtualCurrency vc)
    {
        if (_recyclePrice.ContainsKey(vc))
        {
            return _recyclePrice[vc];
        }

        return 0;
    }
    #endregion
}

public class RecycleCell : MonoBehaviour
{
    #region Inspector
    [SerializeField] private TextMeshProUGUI TEXT_CardCount;
    [SerializeField] private TextMeshProUGUI TEXT_FR;
    [SerializeField] private TextMeshProUGUI TEXT_RF;
    [SerializeField] private TextMeshProUGUI TEXT_S1;
    #endregion

    #region Methods
    public void Setup(RecycleCellData recycleCellData)
    {
        TEXT_CardCount.text = recycleCellData.CardAmount.ToString("N0");
        TEXT_FR.text = "<sprite name=S_FR> " + recycleCellData.GetRecyclePrice(VirtualCurrency.FR).ToString("N0");
        TEXT_RF.text = "<sprite name=S_RF> " + recycleCellData.GetRecyclePrice(VirtualCurrency.RF).ToString("N0");
        TEXT_S1.text = "<sprite name=S_S1> " + recycleCellData.GetRecyclePrice(VirtualCurrency.S1).ToString("N0");
    }
    #endregion
}