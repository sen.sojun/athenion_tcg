﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using TMPro;
using System;

namespace Karamucho.Collection
{
    public class CollectionDeckCell : CollectionItem
    {
        #region Public Properties
        public DeckData Data { get { return _data; } }
        public Image Flag;
        public Image HeroImage;
        public TextMeshProUGUI DeckName;
        public Button BTN_Delete;
        public GameObject ERROR_ICON;

        [Header("Faction Sprite")]
        public Sprite SpriteFire;
        public Sprite SpriteWater;
        public Sprite SpriteAir;
        public Sprite SpriteEarth;
        public Sprite SpriteHoly;
        public Sprite SpriteDark;
        public Sprite SpriteNeutral;
        #endregion

        #region Private Properties
        private DeckData _data;
        private CardElementType _elementID;
        #endregion

        #region Contructors
        public CollectionDeckCell()
        {
        }

        public CollectionDeckCell(DeckData deckData)
        {
            SetupData(deckData);
        }
        #endregion

        #region Methods
        public void SetupData(DeckData deckData)
        {
            _data = deckData;
            SetDeckName();
            SetFaction();
            SetHeroImage(deckData.HeroID);

            RectTransform rect = GetComponent<RectTransform>();
            rect.anchorMin = Vector2.zero;
            rect.anchorMax = Vector2.zero;
        }

        private void SetHeroImage(string heroID)
        {
            Sprite sprite = SpriteResourceHelper.LoadSprite(heroID, SpriteResourceHelper.SpriteType.Hero_Square);
            HeroImage.sprite = sprite;
        }

        private void SetFaction()
        {
            HeroData hero = HeroData.CreateHero(_data.HeroID, PlayerIndex.One);
            //Debug.Log("Hero: " + _data.HeroID + " Faction: " + hero.ElementID);

            switch (hero.ElementType)
            {
                case CardElementType.FIRE:
                {
                    Flag.sprite = SpriteFire;
                }
                break;

                case CardElementType.WATER:
                {
                    Flag.sprite = SpriteWater;
                }
                break;

                case CardElementType.AIR:
                {
                    Flag.sprite = SpriteAir;
                }
                break;

                case CardElementType.EARTH:
                {
                    Flag.sprite = SpriteEarth;
                }
                break;

                case CardElementType.HOLY:
                {
                    Flag.sprite = SpriteHoly;
                }
                break;

                case CardElementType.DARK:
                {
                    Flag.sprite = SpriteDark;
                }
                break;

                case CardElementType.NEUTRAL:
                {
                    Flag.sprite = SpriteNeutral;
                }
                break;

                default:
                {
                    Flag.sprite = SpriteNeutral;
                }
                break;
            }
        }

        private void SetDeckName()
        {
            DeckName.text = _data.DeckName;
        }

        public void SetOnClickEvent(UnityAction onClick)
        {
            GetComponent<Button>().onClick.RemoveAllListeners();
            GetComponent<Button>().onClick.AddListener(onClick);
        }

        public void ShowDeckError(bool isShow)
        {
            ERROR_ICON.SetActive(isShow);
        }

        private void InitOnClickDelete()
        {
            BTN_Delete.onClick.RemoveAllListeners();
            BTN_Delete.onClick.AddListener(delegate
            {
                DeleteDeck();
            });
        }

        private void ShowDeleteBTN()
        {
            BTN_Delete.gameObject.SetActive(true);
            InitOnClickDelete();
        }

        public void HideDeleteBTN()
        {
            BTN_Delete.gameObject.SetActive(false);
        }

        private void DeleteDeck()
        {
            // Delete Deck
        }
        #endregion
    }
}