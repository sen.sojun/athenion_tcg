﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using Karamucho.UI;
using DG.Tweening;

public class CollectionCosmetic : MonoBehaviour
{
    #region Enums
    public enum TabType
    {
        CardBack
        , Heroes
        , Console
    }
    #endregion

    #region Public Properties
    [Header("Prefab")]
    public SkinSlotCell Prefab_SkinCell;

    [Header("Popup")]
    public CosmeticCardBackDetail Popup_CardBackDetail;
    public CosmeticHeroDetail Popup_HeroDetail;
    public CosmeticHUDDetail Popup_HUDDetail;

    [Header("View Group")]
    public GameObject View_CardBack;
    public GameObject View_Hero;
    public GameObject View_HUD;

    [Header("Ref")]
    public Transform Container_CardBack;
    public Transform Container_Hero;
    public Transform Container_HUD;

    [Header("BTN")]
    public Button BTN_Close;

    [Header("Navigation")]
    public GameObject Hilight_Image;
    public Button BTN_CardBack;
    public Button BTN_HeroSkin;
    public Button BTN_HUD;
    #endregion

    #region Private Properties
    private List<SkinSlotCell> _cellCardBackList = new List<SkinSlotCell>();
    private List<SkinSlotCell> _cellHeroSkinList = new List<SkinSlotCell>();
    private List<SkinSlotCell> _cellHUDSkinList = new List<SkinSlotCell>();

    private TabType _currentTab = TabType.CardBack;
    #endregion

    #region Method
    public void Show(UnityAction onComplete = null)
    {
        TopToolbar.AddSetting(true, true, delegate
        {
            HomeManager.Instance.ShowMainCanvas();
            Hide();
        });
        GameHelper.UITransition_FadeIn(this.gameObject, onComplete);
        InitData();
        InitBTN();
        InitNavigation();
    }

    public void Hide()
    {
        TopToolbar.RemoveLatestSetting();
        GameHelper.UITransition_FadeOut(this.gameObject);
    }

    public void InitData()
    {
        // Check Contained
        if (_cellCardBackList.Count > 0)
        {
            return;
        }
        if (_cellHeroSkinList.Count > 0)
        {
            return;
        }
        if (_cellHUDSkinList.Count > 0)
        {
            return;
        }

        // Skin Card Back
        List<CardBackDBData> cardBackDataList = new List<CardBackDBData>();
        CardBackDB.Instance.GetAllData(out cardBackDataList);
        List<string> cardBackIdList = DataManager.Instance.InventoryData.CardBackIDList;

        foreach (CardBackDBData item in cardBackDataList)
        {
            bool isAvailable = cardBackIdList.Contains(item.CardBackID);
            // Check Listing
            if (isAvailable || item.IsListing)
            {
                _cellCardBackList.Add(CreateCell(item.CardBackID, SkinSlotCell.SkinType.CardBack, isAvailable, Container_CardBack, delegate
                {
                    SelectCardBack(item, isAvailable);
                }));
            }
        }

        // Skin Skin Hero
        List<HeroDBData> heroDataList = new List<HeroDBData>();
        HeroDB.Instance.GetAllData(out heroDataList);
        List<string> heroIdList = DataManager.Instance.InventoryData.HeroIDList;

        foreach (HeroDBData item in heroDataList)
        {
            bool isAvailable = heroIdList.Contains(item.ID);
            // Check Listing
            if (isAvailable || item.IsListing)
            {
                _cellHeroSkinList.Add(CreateCell(item.ID, SkinSlotCell.SkinType.Hero, isAvailable, Container_Hero, delegate
                {
                    SelectHero(item, isAvailable);
                }));
            }
        }

        // Skin HUD
        List<DockDBData> hudDataList = new List<DockDBData>();
        DockDB.Instance.GetAllData(out hudDataList);
        List<string> hudIdList = DataManager.Instance.InventoryData.DockIDList;

        foreach (DockDBData item in hudDataList)
        {
            bool isAvailable = hudIdList.Contains(item.DockID);
            // Check Listing
            if (isAvailable || item.IsListing)
            {
                _cellHUDSkinList.Add(CreateCell(item.DockID, SkinSlotCell.SkinType.HUD, isAvailable, Container_HUD, delegate
                {
                    SelectHUD(item, isAvailable);
                }));
            }
        }
    }

    private void InitBTN()
    {
        BTN_Close.onClick.RemoveAllListeners();
        BTN_Close.onClick.AddListener(delegate
        {
            HomeManager.Instance.ShowMainCanvas();
            Hide();
        });
    }

    private void InitNavigation()
    {
        BTN_CardBack?.onClick.RemoveAllListeners();
        BTN_CardBack?.onClick.AddListener(()=>ChangePage(TabType.CardBack));

        BTN_HeroSkin?.onClick.RemoveAllListeners();
        BTN_HeroSkin?.onClick.AddListener(() => ChangePage(TabType.Heroes));

        BTN_HUD?.onClick.RemoveAllListeners();
        BTN_HUD?.onClick.AddListener(() => ChangePage(TabType.Console));

    }

    private void SelectCardBack(CardBackDBData data, bool isAvailable)
    {
        Popup_CardBackDetail.Show(data.CardBackID, data.CardBackName, isAvailable);
    }

    private void SelectHero(HeroDBData data, bool isAvailable)
    {
        HeroData hero = HeroData.CreateHero(data.ID, PlayerIndex.One);
        Popup_HeroDetail.Show(hero.ID, hero.GetName(), isAvailable);
    }

    private void SelectHUD(DockDBData data, bool isAvailable)
    {
        Popup_HUDDetail.Show(data.DockID, data.DockName, isAvailable);
    }

    #endregion

    #region Navigation
    private void ChangePage(TabType tab)
    {
        _currentTab = tab;

        View_CardBack.SetActive(_currentTab == TabType.CardBack);
        View_Hero.SetActive(_currentTab == TabType.Heroes);
        View_HUD.SetActive(_currentTab == TabType.Console);

        // Animation Naviagation
        float duration = 0.5f;
        Transform targetParent = null;
        switch (_currentTab)
        {
            case TabType.CardBack:
            {
                targetParent = BTN_CardBack.transform;
                break;
            }
            case TabType.Heroes:
            {
                targetParent = BTN_HeroSkin.transform;
                break;
            }
            case TabType.Console:
            {
                targetParent = BTN_HUD.transform;
                break;
            }
        }
        Hilight_Image.transform.SetParent(targetParent);
        Hilight_Image.transform.SetAsFirstSibling();
        Hilight_Image.transform.DOLocalMove(Vector3.zero, duration).SetEase(Ease.OutExpo);
    }

    #endregion

    #region Cell Generator
    private SkinSlotCell CreateCell(string id,SkinSlotCell.SkinType type, bool isAvailable,Transform parent, UnityAction onClick)
    {
        GameObject obj = Instantiate(Prefab_SkinCell.gameObject, parent);
        SkinSlotCell cell = obj.GetComponent<SkinSlotCell>();
        cell.InitData(id, type, isAvailable, onClick);
        return cell;
    }
    #endregion
}
