﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Karamucho.UI;
using System;

namespace Karamucho.Collection
{
    public class CollectionCardBar : CollectionItem
    {
        #region Inspector Properties
        [Header("Game Stat")]
        [SerializeField] private TextMeshProUGUI TEXT_CardName;
        [SerializeField] private TextMeshProUGUI TEXT_Soul;
        [SerializeField] private TextMeshProUGUI TEXT_Atk;
        [SerializeField] private TextMeshProUGUI TEXT_Hp;
        [SerializeField] private TextMeshProUGUI TEXT_Armor;

        [Header("Stat Icon")]
        [SerializeField] private Image ICON_Atk;
        [SerializeField] private Image ICON_Hp;
        [SerializeField] private Image ICON_Armor;

        [Header("Art")]
        [SerializeField] private Image FRAME_Card;
        [SerializeField] private GameObject GROUP_Artwork;
        [SerializeField] private Image GROUP_RainbowEffect;

        [Header("Card Count")]
        [SerializeField] private GameObject GROUP_CardCount;
        [SerializeField] private TextMeshProUGUI TEXT_CardCount;

        [Header("Effect")]
        [SerializeField] private GameObject Hilight;
        [SerializeField] private GameObject SpawnEffect;
        [SerializeField] private GameObject E_Owned;

        [Header("Arrow")]
        [SerializeField] private List<GameObject> ArrowDir = new List<GameObject>();
        #endregion

        #region Public Properties
        public CardUIData Data;
        #endregion

        #region Private Properties
        private string _id;
        private bool _isRelease = false;
        private bool _isDragOut = false;
        private Vector2 _startPosition;
        #endregion

        #region Events
        public event Action<CollectionCardBar> OnCardBarClick;
        public event Action<CollectionCardBar> OnCardBarDoubleClick;
        public event Action<CollectionCardBar> OnCardBarBeginDragEvent;
        public event Action<CollectionCardBar> OnCardBarDragEvent;
        public event Action<CollectionCardBar> OnCardBarBeginHold;
        public event Action<CollectionCardBar> OnCardBarEndHold;
        public event Action OnCardBarEndDragEvent;
        #endregion

        #region Methods
        protected new void Update()
        {
            base.Update();
        }

        public string GetID()
        {
            return _id;
        }

        public void SetID(string id)
        {
            _id = id;
        }

        public void SetupData(CardUIData data)
        {
            if (data != null)
            {
                Data = data;
            }
            _id = Data.FullID;

            SetSpirit(data.SpiritDefault);
            SetATK(data.ATKDefault);
            SetHP(data.HPDefault);
            SetArmor(data.ArmorCurrent, data);
            SetImage(data);
            SetFrame(data);
            SetCardName(Data.Name.ToString());
            ShowDirection(data.CardDir);
            SetFoil();
        }

        public void ShowDirection(CardDirection dirValue)
        {
            foreach (CardDirectionType item in System.Enum.GetValues(typeof(CardDirectionType)))
            {
                bool isShow = dirValue.IsDirection(item);
                ArrowDir[(int)item].SetActive(isShow);
            }
        }

        public void ShowEffect()
        {
            SpawnEffect.SetActive(false);
            SpawnEffect.SetActive(true);
        }

        public void SetFoil()
        {
            GROUP_RainbowEffect.gameObject.SetActive(Data.IsFoil);
        }

        public void SetSpirit(int value)
        {
            TEXT_Soul.text = value.ToString();
        }

        public void SetArmor(int value, CardUIData data)
        {
            if (data.ArmorDefault <= 0)
            {
                TEXT_Armor.gameObject.SetActive(false);
                ICON_Armor.gameObject.SetActive(false);
            }
            else
            {
                TEXT_Armor.gameObject.SetActive(true);
                ICON_Armor.gameObject.SetActive(true);
            }

            TEXT_Armor.text = value.ToString();
        }

        public void SetHP(int value)
        {
            TEXT_Hp.text = value.ToString();
        }

        public void SetATK(int value)
        {
            TEXT_Atk.text = value.ToString();
        }

        private void SetCardName(string message)
        {
            TEXT_CardName.text = message;
        }

        private void SetFrame(CardUIData data)
        {
            if (data.Type == CardType.Minion || data.Type == CardType.Minion_NotInDeck || data.Type == CardType.Spell)
            {
                Sprite tempSprite;
                CardResourceManager.LoadBannerFrame(data.Rarity, out tempSprite);
                FRAME_Card.sprite = tempSprite;
            }
        }

        public void SetOwned(bool isOwned)
        {
            E_Owned.SetActive(!isOwned);
            if (isOwned)
            {
                TEXT_CardCount.color = GameHelper.GetColor_Number_Normal();
            }
            else
            {
                TEXT_CardCount.color = GameHelper.GetColor_Number_Decrease();
            }
        }

        public void SetCardCount(int value)
        {
            if (value == 1)
            {
                GROUP_CardCount.SetActive(false);
                TEXT_CardCount.text = "";
            }
            else
            {
                GROUP_CardCount.SetActive(true);
                TEXT_CardCount.text = "x " + value.ToString();
            }

        }

        public void SetIsRelease(bool isRelease)
        {
            _isRelease = isRelease;
        }

        private void SetImage(CardUIData data)
        {
            // Clear previous image
            foreach (Transform child in GROUP_Artwork.transform)
            {
                Destroy(child.gameObject);
            }

            //StartCoroutine(SetupImage(data));
            LoadImage(data);
        }

        private IEnumerator SetupImage(CardUIData data)
        {
            LoadImage(data);

            yield return null;
        }

        private void LoadImage(CardUIData data)
        {
            CardResourceManager.CreateAsync(
                  CardResourceManager.ImageType.Banner
                , data.ImageKey
                , GROUP_Artwork.transform
                , false
                , delegate (GameObject obj)
                {
                    obj.transform.localPosition = Vector3.zero;
                    obj.transform.localRotation = Quaternion.identity;
                }
            );
        }
        #endregion

        #region Input Event
        public override void OnEventUnityPointerDown(PointerEventData eventData)
        {
            base.OnEventUnityPointerDown(eventData);
            Hilight.SetActive(true);
        }

        public override void OnEventUnityPointerUp(PointerEventData eventData)
        {
            base.OnEventUnityPointerUp(eventData);
            Hilight.SetActive(false);
        }

        public override void OnEventClick(PointerEventData eventData)
        {
            base.OnEventClick(eventData);
            OnCardBarClick?.Invoke(this);
        }

        public override void OnEventDoubleClick(PointerEventData eventData)
        {
            base.OnEventDoubleClick(eventData);
            OnCardBarDoubleClick?.Invoke(this);
        }

        public override void OnEventBeginDrag(PointerEventData eventData)
        {
            base.OnEventBeginDrag(eventData);
            _startPosition = eventData.position;
        }

        public override void OnEventDrag(PointerEventData eventData)
        {
            float angle = Vector2.Angle(Vector2.left, (eventData.position - _startPosition).normalized);
            //Debug.Log(angle);

            if (((angle >= 0 && angle <= 20) || (angle >= 160 && angle <= 180)) && (eventData.position - _startPosition).magnitude > 15)
            {
                if(_isDragOut == false)
                {
                    OnCardBarDragEvent?.Invoke(this);
                    _isDragOut = true;
                }
            }

            if (_isDragOut == false)
            {
                base.OnEventDrag(eventData);
            }

            if ((eventData.position - _startPosition).magnitude > 15)
            {
                _startPosition = eventData.position;
            }
        }

        public override void OnEventEndDrag(PointerEventData eventData)
        {
            base.OnEventEndDrag(eventData);

            Hilight.SetActive(false);

            if (_isDragOut)
            {
                _isDragOut = false;
                SetIsRelease(false);
                OnCardBarEndDragEvent?.Invoke();
                //CollectionManager.Instance.RemoveCardBarList(this);
            }
            //DownTimerProgess.fillAmount = 0.0f;
            //Debug.Log("Bar Up " + ID);
        }

        public override void OnEventBeginHold(PointerEventData eventData)
        {
            base.OnEventBeginHold(eventData);
            Hilight.SetActive(true);
            CollectionManager.Instance.SetHoldPosition(eventData.position);
            OnCardBarBeginHold?.Invoke(this);
        }

        public override void OnEventEndHold(PointerEventData eventData)
        {
            base.OnEventEndHold(eventData);
            Hilight.SetActive(false);
            CollectionManager.Instance.SetHoldPosition(Vector3.zero);
            OnCardBarEndHold?.Invoke(this);
        }
        #endregion
    }
}