﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AutoDeck
{
    #region Private Properties
    private static int _cardCount = 0;

    private static int _limitSoul1 = 20;
    private static int _limitSoul2_pre = 3;
    private static int _limitSoul2 = 10;
    private static int _limitSoul3 = 10;

    private static int _soul1 = 0;
    private static int _soul2 = 0;
    private static int _soul3 = 0;

    private static int _fill1 = 0;
    private static int _fill2 = 0;
    private static int _fill3 = 0;

    private static bool _isOkay1 = false;
    private static bool _isOkay2 = false;
    private static bool _isOkay3 = false;
    #endregion

    #region Public Methods
    public static void GetAutoDeckList(CardListData currentList, CardElementType element, CardListData inventoryStack, out CardListData resultList)
    {
        CardListData result = new CardListData();
        CalculateCard(currentList);
        result = CalculateList(currentList, element, inventoryStack);

        resultList = result;
    }
    #endregion

    #region Private Methods
    private static void CalculateCard(CardListData currentList)
    {
        _cardCount = 0;
        _soul1 = 0;
        _soul2 = 0;
        _soul3 = 0;

        CountCurrentList(currentList);
        CalculateFillCount();
    }

    private static void CountCurrentList(CardListData currentList)
    {
        // หา card limit
        foreach (KeyValuePair<string, int> item in currentList)
        {
            CardData cardData = CardData.CreateCard(item.Key);
            if (cardData.Spirit == 1)
            {
                _soul1 += item.Value;
            }
            else if (cardData.Spirit == 2)
            {
                _soul2 += item.Value;
            }
            else if (cardData.Spirit == 3)
            {
                _soul3 += item.Value;
            }
            _cardCount += item.Value;
        }
        Debug.Log(string.Format("<b>Count: S1:{0} S2:{1} S3:{2} SUM:{3}</b>", _soul1, _soul2, _soul3, _cardCount));
    }

    private static void CalculateFillCount()
    {
        _fill1 = 0;
        _fill2 = 0;
        _fill3 = 0;
        _isOkay1 = true;
        _isOkay2 = true;
        _isOkay3 = true;

        if (_soul1 < _limitSoul1) _isOkay1 = false;
        if (_soul2 < _limitSoul2) _isOkay2 = false;
        if (_soul3 < _limitSoul3) _isOkay3 = false;

        // Case: Normal 
        if (!_isOkay1)
        {
            _fill1 = _limitSoul1 - _soul1;
        }
        if (!_isOkay2)
        {
            _fill2 = _limitSoul2 - _soul2;
        }
        if (!_isOkay3)
        {
            _fill3 = _limitSoul3 - _soul3;
        }

        // Case: Additional
        int max = GameHelper.MaxCardPerDeck;
        int x = 0;
        int y = 0;
        if ((_isOkay1 && !_isOkay2 && !_isOkay3))
        {
            x = max - (_soul1 + _limitSoul2_pre);
            y = x;

            if (x < 0) y = 0;
            _fill3 = y;

            if (x >= _limitSoul3)
            {
                y = _limitSoul3;
                _fill2 = _limitSoul2_pre + (x - _limitSoul3);
            }
            else
            {
                if (x < 0)
                {
                    _fill2 = _limitSoul2_pre + x;
                }
                else
                {
                    _fill2 = _limitSoul2_pre;
                }
            }      
        }
        else if((!_isOkay1 && !_isOkay2 && _isOkay3))
        {
            x = max - (_soul3 + _limitSoul2_pre);
            y = x;

            if (x < 0) y = 0;
            _fill1 = y;

            if (x >= _limitSoul1)
            {
                y = _limitSoul1;
                _fill2 = _limitSoul2_pre + (x - _limitSoul1);
            }
            else
            {
                if (x < 0)
                {
                    _fill2 = _limitSoul2_pre + x;
                }
                else
                {
                    _fill2 = _limitSoul2_pre;
                }
            }
        }

        Debug.Log(string.Format("<b>NeedToFill: S1:{0} S2:{1} S3:{2} SUM:{3}</b>", _fill1, _fill2, _fill3, _fill1 + _fill2 + _fill3));
    }

    private static CardListData CalculateList(CardListData currentList, CardElementType element, CardListData inventoryStack)
    {
        CardListData result = new CardListData();

        if (!_isOkay1)
        {
            AddCard(inventoryStack, element, 1, ref _fill1, ref result);
            AddCard(inventoryStack, CardElementType.NEUTRAL, 1, ref _fill1, ref result);
        }
        if (!_isOkay2)
        {
            AddCard(inventoryStack, element, 2, ref _fill2, ref result);
            AddCard(inventoryStack, CardElementType.NEUTRAL, 2, ref _fill2, ref result);
        }
        if (!_isOkay3)
        {
            AddCard(inventoryStack, element, 3, ref _fill3, ref result);
            AddCard(inventoryStack, CardElementType.NEUTRAL, 3, ref _fill3, ref result);
        }

        return result;
    }

    private static void AddCard(CardListData inventoryStack, CardElementType element, int soul, ref int limit, ref CardListData resultList)
    {
        CardListData cardStack = new CardListData(inventoryStack);
        cardStack.ShuffleList();
        List<string> orderList = cardStack.GetOrderList();

        foreach (string id in orderList)
        {
            if (limit == 0) break;
            if (cardStack[id] == 0) continue;
            if (resultList.ContainKey(id) && resultList[id] == 3) continue;

            CardData cardData = CardData.CreateCard(id);

            if (cardData.CardElement.ToCardElementType() == element && cardData.Spirit == soul)
            {
                resultList.AddCard(id, 1);
                limit--;
            }
        }

    }
    #endregion
}
