﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventBattleProfileButton : MonoBehaviour
{
    #region Public Properties 
    public string ProfileID;
    #endregion

    #region Private Properties
    private UnityAction<string> _onClick;
    #endregion

    /*
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    */

    public void SetOnClickCallback(UnityAction<string> callback)
    {
        _onClick = callback;
    }

    public void OnClickButton()
    {
        _onClick?.Invoke(ProfileID);
    }
}
