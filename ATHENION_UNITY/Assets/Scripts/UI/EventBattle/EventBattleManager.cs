﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Karamucho.UI;
using System;

public class EventBattleManager : MonoSingleton<EventBattleManager>
{
    public static event Action OnInit;
    public static event Action OnQuit;

    #region Public Properties
    public static readonly string BattleEventDeckIDPrefix = "DECK_EVENT_";
    public static readonly string BattleEventDeckDetailImagePath = "Events/EventBattles/Images/EventBattle_DeckDetail_";
    public static readonly string BattleEventProfilePath = "Events/EventBattles/Prefabs/EventBattleProfile";

    public List<string> DeckIDList { get; private set; }
    public Dictionary<string, EventBattleDBData> ProfileList { get; private set; }
    public bool Is_UIDetailCardShow { get; private set; }

    [Header("Popup")]
    public DetailCard Popup_DetailCard;
    #endregion

    #region Private Properties
    #endregion

    #region Methods
    public void Init()
    {
        ProfileList = new Dictionary<string, EventBattleDBData>();

        List<EventBattleDBData> dataList;
        if (EventBattleDB.Instance.GetAllData(out dataList))
        {
            foreach (EventBattleDBData data in dataList)
            {
                ProfileList.Add(data.ProfileID, data);
            }
        }

        OnInit?.Invoke();
        ShowInfo();
    }

    public void ShowInfo()
    {
        SoundManager.PlayEventBGM(1.5f);

        EventBattleUIManager.Instance.ShowMain(
              OnClickMore
            , OnClickProfile
            , OnClickPlay
            , OnClickBack
        );
    }

    public void ExitEventBattle()
    {
        if (SceneManager.GetSceneByName(GameHelper.EventBattleSceneName).IsValid())
        {
            SoundManager.PlayBGM(SoundManager.BGM.Menu, 1.5f);
            PopupUIManager.Instance.Show_SoftLoad(true);
            GameHelper.UnLoadSceneAsync(GameHelper.EventBattleSceneName, () =>
            {
                PopupUIManager.Instance.Show_SoftLoad(false);
            });
        }
        
        OnQuit?.Invoke();
        EventBattleUIManager.Instance.DestroySelf();
        this.DestroySelf();
    }
    #endregion

    #region Event Methods
    private void OnClickMore()
    {
        string header = LocalizationManager.Instance.GetText("EVENT_BATTLE_RULE_HEADER");
        string detail = LocalizationManager.Instance.GetText("EVENT_BATTLE_RULE_DESCRIPTION");
        PopupUIManager.Instance.ShowPopup_DetailWindow(header, detail);
    }

    private void OnClickProfile(string profileID)
    {
        Debug.Log("Clicked profile " + profileID);

        if (ProfileList != null && ProfileList.ContainsKey(profileID))
        {
            DeckData deckData = DataManager.Instance.GetEventBattleDeck(profileID);

            EventBattleUIManager.Instance.ShowDeckDetail(
                  string.Format("{0}{1}", BattleEventDeckDetailImagePath, profileID)
                , deckData
                , OnClickCard
                , EventBattleUIManager.Instance.HideDeckDetail
            );
        }
    }

    private void OnClickPlay()
    {
        if (EventBattleDB.Instance.DoUsePlayerDeck)
        {
            if (DataManager.Instance.IsHasDeckAvailable())
            {
                HomeManager.Instance.ShowLobby(GameMode.Event, EventBattleDB.Instance.FilterPlayerElementList);
            }
            else
            {
                string title = LocalizationManager.Instance.GetText("ERROR_FIND_DECK_NOT_FOUND");
                string message = LocalizationManager.Instance.GetText("");
                PopupUIManager.Instance.ShowPopup_ConfirmOnlyWithWarning(title, message);
            }

        }
        else
        {
            HomeManager.Instance.StartEventBattleMatch();
        }
    }

    private void OnClickBack()
    {
        ExitEventBattle();
    }

    private void OnClickCard(string cardID)
    {
        Debug.Log("Clicked Card " + cardID);
        CardData cardData = CardData.CreateCard(cardID);
        CardUIData cardUIData = new CardUIData(-1, cardData, DataManager.Instance.DefaultCardBackIDList[0]);

        SoundManager.PlaySFX(SoundManager.SFX.Card_Click);
        ShowDetailCardUI(true);
        Popup_DetailCard.UpdateCard(cardUIData, 1);
    }

    public void ShowDetailCardUI(bool isShow)
    {
        if (isShow)
        {
            Is_UIDetailCardShow = true;
            Popup_DetailCard.Show();
        }
        else
        {
            Is_UIDetailCardShow = false;
            Popup_DetailCard.Hide();
        }
    }
    #endregion
}
