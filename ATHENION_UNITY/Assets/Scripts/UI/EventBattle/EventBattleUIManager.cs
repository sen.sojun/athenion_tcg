﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;
using Karamucho;

public class EventBattleUIManager : MonoSingleton<EventBattleUIManager>
{
    #region Public Properties
    [SerializeField]
    private EventBattleMainUI _infoUI;

    [SerializeField]
    private EventBattleDeckDetailUI _deckDetailUI;

    [SerializeField]
    private GameObject _container;

    private EventBattleProfileUI _profileUI = null;
    #endregion

    #region Methods     
    /*
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    */
    #endregion

    #region Main Methods
    public void ShowMain(
          UnityAction onClickMore
        , UnityAction<string> onClickProfile
        , UnityAction onClickPlay
        , UnityAction onClickBack
    )
    {
        _infoUI.ShowUI(onClickMore, onClickPlay, onClickBack);

        if (_profileUI == null)
        {
            GameObject prefab = ResourceManager.Load<GameObject>(EventBattleManager.BattleEventProfilePath);
            GameObject obj = Instantiate(prefab, _container.transform) as GameObject;

            _profileUI = obj.GetComponent<EventBattleProfileUI>();
        }

        if (_profileUI != null)
        {
            _profileUI.ShowUI(onClickProfile);
        }
    }

    public void HideMain()
    {
        _infoUI.HideUI();
    }
    #endregion

    #region Deck Detail Methods
    public void ShowDeckDetail(string imagePath, DeckData deckData, UnityAction<string> onClickCard, UnityAction onClickClose)
    {
        _deckDetailUI.ShowUI(imagePath, deckData, onClickCard, onClickClose);
    }

    public void HideDeckDetail()
    {
        _deckDetailUI.HideUI();
    }
    #endregion
}
