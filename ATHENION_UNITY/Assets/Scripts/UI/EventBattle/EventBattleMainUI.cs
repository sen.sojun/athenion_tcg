﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;
using Karamucho;

public class EventBattleMainUI : MonoBehaviour
{
    #region Private Properties
    [SerializeField]
    private Button _moreButton;

    [SerializeField]
    private Button _playButton;

    [SerializeField]
    private Button _backButton;

    private UnityAction _onClickMore;
    private UnityAction _onClickPlay;
    private UnityAction _onClickBack;
    #endregion

    #region Methods
    /*
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }
    */

    public void ShowUI(UnityAction onClickMore, UnityAction onClickPlay, UnityAction onClickBack)
    {
        _onClickMore = onClickMore;
        _onClickPlay = onClickPlay;
        _onClickBack = onClickBack;

        _moreButton.onClick.RemoveAllListeners();
        _moreButton.onClick.AddListener(OnClickMore);
        _moreButton.SendMessage("OnEnable", SendMessageOptions.DontRequireReceiver);

        _playButton.onClick.RemoveAllListeners();
        _playButton.onClick.AddListener(OnClickPlay);
        _playButton.SendMessage("OnEnable", SendMessageOptions.DontRequireReceiver);

        _backButton.onClick.RemoveAllListeners();
        _backButton.onClick.AddListener(OnClickBack);
        _backButton.SendMessage("OnEnable", SendMessageOptions.DontRequireReceiver);

        gameObject.SetActive(true);
    }

    public void HideUI()
    {
        gameObject.SetActive(false);
    }
    #endregion

    #region Event Methods
    private void OnClickMore()
    {
        if (_onClickMore != null)
        {
            _onClickMore.Invoke();
        }
    }

    private void OnClickPlay()
    {
        if (_onClickPlay != null)
        {
            _onClickPlay.Invoke();
        }
    }

    private void OnClickBack()
    {
        if (_onClickBack != null)
        {
            _onClickBack.Invoke();
        }
    }
    #endregion
}
