﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;
using Karamucho;

public class EventBattleProfileUI : MonoBehaviour
{
    #region Private Properties
    [SerializeField]
    private TextMeshProUGUI _winCountText;

    [SerializeField]
    private Image _rewardCheck;
    [SerializeField]
    private Image _posterImage;

    [SerializeField]
    private List<EventBattleProfileButton> _buttonList;
    #endregion

    /*
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }
    */

    #region Methods
    public void ShowUI(UnityAction<string> onClickProfile)
    {
        if (_buttonList != null && _buttonList.Count > 0)
        {
            foreach (EventBattleProfileButton button in _buttonList)
            {
                button.SetOnClickCallback(onClickProfile);
            }
        }
    }

    public void SetImage(string posterImagePath)
    {
        Sprite sprite = ResourceManager.Load<Sprite>(posterImagePath);
        _posterImage.sprite = sprite;
    }

    public void ShowRewardCheck(bool isShow)
    {
        _rewardCheck.gameObject.SetActive(isShow);
    }
    #endregion
}
