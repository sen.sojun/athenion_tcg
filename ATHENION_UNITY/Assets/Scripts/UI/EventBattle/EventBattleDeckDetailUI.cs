﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;
using Karamucho;
using Karamucho.Collection;

public class EventBattleDeckDetailUI : MonoBehaviour
{
    #region Private Properties
    [SerializeField]
    private TextMeshProUGUI _headerText;

    [SerializeField]
    private Image _image;

    [SerializeField]
    private ScrollRect _scrollRect;

    [SerializeField]
    private GameObject _content;

    [SerializeField]
    private Button _closeButton;

    private DeckData _deckData;
    private List<CardUI_Collection> _uiList;
    private UnityAction<string> _onClickCard;
    private UnityAction _onClickClose;
    #endregion

    #region Methods
    /*
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }
    */

    public void ShowUI(string profileImagePath, DeckData deckData, UnityAction<string> onClickCard, UnityAction onClickClose)
    {
        _image.gameObject.SetActive(false);
        _deckData = new DeckData(deckData);
        _onClickCard = onClickCard;
        _onClickClose = onClickClose;

        if (_deckData != null)
        {
            SetHeaderText(_deckData.DeckName);
        }

        _closeButton.onClick.RemoveAllListeners();
        _closeButton.onClick.AddListener(OnClickClose);
        _closeButton.SendMessage("OnEnable", SendMessageOptions.DontRequireReceiver);

        GameHelper.UITransition_FadeIn(this.gameObject);

        SetImage(profileImagePath);
        CreateCardList();
    }

    public void HideUI()
    {
        GameHelper.UITransition_FadeOut(this.gameObject);

        ClearCardList();
    }

    private void SetHeaderText(string deckName)
    {
        _headerText.text = string.Format(
              LocalizationManager.Instance.GetText("EVENT_BATTLE_DECK_DETAIL_HEADER")
            , LocalizationManager.Instance.GetText(deckName)
        );
    }

    private void SetImage(string imagePath)
    {
        StartCoroutine(OnLoadImage(imagePath));
    }

    private IEnumerator OnLoadImage(string imagePath)
    {
        _image.gameObject.SetActive(false);

        ResourceManager.LoadAsync<Sprite>(
              imagePath
            , delegate (Sprite sprite)
            {
                if (sprite != null)
                {
                    _image.sprite = sprite;
                    _image.gameObject.SetActive(true);
                }
            }
        );

        yield break;
    }

    private void CreateCardList()
    {
        StartCoroutine(OnCreateCardList());
    }

    private IEnumerator OnCreateCardList()
    {
        ClearCardList(); // Clear all previous card UI.

        PopupUIManager.Instance.Show_MidSoftLoad(true);

        yield return new WaitForEndOfFrame();

        _uiList = new List<CardUI_Collection>();
        if (_deckData != null)
        {
            GameObject prefab = ResourceManager.Load("Prefabs/Collections/_CardUI_Collection") as GameObject;
            if (prefab != null)
            {
                int index = 0;
                _uiList = new List<CardUI_Collection>();
                string cardID = "";
                int amount = 0;
                GameObject obj = null;
                CardUI_Collection ui = null;
                CardData cardData = null;
                CardUIData cardUIData = null;

                foreach (KeyValuePair<string, int> card in _deckData.Deck)
                {
                    cardID = card.Key;
                    amount = card.Value;

                    obj = Instantiate(prefab, _content.transform);
                    ui = obj.GetComponent<CardUI_Collection>();

                    //Debug.LogFormat("{0}:{1}", cardID, amount);

                    cardData = CardData.CreateCard(cardID);
                    cardUIData = new CardUIData(index, cardData, _deckData.CardBackID);
                    ui.SetData(cardUIData);
                    ui.SetCardAmount(amount);

                    ui.OnCardClick += OnClickCard;
                    ui.AddListenerOnUnityBeginDrag(_scrollRect.OnBeginDrag);
                    ui.AddListenerOnUnityDrag(_scrollRect.OnDrag);
                    ui.AddListenerOnUnityEndDrag(_scrollRect.OnEndDrag);

                    _uiList.Add(ui);
                    index++;

                    yield return new WaitForEndOfFrame();
                }
            }
        }
        PopupUIManager.Instance.Show_MidSoftLoad(false);

        yield break;
    }

    private void ClearCardList()
    {
        foreach (Transform child in _content.transform)
        {
            Destroy(child.gameObject);
        }

        if (_uiList != null && _uiList.Count > 0)
        {
            _uiList.Clear();
        }
    }
    #endregion

    #region Event Methods
    private void OnClickCard(CardUI_Collection ui)
    {
        if (_onClickCard != null)
        {
            _onClickCard.Invoke(ui.Data.FullID);
        }
    }

    private void OnClickClose()
    {
        if (_onClickClose != null)
        {
            _onClickClose.Invoke();
        }
    }
    #endregion
}
