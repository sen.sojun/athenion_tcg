﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class LoadingUI : MonoBehaviour
{
    public bool IsShowProgressText = false;
    public bool IsTweenAlpha = false;
    public Slider progress;
    public TextMeshProUGUI loadingText;
    public TextMeshProUGUI progressText;
    public CanvasGroup CanvasGroup;

    private Sequence _sq;

    public void ShowUI()
    {
        SetDefaultLoadingText();
        SetProgress(0.0f);

        if (CanvasGroup != null)
        {
            if (!gameObject.activeSelf)
            {
                gameObject.SetActive(true);

                if (IsTweenAlpha)
                {
                    GameHelper.UITransition_FadeIn(this.gameObject);
                }
                else
                {
                    CanvasGroup.alpha = 1.0f;
                }
            }
        }
        else
        {
            gameObject.SetActive(true);
        }

        if(progress != null)
        {
            progress.onValueChanged.RemoveAllListeners();
            progress.onValueChanged.AddListener(SetProgressText);
        }
    }

    public void HideUI()
    {
        if (gameObject.activeSelf)
        {
            if (IsTweenAlpha && CanvasGroup != null)
            {
                GameHelper.UITransition_FadeOut(this.gameObject);
            }
            else
            {
                gameObject.SetActive(false);
            }
        }
    }

    public void SetLoadingText(string text)
    {
        if (loadingText != null)
        {
            loadingText.text = text;
        }
    }

    public void SetDefaultLoadingText()
    {
        SetLoadingText(LocalizationManager.Instance.GetText("LOADING_TEXT"));
    }

    public void SetProgress(float ratio)
    {
        if (progress != null)
        {
            if (ratio <= progress.value)
            {
                progress.value = ratio;
            }
            else
            {
                _sq.Kill();
                _sq = DOTween.Sequence();
                _sq.Append(progress.DOValue(ratio, 0.5f).SetEase(Ease.OutExpo));
            }
        }
    }

    private void SetProgressText(float ratio)
    {
        if (progressText != null)
        {
            progressText.text = string.Format("{0:0.##}%", ratio * 100.0f);
            progressText.gameObject.SetActive(IsShowProgressText);
        }
    }
}
