﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class InputTextClearByBTN : MonoBehaviour
{
    public TMP_InputField TEXT_Input;
    public Button BTN_Clear;

    private void Start()
    {
        BTN_Clear.onClick.RemoveAllListeners();
        BTN_Clear.onClick.AddListener(ClearTextInput);
    }

    private void ClearTextInput()
    {
        TEXT_Input.text = "";
        TEXT_Input.onEndEdit.Invoke("");
    }
}
