﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using PlayFab;
using PlayFab.ClientModels;
using Newtonsoft.Json;
using Karamucho;

public class AssetBundleData
{
    #region Public Properties
    public string Key { get; private set; }
    public uint Version { get; private set; }
    public uint CRC { get; private set; }
    public Hash128 Hash { get; private set; }

    public AssetBundle Asset { get; private set; }
    #endregion

    #region Private Properties
    #endregion

    #region Constructors
    public AssetBundleData(string key, uint version, uint crc, AssetBundle assetBundle)
    {
        Key = key;
        Version = version;
        CRC = crc;
        Hash = new Hash128();

        Asset = assetBundle;
    }

    public AssetBundleData(string key, uint crc, AssetBundle assetBundle)
    {
        Key = key;
        Version = 0;
        CRC = crc;
        Hash = new Hash128();

        Asset = assetBundle;
    }

    public AssetBundleData(string key, Hash128 hash, AssetBundle assetBundle)
    {
        Key = key;
        Version = 0;
        CRC = 0;
        Hash = hash;

        Asset = assetBundle;
    }

    public AssetBundleData(string key, uint version, uint crc, Hash128 hash, AssetBundle assetBundle)
    {
        Key = key;
        Version = version;
        CRC = crc;
        Hash = hash;

        Asset = assetBundle;
    }

    public AssetBundleData(AssetBundleData assetBundleData)
        : this(assetBundleData.Key, assetBundleData.Version, assetBundleData.CRC, assetBundleData.Hash, assetBundleData.Asset)
    {
    }
    #endregion

    #region Methods
    public virtual string GetDebugText()
    {
        int index = 0;

        string result = string.Format(
                "Key: {" + index++ + "}"
              + "\nVersion: {" + index++ + "}"
              + "\nCRC: {" + index++ + "}"
              + "\nHash: {" + index++ + "}"
              + "\nAsset Bundle: {" + index++ + "}"
            , Key
            , Version
            , CRC
            , Hash.ToString()
            , Asset.name
        );

        return result;
    }
    #endregion
}

public class AssetBundlePath
{
    #region Public Properties
    public Dictionary<RuntimePlatform, Dictionary<string, Dictionary<string, uint>>> BundlePath { get; private set; }
    #endregion

    #region Private Properties
    #endregion

    #region Constructors
    public AssetBundlePath()
    {
        BundlePath = new Dictionary<RuntimePlatform, Dictionary<string, Dictionary<string, uint>>>();
    }

    public AssetBundlePath(Dictionary<RuntimePlatform, Dictionary<string, Dictionary<string, uint>>> bundlePath)
    {
        BundlePath = bundlePath;
    }

    public AssetBundlePath(AssetBundlePath assetBundleData) : this(assetBundleData.BundlePath)
    {
    }

    public bool GetFileVersion(string fileName, out uint version)
    {
        if (BundlePath != null)
        {
            foreach (KeyValuePair<RuntimePlatform, Dictionary<string, Dictionary<string, uint>>> platform in BundlePath)
            {
                foreach (KeyValuePair<string, Dictionary<string, uint>> bundleVersion in platform.Value)
                {
                    foreach (KeyValuePair<string, uint> file in bundleVersion.Value)
                    {
                        if (file.Key.ToLower() == fileName.ToLower())
                        {
                            version = file.Value;
                            return true;
                        }
                    }
                }
            }
        }

        version = 0;
        return false;
    }

    public string ToJson()
    {
        if (BundlePath != null)
        {
            string json = JsonConvert.SerializeObject(BundlePath);
            return json;
        }

        return "";
    }

    public static bool JsonToAssetBundlePath(string json, out AssetBundlePath assetBundlePath)
    {
        if (json != null && json.Length > 0)
        {
            Dictionary<RuntimePlatform, Dictionary<string, Dictionary<string, uint>>> bundlePath = null;
            bundlePath = JsonConvert.DeserializeObject<Dictionary<RuntimePlatform, Dictionary<string, Dictionary<string, uint>>>>(json);
            if (bundlePath != null)
            {
                assetBundlePath = new AssetBundlePath(bundlePath);
                return true;
            }
        }

        assetBundlePath = null;
        return false;
    }
    #endregion

    #region Methods
    public virtual string GetDebugText()
    {
        string result = "";

        foreach (KeyValuePair<RuntimePlatform, Dictionary<string, Dictionary<string, uint>>> platform in BundlePath)
        {
            if (result.Length > 0)
            {
                result += "\nPlatform: " + platform.Key.ToString();
            }
            else
            {
                result += "Platform: " + platform.Key.ToString();
            }

            foreach (KeyValuePair<string, Dictionary<string, uint>> version in platform.Value)
            {
                result += "\n\tVersion: " + version.Key.ToString();

                foreach (KeyValuePair<string, uint> file in version.Value)
                {
                    result += string.Format("\n\t\tFile: {0}:{1}", file.Key.ToString(), file.Value.ToString());
                }
            }
        }

        return result;
    }
    #endregion
}

public class AssetBundleManager
{
    #region Private Properties
    private static readonly string _assetBundleRootPath = "AssetBundles";
    private static readonly string _assetBundleCachedVersionKey = "ASSETBUNDLE_CACHED_VERSION";
    private static readonly string _assetBundleCachedCRCKey = "ASSETBUNDLE_CACHED_CRC";
    private static readonly string _assetBundleCachedHashKey = "ASSETBUNDLE_CACHED_HASH";
    private static readonly string _assetBundleATNVersionKey = "ASSETBUNDLE_ATN_VERSION";

    private static Dictionary<string, AssetBundleData> _assetBundleList = null;
    private static Dictionary<string, string> _pathAssetBundleDict = null;
    private static Dictionary<string, string> _pathAssetNameDict = null;

    private static int _requestDownloadSent = 0;
    private static int _requestDownloadComplete = 0;
    private static Dictionary<string, float> _requestProgress;

    private static UnityAction _onLoadingAssetComplete;
    private static UnityAction<string> _onLoadingAssetFail;
    private static UnityAction<float> _onLoadingAssetUpdate;

    private static Dictionary<string, string> _assetPath = null;

    private static Dictionary<string, AssetBundleManifestData> _assetBundleManifestList = null;
    private static AssetBundleObjectLoader _loader = null;
    public static AssetBundleObjectLoader Loader
    {
        get
        {
            if (_loader == null)
            {
                GameObject obj = new GameObject();
                _loader = obj.AddComponent<AssetBundleObjectLoader>();
                obj.name = "AssetBundleObjectLoader";
            }

            return _loader;
        }
    }
    #endregion

    #region Methods
    public static void StartLoadingAsset(UnityAction onComplete, UnityAction<string> onFail, UnityAction<float> onUpdateProgress, bool isClear = true)
    {
        Debug.Log("Start Loading Asset...");
        _onLoadingAssetComplete = onComplete;
        _onLoadingAssetFail = onFail;
        _onLoadingAssetUpdate = onUpdateProgress;

        if (isClear)
        {
            if (_assetBundleList != null && _assetBundleList.Count > 0)
            {
                Clear(); // Clear all previous loaded bundle.
            }
        }

        GetStarterAssetBundlePath(
            delegate () // onCompelte
            {
                if (_assetPath != null)
                {
                    // Compare cache ATN version 
                    if (GetCacheATNVersion() != GameHelper.ATNVersion)
                    {
                        Caching.ClearCache();
                    }

                    _requestDownloadSent = 0;
                    _requestDownloadComplete = 0;
                    _requestProgress = new Dictionary<string, float>();

                    foreach (KeyValuePair<string, string> fileKey in _assetPath)
                    {
                        string progressKey = fileKey.Value;
                        _requestProgress.Add(progressKey, 0.0f);
                        _requestDownloadSent++;

                        LoadingAsset(
                              fileKey.Value
                            , OnLoadAssetComplete
                            , OnLoadAssetFail
                            , delegate(string key, float progress)
                            {
                                //Debug.LogFormat("{0} : {1}", key, progress);
                                _requestProgress[key] = progress;
                                OnLoadAssetUpdate();
                            }
                        );
                    }
                }
                else
                {
                    if (onFail != null)
                    {
                        onFail.Invoke(LocalizationManager.Instance.GetText("ERROR_ASSET_BUNDLE_KEY_EMPTY"));
                    }
                }
            }
            , delegate (string error) // onFail
            {
                if (onFail != null)
                {
                    onFail.Invoke(error);
                }
            }
        );
    }

    public static void LoadingAsset(string filePath, UnityAction onComplete, UnityAction<string> onFail, UnityAction<string, float> onUpdateProgress)
    {
        Debug.LogFormat("Start Loading Asset <color=blue>{0}</color>", filePath);

        StartLoadAssetBundleManifest(
              filePath
            , delegate ()
            {
                string[] fileTexts = filePath.Split('/');
                string fileName = fileTexts[fileTexts.Length - 1];

                uint crc = _assetBundleManifestList[fileName].CRC;
                Hash128 hash = _assetBundleManifestList[fileName].AssetFileHash;

                StartLoadAssetBundleWithHash(
                      filePath
                    , hash
                    , onComplete
                    , onFail
                    , delegate(string key, float progress)
                    {
                        float ratio = (1.0f + progress) / 2.0f;
                        onUpdateProgress?.Invoke(filePath, ratio);
                    }
                );
            }
            , onFail
            , delegate (string key, float progress)
            {
                float ratio = progress / 2.0f;
                onUpdateProgress?.Invoke(filePath, ratio);
            }
        );

    }

    private static void OnLoadAssetComplete()
    {
        _requestDownloadComplete++;
        OnLoadAssetUpdate();

        if (_requestDownloadComplete == _requestDownloadSent)
        {
            SetCacheATNVersion(GameHelper.ATNVersion);

            if (_onLoadingAssetComplete != null)
            {
                _onLoadingAssetComplete.Invoke();
            }
        }
    }

    private static void OnLoadAssetFail(string error)
    {
        _onLoadingAssetComplete = null;

        if (_onLoadingAssetFail != null)
        {
            _onLoadingAssetFail.Invoke(error);
        }
    }

    private static void OnLoadAssetUpdate()
    {
        float ratio = 0.0f;
        if (_requestProgress != null && _requestProgress.Count > 0)
        {
            int downloadProcressCount = _requestProgress.Count;
            float progressValue = 0.0f;
            foreach (KeyValuePair<string, float> progress in _requestProgress)
            {
                progressValue += progress.Value;
            }
            ratio = progressValue / (float)downloadProcressCount;
        }

        if (_onLoadingAssetUpdate != null)
        {
            _onLoadingAssetUpdate.Invoke(ratio);
        }
    }

    private static void GetStarterAssetBundlePath(UnityAction onComplete, UnityAction<string> onFail)
    {
        #if UNITY_ANDROID
        RuntimePlatform platform = RuntimePlatform.Android;
        #elif UNITY_IOS
        RuntimePlatform platform = RuntimePlatform.IPhonePlayer;
        #else
        {
            onFail?.Invoke("Not support platform. " + Application.platform.ToString());
            return;
        }
        #endif

        DataManager.Instance.GetStarterAssetBundlePath(
              GameHelper.ATNVersion
            , platform
            , GameHelper.GetAssetBundleFolder()
            , delegate (Dictionary<string, string> assetBundlePath)
            {
                _assetPath = new Dictionary<string, string>(assetBundlePath);

                if (_assetPath != null && _assetPath.Count > 0)
                {
                    onComplete?.Invoke();
                }
                else
                {
                    onFail?.Invoke(LocalizationManager.Instance.GetText("ERROR_ASSET_BUNDLE_KEY_EMPTY"));
                }
            }
            , delegate (string error)
            {
                onFail?.Invoke(error);
            }
        );
    }

    private static void StartLoadAssetBundleManifest(string path, UnityAction onComplete, UnityAction<string> onFail, UnityAction<string, float> onUpdateProgress)
    {
        if (path != null && path.Length > 0)
        {
            string[] fileTexts = path.Split('/');
            string fileName = fileTexts[fileTexts.Length - 1];
            string filePath = string.Format("{0}/{1}.manifest", _assetBundleRootPath, path);

            PlayFabManager.Instance.GetContentDownloadUrl(
                  filePath
                , delegate (GetContentDownloadUrlResult result)
                {
                    Debug.LogFormat("Start Load Manifest {0}:{1}\n{2}", filePath, fileName, result.URL);
                    LoadAssetBundleManifest(
                          filePath
                        , fileName
                        , result.URL
                        , delegate (AssetBundleManifestData bundleManifest)
                        {
                            if (bundleManifest != null)
                            {
                                onComplete?.Invoke();
                            }
                            else
                            {
                                onFail.Invoke(LocalizationManager.Instance.GetText("ERROR_ASSET_BUNDLE_FAIL_MANIFEST_NULL"));
                            }
                        }
                        , onUpdateProgress
                    );
                }
                , delegate (PlayFabError error)
                {
                    string errorLog = string.Format(
                          "{0}\n{1}"
                        , LocalizationManager.Instance.GetText("ERROR_ASSET_BUNDLE_FAIL_GET_URL_PATH_MANIFEST")
                        , error.ErrorMessage
                    );

                    onFail.Invoke(errorLog);
                }
            );
        }
        else
        {
            if (onComplete != null)
            {
                onComplete.Invoke();
            }
        }
    }

    private static void StartLoadAssetBundleWithVersion(string path, uint version, UnityAction onComplete, UnityAction<string> onFail, UnityAction<string, float> onUpdateProgress)
    {
        if (path != null && path.Length > 0)
        {
            string[] fileTexts = path.Split('/');
            string fileName = fileTexts[fileTexts.Length - 1];
            uint fileVersion = version;
            string filePath = string.Format("{0}/{1}", _assetBundleRootPath, path);

            PlayFabManager.Instance.GetContentDownloadUrl(
                  filePath
                , delegate (GetContentDownloadUrlResult result)
                {
                    Debug.LogFormat("Start Load {0}:{1}\n{2}", filePath, fileVersion, result.URL);
                    LoadAssetBundleWithVersion(
                          filePath
                        , fileName
                        , result.URL
                        , fileVersion
                        , delegate (AssetBundle bundle)
                        {
                            if (bundle != null)
                            {
                                onComplete?.Invoke();
                            }
                            else
                            {
                                onFail.Invoke(LocalizationManager.Instance.GetText("ERROR_ASSET_BUNDLE_FAIL_LOAD_BUNDLE_NULL_VERSION"));
                            }
                        }
                        , onUpdateProgress
                    );
                }
                , delegate (PlayFabError error)
                {
                    string errorLog = string.Format(
                          "{0}\n{1}"
                        , LocalizationManager.Instance.GetText("ERROR_ASSET_BUNDLE_FAIL_GET_URL_PATH_VERSION")
                        , error.ErrorMessage
                    );

                    onFail.Invoke(errorLog);
                }
            );
        }
        else
        {
            if (onComplete != null)
            {
                onComplete.Invoke();
            }
        }
    }

    private static void StartLoadAssetBundleWithCRC(string path, uint crc, UnityAction onComplete, UnityAction<string> onFail, UnityAction<string, float> onUpdateProgress)
    {
        if (path != null && path.Length > 0)
        {
            string[] fileTexts = path.Split('/');
            string fileName = fileTexts[fileTexts.Length - 1];
            uint fileCRC = crc;
            string filePath = string.Format("{0}/{1}", _assetBundleRootPath, path);

            PlayFabManager.Instance.GetContentDownloadUrl(
                  filePath
                , delegate (GetContentDownloadUrlResult result)
                {
                    Debug.LogFormat("Start Load {0}:{1}\n{2}", filePath, fileCRC, result.URL);
                    LoadAssetBundleWithCRC(
                          filePath
                        , fileName
                        , result.URL
                        , fileCRC
                        , delegate (AssetBundle bundle)
                        {
                            if (bundle != null)
                            {
                                onComplete?.Invoke();
                            }
                            else
                            {
                                onFail.Invoke(LocalizationManager.Instance.GetText("ERROR_ASSET_BUNDLE_FAIL_LOAD_BUNDLE_NULL_CRC"));
                            }
                        }
                        , onUpdateProgress
                    );
                }
                , delegate (PlayFabError error)
                {
                    string errorLog = string.Format(
                            "{0}\n{1}"
                        , LocalizationManager.Instance.GetText("ERROR_ASSET_BUNDLE_FAIL_GET_URL_PATH_CRC")
                        , error.ErrorMessage
                    );

                    onFail.Invoke(errorLog);
                }
            );
        }
        else
        {
            if (onComplete != null)
            {
                onComplete.Invoke();
            }
        }
    }

    private static void StartLoadAssetBundleWithHash(string path, Hash128 hash, UnityAction onComplete, UnityAction<string> onFail, UnityAction<string, float> onUpdateProgress)
    {
        if (path != null && path.Length > 0)
        {
            string[] fileTexts = path.Split('/');
            string fileName = fileTexts[fileTexts.Length - 1];
            Hash128 fileHash = hash;
            string filePath = string.Format("{0}/{1}", _assetBundleRootPath, path);

            PlayFabManager.Instance.GetContentDownloadUrl(
                  filePath
                , delegate (GetContentDownloadUrlResult result)
                {
                    Debug.LogFormat("Start Load {0}:{1}\n{2}", filePath, hash, result.URL);
                    LoadAssetBundleWithHash(
                          filePath
                        , fileName
                        , result.URL
                        , hash
                        , delegate (AssetBundle bundle)
                        {
                            if (bundle != null)
                            {
                                onComplete?.Invoke();
                            }
                            else
                            {
                                onFail.Invoke(LocalizationManager.Instance.GetText("ERROR_ASSET_BUNDLE_FAIL_LOAD_BUNDLE_NULL_HASH"));
                            }
                        }
                        , onUpdateProgress
                    );
                }
                , delegate (PlayFabError error)
                {
                    string errorLog = string.Format(
                          "{0}\n{1}"
                        , LocalizationManager.Instance.GetText("ERROR_ASSET_BUNDLE_FAIL_GET_URL_PATH_HASH")
                        , error.ErrorMessage
                    );

                    onFail.Invoke(errorLog);
                }
            );
        }
        else
        {
            if (onComplete != null)
            {
                onComplete.Invoke();
            }
        }
    }

    // Load Asset

    public static void LoadAssetBundleManifest(string key, string fileName, string url, UnityAction<AssetBundleManifestData> onLoadComplete, UnityAction<string, float> onUpdateProgress = null)
    {
        GameObject obj = new GameObject();
        AssetBundleLoader loader = obj.AddComponent<AssetBundleLoader>();
        loader.StartLoadAssetManifest(
              key
            , fileName
            , url
            , delegate (string loadedKey, string loadedName, string loadedURL, AssetBundleManifestData loadedBundleManifest)
            {
                OnLoadAssetBundleManifestCompleted(loadedKey, loadedName, loadedURL, loadedBundleManifest, onLoadComplete);
            }
            , onUpdateProgress
        );
    }

    public static void LoadAssetBundleWithVersion(string key, string fileName, string url, uint version, UnityAction<AssetBundle> onLoadComplete, UnityAction<string, float> onUpdateProgress = null)
    {
        GameObject obj = new GameObject();
        AssetBundleLoader loader = obj.AddComponent<AssetBundleLoader>();
        loader.StartLoadAssetWithVersion(
              key
            , fileName
            , url
            , version
            , delegate (string loadedKey, string loadedURL, uint loadedVersion, AssetBundle loadedBundle)
            {
                OnLoadAssetBundleCompletedWithVersion(loadedKey, loadedURL, loadedVersion, loadedBundle, onLoadComplete);
            }
            , onUpdateProgress
        );
    }

    public static void LoadAssetBundleWithCRC(string key, string fileName, string url, uint crc, UnityAction<AssetBundle> onLoadComplete, UnityAction<string, float> onUpdateProgress = null)
    {
        GameObject obj = new GameObject();
        AssetBundleLoader loader = obj.AddComponent<AssetBundleLoader>();
        loader.StartLoadAssetWithCRC(
              key
            , fileName
            , url
            , crc
            , delegate (string loadedKey, string loadedURL, uint loadedCRC, AssetBundle loadedBundle)
            {
                OnLoadAssetBundleCompletedWithCRC(loadedKey, loadedURL, loadedCRC, loadedBundle, onLoadComplete);
            }
            , onUpdateProgress
        );
    }

    public static void LoadAssetBundleWithHash(string key, string fileName, string url, Hash128 hash, UnityAction<AssetBundle> onLoadComplete, UnityAction<string, float> onUpdateProgress = null)
    {
        GameObject obj = new GameObject();
        AssetBundleLoader loader = obj.AddComponent<AssetBundleLoader>();
        loader.StartLoadAssetHash(
              key
            , fileName
            , url
            , hash
            , delegate (string loadedKey, string loadedURL, Hash128 loadedHash, AssetBundle loadedBundle)
            {
                OnLoadAssetBundleCompletedWithHash(loadedKey, loadedURL, loadedHash, loadedBundle, onLoadComplete);
            }
            , onUpdateProgress
        );
    }

    public static void LoadAssetBundleFromFileWithCRC(string key, string fileName, string path, uint crc, UnityAction<AssetBundle> onLoadComplete, UnityAction<string, float> onUpdateProgress = null)
    {
        GameObject obj = new GameObject();
        AssetBundleLoader loader = obj.AddComponent<AssetBundleLoader>();
        loader.StartLoadAssetFromFileWithCRC(
              key
            , fileName
            , path
            , crc
            , delegate (string loadedKey, string loadedPath, uint loadedCRC, AssetBundle loadedBundle)
            {
                OnLoadAssetBundleCompletedWithCRC(loadedKey, loadedPath, loadedCRC, loadedBundle, onLoadComplete);
            }
            , onUpdateProgress
        );
    }

    // On Load Complete

    private static void OnLoadAssetBundleManifestCompleted(string key, string name, string path, AssetBundleManifestData assetBundleManifest, UnityAction<AssetBundleManifestData> onLoadComplete)
    {
        if (assetBundleManifest != null)
        {
            if (_assetBundleManifestList == null)
            {
                _assetBundleManifestList = new Dictionary<string, AssetBundleManifestData>();
            }

            if (_assetBundleManifestList.ContainsKey(name))
            {
                _assetBundleManifestList[name] = assetBundleManifest; // Replace value.
            }
            else
            {
                _assetBundleManifestList.Add(name, assetBundleManifest); // Add to dict.
            }

            Debug.LogFormat("<color=green>Loaded</color> {0}:{1} {2}", key, name, path);

            if (onLoadComplete != null)
            {
                onLoadComplete.Invoke(assetBundleManifest);
            }
        }
        else
        {
            Debug.LogFormat("<color=red>Load Failed</color> {0}:{1} {2}", key, name, path);

            if (onLoadComplete != null)
            {
                onLoadComplete.Invoke(null);
            }
        }
    }

    private static void OnLoadAssetBundleCompletedWithVersion(string key, string path, uint version, AssetBundle assetBundle, UnityAction<AssetBundle> onLoadComplete)
    {
        if (assetBundle != null)
        {
            AssetBundleData data = new AssetBundleData(key, version, 0, assetBundle);
            CacheAssetData(data);

            Debug.LogFormat("<color=green>Loaded</color> {0}:{1} {2}", key, version, data.Asset.name);

            if (onLoadComplete != null)
            {
                onLoadComplete.Invoke(assetBundle);
            }
        }
        else
        {
            Debug.LogFormat("<color=red>Load Failed</color> {0}:{1} {2}", key, version, path);

            if (onLoadComplete != null)
            {
                onLoadComplete.Invoke(null);
            }
        }
    }

    private static void OnLoadAssetBundleCompletedWithCRC(string key, string path, uint crc, AssetBundle assetBundle, UnityAction<AssetBundle> onLoadComplete)
    {
        if (assetBundle != null)
        {
            AssetBundleData data = new AssetBundleData(key, crc, assetBundle);
            CacheAssetData(data);

            Debug.LogFormat("<color=green>Loaded</color> {0}:{1} {2}", key, crc, data.Asset.name);

            if (onLoadComplete != null)
            {
                onLoadComplete.Invoke(assetBundle);
            }
        }
        else
        {
            Debug.LogFormat("<color=red>Load Failed</color> {0}:{1} {2}", key, crc, path);

            if (onLoadComplete != null)
            {
                onLoadComplete.Invoke(null);
            }
        }
    }

    private static void OnLoadAssetBundleCompletedWithHash(string key, string path, Hash128 hash, AssetBundle assetBundle, UnityAction<AssetBundle> onLoadComplete)
    {
        if (assetBundle != null)
        {
            AssetBundleData data = new AssetBundleData(key, hash, assetBundle);
            CacheAssetData(data);

            Debug.LogFormat("<color=green>Loaded</color> {0}:{1} {2}", key, hash.ToString(), data.Asset.name);
        }
        else
        {
            Debug.LogFormat("<color=red>Load Failed</color> {0}:{1} {2}", key, hash.ToString(), path);
        }

        if (onLoadComplete != null)
        {
            onLoadComplete.Invoke(assetBundle);
        }
    }

    private static void CacheAssetData(AssetBundleData data)
    {
        if (_assetBundleList == null)
        {
            _assetBundleList = new Dictionary<string, AssetBundleData>();
        }

        if (_assetBundleList.ContainsKey(data.Key))
        {
            _assetBundleList[data.Key] = data; // Replace value.
        }
        else
        {
            _assetBundleList.Add(data.Key, data); // Add to dict.
        }

        /*
        GameObject obj = new GameObject();
        AssetBundleCacher cacher = obj.AddComponent<AssetBundleCacher>();

        cacher.StartCache(assetBundle, onLoadComplete);
        */

        // เริ่มสร้าง Hash ข้อมูล
        if (_pathAssetNameDict == null)
        {
            _pathAssetNameDict = new Dictionary<string, string>();
        }
        if (_pathAssetBundleDict == null)
        {
            _pathAssetBundleDict = new Dictionary<string, string>();
        }

        foreach (string assetName in data.Asset.GetAllAssetNames())
        {
            string keyPath = ConvertAssetPath(assetName);
            string realPath = ConvertAssetRealPath(assetName);

            if (!_pathAssetNameDict.ContainsKey(keyPath) && !_pathAssetBundleDict.ContainsKey(keyPath))
            {
                // _pathAssetNameDict เอาไว้สำหรับให้หา assetName จาก path ได้
                _pathAssetNameDict.Add(keyPath, assetName);

                // _pathAssetBundleDict เอาไว้สำหรับให้หา assetBundle key จาก path ได้
                _pathAssetBundleDict.Add(keyPath, data.Key);
            }
            else if (!_pathAssetNameDict.ContainsKey(realPath) && !_pathAssetBundleDict.ContainsKey(realPath))
            {
                // _pathAssetNameDict เอาไว้สำหรับให้หา assetName จาก path ได้
                _pathAssetNameDict.Add(realPath, assetName);

                // _pathAssetBundleDict เอาไว้สำหรับให้หา assetBundle key จาก path ได้
                _pathAssetBundleDict.Add(realPath, data.Key);
            }
            else
            {
                if (_pathAssetNameDict.ContainsKey(keyPath))
                {
                    Debug.LogWarningFormat("AssetBundleManager/CacheAssetData: <color=red>Duplicate key</color> {0} : {1} with {2}", assetName, keyPath, _pathAssetNameDict[keyPath]);
                }

                if (_pathAssetBundleDict.ContainsKey(keyPath))
                {
                    Debug.LogWarningFormat("AssetBundleManager/CacheAssetData: <color=red>Duplicate key</color> {0} : {1} with {2}", assetName, keyPath, _pathAssetBundleDict[keyPath]);
                }
            }
        }
    }

    public static string ConvertAssetPath(string path)
    {
        string pathLower = path.ToLower();

        // remove file extension.
        string[] fileNames = pathLower.ToLower().Split('.');
        string keyPath = fileNames[0];

        // remove prefix assetbundles path.
        {
            string assetbundlePath = "assets/assetbundles/";
            if (keyPath.Contains(assetbundlePath))
            {
                int index = keyPath.IndexOf(assetbundlePath);
                if (index >= 0)
                {
                    keyPath = keyPath.Substring(index + assetbundlePath.Length);
                }
            }
        }

        return keyPath;
    }

    public static string ConvertAssetRealPath(string path)
    {
        string keyPath = path.ToLower();

        // remove prefix assetbundles path.
        {
            string assetbundlePath = "assets/assetbundles/";
            if (keyPath.Contains(assetbundlePath))
            {
                int index = keyPath.IndexOf(assetbundlePath);
                if (index >= 0)
                {
                    keyPath = keyPath.Substring(index + assetbundlePath.Length);
                }
            }
        }

        return keyPath;
    }

    public static bool GetAssetBundleData(string key, out AssetBundleData assetBundleData)
    {
        if (_assetBundleList != null && _assetBundleList.ContainsKey(key))
        {
            assetBundleData = _assetBundleList[key];
            return true;
        }

        assetBundleData = null;
        return false;
    }

    public static void Clear()
    {
        if (_assetBundleList != null)
        {
            foreach (KeyValuePair<string, AssetBundleData> item in _assetBundleList)
            {
                item.Value.Asset.Unload(false);
            }

            _assetBundleList.Clear();
        }

        if (_pathAssetBundleDict != null)
        {
            _pathAssetBundleDict.Clear();
        }

        if (_pathAssetNameDict != null)
        {
            _pathAssetNameDict.Clear();
        }
    }

    public static void SaveFile(byte[] data, string path, UnityAction onComplete)
    {
        //Create the Directory if it does not exist
        if (!Directory.Exists(Path.GetDirectoryName(path)))
        {
            Directory.CreateDirectory(Path.GetDirectoryName(path));

            if (onComplete != null)
            {
                onComplete.Invoke();
            }
        }

        try
        {
            File.WriteAllBytes(path, data);
            Debug.Log("Saved Data to: " + path.Replace("/", "\\"));
        }
        catch (Exception e)
        {
            Debug.LogWarning("Failed To Save Data to: " + path.Replace("/", "\\"));
            Debug.LogWarning("Error: " + e.Message);

            if (onComplete != null)
            {
                onComplete.Invoke();
            }
        }
    }

    public static void SetCachedVersion(string key, uint version)
    {
        string saveKey = string.Format("{0}_{1}", _assetBundleCachedVersionKey, key.ToUpper());
        PlayerPrefs.SetString(saveKey, version.ToString());
    }

    public static bool GetCachedVersion(string key, out uint version)
    {
        string saveKey = string.Format("{0}_{1}", _assetBundleCachedVersionKey, key.ToUpper());

        if (PlayerPrefs.HasKey(saveKey))
        {
            string versionStr = PlayerPrefs.GetString(saveKey);
            version = uint.Parse(versionStr);
            return true;
        }

        version = 0;
        return false;
    }

    public static void SetCachedCRC(string key, uint crc)
    {
        string saveKey = string.Format("{0}_{1}", _assetBundleCachedCRCKey, key.ToUpper());
        PlayerPrefs.SetString(saveKey, crc.ToString());
    }

    public static bool GetCachedCRC(string key, out uint crc)
    {
        string saveKey = string.Format("{0}_{1}", _assetBundleCachedCRCKey, key.ToUpper());

        if (PlayerPrefs.HasKey(saveKey))
        {
            string crcStr = PlayerPrefs.GetString(saveKey);
            crc = uint.Parse(crcStr);
            return true;
        }

        crc = 0;
        return false;
    }

    public static void SetCachedHash(string key, Hash128 hash)
    {
        string saveKey = string.Format("{0}_{1}", _assetBundleCachedHashKey, key.ToUpper());
        PlayerPrefs.SetString(saveKey, hash.ToString());
    }

    public static bool GetCachedHash(string key, out Hash128 hash)
    {
        string saveKey = string.Format("{0}_{1}", _assetBundleCachedHashKey, key.ToUpper());

        if (PlayerPrefs.HasKey(saveKey))
        {
            string hashStr = PlayerPrefs.GetString(saveKey);
            hash = Hash128.Parse(hashStr);
            return true;
        }

        hash = new Hash128();
        return false;
    }

    public static void SetCacheATNVersion(ATNVersion atnVersion)
    {
        PlayerPrefs.SetString(_assetBundleATNVersionKey, atnVersion.ToString());
    }

    public static ATNVersion GetCacheATNVersion()
    {
        string str = PlayerPrefs.GetString(_assetBundleATNVersionKey);

        ATNVersion atnVersion;
        if (GameHelper.StrToEnum<ATNVersion>(str, out atnVersion))
        {
            return atnVersion;
        }

        return GameHelper.ATNVersion;
    }
    #endregion

    #region Object Manage Methods
    public static bool IsContainObject(string path)
    {
        if (_assetBundleList != null && _pathAssetBundleDict != null && _pathAssetNameDict != null)
        {
            string keyPath = ConvertAssetPath(path);

            if (_pathAssetBundleDict.ContainsKey(keyPath) && _pathAssetNameDict.ContainsKey(keyPath))
            {
                return true;
            }
        }

        return false;
    }

    public static UnityEngine.Object LoadObject(string path)
    {
        string pathLower = path.ToLower();

        if (IsContainObject(pathLower))
        {
            string assetName = _pathAssetNameDict[pathLower];
            AssetBundle assetBundle = _assetBundleList[_pathAssetBundleDict[pathLower]].Asset;

            UnityEngine.Object asset = assetBundle.LoadAsset(assetName);

            if (asset != null)
            {
                return asset;
            }
        }

        return null;
    }

    public static T LoadObject<T>(string path) where T : UnityEngine.Object
    {
        string pathLower = path.ToLower();

        if (IsContainObject(pathLower))
        {
            string assetName = _pathAssetNameDict[pathLower];
            AssetBundle assetBundle = _assetBundleList[_pathAssetBundleDict[pathLower]].Asset;

            UnityEngine.Object asset = assetBundle.LoadAsset<T>(assetName);

            if (asset != null)
            {
                return (asset as T);
            }
        }

        return null;
    }

    public static UnityEngine.Object[] LoadObjectAll(string path)
    {
        string pathLower = path.ToLower();

        if (IsContainObject(pathLower))
        {
            string assetName = _pathAssetNameDict[pathLower];
            AssetBundle assetBundle = _assetBundleList[_pathAssetBundleDict[pathLower]].Asset;

            UnityEngine.Object[] asset = assetBundle.LoadAssetWithSubAssets(assetName);

            if (asset != null)
            {
                return asset;
            }
        }

        return null;
    }

    public static T[] LoadObjectAll<T>(string path) where T : UnityEngine.Object
    {
        string pathLower = path.ToLower();

        if (IsContainObject(pathLower))
        {
            string assetName = _pathAssetNameDict[pathLower];
            AssetBundle assetBundle = _assetBundleList[_pathAssetBundleDict[pathLower]].Asset;

            T[] asset = assetBundle.LoadAssetWithSubAssets<T>(assetName);

            if (asset != null)
            {
                return asset;
            }
        }

        return null;
    }

    public static void LoadObjectAsync(string path, UnityAction<UnityEngine.Object> onComplete, UnityAction onFail)
    {
        string pathLower = path.ToLower();

        if (IsContainObject(pathLower))
        {
            string assetName = _pathAssetNameDict[pathLower];
            AssetBundle assetBundle = _assetBundleList[_pathAssetBundleDict[pathLower]].Asset;
            Loader.StartLoadObject(assetName, assetBundle, onComplete, onFail);

            return;
        }

        if (onFail != null)
        {
            onFail.Invoke();
        }
    }

    public static void LoadObjectAsync<T>(string path, UnityAction<T> onComplete, UnityAction onFail) where T : UnityEngine.Object
    {
        string pathLower = path.ToLower();

        if (IsContainObject(pathLower))
        {
            string assetName = _pathAssetNameDict[pathLower];
            AssetBundle assetBundle = _assetBundleList[_pathAssetBundleDict[pathLower]].Asset;
            Loader.StartLoadObject<T>(assetName, assetBundle, onComplete, onFail);

            return;
        }

        if (onFail != null)
        {
            onFail.Invoke();
        }
    }
    #endregion
}
