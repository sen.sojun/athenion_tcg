﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

using Karamucho;

public class AssetBundleObjectLoader : MonoBehaviour
{
    #region Methods
    public void StartLoadObject(string assetName, AssetBundle assetBundle, UnityAction<UnityEngine.Object> onComplete, UnityAction onFail)
    {
        DontDestroyOnLoad(gameObject);


        StartCoroutine(OnLoadObject(assetName, assetBundle, onComplete, onFail));
    }

    public void StartLoadObject<T>(string assetName, AssetBundle assetBundle, UnityAction<T> onComplete, UnityAction onFail) where T : UnityEngine.Object
    {
        DontDestroyOnLoad(gameObject);

        StartCoroutine(OnLoadObject<T>(assetName, assetBundle, onComplete, onFail));
    }

    private IEnumerator OnLoadObject(string assetName, AssetBundle assetBundle, UnityAction<UnityEngine.Object> onComplete, UnityAction onFail)
    {
        AssetBundleRequest request = assetBundle.LoadAssetAsync(assetName);

        yield return request; // Loading...

        UnityEngine.Object asset = request.asset;

        if (asset != null)
        {
            string keyPath = AssetBundleManager.ConvertAssetPath(assetName);
            ResourceManager.AddObject(keyPath, asset);

            Debug.LogFormat("Load Asset Completed {0} : {1}", assetName, asset.GetType());

            if (onComplete != null)
            {
                onComplete.Invoke(asset);
            }
        }
        else
        {
            Debug.LogErrorFormat("Load Asset Failed {0} : {1}", assetName);

            if (onFail != null)
            {
                onFail.Invoke();
            }
        }
    }

    private IEnumerator OnLoadObject<T>(string assetName, AssetBundle assetBundle, UnityAction<T> onComplete, UnityAction onFail) where T : UnityEngine.Object
    {
        AssetBundleRequest request = assetBundle.LoadAssetAsync<T>(assetName);

        yield return request; // Loading...

        UnityEngine.Object asset = request.asset;

        if (asset != null)
        {
            string keyPath = AssetBundleManager.ConvertAssetPath(assetName);
            ResourceManager.AddObject(keyPath, asset);

            //Debug.LogFormat("Load Asset Completed {0} : {1}", _assetName, asset.GetType());

            if (onComplete != null)
            {
                onComplete.Invoke(asset as T);
            }
        }
        else
        {
            Debug.LogErrorFormat("Load Asset Failed {0} : {1}", assetName);

            if (onFail != null)
            {
                onFail.Invoke();
            }
        }
    }
    #endregion
}
