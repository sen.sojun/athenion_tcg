﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

public class AssetBundleLoader : MonoBehaviour
{
    #region Public Properties
    public static float LoadingRetryTime = 1.0f;
    public static int RetryCount = 3;
    #endregion

    #region Private Properties
    private int _retryCount = 0;
    private string _fileKey;
    private string _fileName;
    private string _path;

    private uint _version;
    private uint _crc;
    private Hash128 _hash;

    private UnityAction<string, string, string, AssetBundleManifestData> _onLoadManifestComplete;
    private UnityAction<string, string, uint, AssetBundle> _onLoadWithVersionComplete;
    private UnityAction<string, string, uint, AssetBundle> _onLoadWithCRCComplete;
    private UnityAction<string, string, Hash128, AssetBundle> _onLoadWithHashComplete;
    private UnityAction<string, float> _onProgressUpdate;
    #endregion

    #region Methods
    public void StartLoadAssetManifest(
          string fileKey, string fileName, string url
        , UnityAction<string, string, string, AssetBundleManifestData> onLoadComplete
        , UnityAction<string, float> onProgressUpdate = null
    )
    {
        _fileKey = fileKey;
        _fileName = fileName;
        _path = url;

        _retryCount = 0;
        _version = 0;
        _crc = 0;
        _hash = new Hash128();

        _onLoadManifestComplete = onLoadComplete;
        _onLoadWithVersionComplete = null;
        _onLoadWithCRCComplete = null;
        _onLoadWithHashComplete = null;
        _onProgressUpdate = onProgressUpdate;

        gameObject.name = string.Format("AssetBundleLoader({0})", _path);
        DontDestroyOnLoad(gameObject);

        StartCoroutine(OnLoadingAssetBundleManifest());
    }

    private IEnumerator OnLoadingAssetBundleManifest()
    {
        Debug.LogFormat("Start loading... {0}:{1}\n{2}", _fileKey, _fileName, _path);

        UnityWebRequest request = UnityWebRequest.Get(_path);

        if (_onProgressUpdate != null)
        {
            _onProgressUpdate.Invoke(_fileKey, 0.0f); // start
        }
        //yield return request.SendWebRequest(); // Loading...

        request.SendWebRequest(); // Loading...
        while (!request.downloadHandler.isDone)
        {
            if (_onProgressUpdate != null)
            {
                _onProgressUpdate.Invoke(_fileKey, request.downloadProgress);
            }

            if (request.isNetworkError || request.isHttpError)
            {
                break;
            }

            yield return new WaitForEndOfFrame();
        }

        if (_onProgressUpdate != null)
        {
            if (request.isNetworkError || request.isHttpError)
            {
                _onProgressUpdate.Invoke(_fileKey, 0.0f); // error
            }
            else
            {
                _onProgressUpdate.Invoke(_fileKey, 1.0f); // finish
            }
        }

        bool isLoadComplete = false;
        AssetBundleManifestData data = null;

        try
        {
            // Show log message.
            if (request.isDone)
            {
                data = AssetBundleManifestData.CreateAssetBundleManifestData(_fileName, request.downloadHandler.text);

                if (data != null)
                {
                    isLoadComplete = true;
                    //Debug.LogFormat("<color=green>Load Completed</color> {0}:{1}\n{2}", _fileKey, _hash, _path);
                }
                else
                {
                    Debug.LogErrorFormat("<color=red>Load Failed</color> menifest is null. {0}:{1}\n{2}", _fileKey, _hash, _path);
                }
            }
            else
            {
                if (request.isHttpError)
                {
                    Debug.LogErrorFormat("<color=red>Load Failed</color> Http error {3}. {0}:{1}\n{2}", _fileKey, _hash, _path, request.error);
                }
                if (request.isNetworkError)
                {
                    Debug.LogErrorFormat("<color=red>Load Failed</color> Network error {3}. {0}:{1}\n{2}", _fileKey, _hash, _path, request.error);
                }
            }
        }
        catch
        {
            isLoadComplete = false;
        }

        if (isLoadComplete)
        {
            if (_onLoadManifestComplete != null)
            {
                _onLoadManifestComplete.Invoke(_fileKey, _fileName, _path, data);
            }

            Destroy(gameObject);
        }
        else
        {
            Debug.LogErrorFormat("<color=red>Load Failed</color> OnLoadingAssetBundleManifest({3}) {0}:{1}\n{2}", _fileKey, _hash, _path, _retryCount);

            yield return new WaitForSeconds(LoadingRetryTime);

            _retryCount++;
            if (_retryCount >= RetryCount)
            {
                if (_onLoadManifestComplete != null)
                {
                    _onLoadManifestComplete.Invoke(_fileKey, _fileName, _path, null);
                }

                Destroy(gameObject);
            }
            else
            {
                StartCoroutine(OnLoadingAssetBundleManifest()); // Retry
            }
        }
    }

    public void StartLoadAssetWithVersion(
         string fileKey, string fileName, string url, uint version
       , UnityAction<string, string, uint, AssetBundle> onLoadComplete
       , UnityAction<string, float> onProgressUpdate = null
   )
    {
        _fileKey = fileKey;
        _fileName = fileName;
        _path = url;

        _retryCount = 0;
        _version = version;
        _crc = 0;
        _hash = new Hash128();

        _onLoadManifestComplete = null;
        _onLoadWithVersionComplete = onLoadComplete;
        _onLoadWithCRCComplete = null;
        _onLoadWithHashComplete = null;
        _onProgressUpdate = onProgressUpdate;

        gameObject.name = string.Format("AssetBundleLoader({0})", _path);
        DontDestroyOnLoad(gameObject);

        StartCoroutine(OnLoadingAssetWithVersion());
    }

    private IEnumerator OnLoadingAssetWithVersion()
    {
        //Debug.LogFormat("Start loading... {0}:{1}\n{2}", _fileKey, _version, _path);

        UnityWebRequest request = UnityWebRequestAssetBundle.GetAssetBundle(_path, _version, 0);

        if (_onProgressUpdate != null)
        {
            _onProgressUpdate.Invoke(_fileKey, 0.0f); // start
        }
        //yield return request.SendWebRequest(); // Loading...

        request.SendWebRequest(); // Loading...
        while (!request.downloadHandler.isDone)
        {
            if (_onProgressUpdate != null)
            {
                _onProgressUpdate.Invoke(_fileKey, request.downloadProgress);
            }

            if (request.isNetworkError || request.isHttpError)
            {
                break;
            }

            yield return new WaitForEndOfFrame();
        }

        if (_onProgressUpdate != null)
        {
            if (request.isNetworkError || request.isHttpError)
            {
                _onProgressUpdate.Invoke(_fileKey, 0.0f); // error
            }
            else
            {
                _onProgressUpdate.Invoke(_fileKey, 1.0f); // finish
            }
        }

        bool isLoadComplete = false;
        AssetBundle bundle = null;

        try
        {
            // Show log message.
            if (request.isDone)
            {
                bundle = DownloadHandlerAssetBundle.GetContent(request);

                if (bundle != null)
                {
                    isLoadComplete = true;
                    AssetBundleManager.SetCachedVersion(_fileKey, _version);
                    //Debug.LogFormat("<color=green>Load Completed</color> {0}:{1}\n{2}", _fileKey, _version, _path);
                }
                else
                {
                    Debug.LogErrorFormat("<color=red>Load Failed</color> bundle is null. {0}:{1}\n{2}", _fileKey, _version, _path);
                }
            }
            else
            {
                if (request.isHttpError)
                {
                    Debug.LogErrorFormat("<color=red>Load Failed</color> Http error {3}. {0}:{1}\n{2}", _fileKey, _version, _path, request.error);
                }
                if (request.isNetworkError)
                {
                    Debug.LogErrorFormat("<color=red>Load Failed</color> Network error {3}. {0}:{1}\n{2}", _fileKey, _version, _path, request.error);
                }
            }
        }
        catch
        {
            isLoadComplete = false;
        }

        if (isLoadComplete)
        {
            if (_onLoadWithVersionComplete != null)
            {
                _onLoadWithVersionComplete.Invoke(_fileKey, _path, _version, bundle);
            }

            Destroy(gameObject);
        }
        else
        {
            Debug.LogErrorFormat("<color=red>Load Failed</color> OnLoadingAssetWithVersion {0}:{1}\n{2}", _fileKey, _hash, _path);

            yield return new WaitForSeconds(LoadingRetryTime);

            _retryCount++;
            if (_retryCount >= RetryCount)
            {
                if (_onLoadWithVersionComplete != null)
                {
                    _onLoadWithVersionComplete.Invoke(_fileKey, _path, _version, null);
                }

                Destroy(gameObject);
            }
            else
            {
                StartCoroutine(OnLoadingAssetWithVersion()); // Retry
            }
        }
    }

    public void StartLoadAssetWithCRC(
          string fileKey, string fileName, string url, uint crc
        , UnityAction<string, string, uint, AssetBundle> onLoadComplete
        , UnityAction<string, float> onProgressUpdate = null
    )
    {
        _fileKey = fileKey;
        _fileName = fileName;
        _path = url;

        _retryCount = 0;
        _version = 0;
        _crc = crc;
        _hash = new Hash128();

        _onLoadManifestComplete = null;
        _onLoadWithVersionComplete = null;
        _onLoadWithCRCComplete = onLoadComplete;
        _onLoadWithHashComplete = null;
        _onProgressUpdate = onProgressUpdate;

        gameObject.name = string.Format("AssetBundleLoader({0})", _path);
        DontDestroyOnLoad(gameObject);

        StartCoroutine(OnLoadingAssetWithCRC());
    }

    private IEnumerator OnLoadingAssetWithCRC()
    {
        //Debug.LogFormat("Start loading... {0}:{1}\n{2}", _fileKey, _crc, _path);

        UnityWebRequest request = UnityWebRequestAssetBundle.GetAssetBundle(_path, _crc);

        if (_onProgressUpdate != null)
        {
            _onProgressUpdate.Invoke(_fileKey, 0.0f); // start
        }
        //yield return request.SendWebRequest(); // Loading...

        request.SendWebRequest(); // Loading...
        while (!request.downloadHandler.isDone)
        {
            if (_onProgressUpdate != null)
            {
                _onProgressUpdate.Invoke(_fileKey, request.downloadProgress);
            }

            if (request.isNetworkError || request.isHttpError)
            {
                break;
            }

            yield return new WaitForEndOfFrame();
        }

        if (_onProgressUpdate != null)
        {
            if (request.isNetworkError || request.isHttpError)
            {
                _onProgressUpdate.Invoke(_fileKey, 0.0f); // error
            }
            else
            {
                _onProgressUpdate.Invoke(_fileKey, 1.0f); // finish
            }
        }

        bool isLoadComplete = false;
        AssetBundle bundle = null;

        try
        {
            // Show log message.
            if (request.isDone)
            {
                bundle = DownloadHandlerAssetBundle.GetContent(request);

                if (bundle != null)
                {
                    isLoadComplete = true;
                    AssetBundleManager.SetCachedCRC(_fileKey, _crc);
                    //Debug.LogFormat("<color=green>Load Completed</color> {0}:{1}\n{2}", _fileKey, _crc, _path);
                }
                else
                {
                    Debug.LogErrorFormat("<color=red>Load Failed</color> bundle is null. {0}:{1}\n{2}", _fileKey, _crc, _path);
                }
            }
            else
            {
                if (request.isHttpError)
                {
                    Debug.LogErrorFormat("<color=red>Load Failed</color> Http error {3}. {0}:{1}\n{2}", _fileKey, _crc, _path, request.error);
                }
                if (request.isNetworkError)
                {
                    Debug.LogErrorFormat("<color=red>Load Failed</color> Network error {3}. {0}:{1}\n{2}", _fileKey, _crc, _path, request.error);
                }
            }
        }
        catch
        {
            isLoadComplete = false;
        }

        if (isLoadComplete)
        {
            if (_onLoadWithCRCComplete != null)
            {
                _onLoadWithCRCComplete.Invoke(_fileKey, _path, _crc, bundle);
            }

            Destroy(gameObject);
        }
        else
        {
            Debug.LogErrorFormat("<color=red>Load Failed</color> OnLoadingAssetWithCRC {0}:{1}\n{2}", _fileKey, _hash, _path);

            yield return new WaitForSeconds(LoadingRetryTime);

            _retryCount++;
            if (_retryCount >= RetryCount)
            {
                if (_onLoadWithCRCComplete != null)
                {
                    _onLoadWithCRCComplete.Invoke(_fileKey, _path, _crc, null);
                }

                Destroy(gameObject);
            }
            else
            {
                StartCoroutine(OnLoadingAssetWithCRC());
            }
        }
    }

    public void StartLoadAssetHash(
          string fileKey, string fileName, string url, Hash128 hash
        , UnityAction<string, string, Hash128, AssetBundle> onLoadComplete
        , UnityAction<string, float> onProgressUpdate = null
    )
    {
        _fileKey = fileKey;
        _fileName = fileName;
        _path = url;

        _retryCount = 0;
        _version = 0;
        _crc = 0;
        _hash = hash;

        _onLoadManifestComplete = null;
        _onLoadWithVersionComplete = null;
        _onLoadWithCRCComplete = null;
        _onLoadWithHashComplete = onLoadComplete;
        _onProgressUpdate = onProgressUpdate;

        gameObject.name = string.Format("AssetBundleLoader({0})", _path);
        DontDestroyOnLoad(gameObject);

        StartCoroutine(OnLoadingAssetWithHash());
    }

    private IEnumerator OnLoadingAssetWithHash()
    {
        //Debug.LogFormat("Start loading... {0}:{1}\n{2}", _fileKey, _hash, _path);

        UnityWebRequest request = UnityWebRequestAssetBundle.GetAssetBundle(_path, _hash, 0);

        if (_onProgressUpdate != null)
        {
            _onProgressUpdate.Invoke(_fileKey, 0.0f); // start
        }

        //yield return request.SendWebRequest(); // Loading...

        request.SendWebRequest(); // Loading...
        while (!request.downloadHandler.isDone)
        {
            if (_onProgressUpdate != null)
            {
                _onProgressUpdate.Invoke(_fileKey, request.downloadProgress);
            }

            if (request.isNetworkError || request.isHttpError)
            {
                break;
            }

            yield return new WaitForEndOfFrame();
        }

        if (_onProgressUpdate != null)
        {
            if (request.isNetworkError || request.isHttpError)
            {
                _onProgressUpdate.Invoke(_fileKey, 0.0f); // error
            }
            else
            {
                _onProgressUpdate.Invoke(_fileKey, 1.0f); // finish
            }
        }

        bool isLoadComplete = false;
        AssetBundle bundle = null;

        try
        {
            // Show log message.
            if (request.isDone)
            {
                bundle = DownloadHandlerAssetBundle.GetContent(request);

                if (bundle != null)
                {
                    AssetBundleManager.SetCachedHash(_fileKey, _hash);
                    isLoadComplete = true;

                    //Debug.LogFormat("<color=green>Load Completed</color> {0}:{1}\n{2}", _fileKey, _hash, _path);
                }
                else
                {
                    Debug.LogErrorFormat("<color=red>Load Failed</color> bundle is null. {0}:{1}\n{2}", _fileKey, _hash, _path);
                }
            }
            else
            {
                if (request.isHttpError)
                {
                    Debug.LogErrorFormat("<color=red>Load Failed</color> Http error {3}. {0}:{1}\n{2}", _fileKey, _hash, _path, request.error);
                }
                if (request.isNetworkError)
                {
                    Debug.LogErrorFormat("<color=red>Load Failed</color> Network error {3}. {0}:{1}\n{2}", _fileKey, _hash, _path, request.error);
                }
            }
        }
        catch 
        {
            isLoadComplete = false;
        }

        if (isLoadComplete)
        {
            if (_onLoadWithHashComplete != null)
            {
                _onLoadWithHashComplete.Invoke(_fileKey, _path, _hash, bundle);
            }

            Destroy(gameObject);
        }
        else
        {
            Debug.LogErrorFormat("<color=red>Load Failed</color> OnLoadingAssetWithHash {0}:{1}\n{2}", _fileKey, _hash, _path);

            yield return new WaitForSeconds(LoadingRetryTime);

            _retryCount++;
            if (_retryCount >= RetryCount)
            {
                if (_onLoadWithHashComplete != null)
                {
                    _onLoadWithHashComplete.Invoke(_fileKey, _path, _hash, null);
                }

                Destroy(gameObject);
            }
            else
            {
                StartCoroutine(OnLoadingAssetWithHash());
            }
        }
    }

    public void StartLoadAssetFromFileWithCRC(
          string fileKey, string fileName, string path, uint crc
        , UnityAction<string, string, uint, AssetBundle> onLoadComplete
        , UnityAction<string, float> onProgressUpdate = null
    )
    {
        _fileKey = fileKey;
        _fileName = fileName;
        _path = path;

        _retryCount = 0;
        _version = 0;
        _crc = crc;
        _hash = new Hash128();

        _onLoadManifestComplete = null;
        _onLoadWithVersionComplete = null;
        _onLoadWithCRCComplete = onLoadComplete;
        _onLoadWithHashComplete = null;
        _onProgressUpdate = onProgressUpdate;

        gameObject.name = string.Format("AssetBundleLoader({0})", _path);
        DontDestroyOnLoad(gameObject);

        StartCoroutine(OnLoadingAssetFromFileWithCRC());
    }

    private IEnumerator OnLoadingAssetFromFileWithCRC()
    {
        //Debug.LogFormat("Start loading from file... {0}:{1}\n{2}", _fileKey, _crc, _path);

        AssetBundleCreateRequest request = AssetBundle.LoadFromFileAsync(_path, _crc);

        if (_onProgressUpdate != null)
        {
            _onProgressUpdate.Invoke(_fileKey, 0.0f); // start
        }

        //yield return request; // Loading...

        while (!request.isDone)
        {
            if (_onProgressUpdate != null)
            {
                _onProgressUpdate.Invoke(_fileKey, request.progress);
            }

            yield return new WaitForEndOfFrame();
        }

        if (_onProgressUpdate != null)
        {
            _onProgressUpdate.Invoke(_fileKey, 1.0f); // finish
        }

        AssetBundle bundle = request.assetBundle;

        // Show log message.
        if (bundle != null)
        {
            AssetBundleManager.SetCachedCRC(_fileKey, _crc);
            //Debug.LogFormat("<color=green>Load Completed</color> {0}:{1}\n{2}", _fileKey, _crc, _path);
        }
        else
        {
            Debug.LogErrorFormat("<color=red>Load Failed</color> bundle is null. {0}:{1}\n{2}", _fileKey, _crc, _path);
        }

        if (_onLoadWithCRCComplete != null)
        {
            _onLoadWithCRCComplete.Invoke(_fileKey, _path, _crc, bundle);
        }

        Destroy(gameObject);
    }
    #endregion
}

public class AssetBundleManifestData
{
    #region Properties
    public string FileName { get; private set; }
    public uint CRC { get; private set; }
    public Hash128 AssetFileHash { get; private set; }
    public Hash128 TypeTreeHash { get; private set; }
    public List<string> Assets { get; private set; }
    #endregion

    #region Constructors
    public AssetBundleManifestData()
    {
        FileName = "";
        CRC = 0;
        AssetFileHash = new Hash128();
        TypeTreeHash = new Hash128();
        Assets = new List<string>();
    }

    public AssetBundleManifestData(string fileName, uint crc, Hash128 assetFileHash, Hash128 typeTreeHash, List<string> assets)
    {
        FileName = fileName;
        CRC = crc;
        AssetFileHash = assetFileHash;
        TypeTreeHash = typeTreeHash;
        Assets = new List<string>(assets);
    }

    public AssetBundleManifestData(AssetBundleManifestData data) 
    {
        FileName = data.FileName;
        CRC = data.CRC;
        AssetFileHash = data.AssetFileHash;
        TypeTreeHash = data.TypeTreeHash;
        Assets = new List<string>(data.Assets);
    }
    #endregion

    #region Methods
    public static AssetBundleManifestData CreateAssetBundleManifestData(string fileName, string text)
    {
        AssetBundleManifestData result = new AssetBundleManifestData();

        if (text != null && text.Length > 0)
        {
            string[] rows = text.Split(new char[] { '\r', '\n' }, System.StringSplitOptions.RemoveEmptyEntries);
            if (rows != null && rows.Length > 0)
            {
                uint crc = 0;
                Hash128 assetFileHash = new Hash128();
                Hash128 typeTreeHash = new Hash128();
                List<string> assets = new List<string>();

                for (int rowIndex = 0; rowIndex < rows.Length; ++rowIndex)
                {
                    if (rows[rowIndex].Contains("CRC:"))
                    {
                        string[] column = rows[rowIndex].Split(':');
                        crc = uint.Parse(column[column.Length - 1].Trim());
                    }
                    else if (rows[rowIndex].Contains("AssetFileHash:"))
                    {
                        string[] column = rows[rowIndex + 2].Split(':');
                        assetFileHash = Hash128.Parse(column[column.Length - 1].Trim());
                    }
                    else if (rows[rowIndex].Contains("TypeTreeHash:"))
                    {
                        string[] column = rows[rowIndex + 2].Split(':');
                        typeTreeHash = Hash128.Parse(column[column.Length - 1].Trim());
                    }
                    else if (rows[rowIndex].Contains("Assets:"))
                    {
                        for (int r = rowIndex + 1; r < rows.Length; ++r)
                        {
                            if (rows[r].StartsWith("- "))
                            {
                                string assetName = rows[r].Substring(2, rows[r].Length - 2);
                                assets.Add(assetName);
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }

                result = new AssetBundleManifestData(
                      fileName
                    , crc
                    , assetFileHash
                    , typeTreeHash
                    , assets
                );
            }
        }

        return result;
    }

    public string GetDebugText()
    {
        string assetText = "";
        foreach (string asset in Assets)
        {
            if (assetText.Length > 0)
            {
                assetText += "\n- " + asset;
            }
            else
            {
                assetText += "- " + asset;
            }
        }

        int index = 0;
        string result = string.Format(
                "FileName: {" + index++ + "}"
              + "\nCRC: {" + index++ + "}"
              + "\nAssetFileHash: {" + index++ + "}"
              + "\nTypeTreeHash: {" + index++ + "}"
              + "\nAssets: {" + index++ + "}"
            , FileName
            , CRC
            , AssetFileHash
            , TypeTreeHash
            , assetText
        );

        return result;
    }
    #endregion
}
