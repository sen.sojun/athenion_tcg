﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BooleanParser : DataParser
{
    #region Delegate
    public delegate bool BooleanMethod(DataParam param, Requester requester, out bool result);
    #endregion

    #region Methods
    public static bool ParamToBoolean(DataParam param, Requester requester, out bool result)
    {
        BooleanMethod method;
        bool isSuccess = ParamToMethod(param, requester, out method);
        if (isSuccess)
        {
            return method.Invoke(param, requester, out result);
        }
        else
        {
            result = false;
            return false;
        }
    }

    public static bool ParamToMethod(DataParam param, Requester requester, out BooleanMethod method)
    {
        method = null;

        bool result;
        bool isSuccess = bool.TryParse(param.Key, out result);

        if (isSuccess)
        {
            method = delegate (DataParam p, Requester rq, out bool r)
            {
                r = result;
                return true;
            };
        }
        else
        {
            switch (param.Key)
            {
                case "IsNot": method = IsNot; break;
                case "IsAnd": method = IsAnd; break;
                case "IsOr": method = IsOr; break;
                case "IsIntEqual": method = IsIntEqual; break;
                case "IsIntMore": method = IsIntMore; break;
                case "IsIntLess": method = IsIntLess; break;
                case "IsIntNotEqual": method = IsIntNotEqual; break;
                case "IsIntMoreEqual": method = IsIntMoreEqual; break;
                case "IsIntLessEqual": method = IsIntLessEqual; break;
                case "IsMinionListEmpty": method = IsMinionListEmpty; break;
                case "IsMinionListNotEmpty": method = IsMinionListNotEmpty; break;
                case "IsMinionListInclude": method = IsMinionListInclude; break;
                case "IsMinionListContainID": method = IsMinionListContainID; break;
                case "IsSlotListEmpty": method = IsSlotListEmpty; break;
                case "IsSlotListNotEmpty": method = IsSlotListNotEmpty; break;
                case "IsSlotListInclude": method = IsSlotListInclude; break;
                case "IsCritical": method = IsCritical; break;
                case "IsMyTurn": method = IsMyTurn; break;
                case "IsIntHasKey": method = IsIntHasKey; break;
                case "IsMinionHasKey": method = IsMinionHasKey; break;
                default: Debug.LogError("BooleanParser: Fail to parse boolean. " + param.Key); break;
            }
        }

        return (method != null);
    }
    #endregion

    #region Boolean Methods
    private static bool IsNot(DataParam param, Requester requester, out bool result)
    {
        result = false;
        bool isSuccess = true;

        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.Boolean, out p[0]);

        if (isSuccess)
        {
            bool value;
            isSuccess &= BooleanParser.ParamToBoolean(p[0], requester, out value);
            if (isSuccess)
            {
                result = (!value);
                return true;
            }
        }

        return false;
    }

    private static bool IsAnd(DataParam param, Requester requester, out bool result)
    {
        result = true;
        bool isSuccess = true;

        foreach (string dataParam in param.Parameters)
        {
            isSuccess &= DataParam.TextToDataParam(dataParam, DataParam.ParamType.Boolean, out param);
            if (isSuccess)
            {
                isSuccess &= BooleanParser.ParamToBoolean(
                    param
                    , requester
                    , out bool parserResult
                );

                result &= parserResult;
            }
            else
            {
                return false;
            }
        }

        return true;
    }

    private static bool IsOr(DataParam param, Requester requester, out bool result)
    {
        result = true;
        bool isSuccess = true;

        foreach (string dataParam in param.Parameters)
        {
            isSuccess &= DataParam.TextToDataParam(dataParam, DataParam.ParamType.Boolean, out param);
            if (isSuccess)
            {
                isSuccess &= BooleanParser.ParamToBoolean(
                    param
                    , requester
                    , out bool parserResult
                );

                result |= parserResult;
            }
            else
            {
                return false;
            }
        }

        return true;
    }

    private static bool IsIntEqual(DataParam param, Requester requester, out bool result)
    {
        result = false;
        bool isSuccess = true;

        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.Integer, out p[0]);
        isSuccess &= DataParam.TextToDataParam(param.Parameters[1], DataParam.ParamType.Integer, out p[1]);

        if (isSuccess)
        {
            int value1;
            int value2;
            isSuccess &= IntParser.ParamToInt(p[0], requester, out value1);
            isSuccess &= IntParser.ParamToInt(p[1], requester, out value2);
            if (isSuccess)
            {
                result = (value1 == value2);
                return true;
            }
        }

        return false;
    }

    private static bool IsIntMore(DataParam param, Requester requester, out bool result)
    {
        result = false;
        bool isSuccess = true;

        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.Integer, out p[0]);
        isSuccess &= DataParam.TextToDataParam(param.Parameters[1], DataParam.ParamType.Integer, out p[1]);

        if (isSuccess)
        {
            int value1;
            int value2;
            isSuccess &= IntParser.ParamToInt(p[0], requester, out value1);
            isSuccess &= IntParser.ParamToInt(p[1], requester, out value2);
            if (isSuccess)
            {
                result = (value1 > value2);
                return true;
            }
        }

        return false;
    }

    private static bool IsIntLess(DataParam param, Requester requester, out bool result)
    {
        result = false;
        bool isSuccess = true;

        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.Integer, out p[0]);
        isSuccess &= DataParam.TextToDataParam(param.Parameters[1], DataParam.ParamType.Integer, out p[1]);

        if (isSuccess)
        {
            int value1;
            int value2;
            isSuccess &= IntParser.ParamToInt(p[0], requester, out value1);
            isSuccess &= IntParser.ParamToInt(p[1], requester, out value2);
            if (isSuccess)
            {
                result = (value1 < value2);
                return true;
            }
        }

        return false;
    }

    private static bool IsIntNotEqual(DataParam param, Requester requester, out bool result)
    {
        result = false;
        bool isSuccess = true;

        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.Integer, out p[0]);
        isSuccess &= DataParam.TextToDataParam(param.Parameters[1], DataParam.ParamType.Integer, out p[1]);

        if (isSuccess)
        {
            int value1;
            int value2;
            isSuccess &= IntParser.ParamToInt(p[0], requester, out value1);
            isSuccess &= IntParser.ParamToInt(p[1], requester, out value2);
            if (isSuccess)
            {
                result = (value1 != value2);
                return true;
            }
        }

        return false;
    }

    private static bool IsIntMoreEqual(DataParam param, Requester requester, out bool result)
    {
        result = false;
        bool isSuccess = true;

        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.Integer, out p[0]);
        isSuccess &= DataParam.TextToDataParam(param.Parameters[1], DataParam.ParamType.Integer, out p[1]);

        if (isSuccess)
        {
            int value1;
            int value2;
            isSuccess &= IntParser.ParamToInt(p[0], requester, out value1);
            isSuccess &= IntParser.ParamToInt(p[1], requester, out value2);
            if (isSuccess)
            {
                result = (value1 >= value2);
                return true;
            }
        }

        return false;
    }

    private static bool IsIntLessEqual(DataParam param, Requester requester, out bool result)
    {
        result = false;
        bool isSuccess = true;

        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.Integer, out p[0]);
        isSuccess &= DataParam.TextToDataParam(param.Parameters[1], DataParam.ParamType.Integer, out p[1]);

        if (isSuccess)
        {
            int value1;
            int value2;
            isSuccess &= IntParser.ParamToInt(p[0], requester, out value1);
            isSuccess &= IntParser.ParamToInt(p[1], requester, out value2);
            if (isSuccess)
            {
                result = (value1 <= value2);
                return true;
            }
        }

        return false;
    }

    private static bool IsMinionListEmpty(DataParam param, Requester requester, out bool result)
    {
        result = false;
        bool isSuccess = true;

        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);

        if (isSuccess)
        {
            List<MinionData> minion;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minion);
            if(isSuccess)
            {
                result = (minion.Count <= 0);
                return true;
            }
        }

        return false;
    }

    private static bool IsMinionListNotEmpty(DataParam param, Requester requester, out bool result)
    {
        result = false;
        bool isSuccess = true;

        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);

        if (isSuccess)
        {
            List<MinionData> minion;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minion);
            if (isSuccess)
            {
                result = (minion.Count > 0);
                return true;
            }
        }

        return false;
    }

    private static bool IsMinionListInclude(DataParam param, Requester requester, out bool result)
    {
        result = false;
        bool isSuccess = true;

        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(param.Parameters[1], DataParam.ParamType.MinionList, out p[1]);

        if (isSuccess)
        {
            List<MinionData> minionList1;
            List<MinionData> minionList2;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minionList1);
            isSuccess &= MinionParser.ParamToMinionList(p[1], requester, out minionList2);
            if (isSuccess)
            {
                foreach (MinionData minion2 in minionList2)
                {
                    bool isFound = false;
                    foreach (MinionData minion1 in minionList1)
                    {
                        if (minion1.UniqueID == minion2.UniqueID)
                        {
                            isFound = true;
                            break;
                        }
                    }

                    if (!isFound)
                    {
                        result = false;
                        return true;
                    }
                }

                result = true;
                return true;
            }
        }

        return false;
    }

    private static bool IsMinionListContainID(DataParam param, Requester requester, out bool result)
    {
        result = false;
        bool isSuccess = true;

        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(param.Parameters[1], DataParam.ParamType.String, out p[1]);

        if (isSuccess)
        {
            List<MinionData> minionList;
            string cardID;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minionList);
            isSuccess &= StringParser.ParamToString(p[1], requester, out cardID);
            if (isSuccess)
            {
                foreach(MinionData minion in minionList)
                {
                    if(minion.BaseID == cardID)
                    {
                        result = true;
                        break;
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool IsSlotListEmpty(DataParam param, Requester requester, out bool result)
    {
        result = false;
        bool isSuccess = true;

        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.SlotList, out p[0]);

        if (isSuccess)
        {
            List<SlotData> slot;
            isSuccess &= SlotParser.ParamToSlotList(p[0], requester, out slot);
            if (isSuccess)
            {
                result = (slot.Count <= 0);
                return true;
            }
        }

        return false;
    }

    private static bool IsSlotListNotEmpty(DataParam param, Requester requester, out bool result)
    {
        result = false;
        bool isSuccess = true;

        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.SlotList, out p[0]);

        if (isSuccess)
        {
            List<SlotData> slot;
            isSuccess &= SlotParser.ParamToSlotList(p[0], requester, out slot);
            if (isSuccess)
            {
                result = (slot.Count > 0);
                return true;
            }
        }

        return false;
    }

    private static bool IsSlotListInclude(DataParam param, Requester requester, out bool result)
    {
        result = false;
        bool isSuccess = true;

        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.SlotList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(param.Parameters[1], DataParam.ParamType.SlotList, out p[1]);

        if (isSuccess)
        {
            List<SlotData> slotList1;
            List<SlotData> slotList2;
            isSuccess &= SlotParser.ParamToSlotList(p[0], requester, out slotList1);
            isSuccess &= SlotParser.ParamToSlotList(p[1], requester, out slotList2);
            if (isSuccess)
            {
                foreach (SlotData slot2 in slotList2)
                {
                    bool isFound = false;
                    foreach(SlotData slot1 in slotList1)
                    {
                        if (slot1.SlotID == slot2.SlotID)
                        {
                            isFound = true;
                            break;
                        }
                    }

                    if (!isFound)
                    {
                        result = false;
                        return true;
                    }
                }

                result = true;
                return true;
            }
        }

        return false;
    }

    private static bool IsCritical(DataParam param, Requester requester, out bool result)
    {
        result = BattleManager.Instance.IsCurrentCritical();
        return true;
    }

    private static bool IsMyTurn(DataParam param, Requester requester, out bool result)
    {
        result = (GameManager.Instance.CurrentPlayerIndex == requester.PlayerIndex);
        return true;
    }

    private static bool IsIntHasKey(DataParam param, Requester requester, out bool result)
    {
        result = false;
        bool isSuccess = true;

        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.String, out p[0]);

        if (isSuccess)
        {
            string key;
            int value;
            isSuccess &= StringParser.ParamToString(p[0], requester, out key);
            if (isSuccess)
            {
                string realSaveKey = GameManager.Instance.CreateSaveKey(key, requester);
                result = GameManager.Instance.GetSavedInteger(realSaveKey, out value);
                return true;
            }
        }

        return false;
    }

    private static bool IsMinionHasKey(DataParam param, Requester requester, out bool result)
    {
        result = false;
        bool isSuccess = true;

        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.String, out p[0]);

        if (isSuccess)
        {
            string key;
            List<MinionData> value;
            isSuccess &= StringParser.ParamToString(p[0], requester, out key);
            if (isSuccess)
            {
                string realSaveKey = GameManager.Instance.CreateSaveKey(key, requester);
                result = GameManager.Instance.GetSavedMinionList(realSaveKey, out value);
                return true;
            }
        }

        return false;
    }

    #endregion
}
