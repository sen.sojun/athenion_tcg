﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectParser
{
    #region Methods
    public static bool ParamToEffect(DataParam param, Requester requester, out EffectData result)
    {
        //result = null;

        return EffectData.CreateEffect(param, requester, out result);

        //return (result != null); // if result != null return true else return false
    }
    #endregion
}
