﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlotBuffParser
{
    #region Methods
    public static bool ParamToSlotBuff(string key, BuffCounterType counterType, int counterAmount, DataParam param, Requester requester, out SlotBuffData result)
    {
        //result = null;

        return SlotBuffData.CreateSlotBuffData(key, counterType, counterAmount, param, requester, out result);

        //return (result != null); // if result != null return true else return false
    }
    #endregion
}
