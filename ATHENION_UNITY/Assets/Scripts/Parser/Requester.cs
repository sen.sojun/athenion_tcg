﻿public class Requester
{
    #region Enums
    public enum RequesterType
    {
          Player
        , Minion
        , Spell
        , Slot
        , Hero
    }
    #endregion

    #region Public Properties
    public RequesterType Type { get; private set; }
    public MinionData Minion { get; private set; }
    public SpellData Spell { get; private set; }
    public SlotData Slot { get; private set; }
    public CardAbilityData Ability { get; private set; }
    public PlayerIndex PlayerIndex { get; private set; }
    public HeroData Hero { get; private set; }
    #endregion

    #region Constructors
    public Requester(MinionData minion)
    {
        Type = RequesterType.Minion;
        Ability = null;
        PlayerIndex = minion.Owner;
        Minion = minion;
        Spell = null;
        Slot = null;
        Hero = null;
    }

    public Requester(SpellData spell)
    {
        Type = RequesterType.Spell;
        Ability = null;
        PlayerIndex = spell.Owner;
        Minion = null;
        Spell = spell;
        Slot = null;
        Hero = null;
    }

    public Requester(SlotData slot, PlayerIndex playerIndex)
    {
        Type = RequesterType.Slot;
        Ability = null;
        PlayerIndex = playerIndex;
        Minion = null;
        Spell = null;
        Slot = slot;
        Hero = null;
    }

    public Requester(CardAbilityData ability)
    {
        Ability = ability;
        PlayerIndex = ability.Owner.Owner;
        Slot = null;
        Hero = null;

        switch(ability.Owner.Type)
        {
            case CardType.Minion:
            case CardType.Minion_NotInDeck:
            case CardType.Hero:
                Minion = ability.Owner as MinionData;
                Spell = null;
                Type = RequesterType.Minion;
                break;

            case CardType.Spell:
            case CardType.Spell_NotInDeck:
                Minion = null;
                Spell = ability.Owner as SpellData;
                Type = RequesterType.Spell;
                break;
        }
    }

    public Requester(PlayerIndex playerIndex)
    {
        Type = RequesterType.Player;
        Ability = null;
        Minion = null;
        Spell = null;
        Slot = null;
        PlayerIndex = playerIndex;
        Hero = null;
    }

    public Requester(HeroData hero)
    {
        Type = RequesterType.Hero;
        Ability = null;
        Minion = null;
        Spell = null;
        Slot = null;
        PlayerIndex = hero.Owner;
        Hero = hero;
    }

    // Copy Constructor
    public Requester(Requester requester)
    {
        Type = requester.Type;
        Ability = requester.Ability;
        Minion = requester.Minion;
        Spell = requester.Spell;
        Slot = requester.Slot;
        PlayerIndex = requester.PlayerIndex;
        Hero = requester.Hero;
    }
    #endregion

    #region Methods
    public int GetUniqueID()
    {
        int uniqueID = 0;
        switch (Type)
        {
            case Requester.RequesterType.Spell: uniqueID = Spell.UniqueID; break;
            case Requester.RequesterType.Minion: uniqueID = Minion.UniqueID; break;
            default: uniqueID = (int)PlayerIndex; break;
        }

        return uniqueID;
    }
    #endregion
}
