﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConditionParser : DataParser
{
    #region Methods
    public static bool ParamToCondition(DataParam param, Requester requester, out ConditionData result)
    {
        //result = null;

        return ConditionData.CreateCondition(param, requester, out result);

        //return (result != null); // if result != null return true else return false
    }
    #endregion
}
