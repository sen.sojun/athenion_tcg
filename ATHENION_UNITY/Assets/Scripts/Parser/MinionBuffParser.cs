﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinionBuffParser
{
    #region Methods
    public static bool ParamToMinionBuff(int effectDataUniqueID, MinionBuffOrigins origin, BuffCounterType counterType, int counterAmount, DataParam param, Requester requester, out MinionBuffData result)
    {
        //result = null;

        return MinionBuffData.CreateMinionBuffData(effectDataUniqueID, origin, counterType, counterAmount, param, requester, out result);

        //return (result != null); // if result != null return true else return false
    }
    #endregion
}
