﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataParser
{
    #region Delegates
    public delegate void OnComplete(bool isSuccess, object data);
    //public delegate void OnListComplete(bool isSuccess, List<object> dataList);

    public delegate void OnParam(DataParam param, Requester requester, OnComplete onComplete);
    //public delegate void OnParamList(DataParam param, Requester requester, OnListComplete onComplete);
    #endregion

    #region Properties
    protected List<object> _objList;
    #endregion

    #region Methods
    public static void ParseParam(DataParam param, Requester requester, OnComplete onComplete)
    {
        if (onComplete != null)
        {
            switch (param.Type)
            {
                /*
                case DataParam.ParamType.String: StringParser.ParamToString(param, requester, onComplete); break;
                case DataParam.ParamType.Integer: IntParser.ParamToInt(param, requester, onComplete); break;
                case DataParam.ParamType.Float: FloatParser.ParamToFloat(param, requester, onComplete); break;
                case DataParam.ParamType.Boolean: BooleanParser.ParamToBoolean(param, requester, onComplete); break;

                case DataParam.ParamType.PawnList: PawnParser.ParamToPawnDataList(param, requester, onComplete); break;

                case DataParam.ParamType.BlockList: BlockParser.ParamToBlockDataList(param, requester, onComplete); break;

                case DataParam.ParamType.EventList: EventParser.ParamToEventDataList(param, requester, onComplete); break;

                case DataParam.ParamType.TrapList: TrapParser.ParamToTrapDataList(param, requester, onComplete); break;

                case DataParam.ParamType.VP: VPParser.ParamToVPTokenDataList(param, requester, onComplete); break;
                //case DataParam.ParamType.VPList: VPParser.ParamToVPTokenDataList(param, requester, onComplete); break;

                case DataParam.ParamType.PlayerIndexList: PlayerIndexParser.ParamToPlayerIndexList(param, requester, onComplete); break;

                //case DataParam.ParamType.Condition: ConditionParser.ParamToCondition(param, requester, onComplete); break;
                //case DataParam.ParamType.Effect: EffectParser.ParamToEffect(param, requester, onComplete); break;
                */

                default: onComplete.Invoke(false, null); break;
            }
        }
    }
    #endregion
}