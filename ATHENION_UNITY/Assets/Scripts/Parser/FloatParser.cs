﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatParser : DataParser
{
    #region Methods
    public static bool ParamToFloat(DataParam param, Requester requester, out float result)
    {
        result = 0.0f;
        bool isSuccess = float.TryParse(param.Key, out result);

        if (isSuccess)
        {
            return true;
        }
        else
        {
            switch (param.Key)
            {
                case "FLOAT_SAMPLE": return FloatSample(param, requester, out result);
                default: Debug.LogError("FloatParser: Fail to parse float. " + param.Key); return false;
            }
        }
    }
    #endregion

    #region Float
    private static bool FloatSample(DataParam param, Requester requester, out float result)
    {
        result = 0.0f;
        return true;
    }
    #endregion
}
