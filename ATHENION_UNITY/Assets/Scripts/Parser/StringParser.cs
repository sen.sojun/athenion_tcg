﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StringParser : DataParser
{
    #region Methods
    public static bool ParamToString(DataParam param, Requester requester, out string result)
    {
        switch (param.Key.ToUpper())
        {
            case "STRMINIONCARDID": return StrMinionCardID(param, requester, out result);
        }

        result = param.Key;
        return true;
    }
    #endregion

    #region String Methods
    private static bool StrMinionCardID(DataParam param, Requester requester, out string result)
    {
        result = string.Empty;
        bool isSuccess = true;

        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);

        if (isSuccess)
        {
            List<MinionData> minions;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minions);
            if (isSuccess)
            {
                if(minions != null && minions.Count > 0)
                {
                    result = minions[0].BaseID;
                    return true;
                }
            }
        }

        return false;
    }
    #endregion
}