﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardDirectionParser : DataParser
{
    #region Methods
    public static bool ParamToCardDirectionList(DataParam param, Requester requester, out CardDirection result)
    {
        result = new CardDirection();
        switch (param.Key)
        {
            case "CardDirectionOfMinion": return CardDirectionOfMinion(param, requester, out result);
            case "CardDirectionBaseOfMinion": return CardDirectionBaseOfMinion(param, requester, out result);
            case "CardDirectionAll": return CardDirectionAll(param, requester, out result);
            case "CardDirectionEmpty": return CardDirectionEmpty(param, requester, out result);
            case "CardDirectionInvert": return CardDirectionInvert(param, requester, out result);
            case "CardDirectionBaseInvert": return CardDirectionBaseInvert(param, requester, out result);
            default: Debug.LogError("CardDirectionParser: Fail to parse direction. " + param.Key); return false;
        }
    }
    #endregion

    #region Card Direction Methods

    private static bool CardDirectionOfMinion(DataParam param, Requester requester, out CardDirection result)
    {
        result = new CardDirection();
        bool isSuccess = true;

        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);

        if (isSuccess)
        {
            List<MinionData> minions;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minions);
            isSuccess &= minions.Count > 0;
            if (isSuccess)
            {
                result = new CardDirection(minions[0].CardDir);
                return true;
            }
        }
        return false;
    }

    private static bool CardDirectionBaseOfMinion(DataParam param, Requester requester, out CardDirection result)
    {
        result = new CardDirection();
        bool isSuccess = true;

        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);

        if (isSuccess)
        {
            List<MinionData> minions;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minions);
            isSuccess &= minions.Count > 0;
            if (isSuccess)
            {
                result = new CardDirection(minions[0].CardDirBase);
                return true;
            }
        }
        return false;
    }

    private static bool CardDirectionAll(DataParam param, Requester requester, out CardDirection result)
    {
        CardDirection allDirection = new CardDirection(new int[8] {1, 1, 1, 1, 1, 1, 1, 1});
        result = new CardDirection(allDirection);
        return true;
    }

    private static bool CardDirectionEmpty(DataParam param, Requester requester, out CardDirection result)
    {
        CardDirection emptyDirection = new CardDirection(new int[8] { -1, -1, -1, -1, -1, -1, -1, -1 });
        //CardDirection emptyDirection = new CardDirection(requester.Minion.CardDir.GetInvertCardDirection());
        result = new CardDirection(emptyDirection);
        return true;
    }

    private static bool CardDirectionInvert(DataParam param, Requester requester, out CardDirection result)
    {
        result = new CardDirection();
        bool isSuccess = true;

        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.CardDirection, out p[0]);

        if (isSuccess)
        {
            CardDirection cardDir;
            isSuccess &= CardDirectionParser.ParamToCardDirectionList(p[0], requester, out cardDir);

            if (isSuccess)
            {
                result = new CardDirection(cardDir.GetInvertCardDirection());
                return true;
            }
        }
        return false;
    }

    private static bool CardDirectionBaseInvert(DataParam param, Requester requester, out CardDirection result)
    {
        result = new CardDirection();
        bool isSuccess = true;

        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.CardDirection, out p[0]);

        if (isSuccess)
        {
            CardDirection cardDir;
            isSuccess &= CardDirectionParser.ParamToCardDirectionList(p[0], requester, out cardDir);

            if (isSuccess)
            {
                result = new CardDirection(cardDir.GetInvertCardDirection());
                return true;
            }
        }
        return false;
    }
    #endregion
}
