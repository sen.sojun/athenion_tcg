﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerIndexParser : DataParser
{
    #region Methods
    public static bool ParamToPlayerIndexList(DataParam param, Requester requester, out List<PlayerIndex> result)
    {
        result = null;
        switch (param.Key)
        {
            case "PlayerMe": return PlayerMe(param, requester, out result);
            case "PlayerNotMe": return PlayerNotMe(param, requester, out result);
            case "PlayerFriend": return PlayerFriend(param, requester, out result);
            case "PlayerEnemy": return PlayerEnemy(param, requester, out result);
            case "PlayerGetSave": return PlayerGetSave(param, requester, out result);
            default: Debug.LogError("PlayerIndexParser: Fail to parse playerIndex. " + param.Key); return false;
        }
    }
    #endregion

    #region PlayerIndex Methods
    private static bool PlayerMe(DataParam param, Requester requester, out List<PlayerIndex> result)
    {
        result = new List<PlayerIndex>{ requester.PlayerIndex };
        return true;
    }

    private static bool PlayerNotMe(DataParam param, Requester requester, out List<PlayerIndex> result)
    {
        result = new List<PlayerIndex>();
        foreach (PlayerIndex playerIndex in GameManager.Instance.PlayerIndexList)
        {
            if (playerIndex != requester.PlayerIndex)
            {
                result.Add(playerIndex);
            }
        }

        return (result != null && result.Count > 0);
    }

    private static bool PlayerFriend(DataParam param, Requester requester, out List<PlayerIndex> result)
    {
        result = new List<PlayerIndex>();

        foreach (PlayerIndex playerIndex in GameManager.Instance.PlayerIndexList)
        {
            if (GameManager.Instance.IsFriend(requester.PlayerIndex, playerIndex))
            {
                result.Add(playerIndex); 
            }
        }

        return true;
    }

    private static bool PlayerEnemy(DataParam param, Requester requester, out List<PlayerIndex> result)
    {
        result = new List<PlayerIndex>();

        foreach (PlayerIndex playerIndex in GameManager.Instance.PlayerIndexList)
        {
            if (!GameManager.Instance.IsFriend(requester.PlayerIndex, playerIndex))
            {
                result.Add(playerIndex);
            }
        }

        return true;
    }

    private static bool PlayerGetSave(DataParam param, Requester requester, out List<PlayerIndex> result)
    {
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.String, out p[0]);
        if (isSuccess)
        {
            string saveKey;
            isSuccess &= StringParser.ParamToString(p[0], requester, out saveKey);
            if (isSuccess)
            {
                List<PlayerIndex> playerIndexList;
                string realSaveKey = GameManager.Instance.CreateSaveKey(saveKey, requester);
                GameManager.Instance.GetSavedPlayerIndexList(realSaveKey, out playerIndexList);
                if (playerIndexList != null)
                {
                    result = new List<PlayerIndex>(playerIndexList);
                    return true;
                }
            }
        }

        result = null;
        return false;
    }
    #endregion
}
