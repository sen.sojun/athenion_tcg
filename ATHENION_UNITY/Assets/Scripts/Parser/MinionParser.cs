﻿using System.Collections.Generic;
using UnityEngine;

public class MinionParser : DataParser
{
    #region Methods
    public static bool ParamToMinionList(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        switch (param.Key)
        {
            case "MinionFilterMe": return MinionFilterMe(param, requester, out result);
            case "MinionFilterNotMe": return MinionFilterNotMe(param, requester, out result);
            case "MinionFilterMine": return MinionFilterMine(param, requester, out result);
            case "MinionFilterNotMine": return MinionFilterNotMine(param, requester, out result);
            case "MinionFilterMineNotMe": return MinionFilterMineNotMe(param, requester, out result);
            case "MinionFilterRandom": return MinionFilterRandom(param, requester, out result);
            case "MinionFilterRandomNotDuplicate": return MinionFilterRandomNotDuplicate(param, requester, out result);
            case "MinionFilterFriend": return MinionFilterFriend(param, requester, out result);
            case "MinionFilterEnemy": return MinionFilterEnemy(param, requester, out result);
            case "MinionFilterAlive": return MinionFilterAlive(param, requester, out result);
            case "MinionFilterOut": return MinionFilterOut(param, requester, out result);
            case "MinionFilterAPLess": return MinionFilterAPLess(param, requester, out result);
            case "MinionFilterHPLess": return MinionFilterHPLess(param, requester, out result);
            case "MinionFilterSPLess": return MinionFilterSPLess(param, requester, out result);
            case "MinionFilterAPMore": return MinionFilterAPMore(param, requester, out result);
            case "MinionFilterHPMore": return MinionFilterHPMore(param, requester, out result);
            case "MinionFilterSPMore": return MinionFilterSPMore(param, requester, out result);
            case "MinionFilterHasBaseArmor": return MinionFilterHasBaseArmor(param, requester, out result);
            case "MinionFilterArmorMore": return MinionFilterArmorMore(param, requester, out result);
            case "MinionFilterAPEqual": return MinionFilterAPEqual(param, requester, out result);
            case "MinionFilterHPEqual": return MinionFilterHPEqual(param, requester, out result);
            case "MinionFilterSPEqual": return MinionFilterSPEqual(param, requester, out result);
            case "MinionFilterAPMax": return MinionFilterAPMax(param, requester, out result);
            case "MinionFilterHPMax": return MinionFilterHPMax(param, requester, out result);
            case "MinionFilterSPMax": return MinionFilterSPMax(param, requester, out result);
            case "MinionFilterAPMin": return MinionFilterAPMin(param, requester, out result);
            case "MinionFilterHPMin": return MinionFilterHPMin(param, requester, out result);
            case "MinionFilterSPMin": return MinionFilterSPMin(param, requester, out result);
            case "MinionFilterHPFull": return MinionFilterHPFull(param, requester, out result);
            case "MinionFilterHPNotFull": return MinionFilterHPNotFull(param, requester, out result);
            case "MinionFilterCardID": return MinionFilterCardID(param, requester, out result);
            case "MinionFilterNotCardID": return MinionFilterNotCardID(param, requester, out result);
            case "MinionFilterBerserk": return MinionFilterBerserk(param, requester, out result);
            case "MinionFilterBeBerserk": return MinionFilterBeBerserk(param, requester, out result);
            case "MinionFilterAura": return MinionFilterAura(param, requester, out result);
            case "MinionFilterBeAura": return MinionFilterBeAura(param, requester, out result);
            case "MinionFilterTaunt": return MinionFilterTaunt(param, requester, out result);
            case "MinionFilterBeTaunt": return MinionFilterBeTaunt(param, requester, out result);
            case "MinionFilterNotBeTaunt": return MinionFilterNotBeTaunt(param, requester, out result);
            case "MinionFilterLink": return MinionFilterLink(param, requester, out result);
            case "MinionFilterLinked": return MinionFilterLinked(param, requester, out result);
            case "MinionFilterNotLink": return MinionFilterNotLink(param, requester, out result);
            case "MinionFilterNotLinked": return MinionFilterNotLinked(param, requester, out result);
            case "MinionFilterFreeze": return MinionFilterFreeze(param, requester, out result);
            case "MinionFilterNotFreeze": return MinionFilterNotFreeze(param, requester, out result);
            case "MinionFilterSilenced": return MinionFilterSilenced(param, requester, out result);
            case "MinionFilterNotSilenced": return MinionFilterNotSilenced(param, requester, out result);
            case "MinionFilterLastwish": return MinionFilterLastwish(param, requester, out result);
            case "MinionFilterDarkPower": return MinionFilterDarkPower(param, requester, out result);
            case "MinionFilterBeDarkMatter": return MinionFilterBeDarkMatter(param, requester, out result);
            case "MinionFilterNotBeDarkMatter": return MinionFilterNotBeDarkMatter(param, requester, out result);
            case "MinionFilterBePiercing": return MinionFilterBePiercing(param, requester, out result);
            case "MinionFilterNotBePiercing": return MinionFilterNotBePiercing(param, requester, out result);
            case "MinionFilterTargetable": return MinionFilterTargetable(param, requester, out result);
            case "MinionFilterUntargetable": return MinionFilterUntargetable(param, requester, out result);
            case "MinionFilterStealth": return MinionFilterStealth(param, requester, out result);
            case "MinionFilterBackstab": return MinionFilterBackstab(param, requester, out result);
            case "MinionFilterChain": return MinionFilterChain(param, requester, out result);
            case "MinionFilterFaction": return MinionFilterFaction(param, requester, out result);
            case "MinionAllAttackerList": return MinionAllAttackerList(param, requester, out result);
            case "MinionAllSlot": return MinionAllSlot(param, requester, out result);
            case "MinionAllHand": return MinionAllHand(param, requester, out result);
            case "MinionAllGraveyard": return MinionAllGraveyard(param, requester, out result);
            case "MinionAllDeck": return MinionAllDeck(param, requester, out result);
            case "MinionAdjacent": return MinionAdjacent(param, requester, out result);
            case "MinionAdjacent8": return MinionAdjacent8(param, requester, out result);
            case "MinionAdjacentAlong": return MinionAdjacentAlong(param, requester, out result);
            case "MinionInMinionDirection": return MinionInMinionDirection(param, requester, out result);
            case "MinionMe": return MinionMe(param, requester, out result);
            case "MinionAtSlot": return MinionAtSlot(param, requester, out result);
            case "MinionMerge": return MinionMerge(param, requester, out result);
            case "MinionGetSave": return MinionGetSave(param, requester, out result);
            case "MinionTriggerer": return MinionTriggerer(param, requester, out result);
            case "MinionAttacker": return MinionAttacker(param, requester, out result);
            case "MinionDefender": return MinionDefender(param, requester, out result);
            case "MinionAllDefender": return MinionAllDefender(param, requester, out result);
            case "MinionLink": return MinionLink(param, requester, out result);
            default: Debug.LogError("MinionParser: Fail to parse minion. " + param.Key); return false;
        }
    }
    #endregion

    #region Minion Methods
    private static bool MinionFilterMe(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        bool isSuccess = true;

        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);

        if (isSuccess)
        {
            List<MinionData> minions;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minions);
            if (isSuccess)
            {
                foreach (MinionData minion in minions)
                {
                    if (minion.UniqueID == requester.Minion.UniqueID)
                    {
                        result.Add(minion);
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterNotMe(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        bool isSuccess = true;

        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);

        if (isSuccess)
        {
            List<MinionData> minions;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minions);
            if (isSuccess)
            {
                foreach (MinionData minion in minions)
                {
                    if (minion.UniqueID != requester.Minion.UniqueID)
                    {
                        result.Add(minion);
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterMine(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        bool isSuccess = true;

        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);

        if (isSuccess)
        {
            List<MinionData> minions;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minions);
            if (isSuccess)
            {
                foreach(MinionData minion in minions)
                {
                    if(minion.Owner == requester.PlayerIndex)
                    {
                        result.Add(minion);
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterNotMine(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        bool isSuccess = true;

        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);

        if (isSuccess)
        {
            List<MinionData> minions;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minions);
            if (isSuccess)
            {
                foreach (MinionData minion in minions)
                {
                    if (minion.Owner != requester.PlayerIndex)
                    {
                        result.Add(minion);
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterMineNotMe(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        bool isSuccess = true;

        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);

        if (isSuccess)
        {
            List<MinionData> minions;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minions);
            if (isSuccess)
            {
                foreach (MinionData minion in minions)
                {
                    if (minion.Owner == requester.PlayerIndex
                        && minion.UniqueID != requester.Minion.UniqueID)
                    {
                        result.Add(minion);
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterRandom(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(param.Parameters[1], DataParam.ParamType.Integer, out p[1]);
        if (isSuccess)
        {
            List<MinionData> minionList;
            int minionCount;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minionList);
            isSuccess &= IntParser.ParamToInt(p[1], requester, out minionCount);
            if (isSuccess)
            {
                int selectedIndex;
                for (int i = 0; i < minionCount; i++)
                {
                    if (minionList.Count > 0)
                    {
                        //No Chance to ซ้ำ
                        //selectedIndex = Random.Range(0, minionList.Count);
                        //result.Add(minionList[selectedIndex]);
                        //minionList.RemoveAt(selectedIndex);

                        //Chance to ซ้ำ
                        selectedIndex = Random.Range(0, minionList.Count);
                        result.Add(minionList[selectedIndex]);
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterRandomNotDuplicate(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(param.Parameters[1], DataParam.ParamType.Integer, out p[1]);
        if (isSuccess)
        {
            List<MinionData> minionList;
            int minionCount;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minionList);
            isSuccess &= IntParser.ParamToInt(p[1], requester, out minionCount);
            if (isSuccess)
            {
                int selectedIndex;
                for (int i = 0; i < minionCount; i++)
                {
                    if (minionList.Count > 0)
                    {
                        //No Chance to ซ้ำ
                        selectedIndex = Random.Range(0, minionList.Count);
                        result.Add(minionList[selectedIndex]);
                        minionList.RemoveAt(selectedIndex);

                        //Chance to ซ้ำ
                        //selectedIndex = Random.Range(0, minionList.Count);
                        //result.Add(minionList[selectedIndex]);
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterFriend(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        bool isSuccess = true;

        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);

        if (isSuccess)
        {
            List<MinionData> minions;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minions);
            if (isSuccess)
            {
                foreach (MinionData minion in minions)
                {
                    if (GameManager.Instance.IsFriend(requester.PlayerIndex, minion.Owner))
                    {
                        result.Add(minion);
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterEnemy(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        bool isSuccess = true;

        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);

        if (isSuccess)
        {
            List<MinionData> minions;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minions);
            if (isSuccess)
            {
                foreach (MinionData minion in minions)
                {
                    if (!GameManager.Instance.IsFriend(requester.PlayerIndex, minion.Owner))
                    {
                        result.Add(minion);
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterAlive(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        bool isSuccess = true;

        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);

        if (isSuccess)
        {
            List<MinionData> minions;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minions);
            if (isSuccess)
            {
                foreach (MinionData minion in minions)
                {
                    if (minion.IsAlive)
                    {
                        result.Add(minion);
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterOut(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        bool isSuccess = true;

        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(param.Parameters[1], DataParam.ParamType.MinionList, out p[1]);

        if (isSuccess)
        {
            List<MinionData> minions;
            List<MinionData> minionsOut;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minions);
            isSuccess &= MinionParser.ParamToMinionList(p[1], requester, out minionsOut);
            if (isSuccess)
            {
                for(int i = 0; i < minions.Count; i++)
                {
                    if (minionsOut.Exists(x => x.UniqueID == minions[i].UniqueID))
                    {
                        minions.RemoveAt(i);
                        i--;
                    }
                }

                result = new List<MinionData>(minions);
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterAPLess(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        bool isSuccess = true;

        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(param.Parameters[1], DataParam.ParamType.Integer, out p[1]);

        if (isSuccess)
        {
            int value;
            List<MinionData> minions;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minions);
            isSuccess &= IntParser.ParamToInt(p[1], requester, out value);
            if (isSuccess)
            {
                foreach (MinionData minion in minions)
                {
                    if (minion.ATKCurrent < value)
                    {
                        result.Add(minion);
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterHPLess(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        bool isSuccess = true;

        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(param.Parameters[1], DataParam.ParamType.Integer, out p[1]);

        if (isSuccess)
        {
            int value;
            List<MinionData> minions;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minions);
            isSuccess &= IntParser.ParamToInt(p[1], requester, out value);
            if (isSuccess)
            {
                foreach (MinionData minion in minions)
                {
                    if (minion.HPCurrent < value)
                    {
                        result.Add(minion);
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterSPLess(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        bool isSuccess = true;

        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(param.Parameters[1], DataParam.ParamType.Integer, out p[1]);

        if (isSuccess)
        {
            int value;
            List<MinionData> minions;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minions);
            isSuccess &= IntParser.ParamToInt(p[1], requester, out value);
            if (isSuccess)
            {
                foreach (MinionData minion in minions)
                {
                    if (minion.SpiritCurrent < value)
                    {
                        result.Add(minion);
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterAPMore(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        bool isSuccess = true;

        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(param.Parameters[1], DataParam.ParamType.Integer, out p[1]);

        if (isSuccess)
        {
            int value;
            List<MinionData> minions;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minions);
            isSuccess &= IntParser.ParamToInt(p[1], requester, out value);
            if (isSuccess)
            {
                foreach (MinionData minion in minions)
                {
                    if (minion.ATKCurrent > value)
                    {
                        result.Add(minion);
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterHPMore(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        bool isSuccess = true;

        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(param.Parameters[1], DataParam.ParamType.Integer, out p[1]);

        if (isSuccess)
        {
            int value;
            List<MinionData> minions;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minions);
            isSuccess &= IntParser.ParamToInt(p[1], requester, out value);
            if (isSuccess)
            {
                foreach (MinionData minion in minions)
                {
                    if (minion.HPCurrent > value)
                    {
                        result.Add(minion);
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterSPMore(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        bool isSuccess = true;

        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(param.Parameters[1], DataParam.ParamType.Integer, out p[1]);

        if (isSuccess)
        {
            int value;
            List<MinionData> minions;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minions);
            isSuccess &= IntParser.ParamToInt(p[1], requester, out value);
            if (isSuccess)
            {
                foreach (MinionData minion in minions)
                {
                    if (minion.SpiritCurrent > value)
                    {
                        result.Add(minion);
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterAPEqual(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        bool isSuccess = true;

        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(param.Parameters[1], DataParam.ParamType.Integer, out p[1]);

        if (isSuccess)
        {
            int value;
            List<MinionData> minions;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minions);
            isSuccess &= IntParser.ParamToInt(p[1], requester, out value);
            if (isSuccess)
            {
                foreach (MinionData minion in minions)
                {
                    if (minion.ATKCurrent == value)
                    {
                        result.Add(minion);
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterHPEqual(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        bool isSuccess = true;

        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(param.Parameters[1], DataParam.ParamType.Integer, out p[1]);

        if (isSuccess)
        {
            int value;
            List<MinionData> minions;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minions);
            isSuccess &= IntParser.ParamToInt(p[1], requester, out value);
            if (isSuccess)
            {
                foreach (MinionData minion in minions)
                {
                    if (minion.HPCurrent == value)
                    {
                        result.Add(minion);
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterSPEqual(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        bool isSuccess = true;

        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(param.Parameters[1], DataParam.ParamType.Integer, out p[1]);

        if (isSuccess)
        {
            int value;
            List<MinionData> minions;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minions);
            isSuccess &= IntParser.ParamToInt(p[1], requester, out value);
            if (isSuccess)
            {
                foreach (MinionData minion in minions)
                {
                    if (minion.SpiritCurrent == value)
                    {
                        result.Add(minion);
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterAPMax(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        bool isSuccess = true;

        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);

        if (isSuccess)
        {
            int value;
            List<int> values = new List<int>();
            List<MinionData> minions;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minions);
            if (isSuccess)
            {
                foreach(MinionData minion in minions)
                {
                    values.Add(minion.ATKCurrent);
                }
                value = Mathf.Max(values.ToArray());

                foreach (MinionData minion in minions)
                {
                    if (minion.ATKCurrent == value)
                    {
                        result.Add(minion);
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterHPMax(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        bool isSuccess = true;

        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);

        if (isSuccess)
        {
            int value;
            List<int> values = new List<int>();
            List<MinionData> minions;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minions);
            if (isSuccess)
            {
                foreach (MinionData minion in minions)
                {
                    values.Add(minion.HPCurrent);
                }
                value = Mathf.Max(values.ToArray());

                foreach (MinionData minion in minions)
                {
                    if (minion.HPCurrent == value)
                    {
                        result.Add(minion);
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterSPMax(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        bool isSuccess = true;

        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);

        if (isSuccess)
        {
            int value;
            List<int> values = new List<int>();
            List<MinionData> minions;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minions);
            if (isSuccess)
            {
                foreach (MinionData minion in minions)
                {
                    values.Add(minion.SpiritCurrent);
                }
                value = Mathf.Max(values.ToArray());

                foreach (MinionData minion in minions)
                {
                    if (minion.SpiritCurrent == value)
                    {
                        result.Add(minion);
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterAPMin(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        bool isSuccess = true;

        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);

        if (isSuccess)
        {
            int value;
            List<int> values = new List<int>();
            List<MinionData> minions;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minions);
            if (isSuccess)
            {
                foreach (MinionData minion in minions)
                {
                    values.Add(minion.ATKCurrent);
                }
                value = Mathf.Min(values.ToArray());

                foreach (MinionData minion in minions)
                {
                    if (minion.ATKCurrent == value)
                    {
                        result.Add(minion);
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterHPMin(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        bool isSuccess = true;

        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);

        if (isSuccess)
        {
            int value;
            List<int> values = new List<int>();
            List<MinionData> minions;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minions);
            if (isSuccess)
            {
                foreach (MinionData minion in minions)
                {
                    values.Add(minion.HPCurrent);
                }
                value = Mathf.Min(values.ToArray());

                foreach (MinionData minion in minions)
                {
                    if (minion.HPCurrent == value)
                    {
                        result.Add(minion);
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterSPMin(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        bool isSuccess = true;

        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);

        if (isSuccess)
        {
            int value;
            List<int> values = new List<int>();
            List<MinionData> minions;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minions);
            if (isSuccess)
            {
                foreach (MinionData minion in minions)
                {
                    values.Add(minion.SpiritCurrent);
                }
                value = Mathf.Min(values.ToArray());

                foreach (MinionData minion in minions)
                {
                    if (minion.SpiritCurrent == value)
                    {
                        result.Add(minion);
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterHPFull(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        bool isSuccess = true;

        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);

        if (isSuccess)
        {
            List<MinionData> minions;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minions);
            if (isSuccess)
            {
                foreach (MinionData minion in minions)
                {
                    if (minion.HPCurrent >= minion.HPMax
                       && minion.IsAlive)
                    {
                        result.Add(minion);
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterHPNotFull(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        bool isSuccess = true;

        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);

        if (isSuccess)
        {
            List<MinionData> minions;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minions);
            if (isSuccess)
            {
                foreach (MinionData minion in minions)
                {
                    if(minion.HPCurrent < minion.HPMax
                       && minion.IsAlive)
                    {
                        result.Add(minion);
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterCardID(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        bool isSuccess = true;

        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(param.Parameters[1], DataParam.ParamType.String, out p[1]);

        if (isSuccess)
        {
            List<MinionData> minions;
            string cardID;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minions);
            isSuccess &= StringParser.ParamToString(p[1], requester, out cardID);
            if (isSuccess)
            {
                result = new List<MinionData>();
                foreach (MinionData minion in minions)
                {
                    if (minion.BaseID == cardID)
                    {
                        result.Add(minion);
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterNotCardID(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        bool isSuccess = true;

        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(param.Parameters[1], DataParam.ParamType.String, out p[1]);

        if (isSuccess)
        {
            List<MinionData> minions;
            string cardID;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minions);
            isSuccess &= StringParser.ParamToString(p[1], requester, out cardID);
            if (isSuccess)
            {
                result = new List<MinionData>();
                foreach (MinionData minion in minions)
                {
                    if (minion.BaseID != cardID)
                    {
                        result.Add(minion);
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterBerserk(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        List<MinionData> nodeList = new List<MinionData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        if (isSuccess)
        {
            List<MinionData> minionList;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minionList);
            if (isSuccess)
            {
                if (minionList != null)
                {
                    foreach (MinionData minion in minionList)
                    {
                        if (minion.IsAbilityBerserk)
                        {
                            result.Add(minion);
                        }
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterBeBerserk(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        List<MinionData> nodeList = new List<MinionData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        if (isSuccess)
        {
            List<MinionData> minionList;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minionList);
            if (isSuccess)
            {
                if (minionList != null)
                {
                    foreach (MinionData minion in minionList)
                    {
                        if (minion.IsBerserkActive)
                        {
                            result.Add(minion);
                        }
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterAura(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        List<MinionData> nodeList = new List<MinionData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        if (isSuccess)
        {
            List<MinionData> minionList;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minionList);
            if (isSuccess)
            {
                if (minionList != null)
                {
                    foreach (MinionData minion in minionList)
                    {
                        if (minion.IsAbilityAura)
                        {
                            result.Add(minion);
                        }
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterBeAura(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        List<MinionData> nodeList = new List<MinionData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        if (isSuccess)
        {
            List<MinionData> minionList;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minionList);
            if (isSuccess)
            {
                if (minionList != null)
                {
                    foreach (MinionData minion in minionList)
                    {
                        if (minion.IsBeAura)
                        {
                            result.Add(minion);
                        }
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterTaunt(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        List<MinionData> nodeList = new List<MinionData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        if (isSuccess)
        {
            List<MinionData> minionList;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minionList);
            if (isSuccess)
            {
                if (minionList != null)
                {
                    foreach (MinionData minion in minionList)
                    {
                        if (minion.IsAbilityTaunt)
                        {
                            result.Add(minion);
                        }
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterBeTaunt(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        List<MinionData> nodeList = new List<MinionData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        if (isSuccess)
        {
            List<MinionData> minionList;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minionList);
            if (isSuccess)
            {
                if (minionList != null)
                {
                    foreach (MinionData minion in minionList)
                    {
                        if (minion.IsBeTaunt)
                        {
                            result.Add(minion);
                        }
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterNotBeTaunt(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        List<MinionData> nodeList = new List<MinionData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        if (isSuccess)
        {
            List<MinionData> minionList;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minionList);
            if (isSuccess)
            {
                if (minionList != null)
                {
                    foreach (MinionData minion in minionList)
                    {
                        if (!minion.IsBeTaunt)
                        {
                            result.Add(minion);
                        }
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterLink(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        List<MinionData> nodeList = new List<MinionData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        if (isSuccess)
        {
            List<MinionData> minionList;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minionList);
            if (isSuccess)
            {
                if (minionList != null)
                {
                    foreach (MinionData minion in minionList)
                    {
                        if (minion.IsAbilityLink)
                        {
                            result.Add(minion);
                        }
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterLinked(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        List<MinionData> nodeList = new List<MinionData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        if (isSuccess)
        {
            List<MinionData> minionList;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minionList);
            if (isSuccess)
            {
                if (minionList != null)
                {
                    foreach (MinionData minion in minionList)
                    {
                        if (minion.IsBeLink)
                        {
                            result.Add(minion);
                        }
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterNotLink(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        List<MinionData> nodeList = new List<MinionData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        if (isSuccess)
        {
            List<MinionData> minionList;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minionList);
            if (isSuccess)
            {
                if (minionList != null)
                {
                    foreach (MinionData minion in minionList)
                    {
                        if (!minion.IsAbilityLink)
                        {
                            result.Add(minion);
                        }
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterNotLinked(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        List<MinionData> nodeList = new List<MinionData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        if (isSuccess)
        {
            List<MinionData> minionList;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minionList);
            if (isSuccess)
            {
                if (minionList != null)
                {
                    foreach (MinionData minion in minionList)
                    {
                        if (!minion.IsBeLink)
                        {
                            result.Add(minion);
                        }
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterFreeze(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        List<MinionData> nodeList = new List<MinionData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        if (isSuccess)
        {
            List<MinionData> minionList;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minionList);
            if (isSuccess)
            {
                if (minionList != null)
                {
                    foreach (MinionData minion in minionList)
                    {
                        if (minion.IsBeFreeze)
                        {
                            result.Add(minion);
                        }
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterNotFreeze(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        List<MinionData> nodeList = new List<MinionData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        if (isSuccess)
        {
            List<MinionData> minionList;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minionList);
            if (isSuccess)
            {
                if (minionList != null)
                {
                    foreach (MinionData minion in minionList)
                    {
                        if (!minion.IsBeFreeze)
                        {
                            result.Add(minion);
                        }
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterSilenced(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        List<MinionData> nodeList = new List<MinionData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        if (isSuccess)
        {
            List<MinionData> minionList;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minionList);
            if (isSuccess)
            {
                if (minionList != null)
                {
                    foreach (MinionData minion in minionList)
                    {
                        if (minion.IsBeSilence)
                        {
                            result.Add(minion);
                        }
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterNotSilenced(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        List<MinionData> nodeList = new List<MinionData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        if (isSuccess)
        {
            List<MinionData> minionList;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minionList);
            if (isSuccess)
            {
                if (minionList != null)
                {
                    foreach (MinionData minion in minionList)
                    {
                        if (!minion.IsBeSilence)
                        {
                            result.Add(minion);
                        }
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterHasBaseArmor(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        List<MinionData> nodeList = new List<MinionData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        if (isSuccess)
        {
            List<MinionData> minionList;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minionList);
            if (isSuccess)
            {
                if (minionList != null)
                {
                    foreach (MinionData minion in minionList)
                    {
                        if (minion.ArmorDefault > 0)
                        {
                            result.Add(minion);
                        }
                    }
                }
                return true;
            }
        }
        return false;
    }

    private static bool MinionFilterArmorMore(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        List<MinionData> nodeList = new List<MinionData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(param.Parameters[1], DataParam.ParamType.Integer, out p[1]);
        if (isSuccess)
        {
            List<MinionData> minionList;
            int value;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minionList);
            isSuccess &= IntParser.ParamToInt(p[1], requester, out value);
            if (isSuccess)
            {
                if (minionList != null)
                {
                    foreach (MinionData minion in minionList)
                    {
                        if (minion.ArmorCurrent > value)
                        {
                            result.Add(minion);
                        }
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterLastwish(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        List<MinionData> nodeList = new List<MinionData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        if (isSuccess)
        {
            List<MinionData> minionList;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minionList);
            if (isSuccess)
            {
                if (minionList != null)
                {
                    foreach (MinionData minion in minionList)
                    {
                        if (minion.IsAbilityLastwish)
                        {
                            result.Add(minion);
                        }
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterDarkPower(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        List<MinionData> nodeList = new List<MinionData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        if (isSuccess)
        {
            List<MinionData> minionList;
            isSuccess &= ParamToMinionList(p[0], requester, out minionList);
            if (isSuccess)
            {
                if (minionList != null)
                {
                    foreach (MinionData minion in minionList)
                    {
                        if (minion.IsAbilityDarkPower)
                        {
                            result.Add(minion);
                        }
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterBeDarkMatter(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        List<MinionData> nodeList = new List<MinionData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        if (isSuccess)
        {
            List<MinionData> minionList;
            isSuccess &= ParamToMinionList(p[0], requester, out minionList);
            if (isSuccess)
            {
                if (minionList != null)
                {
                    foreach (MinionData minion in minionList)
                    {
                        if (minion.IsBeDarkMatter)
                        {
                            result.Add(minion);
                        }
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterNotBeDarkMatter(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        List<MinionData> nodeList = new List<MinionData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        if (isSuccess)
        {
            List<MinionData> minionList;
            isSuccess &= ParamToMinionList(p[0], requester, out minionList);
            if (isSuccess)
            {
                if (minionList != null)
                {
                    foreach (MinionData minion in minionList)
                    {
                        if (!minion.IsBeDarkMatter)
                        {
                            result.Add(minion);
                        }
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterBePiercing(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        List<MinionData> nodeList = new List<MinionData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        if (isSuccess)
        {
            List<MinionData> minionList;
            isSuccess &= ParamToMinionList(p[0], requester, out minionList);
            if (isSuccess)
            {
                if (minionList != null)
                {
                    foreach (MinionData minion in minionList)
                    {
                        if (minion.IsBePiercing)
                        {
                            result.Add(minion);
                        }
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterNotBePiercing(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        List<MinionData> nodeList = new List<MinionData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        if (isSuccess)
        {
            List<MinionData> minionList;
            isSuccess &= ParamToMinionList(p[0], requester, out minionList);
            if (isSuccess)
            {
                if (minionList != null)
                {
                    foreach (MinionData minion in minionList)
                    {
                        if (!minion.IsBePiercing)
                        {
                            result.Add(minion);
                        }
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterTargetable(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        List<MinionData> nodeList = new List<MinionData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        if (isSuccess)
        {
            List<MinionData> minionList;
            isSuccess &= ParamToMinionList(p[0], requester, out minionList);
            if (isSuccess)
            {
                if (minionList != null)
                {
                    foreach (MinionData minion in minionList)
                    {
                        if (!minion.IsBeUntargetable)
                        {
                            result.Add(minion);
                        }
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterUntargetable(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        List<MinionData> nodeList = new List<MinionData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        if (isSuccess)
        {
            List<MinionData> minionList;
            isSuccess &= ParamToMinionList(p[0], requester, out minionList);
            if (isSuccess)
            {
                if (minionList != null)
                {
                    foreach (MinionData minion in minionList)
                    {
                        if (minion.IsBeUntargetable)
                        {
                            result.Add(minion);
                        }
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterStealth(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        List<MinionData> nodeList = new List<MinionData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        if (isSuccess)
        {
            List<MinionData> minionList;
            isSuccess &= ParamToMinionList(p[0], requester, out minionList);
            if (isSuccess)
            {
                if (minionList != null)
                {
                    foreach (MinionData minion in minionList)
                    {
                        if (minion.IsMinionStatus(CardStatusType.Be_Stealth))
                        {
                            result.Add(minion);
                        }
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterBackstab(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        List<MinionData> nodeList = new List<MinionData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        if (isSuccess)
        {
            List<MinionData> minionList;
            isSuccess &= ParamToMinionList(p[0], requester, out minionList);
            if (isSuccess)
            {
                if (minionList != null)
                {
                    foreach (MinionData minion in minionList)
                    {
                        if (minion.IsAbilityBackstab)
                        {
                            result.Add(minion);
                        }
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterChain(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        List<MinionData> nodeList = new List<MinionData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        if (isSuccess)
        {
            List<MinionData> minionList;
            isSuccess &= ParamToMinionList(p[0], requester, out minionList);
            if (isSuccess)
            {
                if (minionList != null)
                {
                    foreach (MinionData minion in minionList)
                    {
                        if (minion.IsAbilityChain)
                        {
                            result.Add(minion);
                        }
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionFilterFaction(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(param.Parameters[1], DataParam.ParamType.String, out p[1]);
        if (isSuccess)
        {
            List<MinionData> minionList;
            string elementString;
            CardElementType elementType;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minionList);
            isSuccess &= StringParser.ParamToString(p[1], requester, out elementString);
            isSuccess &= GameHelper.StrToEnum(elementString, out elementType);
            if (isSuccess)
            {
                if (minionList != null)
                {
                    foreach (MinionData minion in minionList)
                    {
                        if (minion.Element.IsElement(elementType))
                        {
                            result.Add(minion);
                        }
                    }
                }
                return true;
            }
        }
        return false;
    }

    private static bool MinionAllAttackerList(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>(GameManager.Instance.AttackerList);
        return true;
    }

    private static bool MinionAllSlot(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>(GameManager.Instance.Data.GetAllMinionsInSlot());
        return true;
    }

    private static bool MinionAllHand(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        foreach (BattlePlayerData playerData in GameManager.Instance.Data.PlayerDataList)
        {
            foreach(BattleCardData card in playerData.CardList[CardZone.Hand])
            {
                if(card.Type == CardType.Minion || card.Type == CardType.Minion_NotInDeck)
                {
                    result.Add(card as MinionData);
                }
            }
        }
        return true;
    }

    private static bool MinionAllGraveyard(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        foreach (BattlePlayerData playerData in GameManager.Instance.Data.PlayerDataList)
        {
            foreach (BattleCardData card in playerData.CardList[CardZone.Graveyard])
            {
                if (card.Type == CardType.Minion || card.Type == CardType.Minion_NotInDeck)
                {
                    result.Add(card as MinionData);
                }
            }
        }
        return true;
    }

    private static bool MinionAllDeck(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        foreach (BattlePlayerData playerData in GameManager.Instance.Data.PlayerDataList)
        {
            foreach (BattleCardData card in playerData.CardList[CardZone.Deck])
            {
                if (card.Type == CardType.Minion || card.Type == CardType.Minion_NotInDeck)
                {
                    result.Add(card as MinionData);
                }
            }
        }
        return true;
    }

    private static bool MinionAdjacent(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        bool isSuccess = true;

        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);

        if (isSuccess)
        {
            List<MinionData> minions;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minions);
            if (isSuccess)
            {
                SlotData slot;
                MinionData battleCard;
                foreach (MinionData minion in minions)
                {
                    if (minion.CurrentZone == CardZone.Battlefield)
                    {
                        foreach (CardDirectionType dir in System.Enum.GetValues(typeof(CardDirectionType)))
                        {
                            if (GameManager.Instance.Data.SlotDatas[minion.SlotID].GetAdjacentSlot(dir, out slot) && (int)dir % 2 == 0)
                            {
                                if (!slot.IsSlotEmpty)
                                {
                                    if (GameManager.Instance.FindMinionBattleCard(slot.BattleCardUniqueID, out battleCard))
                                    {
                                        result.Add(battleCard);
                                    }
                                }
                            }
                        }
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionAdjacent8(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        bool isSuccess = true;

        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);

        if (isSuccess)
        {
            List<MinionData> minions;
            isSuccess &= ParamToMinionList(p[0], requester, out minions);
            if (isSuccess)
            {
                SlotData slot;
                MinionData battleCard;
                foreach (MinionData minion in minions)
                {
                    if (minion.CurrentZone == CardZone.Battlefield)
                    {
                        foreach (CardDirectionType dir in System.Enum.GetValues(typeof(CardDirectionType)))
                        {
                            if (GameManager.Instance.Data.SlotDatas[minion.SlotID].GetAdjacentSlot(dir, out slot))
                            {
                                if (!slot.IsSlotEmpty)
                                {
                                    if (GameManager.Instance.FindMinionBattleCard(slot.BattleCardUniqueID, out battleCard))
                                    {
                                        result.Add(battleCard);
                                    }
                                }
                            }
                        }
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionAdjacentAlong(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        bool isSuccess = true;

        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);

        if (isSuccess)
        {
            List<MinionData> minions;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minions);
            if (isSuccess)
            {
                bool isFound = true;
                SlotData slot;
                SlotData slotTmp;
                MinionData battleCard;
                foreach (MinionData minion in minions)
                {
                    if (minion.CurrentZone == CardZone.Battlefield)
                    {
                        foreach (CardDirectionType dir in System.Enum.GetValues(typeof(CardDirectionType)))
                        {
                            if (GameManager.Instance.Data.SlotDatas[minion.SlotID].GetAdjacentSlot(dir, out slot) && (int)dir % 2 == 0)
                            {
                                if (GameManager.Instance.FindMinionBattleCard(slot.BattleCardUniqueID, out battleCard))
                                {
                                    result.Add(battleCard);
                                }

                                isFound = true;
                                while (isFound)
                                {
                                    if (slot.GetAdjacentSlot(dir, out slotTmp))
                                    {
                                        if (!slotTmp.IsSlotEmpty)
                                        {
                                            if (GameManager.Instance.FindMinionBattleCard(slotTmp.BattleCardUniqueID, out battleCard))
                                            {
                                                result.Add(battleCard);
                                            }
                                        }
                                        slot = slotTmp;
                                    }
                                    else
                                    {
                                        isFound = false;
                                    }
                                }
                            }
                        }
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionInMinionDirection(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        bool isSuccess = true;

        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);

        if (isSuccess)
        {
            List<MinionData> minions;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minions);
            if (isSuccess)
            {
                bool isFound = true;
                SlotData slot;
                SlotData slotTmp;
                MinionData battleCard;
                foreach (MinionData minion in minions)
                {
                    if (minion.CurrentZone == CardZone.Battlefield)
                    {
                        foreach (CardDirectionType dir in minion.CardDir.GetAllDirection())
                        {
                            if (GameManager.Instance.Data.SlotDatas[minion.SlotID].GetAdjacentSlot(dir, out slot) && (int)dir % 2 == 0)
                            {
                                if (GameManager.Instance.FindMinionBattleCard(slot.BattleCardUniqueID, out battleCard))
                                {
                                    result.Add(battleCard);
                                }

                                isFound = true;
                                while (isFound)
                                {
                                    if (slot.GetAdjacentSlot(dir, out slotTmp))
                                    {
                                        if (!slotTmp.IsSlotEmpty)
                                        {
                                            if (GameManager.Instance.FindMinionBattleCard(slotTmp.BattleCardUniqueID, out battleCard))
                                            {
                                                result.Add(battleCard);
                                            }
                                        }
                                        slot = slotTmp;
                                    }
                                    else
                                    {
                                        isFound = false;
                                    }
                                }
                            }
                        }
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionMe(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        if (requester.Minion != null)
        {
            MinionData minion = requester.Minion;
            result.Add(minion);

            return true;
        }

        return false;
    }

    private static bool MinionAtSlot(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        bool isSuccess = true;

        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.SlotList, out p[0]);

        if (isSuccess)
        {
            List<SlotData> slots;
            isSuccess &= SlotParser.ParamToSlotList(p[0], requester, out slots);
            if (isSuccess)
            {
                MinionData battleCard;
                foreach (SlotData slot in slots)
                {
                    if (!slot.IsSlotEmpty)
                    {
                        if (GameManager.Instance.FindMinionBattleCard(slot.BattleCardUniqueID, out battleCard))
                        {
                            result.Add(battleCard);
                        }
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool MinionMerge(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        bool isSuccess = true;
        foreach (string parameter in param.Parameters)
        {
            DataParam p;
            isSuccess &= DataParam.TextToDataParam(parameter, DataParam.ParamType.MinionList, out p);
            if (isSuccess)
            {
                List<MinionData> minionList;
                isSuccess &= MinionParser.ParamToMinionList(p, requester, out minionList);
                if (isSuccess)
                {
                    foreach (MinionData minion in minionList)
                    {
                        if (!result.Contains(minion))
                        {
                            result.Add(minion);
                        }
                    }
                }
            }
        }

        if (!isSuccess)
        {
            result = new List<MinionData>();
        }

        return isSuccess;
    }

    private static bool MinionGetSave(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.String, out p[0]);
        if (isSuccess)
        {
            string saveKey;
            isSuccess &= StringParser.ParamToString(p[0], requester, out saveKey);
            if (isSuccess)
            {
                List<MinionData> savedValue;
                string realSaveKey = GameManager.Instance.CreateSaveKey(saveKey, requester);
                isSuccess = GameManager.Instance.GetSavedMinionList(realSaveKey, out savedValue);
                if (isSuccess)
                {
                    result = savedValue;
                    return true;
                }
            }
        }

        return false;
    }

    private static bool MinionTriggerer(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        if (requester.Ability != null)
        {
            MinionData battleCard;
            if (requester.Type == Requester.RequesterType.Minion)
            {
                if (requester.Ability.Triggerer != null && GameManager.Instance.FindMinionBattleCard(requester.Ability.Triggerer.Minion.UniqueID, out battleCard))
                {
                    result.Add(battleCard);
                    return true;
                }
            }
        }

        return false;
    }

    private static bool MinionAttacker(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        if (BattleManager.Instance.CurrentAttacker != null)
        {
            result.Add(BattleManager.Instance.CurrentAttacker);
            return true;
        }

        return false;
    }

    private static bool MinionDefender(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        if (BattleManager.Instance.CurrentDefender != null)
        {
            result.Add(BattleManager.Instance.CurrentDefender);
            return true;
        }

        return false;
    }

    private static bool MinionAllDefender(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        if (BattleManager.Instance.CurrentAllDefenderList != null)
        {
            result = new List<MinionData>(BattleManager.Instance.CurrentAllDefenderList);
            return true;
        }

        return false;
    }

    private static bool MinionLink(DataParam param, Requester requester, out List<MinionData> result)
    {
        result = new List<MinionData>();
        List<MinionData> nodeList = new List<MinionData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        if (isSuccess)
        {
            List<MinionData> minionMain;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minionMain);
            if (isSuccess)
            {
                result.Add(minionMain[0]);
                nodeList.Add(minionMain[0]);
                MinionData minionAdjacent;
                CardDirectionType opsDir;
                while(nodeList.Count > 0)
                {
                    foreach (CardDirectionType dir in System.Enum.GetValues(typeof(CardDirectionType)))
                    {
                        if (nodeList[0].CardDir.IsDirection(dir) && nodeList[0].GetAdjacentMinion(dir, out minionAdjacent))
                        {
                            if(minionAdjacent.IsAlive)
                            {
                                if (!result.Contains(minionAdjacent) && minionAdjacent.Owner == requester.PlayerIndex && GameHelper.GetOppositeDirection(dir, out opsDir))
                                {
                                    if (minionAdjacent.CardDir.IsDirection(opsDir))
                                    {
                                        result.Add(minionAdjacent);
                                        nodeList.Add(minionAdjacent);
                                    }
                                }
                            }
                        }
                    }
                    nodeList.RemoveAt(0);
                }

                return true;
            }
        }

        return false;
    }
    #endregion
}