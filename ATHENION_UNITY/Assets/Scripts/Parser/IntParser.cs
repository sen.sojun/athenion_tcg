﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntParser : DataParser
{
    #region Methods
    public static bool ParamToInt(DataParam param, Requester requester, out int result)
    {
        result = 0;
        bool isSuccess = int.TryParse(param.Key, out result);
        if (isSuccess)
        {
            return true;
        }
        else
        {
            switch (param.Key)
            {
                case "IntClamp":                return IntClamp(param, requester, out result);
                case "IntRandomInteger":        return IntRandomInteger(param, requester, out result);
                case "IntRandomIntegerInList":  return IntRandomIntegerInList(param, requester, out result);
                case "IntPlus":                 return IntPlus(param, requester, out result);
                case "IntMinus":                return IntMinus(param, requester, out result);
                case "IntMultiply":             return IntMultiply(param, requester, out result);
                case "IntDivide":               return IntDivide(param, requester, out result);
                case "IntNegative":             return IntNegative(param, requester, out result);
                case "IntMinionMaxHP":          return IntMinionMaxHP(param, requester, out result);
                case "IntMinionHP":             return IntMinionHP(param, requester, out result);
                case "IntMinionAP":             return IntMinionAP(param, requester, out result);
                case "IntMinionSP":             return IntMinionSP(param, requester, out result);
                case "IntMinionArmor":          return IntMinionArmor(param, requester, out result);
                case "IntMinionHPBase":         return IntMinionHPBase(param, requester, out result);
                case "IntMinionAPBase":         return IntMinionAPBase(param, requester, out result);
                case "IntMinionSPBase":         return IntMinionSPBase(param, requester, out result);
                case "IntMinionArmorBase":      return IntMinionArmorBase(param, requester, out result);
                case "IntPlayerLP":             return IntPlayerLP(param, requester, out result);
                case "IntPlayerSP":             return IntPlayerSP(param, requester, out result);
                case "IntPlayerAP":             return IntPlayerAP(param, requester, out result);
                case "IntMinionListCount":      return IntMinionListCount(param, requester, out result);
                case "IntSlotLockCountAmount":  return IntSlotLockCountAmount(param, requester, out result);
                case "IntSlotListLockCountAmount": return IntSlotListLockCountAmount(param, requester, out result);
                case "IntSlotListCount":        return IntSlotListCount(param, requester, out result);
                case "IntSumSPMinionList":      return IntSumSPMinionList(param, requester, out result);
                case "IntSumAPMinionList":      return IntSumAPMinionList(param, requester, out result);
                case "IntSumHPMinionList":      return IntSumHPMinionList(param, requester, out result);
                case "IntSumArmorMinionList":   return IntSumArmorMinionList(param, requester, out result);
                case "IntGetSave":              return IntGetSave(param, requester, out result);
                case "IntBeAuraBuffCount":      return IntBeAuraBuffCount(param, requester, out result);
                default: Debug.LogError("IntParser: Fail to parse integer. " + param.Key); return false;
            }
        }
    }
    #endregion

    #region Int Methods
    private static bool IntClamp(DataParam param, Requester requester, out int result)
    {
        bool isSuccess = true;
        int value;
        int minValue;
        int maxValue;
        result = 0;
        DataParam[] p = new DataParam[3];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.Integer, out p[0]);
        isSuccess &= DataParam.TextToDataParam(param.Parameters[1], DataParam.ParamType.Integer, out p[1]);
        isSuccess &= DataParam.TextToDataParam(param.Parameters[2], DataParam.ParamType.Integer, out p[2]);
        if (isSuccess)
        {
            isSuccess &= IntParser.ParamToInt(p[0], requester, out value);
            isSuccess &= IntParser.ParamToInt(p[1], requester, out minValue);
            isSuccess &= IntParser.ParamToInt(p[2], requester, out maxValue);
            if (isSuccess)
            {
                result = Mathf.Clamp(value, minValue, maxValue);
                return true;
            }
        }
        return false;
    }

    private static bool IntRandomInteger(DataParam param, Requester requester, out int result)
    {
        bool isSuccess = true;
        int minValue;
        int maxValue;
        result = 0;
        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.Integer, out p[0]);
        isSuccess &= DataParam.TextToDataParam(param.Parameters[1], DataParam.ParamType.Integer, out p[1]);
        if (isSuccess)
        {
            isSuccess &= IntParser.ParamToInt(p[0], requester, out minValue);
            isSuccess &= IntParser.ParamToInt(p[1], requester, out maxValue);
            if (isSuccess)
            {
                result = Random.Range(minValue, maxValue + 1);
                return true;
            }
        }
        return false;
    }

    private static bool IntRandomIntegerInList(DataParam param, Requester requester, out int result)
    {
        bool isSuccess = true;
        int value1;
        int value2;
        List<int> valueList = new List<int>();
        result = 0;
        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.Integer, out p[0]);
        isSuccess &= DataParam.TextToDataParam(param.Parameters[1], DataParam.ParamType.Integer, out p[1]);
        if (isSuccess)
        {
            isSuccess &= IntParser.ParamToInt(p[0], requester, out value1);
            isSuccess &= IntParser.ParamToInt(p[1], requester, out value2);
            if (isSuccess)
            {
                valueList.Add(value1);
                valueList.Add(value2);
                int index = Random.Range(0, valueList.Count);
                result = valueList[index];
                return true;
            }
        }
        return false;
    }

    private static bool IntPlus(DataParam param, Requester requester, out int result)
    {
        bool isSuccess = true;
        int value1;
        int value2;
        result = 0;
        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.Integer, out p[0]);
        isSuccess &= DataParam.TextToDataParam(param.Parameters[1], DataParam.ParamType.Integer, out p[1]);
        if (isSuccess)
        {
            isSuccess &= IntParser.ParamToInt(p[0], requester, out value1);
            isSuccess &= IntParser.ParamToInt(p[1], requester, out value2);
            if (isSuccess)
            {
                result = value1 + value2;
                return true;
            }
        }
        return false;
    }

    private static bool IntMinus(DataParam param, Requester requester, out int result)
    {
        bool isSuccess = true;
        int value1;
        int value2;
        result = 0;
        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.Integer, out p[0]);
        isSuccess &= DataParam.TextToDataParam(param.Parameters[1], DataParam.ParamType.Integer, out p[1]);
        if (isSuccess)
        {
            isSuccess &= IntParser.ParamToInt(p[0], requester, out value1);
            isSuccess &= IntParser.ParamToInt(p[1], requester, out value2);
            if (isSuccess)
            {
                result = value1 - value2;
                return true;
            }
        }
        return false;
    }

    private static bool IntMultiply(DataParam param, Requester requester, out int result)
    {
        bool isSuccess = true;
        int value1;
        int value2;
        result = 0;
        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.Integer, out p[0]);
        isSuccess &= DataParam.TextToDataParam(param.Parameters[1], DataParam.ParamType.Integer, out p[1]);
        if (isSuccess)
        {
            isSuccess &= IntParser.ParamToInt(p[0], requester, out value1);
            isSuccess &= IntParser.ParamToInt(p[1], requester, out value2);
            if (isSuccess)
            {
                result = value1 * value2;
                return true;
            }
        }
        return false;
    }

    private static bool IntDivide(DataParam param, Requester requester, out int result)
    {
        bool isSuccess = true;
        int value1;
        int value2;
        result = 0;
        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.Integer, out p[0]);
        isSuccess &= DataParam.TextToDataParam(param.Parameters[1], DataParam.ParamType.Integer, out p[1]);
        if (isSuccess)
        {
            isSuccess &= IntParser.ParamToInt(p[0], requester, out value1);
            isSuccess &= IntParser.ParamToInt(p[1], requester, out value2);
            if (isSuccess)
            {
                result = (value1 / value2);
                return true;
            }
        }
        return false;
    }

    private static bool IntNegative(DataParam param, Requester requester, out int result)
    {
        bool isSuccess = true;
        int value1;
        result = 0;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.Integer, out p[0]);
        if (isSuccess)
        {
            isSuccess &= IntParser.ParamToInt(p[0], requester, out value1);
            if (isSuccess)
            {
                result = -value1;
                return true;
            }
        }
        return false;
    }

    private static bool IntMinionMaxHP(DataParam param, Requester requester, out int result)
    {
        result = 0;
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        if (isSuccess)
        {
            List<MinionData> cardList;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out cardList);
            if (isSuccess)
            {
                if (cardList != null && cardList.Count > 0)
                {
                    result = cardList[0].HPMax;
                    return true;
                }
            }
        }
        return false;
    }

    private static bool IntMinionHP(DataParam param, Requester requester, out int result)
    {
        result = 0;
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        if (isSuccess)
        {
            List<MinionData> cardList;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out cardList);
            if (isSuccess)
            {
                if(cardList != null && cardList.Count > 0)
                {
                    result = cardList[0].HPCurrent;
                    return true;
                }
            }
        }
        return false;
    }

    private static bool IntMinionAP(DataParam param, Requester requester, out int result)
    {
        result = 0;
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        if (isSuccess)
        {
            List<MinionData> cardList;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out cardList);
            if (isSuccess)
            {
                if (cardList != null && cardList.Count > 0)
                {
                    result = cardList[0].ATKCurrent;
                    return true;
                }
            }
        }
        return false;
    }

    private static bool IntMinionSP(DataParam param, Requester requester, out int result)
    {
        result = 0;
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        if (isSuccess)
        {
            List<MinionData> cardList;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out cardList);
            if (isSuccess)
            {
                if (cardList != null && cardList.Count > 0)
                {
                    result = cardList[0].SpiritCurrent;
                    return true;
                }
            }
        }
        return false;
    }

    private static bool IntMinionArmor(DataParam param, Requester requester, out int result)
    {
        result = 0;
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        if (isSuccess)
        {
            List<MinionData> cardList;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out cardList);
            if (isSuccess)
            {
                if (cardList != null && cardList.Count > 0)
                {
                    result = cardList[0].ArmorCurrent;
                    return true;
                }
            }
        }
        return false;
    }

    private static bool IntMinionHPBase(DataParam param, Requester requester, out int result)
    {
        result = 0;
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        if (isSuccess)
        {
            List<MinionData> cardList;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out cardList);
            if (isSuccess)
            {
                if (cardList != null && cardList.Count > 0)
                {
                    result = cardList[0].HPCurrentBase;
                    return true;
                }
            }
        }
        return false;
    }

    private static bool IntMinionAPBase(DataParam param, Requester requester, out int result)
    {
        result = 0;
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        if (isSuccess)
        {
            List<MinionData> cardList;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out cardList);
            if (isSuccess)
            {
                if (cardList != null && cardList.Count > 0)
                {
                    result = cardList[0].ATKCurrentBase;
                    return true;
                }
            }
        }
        return false;
    }

    private static bool IntMinionSPBase(DataParam param, Requester requester, out int result)
    {
        result = 0;
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        if (isSuccess)
        {
            List<MinionData> cardList;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out cardList);
            if (isSuccess)
            {
                if (cardList != null && cardList.Count > 0)
                {
                    result = cardList[0].SpiritCurrentBase;
                    return true;
                }
            }
        }
        return false;
    }

    private static bool IntMinionArmorBase(DataParam param, Requester requester, out int result)
    {
        result = 0;
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        if (isSuccess)
        {
            List<MinionData> cardList;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out cardList);
            if (isSuccess)
            {
                if (cardList != null && cardList.Count > 0)
                {
                    result = cardList[0].ArmorCurrentBase;
                    return true;
                }
            }
        }
        return false;
    }

    private static bool IntPlayerLP(DataParam param, Requester requester, out int result)
    {
        result = 0;
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.PlayerIndexList, out p[0]);
        if (isSuccess)
        {
            List<PlayerIndex> playerIndexList;
            isSuccess &= PlayerIndexParser.ParamToPlayerIndexList(p[0], requester, out playerIndexList);
            if (isSuccess)
            {
                if (playerIndexList != null && playerIndexList.Count > 0)
                {
                    result = GameManager.Instance.Data.PlayerDataList[(int)playerIndexList[0]].CurrentHP;
                    return true;
                }
            }
        }
        return false;
    }

    private static bool IntPlayerSP(DataParam param, Requester requester, out int result)
    {
        result = 0;
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.PlayerIndexList, out p[0]);
        if (isSuccess)
        {
            List<PlayerIndex> playerIndexList;
            isSuccess &= PlayerIndexParser.ParamToPlayerIndexList(p[0], requester, out playerIndexList);
            if (isSuccess)
            {
                if (playerIndexList != null && playerIndexList.Count > 0)
                {
                    result = GameManager.Instance.GetSumSpirit(playerIndexList[0]);
                    return true;
                }
            }
        }
        return false;
    }

    private static bool IntPlayerAP(DataParam param, Requester requester, out int result)
    {
        result = 0;
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.PlayerIndexList, out p[0]);
        if (isSuccess)
        {
            List<PlayerIndex> playerIndexList;
            isSuccess &= PlayerIndexParser.ParamToPlayerIndexList(p[0], requester, out playerIndexList);
            if (isSuccess)
            {
                if (playerIndexList != null && playerIndexList.Count > 0)
                {
                    result = GameManager.Instance.Data.PlayerDataList[(int)playerIndexList[0]].CurrentAP;
                    return true;
                }
            }
        }
        return false;
    }

    private static bool IntMinionListCount(DataParam param, Requester requester, out int result)
    {
        result = 0;
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        if (isSuccess)
        {
            List<MinionData> minionList;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minionList);
            if (isSuccess)
            {
                if (minionList != null)
                {
                    result = minionList.Count;
                    return true;
                }
            }
        }
        return false;
    }

    private static bool IntSlotLockCountAmount(DataParam param, Requester requester, out int result)
    {
        result = 0;
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.SlotList, out p[0]);
        if (isSuccess)
        {
            List<SlotData> slotList;
            isSuccess &= SlotParser.ParamToSlotList(p[0], requester, out slotList);
            if (isSuccess)
            {
                if(slotList.Count > 0)
                {
                    foreach (SlotBuffData buff in slotList[0].BuffDataList)
                    {
                        if (buff.Key.Split('_')[0] == "BuffSlotLock")
                        {
                            result = buff.CounterAmount;
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    private static bool IntSlotListLockCountAmount(DataParam param, Requester requester, out int result)
    {
        result = 0;
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.SlotList, out p[0]);
        if (isSuccess)
        {
            List<SlotData> slotList;
            isSuccess &= SlotParser.ParamToSlotList(p[0], requester, out slotList);
            if (isSuccess)
            {
                if (slotList.Count > 0)
                {
                    foreach (SlotBuffData buff in slotList[0].BuffDataList)
                    {
                        if (buff.Key.Split('_')[0] == "BuffSlotLock")
                        {
                            result += buff.CounterAmount;
                        }
                    }
                }
                return true;
            }
        }
        return false;
    }

    private static bool IntSlotListCount(DataParam param, Requester requester, out int result)
    {
        result = 0;
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.SlotList, out p[0]);
        if (isSuccess)
        {
            List<SlotData> slotList;
            isSuccess &= SlotParser.ParamToSlotList(p[0], requester, out slotList);
            if (isSuccess)
            {
                if (slotList != null)
                {
                    result = slotList.Count;
                    return true;
                }
            }
        }
        return false;
    }

    private static bool IntSumSPMinionList(DataParam param, Requester requester, out int result)
    {
        result = 0;
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        if (isSuccess)
        {
            List<MinionData> minionList;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minionList);
            if (isSuccess)
            {
                if (minionList != null)
                {
                    foreach (MinionData minion in minionList)
                    {
                        result += minion.SpiritCurrent;
                    }
                    return true;
                }
            }
        }
        return false;
    }

    private static bool IntSumAPMinionList(DataParam param, Requester requester, out int result)
    {
        result = 0;
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        if (isSuccess)
        {
            List<MinionData> minionList;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minionList);
            if (isSuccess)
            {
                if (minionList != null)
                {
                    foreach (MinionData minion in minionList)
                    {
                        result += minion.ATKCurrent;
                    }
                    return true;
                }
            }
        }
        return false;
    }

    private static bool IntSumHPMinionList(DataParam param, Requester requester, out int result)
    {
        result = 0;
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        if (isSuccess)
        {
            List<MinionData> minionList;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minionList);
            if (isSuccess)
            {
                if (minionList != null)
                {
                    foreach (MinionData minion in minionList)
                    {
                        result += minion.HPCurrent;
                    }
                    return true;
                }
            }
        }
        return false;
    }

    private static bool IntSumArmorMinionList(DataParam param, Requester requester, out int result)
    {
        result = 0;
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        if (isSuccess)
        {
            List<MinionData> minionList;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minionList);
            if (isSuccess)
            {
                if (minionList != null)
                {
                    foreach (MinionData minion in minionList)
                    {
                        result += minion.ArmorCurrent;
                    }
                    return true;
                }
            }
        }
        return false;
    }

    private static bool IntGetSave(DataParam param, Requester requester, out int result)
    {
        result = 0;
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.String, out p[0]);
        if (isSuccess)
        {
            string saveKey;
            isSuccess &= StringParser.ParamToString(p[0], requester, out saveKey);
            if (isSuccess)
            {
                int savedValue = 0;
                string realSaveKey = GameManager.Instance.CreateSaveKey(saveKey, requester);
                isSuccess = GameManager.Instance.GetSavedInteger(realSaveKey, out savedValue);
                if (isSuccess)
                {
                    result = savedValue;
                    return true;
                }
            }
        }
        return false;
    }

    private static bool IntBeAuraBuffCount(DataParam param, Requester requester, out int result)
    {
        result = 0;
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        if (isSuccess)
        {
            List<MinionData> minionList;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minionList);
            if (isSuccess)
            {
                if(minionList.Count > 0)
                {
                    MinionData minion = minionList[0];
                    if(minion.IsBeAura)
                    {
                        foreach(MinionBuffData buff in minion.BuffDataList)
                        {
                            if(buff.BuffOrigin == MinionBuffOrigins.AURABUFF)
                            {
                                result++;
                            }
                        }
                    }
                }
                return true;
            }
        }
        return false;
    }
    #endregion
}
