﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SlotParser : DataParser
{
    #region Methods
    public static bool ParamToSlotList(DataParam param, Requester requester, out List<SlotData> result)
    {
        result = null;
        switch (param.Key)
        {
            case "SlotFilterRandom": return SlotFilterRandom(param, requester, out result);
            case "SlotFilterEmpty":  return SlotFilterEmpty(param, requester, out result);
            case "SlotFilterLock":   return SlotFilterLock(param, requester, out result);
            case "SlotFilterLockMine": return SlotFilterLockMine(param, requester, out result);
            case "SlotFilterDarkMatter": return SlotFilterDarkMatter(param, requester, out result);
            case "SlotFilterDarkMatterMine": return SlotFilterDarkMatterMine(param, requester, out result);
            case "SlotFilterNotDarkMatter": return SlotFilterNotDarkMatter(param, requester, out result);
            case "SlotFilterNotDarkMatterMine": return SlotFilterNotDarkMatterMine(param, requester, out result);
            case "SlotAll":          return SlotAll(param, requester, out result);
            case "SlotAllEmpty":     return SlotAllEmpty(param, requester, out result);
            case "SlotAdjacent":     return SlotAdjacent(param, requester, out result);
            case "SlotAdjacent8":    return SlotAdjacent8(param, requester, out result);
            case "SlotAdjacentAlong": return SlotAdjacentAlong(param, requester, out result);
            case "SlotOfMinion":     return SlotOfMinion(param, requester, out result);
            case "SlotAtN":          return SlotAtN(param, requester, out result);
            case "SlotAtNE":         return SlotAtNE(param, requester, out result);
            case "SlotAtE":          return SlotAtE(param, requester, out result);
            case "SlotAtSE":         return SlotAtSE(param, requester, out result);
            case "SlotAtS":          return SlotAtS(param, requester, out result);
            case "SlotAtSW":         return SlotAtSW(param, requester, out result);
            case "SlotAtW":          return SlotAtW(param, requester, out result);
            case "SlotAtNW":         return SlotAtNW(param, requester, out result);
            case "SlotMinionDir":    return SlotMinionDir(param, requester, out result);
            case "SlotMerge":        return SlotMerge(param, requester, out result);
            case "SlotGetSave":      return SlotGetSave(param, requester, out result);
            case "SlotTriggerer":    return SlotTriggerer(param, requester, out result);
            case "SlotInDirection":  return SlotInDirection(param, requester, out result);
            default: Debug.LogError("SlotParser: Fail to parse slot. " + param.Key); return false;
        }
    }
    #endregion

    #region Slot Methods
    private static bool SlotFilterRandom(DataParam param, Requester requester, out List<SlotData> result)
    {
        result = new List<SlotData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.SlotList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(param.Parameters[1], DataParam.ParamType.Integer, out p[1]);
        if (isSuccess)
        {
            List<SlotData> slotList;
            int slotCount;
            isSuccess &= SlotParser.ParamToSlotList(p[0], requester, out slotList);
            isSuccess &= IntParser.ParamToInt(p[1], requester, out slotCount);
            if (isSuccess)
            {
                int selectedIndex;
                for (int i = 0; i < slotCount; i++)
                {
                    if (slotList != null && slotList.Count > 0)
                    {
                        selectedIndex = Random.Range(0, slotList.Count);
                        result.Add(slotList[selectedIndex]);
                        slotList.RemoveAt(selectedIndex);
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool SlotFilterEmpty(DataParam param, Requester requester, out List<SlotData> result)
    {
        result = new List<SlotData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.SlotList, out p[0]);
        if (isSuccess)
        {
            List<SlotData> slotList;
            isSuccess &= SlotParser.ParamToSlotList(p[0], requester, out slotList);
            if (isSuccess)
            {
                foreach(SlotData slot in slotList)
                {
                    if(slot.IsSlotEmpty)
                    {
                        result.Add(slot);
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool SlotFilterLock(DataParam param, Requester requester, out List<SlotData> result)
    {
        result = new List<SlotData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.SlotList, out p[0]);
        if (isSuccess)
        {
            List<SlotData> slotList;
            isSuccess &= SlotParser.ParamToSlotList(p[0], requester, out slotList);
            if (isSuccess)
            {
                foreach (SlotData slot in slotList)
                {
                    if (!slot.IsBeUnlock)
                    {
                        result.Add(slot);
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool SlotFilterLockMine(DataParam param, Requester requester, out List<SlotData> result)
    {
        result = new List<SlotData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.SlotList, out p[0]);
        if (isSuccess)
        {
            List<SlotData> slotList;
            isSuccess &= SlotParser.ParamToSlotList(p[0], requester, out slotList);
            if (isSuccess)
            {
                foreach (SlotData slot in slotList)
                {
                    if (!slot.IsBeUnlock)
                    {
                        foreach(SlotBuffData buff in slot.BuffDataList)
                        {
                            if (buff.BuffType == SlotBuffTypes.BuffSlotLock
                                && buff.Requester.PlayerIndex == requester.PlayerIndex)
                            {
                                result.Add(slot);
                                break;
                            }
                        } 
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool SlotFilterDarkMatter(DataParam param, Requester requester, out List<SlotData> result)
    {
        result = new List<SlotData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.SlotList, out p[0]);
        if (isSuccess)
        {
            List<SlotData> slotList;
            isSuccess &= SlotParser.ParamToSlotList(p[0], requester, out slotList);
            if (isSuccess)
            {
                foreach (SlotData slot in slotList)
                {
                    if (slot.IsBeDarkMatter)
                    {
                        result.Add(slot);
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool SlotFilterDarkMatterMine(DataParam param, Requester requester, out List<SlotData> result)
    {
        result = new List<SlotData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.SlotList, out p[0]);
        if (isSuccess)
        {
            List<SlotData> slotList;
            isSuccess &= SlotParser.ParamToSlotList(p[0], requester, out slotList);
            if (isSuccess)
            {
                foreach (SlotData slot in slotList)
                {
                    if (slot.IsBeDarkMatter)
                    {
                        foreach (SlotBuffData buff in slot.BuffDataList)
                        {
                            if (buff.BuffType == SlotBuffTypes.BuffSlotDarkMatter
                                && buff.Requester.PlayerIndex == requester.PlayerIndex)
                            {
                                result.Add(slot);
                                break;
                            }
                        }
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool SlotFilterNotDarkMatter(DataParam param, Requester requester, out List<SlotData> result)
    {
        result = new List<SlotData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.SlotList, out p[0]);
        if (isSuccess)
        {
            List<SlotData> slotList;
            isSuccess &= SlotParser.ParamToSlotList(p[0], requester, out slotList);
            if (isSuccess)
            {
                foreach (SlotData slot in slotList)
                {
                    if (!slot.IsBeDarkMatter)
                    {
                        result.Add(slot);
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool SlotFilterNotDarkMatterMine(DataParam param, Requester requester, out List<SlotData> result)
    {
        result = new List<SlotData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.SlotList, out p[0]);
        if (isSuccess)
        {
            List<SlotData> slotList;
            isSuccess &= SlotParser.ParamToSlotList(p[0], requester, out slotList);
            if (isSuccess)
            {
                foreach (SlotData slot in slotList)
                {
                    if (slot.IsBeDarkMatter)
                    {
                        foreach (SlotBuffData buff in slot.BuffDataList)
                        {
                            if (buff.BuffType == SlotBuffTypes.BuffSlotDarkMatter
                                && buff.Requester.PlayerIndex != requester.PlayerIndex)
                            {
                                result.Add(slot);
                                break;
                            }
                        }
                    }
                    else
                    {
                        result.Add(slot);
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool SlotAll(DataParam param, Requester requester, out List<SlotData> result)
    {
        result = new List<SlotData>();
        foreach(SlotData slot in GameManager.Instance.Data.SlotDatas)
        {
            result.Add(slot);
        }

        return true;
    }

    private static bool SlotAllEmpty(DataParam param, Requester requester, out List<SlotData> result)
    {
        result = new List<SlotData>();
        foreach (SlotData slot in GameManager.Instance.Data.SlotDatas)
        {
            if (slot.IsSlotEmpty)
            {
                result.Add(slot);
            }
        }

        return true;
    }

    private static bool SlotAdjacent(DataParam param, Requester requester, out List<SlotData> result)
    {
        result = new List<SlotData>();
        bool isSuccess = true;

        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.SlotList, out p[0]);

        if (isSuccess)
        {
            List<SlotData> slots;
            isSuccess &= SlotParser.ParamToSlotList(p[0], requester, out slots);
            if (isSuccess)
            {
                SlotData slotAdjacent;
                foreach (SlotData slot in slots)
                {
                    foreach (CardDirectionType dir in System.Enum.GetValues(typeof(CardDirectionType)))
                    {
                        if(slot.GetAdjacentSlot(dir, out slotAdjacent) && (int)dir%2 == 0)
                        {
                            result.Add(slotAdjacent);
                        }
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool SlotAdjacent8(DataParam param, Requester requester, out List<SlotData> result)
    {
        result = new List<SlotData>();
        bool isSuccess = true;

        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.SlotList, out p[0]);

        if (isSuccess)
        {
            List<SlotData> slots;
            isSuccess &= SlotParser.ParamToSlotList(p[0], requester, out slots);
            if (isSuccess)
            {
                SlotData slotAdjacent;
                foreach (SlotData slot in slots)
                {
                    foreach (CardDirectionType dir in System.Enum.GetValues(typeof(CardDirectionType)))
                    {
                        if (slot.GetAdjacentSlot(dir, out slotAdjacent))
                        {
                            result.Add(slotAdjacent);
                        }
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool SlotAdjacentAlong(DataParam param, Requester requester, out List<SlotData> result)
    {
        result = new List<SlotData>();
        bool isSuccess = true;

        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.SlotList, out p[0]);

        if (isSuccess)
        {
            bool isFound = true;
            SlotData slotTmp;
            List<SlotData> slots;
            isSuccess &= SlotParser.ParamToSlotList(p[0], requester, out slots);
            if (isSuccess)
            {
                SlotData slotAdjacent;
                foreach (SlotData slot in slots)
                {
                    foreach (CardDirectionType dir in System.Enum.GetValues(typeof(CardDirectionType)))
                    {
                        if (slot.GetAdjacentSlot(dir, out slotAdjacent) && (int)dir % 2 == 0)
                        {
                            result.Add(slotAdjacent);

                            isFound = true;
                            while (isFound)
                            {
                                if (slotAdjacent.GetAdjacentSlot(dir, out slotTmp))
                                {
                                    result.Add(slotTmp);
                                    slotAdjacent = slotTmp;
                                }
                                else
                                {
                                    isFound = false;
                                }
                            }
                        }
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool SlotOfMinion(DataParam param, Requester requester, out List<SlotData> result)
    {
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        if (isSuccess)
        {
            List<MinionData> minionList;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minionList);
            if (isSuccess)
            {
                result = new List<SlotData>();
                foreach (MinionData minion in minionList)
                {
                    if (minion.SlotID >= 0)
                    {
                        result.Add(GameManager.Instance.Data.SlotDatas[minion.SlotID]);
                    }
                }

                if (result != null && result.Count > 0)
                {
                    return true;
                }
            }
        }

        result = new List<SlotData>();
        return false;
    }

    private static bool SlotAtN(DataParam param, Requester requester, out List<SlotData> result)
    {
        result = new List<SlotData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.SlotList, out p[0]);
        if (isSuccess)
        {
            List<SlotData> slotList;
            isSuccess &= SlotParser.ParamToSlotList(p[0], requester, out slotList);
            if (isSuccess)
            {
                SlotData nearbySlot = null;
                foreach (SlotData slot in slotList)
                {
                    if (slot.GetAdjacentSlot(CardDirectionType.N, out nearbySlot))
                    {
                        result.Add(nearbySlot);
                    }
                }

                return true;
            }
        }

        return false;
    }

    private static bool SlotAtNE(DataParam param, Requester requester, out List<SlotData> result)
    {
        result = new List<SlotData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.SlotList, out p[0]);
        if (isSuccess)
        {
            List<SlotData> slotList;
            isSuccess &= SlotParser.ParamToSlotList(p[0], requester, out slotList);
            if (isSuccess)
            {
                SlotData nearbySlot = null;
                foreach (SlotData slot in slotList)
                {
                    if (slot.GetAdjacentSlot(CardDirectionType.NE, out nearbySlot))
                    {
                        result.Add(nearbySlot);
                    }
                }

                return true;
            }
        }

        return false;
    }

    private static bool SlotAtE(DataParam param, Requester requester, out List<SlotData> result)
    {
        result = new List<SlotData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.SlotList, out p[0]);
        if (isSuccess)
        {
            List<SlotData> slotList;
            isSuccess &= SlotParser.ParamToSlotList(p[0], requester, out slotList);
            if (isSuccess)
            {
                SlotData nearbySlot = null;
                foreach (SlotData slot in slotList)
                {
                    if (slot.GetAdjacentSlot(CardDirectionType.E, out nearbySlot))
                    {
                        result.Add(nearbySlot);
                    }
                }

                return true;
            }
        }

        return false;
    }

    private static bool SlotAtSE(DataParam param, Requester requester, out List<SlotData> result)
    {
        result = new List<SlotData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.SlotList, out p[0]);
        if (isSuccess)
        {
            List<SlotData> slotList;
            isSuccess &= SlotParser.ParamToSlotList(p[0], requester, out slotList);
            if (isSuccess)
            {
                SlotData nearbySlot = null;
                foreach (SlotData slot in slotList)
                {
                    if (slot.GetAdjacentSlot(CardDirectionType.SE, out nearbySlot))
                    {
                        result.Add(nearbySlot);
                    }
                }

                return true;
            }
        }

        return false;
    }

    private static bool SlotAtS(DataParam param, Requester requester, out List<SlotData> result)
    {
        result = new List<SlotData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.SlotList, out p[0]);
        if (isSuccess)
        {
            List<SlotData> slotList;
            isSuccess &= SlotParser.ParamToSlotList(p[0], requester, out slotList);
            if (isSuccess)
            {
                SlotData nearbySlot = null;
                foreach (SlotData slot in slotList)
                {
                    if (slot.GetAdjacentSlot(CardDirectionType.S, out nearbySlot))
                    {
                        result.Add(nearbySlot);
                    }
                }

                return true;
            }
        }

        return false;
    }

    private static bool SlotAtSW(DataParam param, Requester requester, out List<SlotData> result)
    {
        result = new List<SlotData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.SlotList, out p[0]);
        if (isSuccess)
        {
            List<SlotData> slotList;
            isSuccess &= SlotParser.ParamToSlotList(p[0], requester, out slotList);
            if (isSuccess)
            {
                SlotData nearbySlot = null;
                foreach (SlotData slot in slotList)
                {
                    if (slot.GetAdjacentSlot(CardDirectionType.SW, out nearbySlot))
                    {
                        result.Add(nearbySlot);
                    }
                }

                return true;
            }
        }

        return false;
    }

    private static bool SlotAtW(DataParam param, Requester requester, out List<SlotData> result)
    {
        result = new List<SlotData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.SlotList, out p[0]);
        if (isSuccess)
        {
            List<SlotData> slotList;
            isSuccess &= SlotParser.ParamToSlotList(p[0], requester, out slotList);
            if (isSuccess)
            {
                SlotData nearbySlot = null;
                foreach (SlotData slot in slotList)
                {
                    if (slot.GetAdjacentSlot(CardDirectionType.W, out nearbySlot))
                    {
                        result.Add(nearbySlot);
                    }
                }

                return true;
            }
        }

        return false;
    }

    private static bool SlotAtNW(DataParam param, Requester requester, out List<SlotData> result)
    {
        result = new List<SlotData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.SlotList, out p[0]);
        if (isSuccess)
        {
            List<SlotData> slotList;
            isSuccess &= SlotParser.ParamToSlotList(p[0], requester, out slotList);
            if (isSuccess)
            {
                SlotData nearbySlot = null;
                foreach (SlotData slot in slotList)
                {
                    if (slot.GetAdjacentSlot(CardDirectionType.NW, out nearbySlot))
                    {
                        result.Add(nearbySlot);
                    }
                }

                return true;
            }
        }

        return false;
    }

    private static bool SlotMinionDir(DataParam param, Requester requester, out List<SlotData> result)
    {
        result = new List<SlotData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        if (isSuccess)
        {
            List<MinionData> minionList;
            isSuccess &= MinionParser.ParamToMinionList(p[0], requester, out minionList);
            if (isSuccess)
            {
                SlotData slot;
                foreach(MinionData minion in minionList)
                {
                    foreach (CardDirectionType dir in System.Enum.GetValues(typeof(CardDirectionType)))
                    {
                        if (minion.CardDir.IsDirection(dir) && minion.GetAdjacentSlot(dir, out slot))
                        {
                            result.Add(slot);
                        }
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool SlotMerge(DataParam param, Requester requester, out List<SlotData> result)
    {
        result = new List<SlotData>();
        bool isSuccess = true;
        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.SlotList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(param.Parameters[1], DataParam.ParamType.SlotList, out p[1]);
        if (isSuccess)
        {
            List<SlotData> slotList_1;
            List<SlotData> slotList_2;

            isSuccess &= SlotParser.ParamToSlotList(p[0], requester, out slotList_1);
            isSuccess &= SlotParser.ParamToSlotList(p[1], requester, out slotList_2);
            if (isSuccess)
            {
                result = slotList_1; 

                foreach (SlotData slot in slotList_2)
                {
                    bool isSkip = false;
                    foreach (SlotData s in result)
                    {
                        if (s.SlotID == slot.SlotID)
                        {
                            // already in result
                            isSkip = true;
                            break;
                        }
                    }

                    if (!isSkip)
                    {
                        result.Add(slot);
                    }
                }
                return true;
            }
        }

        return false;
    }

    private static bool SlotGetSave(DataParam param, Requester requester, out List<SlotData> result)
    {
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.String, out p[0]);
        if (isSuccess)
        {
            string saveKey;
            isSuccess &= StringParser.ParamToString(p[0], requester, out saveKey);
            if (isSuccess)
            {
                List<SlotData> slotList;
                string realSaveKey = GameManager.Instance.CreateSaveKey(saveKey, requester);
                GameManager.Instance.GetSavedSlotList(realSaveKey, out slotList);
                if (slotList != null)
                {
                    result = slotList;
                    return true;
                }
            }
        }

        result = new List<SlotData>();
        return false;
    }

    private static bool SlotTriggerer(DataParam param, Requester requester, out List<SlotData> result)
    {
        result = new List<SlotData>();
        if (requester.Ability.Triggerer != null)
        {
            result.Add(requester.Ability.Triggerer.Slot);
            return true;
        }

        return false;
    }

    private static bool SlotInDirection(DataParam param, Requester requester, out List<SlotData> result)
    {
        result = new List<SlotData>();
        bool isSuccess = true;

        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(param.Parameters[0], DataParam.ParamType.SlotList, out p[0]);

        if (isSuccess)
        {
            bool isFound = true;
            SlotData slotTmp;
            List<SlotData> slots;
            isSuccess &= SlotParser.ParamToSlotList(p[0], requester, out slots);

            List<CardDirectionType> cardDirList = new List<CardDirectionType>();
            string directionString;
            CardDirectionType directionType;
            DataParam directionParam = new DataParam();
            for (int index = 1; index < param.Parameters.Count; index++)
            {
                isSuccess &= DataParam.TextToDataParam(param.Parameters[index], DataParam.ParamType.String, out directionParam);
                if (isSuccess)
                {
                    isSuccess &= StringParser.ParamToString(directionParam, requester, out directionString);
                    isSuccess &= GameHelper.StrToEnum(directionString, out directionType);
                    if (isSuccess)
                    {
                        cardDirList.Add(directionType);
                    }
                }
            }

            if (isSuccess)
            {
                SlotData slotAdjacent;
                foreach (SlotData slot in slots)
                {
                    result.Add(slot);
                    foreach (CardDirectionType dir in System.Enum.GetValues(typeof(CardDirectionType)))
                    {
                        if(cardDirList.Contains(dir) == false)
                        {
                            continue;
                        }

                        if (slot.GetAdjacentSlot(dir, out slotAdjacent) && (int)dir % 2 == 0)
                        {
                            result.Add(slotAdjacent);

                            isFound = true;
                            while (isFound)
                            {
                                if (slotAdjacent.GetAdjacentSlot(dir, out slotTmp))
                                {
                                    result.Add(slotTmp);
                                    slotAdjacent = slotTmp;
                                }
                                else
                                {
                                    isFound = false;
                                }
                            }
                        }
                    }
                }
                return true;
            }
        }

        return false;
    }
    #endregion
}