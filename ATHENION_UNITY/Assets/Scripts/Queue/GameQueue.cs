﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

public abstract class QueueData
{
    #region Public Properties
    public int UniqueKey { get { return _uniqueKey; } }
    public QueuePlayMode PlayMode { get { return _playMode; } }
    #endregion

    #region Protected Properties
    protected int _uniqueKey = 0;
    protected QueuePlayMode _playMode;
    protected UnityAction<int> _onComplete;
    #endregion

    #region Methods
    public virtual void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        // Please call this when complete.
        OnActiveComplete();
    }

    public void SetUniqueKey(int uniqueKey)
    {
        _uniqueKey = uniqueKey;
    }

    public void SetPlayMode(QueuePlayMode playMode)
    {
        _playMode = playMode;
    }

    protected virtual void OnActiveComplete()
    {
        if (_onComplete != null)
        {
            _onComplete.Invoke(_uniqueKey);
        }
    }

    public virtual string GetDebugText()
    {
        return "";
    }
    #endregion
}

public abstract class GameQueue<TClass, TData> : MonoSingleton<TClass>  where TClass : MonoSingleton<TClass> where TData : QueueData
{
    #region Public Properties
    public virtual bool IsReady
    {
        get
        {
            bool isReady = true;
            if (_queue != null)
            {
                isReady &= (_queue.Count <= 0);
            }
            if (_active != null)
            {
                isReady &= (_active.Count <= 0);
            }
            isReady &= !IsPause;

            return isReady;
        }
    }
    public bool IsReadyNext
    {
        get
        {
            bool isReady = true;
            if (_active != null)
            {
                isReady &= (_active.Count <= 0);
            }
            isReady &= !IsPause;

            return isReady;
        }
    }
    public bool IsPause { get { return (_isPause > 0); } }
    public int PauseCount { get { return _isPause; } }
    #endregion

    #region Protected Properties
    protected List<TData> _queue = null;
    protected List<TData> _active = null;

    protected int _isPause = 0;
    protected int _uniqueKeyIndex = 0;

    protected StringBuilder _sb;
    #endregion

    #region Action
    public event Action OnActiveCompleteEvent;
    #endregion

    protected virtual void Update()
    {
        if (_queue != null)
        {
            OnUpdate();
        }
    }

    #region Methods
    public virtual void Init()
    {
        _queue = new List<TData>();
        _active = new List<TData>();

        _uniqueKeyIndex = 0;
        _isPause = 0;
    }

    public virtual void Add(TData data)
    {
        if (_queue == null || _active == null)
        {
            this.Init();
        }

        data.SetUniqueKey(_uniqueKeyIndex++);
        data.SetPlayMode(QueuePlayMode.Sequence);
        _queue.Add(data);

        //Debug.Log(string.Format("[{0}] Add {1}"
        //    , this.GetType().ToString()
        //    , data.GetType().ToString()
        //));
    }

    public void AddFirst(TData data)
    {
        if (_queue == null || _active == null)
        {
            this.Init();
        }

        data.SetUniqueKey(_uniqueKeyIndex++);
        data.SetPlayMode(QueuePlayMode.Sequence);
        _queue.Insert(0, data);

        //Debug.Log(string.Format("[{0}] Add Front {1}"
        //    , this.GetType().ToString()
        //    , data.GetType().ToString()
        //));
    }

    public void Join(TData data)
    {
        if (_queue == null || _active == null)
        {
            this.Init();
        }

        data.SetUniqueKey(_uniqueKeyIndex++);
        data.SetPlayMode(QueuePlayMode.Parallel);
        _queue.Add(data);

        //Debug.Log(string.Format("[{0}] Join {1}"
        //    , this.GetType().ToString()
        //    , data.GetType().ToString()
        //));
    }

    public void JoinFirst(TData data)
    {
        if (_queue == null || _active == null)
        {
            this.Init();
        }

        data.SetUniqueKey(_uniqueKeyIndex++);
        data.SetPlayMode(QueuePlayMode.Parallel);
        _queue.Insert(0, data);

        //Debug.Log(string.Format("[{0}] Join Front {1}"
        //    , this.GetType().ToString()
        //    , data.GetType().ToString()
        //));
    }

    public virtual void Clear()
    {
        if(_queue != null) _queue.Clear();
        if (_active != null) _active.Clear();

        _uniqueKeyIndex = 0;
    }

    public TData Last()
    {
        return _queue.Last();
    }

    public void SetPause(bool isPause)
    {
        if (isPause)
        {
            _isPause++;
        }
        else
        {
            _isPause--;
        }

        Debug.LogFormat(
              "GameQueue/SetPause: <color={0}>{1}:{2}</color>"
            , IsPause ? "red" : "green"
            , IsPause
            , _isPause
        );
    }

    public void ClearPauseState()
    {
        _isPause = 0;
    }

    protected virtual void OnUpdate()
    {
        if (!IsPause)
        {
            if (_queue != null && _queue.Count > 0)
            {
                switch (_queue[0].PlayMode)
                {
                    case QueuePlayMode.Parallel:
                    {
                        TData data = _queue[0];
                        _queue.RemoveAt(0);
                        Active(data);
                    }
                    break;

                    case QueuePlayMode.Sequence:
                    default:
                    {
                        if (IsReadyNext)
                        {
                            TData data = _queue[0];
                            _queue.RemoveAt(0);
                            Active(data);
                        }
                    }
                    break;
                }
            }
        }
    }

    protected void Active(TData data)
    {
        if (_active == null)
        {
            Init();
        }

        _active.Add(data);
        data.Active(this.OnActiveComplete);

        //Debug.Log(string.Format("[{0}] Start Active {1}"
        //    , this.GetType().ToString()
        //    , data.GetType().ToString()
        //));
    }

    protected virtual void OnActiveComplete(int uniqueKey)
    {
        OnActiveCompleteEvent?.Invoke();

        if (_active != null && _active.Count > 0)
        {
            for (int index = 0; index < _active.Count; ++index)
            {
                if (_active[index].UniqueKey == uniqueKey)
                {
                    //Debug.Log(string.Format("[{0}] Active Completed {1}"
                    //    , this.GetType().ToString()
                    //    , _active[index].GetType().ToString()
                    //));

                    _active.RemoveAt(index);
                    break;
                }
            }
        }

        //OnUpdate();
    }

    public virtual string GetDebugText()
    {
        if (_sb == null) _sb = new StringBuilder();
        _sb.Clear();

        _sb.AppendFormat(
            "<b>{0} : {1} : {2}</b>\n"
            , this.GetType().ToString()
            , IsReady ? "<color=#00ff00ff>Ready</color>" : "<color=#ff0000ff>Unready</color>"
            , IsPause ? "<color=#ff0000ff>Pause</color>" : "<color=#00ff00ff>Run</color>"
        );

        _sb.Append("Active:");
        if (_active != null && _active.Count > 0)
        {
            foreach (TData data in _active)
            {
                _sb.AppendFormat(" {0}", data.GetDebugText());
            }
        }
        else
        {
            _sb.Append(" Empty.");
        }

        _sb.Append("\nQueue:");
        if (_queue != null && _queue.Count > 0)
        {
            foreach (TData data in _queue)
            {
                _sb.AppendFormat(" <b>[{0}]</b>{1}"
                    , data.PlayMode == QueuePlayMode.Parallel ? "J" : "A"
                    , data.GetDebugText()
                );
            }
        }
        else
        {
            _sb.Append(" Empty.");
        }

        return _sb.ToString();
    }
    #endregion
}
