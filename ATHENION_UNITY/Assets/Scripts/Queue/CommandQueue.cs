﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

#region Command Queue Class
public class CommandQueue : GameQueue<CommandQueue, CommandData>
{
    public override bool IsReady
    {
        get
        {
            bool isReady = base.IsReady;

            if (_secondQueue != null)
            {
                isReady &= (_secondQueue.Count <= 0);
            }

            return isReady;
        }
    }

    public bool IsReady2ndNext
    {
        get
        {
            return base.IsReady;
        }
    }

    private List<List<CommandData>> _secondQueue;

    public override void Init()
    {
        _secondQueue = new List<List<CommandData>>();

        base.Init();
    }

    // Average execution time for one OnUpdate()
    private const float AVG_COMMAND_EXECUTE_TIME = 0.0015f;
    private const int MAX_EXECUTE_TIME_PER_UPDATE = 23;
    protected override void Update()
    {
        // execution amount per frame should be between 1 - 23 (optimizable)
        int count = Mathf.Clamp(Mathf.CeilToInt(Time.deltaTime / AVG_COMMAND_EXECUTE_TIME), 1, MAX_EXECUTE_TIME_PER_UPDATE);

        for (int i = 0; i < count; i++)
        {
            if ((_queue != null && _queue.Count > 0)
            || (_secondQueue != null && _secondQueue.Count > 0)
            || (_active != null && _active.Count > 0))
            {
                OnUpdate();
            }
            else
            {
                return;
            }
        }
    }

    protected override void OnUpdate()
    {
        if (!IsPause)
        {
            if (_queue != null && _queue.Count > 0)
            {
                switch (_queue[0].PlayMode)
                {
                    case QueuePlayMode.Parallel:
                    {
                        CommandData data = _queue[0];
                        _queue.RemoveAt(0);
                        Active(data);
                    }
                    break;

                    case QueuePlayMode.Sequence:
                    default:
                    {
                        if (IsReadyNext)
                        {
                            CommandData data = _queue[0];
                            _queue.RemoveAt(0);
                            Active(data);
                        }
                    }
                    break;
                }
            }
            else
            {
                if (_active == null || _active.Count <= 0)
                {
                    // queue is empty
                    if (_secondQueue != null && _secondQueue.Count > 0)
                    {
                        if (_secondQueue[0].Count > 0)
                        {
                            List<CommandData> list = _secondQueue[0];

                            for (int index = 0; index < list.Count; ++index)
                            {
                                if (index == 0 || list[index].PlayMode == QueuePlayMode.Parallel)
                                {
                                    CommandData command = list[0];
                                    list.RemoveAt(0);

                                    _queue.Add(command);
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }
                        else
                        {
                            // remove empty queue.
                            _secondQueue.RemoveAt(0);
                        }
                        OnUpdate();
                    }
                }
            }
        }
    }

    public void AddFirstSecondQueue(List<CommandData> commandList)
    {
        if (_secondQueue == null)
        {
            _secondQueue = new List<List<CommandData>>();
        }

        _secondQueue.Insert(0, commandList);
    }

    public void AddSecondQueue(List<CommandData> commandList)
    {
        if (_secondQueue == null)
        {
            _secondQueue = new List<List<CommandData>>();
        }

        _secondQueue.Add(commandList);
    }

    public override void Clear()
    {
        if (_secondQueue != null) _secondQueue.Clear();

        base.Clear();
    }

    public override string GetDebugText()
    {
        string text = base.GetDebugText();

        if (_sb == null) _sb = new StringBuilder();
        _sb.Clear();

        _sb.Append("\n2nd nQueue:");
        if (_secondQueue != null && _secondQueue.Count > 0)
        {
            int i = 0;
            int index = 0;
            foreach (List<CommandData> list in _secondQueue)
            {
                _sb.AppendFormat(" <b>L[{0}]</b> ", index);
                if (list.Count > 0)
                {
                    foreach (CommandData command in list)
                    {
                        _sb.AppendFormat(" <b>[{0}]</b>{1}"
                            , command.PlayMode == QueuePlayMode.Parallel ? "J" : "A"
                            , command.GetDebugText()
                        );
                        ++i;
                    }
                }
                else
                {
                    _sb.AppendFormat("<b>Empty</b>");
                }

                index++;

                if (i > 10)
                {
                    _sb.AppendFormat(" [and more....]");
                    break;
                }
            }
        }
        else
        {
            _sb.Append(" Empty.");
        }

        return text + _sb.ToString();
    }
}
#endregion

#region Command Data 
public abstract class CommandData : QueueData
{
    #region Constructors
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        // Please call this when complete.
        OnActiveComplete();
    }

    public override string GetDebugText()
    {
        return string.Format("[{0}]{1}", _uniqueKey, this.GetType().ToString());
    }
    #endregion
}

public class CallbackCommand : CommandData
{
    #region Private Properties
    private UnityAction _callback;
    #endregion

    #region Constructors
    public CallbackCommand(UnityAction callback)
    {
        _callback = callback;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;
        if (_callback != null)
        {
            _callback.Invoke();
        }
        OnActiveComplete();
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]CallbackCommand_{1}"
            , _uniqueKey
            , this.GetType().ToString()
        );
    }
    #endregion
}

public class TriggerMinionCommand : CommandData
{
    #region Private Properties
    private MinionData _minion;
    private EventKey _key;
    private Requester _triggerer;
    #endregion

    #region Constructors
    public TriggerMinionCommand(MinionData minion, EventKey key, Requester triggerer)
    {
        _minion = minion;
        _key = key;
        _triggerer = triggerer;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        _minion.TriggerEvent(_key, _triggerer);

        OnActiveComplete();
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]TriggerMinionCommand_{1}_{2}"
            , _uniqueKey
            , _minion.UniqueID
            , _key
        );
    }
    #endregion
}

public class TriggerSlotCommand : CommandData
{
    #region Private Properties
    private SlotData _slot;
    private EventKey _key;
    private Requester _triggerer;
    #endregion

    #region Constructors
    public TriggerSlotCommand(SlotData slot, EventKey key, Requester triggerer)
    {
        _slot = slot;
        _key = key;
        _triggerer = triggerer;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        _slot.TriggerEvent(_key, _triggerer);

        OnActiveComplete();
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]TriggerSlotCommand_{1}_{2}"
            , _uniqueKey
            , _slot.SlotID
            , _key
        );
    }
    #endregion
}

public class BattleStartAttackCommand : CommandData
{
    #region Private Properties
    private int _atkUniqueID;
    private int _defUniqueID;
    private List<int> _piercingUniqueIDList = new List<int>();
    #endregion

    #region Constructors
    public BattleStartAttackCommand(int attackerUniqueID, int defenderUniqueID, List<int> piercingUniqueIDList)
    {
        _atkUniqueID = attackerUniqueID;
        _defUniqueID = defenderUniqueID;
        _piercingUniqueIDList = new List<int>(piercingUniqueIDList);
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;
        BattleManager.Instance.ActionBattleStartAttack(_atkUniqueID, _defUniqueID, _piercingUniqueIDList, this.OnActiveComplete);
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]StartAttackCommand_{1}_{2}"
            , _uniqueKey
            , _atkUniqueID
            , _defUniqueID
        );
    }
    #endregion
}

public class BattleAttackCommand : CommandData
{
    #region Private Properties
    private int _atkUniqueID;
    private int _defUniqueID;
    private List<int> _piercingUniqueIDList = new List<int>();
    #endregion

    #region Constructors
    public BattleAttackCommand(int attackerUniqueID, int defenderUniqueID, List<int> piercingUniqueIDList)
    {
        _atkUniqueID = attackerUniqueID;
        _defUniqueID = defenderUniqueID;
        _piercingUniqueIDList = new List<int>(piercingUniqueIDList);
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;
        BattleManager.Instance.ActionBattleAttack(_atkUniqueID, _defUniqueID, _piercingUniqueIDList, this.OnActiveComplete);
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]AttackingCommand_{1}_{2}"
            , _uniqueKey
            , _atkUniqueID
            , _defUniqueID
        );
    }
    #endregion
}

public class RandomPlayOrderCommand : CommandData
{
    #region Private Properties
    #endregion

    #region Constructors
    public RandomPlayOrderCommand()
    {
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        GameManager.Instance.ActionRandomPlayOrder(OnActiveComplete);
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]RandomPlayOrder"
            , _uniqueKey
        );
    }
    #endregion
}

public class ReHandCommand : CommandData
{
    #region Private Properties
    private PlayerIndex _playerIndex;
    private List<int> _tempIDList;
    private List<int> _uniqueIDList;
    private int _redrawCount;
    #endregion

    #region Constructors
    public ReHandCommand(PlayerIndex playerIndex, List<int> uniqueIDList)
    {
        _playerIndex = playerIndex;
        _uniqueIDList = new List<int>(uniqueIDList);
        _tempIDList = new List<int>(uniqueIDList);
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        if (_tempIDList != null && _tempIDList.Count > 0)
        {
            _redrawCount = _tempIDList.Count;
            OnReHand();
        }
        else
        {
            _redrawCount = 0;
            OnReHandComplete();
        }
    }

    private void OnReHand()
    {
        // Put back to deck
        GameManager.Instance.ActionMoveCardToZone(
              _tempIDList[0]
            , CardZone.Hand
            , CardZone.Deck
            , -1
            , OnReHandComplete
        );
    }

    private void OnReHandComplete()
    {
        _tempIDList.RemoveAt(0);
        if (_tempIDList != null && _tempIDList.Count > 0)
        {
            OnReHand();
        }
        else
        {
            // Add action log.
            {
                List<BattleCardData> mulliganList;
                if (GameManager.Instance.FindBattleCard(_uniqueIDList, out mulliganList))
                {
                    MulliganLog log = new MulliganLog(_playerIndex, mulliganList);
                    GameManager.Instance.ActionAddBattleLog(log);
                }
            }

            OnActiveComplete();
        }
    }

    public override string GetDebugText()
    {
        string idText = "";
        foreach (int uniqueID in _uniqueIDList)
        {
            if (idText.Length > 0)
            {
                idText += ", " + uniqueID;
            }
            else
            {
                idText += uniqueID;
            }
        }

        return string.Format(
              "[{0}]ReHand_{1}_{2}"
            , _uniqueKey
            , _playerIndex
            , idText
        );
    }
    #endregion
}

public class ShuffleDeckCommand : CommandData
{
    #region Private Properties
    private PlayerIndex _playerIndex;
    #endregion

    #region Constructors
    public ShuffleDeckCommand(PlayerIndex playerIndex)
    {
        _playerIndex = playerIndex;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;
        OnShuffle();

    }

    private void OnShuffle()
    {
        // Put back to deck
        GameManager.Instance.ActionShuffleDeck(
              _playerIndex
            , OnShuffleComplete
        );
    }

    private void OnShuffleComplete()
    {
        OnActiveComplete();
    }

    public override string GetDebugText()
    {
        return string.Format(
              "ShuffleDeck_{0}"
            , _playerIndex
        );
    }
    #endregion
}

public class DrawCardCommand : CommandData
{
    #region Private Properties
    private PlayerIndex _playerIndex;
    private int _num = 0;
    private int _complete = 0;
    private bool _isUseAP;
    private List<BattleCardData> _cardList;
    #endregion

    #region Constructors
    public DrawCardCommand(PlayerIndex playerIndex, int num, bool isUseAP)
    {
        _playerIndex = playerIndex;
        _num = num;
        _isUseAP = isUseAP;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        _cardList = new List<BattleCardData>();
        if (_num > 0)
        {
            int temp = _num;
            _complete = 0;

            while (temp > 0)
            {
                BattleCardData battleCard = GameManager.Instance.Data.PlayerDataList[(int)_playerIndex].CardList[CardZone.Deck][0];
                _cardList.Add(battleCard);
                GameManager.Instance.ActionDrawCard(_playerIndex, OnDrawComplete);
                --temp;
            }
        }
        else
        {
            OnActiveComplete();
        }
    }

    private void OnDrawComplete()
    {
        ++_complete;

        if (_complete == _num)
        {
            if (_cardList != null && _cardList.Count > 0)
            {
                DrawLog log = new DrawLog(_playerIndex, _cardList);
                GameManager.Instance.ActionAddBattleLog(log);
            }

            OnActiveComplete();
        }
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]DrawCard_{1}_{2}"
            , _uniqueKey
            , _playerIndex
            , _num
        );
    }
    #endregion
}

public class DiscardDeckCommand : CommandData
{
    #region Private Properties
    private PlayerIndex _playerIndex;
    private int _num = 0;
    #endregion

    #region Constructors
    public DiscardDeckCommand(PlayerIndex playerIndex, int num)
    {
        _playerIndex = playerIndex;
        _num = num;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        if (_num > 0)
        {
            StartDiscardDeck();
        }
        else
        {
            OnActiveComplete();
        }
    }

    private void StartDiscardDeck()
    {
        GameManager.Instance.ActionDiscardDeck(_playerIndex, OnDiscardComplete);
    }

    private void OnDiscardComplete()
    {
        --_num;
        if (_num > 0)
        {
            StartDiscardDeck();
        }
        else
        {
            OnActiveComplete();
        }
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]DiscardDeck_{1}_{2}"
            , _uniqueKey
            , _playerIndex
            , _num
        );
    }
    #endregion
}

public class DiscardHandCommand : CommandData
{
    #region Private Properties
    private PlayerIndex _playerIndex;
    private List<int> _uniqueCardIDList = new List<int>();
    #endregion

    #region Constructors
    public DiscardHandCommand(PlayerIndex playerIndex, List<int> uniqueCardIDList)
    {
        _playerIndex = playerIndex;
        _uniqueCardIDList = new List<int>(uniqueCardIDList);
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        if (_uniqueCardIDList.Count > 0)
        {
            StartDiscardHand();
        }
        else
        {
            OnActiveComplete();
        }
    }

    private void StartDiscardHand()
    {
        GameManager.Instance.ActionDiscardHand(_playerIndex, _uniqueCardIDList[0], OnDiscardComplete);
    }

    private void OnDiscardComplete()
    {
        _uniqueCardIDList.RemoveAt(0);
        if (_uniqueCardIDList.Count > 0)
        {
            StartDiscardHand();
        }
        else
        {
            OnActiveComplete();
        }
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]DiscardHand_{1}_{2}"
            , _uniqueKey
            , _playerIndex
            , _uniqueCardIDList.Count
        );
    }
    #endregion
}

public class BoostPlayerAPCommand : CommandData
{
    #region Enums
    public enum PerformStep
    {
        Start = 0
        , Moving
        , Arrived
    }
    #endregion

    #region Private Properties
    private Requester _requester;
    private PlayerIndex _playerIndex;
    private int _value;

    private DataParam _vfxParam;

    private PerformTypes _performType;
    private Dictionary<PerformStep, string> _vfxKeys;
    #endregion

    #region Constructors
    public BoostPlayerAPCommand(Requester requester, PlayerIndex player, int value, DataParam vfxParam)
    {
        _playerIndex = player;
        _value = value;
        _requester = requester;
        _vfxParam = vfxParam;
        _vfxKeys = new Dictionary<PerformStep, string>();
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        bool isSuccess = true;
        if (_vfxParam != null)
        {
            isSuccess &= GameHelper.StrToEnum<PerformTypes>(_vfxParam.Parameters[0], out _performType);
            _vfxKeys.Add(PerformStep.Start, _vfxParam.Parameters[1]);
            _vfxKeys.Add(PerformStep.Moving, _vfxParam.Parameters[2]);
            _vfxKeys.Add(PerformStep.Arrived, _vfxParam.Parameters[3]);
        }
        else
        {
            isSuccess = false;
        }

        if (!isSuccess)
        {
            Debug.LogWarningFormat(
                  "BoostPlayerAPCommand: Failed to convert PerformType. Will use default effect. {0}"
                , _vfxParam.Parameters[0]
            );

            _performType = PerformTypes.Single;
            _vfxKeys[PerformStep.Start] = "-";
            _vfxKeys[PerformStep.Moving] = "VFX_DMG_0";
            _vfxKeys[PerformStep.Arrived] = "-";
        }

        switch (_performType)
        {
            case PerformTypes.Single:
                {
                    PlayStartVFX(StartDoSingle);
                }
                break;

            default:
                {
                    OnActiveComplete();
                }
                break;
        }
        return;
    }

    private void PlayStartVFX(UnityAction onComplete)
    {
        if (_vfxKeys[PerformStep.Start] != "-")
        {
            CardUIData cardUIData = new CardUIData(_requester.Minion);
            Vector3 position = Karamucho.UI.BoardManager.Instance.GetSlotHolder(cardUIData.SlotID).GetHolderPosition();
            UIManager.Instance.RequestDealDamageGroup(position, _vfxKeys[PerformStep.Start]);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]BoostPlayerAP_{1}_{2}"
            , _uniqueKey
            , _value
            , _playerIndex
        );
    }
    #endregion

    #region Single Mode
    private void StartDoSingle()
    {
        OnDoSingle(_playerIndex);
    }

    private void OnDoSingle(PlayerIndex player)
    {
        GameManager.Instance.ActionBoostAPPlayer(
              _requester
            , player
            , _value
            , _vfxKeys[PerformStep.Moving]
            , _vfxKeys[PerformStep.Arrived]
            , this.OnActiveComplete
        );
    }
    #endregion
}

public class AddPlayerAPCommand : CommandData
{
    #region Private Properties
    private PlayerIndex _playerIndex;
    private int _num = 0;
    #endregion

    #region Constructors
    public AddPlayerAPCommand(PlayerIndex playerIndex, int num)
    {
        _playerIndex = playerIndex;
        _num = num;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;
        GameManager.Instance.ActionAddPlayerAP(_playerIndex, _num, OnActiveComplete);
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]AddPlayerAP_{1}_{2}"
            , _uniqueKey
            , _playerIndex
            , _num
        );
    }
    #endregion
}

public class SetPlayerAPCommand : CommandData
{
    #region Private Properties
    private PlayerIndex _playerIndex;
    private int _num = 0;
    #endregion

    #region Constructors
    public SetPlayerAPCommand(PlayerIndex playerIndex, int num)
    {
        _playerIndex = playerIndex;
        _num = num;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;
        GameManager.Instance.ActionSetPlayerAP(_playerIndex, _num, OnActiveComplete);
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]SetPlayerAP_{1}_{2}"
            , _uniqueKey
            , _playerIndex
            , _num
        );
    }
    #endregion
}

public class EndTurnCommand : CommandData
{
    #region Private Properties
    private PlayerIndex _playerIndex;
    private int _subTurn;
    private bool _isActive;
    #endregion

    #region Constructors
    public EndTurnCommand(PlayerIndex playerIndex, int subTurn, bool isActive)
    {
        _playerIndex = playerIndex;
        _subTurn = subTurn;
        _isActive = isActive;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        GameManager.Instance.ActionEndTurn(_playerIndex, _subTurn, _isActive);

        OnActiveComplete();
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]EndTurnCommand_{1}_{2}"
            , _uniqueKey
            , _playerIndex
            , _subTurn
        );
    }
    #endregion
}

public class SpawnTokenCommand : CommandData
{
    #region Private Properties
    private MinionData _minion;
    private int _slotID;
    private bool _isAwaken;
    #endregion

    #region Constructors
    public SpawnTokenCommand(MinionData minion, int slotID, bool isAwaken)
    {
        _minion = minion;
        _slotID = slotID;
        _isAwaken = isAwaken;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        SpawnTokenLog log = new SpawnTokenLog(_minion.Owner, _minion, _slotID);
        GameManager.Instance.ActionAddBattleLog(log);

        _onComplete = onComplete;

        try
        {
            if (_minion.CurrentZone != CardZone.Battlefield)
            {
                GameManager.Instance.ActionUpdateMinionActiveZone(_minion.UniqueID, CardZone.Battlefield, null);
                GameManager.Instance.ActionMoveCardToZone(
                      _minion.UniqueID
                    , _minion.CurrentZone
                    , CardZone.Battlefield
                    , _slotID
                    , delegate ()
                    {
                        List<EventTriggerData> triggerList = new List<EventTriggerData>();

                        // Trigger analytic
                        GameManager.Instance.AnalyticTriggerCardPlay(_minion);

                        Requester triggerer = new Requester(_minion);
                        // Trigger OnEnter Event
                        if (_isAwaken)
                        {
                            triggerList.Add(new EventTriggerData(EventKey.Awaken, triggerer));

                            // Count PlayerStat
                            PlayerStatManager.Instance.DoUseCardEvent(_minion, null);

                            // Chain event
                            EventKey chainEventKey;
                            for (int i = 1; i <= GameManager.Instance.PlayCardCount; ++i)
                            {
                                string chainEventStr = string.Format("Chain_{0}", i);
                                if (GameHelper.StrToEnum(chainEventStr, out chainEventKey))
                                {
                                    triggerList.Add(new EventTriggerData(chainEventKey, triggerer));
                                }
                            }
                        }
                        else
                        {
                            // Count PlayerStat
                            PlayerStatManager.Instance.DoSummonCardEvent(_minion, null);
                        }

                        triggerList.Add(new EventTriggerData(EventKey.OnEnter, triggerer));
                        triggerList.Add(new EventTriggerData(EventKey.OnMinionSummoned, triggerer));
                        triggerList.Add(new EventTriggerData(EventKey.Rally, triggerer));
                        if (_isAwaken)
                        {
                            triggerList.Add(new EventTriggerData(EventKey.OnFriendAwaken, triggerer));
                        }

                        triggerList.Add(new EventTriggerData(EventKey.Sentinel, triggerer));
                        if (_isAwaken)
                        {
                            triggerList.Add(new EventTriggerData(EventKey.OnEnemyAwaken, triggerer));
                        }

                        GameManager.Instance.ActionAddCommandListToSecondQueue(
                              EventTrigger.TriggerCommandList(triggerList)
                            , true
                        );

                        if (_isAwaken)
                        {
                            GameManager.Instance.AddPlayCardCount(1);
                        }

                        OnActiveComplete();
                    }
                );
            }
            else
            {
                OnActiveComplete();
            }
        }
        catch (System.Exception e)
        {
            if (GameHelper.IsATNVersion_RealDevelopment)
            {
                PopupUIManager.Instance.ShowPopup_Error("Error", e.Message);
            }

            OnActiveComplete();
        }
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]PlayerSpawnToken_{1}"
            , _uniqueKey
            , _minion.UniqueID
        );
    }
    #endregion
}

public class CreateTokenCommand : CommandData
{
    #region Private Properties
    private string _cardID;
    private PlayerIndex _owner;
    private int _slotID;
    private bool _isAwaken;
    #endregion

    #region Constructors
    public CreateTokenCommand(string cardID, PlayerIndex owner, int slotID, bool isAwaken)
    {
        _cardID = cardID;
        _owner = owner;
        _slotID = slotID;
        _isAwaken = isAwaken;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;
        List<EventTriggerData> triggerList = new List<EventTriggerData>();

        try
        {
            GameManager.Instance.ActionCreateCardToZone(
                  _cardID
                , _owner
                , CardZone.Battlefield
                , _slotID
                , delegate (int uniqueID)
                {
                    MinionData minion;
                    if (GameManager.Instance.FindMinionBattleCard(uniqueID, out minion))
                    {
                        Requester triggerer = new Requester(minion);

                        // trigger analytic
                        GameManager.Instance.AnalyticTriggerCardSummon(minion);

                        GameManager.Instance.ActionUpdateMinionActiveZone(minion.UniqueID, CardZone.Battlefield, null);

                        if (_isAwaken)
                        {
                            triggerList.Add(new EventTriggerData(EventKey.Awaken, triggerer));

                            // Count PlayerStat
                            PlayerStatManager.Instance.DoUseCardEvent(minion, null);

                            // Chain event
                            EventKey chainEventKey;
                            for (int i = 1; i <= GameManager.Instance.PlayCardCount; ++i)
                            {
                                string chainEventStr = string.Format("Chain_{0}", i);
                                if (GameHelper.StrToEnum(chainEventStr, out chainEventKey))
                                {
                                    triggerList.Add(new EventTriggerData(chainEventKey, triggerer));
                                }
                            }
                        }
                        else
                        {
                            // Count PlayerStat
                            PlayerStatManager.Instance.DoSummonCardEvent(minion, null);
                        }

                        triggerList.Add(new EventTriggerData(EventKey.OnEnter, triggerer));
                        triggerList.Add(new EventTriggerData(EventKey.OnMinionSummoned, triggerer));
                        triggerList.Add(new EventTriggerData(EventKey.Rally, triggerer));
                        triggerList.Add(new EventTriggerData(EventKey.Sentinel, triggerer));

                        GameManager.Instance.ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), false);

                        if (_isAwaken)
                        {
                            GameManager.Instance.AddPlayCardCount(1);
                        }
                    }

                    OnActiveComplete();
                }
            );
        }
        catch (System.Exception e)
        {
            if (GameHelper.IsATNVersion_RealDevelopment)
            {
                PopupUIManager.Instance.ShowPopup_Error("Error", e.Message);
            }

            OnActiveComplete();
        }
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]CreateToken_{1}_{2}"
            , _uniqueKey
            , _cardID
            , _owner
        );
    }
    #endregion
}

public class MoveTokenCommand : CommandData
{
    #region Private Properties
    private int _uniqueID;
    private int _targetSlotID;
    private Requester _requester;
    #endregion

    #region Constructors
    public MoveTokenCommand(Requester requester, int uniqueID, int targetSlotID)
    {
        _uniqueID = uniqueID;
        _targetSlotID = targetSlotID;
        _requester = requester;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        GameManager.Instance.ActionMoveMinionToSlot(
              _uniqueID
            , _targetSlotID
            , delegate ()
            {
                MinionData minion;
                if (GameManager.Instance.FindMinionBattleCard(_uniqueID, out minion))
                {
                    //Debug.Log("Minion Moved Trigger");
                    Requester triggerer = new Requester(minion);

                    // Trigger Moved Event
                    List<EventTriggerData> triggerList = new List<EventTriggerData>();
                    triggerList.Add(new EventTriggerData(EventKey.OnMeMoved, triggerer));
                    triggerList.Add(new EventTriggerData(EventKey.OnFriendMoved, triggerer));
                    triggerList.Add(new EventTriggerData(EventKey.OnEnemyMoved, triggerer));
                    GameManager.Instance.ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);

                    // Count PlayerStat
                    PlayerStatManager.Instance.DoActMoveMinionToSlotEvent(_requester.Minion, minion, null);
                }

                OnActiveComplete();
            }
            , false
        );
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]MoveTokenCommand{1}_{2}"
            , _uniqueKey
            , _uniqueID
            , _targetSlotID
        );
    }
    #endregion
}

public class UpdateCardActiveZoneCommand : CommandData
{
    #region Private Properties
    private int _uniqueID;
    private CardZone _activeZone;
    #endregion

    #region Constructors
    public UpdateCardActiveZoneCommand(int uniqueID, CardZone activeZone)
    {
        _uniqueID = uniqueID;
        _activeZone = activeZone;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        BattleCardData battleCard;
        bool isSuccess = GameManager.Instance.FindBattleCard(_uniqueID, out battleCard);

        if (isSuccess && battleCard.ActiveZone != _activeZone)
        {
            GameManager.Instance.ActionUpdateMinionActiveZone(
                  _uniqueID
                , _activeZone
                , delegate ()
                {
                    this.OnUpdateMinionActiveZoneComplete();
                }
            );
        }
        else
        {
            this.OnActiveComplete();
        }
    }

    private void OnUpdateMinionActiveZoneComplete()
    {
        this.OnActiveComplete();
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]UpdateCardActiveZone_{1}_{2}"
            , _uniqueKey
            , _uniqueID
            , _activeZone
        );
    }
    #endregion
}

public class MoveCardToZoneCommand : CommandData
{
    #region Private Properties
    private int _uniqueID;
    private CardZone _startZone;
    private CardZone _destinationZone;
    private int _slotID;
    private UnityAction _onMoveCardToZoneComplete;
    #endregion

    #region Constructors
    public MoveCardToZoneCommand(int uniqueID, CardZone startZone, CardZone destinationZone, int slotID, UnityAction onComplete)
    {
        _uniqueID = uniqueID;
        _startZone = startZone;
        _destinationZone = destinationZone;
        _slotID = slotID;
        _onMoveCardToZoneComplete = onComplete;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        BattleCardData battleCard;
        bool isSuccess = GameManager.Instance.FindBattleCard(_uniqueID, out battleCard);

        if (isSuccess && battleCard.CurrentZone != _destinationZone)
        {
            GameManager.Instance.ActionMoveCardToZone(
                  _uniqueID
                , _startZone
                , _destinationZone
                , _slotID
                , OnMoveCardToZoneComplete
            );
        }
        else
        {
            OnMoveCardToZoneComplete();
        }
    }

    private void OnMoveCardToZoneComplete()
    {
        if (_onMoveCardToZoneComplete != null)
        {
            _onMoveCardToZoneComplete.Invoke();
        }

        OnActiveComplete();
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]MoveCardToZone_{1}_{2}_{3}"
            , _uniqueKey
            , _uniqueID
            , _destinationZone
            , _slotID
        );
    }
    #endregion
}

public class CreateCardToZoneCommand : CommandData
{
    #region Private Properties
    private string _cardID;
    private PlayerIndex _owner;
    private CardZone _destination;
    private int _slotID;
    private UnityAction<int> _onCreateCardToZoneComplete;
    #endregion

    #region Constructors
    public CreateCardToZoneCommand(string cardID, PlayerIndex owner, CardZone destination, int slotID, UnityAction<int> onComplete)
    {
        _cardID = cardID;
        _owner = owner;
        _destination = destination;
        _slotID = slotID;
        _onCreateCardToZoneComplete = onComplete;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        GameManager.Instance.ActionCreateCardToZone(
              _cardID
            , _owner
            , _destination
            , _slotID
            , OnCreateCardToZoneComplete
        );
    }

    private void OnCreateCardToZoneComplete(int uniqueID)
    {
        if (_onCreateCardToZoneComplete != null)
        {
            _onCreateCardToZoneComplete.Invoke(uniqueID);
        }

        OnActiveComplete();
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]CreateCardToZone_{1}_{2}_{3}_{4}"
            , _uniqueKey
            , _cardID
            , _owner
            , _destination
            , _slotID
        );
    }
    #endregion
}

public class RemoveDeckCommand : CommandData
{
    #region Private Properties
    private PlayerIndex _playerIndex;
    private UnityAction _onRemoveDeckComplete;

    private List<int> _uniqueIDList;
    private int _removedCount;
    #endregion

    #region Constructors
    public RemoveDeckCommand(PlayerIndex playerIndex, UnityAction onComplete)
    {
        _playerIndex = playerIndex;
        _onRemoveDeckComplete = onComplete;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;
        _uniqueIDList = new List<int>();
        _removedCount = 0;

        List<BattleCardData> deck = GameManager.Instance.Data.PlayerDataList[(int)_playerIndex].CardList[CardZone.Deck];

        if (deck != null && deck.Count > 0)
        {
            foreach (BattleCardData card in deck)
            {
                _uniqueIDList.Add(card.UniqueID);
            }

            foreach (int uniqueID in _uniqueIDList)
            {
                GameManager.Instance.ActionMoveCardToZone(
                      uniqueID
                    , CardZone.Deck
                    , CardZone.Remove
                    , -1
                    , OnRemoveComplete
                );
            }
        }
        else
        {
            OnRemoveComplete();
        }
    }

    private void OnRemoveComplete()
    {
        _removedCount++;

        if (_removedCount == _uniqueIDList.Count || _uniqueIDList.Count == 0)
        {
            if (_onRemoveDeckComplete != null)
            {
                _onRemoveDeckComplete.Invoke();
            }

            OnActiveComplete();
        }
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]RemoveDeck_{1}"
            , _uniqueKey
            , _playerIndex
        );
    }
    #endregion
}

public class DeadBattleCardCommand : CommandData
{
    #region Private Properties
    private List<int> _uniqueIDList;
    private bool _isTriggerEvent;
    private MinionDeathTypes _deathType;
    #endregion

    #region Constructors
    public DeadBattleCardCommand(List<int> uniqueIDList, bool isTriggerEvent, MinionDeathTypes deathType)
    {
        _uniqueIDList = new List<int>(uniqueIDList);
        _isTriggerEvent = isTriggerEvent;
        _deathType = deathType;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        GameManager.Instance.ActionDeadMinion(_uniqueIDList, _isTriggerEvent, _deathType, OnActiveComplete);
    }

    public override string GetDebugText()
    {
        string uniqueIDList = "";
        foreach (int id in _uniqueIDList)
        {
            if (uniqueIDList != "")
            {
                uniqueIDList += ",";
            }
            uniqueIDList += id.ToString();
        }
        return string.Format(
              "[{0}]DeadBattleCard_{1}_{2}"
            , _uniqueKey
            , uniqueIDList
            , _deathType.ToString()
        );
    }
    #endregion
}

public class SpiritDamageCommand : CommandData
{
    #region Private Properties
    private PlayerIndex _playerIndex;
    private int _spiritDamage;
    #endregion

    #region Constructors
    public SpiritDamageCommand(PlayerIndex playerIndex, int spiritDamage)
    {
        _playerIndex = playerIndex;
        _spiritDamage = spiritDamage;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        // Add battle log.
        {
            SpiritDamageLog log = new SpiritDamageLog(_playerIndex, _spiritDamage);
            GameManager.Instance.ActionAddBattleLog(log);
        }

        GameManager.Instance.ActionSpiritDamage(_playerIndex, _spiritDamage, OnActiveComplete);
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]SpiritDamage_{1}_{2}"
            , _uniqueKey
            , _playerIndex
            , _spiritDamage
        );
    }
    #endregion
}

public class SpiritDamageTutorialCommand : CommandData
{
    #region Private Properties
    private PlayerIndex _playerIndex;
    private int _spiritDamage;
    #endregion

    #region Constructors
    public SpiritDamageTutorialCommand(PlayerIndex playerIndex, int spiritDamage)
    {
        _playerIndex = playerIndex;
        _spiritDamage = spiritDamage;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        // Add battle log.
        {
            SpiritDamageLog log = new SpiritDamageLog(_playerIndex, _spiritDamage);
            GameManager.Instance.ActionAddBattleLog(log);
        }

        GameManager.Instance.ActionSpiritDamageTutorial(_playerIndex, _spiritDamage, OnActiveComplete);
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]SpiritDamage_{1}_{2}"
            , _uniqueKey
            , _playerIndex
            , _spiritDamage
        );
    }
    #endregion
}

public class DrawDamageCommand : CommandData
{
    #region Private Properties
    private PlayerIndex _playerIndex;
    private int _drawDamage;
    #endregion

    #region Constructors
    public DrawDamageCommand(PlayerIndex playerIndex, int drawDamage)
    {
        _playerIndex = playerIndex;
        _drawDamage = drawDamage;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;
        OnActionDrawDamage();
    }

    private void OnActionDrawDamage()
    {
        GameManager.Instance.ActionDrawDamage(_playerIndex, 1, this.OnActionDrawDamageComplete);
    }

    private void OnActionDrawDamageComplete()
    {
        _drawDamage--;
        if (_drawDamage > 0
           && GameManager.Instance.Data.PlayerDataList[(int)_playerIndex].CurrentHP > 0)
        {
            OnActionDrawDamage();
        }
        else
        {
            OnActiveComplete();
        }
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]DrawDamage_{1}_{2}"
            , _uniqueKey
            , _playerIndex
            , _drawDamage
        );
    }
    #endregion
}

public class RestoreLifePointDeltaCommand : CommandData
{
    #region Private Properties
    private PlayerIndex _targetPlayerIndex;
    private int _value;
    #endregion

    #region Constructors
    public RestoreLifePointDeltaCommand(PlayerIndex targetPlayerIndex, int value)
    {
        _targetPlayerIndex = targetPlayerIndex;
        _value = value;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        GameManager.Instance.ActionRestoreLifePointDelta(_targetPlayerIndex, _value, OnActiveComplete);
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]LifePointDelta_{1}_{2}"
            , _uniqueKey
            , _targetPlayerIndex
            , _value
        );
    }
    #endregion
}

public class DealDamageMinionCommand : CommandData
{
    #region Private Properties
    private Requester _requester;
    private List<int> _targetIDList;
    private MinionData _minion;
    private List<int> _deadTargetList;
    private int _damage;

    private DataParam _vfxParam;
    private VFXUIData _vfxUIData;
    #endregion

    #region Constructors
    public DealDamageMinionCommand(Requester requester, List<int> targetIDList, int damage, DataParam vfxParam)
    {
        _targetIDList = new List<int>(targetIDList);
        _deadTargetList = new List<int>();
        _damage = damage;
        _requester = requester;
        _vfxParam = vfxParam;
        _vfxUIData = new VFXUIData(_vfxParam);
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;
        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            // Add action log.
            List<MinionData> targetList;
            if (GameManager.Instance.FindMinionBattleCard(_targetIDList, out targetList))
            {
                DealMinionDamageLog log = new DealMinionDamageLog(
                      _requester
                    , targetList
                    , _damage
                );
                GameManager.Instance.ActionAddBattleLog(log);
            }

            // Count PlayerStat
            PlayerStatManager.Instance.DoActDealEffectDamageEvent(_requester.Minion, targetList, _damage, null);
        }

        // Popup Number
        UnityAction<int> onEachDealDamage = delegate (int uniqueId)
        {
            // Action Command 
            GameManager.Instance.ActionDealDamageMinion(
                _requester
                , uniqueId
                , _damage
                , null
            );
        };

        // Show VFX
        VFXUIManager_Minion.ActionPerformVFXMinion(
            _requester
            , _targetIDList
            , _damage
            , _vfxUIData
            , onEachDealDamage
            , CheckMinionDead
        );
    }

    private void CheckMinionDead()
    {
        List<int> minionList = new List<int>();
        MinionData minion;
        for (int i = 0; i < _targetIDList.Count; i++)
        {
            if (GameManager.Instance.FindMinionBattleCard(_targetIDList[i], out minion))
            {
                if (minion.IsAlive == false)
                {
                    minion.SetKillerPlayerIndex(_requester.PlayerIndex);
                    minionList.Add(minion.UniqueID);
                }
            }
        }

        if (minionList.Count > 0)
        {
            GameManager.Instance.ActionDeadMinion(minionList, true, MinionDeathTypes.Effect, OnActiveComplete);
            return;
        }
        OnActiveComplete();
    }

    public override string GetDebugText()
    {
        string idText = "";
        foreach (int id in _targetIDList)
        {
            if (idText == "")
            {
                idText += id.ToString();
            }
            else
            {
                idText += ", " + id.ToString();
            }
        }

        return string.Format(
              "[{0}]DealDamageMinion_{1}_{2}"
            , _uniqueKey
            , _damage
            , idText
        );
    }
    #endregion
}

public class DealDamageMinionWithSlotCommand : CommandData
{
    #region Private Properties
    private Requester _requester;
    private List<int> _slotIDList;
    private List<int> _targetIDList;
    private MinionData _minion;
    private List<int> _deadTargetList;
    private int _damage;

    private DataParam _vfxParam;
    private VFXUIData _vfxUIData;
    #endregion

    #region Constructors
    public DealDamageMinionWithSlotCommand(Requester requester, List<int> slotIDList, List<int> targetIDList, int damage, DataParam vfxParam)
    {
        _slotIDList = new List<int>(slotIDList);
        _targetIDList = new List<int>(targetIDList);
        _deadTargetList = new List<int>();
        _damage = damage;
        _requester = requester;
        _vfxParam = vfxParam;
        _vfxUIData = new VFXUIData(_vfxParam);
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;
        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            // Add action log.
            {
                List<MinionData> targetList;
                if (GameManager.Instance.FindMinionBattleCard(_targetIDList, out targetList))
                {
                    DealMinionDamageLog log = new DealMinionDamageLog(
                          _requester
                        , targetList
                        , _damage
                    );
                    GameManager.Instance.ActionAddBattleLog(log);
                }
            }
        }

        // Show VFX
        VFXUIManager_Slot.ActionPerformVFXSlot(
            _requester
            , _slotIDList
            , _damage
            , _vfxUIData
            , null
            , OnVFXComplete
            );

    }

    private void OnVFXComplete()
    {
        foreach (int item in _targetIDList)
        {
            // Action Command 
            GameManager.Instance.ActionDealDamageMinion(
                _requester
                , item
                , _damage
                , null
            );
        }
        CheckMinionDead();
    }

    private void CheckMinionDead()
    {
        List<int> minionList = new List<int>();
        MinionData minion;
        for (int i = 0; i < _targetIDList.Count; i++)
        {
            if (GameManager.Instance.FindMinionBattleCard(_targetIDList[i], out minion))
            {
                if (minion.IsAlive == false)
                {
                    minion.SetKillerPlayerIndex(_requester.PlayerIndex);
                    minionList.Add(minion.UniqueID);
                }
            }
        }

        if (minionList.Count > 0)
        {
            GameManager.Instance.ActionDeadMinion(minionList, true, MinionDeathTypes.Effect, OnActiveComplete);
            return;
        }
        OnActiveComplete();
    }

    public override string GetDebugText()
    {
        string idText = "";
        foreach (int id in _targetIDList)
        {
            if (idText == "")
            {
                idText += id.ToString();
            }
            else
            {
                idText += ", " + id.ToString();
            }
        }

        return string.Format(
              "[{0}]DealDamageMinion_{1}_{2}"
            , _uniqueKey
            , _damage
            , idText
        );
    }
    #endregion
}

public class DestroyMinionCommand : CommandData
{
    #region Enums
    public enum PerformStep
    {
        Start = 0
        , Moving
        , Arrived
    }
    #endregion

    #region Private Properties
    private Requester _requester;
    private List<int> _targetIDList;
    private MinionData _minion;
    private List<int> _deadTargetList;

    private DataParam _vfxParam;
    private VFXUIData _vfxUIData;
    #endregion

    #region Constructors
    public DestroyMinionCommand(Requester requester, List<int> targetIDList, DataParam vfxParam)
    {
        _targetIDList = new List<int>(targetIDList);
        _deadTargetList = new List<int>();
        _requester = requester;
        _vfxParam = vfxParam;
        _vfxUIData = new VFXUIData(_vfxParam);
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        int forceScreenshake = 5;
        _onComplete = onComplete;
        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            // Add action log.
            {
                List<MinionData> targetList;
                if (GameManager.Instance.FindMinionBattleCard(_targetIDList, out targetList))
                {
                    DestroyMinionLog log = new DestroyMinionLog(
                          _requester
                        , targetList
                    );
                    GameManager.Instance.ActionAddBattleLog(log);
                }
            }
        }

        // Popup Number
        UnityAction<int> onEachDealDamage = delegate (int uniqueId)
        {
            // Action Command 
            GameManager.Instance.ActionDestroyMinion(
                _requester
                , uniqueId
                , null
            );
        };

        // Show VFX
        VFXUIManager_Minion.ActionPerformVFXMinion(
            _requester
            , _targetIDList
            , forceScreenshake
            , _vfxUIData
            , onEachDealDamage
            , CheckMinionDead
        );
    }

    private void CheckMinionDead()
    {
        List<int> minionList = new List<int>();
        MinionData minion;
        for (int i = 0; i < _targetIDList.Count; i++)
        {
            if (GameManager.Instance.FindMinionBattleCard(_targetIDList[i], out minion))
            {
                if (minion.IsAlive == false)
                {
                    minion.SetKillerPlayerIndex(_requester.PlayerIndex);
                    minionList.Add(minion.UniqueID);
                }
            }
        }

        if (minionList.Count > 0)
        {
            GameManager.Instance.ActionDeadMinion(minionList, true, MinionDeathTypes.Effect, OnActiveComplete);
            return;
        }
        OnActiveComplete();
    }

    public override string GetDebugText()
    {
        string idText = "";
        foreach (int id in _targetIDList)
        {
            if (idText == "")
            {
                idText += id.ToString();
            }
            else
            {
                idText += ", " + id.ToString();
            }
        }

        return string.Format(
              "[{0}]DestroyMinion_{1}"
            , _uniqueKey
            , idText
        );
    }
    #endregion
}

public class SacrificeMinionCommand : CommandData
{
    #region Enums
    public enum PerformStep
    {
        Start = 0
        , Moving
        , Arrived
    }
    #endregion

    #region Private Properties
    private Requester _requester;
    private List<int> _targetIDList;
    private MinionData _minion;
    private List<int> _deadTargetList;

    private DataParam _vfxParam;
    private VFXUIData _vfxUIData;
    #endregion

    #region Constructors
    public SacrificeMinionCommand(Requester requester, List<int> targetIDList, DataParam vfxParam)
    {
        _targetIDList = new List<int>(targetIDList);
        _deadTargetList = new List<int>();
        _requester = requester;
        _vfxParam = vfxParam;
        _vfxUIData = new VFXUIData(_vfxParam);
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        int forceScreenshake = 5;
        _onComplete = onComplete;

        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            // Add action log.
            {
                List<MinionData> targetList;
                if (GameManager.Instance.FindMinionBattleCard(_targetIDList, out targetList))
                {
                    SacrificeMinionLog log = new SacrificeMinionLog(
                          _requester
                        , targetList
                    );
                    GameManager.Instance.ActionAddBattleLog(log);
                }
            }
        }

        // Popup Number
        UnityAction<int> onEachDealDamage = delegate (int uniqueId)
        {
            // Action Command 
            GameManager.Instance.ActionSacrificeMinion(
                _requester
                , uniqueId
                , null
            );
        };

        // Show VFX
        VFXUIManager_Minion.ActionPerformVFXMinion(
            _requester
            , _targetIDList
            , forceScreenshake
            , _vfxUIData
            , onEachDealDamage
            , CheckMinionDead
        );
    }

    private void CheckMinionDead()
    {
        List<int> minionList = new List<int>();
        MinionData minion;
        for (int i = 0; i < _targetIDList.Count; i++)
        {
            if (GameManager.Instance.FindMinionBattleCard(_targetIDList[i], out minion))
            {
                if (minion.IsAlive == false)
                {
                    minion.SetKillerPlayerIndex(_requester.PlayerIndex);
                    minionList.Add(minion.UniqueID);
                }
            }
        }

        if (minionList.Count > 0)
        {
            GameManager.Instance.ActionDeadMinion(minionList, true, MinionDeathTypes.Effect, OnActiveComplete);
            return;
        }
        OnActiveComplete();
    }

    public override string GetDebugText()
    {
        string idText = "";
        foreach (int id in _targetIDList)
        {
            if (idText == "")
            {
                idText += id.ToString();
            }
            else
            {
                idText += ", " + id.ToString();
            }
        }

        return string.Format(
              "[{0}]SacrificeMinionLog{1}"
            , _uniqueKey
            , idText
        );
    }
    #endregion

}

public class DealDamagePlayerCommand : CommandData
{
    #region Enums
    public enum PerformStep
    {
        Start = 0
        , Moving
        , Arrived
    }
    #endregion

    #region Private Properties
    private Requester _requester;
    private List<PlayerIndex> _playerList;
    private int _damage;

    private DataParam _vfxParam;
    private VFXUIData _vfxUIData;
    #endregion

    #region Constructors
    public DealDamagePlayerCommand(Requester requester, List<PlayerIndex> playerList, int damage, DataParam vfxParam)
    {
        _playerList = new List<PlayerIndex>(playerList);
        _damage = damage;
        _requester = requester;
        _vfxParam = vfxParam;
        _vfxUIData = new VFXUIData(_vfxParam);
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        if (_playerList != null && _playerList.Count > 0)
        {
            // Add action log.
            {
                DealPlayerDamageLog log = new DealPlayerDamageLog(
                      _requester
                    , _playerList
                    , _damage
                );
                GameManager.Instance.ActionAddBattleLog(log);
            }
        }

        // Popup Number
        UnityAction<PlayerIndex> onEachDealDamage = delegate (PlayerIndex playerID)
        {
            // Action Command 
            GameManager.Instance.ActionDealDamagePlayer(
                _requester
                , playerID
                , _damage
                , null
            );
        };

        // Show VFX
        VFXUIManager_Player.ActionPerformVFXPlayer(
            _requester
            , _playerList
            , _damage
            , _vfxUIData
            , onEachDealDamage
            , OnActiveComplete
        );

    }

    public override string GetDebugText()
    {
        string idText = "";
        foreach (int id in _playerList)
        {
            if (idText == "")
            {
                idText += id.ToString();
            }
            else
            {
                idText += ", " + id.ToString();
            }
        }

        return string.Format(
              "[{0}]DealDamagePlayer_{1}_{2}"
            , _uniqueKey
            , _damage
            , idText
        );
    }
    #endregion
}

public class SetHPMinionCommand : CommandData
{
    #region Private Properties
    private Requester _requester;
    private List<int> _targetIDList;
    private int _value;
    #endregion

    #region Constructors
    public SetHPMinionCommand(Requester requester, List<int> targetIDList, int value)
    {
        _targetIDList = new List<int>(targetIDList);
        _value = value;
        _requester = requester;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            OnSetHP(_targetIDList[0]);
        }
        else
        {
            OnActiveComplete();
        }
    }

    private void OnSetHP(int uniqueID)
    {
        GameManager.Instance.ActionSetHPMinion(_requester, uniqueID, _value, this.OnSetHPComplete);
    }

    private void OnSetHPComplete()
    {
        _targetIDList.RemoveAt(0);
        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            OnSetHP(_targetIDList[0]);
        }
        else
        {
            OnActiveComplete();
        }
    }

    public override string GetDebugText()
    {
        string idText = "";
        foreach (int id in _targetIDList)
        {
            if (idText == "")
            {
                idText += id.ToString();
            }
            else
            {
                idText += ", " + id.ToString();
            }
        }

        return string.Format(
              "[{0}]SetHPMinion_{1}_{2}"
            , _uniqueKey
            , _value
            , idText
        );
    }
    #endregion
}

public class SetAPMinionCommand : CommandData
{
    #region Private Properties
    private Requester _requester;
    private List<int> _targetIDList;
    private int _value;
    #endregion

    #region Constructors
    public SetAPMinionCommand(Requester requester, List<int> targetIDList, int value)
    {
        _targetIDList = new List<int>(targetIDList);
        _value = value;
        _requester = requester;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            OnSetAP(_targetIDList[0]);
        }
        else
        {
            OnActiveComplete();
        }
    }

    private void OnSetAP(int uniqueID)
    {
        GameManager.Instance.ActionSetAPMinion(_requester, uniqueID, _value, this.OnSetAPComplete);
    }

    private void OnSetAPComplete()
    {
        _targetIDList.RemoveAt(0);
        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            OnSetAP(_targetIDList[0]);
        }
        else
        {
            OnActiveComplete();
        }
    }

    public override string GetDebugText()
    {
        string idText = "";
        foreach (int id in _targetIDList)
        {
            if (idText == "")
            {
                idText += id.ToString();
            }
            else
            {
                idText += ", " + id.ToString();
            }
        }

        return string.Format(
              "[{0}]SetAPMinion_{1}_{2}"
            , _uniqueKey
            , _value
            , idText
        );
    }
    #endregion
}

public class SetSPMinionCommand : CommandData
{
    #region Private Properties
    private Requester _requester;
    private List<int> _targetIDList;
    private int _value;
    #endregion

    #region Constructors
    public SetSPMinionCommand(Requester requester, List<int> targetIDList, int value)
    {
        _targetIDList = new List<int>(targetIDList);
        _value = value;
        _requester = requester;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            OnSetSP(_targetIDList[0]);
        }
        else
        {
            OnActiveComplete();
        }
    }

    private void OnSetSP(int uniqueID)
    {
        GameManager.Instance.ActionSetSPMinion(_requester, uniqueID, _value, this.OnSetSPComplete);
    }

    private void OnSetSPComplete()
    {
        _targetIDList.RemoveAt(0);
        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            OnSetSP(_targetIDList[0]);
        }
        else
        {
            OnActiveComplete();
        }
    }

    public override string GetDebugText()
    {
        string idText = "";
        foreach (int id in _targetIDList)
        {
            if (idText == "")
            {
                idText += id.ToString();
            }
            else
            {
                idText += ", " + id.ToString();
            }
        }

        return string.Format(
              "[{0}]SetSPMinion_{1}_{2}"
            , _uniqueKey
            , _value
            , idText
        );
    }
    #endregion
}

public class SetArmorMinionCommand : CommandData
{
    #region Private Properties
    private Requester _requester;
    private List<int> _targetIDList;
    private int _value;
    #endregion

    #region Constructors
    public SetArmorMinionCommand(Requester requester, List<int> targetIDList, int value)
    {
        _targetIDList = new List<int>(targetIDList);
        _value = value;
        _requester = requester;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            OnSetArmor(_targetIDList[0]);
        }
        else
        {
            OnActiveComplete();
        }
    }

    private void OnSetArmor(int uniqueID)
    {
        GameManager.Instance.ActionSetArmorMinion(_requester, uniqueID, _value, this.OnSetArmorComplete);
    }

    private void OnSetArmorComplete()
    {
        _targetIDList.RemoveAt(0);
        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            OnSetArmor(_targetIDList[0]);
        }
        else
        {
            OnActiveComplete();
        }
    }

    public override string GetDebugText()
    {
        string idText = "";
        foreach (int id in _targetIDList)
        {
            if (idText == "")
            {
                idText += id.ToString();
            }
            else
            {
                idText += ", " + id.ToString();
            }
        }

        return string.Format(
              "[{0}]SetArmorMinion_{1}_{2}"
            , _uniqueKey
            , _value
            , idText
        );
    }
    #endregion
}

public class SetAPHPMinionCommand : CommandData
{
    #region Private Properties
    private Requester _requester;
    private List<int> _targetIDList;
    private int _apValue;
    private int _hpValue;
    #endregion

    #region Constructors
    public SetAPHPMinionCommand(Requester requester, List<int> targetIDList, int apValue, int hpValue)
    {
        _targetIDList = new List<int>(targetIDList);
        _apValue = apValue;
        _hpValue = hpValue;
        _requester = requester;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            OnSetSP(_targetIDList[0]);
        }
        else
        {
            OnActiveComplete();
        }
    }

    private void OnSetSP(int uniqueID)
    {
        GameManager.Instance.ActionSetAPHPMinion(_requester, uniqueID, _apValue, _hpValue, this.OnSetAPHPComplete);
    }

    private void OnSetAPHPComplete()
    {
        _targetIDList.RemoveAt(0);
        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            OnSetSP(_targetIDList[0]);
        }
        else
        {
            OnActiveComplete();
        }
    }

    public override string GetDebugText()
    {
        string idText = "";
        foreach (int id in _targetIDList)
        {
            if (idText == "")
            {
                idText += id.ToString();
            }
            else
            {
                idText += ", " + id.ToString();
            }
        }

        return string.Format(
              "[{0}]SetAPHPMinion_{1}_{2}_{3}"
            , _uniqueKey
            , _apValue
            , _hpValue
            , idText
        );
    }
    #endregion
}

public class BoostHPMinionCommand : CommandData
{
    #region Private Properties
    private Requester _requester;
    private List<int> _targetIDList;
    private int _value;
    #endregion

    #region Constructors
    public BoostHPMinionCommand(Requester requester, List<int> targetIDList, int value)
    {
        _targetIDList = new List<int>(targetIDList);
        _value = value;
        _requester = requester;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            OnBoostHP(_targetIDList[0]);
        }
        else
        {
            OnActiveComplete();
        }
    }

    private void OnBoostHP(int uniqueID)
    {
        GameManager.Instance.ActionBoostHPMinion(_requester, uniqueID, _value, this.OnBoostHPComplete);
    }

    private void OnBoostHPComplete()
    {
        _targetIDList.RemoveAt(0);
        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            OnBoostHP(_targetIDList[0]);
        }
        else
        {
            OnActiveComplete();
        }
    }

    public override string GetDebugText()
    {
        string idText = "";
        foreach (int id in _targetIDList)
        {
            if (idText == "")
            {
                idText += id.ToString();
            }
            else
            {
                idText += ", " + id.ToString();
            }
        }

        return string.Format(
              "[{0}]BoostHPMinion_{1}_{2}"
            , _uniqueKey
            , _value
            , idText
        );
    }
    #endregion
}

public class BoostAPMinionCommand : CommandData
{
    #region Private Properties
    private Requester _requester;
    private List<int> _targetIDList;
    private int _value;
    #endregion

    #region Constructors
    public BoostAPMinionCommand(Requester requester, List<int> targetIDList, int value)
    {
        _targetIDList = new List<int>(targetIDList);
        _value = value;
        _requester = requester;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            OnBoostAP(_targetIDList[0]);
        }
        else
        {
            OnActiveComplete();
        }
    }

    private void OnBoostAP(int uniqueID)
    {
        GameManager.Instance.ActionBoostAPMinion(_requester, uniqueID, _value, this.OnBoostAPComplete);
    }

    private void OnBoostAPComplete()
    {
        _targetIDList.RemoveAt(0);
        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            OnBoostAP(_targetIDList[0]);
        }
        else
        {
            OnActiveComplete();
        }
    }

    public override string GetDebugText()
    {
        string idText = "";
        foreach (int id in _targetIDList)
        {
            if (idText == "")
            {
                idText += id.ToString();
            }
            else
            {
                idText += ", " + id.ToString();
            }
        }

        return string.Format(
              "[{0}]BoostAPMinion_{1}_{2}"
            , _uniqueKey
            , _value
            , idText
        );
    }
    #endregion
}

public class BoostAPCriticalMinionCommand : CommandData
{
    #region Private Properties
    private Requester _requester;
    private List<int> _targetIDList;
    private int _value;
    #endregion

    #region Constructors
    public BoostAPCriticalMinionCommand(Requester requester, List<int> targetIDList, int value)
    {
        _targetIDList = new List<int>(targetIDList);
        _value = value;
        _requester = requester;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            OnBoostAP(_targetIDList[0]);
        }
        else
        {
            OnActiveComplete();
        }
    }

    private void OnBoostAP(int uniqueID)
    {
        GameManager.Instance.ActionBoostAPMinionCritical(_requester, uniqueID, _value, this.OnBoostAPComplete);
    }

    private void OnBoostAPComplete()
    {
        _targetIDList.RemoveAt(0);
        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            OnBoostAP(_targetIDList[0]);
        }
        else
        {
            OnActiveComplete();
        }
    }

    public override string GetDebugText()
    {
        string idText = "";
        foreach (int id in _targetIDList)
        {
            if (idText == "")
            {
                idText += id.ToString();
            }
            else
            {
                idText += ", " + id.ToString();
            }
        }

        return string.Format(
              "[{0}]BoostAPMinionCritical_{1}_{2}"
            , _uniqueKey
            , _value
            , idText
        );
    }
    #endregion
}

public class BoostSPMinionCommand : CommandData
{
    #region Private Properties
    private Requester _requester;
    private List<int> _targetIDList;
    private int _value;
    #endregion

    #region Constructors
    public BoostSPMinionCommand(Requester requester, List<int> targetIDList, int value)
    {
        _targetIDList = new List<int>(targetIDList);
        _value = value;
        _requester = requester;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            OnBoostSP(_targetIDList[0]);
        }
        else
        {
            OnActiveComplete();
        }
    }

    private void OnBoostSP(int uniqueID)
    {
        GameManager.Instance.ActionBoostSPMinion(_requester, uniqueID, _value, this.OnBoostSPComplete);
    }

    private void OnBoostSPComplete()
    {
        _targetIDList.RemoveAt(0);
        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            OnBoostSP(_targetIDList[0]);
        }
        else
        {
            OnActiveComplete();
        }
    }

    public override string GetDebugText()
    {
        string idText = "";
        foreach (int id in _targetIDList)
        {
            if (idText == "")
            {
                idText += id.ToString();
            }
            else
            {
                idText += ", " + id.ToString();
            }
        }

        return string.Format(
              "[{0}]BoostSPMinion_{1}_{2}"
            , _uniqueKey
            , _value
            , idText
        );
    }
    #endregion
}

public class BoostAPHPMinionCommand : CommandData
{
    #region Private Properties
    private Requester _requester;
    private List<int> _targetIDList;
    private int _ap;
    private int _hp;
    #endregion

    #region Constructors
    public BoostAPHPMinionCommand(Requester requester, List<int> targetIDList, int ap, int hp)
    {
        _targetIDList = new List<int>(targetIDList);
        _ap = ap;
        _hp = hp;
        _requester = requester;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            OnBoostAPHP(_targetIDList[0]);
        }
        else
        {
            OnActiveComplete();
        }
    }

    private void OnBoostAPHP(int uniqueID)
    {
        GameManager.Instance.ActionBoostAPHPMinion(_requester, uniqueID, _ap, _hp, this.OnBoostAPHPComplete);
    }

    private void OnBoostAPHPComplete()
    {
        _targetIDList.RemoveAt(0);
        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            OnBoostAPHP(_targetIDList[0]);
        }
        else
        {
            OnActiveComplete();
        }
    }

    public override string GetDebugText()
    {
        string idText = "";
        foreach (int id in _targetIDList)
        {
            if (idText == "")
            {
                idText += id.ToString();
            }
            else
            {
                idText += ", " + id.ToString();
            }
        }

        return string.Format(
              "[{0}]BoostAPHPMinion_{1}/{2}_{3}"
            , _uniqueKey
            , _ap
            , _hp
            , idText
        );
    }
    #endregion
}

public class BoostArmorMinionCommand : CommandData
{
    #region Private Properties
    private Requester _requester;
    private List<int> _targetIDList;
    private int _armor;
    #endregion

    #region Constructors
    public BoostArmorMinionCommand(Requester requester, List<int> targetIDList, int armor)
    {
        _targetIDList = new List<int>(targetIDList);
        _armor = armor;
        _requester = requester;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            OnBoostArmor(_targetIDList[0]);
        }
        else
        {
            OnActiveComplete();
        }
    }

    private void OnBoostArmor(int uniqueID)
    {
        GameManager.Instance.ActionBoostArmorMinion(_requester, uniqueID, _armor, this.OnBoostArmorComplete);
    }

    private void OnBoostArmorComplete()
    {
        _targetIDList.RemoveAt(0);
        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            OnBoostArmor(_targetIDList[0]);
        }
        else
        {
            OnActiveComplete();
        }
    }

    public override string GetDebugText()
    {
        string idText = "";
        foreach (int id in _targetIDList)
        {
            if (idText == "")
            {
                idText += id.ToString();
            }
            else
            {
                idText += ", " + id.ToString();
            }
        }

        return string.Format(
              "[{0}]BoostArmorMinion_{1}_{2}"
            , _uniqueKey
            , _armor
            , idText
        );
    }
    #endregion
}

public class BoostBuffPassiveHPMinionCommand : CommandData
{
    #region Private Properties
    private Requester _requester;
    private List<int> _targetIDList;
    private int _value;
    #endregion

    #region Constructors
    public BoostBuffPassiveHPMinionCommand(Requester requester, List<int> targetIDList, int value)
    {
        _targetIDList = new List<int>(targetIDList);
        _value = value;
        _requester = requester;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            OnBoostHP(_targetIDList[0]);
        }
        else
        {
            OnActiveComplete();
        }
    }

    private void OnBoostHP(int uniqueID)
    {
        GameManager.Instance.ActionBoostBuffPassiveHPMinion(_requester, uniqueID, _value, this.OnBoostHPComplete);
    }

    private void OnBoostHPComplete()
    {
        _targetIDList.RemoveAt(0);
        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            OnBoostHP(_targetIDList[0]);
        }
        else
        {
            OnActiveComplete();
        }
    }

    public override string GetDebugText()
    {
        string idText = "";
        foreach (int id in _targetIDList)
        {
            if (idText == "")
            {
                idText += id.ToString();
            }
            else
            {
                idText += ", " + id.ToString();
            }
        }

        return string.Format(
              "[{0}]BoostBuffPassiveHPMinion_{1}_{2}"
            , _uniqueKey
            , _value
            , idText
        );
    }
    #endregion
}

public class BoostBuffPassiveAPMinionCommand : CommandData
{
    #region Private Properties
    private Requester _requester;
    private List<int> _targetIDList;
    private int _value;
    #endregion

    #region Constructors
    public BoostBuffPassiveAPMinionCommand(Requester requester, List<int> targetIDList, int value)
    {
        _targetIDList = new List<int>(targetIDList);
        _value = value;
        _requester = requester;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            OnBoostAP(_targetIDList[0]);
        }
        else
        {
            OnActiveComplete();
        }
    }

    private void OnBoostAP(int uniqueID)
    {
        GameManager.Instance.ActionBoostBuffPassiveAPMinion(_requester, uniqueID, _value, this.OnBoostAPComplete);
    }

    private void OnBoostAPComplete()
    {
        _targetIDList.RemoveAt(0);
        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            OnBoostAP(_targetIDList[0]);
        }
        else
        {
            OnActiveComplete();
        }
    }

    public override string GetDebugText()
    {
        string idText = "";
        foreach (int id in _targetIDList)
        {
            if (idText == "")
            {
                idText += id.ToString();
            }
            else
            {
                idText += ", " + id.ToString();
            }
        }

        return string.Format(
              "[{0}]BoostBuffPassiveAPMinion_{1}_{2}"
            , _uniqueKey
            , _value
            , idText
        );
    }
    #endregion
}

public class BoostBuffPassiveAPCriticalMinionCommand : CommandData
{
    #region Private Properties
    private Requester _requester;
    private List<int> _targetIDList;
    private int _value;
    #endregion

    #region Constructors
    public BoostBuffPassiveAPCriticalMinionCommand(Requester requester, List<int> targetIDList, int value)
    {
        _targetIDList = new List<int>(targetIDList);
        _value = value;
        _requester = requester;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            OnBoostAP(_targetIDList[0]);
        }
        else
        {
            OnActiveComplete();
        }
    }

    private void OnBoostAP(int uniqueID)
    {
        GameManager.Instance.ActionBoostBuffPassiveAPCriticalMinion(_requester, uniqueID, _value, this.OnBoostAPComplete);
    }

    private void OnBoostAPComplete()
    {
        _targetIDList.RemoveAt(0);
        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            OnBoostAP(_targetIDList[0]);
        }
        else
        {
            OnActiveComplete();
        }
    }

    public override string GetDebugText()
    {
        string idText = "";
        foreach (int id in _targetIDList)
        {
            if (idText == "")
            {
                idText += id.ToString();
            }
            else
            {
                idText += ", " + id.ToString();
            }
        }

        return string.Format(
              "[{0}]BoostBuffPassiveAPMinionCritical_{1}_{2}"
            , _uniqueKey
            , _value
            , idText
        );
    }
    #endregion
}

public class BoostBuffPassiveSPMinionCommand : CommandData
{
    #region Private Properties
    private Requester _requester;
    private List<int> _targetIDList;
    private int _value;
    #endregion

    #region Constructors
    public BoostBuffPassiveSPMinionCommand(Requester requester, List<int> targetIDList, int value)
    {
        _targetIDList = new List<int>(targetIDList);
        _value = value;
        _requester = requester;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            OnBoostSP(_targetIDList[0]);
        }
        else
        {
            OnActiveComplete();
        }
    }

    private void OnBoostSP(int uniqueID)
    {
        GameManager.Instance.ActionBoostBuffPassiveSPMinion(_requester, uniqueID, _value, this.OnBoostSPComplete);
    }

    private void OnBoostSPComplete()
    {
        _targetIDList.RemoveAt(0);
        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            OnBoostSP(_targetIDList[0]);
        }
        else
        {
            OnActiveComplete();
        }
    }

    public override string GetDebugText()
    {
        string idText = "";
        foreach (int id in _targetIDList)
        {
            if (idText == "")
            {
                idText += id.ToString();
            }
            else
            {
                idText += ", " + id.ToString();
            }
        }

        return string.Format(
              "[{0}]BoostBuffPassiveSPMinion_{1}_{2}"
            , _uniqueKey
            , _value
            , idText
        );
    }
    #endregion
}

public class BoostBuffPassiveAPHPMinionCommand : CommandData
{
    #region Private Properties
    private Requester _requester;
    private List<int> _targetIDList;
    private int _ap;
    private int _hp;
    #endregion

    #region Constructors
    public BoostBuffPassiveAPHPMinionCommand(Requester requester, List<int> targetIDList, int ap, int hp)
    {
        _targetIDList = new List<int>(targetIDList);
        _ap = ap;
        _hp = hp;
        _requester = requester;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            OnBoostAPHP(_targetIDList[0]);
        }
        else
        {
            OnActiveComplete();
        }
    }

    private void OnBoostAPHP(int uniqueID)
    {
        GameManager.Instance.ActionBoostBuffPassiveAPHPMinion(_requester, uniqueID, _ap, _hp, this.OnBoostAPHPComplete);
    }

    private void OnBoostAPHPComplete()
    {
        _targetIDList.RemoveAt(0);
        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            OnBoostAPHP(_targetIDList[0]);
        }
        else
        {
            OnActiveComplete();
        }
    }

    public override string GetDebugText()
    {
        string idText = "";
        foreach (int id in _targetIDList)
        {
            if (idText == "")
            {
                idText += id.ToString();
            }
            else
            {
                idText += ", " + id.ToString();
            }
        }

        return string.Format(
              "[{0}]BoostBuffPassiveAPHPMinion_{1}/{2}_{3}"
            , _uniqueKey
            , _ap
            , _hp
            , idText
        );
    }
    #endregion
}

public class BoostBuffPassiveArmorMinionCommand : CommandData
{
    #region Private Properties
    private Requester _requester;
    private List<int> _targetIDList;
    private int _armor;
    #endregion

    #region Constructors
    public BoostBuffPassiveArmorMinionCommand(Requester requester, List<int> targetIDList, int armor)
    {
        _targetIDList = new List<int>(targetIDList);
        _armor = armor;
        _requester = requester;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            OnBoostArmor(_targetIDList[0]);
        }
        else
        {
            OnActiveComplete();
        }
    }

    private void OnBoostArmor(int uniqueID)
    {
        GameManager.Instance.ActionBoostBuffPassiveArmorMinion(_requester, uniqueID, _armor, this.OnBoostArmorComplete);
    }

    private void OnBoostArmorComplete()
    {
        _targetIDList.RemoveAt(0);
        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            OnBoostArmor(_targetIDList[0]);
        }
        else
        {
            OnActiveComplete();
        }
    }

    public override string GetDebugText()
    {
        string idText = "";
        foreach (int id in _targetIDList)
        {
            if (idText == "")
            {
                idText += id.ToString();
            }
            else
            {
                idText += ", " + id.ToString();
            }
        }

        return string.Format(
              "[{0}]BoostBuffPassiveArmorMinion_{1}_{2}"
            , _uniqueKey
            , _armor
            , idText
        );
    }
    #endregion
}

public class BoostHeroSPCommand : CommandData
{
    #region Private Properties
    private Requester _requester;
    private PlayerIndex _playerTarget;
    private int _value;
    #endregion

    #region Constructors
    public BoostHeroSPCommand(Requester requester, PlayerIndex playerTarget, int value)
    {
        _requester = requester;
        _playerTarget = playerTarget;
        _value = value;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;
        GameManager.Instance.ActionBoostHeroSP(_requester, _playerTarget, _value, OnActiveComplete);
    }

    public override string GetDebugText()
    {
        return string.Format(
                "[{0}]{1}"
            , _uniqueKey
            , this.GetType().ToString()
        );
    }
    #endregion
}

public class SwapMinionHPCommand : CommandData
{
    #region Private Properties
    private Requester _requester;
    private int _battleCardUniqueID_1;
    private int _battleCardUniqueID_2;
    #endregion

    #region Constructors
    public SwapMinionHPCommand(Requester requester, int battleCardUniqueID_1, int battleCardUniqueID_2)
    {
        _battleCardUniqueID_1 = battleCardUniqueID_1;
        _battleCardUniqueID_2 = battleCardUniqueID_2;
        _requester = requester;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;
        GameManager.Instance.ActionSwapHPMinion(_requester, _battleCardUniqueID_1, _battleCardUniqueID_2, OnActiveComplete);
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]SwapHPMinion_{1}_{2}"
            , _uniqueKey
            , _battleCardUniqueID_1
            , _battleCardUniqueID_2
        );
    }
    #endregion
}

public class SwapMinionAPCommand : CommandData
{
    #region Private Properties
    private Requester _requester;
    private int _battleCardUniqueID_1;
    private int _battleCardUniqueID_2;
    #endregion

    #region Constructors
    public SwapMinionAPCommand(Requester requester, int battleCardUniqueID_1, int battleCardUniqueID_2)
    {
        _battleCardUniqueID_1 = battleCardUniqueID_1;
        _battleCardUniqueID_2 = battleCardUniqueID_2;
        _requester = requester;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;
        GameManager.Instance.ActionSwapAPMinion(_requester, _battleCardUniqueID_1, _battleCardUniqueID_2, OnActiveComplete);
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]SwapAPMinion_{1}_{2}"
            , _uniqueKey
            , _battleCardUniqueID_1
            , _battleCardUniqueID_2
        );
    }
    #endregion
}

public class SwapMinionSPCommand : CommandData
{
    #region Private Properties
    private Requester _requester;
    private int _battleCardUniqueID_1;
    private int _battleCardUniqueID_2;
    #endregion

    #region Constructors
    public SwapMinionSPCommand(Requester requester, int battleCardUniqueID_1, int battleCardUniqueID_2)
    {
        _battleCardUniqueID_1 = battleCardUniqueID_1;
        _battleCardUniqueID_2 = battleCardUniqueID_2;
        _requester = requester;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;
        GameManager.Instance.ActionSwapSPMinion(_requester, _battleCardUniqueID_1, _battleCardUniqueID_2, OnActiveComplete);
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]SwapSPMinion_{1}_{2}"
            , _uniqueKey
            , _battleCardUniqueID_1
            , _battleCardUniqueID_2
        );
    }
    #endregion
}

public class SwapMinionAPHPCommand : CommandData
{
    #region Private Properties
    private Requester _requester;
    private int _battleCardUniqueID_1;
    private int _battleCardUniqueID_2;
    #endregion

    #region Constructors
    public SwapMinionAPHPCommand(Requester requester, int battleCardUniqueID_1, int battleCardUniqueID_2)
    {
        _battleCardUniqueID_1 = battleCardUniqueID_1;
        _battleCardUniqueID_2 = battleCardUniqueID_2;
        _requester = requester;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;
        GameManager.Instance.ActionSwapAPHPMinion(_requester, _battleCardUniqueID_1, _battleCardUniqueID_2, OnActiveComplete);
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]SwapAPHPMinion_{1}_{2}"
            , _uniqueKey
            , _battleCardUniqueID_1
            , _battleCardUniqueID_2
        );
    }
    #endregion
}

public class SwapMinionSlotCommand : CommandData
{
    #region Private Properties
    private Requester _requester;
    private int _battleCardUniqueID_1;
    private int _battleCardUniqueID_2;
    #endregion

    #region Constructors
    public SwapMinionSlotCommand(Requester requester, int battleCardUniqueID_1, int battleCardUniqueID_2)
    {
        _battleCardUniqueID_1 = battleCardUniqueID_1;
        _battleCardUniqueID_2 = battleCardUniqueID_2;
        _requester = requester;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;
        GameManager.Instance.ActionSwapSlotMinion(
              _requester
            , _battleCardUniqueID_1
            , _battleCardUniqueID_2
            , delegate ()
            {
                MinionData minion_1;
                MinionData minion_2;
                bool isSuccess = true;
                isSuccess &= GameManager.Instance.FindMinionBattleCard(_battleCardUniqueID_1, out minion_1);
                isSuccess &= GameManager.Instance.FindMinionBattleCard(_battleCardUniqueID_2, out minion_2);
                if (isSuccess)
                {
                    Debug.Log("Minion Swap Moved Trigger");

                    // Trigger Moved Event
                    List<EventTriggerData> triggerList = new List<EventTriggerData>();
                    Requester triggerer_1 = new Requester(minion_1);
                    Requester triggerer_2 = new Requester(minion_2);

                    triggerList.Add(new EventTriggerData(EventKey.OnMeMoved, triggerer_1));
                    triggerList.Add(new EventTriggerData(EventKey.OnFriendMoved, triggerer_1));
                    triggerList.Add(new EventTriggerData(EventKey.OnEnemyMoved, triggerer_1));

                    triggerList.Add(new EventTriggerData(EventKey.OnMeMoved, triggerer_2));
                    triggerList.Add(new EventTriggerData(EventKey.OnFriendMoved, triggerer_2));
                    triggerList.Add(new EventTriggerData(EventKey.OnEnemyMoved, triggerer_2));

                    GameManager.Instance.ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
                }
                OnActiveComplete();
            }
        );
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]SwapMinion_{1}_{2}"
            , _uniqueKey
            , _battleCardUniqueID_1
            , _battleCardUniqueID_2
        );
    }
    #endregion
}

public class RestoreHPMinionCommand : CommandData
{
    #region Private Properties
    private Requester _requester;
    private List<int> _targetIDList;
    private int _value;
    #endregion

    #region Constructors
    public RestoreHPMinionCommand(Requester requester, List<int> targetIDList, int damage)
    {
        _targetIDList = new List<int>(targetIDList);
        _value = damage;
        _requester = requester;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            OnSetHP(_targetIDList[0]);
        }
        else
        {
            OnActiveComplete();
        }
    }

    private void OnSetHP(int uniqueID)
    {
        GameManager.Instance.ActionRestoreHPMinion(_requester, uniqueID, _value, this.OnSetHPComplete);
    }

    private void OnSetHPComplete()
    {
        _targetIDList.RemoveAt(0);
        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            OnSetHP(_targetIDList[0]);
        }
        else
        {
            OnActiveComplete();
        }
    }

    public override string GetDebugText()
    {
        string idText = "";
        foreach (int id in _targetIDList)
        {
            if (idText == "")
            {
                idText += id.ToString();
            }
            else
            {
                idText += ", " + id.ToString();
            }
        }

        return string.Format(
              "[{0}]RestoreHPMinion_{1}_{2}"
            , _uniqueKey
            , _value
            , idText
        );
    }
    #endregion
}

public class ResetMinionCommand : CommandData
{
    #region Protected Properties
    protected Requester _requester;
    protected List<int> _targetIDList;
    #endregion

    #region Constructors
    public ResetMinionCommand(Requester requester, List<int> targetIDList)
    {
        _targetIDList = new List<int>(targetIDList);
        _requester = requester;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            OnReset(_targetIDList[0]);
        }
        else
        {
            OnActiveComplete();
        }
    }

    protected virtual void OnReset(int uniqueID)
    {
        GameManager.Instance.ActionResetMinion(_requester, uniqueID, this.OnResetComplete);
    }

    protected void OnResetComplete()
    {
        _targetIDList.RemoveAt(0);
        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            OnReset(_targetIDList[0]);
        }
        else
        {
            OnActiveComplete();
        }
    }

    public override string GetDebugText()
    {
        string idText = "";
        foreach (int id in _targetIDList)
        {
            if (idText == "")
            {
                idText += id.ToString();
            }
            else
            {
                idText += ", " + id.ToString();
            }
        }

        return string.Format(
              "[{0}]ResetMinion_{1}"
            , _uniqueKey
            , idText
        );
    }
    #endregion
}

public class ResetMinionInZoneCommand : ResetMinionCommand
{
    #region Protected Properties
    protected CardZone _resetZone;
    #endregion

    #region Constructors
    public ResetMinionInZoneCommand(Requester requester, List<int> targetIDList, CardZone resetZone) : base(requester, targetIDList)
    {
        _resetZone = resetZone;
    }
    #endregion

    #region Methods
    protected override void OnReset(int uniqueID)
    {
        BattleCardData card;
        if (GameManager.Instance.FindBattleCard(uniqueID, out card))
        {
            if (card.CurrentZone == _resetZone)
            {
                GameManager.Instance.ActionResetMinion(_requester, uniqueID, this.OnResetComplete);
                return;
            }
        }

        OnResetComplete();
    }

    public override string GetDebugText()
    {
        string idText = "";
        foreach (int id in _targetIDList)
        {
            if (idText == "")
            {
                idText += id.ToString();
            }
            else
            {
                idText += ", " + id.ToString();
            }
        }

        return string.Format(
              "[{0}]ResetMinionInZone_{1}_{2}"
            , _uniqueKey
            , idText
            , _resetZone
        );
    }
    #endregion
}

public class ResetDefaultMinionCommand : CommandData
{
    #region Protected Properties
    protected Requester _requester;
    protected List<int> _targetIDList;
    #endregion

    #region Constructors
    public ResetDefaultMinionCommand(Requester requester, List<int> targetIDList)
    {
        _targetIDList = new List<int>(targetIDList);
        _requester = requester;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            OnResetDefault(_targetIDList[0]);
        }
        else
        {
            OnActiveComplete();
        }
    }

    protected virtual void OnResetDefault(int uniqueID)
    {
        BattleCardData card;
        if (GameManager.Instance.FindBattleCard(uniqueID, out card))
        {
            card.ResetToDefault();
        }

        OnResetDefaultComplete();
    }

    protected void OnResetDefaultComplete()
    {
        _targetIDList.RemoveAt(0);
        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            OnResetDefault(_targetIDList[0]);
        }
        else
        {
            OnActiveComplete();
        }
    }

    public override string GetDebugText()
    {
        string idText = "";
        foreach (int id in _targetIDList)
        {
            if (idText == "")
            {
                idText += id.ToString();
            }
            else
            {
                idText += ", " + id.ToString();
            }
        }

        return string.Format(
              "[{0}]ResetDefaultMinion_{1}"
            , _uniqueKey
            , idText
        );
    }
    #endregion
}

public class ResetDefaultMinionInZoneCommand : ResetDefaultMinionCommand
{
    #region Protected Properties
    protected CardZone _zone;
    #endregion

    #region Constructors
    public ResetDefaultMinionInZoneCommand(Requester requester, List<int> targetIDList, CardZone zone) : base(requester, targetIDList)
    {
        _zone = zone;
    }
    #endregion

    #region Methods
    protected override void OnResetDefault(int uniqueID)
    {
        BattleCardData card;
        if (GameManager.Instance.FindBattleCard(uniqueID, out card))
        {
            if (card.CurrentZone == _zone)
            {
                Debug.LogFormat("ResetDefaultMinionInZoneCommand: {0}:{1}:{2}", card.UniqueID, card.Name, card.CurrentZone);

                card.ResetToDefault(_zone == CardZone.Battlefield);
            }
        }

        OnResetDefaultComplete();
    }

    public override string GetDebugText()
    {
        string idText = "";
        foreach (int id in _targetIDList)
        {
            if (idText == "")
            {
                idText += id.ToString();
            }
            else
            {
                idText += ", " + id.ToString();
            }
        }

        return string.Format(
              "[{0}]ResetDefaultMinionInZone_{1}_{2}"
            , _uniqueKey
            , idText
            , _zone
        );
    }
    #endregion
}

public class AddMinionDirTauntedCommand : CommandData
{
    #region Private Properties
    private int _bufferUniqueID;
    private int _battleCardUID;
    private CardDirection _buffDir;
    #endregion

    #region Constructors
    public AddMinionDirTauntedCommand(int bufferUniqueID, int targetIDList, CardDirection buffDir)
    {
        _bufferUniqueID = bufferUniqueID;
        _battleCardUID = targetIDList;
        _buffDir = buffDir;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;
        GameManager.Instance.ActionAddMinionDirTaunted(_bufferUniqueID, _battleCardUID, _buffDir, this.OnActiveComplete);
    }

    public override string GetDebugText()
    {
        string idText = _battleCardUID.ToString();

        return string.Format(
              "[{0}]AddMinionDirTaunted_{1}"
            , _uniqueKey
            , idText
        );
    }
    #endregion
}

public class RemoveMinionDirTauntedCommand : CommandData
{
    #region Private Properties
    private int _bufferUniqueID;
    private int _battleCardUID;
    private CardDirection _buffDir;
    #endregion

    #region Constructors
    public RemoveMinionDirTauntedCommand(int bufferUniqueID, int targetIDList, CardDirection buffDir)
    {
        _bufferUniqueID = bufferUniqueID;
        _battleCardUID = targetIDList;
        _buffDir = buffDir;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;
        GameManager.Instance.ActionRemoveMinionDirTauned(_bufferUniqueID, _battleCardUID, _buffDir, this.OnActiveComplete);
    }

    public override string GetDebugText()
    {
        string idText = _battleCardUID.ToString();

        return string.Format(
              "[{0}]RemoveMinionDirTaunted_{1}"
            , _uniqueKey
            , idText
        );
    }
    #endregion
}

public class AddMinionDirBuffPassiveCommand : CommandData
{
    #region Private Properties
    private int _bufferUniqueID;
    private List<int> _targetIDList;
    private CardDirection _buffDir;
    #endregion

    #region Constructors
    public AddMinionDirBuffPassiveCommand(int bufferUniqueID, List<int> targetIDList, CardDirection buffDir)
    {
        _bufferUniqueID = bufferUniqueID;
        _targetIDList = new List<int>(targetIDList);
        _buffDir = buffDir;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            OnAddDirPassiveBuff(_targetIDList[0]);
        }
        else
        {
            OnActiveComplete();
        }
    }

    private void OnAddDirPassiveBuff(int uniqueID)
    {
        GameManager.Instance.ActionAddMinionDirPassiveBuff(_bufferUniqueID, uniqueID, _buffDir, this.OnAddDirPassiveBuffComplete);
    }

    private void OnAddDirPassiveBuffComplete()
    {
        _targetIDList.RemoveAt(0);
        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            OnAddDirPassiveBuff(_targetIDList[0]);
        }
        else
        {
            OnActiveComplete();
        }
    }

    public override string GetDebugText()
    {
        string idText = _targetIDList.ToString();

        return string.Format(
              "[{0}]AddMinionDirBuff_{1}"
            , _uniqueKey
            , idText
        );
    }
    #endregion
}

public class AddMinionDirCommand : CommandData
{
    #region Private Properties
    private List<int> _targetIDList;
    private CardDirection _buffDir;
    #endregion

    #region Constructors
    public AddMinionDirCommand(List<int> targetIDList, CardDirection buffDir)
    {
        _targetIDList = new List<int>(targetIDList);
        _buffDir = buffDir;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            OnAddDirection(_targetIDList[0]);
        }
        else
        {
            OnActiveComplete();
        }
    }

    private void OnAddDirection(int uniqueID)
    {
        GameManager.Instance.ActionAddMinionDir(uniqueID, _buffDir, this.OnAddDirectionComplete);
    }

    private void OnAddDirectionComplete()
    {
        _targetIDList.RemoveAt(0);
        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            OnAddDirection(_targetIDList[0]);
        }
        else
        {
            OnActiveComplete();
        }
    }
    #endregion
}

public class SetMinionDirCommand : CommandData
{
    #region Private Properties
    private List<int> _targetIDList;
    private CardDirection _buffDir;
    #endregion

    #region Constructors
    public SetMinionDirCommand(List<int> targetIDList, CardDirection buffDir)
    {
        _targetIDList = new List<int>(targetIDList);
        _buffDir = buffDir;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            OnSetDirection(_targetIDList[0]);
        }
        else
        {
            OnActiveComplete();
        }
    }

    private void OnSetDirection(int uniqueID)
    {
        GameManager.Instance.ActionSetMinionDir(uniqueID, _buffDir, this.OnSetDirectionComplete);
    }

    private void OnSetDirectionComplete()
    {
        _targetIDList.RemoveAt(0);
        if (_targetIDList != null && _targetIDList.Count > 0)
        {
            OnSetDirection(_targetIDList[0]);
        }
        else
        {
            OnActiveComplete();
        }
    }
    #endregion
}

public class BuffFearCommand : CommandData
{
    #region Private Properties
    private Requester _requester;
    private int _uniqueID;
    private bool _isFear;
    #endregion

    #region Constructors
    public BuffFearCommand(Requester requester, int uniqueID, bool isFear)
    {
        _requester = requester;
        _uniqueID = uniqueID;
        _isFear = isFear;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;
        GameManager.Instance.ActionBuffFear(_requester, _uniqueID, _isFear, this.OnActiveComplete);
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]BuffFear_{1}_{2}"
            , _uniqueKey
            , _uniqueID
            , _isFear
        );
    }
    #endregion
}

public class BuffFreezeCommand : CommandData
{
    #region Private Properties
    private Requester _requester;
    private int _uniqueID;
    private bool _isFreeze;
    #endregion

    #region Constructors
    public BuffFreezeCommand(Requester requester, int uniqueID, bool isFreeze)
    {
        _requester = requester;
        _uniqueID = uniqueID;
        _isFreeze = isFreeze;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;
        GameManager.Instance.ActionBuffFreeze(_requester, _uniqueID, _isFreeze, OnActiveComplete);
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]BuffFreeze_{1}_{2}"
            , _uniqueKey
            , _uniqueID
            , _isFreeze
        );
    }
    #endregion
}

public class BuffSilenceCommand : CommandData
{
    #region Private Properties
    private Requester _requester;
    private int _uniqueID;
    private bool _isSilence;
    #endregion

    #region Constructors
    public BuffSilenceCommand(Requester requester, int uniqueID, bool isSilence)
    {
        _requester = requester;
        _uniqueID = uniqueID;
        _isSilence = isSilence;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;
        GameManager.Instance.ActionBuffSilence(_requester, _uniqueID, _isSilence, this.OnActiveComplete);
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]BuffSilence{1}_{2}"
            , _uniqueKey
            , _uniqueID
            , _isSilence
        );
    }
    #endregion
}

public class BuffStealthCommand : CommandData
{
    #region Private Properties
    private Requester _requester;
    private int _uniqueID;
    private bool _isStealth;
    #endregion

    #region Constructors
    public BuffStealthCommand(Requester requester, int uniqueID, bool isStealth)
    {
        _uniqueID = uniqueID;
        _isStealth = isStealth;
        _requester = requester;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;
        GameManager.Instance.ActionBuffStealth(_requester, _uniqueID, _isStealth, this.OnActiveComplete);
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]BuffStealth{1}_{2}"
            , _uniqueKey
            , _uniqueID
            , _isStealth
        );
    }
    #endregion
}

public class BuffInvincibleCommand : CommandData
{
    #region Private Properties
    private Requester _requester;
    private int _uniqueID;
    private bool _isInvincible;
    #endregion

    #region Constructors
    public BuffInvincibleCommand(Requester requester, int uniqueID, bool isImmune)
    {
        _uniqueID = uniqueID;
        _isInvincible = isImmune;
        _requester = requester;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;
        GameManager.Instance.ActionBuffInvincible(_requester, _uniqueID, _isInvincible, this.OnActiveComplete);
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]BuffInvincible{1}_{2}"
            , _uniqueKey
            , _uniqueID
            , _isInvincible
        );
    }
    #endregion
}

public class BuffUntargetableCommand : CommandData
{
    #region Private Properties
    private Requester _requester;
    private int _uniqueID;
    private bool _isUntargetable;
    #endregion

    #region Constructors
    public BuffUntargetableCommand(Requester requester, int uniqueID, bool isNotBeSelected)
    {
        _uniqueID = uniqueID;
        _isUntargetable = isNotBeSelected;
        _requester = requester;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;
        GameManager.Instance.ActionBuffUntargetable(_requester, _uniqueID, _isUntargetable, this.OnActiveComplete);
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]BuffUntargetable{1}_{2}"
            , _uniqueKey
            , _uniqueID
            , _isUntargetable
        );
    }
    #endregion
}

public class BuffEffectInvincibleCommand : CommandData
{
    #region Private Properties
    private Requester _requester;
    private int _uniqueID;
    private bool _isInvincible;
    #endregion

    #region Constructors
    public BuffEffectInvincibleCommand(Requester requester, int uniqueID, bool isImmune)
    {
        _uniqueID = uniqueID;
        _isInvincible = isImmune;
        _requester = requester;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;
        GameManager.Instance.ActionBuffEffectInvincible(_requester, _uniqueID, _isInvincible, this.OnActiveComplete);
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]BuffEffectInvincible{1}_{2}"
            , _uniqueKey
            , _uniqueID
            , _isInvincible
        );
    }
    #endregion
}

public class BuffPiercingCommand : CommandData
{
    #region Private Properties
    private Requester _requester;
    private int _uniqueID;
    private int _buffUniqueID;
    private bool _isPiercing;
    private int _range;
    #endregion

    #region Constructors
    public BuffPiercingCommand(Requester requester, int uniqueID, int buffUniqueID, bool isPiercing, int range)
    {
        _uniqueID = uniqueID;
        _buffUniqueID = buffUniqueID;
        _isPiercing = isPiercing;
        _requester = requester;
        _range = range;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;
        GameManager.Instance.ActionBuffPiercing(_requester, _uniqueID, _buffUniqueID, _isPiercing, _range, this.OnActiveComplete);
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]BuffPiercing{1}_{2}_{3}"
            , _uniqueKey
            , _uniqueID
            , _isPiercing
            , _range
        );
    }
    #endregion
}

public class BuffOverrideEffectDamageCommand : CommandData
{
    #region Private Properties
    private Requester _requester;
    private int _uniqueID;
    private int _buffUniqueID;
    private bool _isOverride;
    private int _damage;
    #endregion

    #region Constructors
    public BuffOverrideEffectDamageCommand(Requester requester, int uniqueID, int buffUniqueID, bool isOverride, int damage)
    {
        _uniqueID = uniqueID;
        _buffUniqueID = buffUniqueID;
        _requester = requester;
        _isOverride = isOverride;
        _damage = damage;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;
        GameManager.Instance.ActionBuffOverrideEffectDamage(_requester, _uniqueID, _buffUniqueID, _isOverride, _damage, this.OnActiveComplete);
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]BuffOverrideEffectDamage{1}_{2}_{3}"
            , _uniqueKey
            , _uniqueID
            , _isOverride
            , _damage
        );
    }
    #endregion
}

public class BuffMinionDarkMatterCommand : CommandData
{
    #region Private Properties
    private int _uniqueID;
    private bool _isDarkMatter;
    #endregion

    #region Constructors
    public BuffMinionDarkMatterCommand(int uniqueID, bool isDarkMatter)
    {
        _uniqueID = uniqueID;
        _isDarkMatter = isDarkMatter;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;
        GameManager.Instance.ActionBuffMinionDarkMatter(_uniqueID, _isDarkMatter, this.OnActiveComplete);
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]BuffMinionDarkMatter{1}_{2}"
            , _uniqueKey
            , _uniqueID
            , _isDarkMatter
        );
    }
    #endregion
}

public class BuffSlotLockCommand : CommandData
{
    #region Private Properties
    private int _slotID;
    private int _counterAmount;
    private bool _isLock;
    private PlayerIndex _owner;
    #endregion

    #region Constructors
    public BuffSlotLockCommand(int slotID, int counterAmount, PlayerIndex owner, bool isLock)
    {
        _slotID = slotID;
        _counterAmount = counterAmount;
        _isLock = isLock;
        _owner = owner;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;
        GameManager.Instance.ActionBuffSlotLock(_slotID, _counterAmount, _isLock, _owner, this.OnActiveComplete);
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]BuffSlotEnabled_{1}_{2}"
            , _uniqueKey
            , _slotID
            , _isLock
        );
    }
    #endregion
}

public class BuffSlotDarkMatterCommand : CommandData
{
    #region Private Properties
    private int _slotID;
    private bool _isDarkMatter;
    private PlayerIndex _owner;
    private Requester _requester;
    #endregion

    #region Constructors
    public BuffSlotDarkMatterCommand(Requester requester, int slotID, bool isDarkMatter, PlayerIndex owner)
    {
        _slotID = slotID;
        _isDarkMatter = isDarkMatter;
        _owner = owner;
        _requester = requester;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;
        GameManager.Instance.ActionBuffSlotDarkMatter(_slotID, _isDarkMatter, _owner, this.OnActiveComplete);

        if (_isDarkMatter)
        {
            // Count PlayerStat
            PlayerStatManager.Instance.DoActSpawnDarkMatterEvent(_requester.Minion, null);
        }
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]BuffSlotDarkMatter_{1}_{2}"
            , _uniqueKey
            , _slotID
            , _isDarkMatter
        );
    }
    #endregion
}

public class BuffEmptyCommand : CommandData
{
    #region Private Properties
    private Requester _requester;
    private int _uniqueID;
    #endregion

    #region Constructors
    public BuffEmptyCommand(Requester requester, int uniqueID)
    {
        _uniqueID = uniqueID;
        _requester = requester;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;
        GameManager.Instance.ActionBuffEmpty(_requester, _uniqueID, this.OnActiveComplete);
    }

    public override string GetDebugText()
    {
        return string.Format(
            "[{0}]BuffEmpty{1}"
            , _uniqueKey
            , _uniqueID
        );
    }
    #endregion
}

public class PrepareAbilityCommand : CommandData
{
    #region Private Properties
    private int _uniqueCardID;
    private int _uniqueAbilityID;
    private Requester _triggerer;
    private EventKey _triggerKey;
    private BattleCardData _battleCard;
    #endregion

    #region Constructors
    public PrepareAbilityCommand(int uniqueCardID, int uniqueAbilityID, Requester triggerer, EventKey triggerKey)
    {
        _uniqueCardID = uniqueCardID;
        _uniqueAbilityID = uniqueAbilityID;
        _triggerer = triggerer;
        _triggerKey = triggerKey;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        if (GameManager.Instance.FindBattleCard(_uniqueCardID, out _battleCard))
        {
            if (GameManager.Instance.IsNetworkPlay())
            {
                if (!GameManager.Instance.IsLocalPlayer(_battleCard.Owner))
                {
                    switch (_battleCard.Type)
                    {
                        case CardType.Minion:
                        case CardType.Minion_NotInDeck:
                            {
                                MinionData minion = _battleCard as MinionData;
                                minion.SetPrepareAbilityProperties(
                                      _uniqueAbilityID
                                    , _triggerer
                                    , _triggerKey
                                );
                            }
                            break;

                        case CardType.Spell:
                        case CardType.Spell_NotInDeck:
                            {
                                SpellData spell = _battleCard as SpellData;
                                spell.SetPrepareAbilityProperties(
                                      _uniqueAbilityID
                                    , _triggerer
                                    , _triggerKey
                                );
                            }
                            break;
                    }

                    Debug.LogFormat("PrepareAbilityCommand/NotMine:");

                    // If get action BEFORE start prepare. Try checking...
                    GameManager.Instance.ActionReadyActionAbility();

                    OnActiveComplete();
                    return;
                }
            }

            Debug.LogFormat("PrepareAbilityCommand/Mine:");
            CommandQueue.Instance.SetPause(true);

            switch (_battleCard.Type)
            {
                case CardType.Minion:
                case CardType.Minion_NotInDeck:
                    {
                        MinionData minion = _battleCard as MinionData;
                        minion.PrepareAbility(
                              _uniqueAbilityID
                            , _triggerer
                            , _triggerKey
                            , OnPrepareComplete
                            , OnPrepareFail
                        );
                    }
                    break;

                case CardType.Spell:
                case CardType.Spell_NotInDeck:
                    {
                        SpellData spell = _battleCard as SpellData;
                        spell.PrepareAbility(
                              _uniqueAbilityID
                            , _triggerer
                            , _triggerKey
                            , OnPrepareComplete
                            , OnPrepareFail
                        );
                    }
                    break;
            }
        }
        else
        {
            Debug.LogErrorFormat("PrepareAbilityCommand/Active: Not found battle card. {0}", _uniqueCardID);

            // Not found
            OnActiveComplete(); // Ignore
        }
    }

    private void OnPrepareComplete(Dictionary<string, string> paramList)
    {
        System.Random r = new System.Random();
        int randomSeed = r.Next(0, MatchPlayerData.MaxRandomSeed);

        if (GameManager.Instance.IsNetworkPlay())
        {
            if (GameManager.Instance.IsLocalPlayer(_battleCard.Owner))
            {
                GameManager.Instance.PhotonRequestActionAbility(
                      _uniqueCardID
                    , _uniqueAbilityID
                    , randomSeed
                    , paramList
                );
            }
        }
        else
        {
            GameManager.Instance.RequestActionAbility(
                  _uniqueCardID
                , _uniqueAbilityID
                , paramList
                , randomSeed
                , true
            );

            Debug.LogFormat("PrepareAbilityCommand/OnPrepareComplete");
            CommandQueue.Instance.SetPause(false);
        }

        OnActiveComplete();
    }

    private void OnPrepareFail()
    {
        if (GameManager.Instance.IsNetworkPlay())
        {
            if (GameManager.Instance.IsLocalPlayer(_battleCard.Owner))
            {
                Debug.LogFormat("PrepareAbilityCommand/OnPrepareFail:");
                GameManager.Instance.PhotonRequestPrepareFailCommand();
            }
        }
        else
        {
            Debug.LogFormat("PrepareAbilityCommand/OnPrepareFail:");
            CommandQueue.Instance.SetPause(false);
        }

        OnActiveComplete();
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]PrepareAbility_{1}_{2}"
            , _uniqueKey
            , _uniqueCardID
            , _uniqueAbilityID
        );
    }
    #endregion
}

public class ActionAbilityCommand : CommandData
{
    #region Private Properties
    private int _uniqueCardID;
    private int _uniqueAbilityID;
    private Dictionary<string, string> _paramList;
    private int _randomSeed;
    private BattleCardData _battleCard;
    #endregion

    #region Constructors
    public ActionAbilityCommand(int uniqueCardID, int uniqueAbilityID, Dictionary<string, string> paramList, int randomSeed)
    {
        _uniqueCardID = uniqueCardID;
        _uniqueAbilityID = uniqueAbilityID;
        _paramList = paramList;
        _randomSeed = randomSeed;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        if (GameManager.Instance.FindBattleCard(_uniqueCardID, out _battleCard))
        {
            GameManager.Instance.SetRandomSeed(_randomSeed);

            _battleCard.ActiveAbility(_uniqueAbilityID
                                    , _paramList
                                    , OnActionComplete
            );
        }
        else
        {
            // Not found
            OnActionComplete();
        }
    }

    private void OnActionComplete()
    {
        OnActiveComplete();
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]ActionAbility_{1}_{2}"
            , _uniqueKey
            , _uniqueCardID
            , _uniqueAbilityID
        );
    }
    #endregion
}

public class AddMinionBuffCommand : CommandData
{
    #region Private Properties
    private int _uniqueCardID;
    private MinionBuffData _buffData;
    private MinionData _minion;
    #endregion

    #region Constructors
    public AddMinionBuffCommand(int uniqueCardID, MinionBuffData buffData)
    {
        _uniqueCardID = uniqueCardID;
        _buffData = buffData;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        if (GameManager.Instance.FindMinionBattleCard(_uniqueCardID, out _minion))
        {
            _minion.AddBuff(_buffData, OnActionComplete);
        }
        else
        {
            // Not found
            OnActionComplete();
        }
    }

    private void OnActionComplete()
    {
        OnActiveComplete();
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]AddMinionBuff_{1}_{2}"
            , _uniqueKey
            , _uniqueCardID
            , _buffData
        );
    }
    #endregion
}

public class UpdateMinionBuffCommand : CommandData
{
    #region Private Properties
    private int _uniqueCardID;
    private string _buffKey;
    private MinionData _minion;
    #endregion

    #region Constructors
    public UpdateMinionBuffCommand(int uniqueCardID, string buffKey)
    {
        _uniqueCardID = uniqueCardID;
        _buffKey = buffKey;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        if (GameManager.Instance.FindMinionBattleCard(_uniqueCardID, out _minion))
        {
            _minion.UpdateBuff(_buffKey, OnActionComplete);
        }
        else
        {
            // Not found
            OnActionComplete();
        }
    }

    private void OnActionComplete()
    {
        OnActiveComplete();
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]UpdateMinionBuff_{1}_{2}"
            , _uniqueKey
            , _uniqueCardID
            , _buffKey
        );
    }
    #endregion
}

public class RemoveMinionBuffCommand : CommandData
{
    #region Private Properties
    private int _uniqueCardID;
    private string _key;
    private MinionData _minion;
    #endregion

    #region Constructors
    public RemoveMinionBuffCommand(int uniqueCardID, string key)
    {
        _uniqueCardID = uniqueCardID;
        _key = key;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        if (GameManager.Instance.FindMinionBattleCard(_uniqueCardID, out _minion))
        {
            _minion.RemoveBuff(_key, OnActionComplete);
        }
        else
        {
            // Not found
            OnActionComplete();
        }
    }

    private void OnActionComplete()
    {
        OnActiveComplete();
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]RemoveMinionBuff_{1}_{2}"
            , _uniqueKey
            , _uniqueCardID
            , _key
        );
    }
    #endregion
}

public class RemoveMinionBuffTypeCommand : CommandData
{
    #region Private Properties
    private int _uniqueCardID;
    private MinionBuffTypes _type;
    private MinionData _minion;
    #endregion

    #region Constructors
    public RemoveMinionBuffTypeCommand(int uniqueCardID, MinionBuffTypes type)
    {
        _uniqueCardID = uniqueCardID;
        _type = type;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        if (GameManager.Instance.FindMinionBattleCard(_uniqueCardID, out _minion))
        {
            _minion.RemoveBuff(_type, OnActionComplete);
        }
        else
        {
            // Not found
            OnActionComplete();
        }
    }

    private void OnActionComplete()
    {
        OnActiveComplete();
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]RemoveMinionBuffType_{1}_{2}"
            , _uniqueKey
            , _uniqueCardID
            , _type
        );
    }
    #endregion
}

public class AddSlotBuffCommand : CommandData
{
    #region Private Properties
    private int _slotID;
    private SlotBuffData _buffData;

    private SlotData _slot;
    #endregion

    #region Constructors
    public AddSlotBuffCommand(int slotID, SlotBuffData buffData)
    {
        _slotID = slotID;
        _buffData = buffData;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        if (_slotID < GameManager.Instance.Data.SlotDatas.Length && _slotID >= 0)
        {
            GameManager.Instance.Data.SlotDatas[_slotID].AddBuff(_buffData, OnActionComplete);
        }
        else
        {
            // Not found
            OnActionComplete();
        }
    }

    private void OnActionComplete()
    {
        OnActiveComplete();
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]AddSlotBuff_{1}_{2}"
            , _uniqueKey
            , _slotID
            , _buffData
        );
    }
    #endregion
}

public class RemoveSlotBuffCommand : CommandData
{
    #region Private Properties
    private int _slotID;
    private string _key;

    private MinionData _card;
    #endregion

    #region Constructors
    public RemoveSlotBuffCommand(int slotID, string key)
    {
        _slotID = slotID;
        _key = key;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        if (_slotID < GameManager.Instance.Data.SlotDatas.Length && _slotID >= 0)
        {
            GameManager.Instance.Data.SlotDatas[_slotID].RemoveBuff(_key, OnActionComplete);
        }
        else
        {
            // Not found
            OnActionComplete();
        }
    }

    private void OnActionComplete()
    {
        OnActiveComplete();
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]RemoveSlotBuff_{1}_{2}"
            , _uniqueKey
            , _slotID
            , _key
        );
    }
    #endregion
}

public class RemoveSlotBuffTypeCommand : CommandData
{
    #region Private Properties
    private int _slotID;
    private SlotBuffTypes _type;

    private MinionData _card;
    #endregion

    #region Constructors
    public RemoveSlotBuffTypeCommand(int slotID, SlotBuffTypes type)
    {
        _slotID = slotID;
        _type = type;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        if (_slotID < GameManager.Instance.Data.SlotDatas.Length && _slotID >= 0)
        {
            GameManager.Instance.Data.SlotDatas[_slotID].RemoveBuff(_type, OnActionComplete);
        }
        else
        {
            // Not found
            OnActionComplete();
        }
    }

    private void OnActionComplete()
    {
        OnActiveComplete();
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]RemoveSlotBuffType_{1}_{2}"
            , _uniqueKey
            , _slotID
            , _type
        );
    }
    #endregion
}

public class UpdateLinkOnSlotCommand : CommandData
{
    #region Private Properties
    private int _target;

    private MinionData _card;
    #endregion

    #region Constructors
    public UpdateLinkOnSlotCommand(int target)
    {
        _target = target;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;
        OnActive();
    }

    private void OnActive()
    {
        if (_target >= 0 && _target < GameManager.Instance.BoardSize)
        {
            GameManager.Instance.ActionUpdateShowLinkDirOnSlot(_target, OnActionComplete);
        }
        else
        {
            // Not found
            OnActionComplete();
        }
    }

    private void OnActionComplete()
    {
        OnActiveComplete();
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]UpdateJoinOnSlot_{1}"
            , _uniqueKey
            , _target
        );
    }
    #endregion
}

public class AddMinionToAttackerListCommand : CommandData
{
    #region Private Properties
    private MinionData _minion;
    #endregion

    #region Constructors
    public AddMinionToAttackerListCommand(MinionData minion)
    {
        _minion = minion;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;
        OnActive();
    }

    private void OnActive()
    {
        if (_minion != null)
        {
            GameManager.Instance.ActionAddAttackerList(_minion, OnActionComplete);
        }
        else
        {
            OnActionComplete();
        }
    }

    private void OnActionComplete()
    {
        OnActiveComplete();
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]AddMinionToAttackerList_{1}"
            , _uniqueKey
            , _minion.UniqueID
        );
    }
    #endregion
}

public class RemoveMinionFromAttackerListCommand : CommandData
{
    #region Private Properties
    private MinionData _minion;
    #endregion

    #region Constructors
    public RemoveMinionFromAttackerListCommand(MinionData minion)
    {
        _minion = minion;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;
        OnActive();
    }

    private void OnActive()
    {
        if (_minion != null)
        {
            GameManager.Instance.ActionRemoveAttackerFromList(_minion, OnActionComplete);
        }
        else
        {
            OnActionComplete();
        }
    }

    private void OnActionComplete()
    {
        OnActiveComplete();
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]RemoveMinionFromAttackerList_{1}"
            , _uniqueKey
            , _minion.UniqueID
        );
    }
    #endregion
}

public class WaitCommand : CommandData
{
    #region Private Properties
    private float _waitTime = 0.0f;
    private WaitObject _waitObj;
    #endregion

    #region Constructors
    public WaitCommand(float waitTime)
    {
        _waitTime = waitTime;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;
        if (_waitTime > 0.0f)
        {
            UtilityItem item = UtilityPool.Instance.CreateItem(UtilityPool.UtilityType.WaitObject);
            if (item != null)
            {
                _waitObj = item.GetComponent<WaitObject>();
                if (_waitObj != null)
                {
                    _waitObj.name = "WaitObject_" + _uniqueKey.ToString();
                    _waitObj.StartTimer(_waitTime, this.OnActiveComplete);
                    return;
                }
            }
        }

        OnActiveComplete();
    }

    public override string GetDebugText()
    {
        if (_waitObj != null && _waitObj.IsStart)
        {
            return string.Format(
                  "[{0}]{1}_{2}"
                , _uniqueKey
                , this.GetType().ToString()
                , _waitObj.Timer.ToString("0.00")
            );
        }
        else
        {
            return string.Format(
                  "[{0}]{1}"
                , _uniqueKey
                , this.GetType().ToString()
            );
        }
    }
    #endregion
}

public class PauseCommand : CommandData
{
    #region Private Properties
    private bool _isPause;
    #endregion

    #region Constructors
    public PauseCommand(bool isPause)
    {
        _isPause = isPause;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        CommandQueue.Instance.SetPause(_isPause);

        OnActiveComplete();
    }

    public override string GetDebugText()
    {
        return string.Format(
              "[{0}]PauseCommand_{1}"
            , _uniqueKey
            , _isPause
        );
    }
    #endregion
}
#endregion
