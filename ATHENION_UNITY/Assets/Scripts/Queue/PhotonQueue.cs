﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

#region Photon Queue Class
public class PhotonQueue : Singleton<PhotonQueue>
{
    #region Public Properties
    //public int Count { get { if (_queue != null) return _queue.Count; else return 0; } }
    #endregion

    #region Private Properties
    private int _uniqueIndex = 0;
    private Dictionary<int, PhotonData> _dict = null;
    #endregion

    #region Methods
    public void Init()
    {
        _dict = new Dictionary<int, PhotonData>();
    }

    public void Add(int requestIndex, PhotonData data)
    {
        if (_dict == null)
        {
            Init();
        }

        _dict.Add(requestIndex, data);
    }

    public void Active(int requestIndex)
    {
        if (_dict != null && _dict.ContainsKey(requestIndex))
        {
            _dict[requestIndex].Active();
        }
    }

    public bool ContainKey(int key)
    {
        if (_dict != null && _dict.ContainsKey(key))
        {
            return true;
        }

        return false;
    }

    public int RequestUniqueIndex()
    {
        return _uniqueIndex++;
    }

    public virtual void Clear()
    {
        if (_dict != null)
        {
            _dict.Clear();
        }
        _uniqueIndex = 0;
    }
    #endregion
}
#endregion

#region Photon Data
public class PhotonData
{
    #region Public Properties
    public int UniqueKey { get { return _uniqueKey; } }
    #endregion

    #region Protected Properties
    protected int _uniqueKey = 0;
    protected UnityAction _request;
    #endregion

    #region Constructors
    public PhotonData(UnityAction request)
    {
        _uniqueKey = 0;
        _request = request;
    }

    public PhotonData(int uniqueKey, UnityAction request)
    {
        _uniqueKey = uniqueKey;
        _request = request;
    }
    #endregion

    #region Methods
    public virtual void Active()
    {
        if (_request != null)
        {
            _request.Invoke();
        }
    }

    public void SetUniqueKey(int uniqueKey)
    {
        _uniqueKey = uniqueKey;
    }
    #endregion
}
#endregion