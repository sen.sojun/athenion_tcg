﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.Events;

//#region Battle Queue Class
//public class BattleQueue : GameQueue<BattleQueue, BattleQueueData>
//{

//}
//#endregion

//#region Battle Data
//public abstract class BattleQueueData : QueueData
//{
//    #region Constructors
//    #endregion

//    #region Methods
//    public override void Active(UnityAction<int> onComplete)
//    {
//        _onComplete = onComplete;

//        // Please call this when complete.
//        OnActiveComplete();
//    }

//    public override string GetDebugText()
//    {
//        return string.Format("[{0}]{1}", _uniqueKey, this.GetType().ToString());
//    }
//    #endregion
//}



//public class BuffBattleMinionATKQueue : BattleQueueData
//{
//    #region Private Properties
//    private int _minionUniqueID;
//    private int _atkValue;
//    #endregion

//    #region Constructors
//    public BuffBattleMinionATKQueue(int minionUniqueID, int atkValue)
//    {
//        _minionUniqueID = minionUniqueID;
//        _atkValue = atkValue;
//    }
//    #endregion

//    #region Methods
//    public override void Active(UnityAction<int> onComplete)
//    {
//        _onComplete = onComplete;
//        BattleManager.Instance.ActionBuffBattleMinionATK(_minionUniqueID, _atkValue, this.OnActiveComplete);

//    }

//    public override string GetDebugText()
//    {
//        string idText = _minionUniqueID.ToString();

//        return string.Format(
//              "[{0}]BuffBattleMinionATKQueue_{1}_{2}"
//            , _uniqueKey
//            , _atkValue
//            , idText
//        );
//    }
//    #endregion
//}
//#endregion

//#region WaitBattle
//public class WaitBattle : BattleQueueData
//{
//    #region Private Properties
//    private float _waitTime = 0.0f;
//    private WaitObject _waitObj;
//    #endregion

//    #region Constructors
//    public WaitBattle(float waitTime)
//    {
//        _waitTime = waitTime;
//    }
//    #endregion

//    #region Methods
//    public override void Active(UnityAction<int> onComplete)
//    {
//        _onComplete = onComplete;
//        if (_waitTime > 0.0f)
//        {
//            GameObject obj = new GameObject();

//            _waitObj = obj.AddComponent<WaitObject>();
//            _waitObj.name = "WaitObject_" + _uniqueKey.ToString();
//            _waitObj.Init(_waitTime, this.OnActiveComplete);
//        }
//        else
//        {
//            OnActiveComplete();
//        }
//    }

//    public override string GetDebugText()
//    {
//        if (_waitObj != null && _waitObj.IsWait)
//        {
//            return string.Format(
//                  "[{0}]{1}_{2}"
//                , _uniqueKey
//                , this.GetType().ToString()
//                , _waitObj.Timer.ToString("0.00")
//            );
//        }
//        else
//        {
//            return string.Format(
//                  "[{0}]{1}"
//                , _uniqueKey
//                , this.GetType().ToString()
//            );
//        }
//    }
//    #endregion
//}
//#endregion