﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Karamucho;

public abstract class UtilityItem : PoolItem
{
    #region Public Properties
    public UtilityPool.UtilityType Type { get; protected set; }
    #endregion

    #region Methods
    public void SetPopupType(UtilityPool.UtilityType type)
    {
        Type = type;
    }
    #endregion
}

public class UtilityPool : PrefabPool<UtilityPool, UtilityItem>
{
    protected string RootPrefabPath = "Prefabs/Pool/Utility/";

    public enum UtilityType
    {
          WaitObject
        , TaskLoader
        , DelayObject
    }

    public override sealed UtilityItem CreateItem()
    {
        // Hide public reference.
        return base.CreateItem();
    }

    public UtilityItem CreateItem(UtilityType utilityType)
    {
        if (!IsInit) Init();

        for (int i = 0; i < _pool.Count; i++)
        {
            UtilityItem item = (UtilityItem)_pool[i];
            if (item != null && !item.IsLock && item.Type == utilityType)
            {
                item.SetLock(true);
                item.GetGameObject().SetActive(true);
                return _pool[i];
            }
        }

        //Debug.Log("PrefabPath : " + GetPrefabPath(vfxType));
        GameObject go = MonoBehaviour.Instantiate(ResourceManager.Load(GetPrefabPath(utilityType))) as GameObject;
        UtilityItem component = go.GetComponent<UtilityItem>();
        component.SetPopupType(utilityType);

        _pool.Add(component);
        component.SetLock(true);

        return component;
    }

    protected string GetPrefabPath(UtilityType type)
    {
        return RootPrefabPath + type.ToString();
    }
}
