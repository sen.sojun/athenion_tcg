﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using Karamucho;

public interface IPoolItem
{
    bool IsLock { get; }
    void SetLock(bool isLock);
    GameObject GetGameObject();
}

public abstract class PoolItem : MonoBehaviour, IPoolItem
{
    #region Public Properties
    public bool IsLock { get { return _isLock; } }
    #endregion

    #region Private Properties
    private bool _isLock = false;
    #endregion

    #region Methods
    public void SetLock(bool isLock)
    {
        _isLock = isLock;
    }
    public GameObject GetGameObject()
    {
        return gameObject;
    }
    #endregion
}

public abstract class UIInputEventPoolItem : UIInputEvent, IPoolItem
{
    #region Public Properties
    public bool IsLock { get { return _isLock; } }
    #endregion

    #region Private Properties
    private bool _isLock = false;
    #endregion

    #region Methods
    public void SetLock(bool isLock)
    {
        _isLock = isLock;
    }
    public GameObject GetGameObject()
    {
        return gameObject;
    }
    #endregion
}

public abstract class PrefabPool<TClass, TItem> : Singleton<TClass> where TClass : new() where TItem : MonoBehaviour, IPoolItem
{
    #region Public Properties
    public string PrefabPath = "";
    public bool IsInit { get { return _isInit; } }
    #endregion

    #region Protected Properties
    protected bool _isInit = false;
    protected List<TItem> _pool = new List<TItem>();
    protected StringBuilder _sb;
    #endregion

    #region Constructors
    ~PrefabPool()
    {
        _instance = default(TClass);
    }
    #endregion

    #region Methods
    public virtual void Init()
    {
        _isInit = true;
    }

    public virtual TItem CreateItem()
    {
        if (!IsInit) Init();

        for (int i = 0; i < _pool.Count; i++)
        {
            IPoolItem item = (IPoolItem)_pool[i];
            if (item != null && !item.IsLock)
            {
                item.SetLock(true);
                item.GetGameObject().SetActive(true);
                return _pool[i];
            }
        }
        //Debug.Log("PrefabPath : " + PrefabPath);
        GameObject go = MonoBehaviour.Instantiate(ResourceManager.Load(PrefabPath)) as GameObject;
        TItem component = go.GetComponent<TItem>();

        _pool.Add(component);
        component.SetLock(true);

        return component;
    }

    public virtual void ReleaseItem(TItem component)
    {
        if (!IsInit) Init();

        component.gameObject.transform.SetParent(null);
        component.gameObject.SetActive(false);
        component.gameObject.transform.position = Vector3.zero - Vector3.up * 5000.0f; // out of screen.
        component.SetLock(false);
    }

    public void Clear()
    {
        if (_pool != null)
        {
            for(int index = 0; index <_pool.Count; ++index)
            {
                if (_pool[index] == null)
                {
                    // already destroyed
                }
                else
                {
                    MonoBehaviour.Destroy(_pool[index].gameObject);
                }
            }
            _pool.Clear();
        }
        _isInit = false;
    }

    public virtual string GetDebugText()
    {
        if (_sb == null) _sb = new StringBuilder();
        _sb.Clear();

        int activeCount = 0;
        int inactiveCount = 0;
        foreach (TItem item in _pool)
        {
            if (item.IsLock)
            {
                activeCount++;
            }
            else
            {
                inactiveCount++;
            }
        }
        _sb.Append(this.GetType().ToString());
        _sb.AppendFormat("\nItem: {0}/{1}", activeCount, activeCount + inactiveCount);

        return _sb.ToString();
    }
    #endregion
}
