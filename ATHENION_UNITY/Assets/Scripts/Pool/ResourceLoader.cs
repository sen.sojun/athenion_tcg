﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ResourceLoader : MonoBehaviour 
{
    /*
	// Use this for initialization
	void Start () 
    {	
	}
	
	// Update is called once per frame
	void Update () 
    {	
	}
    */

    public void StartAsyncLoad<T>(string path, UnityAction<T> onLoaded) where T : Object
    {
        DontDestroyOnLoad(gameObject);
        StartCoroutine(DoAsyncLoad<T>(path, onLoaded));
    }

    private IEnumerator DoAsyncLoad<T>(string path, UnityAction<T> onLoaded) where T : Object
    {
        ResourceRequest rq = Resources.LoadAsync<T>(path);

        while (!rq.isDone)
        {
            yield return 0;
        }

        T obj = (T)rq.asset;

        if (onLoaded != null)
        {
            onLoaded.Invoke(obj);
        }

        Destroy(gameObject);
    }
}
