﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Karamucho;

public abstract class ActionLogItem : PoolItem
{
    #region Public Properties
    public int Index { get; protected set; }
    public ActionLogPool.LogType Type { get; protected set; }
    #endregion

    #region Methods
    public void SetIndex(int index)
    {
        Index = index;
    }

    public void SetLogType(ActionLogPool.LogType type)
    {
        Type = type;
    }
    #endregion
}

public class ActionLogPool : PrefabPool<ActionLogPool, ActionLogItem>
{
    protected string RootPrefabPath = "Prefabs/Pool/ActionLog";

    public enum LogType
    {
          Action
        , Phase 
        , Summary
    }

    public override sealed ActionLogItem CreateItem()
    {
        // Hide public reference.
        return base.CreateItem();
    }

    public ActionLogItem CreateItem(LogType logType, Transform parent)
    {
        if (!IsInit) Init();

        for (int i = 0; i < _pool.Count; i++)
        {
            ActionLogItem item = (ActionLogItem)_pool[i];
            if (item != null && !item.IsLock && item.Type == logType)
            {
                if (parent != null)
                {
                    item.transform.SetParent(parent);
                }
                item.SetLock(true);
                item.GetGameObject().SetActive(true);
                return _pool[i];
            }
        }

        GameObject go = MonoBehaviour.Instantiate(ResourceManager.Load(GetPrefabPath(logType)), parent) as GameObject;
        ActionLogItem component = go.GetComponent<ActionLogItem>();
        component.SetLogType(logType);

        _pool.Add(component);
        component.SetLock(true);

        return component;
    }

    protected string GetPrefabPath(LogType type)
    {
        switch (type)
        {
            case LogType.Action:
            case LogType.Phase:
            case LogType.Summary:
            default:
            {
                return string.Format("{0}/Log_{1}", RootPrefabPath, type);
            }
        }
    }
}
