﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

namespace Karamucho
{
    public class ResourceManager
    {
        #region Private Properties
        private static int _completePreloadCardCount = 0;
        private static Dictionary<string, Object> _resourceList = null;
        private static Dictionary<string, Object[]> _resourceArrayList = null;
        #endregion

        #region Methods
        public static void PreloadCard(UnityAction onComplete)
        {
            List<CardDBData> cardList;
            CardDB.Instance.GetAllData(out cardList);

            string rootPath = "Images/Prefabs/Cards/";
            _completePreloadCardCount = cardList.Count;

            foreach (CardDBData cardDBData in cardList)
            {
                if (cardDBData.ImageKey != "-")
                {
                    string fileName = string.Format("CARD_{0}", cardDBData.ImageKey);
                    LoadAsync<Object>(
                          (rootPath + fileName).ToLower()
                        , delegate (Object obj)
                        {
                            _completePreloadCardCount--;
                            if (_completePreloadCardCount <= 0)
                            {
                                if (onComplete != null)
                                {
                                    onComplete.Invoke();
                                }
                            }
                        }
                    );
                }
                else
                {
                    _completePreloadCardCount--;
                }
            }
        }

        public static void Clear()
        {
            if (_resourceList != null)
            {
                _resourceList.Clear();
            }

            Resources.UnloadUnusedAssets();
        }

        /*
        public static void ClearAsync()
        {
            GameObject obj = new GameObject();
            CoroutineObject coroutineObject = obj.AddComponent<CoroutineObject>();

            coroutineObject.StartTask(IEClearAsync());
        }

        private static IEnumerator IEClearAsync()
        {
            List<string> keyList = new List<string>(_resourceList.Keys);

            foreach (string key in keyList)
            {
                Resources.UnloadAsset(_resourceList[key]);
                yield return new WaitForEndOfFrame();
            }

            if (_resourceList != null)
            {
                _resourceList.Clear();
            }
            yield break;
        }
        */

        #region Load
        public static Object Load(string path)
        {
            string pathLower = path.ToLower();
            Object obj = null;

            // Try find from resource cache list.
            if (_resourceList != null && _resourceList.ContainsKey(pathLower))
            {
                bool isFound = _resourceList.TryGetValue(pathLower, out obj);
                if (!isFound)
                {
                    obj = null;
                }
            }

            // Try load from asset bundle.
            if (obj == null)
            {
                obj = LoadFromAssetBundle(pathLower);
            }

            // Try load from resource.
            if (obj == null)
            {
                obj = Resources.Load(pathLower);
            }

            if (obj != null)
            {
                //Debug.Log("ResourceManager/Load: Load " + path);

                // cache obj into _resourceList.
                if (_resourceList == null)
                {
                    _resourceList = new Dictionary<string, Object>();
                }
                if (_resourceList.ContainsKey(pathLower))
                {
                    _resourceList[pathLower] = obj; // update.
                }
                else
                {
                    _resourceList.Add(pathLower, obj); // cache object.
                }

                return obj;
            }

            return null;
        }

        public static T Load<T>(string path) where T : Object
        {
            string pathLower = path.ToLower();
            Object obj = null;

            // Try find from resouce cache list.
            if (_resourceList != null && _resourceList.ContainsKey(pathLower))
            {
                bool isFound = _resourceList.TryGetValue(pathLower, out obj);
                if (!isFound)
                {
                    obj = null;
                }
            }

            // Try load from asset bundle.
            if (obj == null)
            {
                obj = LoadFromAssetBundle<T>(pathLower);
            }

            // Try load from resource.
            if (obj == null)
            {
                obj = Resources.Load<T>(pathLower);
            }

            if (obj != null)
            {
                //Debug.Log("ResourceManager/Load: Load " + path);

                // cache obj into _resourceList.
                if (_resourceList == null)
                {
                    _resourceList = new Dictionary<string, Object>();
                }
                if (_resourceList.ContainsKey(pathLower))
                {
                    _resourceList[pathLower] = obj; // update.
                }
                else
                {
                    _resourceList.Add(pathLower, obj); // cache object.
                }

                // Convert Texture2D to Sprite.
                if (obj.GetType() == typeof(UnityEngine.Texture2D) && typeof(T) == typeof(Sprite))
                {
                    Texture2D tex = obj as Texture2D;
                    Sprite newSprite = GameHelper.ConvertTexture2DToSprite(tex);

                    return (newSprite as T);
                }
                else
                {
                    return (obj as T);
                }
            }

            return null;
        }

        public static Object[] LoadAll(string path)
        {
            string pathLower = path.ToLower();
            Object[] obj = null;

            // Try find from resource cache list.
            if (_resourceArrayList != null && _resourceArrayList.ContainsKey(pathLower))
            {
                bool isFound = _resourceArrayList.TryGetValue(pathLower, out obj);
                if (!isFound)
                {
                    obj = null;
                }
            }

            // Try load from asset bundle.
            if (obj == null)
            {
                obj = LoadAllFromAssetBundle(pathLower);
            }

            // Try load from resource.
            if (obj == null)
            {
                obj = Resources.LoadAll(pathLower);
            }

            if (obj != null)
            {
                //Debug.Log("ResourceManager/Load: Load " + path);

                // cache obj into _resourceList.
                if (_resourceArrayList == null)
                {
                    _resourceArrayList = new Dictionary<string, Object[]>();
                }
                if (_resourceArrayList.ContainsKey(pathLower))
                {
                    _resourceArrayList[pathLower] = obj; // update.
                }
                else
                {
                    _resourceArrayList.Add(pathLower, obj); // cache object.
                }

                return obj;
            }

            return null;
        }

        public static T[] LoadAll<T>(string path) where T : Object
        {
            string pathLower = path.ToLower();
            Object[] obj = null;

            // Try find from resouce cache list.
            if (_resourceArrayList != null && _resourceArrayList.ContainsKey(pathLower))
            {
                bool isFound = _resourceArrayList.TryGetValue(pathLower, out obj);
                if (!isFound)
                {
                    obj = null;
                }
            }

            // Try load from asset bundle.
            if (obj == null)
            {
                obj = LoadAllFromAssetBundle<T>(pathLower);
            }

            // Try load from resource.
            if (obj == null)
            {
                obj = Resources.LoadAll<T>(pathLower);
            }

            if (obj != null)
            {
                //Debug.Log("ResourceManager/Load: Load " + path);

                // cache obj into _resourceList.
                if (_resourceArrayList == null)
                {
                    _resourceArrayList = new Dictionary<string, Object[]>();
                }
                if (_resourceArrayList.ContainsKey(pathLower))
                {
                    _resourceArrayList[pathLower] = obj; // update.
                }
                else
                {
                    _resourceArrayList.Add(pathLower, obj); // cache object.
                }

                // Convert Texture2D to Sprite.
                if (obj.GetType() == typeof(UnityEngine.Texture2D) && typeof(T) == typeof(Sprite))
                {
                    Texture2D[] textureArray = System.Array.ConvertAll(obj, input => (Texture2D) input);
                    Sprite[] newSpriteArray = GameHelper.ConvertTexture2DArrayToSpriteArray(textureArray);
                    return (newSpriteArray as T[]);
                }
                else
                {
                    return (obj as T[]);
                }
            }

            return null;
        }

        #endregion

        #region Load Async
        public static void LoadAsync(string path, UnityAction<Object> onComplete)
        {
            string pathLower = path.ToLower();
            Object obj = null;

            // Try find from resouce cache list.
            if (_resourceList != null && _resourceList.ContainsKey(pathLower))
            {
                bool isFound = _resourceList.TryGetValue(pathLower, out obj);
                if (!isFound)
                {
                    obj = null;
                }
                else
                {
                    if (onComplete != null)
                    {
                        onComplete.Invoke(obj);
                    }

                    return;
                }
            }

            // Try load from asset bundle.
            if (obj == null)
            {
                LoadFromAssetBundleAsync(
                      pathLower
                    , delegate (Object result)
                    {
                        OnLoadComplete<Object>(path, result);
                        if (onComplete != null)
                        {
                            onComplete.Invoke(result);
                        }
                    }
                    , delegate ()
                    {
                        // Try load from resource.
                        LoadResourceAsync(path, delegate (Object result) {
                            OnLoadComplete<Object>(path, result);
                            if (onComplete != null)
                            {
                                onComplete.Invoke(result);
                            }
                        });
                    }
                );
            }
        }

        public static void LoadAsync<T>(string path, UnityAction<T> onComplete) where T : Object
        {
            string pathLower = path.ToLower();
            Object obj = null;

            // Try find from resouce cache list.
            if (_resourceList != null && _resourceList.ContainsKey(pathLower))
            {
                bool isFound = _resourceList.TryGetValue(pathLower, out obj);
                if (!isFound)
                {
                    obj = null;
                }
                else
                {
                    OnLoadAsyncComplete(pathLower, obj, onComplete);
                    return;
                }
            }

            // Try load from asset bundle.
            if (obj == null)
            {
                LoadFromAssetBundleAsync<T>(
                      pathLower
                    , delegate(T result) 
                    {
                        OnLoadComplete<T>(path, result);
                        OnLoadAsyncComplete(pathLower, result, onComplete);
                    }
                    , delegate () 
                    {
                        // Try load from resource.
                        LoadResourceAsync<T>(path, delegate(T result) {
                            OnLoadComplete<T>(path, result);
                            OnLoadAsyncComplete(pathLower, result, onComplete);
                        });
                    }
                );
            }
        }

        private static void LoadResourceAsync<T>(string path, UnityAction<T> onComplete) where T : Object
        {
            GameObject gameObject = new GameObject();
            ResourceLoader loader = gameObject.AddComponent<ResourceLoader>();
            loader.gameObject.name = "ResourceLoader_" + path;
            loader.StartAsyncLoad<T>(path, onComplete);
        }

        private static void OnLoadComplete<T>(string path, T obj) where T : Object
        {
            string pathLower = path.ToLower();

            // Cache result.
            if (obj != null)
            {
                // cache obj into _resourceList.
                if (_resourceList == null)
                {
                    _resourceList = new Dictionary<string, Object>();
                }
                if (_resourceList.ContainsKey(pathLower))
                {
                    _resourceList[pathLower] = obj; // update.
                }
                else
                {
                    _resourceList.Add(pathLower, obj); // cache object.
                }
            }
        }

        private static void OnLoadAsyncComplete<T>(string path, Object obj, UnityAction<T> onComplete) where T : Object
        {
            // Convert Texture2D to Sprite.
            if (obj != null && obj.GetType() == typeof(UnityEngine.Texture2D) && typeof(T) == typeof(Sprite))
            {
                Texture2D tex = obj as Texture2D;
                Sprite newSprite = GameHelper.ConvertTexture2DToSprite(tex);

                if (onComplete != null)
                {
                    onComplete.Invoke(newSprite as T);
                }
            }
            else
            {
                if (onComplete != null)
                {
                    onComplete.Invoke(obj as T);
                }
            }
        }
        #endregion

        #region Add Object
        public static void AddObject(string path, Object obj)
        {
            OnLoadComplete<Object>(path, obj);
        }
        public static void AddObject<T>(string path, T obj) where T : Object
        {
            OnLoadComplete<T>(path, obj);
        }
        #endregion

        #endregion

        #region Asset Bundle Methods
        private static Object LoadFromAssetBundle(string path)
        {
            return AssetBundleManager.LoadObject(path);
        }

        private static T LoadFromAssetBundle<T>(string path) where T : Object
        {
            return AssetBundleManager.LoadObject<T>(path); 
        }

        private static void LoadFromAssetBundleAsync(string path, UnityAction<Object> onComplete, UnityAction onFail)
        {
            AssetBundleManager.LoadObjectAsync(path, onComplete, onFail);
        }

        private static void LoadFromAssetBundleAsync<T>(string path, UnityAction<T> onComplete, UnityAction onFail) where T : Object
        {
            AssetBundleManager.LoadObjectAsync<T>(path, onComplete, onFail);
        }

        private static Object[] LoadAllFromAssetBundle(string path)
        {
            return AssetBundleManager.LoadObjectAll(path);
        }

        private static T[] LoadAllFromAssetBundle<T>(string path) where T : Object
        {
            return AssetBundleManager.LoadObjectAll<T>(path);
        }
        #endregion
    }
}
