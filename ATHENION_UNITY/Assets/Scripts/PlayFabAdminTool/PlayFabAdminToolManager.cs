﻿using PlayFab;
using PlayFab.AdminModels;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayFabAdminToolManager : MonoBehaviour
{
    #region Events
    public Action<int> OnGetPlayerListCountEvent;
    public Action OnProcessCompleteEvent;
    #endregion

    #region Protected Properties
    protected readonly uint _maxBatchPlayerSegment = 10000;
    protected readonly int _maxQueue = 250;
    protected readonly string _playerSegmentIDAllDev = "3F47694AE180D44F"; // ALL PLAYERS dev
    protected readonly string _playerSegmentIDAllPro = "27A8C20F51DAFC"; // ALL PLAYERS pro
    protected string _continuationToken = string.Empty;

    protected List<Dictionary<string, string>> _csvTable = new List<Dictionary<string, string>>();
    protected List<Dictionary<string, string>> _exportedData = new List<Dictionary<string, string>>();

    protected int _maxProgressCount;
    protected int _successProgressCount;
    protected int _errorProgressCount;
    #endregion

    #region PlayFab Methods

    #region Player Segment
    private string GetAllPlayerSegment()
    {
        string serverID = PlayFabSettings.TitleId;
        if (serverID == GameHelper.PlayFabTitleID_Development)
        {
            return _playerSegmentIDAllDev;
        }
        else if (serverID == GameHelper.PlayFabTitleID_Production)
        {
            return _playerSegmentIDAllPro;
        }
        else
        {
            return string.Empty;
        }
    }

    private void GetPlayerInSegment(UnityAction<GetPlayersInSegmentResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        string playerSegmentID = GetAllPlayerSegment();
        if(playerSegmentID == string.Empty)
        {
            return;
        }

        PlayFabAdminAPI.GetPlayersInSegment(new GetPlayersInSegmentRequest()
            {
                SegmentId = playerSegmentID,
                MaxBatchSize = _maxBatchPlayerSegment,
                ContinuationToken = _continuationToken
            }
            , (result) =>
            {
                _continuationToken = result.ContinuationToken;
                if (onComplete != null)
                {
                    onComplete.Invoke(result);
                }
            }
            , (error) =>
            {
                if (onFail != null)
                {
                    onFail.Invoke(error);
                }
            }
        );
    }

    protected IEnumerator IEGetPlayerIDInSegment(UnityAction<List<string>> onComplete, UnityAction<PlayFabError> onFail)
    {
        List<string> resultList = new List<string>();
        List<PlayerProfile> players = new List<PlayerProfile>();
        while (true)
        {
            bool isFinish = false;
            GetPlayerInSegment(
                (result) =>
                {
                    players = result.PlayerProfiles;
                    isFinish = true;
                }
                , (error) =>
                {
                    onFail?.Invoke(error);
                    isFinish = true;
                }
            );
            yield return new WaitUntil(() => isFinish == true);
            foreach(PlayerProfile profile in players)
            {
                resultList.Add(profile.PlayerId);
            }

            if (players.Count < _maxBatchPlayerSegment)
            {
                Debug.Log("Complete player segment");
                onComplete?.Invoke(resultList);
                yield break;
            }
        }
    }
    #endregion

    #region Title Data
    protected void GetAllTitleData(UnityAction<GetTitleDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        GetTitleData(new List<string>(), onComplete, onFail);
    }

    protected void GetTitleData(string key, UnityAction<GetTitleDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        GetTitleData(new List<string>() { key }, onComplete, onFail);
    }

    protected void GetTitleData(List<string> keyList, UnityAction<GetTitleDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        List<string> keys = keyList;
        if (keyList.Count <= 0)
        {
            keys = null;
        }

        PlayFabAdminAPI.GetTitleData(
            new GetTitleDataRequest()
            {
                Keys = keys,
            }
            , (result) =>
            {
                if (onComplete != null)
                {
                    onComplete.Invoke(result);
                }
            }
            , (error) =>
            {
                if (onFail != null)
                {
                    onFail.Invoke(error);
                }
            }
        );
    }
    #endregion

    #region Player Data
    protected void GetAllPlayerData(string playerID, UnityAction<GetUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        GetPlayerData(playerID, new List<string>(), onComplete, onFail);
    }

    protected void GetPlayerData(string playerID, string key, UnityAction<GetUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        GetPlayerData(playerID, new List<string>() { key }, onComplete, onFail);
    }

    protected void GetPlayerData(string playerID, List<string> keyList, UnityAction<GetUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        List<string> keys = keyList;
        if (keyList.Count <= 0)
        {
            keys = null;
        }

        PlayFabAdminAPI.GetUserData(
            new GetUserDataRequest()
            {
                PlayFabId = playerID,
                Keys = keys,
            }
            , (result) =>
            {
                if (onComplete != null)
                {
                    onComplete.Invoke(result);
                }
            }
            , (error) =>
            {
                if (onFail != null)
                {
                    onFail.Invoke(error);
                }
            }
        );
    }

    protected void UpdatePlayerData(string playerID, string key, string value, UnityAction<UpdateUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        Dictionary<string, string> data = new Dictionary<string, string>();
        data.Add(key, value);

        UpdatePlayerData(playerID, data, onComplete, onFail);
    }

    protected void UpdatePlayerData(string playerID, Dictionary<string, string> data, UnityAction<UpdateUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabAdminAPI.UpdateUserData(
            new UpdateUserDataRequest()
            {
                PlayFabId = playerID,
                Data = data
            }
            , (result) =>
            {
                if (onComplete != null)
                {
                    onComplete.Invoke(result);
                }
            }
            , (error) =>
            {
                if (onFail != null)
                {
                    onFail.Invoke(error);
                }
            }
        );
    }

    protected void RemovePlayerData(string playerID, List<string> keysToRemove, UnityAction<UpdateUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabAdminAPI.UpdateUserData(
            new UpdateUserDataRequest()
            {
                PlayFabId = playerID,
                KeysToRemove = keysToRemove,
            }
            , (result) =>
            {
                if (onComplete != null)
                {
                    onComplete.Invoke(result);
                }
            }
            , (error) =>
            {
                if (onFail != null)
                {
                    onFail.Invoke(error);
                }
            }
        );
    }
    #endregion

    #region Player ReadOnly Data
    protected void GetAllPlayerReadOnlyData(string playerID, UnityAction<GetUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        GetPlayerReadOnlyData(playerID, new List<string>(), onComplete, onFail);
    }

    protected void GetPlayerReadOnlyData(string playerID, string key, UnityAction<GetUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        GetPlayerReadOnlyData(playerID, new List<string>() { key }, onComplete, onFail);
    }

    protected void GetPlayerReadOnlyData(string playerID, List<string> keyList, UnityAction<GetUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        List<string> keys = keyList;
        if (keyList.Count <= 0)
        {
            keys = null;
        }

        PlayFabAdminAPI.GetUserReadOnlyData(
            new GetUserDataRequest()
            {
                PlayFabId = playerID,
                Keys = keys,
            }
            , (result) =>
            {
                if (onComplete != null)
                {
                    onComplete.Invoke(result);
                }
            }
            , (error) =>
            {
                if (onFail != null)
                {
                    onFail.Invoke(error);
                }
            }
        );
    }

    protected void UpdatePlayerReadOnlyData(string playerID, string key, string value, UnityAction<UpdateUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        Dictionary<string, string> data = new Dictionary<string, string>();
        data.Add(key, value);

        UpdatePlayerReadOnlyData(playerID, data, onComplete, onFail);
    }

    protected void UpdatePlayerReadOnlyData(string playerID, Dictionary<string, string> data, UnityAction<UpdateUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabAdminAPI.UpdateUserReadOnlyData(
            new UpdateUserDataRequest()
            {
                PlayFabId = playerID,
                Data = data
            }
            , (result) =>
            {
                if (onComplete != null)
                {
                    onComplete.Invoke(result);
                }
            }
            , (error) =>
            {
                if (onFail != null)
                {
                    onFail.Invoke(error);
                }
            }
        );
    }

    protected void RemovePlayerReadOnlyData(string playerID, List<string> keysToRemove, UnityAction<UpdateUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabAdminAPI.UpdateUserReadOnlyData(
            new UpdateUserDataRequest()
            {
                PlayFabId = playerID,
                KeysToRemove = keysToRemove,
            }
            , (result) =>
            {
                if (onComplete != null)
                {
                    onComplete.Invoke(result);
                }
            }
            , (error) =>
            {
                if (onFail != null)
                {
                    onFail.Invoke(error);
                }
            }
        );
    }
    #endregion

    #region Player Internal Data
    protected void GetAllPlayerInternalData(string playerID, UnityAction<GetUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        GetPlayerInternalData(playerID, new List<string>(), onComplete, onFail);
    }

    protected void GetPlayerInternalData(string playerID, string key, UnityAction<GetUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        GetPlayerInternalData(playerID, new List<string>() { key }, onComplete, onFail);
    }

    protected void GetPlayerInternalData(string playerID, List<string> keyList, UnityAction<GetUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        List<string> keys = keyList;
        if (keyList.Count <= 0)
        {
            keys = null;
        }

        PlayFabAdminAPI.GetUserInternalData(
            new GetUserDataRequest()
            {
                PlayFabId = playerID,
                Keys = keys,
            }
            , (result) =>
            {
                if (onComplete != null)
                {
                    onComplete.Invoke(result);
                }
            }
            , (error) =>
            {
                if (onFail != null)
                {
                    onFail.Invoke(error);
                }
            }
        );
    }

    protected void UpdatePlayerInternalData(string playerID, string key, string value, UnityAction<UpdateUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        Dictionary<string, string> data = new Dictionary<string, string>();
        data.Add(key, value);

        UpdatePlayerInternalData(playerID, data, onComplete, onFail);
    }

    protected void UpdatePlayerInternalData(string playerID, Dictionary<string, string> data, UnityAction<UpdateUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabAdminAPI.UpdateUserInternalData(
            new UpdateUserInternalDataRequest()
            {
                PlayFabId = playerID,
                Data = data
            }
            , (result) =>
            {
                if (onComplete != null)
                {
                    onComplete.Invoke(result);
                }
            }
            , (error) =>
            {
                if (onFail != null)
                {
                    onFail.Invoke(error);
                }
            }
        );
    }

    protected void RemovePlayerInternalData(string playerID, List<string> keysToRemove, UnityAction<UpdateUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabAdminAPI.UpdateUserInternalData(
            new UpdateUserInternalDataRequest()
            {
                PlayFabId = playerID,
                KeysToRemove = keysToRemove,
            }
            , (result) =>
            {
                if (onComplete != null)
                {
                    onComplete.Invoke(result);
                }
            }
            , (error) =>
            {
                if (onFail != null)
                {
                    onFail.Invoke(error);
                }
            }
        );
    }
    #endregion

    #region Player Inventory
    protected void AddPlayerVirutalCurrency(string playerID, string vcType, int value, UnityAction onComplete, UnityAction<PlayFabError> onFail)
    {
        if (value > 0)
        {
            PlayFabAdminAPI.AddUserVirtualCurrency(
                new AddUserVirtualCurrencyRequest()
                {
                    PlayFabId = playerID,
                    VirtualCurrency = vcType,
                    Amount = value
                }
                , (result) =>
                {
                    onComplete?.Invoke();
                }
                , (error) =>
                {
                    onFail?.Invoke(error);
                }
            );
        }
        else if (value < 0)
        {
            PlayFabAdminAPI.SubtractUserVirtualCurrency(
                new SubtractUserVirtualCurrencyRequest()
                {
                    PlayFabId = playerID,
                    VirtualCurrency = vcType,
                    Amount = -value
                }
                , (result) =>
                {
                    onComplete?.Invoke();
                }
                , (error) =>
                {
                    onFail?.Invoke(error);
                }
            );
        }
        else
        {
            onComplete?.Invoke();
        }
    }

    protected void RevokePlayerInventoryItem(string playerID, string itemInstanceID, UnityAction<RevokeInventoryItemsResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        RevokePlayerInventoryItem(playerID, new List<string>() { itemInstanceID }, onComplete, onFail);
    }

    protected void RevokePlayerInventoryItem(string playerID, List<string> itemInstanceIDList, UnityAction<RevokeInventoryItemsResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        List<RevokeInventoryItem> itemList = new List<RevokeInventoryItem>();
        foreach (string id in itemInstanceIDList)
        {
            itemList.Add(new RevokeInventoryItem()
            {
                PlayFabId = playerID,
                ItemInstanceId = id
            }
            );
        }

        PlayFabAdminAPI.RevokeInventoryItems(
            new RevokeInventoryItemsRequest()
            {
                Items = itemList
            }
            , (result) =>
            {
                if (onComplete != null)
                {
                    onComplete.Invoke(result);
                }
            }
            , (error) =>
            {
                if (onFail != null)
                {
                    onFail.Invoke(error);
                }
            }
        );
    }

    protected void GrantPlayerInventoryItem(string playerID, string itemID, UnityAction<GrantItemsToUsersResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        GrantPlayerInventoryItem(playerID, new List<string>() { itemID }, onComplete, onFail);
    }

    protected void GrantPlayerInventoryItem(string playerID, List<string> itemIDList, UnityAction<GrantItemsToUsersResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        List<ItemGrant> itemList = new List<ItemGrant>();
        foreach (string item in itemIDList)
        {
            itemList.Add(
                new ItemGrant()
                {
                    PlayFabId = playerID,
                    ItemId = item
                }
            );
        }

        PlayFabAdminAPI.GrantItemsToUsers(
            new GrantItemsToUsersRequest()
            {
                CatalogVersion = PlayFabManager.CatalogMainID,
                ItemGrants = itemList
            }
            , (result) =>
            {
                if (onComplete != null)
                {
                    onComplete.Invoke(result);
                }
            }
            , (error) =>
            {
                if (onFail != null)
                {
                    onFail.Invoke(error);
                }
            }
        );
    }

    protected void GetPlayerInventoryData(string playerID, UnityAction<GetUserInventoryResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabAdminAPI.GetUserInventory(
            new GetUserInventoryRequest()
            {
                PlayFabId = playerID
            }
            , (result) =>
            {
                if (onComplete != null)
                {
                    onComplete.Invoke(result);
                }
            }
            , (error) =>
            {
                if (onFail != null)
                {
                    onFail.Invoke(error);
                }
            }
        );
    }
    #endregion

    #endregion
}