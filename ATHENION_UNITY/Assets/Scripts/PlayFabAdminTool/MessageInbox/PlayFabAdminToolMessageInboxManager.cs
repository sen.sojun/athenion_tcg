﻿using Newtonsoft.Json;
using PlayFab;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayFabAdminMessageInboxData
{
    #region Public Properties
    public string ID { get { return _id; } }
    #endregion

    #region Private Properties
    private string _id = string.Empty;
    private string _title_th = string.Empty;
    private string _title_en = string.Empty;
    private string _detail_th = string.Empty;
    private string _detail_en = string.Empty;
    private Dictionary<string, int> _itemObj = new Dictionary<string, int>();

    private DateTime _receivedDateTime = DateTime.UtcNow;
    private DateTime _createdDateTime { get { return DateTime.UtcNow; } }
    private DateTime _expiredDateTime { get { return DateTime.MaxValue; } }
    private string _receivedDateTimeStr { get { return _receivedDateTime.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); } }
    private string _createdDateTimeStr { get { return _createdDateTime.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); } }
    private string _expiredDateTimeStr { get { return _expiredDateTime.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"); } }
    #endregion

    #region Contructors
    public PlayFabAdminMessageInboxData()
    {
    }

    public PlayFabAdminMessageInboxData(string id, string title_th, string title_en, string detail_th, string detail_en, Dictionary<string, int> itemObj, DateTime receivedDateTime)
    {
        _id = id;
        _title_th = title_th;
        _title_en = title_en;
        _detail_th = detail_th;
        _detail_en = detail_en;
        _itemObj = new Dictionary<string, int>(itemObj);
        _receivedDateTime = receivedDateTime;
    }
    #endregion

    #region Methods
    public Dictionary<string, object> ToDictionary()
    {
        Dictionary<string, object> result = new Dictionary<string, object>();

        result.Add("title", new Dictionary<string, string>() { { "Thai", _title_th }, { "English", _title_en } });
        result.Add("msg", new Dictionary<string, string>() { { "Thai", _detail_th }, { "English", _detail_en } });
        result.Add("item", _itemObj);
        result.Add("created_dt", _createdDateTimeStr);
        result.Add("expired_dt", _expiredDateTimeStr);
        result.Add("received_dt", _receivedDateTimeStr);

        return result;
    }
    #endregion
}

public class PlayFabAdminToolMessageInboxManager : PlayFabAdminToolManager
{
    #region Private Properties
    private string _messageInboxKey = "message_inbox";
    private string _messageInboxStatusKey = "message_inbox_status";
    #endregion

    #region Methods
    public void RequestDoSendMessageInbox(List<string> playerIDList, PlayFabAdminMessageInboxData messageData, UnityAction<string> onComplete)
    {
        StartCoroutine(IERequestDoSendMessageInbox(playerIDList, messageData, onComplete));
    }

    private IEnumerator IERequestDoSendMessageInbox(List<string> playerIDList, PlayFabAdminMessageInboxData messageData, UnityAction<string> onComplete)
    {
        PlayFabError error = null;
        if (playerIDList.Count == 0)
        {
            yield return IEGetPlayerIDInSegment((result) => playerIDList = result, (err) => error = err);
        }
        OnGetPlayerListCountEvent?.Invoke(playerIDList.Count);

        if (error != null)
        {
            Debug.LogError(error.ToString());
            onComplete?.Invoke(error.ToString());
            yield break;
        }

        while (playerIDList.Count > 0)
        {
            List<string> lotPlayerProfileList = playerIDList.CutFirst(_maxQueue);
            int count = lotPlayerProfileList.Count;
            for (int i = 0; i < lotPlayerProfileList.Count; i++)
            {
                StartCoroutine(IESendMessageInbox(
                    lotPlayerProfileList[i]
                    , messageData
                    , delegate
                    {
                        OnProcessCompleteEvent?.Invoke();
                        count -= 1;
                    })
                );
            }
            yield return new WaitUntil(() => count <= 0);
            yield return null;
        }
        onComplete?.Invoke("");
    }

    private IEnumerator IESendMessageInbox(string playerID, PlayFabAdminMessageInboxData messageData, UnityAction onComplete)
    {
        Dictionary<string, object> messageInboxData = new Dictionary<string, object>();
        Dictionary<string, List<string>> messageInboxStatus = new Dictionary<string, List<string>>();
        yield return IEGetPlayerMessageInboxData(playerID, (result) => messageInboxData = result, (error) => { Debug.Log(error.ToString()); onComplete?.Invoke(); return; });
        yield return IEGetPlayerMessageInboxStatusData(playerID, (result) => messageInboxStatus = result, (error) => { Debug.Log(error.ToString()); onComplete?.Invoke(); return; });

        // check message exist in inbox
        if(messageInboxData.ContainsKey(messageData.ID))
        {
            onComplete?.Invoke();
            yield break;
        }

        // check message exist in status
        foreach (KeyValuePair<string, List<string>> item in messageInboxStatus)
        {
            if(item.Value.Contains(messageData.ID))
            {
                onComplete?.Invoke();
                yield break;
            }
        }

        // add message
        messageInboxData.Add(messageData.ID, messageData.ToDictionary());

        // update message inbox
        yield return IEUpdateMessageInbox(
            playerID
            , messageInboxData
            , delegate
            {
                onComplete?.Invoke();
            }
            , (error) => Debug.LogError(error)
        );
    }

    private IEnumerator IEGetPlayerMessageInboxData(string playerID, UnityAction<Dictionary<string, object>> onComplete, UnityAction<string> onFail)
    {
        bool isFinish = false;
        Dictionary<string, object> resultData = new Dictionary<string, object>();
        GetPlayerReadOnlyData(
            playerID
            , _messageInboxKey
            , (result) =>
            {
                isFinish = true;
                if (result.Data.ContainsKey(_messageInboxKey))
                {
                    resultData = JsonConvert.DeserializeObject<Dictionary<string, object>>(result.Data[_messageInboxKey].Value);
                }
            }, (error) =>
            {
                Debug.LogError(error.ToString());
            }
        );

        yield return new WaitUntil(() => isFinish);
        onComplete?.Invoke(resultData);
    }

    private IEnumerator IEGetPlayerMessageInboxStatusData(string playerID, UnityAction<Dictionary<string, List<string>>> onComplete, UnityAction<string> onFail)
    {
        bool isFinish = false;
        Dictionary<string, List<string>> resultData = new Dictionary<string, List<string>>();
        GetPlayerReadOnlyData(
            playerID
            , _messageInboxStatusKey
            , (result) =>
            {
                isFinish = true;
                if (result.Data.ContainsKey(_messageInboxStatusKey))
                {
                    resultData = JsonConvert.DeserializeObject<Dictionary<string, List<string>>>(result.Data[_messageInboxStatusKey].Value);
                }
            }, (error) =>
            {
                Debug.LogError(error.ToString());
            }
        );

        yield return new WaitUntil(() => isFinish);
        onComplete?.Invoke(resultData);
    }

    private IEnumerator IEUpdateMessageInbox(string playerID, Dictionary<string, object> messageData, UnityAction onComplete, UnityAction<string> onFail)
    {
        bool isFinish = false;
        UpdatePlayerReadOnlyData(
            playerID
            , _messageInboxKey
            , JsonConvert.SerializeObject(messageData)
            , (result) =>
            {
                isFinish = true;
            }
            , (err) =>
            {
                Debug.LogError(err.ToString());
            }
        );
        yield return new WaitUntil(() => isFinish == true);
        onComplete?.Invoke();
    }
    #endregion
}