﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Newtonsoft.Json;
using System;
using System.Linq;
using PlayFab;
using System.Text;

public class PlayFabAdminToolMessageInboxUIManager : MonoBehaviour
{
    #region Inspector Properties
    [Header("Controller")]
    [SerializeField] private PlayFabAdminToolMessageInboxManager _Manager;

    [Header("Receiver Group")]
    [SerializeField] private TMP_InputField _Input_ReceiverList;
    [SerializeField] private Toggle _Toggle_AllPlayers;

    [Header("Message ID")]
    [SerializeField] private TMP_InputField _Input_MessageID;

    [Header("Message Title")]
    [SerializeField] private TMP_InputField _Input_Title_TH;
    [SerializeField] private TMP_InputField _Input_Title_EN;

    [Header("Message Detail")]
    [SerializeField] private TMP_InputField _Input_Detail_TH;
    [SerializeField] private TMP_InputField _Input_Detail_EN;

    [Header("Message Item")]
    [SerializeField] private TMP_InputField _Input_ItemObj;

    [Header("Message ReceivedDateTime")]
    [SerializeField] private TMP_InputField _Input_ReceivedDateTime_Year;
    [SerializeField] private TMP_InputField _Input_ReceivedDateTime_Month;
    [SerializeField] private TMP_InputField _Input_ReceivedDateTime_Day;
    [SerializeField] private TMP_InputField _Input_ReceivedDateTime_Hour;
    [SerializeField] private TMP_InputField _Input_ReceivedDateTime_Minute;
    [SerializeField] private TMP_InputField _Input_ReceivedDateTime_Second;

    [Header("Button")]
    [SerializeField] private GameObject _ButtonObj_SendMessage;
    [SerializeField] private Button _Button_SendMessage;
    #endregion

    #region Private Properties
    private Text _buttonText;
    private float _currentProcess = 0;
    private float _maxProcess = 1;
    #endregion

    #region Methods
    private void Awake()
    {
        _buttonText = _Button_SendMessage.gameObject.GetComponentInChildren<Text>();

        _Toggle_AllPlayers.onValueChanged.RemoveAllListeners();
        _Toggle_AllPlayers.onValueChanged.AddListener(
            delegate (bool isOn)
            {
                _Input_ReceiverList.text = string.Empty;
                _Input_ReceiverList.interactable = !isOn;
            }
        );

        _Button_SendMessage.onClick.RemoveAllListeners();
        _Button_SendMessage.onClick.AddListener(DoRequestSendMessage);

        SetupEvent();
    }

    private void Update()
    {
        _buttonText.text = string.Format("Send message to {0} ({1}%)", PlayFabSettings.TitleId, ((_currentProcess / _maxProcess) * 100).ToString("0.00"));
    }

    private void SetupEvent()
    {
        _Manager.OnGetPlayerListCountEvent += delegate (int count) { _maxProcess = count; };
        _Manager.OnProcessCompleteEvent += delegate () 
        {
            _currentProcess++;
        };
    }

    private void ResetProgress()
    {
        _currentProcess = 0;
        _maxProcess = 1;
    }

    private void DoRequestSendMessage()
    {
        PlayFabAdminMessageInboxData messageData;
        if(CreateMessageInboxData(out messageData))
        {
            PopupUIManager.Instance.Show_MidSoftLoad(true);
            List<string> playerIDList = GetPlayerIDList();
            _Manager.RequestDoSendMessageInbox(
                playerIDList
                , messageData
                , delegate(string result) 
                {
                    PopupUIManager.Instance.Show_MidSoftLoad(false);
                    if(result == "")
                    {
                        PopupUIManager.Instance.ShowPopup_ConfirmOnly(
                            "Success"
                            , "Message has been sent!"
                            , ResetProgress
                        );
                    }
                    else
                    {
                        PopupUIManager.Instance.ShowPopup_Error(
                            "Something wrong"
                            , result
                            , ResetProgress
                        );
                    }
                }
            );
        }
        else
        {
            PopupUIManager.Instance.ShowPopup_Error("Fail", "Inputfield cannot be empty!");
        }
    }

    private List<string> GetPlayerIDList()
    {
        List<string> result = new List<string>();
        string playerIDListStr = string.Empty;
        if(TryGetInputFieldValue(_Input_ReceiverList, true, out playerIDListStr))
        {
            playerIDListStr = playerIDListStr.Replace("\r\n", "");
            string[] resultSplit = playerIDListStr.Split(',');
            result = resultSplit.ToList();
            for(int i = 0; i < result.Count; i++)
            {
                if(result[i] == "")
                {
                    result.RemoveAt(i);
                    i--;
                }
            }
        }

        if(_Input_ReceiverList.interactable == false
            && _Toggle_AllPlayers.isOn == true)
        {
            result = new List<string>();
        }

        return result;
    }

    private bool CreateMessageInboxData(out PlayFabAdminMessageInboxData result)
    {
        result = new PlayFabAdminMessageInboxData();

        string id = string.Empty;
        string titleTH = string.Empty;
        string titleEN = string.Empty;
        string detailTH = string.Empty;
        string detailEN = string.Empty;
        string itemObjStr = string.Empty;

        string receivedDateTimeYearStr = string.Empty;
        string receivedDateTimeMonthStr = string.Empty;
        string receivedDateTimeDayStr = string.Empty;
        string receivedDateTimeHourStr = string.Empty;
        string receivedDateTimeMinuteStr = string.Empty;
        string receivedDateTimeSecondStr = string.Empty;
        string receivedDateTimeStr = string.Empty;

        bool isSuccess = true;
        isSuccess &= TryGetInputFieldValue(_Input_MessageID, true, out id);
        isSuccess &= TryGetInputFieldValue(_Input_Title_TH, false, out titleTH);
        isSuccess &= TryGetInputFieldValue(_Input_Title_EN, false, out titleEN);
        isSuccess &= TryGetInputFieldValue(_Input_Detail_TH, false, out detailTH);
        isSuccess &= TryGetInputFieldValue(_Input_Detail_EN, false, out detailEN);
        isSuccess &= TryGetInputFieldValue(_Input_ItemObj, true, out itemObjStr);

        isSuccess &= TryGetInputFieldValue(_Input_ReceivedDateTime_Year, true, out receivedDateTimeYearStr);
        isSuccess &= TryGetInputFieldValue(_Input_ReceivedDateTime_Month, true, out receivedDateTimeMonthStr);
        isSuccess &= TryGetInputFieldValue(_Input_ReceivedDateTime_Day, true, out receivedDateTimeDayStr);
        isSuccess &= TryGetInputFieldValue(_Input_ReceivedDateTime_Hour, true, out receivedDateTimeHourStr);
        isSuccess &= TryGetInputFieldValue(_Input_ReceivedDateTime_Minute, true, out receivedDateTimeMinuteStr);
        isSuccess &= TryGetInputFieldValue(_Input_ReceivedDateTime_Second, true, out receivedDateTimeSecondStr);

        Dictionary<string, int> itemObj = new Dictionary<string, int>();
        isSuccess &= GameHelper.TryDeserializeJSONStr(itemObjStr, out itemObj);

        // build DateTime String
        receivedDateTimeStr = string.Format(
            "{0}-{1}-{2}T{3}:{4}:{5}.000Z"
            , receivedDateTimeYearStr
            , receivedDateTimeMonthStr
            , receivedDateTimeDayStr
            , receivedDateTimeHourStr
            , receivedDateTimeMinuteStr
            , receivedDateTimeSecondStr
        );

        DateTime receivedDateTime = DateTime.MaxValue;
        isSuccess &= DateTime.TryParse(receivedDateTimeStr, out receivedDateTime);

        if(isSuccess)
        {
            result = new PlayFabAdminMessageInboxData(id, titleTH, titleEN, detailTH, detailEN, itemObj, receivedDateTime.ToUniversalTime());
        }

        return isSuccess;
    }

    private bool TryGetInputFieldValue(TMP_InputField targetInputField, bool ignoreSpacing, out string result)
    {
        result = targetInputField.text;
        if(ignoreSpacing)
        {
            result.Replace(" ", "");
        }
        return result != string.Empty;
    }
    #endregion
}