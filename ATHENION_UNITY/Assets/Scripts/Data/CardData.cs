﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardData
{
    #region Public Properties
    // Non-value definition
    public string BaseID { get; private set; }  // Base Card ID
    public string FullID { get; private set; }  // Full ID
    public string VariantID { get; private set; } // Varaint ID (Cosmatic skin something)
    public string Name { get { return GetName(); } }
    public string FlavorText { get { return GetFlavor(); } }
    public string Description { get { return GetDescription(); } }
    public string ImageKey { get { return GetImageKey(); } }
    public CardRarity Rarity { get; private set; }
    public CardType Type { get; private set; }
    public CardElement CardElement { get; private set; }
    public string SeriesKey { get; private set; }
    public CardDirection CardDir { get; private set; }
    public CardPassive CardPassive { get; private set; }
    public List<AbilityFeedback> FeedbackList { get; private set; }
    public List<CardAbilityDBData> AbilityList { get { return _abilityList; } }
    public string SpawnEffectKey { get; private set; }
    public string AttackEffectKey { get; private set; }
    public string DeathEffectKey { get; private set; }
    public string InfoScriptKey { get; private set; }
    public bool IsFoil { get; private set; }

    // Base value
    public int HP { get; private set; }
    public int ATK { get; private set; }
    public int Spirit { get; private set; }
    public int Armor { get; private set; }
    #endregion

    #region Private Properties
    private CardDBData _data;
    private List<CardAbilityDBData> _abilityList;
    #endregion

    #region Private Static Properties
    public static readonly string SpawnEffectKey_Default = "VFX_Spawn_Default";
    public static readonly string AttackEffectKey_Default = "VFX_Attack_Default";
    public static readonly string DeathEffectKey_Default = "VFX_Death_Default";
    private static readonly string CardEffectIDPrefix = "CARD_EFX_";

    private static Dictionary<string, CardData> _cardDataListCache = new Dictionary<string, CardData>();
    #endregion

    #region Constructors
    private CardData(CardDBData data, string fullID)
    {
        _data = data;
        BaseID = _data.ID;
        FullID = fullID;
        VariantID = fullID.Split(':')[0]; // Split varient ID.
        HP = data.HP;
        ATK = data.ATK;
        Spirit = data.Spirit;
        Armor = data.Armor;
        SeriesKey = data.SeriesKey;

        SpawnEffectKey = data.SpawnEffectKey;
        AttackEffectKey = data.AttackEffectKey;
        DeathEffectKey = data.DeathEffectKey;
        SetupCardEffect();

        InfoScriptKey = data.InfoScriptKey;

        SetupFoil();
        SetupCardRarity();
        SetupCardType();
        SetupCardElement();
        SetupCardDir();
        SetupCardPassive();
        SetupCardAbilityFeedback();
        SetupCardAbility();
    }

    public CardData(CardData data)
    {
        _data = data._data;
        BaseID = data.BaseID;
        FullID = data.FullID;
        VariantID = data.VariantID;
        Rarity = data.Rarity;
        Type = data.Type;
        HP = data.HP;
        ATK = data.ATK;
        Spirit = data.Spirit;
        Armor = data.Armor;
        SeriesKey = data.SeriesKey;

        SpawnEffectKey = data.SpawnEffectKey;
        AttackEffectKey = data.AttackEffectKey;
        DeathEffectKey = data.DeathEffectKey;
        InfoScriptKey = data.InfoScriptKey;

        IsFoil = data.IsFoil;

        CardElement = new CardElement(data.CardElement);
        CardDir = new CardDirection(data.CardDir);
        CardPassive = new CardPassive(data.CardPassive);
        FeedbackList = new List<AbilityFeedback>(data.FeedbackList);

        _abilityList = new List<CardAbilityDBData>(data._abilityList);
    }
    #endregion

    #region Methods
    private void SetupFoil()
    {
        IsFoil = FullID.ToUpper().Contains("FOIL");
    }

    private void SetupCardRarity()
    {
        CardRarity rarity;
        bool isSucess = GameHelper.StrToEnum(_data.Rarity, out rarity);
        if (isSucess)
        {
            Rarity = rarity;
        }
        else
        {
            Debug.LogErrorFormat("CardData/SetupCardRarity: Failed to parse enum. {0}", _data.Rarity);
            Rarity = CardRarity.Common;
        }
    }

    private void SetupCardType()
    {
        CardType type;
        bool isSucess = GameHelper.StrToEnum(_data.Type, out type);
        if (isSucess)
        {
            Type = type;
        }
        else
        {
            Debug.LogErrorFormat("CardData/SetupCardType: Failed to parse enum. {0}", _data.Type);
            Type = CardType.Minion;
        }
    }

    private void SetupCardDir()
    {
        int[] dirIndex = new int[8] {0,0,0,0,0,0,0,0};
        CardDirectionType dir;
        bool isSucess = true;
        foreach (string dirText in _data.CardDir)
        {
            if (dirText != "-")
            {
                isSucess = GameHelper.StrToEnum(dirText, out dir);
                if (isSucess)
                {
                    dirIndex[(int) dir] = 1;
                }
                else
                {
                    Debug.LogErrorFormat("CardData/SetupCardDir: Failed to parse enum. {0}", dirText);
                }
            }
        }
        CardDir = new CardDirection(dirIndex);
    }

    private void SetupCardPassive()
    {
        CardPassive = CardPassive.Create(_data.PassiveList);
    }

    private void SetupCardElement()
    {
        CardElement = CardElement.Create(_data.ElementList);
    }

    private void SetupCardAbilityFeedback()
    {
        FeedbackList = new List<AbilityFeedback>();
        foreach (string feedbackText in _data.FeedbackList)
        {
            AbilityFeedback feedback;
            if (GameHelper.StrToEnum<AbilityFeedback>(feedbackText.Trim(), out feedback))
            {
                FeedbackList.Add(feedback);
            }
        }
    }

    private void SetupCardAbility()
    {
        _abilityList = new List<CardAbilityDBData>();
        foreach (string abilityID in _data.AbilityIDList)
        {
            CardAbilityDBData ability = new CardAbilityDBData();
            if (CardAbilityDB.Instance.GetData(abilityID, out ability))
            {
                _abilityList.Add(ability);
            }
        }
    }

    private void SetupCardEffect()
    {
        string cardEfxID = string.Format("{0}{1}", CardEffectIDPrefix, BaseID);
        
        CardEffectDBData data;
        if (CardEffectDB.Instance.GetData(cardEfxID, out data))
        {
            SpawnEffectKey = data.SpawnEffectKey;
            AttackEffectKey = data.AttackEffectKey;
            DeathEffectKey = data.DeathEffectKey;
        }
        else
        {
            Debug.LogWarningFormat("CardData/SetupCardEffect: Not found card effect. {0}", BaseID);

            //SpawnEffectKey = SpawnEffectKey_Default;
            //AttackEffectKey = AttackEffectKey_Default;
            //DeathEffectKey = DeathEffectKey_Default;
        }
    }

    public string GetName()
    {
        return GameHelper.GetCardName(BaseID);
    }

    public string GetDescription()
    {
        return GameHelper.GetCardAbilityDescription(BaseID);
    }

    public string GetFlavor()
    {
        return GameHelper.GetCardFlavor(BaseID);
    }

    private string GetImageKey()
    {
        string imageKey = "";

        if (VariantID.Length > 0)
        {
            string indexStr = VariantID.Substring(1, VariantID.Length - 1);
            imageKey = string.Format("IM{0}", indexStr);
        }

        return imageKey;
    }

    public List<AbilityKeyword> GetAbilityKeywordList()
    {
        List<AbilityKeyword> abilityKeywordList = new List<AbilityKeyword>();

        // Init Ability Keyword
        foreach (AbilityFeedback feedback in FeedbackList)
        {
            switch (feedback)
            {               
                case AbilityFeedback.Trigger: AddAbilityKeyword(ref abilityKeywordList, AbilityKeyword.Trigger); break;
                case AbilityFeedback.Awaken: AddAbilityKeyword(ref abilityKeywordList, AbilityKeyword.Awaken); break;
                case AbilityFeedback.Backstab: AddAbilityKeyword(ref abilityKeywordList, AbilityKeyword.Backstab); break;
                case AbilityFeedback.Berserk: AddAbilityKeyword(ref abilityKeywordList, AbilityKeyword.Berserk); break;
                case AbilityFeedback.Lastwish: AddAbilityKeyword(ref abilityKeywordList, AbilityKeyword.Lastwish); break;
                case AbilityFeedback.Link: AddAbilityKeyword(ref abilityKeywordList, AbilityKeyword.Link); break;
                case AbilityFeedback.Taunt: AddAbilityKeyword(ref abilityKeywordList, AbilityKeyword.Taunt); break;
                case AbilityFeedback.Aura: AddAbilityKeyword(ref abilityKeywordList, AbilityKeyword.Aura); break;
                case AbilityFeedback.DarkPower: AddAbilityKeyword(ref abilityKeywordList, AbilityKeyword.DarkPower); break;
                case AbilityFeedback.Chain: AddAbilityKeyword(ref abilityKeywordList, AbilityKeyword.Chain); break;
                case AbilityFeedback.Piercing: AddAbilityKeyword(ref abilityKeywordList, AbilityKeyword.Piercing); break;
            }
        }

        // Ability Action
        foreach (CardAbilityDBData data in _abilityList)
        {
            foreach (string effectKey in data.EffectList)
            {
                if (effectKey.Contains("DoMoveMinion"))
                {
                    AddAbilityKeyword(ref abilityKeywordList, AbilityKeyword.Move);
                }
                if (effectKey.Contains("DoAddSlotBuff"))
                {
                    if (effectKey.Contains("BuffSlotLock"))
                    {
                        AddAbilityKeyword(ref abilityKeywordList, AbilityKeyword.Lock);
                    }
                    if (effectKey.Contains("BuffSlotDarkMatter"))
                    {
                        AddAbilityKeyword(ref abilityKeywordList, AbilityKeyword.DarkMatter);
                    }
                }
                if (effectKey.Contains("DoDrawCard"))
                {
                    AddAbilityKeyword(ref abilityKeywordList, AbilityKeyword.Draw);
                }
                if (effectKey.Contains("DoResetMinion"))
                {
                    AddAbilityKeyword(ref abilityKeywordList, AbilityKeyword.Reset);
                }
                if (effectKey.Contains("BuffMinionFear"))
                {
                    AddAbilityKeyword(ref abilityKeywordList, AbilityKeyword.Fear);
                }
                if (effectKey.Contains("BuffMinionFreeze"))
                {
                    AddAbilityKeyword(ref abilityKeywordList, AbilityKeyword.Freeze);
                }
                if (effectKey.Contains("BuffMinionSilence"))
                {
                    AddAbilityKeyword(ref abilityKeywordList, AbilityKeyword.Silence);
                }
                if (effectKey.Contains("BuffMinionStealth"))
                {
                    AddAbilityKeyword(ref abilityKeywordList, AbilityKeyword.Stealth);
                }
                if (effectKey.Contains("BuffMinionInvincible"))
                {
                    AddAbilityKeyword(ref abilityKeywordList, AbilityKeyword.Invincible);
                }
                if (effectKey.Contains("BuffMinionUntargetable"))
                {
                    AddAbilityKeyword(ref abilityKeywordList, AbilityKeyword.Untargetable);
                }
            }
        }

        return abilityKeywordList;
    }

    private void AddAbilityKeyword(ref List<AbilityKeyword> abilityKeywordList, AbilityKeyword abilityKeyword)
    {
        if (abilityKeywordList == null)
        {
            abilityKeywordList = new List<AbilityKeyword>();
        }

        if (!abilityKeywordList.Contains(abilityKeyword))
        {
            abilityKeywordList.Add(abilityKeyword);
        }
    }

    public static CardData CreateCard(string fullID)
    {
        string cardID = fullID;

        CardDBData data;
        if (CardDB.Instance.GetData(cardID, out data))
        {
            // Found this card ID.
            if (data != null)
            {
                return CreateCard(data, fullID);
            }
        }
        else
        {
            // Not found card ID. // Try get base card ID.
            string[] cardIDSplit = fullID.Split('_', ':');
            cardID = cardIDSplit[0];

            if (CardDB.Instance.GetData(cardID, out data))
            {
                if (data != null)
                {
                    return CreateCard(data, fullID);
                }
            }
        }

        return null;
    }

    public static CardData CreateCard(CardDBData data, string fullID)
    {
        CardData card = null;
        if (data != null)
        {
            card = new CardData(data, fullID);
        }
        return card;
    }

    public static CardData CreateCardOld(string cardBaseID)
    {
        CardData card = null;
        CardDBData data = null;
        string cardIDOld = string.Format("{0}OLD", cardBaseID);
        if (CardDB.Instance.GetData(cardIDOld, out data))
        {
            card = new CardData(data, cardBaseID);
        }

        return card;
    }
    #endregion

    #region Caching Methods
    /// <summary>
    /// Call this method after successfully load card in Catalog (PlayFab)
    /// </summary>
    /// <param name="cardFullIDList"></param>
    public static void SetCardDataList(List<string> cardFullIDList)
    {
        _cardDataListCache.Clear();
        List<CardDBData> cardDBDatas = new List<CardDBData>();
        if (CardDB.Instance.GetAllData(out cardDBDatas))
        {
            foreach (CardDBData card in cardDBDatas)
            {
                if (!cardFullIDList.Contains(card.ID))
                {
                    cardFullIDList.Add(card.ID);
                }
            }
        }

        foreach (string id in cardFullIDList)
        {
            CardData card = CardData.CreateCard(id);
            if (card != null)
            {
                _cardDataListCache.Add(id, card);
            }
        }
    }

    public static bool IsCard(string itemID)
    {
        return _cardDataListCache.ContainsKey(itemID);
    }

    public static bool GetCardData(string fullID, out CardData cardData)
    {
        cardData = null;
        if (_cardDataListCache.ContainsKey(fullID))
        {
            cardData = new CardData(_cardDataListCache[fullID]);

            return true;
        }

        return false;
    }

    public static bool GetAllCardData(out List<CardData> cardDataList)
    {
        cardDataList = new List<CardData>();
        foreach (KeyValuePair<string, CardData> item in _cardDataListCache)
        {
            cardDataList.Add(new CardData(item.Value));
        }

        return true;
    }
    #endregion
}