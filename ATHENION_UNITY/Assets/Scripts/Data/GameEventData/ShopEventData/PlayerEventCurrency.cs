﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Obsolete]
public class PlayerEventCurrency
{
    public string Key { get; set; }
    public int Amount { get; set; }
    public int Used { get; set; }

    public PlayerEventCurrency(string key, string json)
    {
        Dictionary<string, object> data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
        Key = key;
        Amount = Convert.ToInt32(data["amount"]);
        Used = Convert.ToInt32(data["used"]);
    }

    public PlayerEventCurrency(string key, int amount, int used)
    {
        Key = key;
        Amount = amount;
        Used = used;
    }

}
