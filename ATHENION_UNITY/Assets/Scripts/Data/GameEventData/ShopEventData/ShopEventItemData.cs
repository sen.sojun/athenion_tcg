﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Newtonsoft.Json;

public class ShopEventItemData
{
    public enum ShopEventItemSize { S, L }

    public string BundleID { get; private set; }
    public string Currency { get; private set; }
    public string ImageKey { get; private set; }
    public int Price { get; private set; }
    public int MaxCount { get; private set; }
    public bool Buyable { get; private set; }
    public string LocalizeKey { get; private set; }
    public ShopEventItemSize Size { get; set; }

    public DateTime ReleaseDateTimeUTC { get; private set; }
    public DateTime EndDateTimeUTC { get; private set; }

    public List<ItemData> itemDetailList = new List<ItemData>();

    public ShopEventItemData(string json)
    {
        Dictionary<string, object> data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
        BundleID = data["bundle_id"].ToString();
        Currency = data["currency"].ToString();
        Price = Convert.ToInt32(data["price"]);
        MaxCount = Convert.ToInt32(data["max_count"]);
        ImageKey = data["image"].ToString();
        Buyable = true;

        ShopEventItemSize size;
        GameHelper.StrToEnum(data["size"].ToString(), out size);
        this.Size = size;

        LocalizeKey = data.ContainsKey("localize_key") == false ? "" : data["localize_key"].ToString();
        ReleaseDateTimeUTC = data.ContainsKey("release_date") == false ? DateTime.MinValue : Convert.ToDateTime(data["release_date"]).ToUniversalTime();
        EndDateTimeUTC = data.ContainsKey("end_date") == false ? DateTime.MaxValue : Convert.ToDateTime(data["end_date"]).ToUniversalTime();

        List<object> rawItemDetailList = JsonConvert.DeserializeObject<List<object>>(data["item_detail"].ToString());
        for (int i = 0; i < rawItemDetailList.Count; i++)
        {
            Dictionary<string, object> rawItemDetail = JsonConvert.DeserializeObject<Dictionary<string, object>>(rawItemDetailList[i].ToString());

            string itemID = rawItemDetail["item_id"].ToString();
            int amount = Convert.ToInt32(rawItemDetail["amount"]);
            itemDetailList.Add(new ItemData(itemID, amount));
        }
    }

    public void SetBuyable(bool isBuyable)
    {
        Buyable = isBuyable;
    }
}
