﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Newtonsoft.Json;

[Serializable]
public class ShopEventData
{
    public List<ShopEventItemData> itemList = new List<ShopEventItemData>();

    public ShopEventData(string json)
    {
        List<object> rawEventItem = JsonConvert.DeserializeObject<List<object>>(json);

        for (int i = 0; i < rawEventItem.Count; i++)
        {
            string itemJson = rawEventItem[i].ToString();
            itemList.Add(new ShopEventItemData(itemJson));
        }
    }

    public object GetDebugText()
    {
        return JsonConvert.SerializeObject(this);
    }
}
