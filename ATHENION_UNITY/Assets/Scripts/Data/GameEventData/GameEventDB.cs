﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking.NetworkSystem;

public class GameEventDB
{
    private Dictionary<string, GameEventInfo> _gameEventList = new Dictionary<string, GameEventInfo>();
    private Dictionary<string, GameEventThemeInfo> _themeList = new Dictionary<string, GameEventThemeInfo>();
    private Dictionary<string, GameEventActiveECInfo> _eventCurrencyList = new Dictionary<string, GameEventActiveECInfo>();
    private Dictionary<string, GameEventInfo> _gameEventQuestList = new Dictionary<string, GameEventInfo>();

    public GameEventDB(string eventListJson, string eventThemeJson, string activeECJson, string eventQuestListJson)
    {
        CreateGameEventList(eventListJson);
        CreateThemeList(eventThemeJson);
        CreateECList(activeECJson);
        CreateGameEventQuestList(eventQuestListJson);
    }

    private void CreateGameEventList(string eventListJson)
    {
        Dictionary<string, object> rawGameEventData = JsonConvert.DeserializeObject<Dictionary<string, object>>(eventListJson);
        foreach (KeyValuePair<string, object> data in rawGameEventData)
        {
            string json = data.Value.ToString();
            _gameEventList.Add(data.Key, new GameEventInfo(data.Key, json));
        }
    }

    private void CreateThemeList(string eventThemeJson)
    {
        Dictionary<string, object> rawGameEventData = JsonConvert.DeserializeObject<Dictionary<string, object>>(eventThemeJson);
        foreach (KeyValuePair<string, object> data in rawGameEventData)
        {
            string json = data.Value.ToString();
            _themeList.Add(data.Key, new GameEventThemeInfo(data.Key, json));
        }
    }

    private void CreateECList(string activeECJson)
    {
        Dictionary<string, object> rawGameEventData = JsonConvert.DeserializeObject<Dictionary<string, object>>(activeECJson);
        foreach (KeyValuePair<string, object> data in rawGameEventData)
        {
            string json = data.Value.ToString();
            _eventCurrencyList.Add(data.Key, new GameEventActiveECInfo(data.Key, json));
        }
    }

    private void CreateGameEventQuestList(string eventQuestListJson)
    {
        Dictionary<string, object> rawGameEventData = JsonConvert.DeserializeObject<Dictionary<string, object>>(eventQuestListJson);
        foreach (KeyValuePair<string, object> data in rawGameEventData)
        {
            string json = data.Value.ToString();
            _gameEventQuestList.Add(data.Key, new GameEventInfo(data.Key, json));
        }
    }
    
    public Dictionary<string, GameEventInfo> GetGameEventInfo()
    {
        return _gameEventList;
    }

    public GameEventThemeInfo GetCurrentTheme()
    {
        GameEventThemeInfo lastTheme = null;
        foreach (KeyValuePair<string, GameEventThemeInfo> theme in _themeList)
        {
            lastTheme = theme.Value;
            bool isValidDate = DateTimeData.GetDateTimeUTC() > theme.Value.ReleaseDateTimeUTC && DateTimeData.GetDateTimeUTC() < theme.Value.EndDateTimeUTC;
            if (isValidDate)
                return theme.Value;
        }
        return lastTheme;
    }

    public GameEventActiveECInfo GetActiveCurrencyInfo()
    {
        GameEventActiveECInfo lastActiveCurrency = null;
        foreach (KeyValuePair<string, GameEventActiveECInfo> EC in _eventCurrencyList)
        {
            lastActiveCurrency = EC.Value;
            bool isValidDate = DateTimeData.GetDateTimeUTC() > EC.Value.ReleaseDateTimeUTC && DateTimeData.GetDateTimeUTC() < EC.Value.EndDateTimeUTC;
            if (isValidDate)
                return EC.Value;
        }
        return lastActiveCurrency;
    }

    public GameEventInfo GetActiveGameEventQuest()
    {
        GameEventInfo lastEventQuest = null;
        foreach (KeyValuePair<string, GameEventInfo> quest in _gameEventQuestList)
        {
            lastEventQuest = quest.Value;
            bool isValidDate = DateTimeData.GetDateTimeUTC() > quest.Value.ReleaseDateTimeUTC && DateTimeData.GetDateTimeUTC() < quest.Value.EndDateTimeUTC;
            if (isValidDate)
                return quest.Value;
        }
        return lastEventQuest;
    }

    public List<string> GetValidGameEventInfoKeyList()
    {
        List<string> gameEventInfoKeyList = new List<string>();
        foreach (KeyValuePair<string, GameEventInfo> data in _gameEventList)
        {
            string eventKey = data.Key;
            GameEventInfo eventInfo = data.Value;
            bool isTimeValid = DateTimeData.GetDateTimeUTC() > eventInfo.ReleaseDateTimeUTC && DateTimeData.GetDateTimeUTC() < eventInfo.EndDateTimeUTC;
            if (isTimeValid)
            {
                gameEventInfoKeyList.Add(eventInfo.EventKey);
            }
        }
        return gameEventInfoKeyList;
    }
}

public class GameEventInfo
{
    public string EventKey { get; private set; }
    public string Template { get; private set; }

    public DateTime ReleaseDateTimeUTC { get; private set; }
    public DateTime EndDateTimeUTC { get; private set; }

    public GameEventInfo(string eventKey, string json)
    {
        this.EventKey = eventKey;
        Dictionary<string, object> data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);

        Template = data.ContainsKey("template") == false ? "" : data["template"].ToString();
        ReleaseDateTimeUTC = data.ContainsKey("release_date") == false ? DateTime.MinValue : Convert.ToDateTime(data["release_date"]).ToUniversalTime();
        EndDateTimeUTC = data.ContainsKey("end_date") == false ? DateTime.MaxValue : Convert.ToDateTime(data["end_date"]).ToUniversalTime();
    }

}

public class GameEventThemeInfo
{
    public string Theme { get; private set; }
    public DateTime ReleaseDateTimeUTC { get; private set; }
    public DateTime EndDateTimeUTC { get; private set; }

    public GameEventThemeInfo(string theme, string json)
    {
        this.Theme = theme;

        Dictionary<string, string> data = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

        ReleaseDateTimeUTC = data.ContainsKey("release_date") == false ? DateTime.MinValue : Convert.ToDateTime(data["release_date"]).ToUniversalTime();
        EndDateTimeUTC = data.ContainsKey("end_date") == false ? DateTime.MaxValue : Convert.ToDateTime(data["end_date"]).ToUniversalTime();
    }

}

public class GameEventActiveECInfo
{
    public string Currency { get; private set; }
    public DateTime ReleaseDateTimeUTC { get; private set; }
    public DateTime EndDateTimeUTC { get; private set; }

    public GameEventActiveECInfo(string currency, string json)
    {
        this.Currency = currency;
        Dictionary<string, string> data = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

        ReleaseDateTimeUTC = data.ContainsKey("release_date") == false ? DateTime.MinValue : Convert.ToDateTime(data["release_date"]).ToUniversalTime();
        EndDateTimeUTC = data.ContainsKey("end_date") == false ? DateTime.MaxValue : Convert.ToDateTime(data["end_date"]).ToUniversalTime();
    }

}