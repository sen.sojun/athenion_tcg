﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SlotData
{
    #region Enums
    #endregion

    #region Public Properties
    public int SlotID { get; private set; }
    public int BattleCardUniqueID { get; private set; }

    //slot buff status
    public bool IsBeUnlock { get { return !IsSlotStatus(SlotStatusType.Be_Lock); } }
    public bool IsBeDarkMatter { get { return IsSlotStatus(SlotStatusType.Be_DarkMatter); } }

    public bool IsSlotEmpty { get { return (IsBeUnlock && BattleCardUniqueID == -1); } }
    public bool[] LinkDir { get { return _linkDir; } }
    public List<SlotBuffData> BuffDataList { get { return _buffDataList; } }
    #endregion

    #region Private Properties
    private List<SlotBuffData> _buffDataList;
    private Dictionary<CardDirectionType, SlotData> _adjacentSlotDict;
    private Dictionary<EventKey, UnityEvent<Requester>> _triggerCallbackEvent;
    private bool[] _linkDir = new bool[8] { false, false, false, false, false, false, false, false };
    private bool[] _linkDirPrevious = new bool[8] { false, false, false, false, false, false, false, false };

    private SlotStatus _slotStatus;
    #endregion

    #region Constructor
    public SlotData(int slotID)
    {
        SlotID = slotID;
        _slotStatus = new SlotStatus();
        BattleCardUniqueID = -1;
        _adjacentSlotDict = new Dictionary<CardDirectionType, SlotData>();
        _triggerCallbackEvent = new Dictionary<EventKey, UnityEvent<Requester>>();
    }

    //public SlotData(SlotData data)
    //{
    //    SlotID = data.SlotID;
    //    BattleCardUniqueID = data.BattleCardUniqueID;
    //    IsEnabled = data.IsEnabled;
    //    _adjacentSlotDict = new Dictionary<CardDirectionType, SlotData>(data._adjacentSlotDict);
    //    _triggerCallbackEvent = new Dictionary<EventKey, UnityEvent<int>>(data._triggerCallbackEvent);
    //}
    #endregion

    #region Methods
    public bool IsSlotLockBeMined(PlayerIndex playerIndex)
    {
        foreach (SlotBuffData buff in BuffDataList)
        {
            if (buff.BuffType == SlotBuffTypes.BuffSlotLock
                && buff.Requester.PlayerIndex == playerIndex)
            {
                return true;
            }
        }

        return false;
    }

    public bool IsSlotDarkMatterBeMined(PlayerIndex playerIndex)
    {
        foreach (SlotBuffData buff in BuffDataList)
        {
            if (buff.BuffType == SlotBuffTypes.BuffSlotDarkMatter
                && buff.Requester.PlayerIndex == playerIndex)
            {
                return true;
            }
        }

        return false;
    }

    public void AddLinkDir(CardDirectionType cardDirectionType)
    {
        _linkDir[(int)cardDirectionType] = true;
    }

    public void RemoveLinkDir(CardDirectionType cardDirectionType)
    {
        _linkDir[(int)cardDirectionType] = false;
    }

    public void ResetLinkDir()
    {
        _linkDir = new bool[8] { false, false, false, false, false, false, false, false };
    }

    public bool IsLinkStatusUpdated()
    {
        bool result = true;
        for(int i = 0; i < _linkDir.Length; i++)
        {
            if (_linkDir[i] != _linkDirPrevious[i])
            {
                result = false;
                break;
            }
        }

        return result;
    }

    public void UpdateLinkStatus()
    {
        _linkDirPrevious = _linkDir.ToArray();
    }

    public bool GetAdjacentSlot(CardDirectionType cardDirectionType, out SlotData slotData)
    {
        slotData = null;

        if(_adjacentSlotDict.Count <= 0)
        {
            int maxBound = GameManager.Instance.BoardWidth * GameManager.Instance.BoardHeight;

            if (GetNearbySlotID(CardDirectionType.N) >= 0)
            {
                // north
                _adjacentSlotDict.Add(
                      CardDirectionType.N
                    , GameManager.Instance.Data.SlotDatas[GetNearbySlotID(CardDirectionType.N)]
                );

                // north-east
                if (GetNearbySlotID(CardDirectionType.NE) % GameManager.Instance.BoardWidth != 0)
                {
                    _adjacentSlotDict.Add(
                          CardDirectionType.NE
                        , GameManager.Instance.Data.SlotDatas[GetNearbySlotID(CardDirectionType.NE)]
                    );
                }

                // north-west
                if (GetNearbySlotID(CardDirectionType.N) % GameManager.Instance.BoardWidth != 0)
                {
                    _adjacentSlotDict.Add(
                          CardDirectionType.NW
                        , GameManager.Instance.Data.SlotDatas[GetNearbySlotID(CardDirectionType.NW)]
                    );
                }
            }

            if (GetNearbySlotID(CardDirectionType.S) < maxBound)
            {
                //south
                _adjacentSlotDict.Add(
                      CardDirectionType.S
                    , GameManager.Instance.Data.SlotDatas[GetNearbySlotID(CardDirectionType.S)]
                );

                //south-east
                if (GetNearbySlotID(CardDirectionType.SE) % GameManager.Instance.BoardWidth != 0)
                {
                    _adjacentSlotDict.Add(
                          CardDirectionType.SE
                        , GameManager.Instance.Data.SlotDatas[GetNearbySlotID(CardDirectionType.SE)]
                    );
                }

                //south-west
                if (GetNearbySlotID(CardDirectionType.S) % GameManager.Instance.BoardWidth != 0)
                {
                    _adjacentSlotDict.Add(
                          CardDirectionType.SW
                        , GameManager.Instance.Data.SlotDatas[GetNearbySlotID(CardDirectionType.SW)]
                    );
                }
            }

            //east
            if (GetNearbySlotID(CardDirectionType.E) % GameManager.Instance.BoardWidth != 0)
            {
                _adjacentSlotDict.Add(
                      CardDirectionType.E
                    , GameManager.Instance.Data.SlotDatas[GetNearbySlotID(CardDirectionType.E)]
                );
            }

            //west
            if (SlotID % GameManager.Instance.BoardWidth != 0)
            {
                _adjacentSlotDict.Add(
                      CardDirectionType.W
                    , GameManager.Instance.Data.SlotDatas[GetNearbySlotID(CardDirectionType.W)]
                );
            }
        }

        if(_adjacentSlotDict.ContainsKey(cardDirectionType))
        {
            slotData = _adjacentSlotDict[cardDirectionType];
            return true;
        }
        else
        {
            slotData = null;
            return false;
        }
    }

    private int GetNearbySlotID(CardDirectionType directionType)
    {
        int northIndex = SlotID - GameManager.Instance.BoardWidth;
        int eastIndex = SlotID + 1;
        int southIndex = SlotID + GameManager.Instance.BoardWidth;
        int westIndex = SlotID - 1;

        switch (directionType)
        {
            case CardDirectionType.N: return northIndex;
            case CardDirectionType.NE: return northIndex + 1;
            case CardDirectionType.E: return eastIndex;
            case CardDirectionType.SE: return southIndex + 1;
            case CardDirectionType.S: return southIndex;
            case CardDirectionType.SW: return southIndex - 1;
            case CardDirectionType.W: return westIndex;
            case CardDirectionType.NW: return northIndex - 1;
        }

        return SlotID;
    }

    public void SetBattleCardUniqueID(int battleCardUniqueID)
    {
        BattleCardUniqueID = battleCardUniqueID;
    }

    public void SetBeLock(bool isLock)
    {
        if (isLock)
        {
            _slotStatus.Add(SlotStatusType.Be_Lock);
        }
        else
        {
            _slotStatus.Remove(SlotStatusType.Be_Lock);
        }
    }

    public void SetBeDarkMatter(bool isDarkMatter)
    {
        if (isDarkMatter)
        {
            _slotStatus.Add(SlotStatusType.Be_DarkMatter);
        }
        else
        {
            _slotStatus.Remove(SlotStatusType.Be_DarkMatter);
        }
    }

    public void ResetBattleCardUniqueID()
    {
        BattleCardUniqueID = -1;
    }

    public bool IsSlotStatus(SlotStatusType status)
    {
        if (_slotStatus != null)
        {
            return (_slotStatus.IsSlotStatus(status));
        }

        return false;
    }

    #region Event
    public bool IsKeySubscribed(EventKey key)
    {
        if (_triggerCallbackEvent.ContainsKey(key))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void SubscribeEvent(EventKey key, UnityAction<Requester> callback)
    {
        if (_triggerCallbackEvent.ContainsKey(key))
        {
            _triggerCallbackEvent[key].AddListener(callback);
        }
        else
        {
            UnityEvent<Requester> events = new RequesterEvent();
            events.AddListener(callback);

            _triggerCallbackEvent.Add(key, events);
        }
    }

    public bool UnsubscribeEvent(EventKey key, UnityAction<Requester> callback)
    {
        if (_triggerCallbackEvent.ContainsKey(key))
        {
            _triggerCallbackEvent[key].RemoveListener(callback);
            return true;
        }

        return false;
    }

    public void TriggerEvent(EventKey key, Requester triggerer)
    {
        if (_triggerCallbackEvent.ContainsKey(key))
        {
            if (key == EventKey.LastWish)
            {
                // Last wish event
                // Play last wish effect.
                //UIManager.Instance.ShowLastWishOnMinion();
            }

            _triggerCallbackEvent[key].Invoke(triggerer);
        }
    }
    #endregion

    #region Buff
    public void AddBuff(SlotBuffData buffData, UnityAction onComplete)
    {
        //Debug.LogWarning("AddSlotBuff");
        if (_buffDataList == null)
        {
            _buffDataList = new List<SlotBuffData>();
        }

        buffData.Subscribe(this);
        _buffDataList.Add(buffData);
        _buffDataList.Last().ActiveBuff();

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void CountBuff(SlotBuffData buffData, UnityAction onComplete)
    {
        UIManager.Instance.SetLockDuration(SlotID, buffData.CounterAmount, buffData.Requester.PlayerIndex);

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void RemoveBuff(string key, UnityAction onComplete)
    {
        SlotBuffData buff = null;
        if (_buffDataList != null && _buffDataList.Count > 0)
        {
            for (int index = 0; index < _buffDataList.Count; ++index)
            {
                if (_buffDataList[index].Key == key)
                {
                    buff = _buffDataList[index];
                    buff.RemoveBuff();
                    _buffDataList.RemoveAt(index);
                    break;
                }
            }
        }

        //ResetAllBuff();

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void RemoveBuff(SlotBuffTypes type, UnityAction onComplete)
    {
        SlotBuffData buff = null;
        if (_buffDataList != null && _buffDataList.Count > 0)
        {
            for (int index = 0; index < _buffDataList.Count; ++index)
            {
                if (_buffDataList[index].BuffType == type)
                {
                    buff = _buffDataList[index];
                    RemoveBuff(buff.Key, null);
                }
            }
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ResetAllBuff()
    {
        _slotStatus = new SlotStatus();
        if (_buffDataList != null && _buffDataList.Count > 0)
        {
            for (int index = 0; index < _buffDataList.Count; ++index)
            {
                SlotBuffData buff = _buffDataList[index];
                buff.UnsubscribeAll();
            }
        }
    }
    #endregion

    #endregion
}
