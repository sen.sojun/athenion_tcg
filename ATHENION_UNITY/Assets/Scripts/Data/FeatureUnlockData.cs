﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FeatureUnlockData
{
    #region Properties
    public bool IsEnable;
    public int UnlockCount;
    #endregion

    #region Constructors
    public FeatureUnlockData()
    {
        IsEnable = false;
        UnlockCount = 0;
    }

    public FeatureUnlockData(bool isEnable, int unlockCount)
    {
        IsEnable = isEnable;
        UnlockCount = unlockCount;
    }

    public FeatureUnlockData(FeatureUnlockData data)
    {
        IsEnable = data.IsEnable;
        UnlockCount = data.UnlockCount;
    }
    #endregion

    #region Methods
    #endregion
}