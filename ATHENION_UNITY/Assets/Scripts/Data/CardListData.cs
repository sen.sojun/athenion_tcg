﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class CardListData : IEnumerable<KeyValuePair<string, int>>
{
    #region Public Properties
    public int CountCard
    {
        get
        {
            if (_dict != null)
            {
                return _dict.Count;
            }

            return 0;
        }
    }

    public int CountAmount
    {
        get
        {
            if (_dict != null)
            {
                int count = 0;
                foreach (KeyValuePair<string, int> item in _dict)
                {
                    count += item.Value;
                }

                return count;
            }

            return 0;
        }
    }
    #endregion

    #region Private Properties
    private Dictionary<string, int> _dict;
    private List<string> _order;
    #endregion

    #region Constructors
    public CardListData()
    {
        _dict = new Dictionary<string, int>();
        _order = new List<string>();
    }

    public CardListData(Dictionary<string, int> dict)
    {
        _dict = new Dictionary<string, int>(dict);
        _order = new List<string>();

        foreach (KeyValuePair<string, int> item in _dict)
        {
            for (int count = 0; count < item.Value; ++count)
            {
                _order.Add(item.Key);
            }
        }
    }

    public CardListData(List<string> list)
    {
        _dict = new Dictionary<string, int>();
        _order = new List<string>(list);

        foreach (string id in _order)
        {
            if (_dict.ContainsKey(id))
            {
                _dict[id] += 1;
            }
            else
            {
                _dict.Add(id, 1);
            }
        }
    }

    // Copy Contructor
    public CardListData(CardListData cardListData)
    {
        _dict = new Dictionary<string, int>(cardListData._dict);
        _order = new List<string>(cardListData._order);
    }
    #endregion

    #region Methods
    public List<string> GetOrderList()
    {
        return new List<string>(_order);
    }

    public void ShuffleList()
    {
        _order = _order.Shuffle();
    }

    public bool TryGetValue(string cardID, out int value)
    {
        return _dict.TryGetValue(cardID, out value);
    }

    private void SetValue(string cardID, int value)
    {
        if (_dict.ContainsKey(cardID))
        {
            _dict[cardID] = value;
        }
    }

    public void AddCard(string cardID, int value = 1)
    {
        if (value >= 0)
        {
            if (_dict.ContainsKey(cardID))
            {
                SetValue(cardID, _dict[cardID] + value);
            }
            else
            {
                _dict.Add(cardID, value);
            }

            for (int count = 0; count < value; ++count)
            {
                _order.Add(cardID);
            }
        }
        else if (value < 0)
        {
            RemoveCard(cardID, -value);

            _order.Remove(cardID);
        }
    }

    public bool RemoveCard(string cardID, int value = 1)
    {
        if (value > 0)
        {
            if (_dict.ContainsKey(cardID))
            {
                if (_dict[cardID] > value)
                {
                    _dict[cardID] = _dict[cardID] - value;
                }
                else
                {
                    _dict[cardID] = 0;
                }

                for (int count = 0; count < value; ++count)
                {
                    _order.Remove(cardID);
                }

                return true;
            }
        }

        return false;
    }

    public bool RemoveClearCard(string cardID, int value = 1)
    {
        if (value > 0)
        {
            if (_dict.ContainsKey(cardID))
            {
                if (value < _dict[cardID])
                {
                    _dict[cardID] = _dict[cardID] - value;
                }
                else
                {
                    _dict.Remove(cardID);
                }

                for (int count = 0; count < value; ++count)
                {
                    _order.Remove(cardID);
                }

                return true;
            }
        }

        return false;
    }

    public void ClearEmptyCards()
    {
        List<string> keys = new List<string>();
        foreach (KeyValuePair<string, int> cardKey in _dict)
        {
            if (cardKey.Value == 0)
                keys.Add(cardKey.Key);
        }
        for (int i = 0; i < keys.Count; i++)
        {
            if (_dict != null && _dict.ContainsKey(keys[i]))
                _dict.Remove(keys[i]);
            if (_order != null && _order.Contains(keys[i]))
                _order.Remove(keys[i]);
        }
    }

    public bool ContainKey(string key)
    {
        if (_dict != null)
        {
            return _dict.ContainsKey(key);
        }

        return false;
    }

    public void Clear()
    {
        if (_dict != null)
        {
            _dict.Clear();
        }

        if (_order != null)
        {
            _order.Clear();
        }
    }

    public IEnumerator<KeyValuePair<string, int>> GetEnumerator()
    {
        return _dict.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    /*
    IEnumerator<KeyValuePair<string, int>> IEnumerable<KeyValuePair<string, int>>.GetEnumerator()
    {
        return GetEnumerator();
    }
    */

    public static CardListData JsonToCardListData(string json)
    {
        try
        {
            Dictionary<string, int> dict = JsonConvert.DeserializeObject<Dictionary<string, int>>(json);
            return new CardListData(dict);
        }
        catch (Exception e)
        {
            Debug.LogErrorFormat("JsonToCardListData: Failed to convert JSON. {0} json={1}", e.Message, json);
        }

        return null;
    }

    public string ToJson()
    {
        return JsonConvert.SerializeObject(_dict);
    }

    public override string ToString()
    {
        string result = "";

        foreach (KeyValuePair<string, int> item in _dict)
        {
            if (result.Length > 0)
            {
                result += string.Format(",{0}:{1}", item.Key, item.Value);
            }
            else
            {
                result += string.Format("{0}:{1}", item.Key, item.Value);
            }
        }

        return result;
    }
    #endregion

    #region Operators
    public int this[string key]
    {
        get
        {
            return _dict[key];
        }
        set
        {
            SetValue(key, value);
        }
    }
    #endregion
}
