﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ConditionData
{
    #region Delegates
    #endregion

    #region Public Properties
    public int UniqueID { get; protected set; }
    public DataParam Param { get; protected set; }
    public Requester Requester { get; protected set; }
    #endregion

    #region Protected Properties
    protected BooleanParser.BooleanMethod _method;
    protected static int _uniqueIndex = 0;
    #endregion

    #region Constructors
    public ConditionData(DataParam param, Requester requester, BooleanParser.BooleanMethod method)
    {
        UniqueID = RequestUniqueID();
        Param = param;
        Requester = requester;
        _method = method;
    }

    public ConditionData(ConditionData data)
    {
        UniqueID = data.UniqueID;
        Param = data.Param;
        Requester = data.Requester;

        _method = data._method;
    }
    #endregion

    #region Methods
    public virtual bool IsValid()
    {
        bool result;
        bool isSuccess = _method.Invoke(Param, Requester, out result);

        if (isSuccess)
        {
            return result;
        }
        else
        {
            return true;
        }
    }

    public virtual string GetDebugText()
    {
        return this.GetType().ToString();
    }

    public static bool CreateCondition(DataParam param, Requester requester, out ConditionData data)
    {
        data = null;

        BooleanParser.BooleanMethod method;
        bool isSuccess = BooleanParser.ParamToMethod(param, requester, out method);
        if (isSuccess)
        {
            data = new ConditionData(param, requester, method);
        }

        /*
        switch (param.Key)
        {
            //case "GetVP": data = new GetVPEffect(param, requester); break;
            default: break;
        }
        */

        return (data != null);
    }

    private static int RequestUniqueID()
    {
        return _uniqueIndex++;
    }

    public static void ResetUniqueID()
    {
        _uniqueIndex = 0;
    }
    #endregion
}
