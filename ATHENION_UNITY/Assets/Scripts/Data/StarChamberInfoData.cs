﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class StarChamberInfoData
{
    #region Properties
    public Dictionary<int, StarCubeData> CubeDataList { get; private set; }

    public DateTime NewArrivalEndDate { get; private set; }
    public Dictionary<string, ItemData> NewArrivalList { get; private set; }

    public Dictionary<string, ItemData> RotationPoolList { get; private set; }

    public int RefreshTime { get; private set; }
    
    #endregion

    #region Constructors
    public StarChamberInfoData()
    {
        CubeDataList = new Dictionary<int, StarCubeData>();
        NewArrivalList = new Dictionary<string, ItemData>();
        RotationPoolList = new Dictionary<string, ItemData>();
        RefreshTime = 0;
    }

    public StarChamberInfoData(StarChamberInfoData data)
    {
        CubeDataList = new Dictionary<int, StarCubeData>(data.CubeDataList);
        NewArrivalEndDate = data.NewArrivalEndDate;
        NewArrivalList = new Dictionary<string, ItemData>(data.NewArrivalList);
        RotationPoolList = new Dictionary<string, ItemData>(data.RotationPoolList);
        RefreshTime = data.RefreshTime;
    }
    #endregion

    #region Methods
    public void AddCubeData(int index, StarCubeData data)
    {
        if (CubeDataList == null)
        {
            CubeDataList = new Dictionary<int, StarCubeData>();
        }

        if (CubeDataList.ContainsKey(index))
        {
            CubeDataList[index] = data;
        }
        else
        {
            CubeDataList.Add(index, data);
        }
    }

    public bool RemoveCubeData(int index)
    {
        if (CubeDataList != null)
        {
            if (CubeDataList.ContainsKey(index))
            {
                return CubeDataList.Remove(index);
            }
        }

        return false;
    }

    public void AddNewArrivalCard(string cardID, int amount)
    {
        if (NewArrivalList == null)
        {
            NewArrivalList = new Dictionary<string, ItemData>();
        }

        ItemData data = new ItemData(cardID, amount);

        if (NewArrivalList.ContainsKey(data.ItemID))
        {
            NewArrivalList[data.ItemID] = data;
        }
        else
        {
            NewArrivalList.Add(data.ItemID, data);
        }
    }

    public bool RemoveNewArrivalCard(string cardID, int amount)
    {
        if (NewArrivalList != null && NewArrivalList.ContainsKey(cardID))
        {
            ItemData data = NewArrivalList[cardID];
            if (amount < data.Amount)
            {                
                int newAmount = data.Amount - amount;
                data = new ItemData(cardID, newAmount);
                NewArrivalList[cardID] = data;

                return true;
            }
            else
            {
                return NewArrivalList.Remove(cardID);
            }
        }

        return false;
    }

    public void AddRotationPoolCard(string cardID, int amount)
    {
        if (RotationPoolList == null)
        {
            RotationPoolList = new Dictionary<string, ItemData>();
        }

        ItemData data = new ItemData(cardID, amount);

        if (RotationPoolList.ContainsKey(data.ItemID))
        {
            RotationPoolList[data.ItemID] = data;
        }
        else
        {
            RotationPoolList.Add(data.ItemID, data);
        }
    }

    public bool RemoveRotationPoolCard(string cardID, int amount)
    {
        if (RotationPoolList != null && RotationPoolList.ContainsKey(cardID))
        {
            ItemData data = RotationPoolList[cardID];
            if (amount < data.Amount)
            {
                int newAmount = data.Amount - amount;
                data = new ItemData(cardID, newAmount);
                RotationPoolList[cardID] = data;

                return true;
            }
            else
            {
                return RotationPoolList.Remove(cardID);
            }
        }

        return false;
    }

    public void SetRefreshTime(int refreshTime)
    {
        RefreshTime = refreshTime;
    }

    public string GetDebugText()
    {
        string result = "";

        // Star Cube
        result += "[Star Cube]";
        foreach (KeyValuePair<int, StarCubeData> cube in CubeDataList)
        {
            if (result.Length > 0)
            {
                result += "\n" + cube.Value.GetDebugText();
            }
            else
            {
                result += cube.Value.GetDebugText();
            }
        }

        // New Arrival
        result += "\n[New Arrival]";
        result += string.Format("\nEnd Date : {0}", NewArrivalEndDate.ToString());
        foreach (KeyValuePair<string, ItemData> card in NewArrivalList)
        {
            if (result.Length > 0)
            {
                result += "\n" + card.Value.GetDebugText();
            }
            else
            {
                result += card.Value.GetDebugText();
            }
        }

        // Rotation Pool List
        result += "\n[Rotation Pool List]";
        result += string.Format("\nRefreshTime : {0}", RefreshTime);
        foreach (KeyValuePair<string, ItemData> card in RotationPoolList)
        {
            if (result.Length > 0)
            {
                result += "\n" + card.Value.GetDebugText();
            }
            else
            {
                result += card.Value.GetDebugText();
            }
        }

        return result;
    }

    public static StarChamberInfoData ConvertToStarChamberInfoData(Dictionary<string, string> customData)
    {
        StarChamberInfoData result = new StarChamberInfoData();

        if (customData.ContainsKey("star_cube"))
        {
            List<Dictionary<string, string>> data = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(customData["star_cube"]);

            foreach (Dictionary<string, string> item in data)
            {
                if (item.ContainsKey("index") && item.ContainsKey("last_open_timestamp"))
                {
                    int cubeIndex = int.Parse(item["index"]);
                    DateTime lastOpenTimestamp = GameHelper.ISOTimeStrToUTCDateTime(item["last_open_timestamp"]);
                    int duration = int.Parse(item["duration"]);

                    StarCubeData cubeData = new StarCubeData(cubeIndex, lastOpenTimestamp, duration);
                    result.AddCubeData(cubeIndex, cubeData);
                }
            }
        }

        if (customData.ContainsKey("new_arrival"))
        {
            Dictionary<string, object> data = JsonConvert.DeserializeObject<Dictionary<string, object>>(customData["new_arrival"]);

            result.NewArrivalEndDate = (DateTime)data["end_date"];
            List<Dictionary<string, string>> itemList = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(data["card_list"].ToString());

            foreach (Dictionary<string, string> item in itemList)
            {
                if (item.ContainsKey("card_id") && item.ContainsKey("amount"))
                {
                    string cardID = item["card_id"];
                    int amount = int.Parse(item["amount"]);
                    result.AddNewArrivalCard(cardID, amount);
                }
            }
        }

        if (customData.ContainsKey("rotation_pool"))
        {
            List<Dictionary<string, string>> data = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(customData["rotation_pool"]);

            foreach (Dictionary<string, string> item in data)
            {
                if (item.ContainsKey("card_id") && item.ContainsKey("amount"))
                {
                    string cardID = item["card_id"];
                    int amount = int.Parse(item["amount"]);
                    result.AddRotationPoolCard(cardID, amount);
                }
            }
        }

        if (customData.ContainsKey("refresh_time"))
        {
            result.RefreshTime = int.Parse(customData["refresh_time"]);
        }

        return result;
    }
    #endregion
}
