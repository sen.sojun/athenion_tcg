﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CardDirection
{
    #region Public Properties

    public int[] DirIndex => _dirIndex;

    #endregion

    #region Private Properties
    private int[] _dirIndex;
    #endregion

    #region Constructors
    public CardDirection()
    {
        _dirIndex = new int[8] { 0, 0, 0, 0, 0, 0, 0, 0 };
    }

    public CardDirection(int[] dirIndex)
    {
        _dirIndex = dirIndex.ToArray();
    }

    public CardDirection(CardDirection data)
    {
        _dirIndex = data._dirIndex.ToArray();
    }

    public CardDirection(CardDirectionType direction) : base()
    {
        _dirIndex = new int[8] { 0, 0, 0, 0, 0, 0, 0, 0 };
        _dirIndex[(int)direction] = 1;
    }
    #endregion

    #region Methods
    public bool IsDirection(CardDirectionType dir)
    {
        return _dirIndex[(int)dir] > 0;
    }

    public bool IsDirection(CardDirection dir)
    {
        for (int i = 0; i < _dirIndex.Length; i++)
        {
            if (_dirIndex[i] > 0 && dir._dirIndex[i] <= 0)
                return false;
            if (_dirIndex[i] <= 0 && dir._dirIndex[i] > 0)
                return false;
        }
        return true;
    }

    public void Add(CardDirectionType directionType)
    {
        _dirIndex[(int)directionType]++;
    }

    public void Remove(CardDirectionType directionType)
    {
        _dirIndex[(int)directionType]--;
    }

    public bool IsHasDirection()
    {
        foreach (int dir in _dirIndex)
        {
            if (dir > 0)
                return true;
        }
        return false;
    }

    public List<CardDirectionType> GetAllDirection()
    {
        List<CardDirectionType> result = new List<CardDirectionType>();
        foreach (CardDirectionType dir in System.Enum.GetValues(typeof(CardDirectionType)))
        {
            if (IsDirection(dir))
            {
                result.Add(dir);
            }
        }

        return result;
    }

    public CardDirection GetInvertCardDirection()
    {
        int[] invertDir = new int[8] { 0, 0, 0, 0, 0, 0, 0, 0 };
        for (int i = 0; i < this._dirIndex.Length; i++)
        {
            invertDir[i] = -_dirIndex[i];
        }
        return new CardDirection(invertDir);
    }

    public static CardDirection GetMaxCardDirection(params CardDirection[] value)
    {
        CardDirection result = new CardDirection();

        foreach (CardDirection dirValue in value)
        {
            for (int j = 0; j < dirValue._dirIndex.Length; j++)
            {
                result._dirIndex[j] = Mathf.Max(dirValue._dirIndex[j], result._dirIndex[j]);
            }
        }

        return result;
    }

    public static CardDirection GetCardDirectionNormalized(CardDirection value)
    {
        CardDirection normalizedDirection = new CardDirection();
        for (int i = 0; i < value.DirIndex.Length; i++)
        {
            normalizedDirection.DirIndex[i] = Mathf.Clamp(value.DirIndex[i], 0, 1);
        }
        return normalizedDirection;
    }

    public override string ToString()
    {
        string text = "";
        foreach (CardDirectionType dir in System.Enum.GetValues(typeof(CardDirectionType)))
        {
            if (IsDirection(dir))
            {
                if (text.Length > 0)
                {
                    text += ", " + dir.ToString();
                }
                else
                {
                    text += "{" + dir.ToString();
                }
            }
        }

        if (text.Length <= 0)
        {
            text = "None";
        }
        else
        {
            text += "}";
        }

        return text;
    }
    #endregion

    #region Operators
    public static CardDirection operator +(CardDirection data1, CardDirection data2)
    {
        int[] dirIndex = data1._dirIndex.ToArray();
        for (int i = 0; i < data2._dirIndex.Length; i++)
        {
            dirIndex[i] += data2._dirIndex[i];
        }
        CardDirection data = new CardDirection(dirIndex);
        return data;
    }

    public static CardDirection operator -(CardDirection data1, CardDirection data2)
    {
        int[] dirIndex = data1._dirIndex.ToArray();
        for (int i = 0; i < data2._dirIndex.Length; i++)
        {
            dirIndex[i] -= data2._dirIndex[i];
        }
        CardDirection data = new CardDirection(dirIndex);
        return data;
    }

    public static bool operator ==(CardDirection data1, CardDirection data2)
    {
        for (int i = 0; i < data1._dirIndex.Length; i++)
        {
            if (data1._dirIndex[i] != data2._dirIndex[i])
                return false;
        }
        return true;
    }

    public static bool operator !=(CardDirection data1, CardDirection data2)
    {
        for (int i = 0; i < data1._dirIndex.Length; i++)
        {
            if (data1._dirIndex[i] != data2._dirIndex[i])
                return true;
        }
        return false;
    }

    public bool Equals(CardDirection data)
    {
        return (this._dirIndex == data._dirIndex);
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj))
        {
            return false;
        }
        if (ReferenceEquals(this, obj))
        {
            return true;
        }

        return obj.GetType() == GetType() && Equals((CardDirection)obj);
    }

    public override int GetHashCode()
    {
        return (this._dirIndex.GetHashCode());
    }
    #endregion
}
