﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using System.Text;

public class DeckData
{
    #region Enum
    enum ParamTypes : int
    {
          DeckID = 0
        , DeckName = 1
        , ElementID = 2
        , HeroID = 3
        , CardBackID = 4
        , HUDSkinID = 5
        , Deck = 6
        , Order = 7
    }
    #endregion

    #region Public Properties
    public static readonly int FactionHexDigit = 2;
    public static readonly int UniqueCardAmountHexDigit = 2;
    public static readonly int NCopyCardAmountHexDigit = 2;
    public static readonly int CardCodeHexDigit = 4;
    public static readonly int CardCodeDBDigit = 4;
    public static readonly int MaximumDuplicateCards = 3;

    public string DeckID { get; private set; }
    public string DeckName { get; private set; }
    public string ElementID { get; private set; }
    public string HeroID { get; private set; }
    public string CardBackID { get; private set; }
    public string HUDSkinID { get; private set; }
    public CardListData Deck { get; private set; }
    #endregion

    #region Private Properties
    private int _cardCount = 0;

    private int _soul1Limit = 20;
    private int _soul2LimitPre = 3;
    private int _soul2Limit = 10;
    private int _soul3Limit = 10;

    private int _soul1Count = 0;
    private int _soul2Count = 0;
    private int _soul3Count = 0;

    private int _soul1Fill = 0;
    private int _soul2Fill = 0;
    private int _soul3Fill = 0;

    private bool _isFull1 = false;
    private bool _isFull2 = false;
    private bool _isFull3 = false;
    #endregion

    #region Constructors
    public DeckData()
    {
        DeckID = "";
        DeckName = "";
        ElementID = "";
        HeroID = "";
        CardBackID = "";
        HUDSkinID = "";
        Deck = new CardListData();
    }

    public DeckData(string deckID, string deckName, string elementID, string heroID, string cardBackID, string hudSkinID, CardListData deck)
    {
        DeckID = deckID;
        DeckName = deckName;
        ElementID = elementID;
        HeroID = heroID;
        CardBackID = cardBackID;
        HUDSkinID = hudSkinID;
        Deck = new CardListData(deck);
    }

    public DeckData(DeckData deckData)
    {
        if (deckData != null)
        {
            DeckID = deckData.DeckID;
            DeckName = deckData.DeckName;
            ElementID = deckData.ElementID;
            HeroID = deckData.HeroID;
            CardBackID = deckData.CardBackID;
            HUDSkinID = deckData.HUDSkinID;
            Deck = new CardListData(deckData.Deck);
        }
        else
        {
            DeckID = "";
            DeckName = "";
            ElementID = "";
            HeroID = "";
            CardBackID = "";
            HUDSkinID = "";
            Deck = new CardListData();
        }
    }

    public DeckData(string deckCode)
    {
        DeckID = "";
        DeckName = "";
        HeroID = "";
        CardBackID = "";
        HUDSkinID = "";
        Tuple<CardElementType, Dictionary<string, int>> a = DecodeDeckCode(deckCode);
        ElementID = a.Item1.ToString();
        Deck = new CardListData();
        foreach(KeyValuePair<string, int> item in a.Item2)
        {
            Deck.AddCard(item.Key, item.Value);
        }
    }
    #endregion

    #region Methods

    #region Auto Deck
    public void AutoDeck()
    {
        CardListData inventoryDataStack = new CardListData(DataManager.Instance.InventoryData.CardInventoryList);
        inventoryDataStack.ShuffleList();

        CalculateCard();
        CalculateList(inventoryDataStack);
    }

    private void CalculateCard()
    {
        _cardCount = 0;
        _soul1Count = 0;
        _soul2Count = 0;
        _soul3Count = 0;

        CountCurrentList();
        CalculateFillCount();
    }

    private void CountCurrentList()
    {
        // หา card limit
        foreach (KeyValuePair<string, int> item in Deck)
        {
            CardData cardData = CardData.CreateCard(item.Key);
            if (cardData.Spirit == 1)
            {
                _soul1Count += item.Value;
            }
            else if (cardData.Spirit == 2)
            {
                _soul2Count += item.Value;
            }
            else if (cardData.Spirit == 3)
            {
                _soul3Count += item.Value;
            }
            _cardCount += item.Value;
        }
        Debug.Log(string.Format("<b>Count: S1:{0} S2:{1} S3:{2} SUM:{3}</b>", _soul1Count, _soul2Count, _soul3Count, _cardCount));
    }

    private void CalculateFillCount()
    {
        _soul1Fill = 0;
        _soul2Fill = 0;
        _soul3Fill = 0;
        int soul1Fill = Mathf.Max(0, _soul1Limit - _soul1Count);
        int soul2Fill = Mathf.Max(0, _soul2Limit - _soul2Count);
        int soul3Fill = Mathf.Max(0, _soul3Limit - _soul3Count);

        int maxCardPerDeck = GameHelper.MaxCardPerDeck;
        int fillCardCount = maxCardPerDeck - _cardCount;

        // fill soul 2 pre
        int soul2PreValue = Mathf.Min(Mathf.Min(fillCardCount, _soul2LimitPre), soul2Fill);
        _soul2Fill += soul2PreValue;
        soul2Fill = Mathf.Max(0, soul2Fill - soul2PreValue);
        fillCardCount = Mathf.Max(0, fillCardCount - soul2PreValue);

        // fill soul 1
        int soul1Value = Mathf.Min(fillCardCount, soul1Fill);
        _soul1Fill += soul1Value;
        soul1Fill = Mathf.Max(0, soul1Fill - soul1Value);
        fillCardCount = Mathf.Max(0, fillCardCount - soul1Value);

        // fill soul 3
        int soul3Value = Mathf.Min(fillCardCount, soul3Fill);
        _soul3Fill += soul3Value;
        soul3Fill = Mathf.Max(0, soul3Fill - soul3Value);
        fillCardCount = Mathf.Max(0, fillCardCount - soul3Value);

        // fill soul 2
        int soul2Value = Mathf.Min(fillCardCount, soul2Fill);
        _soul2Fill += soul2Value;
        soul2Fill = Mathf.Max(0, soul2Fill - soul2Value);
        fillCardCount = Mathf.Max(0, fillCardCount - soul2Value);

        _isFull1 = _soul1Fill == 0;
        _isFull2 = _soul2Fill == 0;
        _isFull3 = _soul3Fill == 0;

        Debug.Log(string.Format("<b>NeedToFill: S1:{0} S2:{1} S3:{2} SUM:{3}</b>", _soul1Fill, _soul2Fill, _soul3Fill, _soul1Fill + _soul2Fill + _soul3Fill));
    }

    private void CalculateList(CardListData inventoryStack)
    {
        CardListData result = Deck;
        CardListData baseResult = new CardListData();
        CardData cardData = null;
        foreach (KeyValuePair<string, int> card in result)
        {
            if (CardData.GetCardData(card.Key, out cardData))
            {
                baseResult.AddCard(cardData.BaseID, card.Value);
            }
            inventoryStack.RemoveCard(card.Key, card.Value);
        }

        CardElementType element;
        GameHelper.StrToEnum(ElementID, out element);

        if (!_isFull1)
        {
            AddCard(element, 1, ref _soul1Fill, ref inventoryStack, ref result, ref baseResult);
            AddCard(CardElementType.NEUTRAL, 1, ref _soul1Fill, ref inventoryStack, ref result, ref baseResult);
        }
        if (!_isFull2)
        {
            AddCard(element, 2, ref _soul2Fill, ref inventoryStack, ref result, ref baseResult);
            AddCard(CardElementType.NEUTRAL, 2, ref _soul2Fill, ref inventoryStack, ref result, ref baseResult);
        }
        if (!_isFull3)
        {
            AddCard(element, 3, ref _soul3Fill, ref inventoryStack, ref result, ref baseResult);
            AddCard(CardElementType.NEUTRAL, 3, ref _soul3Fill, ref inventoryStack, ref result, ref baseResult);
        }
    }

    private void AddCard(CardElementType element, int soul, ref int limit, ref CardListData inventoryStack, ref CardListData resultList, ref CardListData baseResultList)
    {
        inventoryStack.ShuffleList();
        List<string> orderList = inventoryStack.GetOrderList();

        foreach (string id in orderList)
        {
            CardData cardData = CardData.CreateCard(id);

            // reach limit
            if (limit == 0)
            {
                break;
            }
            // card inventory is empty
            if (inventoryStack[id] <= 0)
            {
                continue;
            }
            // card base is full
            if (baseResultList.ContainKey(cardData.BaseID) && baseResultList[cardData.BaseID] >= GameHelper.MaxCardPerBaseID)
            {
                continue;
            }
            // element valid
            if (cardData.CardElement.ToCardElementType() == element && cardData.Spirit == soul && cardData.Type == CardType.Minion)
            {
                resultList.AddCard(id, 1);
                baseResultList.AddCard(cardData.BaseID, 1);
                inventoryStack.RemoveCard(id, 1);
                limit--;
            }
        }
    }
    #endregion

    #region Manage Deck
    public void RefreshDeck(CardListData refCardListData)
    {
        CardListData result = new CardListData(Deck);
        CardListData refCardList = new CardListData(refCardListData);
        int missingAmount = 0;
        CardData cardData;
        CardData refCardData;
        foreach (KeyValuePair<string, int> item in Deck)
        {
            if(refCardList.ContainKey(item.Key) == false)
            {
                refCardList.AddCard(item.Key, 0);
            }

            if (refCardList[item.Key] >= item.Value)
            {
                continue;
            }

            // find missing amount
            missingAmount = item.Value - refCardList[item.Key];

            cardData = CardData.CreateCard(item.Key);
            foreach (KeyValuePair<string, int> refItem in refCardList)
            {
                if(refItem.Value <= 0
                   || missingAmount <= 0
                   || refItem.Key == item.Key)
                {
                    continue;
                }

                refCardData = CardData.CreateCard(refItem.Key);
                if(cardData.BaseID == refCardData.BaseID)
                {
                    int addAmount = Mathf.Min(missingAmount, refItem.Value);
                    result.AddCard(refItem.Key, addAmount);
                    result.RemoveCard(item.Key, addAmount);
                    missingAmount -= addAmount;
                }
            }
        }

        result.ClearEmptyCards();
        Deck = new CardListData(result);
    }

    public void SetDeckID(string deckID)
    {
        DeckID = deckID;
    }

    public void SetHUDSkinID(string id)
    {
        HUDSkinID = id;
    }
    #endregion

    public string ToJson()
    {
        string json = "";

        Dictionary<string, string> deckDataDict = new Dictionary<string, string>();
        deckDataDict.Add("DeckID", DeckID);
        deckDataDict.Add("DeckName", DeckName);
        deckDataDict.Add("ElementID", ElementID);
        deckDataDict.Add("HeroID", HeroID);
        deckDataDict.Add("CardBackID", CardBackID);
        deckDataDict.Add("HUDSkinID", HUDSkinID);
        deckDataDict.Add("Deck", Deck.ToJson());

        json = JsonConvert.SerializeObject(deckDataDict);

        return json;
    }

    public Dictionary<string, string> ToDictionary()
    {
        Dictionary<string, string> deckDataDict = new Dictionary<string, string>();
        deckDataDict.Add("DeckID", DeckID);
        deckDataDict.Add("DeckName", DeckName);
        deckDataDict.Add("ElementID", ElementID);
        deckDataDict.Add("HeroID", HeroID);
        deckDataDict.Add("CardBackID", CardBackID);
        deckDataDict.Add("HUDSkinID", HUDSkinID);
        deckDataDict.Add("Deck", Deck.ToJson());

        return deckDataDict;
    }

    public static DeckData JsonToDeckData(string json)
    {
        List<string> deckDataStr = new List<string>();
        Dictionary<string, string> rawData = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
        Dictionary<ParamTypes, string> dict = new Dictionary<ParamTypes, string>();

        foreach (ParamTypes param in System.Enum.GetValues(typeof(ParamTypes)))
        {
            if(rawData.ContainsKey(param.ToString()))
            {
                dict.Add(param, rawData[param.ToString()]);
            }
        }

        CardListData cardList = new CardListData();

        if (dict.ContainsKey(ParamTypes.Deck) && !dict.ContainsKey(ParamTypes.Order))
        {
            // Don't care card order.

            Dictionary<string, int> cardIDDict = JsonConvert.DeserializeObject<Dictionary<string, int>>(dict[ParamTypes.Deck]);

            cardList = new CardListData(cardIDDict);
        }

        if (dict.ContainsKey(ParamTypes.Order))
        {
            // Order is matter.

            List<string> cardIDList = JsonConvert.DeserializeObject<List<string>>(dict[ParamTypes.Order]);

            cardList = new CardListData(cardIDList);
        }

        return new DeckData(
                dict[ParamTypes.DeckID]
            , dict[ParamTypes.DeckName]
            , dict[ParamTypes.ElementID]
            , dict[ParamTypes.HeroID]
            , dict[ParamTypes.CardBackID]
            , dict[ParamTypes.HUDSkinID]
            , cardList
        );
    }

    public bool GetElementType(out CardElementType result)
    {
        result = CardElementType.NEUTRAL;
        HeroDBData heroDB;
        if(HeroDB.Instance.GetData(HeroID, out heroDB))
        {
            if (GameHelper.StrToEnum(heroDB.ElementID, out result))
            {
                return true;
            }
        }

        return false;
    }

    //public static DeckData GetDefaultDeck(string deckID, CardElementType elementType)
    //{
    //    DeckData deck = new DeckData();

    //    CardListData cardList;
    //    if (DeckDB.Instance.GetDefaultDeck(elementType, out cardList))
    //    {
    //        string heroID = DataManager.Instance.DefaultHeroIDList[0];
    //        switch (elementType)
    //        {
    //            case CardElementType.FIRE: heroID = DataManager.Instance.DefaultHeroIDList[0]; break;
    //            case CardElementType.WATER: heroID = DataManager.Instance.DefaultHeroIDList[1]; break;
    //            case CardElementType.AIR: heroID = DataManager.Instance.DefaultHeroIDList[3]; break;
    //            case CardElementType.EARTH: heroID = DataManager.Instance.DefaultHeroIDList[2]; break;
    //            case CardElementType.HOLY: heroID = DataManager.Instance.DefaultHeroIDList[4]; break;
    //            case CardElementType.DARK: heroID = DataManager.Instance.DefaultHeroIDList[5]; break;
    //        }

    //        ElementDBData elementDBData;
    //        ElementDB.Instance.GetData(elementType.ToString(), out elementDBData);
    //        string deckName = elementDBData.Name;

    //        deck = new DeckData(
    //              deckID
    //            , deckName
    //            , elementType.ToString()
    //            , heroID
    //            , DataManager.Instance.DefaultCardBackIDList[0]
    //            , DataManager.Instance.DefaultDockIDList[0]
    //            , cardList
    //        );
    //    }

    //    return deck;
    //}

    public static DeckData GetDebugDeck(string deckID)
    {
        CardListData cardList = new CardListData();

        cardList.AddCard("C0015", 20);
        cardList.AddCard("C0081", 20);
        cardList.AddCard("C0093", 20);

        DeckData deck = new DeckData(
              deckID
            , "Debug Deck"
            , CardElementType.FIRE.ToString()
            , DataManager.Instance.DefaultHeroIDList[0]
            , DataManager.Instance.DefaultCardBackIDList[0]
            , DataManager.Instance.DefaultDockIDList[0]
            , cardList
        );

        return deck;
    }

    public static DeckData GetEventBattleDeck(string profileID)
    {
        EventBattleDBData eventBattleData = null;
        HeroData heroData = null;

        if (!EventBattleDB.Instance.GetData(profileID, out eventBattleData))
        {
            return null;
        }

        heroData = HeroData.CreateHero(eventBattleData.Deck.HeroID, PlayerIndex.One);
        if (heroData == null)
        {
            return null;
        }

        return eventBattleData.Deck;
    }

    public static DeckData GetRandomEventBattleDeck()
    {
        List<EventBattleDBData> profileList;

        if (EventBattleDB.Instance.GetAllData(out profileList))
        {
            int index = UnityEngine.Random.Range(0, profileList.Count);
            return GetEventBattleDeck(profileList[index].ProfileID);
        }

        return null;
    }

    public string GetDeckCode()
    {
        string deckCode = null;

        //Validate cardDictInput
        if (Deck == null || Deck.CountCard == 0)
        {
            return null;
        }

        foreach (KeyValuePair<string, int> item in Deck)
        {
            //Start with 'C'
            //Follow with 0-9 & length > 0
            if (!Regex.IsMatch(item.Key, "^C[0-9]+$") || Deck[item.Key] < 1)
            {
                throw new Exception("Invalid input format");
            }
        }

        StringBuilder deckCodeStringBuilder = new StringBuilder();
        string hexToAppend = "";

        //Faction
        hexToAppend = GameHelper.ConvertIntToHexString(int.Parse(ElementID), FactionHexDigit);
        deckCodeStringBuilder.Append(hexToAppend);

        //Initialize Dict<[amount], [list of card codes]> with keys [1, 2, 3]
        Dictionary<int, List<string>> cardAmountDict = new Dictionary<int, List<string>>();
        for (int amount = 1; amount <= MaximumDuplicateCards; amount++)
        {
            cardAmountDict[amount] = new List<string>();
        }

        foreach (KeyValuePair<string, int> item in Deck)
        {
            int amount = Deck[item.Key];

            //n-copy cards case
            if (!cardAmountDict.ContainsKey(amount))
            {
                cardAmountDict[amount] = new List<string>();
            }
            cardAmountDict[amount].Add(item.Key);
        }

        //1-3-copy cards
        for (int amount = 1; amount <= MaximumDuplicateCards; amount++)
        {
            hexToAppend = GameHelper.ConvertIntToHexString(cardAmountDict[amount].Count, UniqueCardAmountHexDigit);
            deckCodeStringBuilder.Append(hexToAppend);
            cardAmountDict[amount].Sort();

            //Add card codes
            foreach (string code in cardAmountDict[amount])
            {
                //Remove "C" prefix
                hexToAppend = GameHelper.ConvertIntToHexString(int.Parse(code.Substring(1)), CardCodeHexDigit);
                deckCodeStringBuilder.Append(hexToAppend);
            }
        }

        //N-copy unique card amount
        List<int> cardAmountList = new List<int>(cardAmountDict.Keys);
        cardAmountList.Sort();
        int sumOfNUniqueCards = 0;
        for (int listIndex = MaximumDuplicateCards; listIndex < cardAmountList.Count; listIndex++)
        {
            int amount = cardAmountList[listIndex];
            sumOfNUniqueCards += cardAmountDict[amount].Count;
        }

        hexToAppend = GameHelper.ConvertIntToHexString(sumOfNUniqueCards, UniqueCardAmountHexDigit);
        deckCodeStringBuilder.Append(hexToAppend);

        //N-copy cards
        for (int listIndex = MaximumDuplicateCards; listIndex < cardAmountList.Count; listIndex++)
        {
            int amount = cardAmountList[listIndex];
            cardAmountDict[amount].Sort();

            //Add card codes
            foreach (string code in cardAmountDict[amount])
            {
                //Remove "C" prefix
                hexToAppend = GameHelper.ConvertIntToHexString(int.Parse(code.Substring(1)), CardCodeHexDigit);
                deckCodeStringBuilder.Append(hexToAppend);

                //Amount 
                hexToAppend = GameHelper.ConvertIntToHexString(amount, NCopyCardAmountHexDigit);
                deckCodeStringBuilder.Append(hexToAppend);
            }
        }

        Debug.Log("Deck code (before encoded): " + deckCodeStringBuilder.ToString());
        byte[] byteData = System.Text.ASCIIEncoding.ASCII.GetBytes(deckCodeStringBuilder.ToString());
        deckCode = System.Convert.ToBase64String(byteData);
        Debug.Log("Deck code (after encoded): " + deckCode);

        return deckCode;
    }

    public Tuple<CardElementType, Dictionary<string, int>> DecodeDeckCode(string deckCode)
    {
        CardElementType deckFaction = CardElementType.NEUTRAL;
        Dictionary<string, int> cardDictOutput = null;

        string base64Decoded;
        byte[] byteData = System.Convert.FromBase64String(deckCode);
        base64Decoded = System.Text.ASCIIEncoding.ASCII.GetString(byteData);

        //Validate length
        int minimumInputLength = FactionHexDigit + ((MaximumDuplicateCards + 1) * UniqueCardAmountHexDigit);
        if (string.IsNullOrWhiteSpace(base64Decoded) || base64Decoded.Length < minimumInputLength)
        {
            throw new Exception("Invalid input format");
        }

        //Increases for each substring calls
        int currentSubStringStartIndex = 0;

        //Faction
        string factionText = base64Decoded.Substring(currentSubStringStartIndex, FactionHexDigit);
        currentSubStringStartIndex += FactionHexDigit;
        int convertedIntFromHex = GameHelper.ConvertHexStringToInt(factionText);
        deckFaction = (CardElementType)convertedIntFromHex;

        cardDictOutput = new Dictionary<string, int>();
        //1-3-copy cards
        for (int cardAmount = 1; cardAmount <= MaximumDuplicateCards; cardAmount++)
        {
            string uniqueCardsAmountText = base64Decoded.Substring(currentSubStringStartIndex, UniqueCardAmountHexDigit);
            currentSubStringStartIndex += UniqueCardAmountHexDigit;
            for (int i = 0; i < GameHelper.ConvertHexStringToInt(uniqueCardsAmountText); i++)
            {
                string hexCardCode = base64Decoded.Substring(currentSubStringStartIndex, CardCodeHexDigit);
                string cardCode = "C" + GameHelper.ConvertHexStringToInt(hexCardCode).ToString().PadLeft(CardCodeDBDigit, '0');
                currentSubStringStartIndex += CardCodeHexDigit;
                cardDictOutput[cardCode] = cardAmount;
            }
        }

        //n-copy cards -> try get next nCardOffset characters
        int nCardOffset = CardCodeHexDigit + UniqueCardAmountHexDigit;
        string uniqueNCardsAmountText = base64Decoded.Substring(currentSubStringStartIndex, UniqueCardAmountHexDigit);
        currentSubStringStartIndex += UniqueCardAmountHexDigit;
        for (int i = 0; i < GameHelper.ConvertHexStringToInt(uniqueNCardsAmountText); i++)
        {
            if (currentSubStringStartIndex + nCardOffset > base64Decoded.Length)
            {
                break;
            }

            string cardCodeAndAmountText = base64Decoded.Substring(currentSubStringStartIndex, nCardOffset);
            currentSubStringStartIndex += nCardOffset;
            string cardCode = "C" + GameHelper.ConvertHexStringToInt(cardCodeAndAmountText.Substring(0, CardCodeHexDigit)).ToString().PadLeft(CardCodeDBDigit, '0');
            int cardAmount = GameHelper.ConvertHexStringToInt(cardCodeAndAmountText.Substring(CardCodeHexDigit, NCopyCardAmountHexDigit));
            cardDictOutput[cardCode] = cardAmount;
        }

        return new Tuple<CardElementType, Dictionary<string, int>>(deckFaction, cardDictOutput);
    }
    #endregion
}
