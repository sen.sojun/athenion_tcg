﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HeroData
{
    #region Public Properties
    public int UniqueID { get; private set; }
    public string ID { get; private set; }
    public string Name { get { return GetName(); } }
    public string ImageKey { get; private set; }
    public string ElementID { get; private set; }
    public CardElementType ElementType { get { return _elementType; } }
    public string ActivePowerEffectKey { get; private set; }
    public MinionData SkillCard { get; private set; }
    public PlayerIndex Owner { get; private set; }
    public int SkillQuantityMax { get; private set; }
    public int SkillQuantityCurrent { get; private set; }

    public bool IsSkillCanUsed
    {
        get
        {
            return SkillQuantityCurrent > 0;
        }
    }
    #endregion

    #region Private Properties
    private HeroDBData _data;
    private CardElementType _elementType;
    private static int _uniqueID = 0;
    #endregion

    #region Constructors
    public HeroData(int uniqueID, PlayerIndex playerIndex, HeroDBData data)
    {
        _data = data;
        UniqueID = uniqueID;
        ID = _data.ID;
        ImageKey = _data.ImageKey;
        ElementID = _data.ElementID;
        ActivePowerEffectKey = _data.ActivePowerEffectKey;
        Owner = playerIndex;
        SkillQuantityMax = _data.Quantity;
        SkillQuantityCurrent = SkillQuantityMax;
        SetupSkillCard();

        GameHelper.StrToEnum(ElementID, out _elementType);
    }

    public HeroData(HeroData data)
    {
        UniqueID = data.UniqueID;
        if(data._data != null)
        {
            _data = data._data;
            ID = data.ID;
            ImageKey = data.ImageKey;
            ElementID = data.ElementID;
            ActivePowerEffectKey = data.ActivePowerEffectKey;
            Owner = data.Owner;
            SkillQuantityMax = data.SkillQuantityMax;
            SkillQuantityCurrent = SkillQuantityMax;
            SetupSkillCard();

            GameHelper.StrToEnum(ElementID, out _elementType);
        }
    }
    #endregion

    #region Methods
    public static void ResetUniqueID()
    {
        _uniqueID = 0;
    }

    public string GetName()
    {
        string text;
        LocalizationManager.Instance.GetText("HERO_NAME_" + ID, out text);
        return text;
    }

    public void SetOwner(PlayerIndex owner)
    {
        Owner = owner;

        if (SkillCard != null)
        {
            SkillCard.SetOwner(Owner);
        }
    }

    private void SetupSkillCard()
    {
        CardDBData cardDBData;
        if(CardDB.Instance.GetData(_data.SkillCard, out cardDBData))
        {
            CardData cardData = CardData.CreateCard(cardDBData.ID);
            SkillCard = MinionData.CreateMinion(cardData, DataManager.Instance.DefaultCardBackIDList[0]);
            SkillCard.SetCurrentZone(CardZone.Hero);
            SkillCard.SetOwner(Owner);
            SkillCard.InitAbility();
        }
        else
        {
            SkillCard = null;
        }
    }

    public void SkillUseCounting()
    {
        SkillQuantityCurrent--;
    }

    public static HeroData CreateHero(string heroID, PlayerIndex owner)
    {
        HeroDBData data;
        HeroDB.Instance.GetData(heroID, out data);

        if (data != null)
        {
            return CreateHero(data, owner);
        }
        return null;
    }

    private static HeroData CreateHero(HeroDBData data, PlayerIndex owner)
    {
        return new HeroData(_uniqueID++, owner, data);
    }
    #endregion
}
