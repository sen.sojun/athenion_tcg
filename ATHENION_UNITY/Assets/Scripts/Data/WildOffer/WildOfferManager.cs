﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using WildOfferController;

public class ShowingOfferData
{
    public string customData;
    public ShopItemData shopItemData;

    public ShowingOfferData(ShopItemData shopItemData, string customData)
    {
        this.customData = customData;
        this.shopItemData = shopItemData;
    }
}

public class WildOfferManager : MonoSingleton<WildOfferManager>
{
    public const int MAX_OFFER_COUNT = 3;

    private struct PriorityOffer
    {
        public string Key;
        public int Priority;

        public PriorityOffer(string key, int priority)
        {
            Key = key;
            Priority = priority;
        }
    }

    public static event Action<ShopItemData> OnPurchaseComplete;
    public static event Action<ShopItemData> OnPurchaseFail;
    public static event Action<ShopItemData> OnClose;
    public static event Action<ShopItemData, bool> OnOpen;

    public WildOfferDB WildOfferDB
    {
        get
        {
            if (_wildOfferDB == null)
                _wildOfferDB = DataManager.Instance.GetWildOfferDB();
            return _wildOfferDB;
        }
    }
    private WildOfferDB _wildOfferDB;

    public PlayerWildOfferData PlayerWildOfferData
    {
        get
        {
            if (_playerWildOfferData == null)
                _playerWildOfferData = DataManager.Instance.GetPlayerWildOfferData();
            return _playerWildOfferData;
        }
    }
    private PlayerWildOfferData _playerWildOfferData;

    private Dictionary<string, ShowingOfferData> _showingOffersQueue = new Dictionary<string, ShowingOfferData>();


    private List<OfferController> controllers = new List<OfferController>();

    #region Subscribe Event
    protected override void InitSelf()
    {
        base.InitSelf();
        controllers.Clear();
        controllers.Add(new WO001_OfferController(this));
        controllers.Add(new WO002_OfferController(this));
        controllers.Add(new WO003_OfferController(this));
        controllers.Add(new WO013_OfferController(this));
        controllers.Add(new WO004_OfferController(this));
        controllers.Add(new WO015_OfferController(this));
        controllers.Add(new WO016_OfferController(this));

        // level offers.
        controllers.Add(new LevelOfferController(this, "WO0024", 5));
        controllers.Add(new LevelOfferController(this, "WO0025", 10));
        controllers.Add(new LevelOfferController(this, "WO0026", 20));
        controllers.Add(new LevelOfferController(this, "WO0027", 30));
        controllers.Add(new LevelOfferController(this, "WO0028", 40));

        // login wild offers.
        controllers.Add(new ConsecutiveLoginOfferController(this, "WO0017", 1));
        controllers.Add(new ConsecutiveLoginOfferController(this, "WO0018", 7));
        controllers.Add(new ConsecutiveLoginOfferController(this, "WO0019", 30));

        controllers.Add(new InActiveLoginOfferController(this, "WO0035", 7, 13));
        controllers.Add(new InActiveLoginOfferController(this, "WO0036", 14, int.MaxValue));

        // event wild offers.
        controllers.Add(new WO0022_OfferController(this));
        controllers.Add(new WO0023_OfferController(this));

        // shop wild offers.
        controllers.Add(new WO0020_OfferController(this));

        // hero wild offers.
        if (IsCanAddHeroOffer())
        {
            controllers.Add(new HeroOfferController(this, "WO005", "H0001")); //alex
            controllers.Add(new HeroOfferController(this, "WO006", "H0002")); //tear
            controllers.Add(new HeroOfferController(this, "WO007", "H0005")); //sadia
            controllers.Add(new HeroOfferController(this, "WO008", "H0006")); //tartarus
            controllers.Add(new HeroOfferController(this, "WO009", "H0004")); //ralana
            controllers.Add(new HeroOfferController(this, "WO010", "H0003")); //brundo
        }
        else
        {
            controllers.Add(new WO0021_OfferController(this)); // random hero.
        }

        for (int i = 0; i < controllers.Count; i++)
        {
            controllers[i].OnInit();
        }
    }

    private void OnEnable()
    {
        NavigatorController.OnLogout += NavigatorController_OnLogout;
    }

    private void OnDisable()
    {
        NavigatorController.OnLogout -= NavigatorController_OnLogout;
    }

    #endregion

    #region Event Callback

    private void NavigatorController_OnLogout()
    {
        for (int i = 0; i < controllers.Count; i++)
        {
            controllers[i].OnExit();
        }
        this.DestroySelf();
    }

    #endregion

    public bool IsCanAddOffer()
    {
        return GetShowIconOffer().Count < MAX_OFFER_COUNT;
    }

    public bool IsCanAddHeroOffer()
    {
        return DataManager.Instance.InventoryData.IsHasSkinHero() == false && GetHeroOfferCount() < 4;
    }

    private int GetHeroOfferCount()
    {
        int result = 0;
        List<string> heroOfferKey = new List<string>() { "WO005", "WO006", "WO007", "WO008", "WO009", "WO010" };
        for (int i = 0; i < heroOfferKey.Count; i++)
        {
            if (PlayerWildOfferData.GetAllData().ContainsKey(heroOfferKey[i]))
            {
                result++;
            }
        }
        return result;
    }


    public void AddShowingOfferQueue(string key, ShowingOfferData offerData)
    {
        if (_showingOffersQueue.ContainsKey(key) == false)
        {
            _showingOffersQueue.Add(key, offerData);
        }
        else
        {
            Debug.Log(key + " is already added.");
        }
    }

    public void RemoveShowingOfferQueue(string key)
    {
        _showingOffersQueue.Remove(key);
    }

    public Dictionary<string, ShowingOfferData> GetShowingOfferWithSortingByPriority()
    {
        int maxAmount = MAX_OFFER_COUNT - PlayerWildOfferData.FindValidWildOffer().Count;
        Dictionary<string, WildOfferItemDBData> wildOfferItemData = WildOfferDB.GetWildOfferItemData();

        List<PriorityOffer> sortList = new List<PriorityOffer>();
        foreach (KeyValuePair<string, ShowingOfferData> item in _showingOffersQueue)
        {
            int priority = wildOfferItemData.ContainsKey(item.Key) ? wildOfferItemData[item.Key].Priority : 0;
            sortList.Add(new PriorityOffer(item.Key, priority));
        }
        if (sortList.Count > 0)
        {
            sortList = sortList.OrderBy(data => data.Priority).ToList();
        }

        Dictionary<string, ShowingOfferData> result = new Dictionary<string, ShowingOfferData>();
        for (int i = 0; i < sortList.Count; i++)
        {
            if (i >= maxAmount)
                break;

            string key = sortList[i].Key;
            if (_showingOffersQueue.ContainsKey(key))
            {
                result.Add(key, _showingOffersQueue[key]);
            }
        }
        // set to original data with sorting data.
        _showingOffersQueue = result;
        return result;
    }

    /// <summary>
    /// Get Offers ,that show on home screen.
    /// </summary>
    /// <returns></returns>
    public Dictionary<string, WildOfferData> GetShowIconOffer()
    {
        return PlayerWildOfferData.FindValidWildOffer();
    }

    public void AddDataAndShowWildOffer(string key, string itemID, UnityAction OnBuy = null, UnityAction OnCancel = null, bool isFirstTime = false, string customData = null)
    {
        PopupUIManager.Instance.Show_SoftLoad(true);

        DataManager.Instance.AddWildOffer(key, itemID, () =>
        {
            PopupUIManager.Instance.Show_SoftLoad(false);
            ShowWildOffer(key, itemID, OnBuy, OnCancel, isFirstTime);
        }, customData);
    }

    public void ShowWildOffer(string key, string itemID, UnityAction OnBuy = null, UnityAction OnCancel = null, bool isFirstTime = false)
    {
        Debug.Log("ShowWildOffer " + key + " " + itemID);
        RemoveShowingOfferQueue(key);
        // show popup.
        ShopItemData shopItemData = WildOfferDB.GetShopItemData(itemID);

        UnityAction purchaseSuccess = delegate
        {
            DataManager.Instance.UpdatePurchaseWildOfferStatus(key, null);
            OnBuy?.Invoke();
        };

        UnityAction cancel = delegate
        {
            OnClose?.Invoke(shopItemData);
            OnCancel?.Invoke();
        };

        OnOpen?.Invoke(shopItemData, isFirstTime);
        PopupUIManager.Instance.ShowPopup_PurchaseWildOffer(key, shopItemData, () =>
        {
            PurchaseWildOffer(key, shopItemData, purchaseSuccess);
        }, cancel);
    }

    #region Purchase

    public void PurchaseWildOffer(string key, ShopItemData shopItemData, UnityAction onPurchaseSuccess)
    {
        PopupUIManager.Instance.Show_MidSoftLoad(true);
        StartCoroutine(ShopManager.RequestPurchaseItemProcess(shopItemData, (purchaseResult) => OnPurchaseCallback(purchaseResult, onPurchaseSuccess)));
    }

    public void OnPurchaseCallback(PurchaseResult purchaseResult, UnityAction onPurchaseSuccess)
    {
        PopupUIManager.Instance.HideAllLoad();

        if (purchaseResult.HasError)
        {
            string title = LocalizationManager.Instance.GetText("SHOP_PURCHASE_FAIL_HEADER");
            string message = purchaseResult.GetErrorText();

            PopupUIManager.Instance.ShowPopup_Error(title, message);
            OnPurchaseFail?.Invoke(purchaseResult.PurchaseItemData);
        }
        else
        {
            ShopItemData item = purchaseResult.PurchaseItemData;
            string title = LocalizationManager.Instance.GetText("SHOP_PURCHASE_SUCCESS_HEADER");
            string message = string.Format(LocalizationManager.Instance.GetText("SHOP_YOU_GOT_ITEM"), LocalizationManager.Instance.GetItemName(item.ItemId));

            PopupUIManager.Instance.ShowPopup_GotReward(title, message, item.CustomData.ItemDetail, null);
            // Add analytic event 
            OnPurchaseComplete?.Invoke(purchaseResult.PurchaseItemData);
            onPurchaseSuccess?.Invoke();
        }
    }

    #endregion
}

