﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWildOfferData
{
    private Dictionary<string, WildOfferData> _wildOfferDataList = new Dictionary<string, WildOfferData>();

    public PlayerWildOfferData()
    {
    }

    public PlayerWildOfferData(string json)
    {
        WildOfferDB wildOfferDB = DataManager.Instance.GetWildOfferDB();
        Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
        foreach (KeyValuePair<string, object> data in rawData)
        {
            if (wildOfferDB.IsContain(data.Key))
            {
                int duration = wildOfferDB.GetWildOfferItemData()[data.Key].Duration;
                _wildOfferDataList.Add(data.Key, new WildOfferData(data.Value.ToString(), duration));
            }
        }
    }

    public WildOfferData GetWildOfferData(string key)
    {
        if (_wildOfferDataList.ContainsKey(key))
            return _wildOfferDataList[key];
        else
            return null;
    }

    public Dictionary<string, WildOfferData> GetAllData()
    {
        return _wildOfferDataList;
    }

    public bool IsCanAddWildOffer(string key, string itemID)
    {
        WildOfferDB db = DataManager.Instance.GetWildOfferDB();
        if (db.IsContain(key) && db.GetWildOfferItemData()[key].ItemList.Contains(itemID))
        {
            return FindValidWildOffer().Count <= 3;
        }
        return false;
    }

    public Dictionary<string, WildOfferData> FindValidWildOffer()
    {
        Dictionary<string, WildOfferData> result = new Dictionary<string, WildOfferData>();

        ShopTypeData wildOfferShopType = DataManager.Instance.GetShopTypeData(ShopTypes.WildOffer);
        ShopData wildOfferShop = wildOfferShopType.ShopDict[wildOfferShopType.StoreID[0]];

        foreach (KeyValuePair<string, WildOfferData> data in _wildOfferDataList)
        {
            try
            {
                WildOfferItemDBData wildOfferItem = DataManager.Instance.GetWildOfferDB().GetWildOfferItemData()[data.Key];
                int duration = wildOfferItem.Duration;

                WildOfferData wildOfferData = data.Value;

                bool isValidItemInDB = wildOfferItem.ItemList.Contains(wildOfferData.ItemID);
                bool isValidInStore = wildOfferShop.ShopItemDataList.Find(item => item.ItemId == wildOfferData.ItemID) != null;
                bool isValidTime = DateTimeData.GetDateTimeUTC() < wildOfferData.ExpireDate;
                bool isPurchaseValid = wildOfferData.PurchaseCount < wildOfferItem.Max || wildOfferItem.Max == -1;

                if (isValidItemInDB && isValidInStore && isValidTime && isPurchaseValid)
                {
                    result.Add(data.Key, data.Value);
                }
            }
            catch
            {
                Debug.LogError("key not found " + data.Key);
                break;
            }
        }
        return result;
    }
}

public class WildOfferData
{
    public List<string> LogItemIDList { get => _logItemIDList; private set => _logItemIDList = value; }
    private List<string> _logItemIDList = new List<string>();

    public DateTime TimeStamp { get; set; }
    public int PurchaseCount { get; set; }
    public int OfferCount { get => LogItemIDList.Count; }
    public string ItemID { get => (LogItemIDList.Count > 0) ? LogItemIDList[LogItemIDList.Count - 1] : ""; }
    public DateTime ExpireDate { get; private set; }
    public string CustomData = "";


    public WildOfferData(WildOfferItemDBData wildOfferItemData)
    {
        TimeStamp = DateTimeData.GetDateTimeUTC();
        ExpireDate = CalculateExpireDate(wildOfferItemData.Duration);
    }

    public WildOfferData(string json, int duration)
    {
        Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);

        LogItemIDList = rawData.ContainsKey("logs_item_id") ? JsonConvert.DeserializeObject<List<string>>(rawData["logs_item_id"].ToString()) : new List<string>();
        TimeStamp = rawData.ContainsKey("timestamp") ? DateTime.Parse(rawData["timestamp"].ToString()) : DateTimeData.GetDateTimeUTC();
        PurchaseCount = rawData.ContainsKey("purchase_count") ? int.Parse(rawData["purchase_count"].ToString()) : 0;
         
        CustomData = rawData.ContainsKey("custom_data") ? rawData["custom_data"].ToString() : "";
        ExpireDate = CalculateExpireDate(duration);
    }

    public void AddLogItem(string itemID, int duration)
    {
        TimeStamp = DateTimeData.GetDateTimeUTC();
        LogItemIDList.Add(itemID);
        ExpireDate = CalculateExpireDate(duration);
    }

    public void AddCustomData(string str)
    {
        CustomData = str;
    }

    private DateTime CalculateExpireDate(int duration)
    {
        DateTime result = TimeStamp.AddSeconds(duration);
        return result;
    }

}