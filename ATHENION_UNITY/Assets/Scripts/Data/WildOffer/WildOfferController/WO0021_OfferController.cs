﻿using GameEvent;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WildOfferController
{
    public class WO0021_OfferController : OfferController
    {
        public override string Key => "WO0021";

        public string SaveEmotKey => Key + "Emot";

        private int oldEmotCount = 0;
        private int emotCount = 0;

        private bool trigger = false;
        private bool isCanTrigger { get => emotCount >= 30; }

        ShowingOfferData targetOffer = null;

        public WO0021_OfferController(WildOfferManager manager) : base(manager)
        {
        }

        public override void OnInit()
        {
            emotCount = PlayerPrefs.GetInt(SaveEmotKey, 0);

            GameManager.OnPlayerSendEmote += GameManager_OnPlayerSendEmote;
            ShopManager.OnShowPage += ShopManager_OnShowPage;
            HomeManager.OnInit += HomeManager_OnInit;
        }

        public override void OnExit()
        {
            GameManager.OnPlayerSendEmote -= GameManager_OnPlayerSendEmote;
            HomeManager.OnInit -= HomeManager_OnInit;
            ShopManager.OnShowPage -= ShopManager_OnShowPage;
        }

        #region Methods

        private bool ValidOfferCount()
        {
            WildOfferItemDBData itemDB = _manager.WildOfferDB.GetWildOfferItemData()[Key];
            WildOfferData wildOfferData = _manager.PlayerWildOfferData.GetWildOfferData(Key);
            int offerCount = wildOfferData == null ? 0 : wildOfferData.OfferCount;

            if (offerCount == 1 && itemDB.ItemList.Count == 1)
            {
                return false;
            }
            else if (offerCount >= 2)
            {
                return false;
            }

            return true;
        }

        private List<string> GetNotExistOfferItemID(WildOfferItemDBData item)
        {
            List<string> result = new List<string>(item.ItemList);

            WildOfferData wildOfferData = _manager.PlayerWildOfferData.GetWildOfferData(Key);
            if (wildOfferData != null)
            {
                List<string> logItemList = wildOfferData.LogItemIDList;
                for (int i = 0; i < logItemList.Count; i++)
                {
                    if (result.Contains(logItemList[i]))
                    {
                        result.Remove(logItemList[i]);
                    }
                }
            }

            PlayerInventoryData inventory = DataManager.Instance.InventoryData;
            for (int i = 0; i < result.Count; i++)
            {
                string itemKey = GameHelper.ConvertItemIDToItemKey(result[i]);
                if (inventory.GetAmountByItemID(result[i]) > 0 || inventory.GetAmountByItemID(itemKey) > 0)
                {
                    result.RemoveAt(i);
                    i--;
                }
            }
            return result;
        }

        #endregion

        #region Event Callback

        private void GameManager_OnPlayerSendEmote(string heroID, HeroVoice voice)
        {
            emotCount += 1;
            PlayerPrefs.SetInt(SaveEmotKey, emotCount);
        }

        private void HomeManager_OnInit()
        {
            if (OfferIsShowing())
                return;

            //if (_manager.IsCanAddHeroOffer() == false)
            //{
            //    OnExit();
            //    return;
            //}

            if (IsPlayerContainOffer())
            {
                if (ValidOfferCount() == false)
                {
                    OnExit();
                    return;
                }
            }

            if (isCanTrigger && trigger == false)
            {
                trigger = true;
                oldEmotCount = emotCount;
                return;
            }

            if (trigger && emotCount - oldEmotCount >= 2)
            {
                oldEmotCount = emotCount;
                PlayerPrefs.SetInt(SaveEmotKey, emotCount);

                WildOfferItemDBData item = _manager.WildOfferDB.GetWildOfferItemData()[Key];
                bool shouldOffer = UnityEngine.Random.Range(0f, 100) < item.Chance;
                if (shouldOffer == false)
                    return;

                List<string> validItemID = GetNotExistOfferItemID(item);
                if (validItemID.Count == 0)
                    return;

                string itemID = validItemID[UnityEngine.Random.Range(0, validItemID.Count)];

                ShopItemData shopItemData = DataManager.Instance.GetWildOfferDB().GetShopItemData(itemID);
                targetOffer = new ShowingOfferData(shopItemData, null);

                ClearData();
            }
        }

        private void ClearData()
        {
            oldEmotCount = 0;
            emotCount = 0;
            PlayerPrefs.SetInt(SaveEmotKey, 0);
        }

        private void ShopManager_OnShowPage()
        {
            ShopTypes shopTypes = ShopManager.Instance.CurrentPage.Type;
            if (shopTypes != ShopTypes.Cosmetic)
                return;

            if (targetOffer != null && targetOffer.shopItemData != null)
            {
                string itemId = targetOffer.shopItemData.ItemId;
                DataManager.Instance.AddWildOffer(Key, itemId);
                _manager.ShowWildOffer(Key, itemId);
                targetOffer = null;
                ClearData();
                OnExit();
            }
        }

        #endregion
    }
}
