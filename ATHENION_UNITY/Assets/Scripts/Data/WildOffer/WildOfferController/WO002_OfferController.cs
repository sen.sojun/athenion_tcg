﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WildOfferController
{
    public class WO002_OfferController : OfferController
    {
        public override string Key => "WO002";

        public WO002_OfferController(WildOfferManager manager) : base(manager)
        {
        }

        public override void OnInit()
        {
            if (IsPlayerContainOffer())
            {
                if (_manager.PlayerWildOfferData.GetWildOfferData(Key).OfferCount >= 1)
                    return;
            }

            HomeManager.OnInit += HomeManager_OnInit;
        }

        public override void OnExit()
        {
            HomeManager.OnInit -= HomeManager_OnInit;
        }

        private void HomeManager_OnInit()
        {
            if (DataManager.Instance.PlayerInfo.LevelData.Level < 3)
                return;

            if (IsPlayerContainOffer())
            {
                if (_manager.PlayerWildOfferData.GetWildOfferData(Key).OfferCount >= 1)
                {
                    HomeManager.OnInit -= HomeManager_OnInit;
                    return;
                }
            }

            if (OfferIsShowing())
                return;

            WildOfferItemDBData item = _manager.WildOfferDB.GetWildOfferItemData()[Key];
            //hard core select first item only.
            string itemID = item.ItemList[0];
            bool shouldOffer = UnityEngine.Random.Range(0f, 100) < item.Chance;
            if (shouldOffer == false)
                return;

            //DataManager.Instance.AddWildOffer(Key, itemID);
            ShopItemData shopItemData = DataManager.Instance.GetWildOfferDB().GetShopItemData(itemID);
            ShowingOfferData showingOfferData = new ShowingOfferData(shopItemData, null);
            _manager.AddShowingOfferQueue(Key, showingOfferData);
        }

    }
}
