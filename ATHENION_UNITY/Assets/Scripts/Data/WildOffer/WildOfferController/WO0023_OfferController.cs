﻿using GameEvent;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WildOfferController
{
    public class WO0023_OfferController : OfferController
    {
        public override string Key => "WO0023";

        public WO0023_OfferController(WildOfferManager manager) : base(manager)
        {
        }

        public override void OnInit()
        {
            EventManager.OnOpenTab += EventManager_OnOpenTab;
        }

        public override void OnExit()
        {
            EventManager.OnOpenTab -= EventManager_OnOpenTab;
        }

        /*  (Only count unique item).If the item not purchased 
          in item shop is 3 or more.If the player has lower event currency than the required currency(less than 50%)
          to purchase the rest of event items.Appear only once.*/

        private void EventManager_OnOpenTab(EventPageType pageType)
        {
            GameEventDB gameEventDB = DataManager.Instance.GetGameEventDB();
            TimeSpan deltaTime = gameEventDB.GetCurrentTheme().EndDateTimeUTC - DateTimeData.GetDateTimeUTC();
            bool validTime = deltaTime.TotalDays >= 3 && deltaTime.TotalDays <= 4;

            if (OfferIsShowing() || validTime == false)
            {
                OnExit();
                return;
            }

            string targetOffer = "WO0022";
            if (_manager.GetShowIconOffer().ContainsKey(targetOffer))
            {
                OnExit();
                return;
            }

            if (pageType != EventPageType.Shop)
                return;

            ShopEventManager shopEventManager = EventManager.Instance.CurrentPage.GameObject.GetComponent<ShopEventManager>();
            ShopEventData shopEventData = shopEventManager.ShopEventInfo;
            List<ShopEventItemData> itemList = shopEventData.itemList.FindAll(item => item.Buyable == true);

            if (itemList.Count != 1)
                return;

            string eventCurrency = gameEventDB.GetActiveCurrencyInfo().Currency;
            WildOfferData wildOfferData = _manager.PlayerWildOfferData.GetWildOfferData(Key);
            string customData = wildOfferData == null ? "" : wildOfferData.CustomData;
            if (customData == eventCurrency)
            {
                OnExit();
                return;
            }

            int currentEC = DataManager.Instance.InventoryData.GetEventCurrency(eventCurrency);
            int totalPrice = shopEventManager.CalculateTotolPrice(itemList);

            if (currentEC > totalPrice / 2)
            {
                OnExit();
                return;
            }

            if (_manager.IsCanAddOffer() == false)
                return;

            //hard core select first item only.
            WildOfferItemDBData itemDBData = _manager.WildOfferDB.GetWildOfferItemData()[Key];
            string itemID = itemDBData.ItemList[0];
            bool shouldOffer = UnityEngine.Random.Range(0f, 100) < itemDBData.Chance;
            if (shouldOffer == false)
                return;

            DataManager.Instance.AddWildOffer(Key, itemID, null, eventCurrency);
            _manager.ShowWildOffer(Key, itemID);
        }
    }
}
