﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WildOfferController
{
    public class WO015_OfferController : OfferController
    {
        public override string Key => "WO015";

        private string targetOfferKey = "WO004";
        private int targetItemIndex = 0;

        public WO015_OfferController(WildOfferManager manager) : base(manager)
        {
        }

        public override void OnInit()
        {
            if (IsPlayerContainOffer())
            {
                if (_manager.PlayerWildOfferData.GetWildOfferData(Key).OfferCount >= 1)
                    return;
            }

            GameManager.OnEndGameEvent += GameManager_OnEndGameEvent;
        }

        public override void OnExit()
        {
            GameManager.OnEndGameEvent -= GameManager_OnEndGameEvent;
        }

        private void GameManager_OnEndGameEvent(GameMode mode, bool isVictory, GameManager.EndGameType endGameType)
        {
            if (isVictory)
                return;

            if (mode != GameMode.Rank)
                return;

            if (IsPlayerContainOffer())
            {
                if (_manager.PlayerWildOfferData.GetWildOfferData(Key).OfferCount >= 1)
                {
                    GameManager.OnEndGameEvent -= GameManager_OnEndGameEvent;
                    return;
                }
            }

            int playerRank = DataManager.Instance.PlayerInfo.GetPlayerCurrentRank();
            bool isValidRank = playerRank >= 16 || playerRank <= 35;
            if (isValidRank == false) 
                return;

            if (IsRejectOffer(targetOfferKey) == false)
                return;

            if (IsRejectItemByIndex(targetOfferKey, targetItemIndex) == false)
                return;


            WildOfferItemDBData item = _manager.WildOfferDB.GetWildOfferItemData()[Key];
            // select first item only.
            string itemID = item.ItemList[0];
            bool shouldOffer = UnityEngine.Random.Range(0f, 100) < item.Chance;
            if (shouldOffer == false)
                return;

            //DataManager.Instance.AddWildOffer(Key, itemID);
            ShopItemData shopItemData = DataManager.Instance.GetWildOfferDB().GetShopItemData(itemID);
            ShowingOfferData showingOfferData = new ShowingOfferData(shopItemData, null);
            _manager.AddShowingOfferQueue(Key, showingOfferData);
        }

    }
}
