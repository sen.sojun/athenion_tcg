﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WildOfferController
{
    public abstract class OfferController
    {
        public abstract string Key { get; }

        protected WildOfferManager _manager;

        public OfferController(WildOfferManager manager)
        {
            _manager = manager;
        }

        public abstract void OnInit();
        public abstract void OnExit();

        protected bool IsPlayerContainOffer()
        {
            return _manager.PlayerWildOfferData.GetAllData().ContainsKey(Key);
        }

        protected bool OfferIsShowing()
        {
            return _manager.GetShowIconOffer().ContainsKey(Key);
        }

        protected bool IsRejectOffer(string offerKey)
        {
            bool isContain = _manager.PlayerWildOfferData.GetAllData().ContainsKey(offerKey);
            if (isContain == false)
                return false;

            bool isShowing = _manager.GetShowIconOffer().ContainsKey(offerKey);
            if (isShowing == true)
                return false;

            return true;
        }

        protected bool IsRejectItemByIndex(string key, int index)
        {
            WildOfferItemDBData targetItem = _manager.WildOfferDB.GetWildOfferItemData()[key];
            string targetItemID = targetItem.ItemList[index];

            return IsRejectItemByItemID(key, targetItemID);
        }

        protected bool IsRejectItemByItemID(string key, string itemID)
        {
            if (IsRejectOffer(key) == false)
                return false;

            return _manager.PlayerWildOfferData.GetAllData()[key].LogItemIDList.Contains(itemID);
        }
    }
}