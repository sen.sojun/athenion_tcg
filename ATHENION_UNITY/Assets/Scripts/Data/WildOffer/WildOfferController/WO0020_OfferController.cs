﻿using GameEvent;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WildOfferController
{
    public class WO0020_OfferController : OfferController
    {
        public struct ConsoleOffer
        {
            public string ItemID;
            public string ConsoleID;
        }

        public override string Key => "WO0020";

        private List<ConsoleOffer> _consoleList;

        public WO0020_OfferController(WildOfferManager manager) : base(manager)
        {
        }

        public override void OnInit()
        {
            if (IsPlayerContainOffer())
            {
                if (_manager.PlayerWildOfferData.GetWildOfferData(Key).OfferCount >= 2)
                    return;
            }

            ShopManager.OnShowPage += ShopManager_OnShowPage;
        }

        public override void OnExit()
        {
            ShopManager.OnShowPage -= ShopManager_OnShowPage;
        }

        private void ShopManager_OnShowPage()
        {
            ShopTypes shopTypes = ShopManager.Instance.CurrentPage.Type;
            if (shopTypes != ShopTypes.Cosmetic)
                return;

            if (_consoleList == null)
            {
                InitConsoleList();
            }

            bool isContainConsole = IsContainConsole(_consoleList);
            int level = DataManager.Instance.PlayerInfo.LevelData.Level;
            bool shouldOffer = isContainConsole == false && level >= 10;
            if (shouldOffer == false || OfferIsShowing())
            {
                OnExit();
                return;
            }

            if (_manager.IsCanAddOffer() == false)
                return;

            List<ConsoleOffer> offerConsoleList = FilterValidConsole();

            WildOfferItemDBData data = _manager.WildOfferDB.GetWildOfferItemData()[Key];
            bool randomChance = UnityEngine.Random.Range(0f, 100) < data.Chance;
            if (randomChance == false)
                return;

            int randomOffer = UnityEngine.Random.Range(0, offerConsoleList.Count);
            string itemID = offerConsoleList[randomOffer].ItemID;

            DataManager.Instance.AddWildOffer(Key, itemID);
            _manager.ShowWildOffer(Key, itemID);
        }

        private bool IsContainConsole(List<ConsoleOffer> consoleList)
        {
            PlayerInventoryData inventory = DataManager.Instance.InventoryData;
            for (int i = 0; i < consoleList.Count; i++)
            {
                string itemID = consoleList[i].ConsoleID;
                if (inventory.DockIDList.Contains(itemID))
                {
                    return true;
                }
            }
            return false;
        }

        private List<ConsoleOffer> FilterValidConsole()
        {
            List<ConsoleOffer> result = new List<ConsoleOffer>(_consoleList);
            PlayerInventoryData inventory = DataManager.Instance.InventoryData;
            for (int i = 0; i < result.Count; i++)
            {
                if (inventory.GetAmountByItemID(result[i].ConsoleID) > 0)
                {
                    result.RemoveAt(i);
                    i--;
                }
            }

            WildOfferData offerData = _manager.PlayerWildOfferData.GetWildOfferData(Key);
            if (offerData != null)
            {
                for (int i = 0; i < offerData.LogItemIDList.Count; i++)
                {
                    string itemID = offerData.LogItemIDList[i];
                    for (int j = 0; j < result.Count; j++)
                    {
                        if (result[j].ItemID == itemID)
                        {
                            result.RemoveAt(j);
                            j--;
                            break;
                        }
                    }
                }
            }

            return result;
        }

        private void InitConsoleList()
        {
            _consoleList = new List<ConsoleOffer>();
            WildOfferItemDBData item = _manager.WildOfferDB.GetWildOfferItemData()[Key];
            for (int i = 0; i < item.ItemList.Count; i++)
            {
                string[] text = item.ItemList[i].Split('_');
                string itemID = text[1];

                ConsoleOffer offer = new ConsoleOffer();
                offer.ConsoleID = itemID.ToUpper();
                offer.ItemID = item.ItemList[i];
                _consoleList.Add(offer);
            }
        }
    }
}
