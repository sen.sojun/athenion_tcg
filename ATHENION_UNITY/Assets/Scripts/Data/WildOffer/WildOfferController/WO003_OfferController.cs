﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WildOfferController
{
    public class WO003_OfferController : OfferController
    {
        public override string Key => "WO003";

        private const int LOSE_COUNT_CONDITION = 3;
        int loseCount = 0;
        public WO003_OfferController(WildOfferManager manager) : base(manager)
        {
        }

        public override void OnInit()
        {
            if (IsPlayerContainOffer())
            {
                if (_manager.PlayerWildOfferData.GetWildOfferData(Key).OfferCount >= 1)
                    return;
            }

            GameManager.OnEndGameEvent += GameManager_OnEndGameEvent;
        }

        public override void OnExit()
        {
            GameManager.OnEndGameEvent -= GameManager_OnEndGameEvent;
        }

        private void GameManager_OnEndGameEvent(GameMode mode, bool isVictory, GameManager.EndGameType endGameType)
        {
            if (mode != GameMode.Casual && mode != GameMode.Rank)
                return;

            if (IsPlayerContainOffer())
            {
                if (_manager.PlayerWildOfferData.GetWildOfferData(Key).OfferCount >= 1)
                {
                    GameManager.OnEndGameEvent -= GameManager_OnEndGameEvent;
                    return;
                }
            }

            if (OfferIsShowing())
                return;

            if (isVictory == true)
            {
                loseCount = 0;
                return;
            }
            else
            {
                loseCount++;
            }

            if (loseCount < LOSE_COUNT_CONDITION)
                return;

            if (DataManager.Instance.PlayerInfo.LevelData.Level > 5)
                return;

            WildOfferItemDBData item = _manager.WildOfferDB.GetWildOfferItemData()[Key];
            int chance = CalculateChance(item.Chance, loseCount);

            //hard core select first item only.
            string itemID = item.ItemList[0];
            bool shouldOffer = UnityEngine.Random.Range(0f, 100) < item.Chance;
            if (shouldOffer == false)
                return;

            //DataManager.Instance.AddWildOffer(Key, itemID);

            ShopItemData shopItemData = DataManager.Instance.GetWildOfferDB().GetShopItemData(itemID);
            ShowingOfferData showingOfferData = new ShowingOfferData(shopItemData, null);
            _manager.AddShowingOfferQueue(Key, showingOfferData);
        }

        private int CalculateChance(int chance, int loseCount)
        {
            return chance + (loseCount - LOSE_COUNT_CONDITION) * 20;
        }

    }
}
