﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WildOfferController
{
    public class LevelOfferController : OfferController
    {
        public override string Key => _key;
        private string _key = "";
        private int _level = 0;

        public LevelOfferController(WildOfferManager manager, string key, int level) : base(manager)
        {
            this._key = key;
            this._level = level;
        }

        public override void OnInit()
        {
            HomeManager.OnInit += HomeManager_OnInit;
        }

        public override void OnExit()
        {
            HomeManager.OnInit -= HomeManager_OnInit;
        }

        private void HomeManager_OnInit()
        {
            if (DataManager.Instance.PlayerInfo.LevelData.Level > _level)
            {
                OnExit();
                return;
            }

            if (DataManager.Instance.PlayerInfo.LevelData.Level != _level)
                return;

            if (IsPlayerContainOffer())
            {
                if (_manager.PlayerWildOfferData.GetWildOfferData(Key).OfferCount >= 1)
                {
                    OnExit();
                    return;
                }
            }

            if (OfferIsShowing())
                return;

            WildOfferItemDBData item = _manager.WildOfferDB.GetWildOfferItemData()[Key];
            //hard code select first item only.
            string itemID = item.ItemList[0];
            bool shouldOffer = UnityEngine.Random.Range(0f, 100) < item.Chance;
            if (shouldOffer == false)
                return;

            //DataManager.Instance.AddWildOffer(Key, itemID);
            ShopItemData shopItemData = DataManager.Instance.GetWildOfferDB().GetShopItemData(itemID);
            ShowingOfferData showingOfferData = new ShowingOfferData(shopItemData, null);
            _manager.AddShowingOfferQueue(Key, showingOfferData);
        }

    }

}
