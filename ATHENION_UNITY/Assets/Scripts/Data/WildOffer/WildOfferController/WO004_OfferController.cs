﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WildOfferController
{
    public class WO004_OfferController : OfferController
    {
        public override string Key => "WO004";

        bool isTriggerRankCondition = false;
        int loseCount = 0;

        public WO004_OfferController(WildOfferManager manager) : base(manager)
        {
        }

        public override void OnInit()
        {
            if (IsPlayerContainOffer())
            {
                if (_manager.PlayerWildOfferData.GetWildOfferData(Key).OfferCount >= 1)
                    return;
            }

            GameManager.OnEndGameEvent += GameManager_OnEndGameEvent;
        }

        public override void OnExit()
        {
            GameManager.OnEndGameEvent -= GameManager_OnEndGameEvent;
        }

        private void GameManager_OnEndGameEvent(GameMode mode, bool isVictory, GameManager.EndGameType endGameType)
        {
            if (mode != GameMode.Rank)
                return;

            if (isVictory == true)
                return;

            if (IsPlayerContainOffer())
            {
                if (_manager.PlayerWildOfferData.GetWildOfferData(Key).OfferCount >= 1)
                {
                    GameManager.OnEndGameEvent -= GameManager_OnEndGameEvent;
                    return;
                }
            }

            if (OfferIsShowing())
                return;

            if (isTriggerRankCondition == false)
            {
                isTriggerRankCondition = true;
                return;
            }
            else
            {
                int playerRank = DataManager.Instance.PlayerInfo.GetPlayerCurrentRank();
                bool isValidRank = playerRank >= 16 || playerRank <= 35;
                if (isValidRank == false)
                {
                    return;
                }


                WildOfferItemDBData item = _manager.WildOfferDB.GetWildOfferItemData()[Key];
                int chance = CalculateChance(item.Chance, loseCount);
                loseCount += 1;

                bool shouldOffer = UnityEngine.Random.Range(0f, 100) < chance;
                if (shouldOffer == false)
                    return;

                string itemID = item.ItemList[UnityEngine.Random.Range(0, item.ItemList.Count)];
                //DataManager.Instance.AddWildOffer(Key, itemID);
                ShopItemData shopItemData = DataManager.Instance.GetWildOfferDB().GetShopItemData(itemID);
                ShowingOfferData showingOfferData = new ShowingOfferData(shopItemData, null);
                _manager.AddShowingOfferQueue(Key, showingOfferData);
            }
        }

        private int CalculateChance(int chance, int loseCount)
        {
            return chance + loseCount * 20;
        }

    }
}
