﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WildOfferController
{
    public class WO013_OfferController : OfferController
    {
        public override string Key => "WO013";

        public WO013_OfferController(WildOfferManager manager) : base(manager)
        {
        }

        public override void OnInit()
        {
            if (IsPlayerContainOffer())
            {
                if (_manager.PlayerWildOfferData.GetWildOfferData(Key).OfferCount >= 1)
                    return;
            }

            GameManager.OnEndGameEvent += GameManager_OnEndGameEvent;
        }

        public override void OnExit()
        {
            GameManager.OnEndGameEvent -= GameManager_OnEndGameEvent;
        }

        private void GameManager_OnEndGameEvent(GameMode mode, bool isVictory, GameManager.EndGameType endGameType)
        {
            if (isVictory)
                return;

            if (mode == GameMode.Friendly || mode == GameMode.Event)
                return;

            if (DataManager.Instance.PlayerInfo.LevelData.Level > 7)
                return;

            if (IsPlayerContainOffer())
            {
                if (_manager.PlayerWildOfferData.GetWildOfferData(Key).OfferCount >= 1)
                {
                    GameManager.OnEndGameEvent -= GameManager_OnEndGameEvent;
                    return;
                }
            }

            if (OfferIsShowing())
                return;

            if (IsRejectOffer("WO003") == false)
                return;

            WildOfferItemDBData item = _manager.WildOfferDB.GetWildOfferItemData()[Key];
            //hard core select first item only.
            string itemID = item.ItemList[0];
            bool shouldOffer = UnityEngine.Random.Range(0f, 100) < item.Chance;
            if (shouldOffer == false)
                return;

            //DataManager.Instance.AddWildOffer(Key, itemID);
            ShopItemData shopItemData = DataManager.Instance.GetWildOfferDB().GetShopItemData(itemID);
            ShowingOfferData showingOfferData = new ShowingOfferData(shopItemData, null);
            _manager.AddShowingOfferQueue(Key, showingOfferData);
        }

    }
}
