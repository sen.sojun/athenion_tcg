﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WildOfferController
{
    public class InActiveLoginOfferController : OfferController
    {
        public override string Key => _key;
        private string _key = "";
        private int _minDays = 0;
        private int _maxDays = 0;

        public InActiveLoginOfferController(WildOfferManager manager, string key, int minDays, int maxDays) : base(manager)
        {
            this._key = key;
            this._minDays = minDays;
            this._maxDays = maxDays;
        }

        public override void OnInit()
        {
            HomeManager.OnInit += HomeManager_OnInit;
        }

        public override void OnExit()
        {
            HomeManager.OnInit -= HomeManager_OnInit;
        }

        private void HomeManager_OnInit()
        {
            DailyLoginData dailyLoginData = DataManager.Instance.GetDailyLoginData();
            TimeSpan deltaTime = dailyLoginData.Timestamp - dailyLoginData.LastTimestamp;

            bool canShow = deltaTime.Days >= _minDays && deltaTime.Days <= _maxDays;
            if (canShow == false)
            {
                OnExit();
                return;
            }

            if (IsPlayerContainOffer())
            {
                if (_manager.PlayerWildOfferData.GetWildOfferData(Key).OfferCount >= 1)
                {
                    OnExit();
                    return;
                }
            }

            if (OfferIsShowing())
                return;

            WildOfferItemDBData item = _manager.WildOfferDB.GetWildOfferItemData()[Key];
            //hard core select first item only.
            string itemID = item.ItemList[0];
            bool shouldOffer = UnityEngine.Random.Range(0f, 100) < item.Chance;
            if (shouldOffer == false)
                return;

            //DataManager.Instance.AddWildOffer(Key, itemID);
            ShopItemData shopItemData = DataManager.Instance.GetWildOfferDB().GetShopItemData(itemID);
            ShowingOfferData showingOfferData = new ShowingOfferData(shopItemData, null);
            _manager.AddShowingOfferQueue(Key, showingOfferData);
        }

    }

}