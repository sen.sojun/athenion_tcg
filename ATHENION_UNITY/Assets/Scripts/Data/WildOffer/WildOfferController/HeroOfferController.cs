﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WildOfferController
{
    public class HeroOfferController : OfferController
    {
        public override string Key => _key;
        private string _key = "";

        public string SaveEmotKey => Key + "Emot";

        private string _heroID = "";

        private int oldEmotCount = 0;
        private int emotCount = 0;

        private bool trigger = false;
        private bool isCanTrigger { get => emotCount >= 30; }

        public HeroOfferController(WildOfferManager manager, string key, string heroID) : base(manager)
        {
            _key = key;
            _heroID = heroID;
        }

        public override void OnInit()
        {
            emotCount = PlayerPrefs.GetInt(SaveEmotKey, 0);

            GameManager.OnPlayerSendEmote += GameManager_OnPlayerSendEmote;
            HomeManager.OnInit += HomeManager_OnInit;
        }

        public override void OnExit()
        {
            GameManager.OnPlayerSendEmote -= GameManager_OnPlayerSendEmote;
            HomeManager.OnInit -= HomeManager_OnInit;
        }

        #region Methods

        private bool ValidOfferCount()
        {
            WildOfferItemDBData item = _manager.WildOfferDB.GetWildOfferItemData()[Key];
            int offerCount = _manager.PlayerWildOfferData.GetWildOfferData(Key).OfferCount;

            if (offerCount == 1 && item.ItemList.Count == 1)
            {
                return false;
            }
            else if (offerCount >= 2)
            {
                return false;
            }

            return true;
        }

        private List<string> GetNotExistOfferItemID(WildOfferItemDBData item)
        {
            List<string> result = new List<string>(item.ItemList);
            bool isContainOffer = _manager.PlayerWildOfferData.GetAllData().ContainsKey(Key);
            if (isContainOffer == false)
                return result;

            List<string> logItemList = _manager.PlayerWildOfferData.GetAllData()[Key].LogItemIDList;
            for (int i = 0; i < logItemList.Count; i++)
            {
                if (result.Contains(logItemList[i]))
                {
                    result.Remove(logItemList[i]);
                }
            }

            return result;
        }

        #endregion

        #region Event Callback

        private void GameManager_OnPlayerSendEmote(string heroID, HeroVoice voice)
        {
            if (heroID != this._heroID)
                return;

            emotCount += 1;
            PlayerPrefs.SetInt(SaveEmotKey, emotCount);
        }

        private void HomeManager_OnInit()
        {
            if (OfferIsShowing())
                return;

            if (_manager.IsCanAddHeroOffer() == false)
            {
                OnExit();
                return;
            }

            if (IsPlayerContainOffer())
            {
                if (ValidOfferCount() == false)
                {
                    OnExit();
                    return;
                }
            }

            if (isCanTrigger && trigger == false)
            {
                trigger = true;
                oldEmotCount = emotCount;
                return;
            }

            if (trigger && emotCount - oldEmotCount >= 2)
            {
                oldEmotCount = emotCount;
                PlayerPrefs.SetInt(SaveEmotKey, emotCount);

                WildOfferItemDBData item = _manager.WildOfferDB.GetWildOfferItemData()[Key];
                bool shouldOffer = UnityEngine.Random.Range(0f, 100) < item.Chance;
                if (shouldOffer == false)
                    return;

                List<string> validItemID = GetNotExistOfferItemID(item);
                if (validItemID.Count == 0)
                    return;

                string itemID = validItemID[UnityEngine.Random.Range(0, validItemID.Count)];

                //DataManager.Instance.AddWildOffer(Key, itemID);
                ShopItemData shopItemData = DataManager.Instance.GetWildOfferDB().GetShopItemData(itemID);
                ShowingOfferData showingOfferData = new ShowingOfferData(shopItemData, null);
                _manager.AddShowingOfferQueue(Key, showingOfferData);

            }
        }

        #endregion
    }
}