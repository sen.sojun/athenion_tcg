﻿using Karamucho.Collection;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WildOfferController
{
    public class WO001_OfferController : OfferController
    {
        public override string Key => "WO001";

        public WO001_OfferController(WildOfferManager manager) : base(manager)
        {
        }

        public override void OnInit()
        {
            if (OfferIsShowing())
                return;

            CollectionCardUIManager.OnCancelNewDeckCraftAllMissing += CollectionCardUIManager_OnCancelNewDeckCraftAllMissing;
        }

        public override void OnExit()
        {
            CollectionCardUIManager.OnCancelNewDeckCraftAllMissing -= CollectionCardUIManager_OnCancelNewDeckCraftAllMissing;
        }

        private void CollectionCardUIManager_OnCancelNewDeckCraftAllMissing()
        {
            if (OfferIsShowing())
            {
                OnExit();
                return;
            }

            WildOfferItemDBData item = _manager.WildOfferDB.GetWildOfferItemData()[Key];
            if (IsPlayerContainOffer())
            {
                WildOfferData wildOfferData = _manager.PlayerWildOfferData.GetWildOfferData(Key);
                TimeSpan timeSpan = DateTimeData.GetDateTimeUTC() - wildOfferData.ExpireDate;
                if (timeSpan.Days >= 2)
                {
                    TryOfferItem(item);
                }
            }
            else
            {
                TryOfferItem(item);
            }

        }

        private void TryOfferItem(WildOfferItemDBData item)
        {
            if (_manager.IsCanAddOffer() == false)
                return;

            //hard core select first item only.
            string itemID = item.ItemList[0];
            bool shouldOffer = UnityEngine.Random.Range(0f, 100) < item.Chance;
            if (shouldOffer == false)
                return;

            DataManager.Instance.AddWildOffer(Key, itemID);
            _manager.ShowWildOffer(Key, itemID);
            //_manager.AddShowingOfferQueue(Key, DataManager.Instance.GetWildOfferDB().GetShopItemData(itemID));
        }

    }
}
