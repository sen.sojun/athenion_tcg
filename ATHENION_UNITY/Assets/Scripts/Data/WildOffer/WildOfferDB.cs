﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WildOfferDB
{
    private Dictionary<string, WildOfferItemDBData> WildOfferDict = new Dictionary<string, WildOfferItemDBData>();
    private ShopData _wildOfferShopData;

    public WildOfferDB(string json)
    {
        Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);

        foreach (KeyValuePair<string, object> data in rawData)
        {
            string itemJson = data.Value.ToString();
            WildOfferDict.Add(data.Key, new WildOfferItemDBData(itemJson));
        }
    }

    public Dictionary<string, WildOfferItemDBData> GetWildOfferItemData()
    {
        return WildOfferDict;
    }

    public bool IsContain(string key)
    {
        return WildOfferDict.ContainsKey(key);
    }

    public ShopItemData GetShopItemData(string itemID)
    {
        if (_wildOfferShopData == null)
        {
            ShopTypeData shopTypeData = DataManager.Instance.GetShopTypeData(ShopTypes.WildOffer);
            ShopData shopData = shopTypeData.ShopDict[shopTypeData.StoreID[0]];
            _wildOfferShopData = shopData;
        }
        return _wildOfferShopData.FindItem(itemID);
    }
}

public class WildOfferItemDBData
{
    private const int ONE_DAY_DURATION_MILLISECOND = 3600000;
    private const int MAX_DEFAULT = 1;

    /// <summary>
    /// Duration in millisecion
    /// </summary>
    public int Duration { get; private set; }
    public int Max { get; private set; }
    public int Chance { get; private set; }
    public List<string> ItemList { get; private set; }
    public int Priority { get; private set; }

    public WildOfferItemDBData(string json)
    {
        Dictionary<string, object> rawItemData = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
        ItemList = rawItemData.ContainsKey("item_list") ? JsonConvert.DeserializeObject<List<string>>(rawItemData["item_list"].ToString()) : new List<string>();
        Duration = rawItemData.ContainsKey("duration") ? int.Parse(rawItemData["duration"].ToString()) : ONE_DAY_DURATION_MILLISECOND;
        Max = rawItemData.ContainsKey("max") ? int.Parse(rawItemData["max"].ToString()) : MAX_DEFAULT;
        Chance = rawItemData.ContainsKey("chance") ? int.Parse(rawItemData["chance"].ToString()) : 0;
        Priority = rawItemData.ContainsKey("priority") ? int.Parse(rawItemData["priority"].ToString()) : 0;
    }

    //"item_id": "com.zerobit.athenion.daily_di_125",
    //"duration":3600000
}
