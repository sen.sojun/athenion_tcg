﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

#region EffectData Class
public abstract class EffectData
{
    #region Public Properties
    public int UniqueID { get; protected set; }
    public string UniqueKey { get; protected set; }
    public DataParam Param { get; protected set; }
    public Requester Requester { get; protected set; }
    #endregion

    #region Private Properties
    private static string _emptyString = "#";
    #endregion

    #region Protected Properties
    protected UnityAction _onPrepareComplete;
    protected UnityAction _onPrepareCancel;

    protected static int _uniqueIndex = 0;
    #endregion

    #region Constructors
    public EffectData(DataParam param, Requester requester)
    {
        UniqueID = RequestUniqueID();
        UniqueKey = "";
        Param = param;
        Requester = requester;
    }

    public EffectData(EffectData data)
    {
        UniqueID = data.UniqueID;
        UniqueKey = data.UniqueKey;
        Param = data.Param;
        Requester = data.Requester;
    }
    #endregion

    #region Methods
    //protected virtual bool IsCanBeTarget(PawnData pawnData)
    //{
    //    return true;
    //}

    public virtual bool IsHasTarget()
    {
        return false;
    }

    public virtual void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        if (_onPrepareComplete != null)
        {
            _onPrepareComplete.Invoke();
        }
        else if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    public virtual void Action(UnityAction onComplete)
    {
        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public int GetUniqueID()
    {
        return Requester.GetUniqueID();
    }

    public void SetUniqueKey(string uniqueKey)
    {
        UniqueKey = uniqueKey;
    }

    public string CreateParamID(int index)
    {
        return string.Format("{0}_{1}_{2}", UniqueKey, this.GetType(), index);
    }

    public string CreateSaveKey(string key)
    {
        return GameManager.Instance.CreateSaveKey(key, Requester);
    }

    public virtual string GetDebugText()
    {
        return this.GetType().ToString();
    }

    public static bool CreateEffect(DataParam param, Requester requester, out EffectData data)
    {
        data = null;

        switch (param.Key)
        {
            case "DoRestorePlayerHP": data = new DoRestorePlayerHP(param, requester); break;
            case "DoBoostPlayerAP": data = new DoBoostPlayerAP(param, requester); break;
            case "DoDealMinionDamage": data = new DoDealMinionDamage(param, requester); break;
            case "DoDealMinionDamageWithSlot": data = new DoDealMinionDamageWithSlot(param, requester); break;
            case "DoDestroyMinion": data = new DoDestroyMinion(param, requester); break;
            case "DoSacrificeMinion": data = new DoSacrificeMinion(param, requester); break;
            case "DoDealPlayerDamage": data = new DoDealPlayerDamage(param, requester); break;
            case "DoSetMinionHP": data = new DoSetMinionHP(param, requester); break;
            case "DoSetMinionAP": data = new DoSetMinionAP(param, requester); break;
            case "DoSetMinionSP": data = new DoSetMinionSP(param, requester); break;
            case "DoSetMinionAPHP": data = new DoSetMinionAPHP(param, requester); break;
            case "DoSetMinionArmor": data = new DoSetMinionArmor(param, requester); break;
            case "DoBoostMinionHP": data = new DoBoostMinionHP(param, requester); break;
            case "DoBoostMinionAP": data = new DoBoostMinionAP(param, requester); break;
            case "DoBoostMinionSP": data = new DoBoostMinionSP(param, requester); break;
            case "DoBoostMinionAPHP": data = new DoBoostMinionAPHP(param, requester); break;
            case "DoBoostMinionArmor": data = new DoBoostMinionArmor(param, requester); break;
            case "DoSwapMinionHP": data = new DoSwapMinionHP(param, requester); break;
            case "DoSwapMinionAP": data = new DoSwapMinionAP(param, requester); break;
            case "DoSwapMinionSP": data = new DoSwapMinionSP(param, requester); break;
            case "DoSwapMinionAPHP": data = new DoSwapMinionAPHP(param, requester); break;
            case "DoRestoreMinionHP": data = new DoRestoreMinionHP(param, requester); break;
            case "DoSelectMinionAlive": data = new DoSelectMinionAlive(param, requester); break;
            case "DoSelectPlayer": data = new DoSelectPlayer(param, requester); break;
            case "DoSelectSlot": data = new DoSelectSlot(param, requester); break;
            case "DoSaveInteger": data = new DoSaveInteger(param, requester); break;
            case "DoSaveMinion": data = new DoSaveMinion(param, requester); break;
            case "DoClearMinion": data = new DoClearMinion(param, requester); break;
            case "DoSaveSlot": data = new DoSaveSlot(param, requester); break;
            case "DoIf": data = new DoIf(param, requester); break;
            case "DoIfElse": data = new DoIfElse(param, requester); break;
            case "DoTriggerCounting": data = new DoTriggerCounting(param, requester); break;
            case "DoAddMinionBuff": data = new DoAddMinionBuff(param, requester); break;
            case "DoRemoveMinionBuff": data = new DoRemoveMinionBuff(param, requester); break;
            case "DoAddSlotBuff": data = new DoAddSlotBuff(param, requester); break;
            case "DoRemoveSlotBuff": data = new DoRemoveSlotBuff(param, requester); break;
            case "DoModifySlotBuffCounter": data = new DoModifySlotBuffCounter(param, requester); break;
            case "DoCreateMinion": data = new DoCreateMinion(param, requester); break;
            case "DoSummonMinion": data = new DoSummonMinion(param, requester); break;
            case "DoResetMinion": data = new DoResetMinion(param, requester); break;
            case "DoMoveMinion": data = new DoMoveMinion(param, requester); break;
            case "DoSwapMinionSlot": data = new DoSwapMinionSlot(param, requester); break;
            case "DoDrawCard": data = new DoDrawCard(param, requester); break;
            case "DoMulligan": data = new DoMulligan(param, requester); break;
            case "DoDiscardCard": data = new DoDiscardCard(param, requester); break;
            case "DoRemoveCard": data = new DoRemoveCard(param, requester); break;
            case "DoMoveCardToZone": data = new DoMoveCardToZone(param, requester); break;
            case "DoCreateCardToZone": data = new DoCreateCardToZone(param, requester); break;
            case "DoAddMinionToAttackerList": data = new DoAddMinionToAttackerList(param, requester); break;
            case "DoRemoveMinionFromAttackerList": data = new DoRemoveMinionFromAttackerList(param, requester); break;
            case "DoMinionAura": data = new DoMinionAura(param, requester); break;
            case "DoMinionLink": data = new DoMinionLink(param, requester); break;
            case "DoMinionBerserk": data = new DoMinionBerserk(param, requester); break;
            case "DoBoostHeroSP": data = new DoBoostHeroSP(param, requester); break;
            case "DoTriggerMyAbility": data = new DoTriggerMyAbility(param, requester); break;
            case "DoAddMinionDirection": data = new DoAddMinionDirection(param, requester); break;
            case "DoSetMinionDirection": data = new DoSetMinionDirection(param, requester); break;

            default: Debug.LogError(string.Format("EffectData: Fail to create effect {0} from {1}", param.Key, requester.Minion.BaseID)); break;
        }

        return (data != null);
    }

    public static bool CreateBuffEffect(DataParam param, Requester requester, out EffectData data)
    {
        data = null;

        switch (param.Key)
        {
            //case "AttackBuff": data = new AttackBuff(param, requester); break;
            default: break;
        }

        return (data != null);
    }

    private static int RequestUniqueID()
    {
        return _uniqueIndex++;
    }

    public static void ResetIndex()
    {
        _uniqueIndex = 0;
    }
    #endregion

    #region Convert Methods
    public static string ConvertMinionListToText(List<MinionData> minionList)
    {
        string text = "";

        if (minionList != null && minionList.Count > 0)
        {
            foreach (MinionData minion in minionList)
            {
                if (text.Length > 0)
                {
                    text += "," + minion.UniqueID.ToString();
                }
                else
                {
                    text += minion.UniqueID.ToString();
                }
            }
        }
        else
        {
            text = _emptyString;
        }

        //Debug.Log("ConvertMinionListToText " + text);

        return text;
    }

    public static string ConvertStringListToText(List<string> stringList)
    {
        string text = "";

        if (stringList != null && stringList.Count > 0)
        {
            foreach (string str in stringList)
            {
                if (text.Length > 0)
                {
                    text += "," + str.ToString();
                }
                else
                {
                    text += str.ToString();
                }
            }
        }
        else
        {
            text = _emptyString;
        }

        return text;
    }

    public static string ConvertDataParamToText(DataParam dataParam)
    {
        return dataParam.ToText();
    }

    public static string ConvertPlayerIndexListToText(List<PlayerIndex> playerList)
    {
        string text = "";

        if (playerList != null && playerList.Count > 0)
        {
            foreach (PlayerIndex playerIndex in playerList)
            {
                if (text.Length > 0)
                {
                    text += "," + ((int)playerIndex).ToString();
                }
                else
                {
                    text += ((int)playerIndex).ToString();
                }
            }
        }
        else
        {
            text = _emptyString;
        }

        return text;
    }

    public static string ConvertIntListToText(List<int> intList)
    {
        string text = "";

        if (intList != null && intList.Count > 0)
        {
            foreach (int integer in intList)
            {
                if (text.Length > 0)
                {
                    text += "," + integer.ToString();
                }
                else
                {
                    text += integer.ToString();
                }
            }
        }
        else
        {
            text = _emptyString;
        }

        return text;
    }

    public static string ConvertSlotListToParamText(List<SlotData> slotList)
    {
        string text = "";

        if (slotList != null && slotList.Count > 0)
        {
            foreach (SlotData slot in slotList)
            {
                if (text.Length > 0)
                {
                    text += "," + slot.SlotID.ToString();
                }
                else
                {
                    text += slot.SlotID.ToString();
                }
            }
        }
        else
        {
            text = _emptyString;
        }

        return text;
    }

    public static string ConvertCardDirectionToText(CardDirection cardDirection)
    {
        string text = "";

        //if (cardDirection != null)
        //{
        //    foreach (int direction in cardDirection.DirIndex)
        //    {
        //        if (text.Length > 0)
        //        {
        //            text += "," + direction.ToString();
        //        }
        //        else
        //        {
        //            text += direction.ToString();
        //        }
        //    }
        //}
        //else
        //{
        //    text = _emptyString;
        //}

        //Debug.Log("ConvertMinionListToText " + text);

        foreach (int direction in cardDirection.DirIndex)
        {
            if (text.Length > 0)
            {
                text += "," + direction.ToString();
            }
            else
            {
                text += direction.ToString();
            }
        }

        return text;
    }

    public static List<MinionData> RevertToMinionList(string param)
    {
        List<MinionData> result = new List<MinionData>();

        if (param != null && param.Length > 0)
        {
            if (param == _emptyString)
            {
                return result;
            }

            string[] texts = param.Split(',');
            if (texts != null && texts.Length > 0)
            {
                int id;
                MinionData minion;
                foreach (string text in texts)
                {
                    //Debug.Log("RevertToMinionDataList : " + idText);
                    id = int.Parse(text);
                    if (GameManager.Instance.FindMinionBattleCard(id, out minion))
                    {
                        result.Add(minion);
                    }
                    else
                    {
                        Debug.LogError("ConvertMinionDataList: Failed to convert. " + param);
                    }
                }
            }
        }

        return result;
    }

    public static List<string> RevertToStringList(string param)
    {
        List<string> result = new List<string>();

        if (param != null && param.Length > 0)
        {
            if (param == _emptyString)
            {
                return result;
            }

            string[] texts = param.Split(',');
            if (texts != null && texts.Length > 0)
            {
                foreach (string text in texts)
                {
                    result.Add(text);
                }
            }
        }

        return result;
    }

    public static DataParam RevertToDataParam(string param, DataParam.ParamType type)
    {
        DataParam result;
        if (DataParam.TextToDataParam(param, type, out result))
        {
        }
        else
        {
            result = null;
            Debug.LogError("ConvertDataParam: Failed to convert data. " + param);
        }

        return result;
    }

    public static List<PlayerIndex> RevertToPlayerIndexList(string param)
    {
        List<PlayerIndex> result = new List<PlayerIndex>();

        if (param != null && param.Length > 0)
        {
            if (param == _emptyString)
            {
                return result;
            }

            string[] texts = param.Split(',');
            if (texts != null && texts.Length > 0)
            {
                foreach (string text in texts)
                {
                    result.Add((PlayerIndex)int.Parse(text));
                }
            }
        }

        return result;
    }

    public static List<int> RevertToIntList(string param)
    {
        List<int> result = new List<int>();

        if (param != null && param.Length > 0)
        {
            if (param == _emptyString)
            {
                return result;
            }

            string[] texts = param.Split(',');
            if (texts != null && texts.Length > 0)
            {
                int id;
                foreach (string text in texts)
                {
                    //Debug.Log("RevertToIntList : " + text);
                    id = int.Parse(text);
                    result.Add(id);
                }
            }
        }

        return result;
    }

    public static List<SlotData> RevertToSlotList(string param)
    {
        List<SlotData> result = new List<SlotData>();

        if (param != null && param.Length > 0)
        {
            if (param == _emptyString)
            {
                return result;
            }

            string[] texts = param.Split(',');
            if (texts != null && texts.Length > 0)
            {
                int slotID;
                foreach (string text in texts)
                {
                    slotID = int.Parse(text);
                    if (slotID >= 0 && slotID < GameManager.Instance.BoardSize)
                    {
                        result.Add(GameManager.Instance.Data.SlotDatas[slotID]);
                    }
                }
            }
        }

        return result;
    }

    public static CardDirection RevertToCardDirection(string param)
    {
        CardDirection result = new CardDirection();

        if (param != null && param.Length > 0)
        {
            if (param == _emptyString)
            {
                return result;
            }

            string[] texts = param.Split(',');
            if (texts != null && texts.Length == 8)
            {
                int[] direction = new int[8];

                for (int i = 0; i < texts.Length; i++)
                {
                    direction[i] = int.Parse(texts[i]);
                }
                result = new CardDirection(direction);
            }
            else
            {
                Debug.LogError("RevertCardDirection: Failed to revert. " + param);
            }
        }

        return result;
    }

    public string PopParamList(string key)
    {
        string text = "";
        if (Requester.Ability.ParamList.ContainsKey(key))
        {
            text = Requester.Ability.ParamList[key];
            Requester.Ability.ParamList.Remove(key);
        }
        else
        {
            Debug.LogErrorFormat("PopParamList: PramList is empty. Nothing to POP!! key:{0}", key);

            foreach (KeyValuePair<string, string> item in Requester.Ability.ParamList)
            {
                Debug.LogErrorFormat("PopParamList: {0}:{1}", item.Key, item.Value);
            }
        }
        return text;
    }

    public void AddParamList(string key, string value)
    {
        if (!Requester.Ability.ParamList.ContainsKey(key))
        {
            Requester.Ability.ParamList.Add(key, value);
        }
        else
        {
            Requester.Ability.ParamList[key] = value;
        }
    }
    #endregion
}
#endregion

#region EffectData Children

#region DoRestorePlayerHP Class
public class DoRestorePlayerHP : EffectData
{
    #region Private Properties
    private List<PlayerIndex> _playerIndexList;
    private int _value;
    #endregion

    #region Constructors
    public DoRestorePlayerHP(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.PlayerIndexList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.Integer, out p[1]);

        if (isSuccess)
        {
            isSuccess &= PlayerIndexParser.ParamToPlayerIndexList(p[0], Requester, out _playerIndexList);
            isSuccess &= IntParser.ParamToInt(p[1], Requester, out _value);
            if (isSuccess)
            {
                // Packing ParamList
                {
                    AddParamList(CreateParamID(1), ConvertPlayerIndexListToText(_playerIndexList));
                    AddParamList(CreateParamID(2), _value.ToString());
                }

                if (_onPrepareComplete != null)
                {
                    _onPrepareComplete.Invoke();
                }
                return;
            }
        }
        // Prepare param failed.
        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        // Prepare ParamList.
        {
            _playerIndexList = RevertToPlayerIndexList(PopParamList(CreateParamID(1)));
            _value = int.Parse(PopParamList(CreateParamID(2)));
        }

        List<CommandData> commandList = new List<CommandData>();
        foreach (PlayerIndex player in _playerIndexList)
        {
            RestoreLifePointDeltaCommand cmd = new RestoreLifePointDeltaCommand(player, _value);
            commandList.Add(cmd);
        }

        if (Requester.Ability != null)
        {
            Requester.Ability.AddCommandData(commandList);
        }
        else
        {
            Debug.LogErrorFormat("EffectData Error: Ability not found on {0}", Param.Key);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoBoostPlayerAP Class
public class DoBoostPlayerAP : EffectData
{
    #region Private Properties
    private List<PlayerIndex> _playerIndexList;
    private int _value;
    private string _vfxKey;
    private DataParam _vfxParam;
    #endregion

    #region Constructors
    public DoBoostPlayerAP(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        DataParam[] p = new DataParam[3];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.PlayerIndexList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.Integer, out p[1]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[2], DataParam.ParamType.String, out p[2]);

        if (isSuccess)
        {
            isSuccess &= PlayerIndexParser.ParamToPlayerIndexList(p[0], Requester, out _playerIndexList);
            isSuccess &= IntParser.ParamToInt(p[1], Requester, out _value);
            isSuccess &= StringParser.ParamToString(p[2], Requester, out _vfxKey);
            if (isSuccess)
            {
                // Packing ParamList
                {
                    AddParamList(CreateParamID(1), ConvertPlayerIndexListToText(_playerIndexList));
                    AddParamList(CreateParamID(2), _value.ToString());
                    AddParamList(CreateParamID(3), _vfxKey);
                }

                if (_onPrepareComplete != null)
                {
                    _onPrepareComplete.Invoke();
                }
                return;
            }
        }
        // Prepare param failed.
        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        // Prepare ParamList.
        {
            _playerIndexList = RevertToPlayerIndexList(PopParamList(CreateParamID(1)));
            _value = int.Parse(PopParamList(CreateParamID(2)));
            _vfxKey = PopParamList(CreateParamID(3));
        }

        if (_vfxKey != null)
        {
            Requester.Ability.GetVFXParam(_vfxKey, out _vfxParam);
        }
        else
        {
            _vfxParam = null;
        }

        List<CommandData> commandList = new List<CommandData>();
        foreach (PlayerIndex player in _playerIndexList)
        {
            commandList.Add(new BoostPlayerAPCommand(Requester, player, _value, _vfxParam));
        }

        if (Requester.Ability != null)
        {
            Requester.Ability.AddCommandData(commandList);
        }
        else
        {
            Debug.LogErrorFormat("EffectData Error: Ability not found on {0}", Param.Key);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoDealMinionDamage Class
public class DoDealMinionDamage : EffectData
{
    #region Private Properties
    private List<MinionData> _minionList;
    private int _value;
    private string _vfxIndexKey;
    private DataParam _vfxParam;
    #endregion

    #region Constructors
    public DoDealMinionDamage(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public int GetDamage()
    {
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.Integer, out p[0]);

        if (isSuccess)
        {
            isSuccess &= IntParser.ParamToInt(p[0], Requester, out _value);
            if (isSuccess)
            {
                return _value;
            }
        }

        return 0;
    }

    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        DataParam[] p = new DataParam[3];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.Integer, out p[1]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[2], DataParam.ParamType.String, out p[2]);

        if (isSuccess)
        {
            isSuccess &= MinionParser.ParamToMinionList(p[0], Requester, out _minionList);
            isSuccess &= IntParser.ParamToInt(p[1], Requester, out _value);
            isSuccess &= StringParser.ParamToString(p[2], Requester, out _vfxIndexKey);
            if (isSuccess)
            {
                // Packing ParamList
                {
                    AddParamList(CreateParamID(1), ConvertMinionListToText(_minionList));
                    AddParamList(CreateParamID(2), _value.ToString());
                    AddParamList(CreateParamID(3), _vfxIndexKey);
                }

                if (_onPrepareComplete != null)
                {
                    _onPrepareComplete.Invoke();
                }
                return;
            }
        }
        // Prepare param failed.
        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        // Prepare param list.
        {
            _minionList = RevertToMinionList(PopParamList(CreateParamID(1)));
            string valueStr = PopParamList(CreateParamID(2));
            _value = int.Parse(valueStr);
            _vfxIndexKey = PopParamList(CreateParamID(3));
        }

        List<int> idList = new List<int>();
        foreach (MinionData target in _minionList)
        {
            if (GameManager.Instance.Phase == GamePhase.Battle
               || target.IsAlive)
            {
                idList.Add(target.UniqueID);
            }
        }

        if (_vfxIndexKey != null)
        {
            Requester.Ability.GetVFXParam(_vfxIndexKey, out _vfxParam);
        }
        else
        {
            _vfxParam = null;
        }

        if (Requester.Ability != null)
        {
            Requester.Ability.AddCommandData(new DealDamageMinionCommand(Requester, idList, _value, _vfxParam));
        }
        else
        {
            Debug.LogErrorFormat("EffectData Error: Ability not found on {0}", Param.Key);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoDealMinionDamageWithSlot Class
public class DoDealMinionDamageWithSlot : EffectData
{
    #region Private Properties
    private List<SlotData> _slotList;
    private List<MinionData> _minionList;
    private int _value;
    private string _vfxIndexKey;
    private DataParam _vfxParam;
    #endregion

    #region Constructors
    public DoDealMinionDamageWithSlot(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public int GetDamage()
    {
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[2], DataParam.ParamType.Integer, out p[0]);

        if (isSuccess)
        {
            isSuccess &= IntParser.ParamToInt(p[0], Requester, out _value);
            if (isSuccess)
            {
                return _value;
            }
        }

        return 0;
    }

    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        DataParam[] p = new DataParam[4];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.SlotList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.MinionList, out p[1]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[2], DataParam.ParamType.Integer, out p[2]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[3], DataParam.ParamType.String, out p[3]);

        if (isSuccess)
        {
            isSuccess &= SlotParser.ParamToSlotList(p[0], Requester, out _slotList);
            isSuccess &= MinionParser.ParamToMinionList(p[1], Requester, out _minionList);
            isSuccess &= IntParser.ParamToInt(p[2], Requester, out _value);
            isSuccess &= StringParser.ParamToString(p[3], Requester, out _vfxIndexKey);
            if (isSuccess)
            {
                // Packing ParamList
                {
                    AddParamList(CreateParamID(1), ConvertSlotListToParamText(_slotList));
                    AddParamList(CreateParamID(2), ConvertMinionListToText(_minionList));
                    AddParamList(CreateParamID(3), _value.ToString());
                    AddParamList(CreateParamID(4), _vfxIndexKey);
                }

                if (_onPrepareComplete != null)
                {
                    _onPrepareComplete.Invoke();
                }
                return;
            }
        }
        // Prepare param failed.
        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        // Prepare param list.
        {
            _slotList = RevertToSlotList(PopParamList(CreateParamID(1)));
            _minionList = RevertToMinionList(PopParamList(CreateParamID(2)));
            string valueStr = PopParamList(CreateParamID(3));
            _value = int.Parse(valueStr);
            _vfxIndexKey = PopParamList(CreateParamID(4));
        }

        List<int> idList = new List<int>();
        foreach (MinionData target in _minionList)
        {
            if (GameManager.Instance.Phase == GamePhase.Battle
               || target.IsAlive)
            {
                idList.Add(target.UniqueID);
            }
        }

        List<int> slotIDList = new List<int>();
        foreach (SlotData slot in _slotList)
        {
            slotIDList.Add(slot.SlotID);
        }

        if (_vfxIndexKey != null)
        {
            Requester.Ability.GetVFXParam(_vfxIndexKey, out _vfxParam);
        }
        else
        {
            _vfxParam = null;
        }

        if (Requester.Ability != null)
        {
            Requester.Ability.AddCommandData(new DealDamageMinionWithSlotCommand(Requester, slotIDList, idList, _value, _vfxParam));
        }
        else
        {
            Debug.LogErrorFormat("EffectData Error: Ability not found on {0}", Param.Key);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoDestroyMinion Class
public class DoDestroyMinion : EffectData
{
    #region Private Properties
    private List<MinionData> _minionList;
    private string _vfxIndexKey;
    private DataParam _vfxParam;
    #endregion

    #region Constructors
    public DoDestroyMinion(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.String, out p[1]);

        if (isSuccess)
        {
            isSuccess &= MinionParser.ParamToMinionList(p[0], Requester, out _minionList);
            isSuccess &= StringParser.ParamToString(p[1], Requester, out _vfxIndexKey);
            if (isSuccess)
            {
                // Packing ParamList
                {
                    AddParamList(CreateParamID(1), ConvertMinionListToText(_minionList));
                    AddParamList(CreateParamID(2), _vfxIndexKey);
                }

                if (_onPrepareComplete != null)
                {
                    _onPrepareComplete.Invoke();
                }
                return;
            }
        }
        // Prepare param failed.
        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        // Prepare param list.
        {
            _minionList = RevertToMinionList(PopParamList(CreateParamID(1)));
            _vfxIndexKey = PopParamList(CreateParamID(2));
        }

        List<int> idList = new List<int>();
        foreach (MinionData target in _minionList)
        {
            if (GameManager.Instance.Phase == GamePhase.Battle
               || target.IsAlive)
            {
                idList.Add(target.UniqueID);
            }
        }

        if (_vfxIndexKey != null)
        {
            Requester.Ability.GetVFXParam(_vfxIndexKey, out _vfxParam);
        }
        else
        {
            _vfxParam = null;
        }

        if (Requester.Ability != null)
        {
            Requester.Ability.AddCommandData(new DestroyMinionCommand(Requester, idList, _vfxParam));
        }
        else
        {
            Debug.LogErrorFormat("EffectData Error: Ability not found on {0}", Param.Key);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoSacrificeMinion Class
public class DoSacrificeMinion : EffectData
{
    #region Private Properties
    private List<MinionData> _minionList;
    private string _vfxIndexKey;
    private DataParam _vfxParam;
    #endregion

    #region Constructors
    public DoSacrificeMinion(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.String, out p[1]);

        if (isSuccess)
        {
            isSuccess &= MinionParser.ParamToMinionList(p[0], Requester, out _minionList);
            isSuccess &= StringParser.ParamToString(p[1], Requester, out _vfxIndexKey);
            if (isSuccess)
            {
                foreach (MinionData minion in _minionList)
                {
                    if (minion.Owner != Requester.PlayerIndex)
                    {
                        _onPrepareCancel?.Invoke();
                        return;
                    }
                }

                // Packing ParamList
                {
                    AddParamList(CreateParamID(1), ConvertMinionListToText(_minionList));
                    AddParamList(CreateParamID(2), _vfxIndexKey);
                }

                if (_onPrepareComplete != null)
                {
                    _onPrepareComplete.Invoke();
                }
                return;
            }
        }
        // Prepare param failed.
        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        // Prepare param list.
        {
            _minionList = RevertToMinionList(PopParamList(CreateParamID(1)));
            _vfxIndexKey = PopParamList(CreateParamID(2));
        }

        List<int> idList = new List<int>();
        foreach (MinionData target in _minionList)
        {
            if (GameManager.Instance.Phase == GamePhase.Battle
               || target.IsAlive)
            {
                idList.Add(target.UniqueID);
            }
        }

        if (_vfxIndexKey != null)
        {
            Requester.Ability.GetVFXParam(_vfxIndexKey, out _vfxParam);
        }
        else
        {
            _vfxParam = null;
        }

        if (Requester.Ability != null)
        {
            Requester.Ability.AddCommandData(new SacrificeMinionCommand(Requester, idList, _vfxParam));
        }
        else
        {
            Debug.LogErrorFormat("EffectData Error: Ability not found on {0}", Param.Key);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoDealPlayerDamage Class
public class DoDealPlayerDamage : EffectData
{
    #region Private Properties
    private List<PlayerIndex> _playerList;
    private int _value;
    private string _vfxIndexKey;
    private DataParam _vfxParam;
    #endregion

    #region Constructors
    public DoDealPlayerDamage(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public int GetDamage()
    {
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.Integer, out p[0]);

        if (isSuccess)
        {
            isSuccess &= IntParser.ParamToInt(p[0], Requester, out _value);
            if (isSuccess)
            {
                return _value;
            }
        }

        return 0;
    }

    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        DataParam[] p = new DataParam[3];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.PlayerIndexList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.Integer, out p[1]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[2], DataParam.ParamType.String, out p[2]);

        if (isSuccess)
        {
            isSuccess &= PlayerIndexParser.ParamToPlayerIndexList(p[0], Requester, out _playerList);
            isSuccess &= IntParser.ParamToInt(p[1], Requester, out _value);
            isSuccess &= StringParser.ParamToString(p[2], Requester, out _vfxIndexKey);
            if (isSuccess)
            {
                // Packing ParamList
                {
                    AddParamList(CreateParamID(1), ConvertPlayerIndexListToText(_playerList));
                    AddParamList(CreateParamID(2), _value.ToString());
                    AddParamList(CreateParamID(3), _vfxIndexKey);
                }

                if (_onPrepareComplete != null)
                {
                    _onPrepareComplete.Invoke();
                }
                return;
            }
        }
        // Prepare param failed.
        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        // Prepare param list.
        {
            _playerList = RevertToPlayerIndexList(PopParamList(CreateParamID(1)));
            _value = int.Parse(PopParamList(CreateParamID(2)));
            _vfxIndexKey = PopParamList(CreateParamID(3));
        }

        if (_vfxIndexKey != null)
        {
            Requester.Ability.GetVFXParam(_vfxIndexKey, out _vfxParam);
        }
        else
        {
            _vfxParam = null;
        }

        if (Requester.Ability != null)
        {
            Requester.Ability.AddCommandData(new DealDamagePlayerCommand(Requester, _playerList, _value, _vfxParam));
        }
        else
        {
            Debug.LogErrorFormat("EffectData Error: Ability not found on {0}", Param.Key);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoSetMinionHP Class
public class DoSetMinionHP : EffectData
{
    #region Private Properties
    private List<MinionData> _minionList;
    private int _value;
    #endregion

    #region Constructors
    public DoSetMinionHP(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.Integer, out p[1]);

        if (isSuccess)
        {
            isSuccess &= MinionParser.ParamToMinionList(p[0], Requester, out _minionList);
            isSuccess &= IntParser.ParamToInt(p[1], Requester, out _value);
            if (isSuccess)
            {
                // Packing ParamList
                {
                    AddParamList(CreateParamID(1), ConvertMinionListToText(_minionList));
                    AddParamList(CreateParamID(2), _value.ToString());
                }

                if (_onPrepareComplete != null)
                {
                    _onPrepareComplete.Invoke();
                }
                return;
            }
        }
        // Prepare param failed.
        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        // Prepare ParamList.
        {
            _minionList = RevertToMinionList(PopParamList(CreateParamID(1)));
            _value = int.Parse(PopParamList(CreateParamID(2)));
        }

        List<int> idList = new List<int>();
        foreach (MinionData target in _minionList)
        {
            idList.Add(target.UniqueID);
        }

        if (Requester.Ability != null)
        {
            Requester.Ability.AddCommandData(new SetHPMinionCommand(Requester, idList, _value));
        }
        else
        {
            Debug.LogErrorFormat("EffectData Error: Ability not found on {0}", Param.Key);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoSetMinionAP Class
public class DoSetMinionAP : EffectData
{
    #region Private Properties
    private List<MinionData> _minionList;
    private int _value;
    #endregion

    #region Constructors
    public DoSetMinionAP(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.Integer, out p[1]);

        if (isSuccess)
        {
            isSuccess &= MinionParser.ParamToMinionList(p[0], Requester, out _minionList);
            isSuccess &= IntParser.ParamToInt(p[1], Requester, out _value);
            if (isSuccess)
            {
                // Packing ParamList
                {
                    AddParamList(CreateParamID(1), ConvertMinionListToText(_minionList));
                    AddParamList(CreateParamID(2), _value.ToString());
                }

                if (_onPrepareComplete != null)
                {
                    _onPrepareComplete.Invoke();
                }
                return;
            }
        }
        // Prepare param failed.
        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        // Prepare ParamList.
        {
            _minionList = RevertToMinionList(PopParamList(CreateParamID(1)));
            _value = int.Parse(PopParamList(CreateParamID(2)));
        }

        List<int> idList = new List<int>();
        foreach (MinionData target in _minionList)
        {
            idList.Add(target.UniqueID);
        }

        if (Requester.Ability != null)
        {
            Requester.Ability.AddCommandData(new SetAPMinionCommand(Requester, idList, _value));
        }
        else
        {
            Debug.LogErrorFormat("EffectData Error: Ability not found on {0}", Param.Key);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoSetMinionSP Class
public class DoSetMinionSP : EffectData
{
    #region Private Properties
    private List<MinionData> _minionList;
    private int _value;
    #endregion

    #region Constructors
    public DoSetMinionSP(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.Integer, out p[1]);

        if (isSuccess)
        {
            isSuccess &= MinionParser.ParamToMinionList(p[0], Requester, out _minionList);
            isSuccess &= IntParser.ParamToInt(p[1], Requester, out _value);
            if (isSuccess)
            {
                // Packing ParamList
                {
                    AddParamList(CreateParamID(1), ConvertMinionListToText(_minionList));
                    AddParamList(CreateParamID(2), _value.ToString());
                }

                if (_onPrepareComplete != null)
                {
                    _onPrepareComplete.Invoke();
                }
                return;
            }
        }
        // Prepare param failed.
        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        // Prepare ParamList.
        {
            _minionList = RevertToMinionList(PopParamList(CreateParamID(1)));
            _value = int.Parse(PopParamList(CreateParamID(2)));
        }

        List<int> idList = new List<int>();
        foreach (MinionData target in _minionList)
        {
            idList.Add(target.UniqueID);
        }

        if (Requester.Ability != null)
        {
            Requester.Ability.AddCommandData(new SetSPMinionCommand(Requester, idList, _value));
        }
        else
        {
            Debug.LogErrorFormat("EffectData Error: Ability not found on {0}", Param.Key);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoSetMinionAPHP Class
public class DoSetMinionAPHP : EffectData
{
    #region Private Properties
    private List<MinionData> _minionList;
    private int _ap;
    private int _hp;
    #endregion

    #region Constructors
    public DoSetMinionAPHP(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        DataParam[] p = new DataParam[3];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.Integer, out p[1]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[2], DataParam.ParamType.Integer, out p[2]);

        if (isSuccess)
        {
            isSuccess &= MinionParser.ParamToMinionList(p[0], Requester, out _minionList);
            isSuccess &= IntParser.ParamToInt(p[1], Requester, out _ap);
            isSuccess &= IntParser.ParamToInt(p[2], Requester, out _hp);
            if (isSuccess)
            {
                // Packing ParamList
                {
                    AddParamList(CreateParamID(1), ConvertMinionListToText(_minionList));
                    AddParamList(CreateParamID(2), _ap.ToString());
                    AddParamList(CreateParamID(3), _hp.ToString());
                }

                if (_onPrepareComplete != null)
                {
                    _onPrepareComplete.Invoke();
                }
                return;
            }
        }
        // Prepare param failed.
        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        // Prepare ParamList.
        {
            _minionList = RevertToMinionList(PopParamList(CreateParamID(1)));
            _ap = int.Parse(PopParamList(CreateParamID(2)));
            _hp = int.Parse(PopParamList(CreateParamID(3)));
        }

        List<int> idList = new List<int>();
        foreach (MinionData target in _minionList)
        {
            idList.Add(target.UniqueID);
        }

        if (Requester.Ability != null)
        {
            Requester.Ability.AddCommandData(new SetAPHPMinionCommand(Requester, idList, _ap, _hp));
        }
        else
        {
            Debug.LogErrorFormat("EffectData Error: Ability not found on {0}", Param.Key);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoSetMinionArmor Class
public class DoSetMinionArmor : EffectData
{
    #region Private Properties
    private List<MinionData> _minionList;
    private int _value;
    #endregion

    #region Constructors
    public DoSetMinionArmor(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.Integer, out p[1]);

        if (isSuccess)
        {
            isSuccess &= MinionParser.ParamToMinionList(p[0], Requester, out _minionList);
            isSuccess &= IntParser.ParamToInt(p[1], Requester, out _value);
            if (isSuccess)
            {
                // Packing ParamList
                {
                    AddParamList(CreateParamID(1), ConvertMinionListToText(_minionList));
                    AddParamList(CreateParamID(2), _value.ToString());
                }

                if (_onPrepareComplete != null)
                {
                    _onPrepareComplete.Invoke();
                }
                return;
            }
        }
        // Prepare param failed.
        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        // Prepare ParamList.
        {
            _minionList = RevertToMinionList(PopParamList(CreateParamID(1)));
            _value = int.Parse(PopParamList(CreateParamID(2)));
        }

        List<int> idList = new List<int>();
        foreach (MinionData target in _minionList)
        {
            idList.Add(target.UniqueID);
        }

        if (Requester.Ability != null)
        {
            Requester.Ability.AddCommandData(new SetArmorMinionCommand(Requester, idList, _value));
        }
        else
        {
            Debug.LogErrorFormat("EffectData Error: Ability not found on {0}", Param.Key);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoBoostMinionHP Class
public class DoBoostMinionHP : EffectData
{
    #region Private Properties
    private List<MinionData> _minionList;
    private int _value;
    #endregion

    #region Constructors
    public DoBoostMinionHP(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.Integer, out p[1]);

        if (isSuccess)
        {
            isSuccess &= MinionParser.ParamToMinionList(p[0], Requester, out _minionList);
            isSuccess &= IntParser.ParamToInt(p[1], Requester, out _value);
            if (isSuccess)
            {
                // Packing ParamList
                {
                    AddParamList(CreateParamID(1), ConvertMinionListToText(_minionList));
                    AddParamList(CreateParamID(2), _value.ToString());
                }

                if (_onPrepareComplete != null)
                {
                    _onPrepareComplete.Invoke();
                }
                return;
            }
        }
        // Prepare param failed.
        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        // Prepare ParamList.
        {
            _minionList = RevertToMinionList(PopParamList(CreateParamID(1)));
            _value = int.Parse(PopParamList(CreateParamID(2)));
        }

        List<int> idList = new List<int>();
        foreach (MinionData target in _minionList)
        {
            idList.Add(target.UniqueID);
        }

        if (Requester.Ability != null)
        {
            Requester.Ability.AddCommandData(new BoostHPMinionCommand(Requester, idList, _value));
        }
        else
        {
            Debug.LogErrorFormat("EffectData Error: Ability not found on {0}", Param.Key);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoBoostMinionAP Class
public class DoBoostMinionAP : EffectData
{
    #region Private Properties
    private List<MinionData> _minionList;
    private int _value;
    #endregion

    #region Constructors
    public DoBoostMinionAP(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.Integer, out p[1]);

        if (isSuccess)
        {
            isSuccess &= MinionParser.ParamToMinionList(p[0], Requester, out _minionList);
            isSuccess &= IntParser.ParamToInt(p[1], Requester, out _value);
            if (isSuccess)
            {
                // Packing ParamList
                {
                    AddParamList(CreateParamID(1), ConvertMinionListToText(_minionList));
                    AddParamList(CreateParamID(2), _value.ToString());
                }

                if (_onPrepareComplete != null)
                {
                    _onPrepareComplete.Invoke();
                }
                return;
            }
        }
        // Prepare param failed.
        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        // Prepare ParamList.
        {
            _minionList = RevertToMinionList(PopParamList(CreateParamID(1)));
            _value = int.Parse(PopParamList(CreateParamID(2)));
        }

        List<int> idList = new List<int>();
        foreach (MinionData target in _minionList)
        {
            idList.Add(target.UniqueID);
        }

        if (Requester.Ability != null)
        {
            Requester.Ability.AddCommandData(new BoostAPMinionCommand(Requester, idList, _value));
        }
        else
        {
            Debug.LogErrorFormat("EffectData Error: Ability not found on {0}", Param.Key);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoBoostMinionSP Class
public class DoBoostMinionSP : EffectData
{
    #region Private Properties
    private List<MinionData> _minionList;
    private int _value;
    #endregion

    #region Constructors
    public DoBoostMinionSP(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.Integer, out p[1]);

        if (isSuccess)
        {
            isSuccess &= MinionParser.ParamToMinionList(p[0], Requester, out _minionList);
            isSuccess &= IntParser.ParamToInt(p[1], Requester, out _value);
            if (isSuccess)
            {
                // Packing ParamList
                {
                    AddParamList(CreateParamID(1), ConvertMinionListToText(_minionList));
                    AddParamList(CreateParamID(2), _value.ToString());
                }

                if (_onPrepareComplete != null)
                {
                    _onPrepareComplete.Invoke();
                }
                return;
            }
        }
        // Prepare param failed.
        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        // Prepare ParamList.
        {
            _minionList = RevertToMinionList(PopParamList(CreateParamID(1)));
            _value = int.Parse(PopParamList(CreateParamID(2)));
        }

        List<int> idList = new List<int>();
        foreach (MinionData target in _minionList)
        {
            idList.Add(target.UniqueID);
        }

        if (Requester.Ability != null)
        {
            Requester.Ability.AddCommandData(new BoostSPMinionCommand(Requester, idList, _value));
        }
        else
        {
            Debug.LogErrorFormat("EffectData Error: Ability not found on {0}", Param.Key);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoBoostMinionAPHP Class
public class DoBoostMinionAPHP : EffectData
{
    #region Private Properties
    private List<MinionData> _minionList;
    private int _ap;
    private int _hp;
    #endregion

    #region Constructors
    public DoBoostMinionAPHP(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        DataParam[] p = new DataParam[3];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.Integer, out p[1]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[2], DataParam.ParamType.Integer, out p[2]);

        if (isSuccess)
        {
            isSuccess &= MinionParser.ParamToMinionList(p[0], Requester, out _minionList);
            isSuccess &= IntParser.ParamToInt(p[1], Requester, out _ap);
            isSuccess &= IntParser.ParamToInt(p[2], Requester, out _hp);
            if (isSuccess)
            {
                // Packing ParamList
                {
                    AddParamList(CreateParamID(1), ConvertMinionListToText(_minionList));
                    AddParamList(CreateParamID(2), _ap.ToString());
                    AddParamList(CreateParamID(3), _hp.ToString());
                }

                if (_onPrepareComplete != null)
                {
                    _onPrepareComplete.Invoke();
                }
                return;
            }
        }
        // Prepare param failed.
        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        // Prepare ParamList.
        {
            _minionList = RevertToMinionList(PopParamList(CreateParamID(1)));
            _ap = int.Parse(PopParamList(CreateParamID(2)));
            _hp = int.Parse(PopParamList(CreateParamID(3)));
        }

        List<int> idList = new List<int>();
        foreach (MinionData target in _minionList)
        {
            idList.Add(target.UniqueID);
        }

        if (Requester.Ability != null)
        {
            Requester.Ability.AddCommandData(new BoostAPHPMinionCommand(Requester, idList, _ap, _hp));
        }
        else
        {
            Debug.LogErrorFormat("EffectData Error: Ability not found on {0}", Param.Key);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoBoostMinionArmor Class
public class DoBoostMinionArmor : EffectData
{
    #region Private Properties
    private List<MinionData> _minionList;
    private int _armor;
    #endregion

    #region Constructors
    public DoBoostMinionArmor(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.Integer, out p[1]);

        if (isSuccess)
        {
            isSuccess &= MinionParser.ParamToMinionList(p[0], Requester, out _minionList);
            isSuccess &= IntParser.ParamToInt(p[1], Requester, out _armor);
            if (isSuccess)
            {
                // Packing ParamList
                {
                    AddParamList(CreateParamID(1), ConvertMinionListToText(_minionList));
                    AddParamList(CreateParamID(2), _armor.ToString());
                }

                if (_onPrepareComplete != null)
                {
                    _onPrepareComplete.Invoke();
                }
                return;
            }
        }
        // Prepare param failed.
        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        // Prepare ParamList.
        {
            _minionList = RevertToMinionList(PopParamList(CreateParamID(1)));
            _armor = int.Parse(PopParamList(CreateParamID(2)));
        }

        List<int> idList = new List<int>();
        foreach (MinionData target in _minionList)
        {
            idList.Add(target.UniqueID);
        }

        if (Requester.Ability != null)
        {
            Requester.Ability.AddCommandData(new BoostArmorMinionCommand(Requester, idList, _armor));
        }
        else
        {
            Debug.LogErrorFormat("EffectData Error: Ability not found on {0}", Param.Key);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoSwapMinionHP Class
public class DoSwapMinionHP : EffectData
{
    #region Private Properties
    private List<MinionData> _minion1;
    private List<MinionData> _minion2;
    #endregion

    #region Constructors
    public DoSwapMinionHP(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.MinionList, out p[1]);

        if (isSuccess)
        {
            isSuccess &= MinionParser.ParamToMinionList(p[0], Requester, out _minion1);
            isSuccess &= MinionParser.ParamToMinionList(p[1], Requester, out _minion2);
            if (isSuccess)
            {
                // Packing ParamList
                {
                    AddParamList(CreateParamID(1), ConvertMinionListToText(_minion1));
                    AddParamList(CreateParamID(2), ConvertMinionListToText(_minion2));
                }

                if (_onPrepareComplete != null)
                {
                    _onPrepareComplete.Invoke();
                }
                return;
            }
        }
        // Prepare param failed.
        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        // Prepare ParamList.
        {
            _minion1 = RevertToMinionList(PopParamList(CreateParamID(1)));
            _minion2 = RevertToMinionList(PopParamList(CreateParamID(2)));
        }

        if (Requester.Ability != null)
        {
            Requester.Ability.AddCommandData(new SwapMinionHPCommand(Requester, _minion1[0].UniqueID, _minion2[0].UniqueID));
        }
        else
        {
            Debug.LogErrorFormat("EffectData Error: Ability not found on {0}", Param.Key);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoSwapMinionAP Class
public class DoSwapMinionAP : EffectData
{
    #region Private Properties
    private List<MinionData> _minion1;
    private List<MinionData> _minion2;
    #endregion

    #region Constructors
    public DoSwapMinionAP(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.MinionList, out p[1]);

        if (isSuccess)
        {
            isSuccess &= MinionParser.ParamToMinionList(p[0], Requester, out _minion1);
            isSuccess &= MinionParser.ParamToMinionList(p[1], Requester, out _minion2);
            if (isSuccess)
            {
                // Packing ParamList
                {
                    AddParamList(CreateParamID(1), ConvertMinionListToText(_minion1));
                    AddParamList(CreateParamID(2), ConvertMinionListToText(_minion2));
                }

                if (_onPrepareComplete != null)
                {
                    _onPrepareComplete.Invoke();
                }
                return;
            }
        }
        // Prepare param failed.
        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        // Prepare ParamList.
        {
            _minion1 = RevertToMinionList(PopParamList(CreateParamID(1)));
            _minion2 = RevertToMinionList(PopParamList(CreateParamID(2)));
        }

        if (Requester.Ability != null)
        {
            Requester.Ability.AddCommandData(new SwapMinionAPCommand(Requester, _minion1[0].UniqueID, _minion2[0].UniqueID));
        }
        else
        {
            Debug.LogErrorFormat("EffectData Error: Ability not found on {0}", Param.Key);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoSwapMinionSP Class
public class DoSwapMinionSP : EffectData
{
    #region Private Properties
    private List<MinionData> _minion1;
    private List<MinionData> _minion2;
    #endregion

    #region Constructors
    public DoSwapMinionSP(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.MinionList, out p[1]);

        if (isSuccess)
        {
            isSuccess &= MinionParser.ParamToMinionList(p[0], Requester, out _minion1);
            isSuccess &= MinionParser.ParamToMinionList(p[1], Requester, out _minion2);
            if (isSuccess)
            {
                // Packing ParamList
                {
                    AddParamList(CreateParamID(1), ConvertMinionListToText(_minion1));
                    AddParamList(CreateParamID(2), ConvertMinionListToText(_minion2));
                }

                if (_onPrepareComplete != null)
                {
                    _onPrepareComplete.Invoke();
                }
                return;
            }
        }
        // Prepare param failed.
        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        // Prepare ParamList.
        {
            _minion1 = RevertToMinionList(PopParamList(CreateParamID(1)));
            _minion2 = RevertToMinionList(PopParamList(CreateParamID(2)));
        }

        if (Requester.Ability != null)
        {
            Requester.Ability.AddCommandData(new SwapMinionSPCommand(Requester, _minion1[0].UniqueID, _minion2[0].UniqueID));
        }
        else
        {
            Debug.LogErrorFormat("EffectData Error: Ability not found on {0}", Param.Key);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoSwapMinionAPHP Class
public class DoSwapMinionAPHP : EffectData
{
    #region Private Properties
    private List<MinionData> _minion1;
    private List<MinionData> _minion2;
    #endregion

    #region Constructors
    public DoSwapMinionAPHP(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.MinionList, out p[1]);

        if (isSuccess)
        {
            isSuccess &= MinionParser.ParamToMinionList(p[0], Requester, out _minion1);
            isSuccess &= MinionParser.ParamToMinionList(p[1], Requester, out _minion2);
            if (isSuccess)
            {
                // Packing ParamList
                {
                    AddParamList(CreateParamID(1), ConvertMinionListToText(_minion1));
                    AddParamList(CreateParamID(2), ConvertMinionListToText(_minion2));
                }

                if (_onPrepareComplete != null)
                {
                    _onPrepareComplete.Invoke();
                }
                return;
            }
        }
        // Prepare param failed.
        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        // Prepare ParamList.
        {
            _minion1 = RevertToMinionList(PopParamList(CreateParamID(1)));
            _minion2 = RevertToMinionList(PopParamList(CreateParamID(2)));
        }

        if (Requester.Ability != null)
        {
            Requester.Ability.AddCommandData(new SwapMinionAPHPCommand(Requester, _minion1[0].UniqueID, _minion2[0].UniqueID));
        }
        else
        {
            Debug.LogErrorFormat("EffectData Error: Ability not found on {0}", Param.Key);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoRestoreMinionHP Class
public class DoRestoreMinionHP : EffectData
{
    #region Private Properties
    private List<MinionData> _minionList;
    private int _value;
    #endregion

    #region Constructors
    public DoRestoreMinionHP(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.Integer, out p[1]);

        if (isSuccess)
        {
            isSuccess &= MinionParser.ParamToMinionList(p[0], Requester, out _minionList);
            isSuccess &= IntParser.ParamToInt(p[1], Requester, out _value);
            if (isSuccess)
            {
                // Packing ParamList
                {
                    AddParamList(CreateParamID(1), ConvertMinionListToText(_minionList));
                    AddParamList(CreateParamID(2), _value.ToString());
                }

                if (_onPrepareComplete != null)
                {
                    _onPrepareComplete.Invoke();
                }
                return;
            }
        }
        // Prepare param failed.
        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        // Prepare ParamList.
        {
            _minionList = RevertToMinionList(PopParamList(CreateParamID(1)));
            _value = int.Parse(PopParamList(CreateParamID(2)));
        }

        List<int> idList = new List<int>();
        foreach (MinionData target in _minionList)
        {
            idList.Add(target.UniqueID);
        }

        if (Requester.Ability != null)
        {
            Requester.Ability.AddCommandData(new RestoreHPMinionCommand(Requester, idList, _value));
        }
        else
        {
            Debug.LogErrorFormat("EffectData Error: Ability not found on {0}", Param.Key);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoSelectMinionAlive Class
public class DoSelectMinionAlive : EffectData
{
    #region Private Properties
    private List<int> _selectedList;
    private int _count = 0;
    private string _key;
    private string _info;
    #endregion

    #region Constructors
    public DoSelectMinionAlive(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        bool isHaveInfo = false;
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        DataParam[] p = new DataParam[4];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.Integer, out p[1]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[2], DataParam.ParamType.String, out p[2]);
        if (Param.Parameters.Count >= 4)
        {
            isSuccess &= DataParam.TextToDataParam(Param.Parameters[3], DataParam.ParamType.String, out p[3]);
            isHaveInfo = true;
        }

        if (isSuccess)
        {
            List<MinionData> targetList;

            isSuccess &= MinionParser.ParamToMinionList(p[0], Requester, out targetList);
            isSuccess &= IntParser.ParamToInt(p[1], Requester, out _count);
            isSuccess &= StringParser.ParamToString(p[2], Requester, out _key);

            _info = "-";
            if (isHaveInfo)
            {
                StringParser.ParamToString(p[3], Requester, out _info);
            }

            if (isSuccess && targetList != null && targetList.Count > 0)
            {
                List<int> idList = new List<int>();
                foreach (MinionData card in targetList)
                {
                    if (card.IsAlive
                        && !card.IsBeUntargetable)
                    {
                        idList.Add(card.UniqueID);
                    }
                }

                GameManager.Instance.ActionPlayerSelectMinion(
                      Requester
                    , idList
                    , _count
                    , _info
                    , OnSelected
                    , _onPrepareCancel
                );
                return;
            }
            else
            {
                // Prepare param failed.
                if (_onPrepareCancel != null)
                {
                    _onPrepareCancel.Invoke();
                }
                return;
            }
        }

        // Prepare param failed.
        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    private void OnSelected(List<int> selectedList)
    {
        _selectedList = selectedList;

        string saveKey = CreateSaveKey(_key);
        GameManager.Instance.SaveMinionIDList(saveKey, _selectedList);

        // Packing ParamList
        {
            AddParamList(CreateParamID(1), saveKey);
            AddParamList(CreateParamID(2), ConvertIntListToText(_selectedList));
        }

        if (_onPrepareComplete != null)
        {
            _onPrepareComplete.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        // Packing ParamList
        {
            _key = PopParamList(CreateParamID(1));
            _selectedList = RevertToIntList(PopParamList(CreateParamID(2)));
        }
        GameManager.Instance.SaveMinionIDList(_key, _selectedList);

        MinionData minion;
        Requester triggerer;
        List<EventTriggerData> triggerList = new List<EventTriggerData>();
        foreach (int minionUniqueID in _selectedList)
        {
            if (GameManager.Instance.FindMinionBattleCard(minionUniqueID, out minion))
            {
                triggerer = new Requester(minion);
                triggerList.Add(new EventTriggerData(EventKey.OnMeSelected, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnFriendSelected, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnEnemySelected, triggerer));
            }
        }
        List<CommandData> commandDataList = new List<CommandData>(EventTrigger.TriggerCommandList(triggerList));

        if (Requester.Ability != null)
        {
            Requester.Ability.AddCommandData(commandDataList);
        }
        else
        {
            Debug.LogErrorFormat("EffectData Error: Ability not found on {0}", Param.Key);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoSelectPlayer Class
public class DoSelectPlayer : EffectData
{
    #region Private Properties
    private List<PlayerIndex> _selectedList;
    private int _count = 0;
    private string _key;
    #endregion

    #region Constructors
    public DoSelectPlayer(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        DataParam[] p = new DataParam[3];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.PlayerIndexList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.Integer, out p[1]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[2], DataParam.ParamType.String, out p[2]);

        if (isSuccess)
        {
            List<PlayerIndex> targetList;

            isSuccess &= PlayerIndexParser.ParamToPlayerIndexList(p[0], Requester, out targetList);
            isSuccess &= IntParser.ParamToInt(p[1], Requester, out _count);
            isSuccess &= StringParser.ParamToString(p[2], Requester, out _key);

            if (isSuccess && targetList != null && targetList.Count > 0)
            {
                GameManager.Instance.ActionPlayerSelectPlayer(
                      Requester
                    , targetList
                    , _count
                    , OnSelected
                    , _onPrepareCancel
                );
                return;
            }
        }

        // Prepare param failed.
        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    private void OnSelected(List<PlayerIndex> selectedList)
    {
        _selectedList = selectedList;
        string saveKey = CreateSaveKey(_key);
        GameManager.Instance.SavePlayerIndexList(saveKey, _selectedList);

        // Packing ParamList
        {
            AddParamList(CreateParamID(1), saveKey);
            AddParamList(CreateParamID(2), ConvertPlayerIndexListToText(_selectedList));
        }

        if (_onPrepareComplete != null)
        {
            _onPrepareComplete.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        // Prepare ParamList.
        {
            _key = PopParamList(CreateParamID(1));
            _selectedList = RevertToPlayerIndexList(PopParamList(CreateParamID(2)));
        }
        GameManager.Instance.SavePlayerIndexList(_key, _selectedList);

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoSelectSlot Class
public class DoSelectSlot : EffectData
{
    #region Private Properties
    private List<int> _selectedList;
    private int _count = 0;
    private string _key;
    private string _info;
    #endregion

    #region Constructors
    public DoSelectSlot(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        bool isHaveInfo = false;
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        DataParam[] p = new DataParam[4];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.SlotList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.Integer, out p[1]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[2], DataParam.ParamType.String, out p[2]);
        if (Param.Parameters.Count >= 4)
        {
            isSuccess &= DataParam.TextToDataParam(Param.Parameters[3], DataParam.ParamType.String, out p[3]);
            isHaveInfo = true;
        }

        if (isSuccess)
        {
            List<SlotData> targetList;

            isSuccess &= SlotParser.ParamToSlotList(p[0], Requester, out targetList);
            isSuccess &= IntParser.ParamToInt(p[1], Requester, out _count);
            isSuccess &= StringParser.ParamToString(p[2], Requester, out _key);

            _info = "-";
            if (isHaveInfo)
            {
                StringParser.ParamToString(p[3], Requester, out _info);
            }
        
            if (isSuccess && targetList != null && targetList.Count > 0)
            {
                List<int> idList = new List<int>();
                foreach (SlotData slot in targetList)
                {
                    idList.Add(slot.SlotID);
                }

                GameManager.Instance.ActionPlayerSelectSlot(
                      Requester
                    , idList
                    , _count
                    , _info 
                    , OnSelected
                    , _onPrepareCancel
                );
                return;
            }
            else
            {
                // Prepare param failed.
                if (_onPrepareCancel != null)
                {
                    _onPrepareCancel.Invoke();
                }
                return;
            }
        }

        // Prepare param failed.
        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    private void OnSelected(List<int> selectedList)
    {
        _selectedList = selectedList;
        string saveKey = CreateSaveKey(_key);
        GameManager.Instance.SaveSlotIDList(saveKey, _selectedList);

        // Packing ParamList
        {
            AddParamList(CreateParamID(1), saveKey);
            AddParamList(CreateParamID(2), ConvertIntListToText(_selectedList));
        }

        if (_onPrepareComplete != null)
        {
            _onPrepareComplete.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        // Prepare ParamList.
        {
            _key = PopParamList(CreateParamID(1));
            _selectedList = RevertToIntList(PopParamList(CreateParamID(2)));
        }
        GameManager.Instance.SaveSlotIDList(_key, _selectedList);

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoSaveInteger Class
public class DoSaveInteger : EffectData
{
    #region Private Properties
    private int _value;
    private string _key;
    #endregion

    #region Constructors
    public DoSaveInteger(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        DataParam[] param = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.Integer, out param[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.String, out param[1]);

        if (isSuccess)
        {
            isSuccess &= IntParser.ParamToInt(param[0], Requester, out _value);
            isSuccess &= StringParser.ParamToString(param[1], Requester, out _key);

            if (isSuccess)
            {
                string saveKey = CreateSaveKey(_key);
                GameManager.Instance.SaveInteger(saveKey, _value);

                // Packing ParamList
                {
                    AddParamList(CreateParamID(1), saveKey);
                    AddParamList(CreateParamID(2), _value.ToString());
                }

                if (_onPrepareComplete != null)
                {
                    _onPrepareComplete.Invoke();
                }
                return;
            }
        }

        // Prepare param failed.
        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        // Prepare ParamList.
        {
            _key = PopParamList(CreateParamID(1));
            _value = int.Parse(PopParamList(CreateParamID(2)));
        }
        GameManager.Instance.SaveInteger(_key, _value);

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoSaveMinion Class
public class DoSaveMinion : EffectData
{
    #region Private Properties
    private List<MinionData> _minionList;
    private string _key;
    #endregion

    #region Constructors
    public DoSaveMinion(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        DataParam[] param = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.MinionList, out param[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.String, out param[1]);

        if (isSuccess)
        {
            isSuccess &= MinionParser.ParamToMinionList(param[0], Requester, out _minionList);
            isSuccess &= StringParser.ParamToString(param[1], Requester, out _key);

            if (isSuccess)
            {
                string saveKey = CreateSaveKey(_key);
                GameManager.Instance.SaveMinionList(saveKey, _minionList);

                // Packing ParamList
                {
                    AddParamList(CreateParamID(1), saveKey);
                    AddParamList(CreateParamID(2), ConvertMinionListToText(_minionList));
                }

                if (_onPrepareComplete != null)
                {
                    _onPrepareComplete.Invoke();
                }
                return;
            }
        }

        // Prepare param failed.
        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        // Prepare ParamList.
        {
            _key = PopParamList(CreateParamID(1));
            _minionList = RevertToMinionList(PopParamList(CreateParamID(2)));
        }
        GameManager.Instance.SaveMinionList(_key, _minionList);

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoClearMinion Class
public class DoClearMinion : EffectData
{
    #region Private Properties
    private string _key;
    #endregion

    #region Constructors
    public DoClearMinion(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        DataParam[] param = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.String, out param[0]);

        if (isSuccess)
        {
            isSuccess &= StringParser.ParamToString(param[0], Requester, out _key);

            if (isSuccess)
            {
                string saveKey = CreateSaveKey(_key);
                GameManager.Instance.SaveMinionList(saveKey, new List<MinionData>());

                // Packing ParamList
                {
                    AddParamList(CreateParamID(1), saveKey);
                }

                if (_onPrepareComplete != null)
                {
                    _onPrepareComplete.Invoke();
                }
                return;
            }
        }

        // Prepare param failed.
        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        // Prepare ParamList.
        {
            _key = PopParamList(CreateParamID(1));
        }
        GameManager.Instance.SaveMinionList(_key, new List<MinionData>());

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoSaveSlot Class
public class DoSaveSlot : EffectData
{
    #region Private Properties
    private List<SlotData> _slotDataList;
    private string _key;
    #endregion

    #region Constructors
    public DoSaveSlot(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        DataParam[] param = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.SlotList, out param[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.String, out param[1]);

        if (isSuccess)
        {
            isSuccess &= SlotParser.ParamToSlotList(param[0], Requester, out _slotDataList);
            isSuccess &= StringParser.ParamToString(param[1], Requester, out _key);

            if (isSuccess)
            {
                string saveKey = CreateSaveKey(_key);
                GameManager.Instance.SaveSlotList(saveKey, _slotDataList);

                // Packing ParamList
                {
                    AddParamList(CreateParamID(1), saveKey);
                    AddParamList(CreateParamID(2), ConvertSlotListToParamText(_slotDataList));
                }

                if (_onPrepareComplete != null)
                {
                    _onPrepareComplete.Invoke();
                }
                return;
            }
        }

        // Prepare param failed.
        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        // Prepare ParamList.
        {
            _key = PopParamList(CreateParamID(1));
            _slotDataList = RevertToSlotList(PopParamList(CreateParamID(2)));
        }
        GameManager.Instance.SaveSlotList(_key, _slotDataList);

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoIf Class
public class DoIf : EffectData
{
    #region Private Properties
    private EffectData _effectData;
    private bool _boolCondition;

    private DataParam[] _param;
    private UnityAction _onActionComplete;
    #endregion

    #region Constructors
    public DoIf(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        _param = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.Boolean, out _param[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.Effect, out _param[1]);

        if (isSuccess)
        {
            isSuccess &= EffectParser.ParamToEffect(_param[1], Requester, out _effectData);
            if (isSuccess)
            {
                _effectData.SetUniqueKey(string.Format("{0}_1", UniqueKey));
                if (_onPrepareComplete != null)
                {
                    _onPrepareComplete.Invoke();
                }
                return;
            }
        }
        // Prepare param failed.
        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        bool isSuccess = true;
        _onActionComplete = onComplete;

        _param = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.Boolean, out _param[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.Effect, out _param[1]);

        isSuccess &= BooleanParser.ParamToBoolean(_param[0], Requester, out _boolCondition);
        isSuccess &= EffectParser.ParamToEffect(_param[1], Requester, out _effectData);

        if (isSuccess)
        {
            if (_boolCondition && _effectData != null)
            {
                _effectData.SetUniqueKey(string.Format("{0}_1", UniqueKey));
                _effectData.Prepare(EffectPrepareComplete, EffectPrepareCancel);
                return;
            }
        }

        if (_onActionComplete != null)
        {
            _onActionComplete.Invoke();
        }
    }

    private void EffectPrepareComplete()
    {
        _effectData.Action(_onActionComplete);
    }

    private void EffectPrepareCancel()
    {
        if (_onActionComplete != null)
        {
            _onActionComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoIfElse Class
public class DoIfElse : EffectData
{
    #region Private Properties
    private EffectData _effectData1;
    private EffectData _effectData2;
    private bool _boolCondition;

    private DataParam[] _param;

    private EffectData _activeEffect;
    private UnityAction _onActionComplete;
    #endregion

    #region Constructors
    public DoIfElse(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        _param = new DataParam[3];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.Boolean, out _param[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.Effect, out _param[1]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[2], DataParam.ParamType.Effect, out _param[2]);

        if (isSuccess)
        {
            isSuccess &= BooleanParser.ParamToBoolean(_param[0], Requester, out _boolCondition);
            isSuccess &= EffectParser.ParamToEffect(_param[1], Requester, out _effectData1);
            isSuccess &= EffectParser.ParamToEffect(_param[2], Requester, out _effectData2);

            if (isSuccess)
            {
                if (_boolCondition)
                {
                    _effectData1.SetUniqueKey(string.Format("{0}_1", UniqueKey));
                    _effectData1.Prepare(_onPrepareComplete, _onPrepareCancel);
                }
                else
                {
                    _effectData2.SetUniqueKey(string.Format("{0}_2", UniqueKey));
                    _effectData2.Prepare(_onPrepareComplete, _onPrepareCancel);
                }
                return;
            }
        }
        // Prepare param failed.
        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        bool isSuccess = true;

        _param = new DataParam[3];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.Boolean, out _param[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.Effect, out _param[1]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[2], DataParam.ParamType.Effect, out _param[2]);

        _onActionComplete = onComplete;

        isSuccess &= BooleanParser.ParamToBoolean(_param[0], Requester, out _boolCondition);
        isSuccess &= EffectParser.ParamToEffect(_param[1], Requester, out _effectData1);
        isSuccess &= EffectParser.ParamToEffect(_param[2], Requester, out _effectData2);

        if (isSuccess)
        {
            _effectData1.SetUniqueKey(string.Format("{0}_1", UniqueKey));
            _effectData2.SetUniqueKey(string.Format("{0}_2", UniqueKey));

            if (_boolCondition)
            {
                _activeEffect = _effectData1;
            }
            else
            {
                _activeEffect = _effectData2;
            }

            if (_activeEffect != null)
            {
                _activeEffect.Prepare(EffectPrepareComplete, EffectPrepareCancel);
                return;
            }
        }

        if (_onActionComplete != null)
        {
            _onActionComplete.Invoke();
        }
    }

    private void EffectPrepareComplete()
    {
        _activeEffect.Action(_onActionComplete);
    }

    private void EffectPrepareCancel()
    {
        if (_onActionComplete != null)
        {
            _onActionComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoTriggerCounting Class
public class DoTriggerCounting : EffectData
{
    #region Private Properties
    private EffectData _effectData;
    private int _maxCounting;
    private int _counting = 0;

    private DataParam[] _param;

    private EffectData _activeEffect;
    private UnityAction _onActionComplete;
    #endregion

    #region Constructors
    public DoTriggerCounting(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        _param = new DataParam[3];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.Integer, out _param[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.Effect, out _param[1]);

        if (isSuccess)
        {
            isSuccess &= IntParser.ParamToInt(_param[0], Requester, out _maxCounting);
            isSuccess &= EffectParser.ParamToEffect(_param[1], Requester, out _effectData);
            if (isSuccess)
            {
                _effectData.SetUniqueKey(string.Format("{0}_1", UniqueKey));
                if (_onPrepareComplete != null)
                {
                    _onPrepareComplete.Invoke();
                }
                return;
            }
        }
        // Prepare param failed.
        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        bool isSuccess = true;
        _onActionComplete = onComplete;

        _counting++;
        if (_counting >= _maxCounting)
        {
            _param = new DataParam[2];
            isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.Effect, out _param[1]);
            if (isSuccess)
            {
                isSuccess &= EffectParser.ParamToEffect(_param[1], Requester, out _effectData);
                if (isSuccess)
                {
                    if (_effectData != null)
                    {
                        _effectData.SetUniqueKey(string.Format("{0}_1", UniqueKey));
                        _effectData.Prepare(EffectPrepareComplete, EffectPrepareCancel);
                        _counting = 0;
                        return;
                    }
                }
            }
        }

        if (_onActionComplete != null)
        {
            _onActionComplete.Invoke();
        }
    }

    private void EffectPrepareComplete()
    {
        _effectData.Action(_onActionComplete);
    }

    private void EffectPrepareCancel()
    {
        if (_onActionComplete != null)
        {
            _onActionComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoCreateMinion Class
public class DoCreateMinion : EffectData
{
    #region Private Properties
    private string _cardID;
    private List<SlotData> _slotList;
    #endregion

    #region Constructors
    public DoCreateMinion(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.String, out p[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.SlotList, out p[1]);

        if (isSuccess)
        {
            isSuccess &= StringParser.ParamToString(p[0], Requester, out _cardID);
            isSuccess &= SlotParser.ParamToSlotList(p[1], Requester, out _slotList);
            if (isSuccess && _slotList != null)
            {
                CardDBData rawData = null;
                if (CardDB.Instance.GetData(_cardID, out rawData))
                {
                    // Packing ParamList
                    {
                        AddParamList(CreateParamID(1), _cardID);
                        AddParamList(CreateParamID(2), ConvertSlotListToParamText(_slotList));
                    }

                    // Found raw data.
                    if (_onPrepareComplete != null)
                    {
                        _onPrepareComplete.Invoke();
                    }
                    return;
                }
            }
        }

        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        // Prepare ParamList.
        {
            _cardID = PopParamList(CreateParamID(1));
            _slotList = RevertToSlotList(PopParamList(CreateParamID(2)));
        }

        List<CommandData> commandList = new List<CommandData>();
        foreach (SlotData slot in _slotList)
        {
            commandList.Add(new CreateTokenCommand(_cardID, Requester.PlayerIndex, slot.SlotID, false));
        }

        if (Requester.Ability != null)
        {
            Requester.Ability.AddCommandData(commandList);
        }
        else
        {
            Debug.LogErrorFormat("EffectData Error: Ability not found on {0}", Param.Key);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoSummonMinion Class
public class DoSummonMinion : EffectData
{
    #region Private Properties
    private List<MinionData> _minionList;
    private List<SlotData> _slotList;

    private int _index = 0;
    private UnityAction _onActionComplete;
    #endregion

    #region Constructors
    public DoSummonMinion(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.SlotList, out p[1]);

        if (isSuccess)
        {
            isSuccess &= MinionParser.ParamToMinionList(p[0], Requester, out _minionList);
            isSuccess &= SlotParser.ParamToSlotList(p[1], Requester, out _slotList);
            if (isSuccess && _slotList != null && _minionList != null && _slotList.Count > 0 && _minionList.Count > 0)
            {
                // Packing ParamList
                {
                    AddParamList(CreateParamID(1), ConvertMinionListToText(_minionList));
                    AddParamList(CreateParamID(2), ConvertSlotListToParamText(_slotList));
                }

                if (_onPrepareComplete != null)
                {
                    _onPrepareComplete.Invoke();
                }
                return;
            }
        }

        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        _onActionComplete = onComplete;
        _index = 0;

        // Prepare ParamList.
        {
            _minionList = RevertToMinionList(PopParamList(CreateParamID(1)));
            _slotList = RevertToSlotList(PopParamList(CreateParamID(2)));
        }

        List<CommandData> commandList = new List<CommandData>();
        for (int index = 0; index < _minionList.Count && index < _slotList.Count; ++index)
        {
            commandList.Add(new SpawnTokenCommand(_minionList[index], _slotList[index].SlotID, false));
        }

        if (Requester.Ability != null)
        {
            Requester.Ability.AddCommandData(commandList);
        }
        else
        {
            Debug.LogErrorFormat("EffectData Error: Ability not found on {0}", Param.Key);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoResetMinion Class
public class DoResetMinion : EffectData
{
    #region Private Properties
    private List<MinionData> _minionList;
    #endregion

    #region Constructors
    public DoResetMinion(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        if (isSuccess)
        {
            isSuccess &= MinionParser.ParamToMinionList(p[0], Requester, out _minionList);
            if (isSuccess)
            {
                // Packing ParamList
                {
                    AddParamList(CreateParamID(1), ConvertMinionListToText(_minionList));
                }

                if (_onPrepareComplete != null)
                {
                    _onPrepareComplete.Invoke();
                }
                return;
            }
        }

        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        // Prepare ParamList.
        {
            _minionList = RevertToMinionList(PopParamList(CreateParamID(1)));
        }

        List<int> idList = new List<int>();
        foreach (MinionData target in _minionList)
        {
            idList.Add(target.UniqueID);
        }

        if (Requester.Ability != null)
        {
            Requester.Ability.AddCommandData(new ResetMinionCommand(Requester, idList));
        }
        else
        {
            Debug.LogErrorFormat("EffectData Error: Ability not found on {0}", Param.Key);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoMoveMinion Class
public class DoMoveMinion : EffectData
{
    #region Private Properties
    private List<MinionData> _cardList;
    private List<SlotData> _slotList;
    #endregion

    #region Constructors
    public DoMoveMinion(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.SlotList, out p[1]);

        if (isSuccess)
        {
            isSuccess &= MinionParser.ParamToMinionList(p[0], Requester, out _cardList);
            isSuccess &= SlotParser.ParamToSlotList(p[1], Requester, out _slotList);

            if (isSuccess)
            {
                // Packing ParamList
                {
                    AddParamList(CreateParamID(1), ConvertMinionListToText(_cardList));
                    AddParamList(CreateParamID(2), ConvertSlotListToParamText(_slotList));
                }

                if (_onPrepareComplete != null)
                {
                    _onPrepareComplete.Invoke();
                }
                return;
            }
        }

        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        // Prepare ParamList.
        {
            _cardList = RevertToMinionList(PopParamList(CreateParamID(1)));
            _slotList = RevertToSlotList(PopParamList(CreateParamID(2)));
        }

        List<CommandData> commandList = new List<CommandData>();
        for (int index = 0; index < _cardList.Count && index < _slotList.Count; ++index)
        {
            MoveTokenCommand cmd = new MoveTokenCommand(Requester, _cardList[index].UniqueID, _slotList[index].SlotID);
            commandList.Add(cmd);
        }

        if (Requester.Ability != null)
        {
            Requester.Ability.AddCommandData(commandList);
        }
        else
        {
            Debug.LogErrorFormat("EffectData Error: Ability not found on {0}", Param.Key);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoSwapMinionSlot Class
public class DoSwapMinionSlot : EffectData
{
    #region Private Properties
    private List<MinionData> _minionList_1;
    private List<MinionData> _minionList_2;
    #endregion

    #region Constructors
    public DoSwapMinionSlot(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.MinionList, out p[1]);

        if (isSuccess)
        {
            isSuccess &= MinionParser.ParamToMinionList(p[0], Requester, out _minionList_1);
            isSuccess &= MinionParser.ParamToMinionList(p[1], Requester, out _minionList_2);

            if (isSuccess)
            {
                // Packing ParamList
                {
                    AddParamList(CreateParamID(1), ConvertMinionListToText(_minionList_1));
                    AddParamList(CreateParamID(2), ConvertMinionListToText(_minionList_2));
                }

                if (_onPrepareComplete != null)
                {
                    _onPrepareComplete.Invoke();
                }
                return;
            }
        }

        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        // Prepare ParamList.
        {
            _minionList_1 = RevertToMinionList(PopParamList(CreateParamID(1)));
            _minionList_2 = RevertToMinionList(PopParamList(CreateParamID(2)));
        }

        List<CommandData> commandList = new List<CommandData>();
        for (int index = 0; index < _minionList_1.Count && index < _minionList_2.Count; ++index)
        {
            SwapMinionSlotCommand cmd = new SwapMinionSlotCommand(Requester, _minionList_1[index].UniqueID, _minionList_2[index].UniqueID);
            commandList.Add(cmd);
        }

        if (Requester.Ability != null)
        {
            Requester.Ability.AddCommandData(commandList);
        }
        else
        {
            Debug.LogErrorFormat("EffectData Error: Ability not found on {0}", Param.Key);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoDrawCard Class
public class DoDrawCard : EffectData
{
    #region Private Properties
    private List<PlayerIndex> _playerIndexList;
    private int _count;
    #endregion

    #region Constructors
    public DoDrawCard(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.PlayerIndexList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.Integer, out p[1]);

        if (isSuccess)
        {
            isSuccess &= PlayerIndexParser.ParamToPlayerIndexList(p[0], Requester, out _playerIndexList);
            isSuccess &= IntParser.ParamToInt(p[1], Requester, out _count);

            if (isSuccess && _playerIndexList != null && _playerIndexList.Count > 0 && _count > 0)
            {
                // Packing ParamList
                {
                    AddParamList(CreateParamID(1), ConvertPlayerIndexListToText(_playerIndexList));
                    AddParamList(CreateParamID(2), _count.ToString());
                }

                if (_onPrepareComplete != null)
                {
                    _onPrepareComplete.Invoke();
                }
                return;
            }
        }

        // Prepare param failed.
        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        // Packing ParamList
        {
            _playerIndexList = RevertToPlayerIndexList(PopParamList(CreateParamID(1)));
            _count = int.Parse(PopParamList(CreateParamID(2)));
        }

        List<CommandData> commandList = new List<CommandData>();
        foreach (PlayerIndex playerIndex in _playerIndexList)
        {
            BattlePlayerData data = GameManager.Instance.Data.PlayerDataList[(int)playerIndex];

            int toDrawCount = _count;
            int handCount = data.CardList[CardZone.Hand].Count;
            int deckCount = data.CardList[CardZone.Deck].Count;
            int handSlot = GameManager.Instance.MaxHandCard - handCount;

            int drawCount = Mathf.Min(handSlot, toDrawCount, deckCount);
            int discardCount = Mathf.Min(toDrawCount - handSlot, deckCount);
            int drawDamage = toDrawCount - deckCount;

            // Draw card.
            if (drawCount > 0)
            {
                commandList.Add(new DrawCardCommand(playerIndex, drawCount, false));
            }

            // Discard.
            if (discardCount > 0)
            {
                commandList.Add(new DiscardDeckCommand(playerIndex, discardCount));
            }

            // Draw damage.
            if (drawDamage > 0)
            {
                commandList.Add(new DrawDamageCommand(playerIndex, drawDamage));
            }
        }

        if (Requester.Ability != null)
        {
            Requester.Ability.AddCommandData(commandList);
        }
        else
        {
            Debug.LogErrorFormat("EffectData Error: Ability not found on {0}", Param.Key);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoMulligan Class
public class DoMulligan : EffectData
{
    #region Private Properties
    private List<PlayerIndex> _playerIndexList;
    private int _count;
    private List<int> _selectedList;
    #endregion

    #region Constructors
    public DoMulligan(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.PlayerIndexList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.Integer, out p[1]);

        if (isSuccess)
        {
            isSuccess &= PlayerIndexParser.ParamToPlayerIndexList(p[0], Requester, out _playerIndexList);
            isSuccess &= IntParser.ParamToInt(p[1], Requester, out _count);

            if (isSuccess && _playerIndexList != null && _playerIndexList.Count > 0)
            {
                PlayerIndex playerIndex = _playerIndexList[0];
                List<int> uniqueIDList = new List<int>();

                // Get all hand card. 
                foreach (BattleCardData card in GameManager.Instance.Data.PlayerDataList[(int)playerIndex].CardList[CardZone.Hand])
                {
                    uniqueIDList.Add(card.UniqueID);
                }

                GameManager.Instance.ActionPlayerSelectCard(
                      Requester
                    , _playerIndexList[0]
                    , uniqueIDList
                    , _count
                    , OnSelected
                    , _onPrepareCancel
                );
                return;
            }
        }

        // Prepare param failed.
        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    private void OnSelected(List<int> selectedList)
    {
        _selectedList = selectedList;

        // Packing ParamList
        {
            AddParamList(CreateParamID(1), ConvertPlayerIndexListToText(_playerIndexList));
            AddParamList(CreateParamID(2), ConvertIntListToText(_selectedList));
        }

        if (_onPrepareComplete != null)
        {
            _onPrepareComplete.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        // Packing ParamList
        {
            _playerIndexList = RevertToPlayerIndexList(PopParamList(CreateParamID(1)));
            _selectedList = RevertToIntList(PopParamList(CreateParamID(2)));
        }

        if (Requester.Ability != null)
        {
            //UIManager.Instance.DestroyRedrawCard(_playerIndexList[0], _selectedList);
            Requester.Ability.AddCommandData(new ReHandCommand(_playerIndexList[0], _selectedList));
            Requester.Ability.AddCommandData(new DrawCardCommand(_playerIndexList[0], _selectedList.Count, false));
            Requester.Ability.AddCommandData(new ShuffleDeckCommand(_playerIndexList[0]));

        }
        else
        {
            Debug.LogErrorFormat("EffectData Error: Ability not found on {0}", Param.Key);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoDiscardCard Class
public class DoDiscardCard : EffectData
{
    #region Private Properties
    private List<PlayerIndex> _playerIndexList;
    private int _count;
    private bool _isRandom;
    private List<int> _selectedList;
    #endregion

    #region Constructors
    public DoDiscardCard(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        DataParam[] p = new DataParam[3];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.PlayerIndexList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.Integer, out p[1]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[2], DataParam.ParamType.Boolean, out p[2]);

        if (isSuccess)
        {
            isSuccess &= PlayerIndexParser.ParamToPlayerIndexList(p[0], Requester, out _playerIndexList);
            isSuccess &= IntParser.ParamToInt(p[1], Requester, out _count);
            isSuccess &= BooleanParser.ParamToBoolean(p[2], Requester, out _isRandom);

            if (isSuccess && _playerIndexList != null && _playerIndexList.Count > 0)
            {
                PlayerIndex playerIndex = _playerIndexList[0];
                List<int> uniqueIDList = new List<int>();

                // Get all hand card. 
                foreach (BattleCardData card in GameManager.Instance.Data.PlayerDataList[(int)playerIndex].CardList[CardZone.Hand])
                {
                    uniqueIDList.Add(card.UniqueID);
                }

                if (_isRandom)
                {
                    List<int> cardSelectedList = new List<int>();
                    if (_count >= uniqueIDList.Count)
                    {
                        cardSelectedList = new List<int>(uniqueIDList);
                    }
                    else
                    {
                        System.Random r = new System.Random();
                        for (int i = 0; i < _count; ++i)
                        {
                            int index = r.Next(0, uniqueIDList.Count);
                            cardSelectedList.Add(uniqueIDList[index]);
                            uniqueIDList.RemoveAt(index);
                        }
                    }

                    OnSelected(cardSelectedList);
                }
                else
                {
                    GameManager.Instance.ActionPlayerSelectCard(
                          Requester
                        , _playerIndexList[0]
                        , uniqueIDList
                        , _count
                        , OnSelected
                        , _onPrepareCancel
                    );
                }

                return;
            }
        }

        // Prepare param failed.
        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    private void OnSelected(List<int> selectedList)
    {
        _selectedList = selectedList;

        // Packing ParamList
        {
            AddParamList(CreateParamID(1), ConvertPlayerIndexListToText(_playerIndexList));
            AddParamList(CreateParamID(2), ConvertIntListToText(_selectedList));
        }

        if (_onPrepareComplete != null)
        {
            _onPrepareComplete.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        // Packing ParamList
        {
            _playerIndexList = RevertToPlayerIndexList(PopParamList(CreateParamID(1)));
            _selectedList = RevertToIntList(PopParamList(CreateParamID(2)));
        }

        if (Requester.Ability != null)
        {
            Requester.Ability.AddCommandData(new DiscardHandCommand(_playerIndexList[0], _selectedList));
        }
        else
        {
            Debug.LogErrorFormat("EffectData Error: Ability not found on {0}", Param.Key);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoRemoveCard Class
public class DoRemoveCard : EffectData
{
    #region Private Properties
    private List<MinionData> _minionList;
    #endregion

    #region Constructors
    public DoRemoveCard(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);

        if (isSuccess)
        {
            isSuccess &= MinionParser.ParamToMinionList(p[0], Requester, out _minionList);
            if (isSuccess)
            {
                // Packing ParamList
                {
                    AddParamList(CreateParamID(1), ConvertMinionListToText(_minionList));
                }

                if (_onPrepareComplete != null)
                {
                    _onPrepareComplete.Invoke();
                }
                return;
            }
        }
        // Prepare param failed.
        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        // Prepare param list.
        {
            _minionList = RevertToMinionList(PopParamList(CreateParamID(1)));
        }

        List<CommandData> commandList = new List<CommandData>();
        List<EventTriggerData> triggerList = new List<EventTriggerData>();
        Requester triggerer;
        foreach (MinionData minion in _minionList)
        {
            //commandList.Add(new UpdateCardActiveZoneCommand(minion.UniqueID, CardZone.Remove));
            GameManager.Instance.ActionUpdateMinionActiveZone(minion.UniqueID, CardZone.Remove, null);
            triggerer = new Requester(minion);
            if (minion.CurrentZone == CardZone.Battlefield)
            {
                triggerList.Add(new EventTriggerData(EventKey.OnMeExitBattlefield, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnFriendExitBattlefield, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnEnemyExitBattlefield, triggerer));
                commandList.AddRange(EventTrigger.TriggerCommandList(triggerList));
            }
            commandList.Add(new MoveCardToZoneCommand(minion.UniqueID, minion.CurrentZone, CardZone.Remove, -1, null));
        }

        if (Requester.Ability != null)
        {
            Requester.Ability.AddCommandData(commandList);
        }
        else
        {
            Debug.LogErrorFormat("EffectData Error: Ability not found on {0}", Param.Key);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoMoveCardToZone Class
public class DoMoveCardToZone : EffectData
{
    #region Private Properties
    private List<MinionData> _minionList;
    private string _targetZoneStr;
    private CardZone _targetZone;
    #endregion

    #region Constructors
    public DoMoveCardToZone(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.String, out p[1]);

        if (isSuccess)
        {
            isSuccess &= MinionParser.ParamToMinionList(p[0], Requester, out _minionList);
            isSuccess &= StringParser.ParamToString(p[1], Requester, out _targetZoneStr);
            isSuccess &= GameHelper.StrToEnum(_targetZoneStr, out _targetZone);

            if (isSuccess)
            {
                // Packing ParamList
                {
                    AddParamList(CreateParamID(1), ConvertMinionListToText(_minionList));
                    AddParamList(CreateParamID(2), _targetZoneStr);
                }

                if (_onPrepareComplete != null)
                {
                    _onPrepareComplete.Invoke();
                }
                return;
            }
        }

        // Prepare param failed.
        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        // Packing ParamList
        {
            _minionList = RevertToMinionList(PopParamList(CreateParamID(1)));
            _targetZoneStr = PopParamList(CreateParamID(2));
            GameHelper.StrToEnum(_targetZoneStr, out _targetZone);
        }

        if (_targetZone != CardZone.Battlefield)
        {
            List<EventTriggerData> triggerList = new List<EventTriggerData>();
            Requester triggerer;
            List<CommandData> commandList = new List<CommandData>();
            foreach (MinionData minion in _minionList)
            {
                //commandList.Add(new UpdateCardActiveZoneCommand(minion.UniqueID, _targetZone));
                GameManager.Instance.ActionUpdateMinionActiveZone(minion.UniqueID, _targetZone, null);
                triggerer = new Requester(minion);
                if (minion.CurrentZone == CardZone.Battlefield)
                {
                    triggerList.Add(new EventTriggerData(EventKey.OnMeExitBattlefield, triggerer));
                    triggerList.Add(new EventTriggerData(EventKey.OnFriendExitBattlefield, triggerer));
                    triggerList.Add(new EventTriggerData(EventKey.OnEnemyExitBattlefield, triggerer));
                    commandList.AddRange(EventTrigger.TriggerCommandList(triggerList));
                }
                else
                {
                    if (_targetZone == CardZone.Battlefield)
                    {
                        triggerList.Add(new EventTriggerData(EventKey.OnEnter, triggerer));
                    }
                }
                commandList.Add(new MoveCardToZoneCommand(minion.UniqueID, minion.CurrentZone, _targetZone, -1, null));
            }

            if (Requester.Ability != null)
            {
                Requester.Ability.AddCommandData(commandList);
            }
            else
            {
                Debug.LogErrorFormat("EffectData Error: Ability not found on {0}", Param.Key);
            }
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoCreateCardToZone Class
public class DoCreateCardToZone : EffectData
{
    #region Private Properties
    private string _cardID;
    private string _targetZoneStr;
    private List<PlayerIndex> _playerIndexList;
    private int _amount;

    private CardZone _targetZone;
    #endregion

    #region Constructors
    public DoCreateCardToZone(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        DataParam[] p = new DataParam[4];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.String, out p[1]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[2], DataParam.ParamType.PlayerIndexList, out p[2]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[3], DataParam.ParamType.Integer, out p[3]);

        if (isSuccess)
        {
            isSuccess &= StringParser.ParamToString(p[0], Requester, out _cardID);
            isSuccess &= StringParser.ParamToString(p[1], Requester, out _targetZoneStr);
            isSuccess &= PlayerIndexParser.ParamToPlayerIndexList(p[2], Requester, out _playerIndexList);
            isSuccess &= IntParser.ParamToInt(p[3], Requester, out _amount);

            isSuccess &= GameHelper.StrToEnum(_targetZoneStr, out _targetZone);

            if (isSuccess)
            {
                // Packing ParamList
                {
                    AddParamList(CreateParamID(1), _cardID);
                    AddParamList(CreateParamID(2), _targetZoneStr);
                    AddParamList(CreateParamID(3), ConvertPlayerIndexListToText(_playerIndexList));
                    AddParamList(CreateParamID(4), _amount.ToString());
                }

                if (_onPrepareComplete != null)
                {
                    _onPrepareComplete.Invoke();
                }
                return;
            }
        }

        // Prepare param failed.
        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        // Packing ParamList
        {
            _cardID = PopParamList(CreateParamID(1));
            _targetZoneStr = PopParamList(CreateParamID(2));
            _playerIndexList = RevertToPlayerIndexList(PopParamList(CreateParamID(3)));
            _amount = int.Parse(PopParamList(CreateParamID(4)));

            GameHelper.StrToEnum(_targetZoneStr, out _targetZone);
        }

        if (_targetZone != CardZone.Battlefield)
        {
            List<CommandData> commandList = new List<CommandData>();

            for (int i = 0; i < _amount; i++)
            {
                commandList.Add(new CreateCardToZoneCommand(_cardID, _playerIndexList[0], _targetZone, -1, null));
            }

            if (Requester.Ability != null)
            {
                Requester.Ability.AddCommandData(commandList);
            }
            else
            {
                Debug.LogErrorFormat("EffectData Error: Ability not found on {0}", Param.Key);
            }
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoAddMinionToAttackerList Class
public class DoAddMinionToAttackerList : EffectData
{
    #region Private Properties
    private List<MinionData> _minionList;
    #endregion

    #region Constructors
    public DoAddMinionToAttackerList(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);

        if (isSuccess)
        {
            isSuccess &= MinionParser.ParamToMinionList(p[0], Requester, out _minionList);

            if (isSuccess && _minionList.Count > 0)
            {
                // Packing ParamList
                {
                    AddParamList(CreateParamID(1), ConvertMinionListToText(_minionList));
                }

                if (_onPrepareComplete != null)
                {
                    _onPrepareComplete.Invoke();
                }
                return;
            }
        }

        // Prepare param failed.
        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        // Packing ParamList
        {
            _minionList = RevertToMinionList(PopParamList(CreateParamID(1)));
        }

        List<CommandData> commandList = new List<CommandData>();
        foreach (MinionData minion in _minionList)
        {
            commandList.Add(new AddMinionToAttackerListCommand(minion));
        }

        if (Requester.Ability != null)
        {
            Requester.Ability.AddCommandData(commandList);
        }
        else
        {
            Debug.LogErrorFormat("EffectData Error: Ability not found on {0}", Param.Key);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoRemoveMinionFromAttackerList Class
public class DoRemoveMinionFromAttackerList : EffectData
{
    #region Private Properties
    private List<MinionData> _minionList;
    #endregion

    #region Constructors
    public DoRemoveMinionFromAttackerList(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);

        if (isSuccess)
        {
            isSuccess &= MinionParser.ParamToMinionList(p[0], Requester, out _minionList);

            if (isSuccess && _minionList.Count > 0)
            {
                // Packing ParamList
                {
                    AddParamList(CreateParamID(1), ConvertMinionListToText(_minionList));
                }

                if (_onPrepareComplete != null)
                {
                    _onPrepareComplete.Invoke();
                }
                return;
            }
        }

        // Prepare param failed.
        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        // Packing ParamList
        {
            _minionList = RevertToMinionList(PopParamList(CreateParamID(1)));
        }

        List<CommandData> commandList = new List<CommandData>();
        foreach (MinionData minion in _minionList)
        {
            commandList.Add(new RemoveMinionFromAttackerListCommand(minion));
        }

        if (Requester.Ability != null)
        {
            Requester.Ability.AddCommandData(commandList);
        }
        else
        {
            Debug.LogErrorFormat("EffectData Error: Ability not found on {0}", Param.Key);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoBoostHeroSP Class
public class DoBoostHeroSP : EffectData
{
    #region Private Properties
    private List<PlayerIndex> _playerIndexList;
    private int _value;
    #endregion

    #region Constructors
    public DoBoostHeroSP(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.PlayerIndexList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.Integer, out p[1]);

        if (isSuccess)
        {
            isSuccess &= PlayerIndexParser.ParamToPlayerIndexList(p[0], Requester, out _playerIndexList);
            isSuccess &= IntParser.ParamToInt(p[1], Requester, out _value);

            if (isSuccess && _playerIndexList != null && _playerIndexList.Count > 0)
            {
                if (_onPrepareComplete != null)
                {
                    _onPrepareComplete.Invoke();
                }
                return;
            }
        }

        // Prepare param failed.
        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        List<CommandData> commandList = new List<CommandData>();
        foreach (PlayerIndex player in _playerIndexList)
        {
            commandList.Add(new BoostHeroSPCommand(Requester, player, _value));
        }

        if (Requester.Ability != null)
        {
            Requester.Ability.AddCommandData(commandList);
        }
        else
        {
            Debug.LogErrorFormat("EffectData Error: Ability not found on {0}", Param.Key);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoTriggerMyAbility Class
public class DoTriggerMyAbility : EffectData
{
    #region Private Properties
    private int _uniqueAbilityID;
    #endregion

    #region Constructors
    public DoTriggerMyAbility(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        string abilityID = string.Empty;
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.String, out p[0]);

        if (isSuccess)
        {
            isSuccess &= StringParser.ParamToString(p[0], Requester, out abilityID);
            if (isSuccess && !string.IsNullOrEmpty(abilityID))
            {
                foreach (KeyValuePair<int, CardAbilityData> data in Requester.Minion.AbilityList)
                {
                    if (data.Value.Data.ID == abilityID)
                    {
                        _uniqueAbilityID = data.Value.UniqueID;

                        // Packing ParamList
                        {
                            AddParamList(CreateParamID(1), _uniqueAbilityID.ToString());
                        }

                        if (_onPrepareComplete != null)
                        {
                            _onPrepareComplete.Invoke();
                        }
                        return;
                    }
                }
            }
        }

        // Prepare param failed.
        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        // Packing ParamList
        {
            _uniqueAbilityID = int.Parse(PopParamList(CreateParamID(1)));
        }

        List<CommandData> commandList = new List<CommandData>();
        commandList.Add(new PrepareAbilityCommand(Requester.Minion.UniqueID, _uniqueAbilityID, Requester, EventKey.None));

        if (Requester.Ability != null)
        {
            Requester.Ability.AddCommandData(commandList);
        }
        else
        {
            Debug.LogErrorFormat("EffectData Error: Ability not found on {0}", Param.Key);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoAddMinionDirection Class
public class DoAddMinionDirection : EffectData
{
    #region Private Properties
    private List<MinionData> _minionList;
    private CardDirection _cardDir;
    #endregion

    #region Constructors
    public DoAddMinionDirection(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.CardDirection, out p[1]);

        if (isSuccess)
        {
            isSuccess &= MinionParser.ParamToMinionList(p[0], Requester, out _minionList);
            isSuccess &= CardDirectionParser.ParamToCardDirectionList(p[1], Requester, out _cardDir);
            if (isSuccess)
            {
                // Packing ParamList
                {
                    AddParamList(CreateParamID(1), ConvertMinionListToText(_minionList));
                    AddParamList(CreateParamID(2), ConvertCardDirectionToText(_cardDir));
                }

                if (_onPrepareComplete != null)
                {
                    _onPrepareComplete.Invoke();
                }
                return;
            }
        }

        // Prepare param failed.
        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        // Packing ParamList
        {
            _minionList = RevertToMinionList(PopParamList(CreateParamID(1)));
            _cardDir = RevertToCardDirection(PopParamList(CreateParamID(2)));
        }

        List<int> idList = new List<int>();
        foreach (MinionData target in _minionList)
        {
            idList.Add(target.UniqueID);
        }

        if (Requester.Ability != null)
        {
            Requester.Ability.AddCommandData(new AddMinionDirCommand(idList, _cardDir));
        }
        else
        {
            Debug.LogErrorFormat("EffectData Error: Ability not found on {0}", Param.Key);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoSetMinionDirection Class
public class DoSetMinionDirection : EffectData
{
    #region Private Properties
    private List<MinionData> _minionList;
    private CardDirection _cardDir;
    #endregion

    #region Constructors
    public DoSetMinionDirection(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        bool isSuccess = true;
        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.MinionList, out p[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.CardDirection, out p[1]);

        if (isSuccess)
        {
            isSuccess &= MinionParser.ParamToMinionList(p[0], Requester, out _minionList);
            isSuccess &= CardDirectionParser.ParamToCardDirectionList(p[1], Requester, out _cardDir);
            if (isSuccess)
            {
                // Packing ParamList
                {
                    AddParamList(CreateParamID(1), ConvertMinionListToText(_minionList));
                    AddParamList(CreateParamID(2), ConvertCardDirectionToText(_cardDir));
                }

                if (_onPrepareComplete != null)
                {
                    _onPrepareComplete.Invoke();
                }
                return;
            }
        }

        // Prepare param failed.
        if (_onPrepareCancel != null)
        {
            _onPrepareCancel.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        // Packing ParamList
        {
            _minionList = RevertToMinionList(PopParamList(CreateParamID(1)));
            _cardDir = RevertToCardDirection(PopParamList(CreateParamID(2)));
        }

        List<int> idList = new List<int>();
        foreach (MinionData target in _minionList)
        {
            idList.Add(target.UniqueID);
        }

        if (Requester.Ability != null)
        {
            Requester.Ability.AddCommandData(new SetMinionDirCommand(idList, _cardDir));
        }
        else
        {
            Debug.LogErrorFormat("EffectData Error: Ability not found on {0}", Param.Key);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion
#endregion

#region DoMinionPassiveBase Class
public class DoMinionPassiveBase : EffectData
{
    #region Enums
    public enum PassiveState
    {
        Init
        , Active
        , End
    }
    #endregion

    #region Private Properties
    protected PassiveState _state = PassiveState.Init;
    protected List<int> _currentList;
    #endregion

    #region Constructors
    public DoMinionPassiveBase(DataParam param, Requester requester) : base(param, requester)
    {
        _state = PassiveState.Init;
        _currentList = new List<int>();
    }

    ~DoMinionPassiveBase()
    {
        //DeinitAura();
    }
    #endregion

    #region Methods
    public override void Prepare(UnityAction onComplete, UnityAction onCancel)
    {
        _onPrepareComplete = onComplete;
        _onPrepareCancel = onCancel;

        //OnPrepareUpdate();

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public override void Action(UnityAction onComplete)
    {
        OnActionUpdate();

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    protected void OnPrepareUpdate()
    {
    }

    protected void OnActionUpdate()
    {
        switch (_state)
        {
            case PassiveState.Init:
            {
                InitPassive();
                _state = PassiveState.Active;
            }
            break;

            case PassiveState.Active:
            {
                UpdatePassive(null);
            }
            break;

            case PassiveState.End:
            {
                ClearPassive(null);
            }
            break;
        }
    }

    protected virtual List<int> GetTargetIDList()
    {
        return new List<int>();
    }

    protected virtual List<MinionBuffData> GetBuffDataList()
    {
        return new List<MinionBuffData>();
    }

    protected virtual void InitPassive()
    {
        UpdatePassive(null);
    }

    protected virtual void DeinitPassive()
    {
        ClearPassive(null);
    }

    public virtual void UpdatePassive(Requester triggerer)
    {       
    }

    protected virtual void ClearPassive(Requester triggerer)
    {
        for (int i = 0; i < _currentList.Count; ++i)
        {
            // To Deactivate

            // Remove Buff by Unique ID
            RemoveBuff(_currentList[i]);
        }

        _currentList.Clear();
        _state = PassiveState.Init;
    }

    protected virtual void AddBuff(int uniqueCardID)
    {
        List<MinionBuffData> buffDataList = GetBuffDataList();
        bool isJoin = false;

        foreach (MinionBuffData buffData in buffDataList)
        {
            if (buffData != null)
            {
                GameManager.Instance.RequestAddMinionBuff(uniqueCardID, buffData, isJoin);
                isJoin = true;
            }
        }
    }

    protected virtual void UpdateBuff(int uniqueCardID)
    {
        List<MinionBuffData> buffDataList = GetBuffDataList();
        bool isJoin = false;

        foreach (MinionBuffData buffData in buffDataList)
        {
            if (buffData != null)
            {
                GameManager.Instance.RequestUpdateMinionBuff(uniqueCardID, buffData.Key, isJoin);
                isJoin = true;
            }
        }
    }

    protected virtual void RemoveBuff(int uniqueCardID)
    {
        List<MinionBuffData> buffDataList = GetBuffDataList();
        bool isJoin = false;

        foreach (MinionBuffData buffData in buffDataList)
        {
            if (buffData != null)
            {
                GameManager.Instance.RequestRemoveMinionBuff(uniqueCardID, buffData.Key, isJoin);
                isJoin = true;
            }
        }
    }
    #endregion
}
#endregion

#region DoMinionAura Class
public class DoMinionAura : DoMinionPassiveBase
{
    #region Enums
    private enum ParamOrder
    {
        Minion = 0
        , Condition = 1
        , BuffDataList = 2
    }
    #endregion

    #region Private Properties
    private MinionBuffOrigins _originKey = MinionBuffOrigins.PASSIVEBUFF;

    private List<string> _auraSpecificKeys = new List<string>() { "MinionFilterMine(MinionAdjacent8(MinionMe))"
                                                                , "MinionFilterMine(MinionFilterAura(MinionAdjacent8(MinionMe)))" };
    #endregion

    #region Constructors
    public DoMinionAura(DataParam param, Requester requester) : base(param, requester)
    {
    }

    ~DoMinionAura()
    {
        //DeinitAura();
    }
    #endregion

    #region Methods
    protected override List<int> GetTargetIDList()
    {
        bool isSuccess = true;
        DataParam[] param = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[(int)ParamOrder.Minion], DataParam.ParamType.MinionList, out param[0]);

        if (isSuccess)
        {
            List<MinionData> targetList;
            isSuccess &= MinionParser.ParamToMinionList(param[0], Requester, out targetList);

            if (isSuccess)
            {
                List<int> targetIDList = new List<int>();

                foreach (MinionData card in targetList)
                {
                    if (card.ActiveZone == CardZone.Battlefield && card.IsAlive)
                    {
                        targetIDList.Add(card.UniqueID);
                    }
                }

                return targetIDList;
            }
        }

        return new List<int>();
    }

    protected override List<MinionBuffData> GetBuffDataList()
    {
        bool isSuccess = true;
        DataParam param;
        MinionBuffData buff = null;
        List<MinionBuffData> buffList = new List<MinionBuffData>();
        List<string> buffTextList = new List<string>();

        for (int i = (int)ParamOrder.BuffDataList; i < Param.Parameters.Count; i++)
        {
            buffTextList.Add(Param.Parameters[i]);
        }

        foreach (string buffText in buffTextList)
        {
            isSuccess &= DataParam.TextToDataParam(buffText, DataParam.ParamType.Buff, out param);
            if (isSuccess)
            {
                isSuccess &= MinionBuffParser.ParamToMinionBuff(UniqueID, _originKey, BuffCounterType.None, -1, param, Requester, out buff);
                if (isSuccess)
                {
                    buffList.Add(buff);
                }
            }
        }

        return buffList;
    }

    protected override void InitPassive()
    {
        // Subscribe Event
        if (Requester.Type == Requester.RequesterType.Minion)
        {
            MinionData card = Requester.Minion;

            // Phase
            card.SubscribeEvent(EventKey.BeginPhase, UpdatePassive);

            // Spawning
            //card.SubscribeEvent(EventKey.Awaken, InitBuff);
            card.SubscribeEvent(EventKey.Rally, UpdatePassive);
            card.SubscribeEvent(EventKey.Sentinel, UpdatePassive);

            // Moving
            card.SubscribeEvent(EventKey.OnMeMoved, UpdatePassive);
            card.SubscribeEvent(EventKey.OnFriendMoved, UpdatePassive);
            card.SubscribeEvent(EventKey.OnEnemyMoved, UpdatePassive);

            // Dead
            card.SubscribeEvent(EventKey.OnMeExitBattlefield, ClearPassive);
            card.SubscribeEvent(EventKey.OnFriendExitBattlefield, UpdatePassive);
            card.SubscribeEvent(EventKey.OnEnemyExitBattlefield, UpdatePassive);

            // Buff
            card.SubscribeEvent(EventKey.OnMeChangeDir, UpdatePassive);
            card.SubscribeEvent(EventKey.OnFriendChangeDir, UpdatePassive);
            card.SubscribeEvent(EventKey.OnEnemyChangeDir, UpdatePassive);
            card.SubscribeEvent(EventKey.OnMeBeBuffed, UpdatePassive);
            card.SubscribeEvent(EventKey.OnFriendBeBuffed, UpdatePassive);
            card.SubscribeEvent(EventKey.OnEnemyBeBuffed, UpdatePassive);

            // Slot
            card.SubscribeEvent(EventKey.OnSlotLock, UpdatePassive);
            card.SubscribeEvent(EventKey.OnSlotUnlock, UpdatePassive);

            // Set status
            if (Requester.Minion != null)
            {
                if (Requester.Ability.Data.ID == "TAUNT")
                {
                    Requester.Minion.SetTauntActive(true);
                    _originKey = MinionBuffOrigins.TAUNTBUFF;
                }
                else
                {
                    if (Requester.Minion.IsAbilityAura)
                    {
                        if (_auraSpecificKeys.Contains(Param.Parameters[(int)ParamOrder.Minion]))
                        {
                            Requester.Minion.SetAuraActive(true);
                            _originKey = MinionBuffOrigins.AURABUFF;
                        }
                    }
                }

                CardUIData cardUIData = new CardUIData(Requester.Minion);
                UIManager.Instance.RequestUpdateCardAllPassive(cardUIData);
            }
        }

        UpdatePassive(null);
    }

    protected override void DeinitPassive()
    {
        // Unsubscribe Event
        if (Requester.Type == Requester.RequesterType.Minion)
        {
            MinionData card = Requester.Minion;

            // Phase
            card.UnsubscribeEvent(EventKey.BeginPhase, UpdatePassive);

            // Spawning
            card.UnsubscribeEvent(EventKey.Awaken, UpdatePassive);
            card.UnsubscribeEvent(EventKey.Rally, UpdatePassive);
            card.UnsubscribeEvent(EventKey.Sentinel, UpdatePassive);

            // Moving
            card.UnsubscribeEvent(EventKey.OnMeMoved, UpdatePassive);
            card.UnsubscribeEvent(EventKey.OnFriendMoved, UpdatePassive);
            card.UnsubscribeEvent(EventKey.OnEnemyMoved, UpdatePassive);

            // Dead
            card.UnsubscribeEvent(EventKey.OnMeExitBattlefield, ClearPassive);
            card.UnsubscribeEvent(EventKey.OnFriendExitBattlefield, UpdatePassive);
            card.UnsubscribeEvent(EventKey.OnEnemyExitBattlefield, UpdatePassive);

            // Buff
            card.UnsubscribeEvent(EventKey.OnMeChangeDir, UpdatePassive);
            card.UnsubscribeEvent(EventKey.OnFriendChangeDir, UpdatePassive);
            card.UnsubscribeEvent(EventKey.OnEnemyChangeDir, UpdatePassive);
            card.UnsubscribeEvent(EventKey.OnMeBeBuffed, UpdatePassive);
            card.UnsubscribeEvent(EventKey.OnFriendBeBuffed, UpdatePassive);
            card.UnsubscribeEvent(EventKey.OnEnemyBeBuffed, UpdatePassive);

            // Slot
            card.UnsubscribeEvent(EventKey.OnSlotLock, UpdatePassive);
            card.UnsubscribeEvent(EventKey.OnSlotUnlock, UpdatePassive);
        }

        ClearPassive(null);
    }

    public override void UpdatePassive(Requester triggerer)
    {
        if (
            Requester.Minion != null
            && (Requester.Minion.ActiveZone != CardZone.Battlefield
            || (Requester.Minion.IsDead || Requester.Minion.IsBeSilence)) // dead or silence
        )
        {
            ClearPassive(null);
            return;
        }

        //Debug.Log("UpdateAura " + triggererID);

        // Get new terget list.
        List<int> toActivateList = GetTargetIDList();
        List<int> toDeactivateList = new List<int>();
        List<int> activatedList = new List<int>();

        List<int> currentList = new List<int>(_currentList);

        bool isSuccess = true;
        bool isCanDoAura = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[(int)ParamOrder.Condition], DataParam.ParamType.Boolean, out p[0]);
        if (isSuccess)
        {
            isSuccess &= BooleanParser.ParamToBoolean(p[0], Requester, out isCanDoAura);
        }

        if (!isSuccess || !isCanDoAura)
        {
            ClearPassive(null);
            return;
        }

        // Find and remove old target.
        for (int i = 0; i < currentList.Count; ++i)
        {
            for (int j = 0; j < toActivateList.Count; ++j)
            {
                if (currentList[i] == toActivateList[j])
                {
                    // Already active. 
                    // Update buff.
                    UpdateBuff(currentList[i]);

                    // Remove from to activate list
                    activatedList.Add(currentList[i]);

                    currentList.RemoveAt(i); --i;
                    toActivateList.RemoveAt(j);
                    break;
                }
            }
        }

        // Remaining ID in current list is not in to-active List.
        // Will be put in deactivate list.
        toDeactivateList = new List<int>(currentList);

        // Start activate list.
        for (int i = 0; i < toActivateList.Count; ++i)
        {
            // To Activate

            // Add Buff by Unique ID
            AddBuff(toActivateList[i]);

            // Add to activated list.
            activatedList.Add(toActivateList[i]);
        }

        // Start deactivate list.
        for (int i = 0; i < toDeactivateList.Count; ++i)
        {
            // To Deactivate

            // Remove Buff by Unique ID
            RemoveBuff(toDeactivateList[i]);
        }

        // Save activated list to current list.
        _currentList = new List<int>(activatedList);
    }
    #endregion
}
#endregion

#region DoMinionLink Class
public class DoMinionLink : DoMinionPassiveBase
{
    #region Enums
    private enum ParamOrder
    {
        LinkCount = 0
        , Minion = 1
        , Condition = 2
        , BuffDataList = 3
    }
    #endregion

    #region Private Properties
    private List<int> _currentLinkList;
    private List<int> _currentBuffList;
    private List<int> _currentSlotList;
    private int _linkMinCount;
    #endregion

    #region Constructors
    public DoMinionLink(DataParam param, Requester requester) : base(param, requester)
    {
        _currentLinkList = new List<int>();
        _currentBuffList = new List<int>();
        _currentSlotList = new List<int>();
    }

    ~DoMinionLink()
    {
        //DeinitAura();
    }
    #endregion

    #region Methods
    protected List<int> GetTargetIDLinkList()
    {
        List<int> result = new List<int>();
        List<MinionData> nodeList = new List<MinionData>();
        MinionData requesterMinion = Requester.Minion;
        MinionData minionAdjacent;

        result.Add(requesterMinion.UniqueID);
        nodeList.Add(requesterMinion);

        CardDirectionType opsDir;
        while (nodeList.Count > 0)
        {
            foreach (CardDirectionType dir in System.Enum.GetValues(typeof(CardDirectionType)))
            {
                if (nodeList[0].CardDir.IsDirection(dir)
                    && nodeList[0].GetAdjacentMinion(dir, out minionAdjacent))
                {
                    if (minionAdjacent.IsAlive
                        && minionAdjacent.ActiveZone == CardZone.Battlefield)
                    {
                        if (!result.Contains(minionAdjacent.UniqueID)
                            && minionAdjacent.Owner == Requester.PlayerIndex
                            && GameHelper.GetOppositeDirection(dir, out opsDir))
                        {
                            if (minionAdjacent.CardDir.IsDirection(opsDir))
                            {
                                result.Add(minionAdjacent.UniqueID);
                                nodeList.Add(minionAdjacent);
                            }
                        }
                    }
                }
            }
            nodeList.RemoveAt(0);
        }

        return result;
    }

    protected override List<int> GetTargetIDList()
    {
        bool isSuccess = true;
        DataParam[] param = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[(int)ParamOrder.Minion], DataParam.ParamType.MinionList, out param[0]);

        if (isSuccess)
        {
            List<MinionData> targetList;
            isSuccess &= MinionParser.ParamToMinionList(param[0], Requester, out targetList);

            if (isSuccess)
            {
                List<int> targetIDList = new List<int>();
                foreach (MinionData card in targetList)
                {
                    if (card.ActiveZone == CardZone.Battlefield && card.IsAlive)
                    {
                        targetIDList.Add(card.UniqueID);
                    }
                }

                return targetIDList;
            }
        }

        return new List<int>();
    }

    protected override List<MinionBuffData> GetBuffDataList()
    {
        bool isSuccess = true;
        DataParam param;
        MinionBuffData buff = null;
        List<MinionBuffData> buffList = new List<MinionBuffData>();
        List<string> buffTextList = new List<string>();

        for (int i = (int)ParamOrder.BuffDataList; i < Param.Parameters.Count; i++)
        {
            buffTextList.Add(Param.Parameters[i]);
        }

        foreach (string buffText in buffTextList)
        {
            isSuccess &= DataParam.TextToDataParam(buffText, DataParam.ParamType.Buff, out param);
            if (isSuccess)
            {
                isSuccess &= MinionBuffParser.ParamToMinionBuff(UniqueID, MinionBuffOrigins.LINKBUFF, BuffCounterType.None, -1, param, Requester, out buff);
                if (isSuccess)
                {
                    buffList.Add(buff);
                }
            }
        }

        return buffList;
    }

    protected override void InitPassive()
    {
        // Subscribe Event
        if (Requester.Type == Requester.RequesterType.Minion)
        {
            MinionData card = Requester.Minion;

            // Spawning
            card.SubscribeEvent(EventKey.Rally, UpdatePassive);

            // Moving
            card.SubscribeEvent(EventKey.OnMeMoved, UpdatePassive);
            card.SubscribeEvent(EventKey.OnFriendMoved, UpdatePassive);

            // Dead
            card.SubscribeEvent(EventKey.OnMeExitBattlefield, ClearPassive);
            card.SubscribeEvent(EventKey.OnFriendExitBattlefield, UpdatePassive);

            // Buff
            card.SubscribeEvent(EventKey.OnMeBeBuffed, UpdatePassive);
            card.SubscribeEvent(EventKey.OnMeChangeDir, UpdatePassive);
            card.SubscribeEvent(EventKey.OnFriendChangeDir, UpdatePassive);
        }

        UpdatePassive(null);
    }

    protected override void DeinitPassive()
    {
        // Unsubscribe Event
        if (Requester.Type == Requester.RequesterType.Minion)
        {
            MinionData card = Requester.Minion;

            // Spawning
            card.UnsubscribeEvent(EventKey.Awaken, UpdatePassive);
            card.UnsubscribeEvent(EventKey.Rally, UpdatePassive);

            // Moving
            card.UnsubscribeEvent(EventKey.OnMeMoved, UpdatePassive);
            card.UnsubscribeEvent(EventKey.OnFriendMoved, UpdatePassive);

            // Dead
            card.UnsubscribeEvent(EventKey.OnMeExitBattlefield, ClearPassive);
            card.UnsubscribeEvent(EventKey.OnFriendExitBattlefield, UpdatePassive);

            // Buff
            card.UnsubscribeEvent(EventKey.OnMeBeBuffed, UpdatePassive);
            card.UnsubscribeEvent(EventKey.OnMeChangeDir, UpdatePassive);
            card.UnsubscribeEvent(EventKey.OnFriendChangeDir, UpdatePassive);
        }

        ClearPassive(null);
    }

    public override void UpdatePassive(Requester triggerer)
    {
        if (
            Requester.Minion != null
            && (Requester.Minion.ActiveZone != CardZone.Battlefield
            || (Requester.Minion.IsDead || Requester.Minion.IsBeSilence)) // dead or silence
        )
        {
            ClearPassive(null);
            return;
        }

        // Get new terget list.
        List<int> toActivateBuffList = new List<int>();
        List<int> toActivateLinkList = new List<int>();
        List<int> toDeactivateBuffList = new List<int>();
        List<int> toDeactivateLinkList = new List<int>();
        List<int> activatedBuffList = new List<int>();
        List<int> activatedLinkList = new List<int>();

        List<int> currentLinkList = new List<int>(_currentLinkList);
        List<int> currentBuffList = new List<int>(_currentBuffList);
        bool isSuccess = true;
        bool isCanDoLink = true;
        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[(int)ParamOrder.LinkCount], DataParam.ParamType.Integer, out p[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[(int)ParamOrder.Condition], DataParam.ParamType.Boolean, out p[1]);
        if (isSuccess)
        {
            isSuccess &= IntParser.ParamToInt(p[0], Requester, out _linkMinCount);
            isSuccess &= BooleanParser.ParamToBoolean(p[1], Requester, out isCanDoLink);
            if (isSuccess)
            {
                toActivateLinkList = GetTargetIDLinkList();
                toActivateBuffList = GetTargetIDList();
                isSuccess &= toActivateLinkList.Count >= _linkMinCount;
            }
        }
        //Debug.Log("UpdateLink " + triggererID);

        if (!isSuccess || !isCanDoLink)
        {
            ClearPassive(null);
            return;
        }

        // Find and remove old target.
        for (int i = 0; i < currentLinkList.Count; ++i)
        {
            for (int j = 0; j < toActivateLinkList.Count; ++j)
            {
                if (currentLinkList[i] == toActivateLinkList[j])
                {
                    // Already active. 
                    // Update Link.
                    UpdateLink(currentLinkList[i]);

                    // Remove from to activate list
                    activatedLinkList.Add(currentLinkList[i]);

                    currentLinkList.RemoveAt(i);
                    --i;

                    toActivateLinkList.RemoveAt(j);
                    --j;
                    break;
                }
            }
        }

        for (int i = 0; i < currentBuffList.Count; ++i)
        {
            for (int j = 0; j < toActivateBuffList.Count; ++j)
            {
                if (currentBuffList[i] == toActivateBuffList[j])
                {
                    // Already active. 
                    // Update buff.
                    UpdateBuff(currentBuffList[i]);

                    // Remove from to activate list
                    activatedBuffList.Add(currentBuffList[i]);

                    currentBuffList.RemoveAt(i);
                    --i;

                    toActivateBuffList.RemoveAt(j);
                    --j;
                    break;
                }
            }
        }

        // Remaining ID in current list is not in to-active List.
        // Will be put in deactivate list.
        toDeactivateLinkList = new List<int>(currentLinkList);
        toDeactivateBuffList = new List<int>(currentBuffList);

        //-------------------------------------------------------

        // Clear all previous activated link UI.
        foreach (int slotId in _currentSlotList)
        {
            GameManager.Instance.ActionResetLinkDirOnSlot(slotId, null);
        }

        MinionData minion;
        // Start activate list.
        for (int i = 0; i < toActivateLinkList.Count; ++i)
        {
            // Add Link by Unique ID
            AddLink(toActivateLinkList[i]);

            // Add to activated list.
            activatedLinkList.Add(toActivateLinkList[i]);
        }

        for (int i = 0; i < toActivateBuffList.Count; ++i)
        {
            // Add Buff by Unique ID
            AddBuff(toActivateBuffList[i]);

            // Add to activated list.
            activatedBuffList.Add(toActivateBuffList[i]);
        }

        // Request update link UI
        foreach (int slotID in _currentSlotList)
        {
            GameManager.Instance.RequestUpdateLinkOnSlot(slotID, true);
        }

        // Start deactivate list.
        for (int i = 0; i < toDeactivateLinkList.Count; ++i)
        {
            // Remove Link by Unique ID
            RemoveLink(toDeactivateLinkList[i]);
        }

        for (int i = 0; i < toDeactivateBuffList.Count; ++i)
        {
            // Remove Buff by Unique ID
            RemoveBuff(toDeactivateBuffList[i]);
        }

        // Set activate link UI
        for (int i = 0; i < activatedLinkList.Count; ++i)
        {
            if (GameManager.Instance.FindMinionBattleCard(activatedLinkList[i], out minion))
            {
                MinionData minionAdjacent;
                CardDirectionType opsDir;
                foreach (CardDirectionType dir in System.Enum.GetValues(typeof(CardDirectionType)))
                {
                    if (minion.CardDir.IsDirection(dir) && minion.GetAdjacentMinion(dir, out minionAdjacent))
                    {
                        if (minionAdjacent.IsAlive && minionAdjacent.ActiveZone == CardZone.Battlefield)
                        {
                            if (minionAdjacent.Owner == Requester.PlayerIndex && GameHelper.GetOppositeDirection(dir, out opsDir))
                            {
                                if (minionAdjacent.CardDir.IsDirection(opsDir))
                                {
                                    GameManager.Instance.ActionAddLinkDirOnSlot(minion.SlotID, dir, null);

                                    if (!activatedLinkList.Contains(minionAdjacent.UniqueID))
                                    {
                                        GameManager.Instance.ActionAddLinkDirOnSlot(minionAdjacent.SlotID, opsDir, null);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        // Save activated list to current list.
        _currentLinkList = new List<int>(activatedLinkList);
        _currentBuffList = new List<int>(activatedBuffList);
    }

    protected override void ClearPassive(Requester triggerer)
    {
        MinionData minion;
        MinionData requesterMinion = Requester.Minion;

        List<int> slotToResetList = new List<int>(_currentSlotList);
        for (int i = 0; i < _currentLinkList.Count; ++i)
        {
            RemoveLink(_currentLinkList[i]);
            // Remove Link by Unique ID 
            if (GameManager.Instance.FindMinionBattleCard(_currentLinkList[i], out minion))
            {
                if (minion.IsBeLink && minion.IsAlive && minion.ActiveZone == CardZone.Battlefield)
                {
                    slotToResetList.Remove(minion.SlotID);
                }
            }
        }

        // Clear all previous activated link UI. 
        foreach (int slotId in slotToResetList)
        {
            GameManager.Instance.ActionResetLinkDirOnSlot(slotId, null);
            GameManager.Instance.RequestUpdateLinkOnSlot(slotId, true);
        }

        //// Clear all previous activated link UI.
        //foreach (int slotId in _currentSlotList)
        //{
        //    GameManager.Instance.ActionResetLinkDirOnSlot(slotId, null);
        //    GameManager.Instance.RequestUpdateLinkOnSlot(slotId, true);
        //}

        for (int i = 0; i < _currentBuffList.Count; ++i)
        {
            // Remove Buff by Unique ID
            RemoveBuff(_currentBuffList[i]);
        }

        _currentLinkList.Clear();
        _currentBuffList.Clear();
        _currentSlotList.Clear();

        _state = PassiveState.Init;
    }

    protected override void AddBuff(int uniqueCardID)
    {
        List<MinionBuffData> buffDataList = GetBuffDataList();
        bool isJoin = false;

        foreach (MinionBuffData buffData in buffDataList)
        {
            if (buffData != null)
            {
                MinionData minion;
                if (GameManager.Instance.FindMinionBattleCard(uniqueCardID, out minion))
                {
                    if (minion.IsAlive)
                    {
                        GameManager.Instance.RequestAddMinionBuff(uniqueCardID, buffData, isJoin);
                        isJoin = true;
                    }
                }
            }
        }
    }

    protected void AddLink(int uniqueCardID)
    {
        GameManager.Instance.ActionSetActiveLinkMinion(uniqueCardID, true);
        MinionData minion;
        if (GameManager.Instance.FindMinionBattleCard(uniqueCardID, out minion))
        {
            if (!_currentSlotList.Contains(minion.SlotID))
            {
                _currentSlotList.Add(minion.SlotID);
            }
        }
    }

    protected override void UpdateBuff(int uniqueCardID)
    {
        List<MinionBuffData> buffDataList = GetBuffDataList();
        bool isJoin = false;

        foreach (MinionBuffData buffData in buffDataList)
        {
            if (buffData != null)
            {
                MinionData minion;
                if (GameManager.Instance.FindMinionBattleCard(uniqueCardID, out minion))
                {
                    if (minion.IsAlive)
                    {
                        GameManager.Instance.RequestUpdateMinionBuff(uniqueCardID, buffData.Key, isJoin);
                        isJoin = true;
                    }
                }
            }
        }
    }

    protected void UpdateLink(int uniqueCardID)
    {
        MinionData minion;
        if (GameManager.Instance.FindMinionBattleCard(uniqueCardID, out minion))
        {
            if (!minion.IsBeLink)
            {
                GameManager.Instance.ActionSetActiveLinkMinion(uniqueCardID, true);
            }
        }
    }

    protected override void RemoveBuff(int uniqueCardID)
    {
        List<MinionBuffData> buffDataList = GetBuffDataList();
        bool isJoin = false;

        foreach (MinionBuffData buffData in buffDataList)
        {
            if (buffData != null)
            {
                MinionData minion;
                if (GameManager.Instance.FindMinionBattleCard(uniqueCardID, out minion))
                {
                    if (minion.IsAlive)
                    {
                        GameManager.Instance.RequestRemoveMinionBuff(uniqueCardID, buffData.Key, isJoin);
                        isJoin = true;
                    }
                }
            }
        }
    }

    protected void RemoveLink(int uniqueCardID)
    {
        GameManager.Instance.ActionSetActiveLinkMinion(uniqueCardID, false);
        MinionData minion;
        if (GameManager.Instance.FindMinionBattleCard(uniqueCardID, out minion))
        {
            if (_currentSlotList.Contains(minion.SlotID))
            {
                _currentSlotList.Remove(minion.SlotID);
            }
        }
    }
    #endregion
}
#endregion

#region DoMinionBerserk Class
public class DoMinionBerserk : DoMinionPassiveBase
{
    #region Enums
    private enum ParamOrder
    {
        Minion = 0
        , Condition = 1
        , BuffDataList = 2
    }
    #endregion

    #region Constructors
    public DoMinionBerserk(DataParam param, Requester requester) : base(param, requester)
    {
    }

    ~DoMinionBerserk()
    {
        //DeinitAura();
    }
    #endregion

    #region Methods
    protected override List<int> GetTargetIDList()
    {
        bool isSuccess = true;
        DataParam[] param = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[(int)ParamOrder.Minion], DataParam.ParamType.MinionList, out param[0]);

        if (isSuccess)
        {
            List<MinionData> targetList;
            isSuccess &= MinionParser.ParamToMinionList(param[0], Requester, out targetList);

            if (isSuccess)
            {
                List<int> targetIDList = new List<int>();

                foreach (MinionData card in targetList)
                {
                    if (card.ActiveZone == CardZone.Battlefield && card.IsAlive)
                    {
                        targetIDList.Add(card.UniqueID);
                    }
                }

                return targetIDList;
            }
        }

        return new List<int>();
    }

    protected override List<MinionBuffData> GetBuffDataList()
    {
        bool isSuccess = true;
        DataParam param;
        MinionBuffData buff = null;
        List<MinionBuffData> buffList = new List<MinionBuffData>();
        List<string> buffTextList = new List<string>();

        for (int i = (int)ParamOrder.BuffDataList; i < Param.Parameters.Count; i++)
        {
            buffTextList.Add(Param.Parameters[i]);
        }

        foreach (string buffText in buffTextList)
        {
            isSuccess &= DataParam.TextToDataParam(buffText, DataParam.ParamType.Buff, out param);
            if (isSuccess)
            {
                isSuccess &= MinionBuffParser.ParamToMinionBuff(UniqueID, MinionBuffOrigins.BERSERKBUFF, BuffCounterType.None, -1, param, Requester, out buff);
                if (isSuccess)
                {
                    buffList.Add(buff);
                }
            }
        }

        return buffList;
    }

    protected override void InitPassive()
    {
        // Subscribe Event
        if (Requester.Type == Requester.RequesterType.Minion)
        {
            MinionData card = Requester.Minion;

            // Spawning
            //card.SubscribeEvent(EventKey.Awaken, InitBuff);
            card.SubscribeEvent(EventKey.Rally, UpdatePassive);
            card.SubscribeEvent(EventKey.Sentinel, UpdatePassive);
            card.SubscribeEvent(EventKey.PostBattlePhase, UpdatePassive);

            // Moving
            card.SubscribeEvent(EventKey.OnMeMoved, UpdatePassive);
            card.SubscribeEvent(EventKey.OnFriendMoved, UpdatePassive);
            card.SubscribeEvent(EventKey.OnEnemyMoved, UpdatePassive);

            // Dead
            card.SubscribeEvent(EventKey.OnMeExitBattlefield, ClearPassive);
            card.SubscribeEvent(EventKey.OnFriendExitBattlefield, UpdatePassive);
            card.SubscribeEvent(EventKey.OnEnemyExitBattlefield, UpdatePassive);

            // Hp Change
            card.SubscribeEvent(EventKey.OnMeDamaged, UpdatePassive);
            card.SubscribeEvent(EventKey.OnMeHPIncrease, UpdatePassive);

            // Buff
            card.SubscribeEvent(EventKey.OnMeBeBuffed, UpdatePassive);
            card.SubscribeEvent(EventKey.OnFriendChangeDir, UpdatePassive);
            card.SubscribeEvent(EventKey.OnEnemyChangeDir, UpdatePassive);
        }

        UpdatePassive(null);
    }

    protected override void DeinitPassive()
    {
        // Unsubscribe Event
        if (Requester.Type == Requester.RequesterType.Minion)
        {
            MinionData card = Requester.Minion;

            // Spawning
            card.UnsubscribeEvent(EventKey.Awaken, UpdatePassive);
            card.UnsubscribeEvent(EventKey.Rally, UpdatePassive);
            card.UnsubscribeEvent(EventKey.Sentinel, UpdatePassive);
            card.UnsubscribeEvent(EventKey.PostBattlePhase, UpdatePassive);

            // Moving
            card.UnsubscribeEvent(EventKey.OnMeMoved, UpdatePassive);
            card.UnsubscribeEvent(EventKey.OnFriendMoved, UpdatePassive);
            card.UnsubscribeEvent(EventKey.OnEnemyMoved, UpdatePassive);

            // Dead
            card.UnsubscribeEvent(EventKey.OnMeExitBattlefield, ClearPassive);
            card.UnsubscribeEvent(EventKey.OnFriendExitBattlefield, UpdatePassive);
            card.UnsubscribeEvent(EventKey.OnEnemyExitBattlefield, UpdatePassive);

            // Hp Change
            card.UnsubscribeEvent(EventKey.OnMeDamaged, UpdatePassive);
            card.UnsubscribeEvent(EventKey.OnMeHPIncrease, UpdatePassive);

            // Buff
            card.UnsubscribeEvent(EventKey.OnMeChangeDir, UpdatePassive);
            card.UnsubscribeEvent(EventKey.OnFriendChangeDir, UpdatePassive);
            card.UnsubscribeEvent(EventKey.OnEnemyChangeDir, UpdatePassive);
        }

        ClearPassive(null);
    }

    public override void UpdatePassive(Requester triggerer)
    {
        // Check in battle phase.
        if (GameManager.Instance.Phase == GamePhase.Battle)
        {
            return;
        }

        if (
                Requester.Minion != null
            && (Requester.Minion.ActiveZone != CardZone.Battlefield
            || (Requester.Minion.IsDead || Requester.Minion.IsBeSilence)) // dead or silence
        )
        {
            ClearPassive(null);
            return;
        }

        // become full health.
        if (Requester.Minion != null && Requester.Minion.HPCurrent >= Requester.Minion.HPMax)
        {
            ClearPassive(null);
            return;
        }

        // Update Berserk Feedback
        if (!Requester.Minion.IsBerserkActive)
        {
            GameManager.Instance.ActionSetBerserkMinion(Requester.Minion.UniqueID, true);
        }

        // Get new terget list.
        List<int> toActivateList = GetTargetIDList();
        List<int> toDeactivateList = new List<int>();
        List<int> activatedList = new List<int>();

        List<int> currentList = new List<int>(_currentList);

        bool isSuccess = true;
        bool isCanDoAura = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[(int)ParamOrder.Condition], DataParam.ParamType.Boolean, out p[0]);
        if (isSuccess)
        {
            isSuccess &= BooleanParser.ParamToBoolean(p[0], Requester, out isCanDoAura);
        }

        if (!isSuccess || !isCanDoAura)
        {
            ClearPassive(null);
            return;
        }

        // Find and remove old target.
        for (int i = 0; i < currentList.Count; ++i)
        {
            for (int j = 0; j < toActivateList.Count; ++j)
            {
                if (currentList[i] == toActivateList[j])
                {
                    // Already active. 
                    // Update buff.
                    UpdateBuff(currentList[i]);

                    // Remove from to activate list
                    activatedList.Add(currentList[i]);

                    currentList.RemoveAt(i); --i;
                    toActivateList.RemoveAt(j);
                    break;
                }
            }
        }

        // Remaining ID in current list is not in to-active List.
        // Will be put in deactivate list.
        toDeactivateList = new List<int>(currentList);

        // Start activate list.
        for (int i = 0; i < toActivateList.Count; ++i)
        {
            // To Activate

            // Add Buff by Unique ID
            AddBuff(toActivateList[i]);

            // Add to activated list.
            activatedList.Add(toActivateList[i]);
        }

        // Start deactivate list.
        for (int i = 0; i < toDeactivateList.Count; ++i)
        {
            // To Deactivate

            // Remove Buff by Unique ID
            RemoveBuff(toDeactivateList[i]);
        }

        // Save activated list to current list.
        _currentList = new List<int>(activatedList);
    }

    protected override void ClearPassive(Requester triggerer)
    {
        for (int i = 0; i < _currentList.Count; ++i)
        {
            // To Deactivate

            // Remove Buff by Unique ID
            RemoveBuff(_currentList[i]);
        }

        if (Requester.Minion.IsBerserkActive)
        {
            GameManager.Instance.ActionSetBerserkMinion(Requester.Minion.UniqueID, false);
        }
        _currentList.Clear();

        _state = PassiveState.Init;
    }
    #endregion
}
#endregion

#region DoAddMinionBuff Class
public class DoAddMinionBuff : EffectData
{
    #region Private Properties
    private BuffCounterType _counterType = BuffCounterType.None;
    private int _counterAmount = -1;
    private List<MinionData> _targetList;
    #endregion

    #region Constructors
    public DoAddMinionBuff(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Action(UnityAction onComplete)
    {
        bool isSuccess = true;
        DataParam[] param = new DataParam[3];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.String, out param[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.Integer, out param[1]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[2], DataParam.ParamType.MinionList, out param[2]);
        if (isSuccess)
        {
            string counterType;
            isSuccess &= StringParser.ParamToString(param[0], Requester, out counterType);
            isSuccess &= IntParser.ParamToInt(param[1], Requester, out _counterAmount);
            isSuccess &= MinionParser.ParamToMinionList(param[2], Requester, out _targetList);
            isSuccess &= GameHelper.StrToEnum(counterType, out _counterType); // parse counterType string to enum

            if (isSuccess)
            {
                List<CommandData> commandList = new List<CommandData>();
                for (int i = 0; i < _targetList.Count; ++i)
                {
                    List<MinionBuffData> buffDataList = CreateBuffList();
                    if (buffDataList != null && buffDataList.Count > 0)
                    {
                        foreach (MinionBuffData buffData in buffDataList)
                        {
                            if (buffData != null)
                            {
                                AddMinionBuffCommand cmd = new AddMinionBuffCommand(_targetList[i].UniqueID, buffData);
                                commandList.Add(cmd);
                            }
                        }
                    }
                }

                if (Requester.Ability != null)
                {
                    Requester.Ability.AddCommandData(commandList);
                }
                else
                {
                    Debug.LogErrorFormat("EffectData Error: Ability not found on {0}", Param.Key);
                }
            }
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    protected List<MinionBuffData> CreateBuffList()
    {
        List<MinionBuffData> buffList = new List<MinionBuffData>();

        bool isSuccess = true;
        DataParam param;
        MinionBuffData buff = null;
        string buffText;

        for (int i = 3; i < Param.Parameters.Count; i++)  // From 3rd index is buff script.
        {
            buffText = Param.Parameters[i];
            isSuccess &= DataParam.TextToDataParam(buffText, DataParam.ParamType.Buff, out param);
            if (isSuccess)
            {
                isSuccess &= MinionBuffParser.ParamToMinionBuff(
                      UniqueID, MinionBuffOrigins.ADDBUFF
                    , _counterType
                    , _counterAmount
                    , param
                    , Requester
                    , out buff
                );

                if (isSuccess && buff != null)
                {
                    buffList.Add(buff);
                }
            }
        }

        return buffList;
    }
    #endregion
}
#endregion

#region DoRemoveMinionBuff Class
public class DoRemoveMinionBuff : EffectData
{
    #region Private Properties
    private List<MinionData> _targetList;
    private MinionBuffTypes _type;
    #endregion

    #region Constructors
    public DoRemoveMinionBuff(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Action(UnityAction onComplete)
    {
        bool isSuccess = true;
        DataParam[] param = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.MinionList, out param[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.String, out param[1]);
        if (isSuccess)
        {
            string type;
            isSuccess &= MinionParser.ParamToMinionList(param[0], Requester, out _targetList);
            isSuccess &= StringParser.ParamToString(param[1], Requester, out type);
            isSuccess &= GameHelper.StrToEnum(type, out _type);

            List<CommandData> commandList = new List<CommandData>();
            if (isSuccess)
            {
                for (int i = 0; i < _targetList.Count; ++i)
                {
                    commandList.Add(new RemoveMinionBuffTypeCommand(_targetList[i].UniqueID, _type));
                }

                if (Requester.Ability != null)
                {
                    Requester.Ability.AddCommandData(commandList);
                }
                else
                {
                    Debug.LogErrorFormat("EffectData Error: Ability not found on {0}", Param.Key);
                }

                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            }
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoAddSlotBuff Class
public class DoAddSlotBuff : EffectData
{
    #region Private Properties
    private BuffCounterType _counterType = BuffCounterType.None;
    private int _counterAmount = -1;
    private List<SlotData> _targetList;
    #endregion

    #region Constructors
    public DoAddSlotBuff(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Action(UnityAction onComplete)
    {
        //Debug.LogFormat("DoAddSlotBuff: {0}", Param.ToText());

        bool isSuccess = true;
        DataParam[] param = new DataParam[3];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.String, out param[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.Integer, out param[1]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[2], DataParam.ParamType.SlotList, out param[2]);
        if (isSuccess)
        {
            string counterType;
            isSuccess &= StringParser.ParamToString(param[0], Requester, out counterType);
            isSuccess &= IntParser.ParamToInt(param[1], Requester, out _counterAmount);
            isSuccess &= SlotParser.ParamToSlotList(param[2], Requester, out _targetList);
            isSuccess &= GameHelper.StrToEnum(counterType, out _counterType); // parse counterType string to enum

            if (isSuccess)
            {
                List<CommandData> commandList = new List<CommandData>();
                foreach (SlotData slot in _targetList)
                {
                    List<SlotBuffData> buffDataList = CreateBuffList();
                    foreach (SlotBuffData buffData in buffDataList)
                    {
                        if (buffData != null)
                        {
                            commandList.Add(new AddSlotBuffCommand(slot.SlotID, buffData));
                        }
                    }
                }

                if (Requester.Ability != null)
                {
                    Requester.Ability.AddCommandData(commandList);
                }
                else
                {
                    Debug.LogErrorFormat("EffectData Error: Ability not found on {0}", Param.Key);
                }
            }
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    protected List<SlotBuffData> CreateBuffList()
    {
        List<SlotBuffData> buffList = new List<SlotBuffData>();

        bool isSuccess = true;
        DataParam param;
        SlotBuffData buff = null;
        string buffText;

        for (int i = 3; i < Param.Parameters.Count; i++)  // From 3rd index is buff script.
        {
            buffText = Param.Parameters[i];
            isSuccess &= DataParam.TextToDataParam(buffText, DataParam.ParamType.Buff, out param);
            if (isSuccess)
            {
                isSuccess &= SlotBuffParser.ParamToSlotBuff(
                      string.Format("{0}_{1}", param.Key, UniqueID)
                    , _counterType
                    , _counterAmount
                    , param
                    , Requester
                    , out buff
                );

                if (isSuccess && buff != null)
                {
                    buffList.Add(buff);
                }
            }
        }

        return buffList;
    }
    #endregion
}
#endregion

#region DoRemoveSlotBuff Class
public class DoRemoveSlotBuff : EffectData
{
    #region Private Properties
    private List<SlotData> _targetList;
    private SlotBuffTypes _type;
    #endregion

    #region Constructors
    public DoRemoveSlotBuff(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Action(UnityAction onComplete)
    {
        bool isSuccess = true;
        DataParam[] param = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.SlotList, out param[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.String, out param[1]);
        if (isSuccess)
        {
            string type;
            isSuccess &= SlotParser.ParamToSlotList(param[0], Requester, out _targetList);
            isSuccess &= StringParser.ParamToString(param[1], Requester, out type);
            isSuccess &= GameHelper.StrToEnum(type, out _type); // parse counterType string to enum

            if (isSuccess)
            {
                List<CommandData> commandList = new List<CommandData>();
                foreach (SlotData slot in _targetList)
                {
                    commandList.Add(new RemoveSlotBuffTypeCommand(slot.SlotID, _type));
                }

                if (Requester.Ability != null)
                {
                    Requester.Ability.AddCommandData(commandList);
                }
                else
                {
                    Debug.LogErrorFormat("EffectData Error: Ability not found on {0}", Param.Key);
                }
            }
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }
    #endregion
}
#endregion

#region DoModifySlotBuffCounter Class
public class DoModifySlotBuffCounter : EffectData
{
    #region Private Properties
    private BuffCounterType _counterType = BuffCounterType.None;
    private int _counterAmount = 0;
    private List<SlotData> _targetList;
    #endregion

    #region Constructors
    public DoModifySlotBuffCounter(DataParam param, Requester requester) : base(param, requester)
    {
    }
    #endregion

    #region Methods
    public override void Action(UnityAction onComplete)
    {
        bool isSuccess = true;
        DataParam[] param = new DataParam[3];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.String, out param[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.Integer, out param[1]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[2], DataParam.ParamType.SlotList, out param[2]);
        if (isSuccess)
        {
            string counterType;
            isSuccess &= StringParser.ParamToString(param[0], Requester, out counterType);
            isSuccess &= IntParser.ParamToInt(param[1], Requester, out _counterAmount);
            isSuccess &= SlotParser.ParamToSlotList(param[2], Requester, out _targetList);
            isSuccess &= GameHelper.StrToEnum(counterType, out _counterType); // parse counterType string to enum

            if (isSuccess)
            {
                List<CommandData> commandList = new List<CommandData>();
                foreach (SlotData slot in _targetList)
                {
                    List<string> buffKeyList = GetBuffKeyList();
                    foreach (SlotBuffData buff in slot.BuffDataList)
                    {
                        if (buffKeyList.Contains(buff.Key.Split('_')[0])
                            && buff.CounterType == _counterType)
                        {
                            buff.SetCountAmountDelta(_counterAmount);
                            commandList.Add(new BuffSlotLockCommand(slot.SlotID, buff.CounterAmount, buff.Requester.PlayerIndex, true));
                        }
                    }
                }

                if (Requester.Ability != null)
                {
                    Requester.Ability.AddCommandData(commandList);
                }
                else
                {
                    Debug.LogErrorFormat("EffectData Error: Ability not found on {0}", Param.Key);
                }

                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            }
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    protected List<string> GetBuffKeyList()
    {
        List<string> buffKeyList = new List<string>();

        bool isSuccess = true;
        DataParam param;
        string buffText;

        for (int i = 3; i < Param.Parameters.Count; i++)  // From 3rd index is buff script.
        {
            buffText = Param.Parameters[i];
            isSuccess &= DataParam.TextToDataParam(buffText, DataParam.ParamType.Buff, out param);
            if (isSuccess)
            {
                buffKeyList.Add(param.Key);
            }
        }

        return buffKeyList;
    }
    #endregion
}
#endregion