﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct FriendStatusData
{
    public FriendStatus Status { get; private set; }
    public DateTime TimestampUTC { get; private set; }

    public FriendStatusData(string dataStr)
    {
        Dictionary<string, string> rawData = JsonConvert.DeserializeObject<Dictionary<string, string>>(dataStr);

        TimestampUTC = Convert.ToDateTime(rawData["date"]).ToUniversalTime();
        Status = FriendStatus.None;
        Status = ConvertToFriendStatus(rawData["status"]);
    }

    public FriendStatusData(FriendStatus friendStatus)
    {
        TimestampUTC = DateTimeData.GetDateTimeUTC();
        Status = friendStatus;
    }

    private FriendStatus ConvertToFriendStatus(string status)
    {
        switch (status)
        {
            case "sender":
                return FriendStatus.Request_Sender;

            case "receiver":
                return FriendStatus.Request_Receiver;

            case "friend":
                return FriendStatus.Friend;
        }

        return FriendStatus.None;
    }

    public bool IsExpired()
    {
        if(Status != FriendStatus.Request_Receiver
           || Status != FriendStatus.Request_Sender)
        {
            return false;
        }

        return (DateTimeData.GetDateTimeUTC() - TimestampUTC).TotalDays >= 7;
    }
}

public class FriendInfoData
{
    #region Public Properties
    public PlayerInfoData PlayerInfo { get; private set; }
    public FriendStatusData StatusData { get; private set; }
    #endregion

    #region Contructors
    public FriendInfoData(PlayerInfoData playerInfo, FriendStatusData statusData)
    {
        PlayerInfo = new PlayerInfoData(playerInfo);
        StatusData = statusData;
    }

    public FriendInfoData(FriendInfoData data)
    {
        PlayerInfo = new PlayerInfoData(data.PlayerInfo);
        StatusData = data.StatusData;
    }
    #endregion

    #region Methods
    public void SetFriendStatus(FriendStatusData statusData)
    {
        StatusData = statusData;
    }

    public override string ToString()
    {
        if (PlayerInfo != null)
        {
            return string.Format("{0}{1}({2}:{3})", PlayerInfo.PlayerID, PlayerInfo.DisplayName, StatusData.Status, StatusData.TimestampUTC);
        }
        else
        {
            return null;
        }
    }
    #endregion
}
