﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Newtonsoft.Json;

[Serializable]
public class BannerData
{
    public string ImagePath { get; private set; }
    public string FunctionName { get; private set; }
    public DateTime ReleaseDate { get; private set; }
    public DateTime EndDate { get; private set; }

    public string CustomData { get; private set; }
    public bool IsValidDate
    {
        get
        {
            return DateTimeData.GetDateTimeUTC() > ReleaseDate && DateTimeData.GetDateTimeUTC() < EndDate;
        }
    }

    public BannerData(string json)
    {
        Dictionary<string, string> rawData = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
        ImagePath = rawData["image_path"];
        FunctionName = rawData["function"];
        CustomData = rawData.ContainsKey("custom_data") ? rawData["custom_data"] : null;
        ReleaseDate = rawData.ContainsKey("release_date") ? Convert.ToDateTime(rawData["release_date"]).ToUniversalTime() : DateTime.MinValue;
        EndDate = rawData.ContainsKey("end_date") ? Convert.ToDateTime(rawData["end_date"]).ToUniversalTime() : DateTime.MaxValue;
    }
}
