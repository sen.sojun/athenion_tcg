﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

#region Global Structs
public struct HPStatData
{
    public HPModifyKey Key;
    public int Value;
    public bool IsBattle;

    public HPStatData(HPModifyKey key, int value, bool isBattle = false)
    {
        Key = key;
        Value = value;
        IsBattle = isBattle;
    }
}

public struct ATKStatData
{
    public ATKModifyKey Key;
    public int Value;

    public ATKStatData(ATKModifyKey key, int value)
    {
        Key = key;
        Value = value;
    }
}

public struct SpiritStatData
{
    public SpiritModifyKey Key;
    public int Value;

    public SpiritStatData(SpiritModifyKey key, int value)
    {
        Key = key;
        Value = value;
    }
}

public struct ArmorStatData
{
    public ArmorModifyKey Key;
    public int Value;

    public ArmorStatData(ArmorModifyKey key, int value)
    {
        Key = key;
        Value = value;
    }
}
#endregion

#region Global Enums
public enum HPModifyKey
{
    BOOST_DELTA
    , SET_ALL_TO
    , SET_CURRENT_TO
    , SET_CURRENT_DELTA
    , SET_MAX_TO
    , SET_MAX_DELTA
    , SET_DEFAULT_TO
    , SET_DEFAULT_DELTA
}

public enum ATKModifyKey
{
    BOOST_DELTA
    , SET_TO
    , SET_DELTA
}

public enum SpiritModifyKey
{
    BOOST_DELTA
    , SET_TO
    , SET_DELTA
}

public enum ArmorModifyKey
{
    BOOST_DELTA
    , SET_TO
    , SET_DELTA
}
#endregion

#region BattleCardData
public abstract class BattleCardData
{
    #region Public Properties
    public int UniqueID { get; protected set; }
    public int SlotID { get { return _slotID; } }
    public int SpawnedSlotID { get { return _spawnedSlotID; } }

    public CardData RawData { get { return _data; } }
    public string BaseID { get { if (_data != null) return _data.BaseID; else return ""; } }
    public string FullID { get { if (_data != null) return _data.FullID; else return ""; } }
    public string VariantID { get { if (_data != null) return _data.VariantID; else return ""; } }
    public string Name { get { if (_data != null) return _data.Name; else return ""; } }
    public string FlavorText { get { if (_data != null) return _data.FlavorText; else return ""; } }
    public List<AbilityFeedback> FeedbackList { get { if (_data != null) { return _data.FeedbackList; } else { return new List<AbilityFeedback>(); } } }
    public virtual string Description { get { return GetDescription(); } }
    public string ImageKey { get { if (_data != null) return _data.ImageKey; else return ""; } }
    public string CardBackID { get; protected set; }
    public CardRarity Rarity { get { if (_data != null) return _data.Rarity; else return CardRarity.None; } }
    public CardType Type { get { if (_data != null) return _data.Type; else return CardType.Minion; } }
    public CardElement Element { get { if (_data != null) return _data.CardElement; else return null; } }
    public string SeriesKey { get { if (_data != null) return _data.SeriesKey; else return ""; } }
    public string InfoScriptKey { get { if (_data != null) return _data.InfoScriptKey; else return ""; } }
    public bool IsFoil { get { if (_data != null) return _data.IsFoil; else return false; } }

    public Dictionary<int, CardAbilityData> AbilityList { get { return _abilityList; } }

    public PlayerIndex Owner { get; protected set; }
    public CardZone CurrentZone { get; protected set; }
    public CardZone ActiveZone { get; protected set; }

    public int EncodeKey { get { try { return GameManager.Instance.Data.PlayerDataList[(int)Owner].EncodeKey; } catch { return 0; } } }
    #endregion

    #region Protected Properties
    protected CardData _data;
    protected int _slotID = -1;
    protected int _spawnedSlotID = -1;
    protected static int _uniqueID = 0;
    protected Dictionary<int, CardAbilityData> _abilityList;
    #endregion

    #region Private Properties
    #endregion

    #region Methods
    public static void ResetUniqueID()
    {
        _uniqueID = 0;
    }

    public void SetCardBackID(string cardBackID)
    {
        CardBackID = cardBackID;
    }

    public void SetOwner(PlayerIndex owner)
    {
        Owner = owner;
    }

    public void SetCurrentZone(CardZone zone)
    {
        CurrentZone = zone;
    }

    public void SetActiveZone(CardZone zone)
    {
        ActiveZone = zone;
    }

    public void SetSlotID(int slotID)
    {
        _slotID = slotID;
    }

    public void SetSpawnedSlotID(int slotID)
    {
        _spawnedSlotID = slotID;
    }

    public void ResetSlotID()
    {
        _slotID = -1;
    }

    public bool IsCanActiveChain()
    {
        //bool isChain_1 = false;
        //bool isChain_2 = false;
        //bool isChain_3 = false;

        //foreach (KeyValuePair<int, CardAbilityData> ability in AbilityList)
        //{
        //    switch (ability.Value.TriggerKey)
        //    {
        //        case EventKey.Chain_1: isChain_1 = true; break;
        //        case EventKey.Chain_2: isChain_2 = true; break;
        //        case EventKey.Chain_3: isChain_3 = true; break;
        //    }
        //}

        //if (isChain_1 && GameManager.Instance.PlayCardCount >= 1) return true;
        //if (isChain_2 && GameManager.Instance.PlayCardCount >= 2) return true;
        //if (isChain_3 && GameManager.Instance.PlayCardCount >= 3) return true;

        //return false;

        List<int> chainNumList = new List<int>();
        string triggerKey = "";
        string[] triggerSplit;

        foreach (KeyValuePair<int, CardAbilityData> ability in AbilityList)
        {
            triggerKey = ability.Value.TriggerKey.ToString();
            triggerSplit = triggerKey.Split('_');
            if (triggerSplit[0] == "Chain")
            {
                chainNumList.Add(int.Parse(triggerSplit[1]));
            }
        }

        foreach(int chainNum in chainNumList)
        {
            if(GameManager.Instance.PlayCardCount >= chainNum)
            {
                return true;
            }
        }

        return false;
    }

    public bool IsCanActiveChain(int chainIndex)
    {
        //if (chainIndex <= GameManager.Instance.PlayCardCount)
        //{
        //    bool isChain_1 = false;
        //    bool isChain_2 = false;
        //    bool isChain_3 = false;

        //    foreach (KeyValuePair<int, CardAbilityData> ability in AbilityList)
        //    {
        //        switch (ability.Value.TriggerKey)
        //        {
        //            case EventKey.Chain_1: isChain_1 = true; break;
        //            case EventKey.Chain_2: isChain_2 = true; break;
        //            case EventKey.Chain_3: isChain_3 = true; break;
        //        }
        //    }

        //    if (isChain_1 && chainIndex == 1) return true;
        //    if (isChain_2 && chainIndex == 2) return true;
        //    if (isChain_3 && chainIndex == 3) return true;
        //}

        //return false;

        if (chainIndex <= GameManager.Instance.PlayCardCount)
        {
            List<int> chainNumList = new List<int>();
            string triggerKey = "";
            string[] triggerSplit;

            foreach (KeyValuePair<int, CardAbilityData> ability in AbilityList)
            {
                triggerKey = ability.Value.TriggerKey.ToString();
                triggerSplit = triggerKey.Split('_');
                if (triggerSplit[0] == "Chain")
                {
                    chainNumList.Add(int.Parse(triggerSplit[1]));
                }
            }

            if (chainNumList.Contains(chainIndex))
            {
                return true;
            }
        }

        return false;
    }

    public virtual string GetDescription()
    {
        string description = "";

        if (_data != null)
        {
            description = _data.GetDescription();

        }

        return description;
    }

    public static BattleCardData CreateBattleCard(string fullID, string cardBackID)
    {
        CardData cardData = CardData.CreateCard(fullID);
        if (cardData != null)
        {
            return CreateBattleCard(cardData, cardBackID);
        }
        else
        {
            Debug.LogErrorFormat("BattleCardData/CreateBattleCard: Failed to create card {0}", fullID);
        }
        return null;
    }

    private static BattleCardData CreateBattleCard(CardData cardData, string cardBackID)
    {
        BattleCardData battleCardData = null;
        switch (cardData.Type)
        {
            case CardType.Minion:
            case CardType.Minion_NotInDeck:
                {
                    battleCardData = MinionData.CreateMinion(cardData, cardBackID);
                }
                break;

            case CardType.Spell:
            case CardType.Spell_NotInDeck:
                {
                    battleCardData = SpellData.CreateSpell(cardData, cardBackID);
                }
                break;
        }

        return battleCardData;
    }
    #endregion

    #region Ability Methods
    public virtual void InitAbility()
    {
        _abilityList = new Dictionary<int, CardAbilityData>();

        CardAbilityData ability = null;
        foreach (CardAbilityDBData rawData in _data.AbilityList)
        {
            //Debug.Log("InitAbility " + rawData.ID);
            if (CardAbilityData.CreateAbility(rawData.ID, this, out ability))
            {
                //Debug.Log("InitAbility Add");
                _abilityList.Add(ability.UniqueID, ability);
            }
        }
    }

    public void ActiveAbility(int uniqueAbilityID, Dictionary<string, string> paramList, UnityAction onActionComplete)
    {
        if (_abilityList.ContainsKey(uniqueAbilityID))
        {
            _abilityList[uniqueAbilityID].TryAction(paramList, onActionComplete);
        }
        else
        {
            if (onActionComplete != null)
            {
                onActionComplete.Invoke();
            }
        }
    }

    public List<AbilityKeyword> GetAbilityKeywordList()
    {
        List<AbilityKeyword> result = new List<AbilityKeyword>();

        if (RawData != null)
        {
            return RawData.GetAbilityKeywordList();
        }

        return result;
    }

    private void AddAbilityKeyword(ref List<AbilityKeyword> abilityKeywordList, AbilityKeyword abilityKeyword)
    {
        if (abilityKeywordList == null)
        {
            abilityKeywordList = new List<AbilityKeyword>();
        }

        if (!abilityKeywordList.Contains(abilityKeyword))
        {
            abilityKeywordList.Add(abilityKeyword);
        }
    }

    public virtual void ResetToDefault(bool isUpdateUI = true)
    {
    }

    public virtual bool IsKeySubscribed(EventKey key)
    {
        return false;
    }
    #endregion
}
#endregion

#region MinionData
public class MinionData : BattleCardData
{
    #region Public Properties
    public override string Description { get { return this.GetDescription(); } }

    //public CardPassive CardPassive { get { return _cardPassive; } }
    public string SpawnEffectKey { get { if (_data != null) return _data.SpawnEffectKey; else return ""; } }
    public string AttackEffectKey { get { if (_data != null) return _data.AttackEffectKey; else return ""; } }
    public string DeathEffectKey { get { if (_data != null) return _data.DeathEffectKey; else return ""; } }

    // changeable data
    public CardDirection CardDir { get { return GetMinionDir(); } }
    public CardDirection CardDirBase { get { return _cardDirBase; } }

    public int HPMax { get { return HPMaxBase + HPMaxBuffPassive; } }
    public int HPDefault { get { return _GetHPDefault(); } }
    public int ATKDefault { get { return _GetATKDefault(); } }
    public int SpiritDefault { get { return _GetSpiritDefault(); } }
    public int ArmorDefault { get { return _GetArmorDefault(); } }

    public int HPCurrent { get { return GetMinionHP(); } }
    public int ATKCurrent { get { return GetMinionATK(); } }
    public int SpiritCurrent { get { return GetMinionSpirit(); } }
    public int ArmorCurrent { get { return GetMinionArmor(); } }

    public int HPMaxBase { get { return _GetHPMaxBase(); } }
    public int HPCurrentBase { get { return _GetHPCurrentBase(); } }
    public int ATKCurrentBase { get { return _GetATKCurrentBase(); } }
    public int SpiritCurrentBase { get { return _GetSpiritCurrentBase(); } }
    public int ArmorCurrentBase { get { return _GetArmorCurrentBase(); } }

    public int HPMaxBuffPassive { get { return _GetHPMaxBuffPassive(); } }
    public int HPBuffPassive { get { return _GetHPBuffPassive(); } }
    public int ATKBuffPassive { get { return _GetATKBuffPassive(); } }
    public int SpiritBuffPassive { get { return _GetSpiritBuffPassive(); } }
    public int ArmorBuffPassive { get { return _GetArmorBuffPassive(); } }

    // Encode Properties
    private int _hpMax_encode = 0;
    private int _hpDefault_encode = 0;
    private int _atkDefault_encode = 0;
    private int _spiritDefault_encode = 0;
    private int _armorDefault_encode = 0;

    private string _hpMax_encode_str = "";
    private string _hpDefault_encode_str = "";
    private string _atkDefault_encode_str = "";
    private string _spiritDefault_encode_str = "";
    private string _armorDefault_encode_str = "";

    private int _hpMaxBase_encode = 0;
    private int _hpCurrentBase_encode = 0;
    private int _atkCurrentBase_encode = 0;
    private int _spiritCurrentBase_encode = 0;
    private int _armorCurrentBase_encode = 0;

    private string _hpMaxBase_encode_str = "";
    private string _hpCurrentBase_encode_str = "";
    private string _atkCurrentBase_encode_str = "";
    private string _spiritCurrentBase_encode_str = "";
    private string _armorCurrentBase_encode_str = "";

    private int _hpMaxBuffPassive_encode = 0;
    private int _hpBuffPassive_encode = 0;
    private int _atkBuffPassive_encode = 0;
    private int _spiritBuffPassive_encode = 0;
    private int _armorBuffPassive_encode = 0;

    private string _hpMaxBuffPassive_encode_str = "";
    private string _hpBuffPassive_encode_str = "";
    private string _atkBuffPassive_encode_str = "";
    private string _spiritBuffPassive_encode_str = "";
    private string _armorBuffPassive_encode_str = "";

    // battle data
    public bool IsCanAttack { get { return (IsAlive && !IsBeFreeze && (IsMinion || IsHeroSkill)); } }
    public bool IsCanMove { get { return (IsAlive && IsMinion); } }

    public bool IsDead { get { return !IsAlive; } }
    public bool IsAlive { get { if ((CurrentZone == CardZone.Battlefield || CurrentZone == CardZone.Hero) && HPCurrent > 0 && (IsMinion || IsHeroSkill)) return true; return false; } }

    public bool IsBeFear { get { return IsMinionStatus(CardStatusType.Be_Fear); } }
    public bool IsBeFreeze { get { return IsMinionStatus(CardStatusType.Be_Freeze); } }
    public bool IsBeSilence { get { return IsMinionStatus(CardStatusType.Be_Silence); } }
    public bool IsBeLink { get { return IsMinionStatus(CardStatusType.Be_Link); } }
    public bool IsBeTaunt { get { return IsMinionStatus(CardStatusType.Be_Taunt); } }
    public bool IsBeAura { get { return IsMinionStatus(CardStatusType.Be_Aura); } }
    public bool IsBeDarkMatter { get { return IsMinionStatus(CardStatusType.Be_DarkMatter); } }
    public bool IsBeStealth { get { return IsMinionStatus(CardStatusType.Be_Stealth); } }
    public bool IsBeInvincible { get { return IsMinionStatus(CardStatusType.Be_Invincible); } }
    public bool IsBeUntargetable { get { return IsMinionStatus(CardStatusType.Be_Untargetable); } }
    public bool IsBeEffectInvincible { get { return IsMinionStatus(CardStatusType.Be_EffectInvincible); } }
    public bool IsBePiercing { get { return IsMinionStatus(CardStatusType.Be_Piercing); } }
    public bool IsBeOverrideEffectDamage { get { return IsMinionStatus(CardStatusType.Be_OverrideEffectDamage); } }
    public int PiercingRange { get { return GetPiercingRange(); } }
    public int OverrideEffectDamage { get { return GetOverrideEffectDamage(); } }

    public bool IsTauntActive { get { return IsMinionStatus(CardStatusType.TauntActive); } }
    public bool IsAuraActive { get { return IsMinionStatus(CardStatusType.AuraActive); } }
    public bool IsBerserkActive { get { return IsMinionStatus(CardStatusType.BerserkActive); } }

    public bool IsAbilityTaunt { get { if (FeedbackList != null) return FeedbackList.Contains(AbilityFeedback.Taunt); else return false; } }
    public bool IsAbilityLink { get { if (FeedbackList != null) return FeedbackList.Contains(AbilityFeedback.Link); else return false; } }
    public bool IsAbilityAura { get { if (FeedbackList != null) return FeedbackList.Contains(AbilityFeedback.Aura); else return false; } }
    public bool IsAbilityBerserk { get { if (FeedbackList != null) return FeedbackList.Contains(AbilityFeedback.Berserk); else return false; } }
    public bool IsAbilityBackstab { get { if (FeedbackList != null) return FeedbackList.Contains(AbilityFeedback.Backstab); else return false; } }
    public bool IsAbilityLastwish { get { if (FeedbackList != null) return FeedbackList.Contains(AbilityFeedback.Lastwish); else return false; } }
    public bool IsAbilityDarkPower { get { if (FeedbackList != null) return FeedbackList.Contains(AbilityFeedback.DarkPower); else return false; } }
    public bool IsAbilityChain { get { if (FeedbackList != null) return FeedbackList.Contains(AbilityFeedback.Chain); else return false; } }

    /*
    public bool IsFear { get { if (CardPassive.IsPassive(CardPassiveType.Fear)) return true; return false; } }
    public bool IsFreeze { get { if (CardPassive.IsPassive(CardPassiveType.Freeze)) return true; return false; } }
    public bool IsSilence { get { if (CardPassive.IsPassive(CardPassiveType.Silence)) return true; return false; } }
    public bool IsAbilityStealth { get { if (CardPassive.IsPassive(CardPassiveType.Stealth)) return true; return false; } }
    */

    public bool IsAlreadyLastWish { get; private set; }
    public bool IsAlreadySacrificed { get; private set; }
    public bool IsAlreadyDead { get; private set; }

    public bool IsMinion { get { if (Type == CardType.Minion || Type == CardType.Minion_NotInDeck) return true; else return false; } }
    public bool IsHeroSkill { get { if (Type == CardType.Hero) return true; else return false; } }
    public bool IsSpell { get { if (Type == CardType.Spell || Type == CardType.Spell_NotInDeck) return true; else return false; } }

    //public int LinkCount { get { return _linkCount; } }
    public List<MinionBuffData> BuffDataList { get { return _buffDataList; } }

    public PlayerIndex MinionKillerPlayerIndex { get; private set; }
    #endregion

    #region Private Properties
    private List<MinionBuffData> _buffDataList;
    private Dictionary<EventKey, List<int>> _triggerAbilityEvent;
    private Dictionary<EventKey, UnityEvent<Requester>> _triggerCallbackEvent;

    private CardDirection _cardDirBase;
    private Dictionary<int, CardDirection> _cardDirTaunted = new Dictionary<int, CardDirection>();
    private Dictionary<int, CardDirection> _cardDirBuff = new Dictionary<int, CardDirection>();

    private Dictionary<int, int> _piercingRangeList = new Dictionary<int, int>();
    private Dictionary<int, int> _overrideEffectDamageList = new Dictionary<int, int>();

    private CardStatus _cardStatus;

    private List<int> _attackedList;
    #endregion

    #region Contructor
    public MinionData(int uniqueID, CardData cardData, string cardBackID)
    {
        UniqueID = uniqueID;
        _buffDataList = new List<MinionBuffData>();
        _triggerCallbackEvent = new Dictionary<EventKey, UnityEvent<Requester>>();
        _attackedList = new List<int>();
        _cardStatus = new CardStatus();
        _cardDirTaunted = new Dictionary<int, CardDirection>();
        _cardDirBuff = new Dictionary<int, CardDirection>();

        IsAlreadyLastWish = false;
        IsAlreadySacrificed = false;
        IsAlreadyDead = false;

        if (cardData != null)
        {
            _data = cardData;
            _cardDirBase = new CardDirection(_data.CardDir);

            _SetHPDefault(_data.HP);
            _SetATKDefault(_data.ATK);
            _SetSpiritDefault(_data.Spirit);
            _SetArmorDefault(_data.Armor);

            _SetHPMaxBase(_data.HP);
            _SetHPCurrentBase(_data.HP);
            _SetATKCurrentBase(_data.ATK);
            _SetSpiritCurrentBase(_data.Spirit);
            _SetArmorCurrentBase(_data.Armor);

            _SetHPMaxBuffPassive(0);
            _SetHPBuffPassive(0);
            _SetATKBuffPassive(0);
            _SetSpiritBuffPassive(0);
            _SetArmorBuffPassive(0);
        }

        CardBackID = cardBackID;
    }

    public MinionData(MinionData data)
    {
        UniqueID = data.UniqueID;

        if (data != null)
        {
            IsAlreadyLastWish = false;
            IsAlreadySacrificed = false;
            IsAlreadyDead = false;
            _data = data._data;
            _buffDataList = new List<MinionBuffData>(data._buffDataList);
            _attackedList = new List<int>();
            _cardStatus = new CardStatus(data._cardStatus);

            _triggerCallbackEvent = new Dictionary<EventKey, UnityEvent<Requester>>(data._triggerCallbackEvent);
            _cardDirBase = new CardDirection(data._cardDirBase);
            _cardDirTaunted = new Dictionary<int, CardDirection>(data._cardDirTaunted);
            _cardDirBuff = new Dictionary<int, CardDirection>(data._cardDirBuff);

            _hpCurrentBase_encode = data._hpCurrentBase_encode;
            _hpCurrentBase_encode_str = data._hpCurrentBase_encode_str;
            _atkCurrentBase_encode = data._atkCurrentBase_encode;
            _atkCurrentBase_encode_str = data._atkCurrentBase_encode_str;
            _spiritCurrentBase_encode = data._spiritCurrentBase_encode;
            _spiritCurrentBase_encode_str = data._spiritCurrentBase_encode_str;
            _armorCurrentBase_encode = data._armorCurrentBase_encode;
            _armorCurrentBase_encode_str = data._armorCurrentBase_encode_str;

            _SetHPBuffPassive(0);
            _SetATKBuffPassive(0);
            _SetSpiritBuffPassive(0);
            _SetArmorBuffPassive(0);

            _hpMaxBase_encode = data._hpMaxBase_encode;
            _hpMaxBase_encode_str = data._hpMaxBase_encode_str;
            _hpMaxBuffPassive_encode = data._hpMaxBuffPassive_encode;
            _hpMaxBuffPassive_encode_str = data._hpMaxBuffPassive_encode_str;

            _hpDefault_encode = data._hpDefault_encode;
            _hpDefault_encode_str = data._hpDefault_encode_str;
            _atkDefault_encode = data._atkDefault_encode;
            _atkDefault_encode_str = data._atkDefault_encode_str;
            _spiritDefault_encode = data._spiritDefault_encode;
            _spiritDefault_encode_str = data._spiritDefault_encode_str;
            _armorDefault_encode = data._armorDefault_encode;
            _armorDefault_encode_str = data._armorDefault_encode_str;
        }

        CardBackID = data.CardBackID;
    }
    #endregion

    #region Methods
    public static MinionData CreateMinion(CardData cardData, string cardBackID)
    {
        MinionData minionData = new MinionData(_uniqueID++, cardData, cardBackID);

        return minionData;
    }

    #region Set Properties
    public override void ResetToDefault(bool isUpdateUI = true)
    {
        Debug.LogFormat("ResetToDefault: {0}:{1}:{2}", this.UniqueID, this.Name, ActiveZone);

        if (ActiveZone == CardZone.Battlefield)
        {
            ResetAllBuff();
            Dictionary<CardStatusType, int> statusDataList = new Dictionary<CardStatusType, int>(_cardStatus.StatusDataList);
            if (statusDataList != null && statusDataList.Count > 0)
            {
                foreach (KeyValuePair<CardStatusType, int> status in statusDataList)
                {
                    switch (status.Key)
                    {
                        case CardStatusType.Be_Fear:
                        case CardStatusType.Be_Silence:
                        case CardStatusType.Be_Freeze:
                        case CardStatusType.Be_Stealth:
                        case CardStatusType.Be_DarkMatter:
                        case CardStatusType.Be_Invincible:
                        case CardStatusType.Be_EffectInvincible:
                        case CardStatusType.Be_Untargetable:
                        case CardStatusType.Be_Piercing:
                        {
                            _cardStatus.RemoveAll(status.Key);
                        }
                        break;
                    }
                }
            }
        }
        else
        {
            ClearAllBuff();

            _triggerCallbackEvent = new Dictionary<EventKey, UnityEvent<Requester>>();
            _cardStatus = new CardStatus();

            _SetHPMaxBuffPassive(0);
            _SetHPBuffPassive(0);
            _SetATKBuffPassive(0);
            _SetSpiritBuffPassive(0);
            _SetArmorBuffPassive(0);

            _cardDirTaunted = new Dictionary<int, CardDirection>();
            _cardDirBuff = new Dictionary<int, CardDirection>();
        }

        if (RawData != null)
        {
            _data = RawData;
            _cardDirBase = new CardDirection(_data.CardDir);

            _SetHPCurrentBase(_data.HP);
            _SetATKCurrentBase(_data.ATK);
            _SetSpiritCurrentBase(_data.Spirit);
            _SetArmorCurrentBase(_data.Armor);

            _SetHPMaxBase(_data.HP);
            _SetHPDefault(_data.HP);
            _SetATKDefault(_data.ATK);
            _SetSpiritDefault(_data.Spirit);
            _SetArmorDefault(_data.Armor);

            IsAlreadyLastWish = false;
            IsAlreadySacrificed = false;
            IsAlreadyDead = false;
        }

        ClearAttackedList();

        // TO DO: จะมีบัค ถ้า Reset ตัว Taunt หรือ Aura ที่ถูก Silence ความสามารถจะไม่กลับมา
        //InitAbility();

        // Update card UI
        if (isUpdateUI)
        {
            if (ActiveZone == CardZone.Battlefield || ActiveZone == CardZone.Hand)
            {
                CardUIData cardUIData = new CardUIData(this);

                UIManager.Instance.RequestUpdateCardAllPassive(cardUIData);
                UIManager.Instance.RequestUpdateCardUI(cardUIData);
            }
        }
    }

    public void SetEnableLastWish()
    {
        IsAlreadyLastWish = false;
    }

    public void SetEnableSacrificed()
    {
        IsAlreadySacrificed = false;
    }

    public void SetMinionDead()
    {
        IsAlreadyDead = true;
    }

    public bool IsAlreadyAttack(int uniqueID)
    {
        if (_attackedList != null)
        {
            return _attackedList.Contains(uniqueID);
        }

        return false;
    }

    public void AddAttacked(int uniqueID)
    {
        if (_attackedList == null)
        {
            _attackedList = new List<int>();
        }

        if (!_attackedList.Contains(uniqueID))
        {
            _attackedList.Add(uniqueID);
        }
    }

    public void ClearAttackedList()
    {
        if (_attackedList != null)
        {
            _attackedList.Clear();
        }
    }
    #endregion

    #region Ability
    public override void InitAbility()
    {
        _triggerAbilityEvent = new Dictionary<EventKey, List<int>>();
        base.InitAbility();
    }

    public override bool IsKeySubscribed(EventKey key)
    {
        if (_triggerAbilityEvent != null && _triggerAbilityEvent.ContainsKey(key))
        {
            // have ability trigger
            return true;
        }
        else if (_triggerCallbackEvent != null && _triggerCallbackEvent.ContainsKey(key))
        {
            // have callback trigger
            return true;
        }

        return base.IsKeySubscribed(key);
    }

    private bool FindIsLink()
    {
        if (AbilityList != null)
        {
            foreach (KeyValuePair<int, CardAbilityData> item in AbilityList)
            {
                if (GetDoLink(item.Value.EffectList))
                {
                    return true;
                }
            }
        }

        return false;
    }

    //private bool FindIsTaunted()
    //{
    //    if (Type == CardType.Minion || Type == CardType.Minion_NotInDeck)
    //    {
    //        if (_buffDataList != null)
    //        {
    //            foreach (MinionBuffData buff in _buffDataList)
    //            {
    //                if (buff.GetType() == typeof(BuffMinionTaunted))
    //                {
    //                    return true;
    //                }
    //            }
    //        }
    //    }

    //    return false;
    //}

    //private bool FindIsBeAura()
    //{
    //    if (Type == CardType.Minion || Type == CardType.Minion_NotInDeck)
    //    {
    //        if (_buffDataList != null)
    //        {
    //            foreach (MinionBuffData buff in _buffDataList)
    //            {
    //                if(buff.Key.Split('_')[0] == "AURABUFF")
    //                {
    //                    return true;
    //                }
    //            }
    //        }
    //    }

    //    return false;
    //}

    private bool GetDoLink(List<EffectData> effectList)
    {
        if (effectList != null && effectList.Count > 0)
        {
            foreach (EffectData effect in effectList)
            {
                switch (effect.GetType().ToString())
                {
                    case "DoMinionLink": return true;
                }
            }
        }

        return false;
    }

    public void SubscribeAbility(EventKey key, int abilityUniqueID)
    {
        if (_triggerAbilityEvent.ContainsKey(key))
        {
            if (!_triggerAbilityEvent[key].Contains(abilityUniqueID))
            {
                _triggerAbilityEvent[key].Add(abilityUniqueID);
            }
        }
        else
        {
            List<int> abilityUniqueIDList = new List<int>();
            abilityUniqueIDList.Add(abilityUniqueID);

            _triggerAbilityEvent.Add(key, abilityUniqueIDList);
        }
    }

    public bool UnsubscribeAbility(EventKey key, int abilityUniqueID)
    {
        if (_triggerAbilityEvent.ContainsKey(key))
        {
            return _triggerAbilityEvent[key].Remove(abilityUniqueID);
        }

        return false;
    }

    public void SubscribeEvent(EventKey key, UnityAction<Requester> callback)
    {
        if (_triggerCallbackEvent.ContainsKey(key))
        {
            _triggerCallbackEvent[key].AddListener(callback);
        }
        else
        {
            UnityEvent<Requester> events = new RequesterEvent();
            events.AddListener(callback);

            _triggerCallbackEvent.Add(key, events);
        }
    }

    public bool UnsubscribeEvent(EventKey key, UnityAction<Requester> callback)
    {
        if (_triggerCallbackEvent.ContainsKey(key))
        {
            _triggerCallbackEvent[key].RemoveListener(callback);
            return true;
        }

        return false;
    }

    public void TriggerEvent(EventKey key, Requester triggerer)
    {
        if (_triggerAbilityEvent.ContainsKey(key))
        {
            List<CommandData> commandList = new List<CommandData>();
            foreach (int abilityUniqueID in _triggerAbilityEvent[key])
            {
                PrepareAbilityCommand cmd = new PrepareAbilityCommand(UniqueID, abilityUniqueID, triggerer, key);
                commandList.Add(cmd);
            }
            GameManager.Instance.ActionAddCommandListToSecondQueue(commandList);
        }

        if (_triggerCallbackEvent.ContainsKey(key))
        {
            //if (key == EventKey.LastWish)
            //{
            //    // Last wish event
            //    // Play last wish effect.
            //    UIManager.Instance.RequestShowLastWish(UniqueID, null);
            //}
            _triggerCallbackEvent[key].Invoke(triggerer);
        }
    }

    public void PrepareAbility(int uniqueAbilityID, Requester triggerer, EventKey triggerKey, UnityAction<Dictionary<string, string>> onPrepareComplete, UnityAction onPrepareFail)
    {
        bool isCanSkill = IsAlive;
        if (GameManager.Instance.Phase == GamePhase.Battle && CurrentZone == CardZone.Battlefield)
        {
            isCanSkill = true;
        }

        if (triggerKey == EventKey.LastWish)
        {
            if (!IsAlreadyLastWish)
            {
                isCanSkill = true; // ignore dead
                IsAlreadyLastWish = true;
                UIManager.Instance.RequestShowLastWish(UniqueID, null);
            }
            else
            {
                isCanSkill = false;
            }
        }

        if (triggerKey == EventKey.OnMeSacrificed)
        {
            if (!IsAlreadySacrificed)
            {
                isCanSkill = true; // ignore dead
                IsAlreadySacrificed = true;
            }
            else
            {
                isCanSkill = false;
            }
        }

        // for DoTriggerMyAbility
        if(triggerKey == EventKey.None)
        {
            isCanSkill = true;
        }

        bool isPassive = _abilityList[uniqueAbilityID].IsAbilityLink()
                        || _abilityList[uniqueAbilityID].IsAbilityAura()
                        || _abilityList[uniqueAbilityID].IsAbilityBerserk();
        if (IsBeSilence)
        {
            if (isPassive == false)
            {
                isCanSkill = false;
            }
        }

        if (isCanSkill && _abilityList.ContainsKey(uniqueAbilityID))
        {
            // Add action log
            {
                AbilityTriggerLog log = new AbilityTriggerLog(this, _abilityList[uniqueAbilityID], triggerKey);
                GameManager.Instance.ActionAddBattleLog(log);
            }

            _abilityList[uniqueAbilityID].TryPrepare(triggerer, onPrepareComplete, onPrepareFail);
        }
        else
        {
            if (onPrepareFail != null)
            {
                onPrepareFail.Invoke();
            }
        }
    }

    public void SetPrepareAbilityProperties(int uniqueAbilityID, Requester triggerer, EventKey triggerKey)
    {
        if (_abilityList.ContainsKey(uniqueAbilityID))
        {
            _abilityList[uniqueAbilityID].SetPrepareProperties(triggerer);
        }
    }

    public bool IsCanAbilityActive()
    {
        if (HPCurrent <= 0)
        {
            if (GameManager.Instance.Phase == GamePhase.Battle)
            {
                return true;
            }
            return false;
        }
        return true;
    }
    #endregion

    #region Buff
    public void AddBuff(MinionBuffData buffData, UnityAction onComplete)
    {
        if (_buffDataList == null)
        {
            _buffDataList = new List<MinionBuffData>();
        }

        switch (buffData.BuffOrigin)
        {
            case MinionBuffOrigins.TAUNTBUFF:
                {
                    SetBeTaunted(true);
                    break;
                }

            case MinionBuffOrigins.AURABUFF:
                {
                    SetBeAura(true);
                    break;
                }
        }

        buffData.Subscribe(this);
        _buffDataList.Add(buffData);
        _buffDataList.Last().ActiveBuff();

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void UpdateBuff(string key, UnityAction onComplete)
    {
        bool isUpdated = false;
        MinionBuffData buff = null;
        if (_buffDataList != null && _buffDataList.Count > 0)
        {
            for (int index = 0; index < _buffDataList.Count; ++index)
            {
                if (_buffDataList[index].Key == key)
                {
                    buff = _buffDataList[index];
                    isUpdated |= buff.TryUpdateBuff();
                }
            }

            if(isUpdated)
            {
                CardUIData cardUIData = new CardUIData(this);
                UIManager.Instance.RequestUpdateCardAllPassive(cardUIData, false);
                UIManager.Instance.RequestUpdateCardUI(cardUIData, false);
            }
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void CountBuff(MinionBuffData buffData, UnityAction onComplete)
    {
        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void RemoveBuff(string key, UnityAction onComplete)
    {
        MinionBuffData buff = null;
        if (_buffDataList != null && _buffDataList.Count > 0)
        {
            for (int index = 0; index < _buffDataList.Count; ++index)
            {
                buff = _buffDataList[index];
                if (buff.Key == key)
                {
                    switch (_buffDataList[index].BuffOrigin)
                    {
                        case MinionBuffOrigins.TAUNTBUFF:
                            {
                                SetBeTaunted(false);
                                break;
                            }

                        case MinionBuffOrigins.AURABUFF:
                            {
                                SetBeAura(false);
                                break;
                            }
                    }

                    buff.RemoveBuff();
                    _buffDataList.RemoveAt(index);
                    break;
                }
            }
        }

        if (IsAlive)
        {
            RefreshAllBuff();
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void RemoveBuff(MinionBuffTypes type, UnityAction onComplete)
    {
        if (_buffDataList != null && _buffDataList.Count > 0)
        {
            for (int index = 0; index < _buffDataList.Count; ++index)
            {
                if (_buffDataList[index].BuffType == type)
                {
                    RemoveBuff(_buffDataList[index].Key, null);
                    index--;
                }
            }
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ClearBuff()
    {
        if (_buffDataList != null && _buffDataList.Count > 0)
        {
            for (int index = 0; index < _buffDataList.Count; ++index)
            {
                RemoveBuff(_buffDataList[index].Key, null);
            }
        }
    }

    public void ResetAllBuff()
    {
        if (_buffDataList != null && _buffDataList.Count > 0)
        {
            for (int index = 0; index < _buffDataList.Count; ++index)
            {
                if (_buffDataList[index].BuffOrigin == MinionBuffOrigins.ADDBUFF)
                {
                    RemoveBuff(_buffDataList[index].Key, null);
                    index--;
                }
            }
        }
    }

    public void ClearAllBuff()
    {
        if (_buffDataList != null && _buffDataList.Count > 0)
        {
            for (int index = 0; index < _buffDataList.Count; ++index)
            {
                _buffDataList[index].UnsubscribeAll();
            }

            _buffDataList.Clear();
        }
    }

    public void RefreshAllBuff()
    {
        /*
        _hpBuffBonus = 0;
        _atkBuffBonus = 0;
        _spiritBuffBonus = 0;
        _cardDirBuffOverrideList = new List<CardDirection>();
        _cardPassive = new CardPassive(_data.CardPassive);
        */

        foreach (MinionBuffData buffData in _buffDataList)
        {
            buffData.TryUpdateBuff();
        }

    }
    #endregion

    #region Properties
    /*
    public bool IsPassiveAbility(CardPassive cardPassive)
    {
        return (CardPassive == cardPassive);
    }

    public bool IsPassiveAbility(CardPassiveType passive)
    {
        return (CardPassive.IsPassive(passive));
    }
    */

    public bool IsPointTo(MinionData card)
    {
        if (CurrentZone == CardZone.Battlefield && card != null)
        {
            SlotData slot = null;
            foreach (CardDirectionType dir in System.Enum.GetValues(typeof(CardDirectionType)))
            {
                if (CardDir.IsDirection(dir) && GetAdjacentSlot(dir, out slot))
                {
                    if (!slot.IsSlotEmpty)
                    {
                        if (card.UniqueID == slot.BattleCardUniqueID)
                        {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    public bool IsMinionStatus(CardStatusType status)
    {
        if (_cardStatus != null)
        {
            return (_cardStatus.IsCardStatus(status));
        }

        return false;
    }
    #endregion

    #region Get Methods
    private CardDirection GetMinionDir()
    {
        // gather taunted dir
        CardDirection cardDirTaunted = new CardDirection();
        foreach (KeyValuePair<int, CardDirection> dir in _cardDirTaunted)
        {
            cardDirTaunted += dir.Value;
        }

        // gather buff dir
        CardDirection cardDirResult = new CardDirection();
        foreach (KeyValuePair<int, CardDirection> dir in _cardDirBuff)
        {
            cardDirResult += dir.Value;
        }

        // check dir by priority
        if (cardDirTaunted.IsHasDirection())
        {
            return cardDirTaunted;
        }
        else
        {
            return CardDirection.GetCardDirectionNormalized(_cardDirBase + cardDirResult);
        }
    }

    private int GetMinionHP()
    {
        int hpCurrent = _GetHPCurrentBase();
        int hpBuffPassive = _GetHPBuffPassive();

        return (hpCurrent + hpBuffPassive);
    }

    private int GetMinionATK()
    {
        int atkCurrent = _GetATKCurrentBase();
        int atkBuffPassive = _GetATKBuffPassive();

        return Mathf.Max(0, atkCurrent + atkBuffPassive);
    }

    private int GetMinionSpirit()
    {
        if (IsBeFear)
        {
            return 0;
        }

        int spiritCurrent = _GetSpiritCurrentBase();
        int spiritBuffPassive = _GetSpiritBuffPassive();

        return Mathf.Max(0, spiritCurrent + spiritBuffPassive);
    }

    private int GetMinionArmor()
    {
        int armorCurrent = _GetArmorCurrentBase();
        int armorBuffPassive = _GetArmorBuffPassive();

        return Mathf.Max(0, armorCurrent + armorBuffPassive);
    }

    public bool GetAdjacentMinion(CardDirectionType direction, out MinionData battleCard)
    {
        if (SlotID >= 0 && SlotID < GameManager.Instance.BoardSize)
        {
            SlotData slot = null;
            if (GetAdjacentSlot(direction, out slot))
            {
                if (!slot.IsSlotEmpty)
                {
                    if (GameManager.Instance.FindMinionBattleCard(slot.BattleCardUniqueID, out battleCard))
                    {
                        return true;
                    }
                }
            }
        }

        battleCard = null;
        return false;
    }

    public bool GetAdjacentSlot(CardDirectionType direction, out SlotData slotData)
    {
        if (SlotID >= 0 && SlotID < GameManager.Instance.BoardSize)
        {
            SlotData mySlot = GameManager.Instance.Data.SlotDatas[SlotID];
            SlotData slot = null;
            if (mySlot.GetAdjacentSlot(direction, out slot))
            {
                slotData = slot;
                return true;
            }
        }

        slotData = null;
        return false;
    }

    public CardStatus GetCardStatus()
    {
        CardStatus cardStatus = new CardStatus(_cardStatus);

        //if (IsTaunted)
        //{
        //    cardStatus.Add(CardStatusType.Be_Taunt);
        //}

        //if(IsBeAura)
        //{
        //    cardStatus.Add(CardStatusType.Be_Aura);
        //}

        return cardStatus;
    }

    public override string GetDescription()
    {
        return RawData.Description;
    }

    private int GetPiercingRange()
    {
        if(_piercingRangeList.Count == 0)
        {
            return 0;
        }

        return _piercingRangeList.Last().Value;
    }

    private int GetOverrideEffectDamage()
    {
        if (_overrideEffectDamageList.Count == 0)
        {
            return -1;
        }

        return _overrideEffectDamageList.Last().Value;
    }
    #endregion

    #region Set Methods
    public void ModifyHP(params HPStatData[] dataList)
    {
        ModifyHP(false, dataList);
    }

    public void ModifyHPByIgnoreDamageImmune(params HPStatData[] dataList)
    {
        ModifyHP(true, dataList);
    }

    private void ModifyHP(bool isIgnoreInvincible, params HPStatData[] dataList)
    {
        HPModifyKey key;
        int value;
        foreach (HPStatData data in dataList)
        {
            key = data.Key;
            value = data.Value;
            switch (key)
            {
                case HPModifyKey.BOOST_DELTA:
                {
                    int hpMaxBase = _GetHPMaxBase();
                    int hpCurrent = _GetHPCurrentBase();

                    hpMaxBase = Mathf.Max(0, hpMaxBase + value);
                    _SetHPMaxBase(hpMaxBase);

                    if (value > 0)
                    {
                        hpCurrent = Mathf.Min(hpCurrent + value, hpMaxBase);
                        _SetHPCurrentBase(hpCurrent);
                    }
                    else
                    {
                        hpCurrent = Mathf.Min(hpCurrent, hpMaxBase);
                        _SetHPCurrentBase(hpCurrent);
                    }
                }
                break;

                case HPModifyKey.SET_ALL_TO:
                {
                    int newHP = Mathf.Max(0, value);

                    _SetHPMaxBase(newHP);
                    _SetHPDefault(newHP);
                    _SetHPCurrentBase(newHP);
                }
                break;

                case HPModifyKey.SET_CURRENT_TO:
                {
                    int hpMaxBase = _GetHPMaxBase();

                    _SetHPCurrentBase(Mathf.Min(hpMaxBase, value));
                }
                break;

                case HPModifyKey.SET_CURRENT_DELTA:
                {
                    int remainedValue = value;

                    int hpMaxBase = _GetHPMaxBase();
                    int hpBuffPassive = _GetHPBuffPassive();

                    if (isIgnoreInvincible)
                    {
                        if (remainedValue > 0)
                        {
                            // Got heal
                            int hpCurrent = _GetHPCurrentBase();
                            int newHpCurrent = Mathf.Min(hpMaxBase, hpCurrent + value);
                            int delta = newHpCurrent - hpCurrent;
                            _SetHPCurrentBase(newHpCurrent);

                            remainedValue -= delta; // remove delta point.

                            if (remainedValue > 0)
                            {
                                int hpMaxBuffPassive = _GetHPMaxBuffPassive();
                                hpBuffPassive = Mathf.Max(hpMaxBuffPassive, hpBuffPassive + remainedValue);

                                _SetHPBuffPassive(hpBuffPassive);
                            }
                        }
                        else
                        {
                            // Got damage
                            if (hpBuffPassive > 0)
                            {
                                remainedValue += hpBuffPassive;
                                hpBuffPassive = Mathf.Max(0, hpBuffPassive + value);

                                _SetHPBuffPassive(hpBuffPassive);
                            }

                            if (remainedValue < 0)
                            {
                                int hpCurrent = _GetHPCurrentBase();

                                _SetHPCurrentBase(Mathf.Min(hpMaxBase, hpCurrent + remainedValue));
                            }
                                
                        }
                    }
                    else
                    {
                        if (!IsBeInvincible)
                        {
                            int hpCurrent = _GetHPCurrentBase();

                            if (remainedValue > 0)
                            {
                                _SetHPCurrentBase(Mathf.Min(hpMaxBase, hpCurrent + remainedValue));
                            }
                            else
                            {
                                if (hpBuffPassive > 0)
                                {
                                    remainedValue += hpBuffPassive;
                                    hpBuffPassive = Mathf.Max(0, hpBuffPassive + value);
                                    _SetHPBuffPassive(hpBuffPassive);
                                }

                                if (remainedValue < 0)
                                {
                                    _SetHPCurrentBase(Mathf.Min(hpMaxBase, hpCurrent + remainedValue));
                                }
                            }
                        }
                    }
                }
                break;

                case HPModifyKey.SET_MAX_TO:
                {
                    _SetHPMaxBase(Mathf.Max(0, value));
                }
                break;

                case HPModifyKey.SET_MAX_DELTA:
                {
                    _SetHPMaxBase(Mathf.Max(0, HPMax + value));
                }
                break;

                case HPModifyKey.SET_DEFAULT_TO:
                {
                    _SetHPDefault(Mathf.Max(0, value));
                }
                break;

                case HPModifyKey.SET_DEFAULT_DELTA:
                {
                    _SetHPDefault(Mathf.Max(0, HPDefault + value));
                }
                break;
            }
        }
    }

    public void ModifyATK(params ATKStatData[] dataList)
    {
        ATKModifyKey key;
        int value;
        foreach (ATKStatData data in dataList)
        {
            key = data.Key;
            value = data.Value;
            switch (key)
            {
                case ATKModifyKey.BOOST_DELTA:
                {
                    int atkCurrent = _GetATKCurrentBase();
                    atkCurrent = Mathf.Max(0, atkCurrent + value);
                    _SetATKCurrentBase(atkCurrent);
                }
                break;

                case ATKModifyKey.SET_TO:
                {
                    _SetATKCurrentBase(value);
                    _SetATKDefault(value);
                }
                break;

                case ATKModifyKey.SET_DELTA:
                {
                    int atkCurrent = _GetATKCurrentBase();
                    atkCurrent = atkCurrent + value;
                    _SetATKCurrentBase(atkCurrent);

                    int atkDefault = _GetATKDefault();
                    atkDefault = atkDefault + value;
                    _SetATKDefault(atkDefault);
                }
                break;
            }
        }
    }

    public void ModifySpirit(params SpiritStatData[] dataList)
    {
        SpiritModifyKey key;
        int value;
        foreach (SpiritStatData data in dataList)
        {
            key = data.Key;
            value = data.Value;
            switch (key)
            {
                case SpiritModifyKey.BOOST_DELTA:
                    {
                        int spiritCurrent = _GetSpiritCurrentBase();
                        spiritCurrent = Mathf.Max(0, spiritCurrent + value);
                        _SetSpiritCurrentBase(spiritCurrent);
                    }
                    break;

                case SpiritModifyKey.SET_TO:
                    {
                        _SetSpiritCurrentBase(value);
                        _SetSpiritDefault(value);
                    }
                    break;

                case SpiritModifyKey.SET_DELTA:
                    {
                        int spiritCurrent = _GetSpiritCurrentBase();
                        spiritCurrent = spiritCurrent + value;
                        _SetSpiritCurrentBase(spiritCurrent);

                        int spiritDefault = _GetSpiritDefault();
                        spiritDefault = spiritDefault + value;
                        _SetSpiritDefault(spiritDefault);
                    }
                    break;
            }
        }
    }

    public void ModifyArmor(params ArmorStatData[] dataList)
    {
        ArmorModifyKey key;
        int value;
        foreach (ArmorStatData data in dataList)
        {
            key = data.Key;
            value = data.Value;
            switch (key)
            {
                case ArmorModifyKey.BOOST_DELTA:
                    {
                        int armorCurrent = _GetArmorCurrentBase();
                        armorCurrent = Mathf.Max(0, armorCurrent + value);
                        _SetArmorCurrentBase(armorCurrent);
                    }
                    break;

                case ArmorModifyKey.SET_TO:
                    {
                        _SetArmorCurrentBase(value);
                        _SetArmorDefault(value);
                    }
                    break;

                case ArmorModifyKey.SET_DELTA:
                    {
                        int armorCurrent = _GetArmorCurrentBase();
                        armorCurrent = armorCurrent + value;
                        _SetArmorCurrentBase(armorCurrent);

                        int armorDefault = _GetArmorDefault();
                        armorDefault = armorDefault + value;
                        _SetArmorDefault(armorDefault);
                    }
                    break;
            }
        }
    }

    public void ModifyBuffPassiveHP(int value)
    {
        int hpMaxBuffPassive = _GetHPMaxBuffPassive();
        hpMaxBuffPassive += value;
        _SetHPMaxBuffPassive(hpMaxBuffPassive);

        int hpBuffPassive = _GetHPBuffPassive();
        if (value > 0)
        {
            hpBuffPassive += value;
        }
        else if (value < 0)
        {
            hpBuffPassive = Mathf.Min(hpBuffPassive, hpMaxBuffPassive);
        }
        _SetHPBuffPassive(hpBuffPassive);
    }

    public void ModifyBuffPassiveATK(int value)
    {
        int atkBuffPassive = _GetATKBuffPassive();
        atkBuffPassive = atkBuffPassive + value;
        _SetATKBuffPassive(atkBuffPassive);
    }

    public void ModifyBuffPassiveSpirit(int value)
    {
        int spiritBuffPassive = _GetSpiritBuffPassive();
        spiritBuffPassive = spiritBuffPassive + value;
        _SetSpiritBuffPassive(spiritBuffPassive);
    }

    public void ModifyBuffPassiveArmor(int value)
    {
        int armorBuffPassive = _GetArmorBuffPassive();
        armorBuffPassive = armorBuffPassive + value;
        _SetArmorBuffPassive(armorBuffPassive);
    }

    public void SetCardDir(CardDirection value)
    {
        _cardDirBase = value;
    }

    public void AddCardDir(CardDirection value)
    {
        _cardDirBase += value;
        //for (int i = 0; i < _cardDirBase.DirIndex.Length; i++)
        //{
        //    _cardDirBase.DirIndex[i] += value.DirIndex[i];
        //    //_cardDirBase.DirIndex[i] = Mathf.Clamp(_cardDirBase.DirIndex[i], 0, 1);
        //}
        //_cardDirBase = CardDirection.GetCardDirectionNormalized(_cardDirBase);
    }

    public void AddCardDirBuff(int bufferUniqueID, CardDirection value)
    {
        if (_cardDirBuff.ContainsKey(bufferUniqueID) == false)
        {
            _cardDirBuff.Add(bufferUniqueID, new CardDirection());
        }
        _cardDirBuff[bufferUniqueID] += value;
    }

    public void AddCardDirTaunted(int bufferUniqueID, CardDirection value)
    {
        if (_cardDirTaunted.ContainsKey(bufferUniqueID) == false)
        {
            _cardDirTaunted.Add(bufferUniqueID, new CardDirection());
        }
        _cardDirTaunted[bufferUniqueID] += value;
    }

    public void RemoveCardDirTaunted(int bufferUniqueID, CardDirection value)
    {
        if (_cardDirTaunted.ContainsKey(bufferUniqueID) == false)
        {
            _cardDirTaunted.Add(bufferUniqueID, new CardDirection());
        }
        _cardDirTaunted[bufferUniqueID] -= value;

        if (_cardDirTaunted[bufferUniqueID].IsHasDirection() == false)
        {
            _cardDirTaunted.Remove(bufferUniqueID);
        }
    }

    public void SetInvincible(bool isImmune)
    {
        if (isImmune)
        {
            _cardStatus.Add(CardStatusType.Be_Invincible);
        }
        else
        {
            _cardStatus.Remove(CardStatusType.Be_Invincible);
        }
    }

    public void SetUntargetable(bool isUntargetable)
    {
        if (isUntargetable)
        {
            _cardStatus.Add(CardStatusType.Be_Untargetable);
        }
        else
        {
            _cardStatus.Remove(CardStatusType.Be_Untargetable);
        }
    }

    public void SetEffectInvincible(bool isImmune)
    {
        if (isImmune)
        {
            _cardStatus.Add(CardStatusType.Be_EffectInvincible);
        }
        else
        {
            _cardStatus.Remove(CardStatusType.Be_EffectInvincible);
        }
    }

    public void SetPiercing(int buffUniqueID, bool isPiercing, int range)
    {
        if (isPiercing)
        {
            if(_piercingRangeList.ContainsKey(buffUniqueID))
            {
                _piercingRangeList[buffUniqueID] = range;
            }
            else
            {
                _piercingRangeList.Add(buffUniqueID, range);
                _cardStatus.Add(CardStatusType.Be_Piercing);
            }
        }
        else
        {
            _piercingRangeList.Remove(buffUniqueID);
            _cardStatus.Remove(CardStatusType.Be_Piercing);
        }
    }

    public void SetOverrideEffectDamage(int buffUniqueID, bool isOverride, int damage)
    {
        if (isOverride)
        {
            if (_overrideEffectDamageList.ContainsKey(buffUniqueID))
            {
                _overrideEffectDamageList[buffUniqueID] = damage;
            }
            else
            {
                _overrideEffectDamageList.Add(buffUniqueID, damage);
                _cardStatus.Add(CardStatusType.Be_OverrideEffectDamage);
            }
        }
        else
        {
            _overrideEffectDamageList.Remove(buffUniqueID);
            _cardStatus.Remove(CardStatusType.Be_OverrideEffectDamage);
        }
    }

    public void SetBeLink(bool isLink)
    {
        if (isLink)
        {
            _cardStatus.Add(CardStatusType.Be_Link);
        }
        else
        {
            _cardStatus.Remove(CardStatusType.Be_Link);
        }
    }

    public void SetBeFear(bool isFear)
    {
        if (isFear)
        {
            _cardStatus.Add(CardStatusType.Be_Fear);
        }
        else
        {
            _cardStatus.Remove(CardStatusType.Be_Fear);
        }
    }

    public void SetBeFreeze(bool isFreeze)
    {
        if (isFreeze)
        {
            _cardStatus.Add(CardStatusType.Be_Freeze);
        }
        else
        {
            _cardStatus.Remove(CardStatusType.Be_Freeze);
        }
    }

    public void SetBeSilence(bool isSilence)
    {
        if (isSilence)
        {
            _cardStatus.Add(CardStatusType.Be_Silence);
        }
        else
        {
            _cardStatus.Remove(CardStatusType.Be_Silence);
        }
    }

    public void SetBeStealth(bool isStealth)
    {
        if (isStealth)
        {
            _cardStatus.Add(CardStatusType.Be_Stealth);
        }
        else
        {
            _cardStatus.Remove(CardStatusType.Be_Stealth);
        }
    }

    public void SetBeTaunted(bool isTaunted)
    {
        if (isTaunted)
        {
            _cardStatus.Add(CardStatusType.Be_Taunt);
        }
        else
        {
            _cardStatus.Remove(CardStatusType.Be_Taunt);
        }
    }

    public void SetBeAura(bool isBeAura)
    {
        if (isBeAura)
        {
            _cardStatus.Add(CardStatusType.Be_Aura);
        }
        else
        {
            _cardStatus.Remove(CardStatusType.Be_Aura);
        }
    }

    public void SetBeDarkMatter(bool isDarkMatter)
    {
        if (isDarkMatter)
        {
            _cardStatus.Add(CardStatusType.Be_DarkMatter);
        }
        else
        {
            _cardStatus.Remove(CardStatusType.Be_DarkMatter);
        }
    }

    public void SetBerserkActive(bool isBerserkActive)
    {
        if (isBerserkActive)
        {
            _cardStatus.Add(CardStatusType.BerserkActive);
        }
        else
        {
            _cardStatus.Remove(CardStatusType.BerserkActive);
        }
    }

    public void SetTauntActive(bool isTauntActive)
    {
        if (isTauntActive)
        {
            _cardStatus.Add(CardStatusType.TauntActive);
        }
        else
        {
            _cardStatus.Remove(CardStatusType.TauntActive);
        }
    }

    public void SetAuraActive(bool isAuraActive)
    {
        if (isAuraActive)
        {
            _cardStatus.Add(CardStatusType.AuraActive);
        }
        else
        {
            _cardStatus.Remove(CardStatusType.AuraActive);
        }
    }
    #endregion
    #endregion

    #region Get Set
    public void SetKillerPlayerIndex(PlayerIndex playerIndex)
    {
        MinionKillerPlayerIndex = playerIndex;
    }

    protected void _SetHPMax(int value)
    {
        int value_encode = GameHelper.EncodeInt(value, EncodeKey);

        _hpMax_encode = value_encode;
        _hpMax_encode_str = GameHelper.ConvertIntToCustomStr(value_encode);
    }

    protected void _SetHPDefault(int value)
    {
        int value_encode = GameHelper.EncodeInt(value, EncodeKey);

        _hpDefault_encode = value_encode;
        _hpDefault_encode_str = GameHelper.ConvertIntToCustomStr(value_encode);
    }

    protected void _SetATKDefault(int value)
    {
        int value_encode = GameHelper.EncodeInt(value, EncodeKey);

        _atkDefault_encode = value_encode;
        _atkDefault_encode_str = GameHelper.ConvertIntToCustomStr(value_encode);
    }

    protected void _SetSpiritDefault(int value)
    {
        int value_encode = GameHelper.EncodeInt(value, EncodeKey);

        _spiritDefault_encode = value_encode;
        _spiritDefault_encode_str = GameHelper.ConvertIntToCustomStr(value_encode);
    }

    protected void _SetArmorDefault(int value)
    {
        int value_encode = GameHelper.EncodeInt(value, EncodeKey);

        _armorDefault_encode = value_encode;
        _armorDefault_encode_str = GameHelper.ConvertIntToCustomStr(value_encode);
    }

    protected int _GetHPMax()
    {
        int value_encode = GameHelper.ConvertCustomStrToInt(_hpMax_encode_str);
        int value = GameHelper.DecodeInt(value_encode, EncodeKey);
        int dummy_value = GameHelper.DecodeInt(_hpMax_encode, EncodeKey);

        if (value_encode != _hpMax_encode)
        {
            GameManager.Instance.OnTriggerImpossibleError(
                  "ERROR_MISMATCH_UNIT_HP_MAX"
                , this.Owner
                , GameManager.Instance.Data
                , string.Format("{0} != {1}", value, dummy_value)
            );

            _hpMax_encode = value_encode;

            // error
            Debug.LogErrorFormat("_GetHPMax: [Error] Imposible error. {0} != {1}", value, dummy_value);
        }

        return value;
    }

    protected int _GetHPDefault()
    {
        int value_encode = GameHelper.ConvertCustomStrToInt(_hpDefault_encode_str);
        int value = GameHelper.DecodeInt(value_encode, EncodeKey);
        int dummy_value = GameHelper.DecodeInt(_hpDefault_encode, EncodeKey);

        if (value_encode != _hpDefault_encode)
        {
            GameManager.Instance.OnTriggerImpossibleError(
                  "ERROR_MISMATCH_UNIT_HP_DEFAULT"
                , this.Owner
                , GameManager.Instance.Data
                , string.Format("{0} != {1}", value, dummy_value)
            );

            _hpDefault_encode = value_encode;

            // error
            Debug.LogErrorFormat("_GetHPDefault: [Error] Imposible error. {0} != {1}", value, dummy_value);
        }

        return value;
    }

    protected int _GetATKDefault()
    {
        int value_encode = GameHelper.ConvertCustomStrToInt(_atkDefault_encode_str);
        int value = GameHelper.DecodeInt(value_encode, EncodeKey);
        int dummy_value = GameHelper.DecodeInt(_atkDefault_encode, EncodeKey);

        if (value_encode != _atkDefault_encode)
        {
            GameManager.Instance.OnTriggerImpossibleError(
                  "ERROR_MISMATCH_UNIT_ATK_DEFAULT"
                , this.Owner
                , GameManager.Instance.Data
                , string.Format("{0} != {1}", value, dummy_value)
            );

            _atkDefault_encode = value_encode;

            // error
            Debug.LogErrorFormat("_GetAtkDefault: [Error] Imposible error. {0} != {1}", value, dummy_value);
        }

        return value;
    }

    protected int _GetSpiritDefault()
    {
        int value_encode = GameHelper.ConvertCustomStrToInt(_spiritDefault_encode_str);
        int value = GameHelper.DecodeInt(value_encode, EncodeKey);
        int dummy_value = GameHelper.DecodeInt(_spiritDefault_encode, EncodeKey);

        if (value_encode != _spiritDefault_encode)
        {
            GameManager.Instance.OnTriggerImpossibleError(
                  "ERROR_MISMATCH_UNIT_SOUL_DEFAULT"
                , this.Owner
                , GameManager.Instance.Data
                , string.Format("{0} != {1}", value, dummy_value)
            );

            _spiritDefault_encode = value_encode;

            // error
            Debug.LogErrorFormat("_GetSpiritDefault: [Error] Imposible error. {0} != {1}", value, dummy_value);
        }

        return value;
    }

    protected int _GetArmorDefault()
    {
        int value_encode = GameHelper.ConvertCustomStrToInt(_armorDefault_encode_str);
        int value = GameHelper.DecodeInt(value_encode, EncodeKey);
        int dummy_value = GameHelper.DecodeInt(_armorDefault_encode, EncodeKey);

        if (value_encode != _armorDefault_encode)
        {
            GameManager.Instance.OnTriggerImpossibleError(
                  "ERROR_MISMATCH_UNIT_ARMOR_DEFAULT"
                , this.Owner
                , GameManager.Instance.Data
                , string.Format("{0} != {1}", value, dummy_value)
            );

            _armorDefault_encode = value_encode;

            // error
            Debug.LogErrorFormat("_GetArmorDefault: [Error] Imposible error. {0} != {1}", value, dummy_value);
        }

        return value;
    }

    protected void _SetHPMaxBase(int value)
    {
        int value_encode = GameHelper.EncodeInt(value, EncodeKey);

        _hpMaxBase_encode = value_encode;
        _hpMaxBase_encode_str = GameHelper.ConvertIntToCustomStr(value_encode);
    }

    protected void _SetHPCurrentBase(int value)
    {
        int value_encode = GameHelper.EncodeInt(value, EncodeKey);

        _hpCurrentBase_encode = value_encode;
        _hpCurrentBase_encode_str = GameHelper.ConvertIntToCustomStr(value_encode);
    }

    protected void _SetATKCurrentBase(int value)
    {
        int value_encode = GameHelper.EncodeInt(value, EncodeKey);

        _atkCurrentBase_encode = value_encode;
        _atkCurrentBase_encode_str = GameHelper.ConvertIntToCustomStr(value_encode);
    }

    protected void _SetSpiritCurrentBase(int value)
    {
        int value_encode = GameHelper.EncodeInt(value, EncodeKey);

        _spiritCurrentBase_encode = value_encode;
        _spiritCurrentBase_encode_str = GameHelper.ConvertIntToCustomStr(value_encode);
    }

    protected void _SetArmorCurrentBase(int value)
    {
        int value_encode = GameHelper.EncodeInt(value, EncodeKey);

        _armorCurrentBase_encode = value_encode;
        _armorCurrentBase_encode_str = GameHelper.ConvertIntToCustomStr(value_encode);
    }

    protected int _GetHPMaxBase()
    {
        int value_encode = GameHelper.ConvertCustomStrToInt(_hpMaxBase_encode_str);
        int value = GameHelper.DecodeInt(value_encode, EncodeKey);
        int dummy_value = GameHelper.DecodeInt(_hpMaxBase_encode, EncodeKey);

        if (value_encode != _hpMaxBase_encode)
        {
            GameManager.Instance.OnTriggerImpossibleError(
                  "ERROR_MISMATCH_UNIT_HP_MAX_BASE"
                , this.Owner
                , GameManager.Instance.Data
                , string.Format("{0} != {1}", value, dummy_value)
            );

            _hpMaxBase_encode = value_encode;

            // error
            Debug.LogErrorFormat("_GetHPMaxBase: [Error] Imposible error. {0} != {1}", value, dummy_value);
        }

        return value;
    }

    protected int _GetHPCurrentBase()
    {
        int value_encode = GameHelper.ConvertCustomStrToInt(_hpCurrentBase_encode_str);
        int value = GameHelper.DecodeInt(value_encode, EncodeKey);
        int dummy_value = GameHelper.DecodeInt(_hpCurrentBase_encode, EncodeKey);

        if (value_encode != _hpCurrentBase_encode)
        {
            GameManager.Instance.OnTriggerImpossibleError(
                  "ERROR_MISMATCH_UNIT_HP_CURRENT_BASE"
                , this.Owner
                , GameManager.Instance.Data
                , string.Format("{0} != {1}", value, dummy_value)
            );

            _hpCurrentBase_encode = value_encode;

            // error
            Debug.LogErrorFormat("_GetHPCurrentBase: [Error] Imposible error. {0} != {1}", value, dummy_value);
        }

        return value;
    }

    protected int _GetATKCurrentBase()
    {
        int value_encode = GameHelper.ConvertCustomStrToInt(_atkCurrentBase_encode_str);
        int value = GameHelper.DecodeInt(value_encode, EncodeKey);
        int dummy_value = GameHelper.DecodeInt(_atkCurrentBase_encode, EncodeKey);

        if (value_encode != _atkCurrentBase_encode)
        {
            GameManager.Instance.OnTriggerImpossibleError(
                  "ERROR_MISMATCH_UNIT_ATK_CURRENT_BASE"
                , this.Owner
                , GameManager.Instance.Data
                , string.Format("{0} != {1}", value, dummy_value)
            );

            _atkCurrentBase_encode = value_encode;

            // error
            Debug.LogErrorFormat("_GetAtkCurrentBase: [Error] Imposible error. {0} != {1}", value, dummy_value);
        }

        return value;
    }

    protected int _GetSpiritCurrentBase()
    {
        int value_encode = GameHelper.ConvertCustomStrToInt(_spiritCurrentBase_encode_str);
        int value = GameHelper.DecodeInt(value_encode, EncodeKey);
        int dummy_value = GameHelper.DecodeInt(_spiritCurrentBase_encode, EncodeKey);

        if (value_encode != _spiritCurrentBase_encode)
        {
            GameManager.Instance.OnTriggerImpossibleError(
                  "ERROR_MISMATCH_UNIT_SOUL_CURRENT_BASE"
                , this.Owner
                , GameManager.Instance.Data
                , string.Format("{0} != {1}", value, dummy_value)
            );

            _spiritCurrentBase_encode = value_encode;

            // error
            Debug.LogErrorFormat("_GetSpiritCurrentBase: [Error] Imposible error. {0} != {1}", value, dummy_value);
        }

        return value;
    }

    protected int _GetArmorCurrentBase()
    {
        int value_encode = GameHelper.ConvertCustomStrToInt(_armorCurrentBase_encode_str);
        int value = GameHelper.DecodeInt(value_encode, EncodeKey);
        int dummy_value = GameHelper.DecodeInt(_armorCurrentBase_encode, EncodeKey);

        if (value_encode != _armorCurrentBase_encode)
        {
            GameManager.Instance.OnTriggerImpossibleError(
                  "ERROR_MISMATCH_UNIT_ARMOR_CURRENT_BASE"
                , this.Owner
                , GameManager.Instance.Data
                , string.Format("{0} != {1}", value, dummy_value)
            );

            _armorCurrentBase_encode = value_encode;

            // error
            Debug.LogErrorFormat("_GetArmorCurrentBase: [Error] Imposible error. {0} != {1}", value, dummy_value);
        }

        return value;
    }

    protected void _SetHPMaxBuffPassive(int value)
    {
        int value_encode = GameHelper.EncodeInt(value, EncodeKey);

        _hpMaxBuffPassive_encode = value_encode;
        _hpMaxBuffPassive_encode_str = GameHelper.ConvertIntToCustomStr(value_encode);
    }

    protected void _SetHPBuffPassive(int value)
    {
        int value_encode = GameHelper.EncodeInt(value, EncodeKey);

        _hpBuffPassive_encode = value_encode;
        _hpBuffPassive_encode_str = GameHelper.ConvertIntToCustomStr(value_encode);
    }

    protected void _SetATKBuffPassive(int value)
    {
        int value_encode = GameHelper.EncodeInt(value, EncodeKey);

        _atkBuffPassive_encode = value_encode;
        _atkBuffPassive_encode_str = GameHelper.ConvertIntToCustomStr(value_encode);
    }

    protected void _SetSpiritBuffPassive(int value)
    {
        int value_encode = GameHelper.EncodeInt(value, EncodeKey);

        _spiritBuffPassive_encode = value_encode;
        _spiritBuffPassive_encode_str = GameHelper.ConvertIntToCustomStr(value_encode);
    }

    protected void _SetArmorBuffPassive(int value)
    {
        int value_encode = GameHelper.EncodeInt(value, EncodeKey);

        _armorBuffPassive_encode = value_encode;
        _armorBuffPassive_encode_str = GameHelper.ConvertIntToCustomStr(value_encode);
    }

    protected int _GetHPMaxBuffPassive()
    {
        int value_encode = GameHelper.ConvertCustomStrToInt(_hpMaxBuffPassive_encode_str);
        int value = GameHelper.DecodeInt(value_encode, EncodeKey);
        int dummy_value = GameHelper.DecodeInt(_hpMaxBuffPassive_encode, EncodeKey);

        if (value_encode != _hpMaxBuffPassive_encode)
        {
            GameManager.Instance.OnTriggerImpossibleError(
                  "ERROR_MISMATCH_UNIT_HP_MAX_BUFF_PASSIVE"
                , this.Owner
                , GameManager.Instance.Data
                , string.Format("{0} != {1}", value, dummy_value)
            );

            _hpMaxBuffPassive_encode = value_encode;

            // error
            Debug.LogErrorFormat("_GetHPMaxBuffPassive: [Error] Imposible error. {0} != {1}", value, dummy_value);
        }

        return value;
    }

    protected int _GetHPBuffPassive()
    {
        int value_encode = GameHelper.ConvertCustomStrToInt(_hpBuffPassive_encode_str);
        int value = GameHelper.DecodeInt(value_encode, EncodeKey);
        int dummy_value = GameHelper.DecodeInt(_hpBuffPassive_encode, EncodeKey);

        if (value_encode != _hpBuffPassive_encode)
        {
            GameManager.Instance.OnTriggerImpossibleError(
                  "ERROR_MISMATCH_UNIT_HP_BUFF_PASSIVE"
                , this.Owner
                , GameManager.Instance.Data
                , string.Format("{0} != {1}", value, dummy_value)
            );

            _hpBuffPassive_encode = value_encode;

            // error
            Debug.LogErrorFormat("_GetHPBuffPassive: [Error] Imposible error. {0} != {1}", value, dummy_value);
        }

        return value;
    }

    protected int _GetATKBuffPassive()
    {
        int value_encode = GameHelper.ConvertCustomStrToInt(_atkBuffPassive_encode_str);
        int value = GameHelper.DecodeInt(value_encode, EncodeKey);
        int dummy_value = GameHelper.DecodeInt(_atkBuffPassive_encode, EncodeKey);

        if (value_encode != _atkBuffPassive_encode)
        {
            GameManager.Instance.OnTriggerImpossibleError(
                  "ERROR_MISMATCH_UNIT_ATK_BUFF_PASSIVE"
                , this.Owner
                , GameManager.Instance.Data
                , string.Format("{0} != {1}", value, dummy_value)
            );

            _atkBuffPassive_encode = value_encode;

            // error
            Debug.LogErrorFormat("_GetAtkBuffPassive: [Error] Imposible error. {0} != {1}", value, dummy_value);
        }

        return value;
    }

    protected int _GetSpiritBuffPassive()
    {
        int value_encode = GameHelper.ConvertCustomStrToInt(_spiritBuffPassive_encode_str);
        int value = GameHelper.DecodeInt(value_encode, EncodeKey);
        int dummy_value = GameHelper.DecodeInt(_spiritBuffPassive_encode, EncodeKey);

        if (value_encode != _spiritBuffPassive_encode)
        {
            GameManager.Instance.OnTriggerImpossibleError(
                  "ERROR_MISMATCH_UNIT_SOUL_BUFF_PASSIVE"
                , this.Owner
                , GameManager.Instance.Data
                , string.Format("{0} != {1}", value, dummy_value)
            );

            _spiritBuffPassive_encode = value_encode;

            // error
            Debug.LogErrorFormat("_GetSpiritBuffPassive: [Error] Imposible error. {0} != {1}", value, dummy_value);
        }

        return value;
    }

    protected int _GetArmorBuffPassive()
    {
        int value_encode = GameHelper.ConvertCustomStrToInt(_armorBuffPassive_encode_str);
        int value = GameHelper.DecodeInt(value_encode, EncodeKey);
        int dummy_value = GameHelper.DecodeInt(_armorBuffPassive_encode, EncodeKey);

        if (value_encode != _armorBuffPassive_encode)
        {
            GameManager.Instance.OnTriggerImpossibleError(
                  "ERROR_MISMATCH_UNIT_ARMOR_BUFF_PASSIVE"
                , this.Owner
                , GameManager.Instance.Data
                , string.Format("{0} != {1}", value, dummy_value)
            );

            _armorBuffPassive_encode = value_encode;

            // error
            Debug.LogErrorFormat("_GetArmorBuffPassive: [Error] Imposible error. {0} != {1}", value, dummy_value);
        }

        return value;
    }
    #endregion
}
#endregion

#region SpellData
public class SpellData : BattleCardData
{
    #region Public Properties
    public bool IsCanUse
    {
        get
        {
            foreach (KeyValuePair<int, CardAbilityData> item in _abilityList)
            {
                if (!item.Value.IsCanActive())
                {
                    return false;
                }
            }

            return true;
        }
    }
    #endregion

    #region Private Properties
    #endregion

    #region Contructor
    public SpellData(int uniqueID, CardData cardData, string cardBackID)
    {
        UniqueID = uniqueID;

        if (cardData != null)
        {
            _data = cardData;
        }

        CardBackID = cardBackID;
    }

    public SpellData(SpellData data)
    {
        UniqueID = data.UniqueID;

        if (data != null)
        {
            _data = data._data;
        }

        CardBackID = data.CardBackID;
    }
    #endregion

    #region Methods
    public static SpellData CreateSpell(CardData cardData, string cardBackID)
    {
        SpellData spellData = new SpellData(_uniqueID++, cardData, cardBackID);

        return spellData;
    }
    #endregion

    #region Ability Methods
    public void PrepareAbility(int uniqueAbilityID, Requester triggerer, EventKey triggerKey, UnityAction<Dictionary<string, string>> onPrepareComplete, UnityAction onPrepareFail)
    {
        if (_abilityList.ContainsKey(uniqueAbilityID))
        {
            _abilityList[uniqueAbilityID].TryPrepare(triggerer, onPrepareComplete, onPrepareFail);
        }
        else
        {
            if (onPrepareFail != null)
            {
                onPrepareFail.Invoke();
            }
        }
    }

    public void SetPrepareAbilityProperties(int uniqueAbilityID, Requester triggerer, EventKey triggerKey)
    {
        if (_abilityList.ContainsKey(uniqueAbilityID))
        {
            _abilityList[uniqueAbilityID].SetPrepareProperties(triggerer);
        }
    }

    public override void ResetToDefault(bool isUpdateUI = true)
    {
        if (RawData != null)
        {
            _data = RawData;
        }
        InitAbility();

        // Update card UI
        if (isUpdateUI)
        {
            if (CurrentZone == CardZone.Battlefield || CurrentZone == CardZone.Hand)
            {
                CardUIData cardUIData = new CardUIData(this);

                UIManager.Instance.RequestUpdateCardAllPassive(cardUIData);
                UIManager.Instance.RequestUpdateCardUI(cardUIData);
            }
        }
    }
    #endregion
}
#endregion