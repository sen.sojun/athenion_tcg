﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardChangeLogData
{
    #region Private Properties
    private Dictionary<string, CardChangeLogDetail> _data = new Dictionary<string, CardChangeLogDetail>();
    #endregion

    #region Constructors
    public CardChangeLogData() { }

    public CardChangeLogData(string json)
    {
        _data = new Dictionary<string, CardChangeLogDetail>();
        Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
        foreach (KeyValuePair<string, object> item in rawData)
        {
            _data.Add(item.Key, new CardChangeLogDetail(item.Key,item.Value.ToString()));
        }
    }
    #endregion

    #region Methods
    public List<string> GetCurrentChangeLogCardList()
    {
        foreach (KeyValuePair<string, CardChangeLogDetail> item in _data)
        {
            bool isValidDate = DateTimeData.GetDateTimeUTC() > item.Value.ReleaseDate && DateTimeData.GetDateTimeUTC() < item.Value.EndDate;
            if (isValidDate)
                return item.Value.cardIDList;
        }
        return null;
    }

    public CardChangeLogDetail GetCurrentChangeLogDetail()
    {
        foreach (KeyValuePair<string, CardChangeLogDetail> item in _data)
        {
            bool isValidDate = DateTimeData.GetDateTimeUTC() > item.Value.ReleaseDate && DateTimeData.GetDateTimeUTC() < item.Value.EndDate;
            if (isValidDate)
                return item.Value;
        }
        return null;
    }
    #endregion
}

public class CardChangeLogDetail
{
    #region Public Properties
    public string ID { get; set; }
    public DateTime ReleaseDate { get; private set; }
    public DateTime EndDate { get; private set; }
    public List<string> cardIDList { get; private set; }
    #endregion

    #region Constructors
    public CardChangeLogDetail(string id,string json)
    {
        ID = id;
        Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
        ReleaseDate = rawData.ContainsKey("release_date") ? Convert.ToDateTime(rawData["release_date"].ToString()) : DateTime.MinValue;
        EndDate = rawData.ContainsKey("end_date") ? Convert.ToDateTime(rawData["end_date"].ToString()) : DateTime.MaxValue;

        if (rawData.ContainsKey("cards"))
        {
            SetupCardIDList(rawData["cards"].ToString());
        }
    }
    #endregion

    #region Methods
    private void SetupCardIDList(string json)
    {
        cardIDList = JsonConvert.DeserializeObject<List<string>>(json);
    }
    #endregion
}