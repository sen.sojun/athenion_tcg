﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Newtonsoft.Json;

[Serializable]
public class AdventureStageData
{
    public AdventureChapterID AdventureChapterID { get; private set; }
    public AdventureStageID AdventureStageID { get; private set; }
    public bool ShowSpriteAtStageSelect { get; private set; }
    public bool PlayerUseCustomDeck { get; private set; }
    public List<AdventureStageID> RequiredPrevFinishedStageIDs { get; private set; }
    public Dictionary<string, int> Rewards { get; private set; }
    public List<MatchPlayerDataConfigs> MatchPlayerDataConfigs { get; private set; }
    public string CustomData { get; private set; }

    public AdventureStageData(AdventureChapterID chapterID, string stageID, string json)
    {
        AdventureChapterID = chapterID;
        Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
        if(GameHelper.StrToEnum(stageID, out AdventureStageID adventureStageIDToAdd))
        {
            AdventureStageID = adventureStageIDToAdd;
        }
        else
        {
            Debug.Log("No Stage ID: " + stageID + " in enums");
        }

        ShowSpriteAtStageSelect = rawData.ContainsKey("show_sprite_at_stage_select") ? (bool)rawData["show_sprite_at_stage_select"] : false;
        PlayerUseCustomDeck = rawData.ContainsKey("player_use_custom_deck") ?  (bool)rawData["player_use_custom_deck"] : true;
        RequiredPrevFinishedStageIDs = new List<AdventureStageID>();
        List<string> requiredPrevFinishedStageIDsStringArray = JsonConvert.DeserializeObject<List<string>>(rawData["required_prev_finished_stage_ids"].ToString());
        foreach(string item in requiredPrevFinishedStageIDsStringArray)
        {
            if (GameHelper.StrToEnum(item, out AdventureStageID requiredPrevFinishedStageID))
            {
                RequiredPrevFinishedStageIDs.Add(requiredPrevFinishedStageID);
            }
            else
            {
                Debug.Log("No Stage ID: " + item + " in enums");
            }
        }

        //Match data configs
        List<object> matchplayerDataConfigsObjects = JsonConvert.DeserializeObject<List<object>>(rawData["match_player_data_configs"].ToString());
        MatchPlayerDataConfigs = new List<MatchPlayerDataConfigs>();
        foreach (object item in matchplayerDataConfigsObjects)
        {
            MatchPlayerDataConfigs.Add(new MatchPlayerDataConfigs(item.ToString()));
        }

        //Rewards
        Dictionary<string, object> RewardObjects = JsonConvert.DeserializeObject<Dictionary<string, object>> (rawData["rewards"].ToString());
        Rewards = new Dictionary<string, int>();
        foreach (KeyValuePair<string, object> item in RewardObjects)
        {
            Rewards.Add(item.Key, int.Parse(item.Value.ToString()));
        }
        
        CustomData = rawData.ContainsKey("custom_data") ? rawData["custom_data"].ToString() : null;
    }

    public BotLevel GetOpponentBotLevel()
    {
        if (MatchPlayerDataConfigs.Count < 2)
        {
            return BotLevel.Hard;
        }

        return MatchPlayerDataConfigs[1].BotLevel;
    }
}

[Serializable]
public class MatchPlayerDataConfigs
{
    public bool UsePlayerName { get; private set; }
    public string DeckID { get; private set; }
    public List<string> DeckList { get; private set; }
    public string HeroID { get; private set; }
    public CardElementType ElementType { get; private set; }
    public int HP { get; private set; }
    public BotLevel BotLevel { get; private set; }

    public MatchPlayerDataConfigs(string json)
    {
        Dictionary<string, object> rawMatchPlayerData = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
        UsePlayerName = (bool)rawMatchPlayerData["use_player_name"];
        DeckID = rawMatchPlayerData["deck_id"].ToString();
        DeckList = JsonConvert.DeserializeObject<List<string>>(rawMatchPlayerData["deck_list"].ToString());
        HeroID = rawMatchPlayerData["hero_id"].ToString();
        HP = int.Parse(rawMatchPlayerData["hp"].ToString());
        ElementType = (CardElementType)Enum.Parse(typeof(CardElementType), rawMatchPlayerData["element_type"].ToString(), true);
        BotLevel = (BotLevel)Enum.Parse(typeof(BotLevel), rawMatchPlayerData["bot_level"].ToString(), true);
    }

}