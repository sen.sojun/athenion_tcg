﻿using Newtonsoft.Json;
using System.Collections.Generic;

public class MissionData : GameObjectiveData
{
    #region Enums
    private enum MissionStatus
    {
        active
        , clear
    }
    #endregion

    #region Public Properties
    public bool IsEnabled { get { return _isEnabled; } }
    public bool IsMissionSeen { get { return IsSeen(); } }
    public bool IsMissionHasProgress { get { return IsHasProgress(); } }
    public string GoFuncName { get { return _localData.FunctionName; } }
    public string GoFuncParam { get { return _localData.FunctionParam; } }
    #endregion

    #region Private Properties
    private const string _statusKey = "status";
    private const string _statKey = "stat";

    private Dictionary<string, int> _missionStatStamp = new Dictionary<string, int>();
    private MissionDBData _localData = new MissionDBData();
    private bool _isEnabled = false;
    #endregion

    #region Contructors
    public MissionData()
    {
    }

    public MissionData(MissionData data)
    {
        _id = data.ID;
        _isCleared = data._isCleared;
        _isEnabled = data._isEnabled;
        _data = new MissionDBData(data._localData);
        _itemDataList = new List<ItemData>(data._itemDataList);
        _missionStatStamp = new Dictionary<string, int>(data._missionStatStamp);
    }

    public MissionData(MissionDBData data)
    {
        _id = data.ID;
        _data = new MissionDBData(data);
        _localData = new MissionDBData(_data as MissionDBData);
        _itemDataList = new List<ItemData>(_localData.ItemDataList);
    }

    public MissionData(MissionDBData data, Dictionary<string, object> playerMissionData)
    {
        _id = data.ID;
        _data = new MissionDBData(data);
        _localData = new MissionDBData(_data as MissionDBData);
        _itemDataList = new List<ItemData>(_localData.ItemDataList);

        if (playerMissionData.ContainsKey(_statusKey))
        {
            GameHelper.StrToEnum(playerMissionData[_statusKey].ToString(), out MissionStatus status);
            _isCleared = IsMissionCleared(status);
        }

        if (playerMissionData.ContainsKey(_statKey))
        {
            _missionStatStamp = JsonConvert.DeserializeObject<Dictionary<string, int>>(playerMissionData[_statKey].ToString());
        }
    }
    #endregion

    #region Methods
    protected override int GetCurrentProgress()
    {
        if(_isEnabled == false)
        {
            return 0;
        }

        Dictionary<string, int> playerStatData = DataManager.Instance.PlayerInfo.GetPlayerStatData();
        int progressCount = 0;
        int currentStatCount = 0;

        foreach (PlayerStatDetailData detail in _localData.DetailDataList)
        {
            currentStatCount = 0;
            foreach (string key in detail.PlayerStatList)
            {
                if (!playerStatData.ContainsKey(key))
                {
                    playerStatData.Add(key, 0);
                }

                currentStatCount += playerStatData[key];
            }

            if (currentStatCount >= detail.Amount)
            {
                progressCount += detail.Amount;
            }
            else
            {
                progressCount += currentStatCount;
            }
        }

        return progressCount;
    }

    protected override int GetMaxProgress()
    {
        if (_data != null)
        {
            return _localData.MaxProgress;
        }

        return 0;
    }

    private bool IsSeen()
    {
        Dictionary<string, int> playerStatData = DataManager.Instance.PlayerInfo.GetPlayerStatData();
        PlayerStatData data = new PlayerStatData(new PlayerStatKey(StatKeys.MISSION.ToString(), StatKeys.SEEN.ToString(), ID));
        return playerStatData.ContainsKey(data.Key);
    }

    private bool IsHasProgress()
    {
        Dictionary<string, int> playerStatData = DataManager.Instance.PlayerInfo.GetPlayerStatData();
        foreach (PlayerStatDetailData detail in _localData.DetailDataList)
        {
            foreach (string key in detail.PlayerStatList)
            {
                if(playerStatData.ContainsKey(key) == false)
                {
                    return false;
                }
            }
        }

        if(_localData.DetailDataList.Count == 0)
        {
            return false;
        }
        return true;
    }

    private bool IsMissionCleared(MissionStatus status)
    {
        return status == MissionStatus.clear;
    }

    public void SetMissionEnabled(List<string> missionIDList)
    {
        foreach(string id in _localData.RequireMissionIDList)
        {
            if(missionIDList.Contains(id) == false)
            {
                _isEnabled = false;
                return;
            }
        }

        _isEnabled = true;
    }
    #endregion
}