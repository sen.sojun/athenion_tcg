﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using MissionController;
using System;

public class MissionManager : MonoSingleton<MissionManager>
{
    public MissionUIController UIManager;
    private Controller _controller;
    #region Subscription Event

    protected override void Awake()
    {
        base.Awake();
        UIManager = MissionUIController.Instance;
    }

    private void OnEnable()
    {
        HomeManager.OnInit += HomeManager_OnInit;
        CollectionManager.OnInit += CollectionManager_OnInit;
        ShopManager.OnInit += ShopManager_OnInit;
        StarExchangeManager.OnInit += StarExchangeManager_OnInit;
    }

    private void OnDisable()
    {
        HomeManager.OnInit -= HomeManager_OnInit;
        CollectionManager.OnInit -= CollectionManager_OnInit;
        ShopManager.OnInit -= ShopManager_OnInit;
        StarExchangeManager.OnInit -= StarExchangeManager_OnInit;
    }

    private void ShopManager_OnInit()
    {
        ChangeController(NavigatorController.PageType.Shop);
    }

    private void CollectionManager_OnInit()
    {
        ChangeController(NavigatorController.PageType.Deck);
    }

    private void HomeManager_OnInit()
    {
        ChangeController(NavigatorController.PageType.Home);
    }

    private void StarExchangeManager_OnInit()
    {
        ChangeController(NavigatorController.PageType.StarMarket);
    }

    #endregion

    #region Methods

    public void ChangeController(NavigatorController.PageType Page)
    {
        if (_controller != null)
        {
            _controller.OnExitMission();
        }
        _controller = GetController(Page);
        _controller?.OnEnterMission();
    }

    private Controller GetController(NavigatorController.PageType page)
    {
        switch (page)
        {
            case NavigatorController.PageType.Deck:
                return new CollectionMissionController(this);
            case NavigatorController.PageType.Home:
                return new HomeMissionController(this);
            case NavigatorController.PageType.Shop:
                return new ShopMissionController(this);
            case NavigatorController.PageType.Guild:
                throw new NotImplementedException();
            case NavigatorController.PageType.StarMarket:
                return new StarMarketMissionController(this);
        }
        return null;
    }

    public void RequestClaimMissionReward(string missionID, UnityAction<List<ItemData>> onComplete, UnityAction<string> onFail)
    {
        DataManager.Instance.CheckServerMissionComplete(missionID, onComplete, onFail);
    }
    #endregion
}
