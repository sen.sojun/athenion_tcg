﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MissionUIController : MonoSingleton<MissionUIController>
{
    [Header("Hand Pointer")]
    [SerializeField] private HandPointer _HandPointer_Tap;
    [SerializeField] private HandPointer _HandPointer_DoubleTap;
    [SerializeField] private HandPointer _HandPointer_Move;

    [Header("Masking Area")]
    [SerializeField] private Button _SkipBTN;
    [SerializeField] private AreaMask _AreaMask;
    [SerializeField] private AreaMask _AreaHighlight;

    [Header("Dialog")]
    [SerializeField] private PopupDialog _PopupDialog;
    [SerializeField] private SensorUI _SensorUI;
    [SerializeField] private SensorUI _SensorUITrasnparent;

    [Header("Screen Canvas")]
    [SerializeField] private GameObject _ScreenCanvas;

    private FlexiblePooling _handPointerPool;
    private FlexiblePooling _areaMaskPool;

    protected override void InitSelf()
    {
        _handPointerPool = new FlexiblePooling(this.transform, _HandPointer_Tap.gameObject, 2);
        _areaMaskPool = new FlexiblePooling(this.transform, _AreaMask.gameObject, 2);
        base.InitSelf();
        HideAllUI();
    }

    public Button GetSkipBTN()
    {
        return _SkipBTN;
    }

    public AreaMask GetAreaMask()
    {
        return _AreaMask;
    }


    public AreaMask GetAreaHighlight()
    {
        return _AreaMask;
    }

    public FlexiblePooling GetAreaMaskPool()
    {
        return _areaMaskPool;

    }

    public FlexiblePooling GetHandPointerPool()
    {
        return _handPointerPool;
    }

    public HandPointer GetHandPointer_Tap()
    {
        return _HandPointer_Tap;
    }

    public HandPointer GetHandPointer_Double()
    {
        return _HandPointer_DoubleTap;
    }

    public HandPointer GetHandPointer_Move()
    {
        return _HandPointer_Move;
    }

    public PopupDialog GetPopupDialog()
    {
        return _PopupDialog;
    }

    public SensorUI GetSensorUI()
    {
        return _SensorUI;
    }

    public SensorUI GetSensorUITransparent()
    {
        return _SensorUITrasnparent;
    }

    public void HideAllUI()
    {
        _areaMaskPool.HideAllObject();
        _handPointerPool.HideAllObject();

        //Hide All UI.
        foreach (Transform item in _ScreenCanvas.transform)
        {
            Hidable hidable = item.GetComponent<Hidable>();
            if (hidable != null)
            {
                hidable.HideUI();
            }
            else
            {
                item.gameObject.SetActive(false);
            }
        }
    }
}
