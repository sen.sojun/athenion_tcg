﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Events;
using Spine.Unity;

public class PopupDialog : MonoBehaviour, Showable, Hidable
{

    #region Inspector
    [SerializeField] private TextMeshProUGUI _TEXT_Message;
    [SerializeField] private SkeletonGraphic _SPINE_Character;
    #endregion

    #region Private Properties
    private List<string> _dialogList = new List<string>();
    int index = 0;
    #endregion

    #region Methods
    public void ShowUI()
    {
        index = 0;
        GameHelper.UITransition_FadeIn(this.gameObject, null);
    }

    public void ShowUI(List<string> message, Vector2 position)
    {
        _dialogList = new List<string>(message);

        SetPosition(position);
        ShowUI();
        NextDialog();
    }

    public void ShowUI(List<string> message, ScreenAnchor screenAnchor)
    {
        _dialogList = new List<string>(message);

        SetPosition(screenAnchor);
        ShowUI();
        NextDialog();
    }

    public void ShowUI(string message, ScreenAnchor screenAnchor)
    {
        _dialogList = new List<string>() { message };

        SetPosition(screenAnchor);
        ShowUI();
        NextDialog();
    }

    public void HideUI()
    {
        gameObject.SetActive(false);
    }

    #endregion

    #region Dialog
    private void SetMessage(string message, bool isAnimate = false)
    {
        _TEXT_Message.text = message;

        if (isAnimate) GameHelper.UITextTypingEffect(_TEXT_Message);

    }

    public bool NextDialog()
    {
        if (index < _dialogList.Count)
        {
            SetMessage(_dialogList[index]);
            index++;
            return true;
        }
        else
        {
            HideUI();
            return false;
        }

    }
    #endregion

    #region Position
    public void SetPosition(Vector2 position)
    {
        transform.position = position;
    }

    public void SetPosition(ScreenAnchor anchor)
    {
        RectTransform rect = GetComponent<RectTransform>();
        Vector2 sizeOffset = new Vector2(rect.sizeDelta.x, rect.sizeDelta.y);
        Vector2 anchorPt = GameHelper.RectUI_GetScreenAnchor(anchor);

        // Set Anchor and Position
        rect.anchorMin = anchorPt;
        rect.anchorMax = anchorPt;
        rect.anchoredPosition = new Vector2(sizeOffset.x * (0.5f - anchorPt.x), sizeOffset.y * (0.5f - anchorPt.y));

        // Flip Character 
        _SPINE_Character.transform.localRotation = new Quaternion(0, anchorPt.x == 1 ? 180 : 0, 0, 0);

    }

    #endregion

}
