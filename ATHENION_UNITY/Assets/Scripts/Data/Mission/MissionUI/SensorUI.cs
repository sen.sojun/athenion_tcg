﻿using UnityEngine;
using UnityEngine.EventSystems;
using System;
using UnityEngine.UI;

public class SensorUI : MonoBehaviour, IPointerClickHandler, Showable, Hidable
{
    public const float DEFAULT_DELAY = 0.5f;

    private event Action _onClick;
    private float _delay;
    private DateTime _clickDateStamp;

    [SerializeField] private Image _image;
    [SerializeField] private Color _defaultColor;

    public void HideUI()
    {
        _onClick = null;

        gameObject.SetActive(false);
    }

    public void ShowUI()
    {
        SetDelay(DEFAULT_DELAY);
        gameObject.SetActive(true);
        _clickDateStamp = DateTimeData.GetDateTimeUTC();
    }

    public void SetDelay(float delay)
    {
        _delay = delay;
    }

    public void AddListener(Action onClick)
    {
        _onClick += onClick;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        TimeSpan time = DateTimeData.GetDateTimeUTC() - _clickDateStamp;
        if (time.TotalSeconds > _delay)
        {
            _onClick?.Invoke();
        }
    }

    public void SetColor(Color? color)
    {
        _image.color = color == null ? _defaultColor : (Color)color;
    }
}
