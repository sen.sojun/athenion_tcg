﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class AreaMask : MonoBehaviour, Showable, Hidable
{
    #region Inspector
    [SerializeField] private RectTransform Rect_Target;
    [SerializeField] private Image _image;

    [SerializeField] private Color _defaultColor;
    #endregion

    #region Methods
    Sequence _sequence;
    public void ShowUI()
    {
        //if (_sequence != null && _sequence.IsPlaying())
        //{
        //    _sequence.Kill();
        //}
        //_sequence = DOTween.Sequence();
        //_sequence = GameHelper.UITransition_FadeIn(this.gameObject);
        gameObject.SetActive(true);
    }

    public void ShowUI(Vector2 position, Vector2 size)
    {
        SetPosition(position, true);
        SetSize(size, true);
        ShowUI();
    }

    public void HideUI()
    {
        //GameHelper.UITransition_FadeOut(this.gameObject);
        SetColor(_defaultColor);
        gameObject.SetActive(false);
    }

    public void SetPosition(Vector2 position, bool isAnimate = false)
    {
        float duration = 0.5f;
        if (isAnimate)
        {
            Rect_Target.position = Vector2.zero;
            Rect_Target.DOMove(position, duration).SetEase(Ease.OutExpo);
        }
        else
        {
            Rect_Target.position = position;
        }

    }

    public void SetSize(Vector2 size, bool isAnimate = false)
    {
        float duration = 0.5f;
        size = new Vector2(size.x + 50, size.y + 50);
        if (isAnimate)
        {
            Rect_Target.sizeDelta = Vector2.zero;
            Rect_Target.DOSizeDelta(size, duration).SetEase(Ease.OutExpo);
        }
        else
        {
            Rect_Target.sizeDelta = size;
        }

    }

    public void SetColor(Color? color)
    {
        _image.color = color == null ? _defaultColor : (Color)color;
    }
    #endregion

}
