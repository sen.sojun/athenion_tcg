﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandPointer : MonoBehaviour, Hidable
{
    public RectTransform HandUI;

    private Tweener _tw;

    private void OnDisable()
    {
        _tw?.Kill();
    }

    public bool IsShowing()
    {
        return gameObject.activeSelf;
    }

    public void ShowUI()
    {
        gameObject.SetActive(true);
    }

    public void ShowMoveTo(Vector2 startPos, Vector2 endPos, float duration = 2.0f, int loop = -1)
    {
        _tw?.Kill();
        HandUI.transform.position = startPos;

        _tw = HandUI.DOMove(endPos, duration).SetEase(Ease.InOutSine).SetLoops(loop);
    }

    public void ShowAtWorldPos(Vector2 worldPos)
    {
        HandUI.transform.position = worldPos;
    }

    public void ShowAtScreenPos(Vector2 screenPos)
    {
        HandUI.anchoredPosition = screenPos;
    }

    public void HideUI()
    {
        gameObject.SetActive(false);
    }

}
