﻿using MissionCommandData;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace MissionController
{
    public abstract class Controller
    {
        #region Events
        public static event Action OnEnterMissionEvent;
        public static event Action OnExitMissionEvent;
        public static event Action OnSkipMissionEvent;
        #endregion

        protected MissionManager _manager;

        //protected List<MissionCommand> CommandList = new List<MissionCommand>();

        private MissionCommand _currentCommand;
        private Coroutine _executeCMDProcress;
        private Coroutine _executeCMDListProcess;

        public Controller(MissionManager manager)
        {
            this._manager = manager;
        }

        public void OnEnterMission()
        {
            OnEnter();
        }

        public void OnExitMission()
        {
            OnExit();

            if (_executeCMDListProcess != null)
            {
                _manager.StopCoroutine(_executeCMDListProcess);
            }

            if (_currentCommand != null)
            {
                _currentCommand.OnExit();
                _manager.StopCoroutine(_currentCommand.Execute());
            }

        }

        protected abstract void OnEnter();
        protected abstract void OnExit();

        #region Methods

        protected void StartExecuteCommandList(List<MissionCommand> commandList, UnityAction onComplete = null)
        {
            _executeCMDListProcess = _manager.StartCoroutine(RunCommandList(commandList, onComplete));
        }

        protected IEnumerator RunCommandList(List<MissionCommand> commandList, UnityAction onComplete = null)
        {
            while (commandList != null && commandList.Count > 0)
            {
                if (commandList[0] == null)
                {
                    commandList.RemoveAt(0);
                    continue;
                }
                bool isFinish = false;
                StartExecuteCommand(commandList[0], () => isFinish = true);
                yield return new WaitUntil(() => isFinish == true);
                commandList.RemoveAt(0);
            }
            //ClearCommandList();
            onComplete?.Invoke();
            _executeCMDListProcess = null;
        }

        protected void StartExecuteCommand(MissionCommand command, UnityAction onComplete = null)
        {
            if (_currentCommand != null)
            {
                _currentCommand.OnExit();
            }
            if (_executeCMDProcress != null)
            {
                _manager.StopCoroutine(_executeCMDProcress);
                _executeCMDProcress = null;
            }
            _currentCommand = command;
            _currentCommand.OnEnter();
            Debug.Log("isCanPlay = " + _currentCommand.IsCanPlay);
            if (_currentCommand.IsCanPlay)
            {
                _currentCommand.SendSeenMission();
                ExecuteCurrentCommand(onComplete);
            }
            else
            {
                _currentCommand.OnExit();
                _currentCommand = null;
                onComplete?.Invoke();
            }
        }

        protected void ExecuteCurrentCommand(UnityAction onComplete = null)
        {
            OnEnterMissionEvent?.Invoke();
            _executeCMDProcress = _manager.StartCoroutine(_currentCommand.Execute(delegate
            {
                OnExitMissionEvent?.Invoke();
                _currentCommand.OnExit();
                SetActiveSkipButton(false);
                _currentCommand = null;
                _executeCMDProcress = null;
                onComplete?.Invoke();
            }));
            SetActiveSkipButton(_currentCommand.IsCanSkip);
        }

        protected void SetActiveSkipButton(bool isCanSkip)
        {
            Button skipBTN = MissionUIController.Instance.GetSkipBTN();
            skipBTN.onClick.RemoveAllListeners();
            skipBTN.onClick.AddListener(OnSkipMission);
            skipBTN.gameObject.SetActive(isCanSkip);
        }

        public void OnSkipMission()
        {
            string title = LocalizationManager.Instance.GetText("MF_SKIP");
            string message = LocalizationManager.Instance.GetText("MF_SKIP_DESCRIPTION");
            PopupUIManager.Instance.ShowPopup_SureCheck_Mary(title, message, delegate
            {
                if (_executeCMDProcress != null)
                {
                    _manager.StopCoroutine(_executeCMDProcress);
                    _executeCMDProcress = null;
                }
                if (_currentCommand != null)
                {
                    _currentCommand.OnForceStopProcess();
                    _currentCommand = null;
                }
                OnSkipMissionEvent?.Invoke();
                MissionUIController.Instance.HideAllUI();
            },
            null);

            //ClearCommandList();
        }

        //private void ClearCommandList()
        //{
        //    CommandList.Clear();
        //}
        #endregion

    }
}