﻿using System.Collections;
using System.Collections.Generic;
using MissionCommandData;
using UnityEngine;
using UnityEngine.Events;

namespace MissionCommandData
{
    public class MF_05 : MissionCommand
    {
        #region Protected & Private Properties
        protected override List<string> _missionKeys => new List<string>() { "M0014" };
        private List<string> _textList = new List<string>
        {
            LocalizationManager.Instance.GetText("MF_05_0001")
            ,LocalizationManager.Instance.GetText("MF_05_0002")
            ,LocalizationManager.Instance.GetText("MF_05_0003")
        };
        #endregion

        #region Constructor
        public MF_05(MissionManager manager) : base(manager)
        {
        }
        #endregion

        #region Methods

        public override void OnEnter()
        {
            // Not Used.
        }

        public override IEnumerator Execute(UnityAction onComplete = null)
        {
            yield return RunDialogList(_textList, ScreenAnchor.BotRight, new Color(0, 0, 0, 0.7f));

            onComplete?.Invoke();
        }

        public override void OnExit()
        {
            CloseDialog();
            HideAllAreaMask();
        }

        public override void OnForceStopProcess()
        {
            base.OnForceStopProcess();
            OnExit();
        }
        #endregion
    }
}
