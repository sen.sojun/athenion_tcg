﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace MissionCommandData
{
    /// <summary>
    /// Shop -> Promotion mission.
    /// </summary>
    public class MF_07 : MissionCommand
    {
        protected override List<string> _missionKeys => new List<string>() { "M0016", "M0017" };

        private UnityAction _cacheOnComplete;
        private bool _isNextFlowEditAvatar = false;

        public MF_07(MissionManager manager) : base(manager)
        {
        }

        public override void OnEnter()
        {
            AvatarEditorPanelUI.OnActive += SetNextFlowIsEditAvatar;
            TitleEditorPanelUI.OnActive += SetNextflowIsEditTitle;
        }

        public override void OnExit()
        {
            AvatarEditorPanelUI.OnActive -= SetNextFlowIsEditAvatar;
            TitleEditorPanelUI.OnActive -= SetNextflowIsEditTitle;
            HomeManager.Instance.HomeUIManager.EditProfilePanel.BTN_EditName.enabled = true;
            HomeManager.Instance.HomeUIManager.EditProfilePanel.AvatarPanelUI.Container_Avatar.parent.parent.GetComponent<ScrollRect>().enabled = true;
            HomeManager.Instance.HomeUIManager.EditProfilePanel.TitlePanelUI.Container_Title.parent.parent.GetComponent<ScrollRect>().enabled = true;
        }

        public override void OnForceStopProcess()
        {
            base.OnForceStopProcess();
            OnExit();
        }

        public override IEnumerator Execute(UnityAction onComplete)
        {
            _cacheOnComplete = onComplete;

            List<string> dialog = new List<string>
                {
                    LocalizationManager.Instance.GetText("MF_07_0001"),
                    LocalizationManager.Instance.GetText("MF_07_0002"),
                    LocalizationManager.Instance.GetText("MF_07_0003")
                };
            yield return RunDialogList(dialog, ScreenAnchor.MidCenter);
            HomeManager.Instance.HomeUIManager.EditProfilePanel.BTN_EditName.enabled = false;
            yield return ForceClickAnyButtonsInArea(new List<Button> { HomeManager.Instance.HomeUIManager.EditProfilePanel.BTN_EditAvatar, HomeManager.Instance.HomeUIManager.EditProfilePanel.BTN_EditTitle }, HomeManager.Instance.HomeUIManager.EditProfilePanel.PlayerSettingPanel.GetComponent<RectTransform>());
            HomeManager.Instance.HomeUIManager.EditProfilePanel.BTN_EditName.enabled = true;
            if (_isNextFlowEditAvatar)
            {
                yield return EditAvatarFlowCoroutine();
            }
            else
            {
                yield return EditTitleFlowCoroutine();
            }
        }

        private void SetNextFlowIsEditAvatar()
        {
            _isNextFlowEditAvatar = true;
        }

        private IEnumerator EditAvatarFlowCoroutine()
        {
            Button cell = HomeManager.Instance.HomeUIManager.EditProfilePanel.AvatarPanelUI.Container_Avatar.GetChild(1).GetComponent<Button>();
            yield return null; //Wait for cell in element to update position
            HomeManager.Instance.HomeUIManager.EditProfilePanel.AvatarPanelUI.Container_Avatar.parent.parent.GetComponent<ScrollRect>().enabled = false;
            yield return ForceClickAnyButtonsInArea(new List<Button> { cell }, cell.GetComponent<RectTransform>());
            HomeManager.Instance.HomeUIManager.EditProfilePanel.AvatarPanelUI.Container_Avatar.parent.parent.GetComponent<ScrollRect>().enabled = true;
            List<string> dialog = new List<string>
                {
                    LocalizationManager.Instance.GetText("MF_07_0004")
                };
            yield return RunDialogList(dialog, ScreenAnchor.TopCenter);
            _cacheOnComplete?.Invoke();
        }

        private void SetNextflowIsEditTitle()
        {
            _isNextFlowEditAvatar = false;
        }

        private IEnumerator EditTitleFlowCoroutine()
        {
            Button cell = HomeManager.Instance.HomeUIManager.EditProfilePanel.TitlePanelUI.Container_Title.GetChild(1).GetComponent<Button>();
            yield return null; //Wait for cell in element to update position
            HomeManager.Instance.HomeUIManager.EditProfilePanel.TitlePanelUI.Container_Title.parent.parent.GetComponent<ScrollRect>().enabled = false;
            yield return ForceClickAnyButtonsInArea(new List<Button> { cell }, cell.GetComponent<RectTransform>());
            HomeManager.Instance.HomeUIManager.EditProfilePanel.TitlePanelUI.Container_Title.parent.parent.GetComponent<ScrollRect>().enabled = true;
            List<string> dialog = new List<string>
                {
                    LocalizationManager.Instance.GetText("MF_07_0004")
                };
            yield return RunDialogList(dialog, ScreenAnchor.TopCenter);
            _cacheOnComplete?.Invoke();
        }
    }

}
