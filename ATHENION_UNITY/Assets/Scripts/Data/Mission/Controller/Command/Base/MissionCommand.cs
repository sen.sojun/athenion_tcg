﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace MissionCommandData
{
    public abstract class MissionCommand
    {
        #region Public Properties
        /// <summary>
        /// if is success is true. the Execute() will not invoke.
        /// </summary>
        public virtual bool IsCanPlay { get { return IsMissionCanPlay(); } }
        public virtual bool IsCanSkip { get; protected set; }
        #endregion

        #region Protected Properties
        protected abstract List<string> _missionKeys { get; }
        protected MissionManager _manager { get; }
        protected PlayerMissionData _playerMissionData { get { return DataManager.Instance.PlayerMission; } }
        #endregion

        #region Private Properties
        private Button _cacheInsterestedBtn;
        private List<Button> _cacheInsterestedBtnList = new List<Button>();
        private UnityAction _cacheOnClickBtn;
        private List<UnityAction> _cacheOnClickBtnList = new List<UnityAction>();
        #endregion

        public MissionCommand(MissionManager manager)
        {
            this._manager = manager;
            IsCanSkip = true;
        }

        /// <summary>
        /// will automaticaly call no matter IsCanPlay is true or not.
        /// </summary>
        public abstract void OnEnter();

        public abstract IEnumerator Execute(UnityAction onComplete = null);

        public void SendSeenMission(UnityAction onComplete = null)
        {
            _manager.StartCoroutine(IESendSeenMission(onComplete));
        }
        private IEnumerator IESendSeenMission(UnityAction onComplete)
        {
            //PopupUIManager.Instance.Show_MidSoftLoad(true);
            for (int i = 0; i < _missionKeys.Count; i++)
            {
                bool isFinish = false;
                DataManager.Instance.PlayerMission.SeenMission(_missionKeys[i], () => isFinish = true);
                yield return new WaitUntil(() => isFinish == true);
            }
            //PopupUIManager.Instance.Show_MidSoftLoad(false);
            onComplete?.Invoke();
        }

        public abstract void OnExit();

        protected virtual bool IsMissionCanPlay()
        {
            foreach (string id in _missionKeys)
            {
                if (_playerMissionData.IsMissionCanPlay(id) == false)
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Skip process.
        /// </summary>
        public virtual void OnForceStopProcess()
        {
            if (_cacheInsterestedBtn != null && _cacheOnClickBtn != null)
            {
                _cacheInsterestedBtn.onClick.RemoveListener(_cacheOnClickBtn);
            }
            for (int i = 0; i < _cacheOnClickBtnList.Count; i++)
            {
                _cacheInsterestedBtnList[i].onClick.RemoveListener(_cacheOnClickBtnList[i]);
            }
        }

        protected IEnumerator RunDialogList(List<string> dialog, ScreenAnchor screenAnchor, Color? color = null)
        {
            //Set dialog
            PopupDialog popupDialog = MissionUIController.Instance.GetPopupDialog();
            popupDialog.ShowUI(dialog, screenAnchor);

            do
            {
                yield return WaitToClick(color);
            } while (popupDialog.NextDialog());
            yield return null;
        }

        protected IEnumerator RunDialog(string dialog, ScreenAnchor screenAnchor, Color? color = null)
        {
            //Set dialog
            PopupDialog popupDialog = MissionUIController.Instance.GetPopupDialog();
            popupDialog.ShowUI(dialog, screenAnchor);

            do
            {
                yield return WaitToClick(color);
            } while (popupDialog.NextDialog());
            yield return null;
        }

        protected void OpenDialog(List<string> dialog, ScreenAnchor screenAnchor, Color? color = null)
        {
            //Set dialog
            PopupDialog popupDialog = MissionUIController.Instance.GetPopupDialog();
            popupDialog.ShowUI(dialog, screenAnchor);
        }

        protected void OpenDialog(string dialog, ScreenAnchor screenAnchor, Color? color = null)
        {
            //Set dialog
            PopupDialog popupDialog = MissionUIController.Instance.GetPopupDialog();
            popupDialog.ShowUI(dialog, screenAnchor);
        }

        protected void NextDialog()
        {
            PopupDialog popupDialog = MissionUIController.Instance.GetPopupDialog();
            popupDialog.NextDialog();
        }

        protected void CloseDialog()
        {
            PopupDialog popupDialog = MissionUIController.Instance.GetPopupDialog();
            popupDialog.HideUI();
        }

        protected IEnumerator WaitToClick(Color? color = null)
        {
            MissionUIController ui = MissionUIController.Instance;
            //Set Sensor
            bool isClick = false;
            SensorUI sensor = ui.GetSensorUI();
            sensor.SetColor(color);

            sensor.ShowUI();
            sensor.AddListener(() => isClick = true);
            yield return new WaitUntil(() => isClick == true);

            sensor.HideUI();
        }

        protected IEnumerator WaitToClick_Transparent()
        {
            MissionUIController ui = MissionUIController.Instance;
            //Set Sensor
            bool isClick = false;
            SensorUI sensor = ui.GetSensorUITransparent();

            sensor.ShowUI();
            sensor.AddListener(() => isClick = true);
            yield return new WaitUntil(() => isClick == true);

            sensor.HideUI();
        }

        protected IEnumerator HilightClickButton(Button button)
        {
            bool isClick = false;
            _cacheInsterestedBtn = button;
            _cacheOnClickBtn = () => isClick = true;

            HandPointer handPointer = MissionUIController.Instance.GetHandPointer_Tap();
            handPointer.ShowUI();
            Vector2 targetPos = button.GetComponent<RectTransform>().position;
            handPointer.ShowAtWorldPos(targetPos);

            button.onClick.AddListener(_cacheOnClickBtn);
            yield return new WaitUntil(() => isClick == true);
            button.onClick.RemoveListener(_cacheOnClickBtn);

            handPointer.HideUI();
        }

        protected IEnumerator ForceClickButton(Button button, Color? color = null)
        {
            bool isClick = false;
            _cacheInsterestedBtn = button;
            _cacheOnClickBtn = () => isClick = true;

            HandPointer handPointer = MissionUIController.Instance.GetHandPointer_Tap();
            handPointer.ShowUI();
            Vector2 targetPos = button.GetComponent<RectTransform>().position;
            handPointer.ShowAtWorldPos(targetPos);

            AreaMask areaMask = MissionUIController.Instance.GetAreaMask();
            areaMask.ShowUI();
            areaMask.SetSize(button.GetComponent<RectTransform>().rect.size);
            areaMask.SetPosition(targetPos);
            areaMask.SetColor(color);

            button.onClick.AddListener(_cacheOnClickBtn);
            yield return new WaitUntil(() => isClick == true);
            button.onClick.RemoveListener(_cacheOnClickBtn);

            handPointer.HideUI();
            areaMask.HideUI();
        }

        protected IEnumerator ForceClickButtonInArea(Button button, RectTransform targetTransform, Color? color = null)
        {
            FlexiblePooling areaMaskPool = MissionUIController.Instance.GetAreaMaskPool();
            AreaMask areaMask = areaMaskPool.GetObject().GetComponent<AreaMask>();

            areaMask.ShowUI();
            areaMask.SetSize(targetTransform.rect.size);
            areaMask.SetPosition(targetTransform.position);
            areaMask.SetColor(color);

            yield return ForceClickButton(button, new Color(0, 0, 0, 0));
            areaMaskPool.HideAllObject();
        }

        protected IEnumerator ForceClickAnyButtonsInArea(List<Button> buttonList, RectTransform targetTransform, Color? color = null)
        {
            for (int i = 0; i < _cacheOnClickBtnList.Count; i++)
            {
                _cacheInsterestedBtnList[i].onClick.RemoveListener(_cacheOnClickBtnList[i]);
            }
            _cacheOnClickBtnList.Clear();
            _cacheInsterestedBtnList.Clear();
            bool isClick = false;
            FlexiblePooling handPool = MissionUIController.Instance.GetHandPointerPool();
            for (int i = 0; i < buttonList.Count; i++)
            {
                _cacheOnClickBtnList.Add(() => isClick = true);
                buttonList[i].onClick.AddListener(_cacheOnClickBtnList[i]);
                _cacheInsterestedBtnList.Add(buttonList[i]);

                HandPointer aHand = handPool.GetObject().GetComponent<HandPointer>();
                aHand.ShowUI();
                Vector2 targetPos = buttonList[i].GetComponent<RectTransform>().position;
                aHand.ShowAtWorldPos(targetPos);
            }

            ForceEnableInArea(targetTransform, color);
            yield return new WaitUntil(() => isClick == true);
            Debug.Log("isClick = true");
            for (int i = 0; i < _cacheOnClickBtnList.Count; i++)
            {
                _cacheInsterestedBtnList[i].onClick.RemoveListener(_cacheOnClickBtnList[i]);
            }
            _cacheOnClickBtnList.Clear();
            _cacheInsterestedBtnList.Clear();
            handPool.HideAllObject();
            HideAllAreaMask();

        }

        protected IEnumerator ForceClickAnyButtonsNoPointerInArea(List<Button> buttonList, RectTransform targetTransform, Color? color = null)
        {
            bool isClick = false;
            for (int i = 0; i < buttonList.Count; i++)
            {
                _cacheOnClickBtnList.Add(() => isClick = true);
                buttonList[i].onClick.AddListener(_cacheOnClickBtnList[i]);
                _cacheInsterestedBtnList.Add(buttonList[i]);
            }

            ForceEnableInArea(targetTransform, color);
            yield return new WaitUntil(() => isClick == true);
            for (int i = 0; i < _cacheOnClickBtnList.Count; i++)
            {
                _cacheInsterestedBtnList[i].onClick.RemoveListener(_cacheOnClickBtnList[i]);
            }

            _cacheOnClickBtnList.Clear();
            _cacheInsterestedBtnList.Clear();
            HideAllAreaMask();
        }

        protected IEnumerator IEShowHilightInArea(RectTransform rectTransform, Color? color = null)
        {
            AreaMask areaMask = MissionUIController.Instance.GetAreaHighlight();
            areaMask.ShowUI();
            areaMask.SetSize(rectTransform.rect.size);
            areaMask.SetPosition(rectTransform.position);
            areaMask.SetColor(color);

            yield return WaitToClick_Transparent();
            areaMask.HideUI();
        }

        protected IEnumerator IEForceEnableInArea(RectTransform rectTransform, Color? color = null)
        {
            AreaMask areaMask = MissionUIController.Instance.GetAreaMask();
            areaMask.ShowUI();
            areaMask.SetSize(rectTransform.rect.size);
            areaMask.SetPosition(rectTransform.position);
            areaMask.SetColor(color);

            yield return WaitToClick(color);
            areaMask.HideUI();
        }

        protected void ForceEnableInArea(RectTransform rectTransform, Color? color = null)
        {
            AreaMask areaMask = MissionUIController.Instance.GetAreaMask();
            areaMask.ShowUI();
            areaMask.SetSize(rectTransform.rect.size);
            areaMask.SetPosition(rectTransform.position);
            areaMask.SetColor(color);
        }

        protected void HideAllAreaMask()
        {
            AreaMask areaMask = MissionUIController.Instance.GetAreaMask();
            areaMask.HideUI();
        }
    }
}
