﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace MissionCommandData
{
    public class MF_04 : MissionCommand
    {
        #region Protected Properties
        protected override List<string> _missionKeys => new List<string>() { "M0008" };
        #endregion

        #region Private Properties
        private bool _isCanContinue = false;

        private readonly string _dialog_1 = LocalizationManager.Instance.GetText("MF_04_0001");
        private readonly string _dialog_2 = LocalizationManager.Instance.GetText("MF_04_0002");
        private readonly List<string> _dialog_3 = new List<string>() {
            LocalizationManager.Instance.GetText("MF_04_0003")
            , LocalizationManager.Instance.GetText("MF_04_0004")
            , LocalizationManager.Instance.GetText("MF_04_0005") };
        #endregion

        #region Contructors
        public MF_04(MissionManager manager) : base(manager)
        {

        }
        #endregion

        #region Methods
        public override IEnumerator Execute(UnityAction onComplete = null)
        {
            CollectionMissionUIContainer uiContainer = CollectionManager.Instance.MissionUIContainer;
            OpenDialog(_dialog_1, ScreenAnchor.TopLeft);
            yield return ForceClickButton(uiContainer.BTN_Filter_CraftCard);
            NextDialog();

            CollectionFilterManager.Instance.AddFilterParam(CollectionFilterTypes.Series, SeriesKeyFilterTypes.WOE);
            CollectionFilterManager.Instance.AddFilterParam(CollectionFilterTypes.Foil, FoilFilterTypes.Standard.ToString());
            CollectionManager.Instance.RequestUpdateCollectionFilter();
            _isCanContinue = false;
            OpenDialog(_dialog_2, ScreenAnchor.BotLeft);
            ForceEnableInArea(uiContainer.AREA_CardCollection);
            yield return new WaitUntil(() => _isCanContinue == true);
            for (int i = 0; i < 20; i++)
            {
                yield return new WaitForEndOfFrame();
            }
            HideAllAreaMask();

            OpenDialog(_dialog_3, ScreenAnchor.MidCenter);
            yield return IEShowHilightInArea(uiContainer.AREA_CardDetail_Craft);

            NextDialog();
            yield return IEShowHilightInArea(uiContainer.AREA_CardDetail_Recycle);

            NextDialog();
            yield return IEForceEnableInArea(uiContainer.AREA_CardDetail_Close);

            NextDialog();
            CollectionFilterManager.Instance.ResetFilter(CollectionFilterTypes.Series);
            CollectionFilterManager.Instance.ResetFilter(CollectionFilterTypes.Foil);
            CollectionManager.Instance.RequestUpdateCollectionFilter();

            onComplete?.Invoke();
        }

        private void InitEvents()
        {
            CollectionManager.OnShowCardDetailComplete += OnShowCardDetail;
        }

        private void DeinitEvents()
        {
            CollectionManager.OnShowCardDetailComplete -= OnShowCardDetail;
        }

        private void OnShowCardDetail()
        {
            _isCanContinue = true;
        }

        public override void OnEnter()
        {
            if (IsCanPlay == false)
            {
                PlayerStatManager.Instance.DoCreateNewDeck();
            }
            InitEvents();
        }

        public override void OnExit()
        {
            CloseDialog();
            HideAllAreaMask();
            DeinitEvents();
        }

        public override void OnForceStopProcess()
        {
            base.OnForceStopProcess();
            OnExit();
        }
        #endregion
    }
}