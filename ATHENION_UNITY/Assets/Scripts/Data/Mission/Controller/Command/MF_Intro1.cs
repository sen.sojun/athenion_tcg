﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Object = System.Object;

namespace MissionCommandData
{
    public class MF_Intro1 : MissionCommand
    {
        #region Protected & Private Properties
        protected override List<string> _missionKeys => new List<string>() { "M0002" };


        private readonly MissionListUIController _missionListUIController = HomeManager.Instance.HomeUIManager.MissionPanel;
        private string _missionTargetCellID = "M0003";
        #endregion

        private UnityAction _onComplete;

        public MF_Intro1(MissionManager manager) : base(manager)
        {
        }

        public override void OnEnter()
        {
            _missionListUIController.SetEnableScrollRect(false);
        }

        public override void OnExit()
        {
            //HandPointer handPointer = MissionUIController.Instance.GetHandPointer_Tap();
            //handPointer.HideUI();

            _missionListUIController.SetEnableScrollRect(true);

            CloseDialog();
            HideAllAreaMask();
        }

        public override IEnumerator Execute(UnityAction onComplete = null)
        {
            _onComplete = onComplete;
            Color color = new Color(0, 0, 0, 0.7f);
            var missionList = DataManager.Instance.PlayerMission.MissionList;

            MissionData data = null;
            foreach (MissionData missionData in missionList)
            {
                if (missionData.ID == _missionTargetCellID)
                    data = missionData;
            }

            if (data == null)
            {
                onComplete?.Invoke();
                yield break;
            }

            bool isCanGo = data.IsCompleted == false;
            List<string> textList = GetLocalizeList(isCanGo);

            OpenDialog(textList, ScreenAnchor.MidRight, color);

            yield return new WaitUntil(() => _missionListUIController.IsReady == true);

            MissionCell missionCell = _missionListUIController.GetMissionCell(_missionTargetCellID);
            RectTransform area = missionCell.GetComponent<RectTransform>();
            if (isCanGo)
            {
                yield return IEShowHilightInArea(area, color);
                NextDialog();

            
                yield return ForceClickAnyButtonsInArea(new List<Button> { missionCell.GetGoButton() }, area, color);
                NextDialog();
            }
            else
            {
                yield return IEShowHilightInArea(area, color);
                NextDialog();


                yield return ForceClickAnyButtonsInArea(new List<Button> { missionCell.GetClaimButton() }, area, color);
                NextDialog();
                onComplete?.Invoke();
            }

        }

        private List<string> GetLocalizeList(bool isCanGo)
        {
            if (isCanGo)
            {
                return new List<string>
                {
                    LocalizationManager.Instance.GetText("MF_INTRO02_0001")
                    ,LocalizationManager.Instance.GetText("MF_INTRO02_0002")
                };
            }
            else
            {
                return new List<string>
                {
                    LocalizationManager.Instance.GetText("MF_INTRO02_0001")
                    ,LocalizationManager.Instance.GetText("MF_INTRO02_0003")
                };
            }
        }

        public override void OnForceStopProcess()
        {
            _onComplete?.Invoke();
            base.OnForceStopProcess();
            OnExit();
            // if skip must hide the mission popup.
            _missionListUIController.HideUI();
        }
    }

}
