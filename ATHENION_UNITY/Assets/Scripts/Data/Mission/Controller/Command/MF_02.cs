﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Karamucho.Collection;
using UnityEngine.UI;
using System;
using System.Linq;

namespace MissionCommandData
{
    public class MF_02 : MissionCommand
    {
        #region Protected Properties
        protected override List<string> _missionKeys => new List<string>() { "M0004" };
        #endregion

        #region Private Properties
        private DropZoneName _dropZone = DropZoneName.None;
        private DropZoneName _lastDropZone = DropZoneName.None;
        private bool _isDragToCard = false;
        private bool _isDragToDeck = false;
        private bool _isSaveDeckConfirm = false;
        private bool _isSaveDeckCancel = false;

        private readonly string _dialog_1 = LocalizationManager.Instance.GetText("MF_02_0001");
        //private readonly List<string> _dialog_2 = new List<string>() {
        //    LocalizationManager.Instance.GetText("MF_02_0002")
        //    , LocalizationManager.Instance.GetText("MF_02_0003") };
        //private readonly string _dialog_3 = LocalizationManager.Instance.GetText("MF_02_0004");
        //private readonly string _dialog_4 = LocalizationManager.Instance.GetText("MF_02_0005");
        //private readonly List<string> _dialog_5 = new List<string>() {
        //    LocalizationManager.Instance.GetText("MF_02_0006")
        //    , LocalizationManager.Instance.GetText("MF_02_0007")
        //    , LocalizationManager.Instance.GetText("MF_02_0008")};
        #endregion

        #region Contructors
        public MF_02(MissionManager manager) : base(manager)
        {

        }
        #endregion

        #region Methods
        public override IEnumerator Execute(UnityAction onComplete = null)
        {
            if (IsCollectionCanPlay())
            {
                CollectionMissionUIContainer uiContainer = CollectionManager.Instance.MissionUIContainer;

                OpenDialog(_dialog_1, ScreenAnchor.TopLeft);
                yield return ForceClickButton(uiContainer.BTN_CreateDeck);
                NextDialog();

                //OpenDialog(_dialog_2, ScreenAnchor.TopRight);
                yield return ForceClickButton(uiContainer.BTN_CreateDeck_Faction_Holy);
                //yield return ForceClickAnyButtonsNoPointerInArea(uiContainer.BTN_CreateDeck_Faction, uiContainer.AREA_CreateDeck_FactionList);

                //NextDialog();
                //yield return IEShowHilightInArea(uiContainer.AREA_CreateDeck_DeckPresetList);
                yield return ForceClickButton(uiContainer.BTN_CreateDeck_CustomDeck);
                GameObject deckPresetListObj = CollectionManager.Instance.MissionUIContainer.AREA_CreateDeck_DeckPresetBTNList.gameObject;
                List<Button> buttonList = deckPresetListObj.GetComponentsInChildren<Button>().ToList();
                foreach (Button but in buttonList)
                {
                    but.interactable = false;
                }
                yield return ForceClickButton(uiContainer.BTN_CreateDeck_Confirm);

                //OpenDialog(_dialog_3, ScreenAnchor.MidRight);
                yield return HilightClickButton(uiContainer.BTN_Customize_Confirm);
                //OpenDialog(_dialog_4, ScreenAnchor.TopLeft);
                PopupUIManager.Instance.Show_SoftLoad(true);
                for (int i = 0; i < 20; i++)
                {
                    yield return new WaitForEndOfFrame();
                }
                PopupUIManager.Instance.Show_SoftLoad(false);

                HandPointer handMove = MissionUIController.Instance.GetHandPointer_Move();
                //OpenDialog(_dialog_5, ScreenAnchor.BotLeft);
                ForceEnableInArea(uiContainer.AREA_CardCollection);
                _dropZone = DropZoneName.CollectionCard;
                handMove.ShowUI();
                handMove.ShowMoveTo(uiContainer.AREA_CardCollection.transform.position, uiContainer.AREA_DeckDetail_CardList.transform.position, 1);
                yield return new WaitUntil(() => _isDragToDeck == true);
                //NextDialog();

                //handMove.ShowMoveTo(uiContainer.AREA_DeckDetail_CardList.transform.position, uiContainer.AREA_CardCollection.transform.position, 1);
                //yield return new WaitUntil(() => _isDragToCard == true);
                handMove.HideUI();

                yield return ForceClickButton(uiContainer.BTN_DeckDetail_AutoDeck);
                yield return WaitForSaveDeckButton(uiContainer);
                 
                //HandPointer handDouble = MissionUIController.Instance.GetHandPointer_Double();
                //handDouble.ShowUI();
                //handDouble.ShowAtWorldPos(uiContainer.AREA_CardCollection.transform.position);
                //NextDialog();
                HideAllAreaMask();

                //yield return WaitToClick();
                //NextDialog();
                //handDouble.HideUI();
            }
            else
            {
                PlayerStatManager.Instance.DoCreateNewDeck();
                yield return new WaitForEndOfFrame();
            }

            onComplete?.Invoke();
        }

        private IEnumerator WaitForSaveDeckButton(CollectionMissionUIContainer uiContainer)
        {
            START:
            _isSaveDeckCancel = false;
            yield return ForceClickButton(uiContainer.BTN_DeckDetail_Done);

            while (_isSaveDeckCancel == false)
            { 
                if(_isSaveDeckConfirm == true)
                {
                    yield break;
                }
                yield return null;
            }
            yield return null;
            goto START;
        }

        private void InitEvents()
        {
            CollectionManager.OnActivateFloatingCardFromDeck += EnableCollectionCardArea;
            CollectionManager.OnActivateFloatingCardFromCard += EnableDeckDetailArea;
            CollectionManager.OnUpdateCardDropZone += UpdateCardDropZone;
            CollectionManager.OnUpdateCardZone += UpdateCardZone;
            CollectionManager.OnShowCardDetail += ShowCardDetail;
            CollectionManager.OnShowCardDetailComplete += ShowCardDetail;
            CollectionManager.OnHideCardDetail += HideCardDetail;

            CollectionDeckManager.OnSaveDeckConfirm += SaveDeckConfirm;
            CollectionDeckManager.OnSaveDeckCancel += SaveDeckCancel;

            ScrollRect deckPresetScrollRect = CollectionManager.Instance.MissionUIContainer.AREA_CreateDeck_DeckPresetList.GetComponent<ScrollRect>();
            deckPresetScrollRect.horizontal = false;
        }

        private void DeinitEvents()
        {
            CollectionManager.OnActivateFloatingCardFromDeck -= EnableCollectionCardArea;
            CollectionManager.OnActivateFloatingCardFromCard -= EnableDeckDetailArea;
            CollectionManager.OnUpdateCardDropZone -= UpdateCardDropZone;
            CollectionManager.OnUpdateCardZone -= UpdateCardZone;
            CollectionManager.OnShowCardDetail -= ShowCardDetail;
            CollectionManager.OnShowCardDetailComplete -= ShowCardDetail;
            CollectionManager.OnHideCardDetail -= HideCardDetail;

            CollectionDeckManager.OnSaveDeckConfirm -= SaveDeckConfirm;
            CollectionDeckManager.OnSaveDeckCancel -= SaveDeckCancel;

            ScrollRect deckPresetScrollRect = CollectionManager.Instance.MissionUIContainer.AREA_CreateDeck_DeckPresetList.GetComponent<ScrollRect>();
            deckPresetScrollRect.horizontal = true;

            GameObject deckPresetListObj = CollectionManager.Instance.MissionUIContainer.AREA_CreateDeck_DeckPresetBTNList.gameObject;
            List<Button> buttonList = deckPresetListObj.GetComponentsInChildren<Button>().ToList();
            foreach (Button but in buttonList)
            {
                but.interactable = true;
            }
        }

        private void SaveDeckConfirm()
        {
            _isSaveDeckConfirm = true;
        }

        private void SaveDeckCancel()
        {
            _isSaveDeckCancel = true;
        }

        private void HideCardDetail()
        {
            EnableCurrentCardZone();
        }

        private void ShowCardDetail(string cardID)
        {
            HideAllAreaMask();
        }

        private void ShowCardDetail()
        {
            HideAllAreaMask();
        }

        private void EnableCollectionCardArea()
        {
            HideAllAreaMask();
            ForceEnableInArea(CollectionManager.Instance.MissionUIContainer.AREA_CardCollection);
            _lastDropZone = DropZoneName.CollectionDeckDetail;
        }

        private void EnableDeckDetailArea()
        {
            HideAllAreaMask();
            ForceEnableInArea(CollectionManager.Instance.MissionUIContainer.AREA_DeckDetail_CardList);
            _lastDropZone = DropZoneName.CollectionCard;
        }

        private void UpdateCardDropZone(DropZoneName zoneName)
        {
            _dropZone = zoneName;
            EnableCurrentCardZone();
        }

        private void UpdateCardZone(DropZoneName zoneName, string cardID)
        {
            switch (zoneName)
            {
                case DropZoneName.CollectionCard:
                {
                    _isDragToCard = true;
                }
                break;

                case DropZoneName.CollectionDeckDetail:
                {
                    _isDragToDeck = true;
                }
                break;
            }

            _dropZone = zoneName;
            EnableCurrentCardZone();
        }

        private void EnableCurrentCardZone()
        {
            HideAllAreaMask();
            switch (_dropZone)
            {
                case DropZoneName.CollectionCard:
                {
                    ForceEnableInArea(CollectionManager.Instance.MissionUIContainer.AREA_CardCollection);
                    _isDragToCard = true;
                }
                break;

                case DropZoneName.CollectionDeckDetail:
                {
                    ForceEnableInArea(CollectionManager.Instance.MissionUIContainer.AREA_DeckDetail_CardList);
                }
                break;

                default:
                {
                    switch (_lastDropZone)
                    {
                        case DropZoneName.CollectionCard:
                        {
                            ForceEnableInArea(CollectionManager.Instance.MissionUIContainer.AREA_CardCollection);
                        }
                        break;

                        case DropZoneName.CollectionDeckDetail:
                        {
                            ForceEnableInArea(CollectionManager.Instance.MissionUIContainer.AREA_DeckDetail_CardList);
                        }
                        break;
                    }
                }
                break;
            }
        }

        public override void OnEnter()
        {
            InitEvents();
        }

        public override void OnExit()
        {
            CloseDialog();
            HideAllAreaMask();
            DeinitEvents();
        }

        public override void OnForceStopProcess()
        {
            base.OnForceStopProcess();
            OnExit();
        }

        private bool IsCollectionCanPlay()
        {
            List<string> deckDefaultIDList = new List<string>() { "deck_1", "deck_2", "deck_3", "deck_4", "deck_5", "deck_6" };
            List<DeckData> deckList = DataManager.Instance.GetAllDeck();
            foreach (DeckData data in deckList)
            {
                if (deckDefaultIDList.Contains(data.DeckID) == false)
                {
                    return false;
                }
            }

            return true;
        }
        #endregion
    }
}