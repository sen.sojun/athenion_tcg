﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Karamucho.Collection;
using UnityEngine.UI;
using System;

namespace MissionCommandData
{
    public class MF_06 : MissionCommand
    {
        #region Public Properties
        #endregion

        #region Protected Properties
        protected override List<string> _missionKeys => new List<string>() { "M0011" };
        #endregion

        #region Private Properties
        private readonly List<string> _dialog_1 = new List<string>() {
            LocalizationManager.Instance.GetText("MF_06_0001")
            , LocalizationManager.Instance.GetText("MF_06_0002") };

        private readonly string _dialog_2 = LocalizationManager.Instance.GetText("MF_06_0003");
        private readonly string _dialog_3 = LocalizationManager.Instance.GetText("MF_06_0004");
        #endregion

        #region Contructors
        public MF_06(MissionManager manager) : base(manager)
        {

        }
        #endregion

        #region Methods

        public override IEnumerator Execute(UnityAction onComplete = null)
        {
            //Redirect to Main Quest then show dialog
            SeasonPassUIManager.Instance.BTN_NavMainQ.onClick?.Invoke();

            yield return RunDialogList(_dialog_1, ScreenAnchor.BotRight);
            NextDialog();

            OpenDialog(_dialog_2, ScreenAnchor.MidLeft);
            yield return IEShowHilightInArea(SeasonPassUIManager.Instance.Rect_PanelInfo);
            NextDialog();
            
            yield return ForceClickButton(SeasonPassUIManager.Instance.BTN_NavRewards);
            NextDialog();

            OpenDialog(_dialog_3, ScreenAnchor.BotRight);
            yield return WaitToClick();

            onComplete?.Invoke();
        }

        private void InitEvents()
        {
            //Dont know what to subscribe yet.
            SeasonPassUIManager.OnActive += null;
        }

        private void DeinitEvents()
        {
            //Dont know what to unsubscribe yet.
            SeasonPassUIManager.OnActive -= null;
        }

        public override void OnEnter()
        {
            if (IsCanPlay == false)
            {

            }
            InitEvents();
        }

        public override void OnExit()
        {
            CloseDialog();
            HideAllAreaMask();
            DeinitEvents();
        }

        public override void OnForceStopProcess()
        {
            base.OnForceStopProcess();
            OnExit();
        }

        #endregion
    }
}
