﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Object = UnityEngine.Object;

namespace MissionCommandData
{ 
    public class MF_Intro0 : MissionCommand
    {
        #region Protected & Private Properties
        
        protected override List<string> _missionKeys => new List<string>() { "M0001" };
        private List<string> _textList = new List<string>
        {
            LocalizationManager.Instance.GetText("MF_INTRO01_0001")
            ,LocalizationManager.Instance.GetText("MF_INTRO01_0002")
        };
        private List<string> _testTextList = new List<string>
        {
            "ยินดีต้อนรับเข้าสู่โลกของ Athenion! ฉันคือแมรี่ ผู้ฝึกสอนของเธอ ฉันจะช่วนเธอเรียนรู้สิ่งต่างๆเอง",
            "ลองตรวจดูที่ Mission"
        };
        private HomeMissionUIController _missionUIControl => Object.FindObjectOfType<HomeMissionUIController>();
        #endregion

        #region Public Properties

        public override bool IsCanSkip => false;
        public MF_Intro0(MissionManager manager) : base(manager)
        {
        }

        #endregion

        #region Methods
        public override void OnEnter()
        {
            HomeManager.Instance.HomeUIManager.MissionBTN.SetEnableBTN(false);
        }

        public override void OnExit()
        {
            HandPointer handPointer = MissionUIController.Instance.GetHandPointer_Tap();
            handPointer.HideUI();

            HomeManager.Instance.HomeUIManager.MissionBTN.SetEnableBTN(true);
        }

        public override IEnumerator Execute(UnityAction onComplete = null)
        {
            Color color = new Color(0, 0, 0, 0.7f);

            OpenDialog(_textList, ScreenAnchor.BotRight, color);
            yield return WaitToClick(color);
            NextDialog();

            Button button = _missionUIControl.GetButtonPos(HomeMissionID.M_Intro0);
            yield return ForceClickAnyButtonsInArea(new List<Button> { button }
                , _missionUIControl.GetDialogPos(HomeMissionID.M_Intro0)
                , color);
            NextDialog();

            onComplete?.Invoke();
        }

        #endregion
    }
}
