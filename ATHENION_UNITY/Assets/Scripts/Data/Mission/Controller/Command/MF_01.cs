﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace MissionCommandData
{
    /// <summary>
    /// Shop -> Inventory mission.
    /// </summary>
    public class MF_01 : MissionCommand
    {
        protected override List<string> _missionKeys => new List<string>() { "M0003" };
        private BoosterPackPage boosterPackPage;
        private bool isOpenCardFinish;

        private UnityAction _onTapOpenPack;

        public MF_01(MissionManager manager) : base(manager)
        {
        }

        public override void OnEnter()
        {
            boosterPackPage = ShopManager.Instance.GetPage(ShopTypes.BoosterPack) as BoosterPackPage;
            CardPackController.OnObjectDestroy += CardPackController_OnObjectDestroy;
        }

        public override void OnExit()
        {
            CardPackController.OnObjectDestroy -= CardPackController_OnObjectDestroy;
        }

        private void ShopManager_OnOpenPack(PackReward obj)
        {
            MissionUIController.Instance.HideAllUI();
        }

        private void CardPackController_OnObjectDestroy()
        {
            isOpenCardFinish = true;
        }

        public override IEnumerator Execute(UnityAction onComplete)
        {
            yield return ScrollToWOEPack();
            //Set dialog
            {
                List<string> dialog = new List<string>
                {
                    LocalizationManager.Instance.GetText("MF_01_0001")
                    , LocalizationManager.Instance.GetText("MF_01_0002")
                };
                OpenDialog(dialog, ScreenAnchor.BotLeft);
                yield return WaitToClick();
                NextDialog();
            }
            CardPackUICell packUI = boosterPackPage.GetUI().GetCardPackUICellList().Find(cell => cell.Data.PackID == "CONT_WOE");
            yield return ForceClickButton(packUI.OpenButton);
            CloseDialog();

            UnityEngine.UI.Button confirmButton = boosterPackPage.GetOpenPackPopup()._confirmButton;
            yield return ForceClickButton(confirmButton);

            MissionUIController.Instance.HideAllUI();

            // wait until open pack finish.
            yield return new WaitUntil(() => isOpenCardFinish == true);
            MissionUIController.Instance.GetSkipBTN().gameObject.SetActive(true);

            //Set dialog
            {
                List<string> dialog = new List<string>
                {
                    LocalizationManager.Instance.GetText("MF_01_0003")
                    , LocalizationManager.Instance.GetText("MF_01_0004")
                };
                yield return RunDialogList(dialog, ScreenAnchor.BotLeft);
            }

            onComplete?.Invoke();
        }

        private IEnumerator ScrollToWOEPack()
        {
            ShopItemPage currentPage = ShopManager.Instance.CurrentPage;

            //if (currentPage.UI.ScrollRect.content.rect.size.y < 1400)
            //    yield break;
            bool isFinish = false;
            yield return null;
            currentPage.UI.SetScrollVerticalNormalize(0, () => isFinish = true);
            yield return new WaitUntil(() => isFinish == true);

            Debug.Log("Navigate Finish");
        }

        public override void OnForceStopProcess()
        {
            base.OnForceStopProcess();
            OnExit();
        }
    }
}
