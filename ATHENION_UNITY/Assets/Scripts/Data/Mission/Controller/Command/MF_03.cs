﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace MissionCommandData
{
    /// <summary>
    /// Shop -> Promotion mission.
    /// </summary>
    public class MF_03 : MissionCommand
    {
        protected override List<string> _missionKeys => new List<string>() { "M0006" };

        public MF_03(MissionManager manager) : base(manager)
        {
        }

        public override void OnEnter()
        {
        }

        public override void OnExit()
        {
            HandPointer handPointer = MissionUIController.Instance.GetHandPointer_Tap();
            handPointer.HideUI();

            ShopItemPage currentPage = ShopManager.Instance.CurrentPage;
            currentPage.UI.ScrollRect.vertical = true;
        }


        public override IEnumerator Execute(UnityAction onComplete)
        {
            yield return ScrollToFreedeal();

            ShopItemPage currentPage = ShopManager.Instance.CurrentPage;
            currentPage.UI.ScrollRect.vertical = false;

            //Set dialog
            {
                List<string> dialog = new List<string>
                {
                    LocalizationManager.Instance.GetText("MF_03_0001")
                };
                yield return RunDialogList(dialog, ScreenAnchor.MidRight);
            }

            Debug.Log("ForceClickButton");
            PromotionUIPage promotionUIPage = ((PromotionPage)ShopManager.Instance.GetPage(ShopTypes.Promotion)).GetPromotionUIPage();
            UnityEngine.UI.Button freeDealBTN = promotionUIPage.FreeDeal.BuyButton;
            yield return ForceClickAnyButtonsInArea(new List<UnityEngine.UI.Button>() { freeDealBTN }, promotionUIPage.FreeDeal.GetComponent<RectTransform>());

            Debug.Log("finish this mission");
            currentPage.UI.ScrollRect.vertical = true;
            onComplete?.Invoke();
        }

        private IEnumerator ScrollToFreedeal()
        {
            ShopItemPage currentPage = ShopManager.Instance.CurrentPage;
            {
                bool isFinish = false;
                currentPage.UI.SetScrollVerticalNormalize(0, () => isFinish = true);
                yield return new WaitUntil(() => isFinish == true);
            }
            Debug.Log("Navigate Finish");
        }

        public override void OnForceStopProcess()
        {
            base.OnForceStopProcess();
            OnExit();
        }

    }

}
