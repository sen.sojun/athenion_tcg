﻿using System;
using System.Collections;
using System.Collections.Generic;
using MissionCommandData;
using UnityEngine;

namespace MissionController
{
    public class StarMarketMissionController : Controller
    {
        #region Public Properties
        public StarExchangeManager StarExchangeManager { get; private set; }
        #endregion

        #region Private Properties
        private MissionCommand _currentCommand;
        private Coroutine _currentRoutine;
        #endregion

        #region Constructor
        public StarMarketMissionController(MissionManager manager) : base(manager)
        {
        }
        #endregion

        #region Methods

        protected override void OnEnter()
        { 
            StarExchangeManager.OnOpenPage += StarExchangeManager_OnOpenPage;
        }

        protected override void OnExit()
        {
            StarExchangeManager.OnOpenPage -= StarExchangeManager_OnOpenPage;
        }

        private void StarExchangeManager_OnOpenPage(string obj)
        {
            Debug.Log("StarMarketMissionController/StarExchangeManager_OnOpenPage: " + obj);

            var CommandList = new List<MissionCommand>
            {
                new MF_08(_manager)
            };

            StartExecuteCommandList(CommandList);
        }

        #endregion
    }
}