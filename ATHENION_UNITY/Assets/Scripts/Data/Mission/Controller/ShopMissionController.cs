﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MissionCommandData;
using System;
using UnityEngine.Events;
using UnityEngine.UI;

namespace MissionController
{
    public class ShopMissionController : Controller
    {
        public ShopManager ShopManager { get; private set; }

        public ShopMissionController(MissionManager manager) : base(manager)
        {
        }

        protected override void OnEnter()
        {
            ShopManager = ShopManager.Instance;
            ShopManager.OnShowPage += ShopManager_OnShowPage;
        }

        protected override void OnExit()
        {
            ShopManager.OnShowPage -= ShopManager_OnShowPage;

        }

        private void ShopManager_OnShowPage()
        {
            ShopTypes shopTypes = ShopManager.Instance.CurrentPage.Type;

            MissionCommand command = GetCommand(shopTypes);
            if (command == null)
                return;
            StartExecuteCommand(command);
        }

        private MissionCommand GetCommand(ShopTypes shopTypes)
        {
            if (shopTypes == ShopTypes.Promotion)
            {
                return new MF_03(_manager);
            }
            else if (shopTypes == ShopTypes.BoosterPack)
            {
                return new MF_01(_manager);
            }
            return null;
        }

    }

}