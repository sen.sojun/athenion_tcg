﻿using System;
using System.Collections;
using System.Collections.Generic;
using MissionCommandData;
using UnityEngine;
using UnityEngine.UI;

namespace MissionController
{
    public class HomeMissionController : Controller
    {
        public HomeManager HomeManager { get; private set; }

        #region Private Properties
        private MissionCommand _currentCommand;
        private Coroutine _currentRoutine;
        private HomeMissionUIController _homeMissionUiController;

        private List<MissionCommand> CommandList = new List<MissionCommand>();
        #endregion

        #region Event

        #endregion

        #region Methods

        public HomeMissionController(MissionManager manager) : base(manager)
        {

        }

        protected override void OnEnter()
        {
            HomeManager = HomeManager.Instance;
            HomeManager.OnExecuteInturuptedTask += HomeManager_OnExecuteInturuptedTask;
            //ProfileEditorManager.OnActive += ProfileEditorManager_OnActive;
            AccomplishmentPanelManager.OnCardCollectionActive += AccomplishmentPanelManager_OnActive;
            MissionListUIController.OnActive += MissionListUiController_OnActive;
            SeasonPassUIManager.OnReady += SeasonPassUiManager_OnReady;
        }

        protected override void OnExit()
        {
            HomeManager.OnExecuteInturuptedTask -= HomeManager_OnExecuteInturuptedTask;
            //ProfileEditorManager.OnActive -= ProfileEditorManager_OnActive;
            AccomplishmentPanelManager.OnCardCollectionActive -= AccomplishmentPanelManager_OnActive;
            MissionListUIController.OnActive -= MissionListUiController_OnActive;
            SeasonPassUIManager.OnReady -= SeasonPassUiManager_OnReady;

        }

        #region Home's Mission Section

        private void HomeManager_OnExecuteInturuptedTask()
        {
            MissionCommand intro0 = new MF_Intro0(_manager);

            if (intro0.IsCanPlay)
            {
                CommandList.Add(intro0);
                CommandList.Add(new MF_Intro1(_manager));

                HomeManager.Instance.SetInitInteruptProcess(RunCommandList(CommandList));
            }
        }

        #endregion

        #region Mission's Mission Section

        private void MissionListUiController_OnActive()
        {
            bool isContainCommand = FindCommandInCommandList(typeof(MF_Intro1));
            if (isContainCommand)
                return;

            CommandList = new List<MissionCommand>
            {
                new MF_Intro1(_manager)
            };
            StartExecuteCommandList(CommandList);
        }

        private bool FindCommandInCommandList(Type type)
        {
            foreach (MissionCommand command in CommandList)
            {
                if (command.GetType() == type)
                {
                    return true;
                }
            }
            return false;
        }

        #endregion

        #region Profile Mission

        private void ProfileEditorManager_OnActive()
        {
            CommandList = new List<MissionCommand>
            {
                new MF_07(_manager)
            };

            StartExecuteCommandList(CommandList);
        }

        #endregion

        #region Card Collection Mission

        private void AccomplishmentPanelManager_OnActive()
        {
            CommandList = new List<MissionCommand>
            {
                new MF_05(_manager)
            };

            StartExecuteCommandList(CommandList);
        }

        #endregion

        #region Season Pass Mission

        private void SeasonPassUiManager_OnReady()
        {
            CommandList = new List<MissionCommand>
            {
                new MF_06(_manager)
            };
            SeasonPassUIManager.Instance.SetExtraProcess(RunCommandList(CommandList));
        }

        #endregion

        #endregion
    }
}