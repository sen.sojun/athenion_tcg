﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum HomeMissionID
{
    M_Intro0
    , M_Intro1
}

public class HomeMissionUIController : MonoBehaviour
{
    #region UI Element Properties

    [SerializeField] private RectTransform M_Intro0_Area;
    [SerializeField] private Button M_Intro0_Button;

    #endregion

    #region Method
    public RectTransform GetDialogPos(HomeMissionID missionId)
    {
        switch (missionId)
        {
            case HomeMissionID.M_Intro0:
                return M_Intro0_Area;
            case HomeMissionID.M_Intro1:
                return null;
            default:
                return null;
        }
    }

    public Button GetButtonPos(HomeMissionID missionId)
    {
        switch (missionId)
        {
            case HomeMissionID.M_Intro0:
                return M_Intro0_Button;
            case HomeMissionID.M_Intro1:
                return null;
            default:
                return null;
        }
    }
    #endregion
}
