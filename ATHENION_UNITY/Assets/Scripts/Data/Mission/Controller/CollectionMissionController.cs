﻿using MissionCommandData;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MissionController
{
    public class CollectionMissionController : Controller
    {
        #region Private Properties 
        private readonly List<MissionCommand> _missionFlowCommandList = new List<MissionCommand>();
        #endregion

        #region Contructors
        public CollectionMissionController(MissionManager manager) : base(manager)
        {
        }
        #endregion

        #region Methods
        protected override void OnEnter()
        {
            InitEvents();
            InitMissionFlowList();
        }

        protected override void OnExit()
        {
            DeinitEvents();
            DeinitMissionFlowList();
        }

        private void InitEvents()
        {
            CollectionManager.OnShowPage += OnShowPage;
        }

        private void DeinitEvents()
        {
            CollectionManager.OnShowPage -= OnShowPage;
        }

        private void InitMissionFlowList()
        {
            _missionFlowCommandList.Add(new MF_02(_manager));
            _missionFlowCommandList.Add(new MF_04(_manager));
        }

        private void DeinitMissionFlowList()
        {
            _missionFlowCommandList.Clear();
        }

        private void OnShowPage()
        {
            MissionCommand command = GetCommand();
            if (command != null)
            {
                StartExecuteCommand(command);
            }
        }

        private MissionCommand GetCommand()
        {
            foreach (MissionCommand command in _missionFlowCommandList)
            {
                if (command.IsCanPlay)
                {
                    return command;
                }
            }

            return null;
        }
         
        #endregion
    }
}