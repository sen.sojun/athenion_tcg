﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AchievementCardSetData : GameObjectiveData
{
    #region Public Properties
    public List<string> CardSetIDList { get { return _localData.CardIDList; } }
    #endregion

    #region Private Properties
    private AchievementCardSetDBData _localData = new AchievementCardSetDBData();
    #endregion

    #region Contructors
    public AchievementCardSetData()
    {
    }

    public AchievementCardSetData(AchievementCardSetData data)
    {
        _id = data.ID;
        _isCleared = data._isCleared;
        _data = new AchievementCardSetDBData(data._localData);
        _itemDataList = new List<ItemData>(data._itemDataList);
    }

    public AchievementCardSetData(AchievementCardSetDBData data, bool isCleared)
    {
        _id = data.ID;
        _isCleared = isCleared;
        _data = new AchievementCardSetDBData(data);
        _localData = new AchievementCardSetDBData(_data as AchievementCardSetDBData);
        _itemDataList = new List<ItemData>(_localData.ItemDataList);
    }
    #endregion

    #region Methods
    protected override int GetCurrentProgress()
    {
        Dictionary<string, int> playerStatData = DataManager.Instance.PlayerInfo.GetPlayerStatData();
        int progressCount = 0;
        int currentStatCount = 0;

        foreach (PlayerStatDetailData detail in _localData.DetailDataList)
        {
            currentStatCount = 0;
            foreach (string key in detail.PlayerStatList)
            {
                if (!playerStatData.ContainsKey(key))
                {
                    playerStatData.Add(key, 0);
                }

                currentStatCount += playerStatData[key];
            }

            if (currentStatCount >= detail.Amount)
            {
                progressCount += detail.Amount;
            }
            else
            {
                progressCount += currentStatCount;
            }
        }

        return progressCount;
    }

    protected override int GetMaxProgress()
    {
        if (_data != null)
        {
            return _localData.MaxProgress;
        }

        return 0;
    }

    public List<string> GetCurrentProgressCardIDList()
    {
        Dictionary<string, int> playerStatData = DataManager.Instance.PlayerInfo.GetPlayerStatData();
        List<string> result = new List<string>();
        List<string> currentCardList = new List<string>();

        foreach (PlayerStatDetailData detail in _localData.DetailDataList)
        {
            currentCardList.Clear();
            foreach (string key in detail.PlayerStatList)
            {
                if (!playerStatData.ContainsKey(key))
                {
                    continue;
                }

                currentCardList.Add(_localData.GetCardIDFromStatKey(key));
            }

            result.AddRange(currentCardList);
        }

        return result;
    }

    public bool IsCanClaim()
    {
        return (IsCompleted && !IsCleared);
    }
    #endregion
}
