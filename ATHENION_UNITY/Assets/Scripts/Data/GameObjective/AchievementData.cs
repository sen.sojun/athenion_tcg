﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AchievementData : GameObjectiveData
{
    #region Private Properties
    private AchievementDBData _localData = new AchievementDBData();
    #endregion

    #region Contructors
    public AchievementData()
    {
    }

    public AchievementData(AchievementData data)
    {
        _id = data.ID;
        _isCleared = data._isCleared;
        _data = new AchievementDBData(data._localData);
        _itemDataList = new List<ItemData>(data._itemDataList);
    }

    public AchievementData(AchievementDBData data, bool isCleared)
    {
        _id = data.ID;
        _isCleared = isCleared;
        _data = new AchievementDBData(data);
        _localData = new AchievementDBData(_data as AchievementDBData);
        _itemDataList = new List<ItemData>(_localData.ItemDataList);
    }
    #endregion

    #region Methods
    protected override int GetCurrentProgress()
    {
        Dictionary<string, int> playerStatData = DataManager.Instance.PlayerInfo.GetPlayerStatData();
        int progressCount = 0;
        int currentStatCount = 0;

        foreach (PlayerStatDetailData detail in _localData.DetailDataList)
        {
            currentStatCount = 0;
            foreach (string key in detail.PlayerStatList)
            {
                if (!playerStatData.ContainsKey(key))
                {
                    playerStatData.Add(key, 0);
                }

                currentStatCount += playerStatData[key];
            }

            if (currentStatCount >= detail.Amount)
            {
                progressCount += detail.Amount;
            }
            else
            {
                progressCount += currentStatCount;
            }
        }

        return progressCount;
    }

    protected override int GetMaxProgress()
    {
        if(_data != null)
        {
            return _localData.MaxProgress;
        }

        return 0;
    }

    public bool IsCanClaim()
    {
        return (IsCompleted && !IsCleared);
    }
    #endregion
}
