﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeeklyRankRewardData : GameObjectiveData
{
    public List<WeeklyRankRewardChest> RewardChestList;
    public DateTime LastUpdateTimestamp { get; private set; }

    private int _stampedWeeklyRankWins;

    public int GetMinimumRankForClaimingWeeklyReward()
    {
        if(RewardChestList == null || RewardChestList.Count == 0)
        {
            return 0;
        }

        return RewardChestList[0].MinRankForClaimingReward;
    }

    public int GetRewardListIndexByCurrentRank(int playerRank)
    {
        if(RewardChestList == null || RewardChestList.Count == 0 || playerRank < GetMinimumRankForClaimingWeeklyReward())
        {
            return 0;
        }

        for(int i = 0; i < RewardChestList.Count; i++)
        {
            if(playerRank >= RewardChestList[i].MinRankForClaimingReward && playerRank <= RewardChestList[i].MaxRankForClaimingReward)
            {
                return i;
            }
        }

        return RewardChestList.Count - 1; //Above Second Born
    }

    public List<ItemData> GetListOfRewardByCurrentRank(int playerRank)
    {
        return RewardChestList[GetRewardListIndexByCurrentRank(playerRank)].RewardList;
    }

    public void SetStampedWeeklyRankStats(Dictionary<string, object> rawDataDict)
    {
        if (!rawDataDict.ContainsKey("stamped_rank_wins"))
        {
            if (DataManager.Instance.PlayerInfo.GetPlayerStatData().TryGetValue("W_RANK", out int currentRankWins))
            {
                rawDataDict["stamped_rank_wins"] = currentRankWins;
            }
            else
            {
                rawDataDict["stamped_rank_wins"] = 0;
            }
        }
        int wins = int.Parse(rawDataDict["stamped_rank_wins"].ToString());
        DateTime updateTimestamp = (DateTime)rawDataDict["last_refresh_timestamp"];

        _stampedWeeklyRankWins = wins;
        LastUpdateTimestamp = updateTimestamp;
    }

    protected override int GetMaxProgress()
    {
        return 3;
    }

    protected override int GetCurrentProgress()
    {
        Dictionary<string, int> playerStatData = DataManager.Instance.PlayerInfo.GetPlayerStatData();
        int currentRankWinsCount = playerStatData.ContainsKey("W_RANK") ? playerStatData["W_RANK"] : 0;
        int winsThisWeek = currentRankWinsCount - _stampedWeeklyRankWins;
        if(winsThisWeek > GetMaxProgress())
        {
            winsThisWeek = GetMaxProgress();
        }

        return winsThisWeek;
    }
}

public class WeeklyRankRewardChest
{
    public List<ItemData> RewardList = new List<ItemData>();
    public int MinRankForClaimingReward { get; private set; }
    public int MaxRankForClaimingReward { get; private set; }

    public void SetMinMaxRank(int minRankForClaimingReward, int maxRankForClaimingReward)
    {
        MinRankForClaimingReward = minRankForClaimingReward;
        MaxRankForClaimingReward = maxRankForClaimingReward;
    }
}