﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

#region Global Enums
public enum QuestTypes
{
    None = 0

    // event quest
    , Event

    // season pass quest types
    , SeasonPassMainDaily
    , SeasonPassMainWeekly
    , SeasonPassMainWeek
    , SeasonPassChallenge
}
#endregion

public class QuestData : GameObjectiveData
{
    #region Public Properties
    public QuestTypes Type { get { return _type; } }
    public bool IsPremium { get { return _isPremium; } }
    public bool IsNew { get { return GetIsNew(); } }
    public string GoFuncName { get { return _localData.FunctionName; } }
    public string GoFuncParam { get { return _localData.FunctionParam; } }
    #endregion

    #region Private Properties
    private QuestTypes _type = QuestTypes.None;
    private Dictionary<string, int> _questStampData = new Dictionary<string, int>();
    private QuestDBData _localData = new QuestDBData();
    private bool _isPremium = false;
    #endregion

    #region Contructors
    public QuestData()
    {
    }

    public QuestData(QuestData data)
    {
        _id = data.ID;
        _type = data._type;
        _questStampData = new Dictionary<string, int>(data._questStampData);
        _data = new QuestDBData(data._localData);
        _itemDataList = new List<ItemData>(data._itemDataList);
        _isPremium = data._isPremium;
    }

    public QuestData(string id, QuestTypes type, Dictionary<string, int> questStatStamp, bool isCleared)
    {
        _id = id;
        _type = type;
        _questStampData = new Dictionary<string, int>(questStatStamp);
        _isCleared = isCleared;

        QuestDBData data;
        if(QuestDB.Instance.GetData(id, out data))
        {
            _data = data;
            _localData = new QuestDBData(_data as QuestDBData);
            _isPremium = data.IsPremium;
        }

        _itemDataList = new List<ItemData>(_localData.ItemDataList);
    }
    #endregion

    #region Methods
    protected override int GetCurrentProgress()
    {
        int progressCount = 0;
        int currentStatCount = 0;
        int stampStatCount = 0;
        Dictionary<string, int> playerStatData = DataManager.Instance.PlayerInfo.GetPlayerStatData();

        foreach (PlayerStatDetailData data in _localData.DetailDataList)
        {
            currentStatCount = 0;
            stampStatCount = 0;
            foreach (string key in data.PlayerStatList)
            {
                if (!_questStampData.ContainsKey(key))
                {
                    _questStampData.Add(key, 0);
                }
                if (!playerStatData.ContainsKey(key))
                {
                    playerStatData.Add(key, 0);
                }

                currentStatCount += playerStatData[key];
                stampStatCount += _questStampData[key];
            }

            if (currentStatCount - stampStatCount >= data.Amount)
            {
                progressCount += data.Amount;
            }
            else
            {
                progressCount += currentStatCount - stampStatCount;
            }
        }

        return progressCount;
    }

    protected override int GetMaxProgress()
    {
        if (_data != null)
        {
            return _localData.MaxProgress;
        }

        return 0;
    }

    private bool GetIsNew()
    {
        return PlayerSeenListManager.Instance.IsNewSeasonPassQuest(ID);
    }
    #endregion
}