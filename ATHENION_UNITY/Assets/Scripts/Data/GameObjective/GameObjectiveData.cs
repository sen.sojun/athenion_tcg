﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GameObjectiveData
{
    #region Public Properties
    public string ID { get { return _id; } }
    public List<ItemData> ItemDataList { get { return _itemDataList; } }
    public int CurrentProgress { get { return GetCurrentProgress(); } }
    public int MaxProgress { get { return GetMaxProgress(); } }
    public bool IsCompleted { get { return CurrentProgress >= MaxProgress; } }
    public bool IsAlreadyCompleted { get { return _isAlreadyCompleted; } }
    public bool IsCleared { get { return _isCleared; } }
    public DBData Data { get { return _data; } }
    #endregion

    #region Protected Properties
    protected string _id = string.Empty;
    protected bool _isAlreadyCompleted = false;
    protected bool _isCleared = false;
    protected List<ItemData> _itemDataList = new List<ItemData>();
    protected DBData _data = null;
    #endregion

    #region Contructors
    #endregion

    #region Methods
    public void SetAlreadyComplete()
    {
        _isAlreadyCompleted = IsCompleted;
    }

    public void SetClear()
    {
        _isCleared = IsCompleted;
    }

    protected abstract int GetCurrentProgress();

    protected abstract int GetMaxProgress();
    #endregion
}
