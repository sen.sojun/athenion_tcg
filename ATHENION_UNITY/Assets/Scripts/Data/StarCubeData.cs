﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarCubeData
{
    #region Properties
    public int CubeIndex { get; private set; }
    public DateTime LastOpenTimestamp { get; private set; }
    public int Duration { get; private set; }
    #endregion

    #region Constructors
    public StarCubeData(int index, DateTime lastOpenTimestamp, int duration)
    {
        CubeIndex = index;
        LastOpenTimestamp = lastOpenTimestamp;
        Duration = duration;
    }

    public StarCubeData(StarCubeData data)
    {
        CubeIndex = data.CubeIndex;
        LastOpenTimestamp = data.LastOpenTimestamp;
        Duration = data.Duration;
    }
    #endregion

    #region Methods
    public string GetDebugText()
    {
        return string.Format("Cube[{0}] Duration: {1} LastOpen : {2}", CubeIndex, Duration, LastOpenTimestamp.ToString());
    }
    #endregion
}
