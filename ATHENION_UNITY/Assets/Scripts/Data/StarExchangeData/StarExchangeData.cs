﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarExchangeData
{
    public DateTime EndDate { get; private set; }
    public NewArrival NewArrival { get; private set; }
    public RotationPool RotationPool { get; private set; }

    public StarExchangeData(string json)
    {
        Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
        EndDate = rawData.ContainsKey("end_date") ? Convert.ToDateTime(rawData["end_date"].ToString()) : DateTime.MaxValue;
        NewArrival = new NewArrival(rawData["new_arrival"].ToString());
        RotationPool = new RotationPool(rawData["player_rotation_data"].ToString());
    }
}

public class NewArrival
{
    public DateTime ReleaseDate { get; private set; }
    public DateTime EndDate { get; private set; }
    public List<StarExchangeItem> ItemList { get; private set; }

    public NewArrival(string json)
    {
        Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
        ReleaseDate = rawData.ContainsKey("release_date") ? Convert.ToDateTime(rawData["release_date"].ToString()) : DateTime.MinValue;
        EndDate = rawData.ContainsKey("end_date") ? Convert.ToDateTime(rawData["end_date"].ToString()) : DateTime.MaxValue;

        ItemList = new List<StarExchangeItem>();
        List<object> rawItemDataList = JsonConvert.DeserializeObject<List<object>>(rawData["item_list"].ToString());
        for (int i = 0; i < rawItemDataList.Count; i++)
        {
            ItemList.Add(new StarExchangeItem(rawItemDataList[i].ToString()));
        }
    }
}

public class RotationPool
{
    public List<StarExchangeItem> ItemList { get; private set; }
    public int RefreshCount { get; private set; }
    public DateTime TimeStamp { get; private set; }

    public RotationPool(string json)
    {
        Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
        TimeStamp = rawData.ContainsKey("timestamp") ? Convert.ToDateTime(rawData["timestamp"].ToString()) : DateTime.MinValue;
        RefreshCount = rawData.ContainsKey("refresh_count") ? int.Parse(rawData["refresh_count"].ToString()) : 0;

        ItemList = new List<StarExchangeItem>();
        List<object> rawItemDataList = JsonConvert.DeserializeObject<List<object>>(rawData["rotation_pool"].ToString());
        for (int i = 0; i < rawItemDataList.Count; i++)
        {
            ItemList.Add(new StarExchangeItem(rawItemDataList[i].ToString()));
        }
    }
}

public class StarExchangeItem
{
    public string ItemID { get; private set; }
    public int Amount { get; private set; }
    public int Max { get; private set; }

    public VirtualCurrency Currency { get; private set; }
    public int Price { get; private set; }

    public bool IsBuyable { get { return IsValidCount; } }
    public bool IsValidCount
    {
        get
        {
            return DataManager.Instance.InventoryData.GetAmountByItemID(ItemID) < Max;
        }
    }

    public StarExchangeItem(string json)
    {
        Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
        ItemID = rawData["ItemID"].ToString();
        Amount = rawData.ContainsKey("Amount") ? int.Parse(rawData["Amount"].ToString()) : 1;
        Max = rawData.ContainsKey("Max") ? int.Parse(rawData["Max"].ToString()) : 1;

        SetPrice(rawData["Price"].ToString());
    }

    private void SetPrice(string json)
    {
        Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
        foreach (KeyValuePair<string, object> data in rawData)
        {
            string currencyCode = data.Key.Replace("VC_", "");
            VirtualCurrency currency;
            GameHelper.StrToEnum(currencyCode, out currency);
            Currency = currency;

            Price = int.Parse(data.Value.ToString());
            break;
        }
    }
}
