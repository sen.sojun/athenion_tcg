﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarItemDB
{
    public List<CurrencyReward> itemList = new List<CurrencyReward>();
    public StarItemDB(string json)
    {
        Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);

        foreach (KeyValuePair<string, object> data in rawData)
        {
            string itemID = data.Key;
            string dataJson = data.Value.ToString();
            itemList.Add(new CurrencyReward(itemID, dataJson));
        }
    }

    public CurrencyReward GetStarItem(string itemID)
    {
        return itemList.Find(item => item.BundleID == itemID);
    }

}
