﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SlotStatusType
{
    // Status
    Be_Lock = 0 //Lock
    , Be_DarkMatter = 1 //Dark Matter
}

public class SlotStatus
{
    #region Public Properties
    public List<SlotStatusType> StatusList { get { return _statusList; } }
    #endregion

    #region Private Properties
    private List<SlotStatusType> _statusList = new List<SlotStatusType>();
    #endregion

    #region Constructors
    public SlotStatus()
    {
        _statusList = new List<SlotStatusType>();
    }

    public SlotStatus(List<SlotStatusType> statusList)
    {
        _statusList = new List<SlotStatusType>(statusList);
    }

    public SlotStatus(SlotStatus slotStatus) : this(slotStatus.StatusList)
    {
    }

    public SlotStatus(SlotStatusType slotStatusType)
    {
        _statusList = new List<SlotStatusType>();
        Add(slotStatusType);
    }
    #endregion

    #region Methods
    public bool IsSlotStatus(SlotStatusType slotStatusType)
    {
        if (_statusList != null)
        {
            return (_statusList.Contains(slotStatusType));
        }

        return false;
    }

    public void Add(SlotStatusType slotStatusType)
    {
        if (_statusList == null)
        {
            _statusList = new List<SlotStatusType>();
        }

        if (!_statusList.Contains(slotStatusType))
        {
            _statusList.Add(slotStatusType);
        }
    }

    public void Remove(SlotStatusType slotStatusType)
    {
        if (_statusList != null && _statusList.Contains(slotStatusType))
        {
            _statusList.Remove(slotStatusType);
        }
    }

    public override string ToString()
    {
        string text = "";
        foreach (SlotStatusType passiveType in _statusList)
        {
            if (text.Length > 0)
            {
                text += ", " + passiveType.ToString();
            }
            else
            {
                text += "{" + passiveType.ToString();
            }
        }

        if (text.Length <= 0)
        {
            text = "None";
        }
        else
        {
            text += "}";
        }

        return text;
    }
    #endregion

    #region Operators
    public static bool operator ==(SlotStatus data1, SlotStatus data2)
    {
        return (data1.Equals(data2));
    }

    public static bool operator !=(SlotStatus data1, SlotStatus data2)
    {
        return (!data1.Equals(data2));
    }

    public bool Equals(SlotStatus data)
    {
        if (ReferenceEquals(null, data))
        {
            return false;
        }
        if (ReferenceEquals(this, data))
        {
            return true;
        }

        foreach (SlotStatusType statusType in System.Enum.GetValues(typeof(SlotStatusType)))
        {
            if (IsSlotStatus(statusType) != data.IsSlotStatus(statusType))
            {
                return false;
            }
        }

        return true;
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj))
        {
            return false;
        }
        if (ReferenceEquals(this, obj))
        {
            return true;
        }

        return obj.GetType() == GetType() && Equals((SlotStatus)obj);
    }

    public override int GetHashCode()
    {
        return (this._statusList.GetHashCode());
    }
    #endregion
}
