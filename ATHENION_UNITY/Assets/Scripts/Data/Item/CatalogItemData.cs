﻿using System.Collections;
using System.Collections.Generic;
using PlayFab.ClientModels;
using Newtonsoft.Json;
using System;

public class CatalogItemData
{
    #region Enums
    #endregion

    #region Public Properties
    public string ItemCatalogID { get; protected set; }
    public string ItemID { get; protected set; }
    public string ItemClass { get; protected set; }
    public List<string> ItemTags { get; protected set; }
    public CatalogItemType ItemType { get { return _itemType; } }
    public Dictionary<VirtualCurrency, uint> PurchasePrice { get; protected set; }
    #endregion

    #region Protected Properties
    protected CatalogItemType _itemType = CatalogItemType.None;
    #endregion

    #region Contructors
    public CatalogItemData()
    {
        ItemCatalogID = "";
        ItemID = "";
        ItemClass = "";
        ItemTags = new List<string>();
        _itemType = CatalogItemType.None;
        PurchasePrice = new Dictionary<VirtualCurrency, uint>();
    }

    protected CatalogItemData(CatalogItem catalogItem)
    {
        ConvertFromCatalogItem(catalogItem);
    }

    public CatalogItemData(CatalogItemData data)
    {
        if (data != null)
        {
            ItemCatalogID = data.ItemCatalogID;
            ItemID = data.ItemID;
            ItemClass = data.ItemClass;
            ItemTags = new List<string>(data.ItemTags);
            _itemType = data.ItemType;
            PurchasePrice = new Dictionary<VirtualCurrency, uint>(data.PurchasePrice);
        }
    }
    #endregion

    #region Methods
    protected virtual void ConvertFromCatalogItem(CatalogItem item)
    {
        string[] itemClass = item.ItemClass.Split(':');
        GameHelper.StrToEnum(itemClass[0], out _itemType);

        ItemCatalogID = item.ItemId;
        ItemID = ItemCatalogID.Split('_')[0];
        ItemClass = item.ItemClass;
        ItemTags = new List<string>(item.Tags);

        PurchasePrice = new Dictionary<VirtualCurrency, uint>();
        foreach (KeyValuePair<string, uint> price in item.VirtualCurrencyPrices)
        {
            VirtualCurrency currency;
            if (GameHelper.StrToEnum(price.Key, out currency))
            {
                PurchasePrice.Add(currency, price.Value);
            }
        }
    }

    public virtual string GetDebugText()
    {
        string priceText = "";
        foreach (KeyValuePair<VirtualCurrency, uint> price in PurchasePrice)
        {
            if (priceText.Length > 0)
            {
                priceText += string.Format(" {0}={1}", price.Key, price.Value);
            }
            else
            {
                priceText += string.Format("{0}={1}", price.Key, price.Value);
            }
        }

        string result = string.Format(
            "ItemCatalogID: {0} ID: {1} Type: {2}\nPrice: {3}"
            , ItemCatalogID
            , ItemID
            , ItemType
            , priceText
        );

        return result;
    }

    public static CatalogItemData CreateCatalogItemData(CatalogItem item)
    {
        switch (CheckCatalogItemType(item))
        {
            case CatalogItemType.Card:
            {
                CatalogCardData catalogCardData = CatalogCardData.CreateCatalogCardData(item);
                return catalogCardData;
            }
            //break;

            default:
            {
                CatalogItemData catalogItemData = new CatalogItemData(item);
                return catalogItemData;
            }
            //break;
        }
    }

    public static CatalogItemType CheckCatalogItemType(CatalogItem item)
    {
        CatalogItemType itemType = CatalogItemType.None;
        string[] itemClass = item.ItemClass.Split(':');
        if (GameHelper.StrToEnum(itemClass[0], out itemType))
        {
            return itemType;
        }

        return CatalogItemType.None;
    }
}
#endregion

public class CatalogCardData : CatalogItemData
{
    #region Public Const Properties
    public const string DefaultPriceKey = "normal";
    #endregion

    #region Public Properties
    // card price details
    public Dictionary<VirtualCurrency, int> RecyclePrice { get { return _cardPrice.RecyclePrice; } }
    public Dictionary<VirtualCurrency, int> CraftPrice { get { return _cardPrice.CraftPrice; } }
    public Dictionary<VirtualCurrency, int> TransformPrice { get { return _cardPrice.TransformPrice; } }
    public string TransformItemID { get { return _cardPrice.TransformItemID; } }

    // price override params
    public bool IsPriceOverride { get { return _overridePriceStartDateTimeUTC != DateTime.MinValue || _overridePriceEndDateTimeUTC != DateTime.MaxValue; } }
    public DateTime OverridePriceStartDateTimeUTC { get { return _overridePriceStartDateTimeUTC; } }
    public DateTime OverridePriceEndDateTimeUTC { get { return _overridePriceEndDateTimeUTC; } }
    public DateTime OverridePriceStartDateTime { get { return _overridePriceStartDateTimeUTC.ToLocalTime(); } }
    public DateTime OverridePriceEndDateTime { get { return _overridePriceEndDateTimeUTC.ToLocalTime(); } }

    public bool IsLimited { get { return _quotaAmount > -1; } }
    public int QuotaAmount { get { return _quotaAmount; } }
    #endregion

    #region Private Properties
    private CardPriceData _cardPrice = new CardPriceData();
    private Dictionary<string, CardPriceData> _cardPriceDataDict = new Dictionary<string, CardPriceData>();
    private int _quotaAmount = -1;
    private DateTime _overridePriceStartDateTimeUTC = DateTime.MinValue;
    private DateTime _overridePriceEndDateTimeUTC = DateTime.MaxValue;
    #endregion

    #region Constructor
    public CatalogCardData() : base()
    {
    }

    protected CatalogCardData(CatalogItem catalogItem)
    {
        ConvertFromCatalogItem(catalogItem);
    }

    public CatalogCardData(CatalogCardData data)
    {
        if (data != null)
        {
            ItemCatalogID = data.ItemCatalogID;
            ItemID = data.ItemID;
            _itemType = data.ItemType;
            PurchasePrice = new Dictionary<VirtualCurrency, uint>(data.PurchasePrice);
            _cardPrice = new CardPriceData(data._cardPrice);
            _cardPriceDataDict = new Dictionary<string, CardPriceData>(data._cardPriceDataDict);
        }
    }
    #endregion

    #region Methods
    public void UpdateCardPriceData()
    {
        _quotaAmount = -1;
        _overridePriceStartDateTimeUTC = DateTime.MinValue;
        _overridePriceEndDateTimeUTC = DateTime.MaxValue;

        if (_cardPriceDataDict.ContainsKey(DefaultPriceKey))
        {
            _cardPrice = _cardPriceDataDict[DefaultPriceKey];
        }

        List<CardPriceOverrideDBData> dataList;
        if(CardPriceOverrideDB.Instance.GetAllData(out dataList))
        {
            PlayerCardPriceOverrideData playerData = DataManager.Instance.PlayerCardPriceOverride;
            foreach(CardPriceOverrideDBData data in dataList)
            {
                if (data.IsDateTimeValid())
                {
                    if (data.IsLimited)
                    {
                        if (playerData.CurrentOverrideKey != data.ID)
                        {
                            continue;
                        }

                        if (!playerData.IsCardPriceOverrideQuotaValid(ItemID))
                        {
                            continue;
                        }
                    }

                    CardPriceOverrideData ovrrData = data.GetCardPriceOverrideDataByItemID(ItemID);
                    if (ovrrData != null)
                    {
                        if (_cardPriceDataDict.ContainsKey(ovrrData.CardPriceBaseKey))
                        {
                            _cardPrice = _cardPriceDataDict[ovrrData.CardPriceBaseKey];
                        }

                        _cardPrice = CardPriceData.OverrideCardPriceData(_cardPrice, ovrrData.CardPriceData);
                        _quotaAmount = playerData.GetCardPriceOverrideQuoata(ItemID);
                        _overridePriceStartDateTimeUTC = data.StartDateTime;
                        _overridePriceEndDateTimeUTC = data.EndDateTime;
                    }

                    break;
                }
            }
        }
    }

    /// <summary>
    /// Get list of CardDataPrice which be calculated by quota
    /// </summary>
    /// <param name="amount"></param>
    /// <returns></returns>
    public List<CardPriceData> GetCardPriceDataList(int amount)
    {
        List<CardPriceData> result = new List<CardPriceData>();
        List<CardPriceOverrideDBData> dataList;
        CardPriceOverrideDB.Instance.GetAllData(out dataList);
        PlayerCardPriceOverrideData playerData = DataManager.Instance.PlayerCardPriceOverride;
        int quotaAmount = playerData.GetCardPriceOverrideQuoata(ItemID);

        if (dataList == null)
        {
            return result;
        }

        for(int i = 0; i < amount; i++)
        {
            CardPriceData price = new CardPriceData();
            if (_cardPriceDataDict.ContainsKey(DefaultPriceKey))
            {
                price = _cardPriceDataDict[DefaultPriceKey];
            }
            result.Add(new CardPriceData(price));

            foreach (CardPriceOverrideDBData data in dataList)
            {
                if (data.IsDateTimeValid())
                {
                    if (data.IsLimited)
                    {
                        if (playerData.CurrentOverrideKey != data.ID)
                        {
                            continue;
                        }

                        if (quotaAmount <= 0)
                        {
                            continue;
                        }
                    }

                    CardPriceOverrideData ovrrData = data.GetCardPriceOverrideDataByItemID(ItemID);
                    if (ovrrData != null)
                    {
                        if (_cardPriceDataDict.ContainsKey(ovrrData.CardPriceBaseKey))
                        {
                            price = _cardPriceDataDict[ovrrData.CardPriceBaseKey];
                        }

                        CardPriceData resultPrice = CardPriceData.OverrideCardPriceData(price, ovrrData.CardPriceData);
                        result[i] = new CardPriceData(resultPrice);
                        quotaAmount--;
                    }
                    break;
                }
            }
        }
        return result;
    }

    protected override void ConvertFromCatalogItem(CatalogItem item)
    {
        base.ConvertFromCatalogItem(item);

        string customDataStr = item.CustomData;
        if (!string.IsNullOrEmpty(customDataStr))
        {
            string validStoreID = string.Empty;
            Dictionary<string, object> customData = new Dictionary<string, object>();
            Dictionary<string, object> detailData = new Dictionary<string, object>();
            customData = JsonConvert.DeserializeObject<Dictionary<string, object>>(customDataStr);

            foreach(KeyValuePair<string, object> data in customData)
            {
                _cardPriceDataDict.Add(data.Key, new CardPriceData(data.Value.ToString()));
            }

            UpdateCardPriceData();
        }
    }

    public override string GetDebugText()
    {
        string baseText = base.GetDebugText();

        string carftPriceText = "";
        foreach (KeyValuePair<VirtualCurrency, int> price in CraftPrice)
        {
            if (carftPriceText.Length > 0)
            {
                carftPriceText += string.Format(" {0}={1}", price.Key, price.Value);
            }
            else
            {
                carftPriceText += string.Format("{0}={1}", price.Key, price.Value);
            }
        }

        string recyclePriceText = "";
        foreach (KeyValuePair<VirtualCurrency, int> price in RecyclePrice)
        {
            if (recyclePriceText.Length > 0)
            {
                recyclePriceText += string.Format(" {0}={1}", price.Key, price.Value);
            }
            else
            {
                recyclePriceText += string.Format("{0}={1}", price.Key, price.Value);
            }
        }

        string transformPriceText = "";
        foreach (KeyValuePair<VirtualCurrency, int> price in TransformPrice)
        {
            if (transformPriceText.Length > 0)
            {
                transformPriceText += string.Format(" {0}={1}", price.Key, price.Value);
            }
            else
            {
                transformPriceText += string.Format("{0}={1}", price.Key, price.Value);
            }
        }

        string result = string.Format(
            "{0}\nCardTranformedID: {1}\nCraft Price: {2}\nRecycle Price: {3}\nTransform Price: {4}"
            , baseText
            , TransformItemID
            , carftPriceText
            , recyclePriceText
            , transformPriceText
        );

        return result;
    }

    public static CatalogCardData CreateCatalogCardData(CatalogItem item)
    {
        switch (CheckCatalogItemType(item))
        {
            case CatalogItemType.Card:
            {
                CatalogCardData catalogCardData = new CatalogCardData(item);
                return catalogCardData;
            }
            //break;

            default:
            {
                return null;
            }
            //break;
        }
    }

    public int GetCardRecyclePrice(VirtualCurrency vc)
    {
        return _cardPrice.GetCardRecyclePrice(vc);
    }

    public int GetCardCraftPrice(VirtualCurrency vc)
    {
        return _cardPrice.GetCardCraftPrice(vc);
    }

    public int GetCardTransformPrice(VirtualCurrency vc)
    {
        return _cardPrice.GetCardTransformPrice(vc);
    }
    #endregion
}