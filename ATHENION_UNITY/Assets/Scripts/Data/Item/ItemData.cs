﻿using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;

[Serializable]
public class ItemData
{
    public string ItemID { get; private set; }
    public int Amount { get; private set; }

    public ItemData()
    {
    }

    public ItemData(string json)
    {
        Dictionary<string, object> data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
        ItemID = data["item_id"].ToString();
        Amount = int.Parse(data["amount"].ToString());
    }

    public ItemData(string itemID, int amount)
    {
        ItemID = itemID;
        Amount = amount;
    }

    public ItemData(KeyValuePair<string, int> data)
    {
        ItemID = data.Key;
        Amount = data.Value;
    }

    public void ModifyAmount(int modAmt)
    {
        Amount += modAmt;
    }

    public virtual string GetDebugText()
    {
        return JsonConvert.SerializeObject(this);
    }
}