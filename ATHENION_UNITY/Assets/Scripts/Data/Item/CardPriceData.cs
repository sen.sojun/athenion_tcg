﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class CardPriceData
{
    #region Public Properties
    public Dictionary<VirtualCurrency, int> RecyclePrice { get { return _recyclePrice; } }
    public Dictionary<VirtualCurrency, int> CraftPrice { get { return _craftPrice; } }
    public Dictionary<VirtualCurrency, int> TransformPrice { get { return _transformPrice; } }
    public string TransformItemID { get { return _transformItemID; } }
    #endregion

    #region Private Properties
    private Dictionary<VirtualCurrency, int> _recyclePrice = new Dictionary<VirtualCurrency, int>();
    private Dictionary<VirtualCurrency, int> _craftPrice = new Dictionary<VirtualCurrency, int>();
    private Dictionary<VirtualCurrency, int> _transformPrice = new Dictionary<VirtualCurrency, int>();
    private string _transformItemID = string.Empty;
    #endregion

    #region Contructors
    public CardPriceData()
    {
    }

    public CardPriceData(string jsonStrData)
    {
        Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonStrData);
        Dictionary<string, int> recyclePriceData = new Dictionary<string, int>();
        Dictionary<string, int> craftPriceData = new Dictionary<string, int>();
        Dictionary<string, object> transformPriceData = new Dictionary<string, object>();

        // setup recycle data
        if(rawData.ContainsKey("recycle"))
        {
            recyclePriceData = JsonConvert.DeserializeObject<Dictionary<string, int>>(rawData["recycle"].ToString());
            foreach (KeyValuePair<string, int> price in recyclePriceData)
            {
                string[] keySplit = price.Key.Split('_');
                if (keySplit.Length > 1)
                {
                    VirtualCurrency vcKey;
                    if (GameHelper.StrToEnum(keySplit[1], out vcKey))
                    {
                        _recyclePrice.Add(vcKey, price.Value);
                    }
                }
            }
        }

        // setup craft data
        if (rawData.ContainsKey("craft"))
        {
            craftPriceData = JsonConvert.DeserializeObject<Dictionary<string, int>>(rawData["craft"].ToString());
            foreach (KeyValuePair<string, int> price in craftPriceData)
            {
                string[] keySplit = price.Key.Split('_');
                if (keySplit.Length > 1)
                {
                    VirtualCurrency vcKey;
                    if (GameHelper.StrToEnum(keySplit[1], out vcKey))
                    {
                        _craftPrice.Add(vcKey, price.Value);
                    }
                }
            }
        }

        // setup transform data
        if (rawData.ContainsKey("transform"))
        {
            transformPriceData = JsonConvert.DeserializeObject<Dictionary<string, object>>(rawData["transform"].ToString());
            if (transformPriceData.ContainsKey("VC"))
            {
                Dictionary<string, int> priceData = JsonConvert.DeserializeObject<Dictionary<string, int>>(transformPriceData["VC"].ToString());
                foreach (KeyValuePair<string, int> price in priceData)
                {
                    string[] keySplit = price.Key.Split('_');
                    if (keySplit.Length > 1)
                    {
                        VirtualCurrency vcKey;
                        if (GameHelper.StrToEnum(keySplit[1], out vcKey))
                        {
                            _transformPrice.Add(vcKey, price.Value);
                        }
                    }
                }
            }

            if (transformPriceData.ContainsKey("item"))
            {
                _transformItemID = transformPriceData["item"] as string;
            }
        }
    }

    public CardPriceData(CardPriceData data)
    {
        _recyclePrice = new Dictionary<VirtualCurrency, int>(data._recyclePrice);
        _craftPrice = new Dictionary<VirtualCurrency, int>(data._craftPrice);
        _transformPrice = new Dictionary<VirtualCurrency, int>(data._transformPrice);
        _transformItemID = data._transformItemID;
    }
    #endregion

    #region Methods
    /// <summary>
    /// Override CardPriceData from target into scr
    /// </summary>
    /// <param name="scr"></param>
    /// <param name="target"></param>
    /// <returns></returns>
    public static CardPriceData OverrideCardPriceData(CardPriceData scr, CardPriceData target)
    {
        CardPriceData result = new CardPriceData(scr);
        if(target.RecyclePrice.Count > 0)
        {
            result._recyclePrice = new Dictionary<VirtualCurrency, int>(target._recyclePrice);
        }
        if (target.CraftPrice.Count > 0)
        {
            result._craftPrice = new Dictionary<VirtualCurrency, int>(target._craftPrice);
        }
        if (target.TransformPrice.Count > 0)
        {
            result._transformPrice = new Dictionary<VirtualCurrency, int>(target._transformPrice);
        }

        return result;
    }

    public int GetCardRecyclePrice(VirtualCurrency vc)
    {
        if (_recyclePrice.ContainsKey(vc))
        {
            return _recyclePrice[vc];
        }

        return 0;
    }

    public int GetCardCraftPrice(VirtualCurrency vc)
    {
        if (_craftPrice.ContainsKey(vc))
        {
            return _craftPrice[vc];
        }

        return 0;
    }

    public int GetCardTransformPrice(VirtualCurrency vc)
    {
        if (_transformPrice.ContainsKey(vc))
        {
            return _transformPrice[vc];
        }

        return 0;
    }

    public void AddCardRecyclePrice(VirtualCurrency vc, int value)
    {
        if (_recyclePrice.ContainsKey(vc) == false)
        {
            _recyclePrice.Add(vc, 0);
        }

        _recyclePrice[vc] += value;
    }

    public void AddCardCraftPrice(VirtualCurrency vc, int value)
    {
        if (_craftPrice.ContainsKey(vc) == false)
        {
            _craftPrice.Add(vc, 0);
        }

        _craftPrice[vc] += value;
    }

    public void AddCardTransformPrice(VirtualCurrency vc, int value)
    {
        if (_transformPrice.ContainsKey(vc) == false)
        {
            _transformPrice.Add(vc, 0);
        }

        _transformPrice[vc] += value;
    }
    #endregion
}