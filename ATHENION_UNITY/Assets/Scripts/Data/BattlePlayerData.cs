﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class BattlePlayerData
{
    #region Public Properties
    public Dictionary<CardZone, List<BattleCardData>> CardList;
    public PlayerIndex PlayerIndex { get; private set; }
    public PlayerInfoData PlayerInfo { get; private set; }
    public HeroData Hero { get; private set; }
    public string CardBackID { get { return _deckData.CardBackID; } }
    public string HUDSkinID { get { return _deckData.HUDSkinID; } }
    public string HeroImage { get; private set; }

    public int MaxHP { get { return _GetMaxHP(); } }
    public int CurrentHP { get { return _GetCurrentHP(); } }
    public int MaxAP { get { return _GetMaxAP(); } }
    public int CurrentAP { get { return _GetCurrentAP(); } }
    public int ImmuneCounter { get { return _GetImmuneCounter(); } }

    public DeckData Deck { get { return _deckData; } }

    public int DeckCount 
    { 
        get 
        {
            if (CardList != null)
            {
                return CardList[CardZone.Deck].Count;
            }

            return 0; 
        } 
    }
    public int DrawPenalty { get; private set; }
    public bool IsImmune { get { return (ImmuneCounter > 0); } }

    public int EncodeKey { get { return _encodeKey; } }
    private int _encodeKey;
    #endregion

    #region Private Properties
    private int _maxHP_encode = 0;
    private int _currentHP_encode = 0;
    private int _maxAP_encode = 0;
    private int _currentAP_encode = 0;    
    private int _immuneCounter_encode = 0;

    private string _maxHP_encode_str = "";
    private string _currentHP_encode_str = "";
    private string _maxAP_encode_str = "";
    private string _currentAP_encode_str = "";
    private string _immuneCounter_encode_str = "";

    private readonly static int _debugTextCardCount = 5;
    private StringBuilder _sb;
    private DeckData _deckData = new DeckData();
    #endregion

    #region Contructor
    public BattlePlayerData(PlayerIndex index, int hp, int ap, PlayerInfoData playerInfo, DeckData deckData, int encodeKey)
    {
        _encodeKey = encodeKey;

        PlayerIndex = index;

        _SetMaxHP(hp);
        _SetCurrentHP(hp);
        _SetMaxAP(ap);
        _SetCurrentAP(0);
        _SetImmuneCounter(0);

        PlayerInfo = new PlayerInfoData(playerInfo);

        _deckData = new DeckData(deckData);
        Hero = HeroData.CreateHero(_deckData.HeroID, index);

        HeroImage = Hero.ImageKey;
        DrawPenalty = 1;
    }
    #endregion

    #region Methods
    public void SetHPDeltaClamp(int deltaValue)
    {
        int currentHP = CurrentHP;
        currentHP = Mathf.Min(currentHP + deltaValue, MaxHP);

        _SetCurrentHP(currentHP);
    }

    public void SetAPDeltaClamp(int deltaValue)
    {
        int currentAP = CurrentAP;

        _SetCurrentAP(currentAP + deltaValue);
    }

    public void SetAPClamp(int value)
    {
        int maxAP = MaxAP;
        int currentAP = Mathf.Min(Mathf.Max(0, value), maxAP);

        _SetCurrentAP(currentAP);
    }

    public void SetImmuneCounter(int count)
    {
        if (ImmuneCounter != count)
        {
            bool isShowImmune = (ImmuneCounter > 0);

            _SetImmuneCounter(count);

            if (ImmuneCounter > 0 && !isShowImmune)
            {
                UIManager.Instance.RequestShowPlayerImmune(PlayerIndex, true);
            }
            else if (ImmuneCounter <= 0 && isShowImmune)
            {
                UIManager.Instance.RequestShowPlayerImmune(PlayerIndex, false);
            }
        }
    }

    public void SetPenalty(int value)
    {
        DrawPenalty = value;
    }

    public void UpdateImmune()
    {
        if (ImmuneCounter > 0)
        {
            int count = ImmuneCounter;
            count = count - 1;

            if (count <= 0)
            {
                count = 0;
                UIManager.Instance.RequestShowPlayerImmune(PlayerIndex, false);
            }

            _SetImmuneCounter(count);
        }
    }

    public void ShuffleDeck(bool isPlaySound)
    {
        if (isPlaySound)
        {
            SoundManager.PlaySFX(SoundManager.SFX.Shuffle_Deck);
        }

        int r = 0;
        BattleCardData temp;
        for (int index = 0; index < CardList[CardZone.Deck].Count; ++index)
        {
            r = Random.Range(0, CardList[CardZone.Deck].Count);
            temp = CardList[CardZone.Deck][r];
            CardList[CardZone.Deck][r] = CardList[CardZone.Deck][index];
            CardList[CardZone.Deck][index] = temp;
        }
    }
    
    public string GetDebugText()
    {
        if (_sb == null) _sb = new StringBuilder();
        _sb.Clear();

        foreach (CardZone zone in System.Enum.GetValues(typeof(CardZone)))
        {
            _sb.AppendFormat("\n{0}:\t", zone.ToString());
            if (CardList[zone] != null && CardList[zone].Count > 0)
            {
                switch (zone)
                {
                    case CardZone.Hand:
                    {
                        foreach (BattleCardData card in CardList[zone])
                        {
                            _sb.AppendFormat(" [{0}]{1}", card.UniqueID, card.BaseID);
                        }
                    }
                    break;

                    case CardZone.Deck:
                    case CardZone.Graveyard:
                    {
                        int count = (CardList[zone].Count <= _debugTextCardCount) ? CardList[zone].Count : _debugTextCardCount;
                        for (int index = 0; index < count; ++index)
                        {
                            _sb.AppendFormat(" [{0}]{1}", CardList[zone][index].UniqueID, CardList[zone][index].BaseID);
                        }
                        if (CardList[zone].Count > _debugTextCardCount)
                        {
                            _sb.Append(" ...");
                        }
                    }
                    break;

                    case CardZone.Battlefield:
                    foreach (BattleCardData card in CardList[zone])
                    {
                        _sb.AppendFormat(" [@{0}][{1}]{2}", card.SlotID, card.UniqueID, card.BaseID);
                    }
                    break;
                }
            }
            else
            {
                _sb.Append(" Empty.");
            }
        }

        return _sb.ToString();
    }
    #endregion

    #region Get and Set value
    private int _GetMaxHP() 
    {
        int value_encode = GameHelper.ConvertCustomStrToInt(_maxHP_encode_str);
        int value = GameHelper.DecodeInt(value_encode, _encodeKey);
        int dummy_value = GameHelper.DecodeInt(_maxHP_encode, _encodeKey);

        if (value_encode != _maxHP_encode)
        {
            GameManager.Instance.OnTriggerImpossibleError(
                  "ERROR_MISMATCH_MAX_HP"
                , this.PlayerIndex
                , GameManager.Instance.Data
                , string.Format("{0} != {1}", value, dummy_value)
            );

            _maxHP_encode = value_encode;

            // error
            Debug.LogErrorFormat("_GetMaxHP: [Error] Imposible error. {0} != {1}", value, dummy_value);
        }

        return value;
    }

    private int _GetCurrentHP()
    {
        int value_encode = GameHelper.ConvertCustomStrToInt(_currentHP_encode_str);
        int value = GameHelper.DecodeInt(value_encode, _encodeKey);
        int dummy_value = GameHelper.DecodeInt(_currentHP_encode, _encodeKey);

        if (value_encode != _currentHP_encode)
        {
            GameManager.Instance.OnTriggerImpossibleError(
                  "ERROR_MISMATCH_CURRENT_HP"
                , this.PlayerIndex
                , GameManager.Instance.Data
                , string.Format("{0} != {1}", value, dummy_value)
            );

            _currentHP_encode = value_encode;

            // error
            Debug.LogErrorFormat("_GetCurrentHP: [Error] Imposible error. {0} != {1}", value, dummy_value);
        }

        return value;
    }

    private int _GetMaxAP()
    {
        int value_encode = GameHelper.ConvertCustomStrToInt(_maxAP_encode_str);
        int value = GameHelper.DecodeInt(value_encode, _encodeKey);
        int dummy_value = GameHelper.DecodeInt(_maxAP_encode, _encodeKey);

        if (value_encode != _maxAP_encode)
        {
            GameManager.Instance.OnTriggerImpossibleError(
                  "ERROR_MISMATCH_MAX_AP"
                , this.PlayerIndex
                , GameManager.Instance.Data
                , string.Format("{0} != {1}", value, dummy_value)
            );

            _maxAP_encode = value_encode;

            // error
            Debug.LogErrorFormat("_GetMaxAP: [Error] Imposible error. {0} != {1}", value, dummy_value);
        }

        return value;
    }

    private int _GetCurrentAP()
    {
        int value_encode = GameHelper.ConvertCustomStrToInt(_currentAP_encode_str);
        int value = GameHelper.DecodeInt(value_encode, _encodeKey);
        int dummy_value = GameHelper.DecodeInt(_currentAP_encode, _encodeKey);

        if (value_encode != _currentAP_encode)
        {
            GameManager.Instance.OnTriggerImpossibleError(
                  "ERROR_MISMATCH_CURRENT_AP"
                , this.PlayerIndex
                , GameManager.Instance.Data
                , string.Format("{0} != {1}", value, dummy_value)
            );

            _currentAP_encode = value_encode;

            // error
            Debug.LogErrorFormat("_GetCurrentAP: [Error] Imposible error. {0} != {1}", value, dummy_value);
        }

        return value;
    }

    private int _GetImmuneCounter()
    {
        int value_encode = GameHelper.ConvertCustomStrToInt(_immuneCounter_encode_str);
        int value = GameHelper.DecodeInt(value_encode, _encodeKey);
        int dummy_value = GameHelper.DecodeInt(_immuneCounter_encode, _encodeKey);

        if (value_encode != _immuneCounter_encode)
        {
            GameManager.Instance.OnTriggerImpossibleError(
                  "ERROR_MISMATCH_IMMUNE"
                , this.PlayerIndex
                , GameManager.Instance.Data
                , string.Format("{0} != {1}", value, dummy_value)
            );

            _immuneCounter_encode = value_encode;

            // error
            Debug.LogErrorFormat("_GetImmuneCounter: [Error] Imposible error. {0} != {1}", value, dummy_value);
        }

        return value;
    }

    private void _SetMaxHP(int value)
    {
        int value_encode = GameHelper.EncodeInt(value, _encodeKey);

        _maxHP_encode = value_encode;
        _maxHP_encode_str = GameHelper.ConvertIntToCustomStr(value_encode);
    }

    private void _SetCurrentHP(int value)
    {
        int value_encode = GameHelper.EncodeInt(value, _encodeKey);

        _currentHP_encode = value_encode;
        _currentHP_encode_str = GameHelper.ConvertIntToCustomStr(value_encode);
    }

    private void _SetMaxAP(int value)
    {
        int value_encode = GameHelper.EncodeInt(value, _encodeKey);

        _maxAP_encode = value_encode;
        _maxAP_encode_str = GameHelper.ConvertIntToCustomStr(value_encode);
    }

    private void _SetCurrentAP(int value)
    {
        int value_encode = GameHelper.EncodeInt(value, _encodeKey);

        _currentAP_encode = value_encode;
        _currentAP_encode_str = GameHelper.ConvertIntToCustomStr(value_encode);
    }

    private void _SetImmuneCounter(int value)
    {
        int value_encode = GameHelper.EncodeInt(value, _encodeKey);

        _immuneCounter_encode = value_encode;
        _immuneCounter_encode_str = GameHelper.ConvertIntToCustomStr(value_encode);
    }
    #endregion
}
