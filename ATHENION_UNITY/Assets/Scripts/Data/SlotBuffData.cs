﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#region Global Enums
public enum SlotBuffTypes
{
    BuffSlotLock
    , BuffSlotDarkMatter
}
#endregion

#region SlotBuffData
public class SlotBuffData
{
    #region Public Properties
    public int UniqueID { get; private set; }
    public DataParam Param { get; protected set; }
    public Requester Requester { get; protected set; }

    public string Key { get { return _key; } }
    public int CounterAmount { get { return _counterAmount; } }
    public BuffCounterType CounterType { get { return _counterType; } }
    public SlotData Buffy { get; private set; }

    public SlotBuffTypes BuffType { get; private set; }
    #endregion

    #region Private Properties
    private string _key = string.Empty;
    private int _counterAmount = -1;
    private BuffCounterType _counterType = BuffCounterType.None;

    private static int _uniqueIndex = 0;
    #endregion

    #region Constructor
    public SlotBuffData(string key, BuffCounterType counterType, int counterAmount, DataParam param, Requester requester)
    {
        UniqueID = RequestUniqueID();
        Param = param;

        SlotBuffTypes type;
        if (GameHelper.StrToEnum(Param.Key, out type))
        {
            BuffType = type;
        }

        Requester = requester;
        SetBuffDataProperties(key, counterType, counterAmount);
    }

    public SlotBuffData(SlotBuffData data)
    {
        UniqueID = data.UniqueID;
        Param = data.Param;
        BuffType = data.BuffType;
        Requester = data.Requester;
        SetBuffDataProperties(data.Key, data.CounterType, data.CounterAmount);
    }

    ~SlotBuffData()
    {
        UnsubscribeAll();
    }
    #endregion

    #region Methods
    public void Subscribe(SlotData slot)
    {
        //Debug.Log("SUB...");
        if (Buffy != null)
        {
            // if have previous owner, unsubscribe first.
            UnsubscribeAll();
        }

        Buffy = slot;
        if (Buffy != null)
        {
            //Debug.LogWarning("SUBSCRIBE SLOT BUFF COUNTING : " + CounterType);
            switch (CounterType)
            {
                case BuffCounterType.EndOfTurn:
                case BuffCounterType.EndOfMyTurn:
                    Buffy.SubscribeEvent(EventKey.EndPhase, Counting);
                    break;

                case BuffCounterType.BeginOfMyTurn:
                    Buffy.SubscribeEvent(EventKey.BeginPhase, Counting);
                    break;
            }
        }
    }

    public void UnsubscribeAll()
    {
        if (Buffy != null)
        {
            switch (CounterType)
            {
                case BuffCounterType.EndOfTurn:
                case BuffCounterType.EndOfMyTurn:
                    Buffy.UnsubscribeEvent(EventKey.EndPhase, Counting);
                    break;

                case BuffCounterType.BeginOfMyTurn:
                    Buffy.UnsubscribeEvent(EventKey.BeginPhase, Counting);
                    break;
            }
        }
    }

    public void SetCountAmountDelta(int deltaCountAmount)
    {
        _counterAmount += deltaCountAmount;

        if (_counterAmount <= 0)
        {
            RemoveBuff();
        }
    }

    protected void Counting(Requester triggerer)
    {
        switch (CounterType)
        {
            case BuffCounterType.EndOfMyTurn:
            case BuffCounterType.BeginOfMyTurn:
            {
                if (GameManager.Instance.CurrentPlayerIndex != Requester.PlayerIndex)
                {
                    return;
                }
            }
            break;
        }

        if (_counterAmount > 0)
        {
            _counterAmount--;
            Buffy.CountBuff(this, null);
            if (_counterAmount <= 0)
            {
                RequestRemoveBuff();
            }
        }
    }

    public static bool CreateSlotBuffData(string key, BuffCounterType counterType, int counterAmount, DataParam param, Requester requester, out SlotBuffData data)
    {
        data = null;

        switch (param.Key)
        {
            case "BuffSlotLock": data = new BuffSlotLock(key, counterType, counterAmount, param, requester); return true;
            case "BuffSlotDarkMatter": data = new BuffSlotDarkMatter(key, counterType, counterAmount, param, requester); return true;
        }

        return false;
    }

    public void SetBuffDataProperties(string key, BuffCounterType counterType, int counterAmount)
    {
        _key = key;
        _counterType = counterType;
        _counterAmount = counterAmount;
    }

    private static int RequestUniqueID()
    {
        return _uniqueIndex++;
    }

    public static void ResetIndex()
    {
        _uniqueIndex = 0;
    }

    public virtual void ActiveBuff()
    {
    }

    public virtual void UpdateBuff()
    {
    }

    // Request Buff to remove buff
    private void RequestRemoveBuff()
    {
        Buffy.RemoveBuff(Key, null);
    }

    public virtual void RemoveBuff()
    {
        UnsubscribeAll();
    }
    #endregion
}
#endregion

#region BuffData Children

#region BuffSlotEnabled
public class BuffSlotLock : SlotBuffData
{
    #region Public Properties
    #endregion

    #region Private Properties
    #endregion

    #region Constructors
    public BuffSlotLock(string key, BuffCounterType counterType, int counterAmount, DataParam param, Requester requester)
                        : base(key, counterType, counterAmount, param, requester)
    {
    }
    #endregion

    #region Methods
    public override void ActiveBuff()
    {
        GameManager.Instance.RequestBuffSlotLock(Buffy.SlotID, CounterAmount, true, Requester.PlayerIndex, false, true);
        Requester triggerer = new Requester(Buffy, Requester.PlayerIndex);
        GameManager.Instance.ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(
                    new EventTriggerData(EventKey.OnSlotLock, triggerer),
                    new EventTriggerData(EventKey.OnFriendSlotLock, triggerer),
                    new EventTriggerData(EventKey.OnEnemySlotLock, triggerer)), true);
    }

    public override void RemoveBuff()
    {
        base.RemoveBuff();
        GameManager.Instance.RequestBuffSlotLock(Buffy.SlotID, CounterAmount, false, Requester.PlayerIndex, false, true);
        Requester triggerer = new Requester(Buffy, Requester.PlayerIndex);
        GameManager.Instance.ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(
                    new EventTriggerData(EventKey.OnSlotUnlock, triggerer),
                    new EventTriggerData(EventKey.OnFriendSlotUnlock, triggerer),
                    new EventTriggerData(EventKey.OnEnemySlotUnlock, triggerer)), true);
    }
    #endregion
}
#endregion

#region BuffSlotDarkMatter
public class BuffSlotDarkMatter : SlotBuffData
{
    #region Constructors
    public BuffSlotDarkMatter(string key, BuffCounterType counterType, int counterAmount, DataParam param, Requester requester)
                        : base(key, counterType, counterAmount, param, requester)
    {
    }
    #endregion

    #region Methods
    public override void ActiveBuff()
    {
        GameManager.Instance.RequestBuffSlotDarkMatter(Requester, Buffy.SlotID, true, Requester.PlayerIndex, false, true);
        Requester triggerer = new Requester(Buffy, Requester.PlayerIndex);
        GameManager.Instance.ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(
                    new EventTriggerData(EventKey.OnSlotBeDarkMatter, triggerer),
                    new EventTriggerData(EventKey.OnFriendSlotBeDarkMatter, triggerer),
                    new EventTriggerData(EventKey.OnEnemySlotBeDarkMatter, triggerer)), true);
    }

    public override void RemoveBuff()
    {
        base.RemoveBuff();
        GameManager.Instance.RequestBuffSlotDarkMatter(Requester, Buffy.SlotID, false, Requester.PlayerIndex, false, true);
        Requester triggerer = new Requester(Buffy, Requester.PlayerIndex);
        GameManager.Instance.ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(
                    new EventTriggerData(EventKey.OnSlotBeNotDarkMatter, triggerer),
                    new EventTriggerData(EventKey.OnFriendSlotBeNotDarkMatter, triggerer),
                    new EventTriggerData(EventKey.OnEnemySlotBeNotDarkMatter, triggerer)), true);
    }
    #endregion
}
#endregion

#endregion

