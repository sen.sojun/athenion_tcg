﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#region CardElementType
public enum CardElementType
{
      NEUTRAL // Wild
    , FIRE
    , WATER
    , AIR
    , EARTH
    , HOLY
    , DARK
}
#endregion

public class CardElement
{
    #region Public Properties
    public int ElementIndex { get { return _elementIndex; } }
    #endregion

    #region Private Properties
    private int _elementIndex = 0;
    #endregion

    #region Constructors
    public CardElement()
    {
        _elementIndex = 0;
    }

    public CardElement(int elementIndex)
    {
        _elementIndex = elementIndex;
    }

    public CardElement(CardElement element)
    {
        _elementIndex = element._elementIndex;
    }

    public CardElement(CardElementType elementType)
    {
        _elementIndex = (1 << (int)elementType);
    }
    #endregion

    #region Methods
    public bool IsElement(CardElementType elementType)
    {
        int elementIndex = _elementIndex & (1 << (int)elementType);
        return (elementIndex > 0);
    }

    public void Add(CardElementType elementType)
    {
        _elementIndex = (_elementIndex | (1 << (int)elementType));
    }

    public void Remove(CardElementType elementType)
    {
        _elementIndex = _elementIndex & (~(1 << (int)elementType));
    }

    public CardElementType ToCardElementType()
    {
        foreach (CardElementType element in System.Enum.GetValues(typeof(CardElementType)))
        {
            if (IsElement(element)) return element;
        }

        return CardElementType.NEUTRAL;
    }

    public override string ToString()
    {
        string text = "";
        foreach (CardElementType elementType in System.Enum.GetValues(typeof(CardElementType)))
        {
            if (IsElement(elementType))
            {
                if (text.Length > 0)
                {
                    text += ", " + elementType.ToString();
                }
                else
                {
                    text += "{" + elementType.ToString();
                }
            }
        }

        if (text.Length <= 0)
        {
            text = "None";
        }
        else
        {
            text += "}";
        }

        return text;
    }

    public static CardElement Create(List<string> elementList)
    {
        if (elementList != null && elementList.Count > 0)
        {
            CardElement result = new CardElement();
            foreach (string elementID in elementList)
            {
                if (elementID != "-") // ignore "-"
                {
                    ElementDBData rawData;
                    bool isSuccess = ElementDB.Instance.GetData(elementID, out rawData);
                    if (isSuccess)
                    {
                        CardElementType elementType;
                        if (GameHelper.StrToEnum(rawData.ID, out elementType))
                        {
                            result.Add(elementType);
                        }
                    }
                }
            }
            return result;
        }

        return (new CardElement());
    }
    #endregion

    #region Operators
    public static CardElement operator +(CardElement data1, CardElement data2)
    {
        int elementIndex = (data1._elementIndex | data2._elementIndex);
        CardElement data = new CardElement(elementIndex);
        return data;
    }

    public static CardElement operator -(CardElement data1, CardElement data2)
    {
        int elementIndex = data1._elementIndex & (~data2._elementIndex);
        CardElement data = new CardElement(elementIndex);
        return data;
    }

    public static bool operator ==(CardElement data1, CardElement data2)
    {
        return (data1.Equals(data2));
    }

    public static bool operator !=(CardElement data1, CardElement data2)
    {
        return (!data1.Equals(data2));
    }

    public bool Equals(CardElement data)
    {
        if (ReferenceEquals(null, data))
        {
            return false;
        }
        if (ReferenceEquals(this, data))
        {
            return true;
        }

        return (this._elementIndex == data._elementIndex);
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj))
        {
            return false;
        }
        if (ReferenceEquals(this, obj))
        {
            return true;
        }

        return obj.GetType() == GetType() && Equals((CardElement)obj);
    }

    public override int GetHashCode()
    {
        return (this._elementIndex.GetHashCode());
    }
    #endregion
}