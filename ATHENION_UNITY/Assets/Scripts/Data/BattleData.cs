﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class BattleData
{
    #region Public Properties
    public List<BattlePlayerData> PlayerDataList { get { return _playerDataList; } }
    public SlotData[] SlotDatas { get { return _slotDatas; } }
    public List<int> BattleCardActiveIDList { get { return _battleCardActiveIDList; } }
    #endregion

    #region Private Properties
    private List<BattlePlayerData> _playerDataList = new List<BattlePlayerData>();
    private SlotData[] _slotDatas = new SlotData[GameManager.Instance.BoardHeight * GameManager.Instance.BoardWidth];
    private List<int> _battleCardActiveIDList = new List<int>();

    private StringBuilder _sb;
    #endregion

    #region Set Methods
    public void AddBattleCardActiveID(int battleCardUniqueID)
    {
        RemoveBattleCardActiveID(battleCardUniqueID);
        _battleCardActiveIDList.Add(battleCardUniqueID);
    }

    public void RemoveBattleCardActiveID(int battleCardUniqueID)
    {
        if (_battleCardActiveIDList.Contains(battleCardUniqueID))
        {
            _battleCardActiveIDList.Remove(battleCardUniqueID);
        }
    }
    #endregion

    #region Methods
    public bool FindBattleCard(int uniqueID, out BattleCardData battleCard)
    {
        CardZone cardZone;
        return FindBattleCard(uniqueID, out battleCard, out cardZone);
    }

    public bool FindBattleCard(int uniqueID, out BattleCardData battleCard, out CardZone cardZone)
    {
        if (PlayerDataList != null)
        {
            foreach (BattlePlayerData battlePlayerData in PlayerDataList)
            {
                foreach (CardZone zone in System.Enum.GetValues(typeof(CardZone)))
                {
                    foreach (BattleCardData card in battlePlayerData.CardList[zone])
                    {
                        if (card.UniqueID == uniqueID)
                        {
                            battleCard = card;
                            cardZone = zone;
                            return true;
                        }
                    }
                }

                if (battlePlayerData.Hero.SkillCard != null && battlePlayerData.Hero.SkillCard.UniqueID == uniqueID)
                {
                    battleCard = battlePlayerData.Hero.SkillCard;
                    cardZone = CardZone.Hero;
                    return true;
                }
            }
        }

        battleCard = null;
        cardZone = CardZone.Deck;
        return false;
    }

    public bool FindBattleCard(string cardID, CardZone zone, PlayerIndex playerIndex, out BattleCardData battleCard)
    {
        if (PlayerDataList != null)
        {
            for (int i = PlayerDataList[(int)playerIndex].CardList[zone].Count - 1; i >= 0; i--)
            {
                BattleCardData card = PlayerDataList[(int)playerIndex].CardList[zone][i];
                if (card.BaseID == cardID)
                {
                    battleCard = card;
                    return true;
                }
            }
        }

        battleCard = null;
        return false;
    }

    public bool FindHeroBattleCard(int uniqueID, out MinionData heroSkill)
    {
        if (PlayerDataList != null)
        {
            foreach (BattlePlayerData battlePlayerData in PlayerDataList)
            {
                if (battlePlayerData.Hero.SkillCard != null && battlePlayerData.Hero.SkillCard.UniqueID == uniqueID)
                {
                    heroSkill = battlePlayerData.Hero.SkillCard;
                    return true;
                }
            }
        }

        heroSkill = null;
        return false;
    }

    public bool FindMinionBattleCard(int uniqueID, out MinionData minion)
    {
        if (PlayerDataList != null)
        {
            foreach (BattlePlayerData battlePlayerData in PlayerDataList)
            {
                foreach (CardZone zone in System.Enum.GetValues(typeof(CardZone)))
                {
                    foreach (BattleCardData card in battlePlayerData.CardList[zone])
                    {
                        if (card.UniqueID == uniqueID && (card.Type == CardType.Minion || card.Type == CardType.Minion_NotInDeck))
                        {
                            minion = card as MinionData;
                            return true;
                        }
                    }
                }
            }
        }

        minion = null;
        return false;
    }

    public bool FindMinionBattleCard(string cardBaseID, out List<MinionData> minionList)
    {
        minionList = new List<MinionData>();

        if (PlayerDataList != null)
        {
            foreach (BattlePlayerData battlePlayerData in PlayerDataList)
            {
                foreach (CardZone zone in System.Enum.GetValues(typeof(CardZone)))
                {
                    foreach (BattleCardData card in battlePlayerData.CardList[zone])
                    {
                        if (card.BaseID == cardBaseID && (card.Type == CardType.Minion || card.Type == CardType.Minion_NotInDeck))
                        {
                            minionList.Add(card as MinionData);
                        }
                    }
                }
            }
        }

        return (minionList != null && minionList.Count > 0);
    }

    public bool FindSpellBattleCard(int uniqueID, out SpellData spell)
    {
        if (PlayerDataList != null)
        {
            foreach (BattlePlayerData battlePlayerData in PlayerDataList)
            {
                foreach (CardZone zone in System.Enum.GetValues(typeof(CardZone)))
                {
                    foreach (BattleCardData card in battlePlayerData.CardList[zone])
                    {
                        if (card.UniqueID == uniqueID && (card.Type == CardType.Spell || card.Type == CardType.Spell_NotInDeck))
                        {
                            spell = card as SpellData;
                            return true;
                        }
                    }
                }
            }
        }

        spell = null;
        return false;
    }

    public List<MinionData> GetAllMinionsInSlot()
    {
        List<MinionData> result = new List<MinionData>();
        foreach (SlotData slot in SlotDatas)
        {
            if (!slot.IsSlotEmpty)
            {
                MinionData minion;
                if (FindMinionBattleCard(slot.BattleCardUniqueID, out minion))
                {
                    if (minion.CurrentZone == CardZone.Battlefield)
                    {
                        result.Add(minion);
                    }
                }
            }
        }
        return result;
    }

    public List<int> GetAllEmptySlot()
    {
        List<int> slotIDList = new List<int>();

        if (SlotDatas != null && SlotDatas.Length > 0)
        {
            foreach (SlotData slot in SlotDatas)
            {
                if (slot.IsSlotEmpty)
                {
                    slotIDList.Add(slot.SlotID);
                }
            }
        }

        return slotIDList;
    }

    public string GetBoardDebugText()
    {
        if (_sb == null) _sb = new StringBuilder();
        _sb.Clear();

        for (int row = 0; row < GameManager.Instance.BoardHeight; ++row)
        {
            for (int column = 0; column < GameManager.Instance.BoardWidth; ++column)
            {
                int slotID = (row * GameManager.Instance.BoardWidth) + column;

                if (SlotDatas[slotID].IsSlotEmpty)
                {
                    // Empty
                    _sb.Append("[       ]");
                }
                else if (SlotDatas[slotID].BattleCardUniqueID >= 0)
                {
                    BattleCardData battleCard;
                    bool isFound = FindBattleCard(SlotDatas[slotID].BattleCardUniqueID, out battleCard);
                    if (isFound)
                    {
                        _sb.AppendFormat(
                              "[{0}:{1}]"
                            , battleCard.UniqueID
                            , battleCard.Name
                        );
                    }
                    else
                    {
                        _sb.Append("[Error]");
                    }
                }
                else
                {
                    // Block Slot
                    _sb.Append("[   X   ]");
                }
            }
            _sb.Append("\n");
        }

        return _sb.ToString();
    }

    #endregion
}
