﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CardAbilityData
{
    #region Public Properties
    public int UniqueID { get; protected set; }
    public int UniqueCardID { get; protected set; }
    public BattleCardData Owner { get; private set; }
    public CardAbilityDBData Data { get; private set; }

    public bool IsInit { get { return _isInit; } }
    public EventKey TriggerKey { get; private set; }
    public Requester Triggerer { get { return _triggerer; } }
    public Dictionary<string, string> ParamList { get { return _paramList; } }

    public List<ConditionData> ConditionList { get { return _conditionList; } }
    public List<EffectData> EffectList { get { return _effectList; } }

    public List<CommandData> CommandList { get { return _commandList; } }
    #endregion

    #region Private Properties
    private List<ConditionData> _conditionList;
    private List<EffectData> _effectList;
    private List<CommandData> _commandList;
    
    private int _activeIndex = 0;
    private bool _isInit = false;
    private Dictionary<string, string> _paramList;
    private Requester _triggerer;

    private UnityAction<Dictionary<string, string>> _onPrepareComplete;
    private UnityAction _onPrepareFail;
    private UnityAction _onActionComplete;

    private static int _uniqueIndex = 0;
    #endregion

    #region Constructors
    private CardAbilityData()
    {
        _conditionList = new List<ConditionData>();
        _effectList = new List<EffectData>();
        _commandList = new List<CommandData>();
        _paramList = new Dictionary<string, string>();
        _activeIndex = 0;
        _triggerer = null;
        _isInit = false;
    }

    public CardAbilityData(CardAbilityData data)
    {
        SetupData(data.UniqueID, data.Data.ID, data.Owner);
    }
    #endregion

    #region Prepare Methods
    private void StartPrepare()
    {
        //Debug.Log("Start prepare " + Data.GetDebugText());

        _activeIndex = 0;
        _paramList = new Dictionary<string, string>();

        OnPrepare();
    }

    private void OnPrepare()
    {
        if (_effectList != null && _activeIndex < _effectList.Count)
        {
            //Debug.Log("Start Prepare " + _effectList[_activeIndex].GetDebugText());
            _effectList[_activeIndex].Prepare(OnPrepareEnd, OnPrepareCancel);
        }
        else
        {
            OnPrepareEnd();
        }
    }

    private void OnPrepareEnd()
    {
        if (_effectList != null && _effectList.Count > _activeIndex)
        {
            //Debug.Log("End Prepare " + _effectList[_activeIndex].GetDebugText());
        }

        ++_activeIndex;

        if (_effectList != null && _activeIndex < _effectList.Count)
        {
            OnPrepare();
        }
        else
        {
            // Prepare Successful
            OnPrepareComplete();
        }
    }

    private void OnPrepareCancel()
    {
        //Debug.Log("Cancel Prepare " + Data.ID);

        if (_onPrepareFail != null)
        {
            _onPrepareFail.Invoke();
        }
    }

    private void OnPrepareComplete()
    {
        //Debug.Log("Complete Prepare " + Data.ID);

        if (_onPrepareComplete != null)
        {
            _onPrepareComplete.Invoke(_paramList);
        }

        //StartAction();
    }
    #endregion

    #region Action Methods
    private void StartAction()
    {
        _activeIndex = 0;
        _commandList = new List<CommandData>();

        // Play trigger icon blink
        CardUIData cardUIData = new CardUIData(Owner);
        UIManager.Instance.RequestPassiveActivate(cardUIData, GetAbilityFeedbackKey());

        //WTF code
        //DataParam vfxParam;
        //if (GetVFXParam(0, out vfxParam))
        //{
        //    string vfxKey = vfxParam.Parameters[0];
        //    if (vfxKey != "-")
        //    {
        //        //Debug.LogFormat("vfxKey : {0}", vfxKey);
        //        UIManager.Instance.RequestDealDamageGroup(cardUIData, vfxKey, null);
        //    }
        //}

        OnAction();
    }

    private void OnAction()
    {
        if (_effectList != null && _activeIndex < _effectList.Count)
        {
            //Debug.Log("Start Action " + _effectList[_activeIndex].GetDebugText());
            _effectList[_activeIndex].Action(OnActionEnd);
        }
        else
        {
            OnActionEnd();
        }
    }

    private void OnActionEnd()
    {
        if (_effectList != null && _effectList.Count > _activeIndex)
        {
            //Debug.Log("End Action " + _effectList[_activeIndex].GetDebugText());
        }

        ++_activeIndex;

        if (_effectList != null && _activeIndex < _effectList.Count)
        {
            OnAction();
        }
        else
        {
            // Action Successful
            OnActionComplete();
        }
    }

    private void OnActionComplete()
    {
        Debug.Log("Complete Action " + Data.ID);

        if(_commandList != null && _commandList.Count > 0)
        {
            GameManager.Instance.ActionAddCommandListToSecondQueue(_commandList, true);
        }

        if (_onActionComplete != null)
        {
            _onActionComplete.Invoke();
        }
    }
    #endregion

    #region Methods
    private bool SetupData(int uniqueID, string abilityID, BattleCardData battleCardData)
    {
        bool isSuccess = true;
        _isInit = false;

        UniqueID = uniqueID;
        Owner = battleCardData;
        UniqueCardID = battleCardData.UniqueID;

        Requester requester = new Requester(this);

        CardAbilityDBData cardAbilityDBData;
        isSuccess = CardAbilityDB.Instance.GetData(abilityID, out cardAbilityDBData);
        if (isSuccess)
        {
            Data = cardAbilityDBData;

            // Create condition
            _conditionList = new List<ConditionData>();
            foreach (string conditionIDText in cardAbilityDBData.ConditionList)
            {
                if (conditionIDText != "-") // ignore "-"
                {
                    bool isCreateSuccess = true;
                    DataParam param;
                    isCreateSuccess = DataParam.TextToDataParam(conditionIDText, DataParam.ParamType.Boolean, out param);

                    if (isCreateSuccess)
                    {
                        ConditionData conditionData;
                        isCreateSuccess = ConditionParser.ParamToCondition(param, requester, out conditionData);
                        if (isCreateSuccess)
                        {
                            _conditionList.Add(conditionData);
                        }
                    }
                }
            }

            // Create effect
            _effectList = new List<EffectData>();
            foreach (string effectIDText in cardAbilityDBData.EffectList)
            {
                if (effectIDText != "-") // ignore "-"
                {
                    bool isCreateSuccess = true;
                    DataParam param;
                    isCreateSuccess = DataParam.TextToDataParam(effectIDText, DataParam.ParamType.Effect, out param);

                    if (isCreateSuccess)
                    {
                        EffectData effectData;
                        isCreateSuccess = EffectParser.ParamToEffect(param, requester, out effectData);
                        if (isCreateSuccess)
                        {
                            effectData.SetUniqueKey(string.Format("{0}_{1}", abilityID, _effectList.Count + 1));
                            _effectList.Add(effectData);
                        }
                    }
                }
            }

            Init();

            return true;
        }

        return false;
    }

    private void Init()
    {
        _isInit = false;
        if (Data != null && Owner != null 
            && (Owner.Type == CardType.Minion || Owner.Type == CardType.Minion_NotInDeck || Owner.Type == CardType.Hero))
        {
            EventKey key;
            if (GameHelper.StrToEnum(Data.Trigger, out key))
            {
                TriggerKey = key;
                //Debug.Log("Subscribe " + key);

                // Subscribe to owner.
                MinionData minion = Owner as MinionData;
                minion.SubscribeAbility(TriggerKey, UniqueID);
            }
            else
            {
                TriggerKey = EventKey.None;
            }
        }
    }

    public bool IsCanActive()
    {
        if (_conditionList != null && _conditionList.Count > 0)
        {
            foreach (ConditionData condition in _conditionList)
            {
                if (!condition.IsValid())
                {
                    return false;
                }
            }
        }

        return true;
    }

    /*
    public void Active(UnityAction onActionComplete, UnityAction onPrepareComplete, UnityAction onPrepareFail)
    {
        _onActionComplete = onActionComplete;
        _onPrepareComplete = onPrepareComplete;
        _onPrepareFail = onPrepareFail;
        _paramList = new List<string>();

        if (IsCanActive())
        {
            StartPrepare();
        }
        else
        {
            OnPrepareCancel();
        }
    }
    */

    public void AddCommandData(List<CommandData> commandList)
    {
        if(commandList != null)
        {
            foreach (CommandData cmd in commandList)
            {
                AddCommandData(cmd);
            }
        }
    }

    public void AddCommandData(CommandData cmd)
    {
        if(cmd != null)
        {
            if (_commandList == null)
            {
                _commandList = new List<CommandData>();
            }
            _commandList.Add(cmd);
        }
    }

    public void SetPrepareProperties(Requester triggerer)
    {
        _triggerer = triggerer;
    }

    public void TryPrepare(Requester triggerer, UnityAction<Dictionary<string, string>> onPrepareComplete, UnityAction onPrepareFail)
    {
        _triggerer = triggerer;
        _onActionComplete = null;
        _onPrepareComplete = onPrepareComplete;
        _onPrepareFail = onPrepareFail;
        _paramList = new Dictionary<string, string>();

        if (IsCanActive())
        {
            StartPrepare();
        }
        else
        {
            OnPrepareCancel();
        }
    }

    public void TryAction(Dictionary<string, string> paramList, UnityAction onActionComplete)
    {
        _onActionComplete = onActionComplete;
        _paramList = new Dictionary<string, string>(paramList);

        StartAction();
    }

    public bool GetVFXParam(string indexKey, out DataParam result)
    {
        switch (indexKey.ToUpper())
        {
            case "STARTACTIONVFXKEY":
            case "VFX_PARAM_0":
            {
                return GetVFXParam(0, out result);
            }
            //break;

            case "VFX_PARAM_1":
            {
                return GetVFXParam(1, out result);
            }
            //break;

            case "VFX_PARAM_2":
            {
                return GetVFXParam(2, out result);
            }
            //break;

            case "VFX_PARAM_3":
            {
                return GetVFXParam(3, out result);
            }
            //break;

            default:
            {
                result = null;
                return false;
            }
            //break;
        }
    }

    public bool GetVFXParam(int index, out DataParam result)
    {
        if (this.Data.VFXParams != null && this.Data.VFXParams.Count > index)
        {
            List<string> parameters = new List<string>(this.Data.VFXParams[index]);
            result = new DataParam(DataParam.ParamType.VFX, "VFX_PARAM_" + index.ToString(), parameters);
            return true;
        }

        result = null;
        return false;
    }

    public string GetDebugText()
    {
        string debugText = "";

        string conditionText = "";
        foreach (ConditionData condition in _conditionList)
        {
            if (conditionText.Length > 0)
            {
                conditionText += "\n" + condition.GetDebugText();
            }
            else
            {
                conditionText = condition.GetDebugText();
            }
        }

        string effectText = "";
        foreach (EffectData effect in _effectList)
        {
            if (effectText.Length > 0)
            {
                effectText += "\n" + effect.GetDebugText();
            }
            else
            {
                effectText = effect.GetDebugText();
            }
        }

        debugText = string.Format(
            "{0}\nCondition\n{1}\nEffect\n{2}"
            , Data.GetDebugText()
            , conditionText
            , effectText
        );

        return debugText;
    }

    public static bool CreateAbility(string abilityID, BattleCardData owner, out CardAbilityData data)
    {
        CardAbilityDBData cardAbilityDBData;
        bool isSuccess = CardAbilityDB.Instance.GetData(abilityID, out cardAbilityDBData);
        if (isSuccess && owner != null)
        {
            data = new CardAbilityData();
            bool isComplete = data.SetupData(RequestUniqueID(), abilityID, owner);

            if (!isComplete)
            {
                data = null;
            }

            return isComplete;
        }
        else
        {
            Debug.LogError("CreateCard: Load ability failed " + abilityID);

            data = null;
            return false;
        }
    }

    private static int RequestUniqueID()
    {
        return _uniqueIndex++;
    }

    public static void ResetIndex()
    {
        _uniqueIndex = 0;
    }
    #endregion

    #region Status 
    public bool IsAbilityLink()
    {
        if (_effectList != null && _effectList.Count > 0)
        {
            foreach (EffectData effect in _effectList)
            {
                switch (effect.GetType().ToString())
                {
                    case "DoMinionLink": return true;
                }
            }
        }

        return false;
    }

    public bool IsAbilityAura()
    {
        if (_effectList != null && _effectList.Count > 0)
        {
            foreach (EffectData effect in _effectList)
            {
                switch (effect.GetType().ToString())
                {
                    case "DoMinionAura": return true;
                }
            }
        }

        return false;
    }

    public bool IsAbilityBerserk()
    {
        if (_effectList != null && _effectList.Count > 0)
        {
            foreach (EffectData effect in _effectList)
            {
                switch (effect.GetType().ToString())
                {
                    case "DoMinionBerserk": return true;
                }
            }
        }

        return false;
    }

    private bool IsAbilityBackstab()
    {
        if (TriggerKey == EventKey.OnPreAttack)
        {
            foreach (ConditionData condition in _conditionList)
            {
                switch (condition.Param.Key)
                {
                    case "IsCritical": return true;
                }
            }
        }

        return false;
    }

    private bool IsAbilityLastWish()
    {
        return (TriggerKey == EventKey.LastWish);
    }

    private bool IsAbilityUntargetable()
    {
        // DoMinionAura(MinionMe,true,BuffMinionUntargetable)

        if (TriggerKey == EventKey.OnEnter)
        {
            foreach (EffectData effect in _effectList)
            {
                switch (effect.GetType().ToString())
                {
                    case "DoMinionAura":
                    {
                        if(
                               (effect.Param.Parameters[0].Contains("MinionMe"))
                            && (effect.Param.Parameters[1].Contains("true"))
                            && (effect.Param.Parameters[2].Contains("BuffMinionUntargetable"))
                        )
                        {

                            return true;
                        }
                    }
                    break;
                }
            }
        }

        return false;
    }

    public AbilityFeedback GetAbilityFeedbackKey()
    {
        if(IsAbilityLink())
        {
            return AbilityFeedback.Link;
        }

        if(IsAbilityBerserk())
        {
            return AbilityFeedback.Berserk;
        }

        if(IsAbilityBackstab())
        {
            return AbilityFeedback.Backstab;
        }

        if(IsAbilityLastWish())
        {
            return AbilityFeedback.Lastwish;
        }

        if (IsAbilityUntargetable())
        {
            return AbilityFeedback.Untargetable;
        }

        return AbilityFeedback.Trigger;
    }
    #endregion
}
