﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#region CardPassive
public enum CardPassiveType
{
    // Good Passive
      Stealth
    , Taunt
    , Link
    , Aura
    , Backstab
    , Berserk

    // Bad Passive
    , Fear
    , Freeze
    , Silence
}
#endregion

public class CardPassive 
{
    #region Public Properties
    public int PassiveIndex { get { return _passiveIndex; } }
    #endregion

    #region Private Properties
    private int _passiveIndex = 0;
    #endregion

    #region Constructors
    public CardPassive()
    {
        _passiveIndex = 0;
    }

    public CardPassive(int passiveIndex)
    {
        _passiveIndex = passiveIndex;
    }

    public CardPassive(CardPassive passive)
    {
        _passiveIndex = passive._passiveIndex;
    }

    public CardPassive(CardPassiveType passiveType)
    {
        _passiveIndex = (1 << (int)passiveType);
    }
    #endregion

    #region Methods
    public bool IsPassive(CardPassiveType passiveType)
    {
        int passiveIndex = _passiveIndex & (1 << (int)passiveType);
        return (passiveIndex > 0);
    }

    public void Add(CardPassiveType passiveType)
    {
        _passiveIndex = (_passiveIndex | (1 << (int)passiveType));
    }

    public void Remove(CardPassiveType passiveType)
    {
        _passiveIndex = _passiveIndex & (~(1 << (int)passiveType));
    }

    public override string ToString()
    {
        string text = "";
        foreach (CardPassiveType passiveType in System.Enum.GetValues(typeof(CardPassiveType)))
        {
            if (IsPassive(passiveType))
            {
                if (text.Length > 0)
                {
                    text += ", " + passiveType.ToString();
                }
                else
                {
                    text += "{" + passiveType.ToString();
                }
            }
        }

        if (text.Length <= 0)
        {
            text = "None";
        }
        else
        {
            text += "}";
        }

        return text;
    }

    public static CardPassive Create(List<string> passiveList)
    {
        if (passiveList != null && passiveList.Count > 0)
        {
            CardPassive result = new CardPassive();
            foreach (string passiveKey in passiveList)
            {
                if (passiveKey != "-") // ignore "-"
                {
                    CardPassiveType passiveType;
                    if (GameHelper.StrToEnum<CardPassiveType>(passiveKey, out passiveType))
                    {
                        result.Add(passiveType);
                    }
                }
            }
            return result;
        }

        return (new CardPassive());
    }
    #endregion

    #region Operators
    public static CardPassive operator +(CardPassive data1, CardPassive data2)
    {
        int passiveIndex = (data1._passiveIndex | data2._passiveIndex);
        CardPassive data = new CardPassive(passiveIndex);
        return data;
    }

    public static CardPassive operator -(CardPassive data1, CardPassive data2)
    {
        int passiveIndex = data1._passiveIndex & (~data2._passiveIndex);
        CardPassive data = new CardPassive(passiveIndex);
        return data;
    }

    public static bool operator ==(CardPassive data1, CardPassive data2)
    {
        return (data1.Equals(data2));
    }

    public static bool operator !=(CardPassive data1, CardPassive data2)
    {
        return (!data1.Equals(data2));
    }

    public bool Equals(CardPassive data)
    {
        if (ReferenceEquals(null, data))
        {
            return false;
        }
        if (ReferenceEquals(this, data))
        {
            return true;
        }

        return (this._passiveIndex == data._passiveIndex);
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj))
        {
            return false;
        }
        if (ReferenceEquals(this, obj))
        {
            return true;
        }

        return obj.GetType() == GetType() && Equals((CardPassive)obj);
    }

    public override int GetHashCode()
    {
        return (this._passiveIndex.GetHashCode());
    }
    #endregion
}
