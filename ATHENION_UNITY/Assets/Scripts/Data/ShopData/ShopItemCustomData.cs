﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ShopItemCustomData
{
    public ShopItemType ShopItemType { get; private set; }
    public CosmeticType CosmeticType { get; private set; }

    public ShopOption Option { get { return _option; } }
    private ShopOption _option;

    public List<ItemData> ItemDetail = new List<ItemData>();

    public ShopItemCustomData()
    {
    }

    public ShopItemCustomData(string json)
    {
        Dictionary<string, object> customData = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
        Dictionary<string, object> option = customData.ContainsKey("Option") ? JsonConvert.DeserializeObject<Dictionary<string, object>>(customData["Option"].ToString()) : new Dictionary<string, object>();
        List<Dictionary<string, object>> itemDetail = customData.ContainsKey("ItemDetail") ? JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(customData["ItemDetail"].ToString()) : new List<Dictionary<string, object>>();

        SetOption(option);
        SetItemDetail(itemDetail);
        SetShopItemType(customData);
        SetCosmeticType(customData);
    }

    private void SetShopItemType(Dictionary<string, object> customData)
    {
        try
        {
            ShopItemType = customData.ContainsKey("ShopItemType") ? customData["ShopItemType"].ToString().ToUpper().ToEnum<ShopItemType>() : ShopItemType.ETC;
        }
        catch (Exception e)
        {
            Debug.Log(e.ToString());
        }
    }

    private void SetCosmeticType(Dictionary<string, object> customData)
    {
        try
        {
            CosmeticType = customData.ContainsKey("CosmeticType") ? customData["CosmeticType"].ToString().ToUpper().ToEnum<CosmeticType>() : CosmeticType.SKIN;
        }
        catch (Exception e)
        {
            Debug.Log(e.ToString());
        }
    }

    private void SetItemDetail(List<Dictionary<string, object>> itemDetail)
    {
        ItemDetail = new List<ItemData>();
        foreach (Dictionary<string, object> item in itemDetail)
        {
            string itemID = (string)item["ItemID"];
            int amount = int.Parse(item["Amount"].ToString());

            ItemDetail.Add(new ItemData(itemID, amount));
        }
    }

    private void SetOption(Dictionary<string, object> option)
    {
        _option.MaxCount = option.ContainsKey("MaxCount") ? int.Parse(option["MaxCount"].ToString()) : -1;
        _option.Image = option.ContainsKey("Image") ? option["Image"].ToString() : "";
        _option.IsPack = option.ContainsKey("IsPack") ? bool.Parse(option["IsPack"].ToString()) : false;
        _option.IsLimited = option.ContainsKey("IsLimited") ? bool.Parse(option["IsLimited"].ToString()) : false;

        _option.DiscountTag = option.ContainsKey("DiscountTag") ? option["DiscountTag"].ToString() : "";
        _option.ReleaseDate = option.ContainsKey("ReleaseDate") ? Convert.ToDateTime(option["ReleaseDate"].ToString()) : DateTime.MinValue;
        _option.EndDate = option.ContainsKey("EndDate") ? Convert.ToDateTime(option["EndDate"].ToString()) : DateTime.MaxValue;

        SetTagOption(option);
        SetTagColorOption(option);
    }

    private void SetTagOption(Dictionary<string, object> option)
    {
        if (option.ContainsKey("Tag"))
        {
            string tag = option["Tag"].ToString().ToUpper();
            _option.Tag = tag.ToEnum<ShopOption.TagOption>();
        }
        else
        {
            _option.Tag = ShopOption.TagOption.NONE;
        }
    }

    private void SetTagColorOption(Dictionary<string, object> option)
    {
        if (option.ContainsKey("TagColor"))
        {
            string tagColor = option["TagColor"].ToString().ToUpper();
            _option.TagColor = tagColor.ToEnum<ShopOption.TagColorOption>();
        }
        else
        {
            _option.TagColor = ShopOption.TagColorOption.NONE;
        }
    }

}

public struct ShopOption
{
    public enum TagOption { NONE, BEST, NEW, SPECIAL, HOT, SAVING, GREAT }
    public enum TagColorOption { NONE, ORANGE, RED }

    public bool IsPack;
    public bool IsLimited;
    public int MaxCount;
    public string Image;
    public string DiscountTag;

    public TagOption Tag;
    public TagColorOption TagColor;

    public DateTime ReleaseDate;
    public DateTime EndDate;
}