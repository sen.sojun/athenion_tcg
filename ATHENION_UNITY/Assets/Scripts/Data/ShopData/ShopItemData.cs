﻿using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using PlayFab.ClientModels;
using UnityEngine;

[Serializable]
public class ShopItemData
{
    #region Public Properties
    public ShopTypes ShopType;
    public string StoreID { get; private set; }
    public string ItemId { get; private set; }

    public VirtualCurrency Currency { get; private set; }
    public int Price { get; private set; }

    public bool IsInAppPurchase { get { return Currency == VirtualCurrency.RM; } }
    public int MaxCount { get { return CustomData.Option.MaxCount; } }
    public string DiscountTag { get { return CustomData.Option.DiscountTag; } }

    public bool IsLimited { get { return CustomData.Option.IsLimited; } }
    public bool IsBuyable { get { return Buyable(); } }
    public bool IsPack { get { return CustomData.Option.IsPack; } }

    public bool IsTimeValid { get { return CheckTimeValid(); } }
    public bool IsAtLimitAmount { get { return CheckIsLimit(); } }

    public DateTime ReleaseDate { get { return CustomData.Option.ReleaseDate; } }
    public DateTime EndDate { get { return CustomData.Option.EndDate; } }

    public string ImagePath { get { return CustomData.Option.Image; } }

    public ShopItemCustomData CustomData
    {
        get
        {
            if (_customData == null)
                _customData = new ShopItemCustomData();
            return _customData;
        }
        private set
        {
            _customData = value;
        }
    }

    #endregion

    #region Private Properties

    private ShopItemCustomData _customData;

    #endregion

    #region Constructors

    public ShopItemData(string itemID, VirtualCurrency currency, int price, List<ItemData> itemDataList = null)
    {
        ItemId = ItemId;
        this.Price = price;
        this.Currency = currency;
        CustomData.ItemDetail = itemDataList;
    }

    public ShopItemData(string storeId, ShopTypes shopType, StoreItem item, VirtualCurrency currency, int price)
    {
        ShopType = shopType;
        StoreID = storeId;
        ItemId = item.ItemId;
        this.Price = price;
        this.Currency = currency;

        SetCustomData(item.CustomData);

    }

    public ShopItemData(ShopItemData item)
    {
        StoreID = item.StoreID;
        ItemId = item.ItemId;
        Price = item.Price;
        Currency = item.Currency;

        CustomData = item.CustomData;
    }
    #endregion

    #region Methods

    private bool Buyable()
    {
        if (!IsTimeValid)
            return false;
        if (IsAtLimitAmount)
            return false;
        return true;
    }

    private void SetCustomData(object customData)
    {
        if (customData == null) return;
        string json = customData.ToString();
        CustomData = new ShopItemCustomData(json);
        if (CustomData == null)
        {
            Debug.Log(CustomData == null);
            CustomData = new ShopItemCustomData();
        }
    }

    private bool CheckTimeValid()
    {
        DateTime releaseDate = CustomData.Option.ReleaseDate;
        DateTime endDate = CustomData.Option.EndDate;
        DateTime currentDate = DateTimeData.GetDateTimeUTC();

        return currentDate > releaseDate && currentDate < endDate;
    }

    private bool CheckIsLimit()
    {
        if (MaxCount <= 0)
            return false;

        int amount = 0;
        if (IsLimited)
        {
            int currentSeason = DataManager.Instance.GetCurrentSeasonIndex();
            List<ItemPurchaseLogData> logs = DataManager.Instance.GetPurchaseLimitedItemLogs().GetPurchaseLogsInSeason(currentSeason);
            amount = logs.FindAll(item => item.ItemID == this.ItemId).Count;
        }
        else
        {
            string itemKey = GameHelper.ConvertItemIDToItemKey(ItemId);
            amount = DataManager.Instance.InventoryData.GetAmountByItemKey(itemKey);
        }
        return amount >= MaxCount;
    }

    public string GetDebugText()
    {
        return JsonConvert.SerializeObject(this);
    }
    #endregion
}
