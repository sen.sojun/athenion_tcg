﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoosterPackShopData : ShopData
{
    public string PackID = "";
    public string ShopName = "";
    public Dictionary<string, string> Rate = new Dictionary<string, string>();
    public Dictionary<string, string> CardList = new Dictionary<string, string>();
    public Dictionary<string, string> ItemList = new Dictionary<string, string>();
    /// <summary>
    /// key is CardID
    /// Value is List of ItemData
    /// </summary>
    public Dictionary<string, List<ItemData>> CosmeticRewards;

    public BoosterPackShopData(ShopData data) : base(data)
    {
    }

    public BoosterPackShopData(ShopTypes shopType, string storeID) : base(shopType, storeID)
    {
    }

    public override void SetCustomData(string json)
    {
        if (string.IsNullOrEmpty(json))
            return;

        Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
        base.ReleaseDate = rawData.ContainsKey("ReleaseDate") ? (DateTime)rawData["ReleaseDate"] : DateTime.MinValue;
        base.EndDate = rawData.ContainsKey("EndDate") ? (DateTime)rawData["EndDate"] : DateTime.MaxValue;

        ShopName = rawData.ContainsKey("ShopName") ? rawData["ShopName"].ToString() : "";
        PackID = rawData.ContainsKey("PackID") ? rawData["PackID"].ToString() : "";
        Rate = JsonConvert.DeserializeObject<Dictionary<string, string>>(rawData["Rate"].ToString());
        CardList = JsonConvert.DeserializeObject<Dictionary<string, string>>(rawData["CardList"].ToString());
        ItemList = rawData.ContainsKey("ItemList") ? JsonConvert.DeserializeObject<Dictionary<string, string>>(rawData["ItemList"].ToString()) : null;

        if (rawData.ContainsKey("CosmeticReward"))
        {
            Dictionary<string, object> rawCosmeticRewardData = JsonConvert.DeserializeObject<Dictionary<string, object>>(rawData["CosmeticReward"].ToString());
            CosmeticRewards = new Dictionary<string, List<ItemData>>();
            foreach (KeyValuePair<string, object> item in rawCosmeticRewardData)
            {
                string key = item.Key;
                List<object> rawItemData = JsonConvert.DeserializeObject<List<object>>(item.Value.ToString());
                CosmeticRewards.Add(key, ConvertRawCosmeticDataToItemDataList(rawItemData));
            }
        }
    }

    private List<ItemData> ConvertRawCosmeticDataToItemDataList(List<object> rawData)
    {
        List<ItemData> result = new List<ItemData>();
        for (int i = 0; i < rawData.Count; i++)
        {
            Dictionary<string, object> data = JsonConvert.DeserializeObject<Dictionary<string, object>>(rawData[i].ToString());

            string itemID = data["ItemID"].ToString();
            int amount = int.Parse(data["Amount"].ToString());

            result.Add(new ItemData(itemID, amount));
        }
        return result;
    }
}
