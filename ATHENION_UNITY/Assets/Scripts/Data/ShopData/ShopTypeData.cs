﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

[Serializable]
public class ShopTypeData
{
    #region Enum 
    #endregion

    #region Public Properties
    public ShopTypes ShopType { get; private set; }
    public int Count { get { return ShopDict == null ? -1 : ShopDict.Count; } }
    public string[] StoreID { get; private set; }

    /// <summary>
    /// Dict of ShopData. Key is StoreID.
    /// </summary>
    public Dictionary<string, ShopData> ShopDict { get; private set; }

    public bool IsLoadCompleted
    {
        get
        {
            return ShopDict != null && StoreID != null && IsLoadDataComplete() /*&& (ShopDict.Count == StoreID.Length)*/;
        }
    }
    #endregion

    #region Private Properties  
    #endregion

    #region Constructors
    public ShopTypeData()
    {
    }

    public ShopTypeData(ShopTypes type, string[] storeId)
    {
        this.ShopType = type;
        this.StoreID = storeId;
        ShopDict = new Dictionary<string, ShopData>();
        foreach (string id in storeId)
        {
            if (ShopDict.ContainsKey(id) == false)
                ShopDict.Add(id, null);
        }
    }
    #endregion

    #region Methods 
    public void SetShopData(ShopData shopData)
    {
        if (ShopDict.ContainsKey(shopData.StoreID))
        {
            ShopDict[shopData.StoreID] = shopData;
        }
    }

    private bool IsLoadDataComplete()
    {
        foreach (var item in ShopDict)
        { 
            if (item.Value == null)
                return false;
        }

        return true;
    }

    public ShopData GetFirstShopData()
    {
        return ShopDict[StoreID[0]];
    }

    public string GetDebugText()
    {
        return JsonConvert.SerializeObject(this);
    }
    #endregion
}