﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public abstract class ShopData
{
    public List<ShopItemData> ShopItemDataList { get; protected set; }
    public ShopTypes ShopType { get; protected set; }
    public string StoreID { get; }
    public bool IsValidDate
    {
        get
        {
            return DateTimeData.GetDateTimeUTC() > ReleaseDate && DateTimeData.GetDateTimeUTC() < EndDate;
        }
    }

    public DateTime ReleaseDate = DateTime.MinValue;
    public DateTime EndDate = DateTime.MaxValue;

    public ShopData(ShopTypes shopType, string storeID)
    {
        this.ShopType = shopType;
        this.StoreID = storeID;
        ShopItemDataList = new List<ShopItemData>();
    }

    public ShopData(ShopData data)
    {
        this.ShopType = data.ShopType;
        this.StoreID = data.StoreID;
        ShopItemDataList = new List<ShopItemData>(data.ShopItemDataList);
    }

    public void AddItem(ShopItemData itemData)
    {
        ShopItemDataList.Add(itemData);
    }

    public ShopItemData FindItem(string itemID)
    {
        if (ShopItemDataList != null && ShopItemDataList.Count > 0)
        {
            return ShopItemDataList.Find(obj => obj.ItemId == itemID);
        }

        return null;
    }

    public abstract void SetCustomData(string customDataJson);

    public virtual string GetDebugText()
    {
        return Newtonsoft.Json.JsonConvert.SerializeObject(this);
    }

    public List<string> GetAllShopItemIDList(Predicate<ShopItemData> predicate = null)
    {
        List<string> result = new List<string>();
        for (int i = 0; i < ShopItemDataList.Count; i++)
        {
            if (predicate == null || predicate.Invoke(ShopItemDataList[i]) == true)
            {
                result.Add(ShopItemDataList[i].ItemId);
            }
        }
        return result;
    }

    public List<ShopItemData> GetAllShopItemList(Predicate<ShopItemData> predicate = null)
    {
        List<ShopItemData> result = new List<ShopItemData>();
        for (int i = 0; i < ShopItemDataList.Count; i++)
        {
            if (predicate == null || predicate.Invoke(ShopItemDataList[i]) == true)
            {
                result.Add(ShopItemDataList[i]);
            }
        }
        return result;
    }
}