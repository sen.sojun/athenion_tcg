﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalShopData : ShopData
{
    public NormalShopData(ShopData data) : base(data)
    {
    }

    public NormalShopData(ShopTypes shopType, string storeID) : base(shopType, storeID)
    {
    }
    public override void SetCustomData(string json)
    {
        if (string.IsNullOrEmpty(json))
            return;

        Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
        base.ReleaseDate = rawData.ContainsKey("ReleaseDate") ? (DateTime)rawData["ReleaseDate"] : DateTime.MinValue;
        base.EndDate = rawData.ContainsKey("EndDate") ? (DateTime)rawData["EndDate"] : DateTime.MaxValue;

    }
}
