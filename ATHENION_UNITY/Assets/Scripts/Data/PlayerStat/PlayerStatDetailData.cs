﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerStatDetailData
{
    #region Public Properties
    public List<string> PlayerStatList { get { return _playerStatList; } }
    public int Amount { get { return _amount; } }
    #endregion

    #region Private Properties
    private List<string> _playerStatList = new List<string>();
    private int _amount = 0;
    #endregion

    #region Contructors
    public PlayerStatDetailData(string statKeys, int amount)
    {
        _playerStatList = new List<string>(statKeys.Split(',').ToList());
        _amount = amount;
    }

    public PlayerStatDetailData(KeyValuePair<string, int> data)
    {
        _playerStatList = new List<string>(data.Key.Split(',').ToList());
        _amount = data.Value;
    }
    #endregion
}