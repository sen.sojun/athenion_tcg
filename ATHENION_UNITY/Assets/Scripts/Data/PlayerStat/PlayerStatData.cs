﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerStatKey
{
    #region Public Properties
    public string StatKey { get { return GetStatKey(); } }
    #endregion

    #region Private Properties
    private List<string> _statKeys = new List<string>();
    #endregion

    #region Contructor
    public PlayerStatKey()
    {
    }

    public PlayerStatKey(params string[] statKeys)
    {
        foreach(string key in statKeys)
        {
            _statKeys.Add(key.ToUpper());
        }
    }

    public PlayerStatKey(PlayerStatKey data)
    {
        _statKeys = new List<string>(data._statKeys);
    }
    #endregion

    #region Methods
    private string GetStatKey()
    {
        string result = string.Empty;
        foreach(string key in _statKeys)
        {
            if(key == string.Empty)
            {
                continue;
            }

            if(result != string.Empty)
            {
                result += "_";
            }

            result += key;
        }

        return result;
    }
    #endregion
}

public class PlayerStatData
{
    #region Public Properties
    public string Key { get { return _statKey.StatKey; } }
    public int Value { get; private set; }
    #endregion

    #region Private Properties
    private PlayerStatKey _statKey = new PlayerStatKey();
    #endregion

    #region Contructors
    public PlayerStatData(PlayerStatKey statKeys, int value = 1)
    {
        _statKey = new PlayerStatKey(statKeys);
        Value = value;
    }

    public PlayerStatData(PlayerStatData data)
    {
        _statKey = new PlayerStatKey(data._statKey);
        Value = data.Value;
    }
    #endregion

    #region Methods
    public override string ToString()
    {
        return string.Format("{0}:{1}", Key, Value);
    }

    public void SetValue(int newValue)
    {
        Value = newValue;
    }
    #endregion
}

public class PlayerStatDataList
{
    #region Private Properties
    private List<PlayerStatData> _dataList = new List<PlayerStatData>();
    #endregion

    #region Contructors
    public PlayerStatDataList()
    {
    }

    public PlayerStatDataList(PlayerStatDataList data)
    {
        _dataList = new List<PlayerStatData>(data._dataList);
    }
    #endregion

    #region Methods
    public bool IsDataEmpty()
    {
        if(_dataList != null && _dataList.Count > 0)
        {
            return false;
        }
        return true;
    }

    public void AddPlayerStatData(params PlayerStatData[] playerStatList)
    {
        List<PlayerStatData> result = new List<PlayerStatData>();
        for(int i = 0; i < playerStatList.Length; i++)
        {
            if(playerStatList[i] != null)
            {
                result.Add(playerStatList[i]);
            }
        }
        AddPlayerStatData(result);
    }

    public void AddPlayerStatData(List<PlayerStatData> playerStatList)
    {
        if(_dataList == null)
        {
            _dataList = new List<PlayerStatData>();
        }

        int foundIndex = -1;
        int newValue;
        foreach (PlayerStatData stat in playerStatList)
        {
            foundIndex = _dataList.FindIndex(x => x.Key == stat.Key);
            if (foundIndex > -1)
            {
                newValue = _dataList[foundIndex].Value + stat.Value;
                _dataList[foundIndex].SetValue(newValue);
            }
            else
            {
                _dataList.Add(stat);
            }
        }
    }

    public void AddPlayerStatData(PlayerStatDataList playerStatDataList)
    {
        if (playerStatDataList != null)
        {
            AddPlayerStatData(playerStatDataList._dataList);
        }
    }

    public Dictionary<string, int> ToDictionary()
    {
        Dictionary<string, int> result = new Dictionary<string, int>();
        foreach (PlayerStatData item in _dataList)
        {
            if (result.ContainsKey(item.Key))
            {
                result[item.Key] += item.Value;
            }
            else
            {
                result.Add(item.Key, item.Value);
            }
        }

        return result;
    }

    public override string ToString()
    {
        string result = "";

        if (_dataList != null && _dataList.Count > 0)
        {
            foreach (PlayerStatData stat in _dataList)
            {
                result += string.Format("{0},", stat.ToString());
            }
            result += "\n";
        }
        else
        {
            result = "[Empty]";
        }

        return result;
    }
    #endregion
}