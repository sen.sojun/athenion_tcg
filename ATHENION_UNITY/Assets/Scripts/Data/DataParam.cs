﻿using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataParam
{
    #region Enums
    public enum ParamType
    {
          String = 0
        , Integer
        , Float
        , Boolean
        , Effect
        , SlotList
        , PlayerIndexList
        , MinionList
        , Buff
        , VFX
        , CardDirection
    }
    #endregion

    #region Public Properties
    public ParamType Type { get; private set; }
    public string Key { get; private set; }
    public List<string> Parameters { get; private set; }
    #endregion

    #region Private Properties
    #endregion

    #region Constructors
    public DataParam()
    {
        Key = "";
        Parameters = new List<string>();
    }

    public DataParam(ParamType type, string id, List<string> parameters)
    {
        Type = type;
        Key = id;
        Parameters = new List<string>(parameters);
    }

    public DataParam(DataParam data)
    {
        Type = data.Type;
        Key = data.Key;
        Parameters = new List<string>(data.Parameters);
    }
    #endregion

    #region Methods
    public string ToText()
    {
        string result = "";

        result = Key;
        if (Parameters != null && Parameters.Count > 0)
        {
            result += "(";

            for (int index = 0; index < Parameters.Count; ++index)
            {
                if (index != 0)
                {
                    result += "," + Parameters[index];
                }
                else
                {
                    result += Parameters[index];
                }
            }

            result += ")";
        }

        return result;
    }

    public string GetDebugText()
    {
        string paramText = "";
        foreach (string param in Parameters)
        {
            if (paramText.Length > 0)
            {
                paramText = paramText + ", " + param;
            }
            else
            {
                paramText = param;
            }
        }

        string result = string.Format(
              "ID: {0}\nParams: {1}"
            , Key
            , paramText
        );

        return result;
    }

    public static bool TextToDataParam(string paramText, ParamType type, out DataParam param)
    {
        /*
         *      [Valid Case]
         *      Case 1 : key
         *      Case 2 : key(param1,param2,param3,...)
        */

        if (paramText != null && paramText.Length > 0)
        {
            StringBuilder text = new StringBuilder(paramText);
            string id = "";
            List<string> paramList = new List<string>();

            bool isFoundKey = false;
            
            int startIndex = 0;
            int blanketCount = 0;
            for (int index = 0; index < text.Length; ++index)
            {
                char c = paramText[index];

                if (!isFoundKey)
                {
                    if (c == '(')
                    {
                        id = text.ToString(startIndex, (index - startIndex));
                        startIndex = index + 1;

                        isFoundKey = true;
                    }
                }
                else
                {
                    if (c == '(')
                    {
                        blanketCount++;
                    }
                    else
                    {
                        if (blanketCount > 0)
                        {
                            if (c == ')')
                            {
                                blanketCount--;

                                if (blanketCount == 0)
                                {
                                    paramList.Add(text.ToString(startIndex, (index - startIndex) + 1));
                                    startIndex = index + 2;
                                    index++;
                                }
                            }
                        }
                        else
                        { 
                            if (c == ',')
                            {
                                paramList.Add(text.ToString(startIndex, (index - startIndex)));
                                startIndex = index + 1;
                            }
                        }
                    }
                }
            }

            if (startIndex < paramText.Length)
            {
                if (!isFoundKey)
                {
                    id = text.ToString(startIndex, paramText.Length);
                }
                else
                {
                    if (startIndex < text.Length - 1)
                    {
                        paramList.Add(text.ToString(startIndex, (paramText.Length - startIndex - 1)));
                    }
                }
            }

            /*
            string[] texts = paramText.Split(',', '(', ')');
            id = texts[0];
            for (int index = 1; index < texts.Length; ++index)
            {
                if (texts[index].Length > 0)
                {
                    paramList.Add(texts[index]);
                }
            }
            */

            param = new DataParam(type, id, paramList);

            // debug print
            //Debug.Log(param.GetDebugText());

            return true;
        }

        param = null;
        return false;
    }
    #endregion
}
