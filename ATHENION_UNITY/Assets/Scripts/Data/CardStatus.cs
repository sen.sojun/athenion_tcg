﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CardStatusType
{
    // Status
      Be_Taunt      = 1 // มิเนียนตัวนี้ ถูกดูดลูกศรไปจากความสามารถ Taunt ของตัวอื่น
    , Be_Fear       = 2 // มิเนียนตัวนี้ติดสถานะ กลัว
    , Be_Silence    = 3 // มิเนียนตัวนี้ติดสถานะ ใบ้
    , Be_Freeze     = 4 // มิเนียนตัวนี้ติดสถานะ แช่แข็ง
    , Be_Link       = 5 // มิเนียนตัวนี้เป็นสมาชิกของสายลิงค์
    , Be_Stealth    = 6 // มิเนียนตัวนี้มีสถานะ Stealth
    , Be_Aura       = 10 // มินเนี่ยนตัวนี้ได้รับผลจากความสามารถ Aura
    , Be_DarkMatter = 11 // มินเนี่ยนตัวนี้มี Dark Matter
    , Be_Invincible = 12 // มิเนี่ยนตัวนี้ไม่ได้รับความเสียหายใดๆ
    , Be_Untargetable = 13 // มิเนี่ยนจะไม่ตกเป็นเป้าหมายของความสามารถใดๆ
    , Be_Piercing = 14 // มิเนี่ยนมีความสามารถเจาะเกราะ (Piercing)
    , Be_EffectInvincible = 15 // มิเนี่ยนตัวนี้ไม่ได้รับความเสียหายจาก effect
    , Be_OverrideEffectDamage = 16 // มินเนี่ยนตัวนี้จะเปลี่ยนค่าความเสียหายจาก effect ที่ตัวเองโดน

    , TauntActive   = 7 // มิเนียนตัวนี้แสดงความสามารถ Taunt อยู่
    , AuraActive    = 8 // มิเนียนตัวนี้แสดงความสามารถ Aura อยู่
    , BerserkActive = 9 // มิเนียนตัวนี้แสดงความสามารถ Berserk อยู่
}

public class CardStatus
{
    #region Public Properties
    public Dictionary<CardStatusType, int> StatusDataList { get { return _statusDataList; } }
    #endregion

    #region Private Properties
    private Dictionary<CardStatusType, int> _statusDataList = new Dictionary<CardStatusType, int>();
    #endregion

    #region Constructors
    public CardStatus()
    {
        _statusDataList = new Dictionary<CardStatusType, int>();
    }

    public CardStatus(Dictionary<CardStatusType, int> data)
    {
        _statusDataList = new Dictionary<CardStatusType, int>(data);
    }

    public CardStatus(CardStatus cardStatus) : this(cardStatus.StatusDataList)
    {
    }

    public CardStatus(CardStatusType cardStatusType)
    {
        _statusDataList = new Dictionary<CardStatusType, int>();
        Add(cardStatusType);
    }
    #endregion

    #region Methods
    public bool IsCardStatus(CardStatusType cardStatusType)
    {
        if (_statusDataList != null && _statusDataList.ContainsKey(cardStatusType))
        {
            if (_statusDataList[cardStatusType] > 0)
            {
                return true;
            }
        }

        return false;
    }

    public void Add(CardStatusType cardStatusType)
    {
        if (_statusDataList == null)
        {
            _statusDataList = new Dictionary<CardStatusType, int>();
        }

        if (!_statusDataList.ContainsKey(cardStatusType))
        {
            _statusDataList.Add(cardStatusType, 0);
        }

        _statusDataList[cardStatusType] += 1;
    }

    public void Remove(CardStatusType cardStatusType)
    {
        if (_statusDataList != null && _statusDataList.ContainsKey(cardStatusType))
        {
            _statusDataList[cardStatusType] -= 1;

            if(_statusDataList[cardStatusType] <= 0)
            {
                _statusDataList.Remove(cardStatusType);
            }
        }
    }

    public void RemoveAll(CardStatusType cardStatusType)
    {
        if (_statusDataList != null && _statusDataList.ContainsKey(cardStatusType))
        {
            _statusDataList.Remove(cardStatusType);
        }
    }

    public override string ToString()
    {
        string text = "";
        foreach (KeyValuePair<CardStatusType, int> passiveType in _statusDataList)
        {
            if (text.Length > 0)
            {
                text += ", " + string.Format("{0}={1}", passiveType.Key.ToString(), passiveType.Value);
            }
            else
            {
                text += "{" + string.Format("{0}={1}", passiveType.Key.ToString(), passiveType.Value);
            }
        }

        if (text.Length <= 0)
        {
            text = "None";
        }
        else
        {
            text += "}";
        }

        return text;
    }
    #endregion

    #region Operators
    public static bool operator ==(CardStatus data1, CardStatus data2)
    {
        return (data1.Equals(data2));
    }

    public static bool operator !=(CardStatus data1, CardStatus data2)
    {
        return (!data1.Equals(data2));
    }

    public bool Equals(CardStatus data)
    {
        if (ReferenceEquals(null, data))
        {
            return false;
        }
        if (ReferenceEquals(this, data))
        {
            return true;
        }

        foreach (CardStatusType statusType in System.Enum.GetValues(typeof(CardStatusType)))
        {
            if(_statusDataList.ContainsKey(statusType) && data.StatusDataList.ContainsKey(statusType))
            {
                // both have
                if(_statusDataList[statusType] != data.StatusDataList[statusType])
                {
                    return false;
                }
            }
            else if(_statusDataList.ContainsKey(statusType) != data.StatusDataList.ContainsKey(statusType))
            {
                // NOT same
                return false;
            }
        }

        return true;
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj))
        {
            return false;
        }
        if (ReferenceEquals(this, obj))
        {
            return true;
        }

        return obj.GetType() == GetType() && Equals((CardStatus)obj);
    }

    public override int GetHashCode()
    {
        return (this._statusDataList.GetHashCode());
    }
    #endregion
}
