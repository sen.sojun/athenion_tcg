﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#region Global Enums
public enum MinionBuffTypes
{
    BuffMinionAP
    , BuffMinionAPCritical
    , BuffMinionHP
    , BuffMinionSP
    , BuffMinionAPHP
    , BuffMinionArmor
    , BuffMinionTaunted
    , BuffMinionStealth
    , BuffMinionFear
    , BuffMinionFreeze
    , BuffMinionSilence
    , BuffMinionDamageImmune
    , BuffMinionCanBeSelected
    , BuffMinionAllDir
}

public enum MinionBuffOrigins
{
    ADDBUFF
    , PASSIVEBUFF
    , TAUNTBUFF
    , AURABUFF
    , LINKBUFF
    , BERSERKBUFF
}
#endregion

#region MinionBuffData
public class MinionBuffData
{
    #region Public Properties
    public int UniqueID { get; private set; }
    public DataParam Param { get; protected set; }
    public Requester Requester { get; protected set; }

    public string Key { get { return _key; } }
    public int CounterAmount { get { return _counterAmount; } }
    public BuffCounterType CounterType { get { return _counterType; } }
    public MinionData Buffy { get; private set; }

    public MinionBuffTypes BuffType { get; private set; }
    public MinionBuffOrigins BuffOrigin { get; private set; }
    #endregion

    #region Private Properties
    private string _key = string.Empty;
    private int _counterAmount = -1;
    private BuffCounterType _counterType = BuffCounterType.None;

    private static int _uniqueIndex = 0;
    #endregion

    #region Constructor
    public MinionBuffData(int effectDataUniqueID, MinionBuffOrigins origin, BuffCounterType counterType, int counterAmount, DataParam param, Requester requester)
    {
        UniqueID = RequestUniqueID();
        Param = param;

        MinionBuffTypes type;
        if (GameHelper.StrToEnum(Param.Key, out type))
        {
            BuffType = type;
        }

        BuffOrigin = origin;
        Requester = requester;
        SetBuffDataProperties(string.Format("{0}_{1}", origin.ToString(), effectDataUniqueID), counterType, counterAmount);
    }

    public MinionBuffData(MinionBuffData data)
    {
        UniqueID = data.UniqueID;
        Param = data.Param;
        BuffType = data.BuffType;
        BuffOrigin = data.BuffOrigin;
        Requester = data.Requester;
        SetBuffDataProperties(data.Key, data.CounterType, data.CounterAmount);
    }

    ~MinionBuffData()
    {
        UnsubscribeAll();
    }
    #endregion

    #region Methods
    public void Subscribe(MinionData buffy)
    {
        //Debug.Log("SUB...");
        if (Buffy != null)
        {
            // if have previous owner, unsubscribe first.
            UnsubscribeAll();
        }

        Buffy = buffy;
        if (Buffy != null)
        {
            switch(CounterType)
            {
                case BuffCounterType.EndOfTurn:
                case BuffCounterType.EndOfMyTurn:
                    Buffy.SubscribeEvent(EventKey.EndPhase, Counting);
                    break;

                case BuffCounterType.BeginOfMyTurn:
                    Buffy.SubscribeEvent(EventKey.BeginPhase, Counting);
                    break;

                case BuffCounterType.Critical:
                case BuffCounterType.PostAttack:
                    Buffy.SubscribeEvent(EventKey.OnPostAttack, Counting);
                    break;

                case BuffCounterType.PostDefend:
                    Buffy.SubscribeEvent(EventKey.OnPostDefend, Counting);
                    break;

                case BuffCounterType.PostBattle:
                    Buffy.SubscribeEvent(EventKey.PostBattlePhase, Counting);
                    break;
            }
        }
    }

    public void UnsubscribeAll()
    {
        if (Buffy != null)
        {
            switch (CounterType)
            {
                case BuffCounterType.EndOfTurn:
                case BuffCounterType.EndOfMyTurn:
                    Buffy.UnsubscribeEvent(EventKey.EndPhase, Counting);
                    break;

                case BuffCounterType.BeginOfMyTurn:
                    Buffy.UnsubscribeEvent(EventKey.BeginPhase, Counting);
                    break;

                case BuffCounterType.Critical:
                case BuffCounterType.PostAttack:
                    Buffy.UnsubscribeEvent(EventKey.OnPostAttack, Counting);
                    break;

                case BuffCounterType.PostDefend:
                    Buffy.UnsubscribeEvent(EventKey.OnPostDefend, Counting);
                    break;

                case BuffCounterType.PostBattle:
                    Buffy.UnsubscribeEvent(EventKey.PostBattlePhase, Counting);
                    break;
            }
        }
    }

    protected void Counting(Requester triggerer)
    {
        switch (CounterType)
        {
            case BuffCounterType.EndOfMyTurn:
            case BuffCounterType.BeginOfMyTurn:
            {
                if (GameManager.Instance.CurrentPlayerIndex != Requester.PlayerIndex)
                {
                    return;
                }
            }
            break;
        }

        if (_counterAmount > 0)
        {
            _counterAmount--;
            Buffy.CountBuff(this, null);
            if (_counterAmount <= 0)
            {
                //Debug.Log("REMOVE BUFF BY COUNTING");
                RequestRemoveBuff();
            }
        }
    }

    protected bool IsPassiveBuff()
    {
        switch (BuffOrigin)
        {
            case MinionBuffOrigins.PASSIVEBUFF:
            case MinionBuffOrigins.TAUNTBUFF:
            case MinionBuffOrigins.AURABUFF:
            case MinionBuffOrigins.LINKBUFF:
            case MinionBuffOrigins.BERSERKBUFF:
            {
                return true;
            }
        }
        return false;
    }

    public static bool CreateMinionBuffData(int effectDataUniqueID, MinionBuffOrigins origin, BuffCounterType counterType, int counterAmount, DataParam param, Requester requester, out MinionBuffData data)
    {
        data = null;

        switch(param.Key)
        {
            case "BuffMinionHP": data = new BuffMinionHP(effectDataUniqueID, origin, counterType, counterAmount, param, requester); return true;
            case "BuffMinionAP": data = new BuffMinionAP(effectDataUniqueID, origin, counterType, counterAmount, param, requester); return true;
            case "BuffMinionAPCritical": data = new BuffMinionAPCritical(effectDataUniqueID, origin, counterType, counterAmount, param, requester); return true;
            case "BuffMinionSP": data = new BuffMinionSP(effectDataUniqueID, origin, counterType, counterAmount, param, requester); return true;
            case "BuffMinionAPHP": data = new BuffMinionAPHP(effectDataUniqueID, origin, counterType, counterAmount, param, requester); return true;
            case "BuffMinionArmor": data = new BuffMinionArmor(effectDataUniqueID, origin, counterType, counterAmount, param, requester); return true;
            case "BuffMinionFear": data = new BuffMinionFear(effectDataUniqueID, origin, counterType, counterAmount, param, requester); return true;
            case "BuffMinionFreeze": data = new BuffMinionFreeze(effectDataUniqueID, origin, counterType, counterAmount, param, requester); return true;
            case "BuffMinionTaunted": data = new BuffMinionTaunted(effectDataUniqueID, origin, counterType, counterAmount, param, requester); return true;
            case "BuffMinionStealth": data = new BuffMinionStealth(effectDataUniqueID, origin, counterType, counterAmount, param, requester); return true;
            case "BuffMinionSilence": data = new BuffMinionSilence(effectDataUniqueID, origin, counterType, counterAmount, param, requester); return true;
            case "BuffMinionInvincible": data = new BuffMinionInvincible(effectDataUniqueID, origin, counterType, counterAmount, param, requester); return true;
            case "BuffMinionUntargetable": data = new BuffMinionUntargetable(effectDataUniqueID, origin, counterType, counterAmount, param, requester); return true;
            case "BuffMinionEffectInvincible": data = new BuffMinionEffectInvincible(effectDataUniqueID, origin, counterType, counterAmount, param, requester); return true;
            case "BuffMinionPiercing": data = new BuffMinionPiercing(effectDataUniqueID, origin, counterType, counterAmount, param, requester); return true;
            case "BuffMinionOverrideEffectDamage": data = new BuffMinionOverrideEffectDamage(effectDataUniqueID, origin, counterType, counterAmount, param, requester); return true;
            case "BuffMinionReadyToAttack": data = new BuffMinionReadyToAttack(effectDataUniqueID, origin, counterType, counterAmount, param, requester); return true;
            case "BuffMinionDir": data = new BuffMinionDir(effectDataUniqueID, origin, counterType, counterAmount, param, requester); return true;
            case "BuffMinionEmpty": data = new BuffMinionEmpty(effectDataUniqueID, origin, counterType, counterAmount, param, requester); return true;
            case "-": return false;
            default: Debug.LogError("CreateMinionBuffData: Fail to parse buff. " + string.Format("{0}_{1}", origin, effectDataUniqueID)); return false;
        }
    }

    public void SetBuffDataProperties(string key, BuffCounterType counterType, int counterAmount)
    {
        _key = key;
        _counterType = counterType;
        _counterAmount = counterAmount;
    }

    private static int RequestUniqueID()
    {
        return _uniqueIndex++;
    }

    public static void ResetIndex()
    {
        _uniqueIndex = 0;
    }

    public virtual void ActiveBuff()
    {
    }

    public virtual bool TryUpdateBuff()
    {
        return true;
    }

    // Request Buff to remove buff
    private void RequestRemoveBuff()
    {
        Buffy.RemoveBuff(Key, null);
    }

    public virtual void RemoveBuff()
    {
        UnsubscribeAll();
    }
    #endregion
}
#endregion

#region BuffData Children

#region BuffMinionHP
public class BuffMinionHP : MinionBuffData
{
    private int _value;
    private List<CommandData> _commandList;

    public BuffMinionHP(int effectDataUniqueID, MinionBuffOrigins origin, BuffCounterType counterType, int counterAmount, DataParam param, Requester requester)
                        : base(effectDataUniqueID, origin, counterType, counterAmount, param, requester)
    {
    }

    public override void ActiveBuff()
    {
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.Integer, out p[0]);
        if (isSuccess)
        {
            isSuccess &= IntParser.ParamToInt(p[0], Requester, out _value);
            if (isSuccess)
            {
                List<int> targetList = new List<int>();
                targetList.Add(Buffy.UniqueID);
                _commandList = new List<CommandData>();

                if (IsPassiveBuff())
                {
                    GameManager.Instance.RequestBoostBuffPassiveMinionHP(Requester, targetList, _value);
                }
                else
                {
                    GameManager.Instance.RequestBoostMinionHP(Requester, targetList, _value);
                }
                return;
            }
        }

        Debug.LogError(Param.Key + " : Fail to parse param");
    }

    public override bool TryUpdateBuff()
    {
        int newValue;
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.Integer, out p[0]);
        if (isSuccess)
        {
            isSuccess &= IntParser.ParamToInt(p[0], Requester, out newValue);
            if (isSuccess)
            {
                if (newValue != _value)
                {
                    int oldValue = _value;
                    _value = newValue;

                    List<int> targetList = new List<int>();
                    targetList.Add(Buffy.UniqueID);

                    int deltaValue = _value - oldValue;

                    if(deltaValue != 0)
                    {
                        if (IsPassiveBuff())
                        {
                            GameManager.Instance.RequestBoostBuffPassiveMinionHP(Requester, targetList, deltaValue);
                        }
                        else
                        {
                            GameManager.Instance.RequestBoostMinionHP(Requester, targetList, deltaValue);
                        }
                        return true;
                    }
                }
                return false;
            }
        }

        Debug.LogError(Param.Key + " : Fail to parse param");
        return false;
    }

    public override void RemoveBuff()
    {
        base.RemoveBuff();
        List<int> targetList = new List<int>();
        targetList.Add(Buffy.UniqueID);

        if(Buffy.HPCurrent > 0)
        {
            if (IsPassiveBuff())
            {
                GameManager.Instance.RequestBoostBuffPassiveMinionHP(Requester, targetList, -_value);
            }
            else
            {
                GameManager.Instance.RequestBoostMinionHP(Requester, targetList, -_value);
            }
        }
    }
}
#endregion

#region BuffMinionAP
public class BuffMinionAP : MinionBuffData
{
    private int _value;
    private int _realValue;

    public BuffMinionAP(int effectDataUniqueID, MinionBuffOrigins origin, BuffCounterType counterType, int counterAmount, DataParam param, Requester requester)
                        : base(effectDataUniqueID, origin, counterType, counterAmount, param, requester)
    {
    }

    public override void ActiveBuff()
    {
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.Integer, out p[0]);
        if (isSuccess)
        {
            isSuccess &= IntParser.ParamToInt(p[0], Requester, out _value);
            if (isSuccess)
            {
                List<int> targetList = new List<int>();
                targetList.Add(Buffy.UniqueID);

                if (IsPassiveBuff())
                {
                    _realValue = _value;
                    GameManager.Instance.RequestBoostBuffPasiveMinionAP(Requester, targetList, _realValue);
                }
                else
                {
                    _realValue = Mathf.Max(_value, -Buffy.ATKCurrentBase);
                    GameManager.Instance.RequestBoostMinionAP(Requester, targetList, _realValue);
                }
                return;
            }
        }

        Debug.LogError(Param.Key + " : Fail to parse param");
    }

    public override bool TryUpdateBuff()
    {
        int newValue;
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.Integer, out p[0]);
        if (isSuccess)
        {
            isSuccess &= IntParser.ParamToInt(p[0], Requester, out newValue);
            if (isSuccess)
            {
                if (newValue != _value)
                {
                    int oldValue = _value;
                    _value = newValue;

                    List<int> targetList = new List<int>();
                    targetList.Add(Buffy.UniqueID);

                    int deltaValue = _value - oldValue;
                    if(deltaValue != 0)
                    {
                        if (IsPassiveBuff())
                        {
                            _realValue = _value;
                            GameManager.Instance.RequestBoostBuffPasiveMinionAP(Requester, targetList, deltaValue);
                        }
                        else
                        {
                            Debug.LogErrorFormat("Immposible UpdateBuff : {0}", this.BuffType.ToString());
                        }
                        return true;
                    }
                }
                return false;
            }
        }

        Debug.LogError(Param.Key + " : Fail to parse param");
        return false;
    }

    public override void RemoveBuff()
    {
        base.RemoveBuff();
        List<int> targetList = new List<int>();
        targetList.Add(Buffy.UniqueID);

        if (IsPassiveBuff())
        {
            GameManager.Instance.RequestBoostBuffPasiveMinionAP(Requester, targetList, -_realValue);
        }
        else
        {
            GameManager.Instance.RequestBoostMinionAP(Requester, targetList, -_realValue);
        }
    }
}
#endregion

#region BuffMinionAPCritical
public class BuffMinionAPCritical : MinionBuffData
{
    private int _value;
    private int _realValue;

    public BuffMinionAPCritical(int effectDataUniqueID, MinionBuffOrigins origin, BuffCounterType counterType, int counterAmount, DataParam param, Requester requester)
                        : base(effectDataUniqueID, origin, counterType, counterAmount, param, requester)
    {
    }

    public override void ActiveBuff()
    {
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.Integer, out p[0]);
        if (isSuccess)
        {
            isSuccess &= IntParser.ParamToInt(p[0], Requester, out _value);
            if (isSuccess)
            {
                List<int> targetList = new List<int>();
                targetList.Add(Buffy.UniqueID);

                if (IsPassiveBuff())
                {
                    _realValue = _value;
                    GameManager.Instance.RequestBoostBuffPasiveMinionAPCritical(Requester, targetList, _realValue);
                }
                else
                {
                    _realValue = Mathf.Max(_value, -Buffy.ATKCurrentBase);
                    GameManager.Instance.RequestBoostMinionAPCritical(Requester, targetList, _realValue);
                }
                return;
            }
        }

        Debug.LogError(Param.Key + " : Fail to parse param");
    }

    public override bool TryUpdateBuff()
    {
        int newValue;
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.Integer, out p[0]);
        if (isSuccess)
        {
            isSuccess &= IntParser.ParamToInt(p[0], Requester, out newValue);
            if (isSuccess)
            {
                if (newValue != _value)
                {
                    int oldValue = _value;
                    _value = newValue;

                    List<int> targetList = new List<int>();
                    targetList.Add(Buffy.UniqueID);

                    int deltaValue = _value - oldValue;
                    if (deltaValue != 0)
                    {
                        if (IsPassiveBuff())
                        {
                            _realValue = _value;
                            GameManager.Instance.RequestBoostBuffPasiveMinionAPCritical(Requester, targetList, deltaValue);
                        }
                        else
                        {
                            Debug.LogErrorFormat("Immposible UpdateBuff : {0}", this.BuffType.ToString());
                        }
                        return true;
                    }
                }
                return false;
            }
        }

        Debug.LogError(Param.Key + " : Fail to parse param");
        return false;
    }

    public override void RemoveBuff()
    {
        base.RemoveBuff();
        List<int> targetList = new List<int>();
        targetList.Add(Buffy.UniqueID);

        if (IsPassiveBuff())
        {
            GameManager.Instance.RequestBoostBuffPasiveMinionAPCritical(Requester, targetList, -_realValue);
        }
        else
        {
            GameManager.Instance.RequestBoostMinionAPCritical(Requester, targetList, -_realValue);
        }
    }
}
#endregion

#region BuffMinionSP
public class BuffMinionSP : MinionBuffData
{
    private int _value;
    private int _realValue;

    public BuffMinionSP(int effectDataUniqueID, MinionBuffOrigins origin, BuffCounterType counterType, int counterAmount, DataParam param, Requester requester)
                        : base(effectDataUniqueID, origin, counterType, counterAmount, param, requester)
    {
    }

    public override void ActiveBuff()
    {
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.Integer, out p[0]);
        if (isSuccess)
        {
            isSuccess &= IntParser.ParamToInt(p[0], Requester, out _value);
            if (isSuccess)
            {
                List<int> targetList = new List<int>();
                targetList.Add(Buffy.UniqueID);

                if (IsPassiveBuff())
                {
                    _realValue = _value;
                    GameManager.Instance.RequestBoostBuffPasiveMinionSP(Requester, targetList, _realValue);
                }
                else
                {
                    _realValue = Mathf.Max(_value, -Buffy.SpiritCurrentBase);
                    GameManager.Instance.RequestBoostMinionSP(Requester, targetList, _realValue);
                }
                return;
            }
        }

        Debug.LogError(Param.Key + " : Fail to parse param");
    }

    public override bool TryUpdateBuff()
    {
        int newValue;
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.Integer, out p[0]);
        if (isSuccess)
        {
            isSuccess &= IntParser.ParamToInt(p[0], Requester, out newValue);
            if (isSuccess)
            {
                if (newValue != _value)
                {
                    int oldValue = _value;
                    _value = newValue;

                    List<int> targetList = new List<int>();
                    targetList.Add(Buffy.UniqueID);

                    int deltaValue = _value - oldValue;
                    if (deltaValue != 0)
                    {
                        if (IsPassiveBuff())
                        {
                            _realValue = _value;
                            GameManager.Instance.RequestBoostBuffPasiveMinionSP(Requester, targetList, deltaValue);
                        }
                        else
                        {
                            Debug.LogErrorFormat("Immposible UpdateBuff : {0}", this.BuffType.ToString());
                        }
                        return true;
                    }
                }
                return false;
            }
        }

        Debug.LogError(Param.Key + " : Fail to parse param");
        return false;
    }

    public override void RemoveBuff()
    {
        base.RemoveBuff();
        List<int> targetList = new List<int>();
        targetList.Add(Buffy.UniqueID);

        if (IsPassiveBuff())
        {
            GameManager.Instance.RequestBoostBuffPasiveMinionSP(Requester, targetList, -_realValue);
        }
        else
        {
            GameManager.Instance.RequestBoostMinionSP(Requester, targetList, -_realValue);
        }
    }
}
#endregion

#region BuffMinionAPHP
public class BuffMinionAPHP : MinionBuffData
{
    private int _ap;
    private int _hp;
    private int _realAP;

    public BuffMinionAPHP(int effectDataUniqueID, MinionBuffOrigins origin, BuffCounterType counterType, int counterAmount, DataParam param, Requester requester)
                        : base(effectDataUniqueID, origin, counterType, counterAmount, param, requester)
    {
    }

    public override void ActiveBuff()
    {
        bool isSuccess = true;
        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.Integer, out p[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.Integer, out p[1]);
        if (isSuccess)
        {
            isSuccess &= IntParser.ParamToInt(p[0], Requester, out _ap);
            isSuccess &= IntParser.ParamToInt(p[1], Requester, out _hp);
            if (isSuccess)
            {
                List<int> targetList = new List<int>();
                targetList.Add(Buffy.UniqueID);

                if (IsPassiveBuff())
                {
                    _realAP = _ap;
                    GameManager.Instance.RequestBoostBuffPasiveMinionAPHP(Requester, targetList, _ap, _hp);
                }
                else
                {
                    _realAP = Mathf.Max(_ap, -Buffy.ATKCurrent);
                    GameManager.Instance.RequestBoostMinionAPHP(Requester, targetList, _ap, _hp);
                }
                return;
            }
        }

        Debug.LogError(Param.Key + " : Fail to parse param");
    }

    public override bool TryUpdateBuff()
    {
        int newAP;
        int newHP;
        bool isSuccess = true;
        DataParam[] p = new DataParam[2];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.Integer, out p[0]);
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[1], DataParam.ParamType.Integer, out p[1]);
        if (isSuccess)
        {
            isSuccess &= IntParser.ParamToInt(p[0], Requester, out newAP);
            isSuccess &= IntParser.ParamToInt(p[1], Requester, out newHP);
            if (isSuccess)
            {
                if (newAP != _ap || newHP != _hp)
                {
                    int oldAP = _ap;
                    int oldHP = _hp;
                    _ap = newAP;
                    _hp = newHP;

                    List<int> targetList = new List<int>();
                    targetList.Add(Buffy.UniqueID);

                    int deltaAP = _ap - oldAP;
                    int deltaHP = _hp - oldHP;
                    if (deltaAP != 0 || deltaHP != 0)
                    {
                        if (IsPassiveBuff())
                        {
                            _realAP = _ap;
                            GameManager.Instance.RequestBoostBuffPasiveMinionAPHP(Requester, targetList, deltaAP, deltaHP);
                        }
                        else
                        {
                            Debug.LogErrorFormat("Immposible UpdateBuff : {0}", this.BuffType.ToString());
                        }
                        return true;
                    }
                }
                return false;
            }
        }

        Debug.LogError(Param.Key + " : Fail to parse param");
        return false;
    }

    public override void RemoveBuff()
    {
        base.RemoveBuff();
        List<int> targetList = new List<int>();
        targetList.Add(Buffy.UniqueID);

        if(Buffy.HPCurrent > 0)
        {
            if (IsPassiveBuff())
            {
                GameManager.Instance.RequestBoostBuffPasiveMinionAPHP(Requester, targetList, -_realAP, -_hp);
            }
            else
            {
                GameManager.Instance.RequestBoostMinionAPHP(Requester, targetList, -_realAP, -_hp);
            }
        }
    }
}
#endregion

#region BuffMinionArmor
public class BuffMinionArmor : MinionBuffData
{
    private int _value;
    private int _realValue;

    public BuffMinionArmor(int effectDataUniqueID, MinionBuffOrigins origin, BuffCounterType counterType, int counterAmount, DataParam param, Requester requester)
                        : base(effectDataUniqueID, origin, counterType, counterAmount, param, requester)
    {
    }

    public override void ActiveBuff()
    {
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.Integer, out p[0]);
        if (isSuccess)
        {
            isSuccess &= IntParser.ParamToInt(p[0], Requester, out _value);
            if (isSuccess)
            {
                List<int> targetList = new List<int>();
                targetList.Add(Buffy.UniqueID);

                if (IsPassiveBuff())
                {
                    _realValue = _value;
                    GameManager.Instance.RequestBoostBuffPasiveMinionArmor(Requester, targetList, _realValue);
                }
                else
                {
                    _realValue = Mathf.Max(_value, -Buffy.ArmorCurrent);
                    GameManager.Instance.RequestBoostMinionArmor(Requester, targetList, _realValue);
                }
                return;
            }
        }

        Debug.LogError(Param.Key + " : Fail to parse param");
    }

    public override bool TryUpdateBuff()
    {
        int newValue;
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.Integer, out p[0]);
        if (isSuccess)
        {
            isSuccess &= IntParser.ParamToInt(p[0], Requester, out newValue);
            if (isSuccess)
            {
                if (newValue != _value)
                {
                    int oldValue = _value;
                    _value = newValue;

                    List<int> targetList = new List<int>();
                    targetList.Add(Buffy.UniqueID);

                    int deltaValue = _value - oldValue;
                    if (deltaValue != 0)
                    {
                        if (IsPassiveBuff())
                        {
                            _realValue = _value;
                            GameManager.Instance.RequestBoostBuffPasiveMinionArmor(Requester, targetList, deltaValue);
                        }
                        else
                        {
                            Debug.LogErrorFormat("Immposible UpdateBuff : {0}", this.BuffType.ToString());
                        }
                        return true;
                    }
                }
                return false;
            }
        }

        Debug.LogError(Param.Key + " : Fail to parse param");
        return false;
    }

    public override void RemoveBuff()
    {
        base.RemoveBuff();
        List<int> targetList = new List<int>();
        targetList.Add(Buffy.UniqueID);

        if (IsPassiveBuff())
        {
            GameManager.Instance.RequestBoostBuffPasiveMinionArmor(Requester, targetList, -_realValue);
        }
        else
        {
            GameManager.Instance.RequestBoostMinionArmor(Requester, targetList, -_realValue);
        }
    }
}
#endregion

#region BuffMinionTaunted
public class BuffMinionTaunted : MinionBuffData
{
    private CardDirection _cardDirection;

    public BuffMinionTaunted(int effectDataUniqueID, MinionBuffOrigins origin, BuffCounterType counterType, int counterAmount, DataParam param, Requester requester)
                        : base(effectDataUniqueID, origin, counterType, counterAmount, param, requester)
    {
    }

    public override void ActiveBuff()
    {
        MinionData target;
        MinionData provider = Requester.Minion as MinionData;
        _cardDirection = new CardDirection();
        foreach(CardDirectionType dir in System.Enum.GetValues(typeof(CardDirectionType)))
        {
            if(Buffy.GetAdjacentMinion(dir, out target))
            {
                if(target.UniqueID == provider.UniqueID)
                {
                    _cardDirection.Add(dir);
                    break;
                }
            }
        }

        GameManager.Instance.RequestAddMinionDirTaunted(Requester.Minion.UniqueID, Buffy.UniqueID, _cardDirection);

        // Trigger Analytic
        //{
        //    if (Requester.Minion != null)
        //    {
        //        GameManager.Instance.CardTaunt_Analytic(
        //              GameManager.Instance.Mode
        //            , provider.BaseID
        //            , provider.Owner
        //            , "TAUNT"
        //            , provider.SpiritCurrent
        //        );
        //    }
        //}
    }

    public override bool TryUpdateBuff()
    {
        MinionData target;
        MinionData provider = Requester.Minion as MinionData;
        CardDirection newCardDirection = new CardDirection();
        foreach (CardDirectionType dir in System.Enum.GetValues(typeof(CardDirectionType)))
        {
            if (Buffy.GetAdjacentMinion(dir, out target))
            {
                if (target.UniqueID == provider.UniqueID)
                {
                    newCardDirection.Add(dir);
                    break;
                }
            }
        }

        if (newCardDirection != _cardDirection)
        {
            CardDirection oldCardDirection = _cardDirection;
            _cardDirection = newCardDirection;

            GameManager.Instance.RequestRemoveMinionDirTaunted(Requester.Minion.UniqueID, Buffy.UniqueID, oldCardDirection);
            GameManager.Instance.RequestAddMinionDirTaunted(Requester.Minion.UniqueID, Buffy.UniqueID, _cardDirection);

            return true;
        }

        return false;
    }

    public override void RemoveBuff()
    {
        base.RemoveBuff();
        GameManager.Instance.RequestRemoveMinionDirTaunted(Requester.Minion.UniqueID, Buffy.UniqueID, _cardDirection);
    }
}
#endregion

#region BuffMinionStealth
public class BuffMinionStealth : MinionBuffData
{
    public BuffMinionStealth(int effectDataUniqueID, MinionBuffOrigins origin, BuffCounterType counterType, int counterAmount, DataParam param, Requester requester)
                        : base(effectDataUniqueID, origin, counterType, counterAmount, param, requester)
    {
    }

    public override void ActiveBuff()
    {
        GameManager.Instance.RequestBuffStealth(Requester, Buffy.UniqueID, true);
    }

    public override bool TryUpdateBuff()
    {
        if (!Buffy.IsBeStealth)
        {
            ActiveBuff();
            return true;
        }

        return false;
    }

    public override void RemoveBuff()
    {
        base.RemoveBuff();
        GameManager.Instance.RequestBuffStealth(Requester, Buffy.UniqueID, false);
    }
}
#endregion

#region BuffMinionFear
public class BuffMinionFear : MinionBuffData
{
    public BuffMinionFear(int effectDataUniqueID, MinionBuffOrigins origin, BuffCounterType counterType, int counterAmount, DataParam param, Requester requester)
                        : base(effectDataUniqueID, origin, counterType, counterAmount, param, requester)
    {
    }

    public override void ActiveBuff()
    {
        GameManager.Instance.RequestBuffFear(Requester, Buffy.UniqueID, true);
    }

    public override bool TryUpdateBuff()
    {
        if(!Buffy.IsBeFear)
        {
            ActiveBuff();
            return true;
        }

        return false;
    }

    public override void RemoveBuff()
    {
        base.RemoveBuff();
        GameManager.Instance.RequestBuffFear(Requester, Buffy.UniqueID, false);
    }
}
#endregion

#region BuffMinionFreeze
public class BuffMinionFreeze : MinionBuffData
{
    public BuffMinionFreeze(int effectDataUniqueID, MinionBuffOrigins origin, BuffCounterType counterType, int counterAmount, DataParam param, Requester requester)
                        : base(effectDataUniqueID, origin, counterType, counterAmount, param, requester)
    {
    }

    public override void ActiveBuff()
    {
        GameManager.Instance.RequestBuffFreeze(Requester, Buffy.UniqueID, true);
    }

    public override bool TryUpdateBuff()
    {
        if (!Buffy.IsBeFreeze)
        {
            ActiveBuff();
            return true;
        }

        return false;
    }

    public override void RemoveBuff()
    {
        base.RemoveBuff();
        GameManager.Instance.RequestBuffFreeze(Requester, Buffy.UniqueID, false);
    }
}
#endregion

#region BuffMinionSilence
public class BuffMinionSilence : MinionBuffData
{
    public BuffMinionSilence(int effectDataUniqueID, MinionBuffOrigins origin, BuffCounterType counterType, int counterAmount, DataParam param, Requester requester)
                        : base(effectDataUniqueID, origin, counterType, counterAmount, param, requester)
    {
    }

    public override void ActiveBuff()
    {
        GameManager.Instance.RequestBuffSilence(Requester, Buffy.UniqueID, true);
    }

    public override bool TryUpdateBuff()
    {
        if (!Buffy.IsBeSilence)
        {
            ActiveBuff();
            return true;
        }

        return false;
    }

    public override void RemoveBuff()
    {
        base.RemoveBuff();
        GameManager.Instance.RequestBuffSilence(Requester, Buffy.UniqueID, false);
    }
}
#endregion

#region BuffMinionInvincible
public class BuffMinionInvincible : MinionBuffData
{
    public BuffMinionInvincible(int effectDataUniqueID, MinionBuffOrigins origin, BuffCounterType counterType, int counterAmount, DataParam param, Requester requester)
                        : base(effectDataUniqueID, origin, counterType, counterAmount, param, requester)
    {
    }

    public override void ActiveBuff()
    {
        GameManager.Instance.RequestBuffInvincible(Requester, Buffy.UniqueID, true);
    }

    public override bool TryUpdateBuff()
    {
        if (!Buffy.IsBeInvincible)
        {
            ActiveBuff();
            return true;
        }

        return false;
    }

    public override void RemoveBuff()
    {
        base.RemoveBuff();
        GameManager.Instance.RequestBuffInvincible(Requester, Buffy.UniqueID, false);
    }
}
#endregion

#region BuffMinionUntargetable
public class BuffMinionUntargetable : MinionBuffData
{
    public BuffMinionUntargetable(int effectDataUniqueID, MinionBuffOrigins origin, BuffCounterType counterType, int counterAmount, DataParam param, Requester requester)
                        : base(effectDataUniqueID, origin, counterType, counterAmount, param, requester)
    {
    }

    public override void ActiveBuff()
    {
        GameManager.Instance.RequestBuffUntargetable(Requester, Buffy.UniqueID, true);
    }

    public override bool TryUpdateBuff()
    {
        if (!Buffy.IsBeUntargetable)
        {
            ActiveBuff();
            return true;
        }

        return false;
    }

    public override void RemoveBuff()
    {
        base.RemoveBuff();
        GameManager.Instance.RequestBuffUntargetable(Requester, Buffy.UniqueID, false);
    }
}
#endregion

#region BuffMinionEffectInvincible
public class BuffMinionEffectInvincible : MinionBuffData
{
    public BuffMinionEffectInvincible(int effectDataUniqueID, MinionBuffOrigins origin, BuffCounterType counterType, int counterAmount, DataParam param, Requester requester)
                        : base(effectDataUniqueID, origin, counterType, counterAmount, param, requester)
    {
    }

    public override void ActiveBuff()
    {
        GameManager.Instance.RequestBuffEffectInvincible(Requester, Buffy.UniqueID, true);
    }

    public override bool TryUpdateBuff()
    {
        if (!Buffy.IsBeEffectInvincible)
        {
            ActiveBuff();
            return true;
        }

        return false;
    }

    public override void RemoveBuff()
    {
        base.RemoveBuff();
        GameManager.Instance.RequestBuffEffectInvincible(Requester, Buffy.UniqueID, false);
    }
}
#endregion

#region BuffMinionPiercing
public class BuffMinionPiercing : MinionBuffData
{
    public int Range { get { return _range; } }
    private int _range = 0;

    public BuffMinionPiercing(int effectDataUniqueID, MinionBuffOrigins origin, BuffCounterType counterType, int counterAmount, DataParam param, Requester requester)
                        : base(effectDataUniqueID, origin, counterType, counterAmount, param, requester)
    {
    }

    public override void ActiveBuff()
    {
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.Integer, out p[0]);
        if (isSuccess)
        {
            isSuccess &= IntParser.ParamToInt(p[0], Requester, out _range);
            GameManager.Instance.RequestBuffPiercing(Requester, Buffy.UniqueID, UniqueID, true, _range);
        }
    }

    public override bool TryUpdateBuff()
    {
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.Integer, out p[0]);
        if (isSuccess)
        {
            int oldRange = _range;
            isSuccess &= IntParser.ParamToInt(p[0], Requester, out _range);
            if (oldRange != _range)
            {
                GameManager.Instance.RequestBuffPiercing(Requester, Buffy.UniqueID, UniqueID, true, _range);
                return true;
            }
        }
        return false;
    }

    public override void RemoveBuff()
    {
        base.RemoveBuff();
        GameManager.Instance.RequestBuffPiercing(Requester, Buffy.UniqueID, UniqueID, false, _range);
    }
}
#endregion

#region BuffMinionOverrideEffectDamage
public class BuffMinionOverrideEffectDamage : MinionBuffData
{
    public int Damage { get { return _damage; } }
    private int _damage = 0;

    public BuffMinionOverrideEffectDamage(int effectDataUniqueID, MinionBuffOrigins origin, BuffCounterType counterType, int counterAmount, DataParam param, Requester requester)
                        : base(effectDataUniqueID, origin, counterType, counterAmount, param, requester)
    {
    }

    public override void ActiveBuff()
    {
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.Integer, out p[0]);
        if (isSuccess)
        {
            isSuccess &= IntParser.ParamToInt(p[0], Requester, out _damage);
            GameManager.Instance.RequestBuffOverrideEffectDamage(Requester, Buffy.UniqueID, UniqueID, true, _damage);
        }
    }

    public override bool TryUpdateBuff()
    {
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.Integer, out p[0]);
        if (isSuccess)
        {
            int oldDamage = _damage;
            isSuccess &= IntParser.ParamToInt(p[0], Requester, out _damage);
            if (oldDamage != _damage)
            {
                GameManager.Instance.RequestBuffOverrideEffectDamage(Requester, Buffy.UniqueID, UniqueID, true, _damage);
                return true;
            }
        }
        return false;
    }

    public override void RemoveBuff()
    {
        base.RemoveBuff();
        GameManager.Instance.RequestBuffOverrideEffectDamage(Requester, Buffy.UniqueID, UniqueID, false, _damage);
    }
}
#endregion

#region BuffMinionReadyToAttack
public class BuffMinionReadyToAttack : MinionBuffData
{
    public BuffMinionReadyToAttack(int effectDataUniqueID, MinionBuffOrigins origin, BuffCounterType counterType, int counterAmount, DataParam param, Requester requester)
                        : base(effectDataUniqueID, origin, counterType, counterAmount, param, requester)
    {
    }

    public override void ActiveBuff()
    {
        GameManager.Instance.RequestAddMinionToAttackerList(Buffy);
        Buffy.SubscribeEvent(EventKey.PrePlayPhase, UpdateBuffEvent);
    }

    private void UpdateBuffEvent(Requester requester)
    {
        TryUpdateBuff();
    }

    public override bool TryUpdateBuff()
    {
        GameManager.Instance.RequestAddMinionToAttackerList(Buffy);
        return true;
    }

    public override void RemoveBuff()
    {
        base.RemoveBuff();
        Buffy.UnsubscribeEvent(EventKey.PrePlayPhase, UpdateBuffEvent);
    }
}
#endregion

#region BuffMinionDir
public class BuffMinionDir : MinionBuffData
{
    #region Private Properties
    private CardDirection _cardDir;
    private CardDirection _realCardDir;
    #endregion

    public BuffMinionDir(int effectDataUniqueID, MinionBuffOrigins origin, BuffCounterType counterType, int counterAmount, DataParam param, Requester requester)
                        : base(effectDataUniqueID, origin, counterType, counterAmount, param, requester)
    {
    }

    public override void ActiveBuff()
    {
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.CardDirection, out p[0]);
        if (isSuccess)
        {
            isSuccess &= CardDirectionParser.ParamToCardDirectionList(p[0], Requester, out _cardDir);
            if (isSuccess)
            {
                List<int> targetList = new List<int>();
                targetList.Add(Buffy.UniqueID);
                _realCardDir = new CardDirection(_cardDir);

                if (IsPassiveBuff())
                {
                    GameManager.Instance.RequestAddMinionDirBuffPassive(Requester.Minion.UniqueID, targetList, _realCardDir);
                }
                else
                {
                    GameManager.Instance.RequestAddMinionDir(targetList, _realCardDir);
                }
            }
        }
    }

    public override bool TryUpdateBuff()
    {
        CardDirection oldCardDir = new CardDirection(_cardDir);
        bool isSuccess = true;
        DataParam[] p = new DataParam[1];
        isSuccess &= DataParam.TextToDataParam(Param.Parameters[0], DataParam.ParamType.CardDirection, out p[0]);

        if (isSuccess)
        {
            isSuccess &= CardDirectionParser.ParamToCardDirectionList(p[0], Requester, out _cardDir);
            if (isSuccess)
            {
                if(oldCardDir != _cardDir)
                {
                    CardDirection deltaCardDir = _cardDir - oldCardDir;
                    List<int> targetList = new List<int>();
                    targetList.Add(Buffy.UniqueID);

                    if (IsPassiveBuff())
                    {
                        _realCardDir = new CardDirection(_cardDir);
                        GameManager.Instance.RequestAddMinionDirBuffPassive(Requester.Minion.UniqueID, targetList, deltaCardDir);
                    }
                    else
                    {
                        Debug.LogErrorFormat("Immposible UpdateBuff : {0}", this.BuffType.ToString());
                    }
                    return true;
                }
                return false;
            }
        }

        Debug.LogError(Param.Key + " : Fail to parse param");
        return false;
    }

    public override void RemoveBuff()
    {
        base.RemoveBuff();

        List<int> targetList = new List<int>();
        targetList.Add(Buffy.UniqueID);

        if (IsPassiveBuff())
        {
            GameManager.Instance.RequestAddMinionDirBuffPassive(Requester.Minion.UniqueID, targetList, _realCardDir.GetInvertCardDirection());
        }
        else
        {
            GameManager.Instance.RequestAddMinionDir(targetList, _realCardDir.GetInvertCardDirection());
        }
    }
}
#endregion

#region BuffMinionEmpty
public class BuffMinionEmpty : MinionBuffData
{
    public BuffMinionEmpty(int effectDataUniqueID, MinionBuffOrigins origin, BuffCounterType counterType, int counterAmount, DataParam param, Requester requester)
                        : base(effectDataUniqueID, origin, counterType, counterAmount, param, requester)
    {
    }

    public override void ActiveBuff()
    {
        GameManager.Instance.RequestBuffEmpty(Requester, Buffy.UniqueID);
    }
    
    public override void RemoveBuff()
    {
        base.RemoveBuff();
        GameManager.Instance.RequestBuffEmpty(Requester, Buffy.UniqueID);
    }
}
#endregion

#endregion