﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

#region Based Data
public abstract class PlayerQuestBaseData
{
    #region Enums
    protected enum QuestStatus
    {
        active
        , clear
    }
    #endregion

    #region Protected Properties
    protected const string _dataKey = "data";
    protected const string _propertiesKey = "properties";
    protected const string _questStatStampKey = "stat";
    protected const string _questStatusKey = "status";

    protected List<QuestData> _questDataList = new List<QuestData>();
    protected Dictionary<string, List<string>> _questIDDataList = new Dictionary<string, List<string>>();
    #endregion

    #region Contructors
    protected PlayerQuestBaseData()
    {
    }

    protected PlayerQuestBaseData(string jsonStrData)
    {
        SetData(jsonStrData);
    }
    #endregion

    #region Methods
    public abstract void SetData(string jsonStrData);

    protected void SortQuestDataList()
    {
        List<QuestData> result = new List<QuestData>();
        foreach (KeyValuePair<string, List<string>> item in _questIDDataList)
        {
            foreach (string id in item.Value)
            {
                int index = _questDataList.FindIndex(x => x.ID == id);
                if (index > -1)
                {
                    result.Add(_questDataList[index]);
                }
            }
        }

        _questDataList = new List<QuestData>(result);
    }

    protected void SetCompletedStatusQuest()
    {
        foreach (QuestData data in _questDataList)
        {
            if (data.IsCompleted)
            {
                data.SetAlreadyComplete();
            }
        }
    }

    protected bool IsQuestCleared(QuestStatus status)
    {
        return status == QuestStatus.clear;
    }

    public QuestData GetQuestData(string questID)
    {
        int index = _questDataList.FindIndex(x => x.ID == questID);
        if (index > -1)
        {
            return new QuestData(_questDataList[index]);
        }
        return null;
    }

    public List<QuestData> GetQuestDataList()
    {
        return new List<QuestData>(_questDataList);
    }

    public void SetQuestAlreadyCompleted(string questID)
    {
        int index = _questDataList.FindIndex(x => x.ID == questID);
        if (index > -1)
        {
            _questDataList[index].SetAlreadyComplete();
        }
    }

    public void SetQuestCleared(string questID)
    {
        int index = _questDataList.FindIndex(x => x.ID == questID);
        if (index > -1)
        {
            _questDataList[index].SetClear();
        }
    }
    #endregion
}
#endregion

#region Event Quest Data
public class PlayerEventQuestData : PlayerQuestBaseData
{
    #region Private Properties
    private string _eventKey = string.Empty;
    #endregion

    #region Contructors
    public PlayerEventQuestData()
    {
    }

    public PlayerEventQuestData(string jsonStrData) : base(jsonStrData) { }
    #endregion

    #region Methods
    public override void SetData(string jsonStrData)
    {
        _questDataList = new List<QuestData>();
        _questIDDataList = new Dictionary<string, List<string>>();

        // get raw data
        Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonStrData);

        // get properties
        Dictionary<string, object> properties = JsonConvert.DeserializeObject<Dictionary<string, object>>(rawData[_propertiesKey].ToString());
        _eventKey = properties["event_key"].ToString();
        _questIDDataList = JsonConvert.DeserializeObject<Dictionary<string, List<string>>>(properties["pool"].ToString());

        // get data
        Dictionary<string, object> questData = JsonConvert.DeserializeObject<Dictionary<string, object>>(rawData[_dataKey].ToString());
        Dictionary<string, int> questStatStamp = new Dictionary<string, int>();
        QuestStatus questStatus;
        foreach (KeyValuePair<string, object> quest in questData)
        {
            Dictionary<string, object> data = JsonConvert.DeserializeObject<Dictionary<string, object>>(quest.Value.ToString());
            questStatStamp = JsonConvert.DeserializeObject<Dictionary<string, int>>(data["stat"].ToString());
            GameHelper.StrToEnum(data["status"].ToString(), out questStatus);
            _questDataList.Add(new QuestData(quest.Key, QuestTypes.Event, questStatStamp, IsQuestCleared(questStatus)));
        }

        SortQuestDataList();
        SetCompletedStatusQuest();
    }
    #endregion
}
#endregion

#region Season Pass Quest Data
public class PlayerSeasonPassQuestData : PlayerQuestBaseData
{
    #region Private Properties
    private const string _questMainDailyKey = "main_daily";
    private const string _questMainWeeklyKey = "main_weekly";
    private const string _questMainWeekKey = "main_week";
    private const string _questChallengeKey = "challenge";

    private Dictionary<string, DateTime> _timestampPoolDataList = new Dictionary<string, DateTime>();
    #endregion

    #region Contructors
    public PlayerSeasonPassQuestData()
    {
    }

    public PlayerSeasonPassQuestData(string jsonStrData) : base(jsonStrData) { }
    #endregion

    #region Methods
    public override void SetData(string jsonStrData)
    {
        _questDataList = new List<QuestData>();
        _questIDDataList = new Dictionary<string, List<string>>();
        _timestampPoolDataList = new Dictionary<string, DateTime>();

        // get raw data
        Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonStrData);

        // get properties
        Dictionary<string, Dictionary<string, object>> properties = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, object>>>(rawData[_propertiesKey].ToString());
        foreach(KeyValuePair<string, Dictionary<string, object>> item in properties)
        {
            _questIDDataList.Add(item.Key, JsonConvert.DeserializeObject<List<string>>(item.Value["list"].ToString()));
            _timestampPoolDataList.Add(item.Key, DateTime.Parse(item.Value["update_timestamp"].ToString()));
        }

        // get data
        Dictionary<string, object> questData = JsonConvert.DeserializeObject<Dictionary<string, object>>(rawData[_dataKey].ToString());
        Dictionary<string, int> questStatStamp = new Dictionary<string, int>();
        QuestStatus questStatus;
        foreach (KeyValuePair<string, object> quest in questData)
        {
            Dictionary<string, object> data = JsonConvert.DeserializeObject<Dictionary<string, object>>(quest.Value.ToString());
            questStatStamp = JsonConvert.DeserializeObject<Dictionary<string, int>>(data["stat"].ToString());
            GameHelper.StrToEnum(data["status"].ToString(), out questStatus);
            _questDataList.Add(new QuestData(quest.Key, GetQuestType(quest.Key), questStatStamp, IsQuestCleared(questStatus)));
        }

        SortQuestDataList();
        SetCompletedStatusQuest();
    }

    private QuestTypes GetQuestType(string questID)
    {
        string poolKey = string.Empty;
        foreach(KeyValuePair<string, List<string>> item in _questIDDataList)
        {
            if(item.Value.Contains(questID))
            {
                poolKey = item.Key;
                break;
            }
        }

        if (poolKey.Contains(_questMainDailyKey))
        {
            return QuestTypes.SeasonPassMainDaily;
        }

        if (poolKey.Contains(_questMainWeeklyKey))
        {
            return QuestTypes.SeasonPassMainWeekly;
        }

        if (poolKey.Contains(_questMainWeekKey))
        {
            return QuestTypes.SeasonPassMainWeek;
        }

        if (poolKey.Contains(_questChallengeKey))
        {
            return QuestTypes.SeasonPassChallenge;
        }

        return QuestTypes.None;
    }

    public bool IsTimestampNewDay()
    {
        foreach(KeyValuePair<string, DateTime> item in _timestampPoolDataList)
        {
            if(GameHelper.IsNewDayUTC(item.Value))
            {
                return true;
            }
        }

        return false;
    }

    public List<QuestData> GetMainDailyQuestDataList()
    {
        List<QuestData> result = new List<QuestData>();
        if(_questIDDataList.ContainsKey(_questMainDailyKey))
        {
            foreach(string id in _questIDDataList[_questMainDailyKey])
            {
                int index = _questDataList.FindIndex(x => x.ID == id);
                if(index > -1)
                {
                    result.Add(_questDataList[index]);
                }
            }
        }

        return result;
    }

    public List<QuestData> GetMainWeeklyQuestDataList()
    {
        List<QuestData> result = new List<QuestData>();
        if (_questIDDataList.ContainsKey(_questMainDailyKey))
        {
            foreach (string id in _questIDDataList[_questMainWeeklyKey])
            {
                int index = _questDataList.FindIndex(x => x.ID == id);
                if (index > -1)
                {
                    result.Add(_questDataList[index]);
                }
            }
        }

        return result;
    }

    /// <param name="weekIndex">start from 1</param>
    /// <returns></returns>
    public List<QuestData> GetMainWeekQuestDataList(int weekIndex)
    {
        string poolKey = _questMainWeekKey + "_" + weekIndex;
        List<QuestData> result = new List<QuestData>();
        if (_questIDDataList.ContainsKey(poolKey))
        {
            foreach (string id in _questIDDataList[poolKey])
            {
                int index = _questDataList.FindIndex(x => x.ID == id);
                if (index > -1)
                {
                    result.Add(_questDataList[index]);
                }
            }
        }

        return result;
    }

    public List<QuestData> GetMainAllWeekQuestDataList()
    {
        List<QuestData> result = new List<QuestData>();
        int weekIndex = 1;
        while (true)
        {
            string poolKey = _questMainWeekKey + "_" + weekIndex;
            List<QuestData> data = new List<QuestData>();
            if (_questIDDataList.ContainsKey(poolKey))
            {
                foreach (string id in _questIDDataList[poolKey])
                {
                    int index = _questDataList.FindIndex(x => x.ID == id);
                    if (index > -1)
                    {
                        data.Add(_questDataList[index]);
                    }
                }
                result.AddRange(data);
                weekIndex++;
            }
            else
            {
                break;
            }
        }

        return result;
    }

    public int GetWeekIndexOfMainWeekQuest(string questID)
    {
        List<List<QuestData>> questList = GetMainWeekQuestDataList();
        int index = 0;
        foreach(List<QuestData> list in questList)
        {
            index++;
            if(list.Find(x => x.ID == questID) != null)
            {
                return index;
            }
        }

        return -1;
    }

    public int GetWeekCount()
    {
        List<List<QuestData>> questList = GetMainWeekQuestDataList();
        return questList.Count;
    }

    public List<List<QuestData>> GetMainWeekQuestDataList()
    {
        List<List<QuestData>> result = new List<List<QuestData>>();
        int weekIndex = 1;
        while(true)
        {
            string poolKey = _questMainWeekKey + "_" + weekIndex;
            List<QuestData> data = new List<QuestData>();
            if (_questIDDataList.ContainsKey(poolKey))
            {
                foreach (string id in _questIDDataList[poolKey])
                {
                    int index = _questDataList.FindIndex(x => x.ID == id);
                    if (index > -1)
                    {
                        data.Add(_questDataList[index]);
                    }
                }
                result.Add(data);
                weekIndex++;
            }
            else
            {
                break;
            }
        }      

        return result;
    }

    public List<QuestData> GetChallengeQuestDataList()
    {
        List<QuestData> result = new List<QuestData>();
        if (_questIDDataList.ContainsKey(_questChallengeKey))
        {
            foreach (string id in _questIDDataList[_questChallengeKey])
            {
                int index = _questDataList.FindIndex(x => x.ID == id);
                if (index > -1)
                {
                    result.Add(_questDataList[index]);
                }
            }
        }

        return result;
    }
    #endregion
}
#endregion

#region Sub Event Quest Data
public class PlayerSubEventQuestData : PlayerQuestBaseData
{
    #region Private Properties
    private string _eventKey = string.Empty;
    #endregion

    #region Contructors
    public PlayerSubEventQuestData()
    {
    }

    public PlayerSubEventQuestData(string jsonStrData) : base(jsonStrData) { }
    #endregion

    #region Methods
    public override void SetData(string jsonStrData)
    {
        _questDataList = new List<QuestData>();
        _questIDDataList = new Dictionary<string, List<string>>();

        // get raw data
        Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonStrData);

        // get properties
        Dictionary<string, object> properties = JsonConvert.DeserializeObject<Dictionary<string, object>>(rawData[_propertiesKey].ToString());
        _eventKey = properties["event_key"].ToString();
        _questIDDataList = JsonConvert.DeserializeObject<Dictionary<string, List<string>>>(properties["pool"].ToString());

        // get data
        Dictionary<string, object> questData = JsonConvert.DeserializeObject<Dictionary<string, object>>(rawData[_dataKey].ToString());
        Dictionary<string, int> questStatStamp = new Dictionary<string, int>();
        QuestStatus questStatus;
        foreach (KeyValuePair<string, object> quest in questData)
        {
            Dictionary<string, object> data = JsonConvert.DeserializeObject<Dictionary<string, object>>(quest.Value.ToString());
            questStatStamp = JsonConvert.DeserializeObject<Dictionary<string, int>>(data["stat"].ToString());
            GameHelper.StrToEnum(data["status"].ToString(), out questStatus);
            _questDataList.Add(new QuestData(quest.Key, QuestTypes.Event, questStatStamp, IsQuestCleared(questStatus)));
        }

        SortQuestDataList();
        SetCompletedStatusQuest();
    }
    #endregion
}
#endregion