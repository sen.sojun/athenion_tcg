﻿using System.Collections.Generic;

#region Global Enums
public enum SeenItemTypes
{
    Card
    , Avatar
    , Title
    , Hero
    , CardBack
    , Dock
    , SeasonPassQuest
    , Promotion
    , Event
}
#endregion

public class PlayerSeenListData
{
    #region Private Properties
    private Dictionary<SeenItemTypes, List<string>> _seenItemListData = new Dictionary<SeenItemTypes, List<string>>();
    #endregion

    #region Contructors
    public PlayerSeenListData()
    {
    }

    public PlayerSeenListData(PlayerSeenListData data)
    {
        _seenItemListData = new Dictionary<SeenItemTypes, List<string>>(data._seenItemListData);
    }
    #endregion

    #region Methods
    public void SetSeenItemList(SeenItemTypes type, List<string> itemList)
    {
        if(_seenItemListData.ContainsKey(type) == false)
        {
            _seenItemListData.Add(type, null);
        }

        _seenItemListData[type] = new List<string>(itemList);
    }

    public void AddSeenItemList(SeenItemTypes type, params string[] items)
    {
        if(_seenItemListData.ContainsKey(type) == false)
        {
            _seenItemListData.Add(type, new List<string>());
        }

        foreach(string item in items)
        {
            if (_seenItemListData[type].Contains(item))
            {
                continue;
            }

            _seenItemListData[type].Add(item);
        }
    }

    public List<string> GetSeenItemList(SeenItemTypes type)
    {
        if(_seenItemListData.ContainsKey(type))
        {
            return new List<string>(_seenItemListData[type]);
        }

        return new List<string>();
    }
    #endregion
}