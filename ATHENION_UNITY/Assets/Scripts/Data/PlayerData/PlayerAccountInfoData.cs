﻿using System.Collections;
using System.Collections.Generic;
using PlayFab.ClientModels;
using UnityEngine;

public class PlayerAccountInfoData
{
    #region Public Properties
    public bool IsLinkedEmail { get { return !string.IsNullOrEmpty(_emailAddress); } }
    public bool IsLinkedGoogle { get { return _googleInfo != null; } }
    public bool IsLinkedFacebook { get { return _facebookInfo != null; } }
    #endregion

    #region Private Properties
    private string _emailAddress;
    private UserGoogleInfo _googleInfo;
    private UserFacebookInfo _facebookInfo;
    #endregion

    #region Contructors
    public PlayerAccountInfoData()
    {
    }

    public PlayerAccountInfoData(UserAccountInfo userAccountInfo)
    {
        _emailAddress = userAccountInfo.PrivateInfo.Email;
        _googleInfo = userAccountInfo.GoogleInfo;
        _facebookInfo = userAccountInfo.FacebookInfo;
    }
    #endregion

    #region Get Methods
    public string GetLinkedPlayFabEmail()
    {
        return _emailAddress;
    }

    public string GetLinkedGoogleEmail()
    {
        if(_googleInfo != null)
        {
            return _googleInfo.GoogleEmail;
        }

        return "";
    }

    public string GetLinkedFacebookFullName()
    {
        if(_facebookInfo != null)
        {
            return _facebookInfo.FullName;
        }

        return "";
    }
    #endregion
}