﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab.ClientModels;
using Newtonsoft.Json;

#region Global Structs
public class PlayerVIPData
{
    public DateTime ExpiredDateTime { get; private set; }

    public PlayerVIPData(ItemInstance vipItem)
    {
        if (vipItem != null)
        {
            ExpiredDateTime = vipItem.Expiration.GetValueOrDefault();
        }
        else
        {
            ExpiredDateTime = DateTime.MinValue;
        }

        DataManager.Instance.OnVIPStatusUpdate();
    }

    public bool IsVIP()
    {
        TimeSpan value = ExpiredDateTime.Subtract(DateTimeData.GetDateTimeUTC());

        return (value.Ticks > 0);
    }
}

public class CardPackData
{
    public string PackID { get; private set; }
    public int Amount { get; set; }

    public CardPackData(string id, int amount)
    {
        PackID = id;
        Amount = amount;
    }
}
#endregion

public class PlayerInventoryData
{
    #region Event
    public delegate void EventCurrencyHandler();
    public static event EventCurrencyHandler OnUpdateEventCurrency;

    public delegate void CardInventoryHandler();
    public static event CardInventoryHandler OnUpdateCardInventory;
    #endregion

    #region Public Properties
    public PlayerVIPData VIPData
    {
        get
        {
            if (_vipData == null)
                _vipData = new PlayerVIPData(null);
            return _vipData;
        }
        private set => _vipData = value;
    }
    private PlayerVIPData _vipData;

    public CardListData CardInventoryList { get { return _cardListInventory; } }
    public List<CardPackData> CardPackDataList { get { return _cardPackDataList; } }
    public Dictionary<string, int> BundleIDList { get { return _bundleIDList; } }

    public List<string> AvatarIDList { get { return _avatarIDList; } }
    public List<string> TitleIDList { get { return _titleIDList; } }
    public List<string> HeroIDList { get { return _heroIDList; } }
    public List<string> CardBackIDList { get { return _cardBackIDList; } }
    public List<string> DockIDList { get { return _dockIDList; } }
    #endregion

    #region Private Properties
    private Dictionary<string, int> _itemList = new Dictionary<string, int>();
    private Dictionary<string, int> _virtualCurrency = new Dictionary<string, int>();
    private Dictionary<string, int> _eventCurrency = new Dictionary<string, int>();

    private CardListData _cardListInventory = new CardListData();
    private List<CardPackData> _cardPackDataList = new List<CardPackData>();
    private Dictionary<string, int> _bundleIDList = new Dictionary<string, int>();
    private List<string> _avatarIDList = new List<string>();
    private List<string> _titleIDList = new List<string>();
    private List<string> _heroIDList = new List<string>();
    private List<string> _cardBackIDList = new List<string>();
    private List<string> _dockIDList = new List<string>();
    #endregion

    #region Contructors
    public PlayerInventoryData()
    {
    }

    public PlayerInventoryData(PlayerInventoryData data)
    {
        VIPData = data.VIPData;

        _cardListInventory = new CardListData(data._cardListInventory);
        _cardPackDataList = new List<CardPackData>(data._cardPackDataList);
        _virtualCurrency = new Dictionary<string, int>(data._virtualCurrency);
        _eventCurrency = new Dictionary<string, int>(data._eventCurrency);

        _bundleIDList = new Dictionary<string, int>(data._bundleIDList);
        _avatarIDList = new List<string>(data._avatarIDList);
        _titleIDList = new List<string>(data._titleIDList);
        _heroIDList = new List<string>(data._heroIDList);
        _cardBackIDList = new List<string>(data._cardBackIDList);
        _dockIDList = new List<string>(data._dockIDList);
        _itemList = new Dictionary<string, int>();
    }
    #endregion

    #region Set Methods
    public void SetAllInventoryData(GetUserInventoryResult loadedData)
    {
        ClearAllInventory();
        foreach (ItemInstance item in loadedData.Inventory)
        {
            GrantItemByItemInstance(item);
        }
        SetVirtualCurrencyData(loadedData.VirtualCurrency);

        OnUpdateCardInventory?.Invoke();
    }

    private void GrantItemByItemInstance(ItemInstance item)
    {
        List<string> itemClass = GetItemClassByItemID(item.ItemId);
        if (itemClass == null)
            return;

        if (itemClass[0] == "Subscription")
        {
            VIPData = new PlayerVIPData(item);
        }
        else
        {
            int amount = item.RemainingUses == null ? 0 : (int)item.RemainingUses;
            GrantItemByItemID(item.ItemId, amount);
        }
    }

    public void GrantItemByItemList(List<ItemData> itemList)
    {
        foreach (ItemData data in itemList)
        {
            GrantItemByItemID(data.ItemID, data.Amount);
        }

        OnUpdateCardInventory?.Invoke();
    }

    public void GrantItemByItemID(string itemID, int amount)
    {
        if (amount == 0)
        {
            Debug.LogWarning(itemID + ".amount =  " + amount);
            return;
        }

        if (GameHelper.IsVirtualCurrency(itemID))
            AddOrSubstractVirtualCurrency(itemID, amount);

        if (GameHelper.IsEventCurrency(itemID))
            AddOrSubstractEventCurrency(itemID, amount);

        List<string> itemClass = GetItemClassByItemID(itemID);
        if (itemClass == null)
            return;

        AddItemList(itemID, amount);
        switch (itemClass[0]) //Categorize item class/type
        {
            case "Card":
                {
                    if (!GetItemTagByItemID(itemID).Contains("basic")) // ignore basic card
                    {
                        _cardListInventory.AddCard(itemID, amount);
                    }
                }
                break;

            case "Container":
                {
                    if (itemClass.Contains("Card"))
                    {
                        CardPackData pack = _cardPackDataList.Find(obj => obj.PackID == itemID);
                        if (pack == null)
                        {
                            _cardPackDataList.Add(new CardPackData(itemID, amount));
                        }
                        else
                        {
                            pack.Amount += amount;
                        }
                    }
                }
                break;

            case "Bundle":
                {
                    if (_bundleIDList.ContainsKey(itemID))
                    {
                        _bundleIDList[itemID]++;
                    }
                    else
                    {
                        _bundleIDList.Add(itemID, 1);
                    }
                }
                break;

            case "Avatar":
                {
                    _avatarIDList.Add(itemID);
                }
                break;

            case "Hero":
                {
                    _heroIDList.Add(itemID);
                }
                break;

            case "CardBack":
                {
                    _cardBackIDList.Add(itemID);
                }
                break;

            case "Dock":
                {
                    _dockIDList.Add(itemID);
                }
                break;

            case "Title":
                {
                    _titleIDList.Add(itemID);
                }
                break;

            case "EventCurrency":
                {
                    AddEventCurrency(itemID, amount);
                }
                break;

            default: break;
        }
    }

    private void AddItemList(string itemID, int amount)
    {
        if (!_itemList.ContainsKey(itemID))
        {
            _itemList.Add(itemID, 0);
        }
        _itemList[itemID] += amount;
    }

    public int GetAmountByItemID(string itemID)
    {
        if (!_itemList.ContainsKey(itemID))
            return 0;
        return _itemList[itemID];
    }

    public int GetAmountByItemKey(string itemKey)
    {
        foreach (KeyValuePair<string, int> data in _itemList)
        {
            string itemID = data.Key;
            string _itemKey = GameHelper.ConvertItemIDToItemKey(data.Key);
            if (_itemKey == itemKey)
            {
                return data.Value;
            }
        }
        return 0;
    }

    private List<string> GetItemClassByItemID(string itemID)
    {
        CatalogItemData catalogItemData = null;
        if (DataManager.Instance.GetCatalogItem(itemID, out catalogItemData))
        {
            return catalogItemData.ItemClass.Split(':').ToList();
        }
        return null;
    }

    private List<string> GetItemTagByItemID(string itemID)
    {
        CatalogItemData catalogItemData = null;
        if (DataManager.Instance.GetCatalogItem(itemID, out catalogItemData))
        {
            return catalogItemData.ItemTags;
        }
        return null;
    }

    private void ClearAllInventory()
    {
        VIPData = new PlayerVIPData(null);

        _itemList = new Dictionary<string, int>();

        _cardListInventory = new CardListData();

        // Add default basic card
        {
            List<CardDBData> cardList = null;
            if (CardDB.Instance.GetAllData(out cardList))
            {
                foreach (CardDBData data in cardList)
                {
                    if (data.SeriesKey == "Basic")
                    {
                        if (!data.ID.ToLower().Contains("old"))
                        {
                            CardType cardType;
                            if (GameHelper.StrToEnum<CardType>(data.Type, out cardType))
                            {
                                if (cardType != CardType.Minion_NotInDeck)
                                {
                                    _cardListInventory.AddCard(data.ID, 3); // add 3 cards
                                }
                            }
                        }
                    }
                }
            }
        }

        _cardPackDataList = new List<CardPackData>();
        _virtualCurrency = new Dictionary<string, int>();

        _bundleIDList = new Dictionary<string, int>();

        _avatarIDList = new List<string>(DataManager.Instance.DefaultAvatarIDList); // Add default.
        AddItemsToItemList(_avatarIDList);

        _titleIDList = new List<string>(DataManager.Instance.DefaultTitleIDList); // Add default. 
        AddItemsToItemList(_titleIDList);

        _heroIDList = new List<string>(DataManager.Instance.DefaultHeroIDList); // Add default. 
        AddItemsToItemList(_heroIDList);

        _cardBackIDList = new List<string>(DataManager.Instance.DefaultCardBackIDList); // Add default.
        AddItemsToItemList(_cardBackIDList);

        _dockIDList = new List<string>(DataManager.Instance.DefaultDockIDList); // Add default.
        AddItemsToItemList(_dockIDList);
    }

    private void AddItemsToItemList(List<string> data)
    {
        for (int i = 0; i < data.Count; i++)
        {
            AddItemList(data[i], 1);
        }
    }

    #region VirtualCurrency
    private void SetVirtualCurrencyData(Dictionary<string, int> virtualCurrency)
    {
        _virtualCurrency = new Dictionary<string, int>(virtualCurrency);
    }

    public void AddOrSubstractVirtualCurrency(string itemID, int amount)
    {
        itemID = itemID.Replace("VC_", "");

        if (_virtualCurrency.ContainsKey(itemID) == false)
        {
            _virtualCurrency.Add(itemID, 0);
        }
        _virtualCurrency[itemID] += amount;
    }
    #endregion

    #region EventCurrency
    public int GetEventCurrency(string key)
    {
        if (_eventCurrency.ContainsKey(key) == false)
        {
            _eventCurrency.Add(key, 0);
        }
        return _eventCurrency[key];
    }

    public void SetEventCurrency(string key, int data)
    {
        if (_eventCurrency.ContainsKey(key) == false)
        {
            _eventCurrency.Add(key, data);
        }
        else
        {
            _eventCurrency[key] = data;
        }
        OnUpdateEventCurrency?.Invoke();
    }

    public void AddOrSubstractEventCurrency(string key, int amount)
    {
        if (amount > 0)
        {
            AddEventCurrency(key, amount);
        }
        else if (amount < 0)
        {
            SubEventCurrency(key, -amount);
        }
    }

    public void AddEventCurrency(string key, int amount)
    {
        if (_eventCurrency.ContainsKey(key) == false)
            _eventCurrency.Add(key, 0);
        _eventCurrency[key] += amount;
        OnUpdateEventCurrency?.Invoke();
    }

    public void SubEventCurrency(string key, int amount)
    {
        if (_eventCurrency.ContainsKey(key) == false)
            _eventCurrency.Add(key, 0);
        _eventCurrency[key] -= amount;
        OnUpdateEventCurrency?.Invoke();
    }
    #endregion

    #endregion

    #region Get Methods
    public int GetVirtualCurrency(VirtualCurrency virtualCurrency)
    {
        int result = 0;
        string key = virtualCurrency.ToString();
        if (_virtualCurrency.ContainsKey(key))
        {
            result = _virtualCurrency[key];
        }
        return result;
    }

    public Dictionary<VirtualCurrency, int> GetAllPlayerVirtualCurrency()
    {
        Dictionary<VirtualCurrency, int> result = new Dictionary<VirtualCurrency, int>();
        VirtualCurrency currency;
        foreach (KeyValuePair<string, int> item in _virtualCurrency)
        {
            if (GameHelper.StrToEnum(item.Key, out currency))
            {
                result.Add(currency, item.Value);
            }
        }
        return result;
    }

    public bool IsContainItem(string itemID)
    {
        return GetAmountByItemID(itemID) > 0;
    }

    public int GetDockAmount()
    {
        return DockIDList.Count;
    }

    public int LegendPackAmount()
    {
        int amount = 0;
        for (int i = 0; i < _cardPackDataList.Count; i++)
        {
            if (GameHelper.IsLegendPack(_cardPackDataList[i].PackID))
            {
                amount += _cardPackDataList[i].Amount;
            }
        }
        return amount;
    }

    public bool IsHasLegendPack()
    {
        for (int i = 0; i < _cardPackDataList.Count; i++)
        {
            if (GameHelper.IsLegendPack(_cardPackDataList[i].PackID))
            {
                return true;
            }
        }
        return false;
    }

    public bool IsHasSkinHero()
    {
        return _heroIDList.Find((id) => id.Contains("_")) != null;
    }
    #endregion
}