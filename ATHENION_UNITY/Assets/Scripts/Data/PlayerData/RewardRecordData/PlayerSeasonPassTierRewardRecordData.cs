﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSeasonPassTierRewardRecordData
{
    #region Private Properties
    private Dictionary<string, Dictionary<string, DateTime>> _rewardRecordData = new Dictionary<string, Dictionary<string, DateTime>>();
    #endregion

    #region Contructors
    public PlayerSeasonPassTierRewardRecordData()
    {
    }

    public PlayerSeasonPassTierRewardRecordData(string jsonStrData)
    {
        SetData(jsonStrData);
    }
    #endregion

    #region Methods
    public void SetData(string jsonStrData)
    {
        _rewardRecordData = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, DateTime>>>(jsonStrData);
    }

    public bool IsRewardClaimed(int seasonPassTier)
    {
        string seasonPassIndexStr = DataManager.Instance.GetCurrentSeasonPassIndex().ToString();
        if (_rewardRecordData.ContainsKey(seasonPassIndexStr))
        {
            if (_rewardRecordData[seasonPassIndexStr].ContainsKey(seasonPassTier.ToString()))
            {
                return true;
            }
        }

        return false;
    }

    public bool IsRewardClaimed(int seasonPassIndex, int seasonPassTier)
    {
        string seasonPassIndexStr = seasonPassIndex.ToString();
        if (_rewardRecordData.ContainsKey(seasonPassIndexStr))
        {
            if(_rewardRecordData[seasonPassIndexStr].ContainsKey(seasonPassTier.ToString()))
            {
                return true;
            }
        }

        return false;
    }
    #endregion
}