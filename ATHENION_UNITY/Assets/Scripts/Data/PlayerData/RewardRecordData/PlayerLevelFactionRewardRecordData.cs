﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

public class PlayerLevelFactionRewardRecordData
{
    #region Private Properties
    private Dictionary<string, Dictionary<string, DateTime>> _levelFactionRewardRecord = new Dictionary<string, Dictionary<string, DateTime>>();
    #endregion

    #region Contructors
    public PlayerLevelFactionRewardRecordData()
    {
    }

    public PlayerLevelFactionRewardRecordData(PlayerLevelFactionRewardRecordData data)
    {
        _levelFactionRewardRecord = new Dictionary<string, Dictionary<string, DateTime>>(data._levelFactionRewardRecord);
    }
    #endregion

    #region Methods
    public void SetData(string jsonStrData)
    {
        _levelFactionRewardRecord = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, DateTime>>>(jsonStrData);
    }

    public bool IsRewardClaimed(CardElementType faction, int level)
    {
        string factionStr = faction.ToString();
        string levelStr = level.ToString();
        if (_levelFactionRewardRecord.ContainsKey(factionStr))
        {
            if (_levelFactionRewardRecord[factionStr].ContainsKey(levelStr))
            {
                return true;
            }
        }

        return false;
    }
    #endregion
}