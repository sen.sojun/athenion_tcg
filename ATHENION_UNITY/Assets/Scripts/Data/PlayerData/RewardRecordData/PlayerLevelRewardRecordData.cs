﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

public class PlayerLevelRewardRecordData
{
    #region Private Properties
    private Dictionary<string, DateTime> _levelRewardRecord = new Dictionary<string, DateTime>();
    #endregion

    #region Contructors
    public PlayerLevelRewardRecordData()
    {
    }

    public PlayerLevelRewardRecordData(string jsonStrData)
    {
        SetData(jsonStrData);
    }
    #endregion

    #region Methods
    public void SetData(string jsonStrData)
    {
        _levelRewardRecord = JsonConvert.DeserializeObject<Dictionary<string, DateTime>>(jsonStrData);
    }

    public void AddRewardData(int level)
    {
        string levelStr = level.ToString();
        if (!_levelRewardRecord.ContainsKey(levelStr))
        {
            _levelRewardRecord.Add(levelStr, DateTimeData.GetDateTimeUTC());
        }
    }

    public bool IsRewardClaimed(int level)
    {
        string levelStr = level.ToString();
        if (_levelRewardRecord.ContainsKey(levelStr))
        {
            return true;
        }

        return false;
    }
    #endregion
}