﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReferreeInfoData : PlayerInfoData
{
    public bool ClaimedRewardLevel7 { get; private set; }

    public ReferreeInfoData(Dictionary<string, object> rawData) : base(rawData)
    {
        ClaimedRewardLevel7 = (bool)rawData["claimedRewardLevel7"];
    }
}
