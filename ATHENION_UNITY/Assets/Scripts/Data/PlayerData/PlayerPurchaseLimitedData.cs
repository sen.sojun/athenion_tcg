﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPurchaseLimitedData
{
    public List<ItemPurchaseLogData> PurchaseLogs { get; private set; }
    public int cacheSeason { get; private set; }

    public PlayerPurchaseLimitedData(int season)
    {
        cacheSeason = season;
        PurchaseLogs = new List<ItemPurchaseLogData>();
    }

    public PlayerPurchaseLimitedData(string json)
    {
        PurchaseLogs = new List<ItemPurchaseLogData>();
        Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);

        cacheSeason = int.Parse(rawData["cache_season"].ToString());
        List<object> logsData = JsonConvert.DeserializeObject<List<object>>(rawData["purchase_logs"].ToString());
        foreach (object log in logsData)
        {
            PurchaseLogs.Add(new ItemPurchaseLogData(log.ToString()));
        }
    }

    public List<ItemPurchaseLogData> GetPurchaseLogsInSeason(int currentSeason)
    {
        SeasonInfo currentSeasonInfo = DataManager.Instance.GetSeasonInfoData(currentSeason);
        SeasonInfo nextSeasonInfo = DataManager.Instance.GetSeasonInfoData(currentSeason + 1);
        //SeasonInfo seasonInfo = DataManager.Instance.GetSeasonInfoData(currentSeason);
        List<ItemPurchaseLogData> result = new List<ItemPurchaseLogData>();
        foreach (ItemPurchaseLogData log in PurchaseLogs)
        {
            if (log.TimeStamp > currentSeasonInfo.StartDateTimeUTC && log.TimeStamp < nextSeasonInfo.StartDateTimeUTC)
            {
                result.Add(log);
            }
        }
        return result;
    }

    public void AddPurchaseLog(ItemPurchaseLogData data)
    {
        int currentSeason = DataManager.Instance.GetCurrentSeasonIndex();
        if (currentSeason != cacheSeason)
        {
            cacheSeason = currentSeason;
            PurchaseLogs = new List<ItemPurchaseLogData>();
        }
        PurchaseLogs.Add(data);
    }
}

public class ItemPurchaseLogData
{
    public string ItemID { get; private set; }
    public DateTime TimeStamp { get; private set; }

    /*
      "item_id": "com.zerobit.athenion.diamond_250",
      "timestamp": "2019-11-05T06:30:41.357Z"
     */

    public ItemPurchaseLogData(string json)
    {
        Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
        ItemID = rawData["item_id"].ToString();
        TimeStamp = rawData.ContainsKey("timestamp") ? DateTime.Parse(rawData["timestamp"].ToString()) : DateTime.MinValue;
    }

    public ItemPurchaseLogData(string itemID, DateTime timeStamp)
    {
        ItemID = itemID;
        TimeStamp = timeStamp;
    }
}
