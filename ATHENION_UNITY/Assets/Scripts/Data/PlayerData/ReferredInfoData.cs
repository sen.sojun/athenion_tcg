﻿public class ReferredInfoData
{
    public bool RegisteredReferralCode { get; private set; }
    public string ReferrerPlayerID { get; private set; }
    public bool ClaimedRewardLevel3 { get; private set; }

    public ReferredInfoData()
    {
        RegisteredReferralCode = false;
        ReferrerPlayerID = "";
        ClaimedRewardLevel3 = false;
    }

    public ReferredInfoData(bool registeredReferralCode, string referrerPlayerID, bool claimedRewardLevel3)
    {
        RegisteredReferralCode = registeredReferralCode;
        ReferrerPlayerID = referrerPlayerID;
        ClaimedRewardLevel3 = claimedRewardLevel3;
    }

    public void SetHasRegisteredReferralCode(bool value, string referrerPlayerID)
    {
        RegisteredReferralCode = value;
        ReferrerPlayerID = referrerPlayerID;
    }

    public void SetHasClaimedLevel3Reward(bool value)
    {
        ClaimedRewardLevel3 = value;
    }
}
