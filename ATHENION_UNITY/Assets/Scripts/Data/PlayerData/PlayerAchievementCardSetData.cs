﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class PlayerAchievementCardSet
{
    #region Public Properties
    public List<AchievementCardSetData> AchievementList { get { return _achievementList; } }
    #endregion

    #region Private Properties
    private List<AchievementCardSetData> _achievementList = new List<AchievementCardSetData>();
    private List<string> _completedAchievementIDList = new List<string>();
    #endregion

    #region Contructors
    public PlayerAchievementCardSet()
    {
    }

    public PlayerAchievementCardSet(string completedAchievementJSONDataList)
    {
        if (completedAchievementJSONDataList != string.Empty)
        {
            Dictionary<string, string> completedAchievementDataList = JsonConvert.DeserializeObject<Dictionary<string, string>>(completedAchievementJSONDataList);
            foreach (KeyValuePair<string, string> item in completedAchievementDataList)
            {
                AddCompletedAchievement(item.Key);
            }
        }

        List<AchievementCardSetDBData> allAchievementData;
        AchievementCardSetDB.Instance.GetAllData(out allAchievementData);
        foreach (AchievementCardSetDBData data in allAchievementData)
        {
            AchievementCardSetData achievement = new AchievementCardSetData(data, IsAchievementCompleted(data.ID));
            _achievementList.Add(achievement);
            DoCompleteAchievement(achievement.ID);
        }
    }
    #endregion

    #region Methods
    private void AddCompletedAchievement(string achievementID)
    {
        if (!IsAchievementCompleted(achievementID))
        {
            _completedAchievementIDList.Add(achievementID);
        }
    }

    public void DoCompleteAchievement(string achievementID)
    {
        int index = _achievementList.FindIndex(x => x.ID == achievementID);
        if (index > -1)
        {
            DoCompleteAchievement(_achievementList[index]);
        }
    }

    public void DoCompleteAchievement(AchievementCardSetData data)
    {
        data.SetAlreadyComplete();
    }

    public void DoClearAchievement(string achievementID)
    {
        int index = _achievementList.FindIndex(x => x.ID == achievementID);
        if (index > -1)
        {
            DoClearAchievement(_achievementList[index]);
        }
    }

    public void DoClearAchievement(AchievementCardSetData data)
    {
        data.SetClear();
    }

    private bool IsAchievementCompleted(string achievementID)
    {
        return _completedAchievementIDList.Contains(achievementID);
    }
    #endregion
}