﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerMissionData
{
    #region Public Properties
    public List<MissionData> MissionList { get { return _missionList; } }

    public static event UnityAction<string, UnityAction> OnSeenMission;
    #endregion

    #region Private Properties
    private List<MissionData> _missionList = new List<MissionData>();
    #endregion

    #region Contructors
    public PlayerMissionData()
    {
    }

    public PlayerMissionData(string playerMissionDataJSONStr)
    {
        SetData(playerMissionDataJSONStr);
    }
    #endregion

    #region Methods
    public void SetData(string playerMissionDataJSONStr)
    {
        Dictionary<string, Dictionary<string, object>> rawData = new Dictionary<string, Dictionary<string, object>>();
        if(playerMissionDataJSONStr != string.Empty)
        {
            rawData = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, object>>>(playerMissionDataJSONStr);
        }
        _missionList = new List<MissionData>();
        List<MissionDBData> missionDBDataList;
        MissionDB.Instance.GetAllData(out missionDBDataList);
        foreach (MissionDBData data in missionDBDataList)
        {
            if(rawData.ContainsKey(data.ID))
            {
                _missionList.Add(new MissionData(data, rawData[data.ID]));
            }
            else
            {
                _missionList.Add(new MissionData(data));
            }

            DoCompleteMission(data.ID);
        }

        List<string> completedMissionIDList = new List<string>();
        foreach(MissionData data in _missionList)
        {
            if(data.IsCleared)
            {
                completedMissionIDList.Add(data.ID);
            }
        }
        foreach (MissionData data in _missionList)
        {
            data.SetMissionEnabled(completedMissionIDList);
        }
    }

    public void DoCompleteMission(string missionID)
    {
        int index = _missionList.FindIndex(x => x.ID == missionID);
        if (index > -1)
        {
            _missionList[index].SetAlreadyComplete();
        }
    }

    public void DoClearMission(string missionID)
    {
        int index = _missionList.FindIndex(x => x.ID == missionID);
        if (index > -1)
        {
            _missionList[index].SetClear();
        }
    }

    public bool IsMissionCompleted(string missionID)
    {
        MissionData mission = _missionList.Find(x => x.ID == missionID);
        if(mission != null)
        {
            return mission.IsCompleted;
        }

        return false;
    }

    public bool IsMissionSeen(string missionID)
    {
        MissionData mission = _missionList.Find(x => x.ID == missionID);
        if (mission != null)
        {
            return mission.IsMissionSeen;
        }

        return false;
    }

    public bool IsMissionHasProgress(string missionID)
    {
        MissionData mission = _missionList.Find(x => x.ID == missionID);
        if (mission != null)
        {
            return mission.IsMissionHasProgress;
        }

        return false;
    }

    public bool IsMissionCanPlay(string missionID)
    {
        MissionData mission = _missionList.Find(x => x.ID == missionID);
        if (mission != null)
        {
            if(!mission.IsMissionSeen 
                && !mission.IsMissionHasProgress 
                && mission.IsEnabled
                )
            {
                return true;
            }
        }

        return false;
    }

    public void SeenMission(string missionID, UnityAction onComplete)
    {
        OnSeenMission?.Invoke(missionID, onComplete);
    }
    #endregion
}