﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCardPriceOverrideData
{
    #region Public Properties
    public string CurrentOverrideKey { get { return _currentOverrideKey; } }
    #endregion

    #region Private Properties
    private string _currentOverrideKey = string.Empty;
    private Dictionary<string, int> _currentCardQuotaData = new Dictionary<string, int>();
    #endregion

    #region Contructors
    public PlayerCardPriceOverrideData()
    {
    }

    public PlayerCardPriceOverrideData(string jsonStrData)
    {
        Dictionary<string, object> data = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonStrData);
        if(data.ContainsKey("key"))
        {
            _currentOverrideKey = data["key"] as string;
        }
        if(data.ContainsKey("list"))
        {
            _currentCardQuotaData = JsonConvert.DeserializeObject<Dictionary<string, int>>(data["list"].ToString());
        }
    }
    #endregion

    #region Methods
    public bool IsCardPriceOverrideQuotaValid(string itemID)
    {
        return GetCardPriceOverrideQuoata(itemID) > -1;
    }

    /// <summary>
    /// Return current quota amount for card price override. Return '-1' if item not found.
    /// </summary>
    /// <param name="itemID"></param>
    /// <returns></returns>
    public int GetCardPriceOverrideQuoata(string itemID)
    {
        if (_currentCardQuotaData.ContainsKey(itemID))
        {
            return _currentCardQuotaData[itemID];
        }

        return -1;
    }
    #endregion
}