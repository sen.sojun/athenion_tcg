﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class PlayerAchievementData
{
    #region Public Properties
    public List<AchievementData> AchievementList { get { return _achievementList; } }
    #endregion

    #region Private Properties
    private List<AchievementData> _achievementList = new List<AchievementData>();
    private List<string> _completedAchievementIDList = new List<string>();
    #endregion

    #region Contructors
    public PlayerAchievementData()
    {
    }

    public PlayerAchievementData(string completedAchievementJSONDataList)
    {
        if (completedAchievementJSONDataList != string.Empty)
        {
            Dictionary<string, string> completedAchievementDataList = JsonConvert.DeserializeObject<Dictionary<string, string>>(completedAchievementJSONDataList);
            foreach (KeyValuePair<string, string> item in completedAchievementDataList)
            {
                AddCompletedAchievement(item.Key);
            }
        }

        List<AchievementDBData> allAchievementData;
        AchievementDB.Instance.GetAllData(out allAchievementData);
        foreach (AchievementDBData data in allAchievementData)
        {
            AchievementData achievement = new AchievementData(data, IsAchievementCompleted(data.ID));
            _achievementList.Add(achievement);
            DoCompleteAchievement(achievement.ID);
        }
    }
    #endregion

    #region Methods
    private void AddCompletedAchievement(string achievementID)
    {
        if (!IsAchievementCompleted(achievementID))
        {
            _completedAchievementIDList.Add(achievementID);
        }
    }

    public void DoCompleteAchievement(string achievementID)
    {
        int index = _achievementList.FindIndex(x => x.ID == achievementID);
        if (index > -1)
        {
            DoCompleteAchievement(_achievementList[index]);
        }
    }

    public void DoCompleteAchievement(AchievementData data)
    {
        data.SetAlreadyComplete();
    }

    public void DoClearAchievement(string achievementID)
    {
        int index = _achievementList.FindIndex(x => x.ID == achievementID);
        if (index > -1)
        {
            DoClearAchievement(_achievementList[index]);
        }
    }

    public void DoClearAchievement(AchievementData data)
    {
        data.SetClear();
    }

    private bool IsAchievementCompleted(string achievementID)
    {
        return _completedAchievementIDList.Contains(achievementID);
    }
    #endregion
}