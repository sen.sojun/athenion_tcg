﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSeasonPassData
{
    #region Public Properties
    public int SeasonPassIndex { get { return _seasonPassIndex; } }
    public int SeasonPassScore { get { return _seasonPassScore; } }
    #endregion

    #region Private Properties
    private const string _seasonPassIndexKey = "season_index";
    private const string _seasonPassScoreKey = "score";
    private const string _seasonPassPremiumTransactionKey = "premium_trans";

    private int _seasonPassIndex = 0;
    private int _seasonPassScore = 0;
    private Dictionary<string, DateTime> _premiumTransactionData = new Dictionary<string, DateTime>();
    #endregion

    #region Contructors
    public PlayerSeasonPassData()
    {
    }

    public PlayerSeasonPassData(string jsonStrData)
    {
        SetData(jsonStrData);
    }

    public PlayerSeasonPassData(PlayerSeasonPassData data)
    {
        _seasonPassIndex = data._seasonPassIndex;
        _seasonPassScore = data._seasonPassScore;
        _premiumTransactionData = new Dictionary<string, DateTime>(data._premiumTransactionData);
    }
    #endregion

    #region Methods
    public void SetData(string jsonStrData)
    {
        Dictionary<string, object> data = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonStrData);
        if(data.ContainsKey(_seasonPassIndexKey))
        {
            _seasonPassIndex = int.Parse(data[_seasonPassIndexKey].ToString());
        }

        if(data.ContainsKey(_seasonPassScoreKey))
        {
            _seasonPassScore = int.Parse(data[_seasonPassScoreKey].ToString());
        }

        if(data.ContainsKey(_seasonPassPremiumTransactionKey))
        {
            _premiumTransactionData = JsonConvert.DeserializeObject<Dictionary<string, DateTime>>(data[_seasonPassPremiumTransactionKey].ToString());
        }
    }

    public int GetCurrentSeasonPassScore()
    {
        SeasonPassDBData currentSeasonPassData = SeasonPassDB.Instance.GetCurrentSeasonPassDBData();
        if(currentSeasonPassData != null)
        {
            if(currentSeasonPassData.SeasonPassIndex == _seasonPassIndex)
            {
                return _seasonPassScore;
            }
        }

        return 0;
    }

    public int GetCurrentSeasonPassTier()
    {
        int score = GetCurrentSeasonPassScore();
        int tierLevel = 0;
        SeasonPassTierDB.Instance.GetAllData(out List<SeasonPassTierDBData> dataList);
        foreach (SeasonPassTierDBData item in dataList)
        {
            if (score >= item.TierAccScore)
            {
                tierLevel++;
            }
            else
            {
                break;
            }
        }

        return tierLevel;
    }

    public int GetCurrentTierExcessiveScore()
    {
        int score = GetCurrentSeasonPassScore();
        int lastScore = 0;

        SeasonPassTierDB.Instance.GetAllData(out List<SeasonPassTierDBData> dataList);
        foreach (SeasonPassTierDBData item in dataList)
        {
            if (score >= item.TierAccScore)
            {
                lastScore = item.TierAccScore;
            }
            else
            {
                break;
            }
        }

        return score - lastScore;
    }

    public bool IsBePremium()
    {
        SeasonPassDBData currentSeasonPassData = SeasonPassDB.Instance.GetCurrentSeasonPassDBData();
        if (currentSeasonPassData != null)
        {
            string currentSeasonPassIndexStr = currentSeasonPassData.SeasonPassIndex.ToString();
            if (_premiumTransactionData.ContainsKey(currentSeasonPassIndexStr))
            {
                if(_premiumTransactionData[currentSeasonPassIndexStr] >= currentSeasonPassData.StartDateTime
                   && _premiumTransactionData[currentSeasonPassIndexStr] < currentSeasonPassData.EndDateTime)
                {
                    return true;
                }
            }
        }

        return false;
    }
    #endregion
}