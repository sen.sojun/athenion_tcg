﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#region Progress Data

public class PlayerLevelData
{
    #region Public Properties
    public int Level { get; private set; }
    public int NextLevel { get; private set; }
    public int AccExp { get; private set; }
    public int CurrentLevelAccExp { get; private set; }
    public int NextLevelAccExp { get; private set; }
    #endregion

    #region Private Properties
    #endregion

    #region Contructors
    public PlayerLevelData()
    {
    }

    public PlayerLevelData(int playerExp)
    {
        Level = 0;
        NextLevel = 0;
        AccExp = 0;
        CurrentLevelAccExp = 0;
        NextLevelAccExp = 0;

        List<PlayerExpDBData> expDataList = new List<PlayerExpDBData>();
        List<PlayerExpDBData> data;
        if (PlayerExpDB.Instance.GetAllData(out data))
        {
            expDataList = new List<PlayerExpDBData>(data);
        }
        SetupData(playerExp, expDataList);
    }

    public PlayerLevelData(PlayerLevelData data)
    {
        Level = data.Level;
        NextLevel = data.NextLevel;
        AccExp = data.AccExp;
        CurrentLevelAccExp = data.CurrentLevelAccExp;
        NextLevelAccExp = data.NextLevelAccExp;
    }
    #endregion

    #region Methods
    private void SetupData(int playerExp, List<PlayerExpDBData> expDataList)
    {
        Level = 0;
        NextLevel = 0;
        AccExp = playerExp;
        bool isInRange = false;
        foreach (PlayerExpDBData item in expDataList)
        {
            if (playerExp >= item.AccumulativeExp)
            {
                Level++;
                NextLevel++;
                CurrentLevelAccExp = item.AccumulativeExp;
                NextLevelAccExp = CurrentLevelAccExp;
            }
            else
            {
                NextLevel++;
                NextLevelAccExp = item.AccumulativeExp;
                isInRange = true;
                break;
            }
        }

        if (!isInRange)
        {
            AccExp = Mathf.Min(AccExp, CurrentLevelAccExp);
        }
    }

    public void ForceLevelUp(int levelProgress = 1)
    {
        int playerExp = 0;
        List<PlayerExpDBData> expDataList = new List<PlayerExpDBData>();
        List<PlayerExpDBData> data;
        if (PlayerExpDB.Instance.GetAllData(out data))
        {
            expDataList = new List<PlayerExpDBData>(data);
        }

        for (int i = 0; i < levelProgress; i++)
        {
            playerExp = NextLevelAccExp;
            SetupData(playerExp, expDataList);
        }
    }
    #endregion
}

public class LevelFactionData
{
    #region Public Properties
    public int Level { get; private set; }
    public int NextLevel { get; private set; }
    public int AccExp { get; private set; }
    public int CurrentLevelAccExp { get; private set; }
    public int NextLevelAccExp { get; private set; }
    #endregion

    #region Private Properties
    #endregion

    #region Contructors
    public LevelFactionData()
    {
    }

    public LevelFactionData(int factionExp)
    {
        Level = 0;
        NextLevel = 0;
        AccExp = 0;
        CurrentLevelAccExp = 0;
        NextLevelAccExp = 0;

        List<PlayerExpFactionDBData> expDataList = new List<PlayerExpFactionDBData>();
        List<PlayerExpFactionDBData> data;
        if (PlayerExpFactionDB.Instance.GetAllData(out data))
        {
            expDataList = new List<PlayerExpFactionDBData>(data);
        }
        SetupData(factionExp, expDataList);
    }
    #endregion

    #region Methods
    private void SetupData(int playerExp, List<PlayerExpFactionDBData> expDataList)
    {
        Level = 0;
        NextLevel = 0;
        AccExp = playerExp;
        bool isInRange = false;
        foreach (PlayerExpFactionDBData item in expDataList)
        {
            if (playerExp >= item.AccumulativeExp)
            {
                Level++;
                NextLevel++;
                CurrentLevelAccExp = item.AccumulativeExp;
                NextLevelAccExp = CurrentLevelAccExp;
            }
            else
            {
                NextLevel++;
                NextLevelAccExp = item.AccumulativeExp;
                isInRange = true;
                break;
            }
        }

        if (!isInRange)
        {
            AccExp = Mathf.Min(AccExp, CurrentLevelAccExp);
        }
    }

    public void ForceLevelUp(int levelProgress = 1)
    {
        int factionExp = 0;
        List<PlayerExpFactionDBData> expDataList = new List<PlayerExpFactionDBData>();
        List<PlayerExpFactionDBData> data;
        if (PlayerExpFactionDB.Instance.GetAllData(out data))
        {
            expDataList = new List<PlayerExpFactionDBData>(data);
        }

        for (int i = 0; i < levelProgress; i++)
        {
            factionExp = NextLevelAccExp;
            SetupData(factionExp, expDataList);
        }
    }
    #endregion
}

public class PlayerLevelFactionData
{
    #region Private Properties
    private Dictionary<CardElementType, LevelFactionData> _levelFactionDataList = new Dictionary<CardElementType, LevelFactionData>();
    #endregion

    #region Contructors
    public PlayerLevelFactionData()
    {
    }

    public PlayerLevelFactionData(Dictionary<string, int> playerExpFaction)
    {
        SetupData(playerExpFaction);
    }

    public PlayerLevelFactionData(PlayerLevelFactionData data)
    {
        _levelFactionDataList = new Dictionary<CardElementType, LevelFactionData>(data._levelFactionDataList);
    }
    #endregion

    #region Methods
    private void SetupData(Dictionary<string, int> playerExpFaction)
    {
        foreach (KeyValuePair<string, int> item in playerExpFaction)
        {
            CardElementType cardElementType;
            if (GameHelper.StrToEnum(item.Key, out cardElementType))
            {
                _levelFactionDataList.Add(cardElementType, new LevelFactionData(item.Value));
            }
        }
    }

    public LevelFactionData GetLevelFactionData(CardElementType factionType)
    {
        if (_levelFactionDataList.ContainsKey(factionType))
        {
            return _levelFactionDataList[factionType];
        }

        return null;
    }

    public Dictionary<CardElementType, LevelFactionData> GetLevelFactionDataList()
    {
        return _levelFactionDataList;
    }
    #endregion
}

public class PlayerRankData
{
    #region Public Properties
    public int SeasonIndex { get; private set; }
    public int MaxRankIndex { get; private set; }
    public int CurrentRankIndex { get; private set; }
    public DateTime StartTimestampUTC { get; private set; }
    public DateTime EndTimestampUTC { get; private set; }
    #endregion

    #region Contructors
    public PlayerRankData(int seasonIndex, Dictionary<string, object> rawData)
    {
        SeasonIndex = seasonIndex;
        MaxRankIndex = int.Parse(rawData["max"].ToString());
        CurrentRankIndex = int.Parse(rawData["current"].ToString());
        StartTimestampUTC = Convert.ToDateTime(rawData["start_timestamp"].ToString()).ToUniversalTime();

        if (rawData.ContainsKey("end_timestamp"))
        {
            EndTimestampUTC = Convert.ToDateTime(rawData["end_timestamp"].ToString()).ToUniversalTime();
        }
        else
        {
            EndTimestampUTC = DateTimeData.GetDateTimeUTC();
        }
    }
    #endregion

    #region Methods
    public Dictionary<string, object> ToRawData()
    {
        Dictionary<string, object> data = new Dictionary<string, object>();
        data.Add("season", SeasonIndex);
        data.Add("max", MaxRankIndex);
        data.Add("current", CurrentRankIndex);
        data.Add("start_timestamp", StartTimestampUTC.ToString(GameHelper.DateTimeFormatPlayfab));
        data.Add("end_timestamp", EndTimestampUTC.ToString(GameHelper.DateTimeFormatPlayfab));
        return data;
    }
    #endregion
}

public class PlayerMMRData
{
    #region Public Properties
    public float MaxMMRValue { get; private set; }
    public float MinMMRValue { get; private set; }
    public float CurrentMMRValue { get; private set; }
    #endregion

    #region Contructors
    public PlayerMMRData()
    {
        MaxMMRValue = 3000;
        MinMMRValue = 0;
        CurrentMMRValue = 0;
    }

    public PlayerMMRData(Dictionary<string, float> rawData)
    {
        MaxMMRValue = rawData["max"];
        MinMMRValue = rawData["min"];
        CurrentMMRValue = rawData["current"];
    }

    public PlayerMMRData(PlayerMMRData data)
    {
        MaxMMRValue = data.MaxMMRValue;
        MinMMRValue = data.MinMMRValue;
        CurrentMMRValue = data.CurrentMMRValue;
    }
    #endregion

    #region Methods
    public Dictionary<string, float> ToRawData()
    {
        Dictionary<string, float> data = new Dictionary<string, float>();
        data.Add("max", MaxMMRValue);
        data.Add("min", MinMMRValue);
        data.Add("current", CurrentMMRValue);

        return data;
    }
    #endregion
}
#endregion

public class PlayerInfoData
{
    #region Public Properties
    public string PlayerID { get; private set; }
    public string DisplayName { get { if (string.IsNullOrEmpty(_displayName)) { return "ATN_" + PlayerID; } else { return _displayName; } } }
    public string AvatarID { get { if (string.IsNullOrEmpty(_avatarID)) { return DataManager.Instance.DefaultAvatarIDList[0]; } else { return _avatarID; } } }
    public string TitleID { get { if (string.IsNullOrEmpty(_titleID)) { return DataManager.Instance.DefaultTitleIDList[0]; } else { return _titleID; } } }
    public PlayerLevelData LevelData { get { return _levelData; } }
    public PlayerLevelFactionData LevelFactionData { get { return _levelFactionData; } }
    public PlayerMMRData PlayerMMR { get { return _playerMMR; } }
    public PlayerActivity Activity { get { return GetPlayerActivity(); } }
    public PlayerActivity LastActivity { get; private set; }
    public PlayerActiveStatus ActiveStatus
    {
        get
        {
            PlayerActiveStatus status = GetPlayerActiveStatus();
            if (status == PlayerActiveStatus.Online && _isPlayingMission)
            {
                return PlayerActiveStatus.Busy;
            }
            else
            {
                return status;
            }
        }
    }
    public DateTime LastLogin { get { return GetPlayerLastLogin(); } }
    #endregion

    #region Private Properties
    private string _avatarID = string.Empty;
    private string _titleID = string.Empty;
    private string _displayName = string.Empty;
    private Dictionary<string, Dictionary<string, int>> _playerStatData = new Dictionary<string, Dictionary<string, int>>();
    private Dictionary<int, PlayerRankData> _playerRankData = new Dictionary<int, PlayerRankData>();
    private PlayerMMRData _playerMMR = new PlayerMMRData();
    private Dictionary<string, string> _activityData = new Dictionary<string, string>();
    private bool _isPlayingMission = false;
    private PlayerLevelData _levelData = new PlayerLevelData();
    private PlayerLevelFactionData _levelFactionData = new PlayerLevelFactionData();
    #endregion

    #region Contructors
    public PlayerInfoData()
    {
        PlayerID = "N/A";
        _levelData = new PlayerLevelData();
        _levelFactionData = new PlayerLevelFactionData();
    }

    public PlayerInfoData(Dictionary<string, object> rawData)
    {
        PlayerID = rawData["player_id"].ToString();
        _displayName = rawData["display_name"] == null ? "" : rawData["display_name"].ToString();
        _avatarID = rawData["avatar_id"].ToString();
        _titleID = rawData["title_id"].ToString();
        _activityData = JsonConvert.DeserializeObject<Dictionary<string, string>>(rawData["status_data"].ToString());

        int accExp = int.Parse(rawData["exp"].ToString());
        SetLevelData(accExp);

        if (rawData.ContainsKey("exp_faction"))
        {
            SetLevelFactionData(JsonConvert.DeserializeObject<Dictionary<string, int>>(rawData["exp_faction"].ToString()));
        }

        Dictionary<string, object> playerRank = JsonConvert.DeserializeObject<Dictionary<string, object>>(rawData["rank_data"].ToString());
        SetPlayerRank(playerRank);
    }

    public PlayerInfoData(PlayerInfoData data)
    {
        PlayerID = data.PlayerID;
        _displayName = data._displayName;
        _avatarID = data._avatarID;
        _titleID = data._titleID;
        _levelData = new PlayerLevelData(data.LevelData);
        _levelFactionData = new PlayerLevelFactionData(data.LevelFactionData); 
        _activityData = new Dictionary<string, string>(data._activityData);
        _playerStatData = new Dictionary<string, Dictionary<string, int>>(data._playerStatData);
        _playerRankData = new Dictionary<int, PlayerRankData>(data._playerRankData);
        _playerMMR = new PlayerMMRData(data._playerMMR);
    }
    #endregion

    #region Set Methods
    public void SetIsPlayingMission(bool isPlayingMission)
    {
        _isPlayingMission = isPlayingMission;
    }

    public void SetPlayerID(string playerID)
    {
        PlayerID = playerID;
    }

    public void SetDisplayName(string displayName)
    {
        _displayName = displayName;
    }

    public void SetAvatarID(string avatarID)
    {
        _avatarID = avatarID;
    }

    public void SetTitleID(string titleID)
    {
        _titleID = titleID;
    }

    public void SetLevelData(int accExp)
    {
        _levelData = new PlayerLevelData(accExp);
    }

    public void SetLevelFactionData(Dictionary<string, int> accExpFaction)
    {
        _levelFactionData = new PlayerLevelFactionData(accExpFaction);
    }

    private void SetPlayerActivityData(Dictionary<string, string> data)
    {
        _activityData = new Dictionary<string, string>(data);
    }

    public void SetPlayerActivityNow(string activity)
    {
        if (!_activityData.ContainsKey("activity"))
        {
            _activityData.Add("activity", "");
        }
        if (!_activityData.ContainsKey("timestamp"))
        {
            _activityData.Add("timestamp", "");
        }

        _activityData["activity"] = activity;
        _activityData["timestamp"] = DateTimeData.GetDateTimeUTC().ToString(GameHelper.DateTimeFormatPlayfab);
        try
        {
            LastActivity = activity.ToLower() == "quit" ? LastActivity : activity.ToEnum<PlayerActivity>();
        }
        catch (Exception e)
        {
            LastActivity = PlayerActivity.None;
        }
    }

    public void SetPlayerStatData(Dictionary<string, Dictionary<string, int>> playerStatData)
    {
        _playerStatData = new Dictionary<string, Dictionary<string, int>>(playerStatData);
    }

    public void SetPlayerStatData(string statDataKey, Dictionary<string, int> playerStatData)
    {
        if (!_playerStatData.ContainsKey(statDataKey))
        {
            _playerStatData.Add(statDataKey, new Dictionary<string, int>());
        }
        _playerStatData[statDataKey] = new Dictionary<string, int>(playerStatData);
    }

    public void SetPlayerRank(Dictionary<string, object> playerRank)
    {
        if (playerRank == null)
        {
            return;
        }

        _playerRankData = new Dictionary<int, PlayerRankData>();
        Dictionary<string, object> data = new Dictionary<string, object>();
        int seasonIndex = 0;
        foreach (KeyValuePair<string, object> item in playerRank)
        {
            seasonIndex = int.Parse(item.Key);
            data = JsonConvert.DeserializeObject<Dictionary<string, object>>(item.Value.ToString());
            _playerRankData.Add(seasonIndex, new PlayerRankData(seasonIndex, data));
        }
    }

    private void SetPlayerRank(Dictionary<int, Dictionary<string, object>> playerRank)
    {
        _playerRankData = new Dictionary<int, PlayerRankData>();
        foreach (KeyValuePair<int, Dictionary<string, object>> item in playerRank)
        {
            _playerRankData.Add(item.Key, new PlayerRankData(item.Key, item.Value));
        }
    }

    public void SetPlayerMMR(Dictionary<string, Dictionary<string, float>> playerMMR)
    {
        if (playerMMR == null)
        {
            return;
        }

        int currentSeason = DataManager.Instance.GetCurrentSeasonIndex();
        string currentSeasonStr = currentSeason.ToString();
        if (playerMMR.ContainsKey(currentSeasonStr))
        {
            Dictionary<string, float> data = new Dictionary<string, float>(playerMMR[currentSeasonStr]);
            _playerMMR = new PlayerMMRData(data);
        }
    }

    private void SetPlayerMMR(Dictionary<string, float> playerMMR)
    {
        _playerMMR = new PlayerMMRData(playerMMR);
    }
    #endregion

    #region Methods
    private PlayerActiveStatus GetPlayerActiveStatus()
    {
        switch (Activity)
        {
            case PlayerActivity.None:
            case PlayerActivity.Quit:
            {
                return PlayerActiveStatus.Offline;
            }

            case PlayerActivity.In_MainMenu:
            {
                return PlayerActiveStatus.Online;
            }

            case PlayerActivity.In_Shop:
            case PlayerActivity.In_Pack:
            case PlayerActivity.In_Deck:
            case PlayerActivity.In_Quest:
            case PlayerActivity.In_EventBattle:
            case PlayerActivity.In_GameEvent:
            case PlayerActivity.In_Tutorial:
            case PlayerActivity.In_Mode_Select:
            case PlayerActivity.In_StarExchange:
            case PlayerActivity.In_Option:
            {
                return PlayerActiveStatus.Busy;
            }

            case PlayerActivity.In_Lobby_Tutorial:
            case PlayerActivity.In_Lobby_Practice:
            case PlayerActivity.In_Lobby_Rank:
            case PlayerActivity.In_Lobby_Arena:
            case PlayerActivity.In_Lobby_Casual:
            case PlayerActivity.In_Lobby_Friendly:
            case PlayerActivity.In_Lobby_EventBattle:
            case PlayerActivity.Play_Tutorial:
            case PlayerActivity.Play_Practice:
            case PlayerActivity.Play_Rank:
            case PlayerActivity.Play_Arena:
            case PlayerActivity.Play_Casual:
            case PlayerActivity.Play_Friendly:
            case PlayerActivity.Play_EventBattle:
            {
                return PlayerActiveStatus.Playing;
            }

            default: return PlayerActiveStatus.Offline;
        }
    }

    private PlayerActivity GetPlayerActivity()
    {
        PlayerActivity activity = PlayerActivity.None;
        if (_activityData.ContainsKey("timestamp"))
        {
            DateTime timestamp = Convert.ToDateTime(_activityData["timestamp"]).ToUniversalTime();
            TimeSpan delta = DateTimeData.GetDateTimeUTC() - timestamp;
            if (delta.TotalSeconds < 55.0f)
            {
                if (_activityData.ContainsKey("activity"))
                {
                    GameHelper.StrToEnum(_activityData["activity"], out activity);
                }
            }
        }

        return activity;
    }

    private DateTime GetPlayerLastLogin()
    {
        if (_activityData != null && _activityData.ContainsKey("timestamp"))
        {
            DateTime timestamp = Convert.ToDateTime(_activityData["timestamp"]).ToUniversalTime();

            return timestamp;
        }

        return DateTimeData.GetLoginTimeStamp();
    }

    public Dictionary<string, int> GetPlayerStatData()
    {
        Dictionary<string, int> result = new Dictionary<string, int>();
        if (_playerStatData != null && _playerStatData.Count > 0)
        {
            foreach (KeyValuePair<string, Dictionary<string, int>> data in _playerStatData)
            {
                foreach (KeyValuePair<string, int> item in data.Value)
                {
                    if (!result.ContainsKey(item.Key))
                    {
                        result.Add(item.Key, 0);
                    }
                    result[item.Key] += item.Value;
                }
            }
            return result;
        }
        else
        {
            return new Dictionary<string, int>();
        }
    }

    public int GetPlayerStat(params string[] keyDetails)
    {
        int result = 0;
        string keyStr = string.Empty;

        foreach (string key in keyDetails)
        {
            if (keyStr != string.Empty)
            {
                keyStr += "_";
            }
            keyStr += key;
        }

        foreach (KeyValuePair<string, Dictionary<string, int>> data in _playerStatData)
        {
            if (data.Value.ContainsKey(keyStr))
            {
                result = data.Value[keyStr];
            }
        }

        return result;
    }

    public float GetPlayerWinRate(GameMode gameMode)
    {
        float winRate = 0.0f;
        int winCount = 0;
        int playCount = 0;
        Dictionary<string, int> playerStatData = GetPlayerStatData();
        string gameModeKey = string.Empty;

        switch (gameMode)
        {
            case GameMode.Casual:
                {
                    gameModeKey = StatKeys.CAS.ToString();

                }
                break;

            case GameMode.Rank:
                {
                    gameModeKey = StatKeys.RANK.ToString();
                }
                break;

            case GameMode.Arena:
                {
                    gameModeKey = StatKeys.ARN.ToString();
                }
                break;

            case GameMode.Event:
                {
                    gameModeKey = StatKeys.TB.ToString();
                }
                break;

            case GameMode.Practice:
                {
                    gameModeKey = StatKeys.PRA.ToString();
                }
                break;
        }

        PlayerStatKey winStatKey = new PlayerStatKey(StatKeys.W.ToString(), gameModeKey);
        PlayerStatKey playStatKey = new PlayerStatKey(StatKeys.P.ToString(), gameModeKey);
        string winKey = winStatKey.StatKey;
        string playKey = playStatKey.StatKey;
        if (playerStatData.ContainsKey(winKey)) winCount = playerStatData[winKey];
        if (playerStatData.ContainsKey(playKey)) playCount = playerStatData[playKey];

        if (playCount > 0)
        {
            winRate = Mathf.Min((float)winCount / (float)playCount, 1.0f);
        }

        return winRate;
    }


    public void GetPlayerPlayScore(GameMode gameMode, CardElementType faction, out int playCount, out int winCount, out float winRate)
    {
        winRate = 0.0f;
        winCount = 0;
        playCount = 0;
        Dictionary<string, int> playerStatData = GetPlayerStatData();
        string gameModeKey = string.Empty;

        switch (gameMode)
        {
            case GameMode.Casual:
                {
                    gameModeKey = StatKeys.CAS.ToString();
                }
                break;

            case GameMode.Rank:
                {
                    gameModeKey = StatKeys.RANK.ToString();
                }
                break;

            case GameMode.Arena:
                {
                    gameModeKey = StatKeys.ARN.ToString();
                }
                break;

            case GameMode.Event:
                {
                    gameModeKey = StatKeys.TB.ToString();
                }
                break;

            case GameMode.Practice:
                {
                    gameModeKey = StatKeys.PRA.ToString();
                }
                break;
        }

        PlayerStatKey winStatKey = new PlayerStatKey(StatKeys.W.ToString(), gameModeKey, StatKeys.F.ToString(), faction.ToString());
        PlayerStatKey playStatKey = new PlayerStatKey(StatKeys.P.ToString(), gameModeKey, StatKeys.F.ToString(), faction.ToString());
        string winKey = winStatKey.StatKey;
        string playKey = playStatKey.StatKey;
        if (playerStatData.ContainsKey(winKey)) winCount = playerStatData[winKey];
        if (playerStatData.ContainsKey(playKey)) playCount = playerStatData[playKey];

        if (playCount > 0)
        {
            winRate = Mathf.Min((float)winCount / (float)playCount, 1.0f);
        }
    }


    public float GetPlayerCasualHiddenScore()
    {
        float winRate = GetPlayerWinRate(GameMode.Casual);
        int playerLevel = LevelData.Level;
        float hiddenScore = winRate * (float)playerLevel;

        return hiddenScore;
    }

    public PlayerTierType GetPlayerTier()
    {
        int winTutCount = 0;
        int winCount = 0;
        int loser1Count = 0;
        int loser2Count = 0;

        string winTut1 = "W_TUT_1";
        PlayerStatKey winCasualStat = new PlayerStatKey(StatKeys.W.ToString(), StatKeys.CAS.ToString());
        PlayerStatKey winRankStat = new PlayerStatKey(StatKeys.W.ToString(), StatKeys.RANK.ToString());
        string winCasual = winCasualStat.StatKey;
        string winRank = winRankStat.StatKey;
        string loser1Stack = StatKeys.LOSER_1_STACK.ToString();
        string loser2Stack = StatKeys.LOSER_2_STACK.ToString();

        Dictionary<string, int> playerStat = GetPlayerStatData();
        int newbieStat = GetPlayerStat("NEWBIE_CAS");

        // gather win count
        if (playerStat.ContainsKey(winTut1))
        {
            winTutCount += playerStat[winTut1];
        }

        if (playerStat.ContainsKey(winCasual))
        {
            winCount += playerStat[winCasual];
        }
        if (playerStat.ContainsKey(winRank))
        {
            winCount += playerStat[winRank];
        }

        if (playerStat.ContainsKey(loser1Stack))
        {
            loser1Count += playerStat[loser1Stack];
        }
        if (playerStat.ContainsKey(loser2Stack))
        {
            loser2Count += playerStat[loser2Stack];
        }

        // win tutorial 1 at least 1 time
        if (winTutCount > 0)
        {
            // check if be newbie
            if(newbieStat >= 0)
            {
                if(newbieStat >= 25)
                {
                    return PlayerTierType.Newbie1;
                }
                else if(newbieStat >= 20)
                {
                    return PlayerTierType.Newbie2;
                }
                else if (newbieStat >= 15)
                {
                    return PlayerTierType.Newbie3;
                }
                else if (newbieStat >= 10)
                {
                    return PlayerTierType.Newbie4;
                }
                else if (newbieStat >= 5)
                {
                    return PlayerTierType.Newbie5;
                }
                else if (newbieStat >= 0)
                {
                    return PlayerTierType.Newbie6;
                }
            }
            else
            {
                // there are loser 1 stack at least 3 count
                if (loser1Count >= 3)
                {
                    // there are loser 2 stack at least 1 count
                    if (loser2Count > 0)
                    {
                        return PlayerTierType.Loser2;
                    }
                    else
                    {
                        return PlayerTierType.Loser1;
                    }
                }
                else
                {
                    return PlayerTierType.Player;
                }
            }
        }

        return PlayerTierType.Newbie1;
    }

    public bool IsBeNewbieCasual()
    {
        List<PlayerTierType> newbieList = new List<PlayerTierType>()
        {
            PlayerTierType.Newbie1
            , PlayerTierType.Newbie2
            , PlayerTierType.Newbie3
            , PlayerTierType.Newbie4
            , PlayerTierType.Newbie5
            , PlayerTierType.Newbie6
        };
        return newbieList.Contains(GetPlayerTier());
    }

    public bool IsBeApprentice()
    {
        return (GetPlayerCurrentRank() < 0);
    }

    public int GetNewbieCasualScore()
    {
        return GetPlayerStat("NEWBIE_CAS");
    }

    public int GetPlayerCurrentRank()
    {
        int playerRankResult = 0;
        int currentSeason = DataManager.Instance.GetCurrentSeasonIndex();
        if (_playerRankData.ContainsKey(currentSeason))
        {
            playerRankResult = _playerRankData[currentSeason].CurrentRankIndex;
        }
        return playerRankResult;
    }

    public int GetPlayerCurrentRank(int seasonIndex)
    {
        int playerRankResult = 0;
        int currentSeason = seasonIndex;
        if (_playerRankData.ContainsKey(currentSeason))
        {
            playerRankResult = _playerRankData[currentSeason].CurrentRankIndex;
        }
        return playerRankResult;
    }

    public List<PlayerRankData> GetPlayerRankDataList()
    {
        List<PlayerRankData> result = new List<PlayerRankData>();
        foreach (KeyValuePair<int, PlayerRankData> data in _playerRankData)
        {
            result.Add(data.Value);
        }

        return result;
    }

    public static PlayerInfoData CreateBotData(string id, string displayName, string avatarID, string titleID)
    {
        PlayerInfoData data = new PlayerInfoData();
        data.SetPlayerID(id);
        data.SetDisplayName(displayName);
        data.SetAvatarID(avatarID);
        data.SetTitleID(titleID);
        return data;
    }
    #endregion

    #region Parsing Methods
    public string ToJson()
    {
        Dictionary<string, string> data = new Dictionary<string, string>();
        data.Add("player_id", PlayerID);
        data.Add("display_name", DisplayName);
        data.Add("avatar_id", AvatarID);
        data.Add("title_id", TitleID);
        data.Add("acc_exp", LevelData.AccExp.ToString());
        data.Add("activity_data", JsonConvert.SerializeObject(_activityData));
        data.Add("stat_data", JsonConvert.SerializeObject(_playerStatData));

        Dictionary<int, Dictionary<string, object>> rankData = new Dictionary<int, Dictionary<string, object>>();
        foreach (KeyValuePair<int, PlayerRankData> item in _playerRankData)
        {
            rankData.Add(item.Key, item.Value.ToRawData());
        }
        data.Add("rank_data", JsonConvert.SerializeObject(rankData));

        data.Add("mmr_data", JsonConvert.SerializeObject(PlayerMMR.ToRawData()));

        return JsonConvert.SerializeObject(data);
    }

    public static PlayerInfoData ConvertFromJson(string jsonStr)
    {
        try
        {
            Dictionary<string, string> data = JsonConvert.DeserializeObject<Dictionary<string, string>>(jsonStr);
            PlayerInfoData playerInfoData = new PlayerInfoData();
            playerInfoData.SetPlayerID(data["player_id"]);
            playerInfoData.SetDisplayName(data["display_name"]);
            playerInfoData.SetAvatarID(data["avatar_id"]);
            playerInfoData.SetTitleID(data["title_id"]);
            playerInfoData.SetLevelData(int.Parse(data["acc_exp"]));
            playerInfoData.SetPlayerActivityData(JsonConvert.DeserializeObject<Dictionary<string, string>>(data["activity_data"]));
            playerInfoData.SetPlayerStatData(JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, int>>>(data["stat_data"]));
            playerInfoData.SetPlayerRank(JsonConvert.DeserializeObject<Dictionary<int, Dictionary<string, object>>>(data["rank_data"]));
            playerInfoData.SetPlayerMMR(JsonConvert.DeserializeObject<Dictionary<string, float>>(data["mmr_data"]));

            return playerInfoData;
        }
        catch
        {
            return new PlayerInfoData();
        }
    }
    #endregion
}