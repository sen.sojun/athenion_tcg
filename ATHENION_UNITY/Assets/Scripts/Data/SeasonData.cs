﻿using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using System;
using UnityEngine;

public struct SeasonInfo
{
    public DateTime StartDateTimeUTC { get; private set; }
    public DateTime EndDateTimeUTC { get; private set; }
    public DateTime RewardDateTimeUTC { get; private set; }

    public SeasonInfo(Dictionary<string, string> dataInfo)
    {
        string startDateTimeUTC = dataInfo["start_date"];
        string endDateTimeUTC = dataInfo["end_date"];
        string rewardDateTimeUTC = dataInfo["reward_date"];

        StartDateTimeUTC = string.IsNullOrEmpty(startDateTimeUTC) ? DateTime.MinValue : Convert.ToDateTime(startDateTimeUTC).ToUniversalTime();
        EndDateTimeUTC = string.IsNullOrEmpty(endDateTimeUTC) ? DateTime.MinValue : Convert.ToDateTime(endDateTimeUTC).ToUniversalTime();
        RewardDateTimeUTC = string.IsNullOrEmpty(endDateTimeUTC) ? DateTime.MinValue : Convert.ToDateTime(rewardDateTimeUTC).ToUniversalTime();
    }

    public Dictionary<string, string> ToDictionary()
    {
        Dictionary<string, string> dataResult = new Dictionary<string, string>();
        dataResult.Add("start_date", StartDateTimeUTC.ToString(GameHelper.DateTimeFormatPlayfab));
        dataResult.Add("end_date", EndDateTimeUTC.ToString(GameHelper.DateTimeFormatPlayfab));
        dataResult.Add("reward_date", RewardDateTimeUTC.ToString(GameHelper.DateTimeFormatPlayfab));

        return dataResult;
    }
}

public class SeasonData
{
    #region Public Properties
    public int CurrentSeason { get { return CalculateCurrentSeason(); } }
    public bool IsUpdateSeason;
    public DateTime refreshTimeStamp;

    public Dictionary<string, SeasonInfo> SeasonDataList { get { return _seasonDataList; } }
    #endregion

    #region Private Properties 
    private Dictionary<string, SeasonInfo> _seasonDataList = new Dictionary<string, SeasonInfo>();

    private int _currentSeason;
    private static readonly string _currentSeasonKey = "current_season";
    private static readonly string _dataKey = "data";
    #endregion

    public SeasonData()
    {
        _currentSeason = -1;
        _seasonDataList = new Dictionary<string, SeasonInfo>();
    }

    public SeasonData(int currentSeason, Dictionary<string, SeasonInfo> seasonDataList)
    {
        _currentSeason = currentSeason;
        _seasonDataList = new Dictionary<string, SeasonInfo>(seasonDataList);
    }

    private int CalculateCurrentSeason()
    {
        foreach (KeyValuePair<string, SeasonInfo> season in _seasonDataList)
        {
            DateTime utcTime = DateTimeData.GetDateTimeUTC();
            if (utcTime >= season.Value.StartDateTimeUTC && utcTime <= season.Value.EndDateTimeUTC)
            {
                return Convert.ToInt32(season.Key);
            }
        }
        return _currentSeason;
    }

    public string ToJson()
    {
        Dictionary<string, object> dataResult = new Dictionary<string, object>();
        dataResult.Add(_currentSeasonKey, CurrentSeason);

        Dictionary<string, Dictionary<string, string>> dataList = new Dictionary<string, Dictionary<string, string>>();
        foreach (KeyValuePair<string, SeasonInfo> item in SeasonDataList)
        {
            dataList.Add(item.Key, item.Value.ToDictionary());
        }
        dataResult.Add(_dataKey, dataList);

        return JsonConvert.SerializeObject(dataResult);
    }

    public static SeasonData ConvertJsonToSeasonData(string jsonStr)
    {
        Dictionary<string, object> data = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonStr);
        int currentSeason = -1;
        Dictionary<string, SeasonInfo> seasonDataList = new Dictionary<string, SeasonInfo>();
        if (data.ContainsKey(_currentSeasonKey))
        {
            currentSeason = int.Parse(data[_currentSeasonKey].ToString());
        }

        Dictionary<string, Dictionary<string, string>> dataDict = new Dictionary<string, Dictionary<string, string>>();
        if (data.ContainsKey(_dataKey))
        {
            dataDict = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, string>>>(data[_dataKey].ToString());
            foreach (KeyValuePair<string, Dictionary<string, string>> item in dataDict)
            {
                SeasonInfo info = new SeasonInfo(item.Value);
                seasonDataList.Add(item.Key, info);
            }
        }

        return new SeasonData(currentSeason, seasonDataList);
    }
}