﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class MessageInboxData
{
    #region Public Properties
    public string ID { get; private set; }

    public bool IsRead { get; private set; }
    public bool IsClaimed { get; private set; }
    public bool IsDeleted { get; private set; }

    public bool IsReceived { get { return DateTimeData.GetDateTimeUTC() >= ReceivedDateTimeUTC; } }
    public bool IsExpired { get { if (ExpiredDateTimeUTC == DateTime.MinValue) { return false; } else { return DateTimeData.GetDateTimeUTC() >= ExpiredDateTimeUTC; } } }
    public string Title
    {
        get
        {
            return GetTitle(LocalizationManager.Instance.CurrentLanguage);
        }
    }
    public string Message
    {
        get
        {
            return GetMessage(LocalizationManager.Instance.CurrentLanguage);
        }
    }

    public Dictionary<string, string> TitleData { get { return _titleData; } }
    public Dictionary<string, string> MessageData { get { return _messageData; } }

    public List<ItemData> ItemDataList { get { return _itemDataList; } }
    public DateTime CreatedDateTimeUTC { get { return _createdDateTimeUTC; } }
    public DateTime ReceivedDateTimeUTC { get { return _receivedDateTimeUTC; } }
    public DateTime ExpiredDateTimeUTC { get { return _expiredDateTimeUTC; } }
    #endregion

    #region Private Properties
    private List<ItemData> _itemDataList = new List<ItemData>();
    private Dictionary<string, string> _titleData = new Dictionary<string, string>();
    private Dictionary<string, string> _messageData = new Dictionary<string, string>();
    private DateTime _createdDateTimeUTC = DateTime.MinValue;
    private DateTime _receivedDateTimeUTC = DateTime.MinValue;
    private DateTime _expiredDateTimeUTC = DateTime.MaxValue;
    #endregion

    public MessageInboxData(string id, Dictionary<string, object> msgDetail)
    {
        ID = id;
        IsRead = false;
        IsClaimed = false;
        IsDeleted = false;
        _titleData = JsonConvert.DeserializeObject<Dictionary<string, string>>(msgDetail["title"].ToString());
        _messageData = JsonConvert.DeserializeObject<Dictionary<string, string>>(msgDetail["msg"].ToString());

        if (msgDetail != null)
        {
            if (msgDetail.ContainsKey("item"))
            {
                Dictionary<string, int> itemData = JsonConvert.DeserializeObject<Dictionary<string, int>>(msgDetail["item"].ToString());
                foreach (KeyValuePair<string, int> item in itemData)
                {
                    _itemDataList.Add(new ItemData(item));
                }
            }

            if (msgDetail.ContainsKey("created_dt") && !string.IsNullOrEmpty(msgDetail["created_dt"].ToString()))
            {
                _createdDateTimeUTC = Convert.ToDateTime(msgDetail["created_dt"].ToString());
            }
            if (msgDetail.ContainsKey("expired_dt") && !string.IsNullOrEmpty(msgDetail["expired_dt"].ToString()))
            {
                _expiredDateTimeUTC = Convert.ToDateTime(msgDetail["expired_dt"].ToString());
            }
            if (msgDetail.ContainsKey("received_dt") && !string.IsNullOrEmpty(msgDetail["received_dt"].ToString()))
            {
                _receivedDateTimeUTC = Convert.ToDateTime(msgDetail["received_dt"].ToString());
            }
        }
    }

    public MessageInboxData(MessageInboxData data)
    {
        ID = data.ID;
        IsRead = data.IsRead;
        IsClaimed = data.IsClaimed;
        IsDeleted = data.IsDeleted;
        _titleData = new Dictionary<string, string>(data._titleData);
        _messageData = new Dictionary<string, string>(data._messageData);
        _itemDataList = new List<ItemData>(data._itemDataList);
        _createdDateTimeUTC = data._createdDateTimeUTC;
        _expiredDateTimeUTC = data._expiredDateTimeUTC;
        _receivedDateTimeUTC = data._receivedDateTimeUTC;
    }

    public void SetRead()
    {
        IsRead = true;
    }

    public void SetClaim()
    {
        IsClaimed = true;
    }

    public void SetDelete()
    {
        IsDeleted = true;
    }

    public string GetTitle(Language language)
    {
        string lang = language.ToString();
        if (_titleData.ContainsKey(lang))
        {
            return _titleData[lang];
        }

        return "";
    }

    public string GetMessage(Language language)
    {
        string lang = language.ToString();
        if (_messageData.ContainsKey(lang))
        {
            return _messageData[lang];
        }

        return "";
    }

    public bool IsCanClaim()
    {
        return !IsClaimed && IsReceived && !IsExpired && (ItemDataList != null && ItemDataList.Count > 0);
    }

    public bool IsCanRead()
    {
        return !IsRead && IsReceived && !IsExpired;
    }
}
