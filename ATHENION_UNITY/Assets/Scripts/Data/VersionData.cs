﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class VersionData
{
    #region Public Properties
    public RuntimePlatform Platform { get; private set; }
    public string Version { get; private set; }
    public ClientStatus Status { get; private set; }
    #endregion

    #region Constructors
    public VersionData()
    {
        Platform = RuntimePlatform.Android;
        Version = "";
        Status = ClientStatus.Close;
    }

    public VersionData(RuntimePlatform platform, string version, ClientStatus status)
    {
        Platform = platform;
        Version = version;
        Status = status;
    }

    public VersionData(VersionData data)
    {
        Platform = data.Platform;
        Version = data.Version;
        Status = data.Status;
    }
    #endregion
}

public class ClientVersion
{
    #region Public Properties
    public Dictionary<RuntimePlatform, Dictionary<string, VersionData>> VersionList { get { return _versionList; } }
    #endregion

    #region Private Properties
    private Dictionary<RuntimePlatform, Dictionary<string, VersionData>> _versionList;
    #endregion

    #region Constructors
    public ClientVersion()
    {
        _versionList = new Dictionary<RuntimePlatform, Dictionary<string, VersionData>>();
    }

    public ClientVersion(Dictionary<RuntimePlatform, Dictionary<string, VersionData>> dataList)
    {
        if (dataList != null)
        {
            _versionList = new Dictionary<RuntimePlatform, Dictionary<string, VersionData>>(dataList);
        }
        else
        {
            _versionList = new Dictionary<RuntimePlatform, Dictionary<string, VersionData>>();
        }
    }

    public ClientVersion(ClientVersion clientVersionList)
    {
        if (clientVersionList != null)
        {
            _versionList = new Dictionary<RuntimePlatform, Dictionary<string, VersionData>>(clientVersionList.VersionList);
        }
        else
        {
            _versionList = new Dictionary<RuntimePlatform, Dictionary<string, VersionData>>();
        }
    }
    #endregion

    #region Methods
    public void AddVersion(RuntimePlatform platform, VersionData versionData)
    {
        if (_versionList == null)
        {
            _versionList = new Dictionary<RuntimePlatform, Dictionary<string, VersionData>>();
        }

        if (_versionList.ContainsKey(platform))
        {
            if (_versionList[platform].ContainsKey(versionData.Version))
            {
                // already have this version.
                _versionList[platform][versionData.Version] = versionData; // replace version data.
            }
            else
            {
                _versionList[platform].Add(versionData.Version, versionData);
            }
        }
        else
        {
            Dictionary<string, VersionData> versionList = new Dictionary<string, VersionData>();
            versionList.Add(versionData.Version, versionData);

            _versionList.Add(platform, versionList);
        }
    }

    public List<VersionData> GetVersions(RuntimePlatform platform)
    {
        List<VersionData> result = new List<VersionData>();

        if (VersionList.ContainsKey(platform))
        {
            foreach (KeyValuePair<string, VersionData> data in _versionList[platform])
            {
                result.Add(data.Value);
            }
        }

        return result;
    }

    public bool GetVersion(RuntimePlatform platform, string version, out VersionData result)
    {
        if (VersionList.ContainsKey(platform))
        {
            if (VersionList[platform].ContainsKey(version))
            {
                result = VersionList[platform][version];
                return true;
            }
        }

        result = null;
        return false;
    }

    public string ToJson()
    {
        string json = "";

        json = JsonConvert.SerializeObject(_versionList);

        return json;
    }

    public static ClientVersion JsonToClientVersion(string json)
    {
        ClientVersion clientVersion = new ClientVersion();

        try
        {
            Dictionary<RuntimePlatform, Dictionary<string, string>> versionList = JsonConvert.DeserializeObject<Dictionary<RuntimePlatform, Dictionary<string, string>>>(json);
            VersionData versionData;
            ClientStatus status;
            
            foreach (KeyValuePair<RuntimePlatform, Dictionary<string, string>> versions in versionList)
            {
                foreach (KeyValuePair<string, string> version in versions.Value)
                {
                    if (GameHelper.StrToEnum<ClientStatus>(version.Value, out status))
                    {
                        versionData = new VersionData(versions.Key, version.Key, status);
                        clientVersion.AddVersion(versions.Key, versionData);
                    }
                }
            }
        }
        catch (Exception e)
        {
            Debug.LogErrorFormat("JsonToVersionData: Failed to convert JSON. {0} json={1}", e.Message, json);
        }

        return clientVersion;
    }

    public static ClientVersion CreateVersionData()
    {
        ClientVersion result = new ClientVersion();
        VersionData versionData;

        foreach (RuntimePlatform platform in System.Enum.GetValues(typeof(RuntimePlatform)))
        {
            if (
                   platform != RuntimePlatform.Android
                && platform != RuntimePlatform.IPhonePlayer
            )
            {
                // Skip not mobile platform.
                continue;
            }

            versionData = new VersionData(platform, GameHelper.GetVersionText(platform), ClientStatus.Open);
            result.AddVersion(platform, versionData);
        }

        return result;
    }
    #endregion
}
