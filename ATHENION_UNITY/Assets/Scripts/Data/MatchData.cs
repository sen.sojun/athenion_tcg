﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using Newtonsoft.Json;

public class MatchPlayerData
{
    #region Properties
    public PlayerInfoData PlayerInfo { get; private set; }
    public DeckData Deck { get; private set; }
    public int RandomSeed { get; private set; }

    public static readonly int MaxRandomSeed = 10000;
    #endregion

    #region Constructors
    public MatchPlayerData()
    {
        PlayerInfo = new PlayerInfoData();
        Deck = new DeckData();
        RandomSeed = UnityEngine.Random.Range(0, MaxRandomSeed);
    }

    public MatchPlayerData(PlayerInfoData playerInfo, DeckData deck)
    {
        PlayerInfo = new PlayerInfoData(playerInfo);
        Deck = new DeckData(deck);
        RandomSeed = UnityEngine.Random.Range(0, MaxRandomSeed);
    }

    public MatchPlayerData(PlayerInfoData playerInfo, DeckData deck, int randomSeed)
    {
        PlayerInfo = new PlayerInfoData(playerInfo);
        Deck = new DeckData(deck);
        RandomSeed = randomSeed;
    }

    // Copy Constructor
    public MatchPlayerData(MatchPlayerData matchPlayerData)
    {
        PlayerInfo = new PlayerInfoData(matchPlayerData.PlayerInfo);
        Deck = new DeckData(matchPlayerData.Deck);
        RandomSeed = matchPlayerData.RandomSeed;
    }
    #endregion

    #region Methods
    public static bool JsonToMatchPlayerData(string jsonStr, out MatchPlayerData result)
    {
        result = null;
        try
        {
            Dictionary<string, string> data = JsonConvert.DeserializeObject<Dictionary<string, string>>(jsonStr);
            PlayerInfoData playerInfo = PlayerInfoData.ConvertFromJson(data["player_info"]);
            DeckData deck = DeckData.JsonToDeckData(data["deck"]);
            int randomSeed = int.Parse(data["random_seed"]);
            result = new MatchPlayerData(playerInfo, deck, randomSeed);

            return true;
        }
        catch (Exception e)
        {
            Debug.LogErrorFormat("JsonToMatchPlayerData: Failed to convert JSON. {0} json={1}", e.Message, jsonStr);
            return false;
        }
    }

    public string ToJson()
    {
        Dictionary<string, string> data = new Dictionary<string, string>();
        data.Add("player_info", PlayerInfo.ToJson());
        data.Add("deck", Deck.ToJson());
        data.Add("random_seed", RandomSeed.ToString());

        return JsonConvert.SerializeObject(data);
    }

    public static MatchPlayerData CreateLocalMatchPlayerData()
    {
        MatchPlayerData matchPlayerData = new MatchPlayerData(
              DataManager.Instance.PlayerInfo
            , DataManager.Instance.GetCurrentDeck()
        //, DeckData.GetDebugDeck("deck_debug")
        );

        return matchPlayerData;
    }

    public static MatchPlayerData CreateLocalMatchPlayerData(DeckData customDeck)
    {
        MatchPlayerData matchPlayerData = new MatchPlayerData(
              DataManager.Instance.PlayerInfo
            , customDeck
        );

        return matchPlayerData;
    }

    public static MatchPlayerData CreateEventMatchPlayerData()
    {
        MatchPlayerData matchPlayerData = new MatchPlayerData(
              DataManager.Instance.PlayerInfo
            , DataManager.Instance.GetRandomEventBattleDeck()
        );

        return matchPlayerData;
    }

    public static MatchPlayerData CreateEventMatchPlayerData(string profileID)
    {
        MatchPlayerData matchPlayerData = new MatchPlayerData(
              DataManager.Instance.PlayerInfo
            , DataManager.Instance.GetEventBattleDeck(profileID)
        );

        return matchPlayerData;
    }

    public static MatchPlayerData CreateTutorialMatchPlayerData(GameMode mode)
    {
        switch (mode)
        {
            case GameMode.Tutorial_1:
            case GameMode.Tutorial_2:
            case GameMode.Tutorial_3:
            case GameMode.Tutorial_4:
                break;

            default: return null;
        }

        string deckID = "DECK_TUTOR";

        switch (mode)
        {
            case GameMode.Tutorial_1:
            {
                deckID = "DECK_TUTOR_1_NEW";
            }
            break;

            case GameMode.Tutorial_2:
            {
                deckID = "DECK_TUTOR_2_NEW";
            }
            break;

            case GameMode.Tutorial_3:
            {
                deckID = "DECK_TUTOR_3_NEW";
            }
            break;

            case GameMode.Tutorial_4:
            {
                deckID = "DECK_TUTOR_4_NEW";
            }
            break;
        }

        DeckData deck = new DeckData();
        DeckDB.Instance.GetDeck(deckID, out deck);

        MatchPlayerData matchPlayerData = new MatchPlayerData(DataManager.Instance.PlayerInfo, deck);

        return matchPlayerData;
    }

    public static MatchPlayerData CreateTutorialMatchBotData(GameMode mode)
    {
        switch (mode)
        {
            case GameMode.Tutorial_1:
            case GameMode.Tutorial_2:
            case GameMode.Tutorial_3:
            case GameMode.Tutorial_4:
                break;

            default: return null;
        }

        string deckID = "DECK_TUTOR_AI";
        string botID = "BOT_TUTORIAL";
        string avatarID = "AV0001";
        string titleID = "TT0000";

        switch (mode)
        {
            case GameMode.Tutorial_1:
            {
                deckID = "DECK_TUTOR_AI_1_NEW";
                botID = "BOT_TUTORIAL_1";
                avatarID = "AV0002"; // Female Journalist
                titleID = "TT0000"; // Novice
            }
            break;

            case GameMode.Tutorial_2:
            {
                deckID = "DECK_TUTOR_AI_2_NEW";
                botID = "BOT_TUTORIAL_2";
                avatarID = "AV0001"; // Male Journalist
                titleID = "TT0000"; // Novice
            }
            break;

            case GameMode.Tutorial_3:
            {
                deckID = "DECK_TUTOR_AI_3_NEW";
                botID = "BOT_TUTORIAL_3";
                avatarID = "AV0001"; // Male Journalist
                titleID = "TT0000"; // Novice
            }
            break;

            case GameMode.Tutorial_4:
            {
                deckID = "DECK_TUTOR_AI_4_NEW";
                botID = "BOT_TUTORIAL_4";
                avatarID = "AV0001"; // Male Journalist
                titleID = "TT0000"; // Novice
            }
            break;
        }

        DeckData deck = new DeckData();
        DeckDB.Instance.GetDeck(deckID, out deck);
        HeroData hero = HeroData.CreateHero(deck.HeroID, PlayerIndex.Two);

        // bot profile
        PlayerInfoData playerInfoData = PlayerInfoData.CreateBotData(
              botID
            , hero.Name
            , avatarID
            , titleID
        );

        MatchPlayerData matchPlayerData = new MatchPlayerData(
              playerInfoData
            , deck
        );

        return matchPlayerData;
    }
    
    public static void CreateAdventureMatchData(AdventureStageData stageData, out List<MatchPlayerData> matchPlayerDataList)
    {
        matchPlayerDataList = new List<MatchPlayerData>();

        //Player
        PlayerInfoData playerInfoData = DataManager.Instance.PlayerInfo;
        string deckID;
        string heroID;
        CardElementType elementType;
        HeroData hero;
        DeckData deckToUse = null;
        if (stageData.PlayerUseCustomDeck)
        {
            deckToUse = DataManager.Instance.GetCurrentDeck();
        }
        else //Use pre-made deck
        { 
            deckID = stageData.MatchPlayerDataConfigs[0].DeckID;
            heroID = stageData.MatchPlayerDataConfigs[0].HeroID;
            elementType = stageData.MatchPlayerDataConfigs[0].ElementType;
            hero = HeroData.CreateHero(heroID, PlayerIndex.One);
            if (stageData.MatchPlayerDataConfigs[0].DeckList != null)
            {
                CardListData cardListData = new CardListData();
                foreach (string cardID in stageData.MatchPlayerDataConfigs[0].DeckList)
                {
                    cardListData.AddCard(cardID, 1);
                }

                deckToUse = new DeckData(
                    deckID
                    , stageData.MatchPlayerDataConfigs[0].UsePlayerName ? playerInfoData.DisplayName : hero.Name
                    , elementType.ToString()
                    , heroID
                    , DataManager.Instance.DefaultCardBackIDList[0]
                    , DataManager.Instance.DefaultDockIDList[0]
                    , cardListData
                );
            }
        }
        MatchPlayerData matchPlayerData_Player = new MatchPlayerData(
            playerInfoData
            , deckToUse
        );
        matchPlayerDataList.Add(matchPlayerData_Player);

        //Enemy
        heroID = stageData.MatchPlayerDataConfigs[1].HeroID;
        hero = HeroData.CreateHero(heroID, PlayerIndex.Two);
        playerInfoData = PlayerInfoData.CreateBotData(
                "BOT_" + stageData.AdventureStageID.ToString()
            , LocalizationManager.Instance.GetText("HERO_NAME_" + heroID)
            , "AV0001" // TODO: get from stageData?
            , "TT0000" // TODO: get from stageData?
        );
        deckID = stageData.MatchPlayerDataConfigs[1].DeckID;
        elementType = stageData.MatchPlayerDataConfigs[1].ElementType;
        if (stageData.MatchPlayerDataConfigs[1].DeckList != null)
        {
            CardListData cardListData = new CardListData();
            foreach (string cardID in stageData.MatchPlayerDataConfigs[1].DeckList)
            {
                cardListData.AddCard(cardID, 1);
            }

            deckToUse = new DeckData(
                    deckID
                , playerInfoData.DisplayName
                , elementType.ToString()
                , heroID
                , DataManager.Instance.DefaultCardBackIDList[0]
                , DataManager.Instance.DefaultDockIDList[0]
                , cardListData
            );
        }
        MatchPlayerData matchPlayerData_Enemy = new MatchPlayerData(
                playerInfoData
            , deckToUse
        );
        matchPlayerDataList.Add(matchPlayerData_Enemy);
    }

    public static MatchPlayerData CreateDebugMatchPlayerData()
    {
        MatchPlayerData matchPlayerData = new MatchPlayerData(
              DataManager.Instance.PlayerInfo
            , DeckData.GetDebugDeck("deck_debug")
        );

        return matchPlayerData;
    }
    #endregion
}

public class MatchProperty
{
    #region Public Properties
    public GameMode GameMode { get; private set; }
    public int PlayerCount { get; private set; }
    public bool IsNetwork { get; private set; }
    public bool IsHaveTimer { get; private set; }
    public bool IsVSBot { get; private set; }
    public BotType BotType { get { return BotTypeList[0]; } }
    public BotLevel BotLevel { get { return BotLevelList[0]; } }
    public List<BotType> BotTypeList { get; private set; }
    public List<BotLevel> BotLevelList { get; private set; }
    public AdventureStageData AdventureStageData { get; private set; }
    #endregion

    #region Private Properties
    private StringBuilder _sb;
    #endregion

    public MatchProperty()
    {
        GameMode = GameMode.Casual;
        PlayerCount = 0;
        IsNetwork = false;
        IsHaveTimer = false;
        IsVSBot = false;
        BotTypeList = new List<BotType>() { BotType.None };
        BotLevelList = new List<BotLevel>() { BotLevel.None };
        AdventureStageData = null;
    }

    public MatchProperty(
          GameMode gameMode
        , int playerCount
        , bool isNetwork
        , bool isHaveTimer
        , bool isVSBot
        , BotType botType
        , BotLevel botLevel
        , AdventureStageData adventureStageData
    )
    {
        GameMode = gameMode;
        PlayerCount = playerCount;
        IsNetwork = isNetwork;
        IsHaveTimer = isHaveTimer;
        IsVSBot = isVSBot;
        BotTypeList = new List<BotType>() { botType };
        BotLevelList = new List<BotLevel>() { botLevel };
        AdventureStageData = adventureStageData;
    }

    public MatchProperty(
          GameMode gameMode
        , int playerCount
        , bool isNetwork
        , bool isHaveTimer
        , bool isVSBot
        , List<BotType> botTypeList
        , List<BotLevel> botLevelList
        , AdventureStageData adventureStageData
    )
    {
        GameMode = gameMode;
        PlayerCount = playerCount;
        IsNetwork = isNetwork;
        IsHaveTimer = isHaveTimer;
        IsVSBot = isVSBot;
        BotTypeList = new List<BotType>(botTypeList);
        BotLevelList = new List<BotLevel>(botLevelList);
        AdventureStageData = adventureStageData;
    }

    public MatchProperty(MatchProperty matchProperty)
    {
        GameMode = matchProperty.GameMode;
        PlayerCount = matchProperty.PlayerCount;
        IsNetwork = matchProperty.IsNetwork;
        IsHaveTimer = matchProperty.IsHaveTimer;
        IsVSBot = matchProperty.IsVSBot;
        BotTypeList = new List<BotType>(matchProperty.BotTypeList);
        BotLevelList = new List<BotLevel>(matchProperty.BotLevelList);
        AdventureStageData = matchProperty.AdventureStageData;
    }

    public string GetDebugText()
    {
        if (_sb == null) _sb = new StringBuilder();
        _sb.Clear();

        _sb.AppendFormat("GameMode : {0}", GameMode);
        if (AdventureStageData != null)
        {
            _sb.AppendFormat("\nAdventureChapter : {0}", AdventureStageData.AdventureChapterID);
            _sb.AppendFormat("\nAdventureStage : {0}", AdventureStageData.AdventureStageID);
        }
        _sb.AppendFormat("\nPlayerCount : {0}", PlayerCount);
        _sb.AppendFormat("\nIsNetwork : {0}", IsNetwork);
        _sb.AppendFormat("\nIsHaveTimer : {0}", IsHaveTimer);
        _sb.AppendFormat("\nIsVSBot : {0}", IsVSBot);
        _sb.AppendFormat("\nBotType: {0}", BotType.ToString());
        _sb.AppendFormat("\nBotLevel: {0}", BotLevel.ToString());

        return _sb.ToString();
    }
}

public class MatchData
{
    #region Public Properties
    public GameMode GameMode { get { return Property.GameMode; } }
    public List<MatchPlayerData> PlayerData { get; private set; }

    public MatchProperty Property { get { return _property; } }
    #endregion

    #region Private Properties
    private MatchProperty _property;
    private StringBuilder _sb;
    #endregion

    public MatchData(MatchProperty property, List<MatchPlayerData> matchPlayerDataList)
    {
        if (property == null)
        {
            _property = new MatchProperty();
        }
        else
        {
            _property = new MatchProperty(property);
        }
        PlayerData = new List<MatchPlayerData>(matchPlayerDataList);
    }

    // Copy Constructor
    public MatchData(MatchData matchData)
    {
        _property = new MatchProperty(matchData.Property);
        PlayerData = new List<MatchPlayerData>(matchData.PlayerData);
    }

    public string GetDebugText()
    {
        if (_sb == null) _sb = new StringBuilder();
        _sb.Clear();

        _sb.AppendFormat("Property:\n{0}", _property.GetDebugText());

        return _sb.ToString();
    }

    public static bool CreateMatchContainer(
          MatchProperty matchProperty
        , List<MatchPlayerData> matchPlayerDataList
        , out MatchDataContainer container
    )
    {
        MatchData data = new MatchData(matchProperty, matchPlayerDataList);
        return CreateMatchContainer(data, out container);
    }

    public static bool CreateMatchContainer(MatchData matchData, out MatchDataContainer container)
    {
        if (matchData != null)
        {
            GameObject obj = new GameObject();
            container = obj.AddComponent<MatchDataContainer>();
            container.SetData(matchData);
            obj.name = "MatchDataContainer";

            return true;
        }

        container = null;
        return false;
    }
}
