﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

public class DailyLoginData
{
    public int TotalDays { get; private set; }
    public int AccumulativeLogin { get; private set; }
    public DateTime LastTimestamp { get; private set; }
    public DateTime Timestamp { get; private set; }
    public List<string> LoginLogs { get; private set; }

    private DailyLoginRewardData _rewardData;

    public DailyLoginData(DailyLoginRewardData rewardData, string json)
    {
        this._rewardData = rewardData;

        Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
        TotalDays = rawData.ContainsKey("days") ? int.Parse(rawData["days"].ToString()) : 1;
        AccumulativeLogin = rawData.ContainsKey("accumulative_login") ? int.Parse(rawData["accumulative_login"].ToString()) : 0;

        LastTimestamp = rawData.ContainsKey("last_timestamp") ? Convert.ToDateTime(rawData["last_timestamp"].ToString()) : DateTime.UtcNow;
        Timestamp = rawData.ContainsKey("timestamp") ? Convert.ToDateTime(rawData["timestamp"].ToString()) : DateTime.UtcNow;

        LoginLogs = rawData.ContainsKey("timestamp_log") ? JsonConvert.DeserializeObject<List<string>>(rawData["timestamp_log"].ToString()) : new List<string>();
    }

    public DailyLoginRewardData GetDailyLoginRewardData()
    {
        return _rewardData;
    }
}

[Serializable]
public class DailyLoginRewardData
{
    public bool isNewDay;
    public DateTime timestamp;
    public int rewardIndex;
    public DailyLoginRewardItem rewardItem;
    public DailyLoginRewardItem[] rewardTable;

    public string GetDebugText()
    {
        return JsonConvert.SerializeObject(this);
    }
}

[Serializable]
public class DailyLoginRewardItem
{
    public int day;
    public string itemID;
    public int amount;
}