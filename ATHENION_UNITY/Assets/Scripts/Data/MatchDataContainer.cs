﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatchDataContainer : MonoBehaviour 
{
    public MatchData MatchData { get; private set; }

    public void SetData(MatchData matchData)
    {
        MatchData = matchData;
    }
}
