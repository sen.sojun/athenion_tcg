﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class StarCubeItemData : ItemData
{
    public string NewID { get; private set; }
    public CardRarity Rarity { get; private set; }
    public bool IsDuplicate { get { return (!string.IsNullOrEmpty(NewID)); } }

    public StarCubeItemData(string itemID, int amount, string newID, CardRarity rarity) : base(itemID, amount)
    {
        NewID = newID;
        Rarity = rarity;
    }

    public override string GetDebugText()
    {
        return JsonConvert.SerializeObject(this);
    }
}

public class StarCubeResultData
{
    #region Properties
    public int CubeIndex { get; private set; }
    public DateTime LastOpenTimestamp { get; private set; }
    public int Price { get; private set; }
    public List<StarCubeItemData> ItemList { get; private set; }
    #endregion

    #region Constructors
    private StarCubeResultData()
    {
        CubeIndex = 0;
        Price = 0;
        ItemList = new List<StarCubeItemData>();
    }

    public StarCubeResultData(StarCubeResultData data)
    {
        CubeIndex = data.CubeIndex;
        LastOpenTimestamp = data.LastOpenTimestamp;
        Price = data.Price;
        ItemList = new List<StarCubeItemData>(data.ItemList);
    }
    #endregion

    #region Methods
    public string GetDebugText()
    {
        string result = "";

        // Cube Index
        result += string.Format("[{0}]", CubeIndex);

        // Cube Index
        result += string.Format(" Price: {0}", Price);

        // Item List
        result += "\n[Item List]";
        int count = 0;
        foreach (StarCubeItemData item in ItemList)
        {
            if (result.Length > 0)
            {
                result += string.Format("\n[{0}] {1}", count, item.GetDebugText());
            }
            else
            {
                result += string.Format("[{0}] {1}", count, item.GetDebugText());
            }
            count++;
        }

        return result;
    }

    public static StarCubeResultData ConvertToStarCubeResultData(int cubeIndex, string jsonStr)
    {
        StarCubeResultData result = new StarCubeResultData();
        result.CubeIndex = cubeIndex;

        Dictionary<string, object> customData = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonStr);

        if (customData != null)
        {
            if (customData.ContainsKey("item_list"))
            {
                List<Dictionary<string, string>> data = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(customData["item_list"].ToString());

                foreach (Dictionary<string, string> item in data)
                {
                    if (
                           item.ContainsKey("item_id") 
                        && item.ContainsKey("amount")
                        && item.ContainsKey("new_id")
                        && item.ContainsKey("rarity")
                    )
                    {
                        string itemID = item["item_id"];
                        int amount = int.Parse(item["amount"]);
                        string newID = item["new_id"];

                        CardRarity rarity;
                        GameHelper.StrToEnum(item["rarity"], out rarity);

                        StarCubeItemData itemData = new StarCubeItemData(itemID, amount, newID, rarity);
                        result.ItemList.Add(itemData);
                    }
                }
            }

            if (customData.ContainsKey("last_open_timestamp"))
            {
                DateTime lastOpenTimestamp = (DateTime)customData["last_open_timestamp"];
                result.LastOpenTimestamp = lastOpenTimestamp;
            }

            if (customData.ContainsKey("price"))
            {
                int price = int.Parse(customData["price"].ToString());
                result.Price = price;
            }
        }

        return result;
    }
    #endregion
}
