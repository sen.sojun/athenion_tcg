﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LocalizeTextUI : MonoBehaviour 
{
    public string LocalizeKey = "";

	// Use this for initialization
	void Start () 
    {
	}

    private void OnEnable()
    {
        LocalizationManager.Instance.BindToggleLanguageEvent(this.OnLanguageToggle);

        UpdateText();
    }

    private void OnDisable()
    {
        LocalizationManager.Instance.UnbindToggleLanguageEvent(this.OnLanguageToggle);
    }

    private void OnDestroy()
    {
        LocalizationManager.Instance.UnbindToggleLanguageEvent(this.OnLanguageToggle);
    }

    private void UpdateText()
    {
        string text;
        if (LocalizationManager.Instance.GetText(LocalizeKey, out text))
        {
        }
        else
        {
            text = LocalizeKey;
        }

        Text unityText = GetComponent<Text>();
        if (unityText)
        {
            unityText.text = text;
        }

        TextMeshProUGUI textMeshProUGUIText = GetComponent<TextMeshProUGUI>();
        if (textMeshProUGUIText)
        {
            textMeshProUGUIText.text = text;
        }

        TextMeshPro TextMeshProText = GetComponent<TextMeshPro>();
        if (TextMeshProText)
        {
            TextMeshProText.text = text;
        }
    }

    public void OnLanguageToggle()
    {
        UpdateText();
    }
}
