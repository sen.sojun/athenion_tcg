﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class KTPlayManager : MonoSingleton<KTPlayManager>
{
    #region Enums
    public enum KTPlayGender : int
    {
          NoChange = 0
        , Male = 1
        , Female = 2
        , Confidential = 3
    }
    #endregion

    #region Properties
    private bool isInitKTPlay = false;
    #endregion

    #region Override Methods
    protected override void InitSelf()
    {        
        base.InitSelf();

        InitKTPlay();
    }

    protected override void OnDestroy()
    {
        DeInitKTPlay();

        base.OnDestroy();
    }

    private void OnApplicationPause(bool pause)
    {
        /*
        if (pause)
        {
            // pause
            DeInitKTPlay();
        }
        else
        {
            // resume
            InitKTPlay();
        }
        */
    }
    #endregion

    #region Methods
    private void InitKTPlay()
    {
        if (!isInitKTPlay)
        {
            #if (!UNITY_EDITOR && (UNITY_ANDROID || UNITY_IOS))
            // Subscribe KTPlay event.
            KTPlay.SetAvailabilityChangedDelegate(OnKTPlayAvailabilityChanged);
            KTPlay.SetActivityStatusChangedDelegate(OnKTPlayActivityStatusChanged);
            KTPlay.SetViewDidAppearDelegate(OnKTPlayAppear);
            KTPlay.SetViewDidDisappearDelegate(OnKTPlayDisappear);
            KTPlay.SetSoundStartDelegate(OnKTPlaySoundStart);
            KTPlay.SetSoundStartDelegate(OnKTPlaySoundStop);
            #endif

            isInitKTPlay = true;
        }
    }

    private void DeInitKTPlay()
    {
        if (isInitKTPlay)
        {
            isInitKTPlay = false;
        }
    }

    public void ShowCommunity()
    {
        if (DataManager.Instance.IsFeatureEnable(FeatureUnlock.Community))
        { 
            #if (!UNITY_EDITOR && (UNITY_ANDROID || UNITY_IOS))
            if (KTPlay.IsEnabled())
            {
                StartLogin(
                    delegate()
                    {
                        KTPlay.Show();
                    }
                );
            }
            else
            #endif
            {
                Debug.LogWarning("KTPlay is not ready.");
            }
        }
    }

    public void StartLogin(UnityAction onComplete)
    {
        #if (!UNITY_EDITOR && (UNITY_ANDROID || UNITY_IOS))
        if (!KTAccountManager.isLoggedIn())
        {
            // Auto-Login
            LoginWithGameUser(
                  DataManager.Instance.PlayerInfo.PlayerID
                , delegate (KTUser loginUser)
                {
                    // Login Success
                    Debug.LogFormat("[KTPLAY] Login success : {0}", loginUser.userId);

                    UpdateProfile(
                          DataManager.Instance.PlayerInfo.DisplayName
                        , null
                        , KTPlayGender.NoChange
                        , delegate (KTUser updateProfileUser)
                        {
                            // Update Success
                            Debug.LogFormat("[KTPLAY] Update profile success : {0}", updateProfileUser.userId);
                            onComplete?.Invoke();
                        }
                        , delegate (KTError error)
                        {
                            Debug.LogErrorFormat("[ERROR] Update profile fail : {0}", error.description);
                            onComplete?.Invoke();
                        }
                    );
                }
                , delegate (KTError error)
                {
                    Debug.LogErrorFormat("[ERROR] Login fail : {0}", error.description);

                    onComplete?.Invoke();
                }
            );
        }
        else
        {
            onComplete?.Invoke();
        }
        #else
        onComplete?.Invoke();
        #endif
    }

    public bool IsLoggedIn()
    {
        #if (!UNITY_EDITOR && (UNITY_ANDROID || UNITY_IOS))
        return KTAccountManager.isLoggedIn();
        #else
        return true;
        #endif
    }

    private void ShowLoading()
    {
        if (NavigatorController.Instance.CurrentPage.SceneName == GameHelper.HomeSceneName)
        {
            PopupUIManager.Instance.Show_SoftLoad(true);
        }
    }

    private void HideLoading()
    {
        if (NavigatorController.Instance.CurrentPage.SceneName == GameHelper.HomeSceneName)
        {
            PopupUIManager.Instance.Show_SoftLoad(false);
        }
    }
    #endregion

    #region Event Methods
    private void OnKTPlayAvailabilityChanged(bool isEnabled)
    {
    }

    private void OnKTPlayActivityStatusChanged(bool hasActivityStatus)
    {
    }

    private void OnKTPlayAppear()
    {
    }

    private void OnKTPlayDisappear()
    {
    }

    private void OnKTPlaySoundStart()
    {
        // Mute all other sounds.
        SoundManager.SetBGMOn(false);
        SoundManager.SetVoiceOn(false);
        SoundManager.SetSFXOn(false);
    }

    private void OnKTPlaySoundStop()
    {
        // Resume all other sounds.
        SoundManager.SetBGMOn(true);
        SoundManager.SetVoiceOn(true);
        SoundManager.SetSFXOn(true);
    }
    #endregion

    #region KTPlay Methods
    public void LoginWithGameUser(string playerID, UnityAction<KTUser> onComplete, UnityAction<KTError> onFail)
    {
        #if (!UNITY_EDITOR && (UNITY_ANDROID || UNITY_IOS))
        KTAccountManager.LoginWithGameUser(
              playerID
            , delegate (KTUser user, KTError error)
            {
                if (error == null)
                {
                    //success
                    onComplete?.Invoke(user);
                }
                else
                {
                    //failed
                    onFail?.Invoke(error);
                }
            }
        );
        #else
        Debug.LogWarningFormat("LoginWithGameUser: [Error] KTPlay not available in this platform.");
        #endif
    }

    public void Logout()
    {
        #if (!UNITY_EDITOR && (UNITY_ANDROID || UNITY_IOS))
        if (KTAccountManager.isLoggedIn())
        {
            KTAccountManager.Logout();
        }
        #endif
    }

    public void UpdateProfile(string nickName, string avatarFilePath, KTPlayGender gender, UnityAction<KTUser> onComplete, UnityAction<KTError> onFail)
    {
        #if (!UNITY_EDITOR && (UNITY_ANDROID || UNITY_IOS))
        KTAccountManager.UpdateProfile(
              nickName
            , avatarFilePath
            , (int)gender
            , delegate (bool isSuccess, KTUser user, KTError error)
            {
                if (isSuccess)
                {
                    //success
                    onComplete?.Invoke(user);
                }
                else
                {
                    //failed
                    onFail?.Invoke(error);
                }
            }
        );
        #else
        Debug.LogWarningFormat("UpdateProfile: [Error] KTPlay not available in this platform.");
        #endif
    }

    public void ShowChatWithPlayerID(string playerId)
    {
        string deeplinkSchema = string.Format("ktplay://chat/message?game_user_id={0}", playerId);
        Debug.LogFormat("ShowChatWithPlayerID: deeplink:{0}", deeplinkSchema);
        ShowDeepLink(deeplinkSchema);
    }

    public void ShowDeepLink(string deeplink)
    {
        if (DataManager.Instance.IsFeatureEnable(FeatureUnlock.Community))
        {
            Debug.LogFormat("ShowDeepLink: deeplink:{0}", deeplink);

            #if (!UNITY_EDITOR && (UNITY_ANDROID || UNITY_IOS))
            if (!KTAccountManager.isLoggedIn())
            {
                StartLogin(
                    delegate ()
                    {
                        KTPlay.OpenDeepLink(deeplink);
                    }
                );
            }
            else
            {
                KTPlay.OpenDeepLink(deeplink);
            }
            #else
            Debug.LogWarningFormat("ShowDeepLink: [Error] KTPlay not available in this platform.\ndeeplink:{0}", deeplink);
            #endif
        }
    }
    #endregion
}
