﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Newtonsoft.Json;
using System.Linq;
using System;
using System.IO;
using PlayFab;
#if UNITY_EDITOR && !UNITY_IOS
using PlayFab.AdminModels;
#endif

public class PlayFabAdminManager : MonoBehaviour
{
#if UNITY_EDITOR && !UNITY_IOS

    public enum PlayerDataType { PlayerData = 0, ReadOnlyData = 1, InternalData = 2 }

    #region Public Properties

    public List<string> RemoveDataKeyList = new List<string>();
    public List<string> RemoveReadOnlyDataKeyList = new List<string>();
    public List<string> RemoveInternalDataKeyList = new List<string>();

    #endregion

    #region Private Properties
    private readonly uint _maxBatchPlayerSegment = 10000;
    private readonly string _playerSegmentIDDev = "3F47694AE180D44F"; // ALL PLAYERS dev
    private readonly string _playerSegmentIDPro = "27A8C20F51DAFC"; // ALL PLAYERS pro
    private string _continuationToken = string.Empty;

    private List<PlayerProfile> _playerProfileList = new List<PlayerProfile>();
    private List<Dictionary<string, string>> _csvTable = new List<Dictionary<string, string>>();
    private List<Dictionary<string, string>> _exportedData = new List<Dictionary<string, string>>();

    private int _maxProgressCount;
    private int _successProgressCount;
    private int _errorProgressCount;
    #endregion

    private void Start()
    {

    }

    private void Update()
    {
        UpdateServerName();
    }

    #region General
    [Obsolete("Wait for Refactor code.")]
    public void PrepareProgress()
    {
        PlayFabAdminUIManager.Instance.RequestPrepareProgress();
    }

    [Obsolete("Wait for Refactor code.")]
    public void StartProgress(int maxProgress)
    {
        _maxProgressCount = maxProgress;
        _successProgressCount = 0;
        _errorProgressCount = 0;
        PlayFabAdminUIManager.Instance.RequestStartProgress(maxProgress);
    }

    [Obsolete("Wait for Refactor code.")]
    public void UpdateProgress(int current, int max)
    {
        PlayFabAdminUIManager.Instance.UpdateProgress(current, max);
    }

    [Obsolete("Wait for Refactor code.")]
    public void UpdateProgress(float ratio)
    {
        PlayFabAdminUIManager.Instance.UpdateProgress(ratio);
    }

    [Obsolete("Wait for Refactor code.")]
    private void UpdateProgress(string text)
    {
        PlayFabAdminUIManager.Instance.UpdateProgress(text);
    }

    [Obsolete("Wait for Refactor code.")]
    public bool CountProgressToComplete(bool isSuccess)
    {
        if (isSuccess)
        {
            _successProgressCount++;
            if (_successProgressCount == _maxProgressCount)
            {
                return true;
            }
        }
        else
        {
            _errorProgressCount++;
        }

        PlayFabAdminUIManager.Instance.RequestCountProgress(isSuccess);
        return false;
    }

    private void UpdateServerName()
    {
        string serverName = "";

        if (PlayFabSettings.TitleId == GameHelper.PlayFabTitleID_Development)
        {
            serverName = "Development";
        }
        else if (PlayFabSettings.TitleId == GameHelper.PlayFabTitleID_Production)
        {
            serverName = "Production";
        }
        else
        {
            serverName = "Unknown";
        }

        string result = string.Format(
              "<b>{0} <color=orange>({1})</color></b>"
            , serverName
            , PlayFabSettings.TitleId
        );

        PlayFabAdminUIManager.Instance.UpdateServerText(result);
    }
    #endregion

    #region PlayFab Methods

    #region Player Segment
    private void GetPlayerInSegment(UnityAction<GetPlayersInSegmentResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        string playerSegmentID;
        if (PlayFabSettings.TitleId == "DC17E")
        {
            playerSegmentID = _playerSegmentIDDev;
        }
        else
        {
            playerSegmentID = _playerSegmentIDPro;
        }

        PlayFabAdminAPI.GetPlayersInSegment(new GetPlayersInSegmentRequest()
        {
            SegmentId = playerSegmentID,
            MaxBatchSize = _maxBatchPlayerSegment,
            ContinuationToken = _continuationToken
        }
            , (result) =>
            {
                _continuationToken = result.ContinuationToken;
                if (onComplete != null)
                {
                    onComplete.Invoke(result);
                }
            }
            , (error) =>
            {
                if (onFail != null)
                {
                    onFail.Invoke(error);
                }
            }
        );
    }
    #endregion

    #region Title Data
    private void GetAllTitleData(UnityAction<GetTitleDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        GetTitleData(new List<string>(), onComplete, onFail);
    }

    private void GetTitleData(string key, UnityAction<GetTitleDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        GetTitleData(new List<string>() { key }, onComplete, onFail);
    }

    private void GetTitleData(List<string> keyList, UnityAction<GetTitleDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        List<string> keys = keyList;
        if (keyList.Count <= 0)
        {
            keys = null;
        }

        PlayFabAdminAPI.GetTitleData(
            new GetTitleDataRequest()
            {
                Keys = keys,
            }
            , (result) =>
            {
                if (onComplete != null)
                {
                    onComplete.Invoke(result);
                }
            }
            , (error) =>
            {
                if (onFail != null)
                {
                    onFail.Invoke(error);
                }
            }
        );
    }
    #endregion

    #region Player Data
    private void GetAllPlayerData(string playerID, UnityAction<GetUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        GetPlayerData(playerID, new List<string>(), onComplete, onFail);
    }

    private void GetPlayerData(string playerID, string key, UnityAction<GetUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        GetPlayerData(playerID, new List<string>() { key }, onComplete, onFail);
    }

    private void GetPlayerData(string playerID, List<string> keyList, UnityAction<GetUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        List<string> keys = keyList;
        if (keyList.Count <= 0)
        {
            keys = null;
        }

        PlayFabAdminAPI.GetUserData(
            new GetUserDataRequest()
            {
                PlayFabId = playerID,
                Keys = keys,
            }
            , (result) =>
            {
                if (onComplete != null)
                {
                    onComplete.Invoke(result);
                }
            }
            , (error) =>
            {
                if (onFail != null)
                {
                    onFail.Invoke(error);
                }
            }
        );
    }

    private void UpdatePlayerData(string playerID, string key, string value, UnityAction<UpdateUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        Dictionary<string, string> data = new Dictionary<string, string>();
        data.Add(key, value);

        UpdatePlayerData(playerID, data, onComplete, onFail);
    }

    private void UpdatePlayerData(string playerID, Dictionary<string, string> data, UnityAction<UpdateUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabAdminAPI.UpdateUserData(
            new UpdateUserDataRequest()
            {
                PlayFabId = playerID,
                Data = data
            }
            , (result) =>
            {
                if (onComplete != null)
                {
                    onComplete.Invoke(result);
                }
            }
            , (error) =>
            {
                if (onFail != null)
                {
                    onFail.Invoke(error);
                }
            }
        );
    }

    private void RemovePlayerData(string playerID, List<string> keysToRemove, UnityAction<UpdateUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabAdminAPI.UpdateUserData(
            new UpdateUserDataRequest()
            {
                PlayFabId = playerID,
                KeysToRemove = keysToRemove,
            }
            , (result) =>
            {
                if (onComplete != null)
                {
                    onComplete.Invoke(result);
                }
            }
            , (error) =>
            {
                if (onFail != null)
                {
                    onFail.Invoke(error);
                }
            }
        );
    }
    #endregion

    #region Player ReadOnly Data
    private void GetAllPlayerReadOnlyData(string playerID, UnityAction<GetUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        GetPlayerReadOnlyData(playerID, new List<string>(), onComplete, onFail);
    }

    private void GetPlayerReadOnlyData(string playerID, string key, UnityAction<GetUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        GetPlayerReadOnlyData(playerID, new List<string>() { key }, onComplete, onFail);
    }

    private void GetPlayerReadOnlyData(string playerID, List<string> keyList, UnityAction<GetUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        List<string> keys = keyList;
        if (keyList.Count <= 0)
        {
            keys = null;
        }

        PlayFabAdminAPI.GetUserReadOnlyData(
            new GetUserDataRequest()
            {
                PlayFabId = playerID,
                Keys = keys,
            }
            , (result) =>
            {
                if (onComplete != null)
                {
                    onComplete.Invoke(result);
                }
            }
            , (error) =>
            {
                if (onFail != null)
                {
                    onFail.Invoke(error);
                }
            }
        );
    }

    private void UpdatePlayerReadOnlyData(string playerID, string key, string value, UnityAction<UpdateUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        Dictionary<string, string> data = new Dictionary<string, string>();
        data.Add(key, value);

        UpdatePlayerReadOnlyData(playerID, data, onComplete, onFail);
    }

    private void UpdatePlayerReadOnlyData(string playerID, Dictionary<string, string> data, UnityAction<UpdateUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabAdminAPI.UpdateUserReadOnlyData(
            new UpdateUserDataRequest()
            {
                PlayFabId = playerID,
                Data = data
            }
            , (result) =>
            {
                if (onComplete != null)
                {
                    onComplete.Invoke(result);
                }
            }
            , (error) =>
            {
                if (onFail != null)
                {
                    onFail.Invoke(error);
                }
            }
        );
    }

    private void RemovePlayerReadOnlyData(string playerID, List<string> keysToRemove, UnityAction<UpdateUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabAdminAPI.UpdateUserReadOnlyData(
            new UpdateUserDataRequest()
            {
                PlayFabId = playerID,
                KeysToRemove = keysToRemove,
            }
            , (result) =>
            {
                if (onComplete != null)
                {
                    onComplete.Invoke(result);
                }
            }
            , (error) =>
            {
                if (onFail != null)
                {
                    onFail.Invoke(error);
                }
            }
        );
    }
    #endregion

    #region Player Internal Data
    private void GetAllPlayerInternalData(string playerID, UnityAction<GetUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        GetPlayerInternalData(playerID, new List<string>(), onComplete, onFail);
    }

    private void GetPlayerInternalData(string playerID, string key, UnityAction<GetUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        GetPlayerInternalData(playerID, new List<string>() { key }, onComplete, onFail);
    }

    private void GetPlayerInternalData(string playerID, List<string> keyList, UnityAction<GetUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        List<string> keys = keyList;
        if (keyList.Count <= 0)
        {
            keys = null;
        }

        PlayFabAdminAPI.GetUserInternalData(
            new GetUserDataRequest()
            {
                PlayFabId = playerID,
                Keys = keys,
            }
            , (result) =>
            {
                if (onComplete != null)
                {
                    onComplete.Invoke(result);
                }
            }
            , (error) =>
            {
                if (onFail != null)
                {
                    onFail.Invoke(error);
                }
            }
        );
    }

    private void UpdatePlayerInternalData(string playerID, string key, string value, UnityAction<UpdateUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        Dictionary<string, string> data = new Dictionary<string, string>();
        data.Add(key, value);

        UpdatePlayerInternalData(playerID, data, onComplete, onFail);
    }

    private void UpdatePlayerInternalData(string playerID, Dictionary<string, string> data, UnityAction<UpdateUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabAdminAPI.UpdateUserInternalData(
            new UpdateUserInternalDataRequest()
            {
                PlayFabId = playerID,
                Data = data
            }
            , (result) =>
            {
                if (onComplete != null)
                {
                    onComplete.Invoke(result);
                }
            }
            , (error) =>
            {
                if (onFail != null)
                {
                    onFail.Invoke(error);
                }
            }
        );
    }

    private void RemovePlayerInternalData(string playerID, List<string> keysToRemove, UnityAction<UpdateUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabAdminAPI.UpdateUserInternalData(
            new UpdateUserInternalDataRequest()
            {
                PlayFabId = playerID,
                KeysToRemove = keysToRemove,
            }
            , (result) =>
            {
                if (onComplete != null)
                {
                    onComplete.Invoke(result);
                }
            }
            , (error) =>
            {
                if (onFail != null)
                {
                    onFail.Invoke(error);
                }
            }
        );
    }
    #endregion

    #region Player Inventory
    private void AddPlayerVirutalCurrency(string playerID, string vcType, int value, UnityAction onComplete, UnityAction<PlayFabError> onFail)
    {
        if (value > 0)
        {
            PlayFabAdminAPI.AddUserVirtualCurrency(
                new AddUserVirtualCurrencyRequest()
                {
                    PlayFabId = playerID,
                    VirtualCurrency = vcType,
                    Amount = value
                }
                , (result) =>
                {
                    onComplete?.Invoke();
                }
                , (error) =>
                {
                    onFail?.Invoke(error);
                }
            );
        }
        else if (value < 0)
        {
            PlayFabAdminAPI.SubtractUserVirtualCurrency(
                new SubtractUserVirtualCurrencyRequest()
                {
                    PlayFabId = playerID,
                    VirtualCurrency = vcType,
                    Amount = -value
                }
                , (result) =>
                {
                    onComplete?.Invoke();
                }
                , (error) =>
                {
                    onFail?.Invoke(error);
                }
            );
        }
        else
        {
            onComplete?.Invoke();
        }
    }

    private void RevokePlayerInventoryItem(string playerID, string itemInstanceID, UnityAction<RevokeInventoryItemsResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        RevokePlayerInventoryItem(playerID, new List<string>() { itemInstanceID }, onComplete, onFail);
    }

    private void RevokePlayerInventoryItem(string playerID, List<string> itemInstanceIDList, UnityAction<RevokeInventoryItemsResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        List<RevokeInventoryItem> itemList = new List<RevokeInventoryItem>();
        foreach (string id in itemInstanceIDList)
        {
            itemList.Add(new RevokeInventoryItem()
            {
                PlayFabId = playerID,
                ItemInstanceId = id
            }
            );
        }

        PlayFabAdminAPI.RevokeInventoryItems(
            new RevokeInventoryItemsRequest()
            {
                Items = itemList
            }
            , (result) =>
            {
                if (onComplete != null)
                {
                    onComplete.Invoke(result);
                }
            }
            , (error) =>
            {
                if (onFail != null)
                {
                    onFail.Invoke(error);
                }
            }
        );
    }

    private void GrantPlayerInventoryItem(string playerID, string itemID, UnityAction<GrantItemsToUsersResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        GrantPlayerInventoryItem(playerID, new List<string>() { itemID }, onComplete, onFail);
    }

    private void GrantPlayerInventoryItem(string playerID, List<string> itemIDList, UnityAction<GrantItemsToUsersResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        List<ItemGrant> itemList = new List<ItemGrant>();
        foreach (string item in itemIDList)
        {
            itemList.Add(
                new ItemGrant()
                {
                    PlayFabId = playerID,
                    ItemId = item
                }
            );
        }

        PlayFabAdminAPI.GrantItemsToUsers(
            new GrantItemsToUsersRequest()
            {
                CatalogVersion = PlayFabManager.CatalogMainID,
                ItemGrants = itemList
            }
            , (result) =>
            {
                if (onComplete != null)
                {
                    onComplete.Invoke(result);
                }
            }
            , (error) =>
            {
                if (onFail != null)
                {
                    onFail.Invoke(error);
                }
            }
        );
    }

    private void GetPlayerInventoryData(string playerID, UnityAction<GetUserInventoryResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabAdminAPI.GetUserInventory(
            new GetUserInventoryRequest()
            {
                PlayFabId = playerID
            }
            , (result) =>
            {
                if (onComplete != null)
                {
                    onComplete.Invoke(result);
                }
            }
            , (error) =>
            {
                if (onFail != null)
                {
                    onFail.Invoke(error);
                }
            }
        );
    }

    public IEnumerator IEGetPlayerInventoryData(string playerID, UnityAction<GetUserInventoryResult> onComplete, UnityAction<string> onFail = null)
    {
        bool isFinish = false;

        GetPlayerInventoryData(playerID, (result) =>
        {
            isFinish = true;
            onComplete?.Invoke(result);
        }, (error) =>
        {
            onFail?.Invoke(error.ToString());
            isFinish = true;
        });
        yield return new WaitUntil(() => isFinish == true);
    }
    #endregion

    #endregion

    #region Export All PlayerData
    public void ExportAllPlayerDataToCSV()
    {
        PrepareProgress();
        List<PlayerProfile> playerProfileList = new List<PlayerProfile>();
        GetPlayerInSegment(
            (result) =>
            {
                playerProfileList = result.PlayerProfiles;
                StartProgress(playerProfileList.Count * 3);

                Dictionary<string, string> data = new Dictionary<string, string>();
                int index = 0;
                foreach (PlayerProfile player in playerProfileList)
                {
                    string email = "NONE";
                    if (player.ContactEmailAddresses != null && player.ContactEmailAddresses.Count > 0)
                    {
                        email = player.ContactEmailAddresses[0].EmailAddress;
                    }

                    string displayName = "NONE";
                    if (player.DisplayName != string.Empty)
                    {
                        displayName = player.DisplayName;
                    }

                    _exportedData.Add(
                        new Dictionary<string, string>()
                        {
                                { "PlayerID", player.PlayerId }
                                , { "DisplayName", displayName }
                                , { "Email", email}
                        }
                    );

                    int playerIndex = index;
                    GetAllPlayerData(player.PlayerId
                        , (result1) =>
                        {
                            foreach (KeyValuePair<string, UserDataRecord> userData in result1.Data)
                            {
                                _exportedData[playerIndex].Add(userData.Key, userData.Value.Value);
                            }
                            OnExportAllPlayerDataToCSVComplete(true);
                        }
                        , (error1) =>
                        {
                            OnExportAllPlayerDataToCSVComplete(true);
                        }
                    );

                    GetAllPlayerReadOnlyData(player.PlayerId
                        , (result2) =>
                        {
                            foreach (KeyValuePair<string, UserDataRecord> userData in result2.Data)
                            {
                                _exportedData[playerIndex].Add(userData.Key, userData.Value.Value);
                            }
                            OnExportAllPlayerDataToCSVComplete(true);
                        }
                        , (error2) =>
                        {
                            OnExportAllPlayerDataToCSVComplete(true);
                        }
                    );

                    GetAllPlayerInternalData(player.PlayerId
                        , (result3) =>
                        {
                            foreach (KeyValuePair<string, UserDataRecord> userData in result3.Data)
                            {
                                _exportedData[playerIndex].Add(userData.Key, userData.Value.Value);
                            }
                            OnExportAllPlayerDataToCSVComplete(true);
                        }
                        , (error3) =>
                        {
                            OnExportAllPlayerDataToCSVComplete(true);
                        }
                    );

                    index++;
                }
            }
            , (error) =>
            {
                OnExportAllPlayerDataToCSVComplete(false);
            }
        );
    }

    private void OnExportAllPlayerDataToCSVComplete(bool isSuccess)
    {
        if (CountProgressToComplete(isSuccess))
        {
            CSVWriter.WriteCSV("EXPORTED_PLAYER_DATA", _exportedData);
            _exportedData.Clear();
        }
    }
    #endregion

    #region Change Item in Player

    public void ChangeItemDurationInPlayer()
    {
        if (DateTime.Now < Convert.ToDateTime("2020-05-25T02:00:00.000Z"))
        {
            Debug.Log("Activate when time > " + Convert.ToDateTime("2020-05-25T02:00:00.000Z").ToString());
            return;
        }
        return;
        List<string> playerIDList = new List<string>() { "5B1AAEC6C2F40B88", "7D152AF39E80E56B", "5FECE91B06927FF3", "9AB94D87370780DF", "3D974503551698D9", "693CAE14E6118666", "11FE532CE9E8F5A4", "E073F23EFE8FB393", "CE294309A3981E0B", "4F651D2A8B37EE54", "ED00F6515A04E9FD", "52115851FB5A3C01", "706FD0FD1BD43420", "1BDE992E284E6B65", "2D2201FF468F5B12", "829AFD740214DC93", "F7DC8F8AF25D09AE", "2CA377F479307969", "ADF44FE58276F07E", "8DAF5348849B9E7F", "ECCD526E092AADA2", "A221EDA10AE67563", "1395DC6E19F64AD4", "31CF30EA3AEC266A", "E09D72AC3D15C5DF", "50ED8E18564DF285", "D6FC82AAB335965F", "5823D01568151AF8", "AC52523608CAFF45", "AC4D31633B6923ED", "992D656A1273FB73", "D61C9A7FEAFE364", "2C4A080D981C2585", "D3236F4F1C01FAA8", "AD700F6BBDAADDA2", "D3FF04FF346DE93D", "1804564E7A461948", "451CB6DEDB91CE16", "E272267CF33AC0CA", "57D5A4941E7C71E", "705EEAA3CCC86DE1", "D31CD425BCA26285", "29EE673637ADD293", "5733E7576A633025", "959C68BCFB400F30", "9134195FFC2DF6BB", "A8B2FF727712830D", "1339D60685127117", "8FE3A4F4C8084921", "210F11ECEBB6F0A1", "EC1568E04657C9F3", "7003A108E0546CB1", "1D579F4E4A0660D3", "BBE919E154914A67", "1079831CAE4FE9A4", "94CE744AE628B019", "5E3D24DCC7CD45FA", "8E2FE394474D3896", "6DD4C3495D7D8344", "2E3D6481106CE27A", "7675B306840AF783", "89FA93CC94520A77", "40C8A926DEA2FB0A", "B43FB6BD2CA976E3", "44B9F21D2A61CB66", "6469C5A790F54938", "FFF881ACF5D523D1", "29E2B27EA0FD517E", "241FD24AD2F996B0", "C3C9398A45BA8BAB", "510CB831A83E81F6", "62918F284137D3D7", "6E72ADA2486AB90D", "7AB1A115E3C91ABA", "28561B036B071D63", "9C8EE9F9E70311E", "5D0B28885FE5279B", "1289740E82CE3C9E", "7ED6F8C88B96DCEE", "DAA3D0A7E5EFD5AC", "9EC6DE9F6B9B3E5F", "4EB3BF92A4970035", "28460CA61F663E05", "57052B8B3546B070", "4EA2EA84B6F7A1BC", "2B2FF5FAB2FC4FAD", "C3EBA2BF1B0F81C8", "68A8E31BFF8F4384", "99839CDAFD25DD05", "F15C8B7D99FEFB3D", "9DB49D10B747F5C1", "104D2866C34D3676", "C8CD16A8673F17E5", "1C7B473FB64F7B50", "12BDB336DB157513", "D91A69F57F352048", "E179813ACCAB021", "5CAA2E8A24DAC97B", "A4109760FDEE74D5", "6AE91FF0AE88657A", "68881632A30A8E93", "3AF13C4E93964EB", "AB894D367E9A876", "9683DD1BCF13A3FE", "A433ECBC68594775", "F7893A808D895A39", "2761CA2ADA49F71F", "4122C802AE57F524", "E7154FDD02B530EE", "B238645410A21262", "BE85ACDF9FDEB98E", "E7A4E74F5DED5385", "209264CDF4DC3167", "E258CB4D17899799", "AA8DC6BDD7F63F12", "470B11C46F099986", "6488BB36A71BA141", "33DE54788F3BAA02", "6283D285E8712181", "4F76ECD8F48108E4", "F4FF5376D3D9FFC4", "197B1EC142C3497C", "D837E0DB93F4A58", "2EBBAD4682D3F6B9", "7FDAF33498F05EA9", "D2029FD323C5E005", "8ACEAD5A8ED35F5", "D5374336EB3E19A2", "481DA6A6206759CD", "87CCF90C0C6425DC", "74A0F5268B7A155A", "2E4F486F4F80E4FF", "39C8333AC4A32663", "649E5624E21C774B", "3E0BB99B2CC91654", "76958DBC3AD7EF2C", "EEB1A8AC042163C2", "E868B65E16E3AE95", "929AC8DCD5E06430", "CFF998DC4117958", "AC5819619F84305D", "AA888B3A375F5371", "EDE5AAA75CF726B5", "4E476A88BE34A579", "E41C966500B223D", "E1C4866C248EB672", "475703A5EC1E07B3", "6CD913C0C6DC2E9C", "DF8E795E1C8CDA3A", "E29B90232E8A476C", "6137093051F29371", "BDDF9D294AC583FA", "1B9623226C0BA76F", "2D24C61C2DA72479", "2D05024BA59A2E20", "6018738ED55BFC86", "5793C40F2F0603AD", "7CEA17C50D679E81", "6920090321A38EAE", "7DF5DAD8A7A6ACF4", "9F16C9162EF9A86", "456D4C096A6B7311", "C052E3FB73827EF4", "B71E6C638BA229EB", "DE609B1E68BAEE24", "EFE8809410DCE24D", "E560114DA1B3779D", "15AD9E031B8A6DE9", "884395F118B45466", "90F8E97D39099B1C", "CEA67DDE9D40D074", "1B03DA65FFB6AC13", "1FEE957026545D75", "DFD6BE1D18694CF0", "AEB1BF6091FA0021", "8FD9C2EB712D2DA6", "18AC951CA679BF5E", "1C8F37BB26B7352D", "15C229C3B55C5C1E", "557FC748E4B53D32", "E0EE54BC8210FD72", "CB5C75DF654D76FB", "B5D125E9923BB766", "FB3A347EA9D7274E", "4330894796535C7B", "DFFB2E36A72432C6", "568E3735CD69AFC0", "7EF4749C7148DEE3", "3F5B0EDC46E11ECF", "E4CF59D2081421C3", "84BA5D6240616EC5", "703184E7FBC3D542", "29A673B098B8ABDF", "55FAC8B2751ED143", "433A5DED787C1784", "7DFF6EDB9205C374", "BBA135BCE14ABD80", "C6F0A0D2B2B7B1B5", "70F9F524B4D5F1E9", "57B1FC1CAFFEDF97", "E755A740300422D5", "A99206673A2D4907", "F637E8DB40FFFD73", "EEFC487B7CF15DE1", "AD5AC3324D0406FB", "48A488596145B327", "751A8EC1E8C4E659", "8970B112EC543839", "7E8AE89782C4D1FB", "2B30DBF799AAF753", "53B128FF662875E8", "F9F8FF489F35F1E7", "F98DF5F3588B3874", "822ACB35FE99F615", "AD64BE8F3D3B70BE", "E41F35488B9C82AE", "EC7BCE2659955F19", "746BBDDC23DEB71A", "830CF7AEDF27B0DB", "EA98D58BFCD781A2", "CFCA50812744F136", "7D1D8D8D403E6795", "7EA8CC209443D88A", "3EAADCACDDE14D80", "A058A7B99FC0BE15", "D010A22DA2E87B63", "1F91D2335EC4FFB", "D33341A1992B618D", "2FD73AB12D887CC3", "95AE3BD2791661C2", "521E273E599D96AA", "FAEF42A169426687", "9726A614F829CDA8", "1C94F714D1CD18D", "DB30C636BD014449", "B90C64A1B8358B96", "D817232E15FD5A52", "E2F9F0C99FC0122F", "756CCD00D03E0694", "E6ADD5C7A1AABBBE", "DB595195B324591D", "55E2B3C64035B4EB", "A7F665CA5EA75CB0", "57F81EFF788F131D", "BD265A89299D8C16", "14C84B384A243F84", "83E73E6DDAE20C37", "B0E8D4E45CE34C5E", "E9008DA509C5C5A3", "1FB7E90F189BB4A8" };
        StartCoroutine(StartChangeItemDurationInPlayerList(250,
            playerIDList,
            "com.zerobit.athenion.promo_card_pack_woe_6plus1",
            "com.zerobit.athenion.promo_card_pack_woe_6plus1",
            () =>
            {
                ResetAllValue();
                UpdateProgress("Complete ChangeItem");
            })
        );
    }

    private IEnumerator StartChangeItemDurationInPlayerList(int maxQueue, List<string> playerIDList, string targetItemID, string itemID, UnityAction OnComplete)
    {
        int maxCount = playerIDList.Count;
        Debug.Log("PlayerID Count : " + maxCount);
        UpdateProgress(0);

        while (playerIDList.Count > 0)
        {
            List<string> lotPlayerIDList = playerIDList.CutFirst(maxQueue);

            int count = lotPlayerIDList.Count;
            for (int i = 0; i < lotPlayerIDList.Count; i++)
            {
                StartCoroutine(ChangeItemInPlayer(lotPlayerIDList[i], targetItemID, itemID, () =>
                {
                    count -= 1;
                }));
            }
            yield return new WaitUntil(() => count <= 0);

            float ratio = 1f - ((float)playerIDList.Count / (float)maxCount);
            UpdateProgress(ratio);
            yield return null;

        }
        OnComplete?.Invoke();
    }

    IEnumerator ChangeItemInPlayer(string playerID, string targetItemID, string itemID, UnityAction onComplete)
    {
        ItemInstance item = null;
        yield return FindItemInstanceByItemID(playerID, targetItemID, (result) => item = result);
        yield return RevokePlayerInventoryItems(playerID, new List<string>() { item.ItemInstanceId }, null, (err) => Debug.Log(err));
        yield return IEGrantItem(playerID, itemID);
        onComplete?.Invoke();
    }

    public IEnumerator FindItemInstanceByItemID(string playerID, string itemID, UnityAction<ItemInstance> onComplete, UnityAction<string> onFail = null)
    {
        bool isFinish = false;
        ItemInstance itemInstance = null;
        yield return GetPlayerInventoryItems(playerID, (result) =>
        {
            itemInstance = result.Find(item => item.ItemId == itemID);
            onComplete?.Invoke(itemInstance);
            isFinish = true;
        }, (error) =>
        {
            onFail?.Invoke(error);
            isFinish = true;
        });
        yield return new WaitUntil(() => isFinish == true);
    }

    public IEnumerator FindVirtua(string playerID, string itemID, UnityAction<ItemInstance> onComplete, UnityAction<string> onFail = null)
    {
        bool isFinish = false;
        ItemInstance itemInstance = null;
        yield return GetPlayerInventoryItems(playerID, (result) =>
        {
            itemInstance = result.Find(item => item.ItemId == itemID);
            onComplete?.Invoke(itemInstance);
            isFinish = true;
        }, (error) =>
        {
            onFail?.Invoke(error);
            isFinish = true;
        });
        yield return new WaitUntil(() => isFinish == true);
    }

    public IEnumerator CheckContainItemInInventoryByItemID(string playerID, string itemID, UnityAction<bool> onComplete)
    {
        List<string> itemList = new List<string>();
        yield return GetPlayerInventoryItemIDList(playerID, (result) => itemList = result, (error) => { Debug.Log(error.ToString()); });

        string data = itemList.Find(item => item.Contains(itemID));
        bool isFound = data != null;
        onComplete?.Invoke(isFound);
    }

    #endregion

    #region ReadOnlyData
    public IEnumerator IEGetPlayerReadOnlyData(string playerID, string key, UnityAction<GetUserDataResult> onComplete, UnityAction<string> onFail = null)
    {
        bool isFinish = false;

        GetPlayerReadOnlyData(playerID, key, (result) =>
         {
             isFinish = true;
             onComplete?.Invoke(result);
         }, (error) =>
         {
             onFail?.Invoke(error.ToString());
             isFinish = true;
         });
        yield return new WaitUntil(() => isFinish == true);
    }
    #endregion

    #region InternalData
    public IEnumerator IEGetPlayerInternalData(string playerID, string key, UnityAction<GetUserDataResult> onComplete, UnityAction<string> onFail = null)
    {
        bool isFinish = false;

        GetPlayerInternalData(playerID, key, (result) =>
        {
            isFinish = true;
            onComplete?.Invoke(result);
        }, (error) =>
        {
            onFail?.Invoke(error.ToString());
            isFinish = true;
        });
        yield return new WaitUntil(() => isFinish == true);
    }
    #endregion

    #region Clear Player Data

    #region Reset VirtualCurrency

    public void ResetPlayersVirtualCurrency()
    {
        StartCoroutine(StartResetAllVirtualCurrency(250, () =>
        {
            ResetAllValue();
            UpdateProgress("Complete ResetPlayersVirtualCurrency");
        }));
    }

    private IEnumerator StartResetAllVirtualCurrency(int maxQueue, UnityAction OnComplete)
    {
        PlayFabError error = null;
        List<PlayerProfile> playerProfileList = null;

        UpdateProgress("Start GetPlayerSegment");
        yield return IEGetPlayerInSegment((result) => playerProfileList = result, (err) => error = err);
        UpdateProgress("Complete GetPlayerSegment");

        if (error != null)
        {
            Debug.LogError(error.ToString());
            OnComplete?.Invoke();
            yield break;
        }

        int maxCount = playerProfileList.Count;
        Debug.Log("PlayerProfile Count : " + maxCount);
        UpdateProgress(0);

        while (playerProfileList.Count > 0)
        {
            List<PlayerProfile> lotPlayerProfileList = playerProfileList.CutFirst(maxQueue);

            int count = lotPlayerProfileList.Count;
            for (int i = 0; i < lotPlayerProfileList.Count; i++)
            {
                StartCoroutine(ResetAllVirtualCurrency(lotPlayerProfileList[i].PlayerId, () =>
                {
                    count -= 1;
                }));
            }
            yield return new WaitUntil(() => count <= 0);

            float ratio = 1f - ((float)playerProfileList.Count / (float)maxCount);
            UpdateProgress(ratio);
            yield return null;

        }
        OnComplete?.Invoke();
    }

    private IEnumerator ResetAllVirtualCurrency(string playerID, UnityAction onComplete)
    {
        Dictionary<string, int> vcList = new Dictionary<string, int>();
        yield return GetPlayerVirtualCurrency(playerID, (result) => vcList = result, (error) => { Debug.Log(error.ToString()); });

        yield return UpdatePlayerVirtualCurrency(playerID, vcList, 0, () =>
         {
             Debug.Log("Complete UpdatePlayerVirtualCurrency " + playerID);
             onComplete?.Invoke();
         }, (error) => Debug.LogError(error));
    }

    private IEnumerator GetPlayerVirtualCurrency(string playerID, UnityAction<Dictionary<string, int>> onComplete, UnityAction<string> onFail)
    {
        bool isFinish = false;
        Dictionary<string, int> resultList = new Dictionary<string, int>();
        GetPlayerInventoryData(playerID, (result) =>
        {
            isFinish = true;
            resultList = result.VirtualCurrency;
        }, (error) =>
        {
            Debug.LogError(error.ToString());
        });
        yield return new WaitUntil(() => isFinish);
        onComplete?.Invoke(resultList);
    }

    private IEnumerator UpdatePlayerVirtualCurrency(string playerID, Dictionary<string, int> virtualCurrency, int amount, UnityAction onComplete, UnityAction<string> onFail)
    {
        if (virtualCurrency.Count == 0)
        {
            onComplete?.Invoke();
            yield break;
        }

        foreach (var vc in virtualCurrency)
        {
            if (vc.Value == 0)
                continue;
            int updateValue = amount - vc.Value;
            bool isFinish = false;
            AddPlayerVirutalCurrency(playerID, vc.Key, updateValue, () => isFinish = true, (err) => Debug.LogError(err.ToString()));
            yield return new WaitUntil(() => isFinish == true);
        }
        // 
        onComplete?.Invoke();
    }

    #endregion

    #region RevokeInventory
    public void RevokeAllInventoryItems()
    {
        StartCoroutine(StartRevokeInventoryItems(250, () =>
        {
            ResetAllValue();
            UpdateProgress("Complete RevokeAllInventoryItems");
        }));
    }

    private IEnumerator StartRevokeInventoryItems(int maxQueue, UnityAction OnComplete)
    {
        PlayFabError error = null;
        List<PlayerProfile> playerProfileList = null;

        UpdateProgress("Start GetPlayerSegment");
        yield return IEGetPlayerInSegment((result) => playerProfileList = result, (err) => error = err);
        UpdateProgress("Complete GetPlayerSegment");

        if (error != null)
        {
            Debug.LogError(error.ToString());
            OnComplete?.Invoke();
            yield break;
        }

        int maxCount = playerProfileList.Count;
        Debug.Log("PlayerProfile Count : " + maxCount);
        UpdateProgress(0);

        while (playerProfileList.Count > 0)
        {
            List<PlayerProfile> lotPlayerProfileList = playerProfileList.CutFirst(maxQueue);

            int count = lotPlayerProfileList.Count;
            for (int i = 0; i < lotPlayerProfileList.Count; i++)
            {
                StartCoroutine(RemovePlayerInventory(lotPlayerProfileList[i].PlayerId, () =>
                {
                    count -= 1;
                }));
            }
            yield return new WaitUntil(() => count <= 0);

            float ratio = 1f - ((float)playerProfileList.Count / (float)maxCount);
            UpdateProgress(ratio);
            yield return null;

        }
        OnComplete?.Invoke();
    }

    private IEnumerator RemovePlayerInventory(string playerID, UnityAction onComplete)
    {
        List<string> itemList = new List<string>();
        yield return GetPlayerInventoryItems(playerID, (result) => itemList = result, (error) => { Debug.Log(error.ToString()); });

        yield return RevokePlayerInventoryItems(playerID, itemList, () =>
        {
            Debug.Log("Complete Revoke " + playerID + "// item count :" + itemList.Count);
            onComplete?.Invoke();
        }, (error) => Debug.LogError(error));
    }

    public IEnumerator GetPlayerInventoryItemIDList(string playerID, UnityAction<List<string>> onComplete, UnityAction<string> onFail)
    {
        bool isFinish = false;
        List<string> resultList = new List<string>();
        GetPlayerInventoryData(playerID, (result) =>
        {
            isFinish = true;
            foreach (var item in result.Inventory)
            {
                resultList.Add(item.ItemId);
            }
        }, (error) =>
        {
            isFinish = true;
            Debug.Log(error.ToString());
        });
        yield return new WaitUntil(() => isFinish);
        onComplete?.Invoke(resultList);
    }

    private IEnumerator GetPlayerInventoryItems(string playerID, UnityAction<List<string>> onComplete, UnityAction<string> onFail)
    {
        bool isFinish = false;
        List<string> resultList = new List<string>();
        GetPlayerInventoryData(playerID, (result) =>
        {
            isFinish = true;
            foreach (var item in result.Inventory)
            {
                resultList.Add(item.ItemInstanceId);
            }
        }, (error) =>
        {
            isFinish = true;
            Debug.Log(error.ToString());
        });
        yield return new WaitUntil(() => isFinish);
        onComplete?.Invoke(resultList);
    }

    private IEnumerator GetPlayerInventoryItems(string playerID, UnityAction<List<ItemInstance>> onComplete, UnityAction<string> onFail)
    {
        bool isFinish = false;
        List<ItemInstance> resultList = new List<ItemInstance>();
        GetPlayerInventoryData(playerID, (result) =>
        {
            isFinish = true;
            foreach (var item in result.Inventory)
            {
                resultList.Add(item);
            }
            onComplete?.Invoke(resultList);
        }, (error) =>
        {
            isFinish = true;
            onFail?.Invoke(error.ToString());
        });
        yield return new WaitUntil(() => isFinish);
        
    }

    private IEnumerator RevokePlayerInventoryItems(string playerID, List<string> itemInstanceIDList, UnityAction onComplete, UnityAction<string> onFail)
    {
        if (itemInstanceIDList.Count == 0)
        {
            onComplete?.Invoke();
            yield break;
        }

        int count = itemInstanceIDList.Count;
        foreach (string id in itemInstanceIDList)
        {
            bool isFinish = false;
            RevokePlayerInventoryItem(
                playerID
                , id
                , (result) =>
                {
                    isFinish = true;
                    count--;
                }
                , (error) =>
                {
                    isFinish = true;
                    //count--;
                    onFail?.Invoke(error.ToString());
                }
            );
            yield return new WaitUntil(() => isFinish == true);
        }
        yield return new WaitUntil(() => count == 0);
        onComplete?.Invoke();
    }
    #endregion

    #region Virtual Currency
    public IEnumerator GetPlayerCurrency(string playerID, UnityAction<Dictionary<string, int>> onComplete, UnityAction<string> onFail)
    {
        bool isFinish = false;
        Dictionary<string, int> resultList = new Dictionary<string, int>();
        GetPlayerInventoryData(playerID, (result) =>
        {
            isFinish = true;
            if (result.VirtualCurrency != null && result.VirtualCurrency.Count > 0)
            {
                resultList = result.VirtualCurrency;
            }
        }, (error) =>
        {
            isFinish = true;
            Debug.Log(error.ToString());
        });
        yield return new WaitUntil(() => isFinish);
        onComplete?.Invoke(resultList);
    }
    #endregion

    #region Remove Playerdata

    public void RemoveAllPlayerDataBykeyList(string key)
    {
        bool isProduction = PlayFabSettings.TitleId == GameHelper.PlayFabTitleID_Production;

        UnityAction beginProcess = () =>
        {
            PlayerDataType type = key.ToEnum<PlayerDataType>();
            List<string> removeList = GetRemoveKeyByType(type);
            RemovePlayerDataByKeys(type, removeList);
        };

        //if (isProduction)
        //{
        //    PopupUIManager.Instance.ShowPopup_SureCheck("Check", "message", beginProcess, null);
        //}
        //else
        //{
        //    beginProcess?.Invoke();
        //}

        beginProcess?.Invoke();
    }

    private List<string> GetRemoveKeyByType(PlayerDataType type)
    {
        switch (type)
        {
            case PlayerDataType.PlayerData:
                return RemoveDataKeyList;
            case PlayerDataType.ReadOnlyData:
                return RemoveReadOnlyDataKeyList;
            case PlayerDataType.InternalData:
                return RemoveInternalDataKeyList;
        }
        return null;
    }

    public void RemovePlayerDataByKeys(PlayerDataType type, List<string> removeKeys)
    {
        if (GameHelper.ATNVersion != ATNVersion.Development) { return; }

        StartCoroutine(StartRemovePlayerDataByKeys(type, removeKeys, 250, () =>
            {
                ResetAllValue();
                UpdateProgress("Complete RemovePlayerDataByKeys");
            })
        );
    }

    private IEnumerator StartRemovePlayerDataByKeys(PlayerDataType type, List<string> removeKeyList, int maxQueue, UnityAction OnComplete)
    {
        PlayFabError error = null;
        List<PlayerProfile> playerProfileList = null;

        UpdateProgress("Start GetPlayerSegment");
        yield return IEGetPlayerInSegment((result) => playerProfileList = result, (err) => error = err);
        UpdateProgress("Complete GetPlayerSegment");

        if (error != null)
        {
            Debug.LogError(error.ToString());
            OnComplete?.Invoke();
            yield break;
        }

        int maxCount = playerProfileList.Count;
        UpdateProgress(0);
        while (playerProfileList.Count > 0)
        {
            List<PlayerProfile> lotPlayerProfileList = playerProfileList.CutFirst(maxQueue);
            bool isSuccess = false;
            yield return DORemovePlayerData(type, lotPlayerProfileList, removeKeyList, (result) => isSuccess = result);
            if (isSuccess == false)
            {
                OnComplete?.Invoke();
                yield break;
            }
            float ratio = 1f - ((float)playerProfileList.Count / (float)maxCount);
            UpdateProgress(ratio);
            yield return null;
        }
        OnComplete?.Invoke();
    }

    private IEnumerator IERemovePlayerData(PlayerDataType type, string playerId, List<string> removeList, UnityAction onComplete, UnityAction<PlayFabError> onFail)
    {
        bool isFinish = false;
        switch (type)
        {
            case PlayerDataType.PlayerData:
                RemovePlayerData(playerId, removeList
                     , (result) =>
                     {
                         onComplete?.Invoke();
                         isFinish = true;
                     }
                     , (error) =>
                     {
                         onFail?.Invoke(error);
                         isFinish = true;
                     }
                 );
                break;
            case PlayerDataType.ReadOnlyData:
                RemovePlayerReadOnlyData(playerId, removeList
                     , (result) =>
                     {
                         onComplete?.Invoke();
                         isFinish = true;
                     }
                     , (error) =>
                     {
                         onFail?.Invoke(error);
                         isFinish = true;
                     }
                 );
                break;
            case PlayerDataType.InternalData:
                RemovePlayerInternalData(playerId, removeList
                     , (result) =>
                     {
                         onComplete?.Invoke();
                         isFinish = true;
                     }
                     , (error) =>
                     {
                         onFail?.Invoke(error);
                         isFinish = true;
                     }
                 );
                break;
            default:
                break;
        }
        yield return new WaitUntil(() => isFinish == true);
    }

    #endregion

    #region Remove All PlayerData

    public void RemoveAllPlayerData(string key)
    {
        bool isProduction = PlayFabSettings.TitleId == GameHelper.PlayFabTitleID_Production;

        UnityAction beginProcess = () =>
        {
            PlayerDataType type = key.ToEnum<PlayerDataType>();
            RemoveAllPlayerData(type);
        };
        //if (isProduction)
        //{
        //    PopupUIManager.Instance.ShowPopup_SureCheck("Check", "message", beginProcess, null);
        //}
        //else
        //{
        //    beginProcess?.Invoke();
        //}

        beginProcess?.Invoke();
    }

    public void RemoveAllPlayerData(PlayerDataType type)
    {
        PrepareProgress();
        StartCoroutine(StartRemoveAllPlayerData(type, 300, () =>
            {
                ResetAllValue();
                UpdateProgress("Complete RemoveAllPlayerData");
            })
        );
    }

    private IEnumerator StartRemoveAllPlayerData(PlayerDataType type, int maxQueue, UnityAction OnComplete)
    {
        PlayFabError error = null;
        List<PlayerProfile> playerProfileList = null;

        UpdateProgress("Start GetPlayerSegment");
        yield return IEGetPlayerInSegment((result) => playerProfileList = result, (err) => error = err);
        UpdateProgress("Complete GetPlayerSegment");

        if (error != null)
        {
            Debug.LogError(error.ToString());
            OnComplete?.Invoke();
            yield break;
        }

        int maxCount = playerProfileList.Count;
        UpdateProgress(0);

        while (playerProfileList.Count > 0)
        {
            List<PlayerProfile> lotPlayerProfileList = playerProfileList.CutFirst(maxQueue);
            bool isSuccess = false;

            yield return DORemovePlayerData(type, lotPlayerProfileList, (result) => isSuccess = result);

            if (isSuccess == false)
            {
                OnComplete?.Invoke();
                yield break;
            }
            float ratio = 1f - ((float)playerProfileList.Count / (float)maxCount);
            UpdateProgress(ratio);
            yield return null;

        }
        OnComplete?.Invoke();
    }

    private IEnumerator IEGetAndRemoveAllPlayerData(PlayerDataType type, string playerID, UnityAction onComplete, UnityAction<string> onFail)
    {
        List<string> keys = null;
        yield return IEGetAllPlayerDataKeys(type, playerID, (result) => keys = result, (err) =>
         {
             onFail?.Invoke(err);
         });
        if (keys == null)
        {
            onFail?.Invoke("keys = null");
            yield break;
        }

        while (keys.Count > 0)
        {
            yield return IERemovePlayerData(type, playerID, keys.CutFirst(10), null, (err) => onFail?.Invoke(err.ToString()));
        }
        onComplete?.Invoke();

    }

    #endregion

    #region Utility

    public void WriteFile(string text)
    {
        string path = "Assets/Resources/AdminResult.txt";

        //Write some text to the test.txt file
        StreamWriter writer = new StreamWriter(path, true);
        writer.Write(text);
        writer.Close();

        //Print the text from the file
        Debug.Log("Write successful. " + path);
    }

    public void ResetAllValue()
    {
        _continuationToken = string.Empty;
    }

    public IEnumerator IEUpdatePlayerReadOnlyData(string playerID, string key, string value, UnityAction onComplete, UnityAction<string> onFail)
    {
        bool isFinish = false;
        UpdatePlayerReadOnlyData(playerID, key, value,
            (result) =>
            {
                onComplete?.Invoke();
                isFinish = true;
            },
            (err) =>
            {
                onFail?.Invoke(err.ToString());
                isFinish = true;
            }
        );
        yield return new WaitUntil(() => isFinish == true);
    }

    private IEnumerator DORemovePlayerData(PlayerDataType type, List<PlayerProfile> playerList, List<string> removeKeyList, UnityAction<bool> Complete)
    {
        bool[] completeTag = new bool[playerList.Count];
        List<PlayFabError> errorList = new List<PlayFabError>();
        for (int i = 0; i < playerList.Count; i++)
        {
            int tagIndex = i;
            StartCoroutine(IERemovePlayerData(type,
                playerList[i].PlayerId,
                removeKeyList,
                () =>
                {
                    completeTag[tagIndex] = true;
                },
                (err) =>
                {
                    completeTag[tagIndex] = true;
                    errorList.Add(err);
                }
            ));
        }
        while (completeTag.Contains(false))
        {
            yield return null;
        }

        if (errorList.Count > 0)
        {
            Debug.LogError("errorList : " + errorList.Count);
            Complete?.Invoke(false);
            yield break;
        }
        else
        {
            Complete?.Invoke(true);
        }
    }

    private IEnumerator DORemovePlayerData(PlayerDataType type, List<PlayerProfile> playerList, UnityAction<bool> onComplete)
    {
        bool[] completeTag = new bool[playerList.Count];
        List<string> errorList = new List<string>();
        for (int i = 0; i < playerList.Count; i++)
        {
            int tagIndex = i;
            string id = playerList[i].PlayerId;
            Debug.Log(id);
            StartCoroutine(IEGetAndRemoveAllPlayerData(
                type,
                playerList[i].PlayerId,
                () =>
                {
                    Debug.Log(id + " complete");
                    completeTag[tagIndex] = true;
                },
                (err) =>
                {
                    completeTag[tagIndex] = true;
                    errorList.Add(err);
                }
            ));
        }
        while (completeTag.Contains(false))
        {
            yield return null;
        }

        if (errorList.Count > 0)
        {
            Debug.LogError("errorList : " + errorList.Count);
            onComplete?.Invoke(false);
            yield break;
        }
        else
        {
            onComplete?.Invoke(true);
        }
    }

    public IEnumerator IEGetPlayerInSegment(UnityAction<List<PlayerProfile>> onComplete, UnityAction<PlayFabError> onFail)
    {
        List<PlayerProfile> resultList = new List<PlayerProfile>();
        while (true)
        {
            bool isFinish = false;
            List<PlayerProfile> players = new List<PlayerProfile>();
            Debug.Log("Before");
            GetPlayerInSegment(
                (result) =>
                {
                    Debug.Log("1");
                    players = result.PlayerProfiles;
                    isFinish = true;
                }
                , (error) =>
                {
                    Debug.Log("2");
                    onFail?.Invoke(error);
                    isFinish = true;
                }
            );
            Debug.Log("0");
            yield return new WaitUntil(() => isFinish == true);
            Debug.Log("After");
            resultList.AddRange(players);
            if (players.Count < _maxBatchPlayerSegment)
            {
                Debug.Log("Complete player segment");
                onComplete?.Invoke(resultList);
                yield break;
            }
        }
    }

    private IEnumerator IEGetAllPlayerDataKeys(PlayerDataType type, string playerID, UnityAction<List<string>> onComplete, UnityAction<string> onFail)
    {
        bool isFinish = false;
        switch (type)
        {
            case PlayerDataType.PlayerData:
                GetAllPlayerData(playerID,
                    (result) =>
                    {
                        onComplete?.Invoke(result.Data.Keys.ToList());
                        isFinish = true;
                    }, (error) =>
                    {
                        onFail?.Invoke(error.Error.ToString());
                        isFinish = true;
                    }
                );
                break;

            case PlayerDataType.ReadOnlyData:
                GetAllPlayerReadOnlyData(playerID,
                    (result) =>
                    {
                        onComplete?.Invoke(result.Data.Keys.ToList());
                        isFinish = true;
                    }, (error) =>
                    {
                        onFail?.Invoke(error.Error.ToString());
                        isFinish = true;
                    }
                );
                break;
            case PlayerDataType.InternalData:
                GetAllPlayerInternalData(playerID,
                    (result) =>
                    {
                        onComplete?.Invoke(result.Data.Keys.ToList());
                        isFinish = true;
                    }, (error) =>
                    {
                        onFail?.Invoke(error.Error.ToString());
                        isFinish = true;
                    }
                );
                break;
            default:
                break;
        }

        yield return new WaitUntil(() => isFinish == true);
    }

    #endregion

    private IEnumerator IEGrantItem(string playerID, string itemID)
    {
        bool isFinish = false;
        GrantPlayerInventoryItem(playerID, itemID, (result) => isFinish = true, (err) => Debug.Log(err));
        yield return new WaitUntil(() => isFinish == true);
    }
    #endregion

    #region Inventory Data
    
    #endregion

#endif
}