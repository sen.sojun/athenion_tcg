﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class PlayFabAdminProgressUI : MonoBehaviour
{
    #region Private UI
    [SerializeField] private TMP_Text _serverText;
    [SerializeField] private TMP_Text _progressText;
    [SerializeField] private TMP_Text _errorText;
    #endregion

    #region Private Properties
    private int _maxProgress = 0;
    private int _currentProgress = 0;
    private int _errorCount = 0;
    #endregion

    #region Methods
    private void OnApplicationPause(bool pause)
    {
        
    }

    public void SetServerText(string text)
    {
        _serverText.text = text;
    }

    public void PrepareProgress()
    {
        UpdatePrepareProgressText();
    }

    public void StartProgress(int maxProgress)
    {
        _maxProgress = maxProgress;
        _currentProgress = 0;
        _errorCount = 0;
        UpdateProgressText();
    }

    public void CountProgress(bool isSuccess)
    {
        if (isSuccess)
        {
            _currentProgress++;
        }
        else
        {
            _errorCount++;
        }
        UpdateProgressText();
    }

    private void UpdatePrepareProgressText()
    {
        _progressText.text = "Loading...";
        _errorText.text = "Loading...";
    }

    private void UpdateProgressText()
    {
        if (_currentProgress == _maxProgress)
        {
            _progressText.text = "!COMPLETE!";
        }
        else
        {
            _progressText.text = string.Format("Success: {0}/{1}", _currentProgress, _maxProgress);
        }

        _errorText.text = string.Format("Error: {0}/{1}", _errorCount, _maxProgress);
    }

    public void SetText(string text)
    {
        _progressText.text = text;
        _errorText.text = "";
    }

    public void UpdateProgress(float ratio)
    {
        _progressText.text = (ratio * 100f) + " %";
    }
    #endregion
}