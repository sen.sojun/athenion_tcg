﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Events;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Newtonsoft.Json;
using PlayFab;
#if UNITY_EDITOR && !UNITY_IOS
using PlayFab.AdminModels;
#endif

public class PlayFabAdminHotFixManager : MonoBehaviour
{
#if UNITY_EDITOR && !UNITY_IOS
    [SerializeField]
    private PlayFabAdminManager _manager;

    [SerializeField]
    private PlayFabAdminUIManager _uiManager;


    public void AddSubscriptionToken()
    {
        List<string> playerIDList = new List<string>()
        {
            "F7DC8F8AF25D09AE", "3AF13C4E93964EB", "62918F284137D3D7", "2C33F55E4289DD67", "95AE3BD2791661C2", "5F19CCCE8C349DC8", "A058A7B99FC0BE15", "1C94F714D1CD18D", "2B30DBF799AAF753", "4E476A88BE34A579", "D2029FD323C5E005", "98C473C48DFAB90F", "506068E639E6A684", "6137093051F29371", "F120A1624720600E", "9C8EE9F9E70311E", "9EC6DE9F6B9B3E5F", "CAB63E2AE7500845", "6A70B885D39DD005", "1079831CAE4FE9A4", "F7893A808D895A39", "237BB524CC7F02D7", "BAA499285439BDF5", "26D46FEEBEA7AD2B", "D8BF6CD51AEF95E2", "3DA348DFD3D64103", "115B047E3C1BB535", "D437B76216A25747", "6CD913C0C6DC2E9C", "E09D72AC3D15C5DF", "7E8AE89782C4D1FB", "AD700F6BBDAADDA2", "456D4C096A6B7311", "706FD0FD1BD43420", "40B69ABE4EAF0A8A", "1804564E7A461948", "9AB94D87370780DF", "D3236F4F1C01FAA8", "E272267CF33AC0CA", "AB39B724A36D26E6", "F8EB6EC7D5811106", "916DB8712579B148", "F5AFC23340C63268", "97109E3526B459CC", "50ED8E18564DF285", "5733E7576A633025", "90F8E97D39099B1C", "2761CA2ADA49F71F", "EB33F8DBCE0AAA13", "FAA31BC94342546", "44B9F21D2A61CB66", "994E9EC2F600FC08", "1339D60685127117", "CDBE1EF67FDF46AD", "68D1B508F4A1A8B8", "B5CF01D1E0AF93B1", "ECC709F99B25A87A", "ECE541C25CF55E54", "ADF44FE58276F07E", "FB57C2D34098F5D0", "8FE3A4F4C8084921", "8ACEAD5A8ED35F5", "8970B112EC543839", "F0121EC1952A346", "7CEA17C50D679E81", "CCD74095973854C", "B0E8D4E45CE34C5E", "AE24AFC1F0BDA03E", "9B13E0AF00379031", "A452B74D7FFA3923", "3821852CBA5BB68B", "E4CF59D2081421C3", "70F9F524B4D5F1E9", "EE797C6FCAF679FF", "A4109760FDEE74D5", "9DB49D10B747F5C1", "F7A515910A0C0B58", "17BB0F67C155D18E", "906FD05A5F85B90B", "9F16C9162EF9A86", "AD64BE8F3D3B70BE", "C6F0A0D2B2B7B1B5", "485AC63202A12D1D", "53B128FF662875E8", "830CF7AEDF27B0DB", "929AC8DCD5E06430", "BBA135BCE14ABD80", "8FD9C2EB712D2DA6", "C4AD352EF6957116", "22FBFE88C2CEC5BA", "BC44D2CCA98CB8BA", "C65A107D7A29866A", "EC7BCE2659955F19", "2E4F486F4F80E4FF", "4A1A9A0ABD8A5115", "BE85ACDF9FDEB98E", "CEA67DDE9D40D074", "197B1EC142C3497C", "AA888B3A375F5371", "F637E8DB40FFFD73", "256B40DE750E8820", "B71E6C638BA229EB", "1C8F37BB26B7352D", "18AC951CA679BF5E", "5793C40F2F0603AD", "3225A86B1C475EA6", "59A4CC6908E41E3F", "1B4871C5A12DD41D", "15C229C3B55C5C1E", "756CCD00D03E0694", "D817232E15FD5A52", "E9008DA509C5C5A3", "9726A614F829CDA8", "A7F665CA5EA75CB0", "7800824FD11C8423", "E2F9F0C99FC0122F", "DE609B1E68BAEE24", "EFE8809410DCE24D", "B96262104DBE042C", "5A8AA875557600FA", "85A95749FEA66576", "C7757EFC8DD77771", "1FB7E90F189BB4A8", "6775D152136E2920"
};
        int maxCount = playerIDList.Count;
        StartCoroutine(StartAddSubscriptionToken(250, playerIDList, "subscription_record", 1, (completeList) =>
         {
             Debug.Log("Complete " + completeList.Count + " from " + maxCount);
             Debug.Log("CompleteList : ");
             Debug.Log(JsonConvert.SerializeObject(completeList));
             _manager.ResetAllValue();
             _uiManager.UpdateProgress("Complete GetReadOnlyData");
         }));
    }

    private IEnumerator StartAddSubscriptionToken(int maxQueue, List<string> playerIDList, string key, int amount, UnityAction<List<string>> OnComplete)
    {
        int maxCount = playerIDList.Count;

        _uiManager.UpdateProgress(0);

        List<string> completeList = new List<string>();
        List<string> failPlayerIDList = new List<string>();
        while (playerIDList.Count > 0)
        {
            List<string> lotPlayerList = playerIDList.CutFirst(maxQueue);

            int count = lotPlayerList.Count;
            for (int i = 0; i < lotPlayerList.Count; i++)
            {
                string playerId = lotPlayerList[i];
                StartCoroutine(AddVIPToken(playerId, amount, () =>
                {
                    count -= 1;
                    completeList.Add(playerId);
                },
                (err) =>
                {
                    count -= 1;
                    failPlayerIDList.Add(playerId);
                    Debug.Log(err);
                }));
            }
            yield return new WaitUntil(() => count <= 0);

            float ratio = 1f - ((float)playerIDList.Count / (float)maxCount);
            _uiManager.UpdateProgress(ratio);
            yield return null;

        }
        OnComplete?.Invoke(completeList);

        if (failPlayerIDList.Count > 0)
        {
            Debug.Log("failPlayerIDList : ");
            Debug.Log(JsonConvert.SerializeObject(failPlayerIDList));
        }
    }

    IEnumerator AddVIPToken(string playerID, int amount, UnityAction onComplete, UnityAction<string> onFail)
    {
        string getPlayerDataError = null;
        GetUserDataResult playerDataResult = null;
        yield return _manager.IEGetPlayerReadOnlyData(playerID, "subscription_record", (result) => playerDataResult = result, (err) => getPlayerDataError = err);

        if (string.IsNullOrEmpty(getPlayerDataError) == false)
        {
            onFail?.Invoke(getPlayerDataError);
            yield break;
        }

        if (playerDataResult.Data.ContainsKey("subscription_record") == false)
        {
            onFail?.Invoke(playerID + " not have key");
            yield break;
        }

        Dictionary<string, object> vipData = JsonConvert.DeserializeObject<Dictionary<string, object>>(playerDataResult.Data["subscription_record"].Value);

        string before = JsonConvert.SerializeObject(vipData);

        int token = Convert.ToInt32(vipData["daily_token"]);
        token += amount;
        vipData["daily_token"] = token;
        yield return _manager.IEUpdatePlayerReadOnlyData(playerID, "subscription_record", JsonConvert.SerializeObject(vipData), onComplete, onFail);

        string after = JsonConvert.SerializeObject(vipData);

        Debug.LogFormat("{0} : {1} -> {2}", playerID, before, after);
    }
#endif
}
