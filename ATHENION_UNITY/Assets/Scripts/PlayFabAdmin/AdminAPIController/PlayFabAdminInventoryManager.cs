﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Newtonsoft.Json;
using System.Linq;
using PlayFab;
#if UNITY_EDITOR && !UNITY_IOS
using PlayFab.AdminModels;
#endif

public class PlayFabAdminInventoryManager : MonoBehaviour
{
#if UNITY_EDITOR && !UNITY_IOS
    [SerializeField]
    private string _itemID;
    [SerializeField]
    private PlayFabAdminManager _manager;

    [SerializeField]
    private PlayFabAdminUIManager _uiManager;

    #region Read Inventory

    public void FindItemInInventory()
    {
        if (_itemID == string.Empty)
            return;

        List<Dictionary<string, string>> exportedResult = new List<Dictionary<string, string>>();
        List<string> foundList = new List<string>();
        StartCoroutine(StartFindItemInInventory(250, _itemID,
                delegate ()
                {
                    _manager.ResetAllValue();
                    _uiManager.UpdateProgress("Complete FindItemInInventory");
                    CSVWriter.WriteCSV("ExportedInventoryData" + "_" + _itemID, exportedResult);
                    _uiManager.UpdateProgress("Complete Exported Data");

                },
                delegate (ItemInstance item, string playerID)
                {
                    if (item == null)
                        return;
                    foundList.Add(playerID);

                    Dictionary<string, string> row = new Dictionary<string, string>();
                    row.Add("PlayerID", playerID);
                    row.Add("ItemID", item.ItemId);
                    row.Add("ItemInstanceID", item.ItemInstanceId);
                    exportedResult.Add(row);
                }
            )
        );
    }

    public void FindVirtualCurrency()
    {
        List<Dictionary<string, string>> exportedResult = new List<Dictionary<string, string>>();
        List<string> foundList = new List<string>();
        StartCoroutine(StartFindVirtualCurrencyInInventory(250,
                delegate ()
                {
                    _manager.ResetAllValue();
                    _uiManager.UpdateProgress("Complete FindVirtualCurrency");
                    CSVWriter.WriteCSV("ExportedVirtualCurrencyData", exportedResult);
                    _uiManager.UpdateProgress("Complete Exported Data");

                },
                delegate (Dictionary<string, int> currency, string playerID)
                {
                    if (currency == null)
                        return;
                    foundList.Add(playerID);

                    Dictionary<string, string> row = new Dictionary<string, string>();
                    row.Add("PlayerID", playerID);
                    foreach(KeyValuePair<string, int> item in currency)
                    {
                        row.Add(item.Key, item.Value.ToString());
                    }
                    exportedResult.Add(row);
                    Debug.Log(JsonConvert.SerializeObject(row));
                }
            )
        );
    }

    public void FindVIPItemInInventory()
    {
        List<string> foundList = new List<string>();

        Dictionary<int, List<string>> VIPTier = new Dictionary<int, List<string>>();

        StartCoroutine(StartFindItemInInventory(499, "SUBSCRIPTION_VIP",
                delegate ()
                {
                    _manager.ResetAllValue();
                    Debug.Log("player that contain : ");
                    Debug.Log(JsonConvert.SerializeObject(foundList));

                    string result = "";
                    foreach (var tier in VIPTier)
                    {
                        result += string.Format("{0}\n {1} \n\n", "Tier " + tier.Key, JsonConvert.SerializeObject(tier.Value));

                        //Debug.Log("Tier " + tier.Key);
                        //Debug.Log(JsonConvert.SerializeObject(tier.Value));
                    }
                    _manager.WriteFile(result);

                    _uiManager.UpdateProgress("Complete FindItemInInventory");
                },
                delegate (ItemInstance item, string playerID)
                {
                    if (item == null)
                        return;
                    foundList.Add(playerID);
                    if (item.RemainingUses != null)
                    {
                        int tier = (int)item.RemainingUses / 30;
                        if (VIPTier.ContainsKey(tier) == false)
                            VIPTier.Add(tier, new List<string>());
                        VIPTier[tier].Add(playerID);
                    }
                }
            )
        );
    }

    private IEnumerator StartFindItemInInventory(int maxQueue, string itemID, Action OnComplete, Action<ItemInstance, string> OnCallbackResult)
    {
        PlayFabError error = null;
        List<PlayerProfile> playerProfileList = null;

        _uiManager.UpdateProgress("Start GetPlayerSegment");
        yield return _manager.IEGetPlayerInSegment((result) => playerProfileList = result, (err) => error = err);
        _uiManager.UpdateProgress("Complete GetPlayerSegment");

        if (error != null)
        {
            Debug.LogError(error.ToString());
            OnComplete?.Invoke();
            yield break;
        }

        int maxCount = playerProfileList.Count;
        Debug.Log("PlayerProfile Count : " + maxCount);
        _uiManager.UpdateProgress(0);

        List<string> failPlayerIDList = new List<string>();
        while (playerProfileList.Count > 0)
        {
            List<PlayerProfile> lotPlayerProfileList = playerProfileList.CutFirst(maxQueue);

            int count = lotPlayerProfileList.Count;
            for (int i = 0; i < lotPlayerProfileList.Count; i++)
            {
                string playerId = lotPlayerProfileList[i].PlayerId;
                StartCoroutine(_manager.FindItemInstanceByItemID(playerId, itemID, (ItemInstance) =>
                {
                    count -= 1;
                    OnCallbackResult?.Invoke(ItemInstance, playerId);
                },
                (err) =>
                {
                    count -= 1;
                    failPlayerIDList.Add(playerId);
                    Debug.Log(err);
                }));
            }
            yield return new WaitUntil(() => count <= 0);

            float ratio = 1f - ((float)playerProfileList.Count / (float)maxCount);
            _uiManager.UpdateProgress(ratio);
            yield return null;

        }
        OnComplete?.Invoke();

        if (failPlayerIDList.Count > 0)
        {
            Debug.Log("failPlayerIDList : ");
            Debug.Log(JsonConvert.SerializeObject(failPlayerIDList));
        }
    }

    private IEnumerator StartFindVirtualCurrencyInInventory(int maxQueue, Action OnComplete, Action<Dictionary<string, int>, string> OnCallbackResult)
    {
        PlayFabError error = null;
        List<PlayerProfile> playerProfileList = null;

        _uiManager.UpdateProgress("Start GetPlayerSegment");
        yield return _manager.IEGetPlayerInSegment((result) => playerProfileList = result, (err) => error = err);
        _uiManager.UpdateProgress("Complete GetPlayerSegment");

        if (error != null)
        {
            Debug.LogError(error.ToString());
            OnComplete?.Invoke();
            yield break;
        }

        int maxCount = playerProfileList.Count;
        Debug.Log("PlayerProfile Count : " + maxCount);
        _uiManager.UpdateProgress(0);

        List<string> failPlayerIDList = new List<string>();
        while (playerProfileList.Count > 0)
        {
            List<PlayerProfile> lotPlayerProfileList = playerProfileList.CutFirst(maxQueue);

            int count = lotPlayerProfileList.Count;
            for (int i = 0; i < lotPlayerProfileList.Count; i++)
            {
                string playerId = lotPlayerProfileList[i].PlayerId;
                StartCoroutine(_manager.GetPlayerCurrency(playerId, (currency) =>
                {
                    count -= 1;
                    OnCallbackResult?.Invoke(currency, playerId);
                },
                (err) =>
                {
                    count -= 1;
                    failPlayerIDList.Add(playerId);
                    Debug.Log(err);
                }));
            }
            yield return new WaitUntil(() => count <= 0);

            float ratio = 1f - ((float)playerProfileList.Count / (float)maxCount);
            _uiManager.UpdateProgress(ratio);
            yield return null;

        }
        OnComplete?.Invoke();

        if (failPlayerIDList.Count > 0)
        {
            Debug.Log("failPlayerIDList : ");
            Debug.Log(JsonConvert.SerializeObject(failPlayerIDList));
        }
    }
    #endregion
#endif
}
