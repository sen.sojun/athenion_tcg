﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab;
#if UNITY_EDITOR && !UNITY_IOS
using PlayFab.AdminModels;
#endif
using Newtonsoft.Json;
using System;

public class PlayFabAdminPlayerDataManager : MonoBehaviour
{
#if UNITY_EDITOR && !UNITY_IOS
    public string itemID;
    [SerializeField]
    private PlayFabAdminManager _manager;

    [SerializeField]
    private PlayFabAdminUIManager _uiManager;

    #region Read PlayerData

    public void FindPlayerReferredStatusInPlayerData()
    {
        List<Dictionary<string, string>> exportedResult = new List<Dictionary<string, string>>();
        StartCoroutine(StartFindDataInPlayerReadOnlyData(250, "referred_status",
                delegate ()
                {
                    _manager.ResetAllValue();
                    _uiManager.UpdateProgress("Complete FindReferredStatusInPlayerData");
                    CSVWriter.WriteCSV("ExportedPlayerReferredStatusData", exportedResult);
                    _uiManager.UpdateProgress("Complete Exported Data");
                },
                delegate (GetUserDataResult item, PlayerProfile profile, Dictionary<string, PlayerLocation> location)
                {
                    if (item.Data.ContainsKey("referred_status"))
                    {
                        Debug.LogFormat("{0}:{1}", profile.PlayerId, item.Data["referred_status"].Value);
                        Dictionary<string, string> row = new Dictionary<string, string>();
                        row.Add("PlayerID", profile.PlayerId);
                        row.Add("ReferredStatus", item.Data["referred_status"].Value);
                        exportedResult.Add(row);
                    }
                }
            )
        );
    }

    public void FindPlayerReferreeRewardClaimedStatusListInPlayerData()
    {
        List<Dictionary<string, string>> exportedResult = new List<Dictionary<string, string>>();
        StartCoroutine(StartFindDataInPlayerReadOnlyData(250, "referee_reward_claimed_status_list",
                delegate ()
                {
                    _manager.ResetAllValue();
                    _uiManager.UpdateProgress("Complete FindPlayerReferreeRewardClaimedStatusListInPlayerData");
                    CSVWriter.WriteCSV("ExportedPlayerReferreeRewardClaimedStatusData", exportedResult);
                    _uiManager.UpdateProgress("Complete Exported Data");
                },
                delegate (GetUserDataResult item, PlayerProfile profile, Dictionary<string, PlayerLocation> location)
                {
                    if (item.Data.ContainsKey("referee_reward_claimed_status_list"))
                    {
                        Debug.LogFormat("{0}:{1}", profile.PlayerId, item.Data["referee_reward_claimed_status_list"].Value);
                        Dictionary<string, string> row = new Dictionary<string, string>();
                        row.Add("PlayerID", profile.PlayerId);
                        row.Add("RefereeRewardClaimedStatusList", item.Data["referee_reward_claimed_status_list"].Value);
                        exportedResult.Add(row);
                    }
                }
            )
        );
    }

    public void FindPlayerAbnormalEventInPlayerData()
    {
        List<Dictionary<string, string>> exportedResult = new List<Dictionary<string, string>>();
        StartCoroutine(StartFindDataInPlayerInternalData(499, "abnormal_event",
                delegate ()
                {
                    _manager.ResetAllValue();
                    _uiManager.UpdateProgress("Complete FindPlayerAbnormalEventInPlayerData");
                    CSVWriter.WriteCSV("ExportedPlayerAbnormalEventData", exportedResult);
                    _uiManager.UpdateProgress("Complete Exported Data");
                },
                delegate (GetUserDataResult item, string playerID, Dictionary<string, PlayerLocation> location)
                {
                    if (item.Data.ContainsKey("abnormal_event"))
                    {
                        Debug.LogFormat("{0}:{1}", playerID, item.Data["abnormal_event"].Value);
                        Dictionary<string, string> row = new Dictionary<string, string>();
                        row.Add("PlayerID", playerID);
                        row.Add("AbnormalEventData", item.Data["abnormal_event"].Value);
                        exportedResult.Add(row);
                    }
                }
            )
        );
    }

    public void FindPlayerStatInPlayerData()
    {
        List<string> foundList = new List<string>();

        StartCoroutine(StartFindDataInPlayerReadOnlyData(499, "player_stat",
                delegate ()
                {
                    _manager.ResetAllValue();
                    Debug.Log("count:" + foundList.Count);
                    _uiManager.UpdateProgress("Complete FindPlayerStatInPlayerData");
                },
                delegate (GetUserDataResult item, PlayerProfile profile, Dictionary<string, PlayerLocation> location)
                {
                    if(item.Data.ContainsKey("player_stat"))
                    {
                        Debug.LogFormat("{0}:{1}", profile.PlayerId, item.Data["player_stat"].Value);

                        Dictionary<string, int> statDict = JsonConvert.DeserializeObject<Dictionary<string, int>>(item.Data["player_stat"].Value);
                        if(statDict.ContainsKey("W_TUT_1"))
                        {
                            foundList.Add(profile.PlayerId);
                        }
                    }
                }
            )
        );
    }

    public void FindPlayerExpInPlayerData()
    {
        List<Dictionary<string, string>> exportedResult = new List<Dictionary<string, string>>();
        StartCoroutine(StartFindDataInPlayerReadOnlyData(250, "player_exp",
                delegate ()
                {
                    _manager.ResetAllValue();
                    _uiManager.UpdateProgress("Complete FindPlayerExpInPlayerData");
                    CSVWriter.WriteCSV("ExportedPlayerExpData", exportedResult);
                    _uiManager.UpdateProgress("Complete Exported Data");
                },
                delegate (GetUserDataResult item, PlayerProfile profile, Dictionary<string, PlayerLocation> location)
                {
                    int exp = 0;
                    if (item.Data.ContainsKey("player_exp"))
                    {
                        exp = int.Parse(item.Data["player_exp"].Value);
                    }
                    Debug.LogFormat("{0}:{1}", profile.PlayerId, exp);

                    string lastLoginCity = "null";
                    string lastLoginCountry = "null";
                    if (location != null)
                    {
                        if (location.ContainsKey("LastLogin"))
                        {
                            if (!string.IsNullOrEmpty(location["LastLogin"].City))
                            {
                                lastLoginCity = location["LastLogin"].City.ToString();
                            }

                            lastLoginCountry = location["LastLogin"].CountryCode.ToString();
                        }
                    }
                    
                    Dictionary<string, string> row = new Dictionary<string, string>();
                    row.Add("PlayerID", profile.PlayerId);
                    row.Add("LastLogin City", lastLoginCity);
                    row.Add("LastLogin Country", lastLoginCountry);
                    row.Add("Exp", exp.ToString());
                    exportedResult.Add(row);
                }
            )
        );
    }

    public void FindPlayerRankInPlayerData()
    {
        List<Dictionary<string, string>> exportedResult = new List<Dictionary<string, string>>();
        StartCoroutine(StartFindDataInPlayerReadOnlyData(250, "player_rank",
                delegate ()
                {
                    _manager.ResetAllValue();
                    _uiManager.UpdateProgress("Complete FindPlayerRankInPlayerData");
                    CSVWriter.WriteCSV("ExportedPlayerRankData", exportedResult);
                    _uiManager.UpdateProgress("Complete Exported Data");
                },
                delegate (GetUserDataResult item, PlayerProfile profile, Dictionary<string, PlayerLocation> location)
                {
                    string rankDataStr = "null";
                    Dictionary<string, object> rankData = new Dictionary<string, object>();
                    if (item.Data.ContainsKey("player_rank"))
                    {
                        rankDataStr = item.Data["player_rank"].Value;
                        rankData = JsonConvert.DeserializeObject<Dictionary<string, object>>(rankDataStr);
                    }
                    Debug.LogFormat("{0}:{1}", profile.PlayerId, rankDataStr);

                    string lastLoginCity = "null";
                    string lastLoginCountry = "null";
                    if (location != null)
                    {
                        if (location.ContainsKey("LastLogin"))
                        {
                            if (!string.IsNullOrEmpty(location["LastLogin"].City))
                            {
                                lastLoginCity = location["LastLogin"].City.ToString();
                            }

                            lastLoginCountry = location["LastLogin"].CountryCode.ToString();
                        }
                    }

                    Dictionary<string, string> row = new Dictionary<string, string>();
                    row.Add("PlayerID", profile.PlayerId);
                    row.Add("PlayerName", profile.DisplayName);
                    row.Add("LastLogin City", lastLoginCity);
                    row.Add("LastLogin Country", lastLoginCountry);

                    foreach (var rank in rankData)
                    {
                        string rankIndex = "0";
                        Dictionary<string, object> data = JsonConvert.DeserializeObject<Dictionary<string, object>>(rank.Value.ToString());
                        if (data.ContainsKey("current"))
                        {
                            rankIndex = data["current"].ToString();
                        }
                        row.Add(rank.Key, rankIndex);
                    }

                    exportedResult.Add(row);
                }
            )
        );
    }

    private IEnumerator StartFindDataInPlayerReadOnlyData(int maxQueue, string dataKey, Action OnComplete, Action<GetUserDataResult, PlayerProfile, Dictionary<string, PlayerLocation>> OnCallbackResult)
    {
        PlayFabError error = null;
        List<PlayerProfile> playerProfileList = null;

        _uiManager.UpdateProgress("Start GetPlayerSegment");
        yield return _manager.IEGetPlayerInSegment((result) => playerProfileList = result, (err) => error = err);
        _uiManager.UpdateProgress("Complete GetPlayerSegment");

        if (error != null)
        {
            Debug.LogError(error.ToString());
            OnComplete?.Invoke();
            yield break;
        }

        int maxCount = playerProfileList.Count;
        Debug.Log("PlayerProfile Count : " + maxCount);
        _uiManager.UpdateProgress(0);

        List<string> failPlayerIDList = new List<string>();
        while (playerProfileList.Count > 0)
        {
            List<PlayerProfile> lotPlayerProfileList = playerProfileList.CutFirst(maxQueue);

            int count = lotPlayerProfileList.Count;
            for (int i = 0; i < lotPlayerProfileList.Count; i++)
            {
                PlayerProfile profile = lotPlayerProfileList[i];
                StartCoroutine(_manager.IEGetPlayerReadOnlyData(profile.PlayerId, dataKey
                , (result) =>
                {
                    count -= 1;
                    OnCallbackResult?.Invoke(result, profile, profile.Locations);
                }
                , (err) =>
                {
                    count -= 1;
                    failPlayerIDList.Add(profile.PlayerId);
                    Debug.Log(err);
                }));
            }
            yield return new WaitUntil(() => count <= 0);

            float ratio = 1f - ((float)playerProfileList.Count / (float)maxCount);
            _uiManager.UpdateProgress(ratio);
            yield return null;

        }
        OnComplete?.Invoke();

        if (failPlayerIDList.Count > 0)
        {
            Debug.Log("failPlayerIDList : ");
            Debug.Log(JsonConvert.SerializeObject(failPlayerIDList));
        }
    }

    private IEnumerator StartFindDataInPlayerInternalData(int maxQueue, string dataKey, Action OnComplete, Action<GetUserDataResult, string, Dictionary<string, PlayerLocation>> OnCallbackResult)
    {
        PlayFabError error = null;
        List<PlayerProfile> playerProfileList = null;

        _uiManager.UpdateProgress("Start GetPlayerSegment");
        yield return _manager.IEGetPlayerInSegment((result) => playerProfileList = result, (err) => error = err);
        _uiManager.UpdateProgress("Complete GetPlayerSegment");

        if (error != null)
        {
            Debug.LogError(error.ToString());
            OnComplete?.Invoke();
            yield break;
        }

        int maxCount = playerProfileList.Count;
        Debug.Log("PlayerProfile Count : " + maxCount);
        _uiManager.UpdateProgress(0);

        List<string> failPlayerIDList = new List<string>();
        while (playerProfileList.Count > 0)
        {
            List<PlayerProfile> lotPlayerProfileList = playerProfileList.CutFirst(maxQueue);

            int count = lotPlayerProfileList.Count;
            for (int i = 0; i < lotPlayerProfileList.Count; i++)
            {
                PlayerProfile profile = lotPlayerProfileList[i];
                StartCoroutine(_manager.IEGetPlayerInternalData(profile.PlayerId, dataKey
                , (result) =>
                {
                    count -= 1;
                    OnCallbackResult?.Invoke(result, profile.PlayerId, profile.Locations);
                }
                , (err) =>
                {
                    count -= 1;
                    failPlayerIDList.Add(profile.PlayerId);
                    Debug.Log(err);
                }));
            }
            yield return new WaitUntil(() => count <= 0);

            float ratio = 1f - ((float)playerProfileList.Count / (float)maxCount);
            _uiManager.UpdateProgress(ratio);
            yield return null;

        }
        OnComplete?.Invoke();

        if (failPlayerIDList.Count > 0)
        {
            Debug.Log("failPlayerIDList : ");
            Debug.Log(JsonConvert.SerializeObject(failPlayerIDList));
        }
    }
    #endregion
#endif
}
