﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PlayFabAdminUIManager : MonoSingleton<PlayFabAdminUIManager>
{
    #region Private UI Properties
    [SerializeField] private PlayFabAdminProgressUI _progressUI;
    [SerializeField] private Button _activeButton;
    #endregion

    #region Methods
    public void UpdateServerText(string text)
    {
        _progressUI.SetServerText(text);
    }

    public void DoAction()
    {

    }

    public void RequestPrepareProgress()
    {
        _progressUI.PrepareProgress();
    }

    public void RequestStartProgress(int maxProgress)
    {
        _progressUI.StartProgress(maxProgress);
    }

    public void RequestCountProgress(bool isSuccess)
    {
        _progressUI.CountProgress(isSuccess);
    }

    public void UpdateProgress(int current, int max)
    {
        _progressUI.UpdateProgress(Convert.ToSingle(current) / Convert.ToSingle(max));
    }

    public void UpdateProgress(float ratio)
    {
        _progressUI.UpdateProgress(ratio);
    }

    public void UpdateProgress(string text)
    {
        _progressUI.SetText(text);
    }
    #endregion
}