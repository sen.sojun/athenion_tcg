﻿
public interface Showable
{
    void ShowUI();
}

public interface Hidable
{
    void HideUI();
}
