﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class Bot : MonoBehaviour 
{
    #region Public Properties
    public PlayerIndex PlayerIndex { get { return _playerIndex; } }
    public bool IsEnable { get { return _isEnable; } }

    public bool IsMyTurn 
    {
        get 
        {
            if (GameManager.Instance != null)
            {
                if (GameManager.Instance.CurrentPlayerIndex == PlayerIndex)
                {
                    return true;
                }
            }

            return false;
        }
    }
    public bool IsCanPlay
    {
        get
        {
            if (GameManager.Instance != null)
            {
                if (GameManager.Instance.Phase == GamePhase.Play)
                {
                    return true;
                }
            }

            return false;
        }
    }
    public bool IsQueueReady
    {
        get
        {
            if (GameManager.Instance != null)
            {
                if (   CommandQueue.Instance.IsReady 
                    && UIManager.Instance.GetUIQueueIsReady() 
                    && GameManager.Instance.IsCanNextPhase
                    && !GameManager.Instance.IsEndGame
                )
                {
                    
                    return true;
                }
            }

            return false;
        }
    }
    public bool IsSendingInGameMessage { get { return _isSendingInGameMessage; } }
    public bool IsCardInteract { get { return (_waitCardInteract != null || _isForceInteract); } }
    #endregion

    #region Private Properties
    protected PlayerIndex _playerIndex;
    protected bool _isEnable = false;
    protected bool _isSendingInGameMessage = false;
    protected bool _isForceInteract = false;
    protected WaitObject _waitCardInteract = null;
    protected List<CardDirectionType> _directionList = null;
    #endregion

    #region Update
    protected virtual void Update()
    {
        if (IsEnable && IsMyTurn)
        {
            switch (GameManager.Instance.Phase)
            {
                case GamePhase.Play:
                {
                    if (
                           GameManager.Instance.IsCanUseHandCard(PlayerIndex)    // Have enough AP
                        && GameManager.Instance.IsHaveEmptySlot()                // Have empty slot
                    )
                    {
                        SpawnHandCard();
                    }
                    else
                    {
                        EndTurn();
                    }
                }
                break;
            }
        }
    }
    #endregion

    #region Methods
    public void SetPlayerIndex(PlayerIndex playerIndex)
    {
        _playerIndex = playerIndex;
    }

    public void SetEnable(bool isEnable)
    {
        _isEnable = isEnable;
    }

    public void ReHand()
    {
        GameManager.Instance.OnSelectReHand(this.PlayerIndex, null);
    }

    public void SpawnHandCard()
    {
        if (IsMyTurn && IsCanPlay && IsQueueReady)
        {
            List<BattleCardData> handList = GameManager.Instance.Data.PlayerDataList[(int)PlayerIndex].CardList[CardZone.Hand];
            if (handList != null && handList.Count > 0)
            {
                int handIndex = UnityEngine.Random.Range(0, handList.Count);
                BattleCardData selectedCard = handList[handIndex];

                if (selectedCard.Type == CardType.Spell 
                 || selectedCard.Type == CardType.Spell_NotInDeck)
                {
                    // Spell
                    PlayCard(selectedCard.FullID, 0, 0);
                }
                else
                {
                    // Minion
                    List<int> emptySlotList = GameManager.Instance.Data.GetAllEmptySlot();
                    if (emptySlotList != null && emptySlotList.Count > 0)
                    {
                        int slotIndex = UnityEngine.Random.Range(0, emptySlotList.Count);
                        int slotID = emptySlotList[slotIndex];

                        PlayCard(selectedCard.FullID, slotID, 0);
                    }
                }
            }
        }
    }

    protected bool PlayCard(string cardID, int slotID, float duration = 0.0f)
    {
        List<BattleCardData> handList = GameManager.Instance.Data.PlayerDataList[(int)this.PlayerIndex].CardList[CardZone.Hand];

        if (GameManager.Instance.IsValidSlotID(slotID))
        {
            bool isFound = false;
            for (int index = 0; index < handList.Count; ++index)
            {
                BattleCardData card = handList[index];

                if (card.FullID == cardID)
                {
                    isFound = true;
                    int uniqueID = card.UniqueID;

                    if (GameManager.Instance.IsCanUseCard(uniqueID))
                    {
                        if (card.Type == CardType.Spell || card.Type == CardType.Spell_NotInDeck)
                        {
                            // Spell
                            SpellData spell = card as SpellData;
                            if (spell.IsCanUse)
                            {
                                ShowPreUseCard(
                                      uniqueID
                                    , duration
                                    , false
                                    , delegate () 
                                    {
                                        GameManager.Instance.RpcRequestUseCard(
                                              -1
                                            , GameHelper.ConvertGamePhaseToByte(GameManager.Instance.Phase)
                                            , uniqueID
                                            , GameHelper.ConvertIntToByte(-1)
                                            , false
                                        );

                                        WaitInteract(2.0f, false, null);
                                    } 
                                );

                                return true;
                            }
                            else
                            {
                                Debug.LogFormat("Bot/PlayCard: Can't use spell {0}.", card.Name);
                            }
                        }
                        else
                        {
                            // Minion
                            if (GameManager.Instance.IsCanSpawnToken(slotID, uniqueID))
                            {
                                ShowPreUseCard(
                                      uniqueID
                                    , duration
                                    , false
                                    , delegate ()
                                    {
                                        Debug.LogFormat("AIBot RpcRequestUseCard {0} {1}", uniqueID, slotID);    

                                        GameManager.Instance.RpcRequestUseCard(
                                              -1
                                            , GameHelper.ConvertGamePhaseToByte(GameManager.Instance.Phase)
                                            , uniqueID
                                            , GameHelper.ConvertIntToByte(slotID)
                                            , false
                                        );
                                    }
                                );

                                return true;
                            }
                            else
                            {
                                Debug.LogFormat("Bot/PlayCard: Can't use spell {0}.", card.Name);
                            }
                        }
                    }
                    else
                    {
                        Debug.LogFormat("Bot/PlayCard: Can't use {0}.", card.Name);
                    }

                    return false;
                }
            }

            if (!isFound)
            {
                Debug.LogFormat("Bot/PlayCard: Not found card {0}.", cardID);
            }
        }
        else
        {
            Debug.LogFormat("Bot/PlayCard: Invalid slot {0}.", slotID);
        }

        return false;
    }

    protected void HoverHandCard(float duration, UnityAction onComplete)
    {
        System.Random r = new System.Random();
        BattlePlayerData battlePlayerData = GameManager.Instance.Data.PlayerDataList[(int)this.PlayerIndex];
        List<BattleCardData> handList = battlePlayerData.CardList[CardZone.Hand];

        if (handList != null && handList.Count > 0)
        {
            int cardIndex = r.Next(0, handList.Count);
            int uniqueID = handList[cardIndex].UniqueID;

            HoverCard(uniqueID, duration, onComplete);
        }
        else
        {
            onComplete?.Invoke();
        }
    }

    protected void HoverBattleFieldToken(float duration, UnityAction onComplete)
    {
        System.Random r = new System.Random();
        int playerIndex = r.Next(0, GameManager.Instance.PlayerIndexList.Count);
        BattlePlayerData battlePlayerData = GameManager.Instance.Data.PlayerDataList[playerIndex];
        List<BattleCardData> unitList = battlePlayerData.CardList[CardZone.Battlefield];

        if (unitList != null && unitList.Count > 0)
        {
            int cardIndex = r.Next(0, unitList.Count);
            int uniqueID = unitList[cardIndex].UniqueID;

            HoverCard(uniqueID, duration, onComplete);
        }
        else
        {
            onComplete?.Invoke();
        }
    }

    protected void HoverCard(int uniqueID, float duration, UnityAction onComplete)
    {
        StopCardInteract(true);

        if (duration > 0.0f)
        {
            // Show
            UIManager.Instance.ReceivePlayerHoverCard(uniqueID, true);

            UtilityItem item = UtilityPool.Instance.CreateItem(UtilityPool.UtilityType.WaitObject);
            _waitCardInteract = item.GetComponent<WaitObject>();
            _waitCardInteract.StartTimer(
                  duration
                , delegate ()
                {
                    // Hide
                    UIManager.Instance.ReceivePlayerHoverCard(uniqueID, false);

                    onComplete?.Invoke();
                    _waitCardInteract = null;
                }
                , delegate (float timer)
                {
                }
            );
        }
        else
        {
            onComplete?.Invoke();
        }
    }

    protected void ShowPreUseCard(int uniqueID, float duration, bool isHideOnEnd, UnityAction onComplete)
    {
        StopCardInteract(true);

        if (duration > 0.0f)
        {
            // Show
            UIManager.Instance.ShowMoveToCardPreUse(uniqueID, true, null);

            UtilityItem item = UtilityPool.Instance.CreateItem(UtilityPool.UtilityType.WaitObject);
            _waitCardInteract = item.GetComponent<WaitObject>();
            _waitCardInteract.StartTimer(
                  duration
                , delegate ()
                {
                    // Hide
                    if (isHideOnEnd)
                    {
                        UIManager.Instance.ShowMoveToCardPreUse(
                              uniqueID
                            , false
                            , delegate() 
                            {
                                _waitCardInteract = null;
                                onComplete?.Invoke();
                            } 
                        );
                    }
                    else
                    {
                        _waitCardInteract = null;
                        onComplete?.Invoke();
                    }
                }
                , delegate (float timer)
                {
                }
            );
        }
        else
        {
            onComplete?.Invoke();
        }
    }

    protected void WaitInteract(float duration, bool isCallPrevOnComplete, UnityAction onComplete)
    {
        // do nothing
        StopCardInteract(isCallPrevOnComplete);

        if (duration > 0.0f)
        {
            UtilityItem item = UtilityPool.Instance.CreateItem(UtilityPool.UtilityType.WaitObject);
            _waitCardInteract = item.GetComponent<WaitObject>();
            _waitCardInteract.StartTimer(
                  duration
                , delegate ()
                {
                    _waitCardInteract = null;
                    onComplete?.Invoke();
                }
            );
        }
        else
        {
            onComplete?.Invoke();
        }
    }

    protected void StopCardInteract(bool isCallOnComplete)
    {
        if (_waitCardInteract != null)
        {
            _waitCardInteract.StopTimer(isCallOnComplete);
            _waitCardInteract = null;
        }
    }

    public void SendInGameMessage(HeroVoice heroVoiceIndex, UnityAction onComplete)
    {
        return; // disable bot send message.

        if (!_isSendingInGameMessage)
        {
            _isSendingInGameMessage = true;

            GameManager.Instance.RequestLocalSendMessage(
                  PlayerIndex
                , heroVoiceIndex
                , delegate()
                {
                    _isSendingInGameMessage = false;
                    onComplete?.Invoke();
                }
            );
        }
    }

    protected virtual void EndTurn()
    {
        if (GameManager.Instance.IsCanEndTurn(PlayerIndex))
        {
            GameManager.Instance.RequestEndTurn(
                  PlayerIndex
                , GameManager.Instance.SubTurn
                , true
            );
        }
    }

    public int GetEnemyHP()
    {
        switch (PlayerIndex)
        {
            case PlayerIndex.One: return GameManager.Instance.Data.PlayerDataList[(int)PlayerIndex.Two].CurrentHP;
            case PlayerIndex.Two: return GameManager.Instance.Data.PlayerDataList[(int)PlayerIndex.One].CurrentHP;
            default: return 0;
        }
    }

    public int GetEnemyAP()
    {
        switch (PlayerIndex)
        {
            case PlayerIndex.One: return GameManager.Instance.Data.PlayerDataList[(int)PlayerIndex.Two].CurrentAP;
            case PlayerIndex.Two: return GameManager.Instance.Data.PlayerDataList[(int)PlayerIndex.One].CurrentAP;
            default: return 0;
        }
    }

    public int GetMyHP()
    {
        return GameManager.Instance.Data.PlayerDataList[(int)PlayerIndex].CurrentHP;
    }

    public int GetMyAP()
    {
        return GameManager.Instance.Data.PlayerDataList[(int)PlayerIndex].CurrentAP;
    }

    public List<CardDirectionType> GetAllDirectionList()
    {
        if (_directionList == null)
        {
            _directionList = new List<CardDirectionType>(System.Enum.GetValues(typeof(CardDirectionType)).Cast<CardDirectionType>().ToList());
        }

        return _directionList;
    }

    public virtual List<PlayerIndex> SelectPlayer(Requester requester, List<PlayerIndex> playerIndexList, int count)
    {
        List<PlayerIndex> selectedList = new List<PlayerIndex>();
        List<PlayerIndex> choice = new List<PlayerIndex>(playerIndexList);

        if (choice != null && choice.Count > 0)
        {
            if (count >= playerIndexList.Count)
            {
                // Select All.
                selectedList = new List<PlayerIndex>(playerIndexList);
            }
            else
            {
                System.Random r = new System.Random();

                for (int i = 0; i < count; ++i)
                {
                    int index = r.Next(0, choice.Count);
                    selectedList.Add(choice[index]);
                    choice.RemoveAt(index);
                }
            }
        }

        return selectedList;
    }

    public virtual List<int> SelectMinionSlot(Requester requester, List<int> slotIDList, int count)
    {
        List<int> selectedList = new List<int>();
        List<int> choice = new List<int>(slotIDList);

        if (choice != null && choice.Count > 0)
        {
            if (count >= slotIDList.Count)
            {
                // Select All.
                selectedList = new List<int>(slotIDList);
            }
            else
            {
                System.Random r = new System.Random();

                for (int i = 0; i < count; ++i)
                {
                    int index = r.Next(0, choice.Count);
                    selectedList.Add(choice[index]);
                    choice.RemoveAt(index);
                }
            }
        }

        return selectedList;
    }

    public virtual List<int> SelectSlot(Requester requester, List<int> slotIDList, int count)
    {
        List<int> selectedList = new List<int>();
        List<int> choice = new List<int>(slotIDList);

        if (choice != null && choice.Count > 0)
        {
            if (count >= slotIDList.Count)
            {
                // Select All.
                selectedList = new List<int>(slotIDList);
            }
            else
            {
                System.Random r = new System.Random();

                for (int i = 0; i < count; ++i)
                {
                    int index = r.Next(0, choice.Count);
                    selectedList.Add(choice[index]);
                    choice.RemoveAt(index);
                }
            }
        }

        return selectedList;
    }

    public virtual List<int> SelectCard(Requester requester, List<int> uniqueIDList, int count)
    {
        List<int> selectedList = new List<int>();
        List<int> choice = new List<int>(uniqueIDList);

        if (choice != null && choice.Count > 0)
        {
            if (count >= uniqueIDList.Count)
            {
                // Select All.
                selectedList = new List<int>(uniqueIDList);
            }
            else
            {
                System.Random r = new System.Random();

                for (int i = 0; i < count; ++i)
                {
                    int index = r.Next(0, choice.Count);
                    selectedList.Add(choice[index]);
                    choice.RemoveAt(index);
                }
            }
        }

        return selectedList;
    }
    #endregion

    #region Event Methods
    public virtual void OnGotInGameMessage(HeroVoice heroVoiceIndex)
    {
        Debug.LogFormat("OnGotInGameMessage {0}", heroVoiceIndex);
    }

    public virtual void OnSpiritAttackOnPlayer(PlayerIndex playerIndex, int damage)
    {
        Debug.LogFormat("OnShowSpiritAttackOnPlayerComplete {0} {1}", playerIndex, damage);

        if (playerIndex == PlayerIndex)
        {
            OnGotSpiritAttack(damage);
        }
        else
        {
            OnEnemyGotSpiritAttack(damage);
        }
    }

    public virtual void OnGotSpiritAttack(int damage)
    {
        Debug.LogFormat("OnGotSpiritAttack {0}", damage);
    }

    public virtual void OnEnemyGotSpiritAttack(int damage)
    {
        Debug.LogFormat("OnGotSpiritAttack {0}", damage);
    }

    public virtual void OnPlayHandCard(int uniqueID)
    {
        Debug.LogFormat("OnPlayHandCard {0}", uniqueID);
    }
    #endregion

    #region Generate MatchPlayerData Methods
    /*
    public static MatchPlayerData CreateBotMatchPlayerData()
    {
        List<BotDBData> botList;
        BotDB.Instance.GetAllData(out botList);
        int index = UnityEngine.Random.Range(0, botList.Count);

        return CreateBotMatchPlayerData(botList[index].BotID);
    }

    public static MatchPlayerData CreateBotMatchPlayerData(string botID)
    {
        int index = UnityEngine.Random.Range(0, 6);
        CardElementType element = (CardElementType)(index + 1);

        return CreateBotMatchPlayerData(botID, element);
    }

    public static MatchPlayerData CreateBotMatchPlayerData(string botID, GameMode mode)
    {
        MatchPlayerData result = CreateBotMatchPlayerData(botID);
        string defaultName = result.PlayerInfo.DisplayName;

        BotDBData botDBData;
        BotDB.Instance.GetData(botID, out botDBData);

        string newName = mode == GameMode.Practice ? defaultName : botDBData.BotName;
        result.PlayerInfo.SetDisplayName(newName);
        return result;
    }

    public static MatchPlayerData CreateBotMatchPlayerData(CardElementType element)
    {
        List<BotDBData> botList;
        BotDB.Instance.GetAllData(out botList);
        int index = UnityEngine.Random.Range(0, botList.Count);

        return CreateBotMatchPlayerData(botList[index].BotID, element);
    }

    public static MatchPlayerData CreateBotMatchPlayerData(string botID, CardElementType element)
    {
        BotDBData botDBData = null;
        BotDB.Instance.GetData(botID, out botDBData);
        DeckData deck = DeckData.GetDefaultDeck("deck_bot", element);
        HeroData heroData = HeroData.CreateHero(deck.HeroID, PlayerIndex.Two);

        Debug.LogFormat("Bot deck. [{0}]", element.ToString());

        PlayerInfoData playerInfo = PlayerInfoData.CreateBotData(
              botDBData.PlayfabID
            , heroData.Name
            , DataManager.Instance.DefaultAvatarIDList[0]
            , DataManager.Instance.DefaultTitleIDList[0]
        );

        MatchPlayerData matchPlayerData = new MatchPlayerData(
              playerInfo
            , deck
        );

        return matchPlayerData;
    }
    */

    public static MatchPlayerData CreateBotMatchPlayerData(PlayerInfoData playerInfo, DeckData deck)
    {
        MatchPlayerData matchPlayerData = new MatchPlayerData(
              playerInfo
            , deck
        );

        return matchPlayerData;
    }

    public static BotDBData GetRandomBotDBData()
    {
        BotDBData botData = null;
        
        List<BotDBData> botList;
        if (BotDB.Instance.GetAllData(out botList))
        {
            if (botList != null && botList.Count > 0)
            {
                int botIndex = UnityEngine.Random.Range(0, botList.Count);
                botData = botList[botIndex];
            }
        }

        return botData;
    }

    public static BotProfileDBData GetRandomBotProfileDBData(BotLevel botLevel)
    {
        BotProfileDBData profileData = null;
        
        List<BotProfileDBData> botProfileList;
        if (BotProfileDB.Instance.GetBotProfile(botLevel, out botProfileList))
        {
            if (botProfileList != null && botProfileList.Count > 0)
            {
                int profileIndex = UnityEngine.Random.Range(0, botProfileList.Count);
                profileData = botProfileList[profileIndex];
            }
        }

        return profileData;
    }

    public static MatchPlayerData CreatePracticeBotMatchPlayerData(string heroID, BotLevel botLevel)
    {
        List<BotProfileDBData> botProfileList;
        BotProfileDB.Instance.GetBotProfile(heroID, botLevel, out botProfileList);

        int index = UnityEngine.Random.Range(0, botProfileList.Count);
        BotProfileDBData data = botProfileList[index];

        HeroData heroData = HeroData.CreateHero(heroID, PlayerIndex.Two);
        CardListData cardList;
        DeckDB.Instance.GetPracticeDeck(heroID, botLevel, 1, out cardList);

        DeckData deck = new DeckData(
            data.DeckID
            , heroData.GetName()
            , heroData.ElementID
            , heroData.ID
            , DataManager.Instance.DefaultCardBackIDList[0]
            , DataManager.Instance.DefaultDockIDList[0]
            , cardList
        );

        PlayerInfoData playerInfo = PlayerInfoData.CreateBotData("BOT", heroData.Name, DataManager.Instance.DefaultAvatarIDList[0], DataManager.Instance.DefaultTitleIDList[0]);
        MatchPlayerData matchPlayerData = new MatchPlayerData(
              playerInfo
            , deck
        );

        return matchPlayerData;
    }

    public static MatchPlayerData CreateNewbieBotCasualMatchPlayerData(BotLevel botLevel, BotLevel deckLevel)
    {
        // Create Bot Info
        BotDBData botData = GetRandomBotDBData();

        // Create Deck
        BotProfileDBData profileData = GetRandomBotProfileDBData(botLevel);
        HeroData heroData = HeroData.CreateHero(profileData.HeroID, PlayerIndex.Two);
        CardListData cardList;
        DeckDB.Instance.GetPracticeDeck(profileData.HeroID, deckLevel, 1, out cardList);
        DeckData deck = new DeckData(
              profileData.DeckID
            , heroData.GetName()
            , heroData.ElementID
            , heroData.ID
            , DataManager.Instance.DefaultCardBackIDList[0]
            , DataManager.Instance.DefaultDockIDList[0]
            , cardList
        );

        PlayerInfoData playerInfo = PlayerInfoData.CreateBotData(
              "BOT"
            , botData.BotName
            , DataManager.Instance.DefaultAvatarIDList[0]
            , DataManager.Instance.DefaultTitleIDList[0]
        );

        return CreateBotMatchPlayerData(playerInfo, deck);
    }

    public static MatchPlayerData CreateCasualBotMatchPlayerData(BotLevel botLevel)
    {
        BotDBData botData = GetRandomBotDBData();
        BotProfileDBData profileData = GetRandomBotProfileDBData(botLevel);
        HeroData heroData = HeroData.CreateHero(profileData.HeroID, PlayerIndex.Two);

        CardListData cardList;
        DeckDB.Instance.GetPracticeDeck(profileData.HeroID, botLevel, 1, out cardList);

        DeckData deck = new DeckData(
              profileData.DeckID
            , heroData.GetName()
            , heroData.ElementID
            , heroData.ID
            , DataManager.Instance.DefaultCardBackIDList[0]
            , DataManager.Instance.DefaultDockIDList[0]
            , cardList
        );

        PlayerInfoData playerInfo = PlayerInfoData.CreateBotData(
              "BOT"
            , botData.BotName
            , DataManager.Instance.DefaultAvatarIDList[0]
            , DataManager.Instance.DefaultTitleIDList[0]
        );

        MatchPlayerData matchPlayerData = new MatchPlayerData(
              playerInfo
            , deck
        );

        return matchPlayerData;
    }

    public static MatchPlayerData CreateBotEventBattleMatchPlayerData(EventBattleDBData data)
    {
        HeroData heroData = HeroData.CreateHero(data.Deck.HeroID, PlayerIndex.Two);

        PlayerInfoData playerInfo = PlayerInfoData.CreateBotData(
            "EVENT_BOT"
            , heroData.Name
            , data.AvatarID
            , data.TitleID
        );

        MatchPlayerData matchPlayerData = new MatchPlayerData(
              playerInfo
            , DataManager.Instance.GetEventBattleDeck(data.ProfileID)
        );

        return matchPlayerData;
    }
    #endregion
}
