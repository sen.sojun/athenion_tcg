﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class FixedBot : Bot
{
    #region Update
    // Update is called once per frame
    protected override void Update()
    {
        if (IsEnable && IsMyTurn)
        {
            switch (GameManager.Instance.Phase)
            {
                case GamePhase.Play: OnPlayPhase(); break;
            }
        }
    }

    #endregion

    #region Methods
    protected abstract void OnPlayPhase();

    public bool IsSlotEmpty(int slotID)
    {
        return GameManager.Instance.IsSlotEmpty(slotID);
    }

    public int GetRandomEmptySlot()
    {
        List<SlotData> slotList = GameManager.Instance.GetAllEmptySlots();
        if (slotList == null || slotList.Count == 0)
            return -1;
        return slotList[UnityEngine.Random.Range(0, slotList.Count - 1)].SlotID;
    }

    public int FindMostTopLeftEmptySlot()
    {
        for (int i = 0; i < GameManager.Instance.BoardHeight * GameManager.Instance.BoardWidth; i++)
        {
            if (IsSlotEmpty(i))
            {
                return i;
            }
        }
        return -1;
    }

    public int FindMostBotLeftEmptySlot()
    {
        int slotID = 0;
        for (int row = GameManager.Instance.BoardHeight - 1; row >= 0; --row)
        {
            for (int column = 0; column < GameManager.Instance.BoardWidth; ++column)
            {
                slotID = (row * GameManager.Instance.BoardHeight) + column;

                if (IsSlotEmpty(slotID))
                {
                    return slotID;
                }
            }
        }

        return -1;
    }

    public int FindOpponentTokenAdjacent(int slotID, PlayerIndex opponentIndex)
    {
        int found = -1;
        SlotData currentSlot = GameManager.Instance.Data.SlotDatas[slotID];
        foreach (CardDirectionType dir in GetAllDirectionList())
        {
            SlotData slot;
            if (currentSlot.GetAdjacentSlot(dir, out slot))
            {
                MinionData minion;
                if (!slot.IsSlotEmpty && GameManager.Instance.FindMinionBattleCard(slot.BattleCardUniqueID, out minion))
                {
                    if (minion.Owner == opponentIndex)
                    {
                        found++;
                    }
                }
            }
        }
        return found;
    }

    public int FindFirstAdjacentSlot(int slotID)
    {
        SlotData currentSlot = GameManager.Instance.Data.SlotDatas[slotID];
        foreach (CardDirectionType dir in GetAllDirectionList())
        {
            SlotData slot;
            if (currentSlot.GetAdjacentSlot(dir, out slot))
            {
                if (slot.IsSlotEmpty)
                    return slot.SlotID;
            }
        }
        return -1;
    }
    #endregion
    
}
