﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AT_002AdventureBot : AdventureBot
{
    public override IEnumerator OnBotPlay(Action onComplete)
    {
        switch (GameManager.Instance.SubTurn)
        {
            case 2:
                {
                    PlayCard("C0129", GameHelper.SlotCodeToSlotID("2D"));
                    yield return new WaitForSeconds(2.5f);
                    EndTurn();
                }
                break;
            case 4:
                //Create AIBot level: Hard to play from this turn
                CardElementType cardElementType = GameManager.Instance.Data.PlayerDataList[(int)this.PlayerIndex].Hero.ElementType;
                BotWeight weight = AIBot.GetBotWeight(GameManager.Instance.MatchData.Property.AdventureStageData.GetOpponentBotLevel(), cardElementType);

                weight.WAtk_Attack = 20000;
                weight.WAtk_Battle = 20000;
                weight.WAtk_BattleAndKill = 20000;
                weight.WAtk_BattleAndBothKill = 20000;
                weight.WAtk_BattleAndDeath = 20000;

                GameManager.Instance.CreateBot(PlayerIndex, false, weight, true);
                SetEnable(false);
                Destroy(gameObject);
                break;

            default:
                EndTurn();
                break;
        }

        onComplete?.Invoke();
        yield break;
    }
}
