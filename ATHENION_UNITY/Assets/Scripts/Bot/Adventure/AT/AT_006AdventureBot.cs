﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AT_006AdventureBot : AdventureBot
{
    public override IEnumerator OnBotPlay(Action onComplete)
    {
        switch (GameManager.Instance.SubTurn)
        {
            case 1:
                {
                    PlayCard("C0046", GameHelper.SlotCodeToSlotID("3D"));
                    yield return new WaitForSeconds(2.5f);
                    EndTurn();
                }
                break;
            case 3:
                {
                    PlayCard("C0030", GameHelper.SlotCodeToSlotID("1D"));
                    yield return new WaitForSeconds(2.5f);
                    EndTurn();
                }
                break;
            case 5:
                {
                    CardElementType cardElementType = GameManager.Instance.Data.PlayerDataList[(int)this.PlayerIndex].Hero.ElementType;
                    BotWeight weight = AIBot.GetBotWeight(GameManager.Instance.MatchData.Property.AdventureStageData.GetOpponentBotLevel(), cardElementType);
                    GameManager.Instance.CreateBot(PlayerIndex, false, weight, true);
                    SetEnable(false);
                    Destroy(gameObject);
                }
                break;

            default:
                EndTurn();
                break;
        }

        yield return new WaitForSeconds(1);
        onComplete?.Invoke();
        yield break;
    }


    public override List<int> SelectSlot(Requester requester, List<int> slotIDList, int count)
    {
        switch (GameManager.Instance.SubTurn)
        {
            case 3:
                {
                    List<int> selectedList = new List<int>();
                    selectedList.Add(GameHelper.SlotCodeToSlotID("2C"));
                    return selectedList;
                } 
        }
        return base.SelectSlot(requester, slotIDList, count);
    }

}
