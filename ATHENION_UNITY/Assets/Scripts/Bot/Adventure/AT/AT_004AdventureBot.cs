﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AT_004AdventureBot : AdventureBot
{
    public override IEnumerator OnBotPlay(Action onComplete)
    {
        switch (GameManager.Instance.SubTurn)
        {
            case 1:
                {
                    PlayCard("C0028", GameHelper.SlotCodeToSlotID("2B"));
                    yield return new WaitForSeconds(2.5f);
                    EndTurn();
                }
                break;

            case 3:
                {
                    PlayCard("C0114", GameHelper.SlotCodeToSlotID("1C"));
                    yield return new WaitForSeconds(2.5f);
                    PlayCard("C0114", GameHelper.SlotCodeToSlotID("2C"));
                    yield return new WaitForSeconds(2.5f);
                    EndTurn();
                }
                break;
            case 5:
                {
                    CardElementType cardElementType = GameManager.Instance.Data.PlayerDataList[(int)this.PlayerIndex].Hero.ElementType;
                    BotWeight weight = AIBot.GetBotWeight(GameManager.Instance.MatchData.Property.AdventureStageData.GetOpponentBotLevel(), cardElementType);
                    GameManager.Instance.CreateBot(PlayerIndex, false, weight, true);
                    SetEnable(false);
                    Destroy(gameObject);
                }
                break;
            default:
                EndTurn();
                break;
        }

        yield return new WaitForSeconds(1);
        onComplete?.Invoke();
        yield break;
    }

}
