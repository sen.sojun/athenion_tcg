﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AdventureBot : FixedBot
{
    protected AdventureController Controller
    {
        get
        {
            if (_controller == null)
            {
                _controller = GameManager.Instance.Controller as AdventureController;
            }
            return _controller;
        }
    }
    private AdventureController _controller;

    public bool IsBotCanAction { get { return Controller.ControllerData.IsBotCanAction; } }
    private Coroutine _onPlayTutorialRoutine = null;


    protected override void OnPlayPhase()
    {
        if (GameManager.Instance.IsCanNextPhase && IsBotCanAction)
        {
            if (
                   GameManager.Instance.IsCanUseHandCard(PlayerIndex)    // Have enough AP
                && GameManager.Instance.IsHaveEmptySlot()                // Have empty slot
            )
            {
                OnPlay();
            }
        }
    }

    private void OnPlay()
    {
        if (_onPlayTutorialRoutine == null)
        {
            _onPlayTutorialRoutine = GameManager.Instance.StartCoroutine(OnBotPlay(() => _onPlayTutorialRoutine = null));
        }
    }

    public abstract IEnumerator OnBotPlay(Action onComplete);
}
