﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialBot : FixedBot
{
    #region Properties
    public GameMode TutorialMode { get; private set; }
    public bool IsBotCanAction { get { return tutorialController.ControllerData.IsBotCanAction; } }
    #endregion

    #region Readonly Properties
    StoryGameController tutorialController = GameManager.Instance.Controller as StoryGameController;
    #endregion

    public void SetTutorialMode(GameMode mode)
    {
        TutorialMode = mode;
        _onPlayTutorialRoutine = null;
    }

    protected override void OnPlayPhase()
    {
        if (GameManager.Instance.IsCanNextPhase && IsBotCanAction)
        {
            if (
                   GameManager.Instance.IsCanUseHandCard(PlayerIndex)    // Have enough AP
                && GameManager.Instance.IsHaveEmptySlot()                // Have empty slot
            )
            {
                switch (TutorialMode)
                {
                    case GameMode.Tutorial_1:
                        {
                            OnPlayTutorial_1();
                        }
                        break;

                    case GameMode.Tutorial_2:
                        {
                            OnPlayTutorial_2();
                        }
                        break;

                    case GameMode.Tutorial_3:
                        {
                            OnPlayTutorial_3();
                        }
                        break;
                    case GameMode.Tutorial_4:
                        {
                            OnPlayTutorial_4();
                        }
                        break;
                }
            }
        }
    }

    #region TUT 1

    protected void OnPlayTutorial_1()
    {
        switch (GameManager.Instance.SubTurn)
        {
            case 1: // T1-A002
                {
                    switch (tutorialController.ControllerData.TutorialIndex)
                    {
                        case 0:
                            {
                                tutorialController.ControllerData.TutorialIndex = 1;
                                PlayCard("CT0569", GameHelper.SlotCodeToSlotID("2B"));
                                EndTurn();
                            }
                            break;
                    }
                }
                break;

            case 3: // T1-A005
                {
                    PlayCard("CT0571", GameHelper.SlotCodeToSlotID("2D"));
                    EndTurn();
                }
                break;

            case 5: // T1-A009
                {
                    PlayCard("CT0573", GameHelper.SlotCodeToSlotID("1D"));

                    EndTurn();
                }
                break;

            case 7: // T1-A009
                {
                    PlayCard("CT0575", GameHelper.SlotCodeToSlotID("2C"));

                    EndTurn();
                }
                break;
            case 9:
                {
                    PlayCard("CT0578", GameHelper.SlotCodeToSlotID("3D"));
                    EndTurn();
                }
                break; 
            case 11:
                {
                    CardElementType cardElementType = GameManager.Instance.Data.PlayerDataList[(int)this.PlayerIndex].Hero.ElementType;
                    BotWeight weight = AIBot.GetBotWeight(BotLevel.Hard, cardElementType);

                    weight.WAtk_Attack = 20000;
                    weight.WAtk_Battle = 20000;
                    weight.WAtk_BattleAndKill = 20000;
                    weight.WAtk_BattleAndBothKill = 20000;
                    weight.WAtk_BattleAndDeath = 20000;

                    GameManager.Instance.CreateBot(PlayerIndex, false, weight, true);
                    SetEnable(false);
                    Destroy(gameObject);
                }
                break;
            default:
                {
                    EndTurn();
                }
                break;
        }
    }

    #endregion

    #region TUT 2

    private Coroutine _onPlayTutorialRoutine = null;
    protected void OnPlayTutorial_2()
    {
        if (_onPlayTutorialRoutine == null)
        {
            _onPlayTutorialRoutine = GameManager.Instance.StartCoroutine(OnBotPlayTutorial2());
        }
    }
    IEnumerator OnBotPlayTutorial2()
    {
        switch (GameManager.Instance.SubTurn)
        {
            case 1:
                {
                    PlayCard("CT0579", GameHelper.SlotCodeToSlotID("3C"));
                    EndTurn();
                }
                break;

            case 3:
                {
                    PlayCard("CT0581", GameHelper.SlotCodeToSlotID("2B"));
                    EndTurn();
                }
                break;

            case 5:
                {
                    PlayCard("CT0583", GameHelper.SlotCodeToSlotID("1C"));
                    EndTurn();
                }
                break;
            case 7:
                {
                    PlayCard("CT0581", GameHelper.SlotCodeToSlotID("2B"));
                    EndTurn();
                }
                break;
            case 9:
                {
                    PlayCard("CT0586", GameHelper.SlotCodeToSlotID("3A"));
                    yield return new WaitForSeconds(2.5f);
                    PlayCard("CT0587", GameHelper.SlotCodeToSlotID("4A"));
                    yield return new WaitForSeconds(1f);
                    EndTurn();
                }
                break;
            case 11:
                {
                    CardElementType cardElementType = GameManager.Instance.Data.PlayerDataList[(int)this.PlayerIndex].Hero.ElementType;
                    BotWeight weight = AIBot.GetBotWeight(BotLevel.Hard, cardElementType);

                    weight.WAtk_Attack = 20000;
                    weight.WAtk_Battle = 20000;
                    weight.WAtk_BattleAndKill = 20000;
                    weight.WAtk_BattleAndBothKill = 20000;
                    weight.WAtk_BattleAndDeath = 20000;

                    GameManager.Instance.CreateBot(PlayerIndex, false, weight, true);
                    SetEnable(false);
                    Destroy(gameObject);
                }
                break;
            default:
                EndTurn();
                break;
        }
        _onPlayTutorialRoutine = null;
        yield break;
    }

    #endregion

    #region TUT 3

    protected void OnPlayTutorial_3()
    {
        if (_onPlayTutorialRoutine == null)
        {
            _onPlayTutorialRoutine = GameManager.Instance.StartCoroutine(OnBotPlayTutorial3());
        }
    }
    IEnumerator OnBotPlayTutorial3()
    {
        switch (GameManager.Instance.SubTurn)
        {
            case 1:
                {
                    PlayCard("CT0626", GameHelper.SlotCodeToSlotID("1A"));
                    yield return new WaitForSeconds(2f);
                    PlayCard("CT0626", GameHelper.SlotCodeToSlotID("1D"));
                    yield return new WaitForSeconds(1f);
                    EndTurn();
                }
                break;

            case 3:
                {
                    PlayCard("CT0593", GameHelper.SlotCodeToSlotID("2A"));
                    yield return new WaitForSeconds(2f);
                    PlayCard("CT0598", GameHelper.SlotCodeToSlotID("2C"));
                    yield return new WaitForSeconds(2f);
                    EndTurn();
                }
                break;

            case 5:
                {
                    PlayCard("CT0595", GameHelper.SlotCodeToSlotID("1A"));
                    yield return new WaitForSeconds(2f);
                    PlayCard("CT0626", GameHelper.SlotCodeToSlotID("2B"));
                    yield return new WaitForSeconds(2f);
                    PlayCard("CT0601", GameHelper.SlotCodeToSlotID("1C"));
                    yield return new WaitForSeconds(1f);
                    EndTurn();
                }
                break;
            case 7:
                {
                    PlayCard("CT0596", GameHelper.SlotCodeToSlotID("2A"));
                    yield return new WaitForSeconds(3f);
                    PlayCard("CT0594", GameHelper.SlotCodeToSlotID("3C"));
                    yield return new WaitForSeconds(1f);
                    EndTurn();
                }
                break;
            case 9:
                {
                    PlayCard("CT0628", GameHelper.SlotCodeToSlotID("4A"));
                    yield return new WaitForSeconds(1f);
                    EndTurn();
                }
                break;
            case 11:
                {
                    CardElementType cardElementType = GameManager.Instance.Data.PlayerDataList[(int)this.PlayerIndex].Hero.ElementType;
                    BotWeight weight = AIBot.GetBotWeight(BotLevel.Medium, cardElementType);
                    GameManager.Instance.CreateBot(PlayerIndex, false, weight, true);
                    SetEnable(false);
                    Destroy(gameObject);
                }
                break;

            default:
                {
                    EndTurn();
                }
                break;
        }
        _onPlayTutorialRoutine = null;
        yield break;
    }

    #endregion

    #region TUT 4

    protected void OnPlayTutorial_4()
    {
        if (_onPlayTutorialRoutine == null)
        {
            _onPlayTutorialRoutine = GameManager.Instance.StartCoroutine(OnBotPlayTutorial4());
        }
    }
    IEnumerator OnBotPlayTutorial4()
    {
        switch (GameManager.Instance.SubTurn)
        {
            case 1:
                {
                    CardElementType cardElementType = GameManager.Instance.Data.PlayerDataList[(int)this.PlayerIndex].Hero.ElementType;
                    BotWeight weight = AIBot.GetBotWeight(BotLevel.Hard, cardElementType);
                    var bot = GameManager.Instance.CreateBot(PlayerIndex, false, weight, true);
                    SetEnable(false);
                    //Destroy(gameObject);
                }
                break;
            case 3:
                {
                    int targetSlot = 0;
                    int mostLeft = FindMostTopLeftEmptySlot();
                    if (mostLeft < 4)
                    {
                        targetSlot = mostLeft;
                    }
                    else
                    {
                        targetSlot = GetRandomEmptySlot();
                    }
                    Debug.Log(targetSlot);
                    PlayCard("CT0612", targetSlot);
                    yield return new WaitForSeconds(2f);

                    if (targetSlot < 3)
                        targetSlot += 1;
                    else if (targetSlot == 3)
                        targetSlot = 2;
                    if (IsSlotEmpty(targetSlot))
                    {
                        PlayCard("CT0647", targetSlot);
                    }
                    else
                    {
                        PlayCard("CT0647", GetRandomEmptySlot());
                    }
                    Debug.Log(targetSlot);

                    yield return new WaitForSeconds(2f);
                    EndTurn();
                }
                break;
            default:
                {
                    if (GameManager.Instance.SubTurn % 2 != 0)
                        break;
                    // EndTurn();
                }
                break;
        }
        _onPlayTutorialRoutine = null;
        yield break;
    }

    #endregion

    #region Override Methods
    public override List<PlayerIndex> SelectPlayer(Requester requester, List<PlayerIndex> playerIndexList, int count)
    {
        return base.SelectPlayer(requester, playerIndexList, count);
    }

    public override List<int> SelectMinionSlot(Requester requester, List<int> slotIDList, int count)
    {
        return base.SelectMinionSlot(requester, slotIDList, count);
    }

    public override List<int> SelectSlot(Requester requester, List<int> slotIDList, int count)
    {
        switch (TutorialMode)
        {
            case GameMode.Tutorial_3:
                {
                    switch (GameManager.Instance.SubTurn)
                    {
                        case 5:
                            {
                                List<int> selectedList = new List<int>();
                                selectedList.Add(GameHelper.SlotCodeToSlotID("1B"));
                                return selectedList;
                            }
                        case 7:
                            {
                                List<int> selectedList = new List<int>();
                                selectedList.Add(GameHelper.SlotCodeToSlotID("3A"));
                                selectedList.Add(GameHelper.SlotCodeToSlotID("3B"));
                                return selectedList;
                            }
                    }
                }
                break;
        }

        return base.SelectSlot(requester, slotIDList, count);
    }

    public override List<int> SelectCard(Requester requester, List<int> uniqueIDList, int count)
    {
        return base.SelectCard(requester, uniqueIDList, count);
    }
    #endregion
}
