﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

public class BoardValue
{
    #region Public Properties
    public float MinValue { get { return _min; } }
    public float MaxValue { get { return _max; } }
    public int Count { get { if (_values != null) { return _values.Length; } else { return 0; } } }
    #endregion

    #region Private Properties
    private float[] _values;

    private float _min = float.MaxValue;
    private float _max = float.MinValue;
    #endregion

    #region Constructors
    public BoardValue(int size)
    {
        _values = new float[size];

        _min = float.MaxValue;
        _max = float.MinValue;
    }

    public BoardValue(int sizeX, int sizeY)
    {
        _values = new float[sizeX * sizeY];

        _min = float.MaxValue;
        _max = float.MinValue;
    }

    public BoardValue(BoardValue boardValue)
    {
        _values = new float[boardValue._values.GetLength(0)];

        for (int i = 0; i < _values.GetLength(0); ++i)
        {
            _values[i] = boardValue._values[i];
        }

        _min = boardValue._min;
        _max = boardValue._max;
    }
    #endregion

    #region Methods
    public bool GetValue(int index, out float value)
    {
        if (_values != null && index < _values.Length)
        {
            value = _values[index];
            return true;
        }

        value = 0.0f;
        return false;
    }

    public void SetValue(int index, float value)
    {
        if (_values != null)
        {
            if (index >= 0 && index < _values.GetLength(0))
            {
                _values[index] = value;

                _max = Mathf.Max(_max, value);
                _min = Mathf.Min(_min, value);
            }
        }
    }

    public void AddValue(int index, float value)
    {
        if (_values != null)
        {
            if (index >= 0 && index < _values.GetLength(0))
            {
                _values[index] += value;

                _max = Mathf.Max(_max, _values[index]);
                _min = Mathf.Min(_min, _values[index]);
            }
        }
    }

    public float FindMin()
    {
        _min = float.MaxValue;

        for (int i = 0; i < _values.GetLength(0); ++i)
        {
            _min = Mathf.Min(_values[i], _min);
        }

        return _min;
    }

    public float FindMax()
    {
        _max = float.MinValue;

        for (int i = 0; i < _values.GetLength(0); ++i)
        {
            _max = Mathf.Max(_values[i], _max);
        }

        return _max;
    }

    public List<int> FindMinIndex()
    {
        List<int> result = new List<int>();

        for (int i = 0; i < _values.GetLength(0); ++i)
        {
            if (_values[i] == _min)
            {
                result.Add(i);
            }
        }

        return result;
    }

    public List<int> FindMaxIndex()
    {
        List<int> result = new List<int>();

        for (int i = 0; i < _values.GetLength(0); ++i)
        {
            if (_values[i] == _max)
            {
                result.Add(i);
            }
        }

        return result;
    }

    public override string ToString()
    {
        string result = "";

        for (int row = 0; row < GameManager.Instance.BoardHeight; ++row)
        {
            for (int column = 0; column < GameManager.Instance.BoardWidth; ++column)
            {
                int index = row * GameManager.Instance.BoardWidth + column;
                if (_values[index] < -999999f)
                {
                    result += string.Format("[<color=red>X</color>]");
                }
                else
                {
                    result += string.Format("[{0:F2}]", _values[index]);
                }
            }
            result += "\n";
        }

        return result;
    }
    #endregion
}

public class CardValue
{
    #region Public Properties
    public BattleCardData Card { get { return _card; } }
    public BoardValue BoardValue { get { return _value; } }
    #endregion

    #region Private Properties
    private BattleCardData _card = null;
    private BoardValue _value = null;
    #endregion

    #region Constructors
    public CardValue(BattleCardData card, BoardValue boardValue)
    {
        _card = card;
        _value = boardValue;
    }

    public CardValue(CardValue cardValue)
    {
        _card = cardValue._card;
        _value = cardValue._value;
    }
    #endregion

    #region Methods
    #endregion
}

public class PlayResult
{
    #region Public Properties
    public BattleCardData Card { get { return _card; } }
    public int SlotID { get { return _slotID; } }
    public float SlotValue { get { return _slotValue; } }
    #endregion

    #region Private Properties
    private BattleCardData _card;
    private int _slotID;
    private float _slotValue;
    #endregion

    #region Constructors
    public PlayResult()
    {
        _card = null;
        _slotID = -1;
        _slotValue = float.MinValue;
    }

    public PlayResult(BattleCardData card, int slotID, float slotValue)
    {
        _card = card;
        _slotID = slotID;
        _slotValue = slotValue;
    }
    #endregion

    #region Methods
    public override string ToString()
    {
        string result = "";

        if (_card != null)
        {
            result = string.Format("{0} @[{1}]:({2})", _card.Name, _slotID, _slotValue);
        }

        return result;
    }
    #endregion
}

public class BotWeight
{
    // Setting
    public float DecisionPercent;

    // Arrow
    public float WArrow_Active;
    public float WArrow_Inactive;
    public float WArrow_ShowWeakness;
    public float WArrow_HideWeakness;
    public float WArrow_ConnectLink;

    // Attacking
    public float WAtk_AttackAndKill;
    public float WAtk_Attack;
    public float WAtk_BattleAndKill;
    public float WAtk_BattleAndBothKill;
    public float WAtk_Battle;
    public float WAtk_BattleAndDeath;
    public float WAtk_BeAttacked;
    public float WAtk_BeAttackedAndDeath;

    // Minion
    public float WMinion_SP;
    public float WMinion_AT;
    public float WMinion_HP;
    public float WMinion_Linker;

    public BotWeight(
          float decisionPercent
        , float wArrow_Active
        , float wArrow_Inactive
        , float wArrow_ShowWeakness
        , float wArrow_HideWeakness
        , float wArrow_ConnectLink

        , float wAtk_AttackAndKill
        , float wAtk_Attack
        , float wAtk_BattleAndKill
        , float wAtk_BattleAndBothKill
        , float wAtk_Battle
        , float wAtk_BattleAndDeath
        , float wAtk_BeAttack
        , float wAtk_BeAttackAndDeath

        , float wMinion_SP
        , float wMinion_AT
        , float wMinion_HP
        , float wMinion_Linker
    )
    {
        DecisionPercent = decisionPercent;
        WArrow_Active = wArrow_Active;
        WArrow_Inactive = wArrow_Inactive;
        WArrow_ShowWeakness = wArrow_ShowWeakness;
        WArrow_HideWeakness = wArrow_HideWeakness;
        WArrow_ConnectLink = wArrow_ConnectLink;

        WAtk_AttackAndKill = wAtk_AttackAndKill;
        WAtk_Attack = wAtk_Attack;
        WAtk_BattleAndKill = wAtk_BattleAndKill;
        WAtk_BattleAndBothKill = wAtk_BattleAndBothKill;
        WAtk_Battle = wAtk_Battle;
        WAtk_BattleAndDeath = wAtk_BattleAndDeath;
        WAtk_BeAttacked = wAtk_BeAttack;
        WAtk_BeAttackedAndDeath = wAtk_BeAttackAndDeath;

        WMinion_SP = wMinion_SP;
        WMinion_AT = wMinion_AT;
        WMinion_HP = wMinion_HP;
        WMinion_Linker = wMinion_Linker;
    }
}

public class AIBot : Bot
{
    #region Enums
    public enum BattleResult
    {
        None
        , AttackAndKill
        , Attack
        , BattleAndKill
        , BattleAndBothKill
        , Battle
        , BattleAndDeath
        , BeAttacked
        , BeAttackedAndDeath
    }
    #endregion

    #region Public Properties
    public static float PreUseCardTime_Min = 0.5f;
    public static float PreUseCardTime_Max = 3.0f;

    public static float HoverCardTime_Min = 2.0f;
    public static float HoverCardTime_Max = 5.0f;

    public static float WaitInteractTime_Min = 1.0f;
    public static float WaitInteractTime_Max = 2.0f;

    public static float HoverHandCard_Weight = 4.0f;
    public static float HoverBattleFieldToken_Weight = 1.0f;
    public static float DoNothing_Weight = 5.0f;

    private BotWeight Weight { get { return _weight; } }
    private bool IsHaveDecisionTime { get { return _isHaveDecisionTime; } }
    private bool IsDontCareNegativeValue { get { return _isDontCareNegativeValue; } }
    #endregion

    #region Private Properties
    // Debug
    private static readonly bool _isShowLog = false;
    private static readonly int _maxLog = 10;
    private readonly float _widthMargin = 20.0f;
    private readonly float _heightMargin = 20.0f;

    private BotWeight _weight;
    private List<MinionData> _deathMarkList;
    private List<CardValue> _cardValues;
    private System.Random _r;
    private bool _isHaveDecisionTime = false;
    private bool _isDontCareNegativeValue = false;
    private float _decisionTimer = 0.0f;

    private float _decisionTime = 3.0f;
    private float _deltaDecisionTime = 7.0f;

    private int _activeSubTurn_Begin = -1;
    private int _activeSubTurn_End = -1;

    private Coroutine _playCardTask = null;
    private bool[] _deathSlot;
    private List<string> _logList;
    private StringBuilder _debugText;
    private Rect _debugTextRect;
    #endregion

    private void Awake()
    {
        _debugTextRect = new Rect(
              _widthMargin
            , _heightMargin
            , Screen.width - (2.0f * _widthMargin)
            , Screen.height - (2.0f * _heightMargin)
        );

        _r = new System.Random();
    }

    // Update is called once per frame
    protected override void Update()
    {
        if (IsEnable)
        {
            switch (GameManager.Instance.Phase)
            {
                case GamePhase.Begin: OnBeginPhase(); break;
                case GamePhase.Play: OnPlayPhase(); break;
                case GamePhase.End: OnEndPhase(); break;
            }
        }
    }

    #region Methods
    protected void OnBeginPhase()
    {
        if (IsMyTurn && _activeSubTurn_Begin < GameManager.Instance.SubTurn)
        {
            _activeSubTurn_Begin = GameManager.Instance.SubTurn;

            _deathMarkList = new List<MinionData>();
            _deathSlot = new bool[GameManager.Instance.BoardHeight * GameManager.Instance.BoardWidth];

            SetRandomDecisionTime();
        }
    }

    protected void OnPlayPhase()
    {
        if (IsMyTurn)
        {
            if (IsHaveDecisionTime)
            {
                _decisionTimer -= Time.deltaTime;

                if (_decisionTimer <= 0.0f)
                {
                    if (GameManager.Instance.IsCanNextPhase)
                    {
                        if (_decisionTimer <= 0.0f)
                        {
                            SetRandomDecisionTime();
                        }

                        TryPlayCard();
                    }
                }
                else
                {
                    if (!IsCardInteract)
                    {
                        PreformHoverCard();
                    }
                }
            }
            else
            {
                TryPlayCard();
            }
        }
        else
        {
            if (IsHaveDecisionTime)
            {
                if (!IsCardInteract)
                {
                    PreformHoverCard();
                }
            }
        }
    }

    protected void OnEndPhase()
    {
        /*
        if (IsMyTurn && _activeSubTurn_End < GameManager.Instance.SubTurn)
        {
            _activeSubTurn_End = GameManager.Instance.SubTurn;

            _deathMarkList = new List<MinionData>();
            _deathSlot = new bool[GameManager.Instance.BoardHeight * GameManager.Instance.BoardWidth];
        }
        */
    }

    public void SetWeight(BotWeight botWeight)
    {
        _weight = botWeight;
    }

    private void TryPlayCard()
    {
        if (
               GameManager.Instance.IsCanUseHandCard(PlayerIndex)    // Have enough AP
            && GameManager.Instance.IsHaveEmptySlot()                // Have empty slot
            && CommandQueue.Instance.IsReady
            && (_playCardTask == null)
        )
        {
            MarkDeath();

            List<PlayResult> playResultList = FindBestCard();
            if (playResultList != null && playResultList.Count > 0)
            {
                int index = _r.Next(0, playResultList.Count);
                SpawnHandCard(
                      playResultList[index]
                    , delegate(bool isSuccess) 
                    {
                        if (isSuccess)
                        {
                            BattleCardData card = playResultList[index].Card;
                            int slotID = playResultList[index].SlotID;

                            Debug.LogFormat("AIBot: Played card. {0}({1})", card.Name, slotID);
                        }
                        else
                        {
                            Debug.Log("AIBot: Failed to SpawnHandCard.");
                            EndTurn();
                        }
                    }
                );
            }
            else
            {
                Debug.Log("AIBot: Nothing to play.");
                EndTurn();
            }
        }
        else
        {
            if (
                _playCardTask == null
                && CommandQueue.Instance.IsReady
               )
            {
                Debug.LogFormat(
                      "AIBot: Can't SpawnHandCard. {0} {1}"
                    , GameManager.Instance.IsCanUseHandCard(PlayerIndex) ? "" : "Can't use hand card."
                    , GameManager.Instance.IsHaveEmptySlot() ? "" : "No empty slot."
                );

                EndTurn();
            }
        }
    }

    private List<PlayResult> FindBestCard()
    {
        _cardValues = new List<CardValue>();
        List<PlayResult> result = new List<PlayResult>();

        if (GameManager.Instance.IsCanUseHandCard(this.PlayerIndex) && GameManager.Instance.IsHaveEmptySlot())
        {
            BattlePlayerData battlePlayerData = GameManager.Instance.Data.PlayerDataList[(int)this.PlayerIndex];

            List<BattleCardData> handList = battlePlayerData.CardList[CardZone.Hand];
            if (handList != null && handList.Count > 0)
            {
                // Calculate card value.
                foreach (BattleCardData handCard in handList)
                {
                    if (GameManager.Instance.IsCanUseCard(handCard.UniqueID))
                    {
                        BoardValue boardValue = CalculateCard(handCard);
                        if (boardValue != null)
                        {
                            CardValue cv = new CardValue(handCard, boardValue);
                            _cardValues.Add(cv);
                        }
                    }
                }

                // Find best hand card.
                float max = float.MinValue;
                float min = float.MaxValue;
                for (int index = 0; index < _cardValues.Count; ++index)
                {
                    float cardMax = _cardValues[index].BoardValue.FindMax();
                    if (max < cardMax)
                    {
                        max = cardMax;
                    }

                    float cardMin = _cardValues[index].BoardValue.FindMin();
                    if (min > cardMin)
                    {
                        min = cardMin;
                    }
                }

                // Find acceptable play result.
                float delta = (max - min);
                float acceptableValue = max - (delta * Weight.DecisionPercent);
                float slotValue = 0.0f;
                SlotData currentSlot = null;
                for (int index = 0; index < _cardValues.Count; ++index)
                {
                    for (int slotIndex = 0; slotIndex < _cardValues[index].BoardValue.Count; ++slotIndex)
                    {
                        currentSlot = GameManager.Instance.Data.SlotDatas[slotIndex];

                        if (currentSlot.IsSlotEmpty)
                        {
                            if (_cardValues[index].BoardValue.GetValue(slotIndex, out slotValue))
                            {
                                if (slotValue >= acceptableValue)
                                {
                                    PlayResult playResult = new PlayResult(
                                          _cardValues[index].Card
                                        , slotIndex
                                        , slotValue
                                    );
                                    result.Add(playResult);
                                }
                            }
                        }
                    }
                }
            }
        }

        return result;
    }

    private BoardValue CalculateCard(BattleCardData card)
    {
        BoardValue boardValue = new BoardValue(GameManager.Instance.BoardWidth, GameManager.Instance.BoardHeight);

        CalScore(card, ref boardValue);

        return boardValue;
    }

    private void SpawnHandCard(PlayResult playResult, UnityAction<bool> onComplete)
    {
        if (_playCardTask != null)
        {
            StopCoroutine(_playCardTask);
            _playCardTask = null;
        }

        _playCardTask = StartCoroutine(OnSpawnHandCard(playResult, onComplete));
    }

    private IEnumerator OnSpawnHandCard(PlayResult playResult, UnityAction<bool> onComplete)
    {
        if (IsMyTurn && IsCanPlay)
        {
            while (true)
            {
                if (IsQueueReady)
                {
                    BattleCardData card = playResult.Card;
                    int slotID = playResult.SlotID;

                    float duration = GetPlayDuration();
                    bool isSuccess = PlayCard(card.FullID, slotID, duration);

                    _playCardTask = null;
                    onComplete?.Invoke(isSuccess);
                    yield break;
                }

                yield return new WaitForEndOfFrame();
            }
        }

        _playCardTask = null;
        onComplete?.Invoke(false);
        yield break;
    }

    private void PreformHoverCard()
    {
        Debug.Log("PreformHoverCard");

        float weight = HoverHandCard_Weight + HoverBattleFieldToken_Weight + DoNothing_Weight;
        float r_value = GameHelper.GetRandomFloat(0.0f, weight);

        if (r_value < HoverHandCard_Weight)
        {
            // hover hand card
            float duration = GetHoverDuration();
            HoverHandCard(
                  duration
                , delegate ()
                {
                    float waitTime = GetWaitDuration();
                    WaitInteract(waitTime, false, null);
                }
            );
        }
        else if (r_value < (HoverHandCard_Weight + HoverBattleFieldToken_Weight))
        {
            // hover token in battlefield
            float duration = GetHoverDuration();
            HoverBattleFieldToken(
                  duration
                , delegate ()
                {
                    float waitTime = GetWaitDuration();
                    WaitInteract(waitTime, false, null);
                }
            );
        }
        else
        {
            // do nothing
            float duration = GetWaitDuration();
            WaitInteract(duration, true, null);
        }
    }

    private void ShowLog(string text)
    {
        if (_isShowLog)
        {
            Debug.Log(text);

            if (_logList == null)
            {
                _logList = new List<string>();
            }
            _logList.Insert(0, text);

            if (_logList.Count > _maxLog)
            {
                _logList.RemoveAt(_logList.Count - 1);
            }
        }
    }

    private void MarkDeath()
    {
        if (_deathMarkList == null)
        {
            _deathMarkList = new List<MinionData>();
        }

        if (_deathSlot == null)
        {
            _deathSlot = new bool[GameManager.Instance.BoardHeight * GameManager.Instance.BoardWidth];
        }

        if (GameManager.Instance.AttackerList != null)
        {
            foreach (MinionData attacker in GameManager.Instance.AttackerList)
            {
                SlotData currentSlot = GameManager.Instance.Data.SlotDatas[attacker.SlotID];
                SlotData slot = null;
                MinionData minion = null;

                foreach (CardDirectionType dir in GetAllDirectionList())
                {
                    if (attacker.CardDir.IsDirection(dir))
                    {
                        if (currentSlot.GetAdjacentSlot(dir, out slot))
                        {
                            if (!slot.IsSlotEmpty && GameManager.Instance.FindMinionBattleCard(slot.BattleCardUniqueID, out minion))
                            {
                                if (minion != null && minion.Owner != attacker.Owner && attacker.ATKCurrent >= minion.HPCurrent)
                                {
                                    int slotID = minion.SlotID;
                                    if (!IsMarkDeath(minion) && GameManager.Instance.IsValidSlotID(slotID))
                                    {
                                        _deathMarkList.Add(minion);
                                        _deathSlot[slotID] = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private bool IsMarkDeath(MinionData minion)
    {
        if (_deathMarkList != null && _deathMarkList.Count > 0)
        {
            return _deathMarkList.Contains(minion);
        }

        return false;
    }

    public void SetHaveDecisionTime(bool isHaveDecisionTime)
    {
        _decisionTimer = 0.0f;
        _isHaveDecisionTime = isHaveDecisionTime;
    }

    public void SetIsDontCareNegativeValue(bool isDontCareNegativeValue)
    {
        _isDontCareNegativeValue = isDontCareNegativeValue;
    }

    private void SetRandomDecisionTime()
    {
        if (IsHaveDecisionTime)
        {
            float deltaTime = _deltaDecisionTime * (((float)_r.Next(0, 101)) / 100.0f);
            _decisionTimer = _decisionTime + deltaTime;
        }
        else
        {
            _decisionTimer = 0;
        }
    }

    protected override void EndTurn()
    {
        ShowLog("AIBot: EndTurn!");

        if (GameManager.Instance.IsCanEndTurn(PlayerIndex))
        {
            base.EndTurn();
        }
    }

    public override List<PlayerIndex> SelectPlayer(Requester requester, List<PlayerIndex> playerIndexList, int count)
    {
        return base.SelectPlayer(requester, playerIndexList, count);
    }

    public override List<int> SelectMinionSlot(Requester requester, List<int> slotIDList, int count)
    {
        if (slotIDList != null && slotIDList.Count > count)
        {
            // Have to decision.
            if (requester.Ability != null)
            {
                int damage = GetDoDamage(requester.Ability.EffectList);
                if (damage > 0)
                {
                    // Mark Death
                    MarkDeath();

                    // Dealing Damage
                    List<MinionData> minionSortedList = new List<MinionData>();
                    MinionData minion = null;
                    foreach (int slotID in slotIDList)
                    {
                        SlotData slot = GameManager.Instance.Data.SlotDatas[slotID];
                        if (!slot.IsSlotEmpty && GameManager.Instance.FindMinionBattleCard(slot.BattleCardUniqueID, out minion))
                        {
                            bool isAdd = false;
                            for (int index = 0; index < minionSortedList.Count; ++index)
                            {
                                if (minion.SpiritCurrent >= minionSortedList[index].SpiritCurrent && !IsMarkDeath(minion))
                                {
                                    minionSortedList.Insert(index, minion);
                                    isAdd = true;
                                    break;
                                }
                            }

                            if (!isAdd)
                            {
                                minionSortedList.Add(minion);
                            }
                        }
                    }

                    System.Random r = new System.Random();
                    List<MinionData> selectedList = new List<MinionData>();
                    for (int i = 0; i < count; ++i)
                    {
                        bool isSelect = false;

                        for (int j = 0; j < minionSortedList.Count; ++j)
                        {
                            if (minionSortedList[j].HPCurrent <= damage)
                            {
                                MinionData m = minionSortedList[j];
                                selectedList.Add(m);
                                minionSortedList.RemoveAt(j);
                                isSelect = true;
                                break;
                            }
                        }

                        if (!isSelect)
                        {
                            int index = r.Next(0, minionSortedList.Count);
                            MinionData m = minionSortedList[index];
                            selectedList.Add(m);
                            minionSortedList.RemoveAt(index);
                        }
                    }

                    // convert back to slot id list.
                    List<int> resultSlotIDList = new List<int>();
                    foreach (MinionData selectedMinion in selectedList)
                    {
                        resultSlotIDList.Add(selectedMinion.SlotID);
                    }

                    return resultSlotIDList;
                }
            }
        }

        return base.SelectMinionSlot(requester, slotIDList, count);
    }

    public override List<int> SelectSlot(Requester requester, List<int> slotIDList, int count)
    {
        return base.SelectSlot(requester, slotIDList, count);
    }

    public override List<int> SelectCard(Requester requester, List<int> uniqueIDList, int count)
    {
        return base.SelectCard(requester, uniqueIDList, count);
    }

    private int GetDoDamage(MinionData minion)
    {
        if (minion != null)
        {
            if (minion.AbilityList != null)
            {
                foreach (KeyValuePair<int, CardAbilityData> item in minion.AbilityList)
                {
                    int value = GetDoDamage(item.Value.EffectList);
                    if (value > 0)
                    {
                        return value;
                    }
                }
            }
        }

        return 0;
    }

    private int GetDoDamage(List<EffectData> effectList)
    {
        if (effectList != null && effectList.Count > 0)
        {
            foreach (EffectData effect in effectList)
            {
                switch (effect.GetType().ToString())
                {
                    case "DoDealMinionDamage": return (effect as DoDealMinionDamage).GetDamage();
                    case "DoDealPlayerDamage": return (effect as DoDealPlayerDamage).GetDamage();
                }
            }
        }

        return 0;
    }

    private bool IsLink(MinionData minion)
    {
        if (minion != null)
        {
            return minion.IsAbilityLink;
        }

        return false;
    }

    private bool IsBeLink(MinionData minion)
    {
        if (minion != null)
        {
            return minion.IsBeLink;
        }

        return false;
    }

    private float GetHoverDuration()
    {
        float duration = GameHelper.GetRandomFloat(HoverCardTime_Min, HoverCardTime_Max);
        if (GameManager.Instance.CurrentPlayerIndex == PlayerIndex)
        {
            // My turn
            duration = Mathf.Min(duration, GameManager.Instance.RemainingTimer * 0.5f);
        }

        return duration;
    }

    private float GetWaitDuration()
    {
        float duration = GameHelper.GetRandomFloat(WaitInteractTime_Min, WaitInteractTime_Max);
        if (GameManager.Instance.CurrentPlayerIndex == PlayerIndex)
        {
            // My turn
            duration = Mathf.Min(duration, GameManager.Instance.RemainingTimer * 0.5f);
        }

        return duration;
    }

    private float GetPlayDuration()
    {
        float duration = 0.0f;

        if (IsHaveDecisionTime)
        {
            duration = GameHelper.GetRandomFloat(PreUseCardTime_Min, PreUseCardTime_Max);
            if (GameManager.Instance.CurrentPlayerIndex == PlayerIndex)
            {
                // My turn
                duration = Mathf.Min(duration, GameManager.Instance.RemainingTimer * 0.5f);
            }
        }

        return duration;
    }

    public static BotWeight GetBotWeight(BotLevel botLevel, CardElementType elementType)
    {
        BotWeight weight;

        weight = new BotWeight(
              // Decision Percent
              decisionPercent: 0.0f
            // Arrow
            , wArrow_Active: 1                      // [Little Happy] about arrow active.
            , wArrow_Inactive: -1                   // [Little Upset] about arrow inactive.
            , wArrow_ShowWeakness: -2               // [Little Upset] about show weakness side.
            , wArrow_HideWeakness: 2                // [Little Happy] about hide weakness side.
            , wArrow_ConnectLink: 50                // [Happy]        about connect link.
                                                    // Attacking
            , wAtk_AttackAndKill: 2000              // [Very Happy] about attack and kill enemy.
            , wAtk_Attack: 200                      // [Happy]      about attacking.
            , wAtk_BattleAndKill: 1500              // [Very Happy] about go to battle and kill.
            , wAtk_BattleAndBothKill: 1000          // [Very Happy] about go to battle and both kill.
            , wAtk_Battle: 100                      // [Happy]      about Battle.
            , wAtk_BattleAndDeath: -500             // [Very Upset] about go to battle and death.
            , wAtk_BeAttack: -500                   // [Very Upset] about being attack.
            , wAtk_BeAttackAndDeath: -1000          // [Very Upset] about being attack and death.
                                                    // Minion
            , wMinion_SP: 1000                      // [Very Interest] about high sp minion.
            , wMinion_AT: 0                         // [Not Interest]  about high atk minion.
            , wMinion_HP: 0                         // [Not Interest]  about high hp minion.
            , wMinion_Linker: 2000                  // [Very Interest] about linker minion.
        );

        switch (elementType)
        {
            case CardElementType.WATER:
                {
                    weight.WAtk_Attack = 400;           // [Little Happy] about attack and kill enemy.
                    weight.WAtk_BattleAndKill = 500;    // [Very Happy]   about go to battle and kill.
                    weight.WAtk_BattleAndBothKill = 0;  // [Not Care]     about go to battle and both kill.
                    weight.WArrow_Active = 50;          // [Happy]        about arrow active.
                    weight.WArrow_ConnectLink = 2000;   // [Very Happy]   about connect link.
                }
                break;

            case CardElementType.HOLY:
            case CardElementType.EARTH:
                {
                    weight.WArrow_Active = 50;          // [Happy]      about arrow active.
                    weight.WArrow_HideWeakness = 500;   // [Happy]      about hide weakness side.
                    weight.WArrow_ConnectLink = 0;      // [Not Care]   about connect link.
                }
                break;

            case CardElementType.FIRE:
            case CardElementType.AIR:
            case CardElementType.DARK:
                {
                    weight.WArrow_ConnectLink = 0;      // [Not Care]   about connect link.
                }
                break;
        }

        switch (botLevel)
        {
            case BotLevel.Easy:
            default:
            {
                weight.DecisionPercent = 0.6f; // 60% delta best decision.
            }
            break;

            case BotLevel.Medium:
            {
                weight.DecisionPercent = 0.4f; // 40% delta best decision.
            }
            break;

            case BotLevel.Hard:
            {
                weight.DecisionPercent = 0.0f; // 0% delta best decision.
            }
            break;
        }

        return weight;
    }
    #endregion

    #region Calculate Value Methods
    private void CalScore(BattleCardData card, ref BoardValue boardValue)
    {
        float score = 0.0f;
        MinionData currentMinion = card as MinionData;
        SlotData currentSlot = null;
        bool isEnemyTauntNearby = false;

        for (int slotIndex = 0; slotIndex < GameManager.Instance.Data.SlotDatas.Length; ++slotIndex)
        {
            score = 0.0f;
            currentSlot = GameManager.Instance.Data.SlotDatas[slotIndex];
            isEnemyTauntNearby = IsEnemyTauntNearby(currentMinion);

            // Find all nearby slots/minions.
            if (currentSlot.IsSlotEmpty)
            {
                // Cal slot score.
                score += CalSlotScore(currentMinion, currentSlot, isEnemyTauntNearby);
                score += CalBattleScore(currentMinion, currentSlot);
            }
            else
            {
                score = float.MinValue;
            }

            boardValue.AddValue(slotIndex, score);
        }
    }

    private BattleResult CalBattleResult(MinionData minion1, MinionData minion2, SlotData slot1, SlotData slot2)
    {
        if (minion1 != null && minion2 != null)
        {
            if (IsCanAttack(minion1, minion2, slot1, slot2))
            {
                // M1 can attack.
                if (minion1.ATKCurrent >= minion2.HPCurrent)
                {
                    // M1 can attack and can kill.
                    if (IsMarkDeath(minion2))
                    {
                        // M2 already dead.
                        if (IsCanAttack(minion2, minion1, slot2, slot1))
                        {
                            // M2 already dead and M2 can counter attack.
                            if (minion2.ATKCurrent >= (minion1.HPCurrent + minion1.ArmorCurrent))
                            {
                                // M2 already dead and M2 can counter attack and kill too.
                                return BattleResult.BeAttackedAndDeath;
                            }
                            else
                            {
                                // M2 already dead and M2 can counter attack but cannot kill.
                                return BattleResult.BeAttacked;
                            }
                        }
                        else
                        {
                            // M2 already dead and M2 cannot counter attack.
                            return BattleResult.None;
                        }
                    }
                    else
                    {
                        // M2 NOT already dead.
                        if (IsCanAttack(minion2, minion1, slot2, slot1))
                        {
                            // M1 can attack and kill and M2 can counter attack.
                            if (minion2.ATKCurrent >= (minion1.HPCurrent + minion1.ArmorCurrent))
                            {
                                // M1 can attack and kill and M2 can counter attack and kill too.
                                return BattleResult.BattleAndBothKill;
                            }
                            else
                            {
                                // M1 can attack and kill and M2 can counter attack but can't not kill.
                                return BattleResult.BattleAndKill;
                            }
                        }
                        else
                        {
                            // M1 can attack and kill and M2 can't counter attack.
                            return BattleResult.AttackAndKill;
                        }
                    }
                }
                else
                {
                    // M1 can attack but can't kill.
                    if (IsMarkDeath(minion2))
                    {
                        // M2 already dead.
                        if (IsCanAttack(minion2, minion1, slot2, slot1))
                        {
                            // M2 already dead and M2 can counter attack.
                            if (minion2.ATKCurrent >= (minion1.HPCurrent + minion1.ArmorCurrent))
                            {
                                // M2 already dead and M2 can counter attack and kill.
                                return BattleResult.BeAttackedAndDeath;
                            }
                            else
                            {
                                // M2 already dead and M2 can counter attack but can't not kill too.
                                return BattleResult.BeAttacked;
                            }
                        }
                        else
                        {
                            // M2 can't counter attack.
                            return BattleResult.None;
                        }
                    }
                    else
                    {
                        // M2 NOT already dead.
                        if (IsCanAttack(minion2, minion1, slot2, slot1))
                        {
                            // M1 can attack but can't kill and M2 can counter attack.
                            if (minion2.ATKCurrent >= (minion1.HPCurrent + minion1.ArmorCurrent))
                            {
                                // M1 can attack but can't kill and M2 can counter attack and kill.
                                return BattleResult.BattleAndDeath;
                            }
                            else
                            {
                                // M1 can attack but can't kill and M2 can counter attack but can't not kill too.
                                return BattleResult.Battle;
                            }
                        }
                        else
                        {
                            // M2 can't counter attack.
                            return BattleResult.Attack;
                        }
                    }
                }
            }
            else
            {
                // M1 can't attack.
                if (IsCanAttack(minion2, minion1, slot2, slot1))
                {
                    // M1 can't attack and M2 can counter attack.
                    if (minion2.ATKCurrent >= (minion1.HPCurrent + minion1.ArmorCurrent))
                    {
                        // M1 can't attack and M2 can counter attack and kill.
                        return BattleResult.BeAttackedAndDeath;
                    }
                    else
                    {
                        // M1 can't attack and M2 can counter attack but can't not kill.
                        return BattleResult.BeAttacked;
                    }
                }
                else
                {
                    // M1 can't attack. and M2 can't counter attack.
                    return BattleResult.None;
                }
            }
        }

        Debug.LogError("Error!");
        return BattleResult.None;
    }

    private bool IsCanAttack(MinionData minion1, MinionData minion2, SlotData slot1, SlotData slot2)
    {
        if (minion1 != null && minion2 != null)
        {
            if (minion1.Owner != minion2.Owner && !minion1.IsBeFreeze)
            {
                if (GameManager.Instance.IsValidSlotID(slot1.SlotID) && GameManager.Instance.IsValidSlotID(slot2.SlotID))
                {
                    SlotData nearbySlot = null;
                    bool isEnemyTauntNearby = IsEnemyTauntNearby(minion1);

                    foreach (CardDirectionType dir in GetAllDirectionList())
                    {
                        if (slot1.GetAdjacentSlot(dir, out nearbySlot))
                        {
                            if (nearbySlot.SlotID == slot2.SlotID)
                            {
                                if (
                                        (minion1.CardDir.IsDirection(dir)       // Is point to enemy
                                            && !minion1.IsBeFreeze                // AND I am not freeze
                                            && !isEnemyTauntNearby              // AND No taunt enemy nearby
                                        )
                                    || (minion2.IsAbilityTaunt && !minion2.IsBeSilence)  // Or Is this enemy is active taunt 
                                )
                                {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
        }

        return false;
    }

    private bool IsEnemyTauntNearby(MinionData currentMinion)
    {
        if (currentMinion != null && GameManager.Instance.IsValidSlotID(currentMinion.SlotID))
        {
            SlotData currentSlot = GameManager.Instance.Data.SlotDatas[currentMinion.SlotID];
            SlotData nearbySlot = null;
            MinionData nearbyMinion = null;

            // Prepare
            foreach (CardDirectionType dir in GetAllDirectionList())
            {
                if (currentSlot.GetAdjacentSlot(dir, out nearbySlot))
                {
                    if (!nearbySlot.IsSlotEmpty && GameManager.Instance.FindMinionBattleCard(nearbySlot.BattleCardUniqueID, out nearbyMinion))
                    {
                        if (
                               (nearbyMinion.IsAbilityTaunt && !nearbyMinion.IsBeSilence)    // Is taunt active
                            && (nearbyMinion.Owner != currentMinion.Owner)          // Is enemy
                        )
                        {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    private float CalMinionScore(MinionData minion)
    {
        float score = 0.0f;

        if (minion != null)
        {
            // Stat
            score += minion.SpiritCurrent * Weight.WMinion_SP;
            score += minion.ATKCurrent * Weight.WMinion_AT;
            score += minion.HPCurrent * Weight.WMinion_HP;

            // Ability
            if (IsLink(minion) && IsBeLink(minion)) score += Weight.WMinion_Linker; // Is root of link.
        }

        return score;
    }

    private float CalSlotScore(MinionData currentMinion, SlotData currentSlot, bool isEnemyTauntNearby)
    {
        float score = 0.0f;

        if (currentMinion != null && currentSlot != null)
        {
            if (currentSlot.IsSlotEmpty)
            {
                SlotData nearbySlot = null;
                MinionData nearbyMinion = null;
                foreach (CardDirectionType dir in GetAllDirectionList())
                {
                    if (currentSlot.GetAdjacentSlot(dir, out nearbySlot))
                    {
                        // Have slot in this direction.
                        if (nearbySlot.IsSlotEmpty)
                        {
                            // Slot is empty.
                            if (currentMinion.CardDir.IsDirection(dir) && !isEnemyTauntNearby)
                            {
                                // Minion's arrow point this direction.
                                score += Weight.WArrow_Active;
                            }
                            else
                            {
                                // Show minion weakness.
                                score += Weight.WArrow_ShowWeakness;
                            }
                        }
                        else
                        {
                            // Slot is not empty.
                            if (GameManager.Instance.FindMinionBattleCard(nearbySlot.BattleCardUniqueID, out nearbyMinion))
                            {
                                bool isNearbyMinionTaunt = nearbyMinion.IsAbilityTaunt;

                                // Have minion in this slot
                                if (nearbyMinion.Owner == currentMinion.Owner)
                                {
                                    // Friendly
                                    if (currentMinion.CardDir.IsDirection(dir))
                                    {
                                        // Active minion arrow.
                                        score += Weight.WArrow_Active;

                                        if (nearbyMinion.CardDir.IsDirection(GameHelper.GetOppositeDirection(dir)))
                                        {
                                            // Link!
                                            score += Weight.WArrow_ConnectLink;
                                        }
                                        else
                                        {
                                            // Protect friend weakness.
                                            score += Weight.WArrow_Inactive;
                                            score += Weight.WArrow_HideWeakness;
                                        }
                                    }
                                    else
                                    {
                                        // Protect minion weakness.
                                        score += Weight.WArrow_HideWeakness;

                                        if (nearbyMinion.CardDir.IsDirection(GameHelper.GetOppositeDirection(dir)))
                                        {
                                            // Block friend arrow active.
                                            score += Weight.WArrow_Inactive;
                                        }
                                        else
                                        {
                                            // Protect friend weakness.
                                            score += Weight.WArrow_HideWeakness;
                                        }
                                    }
                                }
                                else
                                {
                                    // Enemy
                                    if (isNearbyMinionTaunt || isEnemyTauntNearby)
                                    {
                                        // Have taunt minion nearby or this nearby minion is taunt.
                                        if (isNearbyMinionTaunt)
                                        {
                                            // this nearby minion is taunt.
                                            score += Weight.WArrow_Active;
                                        }
                                        else
                                        {
                                            // Have taunt minion nearby. => My arrow will be change...
                                            score += Weight.WArrow_ShowWeakness;
                                        }
                                    }
                                    else
                                    {
                                        // No taunt minion nearby.
                                        if (currentMinion.CardDir.IsDirection(dir))
                                        {
                                            // Active minion arrow.
                                            score += Weight.WArrow_Active;
                                        }
                                        else
                                        {
                                            if (nearbyMinion.CardDir.IsDirection(GameHelper.GetOppositeDirection(dir)))
                                            {
                                                // Be attack in weakness.
                                                score += Weight.WArrow_ShowWeakness;
                                            }
                                            else
                                            {
                                                // Protect my weakness.
                                                score += Weight.WArrow_HideWeakness;
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                // Not empty but not minion in this slot.
                                if (currentMinion.CardDir.IsDirection(dir))
                                {
                                    // Minion's arrow point this direction.
                                    score += Weight.WArrow_Inactive;
                                }
                                else
                                {
                                    // Protect minion weakness.
                                    score += Weight.WArrow_HideWeakness;
                                }
                            }
                        }
                    }
                    else
                    {
                        // No slot in this direction.
                        if (currentMinion.CardDir.IsDirection(dir) && !isEnemyTauntNearby)
                        {
                            // Minion's arrow point this direction.
                            score += Weight.WArrow_Inactive;
                        }
                        else
                        {
                            // Protect minion weakness.
                            score += Weight.WArrow_HideWeakness;
                        }
                    }
                }
            }
        }

        return score;
    }

    private float CalBattleScore(MinionData currentMinion, SlotData currentSlot)
    {
        float score = 0.0f;

        if (currentMinion != null && currentSlot != null)
        {
            if (currentSlot.IsSlotEmpty)
            {
                SlotData nearbySlot = null;
                MinionData nearbyMinion = null;
                foreach (CardDirectionType dir in GetAllDirectionList())
                {
                    if (currentSlot.GetAdjacentSlot(dir, out nearbySlot))
                    {
                        if (!nearbySlot.IsSlotEmpty)
                        {
                            if (GameManager.Instance.FindMinionBattleCard(nearbySlot.BattleCardUniqueID, out nearbyMinion))
                            {
                                if (nearbyMinion.Owner != currentMinion.Owner)
                                {
                                    float currentMinionScore = CalMinionScore(currentMinion);
                                    float nearbyMinionScore = CalMinionScore(nearbyMinion);

                                    BattleResult battleResult = CalBattleResult(currentMinion, nearbyMinion, currentSlot, nearbySlot);
                                    /*
                                    Debug.LogFormat("{0}[{1}] VS {2}[{3}] = {4}"
                                        , currentMinion.Name, currentSlot.SlotID
                                        , nearbyMinion.Name, nearbySlot.SlotID
                                        , battleResult
                                    );
                                    */
                                    switch (battleResult)
                                    {
                                        case BattleResult.AttackAndKill: score += nearbyMinionScore + Weight.WAtk_AttackAndKill; break;
                                        case BattleResult.Attack: score += Weight.WAtk_Attack; break;
                                        case BattleResult.BattleAndKill: score += nearbyMinionScore + Weight.WAtk_BattleAndKill; break;
                                        case BattleResult.BattleAndBothKill: score += (nearbyMinionScore - currentMinionScore) + Weight.WAtk_BattleAndBothKill; break;
                                        case BattleResult.Battle: score += Weight.WAtk_Battle; break;
                                        case BattleResult.BattleAndDeath: score += -currentMinionScore + Weight.WAtk_BattleAndDeath; break;
                                        case BattleResult.BeAttacked: score += -currentMinionScore + Weight.WAtk_BeAttacked; break;
                                        case BattleResult.BeAttackedAndDeath: score += -currentMinionScore + Weight.WAtk_BeAttackedAndDeath; break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return score;
    }
    #endregion

    #region Event Methods
    public override void OnGotInGameMessage(HeroVoice heroVoiceIndex)
    {
        if (IsHaveDecisionTime)
        {
            switch (heroVoiceIndex)
            {
                case HeroVoice.Greeting:
                    {
                        int i = _r.Next(0, 6);
                        switch (i)
                        {
                            case 0:
                            case 1: SendInGameMessage(HeroVoice.Greeting, null); break;
                            case 2: SendInGameMessage(HeroVoice.Threaten, null); break;
                        }
                    }
                    break;
            }
        }
    }

    public override void OnGotSpiritAttack(int damage)
    {
        if (IsHaveDecisionTime)
        {
            // I got damage
            if (GetMyHP() > 0)
            {
                if (damage >= Karamucho.UI.HeroAttackAnimation.MedPower)
                {
                    int index = _r.Next(0, 4);
                    switch (index)
                    {
                        case 0: SendInGameMessage(HeroVoice.Mistake, null); break;
                        case 1: SendInGameMessage(HeroVoice.Threaten, null); break;
                    }
                }
            }
        }
    }

    public override void OnEnemyGotSpiritAttack(int damage)
    {
        if (IsHaveDecisionTime)
        {
            // Enemy got damage
            if (GetEnemyHP() > 0)
            {
                if (damage >= Karamucho.UI.HeroAttackAnimation.MedPower)
                {
                    int index = _r.Next(0, 4);
                    switch (index)
                    {
                        case 0: SendInGameMessage(HeroVoice.Thank, null); break;
                        case 1: SendInGameMessage(HeroVoice.Threaten, null); break;
                    }
                }
            }
        }
    }

    public override void OnPlayHandCard(int uniqueID)
    {
        if (IsHaveDecisionTime)
        {
            MinionData minion = null;
            if (GameManager.Instance.FindMinionBattleCard(uniqueID, out minion))
            {
                if (minion.Owner != PlayerIndex)
                {
                    // Enemy spawned unit.
                    float duration = GetHoverDuration();
                    HoverCard(uniqueID, duration, null);
                }
            }
        }
    }
    #endregion

    #region OnGUI
    private void OnGUI()
    {
        if (_isShowLog)
        {
            if (_debugText == null) _debugText = new StringBuilder();
            _debugText.Clear();

            if (_deathMarkList != null)
            {
                _debugText.Append("Death :");
                for (int i = 0; i < _deathMarkList.Count; ++i)
                {
                    _debugText.AppendFormat("{0}[{1}]", _deathMarkList[i].Name, _deathMarkList[i].SlotID);
                }
                _debugText.Append("\n");
                for (int row = 0; row < GameManager.Instance.BoardHeight; ++row)
                {
                    for (int column = 0; column < GameManager.Instance.BoardWidth; ++column)
                    {
                        int index = (row * GameManager.Instance.BoardWidth) + column;
                        //SlotData slot = GameManager.Instance.Data.SlotDatas[index];

                        _debugText.AppendFormat("[{0}]", (_deathSlot[index] ? "<color=red>X</color>" : " "));
                    }
                    _debugText.Append("\n");
                }
            }

            if (_logList != null)
            {
                foreach (string log in _logList)
                {
                    _debugText.AppendFormat("{0}\n", log);
                }
            }

            // Draw debug
            if (_debugText.Length > 0)
            {
                GUI.Label(_debugTextRect, "<color=white>" + _debugText.ToString() + "</color>");
            }
        }
    }
    #endregion
}
