﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class BattleLog : IEnumerable<ActionLog>
{
    #region Public Properties
    public int Count { get { if (_list != null) return _list.Count; else return 0; } }
    #endregion

    #region Private Properties
    private List<ActionLog> _list = null;
    private StringBuilder _sb;
    #endregion

    #region Constructors
    public BattleLog()
    {
        _list = new List<ActionLog>();
    }

    public BattleLog(BattleLog battleLog)
    {
        _list = new List<ActionLog>(battleLog._list);
    }
    #endregion

    #region Methods
    public bool TryGetValue(int index, out ActionLog actionLog)
    {
        if (_list != null && index < _list.Count)
        {
            actionLog = _list[index];
            return true;
        }

        actionLog = null;
        return false;
    }

    public void SetValue(int index, ActionLog actionLog)
    {
        if (_list != null && index < _list.Count)
        {
            _list[index] = actionLog;
            return;
        }

        Debug.LogErrorFormat("BattleLog/SetValue: index out of range at {0}", index);
    }

    public void AddLog(ActionLog actionLog)
    {
        if (_list == null)
        {
            _list = new List<ActionLog>();
        }

        _list.Add(actionLog);
        //Debug.Log(GetDebugText());
    }

    public void InsertLog(int index, ActionLog actionLog)
    {
        if (_list == null)
        {
            _list = new List<ActionLog>();
        }

        _list.Insert(index, actionLog);
    }

    public bool RemoveLog(ActionLog actionLog)
    {
        if (_list != null)
        {
            if (_list.Contains(actionLog))
            {
                _list.Remove(actionLog);
                return true;
            }
        }

        return false;
    }

    public bool RemoveLogAt(int index)
    {
        if (_list != null)
        {
            if (index < _list.Count)
            {
                _list.RemoveAt(index);
                return true;
            }
        }

        return false;
    }

    public void Clear()
    {
        if (_list != null)
        {
            _list.Clear();
        }
    }

    public IEnumerator<ActionLog> GetEnumerator()
    {
        return _list.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public string GetDebugText()
    {
        if (_sb == null) _sb = new StringBuilder();
        _sb.Clear();

        if (_list != null && _list.Count > 0)
        {
            foreach (ActionLog log in _list)
            {
                _sb.AppendFormat("{0}\n", log.GetDebugText());
            }
        }

        return _sb.ToString();
    }
    #endregion

    #region Operators
    public ActionLog this[int index]
    {
        get
        {
            ActionLog result;
            if (TryGetValue(index, out result))
            {
                return result;
            }
            else
            {
                Debug.LogErrorFormat("BattleLog/Getter: index out of range at {0}", index);
                return null;
            }
        }
        set
        {
            SetValue(index, value);
        }
    }
    #endregion
}
