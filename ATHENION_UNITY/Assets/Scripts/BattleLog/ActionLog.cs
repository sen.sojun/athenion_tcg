﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using Karamucho.UI;

#region ActionLog
public abstract class ActionLog
{
    #region Public Properties
    public static readonly string RedColor = "#FF0A00FF";
    public static readonly string BlueColor = "#005FFFFF";
    public static readonly int MaxPlayerName = 16;

    public static readonly string DrawLogKey = "ACTION_LOG_DRAW";
    public static readonly string MulliganLogKey = "ACTION_LOG_MULLIGAN";
    public static readonly string UseCardLogKey = "ACTION_LOG_USECARD";

    public static readonly string AttackLogKey = "ACTION_LOG_ATTACK";
    public static readonly string CounterLogKey = "ACTION_LOG_COUNTER";
    public static readonly string DeathLogKey = "ACTION_LOG_DEATH";

    public static readonly string AbilityTriggerLogKey = "ACTION_LOG_ABILITY_TRIGGER";
    public static readonly string SpiritDamageLogKey = "ACTION_LOG_SPIRIT_DAMAGE";
    public static readonly string DealMinionDamageLogKey = "ACTION_LOG_DEAL_MINION_DAMAGE";
    public static readonly string DestroyMinionLogKey = "ACTION_LOG_DESTROY_MINION";
    public static readonly string SacrificeMinionLogKey = "ACTION_LOG_SACRIFICE_MINION";
    public static readonly string DealPlayerDamageLogKey = "ACTION_LOG_DEAL_PLAYER_DAMAGE";

    public static readonly string PhaseLogKey = "ACTION_LOG_PHASE";
    public static readonly string TurnSummaryLogKey = "ACTION_LOG_TURN_SUMMARY";
    public static readonly string StartTurnLogKey = "ACTION_LOG_START_TURN";

    public static readonly string SetHPLogKey = "ACTION_LOG_SET_HP";
    public static readonly string SetAPLogKey = "ACTION_LOG_SET_AP";
    public static readonly string SetSPLogKey = "ACTION_LOG_SET_SP";
    public static readonly string SetAPHPLogKey = "ACTION_LOG_SET_APHP";
    public static readonly string BoostHPLogKey = "ACTION_LOG_BOOST_HP";
    public static readonly string BoostAPLogKey = "ACTION_LOG_BOOST_AP";
    public static readonly string BoostSPLogKey = "ACTION_LOG_BOOST_SP";
    public static readonly string BoostAPHPLogKey = "ACTION_LOG_BOOST_APHP";
    public static readonly string BoostArmorLogKey = "ACTION_LOG_BOOST_ARMOR";
    public static readonly string SwapHPLogKey = "ACTION_LOG_SWAP_HP";
    public static readonly string SwapAPLogKey = "ACTION_LOG_SWAP_AP";
    public static readonly string SwapSPLogKey = "ACTION_LOG_SWAP_SP";
    public static readonly string SwapAPHPLogKey = "ACTION_LOG_SWAP_APHP";

    public static readonly string BuffFearLogKey = "ACTION_LOG_BUFF_FEAR";
    public static readonly string BuffFearRepeatLogKey = "ACTION_LOG_BUFF_FEAR_REPEAT";
    public static readonly string BuffFearClearLogKey = "ACTION_LOG_BUFF_FEAR_CLEAR";

    public static readonly string BuffFreezeLogKey = "ACTION_LOG_BUFF_FREEZE";
    public static readonly string BuffFreezeRepeatLogKey = "ACTION_LOG_BUFF_FREEZE_REPEAT";
    public static readonly string BuffFreezeClearLogKey = "ACTION_LOG_BUFF_FREEZE_CLEAR";

    public static readonly string BuffSilenceLogKey = "ACTION_LOG_BUFF_SILENCE";
    public static readonly string BuffSilenceRepeatLogKey = "ACTION_LOG_BUFF_SILENCE_REPEAT";
    public static readonly string BuffSilenceClearLogKey = "ACTION_LOG_BUFF_SILENCE_CLEAR";

    public static readonly string BuffStealthLogKey = "ACTION_LOG_BUFF_STEALTH";
    public static readonly string BuffStealthRepeatLogKey = "ACTION_LOG_BUFF_STEALTH_REPEAT";
    public static readonly string BuffStealthClearLogKey = "ACTION_LOG_BUFF_STEALTH_CLEAR";

    public static readonly string BuffDarkMatterLogKey = "ACTION_LOG_BUFF_DARK_MATTER";
    public static readonly string BuffDarkMatterRepeatLogKey = "ACTION_LOG_BUFF_DARK_MATTER_REPEAT";
    public static readonly string BuffDarkMatterClearLogKey = "ACTION_LOG_BUFF_DARK_MATTER_CLEAR";

    public static readonly string BuffInvincibleLogKey = "ACTION_LOG_BUFF_INVINCIBLE";
    public static readonly string BuffInvincibleRepeatLogKey = "ACTION_LOG_BUFF_INVINCIBLE_REPEAT";
    public static readonly string BuffInvincibleClearLogKey = "ACTION_LOG_BUFF_INVINCIBLE_CLEAR";

    public static readonly string BuffUntargetableLogKey = "ACTION_LOG_BUFF_UNTARGETABLE";
    public static readonly string BuffUntargetableRepeatLogKey = "ACTION_LOG_BUFF_UNTARGETABLE_REPEAT";
    public static readonly string BuffUntargetableClearLogKey = "ACTION_LOG_BUFF_UNTARGETABLE_CLEAR";

    public static readonly string BuffPiercingLogKey = "ACTION_LOG_BUFF_PIERCING";
    public static readonly string BuffPiercingRepeatLogKey = "ACTION_LOG_BUFF_PIERCING_REPEAT";
    public static readonly string BuffPiercingClearLogKey = "ACTION_LOG_BUFF_PIERCING_CLEAR";

    public static readonly string BuffSlotLockLogKey = "ACTION_LOG_BUFF_SLOT_LOCK";
    public static readonly string BuffSlotLockClearLogKey = "ACTION_LOG_BUFF_SLOT_LOCK_CLEAR";

    public static readonly string BuffSlotDarkMatterLogKey = "ACTION_LOG_BUFF_SLOT_DARK_MATTER";
    public static readonly string BuffSlotDarkMatterClearLogKey = "ACTION_LOG_BUFF_SLOT_DARK_MATTER_CLEAR";

    public static readonly string RestoreMinionHPLogKey = "ACTION_LOG_RESTORE_MINION_HP";
    public static readonly string RestorePlayerHPLogKey = "ACTION_LOG_RESTORE_PLAYER_HP";

    public int Index { get; protected set; }
    #endregion

    #region Protected Properties
    protected StringBuilder _sb;
    #endregion

    #region Constructors
    protected ActionLog()
    { 
    }
    #endregion

    #region Methods
    public virtual void CreateUILog()
    {
        // Do nothing.
    }

    public override string ToString()
    {
        return this.GetType().ToString();
    }

    public virtual string GetDebugText()
    {
        if (_sb == null) _sb = new StringBuilder();
        _sb.Clear();

        _sb.AppendFormat("{0}: {1}", this.GetType().ToString(), this.ToString());

        return _sb.ToString();
    }

    public void SetIndex(int index)
    {
        Index = index;
    }

    public static string CreateNameString(PlayerIndex playerIndex, string name, bool isClamp = true)
    {
        return string.Format("<color={0}>{1}</color>"
            , (GameManager.Instance.IsLocalPlayer(playerIndex) ? ActionLog.BlueColor : ActionLog.RedColor)
            , (isClamp ? (name.Length > MaxPlayerName ? name.Substring(0, MaxPlayerName) : name) : name)
        );
    }
    #endregion
}
#endregion

#region DrawLog
public class DrawLog : ActionLog
{
    #region Public Properties
    #endregion

    #region Protected Properties
    
    protected PlayerIndex _playerIndex;
    protected List<BattleCardData> _cardList;
    #endregion

    #region Constructors
    public DrawLog(PlayerIndex playerIndex, List<BattleCardData> cardList)
    {
        _playerIndex = playerIndex;
        _cardList = cardList;
    }
    #endregion

    #region Methods
    public override void CreateUILog()
    {
        //Debug.Log(GetDebugText());

        UIManager.Instance.ActionLog_CreateAction(_playerIndex, this.ToString());
    }

    public override string ToString()
    {
        BattlePlayerData data = GameManager.Instance.Data.PlayerDataList[(int)_playerIndex];
        string formatText;
        LocalizationManager.Instance.GetText(ActionLog.DrawLogKey, out formatText);

        string result = string.Format(
              formatText
            , CreateNameString(_playerIndex, data.PlayerInfo.DisplayName)
            , _cardList.Count
            , (_cardList.Count > 1) ? "s" : ""
        );

        return result;
    }
    #endregion
}
#endregion

#region MulliganLog
public class MulliganLog : ActionLog
{
    #region Public Properties
    #endregion

    #region Protected Properties    
    protected PlayerIndex _playerIndex;
    protected List<BattleCardData> _cardList;
    #endregion

    #region Constructors
    public MulliganLog(PlayerIndex playerIndex, List<BattleCardData> cardList)
    {
        _playerIndex = playerIndex;
        _cardList = cardList;
    }
    #endregion

    #region Methods
    public override void CreateUILog()
    {
        //Debug.Log(GetDebugText());

        BattlePlayerData data = GameManager.Instance.Data.PlayerDataList[(int)_playerIndex];
        string formatText;
        LocalizationManager.Instance.GetText(ActionLog.MulliganLogKey, out formatText);

        string result = "";
        foreach (BattleCardData card in _cardList)
        {
            result = string.Format(
                  formatText
                , CreateNameString(_playerIndex, data.PlayerInfo.DisplayName)
                , (GameManager.Instance.IsLocalPlayer(_playerIndex) ? CreateNameString(_playerIndex, card.Name, false) : "")
            );

            UIManager.Instance.ActionLog_CreateAction(_playerIndex, result);
        }
    }

    public override string ToString()
    {
        BattlePlayerData data = GameManager.Instance.Data.PlayerDataList[(int)_playerIndex];
        string formatText;
        LocalizationManager.Instance.GetText(ActionLog.MulliganLogKey, out formatText);

        string result = "";
        foreach (BattleCardData card in _cardList)
        {
            if (result.Length > 0)
            {
                result += "\n";
            }

            result += string.Format(
                  formatText
                , data.PlayerInfo.DisplayName // Player Name
                , card.Name
            );
        }

        return result;
    }
    #endregion
}
#endregion

#region SpawnTokenLog
public class SpawnTokenLog : ActionLog
{
    #region Public Properties
    #endregion

    #region Protected Properties    
    protected PlayerIndex _playerIndex;
    protected MinionData _minion;
    protected int _slotID;
    #endregion

    #region Constructors
    public SpawnTokenLog(PlayerIndex playerIndex, MinionData minion, int slotID)
    {
        _playerIndex = playerIndex;
        _minion = minion;
        _slotID = slotID;
    }
    #endregion

    #region Methods
    public override void CreateUILog()
    {
        //Debug.Log(GetDebugText());

        UIManager.Instance.ActionLog_CreateAction(_minion, this.ToString());
    }

    public override string ToString()
    {
        BattlePlayerData data = GameManager.Instance.Data.PlayerDataList[(int)_playerIndex];
        string formatText;
        LocalizationManager.Instance.GetText(ActionLog.UseCardLogKey, out formatText);

        string result = string.Format(
              formatText
            , CreateNameString(_playerIndex, data.PlayerInfo.DisplayName) // Player Name
            , CreateNameString(_playerIndex, _minion.Name, false)
            , GameHelper.SlotIDToSlotCode(_slotID)
        );

        return result;
    }
    #endregion
}
#endregion

#region AttackLog
public class AttackLog : ActionLog
{
    #region Public Properties
    #endregion

    #region Protected Properties
    protected MinionData _attacker;
    protected MinionData _defender;
    protected int _damage;
    #endregion

    #region Constructors
    public AttackLog(MinionData attacker, MinionData defender, int damage)
    {
        _attacker = attacker;
        _defender = defender;
        _damage = damage;
    }
    #endregion

    #region Methods
    public override void CreateUILog()
    {
        //Debug.Log(GetDebugText());

        UIManager.Instance.ActionLog_CreateAction(_attacker, _defender, this.ToString(), UIActionLog_Action.ActionIcon.Attack);
    }

    public override string ToString()
    {
        string formatText;
        LocalizationManager.Instance.GetText(ActionLog.AttackLogKey, out formatText);

        string result = string.Format(
              formatText
            , CreateNameString(_attacker.Owner, _attacker.Name, false)
            , CreateNameString(_defender.Owner, _defender.Name, false)
            , _damage
        );

        return result;
    }
    #endregion
}
#endregion

#region CounterLog
public class CounterLog : ActionLog
{
    #region Public Properties
    #endregion

    #region Protected Properties
    protected MinionData _attacker;
    protected MinionData _defender;
    protected int _damage;
    #endregion

    #region Constructors
    public CounterLog(MinionData attacker, MinionData defender, int damage)
    {
        _attacker = attacker;
        _defender = defender;
        _damage = damage;
    }
    #endregion

    #region Methods
    public override void CreateUILog()
    {
        //Debug.Log(GetDebugText());

        UIManager.Instance.ActionLog_CreateAction(_attacker, _defender, this.ToString(), UIActionLog_Action.ActionIcon.Attack);
    }

    public override string ToString()
    {
        string formatText;
        LocalizationManager.Instance.GetText(ActionLog.CounterLogKey, out formatText);

        string result = string.Format(
              formatText
            , CreateNameString(_attacker.Owner, _attacker.Name, false)
            , CreateNameString(_defender.Owner, _defender.Name, false)
            , _damage
        );

        return result;
    }
    #endregion
}
#endregion

#region DeathLog
public class DeathLog : ActionLog
{
    #region Public Properties
    #endregion

    #region Protected Properties
    protected MinionData _minion;
    #endregion

    #region Constructors
    public DeathLog(MinionData minion)
    {
        _minion = minion;
    }
    #endregion

    #region Methods
    public override void CreateUILog()
    {
        //Debug.Log(GetDebugText());

        UIManager.Instance.ActionLog_CreateAction(_minion, this.ToString());
    }

    public override string ToString()
    {
        string formatText;
        LocalizationManager.Instance.GetText(ActionLog.DeathLogKey, out formatText);

        string result = string.Format(
              formatText
            , CreateNameString(_minion.Owner, _minion.Name, false) // Unit Name
        );

        return result;
    }
    #endregion
}
#endregion

#region AbilityTriggerLog
public class AbilityTriggerLog : ActionLog
{
    #region Public Properties
    #endregion

    #region Protected Properties
    protected MinionData _minion;
    protected CardAbilityData _ability;
    protected EventKey _eventKey;
    #endregion

    #region Constructors
    public AbilityTriggerLog(MinionData minion, CardAbilityData ability, EventKey eventKey)
    {
        _minion = minion;
        _ability = ability;
        _eventKey = eventKey;
    }
    #endregion

    #region Methods
    public override void CreateUILog()
    {
        //Debug.Log(GetDebugText());

        UIManager.Instance.ActionLog_CreateAction(_minion, this.ToString());
    }

    public override string ToString()
    {
        string formatText;
        LocalizationManager.Instance.GetText(ActionLog.AbilityTriggerLogKey, out formatText);

        string eventKeyStr = "";
        if(_eventKey != EventKey.None)
        {
            eventKeyStr = _eventKey.ToString();
        }

        string result = string.Format(
              formatText
            , CreateNameString(_minion.Owner, _minion.Name, false) // Unit Name
            , eventKeyStr
            , _minion.Description
        );

        return result;
    }
    #endregion
}
#endregion

#region SpiritDamageLog
public class SpiritDamageLog : ActionLog
{
    #region Public Properties
    #endregion

    #region Protected Properties
    protected PlayerIndex _playerIndex;
    protected int _damage;
    #endregion

    #region Constructors
    public SpiritDamageLog(PlayerIndex playerIndex, int damage)
    {
        _playerIndex = playerIndex;
        _damage = damage;
    }
    #endregion

    #region Methods
    public override void CreateUILog()
    {
        //Debug.Log(GetDebugText());

        string formatText;
        LocalizationManager.Instance.GetText(ActionLog.SpiritDamageLogKey, out formatText);

        string result = "";
        PlayerIndex dealer = PlayerIndex.One;

        if (GameManager.Instance.GetPlayerCount() == 2) // 2 players
        {
            if (_playerIndex == PlayerIndex.One) dealer = PlayerIndex.Two;
            else dealer = PlayerIndex.One;

            result = string.Format(
                    formatText
                , CreateNameString(dealer, GameManager.Instance.Data.PlayerDataList[(int)dealer].PlayerInfo.DisplayName)
                , CreateNameString(_playerIndex, GameManager.Instance.Data.PlayerDataList[(int)_playerIndex].PlayerInfo.DisplayName)
                , _damage
            );
        }

        if (result != null && result.Length > 0)
        {
            UIManager.Instance.ActionLog_CreateAction(dealer, result);
        }
    }

    public override string ToString()
    {
        string formatText;
        LocalizationManager.Instance.GetText(ActionLog.SpiritDamageLogKey, out formatText);

        string result = "";
        PlayerIndex dealer;
        if (GameManager.Instance.GetPlayerCount() == 2) // 2 players
        {
            if (_playerIndex == PlayerIndex.One)    dealer = PlayerIndex.Two;
            else                                    dealer = PlayerIndex.One;

            result = string.Format(
                    formatText
                , CreateNameString(dealer, GameManager.Instance.Data.PlayerDataList[(int)dealer].PlayerInfo.DisplayName)
                , CreateNameString(_playerIndex, GameManager.Instance.Data.PlayerDataList[(int)_playerIndex].PlayerInfo.DisplayName)
                , _damage
            );
        }

        return result;
    }
    #endregion
}
#endregion

#region DealMinionDamageLog
public class DealMinionDamageLog : ActionLog
{
    #region Public Properties
    #endregion

    #region Protected Properties

    protected Requester _requester;
    protected List<MinionData> _targetList;
    protected int _damage;
    #endregion

    #region Constructors
    public DealMinionDamageLog(Requester requester, List<MinionData> targetList, int damage)
    {
        _requester = requester;
        _targetList = targetList;
        _damage = damage;
    }
    #endregion

    #region Methods
    public override void CreateUILog()
    {
        //Debug.Log(GetDebugText());

        string formatText;
        LocalizationManager.Instance.GetText(ActionLog.DealMinionDamageLogKey, out formatText);

        string name = "";
        switch (_requester.Type)
        {
            case Requester.RequesterType.Minion:
            name = _requester.Minion.Name;
            break;

            case Requester.RequesterType.Spell:
            name = _requester.Spell.Name;
            break;

            case Requester.RequesterType.Hero:
            case Requester.RequesterType.Player:
            case Requester.RequesterType.Slot:
            name = GameManager.Instance.Data.PlayerDataList[(int)_requester.PlayerIndex].PlayerInfo.DisplayName;
            break;
        }

        int damage = _damage;
        foreach (MinionData minion in _targetList)
        {
            string result = "";
            if(_damage == -1)
            {
                damage = minion.HPCurrent;
            }

            result = string.Format(
                  formatText
                , CreateNameString(_requester.PlayerIndex, name)
                , CreateNameString(minion.Owner, minion.Name, false)
                , damage
            );

            switch (_requester.Type)
            {
                case Requester.RequesterType.Minion:
                UIManager.Instance.ActionLog_CreateAction(_requester.Minion, minion, result, UIActionLog_Action.ActionIcon.Effect);
                break;

                case Requester.RequesterType.Spell:
                case Requester.RequesterType.Hero:
                case Requester.RequesterType.Player:
                case Requester.RequesterType.Slot:
                UIManager.Instance.ActionLog_CreateAction(minion, result);
                break;
            }
        }
    }

    public override string ToString()
    {
        string name = "";
        switch (_requester.Type)
        {
            case Requester.RequesterType.Minion: 
                name = _requester.Minion.Name; 
                break;

            case Requester.RequesterType.Spell:
                name = _requester.Spell.Name;
                break;

            case Requester.RequesterType.Hero:
            case Requester.RequesterType.Player:
            case Requester.RequesterType.Slot: 
                name = GameManager.Instance.Data.PlayerDataList[(int)_requester.PlayerIndex].PlayerInfo.DisplayName; 
                break;
        }

        string formatText;
        LocalizationManager.Instance.GetText(ActionLog.DealMinionDamageLogKey, out formatText);

        string result = "";
        int damage = _damage;
        foreach (MinionData minion in _targetList)
        {
            if (_damage == -1)
            {
                damage = minion.HPCurrent;
            }

            if (result.Length > 0)
            {
                result += "\n";
            }

            result += string.Format(
                  formatText
                , CreateNameString(_requester.PlayerIndex, name)
                , CreateNameString(minion.Owner, minion.Name, false)
                , damage
            );
        }

        return result;
    }
    #endregion
}
#endregion

#region DestroyMinionLog
public class DestroyMinionLog : ActionLog
{
    #region Public Properties
    #endregion

    #region Protected Properties

    protected Requester _requester;
    protected List<MinionData> _targetList;
    #endregion

    #region Constructors
    public DestroyMinionLog(Requester requester, List<MinionData> targetList)
    {
        _requester = requester;
        _targetList = targetList;
    }
    #endregion

    #region Methods
    public override void CreateUILog()
    {
        //Debug.Log(GetDebugText());

        string formatText;
        LocalizationManager.Instance.GetText(ActionLog.DestroyMinionLogKey, out formatText);

        string name = "";
        switch (_requester.Type)
        {
            case Requester.RequesterType.Minion:
                name = _requester.Minion.Name;
                break;

            case Requester.RequesterType.Spell:
                name = _requester.Spell.Name;
                break;

            case Requester.RequesterType.Hero:
            case Requester.RequesterType.Player:
            case Requester.RequesterType.Slot:
                name = GameManager.Instance.Data.PlayerDataList[(int)_requester.PlayerIndex].PlayerInfo.DisplayName;
                break;
        }

        foreach (MinionData minion in _targetList)
        {
            string result = "";

            result = string.Format(
                  formatText
                , CreateNameString(_requester.PlayerIndex, name)
                , CreateNameString(minion.Owner, minion.Name, false)
            );

            switch (_requester.Type)
            {
                case Requester.RequesterType.Minion:
                    UIManager.Instance.ActionLog_CreateAction(_requester.Minion, minion, result, UIActionLog_Action.ActionIcon.Effect);
                    break;

                case Requester.RequesterType.Spell:
                case Requester.RequesterType.Hero:
                case Requester.RequesterType.Player:
                case Requester.RequesterType.Slot:
                    UIManager.Instance.ActionLog_CreateAction(minion, result);
                    break;
            }
        }
    }

    public override string ToString()
    {
        string name = "";
        switch (_requester.Type)
        {
            case Requester.RequesterType.Minion:
                name = _requester.Minion.Name;
                break;

            case Requester.RequesterType.Spell:
                name = _requester.Spell.Name;
                break;

            case Requester.RequesterType.Hero:
            case Requester.RequesterType.Player:
            case Requester.RequesterType.Slot:
                name = GameManager.Instance.Data.PlayerDataList[(int)_requester.PlayerIndex].PlayerInfo.DisplayName;
                break;
        }

        string formatText;
        LocalizationManager.Instance.GetText(ActionLog.DestroyMinionLogKey, out formatText);

        string result = "";
        foreach (MinionData minion in _targetList)
        {
            if (result.Length > 0)
            {
                result += "\n";
            }

            result += string.Format(
                  formatText
                , CreateNameString(_requester.PlayerIndex, name)
                , CreateNameString(minion.Owner, minion.Name, false)
            );
        }

        return result;
    }
    #endregion
}
#endregion

#region DestroyMinionLog
public class SacrificeMinionLog : ActionLog
{
    #region Public Properties
    #endregion

    #region Protected Properties

    protected Requester _requester;
    protected List<MinionData> _targetList;
    #endregion

    #region Constructors
    public SacrificeMinionLog(Requester requester, List<MinionData> targetList)
    {
        _requester = requester;
        _targetList = targetList;
    }
    #endregion

    #region Methods
    public override void CreateUILog()
    {
        //Debug.Log(GetDebugText());

        string formatText;
        LocalizationManager.Instance.GetText(ActionLog.SacrificeMinionLogKey, out formatText);

        string name = "";
        switch (_requester.Type)
        {
            case Requester.RequesterType.Minion:
                name = _requester.Minion.Name;
                break;

            case Requester.RequesterType.Spell:
                name = _requester.Spell.Name;
                break;

            case Requester.RequesterType.Hero:
            case Requester.RequesterType.Player:
            case Requester.RequesterType.Slot:
                name = GameManager.Instance.Data.PlayerDataList[(int)_requester.PlayerIndex].PlayerInfo.DisplayName;
                break;
        }

        foreach (MinionData minion in _targetList)
        {
            string result = "";

            result = string.Format(
                  formatText
                , CreateNameString(_requester.PlayerIndex, name)
                , CreateNameString(minion.Owner, minion.Name, false)
            );

            switch (_requester.Type)
            {
                case Requester.RequesterType.Minion:
                    UIManager.Instance.ActionLog_CreateAction(_requester.Minion, minion, result, UIActionLog_Action.ActionIcon.Effect);
                    break;

                case Requester.RequesterType.Spell:
                case Requester.RequesterType.Hero:
                case Requester.RequesterType.Player:
                case Requester.RequesterType.Slot:
                    UIManager.Instance.ActionLog_CreateAction(minion, result);
                    break;
            }
        }
    }

    public override string ToString()
    {
        string name = "";
        switch (_requester.Type)
        {
            case Requester.RequesterType.Minion:
                name = _requester.Minion.Name;
                break;

            case Requester.RequesterType.Spell:
                name = _requester.Spell.Name;
                break;

            case Requester.RequesterType.Hero:
            case Requester.RequesterType.Player:
            case Requester.RequesterType.Slot:
                name = GameManager.Instance.Data.PlayerDataList[(int)_requester.PlayerIndex].PlayerInfo.DisplayName;
                break;
        }

        string formatText;
        LocalizationManager.Instance.GetText(ActionLog.SacrificeMinionLogKey, out formatText);

        string result = "";
        foreach (MinionData minion in _targetList)
        {
            if (result.Length > 0)
            {
                result += "\n";
            }

            result += string.Format(
                  formatText
                , CreateNameString(_requester.PlayerIndex, name)
                , CreateNameString(minion.Owner, minion.Name, false)
            );
        }

        return result;
    }
    #endregion
}
#endregion

#region DealPlayerDamageLog
public class DealPlayerDamageLog : ActionLog
{
    #region Public Properties
    #endregion

    #region Protected Properties

    protected Requester _requester;
    protected List<PlayerIndex> _targetList;
    protected int _damage;
    #endregion

    #region Constructors
    public DealPlayerDamageLog(Requester requester, List<PlayerIndex> targetList, int damage)
    {
        _requester = requester;
        _targetList = targetList;
        _damage = damage;
    }
    #endregion

    #region Methods
    public override void CreateUILog()
    {
        //Debug.Log(GetDebugText());

        string formatText;
        LocalizationManager.Instance.GetText(ActionLog.DealPlayerDamageLogKey, out formatText);

        string name = "";
        switch (_requester.Type)
        {
            case Requester.RequesterType.Minion:
            name = _requester.Minion.Name;
            break;

            case Requester.RequesterType.Spell:
            name = _requester.Spell.Name;
            break;

            case Requester.RequesterType.Hero:
            case Requester.RequesterType.Player:
            case Requester.RequesterType.Slot:
            name = GameManager.Instance.Data.PlayerDataList[(int)_requester.PlayerIndex].PlayerInfo.DisplayName;
            break;
        }

        foreach (PlayerIndex playerIndex in _targetList)
        {
            string result = "";
            result = string.Format(
                  formatText
                , CreateNameString(_requester.PlayerIndex, name)
                , CreateNameString(playerIndex, GameManager.Instance.Data.PlayerDataList[(int)playerIndex].PlayerInfo.DisplayName)
                , _damage
            );

            switch (_requester.Type)
            {
                case Requester.RequesterType.Minion:
                UIManager.Instance.ActionLog_CreateAction(_requester.Minion, result);
                break;

                case Requester.RequesterType.Spell:
                case Requester.RequesterType.Hero:
                case Requester.RequesterType.Player:
                case Requester.RequesterType.Slot:
                UIManager.Instance.ActionLog_CreateAction(_requester.PlayerIndex, result);
                break;
            }
        }
    }

    public override string ToString()
    {
        string name = "";
        switch (_requester.Type)
        {
            case Requester.RequesterType.Minion:
            name = _requester.Minion.Name;
            break;

            case Requester.RequesterType.Spell:
            name = _requester.Spell.Name;
            break;

            case Requester.RequesterType.Hero:
            case Requester.RequesterType.Player:
            case Requester.RequesterType.Slot:
            name = GameManager.Instance.Data.PlayerDataList[(int)_requester.PlayerIndex].PlayerInfo.DisplayName;
            break;
        }

        string formatText;
        LocalizationManager.Instance.GetText(ActionLog.DealPlayerDamageLogKey, out formatText);

        string result = "";
        foreach (PlayerIndex playerIndex in _targetList)
        {
            if (result.Length > 0)
            {
                result += "\n";
            }

            result += string.Format(
                  formatText
                , name
                , GameManager.Instance.Data.PlayerDataList[(int)playerIndex].PlayerInfo.DisplayName
                , _damage
            );
        }

        return result;
    }
    #endregion
}
#endregion

#region PhaseLog
public class PhaseLog : ActionLog
{
    #region Public Properties
    #endregion

    #region Protected Properties
    protected DisplayGamePhase _phase;
    #endregion

    #region Constructors
    public PhaseLog(DisplayGamePhase phase)
    {
        _phase = phase;
    }
    #endregion

    #region Methods
    public override void CreateUILog()
    {
        //Debug.Log(GetDebugText());

        UIManager.Instance.ActionLog_CreatePhase(this.ToString());
    }

    public override string ToString()
    {
        string formatText;
        LocalizationManager.Instance.GetText(ActionLog.PhaseLogKey, out formatText);

        string result = string.Format(
              formatText
            , _phase
        );

        return result;
    }
    #endregion
}
#endregion

#region TurnSummaryLog
public class TurnSummaryLog : ActionLog
{
    #region Public Properties
    #endregion

    #region Protected Properties
    protected int _turn;
    protected BattlePlayerData _player_L;
    protected BattlePlayerData _player_R;
    #endregion

    #region Constructors
    public TurnSummaryLog(int turn, BattlePlayerData player_L, BattlePlayerData player_R)
    {
        _turn = turn;
        _player_L = player_L;
        _player_R = player_R;
    }
    #endregion

    #region Methods
    public override void CreateUILog()
    {
        //Debug.Log(GetDebugText());

        UIManager.Instance.ActionLog_CreateSummary(
              _player_L
            , _player_R
            , _turn
            , GameManager.Instance.GetSumSpirit(_player_L.PlayerIndex)
            , GameManager.Instance.GetSumSpirit(_player_R.PlayerIndex)
        );
    }

    public override string ToString()
    {
        string formatText;
        LocalizationManager.Instance.GetText(ActionLog.TurnSummaryLogKey, out formatText);

        string result = string.Format(
              formatText
            , _turn
            , _player_L.PlayerInfo.DisplayName, _player_L.CurrentHP, GameManager.Instance.GetSumSpirit(_player_L.PlayerIndex)
            , _player_R.PlayerInfo.DisplayName, _player_R.CurrentHP, GameManager.Instance.GetSumSpirit(_player_R.PlayerIndex)
        );

        return result;
    }
    #endregion
}
#endregion

#region StartTurnLog
public class StartTurnLog : ActionLog
{
    #region Public Properties
    #endregion

    #region Protected Properties
    protected int _turn;
    protected int _subTurn;
    protected BattlePlayerData _player;
    #endregion

    #region Constructors
    public StartTurnLog(int turn, int subTurn, BattlePlayerData player)
    {
        _turn = turn;
        _subTurn = subTurn;
        _player = player;
    }
    #endregion

    #region Methods
    public override void CreateUILog()
    {
        //Debug.Log(GetDebugText());

        UIManager.Instance.ActionLog_CreatePhase(this.ToString());
    }

    public override string ToString()
    {
        string formatText;
        LocalizationManager.Instance.GetText(ActionLog.StartTurnLogKey, out formatText);

        string result = string.Format(
              formatText
            , CreateNameString(_player.PlayerIndex, _player.PlayerInfo.DisplayName)
        );

        return result;
    }
    #endregion
}
#endregion

#region SetHPLog
public class SetHPLog : ActionLog
{
    #region Public Properties
    #endregion

    #region Protected Properties
    protected Requester _requester;
    protected MinionData _minion;
    protected int _value;
    #endregion

    #region Constructors
    public SetHPLog(Requester requester, MinionData minion, int value)
    {
        _requester = requester;
        _minion = minion;
        _value = value;
    }
    #endregion

    #region Methods
    public override void CreateUILog()
    {
        //Debug.Log(GetDebugText());

        if (_requester.Type == Requester.RequesterType.Minion)
        {
            UIManager.Instance.ActionLog_CreateAction(
                  _requester.Minion
                , _minion
                , this.ToString()
                , UIActionLog_Action.ActionIcon.Effect
            );
        }
        else
        {
            UIManager.Instance.ActionLog_CreateAction(_minion, this.ToString());
        }
    }

    public override string ToString()
    {
        string name = "";
        switch (_requester.Type)
        {
            case Requester.RequesterType.Minion:
            name = _requester.Minion.Name;
            break;

            case Requester.RequesterType.Spell:
            name = _requester.Spell.Name;
            break;

            case Requester.RequesterType.Hero:
            case Requester.RequesterType.Player:
            case Requester.RequesterType.Slot:
            name = GameManager.Instance.Data.PlayerDataList[(int)_requester.PlayerIndex].PlayerInfo.DisplayName;
            break;
        }

        string formatText;
        LocalizationManager.Instance.GetText(ActionLog.SetHPLogKey, out formatText);

        string result = "";
        result = string.Format(
              formatText
            , CreateNameString(_requester.PlayerIndex, name)
            , CreateNameString(_minion.Owner, _minion.Name, false)
            , _value
        );

        return result;
    }
    #endregion
}
#endregion

#region SetAPLog
public class SetAPLog : ActionLog
{
    #region Public Properties
    #endregion

    #region Protected Properties
    protected Requester _requester;
    protected MinionData _minion;
    protected int _value;
    #endregion

    #region Constructors
    public SetAPLog(Requester requester, MinionData minion, int value)
    {
        _requester = requester;
        _minion = minion;
        _value = value;
    }
    #endregion

    #region Methods
    public override void CreateUILog()
    {
        //Debug.Log(GetDebugText());

        if (_requester.Type == Requester.RequesterType.Minion)
        {
            UIManager.Instance.ActionLog_CreateAction(
                  _requester.Minion
                , _minion
                , this.ToString()
                , UIActionLog_Action.ActionIcon.Effect
            );
        }
        else
        {
            UIManager.Instance.ActionLog_CreateAction(_minion, this.ToString());
        }
    }

    public override string ToString()
    {
        string name = "";
        switch (_requester.Type)
        {
            case Requester.RequesterType.Minion:
            name = _requester.Minion.Name;
            break;

            case Requester.RequesterType.Spell:
            name = _requester.Spell.Name;
            break;

            case Requester.RequesterType.Hero:
            case Requester.RequesterType.Player:
            case Requester.RequesterType.Slot:
            name = GameManager.Instance.Data.PlayerDataList[(int)_requester.PlayerIndex].PlayerInfo.DisplayName;
            break;
        }

        string formatText;
        LocalizationManager.Instance.GetText(ActionLog.SetAPLogKey, out formatText);

        string result = "";
        result = string.Format(
              formatText
            , CreateNameString(_requester.PlayerIndex, name)
            , CreateNameString(_minion.Owner, _minion.Name, false)
            , _value
        );

        return result;
    }
    #endregion
}
#endregion

#region SetSPLog
public class SetSPLog : ActionLog
{
    #region Public Properties
    #endregion

    #region Protected Properties
    protected Requester _requester;
    protected MinionData _minion;
    protected int _value;
    #endregion

    #region Constructors
    public SetSPLog(Requester requester, MinionData minion, int value)
    {
        _requester = requester;
        _minion = minion;
        _value = value;
    }
    #endregion

    #region Methods
    public override void CreateUILog()
    {
        //Debug.Log(GetDebugText());

        if (_requester.Type == Requester.RequesterType.Minion)
        {
            UIManager.Instance.ActionLog_CreateAction(
                  _requester.Minion
                , _minion
                , this.ToString()
                , UIActionLog_Action.ActionIcon.Effect
            );
        }
        else
        {
            UIManager.Instance.ActionLog_CreateAction(_minion, this.ToString());
        }
    }

    public override string ToString()
    {
        string name = "";
        switch (_requester.Type)
        {
            case Requester.RequesterType.Minion:
            name = _requester.Minion.Name;
            break;

            case Requester.RequesterType.Spell:
            name = _requester.Spell.Name;
            break;

            case Requester.RequesterType.Hero:
            case Requester.RequesterType.Player:
            case Requester.RequesterType.Slot:
            name = GameManager.Instance.Data.PlayerDataList[(int)_requester.PlayerIndex].PlayerInfo.DisplayName;
            break;
        }

        string formatText;
        LocalizationManager.Instance.GetText(ActionLog.SetSPLogKey, out formatText);

        string result = "";
        result = string.Format(
              formatText
            , CreateNameString(_requester.PlayerIndex, name)
            , CreateNameString(_minion.Owner, _minion.Name, false)
            , _value
        );

        return result;
    }
    #endregion
}
#endregion

#region SetAPHPLog
public class SetAPHPLog : ActionLog
{
    #region Public Properties
    #endregion

    #region Protected Properties
    protected Requester _requester;
    protected MinionData _minion;
    protected int _ap;
    protected int _hp;
    #endregion

    #region Constructors
    public SetAPHPLog(Requester requester, MinionData minion, int apValue, int hpValue)
    {
        _requester = requester;
        _minion = minion;
        _ap = apValue;
        _hp = hpValue;
    }
    #endregion

    #region Methods
    public override void CreateUILog()
    {
        Debug.Log(GetDebugText());

        if (_requester.Type == Requester.RequesterType.Minion)
        {
            UIManager.Instance.ActionLog_CreateAction(
                  _requester.Minion
                , _minion
                , this.ToString()
                , UIActionLog_Action.ActionIcon.Effect
            );
        }
        else
        {
            UIManager.Instance.ActionLog_CreateAction(_minion, this.ToString());
        }
    }

    public override string ToString()
    {
        string name = "";
        switch (_requester.Type)
        {
            case Requester.RequesterType.Minion:
                name = _requester.Minion.Name;
                break;

            case Requester.RequesterType.Spell:
                name = _requester.Spell.Name;
                break;

            case Requester.RequesterType.Hero:
            case Requester.RequesterType.Player:
            case Requester.RequesterType.Slot:
                name = GameManager.Instance.Data.PlayerDataList[(int)_requester.PlayerIndex].PlayerInfo.DisplayName;
                break;
        }

        string formatText;
        LocalizationManager.Instance.GetText(ActionLog.SetAPHPLogKey, out formatText);

        string result = "";
        result = string.Format(
              formatText
            , CreateNameString(_requester.PlayerIndex, name)
            , CreateNameString(_minion.Owner, _minion.Name, false)
            , _ap
            , _hp
        );

        return result;
    }
    #endregion
}
#endregion

#region BoostHPLog
public class BoostHPLog : ActionLog
{
    #region Public Properties
    #endregion

    #region Protected Properties
    protected Requester _requester;
    protected MinionData _minion;
    protected int _value;
    #endregion

    #region Constructors
    public BoostHPLog(Requester requester, MinionData minion, int value)
    {
        _requester = requester;
        _minion = minion;
        _value = value;
    }
    #endregion

    #region Methods
    public override void CreateUILog()
    {
        //Debug.Log(GetDebugText());

        if (_requester.Type == Requester.RequesterType.Minion)
        {
            UIManager.Instance.ActionLog_CreateAction(
                  _requester.Minion
                , _minion
                , this.ToString()
                , _value > 0 ? UIActionLog_Action.ActionIcon.Buff : UIActionLog_Action.ActionIcon.Debuff
            );
        }
        else
        {
            UIManager.Instance.ActionLog_CreateAction(_minion, this.ToString());
        }
    }

    public override string ToString()
    {
        string name = "";
        switch (_requester.Type)
        {
            case Requester.RequesterType.Minion:
            name = _requester.Minion.Name;
            break;

            case Requester.RequesterType.Spell:
            name = _requester.Spell.Name;
            break;

            case Requester.RequesterType.Hero:
            case Requester.RequesterType.Player:
            case Requester.RequesterType.Slot:
            name = GameManager.Instance.Data.PlayerDataList[(int)_requester.PlayerIndex].PlayerInfo.DisplayName;
            break;
        }

        string formatText;
        LocalizationManager.Instance.GetText(ActionLog.BoostHPLogKey, out formatText);

        string result = "";
        result = string.Format(
              formatText
            , CreateNameString(_requester.PlayerIndex, name)
            , CreateNameString(_minion.Owner, _minion.Name)
            , _value
        );

        return result;
    }
    #endregion
}
#endregion

#region BoostAPLog
public class BoostAPLog : ActionLog
{
    #region Public Properties
    #endregion

    #region Protected Properties
    protected Requester _requester;
    protected MinionData _minion;
    protected int _value;
    #endregion

    #region Constructors
    public BoostAPLog(Requester requester, MinionData minion, int value)
    {
        _requester = requester;
        _minion = minion;
        _value = value;
    }
    #endregion

    #region Methods
    public override void CreateUILog()
    {
        //Debug.Log(GetDebugText());

        if (_requester.Type == Requester.RequesterType.Minion)
        {
            UIManager.Instance.ActionLog_CreateAction(
                  _requester.Minion
                , _minion
                , this.ToString()
                , _value > 0 ? UIActionLog_Action.ActionIcon.Buff : UIActionLog_Action.ActionIcon.Debuff
            );
        }
        else
        {
            UIManager.Instance.ActionLog_CreateAction(_minion, this.ToString());
        }
    }

    public override string ToString()
    {
        string name = "";
        switch (_requester.Type)
        {
            case Requester.RequesterType.Minion:
            name = _requester.Minion.Name;
            break;

            case Requester.RequesterType.Spell:
            name = _requester.Spell.Name;
            break;

            case Requester.RequesterType.Hero:
            case Requester.RequesterType.Player:
            case Requester.RequesterType.Slot:
            name = GameManager.Instance.Data.PlayerDataList[(int)_requester.PlayerIndex].PlayerInfo.DisplayName;
            break;
        }

        string formatText;
        LocalizationManager.Instance.GetText(ActionLog.BoostAPLogKey, out formatText);

        string result = "";
        result = string.Format(
              formatText
            , CreateNameString(_requester.PlayerIndex, name)
            , CreateNameString(_minion.Owner, _minion.Name, false)
            , _value
        );

        return result;
    }
    #endregion
}
#endregion

#region BoostSPLog
public class BoostSPLog : ActionLog
{
    #region Public Properties
    #endregion

    #region Protected Properties
    protected Requester _requester;
    protected MinionData _minion;
    protected int _value;
    #endregion

    #region Constructors
    public BoostSPLog(Requester requester, MinionData minion, int value)
    {
        _requester = requester;
        _minion = minion;
        _value = value;
    }
    #endregion

    #region Methods
    public override void CreateUILog()
    {
        //Debug.Log(GetDebugText());

        if (_requester.Type == Requester.RequesterType.Minion)
        {
            UIManager.Instance.ActionLog_CreateAction(
                  _requester.Minion
                , _minion
                , this.ToString()
                , _value > 0 ? UIActionLog_Action.ActionIcon.Buff : UIActionLog_Action.ActionIcon.Debuff
            );
        }
        else
        {
            UIManager.Instance.ActionLog_CreateAction(_minion, this.ToString());
        }
    }

    public override string ToString()
    {
        string name = "";
        switch (_requester.Type)
        {
            case Requester.RequesterType.Minion:
            name = _requester.Minion.Name;
            break;

            case Requester.RequesterType.Spell:
            name = _requester.Spell.Name;
            break;

            case Requester.RequesterType.Hero:
            case Requester.RequesterType.Player:
            case Requester.RequesterType.Slot:
            name = GameManager.Instance.Data.PlayerDataList[(int)_requester.PlayerIndex].PlayerInfo.DisplayName;
            break;
        }

        string formatText;
        LocalizationManager.Instance.GetText(ActionLog.BoostSPLogKey, out formatText);

        string result = "";
        result = string.Format(
              formatText
            , CreateNameString(_requester.PlayerIndex, name)
            , CreateNameString(_minion.Owner, _minion.Name, false)
            , _value
        );

        return result;
    }
    #endregion
}
#endregion

#region BoostAPHPLog
public class BoostAPHPLog : ActionLog
{
    #region Public Properties
    #endregion

    #region Protected Properties
    protected Requester _requester;
    protected MinionData _minion;
    protected int _ap;
    protected int _hp;
    #endregion

    #region Constructors
    public BoostAPHPLog(Requester requester, MinionData minion, int ap, int hp)
    {
        _requester = requester;
        _minion = minion;
        _ap = ap;
        _hp = hp;
    }
    #endregion

    #region Methods
    public override void CreateUILog()
    {
        //Debug.Log(GetDebugText());

        if (_requester.Type == Requester.RequesterType.Minion)
        {
            UIManager.Instance.ActionLog_CreateAction(
                  _requester.Minion
                , _minion
                , this.ToString()
                , ((_ap > 0) || (_hp > 0)) ? UIActionLog_Action.ActionIcon.Buff : UIActionLog_Action.ActionIcon.Debuff
            );
        }
        else
        {
            UIManager.Instance.ActionLog_CreateAction(_minion, this.ToString());
        }
    }

    public override string ToString()
    {
        string name = "";
        switch (_requester.Type)
        {
            case Requester.RequesterType.Minion:
            name = _requester.Minion.Name;
            break;

            case Requester.RequesterType.Spell:
            name = _requester.Spell.Name;
            break;

            case Requester.RequesterType.Hero:
            case Requester.RequesterType.Player:
            case Requester.RequesterType.Slot:
            name = GameManager.Instance.Data.PlayerDataList[(int)_requester.PlayerIndex].PlayerInfo.DisplayName;
            break;
        }

        string formatText;
        LocalizationManager.Instance.GetText(ActionLog.BoostAPHPLogKey, out formatText);

        string result = "";
        result = string.Format(
              formatText
            , CreateNameString(_requester.PlayerIndex, name)
            , CreateNameString(_minion.Owner, _minion.Name, false)
            , _ap
            , _hp
        );

        return result;
    }
    #endregion
}
#endregion

#region BoostArmorLog
public class BoostArmorLog : ActionLog
{
    #region Public Properties
    #endregion

    #region Protected Properties
    protected Requester _requester;
    protected MinionData _minion;
    protected int _armor;
    #endregion

    #region Constructors
    public BoostArmorLog(Requester requester, MinionData minion, int armor)
    {
        _requester = requester;
        _minion = minion;
        _armor = armor;
    }
    #endregion

    #region Methods
    public override void CreateUILog()
    {
        //Debug.Log(GetDebugText());

        if (_requester.Type == Requester.RequesterType.Minion)
        {
            UIManager.Instance.ActionLog_CreateAction(
                  _requester.Minion
                , _minion
                , this.ToString()
                , (_armor > 0) ? UIActionLog_Action.ActionIcon.Buff : UIActionLog_Action.ActionIcon.Debuff
            );
        }
        else
        {
            UIManager.Instance.ActionLog_CreateAction(_minion, this.ToString());
        }
    }

    public override string ToString()
    {
        string name = "";
        switch (_requester.Type)
        {
            case Requester.RequesterType.Minion:
                name = _requester.Minion.Name;
                break;

            case Requester.RequesterType.Spell:
                name = _requester.Spell.Name;
                break;

            case Requester.RequesterType.Hero:
            case Requester.RequesterType.Player:
            case Requester.RequesterType.Slot:
                name = GameManager.Instance.Data.PlayerDataList[(int)_requester.PlayerIndex].PlayerInfo.DisplayName;
                break;
        }

        string formatText;
        LocalizationManager.Instance.GetText(ActionLog.BoostArmorLogKey, out formatText);

        string result = "";
        result = string.Format(
              formatText
            , CreateNameString(_requester.PlayerIndex, name)
            , CreateNameString(_minion.Owner, _minion.Name, false)
            , _armor
        );

        return result;
    }
    #endregion
}
#endregion

#region SwapHPLog
public class SwapHPLog : ActionLog
{
    #region Public Properties
    #endregion

    #region Protected Properties
    protected Requester _requester;
    protected MinionData _minion1;
    protected MinionData _minion2;
    protected int _value1;
    protected int _value2;
    #endregion

    #region Constructors
    public SwapHPLog(Requester requester, MinionData minion1, int value1, MinionData minion2, int value2)
    {
        _requester = requester;
        _minion1 = minion1;
        _value1 = value1;
        _minion2 = minion2;
        _value2 = value2;
    }
    #endregion

    #region Methods
    public override void CreateUILog()
    {
        //Debug.Log(GetDebugText());

        UIManager.Instance.ActionLog_CreateAction(_minion1, _minion2, this.ToString(), UIActionLog_Action.ActionIcon.Effect);
    }

    public override string ToString()
    {
        string name = "";
        switch (_requester.Type)
        {
            case Requester.RequesterType.Minion:
            name = _requester.Minion.Name;
            break;

            case Requester.RequesterType.Spell:
            name = _requester.Spell.Name;
            break;

            case Requester.RequesterType.Hero:
            case Requester.RequesterType.Player:
            case Requester.RequesterType.Slot:
            name = GameManager.Instance.Data.PlayerDataList[(int)_requester.PlayerIndex].PlayerInfo.DisplayName;
            break;
        }

        string formatText;
        LocalizationManager.Instance.GetText(ActionLog.SwapHPLogKey, out formatText);

        string result = "";
        result = string.Format(
              formatText
            , CreateNameString(_requester.PlayerIndex, name)
            ,  CreateNameString(_minion1.Owner, _minion1.Name)
            , _value1
            ,  CreateNameString(_minion2.Owner, _minion2.Name)
            , _value2
        );

        return result;
    }
    #endregion
}
#endregion

#region SwapAPLog
public class SwapAPLog : ActionLog
{
    #region Public Properties
    #endregion

    #region Protected Properties
    protected Requester _requester;
    protected MinionData _minion1;
    protected MinionData _minion2;
    protected int _value1;
    protected int _value2;
    #endregion

    #region Constructors
    public SwapAPLog(Requester requester, MinionData minion1, int value1, MinionData minion2, int value2)
    {
        _requester = requester;
        _minion1 = minion1;
        _value1 = value1;
        _minion2 = minion2;
        _value2 = value2;
    }
    #endregion

    #region Methods
    public override void CreateUILog()
    {
        //Debug.Log(GetDebugText());

        UIManager.Instance.ActionLog_CreateAction(_minion1, _minion2, this.ToString(), UIActionLog_Action.ActionIcon.Effect);
    }

    public override string ToString()
    {
        string name = "";
        switch (_requester.Type)
        {
            case Requester.RequesterType.Minion:
            name = _requester.Minion.Name;
            break;

            case Requester.RequesterType.Spell:
            name = _requester.Spell.Name;
            break;

            case Requester.RequesterType.Hero:
            case Requester.RequesterType.Player:
            case Requester.RequesterType.Slot:
            name = GameManager.Instance.Data.PlayerDataList[(int)_requester.PlayerIndex].PlayerInfo.DisplayName;
            break;
        }

        string formatText;
        LocalizationManager.Instance.GetText(ActionLog.SwapAPLogKey, out formatText);

        string result = "";
        result = string.Format(
              formatText
            , CreateNameString(_requester.PlayerIndex, name)
            , CreateNameString(_minion1.Owner, _minion1.Name, false)
            , _value1
            , CreateNameString(_minion2.Owner, _minion2.Name, false)
            , _value2
        );

        return result;
    }
    #endregion
}
#endregion

#region SwapSPLog
public class SwapSPLog : ActionLog
{
    #region Public Properties
    #endregion

    #region Protected Properties
    protected Requester _requester;
    protected MinionData _minion1;
    protected MinionData _minion2;
    protected int _value1;
    protected int _value2;
    #endregion

    #region Constructors
    public SwapSPLog(Requester requester, MinionData minion1, int value1, MinionData minion2, int value2)
    {
        _requester = requester;
        _minion1 = minion1;
        _value1 = value1;
        _minion2 = minion2;
        _value2 = value2;
    }
    #endregion

    #region Methods
    public override void CreateUILog()
    {
        //Debug.Log(GetDebugText());

        UIManager.Instance.ActionLog_CreateAction(_minion1, _minion2, this.ToString(), UIActionLog_Action.ActionIcon.Effect);
    }

    public override string ToString()
    {
        string name = "";
        switch (_requester.Type)
        {
            case Requester.RequesterType.Minion:
            name = _requester.Minion.Name;
            break;

            case Requester.RequesterType.Spell:
            name = _requester.Spell.Name;
            break;

            case Requester.RequesterType.Hero:
            case Requester.RequesterType.Player:
            case Requester.RequesterType.Slot:
            name = GameManager.Instance.Data.PlayerDataList[(int)_requester.PlayerIndex].PlayerInfo.DisplayName;
            break;
        }

        string formatText;
        LocalizationManager.Instance.GetText(ActionLog.SwapSPLogKey, out formatText);

        string result = "";
        result = string.Format(
              formatText
            , CreateNameString(_requester.PlayerIndex, name)
            , CreateNameString(_minion1.Owner, _minion1.Name, false)
            , _value1
            , CreateNameString(_minion2.Owner, _minion2.Name, false)
            , _value2
        );

        return result;
    }
    #endregion
}
#endregion

#region SwapAPHPLog
public class SwapAPHPLog : ActionLog
{
    #region Public Properties
    #endregion

    #region Protected Properties
    protected Requester _requester;
    protected MinionData _minion1;
    protected MinionData _minion2;
    protected int _ap1;
    protected int _ap2;
    protected int _hp1;
    protected int _hp2;
    #endregion

    #region Constructors
    public SwapAPHPLog(Requester requester
        , MinionData minion1, int ap1, int hp1
        , MinionData minion2, int ap2, int hp2
    )
    {
        _requester = requester;

        _minion1 = minion1;
        _ap1 = ap1;
        _hp1 = hp1;

        _minion2 = minion2;
        _ap2 = ap2;
        _hp2 = hp2;
    }
    #endregion

    #region Methods
    public override void CreateUILog()
    {
        //Debug.Log(GetDebugText());

        UIManager.Instance.ActionLog_CreateAction(_minion1, _minion2, this.ToString(), UIActionLog_Action.ActionIcon.Effect);
    }

    public override string ToString()
    {
        string name = "";
        switch (_requester.Type)
        {
            case Requester.RequesterType.Minion:
            name = _requester.Minion.Name;
            break;

            case Requester.RequesterType.Spell:
            name = _requester.Spell.Name;
            break;

            case Requester.RequesterType.Hero:
            case Requester.RequesterType.Player:
            case Requester.RequesterType.Slot:
            name = GameManager.Instance.Data.PlayerDataList[(int)_requester.PlayerIndex].PlayerInfo.DisplayName;
            break;
        }

        string formatText;
        LocalizationManager.Instance.GetText(ActionLog.SwapAPHPLogKey, out formatText);

        string result = "";
        result = string.Format(
              formatText
            , CreateNameString(_requester.PlayerIndex, name)
            , CreateNameString(_minion1.Owner, _minion1.Name, false)
            , _ap1
            , _hp1
            , CreateNameString(_minion2.Owner, _minion2.Name, false)
            , _ap2
            , _hp2
        );

        return result;
    }
    #endregion
}
#endregion

#region BuffFearLog
public class BuffFearLog : ActionLog
{
    #region Public Properties
    #endregion

    #region Protected Properties
    protected Requester _requester;
    protected MinionData _minion;
    protected bool _isFear;
    protected bool _isRepeated;
    #endregion

    #region Constructors
    public BuffFearLog(Requester requester, MinionData minion, bool isFear, bool isRepeated)
    {
        _requester = requester;
        _minion = minion;
        _isFear = isFear;
        _isRepeated = isRepeated;
    }
    #endregion

    #region Methods
    public override void CreateUILog()
    {
        if (_requester.Minion != null)
        {
            if(_isFear)
            {
                UIManager.Instance.ActionLog_CreateAction(_requester.Minion, _minion, this.ToString(), UIActionLog_Action.ActionIcon.Effect);
            }
            else
            {
                UIManager.Instance.ActionLog_CreateAction(_minion, this.ToString());
            }
        }
        else
        {
            UIManager.Instance.ActionLog_CreateAction(_minion, this.ToString());
        }
    }

    public override string ToString()
    {
        string name = "";
        switch (_requester.Type)
        {
            case Requester.RequesterType.Minion:
            name = _requester.Minion.Name;
            break;

            case Requester.RequesterType.Spell:
            name = _requester.Spell.Name;
            break;

            case Requester.RequesterType.Hero:
            case Requester.RequesterType.Player:
            case Requester.RequesterType.Slot:
            name = GameManager.Instance.Data.PlayerDataList[(int)_requester.PlayerIndex].PlayerInfo.DisplayName;
            break;
        }

        string formatText;
        string keyString;
        if (_isFear)
        {
            if (_isRepeated)
            {
                keyString = BuffFearRepeatLogKey;
            }
            else
            {
                keyString = BuffFearLogKey;
            }
        }
        else
        {
            keyString = BuffFearClearLogKey;
        }

        LocalizationManager.Instance.GetText(keyString, out formatText);

        string result = "";
        result = string.Format(
              formatText
            , CreateNameString(_requester.PlayerIndex, name)
            , CreateNameString(_minion.Owner, _minion.Name, false)
        );

        return result;
    }
    #endregion
}
#endregion

#region BuffFreezeLog
public class BuffFreezeLog : ActionLog
{
    #region Public Properties
    #endregion

    #region Protected Properties
    protected Requester _requester;
    protected MinionData _minion;
    protected bool _isFreeze;
    protected bool _isRepeated;
    #endregion

    #region Constructors
    public BuffFreezeLog(Requester requester, MinionData minion, bool isFreeze, bool isRepeated)
    {
        _requester = requester;
        _minion = minion;
        _isFreeze = isFreeze;
        _isRepeated = isRepeated;
    }
    #endregion

    #region Methods
    public override void CreateUILog()
    {
        if (_requester.Minion != null)
        {
            if (_isFreeze)
            {
                UIManager.Instance.ActionLog_CreateAction(_requester.Minion, _minion, this.ToString(), UIActionLog_Action.ActionIcon.Effect);
            }
            else
            {
                UIManager.Instance.ActionLog_CreateAction(_minion, this.ToString());
            }
        }
        else
        {
            UIManager.Instance.ActionLog_CreateAction(_minion, this.ToString());
        }
    }

    public override string ToString()
    {
        string name = "";
        switch (_requester.Type)
        {
            case Requester.RequesterType.Minion:
            name = _requester.Minion.Name;
            break;

            case Requester.RequesterType.Spell:
            name = _requester.Spell.Name;
            break;

            case Requester.RequesterType.Hero:
            case Requester.RequesterType.Player:
            case Requester.RequesterType.Slot:
            name = GameManager.Instance.Data.PlayerDataList[(int)_requester.PlayerIndex].PlayerInfo.DisplayName;
            break;
        }

        string formatText;
        string keyString;
        if(_isFreeze)
        {
            if(_isRepeated)
            {
                keyString = BuffFreezeRepeatLogKey;
            }
            else
            {
                keyString = BuffFreezeLogKey;
            }
        }
        else
        {
            keyString = BuffFreezeClearLogKey;
        }

        LocalizationManager.Instance.GetText(keyString, out formatText);

        string result = "";
        result = string.Format(
              formatText
            , CreateNameString(_requester.PlayerIndex, name)
            , CreateNameString(_minion.Owner, _minion.Name, false)
        );

        return result;
    }
    #endregion
}
#endregion

#region BuffSilenceLog
public class BuffSilenceLog : ActionLog
{
    #region Public Properties
    #endregion

    #region Protected Properties
    protected Requester _requester;
    protected MinionData _minion;
    protected bool _isSilence;
    protected bool _isRepeated;
    #endregion

    #region Constructors
    public BuffSilenceLog(Requester requester, MinionData minion, bool isSilence, bool isRepeated)
    {
        _requester = requester;
        _minion = minion;
        _isSilence = isSilence;
        _isRepeated = isRepeated;
    }
    #endregion

    #region Methods
    public override void CreateUILog()
    {
        if (_requester.Minion != null)
        {
            if (_isSilence)
            {
                UIManager.Instance.ActionLog_CreateAction(_requester.Minion, _minion, this.ToString(), UIActionLog_Action.ActionIcon.Effect);
            }
            else
            {
                UIManager.Instance.ActionLog_CreateAction(_minion, this.ToString());
            }
        }
        else
        {
            UIManager.Instance.ActionLog_CreateAction(_minion, this.ToString());
        }
    }

    public override string ToString()
    {
        string name = "";
        switch (_requester.Type)
        {
            case Requester.RequesterType.Minion:
            name = _requester.Minion.Name;
            break;

            case Requester.RequesterType.Spell:
            name = _requester.Spell.Name;
            break;

            case Requester.RequesterType.Hero:
            case Requester.RequesterType.Player:
            case Requester.RequesterType.Slot:
            name = GameManager.Instance.Data.PlayerDataList[(int)_requester.PlayerIndex].PlayerInfo.DisplayName;
            break;
        }

        string formatText;
        string keyString;
        if (_isSilence)
        {
            if (_isRepeated)
            {
                keyString = BuffSilenceRepeatLogKey;
            }
            else
            {
                keyString = BuffSilenceLogKey;
            }
        }
        else
        {
            keyString = BuffSilenceClearLogKey;
        }

        LocalizationManager.Instance.GetText(keyString, out formatText);

        string result = "";
        result = string.Format(
              formatText
            , CreateNameString(_requester.PlayerIndex, name)
            , CreateNameString(_minion.Owner, _minion.Name, false)
        );

        return result;
    }
    #endregion
}
#endregion

#region BuffStealthLog
public class BuffStealthLog : ActionLog
{
    #region Public Properties
    #endregion

    #region Protected Properties
    protected Requester _requester;
    protected MinionData _minion;
    protected bool _isStealth;
    protected bool _isRepeated;
    #endregion

    #region Constructors
    public BuffStealthLog(Requester requester, MinionData minion, bool isStealth, bool isRepeated)
    {
        _requester = requester;
        _minion = minion;
        _isStealth = isStealth;
        _isRepeated = isRepeated;
    }
    #endregion

    #region Methods
    public override void CreateUILog()
    {
        if (_requester.Minion != null)
        {
            if (_isStealth)
            {
                UIManager.Instance.ActionLog_CreateAction(_requester.Minion, _minion, this.ToString(), UIActionLog_Action.ActionIcon.Effect);
            }
            else
            {
                UIManager.Instance.ActionLog_CreateAction(_minion, this.ToString());
            }
        }
        else
        {
            UIManager.Instance.ActionLog_CreateAction(_minion, this.ToString());
        }
    }

    public override string ToString()
    {
        string name = "";
        switch (_requester.Type)
        {
            case Requester.RequesterType.Minion:
                name = _requester.Minion.Name;
                break;

            case Requester.RequesterType.Spell:
                name = _requester.Spell.Name;
                break;

            case Requester.RequesterType.Hero:
            case Requester.RequesterType.Player:
            case Requester.RequesterType.Slot:
                name = GameManager.Instance.Data.PlayerDataList[(int)_requester.PlayerIndex].PlayerInfo.DisplayName;
                break;
        }

        string formatText;
        string keyString;
        if (_isStealth)
        {
            if (_isRepeated)
            {
                keyString = BuffStealthRepeatLogKey;
            }
            else
            {
                keyString = BuffStealthLogKey;
            }
        }
        else
        {
            keyString = BuffStealthClearLogKey;
        }

        LocalizationManager.Instance.GetText(keyString, out formatText);

        string result = "";
        result = string.Format(
              formatText
            , CreateNameString(_requester.PlayerIndex, name)
            , CreateNameString(_minion.Owner, _minion.Name, false)
        );

        return result;
    }
    #endregion
}
#endregion

#region BuffDarkMatterLog
public class BuffDarkMatterLog : ActionLog
{
    #region Public Properties
    #endregion

    #region Protected Properties
    protected MinionData _minion;
    protected bool _isBeDarkMatter;
    protected bool _isRepeated;
    #endregion

    #region Constructors
    public BuffDarkMatterLog(MinionData minion, bool isBeDarkMatter, bool isRepeated)
    {
        _minion = minion;
        _isBeDarkMatter = isBeDarkMatter;
        _isRepeated = isRepeated;
    }
    #endregion

    #region Methods
    public override void CreateUILog()
    {
        UIManager.Instance.ActionLog_CreateAction(_minion, this.ToString());
    }

    public override string ToString()
    {
        string formatText;
        string keyString;
        if (_isBeDarkMatter)
        {
            if (_isRepeated)
            {
                keyString = BuffDarkMatterRepeatLogKey;
            }
            else
            {
                keyString = BuffDarkMatterLogKey;
            }
        }
        else
        {
            keyString = BuffDarkMatterClearLogKey;
        }

        LocalizationManager.Instance.GetText(keyString, out formatText);

        string result = "";
        result = string.Format(
              formatText
            , CreateNameString(_minion.Owner, _minion.Name, false)
        );

        return result;
    }
    #endregion
}
#endregion

#region BuffInvincibleLog
public class BuffInvincibleLog : ActionLog
{
    #region Public Properties
    #endregion

    #region Protected Properties
    protected Requester _requester;
    protected MinionData _minion;
    protected bool _isInvincible;
    protected bool _isRepeated;
    #endregion

    #region Constructors
    public BuffInvincibleLog(Requester requester, MinionData minion, bool isInvincible, bool isRepeated)
    {
        _requester = requester;
        _minion = minion;
        _isInvincible = isInvincible;
        _isRepeated = isRepeated;
    }
    #endregion

    #region Methods
    public override void CreateUILog()
    {
        if (_requester.Minion != null)
        {
            if (_isInvincible)
            {
                UIManager.Instance.ActionLog_CreateAction(_requester.Minion, _minion, this.ToString(), UIActionLog_Action.ActionIcon.Effect);
            }
            else
            {
                UIManager.Instance.ActionLog_CreateAction(_minion, this.ToString());
            }
        }
        else
        {
            UIManager.Instance.ActionLog_CreateAction(_minion, this.ToString());
        }
    }

    public override string ToString()
    {
        string name = "";
        switch (_requester.Type)
        {
            case Requester.RequesterType.Minion:
                name = _requester.Minion.Name;
                break;

            case Requester.RequesterType.Spell:
                name = _requester.Spell.Name;
                break;

            case Requester.RequesterType.Hero:
            case Requester.RequesterType.Player:
            case Requester.RequesterType.Slot:
                name = GameManager.Instance.Data.PlayerDataList[(int)_requester.PlayerIndex].PlayerInfo.DisplayName;
                break;
        }

        string formatText;
        string keyString;
        if (_isInvincible)
        {
            if (_isRepeated)
            {
                keyString = BuffInvincibleRepeatLogKey;
            }
            else
            {
                keyString = BuffInvincibleLogKey;
            }
        }
        else
        {
            keyString = BuffInvincibleClearLogKey;
        }

        LocalizationManager.Instance.GetText(keyString, out formatText);

        string result = "";
        result = string.Format(
              formatText
            , CreateNameString(_requester.PlayerIndex, name)
            , CreateNameString(_minion.Owner, _minion.Name, false)
        );

        return result;
    }
    #endregion
}
#endregion

#region BuffUntargetableLog
public class BuffUntargetableLog : ActionLog
{
    #region Public Properties
    #endregion

    #region Protected Properties
    protected Requester _requester;
    protected MinionData _minion;
    protected bool _isUntargetable;
    protected bool _isRepeated;
    #endregion

    #region Constructors
    public BuffUntargetableLog(Requester requester, MinionData minion, bool isUntargetable, bool isRepeated)
    {
        _requester = requester;
        _minion = minion;
        _isUntargetable = isUntargetable;
        _isRepeated = isRepeated;
    }
    #endregion

    #region Methods
    public override void CreateUILog()
    {
        if (_requester.Minion != null)
        {
            if(_isUntargetable)
            {
                UIManager.Instance.ActionLog_CreateAction(_requester.Minion, _minion, this.ToString(), UIActionLog_Action.ActionIcon.Effect);
            }
            else
            {
                UIManager.Instance.ActionLog_CreateAction(_minion, this.ToString());
            }
        }
        else
        {
            UIManager.Instance.ActionLog_CreateAction(_minion, this.ToString());
        }
    }

    public override string ToString()
    {
        string name = "";
        switch (_requester.Type)
        {
            case Requester.RequesterType.Minion:
                name = _requester.Minion.Name;
                break;

            case Requester.RequesterType.Spell:
                name = _requester.Spell.Name;
                break;

            case Requester.RequesterType.Hero:
            case Requester.RequesterType.Player:
            case Requester.RequesterType.Slot:
                name = GameManager.Instance.Data.PlayerDataList[(int)_requester.PlayerIndex].PlayerInfo.DisplayName;
                break;
        }

        string formatText;
        string keyString;
        if (_isUntargetable)
        {
            if (_isRepeated)
            {
                keyString = BuffUntargetableRepeatLogKey;
            }
            else
            {
                keyString = BuffUntargetableLogKey;
            }
        }
        else
        {
            keyString = BuffUntargetableClearLogKey;
        }

        LocalizationManager.Instance.GetText(keyString, out formatText);

        string result = "";
        result = string.Format(
              formatText
            , CreateNameString(_requester.PlayerIndex, name)
            , CreateNameString(_minion.Owner, _minion.Name, false)
        );

        return result;
    }
    #endregion
}
#endregion

#region BuffPiercingLog
public class BuffPiercingLog : ActionLog
{
    #region Public Properties
    #endregion

    #region Protected Properties
    protected Requester _requester;
    protected MinionData _minion;
    protected bool _isPiercing;
    protected bool _isRepeated;
    #endregion

    #region Constructors
    public BuffPiercingLog(Requester requester, MinionData minion, bool isPiercing, bool isRepeated)
    {
        _requester = requester;
        _minion = minion;
        _isPiercing = isPiercing;
        _isRepeated = isRepeated;
    }
    #endregion

    #region Methods
    public override void CreateUILog()
    {
        if (_requester.Minion != null)
        {
            if (_isPiercing)
            {
                UIManager.Instance.ActionLog_CreateAction(_requester.Minion, _minion, this.ToString(), UIActionLog_Action.ActionIcon.Effect);
            }
            else
            {
                UIManager.Instance.ActionLog_CreateAction(_minion, this.ToString());
            }
        }
        else
        {
            UIManager.Instance.ActionLog_CreateAction(_minion, this.ToString());
        }
    }

    public override string ToString()
    {
        string name = "";
        switch (_requester.Type)
        {
            case Requester.RequesterType.Minion:
                name = _requester.Minion.Name;
                break;

            case Requester.RequesterType.Spell:
                name = _requester.Spell.Name;
                break;

            case Requester.RequesterType.Hero:
            case Requester.RequesterType.Player:
            case Requester.RequesterType.Slot:
                name = GameManager.Instance.Data.PlayerDataList[(int)_requester.PlayerIndex].PlayerInfo.DisplayName;
                break;
        }

        string formatText;
        string keyString;
        if (_isPiercing)
        {
            if (_isRepeated)
            {
                keyString = BuffPiercingRepeatLogKey;
            }
            else
            {
                keyString = BuffPiercingLogKey;
            }
        }
        else
        {
            keyString = BuffPiercingClearLogKey;
        }

        LocalizationManager.Instance.GetText(keyString, out formatText);

        string result = "";
        result = string.Format(
              formatText
            , CreateNameString(_requester.PlayerIndex, name)
            , CreateNameString(_minion.Owner, _minion.Name, false)
        );

        return result;
    }
    #endregion
}
#endregion

#region BuffSlotLockLog
public class BuffSlotLockLog : ActionLog
{
    #region Public Properties
    #endregion

    #region Protected Properties
    protected Requester _requester;
    protected int _slotID;
    protected bool _isSlotLock;
    #endregion

    #region Constructors
    public BuffSlotLockLog(Requester requester, int slotID, bool isSlotLock)
    {
        _requester = requester;
        _slotID = slotID;
        _isSlotLock = isSlotLock;
    }
    #endregion

    #region Methods
    public override void CreateUILog()
    {
        if (_requester.Minion != null)
        {
            UIManager.Instance.ActionLog_CreateAction(_requester.Minion, this.ToString());
        }
        else
        {
            UIManager.Instance.ActionLog_CreateAction(_requester.PlayerIndex, this.ToString());
        }
    }

    public override string ToString()
    {
        string name = "";
        switch (_requester.Type)
        {
            case Requester.RequesterType.Minion:
                name = _requester.Minion.Name;
                break;

            case Requester.RequesterType.Spell:
                name = _requester.Spell.Name;
                break;

            case Requester.RequesterType.Hero:
            case Requester.RequesterType.Player:
            case Requester.RequesterType.Slot:
                name = GameManager.Instance.Data.PlayerDataList[(int)_requester.PlayerIndex].PlayerInfo.DisplayName;
                break;
        }

        string formatText;
        string keyString;
        if (_isSlotLock)
        {
            keyString = BuffSlotLockLogKey;
        }
        else
        {
            keyString = BuffSlotLockClearLogKey;
        }

        LocalizationManager.Instance.GetText(keyString, out formatText);

        string result = "";
        result = string.Format(
              formatText
            , GameHelper.SlotIDToSlotCode(_slotID)
        );

        return result;
    }
    #endregion
}
#endregion

#region BuffSlotDarkMatterLog
public class BuffSlotDarkMatterLog : ActionLog
{
    #region Public Properties
    #endregion

    #region Protected Properties
    protected Requester _requester;
    protected int _slotID;
    protected bool _isDarkMatter;
    #endregion

    #region Constructors
    public BuffSlotDarkMatterLog(Requester requester, int slotID, bool isDarkMatter)
    {
        _requester = requester;
        _slotID = slotID;
        _isDarkMatter = isDarkMatter;
    }
    #endregion

    #region Methods
    public override void CreateUILog()
    {
        if (_requester.Minion != null)
        {
            UIManager.Instance.ActionLog_CreateAction(_requester.Minion, this.ToString());
        }
        else
        {
            UIManager.Instance.ActionLog_CreateAction(_requester.PlayerIndex, this.ToString());
        }
    }

    public override string ToString()
    {
        string name = "";
        switch (_requester.Type)
        {
            case Requester.RequesterType.Minion:
                name = _requester.Minion.Name;
                break;

            case Requester.RequesterType.Spell:
                name = _requester.Spell.Name;
                break;

            case Requester.RequesterType.Hero:
            case Requester.RequesterType.Player:
            case Requester.RequesterType.Slot:
                name = GameManager.Instance.Data.PlayerDataList[(int)_requester.PlayerIndex].PlayerInfo.DisplayName;
                break;
        }

        string formatText;
        string keyString;
        if (_isDarkMatter)
        {
            keyString = BuffSlotDarkMatterLogKey;
        }
        else
        {
            keyString = BuffSlotDarkMatterClearLogKey;
        }

        LocalizationManager.Instance.GetText(keyString, out formatText);

        string result = "";
        result = string.Format(
              formatText
            , GameHelper.SlotIDToSlotCode(_slotID)
        );

        return result;
    }
    #endregion
}
#endregion