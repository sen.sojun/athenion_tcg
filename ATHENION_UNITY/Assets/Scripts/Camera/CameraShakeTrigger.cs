﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShakeTrigger : MonoBehaviour {

    public void CameraShake(float strength)
    {
        UIManager.Instance.CameraScreenShake(strength);
    }

}
