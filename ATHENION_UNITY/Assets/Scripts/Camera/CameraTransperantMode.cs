﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTransperantMode : MonoBehaviour 
{
    public TransparencySortMode Mode;

    private void Start()
    {
        GetComponent<Camera>().transparencySortMode = Mode;
    }
}
