﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(Camera))]
public class CameraPositionSetup : MonoBehaviour
{
    public Vector3 Origin = new Vector3(0f, 0f, -0.188f);
    public float Angle = 80.0f;
    public float farClipOffset = 1.3f;

    public Vector3 WidthMarker = new Vector3(4f, 0f, 0f);       // new Vector3(4f, 0f, -0.188f);
    public Vector3 HeightMarker = new Vector3(0f, 0f, 7.208f);  // new Vector3(0f, 0f, 7.02f);

    public ScreenFitMode FitMode { get; private set; }

    // Use this for initialization
    void Start () 
    {
    }
	
	// Update is called once per frame
	void Update () 
    {
        #if UNITY_EDITOR
        if (
               SceneManager.GetActiveScene().name != GameHelper.BattleSceneName
            && SceneManager.GetActiveScene().name != GameHelper.BattleUISceneName
        )
        {
            UpdateCameraPosition();
        }
        #endif
    }

    private void OnEnable()
    {
        UpdateCameraPosition();
    }

    public void UpdateCameraPosition()
    {
        Vector3 currentDir = (Quaternion.Euler(Angle, 0f, 0f) * Vector3.forward).normalized;

        if (currentDir.magnitude > 0.0f)
        {
            transform.forward = currentDir;

            switch (GameHelper.GetScreenFitMode())
            {
                case ScreenFitMode.Width:
                {
                    // match width
                    //Debug.Log("match width!");
                    {
                        Camera camera = GetComponent<Camera>();
                        Vector3 position = WidthMarker;
                        float angle = GameHelper.GetHorizontalAngle(camera);

                        float height = Mathf.Abs(position.x / Mathf.Tan(Mathf.Deg2Rad * angle * 0.5f));

                        transform.position = Origin + (-currentDir * height);
                        camera.orthographicSize = 1.0f;
                        camera.nearClipPlane = 0.3f;
                        camera.farClipPlane = Mathf.Ceil(height * farClipOffset);
                    }
                }
                break;

                case ScreenFitMode.Height:
                default:
                {
                    // match height
                    //Debug.Log("match height!");
                    {
                        Camera camera = GetComponent<Camera>();
                        Vector3 position = HeightMarker;
                        float angle = GameHelper.GetVerticalAngle(camera);

                        float height = Mathf.Abs(position.z / Mathf.Tan(Mathf.Deg2Rad * angle * 0.5f));

                        transform.position = Origin + (-currentDir * height);
                        camera.orthographicSize = 1.0f;
                        camera.nearClipPlane = 0.3f;
                        camera.farClipPlane = Mathf.Ceil(height * farClipOffset);
                    }
                }
                break;
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawSphere(transform.position, 0.1f);

        Gizmos.color = Color.red;
        Gizmos.DrawSphere(Origin, 0.1f);

        Gizmos.color = Color.white;
        Gizmos.DrawLine(transform.position, Origin);

        {
            Gizmos.color = Color.white;
            Gizmos.DrawLine(transform.position, Origin + WidthMarker);
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(Origin + WidthMarker, 0.1f);
        }

        {
            Gizmos.color = Color.white;
            Gizmos.DrawLine(transform.position, Origin + HeightMarker);
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(Origin + HeightMarker, 0.1f);
        }
    }
}
