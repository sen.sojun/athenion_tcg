using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using PlayFab;
using PlayFab.ClientModels;
using LoginResult = PlayFab.ClientModels.LoginResult;
using Newtonsoft.Json;
using System;
using Facebook.Unity;

[Serializable]
public struct CloudScriptResultProcess
{
    #region Enums
    public enum ResultTypes
    {
        None = 0
        , Success
        , Fail
        , Error
    }
    #endregion

    #region Properties
    public bool IsSuccess { get; private set; }
    public ResultTypes ResultType { get; private set; }
    public string MessageValue { get; private set; }
    public string MessageDetail { get; private set; }
    public Dictionary<string, string> CustomData { get; private set; }
    #endregion

    #region Constructors
    public CloudScriptResultProcess(ExecuteCloudScriptResult result)
    {
        IsSuccess = false;
        ResultType = ResultTypes.None;
        MessageValue = string.Empty;
        MessageDetail = "UNKNOWN";
        CustomData = new Dictionary<string, string>();
        if (result.FunctionResult != null)
        {
            CustomData = JsonConvert.DeserializeObject<Dictionary<string, string>>(result.FunctionResult.ToString());
        }

        if (CustomData != null && CustomData.ContainsKey("messageValue"))
        {
            string[] resultSplit = CustomData["messageValue"].Split(':');
            if (resultSplit.Length > 1)
            {
                IsSuccess = resultSplit[0] == "1" ? true : false;
                MessageValue = resultSplit[1];

                string[] messageDetail = MessageValue.Split('=');
                ResultTypes type = ResultTypes.None;
                if (GameHelper.StrToEnum(messageDetail[0], out type))
                {
                    ResultType = type;
                }

                if (messageDetail.Length > 1)
                {
                    MessageDetail = messageDetail[1];
                }
            }
        }
    }
    #endregion

    #region Methods
    #endregion
}

public class PlayFabManager : MonoSingleton<PlayFabManager>
{
    #region Enums
    /// <summary>
    /// Supported Authentication types
    /// Note: Add types to there to support more AuthTypes
    /// See - https://api.playfab.com/documentation/client#Authentication
    /// </summary>
    //public enum Authtypes
    //{
    //    None,
    //    Guest,
    //    UsernameAndPassword,
    //    EmailAndPassword,
    //    AddEmailPassword,
    //    RegisterNewAccount,
    //    Steam,
    //    Facebook,
    //    GooglePlayGames
    //}

    public enum PushNotificationTypes
    {
        QUEST_COMPLETE
        , ACHIEVEMENT_COMPLETE
        , INBOX_MESSAGE
    }
    #endregion

    #region Public Properties
    //Events to subscribe to for this service
    public delegate void DisplayAuthenticationEvent();
    public static event DisplayAuthenticationEvent OnDisplayAuthentication;

    //These are fields that we set when we are using the service.
    public GetPlayerCombinedInfoRequestParams InfoRequestParams;

    //Accessbility for PlayFab ID & Session Tickets
    public static string PlayFabId { get { return _playFabId; } }
    public static string SessionTicket { get { return _sessionTicket; } }

    /// <summary>
    /// Remember the type of authenticate for the user
    /// </summary>
    //public Authtypes AuthType { get; private set; }
    #endregion

    #region Private Properties
    public static readonly string CatalogMainID = "CT_MAIN";
    public static readonly string StoreSystemID = "ST_STARTER";

    public static readonly string StarterItemID = "BUND_STARTER";

    private static string _playFabId;
    private static string _sessionTicket;
    #endregion

    #region Methods

    #region Authentication & Profile


    public void CheckPlayerFirstTimeLogin(UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript(
            "CheckFirstTimeLogin"
            , new
            {
                SessionTicket = _sessionTicket
            }
            , onComplete
            , onFail
        );
    }

    public void AuthenticateCustomID(string customID, UnityAction<LoginResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabClientAPI.LoginWithCustomID(new LoginWithCustomIDRequest()
        {
            TitleId = PlayFabSettings.TitleId,
            CustomId = customID,
            InfoRequestParameters = InfoRequestParams
        },

        (result) =>
        {
            //Store identity and session
            _playFabId = result.PlayFabId;
            _sessionTicket = result.SessionTicket;

            //Debug.Log(_sessionTicket);

            if (onComplete != null)
            {
                //report login result back to subscriber
                onComplete.Invoke(result);
            }
        },

        (error) =>
        {
            if (onFail != null)
            {
                //report error back to subscriber
                onFail.Invoke(error);
            }
        });
        return;
    }

    public void AuthenticateEmailPassword(string email, string password, UnityAction<LoginResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        //We have not opted for remember me in a previous session, so now we have to login the user with email & password.
        PlayFabClientAPI.LoginWithEmailAddress(new LoginWithEmailAddressRequest()
        {
            TitleId = PlayFabSettings.TitleId,
            Email = email,
            Password = password,
            InfoRequestParameters = InfoRequestParams
        },

        (result) =>
        {
            //store identity and session
            _playFabId = result.PlayFabId;
            _sessionTicket = result.SessionTicket;

            if (onComplete != null)
            {
                //report login result back to subscriber
                onComplete.Invoke(result);
            }
        },

        (error) =>
        {
            if (onFail != null)
            {
                //Report error back to subscriber
                onFail.Invoke(error);
            }
        });
    }

    public void AddEmailAndPassword(string email, string password, UnityAction<LoginResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        //Any time we attempt to register a player, first silently authenticate the player.
        //This will retain the players True Origination (Android, iOS, Desktop)
        AuthenticateAsGuest((result) =>
        {
            //Debug.Log("AuthGuest Complete");
            if (result == null)
            {
                //something went wrong with Silent Authentication, Check the debug console.
                onFail.Invoke(new PlayFabError()
                {
                    Error = PlayFabErrorCode.UnknownError,
                    ErrorMessage = "Silent Authentication by Device failed"
                });
            }

            //Note: If silent auth is success, which is should always be and the following 
            //below code fails because of some error returned by the server ( like invalid email or bad password )
            //this is okay, because the next attempt will still use the same silent account that was already created.

            //Now add our username & password.
            PlayFabClientAPI.AddUsernamePassword(new AddUsernamePasswordRequest()
            {
                Username = result.PlayFabId, //Because it is required & Unique and not supplied by User.
                Email = email,
                Password = password,
            },
            (addResult) =>
            {
                //Debug.Log("AddUsername Complete");
                if (onComplete != null)
                {
                    //Store identity and session
                    _playFabId = result.PlayFabId;
                    _sessionTicket = result.SessionTicket;

                    UnlinkGuestAuth(delegate ()
                    {
                        //Report login result back to subscriber.
                        //Debug.Log("UnlinkGuest Complete");
                        onComplete.Invoke(result);
                    }
                    , onFail);
                }
            },

            (error) =>
            {
                if (onFail != null)
                {
                    //Report error result back to subscriber
                    onFail.Invoke(error);
                }
            });

        }, onFail);
    }

    public void AuthenticateFacebook(string accessToken, UnityAction<LoginResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        //#if FACEBOOK
        if (FB.IsInitialized && FB.IsLoggedIn && !string.IsNullOrEmpty(accessToken))
        {
            PlayFabClientAPI.LoginWithFacebook(new LoginWithFacebookRequest()
            {
                TitleId = PlayFabSettings.TitleId,
                AccessToken = accessToken,
                CreateAccount = true,
                InfoRequestParameters = InfoRequestParams
            },

            (result) =>
            {
                //Store Identity and session
                _playFabId = result.PlayFabId;
                _sessionTicket = result.SessionTicket;

                //check if we want to get this callback directly or send to event subscribers.
                if (onComplete != null)
                {
                    //report login result back to the subscriber
                    onComplete.Invoke(result);
                }
            },

            (error) =>
            {
                //report errro back to the subscriber
                if (onFail != null)
                {
                    onFail.Invoke(error);
                }
            });
        }
        else
        {
            if (OnDisplayAuthentication != null)
            {
                OnDisplayAuthentication.Invoke();
            }
        }
        //#endif
    }

    public void AuthenticateGooglePlayGames(string authTicket, UnityAction<LoginResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabClientAPI.LoginWithGoogleAccount(new LoginWithGoogleAccountRequest()
        {
            TitleId = PlayFabSettings.TitleId,
            ServerAuthCode = authTicket,
            InfoRequestParameters = InfoRequestParams,
            CreateAccount = true
        },
        (result) =>
        {
            //Store Identity and session
            _playFabId = result.PlayFabId;
            _sessionTicket = result.SessionTicket;

            //check if we want to get this callback directly or send to event subscribers.
            if (onComplete != null)
            {
                //report login result back to the subscriber
                onComplete.Invoke(result);
            }
        },
        (error) =>
        {
            //report errro back to the subscriber
            if (onFail != null)
            {
                onFail.Invoke(error);
            }
        });
    }

    public void AuthenticateAsGuest(UnityAction<LoginResult> onComplete, UnityAction<PlayFabError> onFail)
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        //Get the device id from native android
        AndroidJavaClass up = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject currentActivity = up.GetStatic<AndroidJavaObject>("currentActivity");
        AndroidJavaObject contentResolver = currentActivity.Call<AndroidJavaObject>("getContentResolver");
        AndroidJavaClass secure = new AndroidJavaClass("android.provider.Settings$Secure");
        string deviceId = secure.CallStatic<string>("getString", contentResolver, "android_id");

        //Login with the android device ID
        PlayFabClientAPI.LoginWithAndroidDeviceID(new LoginWithAndroidDeviceIDRequest() 
        {
            TitleId = PlayFabSettings.TitleId,
            AndroidDevice = SystemInfo.deviceModel,
            OS = SystemInfo.operatingSystem,
            AndroidDeviceId = deviceId,
            CreateAccount = true,
            InfoRequestParameters = InfoRequestParams
        }, 
        
        (result) => 
        {
            //Store Identity and session
            _playFabId = result.PlayFabId;
            _sessionTicket = result.SessionTicket;

            //check if we want to get this callback directly or send to event subscribers.
            if (onComplete != null)
            {
                //report login result back to the caller
                onComplete.Invoke(result);
            }
        }, 
        
        (error) => 
        {
            if(onFail != null)
            {
                onFail.Invoke(error);
                Debug.LogError(error.GenerateErrorReport());
            }
            ////report errro back to the subscriber
            //if(callback == null && OnPlayFabError != null)
            //{
            //    OnPlayFabError.Invoke(error);
            //}
            //else
            //{
            //    //make sure the loop completes, callback with null
            //    callback.Invoke(null);
            //    //Output what went wrong to the console.
            //    Debug.LogError(error.GenerateErrorReport());
            //}
        });

#elif UNITY_IPHONE || UNITY_IOS && !UNITY_EDITOR
        PlayFabClientAPI.LoginWithIOSDeviceID(new LoginWithIOSDeviceIDRequest() {
            TitleId = PlayFabSettings.TitleId,
            DeviceModel = SystemInfo.deviceModel, 
            OS = SystemInfo.operatingSystem,
            DeviceId = SystemInfo.deviceUniqueIdentifier,
            CreateAccount = true,
            InfoRequestParameters = InfoRequestParams
        }, 
        
        (result) => 
        {
            //Store Identity and session
            _playFabId = result.PlayFabId;
            _sessionTicket = result.SessionTicket;

            if (onComplete != null)
            {
                //report login result back to the caller
                onComplete.Invoke(result);
            }
        }, 
        
        (error) => 
        {
            if (onFail != null)
            {
                onFail.Invoke(error);
                Debug.LogError(error.GenerateErrorReport());
            }
        });

#else
        //Debug.LogWarning(SystemInfo.deviceUniqueIdentifier);
        PlayFabClientAPI.LoginWithCustomID(new LoginWithCustomIDRequest()
        {
            TitleId = PlayFabSettings.TitleId,
            CustomId = SystemInfo.deviceUniqueIdentifier,
            CreateAccount = true,
            InfoRequestParameters = InfoRequestParams
        },
        (result) =>
        {
            //Store Identity and session
            _playFabId = result.PlayFabId;
            _sessionTicket = result.SessionTicket;

            if (onComplete != null)
            {
                onComplete.Invoke(result);
            }
        },

        (error) =>
        {
            if (onFail != null)
            {
                onFail.Invoke(error);
                Debug.LogError(error.GenerateErrorReport());
            }
        });
#endif
    }

    private void UnlinkGuestAuth(UnityAction onComplete, UnityAction<PlayFabError> onFail)
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        //Get the device id from native android
        AndroidJavaClass up = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject currentActivity = up.GetStatic<AndroidJavaObject>("currentActivity");
        AndroidJavaObject contentResolver = currentActivity.Call<AndroidJavaObject>("getContentResolver");
        AndroidJavaClass secure = new AndroidJavaClass("android.provider.Settings$Secure");
        string deviceId = secure.CallStatic<string>("getString", contentResolver, "android_id");

        //Fire and forget, unlink this android device.
        PlayFabClientAPI.UnlinkAndroidDeviceID(new UnlinkAndroidDeviceIDRequest() 
        {
            AndroidDeviceId = deviceId
        }
        , delegate(UnlinkAndroidDeviceIDResult result)
        {
            if (onComplete != null)
            {
                onComplete.Invoke();
            }
        }
        , delegate(PlayFabError error) 
        {
            if (onFail != null)
            {
                onFail.Invoke(error);
            }
        });

#elif UNITY_IPHONE || UNITY_IOS && !UNITY_EDITOR
        PlayFabClientAPI.UnlinkIOSDeviceID(new UnlinkIOSDeviceIDRequest()
        {
            DeviceId = SystemInfo.deviceUniqueIdentifier
        }
        , delegate (UnlinkIOSDeviceIDResult result)
        {
            if (onComplete != null)
            {
                onComplete.Invoke();
            }
        }
        , delegate (PlayFabError error)
        {
            if (onFail != null)
            {
                onFail.Invoke(error);
            }
        });

#else
        PlayFabClientAPI.UnlinkCustomID(new UnlinkCustomIDRequest()
        {
            CustomId = SystemInfo.deviceUniqueIdentifier
        }
            , delegate (UnlinkCustomIDResult result)
            {
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            }
            , delegate (PlayFabError error)
            {
                if (onFail != null)
                {
                    onFail.Invoke(error);
                }
            }
        );
#endif   
    }

    public void LinkCustomID(string customID, bool isForce, UnityAction<LinkCustomIDResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabClientAPI.LinkCustomID(new LinkCustomIDRequest()
        {
            CustomId = customID,
            ForceLink = isForce
        },
            (result) =>
            {
                if (onComplete != null)
                {
                    onComplete.Invoke(result);
                }
            },
            (error) =>
            {
                if (onFail != null)
                {
                    onFail.Invoke(error);
                }
            }
        );
    }

    public void UnlinkCustomID(string customID, UnityAction<UnlinkCustomIDResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabClientAPI.UnlinkCustomID(new UnlinkCustomIDRequest()
        {
            CustomId = customID
        },
            (result) =>
            {
                if (onComplete != null)
                {
                    onComplete.Invoke(result);
                }
            },
            (error) =>
            {
                if (onFail != null)
                {
                    onFail.Invoke(error);
                }
            }
        );
    }

    public void AddOrUpdateContactEmail(string email, UnityAction onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabClientAPI.AddOrUpdateContactEmail(new AddOrUpdateContactEmailRequest()
        {
            EmailAddress = email,
        },
            (result) =>
            {
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            }
            ,
            (error) =>
            {
                if (onFail != null)
                {
                    onFail.Invoke(error);
                }
            }
        );
    }

    public void UpdateUserTitleDisplayName(string displayName, UnityAction<UpdateUserTitleDisplayNameResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabClientAPI.UpdateUserTitleDisplayName(new UpdateUserTitleDisplayNameRequest()
        {
            DisplayName = displayName,
        },
            (result) =>
            {
                if (onComplete != null)
                {
                    onComplete.Invoke(result);
                }
            }
            ,
            (error) =>
            {
                if (onFail != null)
                {
                    onFail.Invoke(error);
                }
            }
        );
    }

    public void UpdateUserAvatarID(string avatarID, UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript(
            "UpdatePlayerAvatarID"
            , new
            {
                AvatarId = avatarID
            }
            , onComplete
            , onFail
        );
    }

    public void UpdateUserTitleID(string titleID, UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript(
            "UpdatePlayerTitleID"
            , new
            {
                TitleId = titleID
            }
            , onComplete
            , onFail
        );
    }

    public void LinkWithFacebookAccount(string accessToken, UnityAction<LinkFacebookAccountResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabClientAPI.LinkFacebookAccount(new LinkFacebookAccountRequest()
        {
            AccessToken = accessToken,
            ForceLink = false,
        },
        (result) =>
        {
            if (onComplete != null)
            {
                onComplete.Invoke(result);
            }
        }
        ,
        (error) =>
        {
            if (onFail != null)
            {
                onFail.Invoke(error);
            }
        });
    }

    public void LinkWithGoogleAccount(string authCode, UnityAction<LinkGoogleAccountResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabClientAPI.LinkGoogleAccount(new LinkGoogleAccountRequest()
        {
            ServerAuthCode = authCode,
            ForceLink = false,
        },
        (result) =>
        {
            if (onComplete != null)
            {
                onComplete.Invoke(result);
            }
        }
        ,
        (error) =>
        {
            if (onFail != null)
            {
                onFail.Invoke(error);
            }
        });
    }

    public void SendRecoveryEmail(string email, UnityAction<SendAccountRecoveryEmailResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabClientAPI.SendAccountRecoveryEmail(new SendAccountRecoveryEmailRequest()
        {
            Email = email,
            //EmailTemplateId = "40AADA9783E315C4",
            TitleId = PlayFabSettings.TitleId,
        },
        (result) =>
        {
            Debug.LogWarning(result.ToString());
            if (onComplete != null)
            {
                onComplete.Invoke(result);
            }
        }
        ,
        (error) =>
        {
            if (onFail != null)
            {
                onFail.Invoke(error);
            }
        });
    }

    public void UpdateAndPurchaseRenameFeature(UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript("UpdateAndPurchaseRenameFeature", null, onComplete, onFail);
    }
    #endregion

    #region Asset Bundle
    public void GetAssetBundlePath(List<string> assetKeys, ATNVersion atnVersion, RuntimePlatform platform, string buildDate, UnityAction<ExecuteCloudScriptResult> onComplete)
    {
        var request = new
        {
            AssetKeys = assetKeys
            ,
            ATNVersion = atnVersion.ToString()
            ,
            Platform = platform.ToString()
            ,
            BuildVersion = buildDate
        };

        ExecuteCloudScript("GetAssetBundlePathByKey", request, onComplete, null);
    }

    public void GetStarterAssetBundlePath(ATNVersion atnVersion, RuntimePlatform platform, string buildDate, UnityAction<ExecuteCloudScriptResult> onComplete)
    {
        var request = new
        {
            ATNVersion = atnVersion.ToString()
            ,
            Platform = platform.ToString()
            ,
            BuildVersion = buildDate
        };

        ExecuteCloudScript("GetStarterAssetBundlePath", request, onComplete, null);
    }

    #endregion

    #region Game Data

    public void GetSeasonDB(UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript("GetSeasonDBData", null, onComplete, onFail);
    }

    public void GetTitleData(string key, UnityAction<GetTitleDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabClientAPI.GetTitleData(new GetTitleDataRequest()
        {
            Keys = new List<string>() { key },
        },
        (result) =>
        {
            if (onComplete != null)
            {
                onComplete.Invoke(result);
            }
        },
        (error) =>
        {
            if (onFail != null)
            {
                onFail.Invoke(error);
            }
        });
    }

    public void GetTitleData(List<string> keys, UnityAction<GetTitleDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabClientAPI.GetTitleData(new GetTitleDataRequest()
        {
            Keys = new List<string>(keys),
        },
        (result) =>
        {
            if (onComplete != null)
            {
                onComplete.Invoke(result);
            }
        },
        (error) =>
        {
            if (onFail != null)
            {
                onFail.Invoke(error);
            }
        });
    }

    public void GetTitleData(UnityAction<GetTitleDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabClientAPI.GetTitleData(new GetTitleDataRequest()
        {
        },
        (result) =>
        {
            if (onComplete != null)
            {
                onComplete.Invoke(result);
            }
        },
        (error) =>
        {
            if (onFail != null)
            {
                onFail.Invoke(error);
            }
        });
    }

    public void GetServerTime(UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript("GetServerTime", null, onComplete, onFail);
    }

    public void GetRealServerTime(UnityAction<GetTimeResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        /*
         This query retrieves the current time from one of the servers in PlayFab. 
         Please note that due to clock drift between servers, there is a potential variance of up to 5 seconds.
        */

        PlayFabClientAPI.GetTime(
              new GetTimeRequest()
            , (result) =>
            {
                if (onComplete != null)
                {
                    onComplete.Invoke(result);
                }
            }
            , (error) =>
            {
                if (onFail != null)
                {
                    onFail.Invoke(error);
                }
            }
        );
    }
    #endregion

    #region Player Data
    public void GetPlayerData(UnityAction<GetUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabClientAPI.GetUserData(new GetUserDataRequest()
        {
            PlayFabId = PlayFabId,
        },
        (result) =>
        {
            if (onComplete != null)
            {
                onComplete.Invoke(result);
            }
        },
        (error) =>
        {
            if (onFail != null)
            {
                onFail.Invoke(error);
            }
        });
    }

    public void UpdateNUFStatus(PlayerActivity activity, int version, UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript("UpdateNUFStatus", new { Activity = activity.ToString(), Version = version }, onComplete, (err) => Debug.Log(err.ToString()));
    }

    public void UpdatePlayerActivity(PlayerActivity activity, UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript("UpdatePlayerActivity", new { Activity = activity.ToString() }, onComplete, (err) => Debug.Log(err.ToString()));
    }

    public void GetPlayerData(string key, UnityAction<GetUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabClientAPI.GetUserData(new GetUserDataRequest()
        {
            Keys = new List<string>() { key },
            PlayFabId = PlayFabId,
        },
        (result) =>
        {
            if (onComplete != null)
            {
                onComplete.Invoke(result);
            }
        },
        (error) =>
        {
            if (onFail != null)
            {
                onFail.Invoke(error);
            }
        });
    }

    public void GetPlayerData(List<string> keys, UnityAction<GetUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabClientAPI.GetUserData(new GetUserDataRequest()
        {
            Keys = new List<string>(keys),
            PlayFabId = PlayFabId,
        },
        (result) =>
        {
            if (onComplete != null)
            {
                onComplete.Invoke(result);
            }
        },
        (error) =>
        {
            if (onFail != null)
            {
                onFail.Invoke(error);
            }
        });
    }

    public void GetPlayerReadOnlyData(UnityAction<GetUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabClientAPI.GetUserReadOnlyData(new GetUserDataRequest()
        {
            PlayFabId = PlayFabId,
        },
        (result) =>
        {
            if (onComplete != null)
            {
                onComplete.Invoke(result);
            }
        },
        (error) =>
        {
            if (onFail != null)
            {
                onFail.Invoke(error);
            }
        });
    }

    public void GetPlayerReadOnlyData(string key, UnityAction<GetUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabClientAPI.GetUserReadOnlyData(new GetUserDataRequest()
        {
            Keys = new List<string>() { key },
            PlayFabId = PlayFabId,
        },
        (result) =>
        {
            if (onComplete != null)
            {
                onComplete.Invoke(result);
            }
        },
        (error) =>
        {
            if (onFail != null)
            {
                onFail.Invoke(error);
            }
        });
    }

    public void GetPlayerReadOnlyData(List<string> keys, UnityAction<GetUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabClientAPI.GetUserReadOnlyData(new GetUserDataRequest()
        {
            Keys = new List<string>(keys),
            PlayFabId = PlayFabId,
        },
        (result) =>
        {
            if (onComplete != null)
            {
                onComplete.Invoke(result);
            }
        },
        (error) =>
        {
            if (onFail != null)
            {
                onFail.Invoke(error);
            }
        });
    }

    public void UpdatePlayerData(string key, string value, UnityAction<UpdateUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabClientAPI.UpdateUserData(new UpdateUserDataRequest()
        {
            Data = new Dictionary<string, string>() { { key, value } },
        },
        (result) =>
        {
            if (onComplete != null)
            {
                onComplete.Invoke(result);
            }
        },
        (error) =>
        {
            if (onFail != null)
            {
                onFail.Invoke(error);
            }
        });
    }

    public void UpdatePlayerData(string key, object value, UnityAction<UpdateUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        string valueStr = JsonConvert.SerializeObject(value);
        PlayFabClientAPI.UpdateUserData(new UpdateUserDataRequest()
        {
            Data = new Dictionary<string, string>() { { key, valueStr } },
        },
        (result) =>
        {
            if (onComplete != null)
            {
                onComplete.Invoke(result);
            }
        },
        (error) =>
        {
            if (onFail != null)
            {
                onFail.Invoke(error);
            }
        });
    }

    public void UpdatePlayerData(Dictionary<string, string> dict, UnityAction<UpdateUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabClientAPI.UpdateUserData(
            new UpdateUserDataRequest()
            {
                Data = new Dictionary<string, string>(dict)
            }
            , (result) =>
            {
                if (onComplete != null)
                {
                    onComplete.Invoke(result);
                }
            }
            , (error) =>
            {
                if (onFail != null)
                {
                    onFail.Invoke(error);
                }
            }
        );
    }

    public void UpdatePlayerData(Dictionary<string, string> dict, List<string> keysToRemove, UnityAction<UpdateUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabClientAPI.UpdateUserData(
            new UpdateUserDataRequest()
            {
                Data = new Dictionary<string, string>(dict),
                KeysToRemove = new List<string>(keysToRemove)
            }
            , (result) =>
            {
                if (onComplete != null)
                {
                    onComplete.Invoke(result);
                }
            }
            , (error) =>
            {
                if (onFail != null)
                {
                    onFail.Invoke(error);
                }
            }
        );
    }

    public void RemovePlayerData(string keyToRemove, UnityAction<UpdateUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabClientAPI.UpdateUserData(
            new UpdateUserDataRequest()
            {
                KeysToRemove = new List<string>() { keyToRemove },
            }
            , (result) =>
            {
                if (onComplete != null)
                {
                    onComplete.Invoke(result);
                }
            }
            , (error) =>
            {
                if (onFail != null)
                {
                    onFail.Invoke(error);
                }
            }
        );
    }

    public void RemovePlayerData(List<string> keysToRemove, UnityAction<UpdateUserDataResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabClientAPI.UpdateUserData(
            new UpdateUserDataRequest()
            {
                KeysToRemove = keysToRemove,
            }
            , (result) =>
            {
                if (onComplete != null)
                {
                    onComplete.Invoke(result);
                }
            }
            , (error) =>
            {
                if (onFail != null)
                {
                    onFail.Invoke(error);
                }
            }
        );
    }

    public void GetPlayerProfileData(UnityAction<GetPlayerProfileResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabClientAPI.GetPlayerProfile(new GetPlayerProfileRequest()
        {
            PlayFabId = _playFabId,
            ProfileConstraints = new PlayerProfileViewConstraints()
            {
                ShowDisplayName = true,
            }
        },
        (result) =>
        {
            if (onComplete != null)
            {
                onComplete.Invoke(result);
            }
        },
        (error) =>
        {
            if (onFail != null)
            {
                onFail.Invoke(error);
            }
        });
    }

    public void SubtractUserVirtualCurrency(VirtualCurrency currency, int amount, UnityAction<ModifyUserVirtualCurrencyResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabClientAPI.SubtractUserVirtualCurrency(
        new SubtractUserVirtualCurrencyRequest() { VirtualCurrency = currency.ToString(), Amount = amount },
        (result) =>
        {
            if (onComplete != null)
            {
                onComplete.Invoke(result);
            }
        },
        (error) =>
        {
            if (onFail != null)
            {
                onFail.Invoke(error);
            }
        });
    }

    public void GetPlayerRankData(UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript("GetPlayerRankData", null, onComplete, onFail);
    }

    public void GetPlayerMMRData(UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript("GetPlayerMMRData", null, onComplete, onFail);
    }

    public void SetLastMatchPlayer(string playerID, UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript("SetLastMatchPlayer", new { playerID = playerID }, onComplete, onFail);
    }
    #endregion

    #region Store & Catalog
    public void GetStoreItems(string storeID, UnityAction<GetStoreItemsResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabClientAPI.GetStoreItems(new GetStoreItemsRequest()
        {
            CatalogVersion = PlayFabManager.CatalogMainID,
            StoreId = storeID,
        }
        , (result) =>
        {
            if (onComplete != null)
            {
                onComplete.Invoke(result);
            }
        }
        , (error) =>
        {
            if (onFail != null)
            {
                onFail.Invoke(error);
            }
        });
    }

    public void PurchaseStoreItems(string catalogVer, string storeID, string itemID, string currency, int price, UnityAction<PurchaseItemResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabClientAPI.PurchaseItem(new PurchaseItemRequest()
        {
            CatalogVersion = catalogVer,
            StoreId = storeID,
            ItemId = itemID,
            VirtualCurrency = currency,
            Price = price,
        }
        , (result) =>
        {
            if (onComplete != null)
            {
                onComplete.Invoke(result);
            }
        }
        , (error) =>
        {
            if (onFail != null)
            {
                onFail.Invoke(error);
            }
        });
    }

    public void GetCatalogItems(string catalogVer, UnityAction<GetCatalogItemsResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabClientAPI.GetCatalogItems(new GetCatalogItemsRequest()
        {
            CatalogVersion = catalogVer,
        }
        , (result) =>
        {
            if (onComplete != null)
            {
                onComplete.Invoke(result);
            }
        }
        , (error) =>
        {
            if (onFail != null)
            {
                onFail.Invoke(error);
            }
        });
    }

    public void GetInventoryItems(UnityAction<GetUserInventoryResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabClientAPI.GetUserInventory(new GetUserInventoryRequest()
        {

        }
        , (result) =>
        {
            if (onComplete != null)
            {
                onComplete.Invoke(result);
            }
        }
        , (error) =>
        {
            if (onFail != null)
            {
                onFail.Invoke(error);
            }
        });
    }

    public void PurchaseStarterItem(UnityAction<PurchaseItemResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabClientAPI.PurchaseItem(new PurchaseItemRequest()
        {
            CatalogVersion = CatalogMainID
            ,
            ItemId = StarterItemID
            ,
            VirtualCurrency = VirtualCurrency.CO.ToString()
            ,
            Price = 0
            ,
            StoreId = StoreSystemID
        }
        , (result) =>
        {
            if (onComplete != null)
            {
                onComplete.Invoke(result);
            }
        }
        , (error) =>
        {
            if (onFail != null)
            {
                onFail.Invoke(error);
            }
        });
    }

    public void UnlockContainerInstance(string catalogVer, string instanceID, UnityAction<UnlockContainerItemResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabClientAPI.UnlockContainerInstance(new UnlockContainerInstanceRequest()
        {
            CatalogVersion = catalogVer,
            ContainerItemInstanceId = instanceID
        }
        , (result) =>
        {
            if (onComplete != null)
            {
                onComplete.Invoke(result);
            }
        }
        , (error) =>
        {
            if (onFail != null)
            {
                onFail.Invoke(error);
            }
        });
    }

    public void UnlockContainerItem(string catalogVer, string containerID, UnityAction<UnlockContainerItemResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabClientAPI.UnlockContainerItem(new UnlockContainerItemRequest()
        {
            CatalogVersion = catalogVer,
            ContainerItemId = containerID
        }
        , (result) =>
        {
            if (onComplete != null)
            {
                onComplete.Invoke(result);
            }
        }
        , (error) =>
        {
            if (onFail != null)
            {
                onFail.Invoke(error);
            }
        });
    }

    public void RefreshCardPriceOverrideQuota(UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript("RefreshCardPriceOverrideQuota", null, onComplete, onFail);
    }

    public void RecycleCard(string itemID, UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript(
            "RecycleCard"
            , new { ItemId = itemID }
            , onComplete
            , onFail
        );
    }

    public void CraftCard(string itemID, UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript(
            "CraftCard"
            , new { ItemId = itemID }
            , onComplete
            , onFail
        );
    }

    public void TransformCard(string itemID, UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript(
            "TransformCard"
            , new { ItemId = itemID }
            , onComplete
            , onFail
        );
    }
    #endregion

    #region FriendMode

    public void InviteFriendModeRequest(InviteFriendModeRequest request, UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript("InviteFriendModeRequest", request, onComplete, onFail);
    }

    public void ClearFriendMatchRequest(UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript("ClearFriendMatchRequest", null, onComplete, onFail);
    }

    public void DeclineFriendMatchRequest(string playerID, UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript("DeclineFriendMatchRequest", new { PlayerID = playerID }, onComplete, onFail);
    }

    public void RefreshFriendMatchRequest(UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript("RefreshFriendMatchRequest", null, onComplete, onFail);
    }

    #endregion

    #region Referral
    public void RegisterFriendReferralCode(string referralCode, UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript("RegisterFriendReferralCode", new { ReferralCode = referralCode }, onComplete, onFail);
    }

    public void RefreshReferralList(UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript("GetReferreeStatusList", null, onComplete, onFail);
    }

    public void ClaimReferralLevel7Reward(string playerID, UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript("ClaimReferrerReward", new { PlayerID = playerID }, onComplete, onFail);
    }
    #endregion

    #region Weekly Rank Reward
    public void RefreshWeeklyRankRewardStatus(UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript("RefreshWeeklyRankRewardStatus", null, onComplete, onFail);
    }
    #endregion

    #region Friends/Players
    public void RefreshFriendRequest(UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript("RefreshFriendRequest", null, onComplete, onFail);
    }

    public void GetPlayerInfoList(List<string> playerIDList, UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript("GetPlayerInfoDataList", new { PlayerIDList = playerIDList }, onComplete, onFail);
    }

    public void GetThisPlayerAccountInfo(UnityAction<GetAccountInfoResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabClientAPI.GetAccountInfo(new GetAccountInfoRequest()
        {
            PlayFabId = DataManager.Instance.PlayerInfo.PlayerID,
        }
            , (result) =>
            {
                if (onComplete != null)
                {
                    onComplete.Invoke(result);
                }

            }
            , (error) =>
            {
                if (onFail != null)
                {
                    onFail.Invoke(error);
                }
            }
        );
    }

    public void GetPlayerAccountInfoByDisplayName(string displayName, UnityAction<GetAccountInfoResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabClientAPI.GetAccountInfo(
            new GetAccountInfoRequest()
            {
                TitleDisplayName = displayName,
            }
            , (result) =>
            {
                if (onComplete != null)
                {
                    onComplete.Invoke(result);
                }

            }
            , (error) =>
            {
                if (onFail != null)
                {
                    onFail.Invoke(error);
                }
            }
        );
    }

    public void GetPlayerAccountInfoByPlayerID(string playerID, UnityAction<GetAccountInfoResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabClientAPI.GetAccountInfo(
            new GetAccountInfoRequest()
            {
                PlayFabId = playerID,
            }
            , (result) =>
            {
                if (onComplete != null)
                {
                    onComplete.Invoke(result);
                }

            }
            , (error) =>
            {
                if (onFail != null)
                {
                    onFail.Invoke(error);
                }
            }
        );
    }

    public void AddFriendRequest(string playerID, UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript("AddFriendRequest", new { PlayerID = playerID }, onComplete, onFail);
    }

    public void AcceptFriendRequest(string playerID, UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript("AcceptFriendRequest", new { PlayerID = playerID }, onComplete, onFail);
    }

    public void RemoveFriendRequest(string playerID, UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript("RemoveFriendRequest", new { PlayerID = playerID }, onComplete, onFail);
    }

    public void GetFriendList(UnityAction<GetFriendsListResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        PlayFabClientAPI.GetFriendsList(
            new GetFriendsListRequest()
            {
                IncludeFacebookFriends = false,
                IncludeSteamFriends = false
            }
            , (result) =>
            {
                if (onComplete != null)
                {
                    onComplete.Invoke(result);
                }

            }
            , (error) =>
            {
                if (onFail != null)
                {
                    onFail.Invoke(error);
                }
            }
        );
    }
    #endregion

    #region Notification
    public string ToPushNotificationMessage(string recipient, string subject, string message)
    {
        Dictionary<string, string> data = new Dictionary<string, string>();
        data.Add("recipient", recipient);
        data.Add("subject", subject);
        data.Add("message", message);
        return JsonConvert.SerializeObject(data);
    }
    #endregion

    #region Message Inbox
    public void RefreshMessageInboxPreset(UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript("RefreshMessageInboxPreset", null, onComplete, onFail);
    }

    public void ReadMessageInbox(string messageID, UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript(
            "ReadMessageInbox"
            , new
            {
                MessageInboxId = messageID
            }
            , onComplete
            , onFail
        );
    }

    public void ClaimMessageInboxItem(string messageID, UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript(
            "ClaimMessageInboxItem"
            , new
            {
                MessageInboxId = messageID
            }
            , onComplete
            , onFail
        );
    }

    public void ClaimAllMessageInboxItem(UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript(
            "ClaimAllMessageInboxItem"
            , null
            , onComplete
            , onFail
        );
    }

    public void DeleteMessageInbox(string messageID, UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript(
            "DeleteMessageInbox"
            , new
            {
                MessageInboxId = messageID
            }
            , onComplete
            , onFail
        );
    }

    public void DeleteAllMessageInbox(UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript(
              "DeleteAllMessageInbox"
            , null
            , onComplete
            , onFail
        );
    }
    #endregion

    #region Player Progress & Reward
    public void CountPlayerStat(Dictionary<string, int> playerStatDataDict, UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript(
            "CountPlayerStat"
            , new
            {
                StatKeys = playerStatDataDict
            }
            , onComplete
            , onFail
        );
    }

    public void CountPlayerStatMatch(string matchID, float opponentMMR, Dictionary<string, int> playerStatDataDict, UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript(
            "CountPlayerStatMatch"
            , new
            {
                StatKeys = playerStatDataDict,
                MatchID = matchID,
                OppMMR = opponentMMR
            }
            , onComplete
            , onFail
        );
    }

    public void CountPlayerStatSet(Dictionary<string, int> playerStatDataDict, UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript(
            "CountPlayerStatSet"
            , new
            {
                StatKeys = playerStatDataDict
            }
            , onComplete
            , onFail
        );
    }

    public void CountPlayerStatCardSet(Dictionary<string, int> playerStatDataDict, UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript(
            "CountPlayerStatCardSet"
            , new
            {
                CardSet = playerStatDataDict
            }
            , onComplete
            , onFail
        );
    }

    public void UpdatePlayerMatchTicket(Dictionary<string, int> playerStatDataDict, string matchID, UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript(
            "UpdatePlayerMatchTicket"
            , new
            {
                LoseStatKeys = playerStatDataDict,
                MatchID = matchID
            }
            , onComplete
            , onFail
        );
    }

    public void ClearPlayerMatchTicket(string matchID, UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript(
            "ClearPlayerMatchTicket"
            , new
            {
                MatchID = matchID
            }
            , onComplete
            , onFail
        );
    }

    public void ConvertEventQuest(UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript("ConvertOldEventQuest", null, onComplete, onFail);
    }

    public void RefreshEventQuest(UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript("RefreshEventQuest", null, onComplete, onFail);
    }

    public void RefreshSubEventQuest(UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript("RefreshSubEventQuest", null, onComplete, onFail);
    }

    public void CheckEventQuest(string questID, UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript(
            "CheckEventQuest"
            , new
            {
                QuestID = questID
            }
            , onComplete
            , onFail
        );
    }

    public void CheckSubEventQuest(string questID, string eventKey, UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript(
            "CheckSubEventQuest"
            , new
            {
                QuestID = questID
                , EventKey = eventKey
            }
            , onComplete
            , onFail
        );
    }

    public void CheckAchievement(string achievementId, UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript(
            "CheckAchievement"
            , new
            {
                AchId = achievementId
            }
            , onComplete
            , onFail
        );
    }

    public void CheckAchievementCardSet(string achievementId, UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript(
            "CheckAchievementCardSet"
            , new
            {
                AchId = achievementId
            }
            , onComplete
            , onFail
        );
    }

    public void CheckLevelReward(UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript(
            "CheckLevelReward"
            , new
            {
            }
            , onComplete
            , onFail
        );
    }

    public void CheckLevelRewardByLevel(int level, UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript(
            "CheckLevelRewardByLevel"
            , new
            {
                Level = level
            }
            , onComplete
            , onFail
        );
    }

    public void CheckLevelFactionReward(string factionID, UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript(
            "CheckLevelFactionReward"
            , new
            {
                FactionID = factionID
            }
            , onComplete
            , onFail
        );
    }

    public void CheckLevelFactionRewardByLevel(string factionID, int level, UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript(
            "CheckLevelFactionRewardByLevel"
            , new
            {
                FactionID = factionID,
                Level = level
            }
            , onComplete
            , onFail
        );
    }

    public void OpenPacks(string packID, int amount, UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript(
            "OpenPack"
            , new
            {
                packID = packID,
                amount = amount
            }
            , onComplete
            , onFail
        );
    }

    public void CheckClaimFinishTutorial2Reward(UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript("CheckClaimFinishTutorial2Reward", null, onComplete, onFail);
    }

    public void ClaimFinishTutorial2Reward(UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript("ClaimFinishTutorial2Reward", null, onComplete, onFail);
    }

    public void ClaimTutorialReward(UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript("ClaimTutorialReward", null, onComplete, onFail);
    }

    public void ClaimAdventureReward(AdventureChapterID chapterID, AdventureStageID stageID, UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        object param = new
        {
            AdventureChapterID = chapterID.ToString(),
            AdventureStageID = stageID.ToString()
        };
        ExecuteCloudScript("ClaimAdventureReward", param, onComplete, onFail);
    }
    #endregion

    #region Season Pass
    public void RefreshSeasonPassQuests(UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript(
            "RefreshSeasonPassQuests"
            , null
            , onComplete
            , onFail
        );
    }

    public void CheckSeasonPassQuest(string questID, UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript(
            "CheckSeasonPassQuest"
            , new
            {
                QuestID = questID
            }
            , onComplete
            , onFail
        );
    }

    public void CheckSeasonPassAllQuests(UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript(
            "CheckSeasonPassAllQuests"
            , null
            , onComplete
            , onFail
        );
    }

    public void CheckSeasonPassAllTierReward(UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript(
            "CheckSeasonPassAllTierReward"
            , null
            , onComplete
            , onFail
        );
    }

    public void PurchaseSeasonPassPremium(string seasonPassIndex, UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript(
            "PurchaseSeasonPassPremium"
             , new
             {
                 SeasonPassIndex = seasonPassIndex
             }
            , onComplete
            , onFail
        );
    }

    public void PurchaseSeasonPassTier(string seasonPassIndex, int tierAmount, UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript(
            "PurchaseSeasonPassTier"
             , new
             {
                 SeasonPassIndex = seasonPassIndex,
                 TierAmount = tierAmount
             }
            , onComplete
            , onFail
        );
    }
    #endregion

    #region Feature Unlock
    public void UpdatePlayerFeatureUnlock(Dictionary<string, FeatureUnlockData> data, UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        string json = JsonConvert.SerializeObject(data);

        ExecuteCloudScript(
            "UpdatePlayerFeatureUnlock"
            , new
            {
                DataJson = json
            }
            , onComplete
            , onFail
        );
    }
    #endregion

    #region Mission
    //public void RefreshMissionData(UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    //{
    //    ExcecuteCloudScript(
    //        "RefreshMissionData"
    //        , new
    //        {
    //        }
    //        , onComplete
    //        , onFail
    //    );
    //}

    public void CheckMissionComplete(string missionID, UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript(
            "CheckMissionComplete"
            , new
            {
                MissionID = missionID
            }
            , onComplete
            , onFail
        );
    }
    #endregion

    #region Daily Login Reward
    public void CheckLoginReward(UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript("ClaimAllLoginReward", null, onComplete, onFail);
    }
    #endregion

    #region WildOffer

    public void AddWildOffer(string key, string itemID, UnityAction onComplete, string customData = null)
    {
        object param = new
        {
            Key = key,
            ItemID = itemID,
            CustomData = customData
        };
        ExecuteCloudScript("AddWildOffer", param, (result) => onComplete?.Invoke(), null);
    }

    public void UpdatePurchaseWildOfferStatus(string key)
    {
        ExecuteCloudScript("UpdatePurchaseWildOfferStatus", new { Key = key }, null, null);
    }
    #endregion

    #region Limited Item

    public void PurchaseLimitedItem(string itemID, UnityAction<ExecuteCloudScriptResult> onComplete)
    {
        ExecuteCloudScript("PurchaseLimitedItem", new { ItemID = itemID }, onComplete, null);
    }

    #endregion

    #region Daily Deal & Daily Gift
    public void RefreshDailyDeal(UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript("RefreshDailyDeal", null, onComplete, onFail);
    }

    public void RevokeDailyDeal(UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript("RevokeDailyDeal", null, onComplete, onFail);
    }

    public void RefreshFreeDeal(UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript("RefreshFreeDeal", null, onComplete, onFail);
    }

    public void ClaimFreeDeal(UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript("ClaimFreeDeal", null, onComplete, onFail);
    }

    public void RefreshWeeklyDeal(UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript("RefreshWeeklyDeal", null, onComplete, onFail);
    }

    public void RevokeWeeklyDeal(UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript("RevokeWeeklyDeal", null, onComplete, onFail);
    }
    #endregion

    #region End of Season Reward
    public void CheckSeasonScoreReward(UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript("CheckSeasonScoreReward", null, onComplete, onFail);
    }

    public void CheckRankReward(UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript("CheckRankReward", null, onComplete, onFail);
    }
    #endregion

    #region CloudScript Result Process
    /// <summary>
    /// ExcecuteCloudScript with custom param
    /// </summary>
    /// <param name="functionName"></param>
    /// <param name="param"></param>
    /// <param name="onComplete"></param>
    /// <param name="onFail"> if onFail is null it's will retry to ExcecuteCloudScript automaticaly</param>
    public void ExecuteCloudScript(string functionName, object param, UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScriptRequest request = new ExecuteCloudScriptRequest()
        {
            FunctionName = functionName,
            FunctionParameter = param,
            GeneratePlayStreamEvent = true
        };

        PlayFabClientAPI.ExecuteCloudScript(request, (result) => onComplete?.Invoke(result), (error) =>
        {
            if (onFail != null)
            {
                onFail?.Invoke(error);
            }
            else
            {
                Debug.LogError(error.ToString());
                GameHelper.DelayCallback(1f, () =>
                {
                    ExecuteCloudScript(functionName, param, onComplete, null);
                });
            }
        });
    }
    #endregion

    #region Get Content Download Url
    public void GetContentDownloadUrl(string path, UnityAction<GetContentDownloadUrlResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        bool isThruCDN = false;
        switch (GameHelper.ATNVersion)
        {
            case ATNVersion.Development:
            {
                isThruCDN = false;
            }
            break;

            case ATNVersion.PreProduction:
            case ATNVersion.Production:
            default:
            {
                isThruCDN = true;
            }
            break;
        }

        PlayFabClientAPI.GetContentDownloadUrl(new GetContentDownloadUrlRequest()
        {
              Key = path
            , ThruCDN = isThruCDN
        }
        , (result) =>
        {
            if (onComplete != null)
            {
                onComplete.Invoke(result);
            }
        }
        , (error) =>
        {
            if (onFail != null)
            {
                onFail.Invoke(error);
            }
        });
    }
    #endregion

    #region Game Event

    public void PurchaseShopEventItem(string itemID, UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript("PurchaseShopEventItem", new { ItemID = itemID }, (result) => onComplete?.Invoke(result), (error) => onFail?.Invoke(error));
    }

    public void RefreshGameEventStat(UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript(
            "RefreshGameEventStat"
            , null
            , onComplete
            , onFail
        );
    }

    public void RefreshGameEventPlayerDataValid(UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript(
            "RefreshGameEventPlayerDataValid"
            , null
            , onComplete
            , onFail
        );
    }

    #endregion

    #region Specific Cloud Function
    public void ExecuteSpecificCloudFunction(UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
        ExecuteCloudScript(
            "ExecuteSpecificCloudFunction"
            , null
            , onComplete
            , onFail
        );
    }
    #endregion

    #region DEBUG
    private void DebugLogResult(ExecuteCloudScriptResult result)
    {
        string messageValue = string.Empty;
        CloudScriptResultProcess cloudScriptResult = new CloudScriptResultProcess(result);
        if (cloudScriptResult.IsSuccess)
        {
            messageValue = string.Format("<color=green>{0}</color>", cloudScriptResult.MessageValue);
        }
        else
        {
            messageValue = string.Format("<color=red>{0}</color>", cloudScriptResult.MessageValue);
        }

        Debug.Log(string.Format("{0}:{1}", result.FunctionName, messageValue));
    }

    public void CheckSessionValid(UnityAction<ExecuteCloudScriptResult> onComplete, UnityAction<PlayFabError> onFail)
    {
#if UNITY_ANDROID
        RuntimePlatform platform = RuntimePlatform.Android;
#elif UNITY_IOS
        RuntimePlatform platform = RuntimePlatform.IPhonePlayer;
#else
        {
            onFail?.Invoke("Not support platform. " + Application.platform.ToString());
            return;
        }
#endif

        ExecuteCloudScript(
              "CheckSessionTicketValid"
            , new
            {
                SessionTicket = _sessionTicket
                ,
                ATNVersion = GameHelper.ATNVersion.ToString()
                ,
                Platform = platform.ToString()
                ,
                BuildVersion = GameHelper.GetAssetBundleFolder()
            }
            , (result) =>
            {
                onComplete?.Invoke(result);
            }
            , (error) =>
            {
                onFail?.Invoke(error);
            }
        );
    }
    #endregion

    #endregion
}
