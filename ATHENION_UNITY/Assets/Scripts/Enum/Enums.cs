﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#region DeckEditor

public enum DeckValidStatus
{
    Valid,
    InvalidFaction,
    InvalidCount,
    InvalidDeck,
    InvalidCardCount,
    InvalidCardData
}

#endregion

#region CatalogItem
public enum CatalogItemType
{
    None = 0
    , Card
    , Container
    , Bundle

    // Cosmetic
    , Hero
    , Avatar
    , Title
    , CardBack
    , Dock

    , Subscription
}
#endregion

#region PlayerInfo
public enum PlayerActiveStatus
{
    Offline = 0
    , Online
    , Busy
    , Playing
}

public enum PlayerActivity
{
    None = 0
    
    , In_Option

    , In_MainMenu
    , In_Shop
    , In_Pack
    , In_Deck
    , In_Quest
    , In_StarExchange

    , In_EventBattle
    , In_GameEvent
    , In_Tutorial

    , In_Mode_Select

    , In_Lobby_Tutorial
    , In_Lobby_Practice
    , In_Lobby_Rank
    , In_Lobby_Arena
    , In_Lobby_Casual
    , In_Lobby_Friendly
    , In_Lobby_EventBattle

    , Play_Tutorial
    , Play_Practice
    , Play_Rank
    , Play_Arena
    , Play_Casual
    , Play_Friendly
    , Play_EventBattle

    , Quit

    //obsolete
    , In_CardCollection
    , In_StarChamber
}

public enum FriendStatus
{
    None = 0
    , Request_Sender
    , Request_Receiver
    , Friend
}
#endregion

#region GamePhase
public enum GamePhase
{
    Init = 0
    , SetupData = 1

    , Intro = 2
    , ReHand = 3
    , Begin = 4

    , Spirit = 5
    , Draw = 6
    , PostDraw = 7

    , PrePlay = 8
    , Play = 9
    , PostPlay = 10

    , PreBattle = 11
    , Battle = 12
    , PostBattle = 13

    , End = 14
}
#endregion

#region DisplayGamePhase
public enum DisplayGamePhase
{
    Play
    , Battle
    , Resolve
}
#endregion

#region PlayerIndex
public enum PlayerIndex
{
    One = 0
    , Two = 1
    , Three = 2
    , Four = 3
}
#endregion

#region LocalPlayerIndex
public enum LocalPlayerIndex
{
    Me
    , Enemy
}
#endregion

#region CardDirectionType
public enum CardDirectionType
{
    N = 0
    , NE
    , E
    , SE
    , S
    , SW
    , W
    , NW
}
#endregion

#region GameMode
public enum GameMode
{
    None
    , Tutorial_1    // Tutorial Step 1
    , Tutorial_2    // Tutorial Step 2
    , Tutorial_3    // Tutorial Step 3
    , Tutorial_4    // Tutorial Step 4
    , Casual        // Casual 1vs1
    , Friendly      // Friendly 1vs1
    , Rank
    , Arena
    , Event
    , Practice
    , Adventure

    , BotSimulate
}
#endregion

#region Adventure
public enum AdventureChapterID
{
    None,
    AT
}

public enum AdventureStageID
{
    None
    , AT_001
    , AT_002
    , AT_003
    , AT_004
    , AT_005
    , AT_006
}
#endregion

#region QueuePlayMode
public enum QueuePlayMode
{
    Sequence = 0
    , Parallel
}
#endregion

#region Ability Status
public enum AbilityFeedback
{
    Trigger
    , Awaken
    , Backstab
    , Lastwish
    , Stealth
    , Taunt
    , Link
    , Aura
    , Berserk
    , DarkPower
    , Chain
    , Untargetable
    , Lock
    , Piercing
}

public enum AbilityKeyword
{
    // Ability
    Trigger
    , Awaken
    , Absorb
    , Backstab
    , Lastwish
    , Silence
    , Fear
    , Stealth
    , Freeze
    , Lock
    , Move
    , Draw
    , Taunt
    , Link
    , Aura
    , Berserk
    , DarkPower
    , DarkMatter
    , Reset
    , Invincible
    , Untargetable
    , Chain
    , Piercing
}
#endregion

#region CardRarity
public enum CardRarity
{
    None
    , Common
    , Rare
    , Epic
    , Legend
    , StarChamber
}
#endregion

#region CardType
public enum CardType
{
    Minion
    , Spell
    , Minion_NotInDeck
    , Spell_NotInDeck
    , Hero
}
#endregion

#region CardZone
public enum CardZone
{
    Deck
    , Hand
    , Battlefield
    , Graveyard
    , Remove
    , Hero
}
#endregion

#region Card Stat
public enum CardStat
{
    HP
    , ATK
    , Soul
    , Armor
}
#endregion

#region MinionDeadTypes
public enum MinionDeathTypes
{
    Battle
    , Effect
}
#endregion

#region EventKey
public enum EventKey
{
    None = 0
    , Command
    , OnSlotLock
    , OnFriendSlotLock
    , OnEnemySlotLock
    , OnSlotUnlock
    , OnFriendSlotUnlock
    , OnEnemySlotUnlock

    , OnSlotBeDarkMatter
    , OnFriendSlotBeDarkMatter
    , OnEnemySlotBeDarkMatter
    , OnSlotBeNotDarkMatter
    , OnFriendSlotBeNotDarkMatter
    , OnEnemySlotBeNotDarkMatter

    // Battle Phase
    , BeginPhase
    , PowerPhase
    , DrawPhase
    , PostDrawPhase
    , PrePlayPhase
    //, PlayPhase
    , PostPlayPhase
    , PreBattlePhase
    , BattlePhase
    , PostBattlePhase
    , EndPhase

    , Absorb
    , Awaken
    , Chain_1
    , Chain_2
    , Chain_3
    , Chain_4
    , OnFriendAwaken
    , OnEnemyAwaken
    , OnEnter
    , OnMinionSummoned
    , Rally
    , Sentinel
    , OnPreAttack
    , OnFriendDoBackstab
    , OnPostAttack
    , OnPreDefend
    , OnPostDefend
    , OnMeDamaged
    , OnFriendDamaged
    , OnEnemyDamaged
    , OnMeDamagedByBattle
    , OnFriendDamagedByBattle
    , OnEnemyDamagedByBattle
    , OnMeDamagedByEffect
    , OnFriendDamagedByEffect
    , OnEnemyDamagedByEffect
    , OnMeDamagedInBattle
    , OnFriendDamagedInBattle
    , OnEnemyDamagedInBattle
    , OnMeDamagedOutBattle
    , OnFriendDamagedOutBattle
    , OnEnemyDamagedOutBattle
    , OnMeDestroyed
    , OnFriendDestroyed
    , OnEnemyDestroyed
    , OnMinionDestroyed
    , OnMeSacrificed
    , OnFriendSacrificed
    , OnMinionSacrificed
    , LastWish
    , OnMinionDied
    , OnFriendDied
    , OnEnemyDied
    , OnMeDiedByBattle
    , OnMinionDiedByBattle
    , OnFriendDiedByBattle
    , OnEnemyDiedByBattle
    , OnMeDiedByEffect
    , OnMinionDiedByEffect
    , OnFriendDiedByEffect
    , OnEnemyDiedByEffect
    , OnMeMoved
    , OnFriendMoved
    , OnEnemyMoved
    , OnMeMovingToGraveyard
    , OnFriendMovingToGraveyard
    , OnEnemyMovingToGraveyard
    , OnMeMovedToGraveyard
    , OnFriendMovedToGraveyard
    , OnEnemyMovedToGraveyard
    , OnMeMovedToHandFromBattlefield
    , OnFriendMovedToHandFromBattlefield
    , OnEnemyMovedToHandFromBattlefield
    , OnMeMovedToHandFromDeck
    , OnFriendMovedToHandFromDeck
    , OnEnemyMovedToHandFromDeck
    , OnMeBeBuffed
    , OnFriendBeBuffed
    , OnEnemyBeBuffed

    , OnMeBeFreeze
    , OnFriendBeFreeze
    , OnEnemyBeFreeze
    , OnMeBeRemoveFreeze
    , OnFriendBeRemoveFreeze
    , OnEnemyBeRemoveFreeze

    , OnMeHPIncrease
    , OnFriendHPIncrease
    , OnEnemyHPIncrease
    , OnMeHPRestore
    , OnFriendHPRestore
    , OnEnemyHPRestore
    , OnMeAPIncrease
    , OnFriendAPIncrease
    , OnEnemyAPIncrease
    , OnMeArmorIncrease
    , OnFriendArmorIncrease
    , OnEnemyArmorIncrease
    , OnMeArmorIncreaseByBoost
    , OnFriendArmorIncreaseByBoost
    , OnEnemyArmorIncreaseByBoost
    , OnMeChangeDir
    , OnFriendChangeDir
    , OnEnemyChangeDir
    , OnMeSelected
    , OnFriendSelected
    , OnEnemySelected
    , OnMeAddedToAttackerList
    , OnFriendAddedToAttackerList
    , OnEnemyAddedToAttackerList
    , OnMeRemovedFromAttackerList
    , OnFriendRemovedFromAttackerList
    , OnEnemyRemovedFromAttackerList

    , OnMeExitBattlefield
    , OnFriendExitBattlefield
    , OnEnemyExitBattlefield
    //, OnMeBeLinked
    //, OnFriendBeLinked
    //, OnEnemyBeLinked
    //, OnMeBeNotLinked
    //, OnFriendBeNotLinked
    //, OnEnemyBeNotLinked

    // Repeat
    , OnFriendGotFearRepeat
    , OnEnemyGotFearRepeat
    , OnFriendGotFreezeRepeat
    , OnEnemyGotFreezeRepeat
    , OnFriendGotSilenceRepeat
    , OnEnemyGotSilenceRepeat

    , OnMeDidRepeatFear
    , OnFriendDidRepeatFear
    , OnEnemyDidRepeatFear
    , OnMeDidRepeatFreeze
    , OnFriendDidRepeatFreeze
    , OnEnemyDidRepeatFreeze
    , OnMeDidRepeatSilence
    , OnFriendDidRepeatSilence
    , OnEnemyDidRepeatSilence

    , OnMyAbilityCompleted
}
#endregion

#region PerformType
public enum PerformTypes
{
    Global
    , Group
    , Single
    , Area
    , Multi
    , DirAuto
    , DirCustom
    , DirCross
    , DirPlus
}

public enum PerformSteps
{
    Start = 0
    , Moving
    , Arrived
}
#endregion

#region Buff
public enum BuffCounterType
{
    None,
    EndOfTurn,
    EndOfMyTurn,
    BeginOfMyTurn,
    Critical,
    PostAttack,
    PostDefend,
    PostBattle
}

public enum BuffParamIndex
{
    Key,
    CounterType,
    CounterAmount
}
#endregion

#region Language
public enum Language
{
    English
    , Thai
    , Japanese
}
#endregion

#region ScreenFitMode
public enum ScreenFitMode
{
    Width
    , Height
}
#endregion

#region BattleType
public enum BattleCalType
{
    None
    , Battle
    , Attack
    , BeAttack
}
#endregion

#region PlayerStatKey
public enum StatKeys
{
    // main action
    P              //PLAY
    , W              //WIN
    , L              //LOSE
    , TURN           //TURN
    , C_DRAW         //CARD_DRAW
    , C_USE          //CARD_USE
    , C_SUMM         //CARD_SUMMON
    , C_DES          //CARD_DESTROY
    , C_REMOVE       //CARD_REMOVE
    , C_ACT          //CARD_ACT
    , C_AL           //CARD_ALIVE
    , H_DAM          //HERO_DAMAGE
    , LOSER_1_STACK  //LOSER_1_STACK
    , LOSER_2_STACK  //LOSER_2_STACK
    , C_SET          //CARD_SET
    , NEWBIE_CAS     //NEWBIE_CASUAL

    , RM_PAY         //REAL_MONEY_PAY
    , PC             //PURCHASE
    , C              //CARD
    , SL             //SLOT
    , REVIEW_SHOW    //REVIEW_SHOW
    , Q              //QUEST
    , EMO_MATCH      //EMOTE_MATCH
    , LOGIN          //LOGIN
    , CP             //CARD_PACK
    , MISSION        //MISSION
    , DECK           //DECK
    , GY           //GRAVEYARD
    , LV             //LEVEL
    , OPN            //OPEN
    , ACH            //ACHIEVMENT
    , AVATAR         //AVATAR
    , TITLE          //TITLE
    , SS             //SEASON
    , FACEBOOK_SEEN  //Seen facebook at level 4
    , FL_SEEN       //Seen Faction Level Tip

    // category
    , ANY            //ANY_MODE
    , STL            //SAFE_TO_LEAVE
    , TUT            //TUTORIAL
    , PVP            //PVP
    , PVE            //PVE
    , CAS            //CASUAL
    , RANK           //RANK
    , ARN            //ARENA
    , PRA            //PRACTICE
    , TB             //THEME_BATTLE(EVENT_BATTLE)
    , FM             //FRIENDLY
    , ADV            //ADVENTURE
    , DL_DEAL        //DAILY_DEAL
    , DL_FREE        //DAILY_GIFT/FREE
    , CRAFT          //CRAFT
    , RECYC          //RECYCLE
    , TRANS          //TRANSFORM
    , DL             //DAILY
    , WL             //WEEKLY
    , WEEK           //WEEK
    , EVENT          //EVENT
    , CHA            //CHALLENGE
    , OPEN           //OPEN
    , G_COMM         //GET_COMMON
    , SEEN           //SEEN
    , CREATE         //CREATE
    , SSP            //SEASON PASS
    , PG             //PAGE
    , CSET           //CARD_SET
    , CHANGE         //CHANGE

    // sub category
    , H              //HERO
    , F              //FACTION
    , T              //TITLE
    , ER             //END_RESULT
    , MIN            //MINIMUM_REQUIREMENT (SUBTURN >= 7) for end game only!!!
    , HP             //HP
    , S              //SOUL
    , R              //RARITY
    , A              //ABILITY
    , ID             //CARD_ID
    , E              //ENEMY
    , M              //MY
    , COMP           //COMPLETE

    // specific key   
}
#endregion

#region PlayerTierType
public enum PlayerTierType
{
    Newbie1 = 0
    , Newbie2
    , Newbie3
    , Newbie4
    , Newbie5
    , Newbie6
    , Player
    , Loser1
    , Loser2
}
#endregion

#region RewardType
public enum RewardType
{
    CardPack
}
#endregion

#region HeroVoice
public enum HeroVoice
{
    Begin
    , Lose
    , Greeting
    , Threaten
    , GoodMove
    , Mistake
    , Thank
    , Sorry
    , None
}
#endregion

#region Bot Type
public enum BotType
{
    None
    , TutorialBot
    , SimpleBot
    , AIBot
    , AdventureBot
}

public enum BotLevel
{
    None = 0
    , Easy = 1
    , Medium = 2
    , Hard = 3
}
#endregion

#region ScreenQuality Setting
public enum ScreenQuality : int
{
    Low = 0
    , Med = 1
    , High = 2
}
#endregion

#region Athenion Version
public enum ATNVersion
{
      Development
    , PreProduction
    , Production
}
#endregion

#region Photon Region
public enum PhotonRegion
{
    None
    , Singapore
    , Melbourne
    , Montreal
    , Amsterdam
    , Chennai
    , Tokyo
    , Moscow
    , Khabarovsk
    , SaoPaulo
    , Seoul
    , Washington
    , SanJose
}

public enum PhotonChatRegion
{
    None
    , Singapore
    , Amsterdam
    , Washington
}
#endregion

#region Client Status
public enum ClientStatus
{
    Open
    , Update
    , Close
}
#endregion

#region VirtualCurrency
public enum VirtualCurrency
{
    CO // Coin
    , FR // Fragment
    , RF // Rainbow Fragment
    , DI // Diamond

    // Normal Sign
    , SC // Circle Sign
    , ST // Triangle Sign
    , SS // Square Sign

    // Super Sign
    , S1 // Nova Sign
    , S2 // Umbra Sign
    , S3 // Quasar Sign
    , S4 // Solar Sign
    , S5 // Lunar Sign

    , RM // Real Money (US Cent)
}
#endregion

#region Name
public enum NameValidCase
{
    Valid,
    SameCurrent,
    Empty,
    ProfanityWords,
    SpecialAlphabet,
    NotAlphanumeric,
    SpecialName
}
#endregion

#region Authentication Types
public enum AuthTypes
{
    None,
    Guest,
    EmailAndPassword,
    Facebook,
    GooglePlayGames
}
#endregion

#region Shop

#region ShopTypes
public enum ShopTypes
{
    None = -1
    , Promotion
    , BoosterPack
    , Diamond
    , Cosmetic
    , Miscellaneous
    , DailyDeal
    , WeeklyDeal
    , Subscription
    , WildOffer
}
#endregion

#region ShopItemType

public enum ShopItemType { ETC, COSMETIC }

#endregion

#region CosmeticType

public enum CosmeticType { SKIN, CARDBACK, AVATAR, TITLE, CONSOLE }

#endregion

#endregion
#region Client Region
public enum ClientRegions
{
    Global
    , Asia
    , SEA
    , NA
}
#endregion

#region UI Reward Icon
public enum SpriteIconType
{
    VC_COIN
    , VC_DIAMOND
    , VC_FRAGMENT
    , VC_RAINBOW_FRAGMENT
    , PACK_CARD_WOE
}
#endregion

#region Feature Unlock
public enum FeatureUnlock
{
    CasualMode
    , RankMode
    , FriendMode
    , PracticeMode
    , EventBattleMode

    , Shop
    , Deck
    , Guild
    , StarMarket

    , CardCrafting
    , Community // KTPlay
    , SeasonPass
    , Event
    , FactionLevel
    , Accomplishment
    , Achievement
    , CardSet
    , EditAvatar
    , EditTitle
}
#endregion

#region UI Anchor
public enum ScreenAnchor
{
    TopLeft
    , TopCenter
    , TopRight
    , MidLeft
    , MidCenter
    , MidRight
    , BotLeft
    , BotCenter
    , BotRight
}
#endregion