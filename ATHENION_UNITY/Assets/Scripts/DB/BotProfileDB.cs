﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Karamucho;

public class BotProfileDBData : DBData
{
    #region Public Properties
    public string BotProfileID { get; private set; }
    public string HeroID { get; private set; }
    public string Difficulty { get; private set; }
    public string DeckID { get; private set; }
    #endregion

    #region Constructors
    public BotProfileDBData()
    {
        BotProfileID = "";
        HeroID = "";
        Difficulty = "Easy";
        DeckID = "";
    }

    public BotProfileDBData(string botProfileID, string heroID, string difficulty, string deckID)
    {
        BotProfileID = botProfileID;
        HeroID = heroID;
        Difficulty = difficulty;
        DeckID = deckID;
    }

    public BotProfileDBData(BotProfileDBData data)
    {
        BotProfileID = data.BotProfileID;
        HeroID = data.HeroID;
        Difficulty = data.Difficulty;
        DeckID = data.DeckID;
    }
    #endregion

    #region Methods
    public override string GetDebugText()
    {
        int index = 0;
        string result = string.Format(
                "BotProfileID: {" + index++ + "}"
              + "\nHeroID: {" + index++ + "}"
              + "\nDifficulty: {" + index++ + "}"
              + "\nDeckID: {" + index++ + "}"
            , BotProfileID
            , HeroID
            , Difficulty
            , DeckID
        );

        return result;
    }
    #endregion
}

public class BotProfileDB : DictDB<BotProfileDB, BotProfileDBData>
{
    #region Enums
    public enum DeckDBIndex
    {
          BotProfileID = 0
        , HeroID
        , Difficulty
        , DeckID
    }
    #endregion

    #region Protected Properties
    protected string _fileName = "Databases/KaramuchoDB - BotProfileDB";
    #endregion

    #region Private Properties
    #endregion

    #region Constructors
    #endregion

    #region Methods
    public override void LoadDB()
    {
        _dict = new Dictionary<string, BotProfileDBData>();

        List<List<string>> rawDataList;
        TextAsset csvText = ResourceManager.Load<TextAsset>(_fileName);

        List<string> conditionList = new List<string>();
        List<string> effectList = new List<string>();

        bool isSuccess = CSVReader.ReadCSV(csvText, out rawDataList);
        if (isSuccess)
        {
            foreach (List<string> row in rawDataList)
            {
                BotProfileDBData data = new BotProfileDBData(
                      row[(int)DeckDBIndex.BotProfileID]
                    , row[(int)DeckDBIndex.HeroID]
                    , row[(int)DeckDBIndex.Difficulty]
                    , row[(int)DeckDBIndex.DeckID]
                );
                _dict.Add(data.BotProfileID, data);
            }

            //Debug.Log("DeckDB: Load Succeed.");
            _isLoad = true;
            return;
        }

        Debug.LogWarning("BotDB: Load Failed.");
        _isLoad = false;
    }

    public bool GetBotProfile(BotLevel difficulty, out List<BotProfileDBData> botProfileList)
    {
        if (!IsLoad)
        {
            LoadDB();
        }

        botProfileList = new List<BotProfileDBData>();

        foreach (KeyValuePair<string, BotProfileDBData> data in _dict)
        {
            if (data.Value.Difficulty.ToUpper() == difficulty.ToString().ToUpper())
            {
                botProfileList.Add(data.Value);
            }
        }

        return (botProfileList.Count > 0);
    }

    public bool GetBotProfile(string heroID, BotLevel difficulty, out List<BotProfileDBData> botProfileList)
    {
        if (!IsLoad)
        {
            LoadDB();
        }

        botProfileList = new List<BotProfileDBData>();

        foreach (KeyValuePair<string, BotProfileDBData> data in _dict)
        {
            if (data.Value.HeroID == heroID && data.Value.Difficulty.ToUpper() == difficulty.ToString().ToUpper())
            {
                botProfileList.Add(data.Value);
            }
        }

        return (botProfileList.Count > 0);
    }
    #endregion
}
