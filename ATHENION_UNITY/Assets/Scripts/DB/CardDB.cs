﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Karamucho;

public class CardDBData : DBData
{
    #region Public Properties
    public string ID { get; private set; }
    public string Name { get; set; }
    public string FlavorText { get; private set; }
    public string ImageKey { get; private set; }
    public List<string> ElementList { get; private set; }
    public string SeriesKey { get; private set; }
    public List<string> PassiveList { get; private set; }
    public List<string> FeedbackList { get; private set; }
    public string Rarity { get; private set; }
    public string Type { get; private set; }
    public int HP { get; private set; }
    public int ATK { get; private set; }
    public int Spirit { get; private set; }
    public int Armor { get; private set; }
    public List<string> CardDir { get; private set; }
    public List<string> AbilityIDList { get; private set; }

    public string SpawnEffectKey { get; private set; }
    public string AttackEffectKey { get; private set; }
    public string DeathEffectKey { get; private set; }

    public string InfoScriptKey { get; private set; }
    #endregion

    #region Constructors
    public CardDBData()
    {
        ID = "";
        Name = "";
        FlavorText = "";
        ImageKey = "";
        ElementList = new List<string>();
        SeriesKey = "";
        PassiveList = new List<string>();
        FeedbackList = new List<string>();
        Rarity = "";
        Type = "";
        HP = 0;
        ATK = 0;
        Spirit = 0;
        Armor = 0;
        CardDir = new List<string>();
        AbilityIDList = new List<string>();

        SpawnEffectKey = "";
        AttackEffectKey = "";
        DeathEffectKey = "";

        InfoScriptKey = "";
    }

    public CardDBData(string id, string name, string flavorText, string imageKey
        , List<string> elementList, string seriesKey, List<string> passiveList, List<string> feedbackList, string rarity, string type
        , int hp, int atk, int spirit, int armor
        , List<string> cardDir, List<string> abilityIDList
        , string spawnEffectKey, string attackEffectKey, string deathEffectKey
        , string infoScriptKey
    )
    {
        ID = id;
        Name = name;
        FlavorText = flavorText;
        ImageKey = imageKey;
        ElementList = new List<string>(elementList);
        SeriesKey = seriesKey;
        PassiveList = new List<string>(passiveList);
        FeedbackList = new List<string>(feedbackList);
        Rarity = rarity;
        Type = type;
        HP = hp;
        ATK = atk;
        Spirit = spirit;
        Armor = armor;
        CardDir = new List<string>(cardDir);
        AbilityIDList = new List<string>(abilityIDList);

        SpawnEffectKey = spawnEffectKey;
        AttackEffectKey = attackEffectKey;
        DeathEffectKey = deathEffectKey;

        InfoScriptKey = infoScriptKey;
    }

    public CardDBData(CardDBData data)
    {
        ID = data.ID;
        Name = data.Name;
        FlavorText = data.FlavorText;
        ImageKey = data.ImageKey;
        ElementList = new List<string>(data.ElementList);
        SeriesKey = data.SeriesKey;
        PassiveList = new List<string>(data.PassiveList);
        FeedbackList = new List<string>(data.FeedbackList);
        Rarity = data.Rarity;
        Type = data.Type;
        HP = data.HP;
        ATK = data.ATK;
        Spirit = data.Spirit;
        Armor = data.Armor;
        CardDir = new List<string>(data.CardDir);
        AbilityIDList = new List<string>(data.AbilityIDList);

        SpawnEffectKey = data.SpawnEffectKey;
        AttackEffectKey = data.AttackEffectKey;
        DeathEffectKey = data.DeathEffectKey;

        InfoScriptKey = data.InfoScriptKey;
    }
    #endregion

    #region Methods
    public override string GetDebugText()
    {
        string dir = "";
        foreach (string cardDir in CardDir)
        {
            if (dir != string.Empty)
            {
                dir += ", ";
            }
            dir += cardDir;
        }

        string elements = "";
        foreach (string element in ElementList)
        {
            if (elements != string.Empty)
            {
                elements += ", ";
            }
            elements += element;
        }

        string passives = "";
        foreach (string passive in PassiveList)
        {
            if (passives != string.Empty)
            {
                passives += ", ";
            }
            passives += passive;
        }

        string feedbacks = "";
        foreach (string feedback in FeedbackList)
        {
            if (feedbacks != string.Empty)
            {
                feedbacks += ", ";
            }
            feedbacks += feedback;
        }

        string abilities = "";
        foreach (string cardAbility in AbilityIDList)
        {
            if (abilities != string.Empty)
            {
                abilities += ", ";
            }
            abilities += cardAbility;
        }

        int index = 0;
        string result = string.Format(
                "ID: {" + index++ + "}"
              + "\nName: {" + index++ + "}"
              + "\nFlavorText: {" + index++ + "}"
              + "\nImage Key: {" + index++ + "}"
              + "\nElement: {" + index++ + "}"
              + "\nSeries: {" + index++ + "}"
              + "\nPassive: {" + index++ + "}"
              + "\nFeedback: {" + index++ + "}"
              + "\nRarity: {" + index++ + "}"
              + "\nType: {" + index++ + "}"
              + "\nHP: {" + index++ + "}"
              + "\nATK: {" + index++ + "}"
              + "\nSPIRIT: {" + index++ + "}"
              + "\nArmor: {" + index++ + "}"
              + "\nDir: {" + index++ + "}"
              + "\nAbilities: {" + index++ + "}"
              + "\nSpawn Effect ID: {" + index++ + "}"
              + "\nAttack Effect ID: {" + index++ + "}"
              + "\nDeath Effect ID: {" + index++ + "}"
              + "\nInfo Script Key: {" + index++ + "}"
            , ID
            , Name
            , FlavorText
            , ImageKey
            , elements
            , SeriesKey
            , passives
            , feedbacks
            , Rarity
            , Type
            , HP
            , ATK
            , Spirit
            , Armor
            , dir
            , abilities
            , SpawnEffectKey
            , AttackEffectKey
            , DeathEffectKey
            , InfoScriptKey
        );

        return result;
    }
    #endregion
}

public class CardDB : DictDB<CardDB, CardDBData>
{
    #region Enums
    public enum CardDBIndex
    {
        CardID = 0
        , Name
        , FlavorText
        , ImageKey
        , ElementList
        , SeriesKey
        , PassiveList
        , FeedbackList
        , Rarity
        , Type
        , Spirit
        , ATK
        , HP
        , Armor
        , CardDir
        , AbilityIDList //Load from AbilityDB
        , SpawnEffectKey
        , AttackEffectKey
        , DeathEffectKey
        , InfoScriptKey
    }
    #endregion

    #region Protected Properties
    private static string _rootPath = "Databases/";
    private static string[] _fileNames = {
          "KaramuchoDB - CardDB"
        , "KaramuchoDB - CardDB_Old"
    };
    #endregion

    #region Private Properties
    #endregion

    #region Constructors
    #endregion

    #region Methods
    public void LoadOverrideDB(string csvText)
    {
        List<List<string>> rawDataList;
        csvText = csvText.Replace("\\r", "");
        csvText = csvText.Replace("\\n", "\n");
        bool isSuccess = CSVReader.ReadCSV(csvText, out rawDataList);
        if (isSuccess)
        {
            foreach (List<string> row in rawDataList)
            {
                try
                {
                    CardDBData data = CreateCardDBDataByCSV(row);
                    if (_dict.ContainsKey(data.ID))
                    {
                        _dict[data.ID] = data;
                    }
                    else
                    {
                        _dict.Add(data.ID, data);
                    }
                }
                catch (System.Exception e)
                {
                    Debug.LogWarning(string.Format(
                        "CardDB/LoadDB: Failed to load data. {0} Skip! ERROR: {1}"
                        , row[(int)CardDBIndex.CardID]
                        , e.ToString()
                    ));
                    continue;
                }
            }
        }
    }

    public override void LoadDB()
    {
        _dict = new Dictionary<string, CardDBData>();

        List<List<string>> rawDataList;
        string filePath = "";
        foreach (string fileName in _fileNames)
        {
            filePath = _rootPath + fileName;
            TextAsset csvText = ResourceManager.Load<TextAsset>(filePath);

            bool isSuccess = CSVReader.ReadCSV(csvText, out rawDataList);
            if (isSuccess)
            {
                foreach (List<string> row in rawDataList)
                {
                    try
                    {
                        CardDBData data = CreateCardDBDataByCSV(row);
                        _dict.Add(data.ID, data);
                    }
                    catch (System.Exception e)
                    {
                        Debug.LogWarning(string.Format(
                            "CardDB/LoadDB: Failed to load data. {0} Skip! ERROR: {1}"
                            , row[(int)CardDBIndex.CardID]
                            , e.ToString()
                        ));
                        continue;
                    }
                }

                //Debug.Log("CardDB: Load Succeed.");
                _isLoad = true;
            }
        }
        if (_isLoad) return;

        Debug.LogWarning("CardDB: Load Failed.");
        _isLoad = false;
    }

    private CardDBData CreateCardDBDataByCSV(List<string> row)
    {
        int _hp = int.Parse(row[(int)CardDBIndex.HP]);
        int _atk = int.Parse(row[(int)CardDBIndex.ATK]);
        int _spirit = int.Parse(row[(int)CardDBIndex.Spirit]);
        int _armor = int.Parse(row[(int)CardDBIndex.Armor]);


        List<string> elementList = new List<string>(row[(int)CardDBIndex.ElementList].Split(','));
        List<string> passiveList = new List<string>(row[(int)CardDBIndex.PassiveList].Split(','));
        List<string> feedbackList = new List<string>(row[(int)CardDBIndex.FeedbackList].Split(','));
        List<string> cardDir = new List<string>(row[(int)CardDBIndex.CardDir].Split(','));
        List<string> abilityIDList = new List<string>(row[(int)CardDBIndex.AbilityIDList].Split(','));


        CardDBData data = new CardDBData(
              row[(int)CardDBIndex.CardID]
            , row[(int)CardDBIndex.Name]
            , row[(int)CardDBIndex.FlavorText]
            , row[(int)CardDBIndex.ImageKey]
            , elementList
            , row[(int)CardDBIndex.SeriesKey]
            , passiveList
            , feedbackList
            , row[(int)CardDBIndex.Rarity]
            , row[(int)CardDBIndex.Type]
            , _hp
            , _atk
            , _spirit
            , _armor
            , cardDir
            , abilityIDList
            , row[(int)CardDBIndex.SpawnEffectKey]
            , row[(int)CardDBIndex.AttackEffectKey]
            , row[(int)CardDBIndex.DeathEffectKey]
            , row[(int)CardDBIndex.InfoScriptKey]
        );
        return data;
    }

    public CardRarity ReadCardRarity(string cardID)
    {
        CardRarity result = CardRarity.None;
        CardDBData cardData;
        if (CardDB.Instance.GetData(cardID, out cardData))
        {
            GameHelper.StrToEnum(cardData.Rarity, out result);
        }
        return result;
    }

    #endregion
}