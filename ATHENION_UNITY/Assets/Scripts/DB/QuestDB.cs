﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using System;

public class QuestDBData : DBData
{
    #region Public Properties
    public string ID { get; private set; }
    public List<PlayerStatDetailData> DetailDataList { get { return _detailDataList; } }
    public List<ItemData> ItemDataList { get { return _itemDataList; } }
    public int MaxProgress { get { return GetMaxProgress(); } }
    public bool IsPremium { get { return _isPremium; } }
    public string FunctionName { get { return _functionName; } }
    public string FunctionParam { get { return _functionParam; } }
    #endregion

    #region Private Properties
    private const string _detailKey = "detail";
    private const string _rewardKey = "reward";
    private const string _isPremiumKey = "is_premium";

    private const string _functionKey = "go_func";
    private const string _function_nameKey = "name";
    private const string _function_paramKey = "param";

    private List<PlayerStatDetailData> _detailDataList = new List<PlayerStatDetailData>();
    private List<ItemData> _itemDataList = new List<ItemData>();
    private bool _isPremium = false;
    private string _functionName = string.Empty;
    private string _functionParam = string.Empty;
    #endregion

    #region Contructors
    public QuestDBData()
    {
    }

    public QuestDBData(string id, string jsonStrData)
    {
        ID = id;
        Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonStrData);

        // set detail
        if(rawData.ContainsKey(_detailKey))
        {
            SetupDetailData(JsonConvert.DeserializeObject<Dictionary<string, int>>(rawData[_detailKey].ToString()));
        }

        // set reward
        if (rawData.ContainsKey(_rewardKey))
        {
            SetupRewardData(JsonConvert.DeserializeObject<Dictionary<string, int>>(rawData[_rewardKey].ToString()));
        }

        // set premium status
        if(rawData.ContainsKey(_isPremiumKey))
        {
            SetupIsPremium(bool.Parse(rawData[_isPremiumKey].ToString()));
        }

        // set function
        if (rawData.ContainsKey(_functionKey))
        {
            Dictionary<string, string> functionData = JsonConvert.DeserializeObject<Dictionary<string, string>>(rawData[_functionKey].ToString());
            if (functionData.ContainsKey(_function_nameKey))
            {
                _functionName = functionData[_function_nameKey];
            }
            if (functionData.ContainsKey(_function_paramKey))
            {
                _functionParam = functionData[_function_paramKey];
            }
        }
    }

    public QuestDBData(QuestDBData data)
    {
        ID = data.ID;
        _detailDataList = new List<PlayerStatDetailData>(data._detailDataList);
        _itemDataList = new List<ItemData>(data._itemDataList);
        _functionName = data._functionName;
        _functionParam = data._functionParam;
    }
    #endregion

    #region Methods
    private void SetupDetailData(Dictionary<string, int> dictData)
    {
        foreach(KeyValuePair<string, int> detail in dictData)
        {
            _detailDataList.Add(new PlayerStatDetailData(detail));
        }
    }

    private void SetupRewardData(Dictionary<string, int> dictData)
    {
        foreach (KeyValuePair<string, int> item in dictData)
        {
            _itemDataList.Add(new ItemData(item));
        }
    }

    private void SetupIsPremium(bool isPremium)
    {
        _isPremium = isPremium;
    }

    private int GetMaxProgress()
    {
        int reqCount = 0;
        foreach (PlayerStatDetailData data in _detailDataList)
        {
            reqCount += data.Amount;
        }

        return reqCount;
    }
    #endregion
}

public class QuestDB : DictDB<QuestDB, QuestDBData>
{
    #region Methods
    public override void LoadFromJSON(string jsonStrData)
    {
        _dict = new Dictionary<string, QuestDBData>();
        Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonStrData);
        foreach(KeyValuePair<string, object> data in rawData)
        {
            QuestDBData dbData = new QuestDBData(data.Key, data.Value.ToString());
            _dict.Add(data.Key, dbData);
        }

        _isLoad = true;
    }
    #endregion
}