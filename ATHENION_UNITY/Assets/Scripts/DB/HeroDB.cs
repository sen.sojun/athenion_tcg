﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Karamucho;

public class HeroDBData : DBData
{
    #region Public Properties
    public string ID { get; private set; }
    public string Name { get; private set; }
    public string ImageKey { get; private set; }
    public string ElementID { get; private set; }
    public bool IsListing { get; private set; }
    public string ActivePowerEffectKey { get; private set; }
    public string SkillCard { get; private set; }
    public int Quantity { get; private set; }
    #endregion

    #region Constructors
    public HeroDBData()
    {
        ID = "";
        Name = "";
        ImageKey = "";
        ElementID = "";
        IsListing = false;
        ActivePowerEffectKey = "";
        SkillCard = "";
        Quantity = 0;
    }

    public HeroDBData(string id, string name, string imageKey, string elementID, bool isListing, string activePowerEffectKey, string skillCard, int quantity)
    {
        ID = id;
        Name = name;
        ImageKey = imageKey;
        ElementID = elementID;
        IsListing = isListing;
        ActivePowerEffectKey = activePowerEffectKey;
        SkillCard = skillCard;
        Quantity = quantity;
    }

    public HeroDBData(HeroDBData data)
    {
        ID = data.ID;
        Name = data.Name;
        ImageKey = data.ImageKey;
        ElementID = data.ElementID;
        IsListing = data.IsListing;
        ActivePowerEffectKey = data.ActivePowerEffectKey;
        SkillCard = data.SkillCard;
        Quantity = data.Quantity;
    }
    #endregion

    #region Methods
    public override string GetDebugText()
    {
        int index = 0;
        string result = string.Format(
                "ID: {" + index++ + "}"
              + "\nName: {" + index++ + "}"
              + "\nImageKey: {" + index++ + "}"
              + "\nElementID: {" + index++ + "}"
              + "\nIsListing: {" + index++ + "}"
              + "\nActivePowerEffectKey: {" + index++ + "}"
              + "\nSkillCard: {" + index++ + "}"
              + "\nQuantity: {" + index++ + "}"
            , ID
            , Name
            , ImageKey
            , ElementID
            , IsListing
            , ActivePowerEffectKey
            , SkillCard
            , Quantity
        );

        return result;
    }
    #endregion
}

public class HeroDB : DictDB<HeroDB, HeroDBData>
{
    #region Enums
    public enum HeroDBIndex
    {
          HeroID = 0
        , Name
        , ImageKey
        , ElementID
        , IsListing
        , ActivePowerEffectKey
        , SkillCard
        , Quantity
    }
    #endregion

    #region Protected Properties
    protected string _fileName = "Databases/KaramuchoDB - HeroDB";
    #endregion

    #region Private Properties
    #endregion

    #region Constructors
    #endregion

    #region Methods
    public override void LoadDB()
    {
        _dict = new Dictionary<string, HeroDBData>();

        List<List<string>> rawDataList;
        TextAsset csvText = ResourceManager.Load<TextAsset>(_fileName);

        bool isSuccess = CSVReader.ReadCSV(csvText, out rawDataList);
        if (isSuccess)
        {
            bool isListing = false;

            foreach (List<string> row in rawDataList)
            {
                isListing = bool.Parse(row[(int)HeroDBIndex.IsListing]);

                HeroDBData data = new HeroDBData(
                          row[(int)HeroDBIndex.HeroID]
                        , row[(int)HeroDBIndex.Name]
                        , row[(int)HeroDBIndex.ImageKey]
                        , row[(int)HeroDBIndex.ElementID]
                        , isListing
                        , row[(int)HeroDBIndex.ActivePowerEffectKey]
                        , row[(int)HeroDBIndex.SkillCard]
                        , int.Parse(row[(int)HeroDBIndex.Quantity])
                    );
                    _dict.Add(data.ID, data);
            }

            //Debug.Log("HeroDB: Load Succeed.");
            _isLoad = true;
            return;
        }

        Debug.LogWarning("HeroDB: Load Failed.");
        _isLoad = false;
    }
    #endregion
}
