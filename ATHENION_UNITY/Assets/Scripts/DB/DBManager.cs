﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DBManager : Singleton<DBManager>
{
    #region Enums
    public enum DBType
    {
          CardDB
        , CardEffectDB
        , CardAbilityDB
        , ElementDB
        , HeroDB
        , DeckDB
        , ImageDB
        , RankDB
        , LocalizationDB
        , BotDB
        , BotProfileDB
        , CardPackDB
        , AvatarDB
        , CardBackDB
        , TitleDB
        , DockDB
        , ProfanityDB
    }
    #endregion

    #region Private Properties
    #endregion

    #region Methods
    public static void LoadDB()
    {
        foreach (DBManager.DBType dbType in System.Enum.GetValues(typeof(DBManager.DBType)))
        {
            DBManager.LoadDB(dbType);
        }
    }

    public static void LoadDB(DBType dBType)
    {
        switch (dBType)
        {
            case DBType.CardDB:         CardDB.Instance.LoadDB(); break;
            case DBType.CardEffectDB:   CardEffectDB.Instance.LoadDB(); break;
            case DBType.ElementDB:      ElementDB.Instance.LoadDB(); break;
            case DBType.CardAbilityDB:  CardAbilityDB.Instance.LoadDB(); break;
            case DBType.HeroDB:         HeroDB.Instance.LoadDB(); break;
            case DBType.DeckDB:         DeckDB.Instance.LoadDB(); break;
            case DBType.ImageDB:        ImageDB.Instance.LoadDB(); break;
            case DBType.RankDB:         RankDB.Instance.LoadDB(); break;
            case DBType.LocalizationDB: LocalizationDB.Instance.LoadDB(); break;
            case DBType.BotDB:          BotDB.Instance.LoadDB(); break;
            case DBType.BotProfileDB:   BotProfileDB.Instance.LoadDB(); break;
            case DBType.AvatarDB:       AvatarDB.Instance.LoadDB(); break;
            case DBType.CardBackDB:     CardBackDB.Instance.LoadDB(); break;
            case DBType.TitleDB:        TitleDB.Instance.LoadDB(); break;
            case DBType.DockDB:         DockDB.Instance.LoadDB(); break;
            case DBType.ProfanityDB:    ProfanityDB.Instance.LoadDB(); break;
        }
    }

    public static bool IsLoad(DBType dBType)
    {
        switch (dBType)
        {
            case DBType.CardDB:         return CardDB.Instance.IsLoad;
            case DBType.CardEffectDB:   return CardEffectDB.Instance.IsLoad;
            case DBType.ElementDB:      return ElementDB.Instance.IsLoad;
            case DBType.CardAbilityDB:  return CardAbilityDB.Instance.IsLoad;
            case DBType.HeroDB:         return HeroDB.Instance.IsLoad;
            case DBType.DeckDB:         return DeckDB.Instance.IsLoad;
            case DBType.ImageDB:        return ImageDB.Instance.IsLoad;
            case DBType.RankDB:         return RankDB.Instance.IsLoad;
            case DBType.LocalizationDB: return LocalizationDB.Instance.IsLoad;
            case DBType.BotDB:          return BotDB.Instance.IsLoad;
            case DBType.BotProfileDB:   return BotProfileDB.Instance.IsLoad;
            case DBType.AvatarDB:       return AvatarDB.Instance.IsLoad;
            case DBType.CardBackDB:     return CardBackDB.Instance.IsLoad;
            case DBType.TitleDB:        return TitleDB.Instance.IsLoad;
            case DBType.DockDB:         return DockDB.Instance.IsLoad;
            case DBType.ProfanityDB:    return ProfanityDB.Instance.IsLoad;
        }

        return false;
    }

    public static string GetDebugText(DBType dBType)
    {
        switch (dBType)
        {
            case DBType.CardDB:         return CardDB.Instance.GetDebugText();
            case DBType.CardEffectDB:   return CardEffectDB.Instance.GetDebugText();
            case DBType.ElementDB:      return ElementDB.Instance.GetDebugText();
            case DBType.CardAbilityDB:  return CardAbilityDB.Instance.GetDebugText();
            case DBType.HeroDB:         return HeroDB.Instance.GetDebugText();
            case DBType.DeckDB:         return DeckDB.Instance.GetDebugText();
            case DBType.ImageDB:        return ImageDB.Instance.GetDebugText();
            case DBType.RankDB:         return RankDB.Instance.GetDebugText();
            case DBType.LocalizationDB: return LocalizationDB.Instance.GetDebugText();
            case DBType.BotDB:          return BotDB.Instance.GetDebugText();
            case DBType.BotProfileDB:   return BotProfileDB.Instance.GetDebugText();
            case DBType.AvatarDB:       return AvatarDB.Instance.GetDebugText();
            case DBType.CardBackDB:     return CardBackDB.Instance.GetDebugText();
            case DBType.TitleDB:        return TitleDB.Instance.GetDebugText();
            case DBType.DockDB:         return DockDB.Instance.GetDebugText();
            case DBType.ProfanityDB:    return ProfanityDB.Instance.GetDebugText();
        }

        return "";
    }
    #endregion
}
