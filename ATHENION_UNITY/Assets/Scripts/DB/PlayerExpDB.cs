﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerExpDBData : DBData
{
    #region Public Properties
    public int Level { get; private set; }
    public string LevelStr { get { return Level.ToString(); } }
    public int AccumulativeExp { get; private set; }
    #endregion

    #region Constructors
    public PlayerExpDBData()
    {
        Level = 0;
        AccumulativeExp = 0;
    }

    public PlayerExpDBData(string level, int accumulativeExp)
    {
        Level = int.Parse(level);
        AccumulativeExp = accumulativeExp;
    }

    public PlayerExpDBData(PlayerExpDBData data)
    {
        Level = data.Level;
        AccumulativeExp = data.AccumulativeExp;
    }
    #endregion
}

public class PlayerExpDB : DictDB<PlayerExpDB, PlayerExpDBData>
{
    #region Methods
    public override void LoadFromJSON(string jsonStrData)
    {
        _dict = new Dictionary<string, PlayerExpDBData>();
        Dictionary<string, int> dataList = JsonConvert.DeserializeObject<Dictionary<string, int>>(jsonStrData);
        foreach(KeyValuePair<string, int> item in dataList)
        {
            PlayerExpDBData data = new PlayerExpDBData(item.Key, item.Value);
            _dict.Add(item.Key, data);
        }

        _isLoad = true;
    }
    #endregion
}