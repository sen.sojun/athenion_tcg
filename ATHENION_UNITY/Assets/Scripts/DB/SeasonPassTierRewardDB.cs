﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeasonPassTierRewardData
{
    #region Public Properties
    public string ID { get { return _id; } }
    public int TierLevel { get { return _tierLevel; } }
    public bool IsPremium { get { return _isPremium; } }
    public List<ItemData> ItemDataList { get { return _itemDataList; } }
    #endregion

    #region Private Properties
    private const string _tierKey = "tier";
    private const string _rewardKey = "reward";
    private const string _premiumStatusKey = "is_premium";

    private string _id = string.Empty;
    private int _tierLevel = 0;
    private bool _isPremium = false;
    private List<ItemData> _itemDataList = new List<ItemData>();
    #endregion

    #region Contructors
    public SeasonPassTierRewardData()
    {
    }

    public SeasonPassTierRewardData(string id, string jsonStrData)
    {
        SetData(id, jsonStrData);
    }

    public SeasonPassTierRewardData(SeasonPassTierRewardData data)
    {
        _id = data._id;
        _tierLevel = data._tierLevel;
        _isPremium = data._isPremium;
        _itemDataList = new List<ItemData>(data._itemDataList);
    }
    #endregion

    #region Methods
    public void SetData(string id, string jsonStrData)
    {
        _id = id;
        Dictionary<string, object> data = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonStrData);

        // get tier level
        if(data.ContainsKey(_tierKey))
        {
            _tierLevel = int.Parse(data[_tierKey].ToString());
        }

        // get reward data
        if(data.ContainsKey(_rewardKey))
        {
            Dictionary<string, int> rewardData = JsonConvert.DeserializeObject<Dictionary<string, int>>(data[_rewardKey].ToString());
            _itemDataList = DataManager.ConvertDictionaryToItemDataList(rewardData);
        }

        // get premium status
        if(data.ContainsKey(_premiumStatusKey))
        {
            _isPremium = bool.Parse(data[_premiumStatusKey].ToString());
        }
    }
    #endregion
}

public class SeasonPassTierRewardDBData : DBData
{
    #region Public Properties
    public int SeasonPassIndex { get { return _seasonPassIndex; } }
    public string SeasonPassIndexStr { get { return _seasonPassIndex.ToString(); } }
    #endregion

    #region Private Properties
    private int _seasonPassIndex = 0;
    private List<SeasonPassTierRewardData> _rewardDataList = new List<SeasonPassTierRewardData>();
    #endregion

    #region Contructors
    public SeasonPassTierRewardDBData(string seasonPassIndex, string jsonStrData)
    {
        _seasonPassIndex = int.Parse(seasonPassIndex);
        Dictionary<string, object> data = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonStrData);
        foreach(KeyValuePair<string, object> item in data)
        {
            _rewardDataList.Add(new SeasonPassTierRewardData(item.Key, item.Value.ToString()));
        }
    }

    public SeasonPassTierRewardDBData(SeasonPassTierRewardDBData data)
    {
        _seasonPassIndex = data._seasonPassIndex;
        _rewardDataList = new List<SeasonPassTierRewardData>(data._rewardDataList);
    }

    public List<SeasonPassTierRewardData> GetAllSeasonPassTierRewardData()
    {
        return new List<SeasonPassTierRewardData>(_rewardDataList);
    }

    public SeasonPassTierRewardData GetSeasonPassTierRewardData(int tierLevel)
    {
        SeasonPassTierRewardData result = _rewardDataList.Find(x => x.TierLevel == tierLevel);
        return result;
    }

    public List<SeasonPassTierRewardData> GetSeasonPassTierRewardDataList(int tierLevel)
    {
        List<SeasonPassTierRewardData> result = _rewardDataList.FindAll(x => x.TierLevel == tierLevel);
        return result;
    }
    #endregion
}

public class SeasonPassTierRewardDB : DictDB<SeasonPassTierRewardDB, SeasonPassTierRewardDBData>
{
    #region Methods
    public override void LoadFromJSON(string jsonStrData)
    {
        _dict = new Dictionary<string, SeasonPassTierRewardDBData>();
        Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonStrData);
        foreach (KeyValuePair<string, object> data in rawData)
        {
            SeasonPassTierRewardDBData dbData = new SeasonPassTierRewardDBData(data.Key, data.Value.ToString());
            _dict.Add(data.Key, dbData);
        }

        _isLoad = true;
    }
    #endregion
}