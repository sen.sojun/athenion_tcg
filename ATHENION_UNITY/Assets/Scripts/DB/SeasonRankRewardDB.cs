﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeasonRankRewardDBData : DBData
{
    #region Public Properties
    public int SeasonIndex { get { return _seasonIndex; } }
    public string SeasonIndexStr { get { return _seasonIndex.ToString(); } }
    #endregion

    #region Private Properties
    private int _seasonIndex = 0;
    private Dictionary<string, List<ItemData>> _itemDataList = new Dictionary<string, List<ItemData>>();
    #endregion

    #region Contructors
    public SeasonRankRewardDBData(string seasonIndex, string jsonStrData)
    {
        _seasonIndex = int.Parse(seasonIndex);
        Dictionary<string, Dictionary<string, Dictionary<string, int>>> unitData = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, Dictionary<string, int>>>>(jsonStrData);
        foreach(KeyValuePair<string, Dictionary<string, Dictionary<string, int>>> unit in unitData)
        {
            _itemDataList.Add(unit.Key, new List<ItemData>());
            foreach(KeyValuePair<string, int> item in unit.Value["reward"])
            {
                _itemDataList[unit.Key].Add(new ItemData(item));
            }
        }
    }

    public SeasonRankRewardDBData(SeasonRankRewardDBData data)
    {
        _seasonIndex = data._seasonIndex;
        _itemDataList = new Dictionary<string, List<ItemData>>(data._itemDataList);
    }
    #endregion

    #region Methods
    public List<ItemData> GetRewardItemList(int rankIndex)
    {
        string rankIndexStr = rankIndex.ToString();
        foreach(KeyValuePair<string, List<ItemData>> item in _itemDataList)
        {
            string[] rankRangeStr = item.Key.Split('_');
            if(rankRangeStr.Length > 0)
            {
                if (rankRangeStr.Length > 1)
                {
                    int rank1 = int.Parse(rankRangeStr[0]);
                    int rank2 = int.Parse(rankRangeStr[1]);
                    if(rankIndex >= rank1 && rankIndex <= rank2)
                    {
                        return _itemDataList[item.Key];
                    }
                }
                else
                {
                    int rank1 = int.Parse(rankRangeStr[0]);
                    if (rankIndex == rank1)
                    {
                        return _itemDataList[item.Key];
                    }
                }
            }
        }

        return new List<ItemData>();
    }
    #endregion
}

public class SeasonRankRewardDB : DictDB<SeasonRankRewardDB, SeasonRankRewardDBData>
{
    #region Methods
    public override void LoadFromJSON(string jsonStrData)
    {
        _dict = new Dictionary<string, SeasonRankRewardDBData>();
        Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonStrData);
        foreach (KeyValuePair<string, object> data in rawData)
        {
            SeasonRankRewardDBData dbData = new SeasonRankRewardDBData(data.Key, data.Value.ToString());
            _dict.Add(data.Key, dbData);
        }

        _isLoad = true;
    }
    #endregion
}