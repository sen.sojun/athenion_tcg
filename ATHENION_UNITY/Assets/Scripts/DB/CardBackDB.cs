﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Karamucho;

public class CardBackDBData : DBData
{
    #region Public Properties
    public string CardBackID { get; private set; }
    public string CardBackName { get; private set; }
    public bool IsListing { get; private set; }
    public string Description { get; private set; }
    #endregion

    #region Constructors
    public CardBackDBData()
    {
        CardBackID = "";
        CardBackName = "";
        IsListing = false;
        Description = "";
    }

    public CardBackDBData(string avatarID, string avatarName, bool isListing, string description)
    {
        CardBackID = avatarID;
        CardBackName = avatarName;
        IsListing = isListing;
        Description = description;
    }

    public CardBackDBData(CardBackDBData data)
    {
        CardBackID = data.CardBackID;
        CardBackName = data.CardBackName;
        IsListing = data.IsListing;
        Description = data.Description;
    }
    #endregion

    #region Methods
    public override string GetDebugText()
    {
        int index = 0;
        string result = string.Format(
                "CardBackID: {" + index++ + "}"
              + "\nCardBack Name: {" + index++ + "}"
              + "\nIsListing: {" + index++ + "}"
              + "\nDescription: {" + index++ + "}"
            , CardBackID
            , CardBackName
            , IsListing
            , Description
        );

        return result;
    }
    #endregion
}

public class CardBackDB : DictDB<CardBackDB, CardBackDBData>
{
    #region Enums
    public enum CardBackDBIndex
    {
          CardBackID = 0
        , CardBackName
        , IsListing
        , Description
    }
    #endregion

    #region Protected Properties
    protected string _fileName = "Databases/KaramuchoDB - CardBackDB";
    #endregion

    #region Private Properties
    #endregion

    #region Constructors
    #endregion

    #region Methods
    public override void LoadDB()
    {
        _dict = new Dictionary<string, CardBackDBData>();

        List<List<string>> rawDataList;
        TextAsset csvText = ResourceManager.Load<TextAsset>(_fileName);

        bool isSuccess = CSVReader.ReadCSV(csvText, out rawDataList);
        if (isSuccess)
        {
            bool isListing = false;

            foreach (List<string> row in rawDataList)
            {
                isListing = bool.Parse(row[(int)CardBackDBIndex.IsListing]);

                CardBackDBData data = new CardBackDBData(
                      row[(int)CardBackDBIndex.CardBackID]
                    , row[(int)CardBackDBIndex.CardBackName]
                    , isListing
                    , row[(int)CardBackDBIndex.Description]
                );
                _dict.Add(data.CardBackID, data);
            }

            //Debug.Log("CardBackDB: Load Succeed.");
            _isLoad = true;
            return;
        }

        Debug.LogWarning("CardBackDB: Load Failed.");
        _isLoad = false;
    }
    #endregion
}
