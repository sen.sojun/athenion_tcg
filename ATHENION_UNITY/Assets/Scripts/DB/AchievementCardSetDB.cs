﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class AchievementCardSetDBData : DBData
{
    #region Public Properties
    public string ID { get; private set; }
    public List<PlayerStatDetailData> DetailDataList { get { return _detailDataList; } }
    public List<string> CardIDList { get { return _cardIDList; } }
    public List<ItemData> ItemDataList { get { return _itemDataList; } }
    public int MaxProgress { get { return GetMaxProgress(); } }
    #endregion

    #region Private Properties
    private const string _detailKey = "detail";
    private const string _rewardKey = "reward";

    private List<PlayerStatDetailData> _detailDataList = new List<PlayerStatDetailData>();
    private List<string> _cardIDList = new List<string>();
    private List<ItemData> _itemDataList = new List<ItemData>();
    #endregion

    #region Contructors
    public AchievementCardSetDBData()
    {
    }

    public AchievementCardSetDBData(string id, string jsonStrData)
    {
        ID = id;
        Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonStrData);
        if (rawData.ContainsKey(_detailKey))
        {
            SetupDetailData(JsonConvert.DeserializeObject<Dictionary<string, int>>(rawData[_detailKey].ToString()));
        }

        if (rawData.ContainsKey(_rewardKey))
        {
            SetupRewardData(JsonConvert.DeserializeObject<Dictionary<string, int>>(rawData[_rewardKey].ToString()));
        }
    }

    public AchievementCardSetDBData(AchievementCardSetDBData data)
    {
        ID = data.ID;
        _detailDataList = new List<PlayerStatDetailData>(data._detailDataList);
        _cardIDList = new List<string>(data._cardIDList);
        _itemDataList = new List<ItemData>(data._itemDataList);
    }
    #endregion

    #region Methods
    private void SetupDetailData(Dictionary<string, int> dictData)
    {
        foreach (KeyValuePair<string, int> detail in dictData)
        {
            _detailDataList.Add(new PlayerStatDetailData(detail));
        }

        // add card list
        string keyToRemove = StatKeys.C_SET.ToString() + "_";
        foreach (PlayerStatDetailData data in _detailDataList)
        {
            foreach (string key in data.PlayerStatList)
            {
                string cardID = key.Replace(keyToRemove, "");
                if(!string.IsNullOrEmpty(cardID))
                {
                    _cardIDList.Add(cardID);
                }
            }
        }
    }

    private void SetupRewardData(Dictionary<string, int> dictData)
    {
        foreach (KeyValuePair<string, int> item in dictData)
        {
            _itemDataList.Add(new ItemData(item));
        }
    }

    private int GetMaxProgress()
    {
        int reqCount = 0;
        foreach (PlayerStatDetailData detail in _detailDataList)
        {
            reqCount += detail.Amount;
        }

        return reqCount;
    }

    public string GetCardIDFromStatKey(string statKey)
    {
        string keyToRemove = StatKeys.C_SET.ToString() + "_";
        return statKey.Replace(keyToRemove, "");
    }
    #endregion
}

public class AchievementCardSetDB : DictDB<AchievementCardSetDB, AchievementCardSetDBData>
{
    #region Methods
    public override void LoadFromJSON(string jsonStrData)
    {
        _dict = new Dictionary<string, AchievementCardSetDBData>();
        Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonStrData);
        foreach (KeyValuePair<string, object> data in rawData)
        {
            _dict.Add(data.Key, new AchievementCardSetDBData(data.Key, data.Value.ToString()));
        }

        _isLoad = true;
    }
    #endregion
}