﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#region Enums
public enum SeasonPassPriceTypes
{
    None = 0
    , Premium
    , Tier
}
#endregion

public class SeasonPassPriceData
{
    #region Private Properties
    private const string _priceKey = "price";
    private const string _startDateTimeKey = "start_date";
    private const string _endDateTimeKey = "end_date";

    private Dictionary<VirtualCurrency, int> _price = new Dictionary<VirtualCurrency, int>();
    private DateTime _startDateTime = DateTime.MinValue;
    private DateTime _endDateTime = DateTime.MaxValue;
    #endregion

    #region Contructors
    public SeasonPassPriceData()
    {
    }

    public SeasonPassPriceData(string jsonStrData)
    {
        SetData(jsonStrData);
    }
    #endregion

    #region Methods
    public void SetData(string jsonStrData)
    {
        Dictionary<string, object> data = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonStrData);
        if(data.ContainsKey(_priceKey))
        {
            Dictionary<string, int> priceData = JsonConvert.DeserializeObject<Dictionary<string, int>>(data[_priceKey].ToString());
            foreach(KeyValuePair<string, int> price in priceData)
            {
                GameHelper.StrToEnum(price.Key.Replace("VC_", ""), out VirtualCurrency vc);
                _price.Add(vc, price.Value);
            }
        }

        if(data.ContainsKey(_startDateTimeKey))
        {
            _startDateTime = DateTime.Parse(data[_startDateTimeKey].ToString());
        }

        if (data.ContainsKey(_endDateTimeKey))
        {
            _endDateTime = DateTime.Parse(data[_endDateTimeKey].ToString());
        }
    }

    public bool IsCurrentDateTimeValid()
    {
        return GameHelper.IsCurrentUTCDateTimeValid(_startDateTime, _endDateTime);
    }

    public int GetPrice(VirtualCurrency vc)
    {
        int result = 0;
        if(_price.ContainsKey(vc))
        {
            result = _price[vc];
        }

        return result;
    }
    #endregion
}

public class SeasonPassPriceDBData : DBData
{
    #region Public Properties
    public SeasonPassPriceTypes PriceType { get { return _priceType; } }
    #endregion

    #region Private Properties
    private SeasonPassPriceTypes _priceType = SeasonPassPriceTypes.None;
    private List<SeasonPassPriceData> _priceDataList = new List<SeasonPassPriceData>();
    #endregion

    #region Contructors
    public SeasonPassPriceDBData(string priceTypeKey, string priceData)
    {
        _priceType = GetSeasonPriceType(priceTypeKey);
        List<object> data = JsonConvert.DeserializeObject<List<object>>(priceData);
        foreach(object item in data)
        {
            _priceDataList.Add(new SeasonPassPriceData(item.ToString()));
        }
    }
    #endregion

    #region Methods
    private SeasonPassPriceTypes GetSeasonPriceType(string typeKey)
    {
        switch (typeKey)
        {
            case "premium": return SeasonPassPriceTypes.Premium;
            case "tier": return SeasonPassPriceTypes.Tier;
            default: return SeasonPassPriceTypes.None;
        }
    }

    public int GetPrice(VirtualCurrency vc)
    {
        foreach(SeasonPassPriceData data in _priceDataList)
        {
            if(data.IsCurrentDateTimeValid())
            {
                return data.GetPrice(vc);
            }
        }

        return 0;
    }
    #endregion
}

public class SeasonPassPriceDB : DictDB<SeasonPassPriceDB, SeasonPassPriceDBData>
{
    #region Methods
    public override void LoadFromJSON(string jsonStrData)
    {
        _dict = new Dictionary<string, SeasonPassPriceDBData>();
        Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonStrData);
        foreach (KeyValuePair<string, object> data in rawData)
        {
            SeasonPassPriceDBData dbData = new SeasonPassPriceDBData(data.Key, data.Value.ToString());
            _dict.Add(data.Key, dbData);
        }

        _isLoad = true;
    }

    public int GetSeasonPassPrice(SeasonPassPriceTypes type, VirtualCurrency vc)
    {
        foreach(KeyValuePair<string, SeasonPassPriceDBData> data in _dict)
        {
            if(data.Value.PriceType == type)
            {
                return data.Value.GetPrice(vc);
            }
        }

        return 0;
    }
    #endregion
}
