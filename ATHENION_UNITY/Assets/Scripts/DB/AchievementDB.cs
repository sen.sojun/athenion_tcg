﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class AchievementDBData : DBData
{
    #region Public Properties
    public string ID { get; private set; }
    public List<PlayerStatDetailData> DetailDataList { get { return _detailDataList; } }
    public List<ItemData> ItemDataList { get { return _itemDataList; } }
    public int MaxProgress { get { return GetMaxProgress(); } }
    #endregion

    #region Private Properties
    private const string _detailKey = "detail";
    private const string _rewardKey = "reward";

    private List<PlayerStatDetailData> _detailDataList = new List<PlayerStatDetailData>();
    private List<ItemData> _itemDataList = new List<ItemData>();
    #endregion

    #region Contructors
    public AchievementDBData()
    {
    }

    public AchievementDBData(string id, string jsonStrData)
    {
        ID = id;
        Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonStrData);
        if(rawData.ContainsKey(_detailKey))
        {
            SetupDetailData(JsonConvert.DeserializeObject<Dictionary<string, int>>(rawData[_detailKey].ToString()));
        }

        if (rawData.ContainsKey(_rewardKey))
        {
            SetupRewardData(JsonConvert.DeserializeObject<Dictionary<string, int>>(rawData[_rewardKey].ToString()));
        }
    }

    public AchievementDBData(AchievementDBData data)
    {
        ID = data.ID;
        _detailDataList = new List<PlayerStatDetailData>(data._detailDataList);
        _itemDataList = new List<ItemData>(data._itemDataList);
    }
    #endregion

    #region Methods
    private void SetupDetailData(Dictionary<string, int> dictData)
    {
        foreach(KeyValuePair<string, int> detail in dictData)
        {
            _detailDataList.Add(new PlayerStatDetailData(detail));
        }
    }

    private void SetupRewardData(Dictionary<string, int> dictData)
    {
        foreach (KeyValuePair<string, int> item in dictData)
        {
            _itemDataList.Add(new ItemData(item));
        }
    }

    private int GetMaxProgress()
    {
        int reqCount = 0;
        foreach (PlayerStatDetailData detail in _detailDataList)
        {
            reqCount += detail.Amount;
        }

        return reqCount;
    }
    #endregion
}

public class AchievementDB : DictDB<AchievementDB, AchievementDBData>
{
    #region Methods
    public override void LoadFromJSON(string jsonStrData)
    {
        _dict = new Dictionary<string, AchievementDBData>();
        Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonStrData);
        foreach(KeyValuePair<string, object> data in rawData)
        {
            _dict.Add(data.Key, new AchievementDBData(data.Key, data.Value.ToString()));
        }

        _isLoad = true;
    }
    #endregion
}