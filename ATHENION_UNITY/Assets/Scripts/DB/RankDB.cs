﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Karamucho;

public class RankDBData : DBData
{
    #region Public Properties
    public string RankID { get; private set; }
    public string RankName { get; private set; }
    public int RankIndex { get; private set; }
    public int RankScore { get; private set; }
    public int RankStartScore { get; private set; }
    public int GroupIndex { get; private set; }
    #endregion

    #region Constructors
    public RankDBData()
    {
        RankID = "";
        RankName = "";
        RankIndex = -1;
        RankScore = 0;
        RankStartScore = 0;
        GroupIndex = 0;
    }

    public RankDBData(string rankID, string rankName, int rankIndex, int rankScore, int rankStartScore, int groupIndex)
    {
        RankID = rankID;
        RankName = rankName;
        RankIndex = rankIndex;
        RankScore = rankScore;
        RankStartScore = rankStartScore;
        GroupIndex = groupIndex;
    }

    public RankDBData(RankDBData data)
    {
        RankID = data.RankID;
        RankName = data.RankName;
        RankIndex = data.RankIndex;
        RankScore = data.RankScore;
        RankStartScore = data.RankStartScore;
        GroupIndex = data.GroupIndex;
    }
    #endregion

    #region Methods
    public override string GetDebugText()
    {
        int index = 0;
        string result = string.Format(
                "RankID: {" + index++ + "}"
              + "\nRank Name: {" + index++ + "}"
              + "\nRank Index: {" + index++ + "}"
              + "\nRank Score: {" + index++ + "}"
              + "\nRank StartScore: {" + index++ + "}"
              + "\nGroupIndex: {" + index++ + "}"
            , RankID
            , RankName
            , RankIndex
            , RankScore
            , RankStartScore
            , GroupIndex
        );

        return result;
    }
    #endregion
}

public class RankDB : DictDB<RankDB, RankDBData>
{
    #region Enums
    public enum RankDBIndex
    {
          RankID = 0
        , RankName
        , RankIndex
        , RankScore
        , RankStartScore
        , GroupIndex
    }
    #endregion

    #region Protected Properties
    protected string _fileName = "Databases/KaramuchoDB - RankDB";
    #endregion

    #region Private Properties
    #endregion

    #region Constructors
    #endregion

    #region Methods
    public override void LoadDB()
    {
        _dict = new Dictionary<string, RankDBData>();

        List<List<string>> rawDataList;
        TextAsset csvText = ResourceManager.Load<TextAsset>(_fileName);

        bool isSuccess = CSVReader.ReadCSV(csvText, out rawDataList);
        if (isSuccess)
        {
            foreach (List<string> row in rawDataList)
            {
                RankDBData data = new RankDBData(
                      row[(int)RankDBIndex.RankID]
                    , row[(int)RankDBIndex.RankName]
                    , int.Parse(row[(int)RankDBIndex.RankIndex])
                    , int.Parse(row[(int)RankDBIndex.RankScore])
                    , int.Parse(row[(int)RankDBIndex.RankStartScore])
                    , int.Parse(row[(int)RankDBIndex.GroupIndex])
                );
                _dict.Add(data.RankID, data);
            }

            //Debug.Log("RankDB: Load Succeed.");
            _isLoad = true;
            return;
        }

        Debug.LogWarning("RankDB: Load Failed.");
        _isLoad = false;
    }

    public bool GetRank(int playerRank, out RankDBData data)
    {
        if (!IsLoad)
        {
            this.LoadDB();
        }

        data = null;
        foreach (KeyValuePair<string, RankDBData> item in _dict)
        {
            if (playerRank >= item.Value.RankStartScore)
            {
                data = item.Value;
            }
            else
            {
                break;
            }
        }

        return (data != null);
    }
    #endregion
}
