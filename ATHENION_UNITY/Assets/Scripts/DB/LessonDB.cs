﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Karamucho;

public class LessonDBData : DBData
{
    #region Public Properties
    public string LessonID { get; private set; }
    public string LessonStepID { get; private set; }
    #endregion

    #region Constructors
    public LessonDBData()
    {
        LessonID = "";
        LessonStepID = "";
    }

    public LessonDBData(string lessonID, string lessonStepID)
    {
        LessonID = lessonID;
        LessonStepID = lessonStepID;
    }

    public LessonDBData(LessonDBData data)
    {
        LessonID = data.LessonID;
        LessonStepID = data.LessonStepID;
    }
    #endregion

    #region Methods
    public override string GetDebugText()
    {
        int index = 0;
        string result = string.Format(
                "LessonID: {" + index++ + "}"
              + "\nLessonStepID: {" + index++ + "}"
            , LessonID
            , LessonStepID
        );

        return result;
    }
    #endregion
}

public class LessonDB : ListDB<LessonDB, LessonDBData>
{
    #region Enums
    public enum LessonDBIndex
    {
          LessonID = 0
        , LessonStepID
    }
    #endregion

    #region Protected Properties
    protected string _fileName = "Databases/KaramuchoDB - LessonDB";
    #endregion

    #region Private Properties
    #endregion

    #region Constructors
    #endregion

    #region Methods
    public override void LoadDB()
    {
        _list = new List<LessonDBData>();

        List<List<string>> rawDataList;
        TextAsset csvText = ResourceManager.Load<TextAsset>(_fileName);

        bool isSuccess = CSVReader.ReadCSV(csvText, out rawDataList);
        if (isSuccess)
        {
            foreach (List<string> row in rawDataList)
            {
                LessonDBData data = new LessonDBData(
                      row[(int)LessonDBIndex.LessonID]
                    , row[(int)LessonDBIndex.LessonStepID]
                );
                _list.Add(data);
            }

            //Debug.Log("LessonDB: Load Succeed.");
            _isLoad = true;
            return;
        }

        Debug.LogWarning("LessonDB: Load Failed.");
        _isLoad = false;
    }

    public bool GetAllLesson(out Dictionary<string, List<string>> lessonList)
    {
        if (!IsLoad)
        {
            LoadDB();
        }

        lessonList = new Dictionary<string, List<string>>();

        if (IsLoad)
        {
            foreach (LessonDBData data in _list)
            {
                if (lessonList.ContainsKey(data.LessonID))
                {
                    lessonList[data.LessonID].Add(data.LessonStepID);
                }
                else
                {
                    List<string> stepList = new List<string>();
                    stepList.Add(data.LessonStepID);

                    lessonList.Add(data.LessonID, stepList);
                }
            }

            if (lessonList.Count > 0)
            {
                return true;
            }
        }

        return false;
    }

    public bool GetLesson(string lessonID, out List<string> lessonStepIDList)
    {
        if (!IsLoad)
        {
            LoadDB();
        }

        lessonStepIDList = new List<string>();

        if (IsLoad)
        {
            foreach (LessonDBData data in _list)
            {
                if (data.LessonID == lessonID)
                {
                    lessonStepIDList.Add(data.LessonStepID);
                }
            }

            if (lessonStepIDList.Count > 0)
            {
                return true;
            }
        }

        return false;
    }
    #endregion
}
