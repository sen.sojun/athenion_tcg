﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Karamucho;

public class LocalizationDBData : DBData
{
    #region Public Properties
    public string Key { get; private set; }
    public List<string> TextList { get; private set; }
    #endregion

    #region Constructors
    public LocalizationDBData()
    {
        Key = "";
        TextList = new List<string>();
    }

    public LocalizationDBData(string key, List<string> textList)
    {
        Key = key;
        TextList = new List<string>(textList);
    }

    public LocalizationDBData(LocalizationDBData data)
    {
        Key = data.Key;
        TextList = new List<string>(data.TextList);
    }
    #endregion

    #region Methods
    public override string GetDebugText()
    {
        string texts = "";
        foreach (Language language in System.Enum.GetValues(typeof(Language)))
        {
            if (texts.Length > 0)
            {
                texts = string.Format(
                      "{0}\n{1}:{2}"
                    , texts
                    , language.ToString()
                    , TextList[(int)language]
                );
            }
            else
            {
                texts = string.Format(
                     "{0}:{1}"
                   , language.ToString()
                   , TextList[(int)language]
               );
            }
        }

        string result = string.Format(
            "Key: {0}\n{1}\n "
            , Key
            , texts
        );

        return result;
    }
    #endregion
}

public class LocalizationDB : DictDB<LocalizationDB, LocalizationDBData>
{
    #region Enums
    public enum LocalizationDBIndex
    {
        Key = 0
        , Language_1
        , Language_2
        , Language_3
    }
    #endregion

    #region Private Properties
    private static string _rootPath = "Databases/Localizations/";
    private static string[] _fileNames = {
          "KaramuchoDB - LocalizationDB"
        , "KaramuchoDB - Localize_QuestAchievementDB"
        , "KaramuchoDB - Localize_CardNameDB"
        , "KaramuchoDB - Localize_CardNameDB_Old"
        , "KaramuchoDB - Localize_CardAbilityDB"
        , "KaramuchoDB - Localize_CardAbilityDB_Old"
        , "KaramuchoDB - Localize_CardDescriptionDB"
        , "KaramuchoDB - Localize_HeroNameDB"
        , "KaramuchoDB - Localize_RankDB"
        , "KaramuchoDB - Localize_StoreDB"
        , "KaramuchoDB - Localize_EventBattleDB"
        , "KaramuchoDB - Localize_CardBackDB"
        , "KaramuchoDB - Localize_TitleDB"
        , "KaramuchoDB - Localize_DockDB"
        , "KaramuchoDB - Localize_AvatarDB"
        , "KaramuchoDB - Localize_LoadingTipDB"
        , "KaramuchoDB - Localize_TutorialDB"
        , "KaramuchoDB - Localize_EventDB"
        , "KaramuchoDB - Localize_CardSetDB"
        , "KaramuchoDB - Localize_DeckDB"
        , "KaramuchoDB - Localize_MissionDB"
        , "KaramuchoDB - Localize_AdventureDB"
        , "KaramuchoDB - Localize_LessonDB"
    };
    #endregion

    #region Constructors
    #endregion

    #region Methods

    public void LoadOverrideDB(string csvText)
    {
        csvText = csvText.Replace("\\r", "");
        csvText = csvText.Replace("\\n", "\n");
        List<List<string>> rawDataList;
        bool isSuccess = CSVReader.ReadCSV(csvText, out rawDataList);
        if (isSuccess)
        {
            int rowIndex = 2; // row index in sheet
            foreach (List<string> row in rawDataList)
            {
                LocalizationDBData data = CreateLocalizationDBDataFromCSVByRow(row);

                if (data != null && data.Key.Length > 0)
                {
                    if (_dict.ContainsKey(data.Key))
                    {
                        _dict[data.Key] = data;
                    }
                    else
                    {
                        _dict.Add(data.Key, data);
                    }
                }
                else
                {
                    Debug.LogWarningFormat("LocalizationDB/LoadOverrideDB: Key is empty! row[{0}]", rowIndex);
                }

                ++rowIndex;
            }
        }
    }

    public override void LoadDB()
    {
        _dict = new Dictionary<string, LocalizationDBData>();

        List<List<string>> rawDataList;
        string filePath = "";
        foreach (string fileName in _fileNames)
        {
            filePath = _rootPath + fileName;
            TextAsset csvText = ResourceManager.Load<TextAsset>(filePath);

            if (csvText != null)
            {
                bool isSuccess = CSVReader.ReadCSV(csvText, out rawDataList);
                if (isSuccess)
                {
                    //Debug.LogFormat("LocalizationDB: Loading file... {0}", filePath);

                    int rowIndex = 2; // row index in sheet
                    foreach (List<string> row in rawDataList)
                    {
                        LocalizationDBData data = CreateLocalizationDBDataFromCSVByRow(row);

                        if (!_dict.ContainsKey(data.Key))
                        {
                            if (data != null && data.Key.Length > 0)
                            {
                                _dict.Add(data.Key, data);
                                //Debug.LogFormat("LocalizationDB: Add Key {0}", data.Key);
                            }
                            else
                            {
                                Debug.LogWarningFormat("LocalizationDB/LoadDB: Key is empty! {0} row[{1}]", filePath, rowIndex);
                            }
                        }
                        else
                        {
                            Debug.LogWarningFormat("LocalizationDB/LoadDB: Duplicate key ! row[{2}] {0} : {1}", data.Key, filePath, rowIndex);
                        }

                        ++rowIndex;
                    }
                }
            }
        }
        _isLoad = true;
    }

    public static LocalizationDBData CreateLocalizationDBDataFromCSVByRow(List<string> row)
    {
        string key = row[(int)LocalizationDBIndex.Key];

        List<string> text = new List<string>();
        for (int index = (int)LocalizationDBIndex.Language_1; index <= (int)LocalizationDBIndex.Language_3; ++index)
        {
            if (index < row.Count)
            {
                text.Add(row[index]);
            }
            else
            {
                text.Add("");
                Debug.LogErrorFormat("LocalizationDB: Localize missing !! {0} index:{1}", key, index.ToString());
            }
        }

        LocalizationDBData data = new LocalizationDBData(key, text);
        return data;
    }
    #endregion
}