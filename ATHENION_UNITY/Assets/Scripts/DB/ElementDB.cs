﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Karamucho;

public class ElementDBData : DBData
{
    #region Public Properties
    public string ID { get; private set; }
    public string Name { get; private set; }
    #endregion

    #region Constructors
    public ElementDBData()
    {
        ID = "";
        Name = "";
    }

    public ElementDBData(string id, string name)
    {
        ID = id;
        Name = name;
    }

    public ElementDBData(ElementDBData data)
    {
        ID = data.ID;
        Name = data.Name;
    }
    #endregion

    #region Methods
    public override string GetDebugText()
    {
        int index = 0;
        string result = string.Format(
                "ID: {" + index++ + "}"
              + "\nName: {" + index++ + "}"
            , ID
            , Name
        );

        return result;
    }
    #endregion
}

public class ElementDB : DictDB<ElementDB, ElementDBData>
{
    #region Enums
    public enum ElementDBIndex
    {
          ElementID = 0
        , Name
    }
    #endregion

    #region Protected Properties
    protected string _fileName = "Databases/KaramuchoDB - ElementDB";
    #endregion

    #region Private Properties
    #endregion

    #region Constructors
    #endregion

    #region Methods
    public override void LoadDB()
    {
        _dict = new Dictionary<string, ElementDBData>();

        List<List<string>> rawDataList;
        TextAsset csvText = ResourceManager.Load<TextAsset>(_fileName);

        bool isSuccess = CSVReader.ReadCSV(csvText, out rawDataList);
        if (isSuccess)
        {
            foreach (List<string> row in rawDataList)
            {
                if (isSuccess)
                {
                    ElementDBData data = new ElementDBData(
                          row[(int)ElementDBIndex.ElementID]
                        , row[(int)ElementDBIndex.Name]
                    );
                    _dict.Add(data.ID, data);

                    //Debug.Log(data.GetDebugText());
                }
                else
                {
                    Debug.LogWarning(string.Format(
                        "ElementDB/LoadDB: Failed to load data. {0} Skip!"
                        , row[(int)ElementDBIndex.ElementID]
                    ));
                }
            }
            //Debug.Log("ElementDB: Load Succeed.");
            _isLoad = true;
            return;
        }

        Debug.LogWarning("ElementDB: Load Failed.");
        _isLoad = false;
    }
    #endregion
}
