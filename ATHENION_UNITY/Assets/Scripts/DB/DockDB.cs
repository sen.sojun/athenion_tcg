﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Karamucho;

public class DockDBData : DBData
{
    #region Public Properties
    public string DockID { get; private set; }
    public string DockName { get; private set; }
    public bool IsListing { get; private set; }
    public string Description { get; private set; }
    #endregion

    #region Constructors
    public DockDBData()
    {
        DockID = "";
        DockName = "";
        IsListing = false;
        Description = "";
    }

    public DockDBData(string avatarID, string avatarName, bool isListing, string description)
    {
        DockID = avatarID;
        DockName = avatarName;
        IsListing = isListing;
        Description = description;
    }

    public DockDBData(DockDBData data)
    {
        DockID = data.DockID;
        DockName = data.DockName;
        IsListing = data.IsListing;
        Description = data.Description;
    }
    #endregion

    #region Methods
    public override string GetDebugText()
    {
        int index = 0;
        string result = string.Format(
                "DockID: {" + index++ + "}"
              + "\nDock Name: {" + index++ + "}"
              + "\nIsListing: {" + index++ + "}"
              + "\nDescription: {" + index++ + "}"
            , DockID
            , DockName
            , IsListing
            , Description
        );

        return result;
    }
    #endregion
}

public class DockDB : DictDB<DockDB, DockDBData>
{
    #region Enums
    public enum DockDBIndex
    {
          DockID = 0
        , DockName
        , IsListing
        , Description
    }
    #endregion

    #region Protected Properties
    protected string _fileName = "Databases/KaramuchoDB - DockDB";
    #endregion

    #region Private Properties
    #endregion

    #region Constructors
    #endregion

    #region Methods
    public override void LoadDB()
    {
        _dict = new Dictionary<string, DockDBData>();

        List<List<string>> rawDataList;
        TextAsset csvText = ResourceManager.Load<TextAsset>(_fileName);

        bool isSuccess = CSVReader.ReadCSV(csvText, out rawDataList);
        if (isSuccess)
        {
            bool isListing = false;

            foreach (List<string> row in rawDataList)
            {
                isListing = bool.Parse(row[(int)DockDBIndex.IsListing]);

                DockDBData data = new DockDBData(
                      row[(int)DockDBIndex.DockID]
                    , row[(int)DockDBIndex.DockName]
                    , isListing
                    , row[(int)DockDBIndex.Description]
                );
                _dict.Add(data.DockID, data);
            }

            //Debug.Log("DockDB: Load Succeed.");
            _isLoad = true;
            return;
        }

        Debug.LogWarning("DockDB: Load Failed.");
        _isLoad = false;
    }
    #endregion
}
