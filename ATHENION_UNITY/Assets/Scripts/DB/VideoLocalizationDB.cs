﻿using System;
using System.Collections.Generic;
using Karamucho;
using UnityEngine;

public class VideoLocalizationData
{
    public double StartSecond => _startSecond;
    public double EndSecond => _endSecond;
    public List<string> TextList => _textList;

    private double _startSecond = 0;
    private double _endSecond = 0;
    private List<string> _textList = new List<string>();

    public VideoLocalizationData(double startSecond, double endSecond, List<string> textList)
    {
        _startSecond = startSecond;
        _endSecond = endSecond;
        _textList = textList;
    }

    public VideoLocalizationData(VideoLocalizationData data)
    {
        _startSecond = data.StartSecond;
        _endSecond = data.EndSecond;
        _textList = new List<string>(data.TextList);
    }
}

public class VideoSubtitleData
{
    public double StartSecond => _startSecond;
    public double EndSecond => _endSecond;
    public string Text => _text;

    private double _startSecond = 0;
    private double _endSecond = 0;
    private string _text = string.Empty;

    public VideoSubtitleData(VideoLocalizationData data, Language language)
    {
        _startSecond = data.StartSecond;
        _endSecond = data.EndSecond;
        _text = data.TextList[(int) language];
    }
}

public class VideoLocalizationDBData : DBData
{
    #region Public Properties
    public string Key { get; private set; }
    public List<VideoLocalizationData> VideoLocalizationDataList { get; private set; }
    #endregion

    #region Constructors
    public VideoLocalizationDBData()
    {
        Key = "";
        VideoLocalizationDataList = new List<VideoLocalizationData>();
    }

    public VideoLocalizationDBData(string key, List<VideoLocalizationData> videoLocalizationDataList)
    {
        Key = key;
        VideoLocalizationDataList = videoLocalizationDataList;
    }

    public VideoLocalizationDBData(VideoLocalizationDBData data)
    {
        Key = data.Key;
        VideoLocalizationDataList = new List<VideoLocalizationData>(data.VideoLocalizationDataList);
    }
    #endregion

    #region Methods
    public override string GetDebugText()
    {
        string text = Key;
        foreach (VideoLocalizationData videoSubTitleData in VideoLocalizationDataList)
        {
            text += $"\n{videoSubTitleData.StartSecond} : {videoSubTitleData.EndSecond}";
            foreach (string str in videoSubTitleData.TextList)
            {
                text += $" : {str}";
            }
        }

        return text;
    }
    #endregion
}

public class VideoLocalizationDB : DictDB<VideoLocalizationDB, VideoLocalizationDBData>
{
    #region Enums

    public enum VideoLocalizationDBIndex
    {
        StartSecond = 0
        , EndSecond
        , Language_1
        , Language_2
        , Language_3
    }

    #endregion

    #region Protected Properties
    private static readonly string RootPath = "Databases/Localizations/VideoLocalizations/";
    private static readonly string[] FileNames =
    {
        "KaramuchoDB - Localize_TestVideo"
    };
    #endregion

    #region Methods

    public override void LoadDB()
    {
        _dict = new Dictionary<string, VideoLocalizationDBData>();
        
        List<List<string>> rawDataList;
        string filePath = string.Empty;
        foreach (string fileName in FileNames)
        {
            filePath = RootPath + fileName;
            string key = fileName.Split(new[] {"_"}, StringSplitOptions.None)[1];
            TextAsset csvText = ResourceManager.Load<TextAsset>(filePath);

            bool isSuccess = CSVReader.ReadCSV(csvText, out rawDataList);

            if (isSuccess)
            {
                List<VideoLocalizationData> videoSubTitleData = new List<VideoLocalizationData>();
                List<double> startSecondList = new List<double>();

                foreach (List<string> row in rawDataList)
                {
                    List<string> text = new List<string>();
                    for (int index = (int) VideoLocalizationDBIndex.Language_1;
                        index <= (int) VideoLocalizationDBIndex.Language_3;
                        index++)
                    {
                        if (index < row.Count)
                        {
                            text.Add(row[index]);
                        }
                        else
                        {
                            text.Add("");
                            Debug.LogErrorFormat("VideoLocalizationDB: Localize missing !! Key:{0} Second:{1} index:{2}", fileName, row[(int)VideoLocalizationDBIndex.StartSecond],  index.ToString());
                        }
                    }

                    isSuccess &= double.TryParse(row[(int) VideoLocalizationDBIndex.StartSecond], out double startSecond);
                    isSuccess &= double.TryParse(row[(int) VideoLocalizationDBIndex.EndSecond], out double endSecond);

                    if (isSuccess)
                    {
                        if (!startSecondList.Contains(startSecond))
                        {
                            startSecondList.Add(startSecond);
                            videoSubTitleData.Add(new VideoLocalizationData(startSecond, endSecond, text));
                        }
                        else
                        {
                            Debug.LogErrorFormat("VideoLocalizationDB: Duplicate timestamp !! Video:{0} Time:{1}", key.ToUpper(), startSecond);
                        }
                    }
                    
                }

                if (!_dict.ContainsKey(key))
                {
                    _dict.Add(key.ToUpper(), new VideoLocalizationDBData(key.ToUpper(), videoSubTitleData));
                }
                else
                {
                    Debug.LogErrorFormat("VideoLocalizationDB: Duplicate key !! {0}", key.ToUpper());
                }

            }
        }

        _isLoad = true;
    }

    #endregion
}
