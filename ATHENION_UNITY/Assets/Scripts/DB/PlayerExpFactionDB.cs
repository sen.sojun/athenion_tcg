﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerExpFactionDBData : DBData
{
    #region Public Properties
    public int Level { get; private set; }
    public string LevelStr { get { return Level.ToString(); } }
    public int AccumulativeExp { get; private set; }
    #endregion

    #region Constructors
    public PlayerExpFactionDBData()
    {
        Level = 0;
        AccumulativeExp = 0;
    }

    public PlayerExpFactionDBData(string level, int accumulativeExp)
    {
        Level = int.Parse(level);
        AccumulativeExp = accumulativeExp;
    }

    public PlayerExpFactionDBData(PlayerExpFactionDBData data)
    {
        Level = data.Level;
        AccumulativeExp = data.AccumulativeExp;
    }
    #endregion
}

public class PlayerExpFactionDB : DictDB<PlayerExpFactionDB, PlayerExpFactionDBData>
{
    #region Methods
    public override void LoadFromJSON(string jsonStrData)
    {
        _dict = new Dictionary<string, PlayerExpFactionDBData>();
        Dictionary<string, int> dataList = JsonConvert.DeserializeObject<Dictionary<string, int>>(jsonStrData);
        foreach (KeyValuePair<string, int> item in dataList)
        {
            PlayerExpFactionDBData data = new PlayerExpFactionDBData(item.Key, item.Value);
            _dict.Add(item.Key, data);
        }

        _isLoad = true;
    }
    #endregion
}