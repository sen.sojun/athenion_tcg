﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Karamucho;

public class CardAbilityDBData : DBData
{
    #region Public Properties
    public string ID { get; private set; }
    public string Trigger { get; private set; }
    public List<string> ConditionList { get; private set; }
    public List<string> EffectList { get; private set; }
    public List<List<string>> VFXParams { get; private set; }
    #endregion

    #region Constructors
    public CardAbilityDBData()
    {
        ID = "";
        Trigger = "";
        ConditionList = new List<string>();
        EffectList = new List<string>();
        VFXParams = new List<List<string>>();
    }

    public CardAbilityDBData(string id, string trigger, List<string> conditionList, List<string> effectList, List<List<string>> vfxParams)
    {
        ID = id;
        Trigger = trigger;
        ConditionList = new List<string>(conditionList);
        EffectList = new List<string>(effectList);
        VFXParams = new List<List<string>>(vfxParams);
    }

    public CardAbilityDBData(CardAbilityDBData data)
    {
        ID = data.ID;
        Trigger = data.Trigger;
        ConditionList = new List<string>(data.ConditionList);
        EffectList = new List<string>(data.EffectList);
        VFXParams = new List<List<string>>(data.VFXParams);
    }
    #endregion

    #region Methods
    public override string GetDebugText()
    {
        string conditions = "";
        foreach (string condition in ConditionList)
        {
            if (conditions != string.Empty)
            {
                conditions += "| ";
            }
            conditions += condition;
        }

        string effects = "";
        foreach (string effect in EffectList)
        {
            if (effects != string.Empty)
            {
                effects += "| ";
            }
            effects += effect;
        }

        string vfxs = "";
        for (int i = 0; i < VFXParams.Count; ++i)
        {
            string vfxText = "";
            for (int j = 0; j < VFXParams[i].Count; ++j)
            {
                string t = VFXParams[i][j];
                if (j == 0)
                {
                    vfxText += t;
                }
                else
                {
                    vfxText += ", " + t;
                }
            }

            vfxs += string.Format("\n[{0}]:{1}", i, vfxText);
        }

        int index = 0;
        string result = string.Format(
                "ID: {" + index++ + "}"
              + "\nTrigger: {" + index++ + "}"
              + "\nConditions: {" + index++ + "}"
              + "\nEffects: {" + index++ + "}"
              + "\nVFXParams: {" + index++ + "}"
            , ID
            , Trigger
            , conditions
            , effects
            , vfxs
        );

        return result;
    }
    #endregion
}

public class CardAbilityDB : DictDB<CardAbilityDB, CardAbilityDBData>
{
    #region Enums
    public enum AbilityDBIndex
    {
        AbilityID = 0
        , Description
        , Trigger
        , ConditionList
        , EffectList
        , VFXParam_0
        , VFXParam_1
        , VFXParam_2
        , VFXParam_3
    }
    #endregion

    #region Protected Properties
    private static string _rootPath = "Databases/";
    private static string[] _fileNames = {
          "KaramuchoDB - CardAbilityDB"
        , "KaramuchoDB - CardAbilityDB_Old"
    };
    #endregion

    #region Methods

    public void LoadOverrideDB(string csvText)
    {
        csvText = csvText.Replace("\\r", "");
        csvText = csvText.Replace("\\n", "\n");
        List<List<string>> rawDataList;
        bool isSuccess = CSVReader.ReadCSV(csvText, out rawDataList);
        if (isSuccess)
        {
            foreach (List<string> row in rawDataList)
            {
                try
                {
                    CardAbilityDBData data = CreateAbilityDBDataFromCSVByRow(row);
                    if (_dict.ContainsKey(data.ID))
                    {
                        _dict[data.ID] = data;
                    }
                    else
                    {
                        _dict.Add(data.ID, data);
                    }
                }
                catch (System.Exception e)
                {
                    Debug.LogFormat("CardAbilityDB: LoadOverrideDB Failed. {0}", e.ToString());
                    continue;
                }
            }
        }
    }

    public override void LoadDB()
    {
        _dict = new Dictionary<string, CardAbilityDBData>();

        List<List<string>> rawDataList;
        string filePath = "";
        foreach (string fileName in _fileNames)
        {
            filePath = _rootPath + fileName;
            TextAsset csvText = ResourceManager.Load<TextAsset>(filePath);

            bool isSuccess = CSVReader.ReadCSV(csvText, out rawDataList);
            if (isSuccess)
            {
                foreach (List<string> row in rawDataList)
                {
                    CardAbilityDBData data = CreateAbilityDBDataFromCSVByRow(row);
                    _dict.Add(data.ID, data);
                }
                _isLoad = true;
            }
        }
        if (_isLoad) return;

        Debug.LogWarning("CardAbilityDB: Load Failed.");
        _isLoad = false;
    }

    private static CardAbilityDBData CreateAbilityDBDataFromCSVByRow(List<string> row)
    {
        List<string> conditionList = new List<string>(GameHelper.SplitParam(row[(int)AbilityDBIndex.ConditionList]));
        List<string> effectList = new List<string>(GameHelper.SplitParam(row[(int)AbilityDBIndex.EffectList]));

        List<List<string>> vfxParams = new List<List<string>>();
        for (int index = (int)AbilityDBIndex.VFXParam_0; index <= (int)AbilityDBIndex.VFXParam_3; ++index)
        {
            List<string> param = new List<string>(GameHelper.SplitParam(row[index]));
            vfxParams.Add(param);
        }

        CardAbilityDBData data = new CardAbilityDBData(
              row[(int)AbilityDBIndex.AbilityID]
            , row[(int)AbilityDBIndex.Trigger]
            , conditionList
            , effectList
            , vfxParams
        );
        return data;
    }
    #endregion
}
