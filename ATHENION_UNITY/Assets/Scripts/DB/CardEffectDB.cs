﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Karamucho;

public class CardEffectDBData : DBData
{
    #region Public Properties
    public string ID { get; private set; }

    public string SpawnEffectKey { get; private set; }
    public string AttackEffectKey { get; private set; }
    public string DeathEffectKey { get; private set; }
    #endregion

    #region Constructors
    public CardEffectDBData()
    {
        ID = "";

        SpawnEffectKey = "";
        AttackEffectKey = "";
        DeathEffectKey = "";
    }

    public CardEffectDBData(string id, string spawnEffectKey, string attackEffectKey, string deathEffectKey)
    {
        ID = id;

        SpawnEffectKey = spawnEffectKey;
        AttackEffectKey = attackEffectKey;
        DeathEffectKey = deathEffectKey;
    }

    public CardEffectDBData(CardDBData data)
    {
        ID = data.ID;

        SpawnEffectKey = data.SpawnEffectKey;
        AttackEffectKey = data.AttackEffectKey;
        DeathEffectKey = data.DeathEffectKey;
    }
    #endregion

    #region Methods
    public override string GetDebugText()
    {
        int index = 0;
        string result = string.Format(
                "ID: {" + index++ + "}"
              + "\nSpawn Effect ID: {" + index++ + "}"
              + "\nAttack Effect ID: {" + index++ + "}"
              + "\nDeath Effect ID: {" + index++ + "}"
            , ID
            , SpawnEffectKey
            , AttackEffectKey
            , DeathEffectKey
        );

        return result;
    }
    #endregion
}

public class CardEffectDB : DictDB<CardEffectDB, CardEffectDBData>
{
    #region Enums
    public enum CardEffectDBIndex
    {
          CardEffectID = 0
        , SpawnEffectKey
        , AttackEffectKey
        , DeathEffectKey
    }
    #endregion

    #region Protected Properties
    private static string _rootPath = "Databases/";
    private static string[] _fileNames = {
          "KaramuchoDB - CardEffectDB"
    };
    #endregion

    #region Private Properties
    #endregion

    #region Constructors
    #endregion

    #region Methods
    public void LoadOverrideDB(string csvText)
    {
        List<List<string>> rawDataList;
        csvText = csvText.Replace("\\r", "");
        csvText = csvText.Replace("\\n", "\n");
        bool isSuccess = CSVReader.ReadCSV(csvText, out rawDataList);
        if (isSuccess)
        {
            foreach (List<string> row in rawDataList)
            {
                try
                {
                    CardEffectDBData data = CreateCardEffectDBDataByCSV(row);
                    if (_dict.ContainsKey(data.ID))
                    {
                        _dict[data.ID] = data;
                    }
                    else
                    {
                        _dict.Add(data.ID, data);
                    }
                }
                catch (System.Exception e)
                {
                    Debug.LogWarning(string.Format(
                        "CardEffectDB/LoadDB: Failed to load data. {0} Skip! ERROR: {1}"
                        , row[(int)CardEffectDBIndex.CardEffectID]
                        , e.ToString()
                    ));
                    continue;
                }
            }
        }
    }

    public override void LoadDB()
    {
        _dict = new Dictionary<string, CardEffectDBData>();

        List<List<string>> rawDataList;
        string filePath = "";
        foreach (string fileName in _fileNames)
        {
            filePath = _rootPath + fileName;
            TextAsset csvText = ResourceManager.Load<TextAsset>(filePath);

            bool isSuccess = CSVReader.ReadCSV(csvText, out rawDataList);
            if (isSuccess)
            {
                foreach (List<string> row in rawDataList)
                {
                    try
                    {
                        CardEffectDBData data = CreateCardEffectDBDataByCSV(row);
                        _dict.Add(data.ID, data);
                    }
                    catch (System.Exception e)
                    {
                        Debug.LogWarning(string.Format(
                              "CardEffectDB/LoadDB: Failed to load data. {0} Skip! ERROR: {1}"
                            , row[(int)CardEffectDBIndex.CardEffectID]
                            , e.ToString()
                        ));
                        continue;
                    }
                }

                //Debug.Log("CardEffectDB: Load Succeed.");
                _isLoad = true;
            }
        }
        if (_isLoad) return;

        Debug.LogWarning("CardEffectDB: Load Failed.");
        _isLoad = false;
    }

    private CardEffectDBData CreateCardEffectDBDataByCSV(List<string> row)
    {
        CardEffectDBData data = new CardEffectDBData(
              row[(int)CardEffectDBIndex.CardEffectID]
            , row[(int)CardEffectDBIndex.SpawnEffectKey]
            , row[(int)CardEffectDBIndex.AttackEffectKey]
            , row[(int)CardEffectDBIndex.DeathEffectKey]
        );
        return data;
    }
    #endregion
}
