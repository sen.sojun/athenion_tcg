﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeasonPassDBData : DBData
{
    #region Public Properties
    public int SeasonPassIndex { get { return _seasonPassIndex; } }
    public DateTime StartDateTime { get { return _startDateTime; } }
    public DateTime EndDateTime { get { return _endDateTime; } }
    #endregion

    #region Private Properties
    private int _seasonPassIndex = 0;
    private DateTime _startDateTime = DateTime.MaxValue;
    private DateTime _endDateTime = DateTime.MaxValue;
    #endregion

    #region Contructors
    public SeasonPassDBData(string seasonPassIndex, string seasonPassData)
    {
        _seasonPassIndex = int.Parse(seasonPassIndex);
        Dictionary<string, DateTime> data = JsonConvert.DeserializeObject<Dictionary<string, DateTime>>(seasonPassData);
        _startDateTime = data["start_date"];
        _endDateTime = data["end_date"];
    }

    public SeasonPassDBData(SeasonPassDBData data)
    {
        _seasonPassIndex = data._seasonPassIndex;
        _startDateTime = data._startDateTime;
        _endDateTime = data._endDateTime;
    }
    #endregion
}

public class SeasonPassDB : DictDB<SeasonPassDB, SeasonPassDBData>
{
    #region Methods
    public override void LoadFromJSON(string jsonStrData)
    {
        _dict = new Dictionary<string, SeasonPassDBData>();
        Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonStrData);
        foreach (KeyValuePair<string, object> data in rawData)
        {
            SeasonPassDBData dbData = new SeasonPassDBData(data.Key, data.Value.ToString());
            _dict.Add(data.Key, dbData);
        }

        _isLoad = true;
    }

    public SeasonPassDBData GetCurrentSeasonPassDBData()
    {
        foreach (KeyValuePair<string, SeasonPassDBData> data in _dict)
        {
            if (GameHelper.IsCurrentUTCDateTimeValid(data.Value.StartDateTime, data.Value.EndDateTime))
            {
                return data.Value;
            }
        }

        return null;
    }
    #endregion
}
