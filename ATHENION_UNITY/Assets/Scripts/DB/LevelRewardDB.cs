﻿using Newtonsoft.Json;
using System.Collections.Generic;

public class LevelRewardDBData : DBData
{
    #region Public Properties
    public int Level { get { return _level; } }
    public string LevelStr { get { return _level.ToString(); } }
    public List<ItemData> RewardItemList { get { return _rewardItemList; } }
    #endregion

    #region Private Properties
    private int _level = -1;
    private List<ItemData> _rewardItemList = new List<ItemData>();
    #endregion

    #region Contructors
    public LevelRewardDBData(string level, string jsonStrData)
    {
        _level = int.Parse(level);
        Dictionary<string, Dictionary<string, int>> data = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, int>>>(jsonStrData);
        foreach(KeyValuePair<string, int> item in data["reward"])
        {
            _rewardItemList.Add(new ItemData(item));
        }
    }

    public LevelRewardDBData(LevelRewardDBData data)
    {
        _level = data._level;
        _rewardItemList = new List<ItemData>(data._rewardItemList);
    }
    #endregion
}

public class LevelRewardDB : DictDB<LevelRewardDB, LevelRewardDBData>
{
    #region Methods
    public override void LoadFromJSON(string jsonStrData)
    {
        _dict = new Dictionary<string, LevelRewardDBData>();
        Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonStrData);
        foreach (KeyValuePair<string, object> data in rawData)
        {
            LevelRewardDBData dbData = new LevelRewardDBData(data.Key, data.Value.ToString());
            _dict.Add(data.Key, dbData);
        }

        _isLoad = true;
    }
    #endregion
}