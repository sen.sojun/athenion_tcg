﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardPriceOverrideData
{
    #region Public Properties
    public string CardPriceBaseKey { get { return _cardPriceBaseKey; } }
    public CardPriceData CardPriceData { get { return _cardPriceData; } }
    public List<string> ItemIDList { get { return _itemIDList; } }
    #endregion

    #region Private Properties
    private string _cardPriceBaseKey = CatalogCardData.DefaultPriceKey;
    private CardPriceData _cardPriceData = new CardPriceData();
    private List<string> _itemIDList = new List<string>();

    private const string _cardDataPriceBaseKey = "price_base";
    private const string _cardDataPriceOverrideKey = "price_ovrr";
    private const string _cardDataListKey = "list";
    #endregion

    #region Contructors
    public CardPriceOverrideData(string rawData)
    {
        Dictionary<string, object> data = JsonConvert.DeserializeObject<Dictionary<string, object>>(rawData);
        if (data.ContainsKey(_cardDataPriceBaseKey))
        {
            _cardPriceBaseKey = (string)data[_cardDataPriceBaseKey];
        }
        if (data.ContainsKey(_cardDataPriceOverrideKey))
        {
            _cardPriceData = new CardPriceData(data[_cardDataPriceOverrideKey].ToString());
        }
        if (data.ContainsKey(_cardDataListKey))
        {
            _itemIDList = JsonConvert.DeserializeObject<List<string>>(data[_cardDataListKey].ToString());
        }
    }

    public CardPriceOverrideData(CardPriceOverrideData data)
    {
        _cardPriceBaseKey = data._cardPriceBaseKey;
        _cardPriceData = new CardPriceData(data._cardPriceData);
        _itemIDList = new List<string>(_itemIDList);
    }
    #endregion

    #region Methods
    public bool IsItemIDValid(string itemID)
    {
        return _itemIDList.Contains(itemID);
    }
    #endregion
}

public class CardPriceOverrideDBData : DBData
{
    #region Public Properties
    public string ID { get { return _id; } }
    public DateTime StartDateTime { get { return _startDateTime; } }
    public DateTime EndDateTime { get { return _endDateTime; } }
    public bool IsLimited { get { return _isLimited; } }
    #endregion

    #region Private Properties
    private string _id = "";
    private DateTime _startDateTime = DateTime.MinValue;
    private DateTime _endDateTime = DateTime.MaxValue;
    private bool _isLimited = false;
    private List<CardPriceOverrideData> _cardPriceOverrideDataList = new List<CardPriceOverrideData>();

    private const string _startDateKey = "start_date";
    private const string _endDateKey = "end_date";
    private const string _limitsKey = "limits";
    private const string _cardDataKey = "card_data";
    #endregion

    #region Contructors
    public CardPriceOverrideDBData()
    {
    }

    public CardPriceOverrideDBData(string id, string jsonStrData)
    {
        _id = id;
        Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonStrData);
        if (rawData.ContainsKey(_startDateKey))
        {
            _startDateTime = (DateTime)rawData[_startDateKey];
        }
        if (rawData.ContainsKey(_endDateKey))
        {
            _endDateTime = (DateTime)rawData[_endDateKey];
        }

        if (rawData.ContainsKey(_limitsKey))
        {
            _isLimited = true;
        }

        if (rawData.ContainsKey(_cardDataKey))
        {
            List<object> cardDataList = JsonConvert.DeserializeObject<List<object>>(rawData[_cardDataKey].ToString());
            foreach (object item in cardDataList)
            {
                _cardPriceOverrideDataList.Add(new CardPriceOverrideData(item.ToString()));
            }
        }
    }

    public CardPriceOverrideDBData(CardPriceOverrideDBData data)
    {
        _id = data._id;
        _startDateTime = data._startDateTime;
        _endDateTime = data._endDateTime;
        _isLimited = data._isLimited;
        _cardPriceOverrideDataList = new List<CardPriceOverrideData>(data._cardPriceOverrideDataList);
    }
    #endregion

    #region Methods
    public CardPriceOverrideData GetCardPriceOverrideDataByItemID(string itemID)
    {
        foreach(CardPriceOverrideData data in _cardPriceOverrideDataList)
        {
            if(data.IsItemIDValid(itemID))
            {
                return data;
            }
        }

        return null;
    }

    public bool IsDateTimeValid()
    {
        return GameHelper.IsCurrentUTCDateTimeValid(_startDateTime, _endDateTime);
    }
    #endregion
}

public class CardPriceOverrideDB : DictDB<CardPriceOverrideDB, CardPriceOverrideDBData>
{
    #region Methods
    public override void LoadFromJSON(string jsonStrData)
    {
        _dict = new Dictionary<string, CardPriceOverrideDBData>();
        Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonStrData);
        foreach (KeyValuePair<string, object> data in rawData)
        {
            _dict.Add(data.Key, new CardPriceOverrideDBData(data.Key, data.Value.ToString()));
        }

        _isLoad = true;
    }
    #endregion
}