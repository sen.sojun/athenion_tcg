﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Karamucho;

public class AvatarDBData : DBData
{
    #region Public Properties
    public string AvatarID { get; private set; }
    public string AvatarName { get; private set; }
    public bool IsListing { get; private set; }
    public string Description { get; private set; }
    #endregion

    #region Constructors
    public AvatarDBData()
    {
        AvatarID = "";
        AvatarName = "";
        IsListing = false;
        Description = "";
    }

    public AvatarDBData(string avatarID, string avatarName, bool isListing, string description)
    {
        AvatarID = avatarID;
        AvatarName = avatarName;
        IsListing = isListing;
        Description = description;
    }

    public AvatarDBData(AvatarDBData data)
    {
        AvatarID = data.AvatarID;
        AvatarName = data.AvatarName;
        IsListing = data.IsListing;
        Description = data.Description;
    }
    #endregion

    #region Methods
    public override string GetDebugText()
    {
        int index = 0;
        string result = string.Format(
                "AvatarID: {" + index++ + "}"
              + "\nAvatar Name: {" + index++ + "}"
              + "\nIsListing: {" + index++ + "}"
              + "\nDescription: {" + index++ + "}"
            , AvatarID
            , AvatarName
            , IsListing
            , Description
        );

        return result;
    }
    #endregion
}

public class AvatarDB : DictDB<AvatarDB, AvatarDBData>
{
    #region Enums
    public enum AvatarDBIndex
    {
          AvatarID = 0
        , AvatarName
        , IsListing
        , Description
    }
    #endregion

    #region Protected Properties
    protected string _fileName = "Databases/KaramuchoDB - AvatarDB";
    #endregion

    #region Private Properties
    #endregion

    #region Constructors
    #endregion

    #region Methods
    public override void LoadDB()
    {
        _dict = new Dictionary<string, AvatarDBData>();

        List<List<string>> rawDataList;
        TextAsset csvText = ResourceManager.Load<TextAsset>(_fileName);

        bool isSuccess = CSVReader.ReadCSV(csvText, out rawDataList);
        if (isSuccess)
        {
            bool isListing = false;

            foreach (List<string> row in rawDataList)
            {
                isListing = bool.Parse(row[(int)AvatarDBIndex.IsListing]);

                AvatarDBData data = new AvatarDBData(
                      row[(int)AvatarDBIndex.AvatarID]
                    , row[(int)AvatarDBIndex.AvatarName]
                    , isListing
                    , row[(int)AvatarDBIndex.Description]
                );
                _dict.Add(data.AvatarID, data);
            }

            //Debug.Log("AvatarDB: Load Succeed.");
            _isLoad = true;
            return;
        }

        Debug.LogWarning("AvatarDB: Load Failed.");
        _isLoad = false;
    }
    #endregion
}
