﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Karamucho;

public class ImageDBData : DBData
{
    #region Public Properties
    public string ID { get; private set; }
    public string ArtistName_EN { get; private set; }
    public string ArtistName_TH { get; private set; }
    #endregion

    #region Constructors
    public ImageDBData()
    {
        ID = "";
        ArtistName_EN = "";
        ArtistName_TH = "";
    }

    public ImageDBData(string id, string artistName_en, string artistName_th)
    {
        ID = id;
        ArtistName_EN = artistName_en;
        ArtistName_TH = artistName_th;
    }

    public ImageDBData(ImageDBData data)
    {
        ID = data.ID;
        ArtistName_EN = data.ArtistName_EN;
        ArtistName_TH = data.ArtistName_TH;
    }
    #endregion

    #region Methods
    public override string GetDebugText()
    {
        int index = 0;
        string result = string.Format(
                "ID: {" + index++ + "}"
              + "\nArtistName EN: {" + index++ + "}"
              + "\nArtistName TH: {" + index++ + "}"
            , ID
            , ArtistName_EN
            , ArtistName_TH
        );

        return result;
    }
    #endregion
}

public class ImageDB : DictDB<ImageDB, ImageDBData>
{
    #region Enums
    public enum ImageDBIndex
    {
          ImageID = 0
        , ArtistName_EN
        , ArtistName_TH
    }
    #endregion

    #region Protected Properties
    protected string _fileName = "Databases/KaramuchoDB - ImageDB";
    #endregion

    #region Private Properties
    #endregion

    #region Constructors
    #endregion

    #region Methods
    public override void LoadDB()
    {
        _dict = new Dictionary<string, ImageDBData>();

        List<List<string>> rawDataList;
        TextAsset csvText = ResourceManager.Load<TextAsset>(_fileName);

        bool isSuccess = CSVReader.ReadCSV(csvText, out rawDataList);
        if (isSuccess)
        {
            foreach (List<string> row in rawDataList)
            {
                if (isSuccess)
                {
                    ImageDBData data = new ImageDBData(
                          row[(int)ImageDBIndex.ImageID]
                        , row[(int)ImageDBIndex.ArtistName_EN]
                        , row[(int)ImageDBIndex.ArtistName_TH]
                    );
                    _dict.Add(data.ID, data);

                    //Debug.Log(data.GetDebugText());
                }
                else
                {
                    Debug.LogWarning(string.Format(
                        "ImageDB/LoadDB: Failed to load data. {0} Skip!"
                        , row[(int)ImageDBIndex.ImageID]
                    ));
                }
            }
            //Debug.Log("ImageDB: Load Succeed.");
            _isLoad = true;
            return;
        }

        Debug.LogWarning("ImageDB: Load Failed.");
        _isLoad = false;
    }
    #endregion
}
