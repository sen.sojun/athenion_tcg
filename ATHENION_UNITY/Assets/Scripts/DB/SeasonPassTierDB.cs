﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeasonPassTierDBData : DBData
{
    #region Public Properties
    public int TierLevel { get { return _tierLevel; } }
    public string TierLevelStr { get { return _tierLevel.ToString(); } }
    public int TierAccScore { get { return _tierAccScore; } }
    #endregion

    #region Private Properties
    private int _tierLevel = 0;
    private int _tierAccScore = 0;
    #endregion

    #region Contructors
    public SeasonPassTierDBData(string tierLevelStr, int tierAccScore)
    {
        _tierLevel = int.Parse(tierLevelStr);
        _tierAccScore = tierAccScore;
    }

    public SeasonPassTierDBData(SeasonPassTierDBData data)
    {
        _tierLevel = data._tierLevel;
        _tierAccScore = data._tierAccScore;
    }
    #endregion
}

public class SeasonPassTierDB : DictDB<SeasonPassTierDB, SeasonPassTierDBData>
{
    #region Methods
    public override void LoadFromJSON(string jsonStrData)
    {
        _dict = new Dictionary<string, SeasonPassTierDBData>();
        Dictionary<string, int> rawData = JsonConvert.DeserializeObject<Dictionary<string, int>>(jsonStrData);
        foreach (KeyValuePair<string, int> data in rawData)
        {
            SeasonPassTierDBData dbData = new SeasonPassTierDBData(data.Key, data.Value);
            _dict.Add(data.Key, dbData);
        }

        _isLoad = true;
    }
    #endregion
}
