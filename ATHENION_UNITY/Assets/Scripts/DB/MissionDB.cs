﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissionDBData : DBData
{
    #region Public Properties
    public string ID { get; private set; }
    public List<PlayerStatDetailData> DetailDataList { get { return _detailDataList; } }
    public List<ItemData> ItemDataList { get { return _itemDataList; } }
    public int MaxProgress { get { return GetMaxProgress(); } }
    public string FunctionName { get { return _functionName; } }
    public string FunctionParam { get { return _functionParam; } }
    public List<string> RequireMissionIDList { get { return _reqMissionList; } }
    #endregion

    #region Private Properties
    private const string _conditionKey = "condition";
    private const string _condition_detailKey = "detail";
    private const string _condition_rewardKey = "reward";

    private const string _functionKey = "go_func";
    private const string _function_nameKey = "name";
    private const string _function_paramKey = "param";

    private const string _requireMissionsKey = "req_missions";

    private List<PlayerStatDetailData> _detailDataList = new List<PlayerStatDetailData>();
    private List<ItemData> _itemDataList = new List<ItemData>();
    private string _functionName = string.Empty;
    private string _functionParam = string.Empty;
    private List<string> _reqMissionList = new List<string>();
    #endregion

    #region Contructors
    public MissionDBData()
    {
    }

    public MissionDBData(string id, string jsonStrData)
    {
        ID = id;
        Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonStrData);

        // set condition
        if(rawData.ContainsKey(_conditionKey))
        {
            Dictionary<string, Dictionary<string, int>> conditionData = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, int>>>(rawData[_conditionKey].ToString());
            if (conditionData.ContainsKey(_condition_detailKey))
            {
                SetupDetailData(conditionData[_condition_detailKey]);
            }
            if (conditionData.ContainsKey(_condition_rewardKey))
            {
                SetupRewardData(conditionData[_condition_rewardKey]);
            }
        }

        // set function
        if(rawData.ContainsKey(_functionKey))
        {
            Dictionary<string, string> functionData = JsonConvert.DeserializeObject<Dictionary<string, string>>(rawData[_functionKey].ToString());
            if(functionData.ContainsKey(_function_nameKey))
            {
                _functionName = functionData[_function_nameKey];
            }
            if (functionData.ContainsKey(_function_paramKey))
            {
                _functionParam = functionData[_function_paramKey];
            }
        }

        // set require mission list
        if (rawData.ContainsKey(_requireMissionsKey))
        {
            _reqMissionList = JsonConvert.DeserializeObject<List<string>>(rawData[_requireMissionsKey].ToString());
        }
    }

    public MissionDBData(MissionDBData data)
    {
        ID = data.ID;
        _detailDataList = new List<PlayerStatDetailData>(data._detailDataList);
        _itemDataList = new List<ItemData>(data._itemDataList);
        _functionName = data._functionName;
        _functionParam = data._functionParam;
        _reqMissionList = new List<string>(data._reqMissionList);
    }
    #endregion

    #region Methods
    private void SetupDetailData(Dictionary<string, int> dictData)
    {
        foreach (KeyValuePair<string, int> detail in dictData)
        {
            _detailDataList.Add(new PlayerStatDetailData(detail));
        }
    }

    private void SetupRewardData(Dictionary<string, int> dictData)
    {
        foreach (KeyValuePair<string, int> item in dictData)
        {
            _itemDataList.Add(new ItemData(item));
        }
    }

    private int GetMaxProgress()
    {
        int reqCount = 0;
        foreach (PlayerStatDetailData data in _detailDataList)
        {
            reqCount += data.Amount;
        }

        return reqCount;
    }
    #endregion
}

public class MissionDB : DictDB<MissionDB, MissionDBData>
{
    #region Methods
    public override void LoadFromJSON(string jsonStrData)
    {
        _dict = new Dictionary<string, MissionDBData>();
        Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonStrData);
        foreach (KeyValuePair<string, object> data in rawData)
        {
            MissionDBData dbData = new MissionDBData(data.Key, data.Value.ToString());
            _dict.Add(data.Key, dbData);
        }

        _isLoad = true;
    }
    #endregion
}