﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Karamucho;
using Newtonsoft.Json;

public class DeckDBData : DBData
{
    #region Public Properties
    public string DeckID { get; private set; }
    public DeckData Deck { get; private set; }
    #endregion

    #region Constructors
    public DeckDBData()
    {
        DeckID = "";
        Deck = null;
    }

    public DeckDBData(string deckID, string deckJson)
    {
        DeckID = deckID;
        Deck = DeckData.JsonToDeckData(deckJson);
    }

    public DeckDBData(string deckID, DeckData deck)
    {
        DeckID = deckID;
        Deck = new DeckData(deck);
    }

    public DeckDBData(DeckDBData data)
    {
        DeckID = data.DeckID;
        Deck = new DeckData(data.Deck);
    }
    #endregion

    #region Methods
    public override string GetDebugText()
    {
        int index = 0;
        string result = string.Format(
                "DeckID: {" + index++ + "}"
              + "\nDeck: {" + index++ + "}"
            , DeckID
            , Deck.ToJson()
        );

        return result;
    }
    #endregion
}

public class DeckDB : DictDB<DeckDB, DeckDBData>
{
    #region Private Properties
    private const string _deckDefaultFormat = "DECK_DEFAULT_{0}";
    private const string _deckPracticeFormat = "DECK_PRACTICE_{0}_{1}_{2}";
    private const string _deckPresetPrefix = "DECK_PRESET_";
    #endregion

    #region Methods
    public override void LoadDB()
    {
    }

    public override void LoadFromJSON(string json)
    {
        _dict = new Dictionary<string, DeckDBData>();

        Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
        foreach (KeyValuePair<string, object> data in rawData)
        {
            DeckDBData dbData = new DeckDBData(data.Key, data.Value.ToString());
            _dict.Add(data.Key, dbData);
        }

        _isLoad = true;
    }

    public bool GetDeck(string deckID, out DeckData deckData)
    {
        deckData = new DeckData();

        if (IsLoad)
        {
            if (_dict.ContainsKey(deckID))
            {
                deckData = new DeckData(_dict[deckID].Deck);
                return true;
            }
        }

        return false;
    }

    public bool GetDeck(string deckID, out CardListData cardListData)
    {
        cardListData = new CardListData();

        if (IsLoad)
        {
            if (_dict.ContainsKey(deckID))
            {
                cardListData = new CardListData(_dict[deckID].Deck.Deck);
                return true;
            }
        }
        
        return false;
    }

    public bool GetDeck(string deckID, out List<string> cardList)
    {
        cardList = new List<string>();

        if (IsLoad)
        {
            if (IsLoad)
            {
                if (_dict.ContainsKey(deckID))
                {
                    foreach (KeyValuePair<string, int> item in _dict[deckID].Deck.Deck)
                    {
                        for (int count = 0; count < item.Value; ++count)
                        {
                            cardList.Add(item.Key);
                        }
                    }

                    return true;
                }
            }
        }
        return false;
    }

    public bool GetPresetDecks(out List<DeckData> deckData)
    {
        deckData = new List<DeckData>();
        if (IsLoad)
        {
            foreach(KeyValuePair<string, DeckDBData> item in _dict)
            {
                if(item.Key.Contains(_deckPresetPrefix))
                {
                    deckData.Add(item.Value.Deck);
                }
            }

            return true;
        }

        return false;
    }

    public bool GetDefaultDeck(CardElementType cardElement, out DeckData deckData)
    {
        string deckID = string.Format(_deckDefaultFormat, cardElement.ToString());

        return GetDeck(deckID, out deckData);
    }

    public bool GetDefaultDeck(CardElementType cardElement, out CardListData cardListData)
    {
        string deckID = string.Format(_deckDefaultFormat, cardElement.ToString());

        return GetDeck(deckID, out cardListData);
    }

    public bool GetPracticeDeck(string heroID, BotLevel difficulty, int index, out CardListData cardListData)
    {
        string deckID = string.Format(_deckPracticeFormat, heroID.ToUpper(), difficulty.ToString().ToUpper(), index);

        return GetDeck(deckID, out cardListData);
    }
    #endregion
}
