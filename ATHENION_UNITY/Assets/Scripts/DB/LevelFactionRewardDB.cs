﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelFactionRewardDBData : DBData
{
    #region Public Properties
    public CardElementType FactionType { get { return _factionType; } }
    #endregion

    #region Private Properties
    private CardElementType _factionType;
    private string _factionTypeStr = "None";
    private Dictionary<string, List<ItemData>> _itemDataList = new Dictionary<string, List<ItemData>>();
    #endregion

    #region Contructors
    public LevelFactionRewardDBData(string factionTypeStr, string jsonStrData)
    {
        // set faction type
        _factionTypeStr = factionTypeStr;
        GameHelper.StrToEnum(_factionTypeStr, out _factionType);

        Dictionary<string, Dictionary<string, Dictionary<string, int>>> unitData = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, Dictionary<string, int>>>>(jsonStrData);
        foreach (KeyValuePair<string, Dictionary<string, Dictionary<string, int>>> unit in unitData)
        {
            _itemDataList.Add(unit.Key, new List<ItemData>());
            foreach (KeyValuePair<string, int> item in unit.Value["reward"])
            {
                _itemDataList[unit.Key].Add(new ItemData(item));
            }
        }
    }

    public LevelFactionRewardDBData(LevelFactionRewardDBData data)
    {
        _factionTypeStr = data._factionTypeStr;
        _itemDataList = new Dictionary<string, List<ItemData>>(data._itemDataList);
    }
    #endregion

    #region Methods
    public List<ItemData> GetRewardItemList(int level)
    {
        string levelStr = level.ToString();
        if(_itemDataList.ContainsKey(levelStr))
        {
            return _itemDataList[levelStr];
        }

        return new List<ItemData>();
    }
    #endregion
}

public class LevelFactionRewardDB : DictDB<LevelFactionRewardDB, LevelFactionRewardDBData>
{
    #region Methods
    public override void LoadFromJSON(string jsonStrData)
    {
        _dict = new Dictionary<string, LevelFactionRewardDBData>();
        Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonStrData);
        foreach (KeyValuePair<string, object> data in rawData)
        {
            LevelFactionRewardDBData dbData = new LevelFactionRewardDBData(data.Key, data.Value.ToString());
            _dict.Add(data.Key, dbData);
        }

        _isLoad = true;
    }
    #endregion
}