﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public interface IDataDB
{
    #region Properties
    bool IsLoad { get; }
    #endregion

    #region Methods
    void LoadDB();
    #endregion
}

public abstract class DBData : System.ICloneable
{
    #region Methods
    public virtual object Clone()
    {
        var clone = (DBData)this.MemberwiseClone();
        return clone;
    }

    public virtual string GetDebugText()
    {
        return "";
    }
    #endregion
}

public abstract class DictDB<TClass, TData> : Singleton<TClass>, IDataDB where TClass : new() where TData : DBData 
{
    #region Public Properties
    public bool IsLoad { get { return _isLoad; } }
    #endregion

    #region Protected Properties
    protected bool _isLoad = false;
    protected Dictionary<string, TData> _dict = null;

    protected StringBuilder _sb;
    #endregion

    #region Methods
    public virtual void LoadDB()
    {
        Debug.Log(string.Format("[{0}] Load Failed.", typeof(TClass).ToString()));
        _isLoad = false;
    }

    public virtual void LoadFromJSON(string json)
    {
        Debug.Log(string.Format("[{0}] Load Failed.", typeof(TClass).ToString()));
        _isLoad = false;
    }

    public virtual bool GetData(string key, out TData data)
    {
        if (!IsLoad)
        {
            this.LoadDB();
        }

        return _dict.TryGetValue(key, out data);
    }

    public virtual bool GetAllData(out List<TData> dataList)
    {
        if (!IsLoad)
        {
            this.LoadDB();
        }

        dataList = new List<TData>();
        foreach (KeyValuePair<string, TData> data in _dict)
        {
            dataList.Add((TData)data.Value.Clone());
        }

        return true;
    }

    public virtual string GetDebugText()
    {
        if (_sb == null) _sb = new StringBuilder();
        _sb.Clear();

        foreach (KeyValuePair<string, TData> data in _dict)
        {
            if (_sb.Length > 0)
            {
                _sb.AppendFormat("\n\n{0}", data.Value.GetDebugText());
            }
            else
            {
                _sb.Append(data.Value.GetDebugText());
            }
        }

        return _sb.ToString();
    }
    #endregion
}

public abstract class ListDB<TClass, TData> : Singleton<TClass>, IDataDB where TClass : new() where TData : DBData
{
    #region Public Properties
    public bool IsLoad { get { return _isLoad; } }
    #endregion

    #region Protected Properties
    protected bool _isLoad = false;
    protected List<TData> _list = null;

    protected StringBuilder _sb;
    #endregion

    #region Methods
    public virtual void LoadDB()
    {
        Debug.Log(string.Format("[ListDB] Load Failed {0}.", typeof(TClass).ToString()));
        _isLoad = false;
    }

    public virtual void LoadFromJSON(string json)
    {
        Debug.Log(string.Format("[{0}] Load Failed.", typeof(TClass).ToString()));
        _isLoad = false;
    }

    public virtual bool GetData(int index, out TData data)
    {
        if (!IsLoad)
        {
            LoadDB();
        }

        if (IsLoad)
        {
            if (index >= 0 && index < _list.Count)
            {
                data = (TData)_list[index].Clone();
                return true;
            }
        }

        data = default(TData);
        return false;
    }

    public virtual bool GetAllData(out List<TData> dataList)
    {
        if (!IsLoad)
        {
            LoadDB();
        }

        if (IsLoad)
        {
            dataList = new List<TData>();
            foreach (TData data in _list)
            {
                dataList.Add((TData)data.Clone());
            }

            return true;
        }

        dataList = null;
        return false;
    }

    public virtual string GetDebugText()
    {
        if (_sb == null) _sb = new StringBuilder();
        _sb.Clear();

        foreach (TData data in _list)
        {
            if (_sb.Length > 0)
            {
                _sb.AppendFormat("\n\n{0}", data.GetDebugText());
            }
            else
            {
                _sb.Append(data.GetDebugText());
            }
        }

        return _sb.ToString();
    }
    #endregion
}
