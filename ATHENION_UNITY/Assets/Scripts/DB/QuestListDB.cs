﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using System;

public class EventQuestListDBData
{
    #region Private Properties
    private Dictionary<string, List<QuestDBData>> _dictData = new Dictionary<string, List<QuestDBData>>();
    #endregion

    #region Contructors
    public EventQuestListDBData()
    {
    }

    public EventQuestListDBData(string rawData)
    {
        SetData(rawData);
    }
    #endregion

    #region Methods
    private void SetData(string rawData)
    {
        Dictionary<string, List<string>> dictData = JsonConvert.DeserializeObject<Dictionary<string, List<string>>>(rawData);
        foreach(KeyValuePair<string, List<string>> item in dictData)
        {
            _dictData.Add(item.Key, new List<QuestDBData>());
            foreach(string id in item.Value)
            {
                if(QuestDB.Instance.GetData(id, out QuestDBData data))
                {
                    _dictData[item.Key].Add(data);
                }
            }
        }
    }

    public List<QuestDBData> GetQuestDBDataList(string eventQuestID)
    {
        List<QuestDBData> result = new List<QuestDBData>();
        if(_dictData.ContainsKey(eventQuestID))
        {
            result = new List<QuestDBData>(_dictData[eventQuestID]);
        }

        return result;
    }
    #endregion
}

#region SeasonPassQuestList
public enum SeasonPassQuestRefreshTypes
{
    none = 0
    , daily
    , weekly
}

public class SeasonPassQuestList
{
    #region Public Properties
    public string ID { get; private set; }
    public List<QuestDBData> QuestList { get { return _questList; } }
    public SeasonPassQuestRefreshTypes RefreshType { get; private set; }
    public DateTime StartDateTimeUTC { get; private set; }
    public DateTime EndDateTimeUTC { get; private set; }
    #endregion

    #region Private Properties
    private string _rawData = string.Empty;
    private List<QuestDBData> _questList = new List<QuestDBData>();

    private const string _questListKey = "list";
    private const string _refreshTypeKey = "refresh_type";
    private const string _validDateKey = "valid_date";
    private const string _validDateStartKey = "start_date";
    private const string _validDateEndKey = "end_date";
    #endregion

    #region Contructors
    public SeasonPassQuestList()
    {
    }

    public SeasonPassQuestList(string id, string jsonStrData)
    {
        ID = id;
        _rawData = jsonStrData;

        SetData(_rawData);
    }

    public SeasonPassQuestList(SeasonPassQuestList data)
    {
        ID = data.ID;
        _rawData = data._rawData;

        SetData(_rawData);
    }
    #endregion

    #region Methods
    private void SetData(string jsonStrData)
    {
        Dictionary<string, object> rawDict = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonStrData);
        if (rawDict.ContainsKey(_questListKey))
        {
            List<string> questIDList = JsonConvert.DeserializeObject<List<string>>(rawDict[_questListKey].ToString());
            foreach (string id in questIDList)
            {
                if (QuestDB.Instance.GetData(id, out QuestDBData data))
                {
                    _questList.Add(data);
                }
            }
        }

        if (rawDict.ContainsKey(_refreshTypeKey))
        {
            GameHelper.StrToEnum(rawDict[_refreshTypeKey].ToString(), out SeasonPassQuestRefreshTypes refreshType);
            RefreshType = refreshType;
        }

        StartDateTimeUTC = DateTime.MinValue;
        EndDateTimeUTC = DateTime.MaxValue;
        if (rawDict.ContainsKey(_validDateKey))
        {
            Dictionary<string, DateTime> validDateTime = JsonConvert.DeserializeObject<Dictionary<string, DateTime>>(rawDict[_validDateKey].ToString());
            if (validDateTime.ContainsKey(_validDateStartKey))
            {
                StartDateTimeUTC = validDateTime[_validDateStartKey];
            }
            if (validDateTime.ContainsKey(_validDateEndKey))
            {
                EndDateTimeUTC = validDateTime[_validDateEndKey];
            }
        }
    }

    public List<QuestData> GetQuestDataList()
    {
        List<QuestData> result = new List<QuestData>();
        Dictionary<string, int> currentStatStamp = DataManager.Instance.PlayerInfo.GetPlayerStatData();
        foreach(QuestDBData data in _questList)
        {
            result.Add(new QuestData(data.ID, QuestTypes.SeasonPassMainWeek, currentStatStamp, false));
        }

        return result;
    }
    #endregion
}

public class SeasonPassQuestListDBData
{
    #region Private Properties
    private const string _questMainDailyKey = "main_daily";
    private const string _questMainWeeklyKey = "main_weekly";
    private const string _questMainWeekKey = "main_week";
    private const string _questChallengeKey = "challenge";

    private Dictionary<string, Dictionary<string, SeasonPassQuestList>> _questListData = new Dictionary<string, Dictionary<string, SeasonPassQuestList>>();
    #endregion

    #region Contructors
    public SeasonPassQuestListDBData()
    {
    }

    public SeasonPassQuestListDBData(string jsonStrData)
    {
        SetData(jsonStrData);
    }
    #endregion

    #region Methods
    private void SetData(string jsonStrData)
    {
        Dictionary<string, object> dictData = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonStrData);
        foreach (KeyValuePair<string, object> item in dictData)
        {
            _questListData.Add(item.Key, new Dictionary<string, SeasonPassQuestList>());
            Dictionary<string, object> seasonPassQuests = JsonConvert.DeserializeObject<Dictionary<string, object>>(item.Value.ToString());
            foreach (KeyValuePair<string, object> quests in seasonPassQuests)
            {
                _questListData[item.Key].Add(quests.Key, new SeasonPassQuestList(quests.Key, quests.Value.ToString()));
            }
        }
    }

    public SeasonPassQuestList GetSeasonPassQuestListData_Daily(int seasonPassIndex)
    {
        SeasonPassQuestList result = new SeasonPassQuestList();
        string seasonPassIndexStr = seasonPassIndex.ToString();
        if (_questListData.ContainsKey(seasonPassIndexStr))
        {
            if(_questListData[seasonPassIndexStr].ContainsKey(_questMainDailyKey))
            {
                result = new SeasonPassQuestList(_questListData[seasonPassIndexStr][_questMainDailyKey]);
            }
        }
        return result;
    }

    public SeasonPassQuestList GetSeasonPassQuestListData_Weekly(int seasonPassIndex)
    {
        SeasonPassQuestList result = new SeasonPassQuestList();
        string seasonPassIndexStr = seasonPassIndex.ToString();
        if (_questListData.ContainsKey(seasonPassIndexStr))
        {
            if (_questListData[seasonPassIndexStr].ContainsKey(_questMainWeeklyKey))
            {
                result = new SeasonPassQuestList(_questListData[seasonPassIndexStr][_questMainWeeklyKey]);
            }
        }
        return result;
    }

    public List<SeasonPassQuestList> GetSeasonPassQuestListData_AllWeek(int seasonPassIndex)
    {
        List<SeasonPassQuestList> result = new List<SeasonPassQuestList>();
        string seasonPassIndexStr = seasonPassIndex.ToString();
        string weekQuestKey = _questMainWeekKey;
        if (_questListData.ContainsKey(seasonPassIndexStr))
        {
            int weekIndex = 0;
            while(true)
            {
                weekIndex++;
                weekQuestKey = _questMainWeekKey + "_" + weekIndex;
                if (_questListData[seasonPassIndexStr].ContainsKey(weekQuestKey))
                {
                    result.Add(new SeasonPassQuestList(_questListData[seasonPassIndexStr][weekQuestKey]));
                }
                else
                {
                    break;
                }
            }
        }
        return result;
    }

    public SeasonPassQuestList GetSeasonPassQuestListData_Challenge(int seasonPassIndex)
    {
        SeasonPassQuestList result = new SeasonPassQuestList();
        string seasonPassIndexStr = seasonPassIndex.ToString();
        if (_questListData.ContainsKey(seasonPassIndexStr))
        {
            if (_questListData[seasonPassIndexStr].ContainsKey(_questChallengeKey))
            {
                result = new SeasonPassQuestList(_questListData[seasonPassIndexStr][_questChallengeKey]);
            }
        }
        return result;
    }
    #endregion
}
#endregion

public class QuestListDB : Singleton<QuestListDB>
{
    #region Public Properties
    public EventQuestListDBData EventQuestList { get { return _eventQuestList; } }
    public SeasonPassQuestListDBData SeasonPassQuestList { get { return _seasonPassQuestList; } }
    #endregion

    #region Private Properties
    private const string _eventQuestKey = "event";
    private const string _seasonPassQuestKey = "season_pass";

    private EventQuestListDBData _eventQuestList = new EventQuestListDBData();
    private SeasonPassQuestListDBData _seasonPassQuestList = new SeasonPassQuestListDBData();
    #endregion

    #region Methods
    public void SetData(string jsonStrData)
    {
        Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonStrData);
        foreach (KeyValuePair<string, object> data in rawData)
        {
            switch (data.Key)
            {
                case _eventQuestKey:
                {
                    _eventQuestList = new EventQuestListDBData(data.Value.ToString());
                }
                break;

                case _seasonPassQuestKey:
                {
                    _seasonPassQuestList = new SeasonPassQuestListDBData(data.Value.ToString());
                }
                break;
            }
        }
    }
    #endregion
}