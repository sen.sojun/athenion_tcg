﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Karamucho;

public class BotDBData : DBData
{
    #region Public Properties
    public string BotID { get; private set; }
    public string BotName { get; private set; }
    public string PlayfabID { get; private set; }
    #endregion

    #region Constructors
    public BotDBData()
    {
        BotID = "";
        BotName = "";
        PlayfabID = "";
    }

    public BotDBData(string botID, string botName, string playfabID)
    {
        BotID = botID;
        BotName = botName;
        PlayfabID = playfabID;
    }

    public BotDBData(BotDBData data)
    {
        BotID = data.BotID;
        BotName = data.BotName;
        PlayfabID = data.PlayfabID;
    }
    #endregion

    #region Methods
    public override string GetDebugText()
    {
        int index = 0;
        string result = string.Format(
                "BotID: {" + index++ + "}"
              + "\nBot Name: {" + index++ + "}"
            , BotID
            , BotName
        );

        return result;
    }
    #endregion
}

public class BotDB : DictDB<BotDB, BotDBData>
{
    #region Enums
    public enum BotDBIndex
    {
        BotID = 0
        , BotName
        , PlayfabID
    }
    #endregion

    #region Protected Properties
    protected string _fileName = "Databases/KaramuchoDB - BotDB";
    #endregion

    #region Private Properties
    #endregion

    #region Constructors
    #endregion

    #region Methods
    public override void LoadDB()
    {
        _dict = new Dictionary<string, BotDBData>();

        List<List<string>> rawDataList;
        TextAsset csvText = ResourceManager.Load<TextAsset>(_fileName);

        List<string> conditionList = new List<string>();
        List<string> effectList = new List<string>();

        bool isSuccess = CSVReader.ReadCSV(csvText, out rawDataList);
        if (isSuccess)
        {
            foreach (List<string> row in rawDataList)
            {
                BotDBData data = new BotDBData(
                      row[(int)BotDBIndex.BotID]
                    , row[(int)BotDBIndex.BotName]
                    , row[(int)BotDBIndex.PlayfabID]
                );
                _dict.Add(data.BotID, data);
            }

            //Debug.Log("DeckDB: Load Succeed.");
            _isLoad = true;
            return;
        }

        Debug.LogWarning("BotDB: Load Failed.");
        _isLoad = false;
    }
    #endregion
}
