﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Karamucho;
using Newtonsoft.Json;
using System;

public class EventBattleDBData : DBData
{
    public const int DEFAULT_HP = 30;

    #region Public Properties
    public string ProfileID { get; private set; }
    public string AvatarID { get; private set; }
    public int HP { get; private set; }
    public string TitleID { get; private set; }
    public DeckData Deck { get { return _deck; } }
    public DateTime StartDate { get; private set; }
    public DateTime EndDate { get; private set; }
    public BotLevel BotLevel { get; private set; }
    #endregion

    #region Private Properties
    private DeckData _deck;

    private const string _avatarIDKey = "avatar_id";
    private const string _hpKey = "hp";
    private const string _titleIDKey = "title_id";
    private const string _deckDataKey = "deck_data";
    private const string _deckStartDateKey = "start_date";
    private const string _deckEndDateKey = "end_date";
    private const string _botLevelKey = "bot_level";
    #endregion

    #region Constructors
    public EventBattleDBData()
    {
        ProfileID = "";
        AvatarID = "";
        HP = DEFAULT_HP;
        TitleID = "";
        _deck = new DeckData();
        StartDate = DateTime.MinValue;
        EndDate = DateTime.MaxValue;
    }

    public EventBattleDBData(string profileID, string jsonStrData)
    {
        Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonStrData);

        ProfileID = profileID;
        AvatarID = rawData[_avatarIDKey].ToString();
        HP = int.Parse(rawData[_hpKey].ToString());
        TitleID = rawData[_titleIDKey].ToString();
        _deck = DeckData.JsonToDeckData(rawData[_deckDataKey].ToString());
        BotLevel = (BotLevel)Enum.Parse(typeof(BotLevel), rawData[_botLevelKey].ToString(), true);
        StartDate = Convert.ToDateTime(rawData[_deckStartDateKey].ToString());
        EndDate = Convert.ToDateTime(rawData[_deckEndDateKey].ToString());
    }

    public EventBattleDBData(EventBattleDBData data)
    {
        ProfileID = data.ProfileID;
        AvatarID = data.AvatarID;
        HP = data.HP;
        TitleID = data.TitleID;
        _deck = new DeckData(data._deck);
        BotLevel = data.BotLevel;
        StartDate = data.StartDate;
        EndDate = data.EndDate;
    }
    #endregion

    #region Methods
    public override string GetDebugText()
    {
        int index = 0;
        string result = string.Format(
                "ProfileID: {" + index++ + "}"
              + "\nHeroID: {" + index++ + "}"
              + "\nDeckID: {" + index++ + "}"
              + "\nCardBackID: {" + index++ + "}"
              + "\nDockID: {" + index++ + "}"
              + "\nHP: {" + index++ + "}"
              + "\nAvatarID: {" + index++ + "}"
              + "\nTitleID: {" + index++ + "}"
              + "\nBotLevel: {" + index++ + "}"
              + "\nStartDate: {" + index++ + "}"
              + "\nEndDate: {" + index++ + "}"
            , ProfileID
            , _deck.HeroID
            , _deck.DeckID
            , _deck.CardBackID
            , _deck.HUDSkinID
            , HP.ToString()
            , AvatarID
            , TitleID
            , BotLevel
            , StartDate
            , EndDate
        );

        return result;
    }

    public bool IsValidDate(DateTime now)
    {
        return now > StartDate && now < EndDate;
    }
    #endregion
}

public class EventBattleDB : DictDB<EventBattleDB, EventBattleDBData>
{
    public bool IsVSBot { get; private set; }
    public bool DoUsePlayerDeck { get; private set; }
    public bool DoFilterElement { get; private set; }
    public List<string> FilterPlayerElementList { get; private set; }
    public bool DoAddCustomCardsToPlayerDeck { get; private set; }
    public int PlayerDecksAmount { get; private set; }

    #region Methods
    public override void LoadFromJSON(string JSONStr)
    {
        _dict = new Dictionary<string, EventBattleDBData>();

        Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(JSONStr);
        Dictionary<string, object> availableEventDecks = JsonConvert.DeserializeObject<Dictionary<string, object>>(rawData["available_decks"].ToString());
        foreach (KeyValuePair<string, object> deck in availableEventDecks)
        {
            EventBattleDBData dbData = new EventBattleDBData(deck.Key, deck.Value.ToString());
            _dict.Add(deck.Key, dbData);
        }

        IsVSBot = bool.Parse(rawData["is_vs_bot"].ToString());
        DoUsePlayerDeck = bool.Parse(rawData["do_use_player_deck"].ToString());
        DoAddCustomCardsToPlayerDeck = bool.Parse(rawData["do_add_custom_cards_to_player_deck"].ToString());
        PlayerDecksAmount = int.Parse(rawData["player_decks_amt"].ToString());
        DoFilterElement = bool.Parse(rawData["do_filter_element"].ToString());
        if (DoFilterElement)
        {
            FilterPlayerElementList = JsonConvert.DeserializeObject<List<string>>(rawData["filter_element_ids"].ToString());
        }

        _isLoad = true;
    }
    #endregion
}