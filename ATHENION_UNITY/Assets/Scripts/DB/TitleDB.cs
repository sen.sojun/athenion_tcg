﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Karamucho;

public class TitleDBData : DBData
{
    #region Public Properties
    public string TitleID { get; private set; }
    public string TitleName { get; private set; }
    public bool IsListing { get; private set; }
    public string Description { get; private set; }
    #endregion

    #region Constructors
    public TitleDBData()
    {
        TitleID = "";
        TitleName = "";
        IsListing = false;
        Description = "";
    }

    public TitleDBData(string avatarID, string avatarName, bool isListing, string description)
    {
        TitleID = avatarID;
        TitleName = avatarName;
        IsListing = isListing;
        Description = description;
    }

    public TitleDBData(TitleDBData data)
    {
        TitleID = data.TitleID;
        TitleName = data.TitleName;
        IsListing = data.IsListing;
        Description = data.Description;
    }
    #endregion

    #region Methods
    public override string GetDebugText()
    {
        int index = 0;
        string result = string.Format(
                "TitleID: {" + index++ + "}"
              + "\nTitle Name: {" + index++ + "}"
              + "\nIsListing: {" + index++ + "}"
              + "\nDescription: {" + index++ + "}"
            , TitleID
            , TitleName
            , IsListing
            , Description
        );

        return result;
    }
    #endregion
}

public class TitleDB : DictDB<TitleDB, TitleDBData>
{
    #region Enums
    public enum TitleDBIndex
    {
          TitleID = 0
        , TitleName
        , IsListing
        , Description
    }
    #endregion

    #region Protected Properties
    protected string _fileName = "Databases/KaramuchoDB - TitleDB";
    #endregion

    #region Private Properties
    #endregion

    #region Constructors
    #endregion

    #region Methods
    public override void LoadDB()
    {
        _dict = new Dictionary<string, TitleDBData>();

        List<List<string>> rawDataList;
        TextAsset csvText = ResourceManager.Load<TextAsset>(_fileName);

        bool isSuccess = CSVReader.ReadCSV(csvText, out rawDataList);
        if (isSuccess)
        {
            bool isListing = false;

            foreach (List<string> row in rawDataList)
            {
                isListing = bool.Parse(row[(int)TitleDBIndex.IsListing]);

                TitleDBData data = new TitleDBData(
                      row[(int)TitleDBIndex.TitleID]
                    , row[(int)TitleDBIndex.TitleName]
                    , isListing
                    , row[(int)TitleDBIndex.Description]
                );
                _dict.Add(data.TitleID, data);
            }

            //Debug.Log("TitleDB: Load Succeed.");
            _isLoad = true;
            return;
        }

        Debug.LogWarning("TitleDB: Load Failed.");
        _isLoad = false;
    }
    #endregion
}
