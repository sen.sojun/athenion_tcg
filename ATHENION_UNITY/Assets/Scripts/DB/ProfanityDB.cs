﻿using Karamucho;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProfanityDBData : DBData
{
    public List<string> Words = new List<string>();
}

public class ProfanityDB : ListDB<ProfanityDB, ProfanityDBData>
{
    #region Protected Properties
    protected string _fileName = "Databases/KaramuchoDB - ProfanityDB";
    #endregion

    #region Methods
    public override void LoadDB()
    {
        _list = new List<ProfanityDBData>();
        _list.Add(new ProfanityDBData());
        List<List<string>> rawDataList;
        TextAsset csvText = ResourceManager.Load<TextAsset>(_fileName);

        bool isSuccess = CSVReader.ReadCSV(csvText, out rawDataList);
        if (isSuccess)
        {
            foreach (List<string> row in rawDataList)
            {
                _list[0].Words.Add(row[0]);
            }

            _isLoad = true;
            return;
        }

        Debug.LogWarning("ProfanityDB: Load Failed.");
        _isLoad = false;
    }
    #endregion

    public List<string> GetProfanityWords()
    {
        if (!_isLoad)
            LoadDB();
        return _list[0].Words;
    }
}
