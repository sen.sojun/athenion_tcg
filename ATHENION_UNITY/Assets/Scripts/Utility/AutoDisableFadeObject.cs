﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDisableFadeObject : MonoBehaviour
{
    [SerializeField]
    private float _Time;

    private void OnEnable()
    {
        StartCoroutine(AutoHide());
    }

    IEnumerator AutoHide()
    {
        yield return new WaitForSeconds(_Time);
        GameHelper.UITransition_FadeOut(this.gameObject);
    }
}
