﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDisableObject : MonoBehaviour
{
    [SerializeField]
    private float _Time;

    private void OnEnable()
    {
        StartCoroutine(AutoHide());
    }

    IEnumerator AutoHide()
    {
        yield return new WaitForSeconds(_Time);
        this.gameObject.SetActive(false);
    }
}
