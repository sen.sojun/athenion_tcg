﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Events;
using Photon.Pun;
using Photon.Chat;
using ExitGames.Client.Photon;

public class ChatMessage
{
    #region Properties
    public string Channel { get; private set; }
    public string Sender { get; private set; }
    public string Message { get; private set; }
    public DateTime RecieveTime { get; private set; }
    #endregion

    #region Constructors
    public ChatMessage()
    {
        Channel = "";
        Sender = "";
        Message = "";
        RecieveTime = DateTime.MinValue;
    }

    public ChatMessage(string channel, string sender, string message, DateTime recieveTime)
    {
        Channel = channel;
        Sender = sender;
        Message = message;
        RecieveTime = recieveTime;
    }

    public ChatMessage(ChatMessage chatMessage)
    {
        Channel = chatMessage.Channel;
        Sender = chatMessage.Sender;
        Message = chatMessage.Message;
        RecieveTime = chatMessage.RecieveTime;
    }
    #endregion
}

public class ChatManager : MonoSingleton<ChatManager>, IChatClientListener
{
    #region Enums
    public enum ChatSystemStatus
    {
          Idle
        , Connecting
        , Connected
        , Disconnecting
        , Disconnected
    }
    #endregion

    #region Private Properties
    private string[] _publicChannels = { "Channel_EN", "Channel_TH", "Channel_JP" };

    private string _chatAppVersion = "ATN_1";
    private ChatClient _chatClient = null;
    private ChatSystemStatus _status = ChatSystemStatus.Idle;

    private List<string> _subscribedPublicChannelList = new List<string>();
    private List<string> _subscribedFriendList = new List<string>();

    private bool _isShowLog = true;
    private List<ChatMessage> _messageList;
    private StringBuilder _debugText;
    private Rect _debugTextRect;
    private readonly int _maxMessage = 20;
    private readonly float _widthMargin = 20.0f;
    private readonly float _heightMargin = 20.0f;

    private static event UnityAction<string, string[], object[]> _onGetMessage;
    private static event UnityAction<string, object, string> _onGetPrivateMessage;
    #endregion

    #region Unity Methods
    protected override void Awake()
    {
        _debugTextRect = new Rect(
              _widthMargin
            , _heightMargin
            , Screen.width - (2.0f * _widthMargin)
            , Screen.height - (2.0f * _heightMargin)
        );

        base.Awake();
    }

    public void Update()
    {
        #if UNITY_EDITOR
        if (_status == ChatSystemStatus.Connected)
        {
            if (Input.GetKeyUp(KeyCode.T))
            {
                SendPublicMessage(string.Format("Hello at {0}", DateTimeData.GetDateTimeUTC()));
            }
        }
        #endif

        if (_chatClient != null)
        {
            // Call Service to keep the connection alive and to get incoming messages continuously call.
            _chatClient.Service();

            SetOnlineStatus(DataManager.Instance.PlayerInfo.ActiveStatus);
        }
    }
    #endregion

    #region Methods
    public void Connect(PhotonChatRegion region)
    {
        if (_status != ChatSystemStatus.Connecting && _status != ChatSystemStatus.Connected)
        {
            Debug.LogFormat("ChatManager/Connect: try connect to {0}...", region);

            // In the C# SDKs, the callbacks are defined in the `IChatClientListener` interface.
            // In the demos, we instantiate and use the ChatClient class to implement the IChatClientListener interface.
            _chatClient = new ChatClient(this);

            // Set your favourite region. "EU", "US", and "ASIA" are currently supported.
            _chatClient.ChatRegion = GetChatRegion(region);

            _status = ChatSystemStatus.Connecting;
            _chatClient.Connect(
                  PhotonNetwork.PhotonServerSettings.AppSettings.AppIdChat
                , _chatAppVersion
                , new AuthenticationValues(DataManager.Instance.PlayerInfo.DisplayName)
            );
        }
        else
        {
            Debug.LogFormat("ChatManager/Connect: Failed to connect to chat server. status:{0}", _status);
        }
    }

    public void Disconnect(UnityAction onComplete)
    {
        Debug.LogFormat("ChatManager/Disconnect: try disconnect...");

        if (_chatClient != null)
        {
            if (_chatClient.chatPeer == null || _chatClient.chatPeer.PeerState == PeerStateValue.Disconnected)
            {
                _status = ChatSystemStatus.Disconnected;
            }
            else
            {
                _status = ChatSystemStatus.Disconnecting;
                _chatClient.Disconnect();
            }
        }
        else
        {
            onComplete?.Invoke();
        }
    }

    public void SubscribeChannel(string[] channels)
    {
        if (_chatClient != null && _status == ChatSystemStatus.Connected)
        {
            _chatClient.Subscribe(channels);
        }
    }

    public void UnsubscribeChannel(string[] channels)
    {
        if (_chatClient != null && _status == ChatSystemStatus.Connected)
        {
            _chatClient.Unsubscribe(channels);
        }
    }

    public void SendPublicMessage(string message)
    {
        if (_chatClient != null)
        {
            foreach (string channel in _subscribedPublicChannelList)
            {
                _chatClient.PublishMessage(channel, message);
            }
        }
        else
        {
            Debug.LogErrorFormat("SendPublicMessage: Failed to send public message.");
        }
    }

    public void SendPublicMessage(string channel, string message)
    {
        if (_chatClient != null)
        {
            _chatClient.PublishMessage(channel, message);
        }
        else
        {
            Debug.LogErrorFormat("SendPublicMessage: Failed to send public message.");
        }
    }

    public void SendPrivateMessage(string target, string message)
    {
        if (_chatClient != null)
        {
            _chatClient.SendPrivateMessage(target, message);
        }
        else
        {
            Debug.LogErrorFormat("SendPrivateMessage: Failed to send private message.");
        }
    }

    public void SetOnlineStatus(PlayerActiveStatus playerStatus)
    {
        if (_chatClient != null)
        {
            switch (playerStatus)
            {
                case PlayerActiveStatus.Offline:
                {
                    _chatClient.SetOnlineStatus(ChatUserStatus.Offline);
                }
                break;

                case PlayerActiveStatus.Online:
                {
                    _chatClient.SetOnlineStatus(ChatUserStatus.Online);
                }
                break;

                case PlayerActiveStatus.Busy:
                {
                    _chatClient.SetOnlineStatus(ChatUserStatus.Away);
                }
                break;

                case PlayerActiveStatus.Playing:
                {
                    _chatClient.SetOnlineStatus(ChatUserStatus.Playing);
                }
                break;
            }
        }
    }

    public void SubscribeFriendStatus(List<string> friendNameList)
    {
        if (_chatClient != null && friendNameList != null && friendNameList.Count > 0)
        {
            if (_subscribedFriendList == null)
            {
                _subscribedFriendList = new List<string>();
            }

            _chatClient.AddFriends(friendNameList.ToArray());

            foreach (string name in friendNameList)
            {
                _subscribedFriendList.Add(name);
            }
        }
    }

    public void UnsubscribeFriendStatus(List<string> friendNameList)
    {
        if (_chatClient != null && friendNameList != null && friendNameList.Count > 0)
        {
            if (_subscribedFriendList == null)
            {
                _subscribedFriendList = new List<string>();
            }

            _chatClient.RemoveFriends(friendNameList.ToArray());

            foreach (string name in friendNameList)
            {
                _subscribedFriendList.Remove(name);
            }
        }
    }

    public static void SubscribeOnGetMessage(UnityAction<string, string[], object[]> onGetMessage)
    {
        _onGetMessage += onGetMessage;
    }

    public static void SubscribeOnGetPrivateMessage(UnityAction<string, object, string> onGetPrivateMessage)
    {
        _onGetPrivateMessage += onGetPrivateMessage;
    }

    public static void UnsubscribeOnGetMessage(UnityAction<string, string[], object[]> onGetMessage)
    {
        _onGetMessage += onGetMessage;
    }

    public static void UnsubscribeOnGetPrivateMessage(UnityAction<string, object, string> onGetPrivateMessage)
    {
        _onGetPrivateMessage += onGetPrivateMessage;
    }

    public static void ClearAllSubscribeOnGetMessage()
    {
        _onGetMessage = null;
    }

    public static void ClearAllSubscribeOnGetPrivateMessage()
    {
        _onGetPrivateMessage = null;
    }

    public static string GetChatRegion(PhotonChatRegion region)
    {
        switch (region)
        {
            case PhotonChatRegion.Singapore: return "asia";
            case PhotonChatRegion.Amsterdam: return "eu";
            case PhotonChatRegion.Washington: return "us";
            default: return "asia";
        }
    }

    private void AddMessage(ChatMessage chatMessage)
    {
        if (_messageList == null)
        {
            _messageList = new List<ChatMessage>();
        }

        _messageList.Add(chatMessage);

        if (_messageList.Count > _maxMessage)
        {
            _messageList.RemoveAt(0);
        }
    }
    #endregion

    #region IChatClientListener Methods
    public void DebugReturn(DebugLevel level, string message)
    {
        Debug.LogFormat("ChatManager/DebugReturn: level:{0} message:{1}", level, message);
    }

    public void OnDisconnected()
    {
        Debug.LogFormat("ChatManager/OnDisconnected:");
        _status = ChatSystemStatus.Disconnected;
    }

    public void OnConnected()
    {
        Debug.LogFormat("ChatManager/OnConnected:");
        _status = ChatSystemStatus.Connected;

        // Subscribe to current language channel.
        SubscribeChannel(new string[] { _publicChannels[(int)LocalizationManager.Instance.CurrentLanguage] });
    }

    public void OnChatStateChange(ChatState state)
    {
        Debug.LogFormat("ChatManager/OnChatStateChange: state:{0}", state);
    }

    public void OnGetMessages(string channelName, string[] senders, object[] messages)
    {
        string msgs = "";
        for (int i = 0; i < senders.Length; i++)
        {
            msgs += string.Format("\n{0}: {1}", senders[i], messages[i]);

            ChatMessage chatMsg = new ChatMessage(channelName, senders[i], messages[i].ToString(), DateTimeData.GetDateTimeUTC());
            AddMessage(chatMsg);
        }

        Debug.LogFormat("ChatManager/OnGetMessages: [{0}] (sender:{1}) {2} .", channelName, senders.Length, msgs);

        // All public messages are automatically cached in `Dictionary<string, ChatChannel> PublicChannels`.
        // So you don't have to keep track of them.
        // The channel name is the key for `PublicChannels`.
        // In very long or active conversations, you might want to trim each channels history.

        _onGetMessage?.Invoke(channelName, senders, messages);
    }

    public void OnPrivateMessage(string sender, object message, string channelName)
    {
        ChatMessage chatMsg = new ChatMessage(channelName, sender, message.ToString(), DateTimeData.GetDateTimeUTC());
        AddMessage(chatMsg);

        Debug.LogFormat("ChatManager/OnPrivateMessage: [{0}] {1}: {2} .", channelName, sender, message);

        // All private messages are automatically cached in `ChatClient.PrivateChannels`, so you don't have to keep track of them.
        // A channel name is applied as key for `PrivateChannels`.
        // Get a (remote) user's channel name with `ChatClient.GetPrivateChannelNameByUser(name)`.
        // e.g. To get and show all messages of a private channel:
        // ChatChannel ch = this.chatClient.PrivateChannels[ channelName ];
        // foreach ( object msg in ch.Messages )
        // {
        //     Console.WriteLine( msg );
        // }

        _onGetPrivateMessage?.Invoke(sender, message, channelName);
    }

    public void OnSubscribed(string[] channels, bool[] results)
    {
        string channelText = "";
        for (int i = 0; i < channels.Length; ++i)
        {
            channelText += string.Format(" {0}:{1}:{2}", i, channels[i], results[i]);

            if (results[i])
            {
                _subscribedPublicChannelList.Add(channels[i]);
            }
        }

        Debug.LogFormat("ChatManager/OnSubscribed: channels:{0}", channelText);
    }

    public void OnUnsubscribed(string[] channels)
    {
        string channelText = "";
        for(int i = 0; i < channels.Length; ++i)
        {
            channelText += string.Format(" {0}:{1}", i, channels[i]);

            _subscribedPublicChannelList.Remove(channels[i]);
        }

        Debug.LogFormat("ChatManager/OnUnsubscribed: channels:{0}", channelText);
    }

    public void OnStatusUpdate(string user, int status, bool gotMessage, object message)
    {
        Debug.LogFormat("ChatManager/OnStatusUpdate: user:{0} status:{1} gotMessage:{2} message:{3}", user, status, gotMessage, message);
    }

    public void OnUserSubscribed(string channel, string user)
    {
        Debug.LogFormat("ChatManager/OnUserSubscribed: channel:{0} user:{1}", channel, user);
    }

    public void OnUserUnsubscribed(string channel, string user)
    {
        Debug.LogFormat("ChatManager/OnUserUnsubscribed: channel:{0} user:{1}", channel, user);
    }
    #endregion

    #region OnGUI
    public void OnGUI()
    {
        if (_isShowLog)
        {
            if (_debugText == null) _debugText = new StringBuilder();
            _debugText.Clear();

            _debugText.AppendFormat("Chat Status : {0}\n", _status);
            _debugText.Append("Channels :");
            if (_subscribedPublicChannelList != null && _subscribedPublicChannelList.Count > 0)
            {
                foreach (string channel in _subscribedPublicChannelList)
                {
                    _debugText.AppendFormat(" {0}", channel);
                }
            }
            else
            {
                _debugText.Append(" None");
            }
            _debugText.Append("\n");
            if (_messageList != null && _messageList.Count > 0)
            {
                foreach (ChatMessage msg in _messageList)
                {
                    _debugText.AppendFormat(
                          "[{0}]{1} {2}:{3}\n"
                        , msg.Channel
                        , msg.RecieveTime.ToString("yyyy-MM-dd HH:mm:ss")
                        , msg.Sender
                        , msg.Message
                    );
                }
            }

            // Draw debug
            if (_debugText.Length > 0)
            {
                GUI.Label(_debugTextRect, "<color=white>" + _debugText.ToString() + "</color>");
            }
        }
    }
    #endregion
}
