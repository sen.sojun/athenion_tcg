﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
[RequireComponent(typeof(CanvasScaler))]
public class ScreenCanvas : MonoBehaviour
{
    #region Private Properties
    private double _screenRatio = 0.0;
    #endregion

    #region Start
    // Use this for initialization
    void Start()
    {
        UpdateCanvas();
    }
    #endregion

    #region Update
    private void Update()
    {
        #if UNITY_EDITOR
        UpdateCanvas();
        #endif
    }
    #endregion

    #region Methods
    private void OnEnable()
    {
        UpdateCanvas();
    }

    void UpdateCanvas()
    {
        if (Screen.width > 0 && Screen.height > 0)
        {
            double screenRatioSafe = (double)Screen.safeArea.height / (double)Screen.safeArea.width;
            double screenRatio = (double)Screen.height / (double)Screen.width;

            //Debug.LogFormat("Safe {0} : Normal {1}", screenRatioSafe, screenRatio);

            if (screenRatioSafe != _screenRatio || !Application.isPlaying)
            {
                _screenRatio = screenRatioSafe;
                CanvasScaler canvasScaler = GetComponent<CanvasScaler>();
                
                switch (GameHelper.GetScreenFitMode())
                {
                    case ScreenFitMode.Width:
                    {
                        // height equal or more than 16:9
                        canvasScaler.matchWidthOrHeight = 0.0f; // match width
                    }
                    break;

                    case ScreenFitMode.Height:
                    default:
                    {
                        // height less than 16:9
                        canvasScaler.matchWidthOrHeight = 1.0f; // match height
                    }
                    break;
                }
            }
        }        
    }
    #endregion
}
