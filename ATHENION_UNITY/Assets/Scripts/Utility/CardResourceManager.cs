﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace Karamucho.UI
{
    public class CardResourceManager
    {
        public enum ImageType
        { 
              Card
            , Token
            , InfoCard
            , InfoToken
            , InfoItem

            , Banner
            , Hero
            , HeroAnimated
            , HeroFull
        }
        //FrameCard Path
        private static readonly string _cardFrameRootPath = "Images/Cards/FrameImages/Card";
        private static readonly string _cardFrameBoxRootPath = "Images/Cards/FrameImages/Card_Box";
        private static readonly string _tokenFrameRootPath = "Images/Cards/FrameImages/Token";
        private static readonly string _bannerFrameRootPath = "Images/Cards/FrameImages/Banner";
        private static readonly string _itemFrameRootPath = "Images/Cards/FrameImages/Item";

        // Artwork Path
        private static readonly string _cardImageRootPath = "Images/Cards/ArtCardImages/ART_CARD_";
        private static readonly string _tokenImageRootPath = "Images/Cards/ArtTokenImages/ART_TOKEN_";
        private static readonly string _bannerImageRootPath = "Images/Cards/ArtBannerImages/ART_BANNER_";
        private static readonly string _itemImageRootPath = "Images/Cards/ArtItemImages/ART_ITEM_";

        // Icon Series Path
        private static readonly string _seriesIconRootPath = "Images/CardSeries/SERIE_";

        // Material Path 
        private static readonly string _cardImageMaterialPath = "Materials/M_ArtCard_Default";
        private static readonly string _tokenImageMaterialPath = "Materials/M_ArtToken_Default";
        private static readonly string _itemImageMaterialPath = "Materials/M_ArtItem_Default";

        // Root Path
        private static readonly string _itemRootPath = "Prefabs/Items/";
        private static readonly string _cardRootPath = "Prefabs/Cards/";
        private static readonly string _tokenRootPath = "Prefabs/Tokens/";
        private static readonly string _bannerRootPath = "Prefabs/Banners/";
        private static readonly string _heroRootPath = "Prefabs/Heros/";
        private static readonly string _heroAnimatedRootPath = "Prefabs/AnimatedHero/";

        // Folder
        private static readonly string _rendererFolder = "Renderers/";
        private static readonly string _imagesFolder = "UIImages/";

        // Prefix
        private static readonly string _cardRootPrefix = "CARD_";
        private static readonly string _tokenRootPrefix = "TOKEN_";
        private static readonly string _infoCardRootPrefix = "INFO_CARD_";
        private static readonly string _infoTokenRootPrefix = "INFO_TOKEN_";
        private static readonly string _infoItemRootPrefix = "INFO_ITEM_";

        private static readonly string _heroRootPrefix = "HERO_";
        private static readonly string _heroUIRootPrefix = "GROUP_";
        private static readonly string _heroAnimatedRootPrefix = "INGAME_";
        private static readonly string _maskPostfix = "_Mask";

        // Prefab Name
        private static readonly string _cardImagePrefabName = "CARD_ART";
        private static readonly string _tokenImagePrefabName = "TOKEN_ART";
        private static readonly string _infoCardImagePrefabName = "INFO_CARD_ART";
        private static readonly string _infoTokenImagePrefabName = "INFO_TOKEN_ART";
        private static readonly string _infoItemImagePrefabName = "INFO_ITEM_ART";
        private static readonly string _bannerImagePrefabName = "INFO_BANNER_ART";


        private static Dictionary<string, GameObject> _resourceList = null;

        private static string GetAnimatePath(ImageType type, string imageKey)
        {
            switch (type)
            {
                case ImageType.Card:    return string.Format("{0}{1}{2}{3}", _cardRootPath, _rendererFolder, _cardRootPrefix, imageKey);
                case ImageType.Token:   return string.Format("{0}{1}{2}{3}", _tokenRootPath, _rendererFolder, _tokenRootPrefix, imageKey);

                case ImageType.InfoCard: return string.Format("{0}{1}{2}{3}", _cardRootPath, _imagesFolder, _infoCardRootPrefix, imageKey);
                case ImageType.InfoToken: return string.Format("{0}{1}{2}{3}", _tokenRootPath, _imagesFolder, _infoTokenRootPrefix, imageKey);
                case ImageType.InfoItem: return string.Format("{0}{1}{2}{3}", _itemRootPath, _imagesFolder, _infoItemRootPrefix, imageKey);
            }
            return "";
        }

        private static string GetDefaultPrefabPath(ImageType type)
        {
            switch (type)
            {
                case ImageType.Card:    return string.Format("{0}{1}{2}", _cardRootPath, _rendererFolder, _cardImagePrefabName);
                case ImageType.Token:   return string.Format("{0}{1}{2}", _tokenRootPath, _rendererFolder, _tokenImagePrefabName);

                case ImageType.InfoCard: return string.Format("{0}{1}{2}", _cardRootPath, _imagesFolder, _infoCardImagePrefabName);
                case ImageType.InfoToken: return string.Format("{0}{1}{2}", _tokenRootPath, _imagesFolder, _infoTokenImagePrefabName);
                case ImageType.InfoItem: return string.Format("{0}{1}{2}", _itemRootPath, _imagesFolder, _infoItemImagePrefabName);
                case ImageType.Banner: return string.Format("{0}{1}{2}", _bannerRootPath, _imagesFolder, _bannerImagePrefabName);
            }

            return "";
        }

        public static bool LoadCardFrame(CardRarity rarity, CardElementType element, out Sprite sprite)
        {
            string path = "";
            switch (rarity)
            {
                case CardRarity.None:
                case CardRarity.Common:
                {
                    path += "_Common";
                }
                break;
                case CardRarity.Rare:
                {
                    path += "_Rare";
                }
                break;
                case CardRarity.Epic:
                {
                    path += "_Epic";
                }
                break;
                case CardRarity.Legend:
                {
                    path += "_Legendary";
                }
                break;
                case CardRarity.StarChamber:
                {
                    path += "_StarChamber";
                }
                break;
            }
            switch (element)
            {
                case CardElementType.NEUTRAL: path += "_None";break;
                case CardElementType.AIR: path += "_Air";break;
                case CardElementType.FIRE: path += "_Fire"; break;
                case CardElementType.EARTH: path += "_Earth"; break;
                case CardElementType.WATER: path += "_Water"; break;
                case CardElementType.HOLY: path += "_Holy"; break;
                case CardElementType.DARK: path += "_Dark"; break;
            }
            Sprite temp = ResourceManager.Load<Sprite>(_cardFrameRootPath + path);
            
            if (temp != null)
            {
                sprite = temp;
                return true;
            }
            else
            {
                sprite = null;
                return false;
            }

        }

        public static bool LoadCardFrameBox(CardRarity rarity, out Sprite sprite)
        {
            string path = "";
            switch (rarity)
            {
                case CardRarity.None:
                case CardRarity.Common:
                {
                    path += "_Common";
                }
                break;
                case CardRarity.Rare:
                {
                    path += "_Rare";
                }
                break;
                case CardRarity.Epic:
                {
                    path += "_Epic";
                }
                break;
                case CardRarity.Legend:
                {
                    path += "_Legendary";
                }
                break;
                case CardRarity.StarChamber:
                {
                    path += "_StarChamber";
                }
                break;
            }
            Sprite temp = ResourceManager.Load<Sprite>(_cardFrameBoxRootPath + path);

            if (temp != null)
            {
                sprite = temp;
                return true;
            }
            else
            {
                sprite = null;
                return false;
            }

        }

        public static bool LoadCardMaskFrame(CardRarity rarity, CardElementType element, out Sprite sprite)
        {
            string path = "";
            switch (rarity)
            {
                case CardRarity.None:
                case CardRarity.Common:
                {
                    path += "_Common";
                }
                break;
                case CardRarity.Rare:
                {
                    path += "_Rare";
                }
                break;
                case CardRarity.Epic:
                {
                    path += "_Epic";
                }
                break;
                case CardRarity.Legend:
                {
                    path += "_Legendary";
                }
                break;
                case CardRarity.StarChamber:
                {
                    path += "_StarChamber";
                }
                break;
            }
            switch (element)
            {
                case CardElementType.NEUTRAL: path += "_None"; break;
                case CardElementType.AIR: path += "_Air"; break;
                case CardElementType.FIRE: path += "_Fire"; break;
                case CardElementType.EARTH: path += "_Earth"; break;
                case CardElementType.WATER: path += "_Water"; break;
                case CardElementType.HOLY: path += "_Holy"; break;
                case CardElementType.DARK: path += "_Dark"; break;
            }
            Sprite temp = ResourceManager.Load<Sprite>(_cardFrameRootPath + path + _maskPostfix);

            if (temp != null)
            {
                sprite = temp;
                return true;
            }
            else
            {
                sprite = null;
                return false;
            }

        }

        public static bool LoadTokenFrame(CardRarity rarity, out Sprite sprite)
        {
            string path = "";
            switch (rarity)
            {
                case CardRarity.None:
                case CardRarity.Common:
                {
                    path += "_Common";
                }
                break;
                case CardRarity.Rare:
                {
                    path += "_Rare";
                }
                break;
                case CardRarity.Epic:
                {
                    path += "_Epic";
                }
                break;
                case CardRarity.Legend:
                {
                    path += "_Legendary";
                }
                break;
                case CardRarity.StarChamber:
                {
                    path += "_StarChamber";
                }
                break;
            }

            Sprite temp = ResourceManager.Load<Sprite>(_tokenFrameRootPath + path);

            if (temp != null)
            {
                sprite = temp;
                return true;
            }
            else
            {
                sprite = null;
                return false;
            }
        }

        public static bool LoadTokenMaskFrame(CardRarity rarity, out Sprite sprite)
        {
            string path = "";
            switch (rarity)
            {
                case CardRarity.None:
                case CardRarity.Common:
                {
                    path += "_Common";
                }
                break;
                case CardRarity.Rare:
                {
                    path += "_Rare";
                }
                break;
                case CardRarity.Epic:
                {
                    path += "_Epic";
                }
                break;
                case CardRarity.Legend:
                {
                    path += "_Legendary";
                }
                break;
                case CardRarity.StarChamber:
                {
                    path += "_StarChamber";
                }
                break;
            }

            Sprite temp = ResourceManager.Load<Sprite>(_tokenFrameRootPath + path + _maskPostfix);

            if (temp != null)
            {
                sprite = temp;
                return true;
            }
            else
            {
                sprite = null;
                return false;
            }
        }

        public static bool LoadBannerFrame(CardRarity rarity, out Sprite sprite)
        {
            string path = "";
            switch (rarity)
            {
                case CardRarity.None:
                case CardRarity.Common:
                {
                    path += "_Common";
                }
                break;
                case CardRarity.Rare:
                {
                    path += "_Rare";
                }
                break;
                case CardRarity.Epic:
                {
                    path += "_Epic";
                }
                break;
                case CardRarity.Legend:
                {
                    path += "_Legendary";
                }
                break;
                case CardRarity.StarChamber:
                {
                    path += "_StarChamber";
                }
                break;
            }

            Sprite temp = ResourceManager.Load<Sprite>(_bannerFrameRootPath + path);

            if (temp != null)
            {
                sprite = temp;
                return true;
            }
            else
            {
                sprite = null;
                return false;
            }
        }

        public static bool LoadItemFrame(CardRarity rarity, out Sprite sprite)
        {
            string path = "";
            switch (rarity)
            {
                case CardRarity.None:
                case CardRarity.Common:
                {
                    path += "_Common";
                }
                break;
                case CardRarity.Rare:
                {
                    path += "_Rare";
                }
                break;
                case CardRarity.Epic:
                {
                    path += "_Epic";
                }
                break;
                case CardRarity.Legend:
                {
                    path += "_Legendary";
                }
                break;
                case CardRarity.StarChamber:
                {
                    path += "_StarChamber";
                }
                break;
            }
            Sprite temp = ResourceManager.Load<Sprite>(_itemFrameRootPath + path);

            if (temp != null)
            {
                sprite = temp;
                return true;
            }
            else
            {
                sprite = null;
                return false;
            }

        }

        public static bool LoadItemMaskFrame(CardRarity rarity, out Sprite sprite)
        {
            string path = "";
            switch (rarity)
            {
                case CardRarity.None:
                case CardRarity.Common:
                {
                    path += "_Common";
                }
                break;
                case CardRarity.Rare:
                {
                    path += "_Rare";
                }
                break;
                case CardRarity.Epic:
                {
                    path += "_Epic";
                }
                break;
                case CardRarity.Legend:
                {
                    path += "_Legendary";
                }
                break;
                case CardRarity.StarChamber:
                {
                    path += "_StarChamber";
                }
                break;
            }

            Sprite temp = ResourceManager.Load<Sprite>(_itemFrameRootPath + path + _maskPostfix);

            if (temp != null)
            {
                sprite = temp;
                return true;
            }
            else
            {
                sprite = null;
                return false;
            }

        }

        public static bool LoadSeriesIcon(string series, out Sprite sprite)
        {
            Sprite temp = ResourceManager.Load<Sprite>(_seriesIconRootPath + series);

            if (temp != null)
            {
                sprite = temp;
                return true;
            }
            else
            {
                sprite = ResourceManager.Load<Sprite>(_seriesIconRootPath + "Basic"); ;
                return false;
            }
        }

        private static bool Load(ImageType type, string imageKey, bool isAnimate, out GameObject obj)
        {
            switch(type)
            {
                case ImageType.Card:
                case ImageType.Token:
                case ImageType.InfoCard:
                case ImageType.InfoToken:
                case ImageType.InfoItem:
                case ImageType.Banner:
                {
                    if (isAnimate)
                    {
                        string path = GetAnimatePath(type, imageKey);
                        //Debug.Log("CardResourceManager: Loading... " + path);

                        if (GetResource(path, out obj))
                        {
                            return true;
                        }
                        else
                        {
                            obj = ResourceManager.Load(path) as GameObject;

                            if (obj != null)
                            {
                                CacheResource(path, obj);
                            }

                            return (obj != null);
                        }
                    }
                    else
                    {
                        string path = GetDefaultPrefabPath(type);
                        if (GetResource(path, out obj))
                        {
                            return true;
                        }
                        else
                        {
                            obj = ResourceManager.Load(path) as GameObject;

                            if (obj != null)
                            {
                                CacheResource(path, obj);
                                Setup(type, imageKey, ref obj);
                            }

                            return (obj != null);
                        }
                    }
                
                }
                //break;

                case ImageType.Hero:
                {
                    string path = string.Format("{0}{1}{2}", _heroRootPath, _heroRootPrefix, imageKey);
                    if (GetResource(path, out obj))
                    {
                        return true;
                    }
                    else
                    {
                        //Debug.Log("CardResourceManager: Loading... " + path);
                        obj = ResourceManager.Load(path) as GameObject;

                        if (obj != null)
                        {
                            CacheResource(path, obj);
                        }

                        return (obj != null);
                    }
                }
                //break;

                default:
                {
                    obj = null;
                    return false;
                }
                //break;
            }
        }

        private static void LoadAsync(ImageType type, string imageKey, bool isAnimate, UnityAction<GameObject> onComplete)
        {
            GameObject obj = null;
            switch (type)
            {
                case ImageType.Card:
                case ImageType.Token:
                case ImageType.InfoCard:
                case ImageType.InfoToken:
                case ImageType.InfoItem:
                case ImageType.Banner:
                {
                    if (isAnimate)
                    {
                        string path = GetAnimatePath(type, imageKey);
                        //Debug.Log("CardResourceManager: Loading... " + path);

                        if (GetResource(path, out obj))
                        {
                            if (onComplete != null)
                            {
                                onComplete.Invoke(obj);
                            }
                            return;
                        }
                        else
                        {
                            ResourceManager.LoadAsync<GameObject>(
                                  path
                                , delegate(GameObject gameObject) 
                                {
                                    if (obj != null)
                                    {
                                        CacheResource(path, obj);
                                    }

                                    if (onComplete != null)
                                    {
                                        onComplete.Invoke(obj);
                                    }
                                }
                            );
                            return;
                        }
                    }
                    else
                    {
                        string path = GetDefaultPrefabPath(type);
                        if (GetResource(path, out obj))
                        {
                            if (onComplete != null)
                            {
                                onComplete.Invoke(obj);
                            }
                            return;
                        }
                        else
                        {
                            ResourceManager.LoadAsync<GameObject>(
                                  path
                                , delegate (GameObject loadObj)
                                {
                                    if (loadObj != null)
                                    {
                                        CacheResource(path, loadObj);
                                        //Setup(type, imageKey, ref loadObj);
                                    }

                                    if (onComplete != null)
                                    {
                                        onComplete.Invoke(loadObj);
                                    }
                                }
                            );
                            return;
                        }
                    }
                }
                //break;

                case ImageType.Hero:
                {
                    string path = string.Format("{0}{1}{2}", _heroRootPath, _heroRootPrefix, imageKey);
                    if (GetResource(path, out obj))
                    {
                        if (onComplete != null)
                        {
                            onComplete.Invoke(obj);
                        }
                        return;
                    }
                    else
                    {
                        //Debug.Log("CardResourceManager: Loading... " + path);
                        ResourceManager.LoadAsync<GameObject>(
                              path
                            , delegate (GameObject loadObj)
                            {
                                if (loadObj != null)
                                {
                                    CacheResource(path, loadObj);
                                }

                                if (onComplete != null)
                                {
                                    onComplete.Invoke(loadObj);
                                }
                            }
                        );
                        return;
                    }
                }
                //break;

                default:
                {
                    if (onComplete != null)
                    {
                        onComplete.Invoke(null);
                    }
                    return;
                }
                //break;
            }
        }

        private static void Setup(ImageType type, string imageKey, ref GameObject obj)
        {
            if (obj != null)
            {
                switch (type)
                {
                    case ImageType.Card:
                    case ImageType.Token:
                    {
                        string path = "";
                        if (type == ImageType.Card)
                        {
                            path = _cardImageRootPath + imageKey;
                        }
                        else
                        {
                            path = _tokenImageRootPath + imageKey;
                        }
                        Texture texture = ResourceManager.Load<Texture>(path);

                        if (texture == null)
                        {
                            if (type == ImageType.Card)
                            {
                                path = _cardImageRootPath + "IM0000";
                            }
                            else
                            {
                                path = _tokenImageRootPath + "IM0000";
                            }
                            texture = ResourceManager.Load(path) as Texture;
                        }

                        Renderer r = obj.GetComponent<Renderer>();
                        r.material.SetTexture("_NewTex_1", texture);
                    }
                    break;

                    case ImageType.InfoCard:
                    case ImageType.InfoToken:
                    case ImageType.InfoItem:
                    case ImageType.Banner:
                    {
                        string path = "";
                        if (type == ImageType.InfoCard)
                        {
                            path = _cardImageRootPath + imageKey;
                        }
                        else if(type == ImageType.InfoToken)
                        {
                            path = _tokenImageRootPath + imageKey;
                        }
                        else if (type == ImageType.InfoItem)
                        {
                            path = _itemImageRootPath + imageKey;
                        }
                        else
                        {
                            path = _bannerImageRootPath + imageKey;
                        }
                        Texture texture = ResourceManager.Load<Texture>(path);

                        if (texture == null)
                        {
                            Debug.LogWarning("Cannot find:" + path);
                            if (type == ImageType.InfoCard)
                            {
                                path = _cardImageRootPath + "IM0000";
                            }
                            else if(type == ImageType.InfoToken)
                            {
                                path = _tokenImageRootPath + "IM0000";
                            }
                            else if (type == ImageType.InfoItem)
                            {
                                path = _itemImageRootPath + "IM0000";
                            }
                            else
                            {
                                path = _bannerImageRootPath + "IM0000";
                            }
                            texture = ResourceManager.Load(path) as Texture;
                        }

                        Image image = obj.GetComponent<Image>();                        
                        //Material mat = MonoBehaviour.Instantiate(ResourceManager.Load<Material>(_cardImageMaterialPath));
                        Material mat = new Material(ResourceManager.Load<Material>(_cardImageMaterialPath));
                        mat.SetTexture("_NewTex_1", texture);
                        image.material = mat;
                    }
                    break;
                }
            }
        }

        private static void SetupAsync(ImageType type, string imageKey, GameObject obj, UnityAction onComplete)
        {
            if (obj != null)
            {
                switch (type)
                {
                    case ImageType.Card:
                    case ImageType.Token:
                    {
                        string path = "";
                        if (type == ImageType.Card)
                        {
                            path = _cardImageRootPath + imageKey;
                        }
                        else
                        {
                            path = _tokenImageRootPath + imageKey;
                        }

                        ResourceManager.LoadAsync<Texture>(
                              path
                            , delegate(Texture texture)
                            {
                                if (obj != null)
                                {
                                    if (texture == null)
                                    {
                                        // texture is null. load default path.

                                        if (type == ImageType.Card)
                                        {
                                            path = _cardImageRootPath + "IM0000";
                                        }
                                        else
                                        {
                                            path = _tokenImageRootPath + "IM0000";
                                        }

                                        texture = ResourceManager.Load(path) as Texture;
                                    }

                                    Renderer r = obj.GetComponent<Renderer>();
                                    r.material.SetTexture("_NewTex_1", texture);
                                }

                                onComplete?.Invoke();
                            }
                        );

                    }
                    break;

                    case ImageType.InfoCard:
                    case ImageType.InfoToken:
                    case ImageType.InfoItem:
                    case ImageType.Banner:
                    {
                        string path = "";
                        if (type == ImageType.InfoCard)
                        {
                            path = _cardImageRootPath + imageKey;
                        }
                        else if (type == ImageType.InfoToken)
                        {
                            path = _tokenImageRootPath + imageKey;
                        }
                        else if (type == ImageType.InfoItem)
                        {
                            path = _itemImageRootPath + imageKey;
                        }
                        else
                        {
                            path = _bannerImageRootPath + imageKey;
                        }

                        ResourceManager.LoadAsync<Texture>(
                              path
                            , delegate(Texture texture)
                            {
                                if (obj != null)
                                {
                                    if (texture == null)
                                    {
                                        // texture is null. load default path.

                                        if (type == ImageType.InfoCard)
                                        {
                                            path = _cardImageRootPath + "IM0000";
                                        }
                                        else if (type == ImageType.InfoToken)
                                        {
                                            path = _tokenImageRootPath + "IM0000";
                                        }
                                        else if (type == ImageType.InfoItem)
                                        {
                                            path = _itemImageRootPath + "IM0000";
                                        }
                                        else
                                        {
                                            path = _bannerImageRootPath + "IM0000";
                                        }

                                        texture = ResourceManager.Load(path) as Texture;
                                    }

                                    Image image = obj.GetComponent<Image>();
                                    Material mat = new Material(ResourceManager.Load<Material>(_cardImageMaterialPath));
                                    mat.SetTexture("_NewTex_1", texture);
                                    image.material = mat;
                                }

                                onComplete?.Invoke();
                            }
                        );
                    }
                    break;
                }
            }
        }

        private static bool Create(ImageType type, string imageKey, bool isAnimate, out GameObject obj)
        {
            GameObject prefab = null;
            if (Load(type, imageKey, isAnimate, out prefab))
            {
                obj = GameObject.Instantiate(prefab) as GameObject;
                if (obj != null && !isAnimate)
                {
                    Setup(type, imageKey, ref obj);
                }
                return (obj != null);
            }

            obj = null;
            return false;
        }

        public static bool Create(ImageType type, string imageKey, Transform parent, bool isAnimate, out GameObject obj)
        {
            GameObject prefab = null;
            if (Load(type, imageKey, isAnimate, out prefab))
            {
                if (parent != null)
                {
                    obj = GameObject.Instantiate(prefab, parent) as GameObject;
                }
                else
                {
                    obj = GameObject.Instantiate(prefab) as GameObject;
                }

                if (obj != null && !isAnimate)
                {
                    Setup(type, imageKey, ref obj);
                }
                return (obj != null);
            }

            obj = null;
            return false;
        }

        public static bool LoadHero(string imageKey, out GameObject obj)
        {
            GameObject prefab = ResourceManager.Load<GameObject>(_heroAnimatedRootPath + _heroAnimatedRootPrefix + imageKey);
            if (prefab != null)
            {
                obj = prefab;
                return true;
            }
            else
            {
                obj = null;
                return false;
            }

        }

        public static bool LoadUIHero(string imageKey, out GameObject obj)
        {
            GameObject prefab = ResourceManager.Load<GameObject>(_heroAnimatedRootPath + _heroUIRootPrefix + imageKey);
            if (prefab != null)
            {
                obj = prefab;
                return true;
            }
            else
            {
                obj = null;
                return false;
            }

        }



        public static void CreateAsync(ImageType type, string imageKey, Transform parent, bool isAnimate, UnityAction<GameObject> onComplete)
        {
            LoadAsync(
                  type
                , imageKey
                , isAnimate
                , delegate (GameObject prefab) 
                {
                    GameObject obj;
                    if (parent != null)
                    {
                        obj = GameObject.Instantiate(prefab, parent) as GameObject;
                    }
                    else
                    {
                        obj = GameObject.Instantiate(prefab) as GameObject;
                    }

                    if (obj != null && !isAnimate)
                    {
                        SetupAsync(
                              type
                            , imageKey
                            , obj
                            , delegate() 
                            {
                                onComplete?.Invoke(obj);
                            }
                        );
                    }
                    else
                    {
                        onComplete?.Invoke(obj);
                    }
                }
            );
        }

        public static void ClearCache()
        {
            /*
            foreach (KeyValuePair<string, Object> item in _resourceList)
            {
                Resources.UnloadAsset(item.Value);
            }
            */

            if (_resourceList != null)
            {
                _resourceList.Clear();
            }

            Resources.UnloadUnusedAssets();
        }

        private static void CacheResource(string key, GameObject obj)
        {
            if (_resourceList == null)
            {
                _resourceList = new Dictionary<string, GameObject>();
            }

            if (!_resourceList.ContainsKey(key))
            {
                _resourceList.Add(key, obj);
            }
            else
            {
                _resourceList[key] = obj;
            }
        }

        private static bool GetResource(string key, out GameObject obj)
        {
            if (_resourceList != null)
            {
                return _resourceList.TryGetValue(key, out obj);
            }

            obj = null;
            return false;
        }
    }
}
