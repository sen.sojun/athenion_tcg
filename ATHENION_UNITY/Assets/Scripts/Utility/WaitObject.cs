﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public interface ITimerUI
{ 
    void SetMaxTime(float maxTime);
    void OnUpdateTime(float remainTime);
    void SetSound(SoundManager.SFX tickSFX, SoundManager.SFX endSFX);
}

public class WaitObject : UtilityItem
{
    #region Public Properties
    public float Timer { get { return _timer; } }
    public bool IsStart { get { return _isStart; } }
    #endregion

    #region Private Properties
    private DateTime _startTimestamp;
    private float _countdownTime;

    private float _timer = 0.0f;
    private bool _isStart = false;
    private ITimerUI _timerUI;
    private UnityAction<float> _onUpdateTimer;
    private UnityAction _onComplete;
    #endregion

    #region Start
    /*
    // Use this for initialization
    void Start ()
    {
	}
    */
    #endregion

    #region Update
    // Update is called once per frame
    void Update ()
    {
        if (_isStart)
        {
            if (
                   _timerUI == null                         // use without UI.
                || GameManager.Instance.IsEndGame 
                || !GameManager.Instance.IsNetworkPlay() 
                || !GameManager.Instance.IsDisconnect
            )
            {
                _timer -= Time.deltaTime;

                // Update value
                if (_timerUI != null)
                {
                    _timerUI.OnUpdateTime(_timer);
                }

                if (_onUpdateTimer != null)
                {
                    _onUpdateTimer.Invoke(_timer);
                }

                if (_timer <= 0.0f)
                {
                    // done
                    OnComplete();
                }
            }
        }
	}
    #endregion

    #region Methods
    public void StartTimer(float time, UnityAction onComplete, UnityAction<float> onUpdateTimer)
    {
        _startTimestamp = DateTimeData.GetDateTimeUTC();
        _timer = time;
        _countdownTime = time;

        _timerUI = null;

        DontDestroyOnLoad(gameObject);

        _onComplete = onComplete;
        _onUpdateTimer = onUpdateTimer;
        GameManager.OnUpdatePassingTime += OnUpdatePassingTime;

        _isStart = true;
    }

    public void StartTimer(float time, UnityAction onComplete, ITimerUI timerUI = null)
    {
        _startTimestamp = DateTimeData.GetDateTimeUTC();
        _timer = time;
        _countdownTime = time;

        _timerUI = timerUI;
        if (_timerUI != null)
        {
            _timerUI.SetMaxTime(_timer);
        }

        DontDestroyOnLoad(gameObject);

        _onComplete = onComplete;
        _onUpdateTimer = null;
        GameManager.OnUpdatePassingTime += OnUpdatePassingTime;

        _isStart = true;
    }

    public void StartTimer(float time, UnityAction onComplete, SoundManager.SFX tickSFX, SoundManager.SFX endSFX, ITimerUI timerUI = null)
    {
        _startTimestamp = DateTimeData.GetDateTimeUTC();
        _timer = time;
        _countdownTime = time;

        _timerUI = timerUI;
        if (_timerUI != null)
        {
            _timerUI.SetMaxTime(_timer);
            _timerUI.SetSound(tickSFX, endSFX);
        }

        DontDestroyOnLoad(gameObject);

        _onComplete = onComplete;
        _onUpdateTimer = null;
        GameManager.OnUpdatePassingTime += OnUpdatePassingTime;

        _isStart = true;
    }

    public void StopTimer(bool isCallComplete = true)
    {
        _isStart = false;

        if (isCallComplete)
        {
            if (_onComplete != null)
            {
                _onComplete.Invoke();
            }
        }

        GameManager.OnUpdatePassingTime -= OnUpdatePassingTime;
        UtilityPool.Instance.ReleaseItem(this);
    }

    private void OnComplete()
    {
        _isStart = false;

        if (_onComplete != null)
        {
            _onComplete.Invoke();
        }

        GameManager.OnUpdatePassingTime -= OnUpdatePassingTime;
        UtilityPool.Instance.ReleaseItem(this);
    }

    private void OnUpdatePassingTime()
    {
        DateTime currentTimeStamp = DateTimeData.GetDateTimeUTC();
        float passTime = (float)(currentTimeStamp - _startTimestamp).TotalSeconds;

        float timer = (_countdownTime - passTime);
        _timer = timer;
    }
    #endregion
}
