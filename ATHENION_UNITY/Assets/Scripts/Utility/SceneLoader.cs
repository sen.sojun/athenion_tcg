﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public void StartUnLoadSceneAsync(string sceneName, UnityAction onComplete)
    {
        DontDestroyOnLoad(gameObject);
        StartCoroutine(IEUnloadSceneAsync(sceneName, onComplete));
    }

    private IEnumerator IEUnloadSceneAsync(string sceneName, UnityAction onComplete)
    {
        Scene targetScene = SceneManager.GetSceneByName(sceneName);
        if (targetScene == null)
        {
            onComplete?.Invoke();
            Destroy(gameObject);
        }
        else
        {
            AsyncOperation operation = SceneManager.UnloadSceneAsync(sceneName);
            yield return new WaitUntil(() => operation.isDone);
            onComplete?.Invoke();
            Destroy(gameObject);
        }
    }

    public void StartLoadAsync(string sceneName, LoadSceneMode loadSceneMode, UnityAction onComplete, UnityAction<float> onLoading)
    {
        DontDestroyOnLoad(gameObject);

        //QualitySettings.asyncUploadTimeSlice = 4;
        //QualitySettings.asyncUploadBufferSize = 16;
        //QualitySettings.asyncUploadPersistentBuffer = true;

        StartCoroutine(LoadingSceneAsync(sceneName, loadSceneMode, onComplete, onLoading));
    }

    private IEnumerator LoadingSceneAsync(string sceneName, LoadSceneMode loadSceneMode, UnityAction onComplete, UnityAction<float> onLoading)
    {
        Debug.LogFormat("Loading scene {0}", sceneName);
        yield return new WaitForSeconds(0.1f);
        AsyncOperation asyncLoadUI = SceneManager.LoadSceneAsync(sceneName, loadSceneMode);

        // Wait until the asynchronous scene fully loads
        while (!asyncLoadUI.isDone)
        {
            if (onLoading != null)
            {
                onLoading.Invoke(asyncLoadUI.progress);
            }

            yield return null;
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }

        Destroy(gameObject);
    }
}
