﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DelayObject : UtilityItem
{
    private bool _isStart = false;
    private float _delayTime;
    private UnityAction _onComplete;

    private float _timer = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (_isStart)
        {
            _timer += Time.deltaTime;

            if (_timer >= _delayTime)
            {
                OnComplete();
            }
        }
    }

    public void StartDelay(float delayTime, UnityAction onComplete)
    {
        _delayTime = delayTime;
        _onComplete = onComplete;
        _timer = 0.0f;

        _isStart = true;
    }

    private void OnComplete()
    {
        _onComplete?.Invoke();

        UtilityPool.Instance.ReleaseItem(this);
    }
}
