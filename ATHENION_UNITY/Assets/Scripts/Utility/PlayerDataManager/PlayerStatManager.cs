﻿using GameEvent;
using Karamucho.Collection;
using Karamucho.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

#region Global Enums
public enum CountPlayerStatType
{
    CountPlayerStat
    , CountPlayerStatMatch
    , CountPlayerStatCardSet
    , CountPlayerStatSet
}
#endregion

#region CountPlayerStatData
public class CountPlayerStatAction : QueueData
{
    #region Public Properties
    public PlayerStatDataList DataList { get { return _playerStatDataList; } }
    #endregion

    #region Private Properties
    private CountPlayerStatType _type;
    private PlayerStatDataList _playerStatDataList = new PlayerStatDataList();
    private UnityAction _onActionComplete;
    #endregion

    #region Contructors
    public CountPlayerStatAction(CountPlayerStatType type, PlayerStatDataList playerStatDataList, UnityAction onComplete)
    {
        _type = type;
        _playerStatDataList = new PlayerStatDataList(playerStatDataList);
        _onActionComplete = onComplete;
    }

    public CountPlayerStatAction(CountPlayerStatAction data)
    {
        _type = data._type;
        _playerStatDataList = new PlayerStatDataList(data._playerStatDataList);
        _onActionComplete = data._onActionComplete;
    }
    #endregion

    #region Methods
    public override void Active(UnityAction<int> onComplete)
    {
        _onComplete = onComplete;

        switch (_type)
        {
            case CountPlayerStatType.CountPlayerStat:
            {
                DataManager.Instance.CountPlayerStat(
                    _playerStatDataList
                    , OnActiveComplete
                    , OnActiveFail
                );
            }
            break;

            case CountPlayerStatType.CountPlayerStatMatch:
            {
                DataManager.Instance.CountPlayerStatMatch(
                    _playerStatDataList
                    , OnActiveComplete
                    , OnActiveFail
                );
            }
            break;

            case CountPlayerStatType.CountPlayerStatCardSet:
            {
                DataManager.Instance.CountPlayerStatCardSet(
                    _playerStatDataList
                    , OnActiveComplete
                    , OnActiveFail
                );
            }
            break;

            case CountPlayerStatType.CountPlayerStatSet:
            {
                DataManager.Instance.CountPlayerStatSet(
                   _playerStatDataList
                   , OnActiveComplete
                   , OnActiveFail
               );
            }
            break;
        }
    }

    protected override void OnActiveComplete()
    {
        _onActionComplete?.Invoke();

        base.OnActiveComplete();
    }

    protected void OnActiveFail(string errorMsg)
    {
        Debug.LogError(errorMsg);

        this.OnActiveComplete();
    }

    public bool IsActionEqual(CountPlayerStatAction action)
    {
        if (_type == action._type)
        {
            if (_onComplete == null && action._onComplete == null)
            {
                return true;
            }
        }

        return false;
    }
    #endregion
}
#endregion

public class PlayerStatManager : GameQueue<PlayerStatManager, CountPlayerStatAction>
{
    public static event Action OnCountPlayerStatCompleted;

    #region Private Properties
    private static float _waitTime = 2.5f; // prevent cloud script error. Too many request.
    private float _timer = 0.0f;
    private Dictionary<int, Dictionary<string, int>> _actionCountInTurn = new Dictionary<int, Dictionary<string, int>>();
    private Dictionary<string, Dictionary<string, int>> _actionCountInMatch = new Dictionary<string, Dictionary<string, int>>();
    #endregion

    protected override void Update()
    {
        _timer -= Time.deltaTime;

        if (_timer <= 0.0f)
        {
            _timer = _waitTime;

            base.Update();
        }
    }

    #region Methods
    protected override void InitSelf()
    {
        base.InitSelf();
        AddListenerShopEvent();
        AddListenerInventoryEvent();
        AddListenerCardCollectionEvent();
        AddListenerPlayerInventory();
        AddListenerGameManagerEvent();
        AddListenerDataManagerEvents();
        AddListenerDeck();
        AddListenerMission();
        AddListenerOpenPageEvents();
        AddListenerInGameEvents();
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        RemoveListenerInventoryEvent();
        RemoveListenerShopEvent();
        RemoveListenerCardCollectionEvent();
        RemoveListenerPlayerInventory();
        RemoveListenerGameManagerEvent();
        RemoveListenerDataManagerEvents();
        RemoveListenerDeck();
        RemoveListenerMission();
        RemoveListenerOpenPageEvents();
        RemoveListenerInGameEvents();
    }

    #region Event

    #region Shop
    private void AddListenerShopEvent()
    {
        ShopManager.OnPurchaseComplete += ShopManager_OnPurchaseComplete;
        ShopManager.OnClaimDailyFree += ShopManager_OnClaimDailyFree;
    }

    private void RemoveListenerShopEvent()
    {
        ShopManager.OnPurchaseComplete -= ShopManager_OnPurchaseComplete;
        ShopManager.OnClaimDailyFree -= ShopManager_OnClaimDailyFree;
    }
    #endregion

    #region CardPack Inventory
    private void AddListenerInventoryEvent()
    {
        CardPackController.OnSpawnCards += Inventory_OnOpenPack;
        CardPackController.OnSkip += Inventory_OnOpenAllPack;
        CardPackController.OnStartSystem += Inventory_OnStartSystem;
    }

    private void RemoveListenerInventoryEvent()
    {
        CardPackController.OnSpawnCards -= Inventory_OnOpenPack;
        CardPackController.OnSkip -= Inventory_OnOpenAllPack;
        CardPackController.OnStartSystem -= Inventory_OnStartSystem;
    }
    #endregion

    #region Card Collection
    private void AddListenerCardCollectionEvent()
    {
        CollectionCardManager.OnCraftCardEvent += CollectionManager_OnCraftCard;
        CollectionCardManager.OnCraftCardListEvent += CollectionManager_OnCraftCardList;
        CollectionCardManager.OnRecycleCardEvent += CollectionManager_OnRecycleCard;
        CollectionCardManager.OnRecycleCardListEvent += CollectionManager_OnRecycleCardList;
        CollectionCardManager.OnTransformCardEvent += CollectionManager_OnTransformCard;
    }

    private void RemoveListenerCardCollectionEvent()
    {
        CollectionCardManager.OnCraftCardEvent -= CollectionManager_OnCraftCard;
        CollectionCardManager.OnCraftCardListEvent -= CollectionManager_OnCraftCardList;
        CollectionCardManager.OnRecycleCardEvent -= CollectionManager_OnRecycleCard;
        CollectionCardManager.OnRecycleCardListEvent -= CollectionManager_OnRecycleCardList;
        CollectionCardManager.OnTransformCardEvent -= CollectionManager_OnTransformCard;
    }
    #endregion

    #region PlayerInventory
    private void AddListenerPlayerInventory()
    {
        PlayerInventoryData.OnUpdateCardInventory += UpdateCardSet;
    }

    private void RemoveListenerPlayerInventory()
    {
        PlayerInventoryData.OnUpdateCardInventory -= UpdateCardSet;
    }
    #endregion

    #region Deck
    private void AddListenerDeck()
    {
        CollectionDeckManager.OnSaveNewDeck += DoCreateNewDeck;
    }

    private void RemoveListenerDeck()
    {
        CollectionDeckManager.OnSaveNewDeck -= DoCreateNewDeck;
    }
    #endregion

    #region Mission
    private void AddListenerMission()
    {
        PlayerMissionData.OnSeenMission += DoSeenMission;
    }

    private void RemoveListenerMission()
    {
        PlayerMissionData.OnSeenMission -= DoSeenMission;
    }
    #endregion

    #region In-Game
    private void AddListenerInGameEvents()
    {
        GameManagerController.OnGamePhaseUpdate += OnGamePhaseUpdateEvent;
    }

    private void RemoveListenerInGameEvents()
    {
        GameManagerController.OnGamePhaseUpdate -= OnGamePhaseUpdateEvent;
    }
    #endregion

    #region DataManager
    private void AddListenerDataManagerEvents()
    {
        DataManager.OnCompleteQuest += DataManager_OnCompleteQuest;
        DataManager.OnDailyLogin += DataManager_OnDailyLogin;
        DataManager.OnSetPlayerLevelFaction += DoSetPlayerLevelFaction;
        DataManager.OnSetPlayerLevel += DoSetPlayerLevel;
        DataManager.OnSetSeasonRank += DoSetMostSeasonRank;
        DataManager.OnChangeAvatar += DoChangeAvatar;
        DataManager.OnChangeTitle += DoChangeTitle;
        DataManager.OnCompleteAchievementCardSet += DoCompleteAchievementCardSet;
    }

    private void RemoveListenerDataManagerEvents()
    {
        DataManager.OnCompleteQuest -= DataManager_OnCompleteQuest;
        DataManager.OnDailyLogin -= DataManager_OnDailyLogin;
        DataManager.OnSetPlayerLevelFaction -= DoSetPlayerLevelFaction;
        DataManager.OnSetPlayerLevel -= DoSetPlayerLevel;
        DataManager.OnSetSeasonRank -= DoSetMostSeasonRank;
        DataManager.OnChangeAvatar -= DoChangeAvatar;
        DataManager.OnChangeTitle -= DoChangeTitle;
        DataManager.OnCompleteAchievementCardSet -= DoCompleteAchievementCardSet;
    }
    #endregion

    #region Open Page
    private void AddListenerOpenPageEvents()
    {
        StarExchangeManager.OnOpenPage += DoOpenPage;
        EventManager.OnOpenPage += DoOpenPage;
    }

    private void RemoveListenerOpenPageEvents()
    {
        StarExchangeManager.OnOpenPage -= DoOpenPage;
        EventManager.OnOpenPage -= DoOpenPage;
    }
    #endregion

    #endregion

    #region In-Game Events
    private string GetGameModeKey()
    {
        GameMode mode = GameManager.Instance.Mode;
        string modeKey = string.Empty;
        switch (mode)
        {
            case GameMode.Casual:
            {
                modeKey = StatKeys.CAS.ToString();
            }
            break;

            case GameMode.Rank:
            {
                modeKey = StatKeys.RANK.ToString();
            }
            break;

            case GameMode.Event:
            {
                modeKey = StatKeys.TB.ToString();
            }
            break;

            case GameMode.Friendly:
            {
                modeKey = StatKeys.FM.ToString();
            }
            break;
        }

        return modeKey;
    }

    #region Game Phase
    private void OnGamePhaseUpdateEvent(GamePhase phase)
    {
        switch (phase)
        {
            case GamePhase.Intro:
            {
                _actionCountInMatch.Clear();
            }
            break;

            case GamePhase.Begin:
            {
                _actionCountInTurn.Clear();
            }
            break;
        }
    }
    #endregion

    #region Action In Turn/Match
    private void CountActionInTurn(string actionName, int count = 1)
    {
        int subTurn = GameManager.Instance.SubTurn;
        if (_actionCountInTurn.ContainsKey(subTurn) == false)
        {
            _actionCountInTurn.Add(subTurn, new Dictionary<string, int>());
        }

        if (_actionCountInTurn[subTurn].ContainsKey(actionName) == false)
        {
            _actionCountInTurn[subTurn].Add(actionName, 0);
        }

        _actionCountInTurn[subTurn][actionName] += count;
    }

    private int GetActionCountInTurn(string actionName)
    {
        int subTurn = GameManager.Instance.SubTurn;
        if (_actionCountInTurn.ContainsKey(subTurn))
        {
            if (_actionCountInTurn[subTurn].ContainsKey(actionName))
            {
                return _actionCountInTurn[subTurn][actionName];
            }
        }

        return 0;
    }

    private void CountActionInMatch(string actionName, int count = 1)
    {
        string matchID = DataManager.Instance.GetRoomName();
        if (_actionCountInMatch.ContainsKey(matchID) == false)
        {
            _actionCountInMatch.Add(matchID, new Dictionary<string, int>());
        }

        if (_actionCountInMatch[matchID].ContainsKey(actionName) == false)
        {
            _actionCountInMatch[matchID].Add(actionName, 0);
        }

        _actionCountInMatch[matchID][actionName] += count;
    }

    private int GetActionCountInMatch(string actionName)
    {
        string matchID = DataManager.Instance.GetRoomName();
        if (_actionCountInMatch.ContainsKey(matchID))
        {
            if (_actionCountInMatch[matchID].ContainsKey(actionName))
            {
                return _actionCountInMatch[matchID][actionName];
            }
        }

        return 0;
    }
    #endregion

    #region Card
    public void DoUseCardEvent(MinionData cardData, UnityAction onComplete)
    {
        string modeKey = GetGameModeKey();
        string cardUseKey = StatKeys.C_USE.ToString();
        string cardIDKey = StatKeys.ID.ToString();
        string cardFactionKey = StatKeys.F.ToString();
        string cardSoulKey = StatKeys.S.ToString();
        string cardRarityKey = StatKeys.R.ToString();
        string cardAbilityKey = StatKeys.A.ToString();

        PlayerStatDataList playerStatDatas = new PlayerStatDataList();
        if (string.IsNullOrEmpty(modeKey) == false
            && GameManager.Instance.IsLocalPlayer(cardData.Owner))
        {
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(cardUseKey)));
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(cardUseKey, modeKey)));
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(cardUseKey, modeKey, cardIDKey, cardData.BaseID)));
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(cardUseKey, modeKey, cardFactionKey, cardData.Element.ToCardElementType().ToString())));
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(cardUseKey, modeKey, cardSoulKey, cardData.SpiritCurrent.ToString())));
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(cardUseKey, modeKey, cardRarityKey, cardData.Rarity.ToString())));
            foreach (AbilityFeedback feedback in cardData.FeedbackList)
            {
                playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(cardUseKey, modeKey, cardAbilityKey, feedback.ToString())));
            }

            DoCheckCardAlive();

            if (cardData.IsAbilityChain)
            {
                string useCardChainAction = "USE_CARD_CHAIN";
                CountActionInTurn(useCardChainAction);

                //เล่นการ์ดที่มีความสามารถ chain ตั้งแต่ 2 ตัวขึ้นไปในเทิร์นเดียว
                if (GetActionCountInTurn(useCardChainAction) == 2)
                {
                    playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(cardUseKey, modeKey, "A_CHAIN_STREAK_1M")));
                }
            }
        }

        AddCountPlayerStatAction(CountPlayerStatType.CountPlayerStat, playerStatDatas, onComplete);
    }

    public void DoSummonCardEvent(MinionData cardData, UnityAction onComplete)
    {
        string modeKey = GetGameModeKey();
        string cardSummonKey = StatKeys.C_SUMM.ToString();
        string cardIDKey = StatKeys.ID.ToString();
        string cardFactionKey = StatKeys.F.ToString();
        string cardSoulKey = StatKeys.S.ToString();
        string cardRarityKey = StatKeys.R.ToString();
        string cardAbilityKey = StatKeys.A.ToString();

        PlayerStatDataList playerStatDatas = new PlayerStatDataList();
        if (string.IsNullOrEmpty(modeKey) == false
            && GameManager.Instance.IsLocalPlayer(cardData.Owner))
        {
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(cardSummonKey)));
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(cardSummonKey, modeKey)));
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(cardSummonKey, modeKey, cardIDKey, cardData.BaseID)));
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(cardSummonKey, modeKey, cardFactionKey, cardData.Element.ToCardElementType().ToString())));
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(cardSummonKey, modeKey, cardSoulKey, cardData.SpiritCurrent.ToString())));
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(cardSummonKey, modeKey, cardRarityKey, cardData.Rarity.ToString())));

            DoCheckCardAlive();

            if (cardData.BaseID == "C0345")
            {
                string useC0345Action = "SUMMON_CARD_ID_C0345";
                CountActionInTurn(useC0345Action);

                //เรียกยูนิต 'ต้นไม้ใหญ่' ลงสู่สนามตั้งแต่ 3 ตัวขึ้นไปภายในเทิร์นเดียว
                if (GetActionCountInTurn(useC0345Action) == 3)
                {
                    playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(cardSummonKey, modeKey, "ID_C0345_STREAK_2M")));
                }
            }
        }

        AddCountPlayerStatAction(CountPlayerStatType.CountPlayerStat, playerStatDatas, onComplete);
    }

    public void DoCheckCardAlive()
    {
        string modeKey = GetGameModeKey();
        string cardAliveKey = StatKeys.C_AL.ToString();
        PlayerStatDataList playerStatDatas = new PlayerStatDataList();
        Dictionary<string, int> cardCount = new Dictionary<string, int>();
        List<MinionData> cardList = GameManager.Instance.Data.GetAllMinionsInSlot();
        foreach (MinionData data in cardList)
        {
            if(!cardCount.ContainsKey(data.BaseID))
            {
                cardCount.Add(data.BaseID, 0);
            }
            cardCount[data.BaseID]++;
        }

        if (cardCount.ContainsKey("C0530"))
        {
            //ทำให้บนสนามมียูนิต 'ลมอาฆาต' ของคุณอยู่ตั้งแต่ 4 ตัวขึ้นไป
            if (cardCount["C0530"] > 3)
            {
                playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(cardAliveKey, modeKey, "ID_C0530_3M")));
            }
        }

        AddCountPlayerStatAction(CountPlayerStatType.CountPlayerStat, playerStatDatas, null);
    }

    public void DoDrawCardEvent(PlayerIndex playerIndex, UnityAction onComplete)
    {
        string modeKey = GetGameModeKey();
        string cardDrawKey = StatKeys.C_DRAW.ToString();
        PlayerStatDataList playerStatDatas = new PlayerStatDataList();

        if (string.IsNullOrEmpty(modeKey) == false
            && GameManager.Instance.IsLocalPlayer(playerIndex))
        {
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(cardDrawKey, modeKey)));
        }
        AddCountPlayerStatAction(CountPlayerStatType.CountPlayerStat, playerStatDatas, onComplete);
    }

    public void DoDestroyEnemyCardEvent(MinionData cardData, UnityAction onComplete)
    {
        string modeKey = GetGameModeKey();
        string cardDestroyKey = StatKeys.C_DES.ToString();
        string enemyKey = StatKeys.E.ToString();
        string cardIDKey = StatKeys.ID.ToString();
        PlayerStatDataList playerStatDatas = new PlayerStatDataList();

        if (string.IsNullOrEmpty(modeKey) == false
            && cardData.MinionKillerPlayerIndex != cardData.Owner
            && GameManager.Instance.IsLocalPlayer(cardData.Owner) == false)
        {
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(cardDestroyKey, modeKey, enemyKey)));
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(cardDestroyKey, modeKey, enemyKey, cardIDKey, cardData.BaseID)));

            string destroyEnemyAction = "DESTROY_ENEMY";
            CountActionInTurn(destroyEnemyAction);
            //ทำลายยูนิตศัตรูตั้งแต่ 6 ตัวขึ้นไป ภายในเทิร์นเดียว
            if (GetActionCountInTurn(destroyEnemyAction) == 6)
            {
                playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(cardDestroyKey, modeKey, "E_STREAK_5M")));
            }

            if (GameManager.Instance.CurrentPlayerIndex == cardData.Owner)
            {
                string destroyEnemyInEnemyTurnAction = "DESTROY_ENEMY_IN_ENEMY_TURN";
                CountActionInMatch(destroyEnemyInEnemyTurnAction);
                //สังหารยูนิตศัตรูรวม 20 ตัวในเทิร์นศัตรู ภายในการเล่นครั้งเดียวในโหมดทั่วไปหรือจัดอันดับ
                if (GetActionCountInMatch(destroyEnemyInEnemyTurnAction) == 20)
                {
                    playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(cardDestroyKey, modeKey, "E_MATCH_20")));
                }
            }
        }
        AddCountPlayerStatAction(CountPlayerStatType.CountPlayerStat, playerStatDatas, onComplete);
    }

    public void DoRemoveEnemyCardEvent(MinionData cardData, UnityAction onComplete)
    {
        string modeKey = GetGameModeKey();
        string cardRemoveKey = StatKeys.C_REMOVE.ToString();
        string enemyKey = StatKeys.E.ToString();
        string cardIDKey = StatKeys.ID.ToString();
        PlayerStatDataList playerStatDatas = new PlayerStatDataList();

        if (string.IsNullOrEmpty(modeKey) == false
            && GameManager.Instance.IsLocalPlayer(cardData.Owner) == false)
        {
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(cardRemoveKey, modeKey, enemyKey)));
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(cardRemoveKey, modeKey, enemyKey, cardIDKey, cardData.BaseID)));
        }
        AddCountPlayerStatAction(CountPlayerStatType.CountPlayerStat, playerStatDatas, onComplete);
    }
    #endregion

    #region Hero
    public void DoDealDamageEnemyHeroEvent(PlayerIndex playerIndex, int damage, UnityAction onComplete)
    { 
        string modeKey = GetGameModeKey();
        string heroDamageKey = StatKeys.H_DAM.ToString();
        PlayerStatDataList playerStatDatas = new PlayerStatDataList();

        if (string.IsNullOrEmpty(modeKey) == false
            && GameManager.Instance.IsLocalPlayer(playerIndex) == false)
        {
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(heroDamageKey, modeKey, StatKeys.E.ToString()), damage));
            if (damage > 5)
            {
                playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(heroDamageKey, modeKey, StatKeys.E.ToString(), "5M")));
            }
            if (damage > 7)
            {
                playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(heroDamageKey, modeKey, StatKeys.E.ToString(), "7M")));
            }
        }
        AddCountPlayerStatAction(CountPlayerStatType.CountPlayerStat, playerStatDatas, onComplete);
    }

    public void DoSendEmoteEvent(HeroVoice heroVoiceType, UnityAction onComplete)
    {
        PlayerStatDataList playerStatDatas = new PlayerStatDataList();
        string modeKey = GetGameModeKey();
        string actionName = string.Format("EMO_{0}", heroVoiceType.ToString());

        CountActionInMatch(actionName);

        if (GetActionCountInMatch(actionName) == 1)
        {
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(StatKeys.EMO_MATCH.ToString(), modeKey, heroVoiceType.ToString())));
        }

        AddCountPlayerStatAction(CountPlayerStatType.CountPlayerStat, playerStatDatas, onComplete);
    }
    #endregion

    #region Act for Specific Event
    public void DoActRestoreMinionHPEvent(BattleCardData performer, BattleCardData subject, int value, UnityAction onComplete)
    {
        string modeKey = GetGameModeKey();
        string cardAbilityActiveKey = StatKeys.C_ACT.ToString();
        PlayerStatDataList playerStatDatas = new PlayerStatDataList();

        //ฟื้นฟู HP แก่ยูนิตของคุณ
        if (performer.Owner == subject.Owner && GameManager.Instance.IsLocalPlayer(performer.Owner))
        {
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(cardAbilityActiveKey, modeKey, "M_RESTORE_HP"), value));

            string restoreMinionHP = "RESTORE_HP";
            CountActionInTurn(restoreMinionHP);

            //ฟื้นฟู HP แก่ยูนิตของคุณตั้งแต่ 3 ตัวขึ้นไปภายในเทิร์นเดียว
            if (GetActionCountInTurn(restoreMinionHP) == 3 && subject.Owner == performer.Owner)
            {
                playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(cardAbilityActiveKey, modeKey, "M_RESTORE_HP_STREAK_2M")));
            }
        }

        AddCountPlayerStatAction(CountPlayerStatType.CountPlayerStat, playerStatDatas, onComplete);
    }

    public void DoActBuffMinionFreezeEvent(BattleCardData performer, BattleCardData subject, UnityAction onComplete)
    {
        string modeKey = GetGameModeKey();
        string cardAbilityActiveKey = StatKeys.C_ACT.ToString();
        PlayerStatDataList playerStatDatas = new PlayerStatDataList();

        //ใช้ความสามารถ 'แช่แข็ง'
        if (GameManager.Instance.IsLocalPlayer(performer.Owner))
        {
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(cardAbilityActiveKey, modeKey, "M_BUFFFREEZE")));

            string buffEnemyFreeze = "BUFF_ENEMY_FREEZE";
            CountActionInTurn(buffEnemyFreeze);

            //ใช้ความสามารถ 'แช่แข็ง' แก่ยูนิตศัตรูตั้งแต่ 3 ตัวขึ้นไปภายในเทิร์นเดียว
            if (GetActionCountInTurn(buffEnemyFreeze) == 3 && subject.Owner != performer.Owner)
            {
                playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(cardAbilityActiveKey, modeKey, "M_BUFFFREEZE_E_STREAK_2M")));
            }
        }

        AddCountPlayerStatAction(CountPlayerStatType.CountPlayerStat, playerStatDatas, onComplete);
    }

    public void DoActMinionBerserkEvent(BattleCardData subject, UnityAction onComplete)
    {
        string modeKey = GetGameModeKey();
        string cardAbilityActiveKey = StatKeys.C_ACT.ToString();
        PlayerStatDataList playerStatDatas = new PlayerStatDataList();

        //แสดงผลของความสามารถ Berserk ยูนิตของคุณ
        if (GameManager.Instance.IsLocalPlayer(subject.Owner))
        {
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(cardAbilityActiveKey, modeKey, "M_BERSERK")));

            string berserkActiveAction = "ACTIVE_BERSERK";
            CountActionInTurn(berserkActiveAction);
            // แสดงผลของความสามารถ Berserk ยูนิตของคุณตั้งแต่ 3 ตัวขึ้นไปภายในเทิร์นเดียว
            if (GetActionCountInTurn(berserkActiveAction) == 3)
            {
                playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(cardAbilityActiveKey, modeKey, "M_BERSERK_STREAK_2M")));
            }
        }

        AddCountPlayerStatAction(CountPlayerStatType.CountPlayerStat, playerStatDatas, onComplete);
    }

    public void DoActLockSlotEvent(PlayerIndex owner, UnityAction onComplete)
    {
        string modeKey = GetGameModeKey();
        string cardAbilityActiveKey = StatKeys.C_ACT.ToString();
        PlayerStatDataList playerStatDatas = new PlayerStatDataList();

        //สร้าง Lock บนกระดาน
        if (GameManager.Instance.IsLocalPlayer(owner))
        {
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(cardAbilityActiveKey, modeKey, "M_SLOTLOCK")));
        }

        AddCountPlayerStatAction(CountPlayerStatType.CountPlayerStat, playerStatDatas, onComplete);
    }

    public void DoActBattleAttackEvent(MinionData attacker, List<MinionData> defenderList, UnityAction onComplete)
    {
        string modeKey = GetGameModeKey();
        string cardAbilityActiveKey = StatKeys.C_ACT.ToString();
        PlayerStatDataList playerStatDatas = new PlayerStatDataList();

        //ใช้ยูนิตโจมตีศัตรูตั้งแต่ 4 ตัวขึ้นไปภายในครั้งเดียว
        if (GameManager.Instance.IsLocalPlayer(attacker.Owner))
        {
            if(defenderList.Count > 3)
            {
                playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(cardAbilityActiveKey, modeKey, "M_BTAT_3M")));
            }
        }

        AddCountPlayerStatAction(CountPlayerStatType.CountPlayerStat, playerStatDatas, onComplete);
    }

    public void DoActBattleAttackPiercingEvent(MinionData attacker, List<MinionData> piercingTargetList, UnityAction onComplete)
    {
        string modeKey = GetGameModeKey();
        string cardAbilityActiveKey = StatKeys.C_ACT.ToString();
        PlayerStatDataList playerStatDatas = new PlayerStatDataList();

        //สร้างความเสียหายแก่ยูนิตศัตรูจากความสามารถ Piercing ตั้งแต่ 2 ตัวขึ้นไปในครั้งเดียว
        if (GameManager.Instance.IsLocalPlayer(attacker.Owner))
        {
            if(attacker.IsBePiercing)
            {
                if (piercingTargetList.Count > 1)
                {
                    playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(cardAbilityActiveKey, modeKey, "M_BTAT_PIERCING_1M")));
                }
            }
        }

        AddCountPlayerStatAction(CountPlayerStatType.CountPlayerStat, playerStatDatas, onComplete);
    }

    public void DoActCountBattleAttackPiercingDamageEvent(MinionData attacker, int damage, UnityAction onComplete)
    {
        string modeKey = GetGameModeKey();
        string cardAbilityActiveKey = StatKeys.C_ACT.ToString();
        PlayerStatDataList playerStatDatas = new PlayerStatDataList();

        //สร้างความเสียหายจาก Piercing
        if (GameManager.Instance.IsLocalPlayer(attacker.Owner))
        {
            if (attacker.IsBePiercing)
            {
                playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(cardAbilityActiveKey, modeKey, "M_COUNT_PIERCING_DAM"), damage));
            }
        }

        AddCountPlayerStatAction(CountPlayerStatType.CountPlayerStat, playerStatDatas, onComplete);
    }

    public void DoActBattleDefenseEvent(MinionData defender, int damage, UnityAction onComplete)
    {
        string modeKey = GetGameModeKey();
        string cardAbilityActiveKey = StatKeys.C_ACT.ToString();
        PlayerStatDataList playerStatDatas = new PlayerStatDataList();

        //ใช้ยูนิต 'ยั่วยุ' ของคุณรับความเสียหายมากกว่า 10 หน่วยภายในเทิร์นเดียว
        if (GameManager.Instance.IsLocalPlayer(defender.Owner))
        {
            if (defender.IsAbilityTaunt && damage > 10)
            {
                playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(cardAbilityActiveKey, modeKey, "M_BTDF_TAUNT_DAM_10M")));
            }
        }

        AddCountPlayerStatAction(CountPlayerStatType.CountPlayerStat, playerStatDatas, onComplete);
    }

    public void DoActBattleDefenseCountEvent(MinionData defender, int defendedCount, UnityAction onComplete)
    {
        string modeKey = GetGameModeKey();
        string cardAbilityActiveKey = StatKeys.C_ACT.ToString();
        PlayerStatDataList playerStatDatas = new PlayerStatDataList();

        //ใช้ Taunt ยูนิตของคุณรับการโจมตีจากยูนิตศัตรูตั้งแต่ 5 ตัวขึ้นไปในเทิร์นเดียว
        if (GameManager.Instance.IsLocalPlayer(defender.Owner))
        {
            if (defender.IsAbilityTaunt && defendedCount > 4)
            {
                playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(cardAbilityActiveKey, modeKey, "M_BTDFC_TAUNT_COUNT_4M")));
            }
        }

        AddCountPlayerStatAction(CountPlayerStatType.CountPlayerStat, playerStatDatas, onComplete);
    }

    public void DoActDealEffectDamageEvent(MinionData attacker, List<MinionData> defenderList, int damage, UnityAction onComplete)
    {
        string modeKey = GetGameModeKey();
        string cardAbilityActiveKey = StatKeys.C_ACT.ToString();
        PlayerStatDataList playerStatDatas = new PlayerStatDataList();

        //สร้างความเสียหายแก่ยูนิตศัตรูจากความสามารถของยูนิตตั้งแต่ 6 ตัวขึ้นไปในเทิร์นเดียว
        int enemyCount = 0;
        if (GameManager.Instance.IsLocalPlayer(attacker.Owner))
        {
            foreach(MinionData defender in defenderList)
            {
                if(defender.Owner != attacker.Owner)
                {
                    enemyCount++;
                }
            }

            string dealEffectDamageAction = "DEAL_EFFECT_DAMAGE";
            CountActionInTurn(dealEffectDamageAction, enemyCount);

            if (GetActionCountInTurn(dealEffectDamageAction) == 6)
            {
                playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(cardAbilityActiveKey, modeKey, "M_DED_E_STREAK_5M")));
            }
        }

        AddCountPlayerStatAction(CountPlayerStatType.CountPlayerStat, playerStatDatas, onComplete);
    }

    public void DoActMoveMinionToSlotEvent(BattleCardData performer, BattleCardData subject, UnityAction onComplete)
    {
        string modeKey = GetGameModeKey();
        string cardAbilityActiveKey = StatKeys.C_ACT.ToString();
        PlayerStatDataList playerStatDatas = new PlayerStatDataList();

        //เคลื่อนย้ายยูนิตของคุณ
        if (GameManager.Instance.IsLocalPlayer(performer.Owner)
            && performer.Owner == subject.Owner)
        {
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(cardAbilityActiveKey, modeKey, "M_MOVE")));
        }

        AddCountPlayerStatAction(CountPlayerStatType.CountPlayerStat, playerStatDatas, onComplete);
    }

    public void DoActSpawnDarkMatterEvent(BattleCardData performer, UnityAction onComplete)
    {
        string modeKey = GetGameModeKey();
        string cardAbilityActiveKey = StatKeys.C_ACT.ToString();
        PlayerStatDataList playerStatDatas = new PlayerStatDataList();

        //สร้าง สสารมืด
        if (GameManager.Instance.IsLocalPlayer(performer.Owner))
        {
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(cardAbilityActiveKey, modeKey, "M_DARKMATTER")));

            string spawnDarkMatterAction = "SPAWN_DARKMATTER";
            CountActionInTurn(spawnDarkMatterAction);
            //สร้าง สสารมืด บนกระดานตั้งแต่ 4 ช่องขึ้นไปภายในเทิร์นเดียว
            if (GetActionCountInTurn(spawnDarkMatterAction) == 4)
            {
                playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(cardAbilityActiveKey, modeKey, "M_DARKMATTER_STREAK_3M")));
            }
        }

        AddCountPlayerStatAction(CountPlayerStatType.CountPlayerStat, playerStatDatas, onComplete);
    }

    public void DoActCheckMinionSPEvent(MinionData subject, UnityAction onComplete)
    {
        string modeKey = GetGameModeKey();
        string cardAbilityActiveKey = StatKeys.C_ACT.ToString();
        PlayerStatDataList playerStatDatas = new PlayerStatDataList();

        if (GameManager.Instance.IsLocalPlayer(subject.Owner))
        {
            //ทำให้ยูนิต 'ต้นไม้ใหญ่' ของคุณบนสนามมีค่าโซลเท่ากับ 1
            if(subject.SpiritCurrent == 1
                && subject.BaseID == "C0345")
            {
                playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(cardAbilityActiveKey, modeKey, "M_SET_S_1_ID_C0345")));
            }
        }

        AddCountPlayerStatAction(CountPlayerStatType.CountPlayerStat, playerStatDatas, onComplete);
    }
    #endregion

    #region EndGame
    private PlayerStatDataList GetEndGameResultStatData(bool isVictory)
    {
        PlayerStatDataList playerStatDatas = new PlayerStatDataList();

        // set key
        string modeKey = string.Empty;
        string turnKey = StatKeys.TURN.ToString();
        string playKey = StatKeys.P.ToString();
        string resultKey = isVictory ? StatKeys.W.ToString() : StatKeys.L.ToString();
        string heroKey = StatKeys.H.ToString();
        string factionKey = StatKeys.F.ToString();
        string titleKey = StatKeys.T.ToString();
        string endResultKey = StatKeys.ER.ToString();
        string minLoseKey = StatKeys.MIN.ToString();

        // end game type
        GameManager.EndGameType endGameType = GameManager.Instance.EndType;
        string endGameTypeStr = endGameType.ToString().ToUpper();

        // safe to leave type
        if (endGameType == GameManager.EndGameType.SafeToLeave)
        {
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(playKey, StatKeys.STL.ToString())));
            return playerStatDatas;
        }

        // gather info
        int localTurn = GameManager.Instance.LocalTurn;
        PlayerIndex localPlayerIndex = GameManager.Instance.GetLocalPlayerIndex();
        BattlePlayerData playerBattleData = GameManager.Instance.Data.PlayerDataList[(int)localPlayerIndex];
        GameMode mode = GameManager.Instance.Mode;

        // current PlayerTier
        PlayerTierType playerTier = DataManager.Instance.PlayerInfo.GetPlayerTier();

        // match type
        bool isPlayOnline = GameManager.Instance.IsNetworkPlay()
                        || (GameManager.Instance.IsVSBot() && GameManager.Instance.IsHaveTimer());

        // minimum play turn
        bool isPlayMinimum = GameManager.Instance.SubTurn >= GameManager.Instance.MinSubTurnStatCount;

        // player hp
        int playerHP = playerBattleData.CurrentHP;

        // get hero info
        string rawHeroID = playerBattleData.Hero.ID;
        string realHeroID = string.Empty;
        string[] heroIDArr = rawHeroID.Split('_');
        if (heroIDArr.Length > 1)
        {
            realHeroID = rawHeroID.Split('_')[0];
        }

        switch (mode)
        {
            case GameMode.Tutorial_1:
            case GameMode.Tutorial_2:
            case GameMode.Tutorial_3:
            case GameMode.Tutorial_4:
            {
                string gameModeStr = mode.ToString();
                string tutIndex = gameModeStr.Split('_')[1];
                modeKey = StatKeys.TUT.ToString() + "_" + tutIndex;
            }
            break;

            case GameMode.Adventure:
            {
                AdventureController adventureController = GameManager.Instance.Controller as AdventureController;
                AdventureStageID stageID = adventureController.StageID;
                modeKey = StatKeys.ADV.ToString() + "_" + stageID;
            }
            break;

            case GameMode.Casual:
            {
                modeKey = StatKeys.CAS.ToString();
                if (isPlayMinimum && !isVictory) // จะได้ EXP เมื่อเล่น "แพ้" อย่างน้อย 7 Subturn
                {
                    playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(resultKey, modeKey, minLoseKey)));
                }
                playerStatDatas.AddPlayerStatData(CountPlayerTierStat(mode, playerTier, isVictory));
            }
            break;

            case GameMode.Rank:
            {
                modeKey = StatKeys.RANK.ToString();
                if (isPlayMinimum && !isVictory) // จะได้ EXP เมื่อเล่น "แพ้" อย่างน้อย 7 Subturn
                {
                    playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(resultKey, modeKey, minLoseKey)));
                }
                playerStatDatas.AddPlayerStatData(CountPlayerTierStat(mode, playerTier, isVictory));
            }
            break;

            case GameMode.Friendly:
            {
                modeKey = StatKeys.FM.ToString();
            }
            break;

            case GameMode.Arena:
            {
                modeKey = StatKeys.ARN.ToString();
            }
            break;

            case GameMode.Event:
            {
                modeKey = StatKeys.TB.ToString();
            }
            break;

            case GameMode.Practice:
            {
                modeKey = StatKeys.PRA.ToString();
            }
            break;
        }

        // not-valid mode to count stats
        if (string.IsNullOrEmpty(modeKey))
        {
            return playerStatDatas;
        }

        // calculate stat
        playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(turnKey), localTurn));
        playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(playKey, StatKeys.ANY.ToString())));
        playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(resultKey, StatKeys.ANY.ToString())));
        playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(playKey, isPlayOnline ? StatKeys.PVP.ToString() : StatKeys.PVE.ToString())));
        playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(resultKey, isPlayOnline ? StatKeys.PVP.ToString() : StatKeys.PVE.ToString())));

        playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(turnKey, modeKey), localTurn));
        playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(playKey, modeKey)));
        playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(resultKey, modeKey)));

        // hero
        playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(playKey, modeKey, heroKey, rawHeroID)));
        playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(resultKey, modeKey, heroKey, rawHeroID)));
        if (string.IsNullOrEmpty(realHeroID) == false)
        {
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(playKey, modeKey, heroKey, realHeroID)));
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(resultKey, modeKey, heroKey, realHeroID)));
        }

        // faction
        playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(playKey, modeKey, factionKey, playerBattleData.Hero.ElementID)));
        playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(resultKey, modeKey, factionKey, playerBattleData.Hero.ElementID)));

        // title
        playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(playKey, modeKey, titleKey, DataManager.Instance.PlayerInfo.TitleID)));
        playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(resultKey, modeKey, titleKey, DataManager.Instance.PlayerInfo.TitleID)));

        // end result
        playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(playKey, modeKey, endResultKey, endGameTypeStr)));
        playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(playKey, modeKey, endResultKey, endGameTypeStr, heroKey, rawHeroID)));
        if (string.IsNullOrEmpty(realHeroID) == false)
        {
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(playKey, modeKey, endResultKey, endGameTypeStr, heroKey, realHeroID)));
        }

        // player hp
        if (playerHP > 5)
        {
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(resultKey, modeKey, StatKeys.HP.ToString(), "5M")));
        }
        if (playerHP > 10)
        {
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(resultKey, modeKey, StatKeys.HP.ToString(), "10M")));
        }
        if (playerHP > 15)
        {
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(resultKey, modeKey, StatKeys.HP.ToString(), "15M")));
        }
        if (playerHP > 20)
        {
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(resultKey, modeKey, StatKeys.HP.ToString(), "20M")));
        }
        if (playerHP < 5)
        {
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(resultKey, modeKey, StatKeys.HP.ToString(), "5L")));
        }
        if(playerHP == 1)
        {
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(resultKey, modeKey, StatKeys.HP.ToString(), "1E")));
        }

        #region Unit

        #region Battlefield
        BattleData battleData = GameManager.Instance.Data;
        List<MinionData> cardList = battleData.GetAllMinionsInSlot();
        int isTauntCount = 0;
        int isBerserkActiveCount = 0;
        int isAbilityBackstabCount = 0;
        int isAbilityAuraSoul3Count = 0;
        int isLinkCount = 0;
        int meAliveUnitCount = 0;
        int enemyAliveUnitCount = 0;
        foreach(MinionData card in cardList)
        {
            if(card.IsAlive == false)
            {
                continue;
            }

            if (card.Owner == localPlayerIndex)
            {
                //Taunt ยูนิตของคุณเหลือรอดอยู่บนสนาม
                if (card.IsAbilityTaunt)
                {
                    isTauntCount++;
                }

                //มียูนิตของคุณที่แสดงผล Berserk เหลือรอดอยู่บนสนาม
                if (card.IsBerserkActive)
                {
                    isBerserkActiveCount++;
                }

                //มียูนิต Backstab ของคุณเหลือรอดอยู่บนสนาม
                if (card.IsAbilityBackstab)
                {
                    isAbilityBackstabCount++;
                }

                //มี Aura ยูนิตโซล(3)ของคุณเหลือรอดอยู่บนสนาม
                if (card.IsAbilityAura && card.SpiritCurrent == 3)
                {
                    isAbilityAuraSoul3Count++;
                }

                //มี Link ยูนิตของคุณหลือรอดอยู่บนสนาม
                if (card.IsAbilityLink)
                {
                    isLinkCount++;
                }

                //มียูนิตของคุณบนสนาม
                meAliveUnitCount++;
            }
            else
            {
                //มียูนิตของศัตรูบนสนาม
                enemyAliveUnitCount++;
            }
        }

        //Taunt ยูนิตของคุณเหลือรอดอยู่บนสนาม
        if (isTauntCount > 0)
        {
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(resultKey, modeKey, StatKeys.M.ToString(), StatKeys.C.ToString(), "TAUNT")));
        }

        //มียูนิตของคุณที่แสดงผล Berserk เหลือรอดอยู่บนสนาม
        if (isBerserkActiveCount > 0)
        {
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(resultKey, modeKey, StatKeys.M.ToString(), StatKeys.C.ToString(), "BERSERKING")));

            //มียูนิตของคุณที่แสดงผล Berserk ตั้งแต่ 2 ตัวขึ้นไป
            if (isBerserkActiveCount > 1)
            {
                playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(resultKey, modeKey, StatKeys.M.ToString(), StatKeys.C.ToString(), "BERSERKING_1M")));
            }
        }

        //มียูนิต Backstab ของคุณเหลือรอดอยู่บนสนาม
        if (isAbilityBackstabCount > 0)
        {
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(resultKey, modeKey, StatKeys.M.ToString(), StatKeys.C.ToString(), "BACKSTAB")));
        }

        //มี Aura ยูนิตโซล(3)ของคุณเหลือรอดอยู่บนสนาม
        if (isAbilityAuraSoul3Count > 0)
        {
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(resultKey, modeKey, StatKeys.M.ToString(), StatKeys.C.ToString(), "AURA_S3")));

            //มี Aura ยูนิตโซล(3)ของคุณเหลือรอดอยู่บนสนาม 2 ตัวขึ้นไป
            if (isAbilityAuraSoul3Count > 1)
            {
                playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(resultKey, modeKey, StatKeys.M.ToString(), StatKeys.C.ToString(), "AURA_S3_1M")));
            }
        }

        //มียูนิตของคุณเหลือรอดอยู่บนสนาม
        if (meAliveUnitCount > 0)
        {
            //มียูนิตของคุณตั้งแต่ 7 ตัวขึ้นไปเหลือรอดอยู่บนสนาม
            if (meAliveUnitCount > 6)
            {
                playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(resultKey, modeKey, StatKeys.M.ToString(), StatKeys.C.ToString(), "6M")));
            }

            //มียูนิตของคุณตั้งแต่ 10 ตัวขึ้นไปเหลือรอดอยู่บนสนาม
            if (meAliveUnitCount > 9)
            {
                playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(resultKey, modeKey, StatKeys.M.ToString(), StatKeys.C.ToString(), "9M")));
            }
        }

        //มียูนิตของคุณบนสนามน้อยกว่ายูนิตศัตรู
        if (meAliveUnitCount < enemyAliveUnitCount)
        {
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(resultKey, modeKey, StatKeys.M.ToString(), StatKeys.C.ToString(), "LESS_E")));
        }

        //มี Link ยูนิตของคุณเหลือรอดอยู่บนสนาม
        if (isLinkCount > 0)
        {
            //มี Link ยูนิตของคุณมากกว่า 2 ตัวเหลือรอดอยู่บนสนาม
            if (isLinkCount > 2)
            {
                playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(resultKey, modeKey, StatKeys.M.ToString(), StatKeys.C.ToString(), "LINK_2M")));
            }

            //มี Link ยูนิตของคุณมากกว่า 4 ตัวเหลือรอดอยู่บนสนาม
            if (isLinkCount > 4)
            {
                playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(resultKey, modeKey, StatKeys.M.ToString(), StatKeys.C.ToString(), "LINK_4M")));
            }
        }
        #endregion

        #region Graveyard
        List<BattleCardData> cardGraveyardList = playerBattleData.CardList[CardZone.Graveyard];
        //มียูนิตอยู่ในสุสานของคุณตั้งแต่ 30 ตัวขึ้นไป
        if (cardGraveyardList.Count > 29)
        {
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(resultKey, modeKey, StatKeys.M.ToString(), StatKeys.C.ToString(), StatKeys.GY.ToString(), "29M")));
        }
        //มีการ์ดในสุสานของคุณไม่ถึง 20 ใบ
        if (cardGraveyardList.Count < 20)
        {
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(resultKey, modeKey, StatKeys.M.ToString(), StatKeys.C.ToString(), StatKeys.GY.ToString(), "20L")));
        }

        isAbilityBackstabCount = 0;
        foreach(BattleCardData data in cardGraveyardList)
        {
            if(data.Type == CardType.Minion || data.Type == CardType.Minion_NotInDeck)
            {
                MinionData minion = data as MinionData;
                if(minion.IsAbilityBackstab)
                {
                    isAbilityBackstabCount++;
                }
            }
        }

        if(isAbilityBackstabCount > 0)
        {
            //มียูนิต Backstab อยู่ในสุสานของคุณตั้งแต่ 10 ตัวขึ้นไป
            if (isAbilityBackstabCount > 9)
            {
                playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(resultKey, modeKey, StatKeys.M.ToString(), StatKeys.C.ToString(), StatKeys.GY.ToString(), "BACKSTAB_9M")));
            }
        }
        #endregion

        #region Deck
        List<BattleCardData> cardDeckList = playerBattleData.CardList[CardZone.Deck];
        //ไม่มีการ์ดเหลือในกอง
        if (cardDeckList.Count == 0)
        {
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(resultKey, modeKey, StatKeys.M.ToString(), StatKeys.C.ToString(), StatKeys.DECK.ToString(), "0")));
        }
        #endregion

        #endregion

        #region Slot
        List<SlotData> slotList = battleData.SlotDatas.ToList();
        int slotLockCount = 0;
        foreach (SlotData slot in slotList)
        {
            //มีช่อง Lock ของคุณบนสนาม
            if (slot.IsBeUnlock == false)
            {
                if (slot.IsSlotLockBeMined(localPlayerIndex))
                {
                    slotLockCount++;
                }
            }
        }

        //มีช่อง Lock ของคุณมากกว่า 1 ช่องบนสนาม
        if (slotLockCount > 1)
        {
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(resultKey, modeKey, StatKeys.M.ToString(), StatKeys.SL.ToString(), "LOCK_1M")));
        }

        //มีช่อง Lock ของคุณมากกว่า 3 ช่องบนสนาม
        if (slotLockCount > 3)
        {
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(resultKey, modeKey, StatKeys.M.ToString(), StatKeys.SL.ToString(), "LOCK_3M")));
        }
        #endregion

        #region Deck
        List<CardElementType> elementList = new List<CardElementType>();
        int soul3CardCount = 0;
        bool isDuplicated = false;
        DeckData deck = DataManager.Instance.GetDeck(playerBattleData.Deck.DeckID);
        foreach (KeyValuePair<string, int> item in deck.Deck)
        {
            if (item.Value > 1)
            {
                isDuplicated = true;
            }

            CardData cardData = CardData.CreateCard(item.Key);
            CardElementType type = cardData.CardElement.ToCardElementType();
            if (elementList.Contains(type) == false)
            {
                elementList.Add(type);
            }
            if(cardData.Spirit == 3)
            {
                soul3CardCount += item.Value;
            }
        }

        // ใช้ชุดการ์ดที่มีแต่ยูนิตโซล 3
        if (soul3CardCount == deck.Deck.CountAmount)
        {
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(resultKey, modeKey, StatKeys.M.ToString(), StatKeys.DECK.ToString(), "U_SOUL3")));
        }

        //ใช้ชุดการ์ดที่ไม่ซ้ำกันเลย
        if (isDuplicated == false)
        {
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(resultKey, modeKey, StatKeys.M.ToString(), StatKeys.DECK.ToString(), "UNIQ")));
        }

        //ใช้ชุดการ์ดที่มีแต่สังกัด Mercenary เท่านั้น
        bool isOnlyNeutral = true;
        foreach (CardElementType item in elementList)
        {
            if (item != CardElementType.NEUTRAL)
            {
                isOnlyNeutral = false;
            }
        }
        if (isOnlyNeutral == true)
        {
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(resultKey, modeKey, StatKeys.M.ToString(), StatKeys.DECK.ToString(), "U_NEUTRAL")));
        }
        #endregion

        return playerStatDatas;
    }

    private PlayerStatDataList CountPlayerTierStat(GameMode mode, PlayerTierType playerTier, bool isVictory)
    {
        Dictionary<string, int> currentPlayerStat = DataManager.Instance.PlayerInfo.GetPlayerStatData();
        PlayerStatDataList result = new PlayerStatDataList();
        string loser1Key = StatKeys.LOSER_1_STACK.ToString();
        string loser2Key = StatKeys.LOSER_2_STACK.ToString();
        string newbieKey = StatKeys.NEWBIE_CAS.ToString();
        int loser1Count = 1;
        int loser2Count = 1;

        switch (playerTier)
        {
            case PlayerTierType.Newbie1:
            case PlayerTierType.Newbie2:
            case PlayerTierType.Newbie3:
            case PlayerTierType.Newbie4:
            case PlayerTierType.Newbie5:
            case PlayerTierType.Newbie6:
            {
                if(mode == GameMode.Casual)
                {
                    if (isVictory)
                    {
                        // decrease newbie (-3)
                        result.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(newbieKey), -3));
                    }
                    else
                    {
                        // increase newbie (+1)
                        result.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(newbieKey), 1));
                    }
                }
            }
            break;

            case PlayerTierType.Player:
            {
                if (isVictory)
                {
                    if (currentPlayerStat.ContainsKey(loser1Key))
                    {
                        loser1Count = -(currentPlayerStat[loser1Key]);
                    }
                }

                result.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(loser1Key), loser1Count));
            }
            break;

            case PlayerTierType.Loser1:
            {
                if (isVictory)
                {
                    if (currentPlayerStat.ContainsKey(loser1Key))
                    {
                        loser1Count = -(currentPlayerStat[loser1Key]);
                    }
                    if (currentPlayerStat.ContainsKey(loser2Key))
                    {
                        loser2Count = -(currentPlayerStat[loser2Key]);
                    }

                    result.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(loser1Key), loser1Count));
                    result.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(loser2Key), loser2Count));
                }
                else
                {
                    result.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(loser2Key), loser2Count));
                }
            }
            break;

            case PlayerTierType.Loser2:
            {
                if (isVictory)
                {
                    if (currentPlayerStat.ContainsKey(loser2Key))
                    {
                        loser2Count = -(currentPlayerStat[loser2Key]);
                    }
                }

                result.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(loser2Key), loser2Count));
            }
            break;
        }

        return result;
    }

    public void DoUpdatePlayerMatchTicket(string matchID, UnityAction onComplete)
    {
        DataManager.Instance.UpdatePlayerMatchTicket(
            GetEndGameResultStatData(false)
            , matchID
            , onComplete
        );
    }

    public void DoClearPlayerMatchTicket(string matchID, UnityAction onComplete)
    {
        DataManager.Instance.ClearPlayerMatchTicket(
            matchID
            , onComplete
        );
    }

    public void DoEndGameResultEvent(bool isVictory, UnityAction onComplete)
    {
        AddCountPlayerStatAction(
              CountPlayerStatType.CountPlayerStatMatch
            , GetEndGameResultStatData(isVictory)
            , onComplete
        );
    }
    #endregion

    #endregion

    #region Out-Game Events

    #region Card
    private void CollectionManager_OnCraftCard(CatalogCardData cardData)
    {
        PlayerStatDataList playerStatDatas = new PlayerStatDataList();
        playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(StatKeys.C.ToString(), StatKeys.CRAFT.ToString())));

        CardData data;
        if (CardData.GetCardData(cardData.ItemID, out data))
        {
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(StatKeys.C.ToString(), StatKeys.CRAFT.ToString(), StatKeys.F.ToString(), data.CardElement.ToCardElementType().ToString())));
        }
        AddCountPlayerStatAction(CountPlayerStatType.CountPlayerStat, playerStatDatas);
    }

    private void CollectionManager_OnCraftCardList(List<CatalogCardData> cardItemList)
    {
        PlayerStatDataList playerStatDatas = new PlayerStatDataList();
        int count = cardItemList.Count;
        playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(StatKeys.C.ToString(), StatKeys.CRAFT.ToString()), count));
        AddCountPlayerStatAction(CountPlayerStatType.CountPlayerStat, playerStatDatas);
    }

    private void CollectionManager_OnRecycleCard(CatalogCardData cardData)
    {
        PlayerStatDataList playerStatDatas = new PlayerStatDataList();
        playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(StatKeys.C.ToString(), StatKeys.RECYC.ToString())));

        CardData data;
        if (CardData.GetCardData(cardData.ItemID, out data))
        {
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(StatKeys.C.ToString(), StatKeys.RECYC.ToString(), StatKeys.F.ToString(), data.CardElement.ToCardElementType().ToString())));
        }
        AddCountPlayerStatAction(CountPlayerStatType.CountPlayerStat, playerStatDatas);
    }

    private void CollectionManager_OnRecycleCardList(List<CatalogCardData> cardItemList)
    {
        PlayerStatDataList playerStatDatas = new PlayerStatDataList();
        int count = cardItemList.Count;
        playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(StatKeys.C.ToString(), StatKeys.RECYC.ToString()), count));
        AddCountPlayerStatAction(CountPlayerStatType.CountPlayerStat, playerStatDatas);
    }

    private void CollectionManager_OnTransformCard(CatalogCardData cardData)
    {
        PlayerStatDataList playerStatDatas = new PlayerStatDataList();
        playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(StatKeys.C.ToString(), StatKeys.TRANS.ToString())));

        CardData data;
        if (CardData.GetCardData(cardData.ItemID, out data))
        {
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(StatKeys.C.ToString(), StatKeys.TRANS.ToString(), StatKeys.F.ToString(), data.CardElement.ToCardElementType().ToString())));
        }
        AddCountPlayerStatAction(CountPlayerStatType.CountPlayerStat, playerStatDatas);
    }
    #endregion

    #region Card Pack
    private PackReward _cachePackReward;
    private void Inventory_OnStartSystem(PackReward packDetail)
    {
        _cachePackReward = new PackReward(packDetail);
    }

    private void Inventory_OnOpenAllPack(PackReward packDetail)
    {
        if (_cachePackReward == null) return;

        PlayerStatDataList playerStatDatas = new PlayerStatDataList();
        for (int i = 0; i < _cachePackReward.PackResultList.Count; i++)
        {
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(StatKeys.CP.ToString(), StatKeys.OPEN.ToString())));
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(StatKeys.CP.ToString(), StatKeys.OPEN.ToString(), StatKeys.ID.ToString(), _cachePackReward.PackID)));
            int commonAmount = _cachePackReward.GetRarityAmount(i, CardRarity.Common);
            if (commonAmount > 0)
            {
                playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(StatKeys.CP.ToString(), StatKeys.G_COMM.ToString(), commonAmount.ToString())));
            }
        }
        AddCountPlayerStatAction(CountPlayerStatType.CountPlayerStat, playerStatDatas);
    }

    private void Inventory_OnOpenPack()
    {
        if (_cachePackReward == null) return;

        List<CardData> cardList = _cachePackReward.GetCardDataListByIndex(0);
        if (cardList == null)
            return;

        PlayerStatDataList playerStatDatas = new PlayerStatDataList();
        playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(StatKeys.CP.ToString(), StatKeys.OPEN.ToString())));
        playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(StatKeys.CP.ToString(), StatKeys.OPEN.ToString(), StatKeys.ID.ToString(), _cachePackReward.PackID)));
        int commonAmount = _cachePackReward.GetRarityAmount(0, CardRarity.Common);
        if (commonAmount > 0)
        {
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(StatKeys.CP.ToString(), StatKeys.G_COMM.ToString(), commonAmount.ToString())));
        }
        AddCountPlayerStatAction(CountPlayerStatType.CountPlayerStat, playerStatDatas);

        _cachePackReward.RemoveList(0);
    }
    #endregion

    #region Shop
    private void ShopManager_OnPurchaseComplete(ShopItemData item)
    {
        PlayerStatDataList dataList = new PlayerStatDataList();
        if (item.IsInAppPurchase)
        {
            dataList.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(StatKeys.RM_PAY.ToString())));
        }

        bool isDailyDealItem = DataManager.Instance.GetDailyDealRecord().ItemID == item.ItemId;
        if (isDailyDealItem)
        {
            dataList.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(StatKeys.PC.ToString(), StatKeys.DL_DEAL.ToString())));
        }

        if(item.IsPack)
        {
            if (item.Currency == VirtualCurrency.CO)
            {
                dataList.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(StatKeys.PC.ToString(), StatKeys.CP.ToString(), "VC_CO")));
            }
        }

        AddCountPlayerStatAction(CountPlayerStatType.CountPlayerStat, dataList);
    }

    private void ShopManager_OnClaimDailyFree(DealRecord item)
    {
        PlayerStatDataList dataList = new PlayerStatDataList();
        dataList.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(StatKeys.PC.ToString(), StatKeys.DL_FREE.ToString())));
        AddCountPlayerStatAction(CountPlayerStatType.CountPlayerStat, dataList);
    }
    #endregion

    #region Review Events
    private void AddListenerGameManagerEvent()
    {
        GameManager.OnEndGameEvent += Review_OnEndGame;
    }

    private void RemoveListenerGameManagerEvent()
    {
        GameManager.OnEndGameEvent -= Review_OnEndGame;
    }

    private void Review_OnEndGame(GameMode mode, bool isWin, GameManager.EndGameType endGameType)
    {
        bool isCanShowReview = true;
        int reviewCount = DataManager.Instance.PlayerInfo.GetPlayerStat(StatKeys.REVIEW_SHOW.ToString());

        if (reviewCount <= 0)
        {
            isCanShowReview &= (mode == GameMode.Rank || mode == GameMode.Casual);
            isCanShowReview &= isWin;
            isCanShowReview &= (endGameType == GameManager.EndGameType.Normal);

            int winCount_Rank = DataManager.Instance.PlayerInfo.GetPlayerStat(StatKeys.W.ToString(), StatKeys.RANK.ToString());
            int winCount_Casual = DataManager.Instance.PlayerInfo.GetPlayerStat(StatKeys.W.ToString(), StatKeys.CAS.ToString());

            isCanShowReview &= (winCount_Rank > 0);
            isCanShowReview &= (winCount_Casual > 0);
            isCanShowReview &= ((winCount_Rank % 5 == 0) || (winCount_Casual % 5 == 0));

            if (isCanShowReview)
            {
                if (GameHelper.ShowReview())
                {
                    CountReviewStat(null, null);
                }
            }
        }

        // Show Facebook At level 4
        bool isLevelReached = DataManager.Instance.PlayerInfo.LevelData.Level >= 4;
        int facebookStat = DataManager.Instance.PlayerInfo.GetPlayerStat(StatKeys.FACEBOOK_SEEN.ToString());
        if (isLevelReached && facebookStat == 0)
        {
            GameHelper.ShowFacebookPopupFirstTime();
            CountFacebookBannerStat(null, null);
        }
    }

    private void CountReviewStat(UnityAction onComplete, UnityAction<string> onFail)
    {
        PlayerStatDataList dataList = new PlayerStatDataList();
        dataList.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(StatKeys.REVIEW_SHOW.ToString())));
        AddCountPlayerStatAction(CountPlayerStatType.CountPlayerStat, dataList);
    }

    private void CountFacebookBannerStat(UnityAction onComplete, UnityAction<string>  onFail)
    {
        PlayerStatDataList dataList = new PlayerStatDataList();
        dataList.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(StatKeys.FACEBOOK_SEEN.ToString())));
        AddCountPlayerStatAction(CountPlayerStatType.CountPlayerStat, dataList);
    }

    public void CountFactionLevelSeenStat(UnityAction onComplete, UnityAction<string> onFail)
    {
        PlayerStatDataList dataList = new PlayerStatDataList();
        dataList.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(StatKeys.FL_SEEN.ToString())));
        AddCountPlayerStatAction(CountPlayerStatType.CountPlayerStat, dataList);
    }
    #endregion

    #region Quest Event
    private void DataManager_OnCompleteQuest(QuestData quest)
    {
        PlayerStatDataList playerStatDatas = new PlayerStatDataList();
        switch (quest.Type)
        {
            case QuestTypes.Event:
            {
                playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(StatKeys.Q.ToString(), StatKeys.EVENT.ToString(), StatKeys.COMP.ToString())));
            }
            break;

            case QuestTypes.SeasonPassMainDaily:
            {
                playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(StatKeys.Q.ToString(), StatKeys.SSP.ToString(), StatKeys.DL.ToString(), StatKeys.COMP.ToString())));
                playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(StatKeys.Q.ToString(), StatKeys.SSP.ToString(), StatKeys.DL.ToString(), StatKeys.COMP.ToString(), StatKeys.ID.ToString(), quest.ID)));
            }
            break;

            case QuestTypes.SeasonPassMainWeekly:
            {
                playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(StatKeys.Q.ToString(), StatKeys.SSP.ToString(), StatKeys.WL.ToString(), StatKeys.COMP.ToString())));
                playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(StatKeys.Q.ToString(), StatKeys.SSP.ToString(), StatKeys.WL.ToString(), StatKeys.COMP.ToString(), StatKeys.ID.ToString(), quest.ID)));
            }
            break;

            case QuestTypes.SeasonPassMainWeek:
            {
                playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(StatKeys.Q.ToString(), StatKeys.SSP.ToString(), StatKeys.WEEK.ToString(), StatKeys.COMP.ToString())));
            }
            break;

            case QuestTypes.SeasonPassChallenge:
            {
                playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(StatKeys.Q.ToString(), StatKeys.SSP.ToString(), StatKeys.CHA.ToString(), StatKeys.COMP.ToString())));
            }
            break;
        }
        AddCountPlayerStatAction(CountPlayerStatType.CountPlayerStat, playerStatDatas);
    }
    #endregion

    #region Daily Login
    private void DataManager_OnDailyLogin()
    {
        PlayerStatDataList playerStatDatas = new PlayerStatDataList();
        playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(StatKeys.LOGIN.ToString(), StatKeys.DL.ToString())));
        AddCountPlayerStatAction(CountPlayerStatType.CountPlayerStat, playerStatDatas);
    }
    #endregion

    #region Card Set
    private void UpdateCardSet()
    {
        Debug.Log("UpdateCardSet !!");

        CardListData cardListData = DataManager.Instance.InventoryData.CardInventoryList;
        List<CardData> cardDataList = new List<CardData>();
        foreach (KeyValuePair<string, int> card in cardListData)
        {
            cardDataList.Add(CardData.CreateCard(card.Key));
        }

        Dictionary<string, int> currentStat = DataManager.Instance.PlayerInfo.GetPlayerStatData();
        PlayerStatDataList playerStatDatas = new PlayerStatDataList();
        string statKey = "";
        foreach (CardData card in cardDataList)
        {
            statKey = string.Format("{0}_{1}", StatKeys.C_SET.ToString(), card.BaseID);
            if (currentStat.ContainsKey(statKey))
            {
                continue;
            }

            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(StatKeys.C_SET.ToString(), card.BaseID)));
        }

        AddCountPlayerStatAction(CountPlayerStatType.CountPlayerStatCardSet, playerStatDatas);
    }
    #endregion

    #region Mission
    private void DoSeenMission(string missionID, UnityAction onComplete = null)
    {
        PlayerStatDataList playerStatDatas = new PlayerStatDataList();
        playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(StatKeys.MISSION.ToString(), StatKeys.SEEN.ToString(), missionID)));
        AddCountPlayerStatAction(CountPlayerStatType.CountPlayerStat, playerStatDatas, onComplete);
    }
    #endregion

    #region Deck
    public void DoCreateNewDeck()
    {
        PlayerStatDataList playerStatDatas = new PlayerStatDataList();
        playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(StatKeys.DECK.ToString(), StatKeys.CREATE.ToString())));
        AddCountPlayerStatAction(CountPlayerStatType.CountPlayerStat, playerStatDatas);
    }
    #endregion

    #region Level Faction
    public void DoSetPlayerLevelFaction()
    {
        PlayerStatDataList playerStatDatas = new PlayerStatDataList();
        Dictionary<CardElementType, LevelFactionData> dataList = DataManager.Instance.PlayerInfo.LevelFactionData.GetLevelFactionDataList();
        int mostLevel = 0;
        foreach (KeyValuePair<CardElementType, LevelFactionData> item in dataList)
        {
            if (item.Value.Level > mostLevel)
            {
                mostLevel = item.Value.Level;
            }
            playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(StatKeys.LV.ToString(), StatKeys.F.ToString(), item.Key.ToString()), item.Value.Level));
        }
        playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(StatKeys.LV.ToString(), StatKeys.F.ToString()), mostLevel));
        AddCountPlayerStatAction(CountPlayerStatType.CountPlayerStatSet, playerStatDatas);
    }
    #endregion

    #region Level
    public void DoSetPlayerLevel()
    {
        PlayerStatDataList playerStatDatas = new PlayerStatDataList();
        playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(StatKeys.LV.ToString()), DataManager.Instance.PlayerInfo.LevelData.Level));
        AddCountPlayerStatAction(CountPlayerStatType.CountPlayerStatSet, playerStatDatas);
    }
    #endregion

    #region Season Rank
    public void DoSetMostSeasonRank()
    {
        PlayerStatDataList playerStatDatas = new PlayerStatDataList();
        List<PlayerRankData> dataList = DataManager.Instance.PlayerInfo.GetPlayerRankDataList();
        int mostRank = 0;
        foreach (PlayerRankData item in dataList)
        {
            if (item.MaxRankIndex > mostRank)
            {
                mostRank = item.MaxRankIndex;
            }
        }
        playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(StatKeys.SS.ToString(), StatKeys.RANK.ToString()), mostRank));
        AddCountPlayerStatAction(CountPlayerStatType.CountPlayerStatSet, playerStatDatas);
    }
    #endregion

    #region Out-Game Page
    private void DoOpenPage(string pageName)
    {
        PlayerStatDataList playerStatDatas = new PlayerStatDataList();
        playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(StatKeys.OPN.ToString(), StatKeys.PG.ToString(), pageName)));
        AddCountPlayerStatAction(CountPlayerStatType.CountPlayerStatSet, playerStatDatas);
    }
    #endregion

    #region Achievement Card Set
    private void DoCompleteAchievementCardSet(string id)
    {
        PlayerStatDataList playerStatDatas = new PlayerStatDataList();
        playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(StatKeys.ACH.ToString(), StatKeys.CSET.ToString(), StatKeys.COMP.ToString())));
        AddCountPlayerStatAction(CountPlayerStatType.CountPlayerStat, playerStatDatas);
    }
    #endregion

    #region Avatar
    private void DoChangeAvatar(string oldAvatar, string newAvatar)
    {
        PlayerStatDataList playerStatDatas = new PlayerStatDataList();
        playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(StatKeys.AVATAR.ToString(), StatKeys.CHANGE.ToString())));
        AddCountPlayerStatAction(CountPlayerStatType.CountPlayerStatSet, playerStatDatas);
    }
    #endregion

    #region Title
    private void DoChangeTitle(string oldTitle, string newTitle)
    {
        PlayerStatDataList playerStatDatas = new PlayerStatDataList();
        playerStatDatas.AddPlayerStatData(new PlayerStatData(new PlayerStatKey(StatKeys.TITLE.ToString(), StatKeys.CHANGE.ToString())));
        AddCountPlayerStatAction(CountPlayerStatType.CountPlayerStatSet, playerStatDatas);
    }
    #endregion

    #endregion

    #endregion

    #region Queue
    private void AddCountPlayerStatAction(CountPlayerStatType type, PlayerStatDataList playerStatDataList, UnityAction onComplete = null)
    {
        CountPlayerStatAction action = new CountPlayerStatAction(type, playerStatDataList, onComplete);

        if (_queue != null && _queue.Count > 0)
        {
            CountPlayerStatAction latestAction = _queue[_queue.Count - 1];

            if (latestAction.IsActionEqual(action))
            {
                latestAction.DataList.AddPlayerStatData(playerStatDataList);

                Debug.LogFormat("AddCountPlayerStatAction merged {0}", _queue.Count);
                return;
            }
        }

        this.Add(action);
        Debug.LogFormat("AddCountPlayerStatAction {0}", _queue.Count);
    }

    protected override void OnActiveComplete(int uniqueKey)
    {
        base.OnActiveComplete(uniqueKey);

        Debug.LogFormat("OnActiveComplete {0}", _queue.Count);

        // Trigger count player stat
        if (PlayerStatManager.OnCountPlayerStatCompleted != null)
        {
            PlayerStatManager.OnCountPlayerStatCompleted.Invoke();
        }
    }
    #endregion
}