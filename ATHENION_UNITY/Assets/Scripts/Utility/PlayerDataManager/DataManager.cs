﻿using Newtonsoft.Json;
using PlayFab;
using PlayFab.ClientModels;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Linq;

#if UNITY_ANDROID
using UnityEngine.Android;
#endif

#region Data Class
[Serializable]
public class PackReward
{
    public string PackID { get; private set; }
    public int Amount { get; private set; }
    public List<PackResult> PackResultList { get; private set; }
    public List<string> BonusReward { get; private set; }

    private List<Dictionary<CardRarity, int>> rarityCountDict = new List<Dictionary<CardRarity, int>>();

    public PackReward(string packID, int openAmount, List<PackResult> packResultList, List<string> bonusReward)
    {
        this.PackID = packID;
        this.Amount = openAmount;

        this.PackResultList = packResultList;
        this.BonusReward = bonusReward;

        rarityCountDict = CalculateRarityCountDict(PackResultList);
    }

    public PackReward(PackReward data)
    {
        this.PackID = data.PackID;
        this.Amount = data.Amount;

        this.PackResultList = new List<PackResult>(data.PackResultList);
        this.BonusReward = new List<string>(data.BonusReward);
        rarityCountDict = CalculateRarityCountDict(PackResultList);
    }

    private List<Dictionary<CardRarity, int>> CalculateRarityCountDict(List<PackResult> packResultList)
    {
        List<Dictionary<CardRarity, int>> result = new List<Dictionary<CardRarity, int>>();
        for (int i = 0; i < packResultList.Count; i++)
        {
            List<CardData> cardDataList = GetCardDataListByIndex(i);
            result.Add(CalculateRarityAmount(cardDataList));
        }
        return result;
    }

    private Dictionary<CardRarity, int> CalculateRarityAmount(List<CardData> cardDataList)
    {
        Dictionary<CardRarity, int> result = new Dictionary<CardRarity, int>();
        for (int i = 0; i < cardDataList.Count; i++)
        {
            CardRarity rarity = cardDataList[i].Rarity;
            if (!result.ContainsKey(rarity))
                result.Add(rarity, 0);
            result[rarity] += 1;
        }
        return result;
    }

    public List<CurrencyReward> GetAllCurrencyRewards()
    {
        if (PackResultList == null)
            return null;
        StarItemDB starItemDB = DataManager.Instance.GetStarItemDB();

        List<CurrencyReward> result = new List<CurrencyReward>();
        for (int i = 0; i < PackResultList.Count; i++)
        {
            for (int j = 0; j < PackResultList[i].ItemList.Count; j++)
            {
                string itemID = PackResultList[i].ItemList[j].BaseID;
                result.Add(starItemDB.GetStarItem(itemID));
            }
        }
        return result;
    }

    public List<CardData> GetAllCardDataList()
    {
        if (PackResultList == null)
            return null;

        List<CardData> result = new List<CardData>();
        for (int i = 0; i < PackResultList.Count; i++)
        {
            List<CardData> cardDataList = GetCardDataListByIndex(i);
            if (cardDataList != null)
            {
                result.AddRange(cardDataList);
            }
        }

        return result;
    }

    public int GetRarityAmount(int packIndex, CardRarity rarity)
    {
        if (packIndex > rarityCountDict.Count - 1)
            return 0;
        if (rarityCountDict[packIndex].ContainsKey(rarity) == false)
            return 0;
        return rarityCountDict[packIndex][rarity];
    }

    public List<CardData> GetCardDataListByIndex(int index)
    {
        if (PackResultList == null || index > PackResultList.Count - 1)
            return null;

        List<CardData> result = new List<CardData>();
        for (int i = 0; i < PackResultList[index].CardList.Count; i++)
        {
            CardData card = null;
            if (CardData.GetCardData(PackResultList[index].CardList[i].BaseID, out card))
            {
                result.Add(card);
            }
        }
        return result;
    }

    public void RemoveList(int index)
    {
        PackResultList.RemoveAt(index);
        rarityCountDict.RemoveAt(index);
    }
}

[Serializable]
public class PackResult
{
    /// <summary>
    /// All of items from container.
    /// </summary>
    public List<ItemReward> ItemResultList { get; private set; }
    /// <summary>
    /// Get only Card List from container.
    /// </summary>
    public List<ItemReward> CardList { get; private set; }
    /// <summary>
    /// Get only item list from container.
    /// </summary>
    public List<ItemReward> ItemList { get; private set; }

    public PackResult(List<ItemReward> itemResultList)
    {
        this.ItemResultList = itemResultList;
        CardList = CreateCardList(itemResultList);
        ItemList = CreateItemList(itemResultList);
    }

    private List<ItemReward> CreateCardList(List<ItemReward> itemResultList)
    {
        List<ItemReward> result = new List<ItemReward>();
        for (int i = 0; i < itemResultList.Count; i++)
        {
            if (itemResultList[i].IsCard)
                result.Add(itemResultList[i]);
        }
        return result;
    }

    private List<ItemReward> CreateItemList(List<ItemReward> itemResultList)
    {
        List<ItemReward> result = new List<ItemReward>();
        for (int i = 0; i < itemResultList.Count; i++)
        {
            if (itemResultList[i].IsCard == false)
                result.Add(itemResultList[i]);
        }
        return result;
    }
}

[Serializable]
public abstract class ItemReward
{
    public string BaseID { get; private set; }

    public abstract bool IsNew { get; }
    public abstract bool IsCard { get; }
    public abstract CardRarity Rarity { get; }
    public abstract int Value { get; }

    public ItemReward(string baseID)
    {
        BaseID = baseID;
    }
}

[Serializable]
public class CardReward : ItemReward
{
    private CardData _cardData;
    public CardReward(string id) : base(id)
    {
        _cardData = CardData.CreateCard(id);
    }

    public override CardRarity Rarity => _cardData.Rarity;


    public override bool IsNew
    {
        get
        {
            CardListData cardList = DataManager.Instance.InventoryData.CardInventoryList;
            return !cardList.ContainKey(BaseID);
        }
    }

    public override bool IsCard => true;

    public override int Value { get { return _cardData.Spirit; } }
}


[Serializable]
public class CurrencyReward : ItemReward
{
    public string BundleID { get; private set; }
    public string ItemID { get { return _detail.ItemID; } }
    public int Amount { get { return _detail.Amount; } }

    public override bool IsNew => false;
    public override bool IsCard => false;
    public override int Value => 0;

    public override CardRarity Rarity => _rarity;
    private CardRarity _rarity;

    private ItemData _detail;

    public CurrencyReward(string bundleID, string json) : base(bundleID)
    {
        Dictionary<string, object> data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);

        BundleID = bundleID;

        CardRarity rarity;
        GameHelper.StrToEnum(data["rarity"].ToString(), out rarity);
        _rarity = rarity;

        _detail = new ItemData(data["item_detail"].ToString());
    }
}

public struct DealRecord
{
    public string ItemID { get; private set; }
    public string TimestampStr { get; private set; }
    public string TimeExpireStr { get; private set; }
    public bool IsRedeem { get; private set; }
    public Dictionary<string, ItemData> ItemList { get; private set; }

    #region Constructors
    public DealRecord(string itemID, string timestampStr, string timeExpireStr, bool isRedeem)
    {
        ItemID = itemID;
        TimestampStr = timestampStr;
        TimeExpireStr = timeExpireStr;
        IsRedeem = isRedeem;
        ItemList = new Dictionary<string, ItemData>();
    }
    #endregion

    #region Methods
    public void SetItemList(Dictionary<string, ItemData> itemList)
    {
        ItemList = new Dictionary<string, ItemData>(itemList);
    }

    public override string ToString()
    {
        return string.Format(
              "[{0}, {1}, {2}, {3}]"
            , ItemID
            , TimestampStr
            , TimeExpireStr
            , IsRedeem
        );
    }

    public List<ItemData> ToListItemData()
    {
        List<ItemData> resultList = new List<ItemData>();
        foreach (var item in ItemList)
        {
            resultList.Add(item.Value);
        }
        return resultList;
    }
    #endregion
}

#region FriendMode
public struct InviteFriendModeRequest
{
    public string PlayerID { get; private set; }
    public string Region { get; private set; }
    public string RoomName { get; private set; }

    public InviteFriendModeRequest(string playerID, PhotonRegion region, string roomName)
    {
        PlayerID = playerID;
        Region = Karamucho.Network.MatchingManager.GetServerRegion(region);
        RoomName = roomName;
    }
}

public struct FriendMatchData
{
    #region Enums
    public enum StatusType { None, Receiver, Sender }
    #endregion

    #region Public Properties
    public string PlayerID { get; private set; }
    public StatusType Status { get; private set; }
    public DateTime TimeStampUTC { get; private set; }
    public string RoomName { get; private set; }
    public PhotonRegion Region { get; private set; }
    #endregion

    #region Private Properties
    #endregion

    #region Contructors
    public FriendMatchData(string playerID, string status, DateTime timeStamp, string roomName, string region)
    {
        PlayerID = playerID;
        Status = ConvertStrToStatusType(status);
        TimeStampUTC = timeStamp;
        RoomName = roomName;
        Region = Karamucho.Network.MatchingManager.GetServerRegion(region);
    }

    public FriendMatchData(string playerID, Dictionary<string, string> rawData)
    {
        PlayerID = playerID;
        Status = ConvertStrToStatusType(rawData["status"]);
        TimeStampUTC = Convert.ToDateTime(rawData["timestamp"]).ToUniversalTime();
        RoomName = rawData["roomName"];
        Region = Karamucho.Network.MatchingManager.GetServerRegion(rawData["region"]);
    }
    #endregion

    private static StatusType ConvertStrToStatusType(string statusStr)
    {
        switch (statusStr.ToLower())
        {
            case "receiver": return StatusType.Receiver;
            case "sender": return StatusType.Sender;

            default: return StatusType.None;
        }
    }

    public string GetDebugText()
    {
        return JsonConvert.SerializeObject(this, Formatting.Indented);
    }
}
#endregion

#endregion

public class DataManager : MonoSingleton<DataManager>
{
    #region Enums
    public enum SaveIndex
    {
        Deck
        , Language
        , ScreenRatio
        , DebugLog
        , RoomName
    }
    #endregion

    #region Save Keys
    // Deck
    private const string _playerDeckKey = "PlayerDeckKey";
    private const string _playerDeckSlotKey = "PlayerDeckSlotKey";
    private const string _playerDeckIDListKey = "PlayerDeckIDListKey";
    private const string _playerCurrentDeckKey = "PlayerCurrentDeckKey";
    private const string _recentDeckSelection = "RecentDeckSelectionKey";

    private const string _playerLanguageKey = "PlayerLanguageKey";
    private const string _bgmOnKey = "BGMOnSettingKey";
    private const string _sfxOnKey = "SFXOnSettingKey";
    private const string _voiceOnKey = "VoiceOnSettingKey";
    private const string _tutorialSkipKey = "TutorialSkipKey";

    private const string _fpsSettingKey = "FPSSettingKey";
    private const string _gameQualityKey = "GameQualityKey";
    private const string _screenRatioKey = "ScreenRatioKey";
    private const string _debugLogKey = "DebugLogKey";
    private const string _roomNameKey = "RoomNameKey";
    private const string _setDeckFirstTimeKey = "SetDeckFirstTimeTestKey";

    // Spacial
    private const string _welcomeCBTKey = "welcomeCBTKey";
    #endregion

    #region Playfab Keys
    // Game Value
    private const string _titleGameValuePlayfabKey = "game_value";

    // Profile
    private const string _playerRenameStatusPlayfabKey = "player_rename_status";

    private const string _playerDeckPropertiesPlayfabKey = "deck_properties";

    private const string _playerSeenCardPlayfabKey = "seen_list_card";
    private const string _playerSeenAvatarPlayfabKey = "seen_list_avatar";
    private const string _playerSeenTitlePlayfabKey = "seen_list_title";
    private const string _playerSeenHeroPlayfabKey = "seen_list_hero";
    private const string _playerSeenCardBackPlayfabKey = "seen_list_card_back";
    private const string _playerSeenDockPlayfabKey = "seen_list_dock";
    private const string _playerSeenSeasonPassQuestPlayfabKey = "seen_list_sp_quest";
    private const string _playerSeenPromotionPlayfabKey = "seen_list_promotion";

    private const string _playerAvatarIDPlayfabKey = "player_avatar_id";
    private const string _playerTitleIDPlayfabKey = "player_title_id";

    private const string _acceptedTermPrivacyPlayfabKey = "accepted_term_condition_privacy";

    // Tutorial
    private const string _playerTutorialStatusKey = "player_tutorial_status";
    private const string _playerRedeemTutorialKey = "player_claim_tutorial_status";
    private const string _titleTutorialRewardKey = "tutorial_reward";

    // Stat
    private const string _titlePlayerStatKeysPlayfabKey = "player_stat_key_data";

    // Record Match
    private const string _recordMatchPlayersKey = "record_match_player_id_list";

    // Quest
    private const string _titleQuestDBDataPlayfabKey = "quest_db";
    private const string _titleQuestListDataPlayfabKey = "quest_list";

    // Event Quest
    private const string _playerEventQuestPlayfabKey = "quest_event";
    private const string _playerSubEventQuestPlayfabKey = "quest_sub_event";

    // Season Pass
    private const string _titleSeasonPassDBDataPlayfabKey = "season_pass_db";
    private const string _titleSeasonPassTierDataPlayfabKey = "season_pass_tier";
    private const string _playerSeasonPassDataPlayfabKey = "season_pass_data";
    private const string _titleSeasonPassTierRewardDataPlayfabKey = "season_pass_tier_reward";
    private const string _playerSeasonPassClaimedRecordPlayfabKey = "season_pass_tier_reward_claimed";
    private const string _playerSeasonPassQuestPlayfabKey = "season_pass_quest";
    private const string _titleSeasonPassPriceDBDataPlayfabKey = "season_pass_price";

    // Achievement
    private const string _titleAchievementDBDataPlayfabKey = "achievement_db";
    private const string _playerAchievementCompletedPropertiesPlayfabKey = "achievement_completed_properties";

    // Achievement Card Set
    private const string _titleAchievementCardSetDBDataPlayfabKey = "achievement_card_set_db";
    private const string _playerAchievementCardSetCompletedPropertiesPlayfabKey = "achievement_card_set_completed_properties";

    // Message Inbox
    private const string _playerMessageInboxDataPlayfabKey = "message_inbox";
    private const string _playerMessageInboxStatusPlayfabKey = "message_inbox_status";
    private const string _titleMessageInboxAnnouncementDataPlayfabKey = "message_inbox_announcement";

    // Match Reward
    private const string _titleMatchDataPlayfabKey = "match_reward";
    private const string _titleMatchExpKey = "match_exp";
    private const string _titleMatchRankKey = "match_rank";

    // Player Exp/Level
    private const string _titlePlayerExpDataPlayfabKey = "player_exp";
    private const string _playerExpPlayfabKey = "player_exp";
    private const string _titleLevelRewardDataPlayfabKey = "reward_level";
    private const string _playerLevelRewardRecordPlayfabKey = "reward_level_claimed";

    // Player Exp/Level Faction
    private const string _titlePlayerExpFactionDataPlayfabKey = "player_exp_faction";
    private const string _playerExpFactionPlayfabKey = "player_exp_faction";
    private const string _titleLevelFactionRewardDataPlayfabKey = "reward_level_faction";
    private const string _playerLevelFactionRewardRecordPlayfabKey = "reward_level_faction_claimed";

    // Season Rank Reward
    private const string _titleRankRewardDataPlayfabKey = "season_rank_reward";

    // Feature Unlock
    private const string _titleFeautureUnlockPlayfabKey = "feature_unlock";
    private const string _playerFeatureUnlockPlayfabKey = "feature_unlock_record";

    private const string _validStoreIDPlayfabKey = "valid_store_id";
    private const string _validServerVersionPlayfabKey = "valid_version";
    private const string _whiteListPlayerPlayfabKey = "white_list_player";
    private const string _validServerSettingKey = "valid_server_setting";
    //private const string _pathAssetBundlePlayfabKey = "path_asset_bundle";

    // Card Store
    private const string _titleCardPriceOverridePlayfabKey = "card_price_override_db";
    private const string _playerCardPriceOverridePlayfabKey = "card_price_override";

    // Store
    public const string _titleStoreDataPlayfabKey = "store_data";

    // Event Battle DB
    private const string _titleEventBattleDBPlayfabKey = "event_battle_db";

    // shop limited
    public const string _shopLimitedKey = "player_purchase_limited";

    // StarPack
    private const string _starItemDBKey = "star_pack_item_db";

    // Banner
    private const string _bannerDBKey = "banner_db";
    private const string _bannerShopDBKey = "banner_shop_db";

    // Game Event
    private const string _gameEventKey = "event_db";
    private const string _storeEventKey = "store_event";

    //Adventure
    private readonly string[] _adventureKeyList = new string[]
    {
        "adventure_db_AT"
    };
    private const string _rewardedAdvKey = "rewarded_adv";

    // Log
    private const string _cardChangeLogPlayfabKey = "card_change_log";

    // Collection
    private const string _deckDBPlayfabKey = "deck_db";

    // WildOffer
    private const string _playerWildOffer = "player_wild_offer_data";

    //Referral
    private const string _playerRegisteredReferralCodeKey = "referred_status";

    //Weekly Rank Reward
    private const string _weeklyRankRewardDBKey = "weekly_rank_reward_db";
    private const string _weeklyRankRewardStatus = "weekly_rank_reward_status";

    //Mission
    private const string _titleMissionDBDataPlayfabKey = "mission_db";
    private const string _playerMissionDataPlayfabKey = "mission_data";
    #endregion

    #region Default Properties
    private float _screenRatioDefault = GameHelper.QualityLowValue;

    // Default Cosmetic
    public readonly List<string> DefaultAvatarIDList = new List<string>() { "AV0001", "AV0002" };
    public readonly List<string> DefaultHeroIDList = new List<string>() { "H0001", "H0002", "H0003", "H0004", "H0005", "H0006" };
    public readonly List<string> DefaultCardBackIDList = new List<string>() { "CB0000" };
    public readonly List<string> DefaultDockIDList = new List<string>() { "HUD0000" };
    public readonly List<string> DefaultTitleIDList = new List<string>() { "TT0000" };
    #endregion

    #region Public Properties
    public bool CanPlayRankMode { get { return DateTimeData.GetDateTimeUTC() < GetSeasonInfoData(GetCurrentSeasonIndex()).EndDateTimeUTC; } }

    #region PlayerData
    public PlayerInfoData PlayerInfo { get { return _playerInfoData; } }
    public PlayerInfoData LastMatchPlayerInfo { get { return _lastMatchPlayerInfoData; } }
    public PlayerAccountInfoData PlayerAccountInfo { get { return _playerAccountInfoData; } }
    public PlayerInventoryData InventoryData { get { return _playerInventoryData; } }
    public PlayerEventQuestData PlayerEventQuest { get { return _playerEventQuestData; } }
    public PlayerAchievementData PlayerAchievement { get { return _playerAchievementData; } }
    public PlayerAchievementCardSet PlayerAchievementCardSet { get { return _playerAchievementCardSet; } }
    public PlayerLevelRewardRecordData PlayerLevelRewardRecord { get { return _playerLevelRewardRecordData; } }
    public PlayerLevelFactionRewardRecordData PlayerLevelFactionRewardRecord { get { return _playerLevelFactionRewardRecordData; } }
    public PlayerCardPriceOverrideData PlayerCardPriceOverride { get { return _playerCardPriceOverrideData; } }
    public ReferredInfoData PlayerReferredInfoData { get; private set; } = new ReferredInfoData();

    public PlayerSeasonPassData PlayerSeasonPass { get { return _playerSeasonPassData; } }
    public PlayerSeasonPassQuestData PlayerSeasonPassQuest { get { return _playerSeasonPassQuestData; } }
    public PlayerSeasonPassData PlayerSeasonPassOld { get { return _playerSeasonPassDataOld; } }
    public PlayerSeasonPassQuestData PlayerSeasonPassQuestOld { get { return _playerSeasonPassQuestDataOld; } }
    public PlayerSeasonPassTierRewardRecordData PlayerSeasonPassRewardRecord { get { return _playerSeasonPassRewardRecordData; } }

    public PlayerMissionData PlayerMission { get { return _playerMissionData; } }
    public Dictionary<string, FeatureUnlockData> PlayerFeatureUnlock { get { return _playerFeatureUnlockDict; } }
    #endregion

    #region Events
    public delegate void DataManagerHandler();
    public static event DataManagerHandler OnUpdateInventory;
    #endregion

    #endregion

    #region Private Properties
    private UnityEvent _onVIPStatusUpdate = null;
    private int _maxLoadCallForGetPlayerInfo = 12;
    private bool _isCardRecycleCompleted = true;
    private bool _isCardCraftCompleted = true;
    private bool _isCardTransformCompleted = true;
    private bool _isCardRecycleAllCompleted = true;
    private bool _isCardCraftAllCompleted = true;
    #endregion

    #region Private Cached Properties
    // Game Value
    private Dictionary<string, object> _gameValue = new Dictionary<string, object>();

    // Player Data Cache
    private bool _isPlayerRenamed = false;
    private PlayerInfoData _playerInfoData = new PlayerInfoData();
    private PlayerInfoData _lastMatchPlayerInfoData = new PlayerInfoData();
    private PlayerAccountInfoData _playerAccountInfoData = new PlayerAccountInfoData();
    private PlayerInventoryData _playerInventoryData = new PlayerInventoryData();
    private PlayerEventQuestData _playerEventQuestData = new PlayerEventQuestData();
    private PlayerAchievementData _playerAchievementData = new PlayerAchievementData();
    private PlayerAchievementCardSet _playerAchievementCardSet = new PlayerAchievementCardSet();
    private PlayerLevelRewardRecordData _playerLevelRewardRecordData = new PlayerLevelRewardRecordData();
    private PlayerLevelFactionRewardRecordData _playerLevelFactionRewardRecordData = new PlayerLevelFactionRewardRecordData();
    private PlayerCardPriceOverrideData _playerCardPriceOverrideData = new PlayerCardPriceOverrideData();

    private PlayerSeasonPassData _playerSeasonPassData = new PlayerSeasonPassData();
    private PlayerSeasonPassQuestData _playerSeasonPassQuestData = new PlayerSeasonPassQuestData();
    private PlayerSeasonPassData _playerSeasonPassDataOld = new PlayerSeasonPassData();
    private PlayerSeasonPassQuestData _playerSeasonPassQuestDataOld = new PlayerSeasonPassQuestData();
    private PlayerSeasonPassTierRewardRecordData _playerSeasonPassRewardRecordData = new PlayerSeasonPassTierRewardRecordData();

    private PlayerMissionData _playerMissionData = new PlayerMissionData();

    // PlayerStat
    private List<string> _playerStatKeys = new List<string>();

    // Tutorial Status
    private bool _isPlayerRedeemReward = false;
    private bool _isPlayerRedeemReward2 = false;
    private bool[] _redeemTutorialRewardStatusList = new bool[4];

    private Dictionary<int, List<ItemData>> _tutorialRewardDataList = new Dictionary<int, List<ItemData>>();

    // Friend and Player
    private List<FriendInfoData> _friendInfoDataList = new List<FriendInfoData>();
    private List<FriendInfoData> _friendInfoDataListTmp = new List<FriendInfoData>();
    private DateTime _friendInfoDataListUpdateTimestamp = DateTime.Now;
    private List<string> _friendIDList = new List<string>();
    private Dictionary<string, FriendStatusData> _friendStatusDataList = new Dictionary<string, FriendStatusData>();

    private List<string> _lastMatchPlayerIDListCache = new List<string>();
    private List<string> _lastMatchPlayerIDList = new List<string>();
    private List<PlayerInfoData> _lastMatchPlayerInfoDataList = new List<PlayerInfoData>();
    private List<FriendMatchData> _friendMatchDataList = new List<FriendMatchData>();

    //Referral
    private List<ReferreeInfoData> _referreeInfoDataList = new List<ReferreeInfoData>();

    // NUF ( New User Flow )
    private Dictionary<PlayerActivity, int> _playerNUF = new Dictionary<PlayerActivity, int>();

    // Event
    private GameEventDB _gameEventDB;
    private Dictionary<string, GameEventInfo> _gameEventData = new Dictionary<string, GameEventInfo>();
    private ShopEventData _shopEventData;

    // Season
    private SeasonData _seasonData = new SeasonData();

    // Feature Unlock
    private Dictionary<string, FeatureUnlockData> _playerFeatureUnlockDict = new Dictionary<string, FeatureUnlockData>();
    private Dictionary<string, FeatureUnlockData> _titleFeatureUnlockDict = new Dictionary<string, FeatureUnlockData>();

    // Inbox Message
    private Dictionary<string, object> _titleMessageInboxAnnouncementData = new Dictionary<string, object>();
    private Dictionary<string, object> _playerMessageInboxData = new Dictionary<string, object>();
    private Dictionary<string, List<string>> _playerMessageInboxStatus = new Dictionary<string, List<string>>();
    private List<MessageInboxData> _messageInboxDataList = new List<MessageInboxData>();

    // Store & Catalog
    private Dictionary<string, CatalogItemData> _catalogItemDataList = new Dictionary<string, CatalogItemData>();

    // Shop
    private DealRecord _dailyDealRecord = new DealRecord();
    private DealRecord _freeDealRecord = new DealRecord();
    private DealRecord _weeklyDealRecord = new DealRecord();

    private Dictionary<ShopTypes, ShopTypeData> _shopTypeDataList = new Dictionary<ShopTypes, ShopTypeData>();
    private Dictionary<ShopTypes, List<string>> _storeIDTypeLoader = new Dictionary<ShopTypes, List<string>>();

    public bool FirstTopupStatus { get; set; }

    // Game Data
    public GameMode LastGameMode { get; set; }
    public bool IsWinLastMatch { get; set; }

    // Open Pack
    private List<List<ItemReward>> _cardPackRewardList = new List<List<ItemReward>>();

    // Special Login Reward
    private SpecialLoginData _specialLoginData;

    // Daily Login Reward
    private DailyLoginData _dailyLoginData;

    // Card Change Log Data
    private CardChangeLogData _cardChangeLogData;

    // Star Pack DB
    private StarItemDB _starItemDB;
    // Star Exchange Data
    private StarExchangeData _starExchangeData;

    // Event Battle Setting
    private bool _isEnableEventBattle = false;

    // Utilities
    private ClientVersion _version;
    private List<string> _whiteList;
    private Dictionary<RuntimePlatform, Dictionary<string, ATNVersion>> _serverSetting;
    private Dictionary<string, object> _playerSave = new Dictionary<string, object>();

    //Adventure
    private Dictionary<AdventureChapterID, List<AdventureStageData>> _adventureDataDict;
    private Dictionary<string, bool> _rewardedAdventureDict;

    //Weekly Rank Reward
    private WeeklyRankRewardData _weeklyRankRewardData = new WeeklyRankRewardData();
    #endregion

    #region Event
    public static event Action<AchievementData> OnCompleteAchievement;
    public static event Action<QuestData> OnCompleteQuest;
    public static event Action<string> OnMissionComplete;
    public static event Action OnSpecialLogin; 
    public static event Action OnDailyLogin;
    public static event Action OnSetPlayerLevel;
    public static event Action OnSetPlayerLevelFaction;
    public static event Action<bool> OnUpdateClaimTutorialReward;

    public static event Action<CardElementType> OnPlayerLevelFactionIncrease;
    public static event Action OnSetSeasonRank;
    public static event Action<string, string> OnChangeAvatar;
    public static event Action<string, string> OnChangeTitle;
    public static event Action<string> OnCompleteAchievementCardSet;
    public static event Action OnRefreshSeasonPassQuest;
    public static event Action OnApplyReferral;
    public static event Action OnClaimedReferral;
    public static event Action OnSeasonUpdate;
    public static event Action OnUpdateFeatureUnlock;

    public static event Action OnWildOfferAdded;
    #endregion

    #region Methods

    #region UnityCallback
    private void OnEnable()
    {
        GameHelper.SubscribeLogMessageReceived();
    }

    private void OnDisable()
    {
        GameHelper.UnsubscribeLogMessageReceived();
    }

    private void OnApplicationQuit()
    {
        // save seen items
        UpdatePlayerSeenData();
    }

    private void OnApplicationPause(bool pause)
    {
        if (pause && !GameHelper.IsInLoginScene())
        {
            // save seen items
            UpdatePlayerSeenData();
        }
    }
    #endregion

    #region Clear Methods
    public static void ClearAll()
    {
        //PlayerPrefs.DeleteKey(_playerLanguageKey);

        PlayerPrefs.DeleteKey("WO005Emot");
        PlayerPrefs.DeleteKey("WO006Emot");
        PlayerPrefs.DeleteKey("WO007Emot");
        PlayerPrefs.DeleteKey("WO008Emot");
        PlayerPrefs.DeleteKey("WO009Emot");
        PlayerPrefs.DeleteKey("WO010Emot");

        PlayerPrefs.DeleteKey(_playerDeckKey);
        PlayerPrefs.DeleteKey(_playerDeckSlotKey);
        PlayerPrefs.DeleteKey(_playerDeckIDListKey);
        PlayerPrefs.DeleteKey(_playerCurrentDeckKey);
        PlayerPrefs.DeleteKey(_tutorialSkipKey);

        PlayerPrefs.DeleteKey(_debugLogKey);
        PlayerPrefs.DeleteKey(_roomNameKey);
        PlayerPrefs.DeleteKey(_setDeckFirstTimeKey);

        PlayerPrefs.DeleteKey(_welcomeCBTKey);
        PlayerPrefs.DeleteKey(_recentDeckSelection);
    }

    public static void ClearSave(SaveIndex saveIndex)
    {
        switch (saveIndex)
        {
            case SaveIndex.Deck:
                PlayerPrefs.DeleteKey(_playerDeckKey);
                PlayerPrefs.DeleteKey(_playerDeckSlotKey);
                PlayerPrefs.DeleteKey(_playerDeckIDListKey);
                PlayerPrefs.DeleteKey(_playerCurrentDeckKey);
                break;
            case SaveIndex.Language: PlayerPrefs.DeleteKey(_playerLanguageKey); break;
            case SaveIndex.ScreenRatio: PlayerPrefs.DeleteKey(_screenRatioKey); break;
        }
    }
    #endregion

    #region First Time Methods
    public void CheckFirstTimeLogin(UnityAction onComplete, UnityAction<string> onFail)
    {
        Debug.Log("Start CheckFirstTimeLogin ...");
        PlayFabManager.Instance.CheckPlayerFirstTimeLogin(
            (result1) =>
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result1);
                if (resultProcess.IsSuccess)
                {
                    // First Time.
                    TaskLoader<string> taskLoader = new TaskLoader<string>();
                    taskLoader.JoinTask(PurchaseStarterItem);
                    taskLoader.StartTasks(
                        delegate ()
                        {
                            Debug.Log("<color=green>Completed</color> CheckFirstTimeLogin");
                            if (onComplete != null)
                            {
                                onComplete.Invoke();
                            }
                        }
                        , delegate (List<string> failList)
                        {
                            if (onFail != null)
                            {
                                string failText = "";
                                foreach (string item in failList)
                                {
                                    failText += "|" + item;
                                }

                                Debug.LogFormat("<color=red>Failed</color> CheckFirstTimeLogin : {0}", failText);
                                onFail.Invoke(failText);
                            }
                        }
                    );
                }
                else
                {
                    // Not first time.
                    Debug.Log("<color=green>Completed</color> CheckFirstTimeLogin");
                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                }
            }
            , (error1) =>
            {
                Debug.LogFormat("<color=red>Failed</color> CheckFirstTimeLogin : {0}", error1.ErrorMessage);
                if (onFail != null)
                {
                    onFail.Invoke(error1.ErrorMessage);
                }
            }
        );
    }

    private void PurchaseStarterItem(UnityAction onComplete, UnityAction<string> onFail)
    {
        Debug.Log("Start PurchaseStarterItem ...");

        PlayFabManager.Instance.PurchaseStarterItem(
            delegate (PurchaseItemResult result)
            {
                Debug.Log("<color=green>Completed</color> PurchaseStarterItem");
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            }
            , delegate (PlayFabError error)
            {
                Debug.LogFormat("<color=red>Failed</color> PurchaseStarterItem : {0}", error.ErrorMessage);
                if (onFail != null)
                {
                    onFail.Invoke(error.Error.ToString());
                }
            }
        );
    }

    public bool GetFirstTimeSetupDeck()
    {
        return PlayerPrefs.GetInt(_setDeckFirstTimeKey, 1) == 1 ? true : false;
    }

    public void SetFirstTimeSetupDeck(bool isFirstTime)
    {
        PlayerPrefs.SetInt(_setDeckFirstTimeKey, isFirstTime ? 1 : 0);
    }

    public void CheckAcceptedCondition(UnityAction<bool> onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.GetPlayerData(
            _acceptedTermPrivacyPlayfabKey
            , delegate (GetUserDataResult result)
            {
                bool isAccepted = false;
                if (result.Data.ContainsKey(_acceptedTermPrivacyPlayfabKey))
                {
                    isAccepted = bool.Parse(result.Data[_acceptedTermPrivacyPlayfabKey].Value);
                }

                onComplete?.Invoke(isAccepted);
            }
            , delegate (PlayFabError error)
            {
                onFail?.Invoke(error.Error.ToString());
            }
        );
    }

    public void UpdateAcceptedCondition(bool isAccepted, UnityAction onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.UpdatePlayerData(
              _acceptedTermPrivacyPlayfabKey
            , isAccepted.ToString()
            , delegate (UpdateUserDataResult result)
            {
                onComplete?.Invoke();
            }
            , delegate (PlayFabError error)
            {
                onFail?.Invoke(error.Error.ToString());
            }
        );
    }
    #endregion

    #region Player ID Methods
    public void SetPlayerID(string playerID)
    {
        _playerInfoData.SetPlayerID(playerID);
    }
    #endregion

    #region Player Profile Methods
    public void UpdateContactEmail(string email, UnityAction onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.AddOrUpdateContactEmail(
            email
            , onComplete
            , delegate (PlayFabError error)
            {
                if (onFail != null)
                {
                    onFail.Invoke(error.Error.ToString());
                }
            }
        );
    }

    public bool IsDisplayNameValid()
    {
        string displayName = PlayerInfo.DisplayName;
        if (GameHelper.CheckValidNameContains(displayName
            , NameValidCase.Valid
            , NameValidCase.SameCurrent))
        {
            return true;
        }

        return false;
    }

    public void UpdatePlayerDisplayName(string displayName, UnityAction onComplete, UnityAction<string> onFail)
    {
        if (AuthenticationManager.GetAuthType() != AuthTypes.Guest
            && GameHelper.CheckValidNameContains(displayName, NameValidCase.Valid) == false)
        {
            if (onFail != null)
            {
                onFail.Invoke(LocalizationManager.Instance.GetText("ERROR_INVALID_DISPLAY_NAME"));
                return;
            }
        }

        PlayFabManager.Instance.UpdateUserTitleDisplayName(
            displayName
            , delegate (UpdateUserTitleDisplayNameResult result)
            {
                _playerInfoData.SetDisplayName(displayName);
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            }
            , delegate (PlayFabError error)
            {
                if (onFail != null)
                {
                    onFail.Invoke(error.Error.ToString());
                }
            }
        );
    }

    public void LoadAllPlayerInfoData(UnityAction onComplete, UnityAction<string> onFail)
    {
        TaskLoader<string> taskLoader = new TaskLoader<string>();
        taskLoader.AddTask(LoadPlayerProfileData);
        taskLoader.JoinTask(LoadPlayerAccountInfoData);
        taskLoader.StartTasks(
            onComplete
            , delegate (List<string> failList)
            {
                if (onFail != null)
                {
                    string failText = "";
                    foreach (string item in failList)
                    {
                        failText += "|" + item;
                    }
                    onFail.Invoke(failText);
                }
            }
        );
    }

    private void LoadPlayerProfileData(UnityAction onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.GetPlayerProfileData(
            delegate (GetPlayerProfileResult result)
            {
                _playerInfoData.SetDisplayName(result.PlayerProfile.DisplayName);

                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            }
            , delegate (PlayFabError error)
            {
                if (onFail != null)
                {
                    onFail.Invoke(error.Error.ToString());
                }
            }
        );
    }

    public void LoadPlayerAccountInfoData(UnityAction onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.GetThisPlayerAccountInfo(
            delegate (GetAccountInfoResult result)
            {
                _playerAccountInfoData = new PlayerAccountInfoData(result.AccountInfo);

                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            }
            , delegate (PlayFabError error)
            {
                if (onFail != null)
                {
                    onFail.Invoke(error.Error.ToString());
                }
            }
        );
    }

    public void LoadPlayerCosmeticSetting(UnityAction onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.GetPlayerReadOnlyData(
            new List<string>() {
                  _playerAvatarIDPlayfabKey
                , _playerTitleIDPlayfabKey
            }
            , delegate (GetUserDataResult result)
            {
                OnLoadPlayerAvatarIDComplete(result);
                OnLoadPlayerTitleIDComplete(result);

                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            }
            , delegate (PlayFabError error)
            {
                if (onFail != null)
                {
                    onFail.Invoke(error.Error.ToString());
                }
            }
        );
    }

    private void OnLoadRedeemTutorialRewardStatus(GetUserDataResult data)
    {
        if (data.Data.ContainsKey(_playerRedeemTutorialKey))
        {
            _redeemTutorialRewardStatusList = JsonConvert.DeserializeObject<bool[]>(data.Data[_playerRedeemTutorialKey].Value);
            Debug.Log("LoadRedeemTutorialRewardStatus <color=green>Completed</color>");
        }
        else
        {
            _redeemTutorialRewardStatusList = new bool[4];
        }
    }

    private void OnLoadPlayerAvatarIDComplete(GetUserDataResult data)
    {
        if (data.Data.ContainsKey(_playerAvatarIDPlayfabKey))
        {
            _playerInfoData.SetAvatarID(data.Data[_playerAvatarIDPlayfabKey].Value);

            Debug.Log("LoadPlayerAvatarID <color=green>Completed</color>");
        }
    }

    private void OnLoadPlayerTitleIDComplete(GetUserDataResult data)
    {
        if (data.Data.ContainsKey(_playerTitleIDPlayfabKey))
        {
            _playerInfoData.SetTitleID(data.Data[_playerTitleIDPlayfabKey].Value);

            Debug.Log("LoadPlayerTitleID <color=green>Completed</color>");
        }
    }

    public void UpdatePlayerAvatarID(string avatarID, UnityAction onComplete, UnityAction<string> onFail)
    {
        string oldAvatar = _playerInfoData.AvatarID;
        _playerInfoData.SetAvatarID(avatarID);
        PlayFabManager.Instance.UpdateUserAvatarID(
              avatarID
            , delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    OnChangeAvatar?.Invoke(oldAvatar, avatarID);
                    LoadPlayerCosmeticSetting(onComplete, onFail);
                }
                else
                {
                    if (onFail != null)
                    {
                        onFail.Invoke(resultProcess.MessageDetail);
                    }
                }
            }
            , delegate (PlayFabError error)
            {
                if (onFail != null)
                {
                    onFail.Invoke(error.Error.ToString());
                }
            }
        );
    }

    public void UpdatePlayerTitleID(string titleID, UnityAction onComplete, UnityAction<string> onFail)
    {
        string oldTitle = _playerInfoData.TitleID;
        _playerInfoData.SetTitleID(titleID);
        PlayFabManager.Instance.UpdateUserTitleID(
              titleID
            , delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    OnChangeTitle?.Invoke(oldTitle, titleID);
                    LoadPlayerCosmeticSetting(onComplete, onFail);
                }
                else
                {
                    if (onFail != null)
                    {
                        onFail.Invoke(resultProcess.MessageDetail);
                    }
                }
            }
            , delegate (PlayFabError error)
            {
                if (onFail != null)
                {
                    onFail.Invoke(error.Error.ToString());
                }
            }
        );
    }

    public void UpdateNUFStatusToServer(PlayerActivity activity, int version)
    {
        PlayFabManager.Instance.UpdateNUFStatus(activity, version, null, null);
    }

    public void UpdatePlayerActivityToServer(PlayerActivity activity)
    {
        _playerInfoData.SetPlayerActivityNow(activity.ToString());
        PlayFabManager.Instance.UpdatePlayerActivity(activity, null, null);
    }

    public void UpdatePlayerIsPlayingMissionStatus(bool isPlayingMission)
    {
        _playerInfoData.SetIsPlayingMission(isPlayingMission);
    }
    #endregion

    #region Load Data Methods
    public void SyncPlayerData(UnityAction onComplete, UnityAction<string> onFail, UnityAction<float> onUpdateProgress)
    {
        Debug.Log("Start SyncPlayerData ...");

        TaskLoader<string> taskLoader = new TaskLoader<string>();

        taskLoader.AddTask(DateTimeData.LoadServerTimeData);
        taskLoader.JoinTask(LoadAllTitleData);

        taskLoader.AddTask(LoadStoreData);
        taskLoader.JoinTask(LoadAllPlayerData);
        taskLoader.JoinTask(LoadAllPlayerReadOnlyData);

        taskLoader.AddTask(LoadSeasonData);
        taskLoader.JoinTask(LoadPlayerRankData);
        taskLoader.JoinTask(LoadPlayerMMRData);

        taskLoader.AddTask(LoadAllFriendList);
        taskLoader.JoinTask(LoadCatalogInventory);

        taskLoader.AddTask(RefreshLoginReward);
        taskLoader.JoinTask(LoadStarExchangeData);

        taskLoader.AddTask(RequestCheckClaimFinishTutorial2Reward);
        taskLoader.JoinTask(RefreshFreeDeal);
        taskLoader.AddTask(RefreshDailyDeal);
        taskLoader.JoinTask(RefreshWeeklyDeal);

        taskLoader.AddTask(InitailizeIAP);

        taskLoader.StartTasks(
              onUpdateProgress
            , delegate ()
            {
                // Setup MessageInbox data list
                SetupAllMessageInboxData();

                Debug.Log("<color=green>Completed</color> SyncPlayerData");

                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            }
            , delegate (List<string> failList)
            {
                if (onFail != null)
                {
                    string failText = "";
                    foreach (string item in failList)
                    {
                        if (failText != "")
                        {
                            failText += "|";
                        }
                        failText += item;
                    }

                    Debug.LogFormat("<color=red>Failed</color> SyncPlayerData : {0}", failText);
                    onFail.Invoke(failText);
                }
            }
        );
    }

    public void LoadAllPlayerData(UnityAction onComplete, UnityAction<string> onFail)
    {
        Debug.Log("Start LoadAllPlayerData ...");

        PlayFabManager.Instance.GetPlayerData(
            delegate (GetUserDataResult result)
            {
                OnLoadDeckPropertiesComplete(result);
                OnLoadDecksComplete(result);
                OnLoadPlayerSeenItemListComplete(result);
                OnLoadPlayerAvatarIDComplete(result);


                Debug.Log("<color=green>Completed</color> LoadAllPlayerData");

                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            }
            , delegate (PlayFabError error)
            {
                Debug.Log("<color=red>Failed</color> LoadAllPlayerData : " + error.ToString());

                GameHelper.DelayCallback(
                      1.0f
                    , delegate ()
                    {
                        Debug.Log("Retry LoadAllPlayerData ...");
                        LoadAllPlayerData(onComplete, onFail);
                    }
                );
            }
        );
    }

    public void LoadAllPlayerReadOnlyData(UnityAction onComplete, UnityAction<string> onFail)
    {
        Debug.Log("Start LoadAllPlayerReadOnlyData ...");

        TaskLoader<string> taskLoader = new TaskLoader<string>();

        //taskLoader.AddTask(ConvertOldEventQuest);
        taskLoader.AddTask(RefreshEventQuest);
        taskLoader.AddTask(RefreshSeasonPassQuests);
        taskLoader.AddTask(RefreshGameEvenPlayerDataValid);
        taskLoader.AddTask(RefreshGameEventStat);

        taskLoader.AddTask(RefreshCardPriceOverrideQuota);
        taskLoader.AddTask(RefreshMessageInboxPreset);
        taskLoader.AddTask(LoadLastMatchPlayers);
        taskLoader.AddTask(RefreshAllReferreeList);
        taskLoader.AddTask(LoadPlayerSave);

        taskLoader.AddTask(ExecuteSpecificCloudFunction);

        taskLoader.AddTask(StartLoadPlayerReadOnlyData);
        taskLoader.StartTasks(
            delegate ()
            {
                Debug.Log("<color=green>Completed</color> LoadAllPlayerReadOnlyData");
                onComplete?.Invoke();
            }
            , delegate (List<string> failList)
            {
                if (onFail != null)
                {
                    string failText = "";
                    foreach (string item in failList)
                    {
                        if (failText != "")
                        {
                            failText += "|";
                        }
                        failText += item;
                    }

                    Debug.LogFormat("<color=red>Failed</color> LoadAllPlayerReadOnlyData : {0}", failText);
                    onFail.Invoke(failText);
                }
            }
        );
    }

    private void RefreshMessageInboxPreset(UnityAction onComplete, UnityAction<string> onFail)
    {
        Debug.Log("Start RefreshMessageInboxPreset ...");

        PlayFabManager.Instance.RefreshMessageInboxPreset(
            delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    Debug.Log("<color=green>Completed</color> RefreshMessageInboxPreset");
                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                }
                else
                {

                    Debug.LogFormat("<color=red>Failed</color> RefreshMessageInboxPreset : {0}", resultProcess.MessageDetail);
                    if (onFail != null)
                    {
                        onFail.Invoke(resultProcess.MessageDetail);
                    }
                }
            }
            , delegate (PlayFabError error)
            {
                Debug.LogFormat("<color=red>Failed</color> RefreshMessageInboxPreset : {0}", error.ErrorMessage);

                GameHelper.DelayCallback(
                      1.0f
                    , delegate ()
                    {
                        Debug.Log("Retry RefreshMessageInboxPreset ...");
                        RefreshMessageInboxPreset(onComplete, onFail);
                    }
                );
            }
        );
    }

    public void RefreshLoginReward(UnityAction onComplete, UnityAction<string> onFail)
    {
        Debug.Log("Start RefreshLoginReward ...");

        PlayFabManager.Instance.CheckLoginReward(
            delegate (ExecuteCloudScriptResult result)
            {
                Dictionary<string, string> msgValueDict = JsonConvert.DeserializeObject<Dictionary<string, string>>(result.FunctionResult.ToString());

                Dictionary<string, string> dailyLoginData = JsonConvert.DeserializeObject<Dictionary<string,string>>(msgValueDict["daily_login"]);
                Dictionary<string, string> specialLoginData = JsonConvert.DeserializeObject<Dictionary<string,string>>(msgValueDict["special_login"]);
                DailyLoginRewardData dailyRewardData = JsonConvert.DeserializeObject<DailyLoginRewardData>(dailyLoginData["rewardData"]);
                SpecialLoginRewardData specialRewardData = JsonConvert.DeserializeObject<SpecialLoginRewardData>(specialLoginData["rewardData"]);

                //set data.
                _dailyLoginData = new DailyLoginData(dailyRewardData, dailyLoginData["data"]);
                _specialLoginData = new SpecialLoginData(specialRewardData, specialLoginData["data"]);
                if (_dailyLoginData.GetDailyLoginRewardData().isNewDay)
                {
                    OnDailyLogin?.Invoke();
                }

                Debug.Log("<color=green>Completed</color> RefreshLoginReward");

                _dailyLoginData.GetDailyLoginRewardData().timestamp = DateTimeData.GetDateTimeUTC();
                _specialLoginData.GetDailyLoginRewardData().timestamp = DateTimeData.GetDateTimeUTC();
                onComplete?.Invoke();
            },
            delegate (PlayFabError error)
            {
                Debug.LogFormat("<color=red>Failed</color> RefreshLoginReward : {0}", error.ErrorMessage);

                GameHelper.DelayCallback(
                      1.0f
                    , delegate ()
                    {
                        Debug.Log("Retry RefreshLoginReward ...");
                        RefreshLoginReward(onComplete, onFail);
                    }
                );
            }
        );
    }

    private void StartLoadPlayerReadOnlyData(UnityAction onComplete, UnityAction<string> onFail)
    {
        Debug.Log("Start StartLoadPlayerReadOnlyData ...");

        PlayFabManager.Instance.GetPlayerReadOnlyData(
            delegate (GetUserDataResult result)
            {
                OnLoadPlayerStatComplete(result);
                OnLoadPlayerEventQuestDataComplete(result);
                OnLoadPlayerAchievementDataComplete(result);
                OnLoadPlayerAchievementCardSetDataComplete(result);
                OnLoadPlayerExpComplete(result);
                OnLoadPlayerExpFactionComplete(result);
                OnLoadPlayerLevelRewardRecordDataComplete(result);
                OnLoadPlayerLevelFactionRewardRecordDataComplete(result);
                OnLoadPlayerMessageInboxComplete(result);
                OnLoadPlayerRenameStatusComplete(result);
                OnLoadPlayerAvatarIDComplete(result);
                OnLoadPlayerTitleIDComplete(result);
                //OnLoadPlayerEventCurrencyComplete(result);
                OnLoadPlayerTutorialStatus(result);
                OnLoadPlayerCardPriceOverrideDataComplete(result);
                OnLoadPlayerAdventureRewarded(result);
                OnLoadPlayerRegisteredReferralCode(result);
                OnLoadPlayerWildOfferData(result);
                OnLoadPlayerPurchaseLimitedData(result);
                OnLoadPlayerWeeklyRankRewardStatus(result);
                OnLoadPlayerFirstTopupStatus(result);

                //season pass
                OnLoadPlayerSeasonPassDataComplete(result);
                OnLoadPlayerSeasonPassQuestDataComplete(result);
                OnLoadPlayerSeasonPassRewardDataComplete(result);

                // Feature Unlock
                OnLoadPlayerFeatureUnlockComplete(result);

                // Mission
                OnLoadPlayerMissionDataComplete(result);

                // Tutorial
                OnLoadRedeemTutorialRewardStatus(result);

                Debug.Log("<color=green>Completed</color> StartLoadPlayerReadOnlyData");
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            }
            , delegate (PlayFabError error)
            {
                Debug.LogFormat("<color=red>Failed</color> StartLoadPlayerReadOnlyData : {0}", error.ErrorMessage);

                GameHelper.DelayCallback(
                      1.0f
                    , delegate ()
                    {
                        Debug.Log("Retry StartLoadPlayerReadOnlyData ...");
                        StartLoadPlayerReadOnlyData(onComplete, onFail);
                    }
                );
            }
        );
    }

    //private void OnLoadPlayerEventCurrencyComplete(GetUserDataResult result)
    //{
    //    if (result.Data.ContainsKey("event_currency"))
    //    {
    //        string rawEventCurrencyJson = result.Data["event_currency"].Value;
    //        //_playerInventoryData.SetEventCurrency(rawEventCurrencyJson);

    //        Debug.Log("PlayerNUFStatus <color=green>Completed</color>");
    //    }
    //}

    public void LoadAllTitleData(UnityAction onComplete, UnityAction<string> onFail)
    {
        Debug.Log("Start LoadAllTitleData ...");

        PlayFabManager.Instance.GetTitleData(
            delegate (GetTitleDataResult result)
            {
                OnLoadPlayerStatKeysComplete(result);

                //game value
                OnLoadTitleGameValueComplete(result);

                //game objectives
                OnLoadTitleQuestDBDataComplete(result);
                OnLoadTitleAchievementDBDataComplete(result);
                OnLoadTitleAchievementCardSetDBDataComplete(result);
                OnLoadTitlePlayerExpDataComplete(result);
                OnLoadTitlePlayerExpFactionDataComplete(result);
                OnLoadLevelRewardDataComplete(result);
                OnLoadLevelFactionRewardDataComplete(result);

                OnLoadCardChangeLogComplete(result);
                OnLoadTitleCardPriceOverrideDBComplete(result);

                //game event data
                OnLoadGameEventDataComplete(result);
                OnLoadShopEventComplete(result);

                //season data
                OnLoadSeasonRankRewardDataComplete(result);

                OnLoadStarItemDBComplete(result);

                OnLoadBannerDBComplete(result);

                OnLoadTitleMessageInboxAnnouncementComplete(result);
                OnLoadShopTypeDataComplete(result);
                OnLoadEventBattleDBComplete(result);

                OnLoadAdventureDBComplete(result);

                OnLoadWildOfferDBComplete(result);

                OnLoadDeckDBComplete(result);

                //season pass
                OnLoadTitleSeasonPassDBComplete(result);
                OnLoadTitleSeasonPassTierDataComplete(result);
                OnLoadTitleSeasonPassTierRewardDataComplete(result);
                OnLoadTitleSeasonPassPriceDBDataComplete(result);

                // feature unlock
                OnLoadTitleFeatureUnlockComplete(result);

                // tutorial
                OnLoadTutorialRewardComplete(result);
                // mission
                OnLoadTitleMissionDBComplete(result);

                OnLoadWeeklyRankRewardDataDBComplete(result);

                // override db
                OnLoadOverrideDBComplete(result);

                Debug.Log("<color=green>Completed</color> LoadAllTitleData");
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            }
            , delegate (PlayFabError error)
            {
                Debug.LogFormat("<color=red>Failed</color> LoadAllTitleData : {0}", error.ErrorMessage);

                GameHelper.DelayCallback(
                      1.0f
                    , delegate ()
                    {
                        Debug.Log("Retry LoadAllTitleData ...");

                        LoadAllTitleData(onComplete, onFail);
                    }
                );
            }
        );
    }

    private void OnLoadOverrideDBComplete(GetTitleDataResult result)
    {
        if (result.Data.ContainsKey("override_card_db"))
        {
            string csvText = result.Data["override_card_db"];
            if (csvText != null && csvText != string.Empty)
            {
                CardDB.Instance.LoadOverrideDB(csvText);
            }
        }
        if (result.Data.ContainsKey("override_card_effect_db"))
        {
            string csvText = result.Data["override_card_effect_db"];
            if (csvText != null && csvText != string.Empty)
            {
                CardEffectDB.Instance.LoadOverrideDB(csvText);
            }
        }
        if (result.Data.ContainsKey("override_card_ability_db"))
        {
            string csvText = result.Data["override_card_ability_db"];
            if (csvText != null && csvText != string.Empty)
            {
                CardAbilityDB.Instance.LoadOverrideDB(csvText);
            }
        }
        if (result.Data.ContainsKey("override_localize_db"))
        {
            string csvText = result.Data["override_localize_db"];
            if (csvText != null && csvText != string.Empty)
            {
                LocalizationDB.Instance.LoadOverrideDB(csvText);
            }
        }
    }
    #endregion

    #region Game Value
    public void OnLoadTitleGameValueComplete(GetTitleDataResult result)
    {
        if (result.Data.ContainsKey(_titleGameValuePlayfabKey))
        {
            _gameValue = JsonConvert.DeserializeObject<Dictionary<string, object>>(result.Data[_titleGameValuePlayfabKey]);
        }

        Debug.Log("Load GameValue <color=green>Completed</color>");
    }

    public bool GetGameValue(string key, out object value)
    {
        return _gameValue.TryGetValue(key, out value);
    }
    #endregion

    #region Deck Properties Methods
    public void SetRecentDeckSelection(string deckID)
    {
        PlayerPrefs.SetString(_recentDeckSelection, deckID);
    }

    public string GetRecentDeckSelection()
    {
        return PlayerPrefs.GetString(_recentDeckSelection, "deck_1");
    }

    private void LoadDeckProperties(UnityAction onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.GetPlayerData(
              _playerDeckPropertiesPlayfabKey
            , delegate (GetUserDataResult result)
            {
                OnLoadDeckPropertiesComplete(result);
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            }
            , delegate (PlayFabError error)
            {
                if (onFail != null)
                {
                    onFail.Invoke(error.Error.ToString());
                }
            }
        );
    }

    private void OnLoadDeckPropertiesComplete(GetUserDataResult result)
    {
        if (result.Data.ContainsKey(_playerDeckPropertiesPlayfabKey))
        {
            // split deck properties.
            Dictionary<string, object> deckPropData = JsonConvert.DeserializeObject<Dictionary<string, object>>(result.Data[_playerDeckPropertiesPlayfabKey].Value);

            int deckSlot = int.Parse(deckPropData["deck_slot"].ToString());
            List<string> deckIDList = JsonConvert.DeserializeObject<List<string>>(deckPropData["deck_list"].ToString());

            SetDeckProperties(deckSlot, deckIDList);

            Debug.Log("LoadDeckProperties <color=green>Completed</color>");
        }
        else
        {
            Debug.Log("LoadDeckProperties <color=red>Failed</color>");
        }
    }

    private string CreateDeckProperties(int deckSlot, List<string> deckIDList)
    {
        Dictionary<string, object> deckPropData = new Dictionary<string, object>();
        deckPropData.Add("deck_slot", deckSlot);
        deckPropData.Add("deck_list", deckIDList);

        return JsonConvert.SerializeObject(deckPropData);
    }

    private void SetDeckProperties(int deckSlot, List<string> deckIDList)
    {
        PlayerPrefs.SetInt(_playerDeckSlotKey, deckSlot);
        PlayerPrefsX.SetStringArray(_playerDeckIDListKey, deckIDList.ToArray());
    }

    public int GetDeckSlot()
    {
        return PlayerPrefs.GetInt(_playerDeckSlotKey, 0);
    }

    public List<string> GetDeckIDList()
    {
        List<string> result = new List<string>();
        string[] ids = PlayerPrefsX.GetStringArray(_playerDeckIDListKey);
        if (ids != null && ids.Length > 0)
        {
            result = new List<string>(ids);
        }

        return result;
    }
    #endregion

    #region Deck Methods
    public void LoadDeck(string deckID, UnityAction onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.GetPlayerData(
              deckID
            , delegate (GetUserDataResult result)
            {
                if (result.Data.ContainsKey(deckID))
                {
                    // Get save data.
                    string deckJson = result.Data[deckID].Value;
                    DeckData deck = DeckData.JsonToDeckData(deckJson);
                    SetDeck(deckID, deck);

                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                }
                else
                {
                    // Not found save data.
                    if (onFail != null)
                    {
                        onFail.Invoke("DataManager/LoadDeck: Error not found key. " + deckID);
                    }
                }
            }
            , delegate (PlayFabError error)
            {
                if (onFail != null)
                {
                    onFail.Invoke(error.Error.ToString());
                }
            }
        );
    }

    public void LoadDecks(List<string> deckIDList, UnityAction onComplete)
    {
        PlayFabManager.Instance.GetPlayerData(
              deckIDList
            , delegate (GetUserDataResult result)
            {
                OnLoadDecksComplete(result);
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            }
            , delegate (PlayFabError error)
            {
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            }
        );
    }

    private void OnLoadDecksComplete(GetUserDataResult result)
    {
        List<string> deckIDList = GetDeckIDList();

        foreach (string deckID in deckIDList)
        {
            if (result.Data.ContainsKey(deckID))
            {
                // Get save data.
                string deckJson = result.Data[deckID].Value;
                DeckData deck = DeckData.JsonToDeckData(deckJson);
                SetDeck(deckID, deck);
            }
        }

        Debug.Log("LoadDecks <color=green>Completed</color>");
    }

    private void LoadAllDeck(UnityAction onComplete)
    {
        List<string> deckIDList = GetDeckIDList();
        if (deckIDList != null && deckIDList.Count > 0)
        {
            LoadDecks(
                  deckIDList
                , delegate ()
                {
                    Debug.Log("LoadAllDeck <color=green>Completed</color>");
                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                }
            );
        }
        else
        {
            if (onComplete != null)
            {
                onComplete.Invoke();
            }
        }
    }

    public void SaveDeck(string deckID, DeckData deck, UnityAction onComplete, UnityAction<string> onFail)
    {
        string deckJson = deck.ToJson();
        PlayFabManager.Instance.UpdatePlayerData(
              deckID
            , deckJson
            , delegate (PlayFab.ClientModels.UpdateUserDataResult result)
            {
                // Success
                SetDeck(deckID, deck);

                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            }
            , delegate (PlayFab.PlayFabError error)
            {
                // Fail
                if (onFail != null)
                {
                    onFail.Invoke(error.Error.ToString());
                }
            }
        );
    }

    private void SetDeck(string deckID, DeckData deck)
    {
        string deckJson = deck.ToJson();
        string key = string.Format("{0}_{1}", _playerDeckKey, deckID);
        PlayerPrefs.SetString(key, deckJson);
    }

    public DeckData GetDeck(string deckID)
    {
        DeckData deck = new DeckData();

        string key = string.Format("{0}_{1}", _playerDeckKey, deckID);
        if (PlayerPrefs.HasKey(key))
        {
            string resultJson = PlayerPrefs.GetString(key);
            deck = DeckData.JsonToDeckData(resultJson);
        }
        else
        {
            //Debug.LogErrorFormat("DataManager/GetDeck: Not found deck. {0}", deckID);
            DeckDB.Instance.GetDefaultDeck(CardElementType.FIRE, out deck);
            deck.SetDeckID(deckID);
        }

        return deck;
    }

    public DeckData GetCurrentDeck()
    {
        return GetDeck(GetCurrentDeckID());
    }

    public List<DeckData> GetAllDeck()
    {
        List<DeckData> result = new List<DeckData>();

        List<string> deckIDList = GetDeckIDList();
        if (deckIDList != null && deckIDList.Count > 0)
        {
            foreach (string deckID in deckIDList)
            {
                DeckData deck = GetDeck(deckID);
                if (deck != null)
                {
                    result.Add(deck);
                }
            }
        }

        return result;
    }

    public bool IsHasDeckAvailable()
    {
        List<DeckData> deckList = GetAllDeck();
        if (deckList == null)
        {
            return false;
        }
        else
        {
            if (deckList.Count == 0)
            {
                return false;
            }
        }

        return true;
    }

    public void AddDeck(DeckData deck, UnityAction onComplete, UnityAction<string> onFail)
    {
        List<string> deckIDList = new List<string>(GetDeckIDList());
        string deckID = deck.DeckID;
        if (!deckIDList.Contains(deckID))
        {
            if (deckIDList.Count < GetDeckSlot())
            {
                string deckJson = deck.ToJson();
                int deckSlot = GetDeckSlot();
                deckIDList.Add(deck.DeckID);

                Dictionary<string, string> updateData = new Dictionary<string, string>();
                updateData.Add(deckID, deckJson);
                updateData.Add(_playerDeckPropertiesPlayfabKey, CreateDeckProperties(deckSlot, deckIDList));

                PlayFabManager.Instance.UpdatePlayerData(
                      updateData
                    , delegate (UpdateUserDataResult result)
                    {
                        SetDeck(deckID, deck);
                        SetDeckProperties(deckSlot, deckIDList);

                        if (onComplete != null)
                        {
                            onComplete.Invoke();
                        }
                    }
                    , delegate (PlayFabError error)
                    {
                        // Fail
                        if (onFail != null)
                        {
                            onFail.Invoke(error.Error.ToString());
                        }
                    }
                );
            }
            else
            {
                // Fail
                if (onFail != null)
                {
                    onFail.Invoke("DeckSlot is full.");
                }
            }
        }
        else
        {
            // Fail
            if (onFail != null)
            {
                onFail.Invoke("DeckID is not available.");
            }
        }
    }

    public void RemoveDeck(string deckID, UnityAction onComplete, UnityAction<string> onFail)
    {
        List<string> deckIDList = new List<string>(GetDeckIDList());
        deckIDList.Remove(deckID);

        int deckSlot = GetDeckSlot();
        Dictionary<string, string> updateData = new Dictionary<string, string>();
        updateData.Add(_playerDeckPropertiesPlayfabKey, CreateDeckProperties(deckSlot, deckIDList));

        PlayFabManager.Instance.UpdatePlayerData(
              updateData
            , new List<string>() { deckID }
            , delegate (UpdateUserDataResult result)
            {
                // Success
                RemoveDeck(deckID);
                SetDeckProperties(deckSlot, deckIDList);

                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            }
            , delegate (PlayFabError error)
            {
                // Fail
                if (onFail != null)
                {
                    onFail.Invoke(error.Error.ToString());
                }
            }
        );
    }

    private void RemoveDeck(string deckID)
    {
        string key = string.Format("{0}_{1}", _playerDeckKey, deckID);
        PlayerPrefs.DeleteKey(key);
    }

    public string GetCurrentDeckID()
    {
        return PlayerPrefs.GetString(_playerCurrentDeckKey, "deck_1");
    }

    public void SetCurrentDeckID(string deckID)
    {
        PlayerPrefs.SetString(_playerCurrentDeckKey, deckID);
    }

    public bool IsLocalDeckValid(string deckID)
    {
        DeckData deck = GetDeck(deckID);
        //Debug.Log("IsLocalDeckValid(deck) : " + IsLocalDeckValid(deck));
        return IsLocalDeckValid(deck) == DeckValidStatus.Valid;
    }

    public DeckValidStatus IsLocalDeckValid(DeckData deckData)
    {
        CardListData cardList = _playerInventoryData.CardInventoryList;
        if (deckData == null)
        {
            return DeckValidStatus.InvalidDeck;
        }

        // check if deck count is valid
        if (deckData.Deck.CountAmount != GameHelper.MaxCardPerDeck)
        {
            return DeckValidStatus.InvalidCount;
        }

        CardData cardData;
        CardListData cardBaseListData = new CardListData();
        CardElementType type;
        if (deckData.GetElementType(out type))
        {
            foreach (KeyValuePair<string, int> cards in deckData.Deck)
            {
                // get card data (all cards should have valid data)
                if (CardData.GetCardData(cards.Key, out cardData))
                {
                    // check if Faction is valid
                    if (!cardData.CardElement.IsElement(type)
                       && !cardData.CardElement.IsElement(CardElementType.NEUTRAL))
                    {
                        return DeckValidStatus.InvalidFaction;
                    }

                    // check if Inventory has enough card
                    if (cardList.ContainKey(cards.Key))
                    {
                        if (cardList[cards.Key] <= 0 || cardList[cards.Key] < cards.Value)
                        {
                            return DeckValidStatus.InvalidCount;
                        }
                    }
                    else
                    {
                        return DeckValidStatus.InvalidCount;
                    }

                    // add card based on baseID
                    cardBaseListData.AddCard(cardData.BaseID, cards.Value);
                }
                // return invalid if card does not has valid data
                else
                {
                    return DeckValidStatus.InvalidCardData;
                }
            }

            // check if card count (baseID) is valid
            foreach (KeyValuePair<string, int> card in cardBaseListData)
            {
                if (card.Value > GameHelper.MaxCardPerBaseID)
                {
                    return DeckValidStatus.InvalidCardCount;
                }
            }
        }
        return DeckValidStatus.Valid;
    }

    public void ReloadDeckInventory(UnityAction onComplete, UnityAction<string> onFail)
    {
        TaskLoader<string> taskLoader = new TaskLoader<string>();
        taskLoader.JoinTask(LoadInventory);
        taskLoader.JoinTask(ReloadPlayerDeckInventory);
        taskLoader.StartTasks(onComplete
            , delegate (List<string> failList)
            {
                if (onFail != null)
                {
                    string failText = "";
                    foreach (string item in failList)
                    {
                        failText += " " + item;
                    }
                    onFail.Invoke(failText);
                }
            }
        );
    }

    private void ReloadPlayerDeckInventory(UnityAction onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.GetPlayerData(delegate (GetUserDataResult result)
        {
            OnLoadDeckPropertiesComplete(result);
            OnLoadDecksComplete(result);
            if (onComplete != null)
            {
                onComplete.Invoke();
            }
        }
        , (error) =>
        {
            if (onFail != null)
            {
                onFail.Invoke(error.Error.ToString());
            }
        });
    }

    private void OnLoadDeckDBComplete(GetTitleDataResult result)
    {
        if (result.Data.ContainsKey(_deckDBPlayfabKey))
        {
            DeckDB.Instance.LoadFromJSON(result.Data[_deckDBPlayfabKey]);
        }

        Debug.Log("Load DeckDB <color=green>Completed</color>");
    }
    #endregion

    #region Inventory Methods 

    private void LoadCatalogInventory(UnityAction onComplete, UnityAction<string> onFail)
    {
        Debug.Log("Start LoadCatalogInventory ...");

        TaskLoader<string> taskLoader = new TaskLoader<string>();
        taskLoader.AddTask(LoadCatalogItems);
        taskLoader.AddTask(LoadInventory); // In LoadInventoryComplete use catalog item class to filter items . So it has to wait LoadCatalogItems to complete first.
        taskLoader.StartTasks(
            delegate ()
            {
                Debug.Log("<color=green>Completed</color> LoadCatalogInventory");
                onComplete?.Invoke();
            }
            , delegate (List<string> failList)
            {
                if (onFail != null)
                {
                    string failText = "";
                    foreach (string errorCode in failList)
                    {
                        failText += " " + errorCode;
                    }

                    Debug.LogFormat("<color=red>Failed</color> LoadCatalogInventory : {0}", failText);
                    onFail.Invoke(failText);
                }
            }
        );
    }

    public void LoadInventory(UnityAction onComplete, UnityAction<string> onFail)
    {
        Debug.Log("Start LoadInventory ...");

        PlayFabManager.Instance.GetInventoryItems(
            delegate (GetUserInventoryResult result)
            {
                OnLoadInventoryComplete(result);

                Debug.Log("<color=green>Completed</color> LoadInventory");
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            }
            , delegate (PlayFabError error)
            {
                Debug.LogFormat("<color=red>Failed</color> LoadInventory : {0}", error.ErrorMessage);

                Debug.Log("Retry LoadInventory ...");
                LoadInventory(onComplete, onFail);
            }
        );
    }


    public IEnumerator IELoadInventory(UnityAction<string> onFail)
    {
        bool isFinish = false;
        PlayFabManager.Instance.GetInventoryItems(
            delegate (GetUserInventoryResult result)
            {
                OnLoadInventoryComplete(result);
                isFinish = true;
            }
            , delegate (PlayFabError error)
            {
                onFail?.Invoke(error.Error.ToString());
                isFinish = true;
            }
        );
        yield return new WaitUntil(() => isFinish);
    }


    private void OnLoadInventoryComplete(GetUserInventoryResult result)
    {
        _playerInventoryData.SetAllInventoryData(result);

        OnUpdateInventory?.Invoke();
        Debug.Log("LoadInventory <color=green>Completed</color>");
    }

    public void LoadPlayerSeenItemList(UnityAction onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.GetPlayerData(new List<string>() {
                _playerSeenCardPlayfabKey
                , _playerSeenAvatarPlayfabKey
                , _playerSeenTitlePlayfabKey
                , _playerSeenHeroPlayfabKey
                , _playerSeenCardBackPlayfabKey
                , _playerSeenDockPlayfabKey
            }
            , delegate (GetUserDataResult result)
            {
                OnLoadPlayerSeenItemListComplete(result);
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            }
            , delegate (PlayFabError error)
            {
                if (onFail != null)
                {
                    onFail.Invoke(error.Error.ToString());
                }
            }
        );
    }

    private void OnLoadPlayerSeenItemListComplete(GetUserDataResult result)
    {
        Dictionary<string, string> rawData = ConvertUserDataResultToDictionary(result);
        PlayerSeenListManager.Instance.SetData(rawData);

        Debug.Log("LoadSeenList <color=green>Completed</color>");
    }

    private Dictionary<string, string> ConvertUserDataResultToDictionary(GetUserDataResult data)
    {
        Dictionary<string, string> result = new Dictionary<string, string>();
        foreach (KeyValuePair<string, UserDataRecord> item in data.Data)
        {
            result.Add(item.Key, item.Value.Value);
        }

        return result;
    }

    public void UpdatePlayerSeenData()
    {
        PlayFabManager.Instance.UpdatePlayerData(PlayerSeenListManager.Instance.GetUpdatedPlayerSeenData(), null, null);
    }

    public void OpenCardPack(string packID, int amount, UnityAction<PackReward> onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.OpenPacks(
             packID
           , amount
           , delegate (ExecuteCloudScriptResult result)
           {
               CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
               if (resultProcess.IsSuccess)
               {
                   PackReward packResult = CreatePackReward(packID, amount, resultProcess.CustomData["cardList"], resultProcess.CustomData["bonusResultList"]);
                   if (onComplete != null)
                   {
                       onComplete.Invoke(packResult);
                   }
               }
               else
               {
                   if (onFail != null)
                   {
                       onFail.Invoke(resultProcess.MessageDetail);
                   }
               }
           }
           , delegate (PlayFabError error)
           {
               if (onFail != null)
               {
                   onFail.Invoke(error.Error.ToString());
               }
           }
       );
    }

    private PackReward CreatePackReward(string packID, int amount, string cardListJson, string bonusResultListJson)
    {
        List<List<string>> cardPackResultStr = JsonConvert.DeserializeObject<List<List<string>>>(cardListJson);
        List<string> bonusResultData = JsonConvert.DeserializeObject<List<string>>(bonusResultListJson);

        CardListData oldCardList = _playerInventoryData.CardInventoryList;
        List<PackResult> packResultList = new List<PackResult>();
        for (int i = 0; i < cardPackResultStr.Count; i++)
        {
            List<ItemReward> itemRewardList = new List<ItemReward>();
            for (int j = 0; j < cardPackResultStr[i].Count; j++)
            {
                string itemID = cardPackResultStr[i][j];

                bool isCard = CardData.IsCard(itemID);
                if (isCard)
                {
                    itemRewardList.Add(new CardReward(itemID));
                }
                else
                {
                    CurrencyReward data = GetStarItemDB().GetStarItem(itemID);
                    itemRewardList.Add(data);
                }
            }
            PackResult pack = new PackResult(itemRewardList);
            packResultList.Add(pack);
        }
        return new PackReward(packID, amount, packResultList, bonusResultData);
    }

    #endregion

    #region Player Progress Methods
    private void OnLoadPlayerExpComplete(GetUserDataResult result)
    {
        int playerExp = 0;
        if (result.Data.ContainsKey(_playerExpPlayfabKey))
        {
            playerExp = int.Parse(result.Data[_playerExpPlayfabKey].Value);
        }
        _playerInfoData.SetLevelData(playerExp);
        OnSetPlayerLevel?.Invoke();

        Debug.Log("Load PlayerExp <color=green>Completed</color>");
    }

    private void OnLoadPlayerExpFactionComplete(GetUserDataResult result)
    {
        Dictionary<string, int> playerExpFaction = new Dictionary<string, int>();
        if (result.Data.ContainsKey(_playerExpFactionPlayfabKey))
        {
            playerExpFaction = JsonConvert.DeserializeObject<Dictionary<string, int>>(result.Data[_playerExpFactionPlayfabKey].Value);
        }
        _playerInfoData.SetLevelFactionData(playerExpFaction);
        OnSetPlayerLevelFaction?.Invoke();

        Debug.Log("Load playerExpFaction <color=green>Completed</color>");
    }

    private void OnLoadTitlePlayerExpDataComplete(GetTitleDataResult result)
    {
        if (result.Data.ContainsKey(_titlePlayerExpDataPlayfabKey))
        {
            PlayerExpDB.Instance.LoadFromJSON(result.Data[_titlePlayerExpDataPlayfabKey]);
        }

        Debug.Log("LoadTitlePlayerExpData <color=green>Completed</color>");
    }

    private void OnLoadTitlePlayerExpFactionDataComplete(GetTitleDataResult result)
    {
        if (result.Data.ContainsKey(_titlePlayerExpFactionDataPlayfabKey))
        {
            PlayerExpFactionDB.Instance.LoadFromJSON(result.Data[_titlePlayerExpFactionDataPlayfabKey]);
        }

        Debug.Log("LoadTitlePlayerExpFactionData <color=green>Completed</color>");
    }

    private void OnLoadPlayerRenameStatusComplete(GetUserDataResult result)
    {
        if (result.Data.ContainsKey(_playerRenameStatusPlayfabKey))
        {
            _isPlayerRenamed = bool.Parse(result.Data[_playerRenameStatusPlayfabKey].Value);
        }
        SetPlayerRenameStatus(_isPlayerRenamed);

        Debug.Log("Load PlayerRenameStatus <color=green>Completed</color>");
    }

    public void UpdateAndPurchaseRenameFeature(UnityAction onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.UpdateAndPurchaseRenameFeature(
           delegate (ExecuteCloudScriptResult result)
           {
               CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
               if (resultProcess.IsSuccess)
               {
                   SetPlayerRenameStatus(true);
               }
               onComplete?.Invoke();
           },
           delegate (PlayFabError error)
           {
               Debug.LogError(error.ErrorMessage);
               onFail?.Invoke(error.ErrorMessage);
           }
       );
    }

    private void SetPlayerRenameStatus(bool isPlayerRenamed)
    {
        _isPlayerRenamed = isPlayerRenamed;
    }

    public bool GetPlayerRenameStatus()
    {
        return _isPlayerRenamed;
    }

    private RankDBData GetPlayerRankData()
    {
        int playerRank = _playerInfoData.GetPlayerCurrentRank();

        RankDBData data = null;
        if (RankDB.Instance.GetRank(playerRank, out data))
        {
            return data;
        }

        Debug.LogError("GetPlayerRankData: Not found rank data. " + playerRank);
        return null;
    }

    public int GetPlayerRankIndex()
    {
        RankDBData data = GetPlayerRankData();
        if (data != null)
        {
            return data.RankIndex;
        }
        else
        {
            return 0;
        }
    }

    public void LoadPlayerRenameStatus(UnityAction onComplete)
    {
        PlayFabManager.Instance.GetPlayerReadOnlyData(
                 _playerRenameStatusPlayfabKey
               , delegate (GetUserDataResult result)
               {
                   OnLoadPlayerRenameStatusComplete(result);
                   if (onComplete != null)
                       onComplete.Invoke();
               }
               , delegate (PlayFabError error)
               {
                   if (onComplete != null)
                       onComplete.Invoke();
               }
           );
    }

    public void LoadPlayerRankData(UnityAction onComplete, UnityAction<string> onFail)
    {
        Debug.Log("Start LoadPlayerRankData ...");

        PlayFabManager.Instance.GetPlayerRankData(
            delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    Dictionary<string, object> playerRank = JsonConvert.DeserializeObject<Dictionary<string, object>>(resultProcess.CustomData["data"]);
                    _playerInfoData.SetPlayerRank(playerRank);
                    OnSetSeasonRank?.Invoke();

                    Debug.Log("<color=green>Completed</color> LoadPlayerRankData");
                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                }
                else
                {
                    Debug.LogFormat("<color=red>Failed</color> LoadPlayerRankData : {0}", resultProcess.MessageDetail);
                    if (onFail != null)
                    {
                        onFail.Invoke(resultProcess.MessageDetail);
                    }
                }
            }
            , delegate (PlayFabError error)
            {
                Debug.LogFormat("<color=red>Failed</color> LoadPlayerRankData : {0}", error.ErrorMessage);

                GameHelper.DelayCallback(
                      1.0f
                    , delegate ()
                    {
                        Debug.Log("Retry LoadPlayerRankData ...");
                        LoadPlayerRankData(onComplete, onFail);
                    }
                );
            }
        );
    }

    public void LoadPlayerMMRData(UnityAction onComplete, UnityAction<string> onFail)
    {
        Debug.Log("Start LoadPlayerMMRData ...");

        PlayFabManager.Instance.GetPlayerMMRData(
            delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    Dictionary<string, Dictionary<string, float>> playerMMR = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, float>>>(resultProcess.CustomData["data"]);
                    _playerInfoData.SetPlayerMMR(playerMMR);

                    Debug.Log("<color=green>Completed</color> LoadPlayerMMRData");
                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                }
                else
                {
                    Debug.LogFormat("<color=red>Failed</color> LoadPlayerMMRData : {0}", resultProcess.MessageDetail);
                    if (onFail != null)
                    {
                        onFail.Invoke(resultProcess.MessageDetail);
                    }
                }
            }
            , delegate (PlayFabError error)
            {
                Debug.LogFormat("<color=red>Failed</color> LoadPlayerMMRData : {0}", error.ErrorMessage);

                GameHelper.DelayCallback(
                      1.0f
                    , delegate ()
                    {
                        Debug.Log("Retry LoadPlayerMMRData ...");
                        LoadPlayerMMRData(onComplete, onFail);
                    }
                );
            }
        );
    }

    private void OnLoadPlayerStatKeysComplete(GetTitleDataResult result)
    {
        _playerStatKeys = new List<string>();
        Dictionary<string, object> data = new Dictionary<string, object>();
        if (result.Data.ContainsKey(_titlePlayerStatKeysPlayfabKey))
        {
            data = JsonConvert.DeserializeObject<Dictionary<string, object>>(result.Data[_titlePlayerStatKeysPlayfabKey]);
            _playerStatKeys = JsonConvert.DeserializeObject<List<string>>(data["key_list"].ToString());
        }

        Debug.Log("Load PlayerStatKeys <color=green>Completed</color>");
    }

    public void LoadPlayerStat(UnityAction onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.GetPlayerReadOnlyData(_playerStatKeys
            , delegate (GetUserDataResult result)
            {
                OnLoadPlayerStatComplete(result);
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            }
            , delegate (PlayFabError error)
            {
                if (onFail != null)
                {
                    onFail.Invoke(error.Error.ToString());
                }
            }
        );
    }

    private void OnLoadPlayerStatComplete(GetUserDataResult result)
    {
        Dictionary<string, Dictionary<string, int>> dataVal = new Dictionary<string, Dictionary<string, int>>();
        foreach (string key in _playerStatKeys)
        {
            if (result.Data.ContainsKey(key))
            {
                dataVal.Add(key, JsonConvert.DeserializeObject<Dictionary<string, int>>(result.Data[key].Value));
            }
        }

        _playerInfoData.SetPlayerStatData(dataVal);

        Debug.Log("Load PlayerStat <color=green>Completed</color>");
    }

    public void CountPlayerStat(PlayerStatDataList playerStatDataList, UnityAction onComplete, UnityAction<string> onFail)
    {
        if (playerStatDataList.IsDataEmpty())
        {
            Debug.Log("CountPlayerStat: [Error] playerStatDataList is empty");

            onComplete?.Invoke();
            return;
        }

        Debug.Log(string.Format(
              "<color=magenta>Start Count PlayerStat</color> : {0}"
            , playerStatDataList.ToString())
        );

        PlayFabManager.Instance.CountPlayerStat(
              playerStatDataList.ToDictionary()
            , delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    Dictionary<string, Dictionary<string, int>> playerStat = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, int>>>(resultProcess.CustomData["stat"]);
                    _playerInfoData.SetPlayerStatData(playerStat);

                    // check GameObjective notification
                    CheckLocalGameObjectiveNotification();

                    Debug.Log("<color=magenta>CountPlayerStat</color> : <color=green>Completed</color>");
                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                }
                else
                {
                    Debug.LogFormat("<color=magenta>CountPlayerStat</color> : <color=red>Failed</color>");

                    if (resultProcess.ResultType == CloudScriptResultProcess.ResultTypes.Fail)
                    {
                        onFail?.Invoke(resultProcess.MessageDetail);
                    }
                    else
                    {
                        // Retry
                        Debug.LogErrorFormat("CountPlayerStat: [Error] {0}. Retry...", resultProcess.MessageDetail);
                        CountPlayerStat(playerStatDataList, onComplete, onFail);
                    }
                }

            }, delegate (PlayFabError error)
            {
                // Retry
                Debug.LogErrorFormat("CountPlayerStat: [Error] {0}. Retry...", error.Error.ToString());
                CountPlayerStat(playerStatDataList, onComplete, onFail);
            }
        );
    }

    public void CountPlayerStatSet(PlayerStatDataList playerStatDataList, UnityAction onComplete, UnityAction<string> onFail)
    {
        if (playerStatDataList.IsDataEmpty())
        {
            Debug.Log("CountPlayerStatSet: [Error] playerStatDataList is empty");

            onComplete?.Invoke();
            return;
        }

        Debug.Log(string.Format(
              "<color=magenta>Start Count PlayerStatSet</color> : {0}"
            , playerStatDataList.ToString())
        );

        PlayFabManager.Instance.CountPlayerStatSet(
              playerStatDataList.ToDictionary()
            , delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    Dictionary<string, Dictionary<string, int>> playerStat = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, int>>>(resultProcess.CustomData["stat"]);
                    _playerInfoData.SetPlayerStatData(playerStat);

                    // check GameObjective notification
                    CheckLocalGameObjectiveNotification();

                    Debug.Log("<color=magenta>CountPlayerStatSet</color> : <color=green>Completed</color>");
                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                }
                else
                {
                    Debug.LogFormat("<color=magenta>CountPlayerStatSet</color> : <color=red>Failed</color>");

                    if (resultProcess.ResultType == CloudScriptResultProcess.ResultTypes.Fail)
                    {
                        onFail?.Invoke(resultProcess.MessageDetail);
                    }
                    else
                    {
                        // Retry
                        Debug.LogErrorFormat("CountPlayerStatSet: [Error] {0}. Retry...", resultProcess.MessageDetail);
                        CountPlayerStat(playerStatDataList, onComplete, onFail);
                    }
                }

            }, delegate (PlayFabError error)
            {
                // Retry
                Debug.LogErrorFormat("CountPlayerStatSet: [Error] {0}. Retry...", error.Error.ToString());
                CountPlayerStatSet(playerStatDataList, onComplete, onFail);
            }
        );
    }

    public void CountPlayerStatMatch(PlayerStatDataList playerStatDataList, UnityAction onComplete, UnityAction<string> onFail)
    {
        if (playerStatDataList.IsDataEmpty())
        {
            Debug.Log("CountPlayerStatMatch: [Error] playerStatDataList is empty");

            onComplete?.Invoke();
            return;
        }

        Debug.Log(string.Format(
              "<color=magenta>Start Count PlayerStatMatch</color> : {0}"
            , playerStatDataList.ToString())
        );

        PlayFabManager.Instance.CountPlayerStatMatch(
              GetRoomName()
            , _lastMatchPlayerInfoData.PlayerMMR.CurrentMMRValue
            , playerStatDataList.ToDictionary()
            , delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    Dictionary<string, Dictionary<string, int>> playerStat = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, int>>>(resultProcess.CustomData["stat"]);
                    Dictionary<string, object> playerRank = JsonConvert.DeserializeObject<Dictionary<string, object>>(resultProcess.CustomData["rank"]);
                    Dictionary<string, Dictionary<string, float>> playerMMR = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, float>>>(resultProcess.CustomData["mmr"]);
                    Dictionary<string, int> playerExpFaction = JsonConvert.DeserializeObject<Dictionary<string, int>>(resultProcess.CustomData["exp_faction"]);
                    int playerExp = int.Parse(resultProcess.CustomData["exp"]);

                    PlayerInfoData oldData = new PlayerInfoData(_playerInfoData);

                    _playerInfoData.SetPlayerStatData(playerStat);
                    _playerInfoData.SetPlayerRank(playerRank);
                    _playerInfoData.SetPlayerMMR(playerMMR);
                    _playerInfoData.SetLevelFactionData(playerExpFaction);
                    _playerInfoData.SetLevelData(playerExp);

                    Dictionary<CardElementType, LevelFactionData> oldLevelFactionDataList = oldData.LevelFactionData.GetLevelFactionDataList();
                    Dictionary<CardElementType, LevelFactionData> currentLevelFactionDataList = _playerInfoData.LevelFactionData.GetLevelFactionDataList();
                    foreach (KeyValuePair<CardElementType, LevelFactionData> item in oldLevelFactionDataList)
                    {
                        if (currentLevelFactionDataList[item.Key].Level > item.Value.Level)
                        {
                            OnPlayerLevelFactionIncrease?.Invoke(item.Key);
                        }
                    }

                    OnSetPlayerLevel?.Invoke();
                    OnSetPlayerLevelFaction?.Invoke();
                    OnSetSeasonRank?.Invoke();

                    // GameObjective Notification
                    CheckLocalGameObjectiveNotification();

                    // level reward
                    //CheckServerLevelReward(null, null);

                    Debug.Log("<color=magenta>CountPlayerStatMatch</color> : <color=green>Completed</color>");

                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                }
                else
                {
                    Debug.LogFormat("<color=magenta>CountPlayerStatMatch</color> : <color=red>Failed</color>");

                    if (resultProcess.ResultType == CloudScriptResultProcess.ResultTypes.Fail)
                    {
                        // "INVALID_MATCH_TICKET"
                        onFail?.Invoke(resultProcess.MessageDetail);
                    }
                    else
                    {
                        // Retry
                        Debug.LogErrorFormat("CountPlayerStatMatch: [Error] {0}. Retry...", resultProcess.MessageDetail);
                        CountPlayerStatMatch(playerStatDataList, onComplete, onFail);
                    }
                }

            }, delegate (PlayFabError error)
            {
                // Retry
                Debug.LogErrorFormat("CountPlayerStatMatch: [Error] {0}. Retry...", error.Error.ToString());
                CountPlayerStatMatch(playerStatDataList, onComplete, onFail);
            }
        );
    }

    public void CountPlayerStatCardSet(PlayerStatDataList playerStatDataList, UnityAction onComplete, UnityAction<string> onFail)
    {
        if (playerStatDataList.IsDataEmpty())
        {
            Debug.Log("CountPlayerStatCardSet: [Error] playerStatDataList is empty");

            onComplete?.Invoke();
            return;
        }

        //Debug.Log(string.Format(
        //      "<color=magenta>Start Count PlayerStatCardSet</color> : {0}"
        //    , playerStatDataList.ToString())
        //);

        PlayFabManager.Instance.CountPlayerStatCardSet(
              playerStatDataList.ToDictionary()
            , delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    Dictionary<string, int> playerStat = JsonConvert.DeserializeObject<Dictionary<string, int>>(resultProcess.CustomData["stat"]);
                    _playerInfoData.SetPlayerStatData("player_stat_c_set", playerStat);

                    // check GameObjective notification
                    CheckLocalGameObjectiveNotification();

                    Debug.Log("<color=magenta>CountPlayerStatCardSet</color> : <color=green>Completed</color>");
                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                }
                else
                {
                    Debug.LogFormat("<color=magenta>CountPlayerStatCardSet</color> : <color=red>Failed</color>");

                    if (resultProcess.ResultType == CloudScriptResultProcess.ResultTypes.Fail)
                    {
                        onFail?.Invoke(resultProcess.MessageDetail);
                    }
                    else
                    {
                        // Retry
                        Debug.LogErrorFormat("CountPlayerStatCardSet: [Error] {0}. Retry...", resultProcess.MessageDetail);
                        CountPlayerStat(playerStatDataList, onComplete, onFail);
                    }
                }

            }, delegate (PlayFabError error)
            {
                // Retry
                Debug.LogErrorFormat("CountPlayerStatCardSet: [Error] {0}. Retry...", error.Error.ToString());
                CountPlayerStatCardSet(playerStatDataList, onComplete, onFail);
            }
        );
    }

    private void CheckLocalGameObjectiveNotification()
    {
        CheckLocalAchievementNotification();
        CheckLocalEventQuestNotification();
        CheckLocalSeasonPassQuestNotification();
    }

    public void ClearPlayerMatchTicket(string matchID, UnityAction onComplete)
    {
        PlayFabManager.Instance.ClearPlayerMatchTicket(
            matchID
            , delegate (ExecuteCloudScriptResult result)
            {
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }

            }, delegate (PlayFabError error)
            {
                ClearPlayerMatchTicket(matchID, onComplete);
            }
        );
    }

    public void UpdatePlayerMatchTicket(PlayerStatDataList loseStatData, string matchID, UnityAction onComplete)
    {
        PlayFabManager.Instance.UpdatePlayerMatchTicket(
              loseStatData.ToDictionary()
            , matchID
            , delegate (ExecuteCloudScriptResult result)
            {
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }

            }, delegate (PlayFabError error)
            {
                UpdatePlayerMatchTicket(loseStatData, matchID, onComplete);
            }
        );
    }
    #endregion

    #region Quest Methods
    private void OnLoadTitleQuestDBDataComplete(GetTitleDataResult result)
    {
        if (result.Data.ContainsKey(_titleQuestDBDataPlayfabKey))
        {
            QuestDB.Instance.LoadFromJSON(result.Data[_titleQuestDBDataPlayfabKey]);
        }

        if (result.Data.ContainsKey(_titleQuestListDataPlayfabKey))
        {
            QuestListDB.Instance.SetData(result.Data[_titleQuestListDataPlayfabKey]);
        }

        Debug.Log("LoadTitleQuestDBData <color=green>Completed</color>");
    }
    #endregion

    #region Event Quest
    private void OnLoadPlayerEventQuestDataComplete(GetUserDataResult result)
    {
        // event quest
        if (result.Data.ContainsKey(_playerEventQuestPlayfabKey))
        {
            _playerEventQuestData = new PlayerEventQuestData(result.Data[_playerEventQuestPlayfabKey].Value);
        }

        Debug.Log("Load PlayerEventQuestData <color=green>Completed</color>");
    }

    private void ConvertOldEventQuest(UnityAction onComplete, UnityAction<string> onFail)
    {
        Debug.Log("Start ConvertOldEventQuest ...");

        PlayFabManager.Instance.ConvertEventQuest(
            delegate (ExecuteCloudScriptResult result)
            {
                Debug.Log("<color=green>Completed</color> ConvertOldEventQuest");
                onComplete?.Invoke();
            }
            , delegate (PlayFabError error)
            {
                Debug.LogFormat("<color=red>Failed</color> ConvertOldEventQuest : {0}", error.ErrorMessage);

                GameHelper.DelayCallback(
                      1.0f
                    , delegate ()
                    {
                        Debug.Log("Retry ConvertOldEventQuest ...");
                        ConvertOldEventQuest(onComplete, onFail);
                    }
                );
            }
        );
    }

    private void RefreshEventQuest(UnityAction onComplete, UnityAction<string> onFail)
    {
        Debug.Log("Start RefreshEventQuest ...");

        PlayFabManager.Instance.RefreshEventQuest(
            delegate (ExecuteCloudScriptResult result)
            {
                Debug.Log("<color=green>Completed</color> RefreshEventQuest");
                onComplete?.Invoke();
            }
            , delegate (PlayFabError error)
            {
                Debug.LogFormat("<color=red>Failed</color> RefreshEventQuest : {0}", error.ErrorMessage);

                GameHelper.DelayCallback(
                      1.0f
                    , delegate ()
                    {
                        Debug.Log("Retry RefreshEventQuest ...");
                        RefreshEventQuest(onComplete, onFail);
                    }
                );
            }
        );
    }

    private void CheckLocalEventQuestNotification()
    {
        foreach (QuestData data in _playerEventQuestData.GetQuestDataList())
        {
            if (data.IsCompleted && !data.IsAlreadyCompleted)
            {
                _playerEventQuestData.SetQuestAlreadyCompleted(data.ID);
                PopupUIManager.Instance.ShowPopup_NoticeOnly(
                    LocalizationManager.Instance.GetText("TEXT_QUEST_COMPLETE")
                    , LocalizationManager.Instance.GetText(string.Format("{0}_TITLE", data.ID))
                    , LocalizationManager.Instance.GetText(string.Format("{0}_DESCRIPTION", data.ID)));
            }
        }
    }

    public void CheckServerEventQuest(string questID, UnityAction onComplete, UnityAction<string> onFail)
    {
        Debug.Log("Start CheckServerEventQuest ...");
        PlayFabManager.Instance.CheckEventQuest(
              questID
            , delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    _playerEventQuestData.SetData(resultProcess.CustomData["quest_data"]);
                    Dictionary<string, int> rewardDataList = JsonConvert.DeserializeObject<Dictionary<string, int>>(resultProcess.CustomData["reward"]);
                    _playerInventoryData.GrantItemByItemList(ConvertDictionaryToItemDataList(rewardDataList));

                    OnCompleteQuest?.Invoke(_playerEventQuestData.GetQuestData(questID));

                    Debug.Log("<color=green>Completed</color> CheckServerEventQuest");
                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                }
                else
                {
                    Debug.LogFormat("<color=red>Failed</color> CheckServerEventQuest : {0}", resultProcess.MessageDetail);
                    if (onFail != null)
                    {
                        onFail.Invoke(resultProcess.MessageDetail);
                    }
                }
            }
            , delegate (PlayFabError error)
            {
                Debug.LogFormat("<color=red>Failed</color> CheckServerEventQuest : {0}", error.ErrorMessage);

                GameHelper.DelayCallback(
                      1.0f
                    , delegate ()
                    {
                        Debug.Log("Retry CheckServerEventQuest ...");
                        CheckServerEventQuest(questID, onComplete, onFail);
                    }
                );
            }
        );
    }

    public void CheckServerSubEventQuest(string questID, string eventKey, UnityAction<string> onComplete, UnityAction<string> onFail)
    {
        Debug.Log("Start CheckServerSubEventQuest ...");
        PlayFabManager.Instance.CheckSubEventQuest(
              questID
            , eventKey
            , delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    Dictionary<string, int> rewardDataList = JsonConvert.DeserializeObject<Dictionary<string, int>>(resultProcess.CustomData["reward"]);
                    _playerInventoryData.GrantItemByItemList(ConvertDictionaryToItemDataList(rewardDataList));

                    Debug.Log("<color=green>Completed</color> CheckServerSubEventQuest");
                    if (onComplete != null)
                    {
                        onComplete.Invoke(resultProcess.CustomData["quest_data"]);
                    }
                }
                else
                {
                    Debug.LogFormat("<color=red>Failed</color> CheckServerSubEventQuest : {0}", resultProcess.MessageDetail);
                    if (onFail != null)
                    {
                        onFail.Invoke(resultProcess.MessageDetail);
                    }
                }
            }
            , delegate (PlayFabError error)
            {
                Debug.LogFormat("<color=red>Failed</color> CheckServerSubEventQuest : {0}", error.ErrorMessage);
                onFail.Invoke(error.ToString());
            }
        );
    }
    #endregion

    #region Achievement
    private void OnLoadTitleAchievementDBDataComplete(GetTitleDataResult result)
    {
        if (result.Data.ContainsKey(_titleAchievementDBDataPlayfabKey))
        {
            AchievementDB.Instance.LoadFromJSON(result.Data[_titleAchievementDBDataPlayfabKey]);
        }

        Debug.Log("LoadTitleAchievementDBData <color=green>Completed</color>");
    }

    private void OnLoadPlayerAchievementDataComplete(GetUserDataResult result)
    {
        string dataStr = "";
        if (result.Data.ContainsKey(_playerAchievementCompletedPropertiesPlayfabKey))
        {
            dataStr = result.Data[_playerAchievementCompletedPropertiesPlayfabKey].Value;
        }
        _playerAchievementData = new PlayerAchievementData(dataStr);

        Debug.Log("Load PlayerAchievementData <color=green>Completed</color>");
    }

    private void CheckLocalAchievementNotification()
    {
        foreach (AchievementData data in _playerAchievementData.AchievementList)
        {
            if (data.IsCompleted && !data.IsAlreadyCompleted)
            {
                _playerAchievementData.DoCompleteAchievement(data.ID);

                PopupUIManager.Instance.ShowPopup_NoticeOnly(
                    LocalizationManager.Instance.GetText("TEXT_ACHIEVEMENT_COMPLETE")
                    , LocalizationManager.Instance.GetText(string.Format("{0}_TITLE", data.ID))
                    , LocalizationManager.Instance.GetText(string.Format("{0}_DESCRIPTION", data.ID)));
                OnCompleteAchievement?.Invoke(data);
            }
        }
    }

    public void CheckServerAchievement(string achievementID, UnityAction onComplete, UnityAction<string> onFail)
    {
        if (!DataManager.Instance.IsFeatureEnable(FeatureUnlock.Achievement))
        {
            onFail?.Invoke("ERROR_CHECK_ACHIEVEMENT_NOT_COMPLETE");
            return;
        }

        PlayFabManager.Instance.CheckAchievement(achievementID
            , delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    _playerAchievementData.DoClearAchievement(achievementID);
                    LoadInventory(null, null);

                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                }
                else
                {
                    if (onFail != null)
                    {
                        onFail.Invoke(resultProcess.MessageDetail);
                    }
                }
            }
            , delegate (PlayFabError error)
            {
                GameHelper.DelayCallback(
                      1.0f
                    , delegate ()
                    {
                        CheckServerAchievement(achievementID, onComplete, onFail);
                    }
                );
            }
        );
    }
    #endregion

    #region Achievement Card Set
    private void OnLoadTitleAchievementCardSetDBDataComplete(GetTitleDataResult result)
    {
        if (result.Data.ContainsKey(_titleAchievementCardSetDBDataPlayfabKey))
        {
            AchievementCardSetDB.Instance.LoadFromJSON(result.Data[_titleAchievementCardSetDBDataPlayfabKey]);
        }

        Debug.Log("LoadTitleAchievementCardSetDBData <color=green>Completed</color>");
    }

    private void OnLoadPlayerAchievementCardSetDataComplete(GetUserDataResult result)
    {
        string dataStr = "";
        if (result.Data.ContainsKey(_playerAchievementCardSetCompletedPropertiesPlayfabKey))
        {
            dataStr = result.Data[_playerAchievementCardSetCompletedPropertiesPlayfabKey].Value;
        }
        _playerAchievementCardSet = new PlayerAchievementCardSet(dataStr);

        Debug.Log("Load PlayerAchievementCardSetData <color=green>Completed</color>");
    }

    private void CheckLocalAchievementCardSetNotification()
    {
        foreach (AchievementCardSetData data in _playerAchievementCardSet.AchievementList)
        {
            if (data.IsCompleted && !data.IsAlreadyCompleted)
            {
                _playerAchievementCardSet.DoCompleteAchievement(data.ID);

                PopupUIManager.Instance.ShowPopup_NoticeOnly(
                    LocalizationManager.Instance.GetText("TEXT_ACHIEVEMENT_COMPLETE")
                    , LocalizationManager.Instance.GetText(string.Format("{0}_TITLE", data.ID))
                    , LocalizationManager.Instance.GetText(string.Format("{0}_DESCRIPTION", data.ID)));
            }
        }
    }

    public void CheckServerAchievementCardSet(string achievementID, UnityAction onComplete, UnityAction<string> onFail)
    {
        if (!DataManager.Instance.IsFeatureEnable(FeatureUnlock.CardSet))
        {
            onFail?.Invoke("ERROR_CHECK_CARD_SET_NOT_COMPLETE");
            return;
        }

        PlayFabManager.Instance.CheckAchievementCardSet(achievementID
            , delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    _playerAchievementCardSet.DoClearAchievement(achievementID);
                    LoadInventory(null, null);

                    OnCompleteAchievementCardSet?.Invoke(achievementID);
                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                }
                else
                {
                    if (onFail != null)
                    {
                        onFail.Invoke(resultProcess.MessageDetail);
                    }
                }
            }
            , delegate (PlayFabError error)
            {
                CheckServerAchievementCardSet(achievementID, onComplete, onFail);
            }
        );
    }
    #endregion

    #region Season Pass Methods
    private void OnLoadTitleSeasonPassDBComplete(GetTitleDataResult result)
    {
        if (result.Data.ContainsKey(_titleSeasonPassDBDataPlayfabKey))
        {
            SeasonPassDB.Instance.LoadFromJSON(result.Data[_titleSeasonPassDBDataPlayfabKey]);
            Debug.Log("LoadTitleSeasonPassDBData <color=green>Completed</color>");
        }
        else
        {
            Debug.Log("LoadTitleSeasonPassDBData <color=red>Fail</color>");
        }
    }

    private void OnLoadTitleSeasonPassTierDataComplete(GetTitleDataResult result)
    {
        if (result.Data.ContainsKey(_titleSeasonPassTierDataPlayfabKey))
        {
            SeasonPassTierDB.Instance.LoadFromJSON(result.Data[_titleSeasonPassTierDataPlayfabKey]);
            Debug.Log("LoadTitleSeasonPassTierData <color=green>Completed</color>");
        }
        else
        {
            Debug.Log("LoadTitleSeasonPassTierData <color=red>Fail</color>");
        }
    }

    private void OnLoadTitleSeasonPassTierRewardDataComplete(GetTitleDataResult result)
    {
        if (result.Data.ContainsKey(_titleSeasonPassTierRewardDataPlayfabKey))
        {
            SeasonPassTierRewardDB.Instance.LoadFromJSON(result.Data[_titleSeasonPassTierRewardDataPlayfabKey]);
            Debug.Log("LoadTitleSeasonPassTierRewardData <color=green>Completed</color>");
        }
        else
        {
            Debug.Log("LoadTitleSeasonPassTierRewardData <color=red>Fail</color>");
        }
    }

    private void OnLoadTitleSeasonPassPriceDBDataComplete(GetTitleDataResult result)
    {
        if (result.Data.ContainsKey(_titleSeasonPassPriceDBDataPlayfabKey))
        {
            SeasonPassPriceDB.Instance.LoadFromJSON(result.Data[_titleSeasonPassPriceDBDataPlayfabKey]);
            Debug.Log("LoadTitleSeasonPassPriceDBData <color=green>Completed</color>");
        }
        else
        {
            Debug.Log("LoadTitleSeasonPassPriceDBData <color=red>Fail</color>");
        }
    }

    private void OnLoadPlayerSeasonPassDataComplete(GetUserDataResult result)
    {
        if (result.Data.ContainsKey(_playerSeasonPassDataPlayfabKey))
        {
            _playerSeasonPassData.SetData(result.Data[_playerSeasonPassDataPlayfabKey].Value);
            Debug.Log("LoadPlayerSeasonPassData <color=green>Completed</color>");
        }
        else
        {
            Debug.Log("LoadPlayerSeasonPassData <color=red>Fail</color>");
        }
    }

    private void OnLoadPlayerSeasonPassQuestDataComplete(GetUserDataResult result)
    {
        if (result.Data.ContainsKey(_playerSeasonPassQuestPlayfabKey))
        {
            _playerSeasonPassQuestData.SetData(result.Data[_playerSeasonPassQuestPlayfabKey].Value);
            Debug.Log("LoadPlayerSeasonPassQuestData <color=green>Completed</color>");
        }
        else
        {
            Debug.Log("LoadPlayerSeasonPassQuestData <color=red>Fail</color>");
        }
    }

    private void OnLoadPlayerSeasonPassRewardDataComplete(GetUserDataResult result)
    {
        if (result.Data.ContainsKey(_playerSeasonPassClaimedRecordPlayfabKey))
        {
            _playerSeasonPassRewardRecordData.SetData(result.Data[_playerSeasonPassClaimedRecordPlayfabKey].Value);
            Debug.Log("LoadPlayerSeasonPassRewardData <color=green>Completed</color>");
        }
        else
        {
            Debug.Log("LoadPlayerSeasonPassRewardData <color=red>Fail</color>");
        }
    }

    public int GetCurrentSeasonPassIndex()
    {
        SeasonPassDBData data = SeasonPassDB.Instance.GetCurrentSeasonPassDBData();
        if (data != null)
        {
            return data.SeasonPassIndex;
        }

        return -1;
    }

    public void RefreshSeasonPassQuests(UnityAction onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.RefreshSeasonPassQuests(
            delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    _playerSeasonPassDataOld.SetData(resultProcess.CustomData["season_pass_data_old"]);
                    _playerSeasonPassQuestDataOld.SetData(resultProcess.CustomData["season_pass_quest_old"]);
                    OnRefreshSeasonPassQuest?.Invoke();
                }

                onComplete?.Invoke();
            }
            , delegate (PlayFabError error)
            {
                RefreshSeasonPassQuests(onComplete, onFail);
            }
        );
    }

    public void RefreshAndUpdateSeasonPassQuests(UnityAction onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.RefreshSeasonPassQuests(
            delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    _playerSeasonPassData.SetData(resultProcess.CustomData["season_pass_data"]);
                    _playerSeasonPassQuestData.SetData(resultProcess.CustomData["season_pass_quest"]);
                    _playerSeasonPassDataOld.SetData(resultProcess.CustomData["season_pass_data_old"]);
                    _playerSeasonPassQuestDataOld.SetData(resultProcess.CustomData["season_pass_quest_old"]);
                    OnRefreshSeasonPassQuest?.Invoke();
                }

                onComplete?.Invoke();
            }
            , delegate (PlayFabError error)
            {
                RefreshAndUpdateSeasonPassQuests(onComplete, onFail);
            }
        );
    }

    public void CheckServerSeasonPassQuest(string questID, UnityAction<List<ItemData>> onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.CheckSeasonPassQuest(
            questID
            , delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    _playerSeasonPassData.SetData(resultProcess.CustomData["season_pass_data"]);
                    _playerSeasonPassQuestData.SetQuestCleared(questID);
                    Dictionary<string, int> reward = JsonConvert.DeserializeObject<Dictionary<string, int>>(resultProcess.CustomData["reward"]);
                    List<ItemData> rewardResult = ConvertDictionaryToItemDataList(reward);

                    OnCompleteQuest?.Invoke(_playerSeasonPassQuestData.GetQuestData(questID));
                    onComplete?.Invoke(rewardResult);
                }
                else
                {
                    onFail?.Invoke(resultProcess.MessageDetail);
                }
            }
            , delegate (PlayFabError error)
            {
                onFail?.Invoke(error.Error.ToString());
            }
        );
    }

    public void CheckServerSeasonPassAllQuests(UnityAction<List<string>> onQuestIDComplete, UnityAction<List<ItemData>> onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.CheckSeasonPassAllQuests(
            delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    _playerSeasonPassData.SetData(resultProcess.CustomData["season_pass_data"]);
                    //_playerSeasonPassQuestData.SetData(resultProcess.CustomData["season_pass_quest"]);
                    List<string> completedQuestIDList = JsonConvert.DeserializeObject<List<string>>(resultProcess.CustomData["completed_quest_id"]);
                    Dictionary<string, int> reward = JsonConvert.DeserializeObject<Dictionary<string, int>>(resultProcess.CustomData["reward"]);
                    List<ItemData> rewardResult = ConvertDictionaryToItemDataList(reward);

                    foreach (string id in completedQuestIDList)
                    {
                        _playerSeasonPassQuestData.SetQuestCleared(id);
                        OnCompleteQuest?.Invoke(_playerSeasonPassQuestData.GetQuestData(id));
                    }
                    onQuestIDComplete?.Invoke(completedQuestIDList);
                    onComplete?.Invoke(rewardResult);
                }
                else
                {
                    onFail?.Invoke(resultProcess.MessageDetail);
                }
            }
            , delegate (PlayFabError error)
            {
                onFail?.Invoke(error.Error.ToString());
            }
        );
    }

    private void CheckLocalSeasonPassQuestNotification()
    {
        foreach (QuestData data in _playerSeasonPassQuestData.GetQuestDataList())
        {
            if (data.IsCompleted && !data.IsAlreadyCompleted)
            {
                _playerSeasonPassQuestData.SetQuestAlreadyCompleted(data.ID);
                PopupUIManager.Instance.ShowPopup_NoticeOnly(
                    LocalizationManager.Instance.GetText("TEXT_QUEST_COMPLETE")
                    , LocalizationManager.Instance.GetText(string.Format("{0}_TITLE", data.ID))
                    , LocalizationManager.Instance.GetText(string.Format("{0}_DESCRIPTION", data.ID)));
            }
        }
    }

    public void CheckSeasonPassAllTierReward(UnityAction<List<ItemData>> onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.CheckSeasonPassAllTierReward(
            delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    _playerSeasonPassRewardRecordData.SetData(resultProcess.CustomData["claimed_reward"]);
                    Dictionary<string, int> reward = JsonConvert.DeserializeObject<Dictionary<string, int>>(resultProcess.CustomData["reward"]);
                    List<ItemData> rewardResult = ConvertDictionaryToItemDataList(reward);
                    onComplete?.Invoke(rewardResult);
                }
                else
                {
                    onFail?.Invoke(resultProcess.MessageDetail);
                }
            }
            , delegate (PlayFabError error)
            {
                onFail?.Invoke(error.Error.ToString());
            }
        );
    }

    public void PurchaseCurrentSeasonPassPremium(UnityAction<List<ItemData>> onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.PurchaseSeasonPassPremium(
            GetCurrentSeasonPassIndex().ToString()
            , delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    _playerSeasonPassData.SetData(resultProcess.CustomData["season_pass_data"]);
                    _playerSeasonPassRewardRecordData.SetData(resultProcess.CustomData["claimed_reward"]);
                    Dictionary<string, int> reward = JsonConvert.DeserializeObject<Dictionary<string, int>>(resultProcess.CustomData["reward"]);
                    List<ItemData> rewardResult = ConvertDictionaryToItemDataList(reward);
                    LoadInventory(
                        delegate ()
                        {
                            onComplete?.Invoke(rewardResult);
                        }
                        , null
                    );
                }
                else
                {
                    if (resultProcess.ResultType == CloudScriptResultProcess.ResultTypes.Fail)
                    {
                        if (resultProcess.MessageDetail == "INVALID_SEASON_INDEX")
                        {
                            RefreshAndUpdateSeasonPassQuests(
                                delegate ()
                                {
                                    onFail?.Invoke(resultProcess.MessageDetail);
                                }
                                , onFail
                            );
                            return;
                        }
                    }
                    onFail?.Invoke(resultProcess.MessageDetail);
                }
            }
            , delegate (PlayFabError error)
            {
                onFail?.Invoke(error.Error.ToString());
            }
        );
    }

    public void PurchaseCurrentSeasonPassTier(int tierAmount, UnityAction<List<ItemData>> onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.PurchaseSeasonPassTier(
            GetCurrentSeasonPassIndex().ToString()
            , tierAmount
            , delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    _playerSeasonPassData.SetData(resultProcess.CustomData["season_pass_data"]);
                    _playerSeasonPassRewardRecordData.SetData(resultProcess.CustomData["claimed_reward"]);
                    Dictionary<string, int> reward = JsonConvert.DeserializeObject<Dictionary<string, int>>(resultProcess.CustomData["reward"]);
                    List<ItemData> rewardResult = ConvertDictionaryToItemDataList(reward);
                    LoadInventory(
                        delegate ()
                        {
                            onComplete?.Invoke(rewardResult);
                        }
                        , null
                    );
                }
                else
                {
                    if (resultProcess.ResultType == CloudScriptResultProcess.ResultTypes.Fail)
                    {
                        if (resultProcess.MessageDetail == "INVALID_SEASON_INDEX")
                        {
                            RefreshAndUpdateSeasonPassQuests(
                                delegate ()
                                {
                                    onFail?.Invoke(resultProcess.MessageDetail);
                                }
                                , onFail
                            );
                            return;
                        }
                    }
                    onFail?.Invoke(resultProcess.MessageDetail);
                }
            }
            , delegate (PlayFabError error)
            {
                onFail?.Invoke(error.Error.ToString());
            }
        );
    }
    #endregion

    #region Level Reward Methods
    private void OnLoadLevelRewardDataComplete(GetTitleDataResult result)
    {
        if (result.Data.ContainsKey(_titleLevelRewardDataPlayfabKey))
        {
            LevelRewardDB.Instance.LoadFromJSON(result.Data[_titleLevelRewardDataPlayfabKey]);
        }

        Debug.Log("LoadLevelRewardData <color=green>Completed</color>");
    }

    private void OnLoadPlayerLevelRewardRecordDataComplete(GetUserDataResult result)
    {
        if (result.Data.ContainsKey(_playerLevelRewardRecordPlayfabKey))
        {
            _playerLevelRewardRecordData.SetData(result.Data[_playerLevelRewardRecordPlayfabKey].Value);
        }

        Debug.Log("LoadLevelRewardRecordData <color=green>Completed</color>");
    }

    public void CheckServerLevelReward(UnityAction onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.CheckLevelReward(
            delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    LoadInventory(null, null);
                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                }
                else
                {
                    if (onFail != null)
                    {
                        onFail.Invoke(resultProcess.MessageDetail);
                    }
                }
            }
            , delegate (PlayFabError error)
            {
                CheckServerLevelReward(onComplete, onFail);
            }
        );
    }

    public void CheckServerLevelReward(int level, UnityAction<List<ItemData>> onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.CheckLevelRewardByLevel(
            level
            , delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    // update reward record
                    _playerLevelRewardRecordData.SetData(resultProcess.CustomData["claimed_reward"]);

                    // get reward data list
                    Dictionary<string, int> rewardDataList = JsonConvert.DeserializeObject<Dictionary<string, int>>(resultProcess.CustomData["reward"]);
                    List<ItemData> completedResult = new List<ItemData>();
                    completedResult = ConvertDictionaryToItemDataList(rewardDataList);

                    LoadInventory(null, null);
                    if (onComplete != null)
                    {
                        onComplete.Invoke(completedResult);
                    }
                }
                else
                {
                    if (onFail != null)
                    {
                        onFail.Invoke(resultProcess.MessageDetail);
                    }
                }
            }
            , delegate (PlayFabError error)
            {
                if (onFail != null)
                {
                    onFail.Invoke(error.Error.ToString());
                }
            }
        );
    }
    #endregion

    #region Level Faction Reward Methods
    private void OnLoadLevelFactionRewardDataComplete(GetTitleDataResult result)
    {
        if (result.Data.ContainsKey(_titleLevelFactionRewardDataPlayfabKey))
        {
            LevelFactionRewardDB.Instance.LoadFromJSON(result.Data[_titleLevelFactionRewardDataPlayfabKey]);
        }

        Debug.Log("LoadLevelFactionRewardData <color=green>Completed</color>");
    }

    private void OnLoadPlayerLevelFactionRewardRecordDataComplete(GetUserDataResult result)
    {
        if (result.Data.ContainsKey(_playerLevelFactionRewardRecordPlayfabKey))
        {
            _playerLevelFactionRewardRecordData.SetData(result.Data[_playerLevelFactionRewardRecordPlayfabKey].Value);
        }

        Debug.Log("LoadLevelFactionRewardRecordData <color=green>Completed</color>");
    }

    public void CheckServerLevelFactionReward(string factionID, UnityAction<List<ItemData>> onComplete, UnityAction<string> onFail)
    {
        if (!DataManager.Instance.IsFeatureEnable(FeatureUnlock.FactionLevel))
        {
            onFail?.Invoke("ERROR_CHECK_LEVEL_FACTION_NOT_COMPLETE");
            return;
        }

        PlayFabManager.Instance.CheckLevelFactionReward(
             factionID
             , delegate (ExecuteCloudScriptResult result)
             {
                 CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                 if (resultProcess.IsSuccess)
                 {
                     // update reward record
                     _playerLevelFactionRewardRecordData.SetData(resultProcess.CustomData["claimed_reward"]);

                     // get reward data list
                     Dictionary<string, int> rewardDataList = JsonConvert.DeserializeObject<Dictionary<string, int>>(resultProcess.CustomData["reward"]);
                     List<ItemData> completedResult = new List<ItemData>();
                     completedResult = ConvertDictionaryToItemDataList(rewardDataList);

                     LoadInventory(null, null);
                     if (onComplete != null)
                     {
                         onComplete.Invoke(completedResult);
                     }
                 }
                 else
                 {
                     if (onFail != null)
                     {
                         onFail.Invoke(resultProcess.MessageDetail);
                     }
                 }
             }
             , delegate (PlayFabError error)
             {
                 if (onFail != null)
                 {
                     onFail.Invoke(error.Error.ToString());
                 }
             }
         );
    }

    public void CheckServerLevelFactionReward(string factionID, int level, UnityAction<List<ItemData>> onComplete, UnityAction<string> onFail)
    {
        if (!DataManager.Instance.IsFeatureEnable(FeatureUnlock.FactionLevel))
        {
            onFail?.Invoke("ERROR_CHECK_LEVEL_FACTION_NOT_COMPLETE");
            return;
        }

        PlayFabManager.Instance.CheckLevelFactionRewardByLevel(
             factionID
             , level
             , delegate (ExecuteCloudScriptResult result)
             {
                 CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                 if (resultProcess.IsSuccess)
                 {
                     // update reward record
                     _playerLevelFactionRewardRecordData.SetData(resultProcess.CustomData["claimed_reward"]);

                     // get reward data list
                     Dictionary<string, int> rewardDataList = JsonConvert.DeserializeObject<Dictionary<string, int>>(resultProcess.CustomData["reward"]);
                     List<ItemData> completedResult = new List<ItemData>();
                     completedResult = ConvertDictionaryToItemDataList(rewardDataList);

                     LoadInventory(null, null);
                     if (onComplete != null)
                     {
                         onComplete.Invoke(completedResult);
                     }
                 }
                 else
                 {
                     if (onFail != null)
                     {
                         onFail.Invoke(resultProcess.MessageDetail);
                     }
                 }
             }
             , delegate (PlayFabError error)
             {
                 if (onFail != null)
                 {
                     onFail.Invoke(error.Error.ToString());
                 }
             }
         );
    }
    #endregion

    #region GameEvent Methods

    private void OnLoadGameEventDataComplete(GetTitleDataResult result)
    {
        if (result.Data.ContainsKey(_gameEventKey))
        {
            Dictionary<string, object> rawGameEventData = JsonConvert.DeserializeObject<Dictionary<string, object>>(result.Data[_gameEventKey]);
            string eventListJson = rawGameEventData["event_list"].ToString();
            string eventThemeJson = rawGameEventData["event_theme"].ToString();
            string activeECJson = rawGameEventData["active_ec"].ToString();
            string eventQuestListJson = rawGameEventData["event_quest"].ToString();
            _gameEventDB = new GameEventDB(eventListJson, eventThemeJson, activeECJson, eventQuestListJson);

        }
        Debug.Log("LoadGameEventData <color=green>Completed</color>");
    }

    public GameEventDB GetGameEventDB()
    {
        return _gameEventDB;
    }

    private void OnLoadShopEventComplete(GetTitleDataResult result)
    {
        if (result.Data.ContainsKey(_storeEventKey))
        {
            _shopEventData = new ShopEventData(result.Data[_storeEventKey]);
        }
        Debug.Log("LoadShopEventData <color=green>Completed</color>");
    }

    public ShopEventData GetShopEventInfo()
    {
        return _shopEventData;
    }

    public void PurchaseShopEventItem(string itemID, UnityAction onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.PurchaseShopEventItem(
        itemID
        , delegate (ExecuteCloudScriptResult result)
        {
            CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
            if (resultProcess.IsSuccess)
            {
                //TODO: Event Currency Rework.

                //string rawEventCurrencyJson = resultProcess.CustomData["player_event_currency"];
                //_playerInventoryData.SetEventCurrency(rawEventCurrencyJson);

                bool isSuccess = GameHelper.TryDeserializeJSONStr(resultProcess.CustomData["used_event_currency"], out Dictionary<string, int> ecDict);
                if (isSuccess)
                {
                    List<ItemData> ecList = ConvertDictionaryToItemDataList(ecDict);
                    foreach (ItemData ec in ecList)
                    {
                        Instance.InventoryData.SubEventCurrency(ec.ItemID, ec.Amount);
                    }
                }

                onComplete?.Invoke();
            }
            else
            {
                onFail?.Invoke(resultProcess.MessageDetail);
            }
        }
        , delegate (PlayFabError error)
        {
            onFail?.Invoke(error.Error.ToString());
        }
    );
    }

    public void ExcecuteEventCloudScript(string functionName, object param, UnityAction<CloudScriptResultProcess> onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.ExecuteCloudScript(
          functionName
          , param
          , delegate (ExecuteCloudScriptResult result)
          {
              CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
              onComplete?.Invoke(resultProcess);
          }
          , delegate (PlayFabError error)
          {
              Debug.LogErrorFormat("<color=red>ExcecuteEventCloudScript</color> {0} => {1}", functionName, error.ToString());
              onFail?.Invoke(error.Error.ToString());
          }
      );
    }

    public void RefreshGameEventStat(UnityAction onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.RefreshGameEventStat(
            delegate (ExecuteCloudScriptResult result)
            {
                onComplete?.Invoke();
            }
            , delegate (PlayFabError error)
            {
                RefreshGameEventStat(onComplete, onFail);
            }
        );
    }

    public void RefreshGameEvenPlayerDataValid(UnityAction onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.RefreshGameEventPlayerDataValid(
            delegate (ExecuteCloudScriptResult result)
            {
                onComplete?.Invoke();
            }
            , delegate (PlayFabError error)
            {
                RefreshGameEventStat(onComplete, onFail);
            }
        );
    }
    #endregion

    #region Season Methods
    public void LoadSeasonData(UnityAction onComplete, UnityAction<string> onFail)
    {
        Debug.Log("Start LoadSeasonData ...");

        PlayFabManager.Instance.GetSeasonDB(
            delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    _seasonData = SeasonData.ConvertJsonToSeasonData(resultProcess.CustomData["data"]);
                    _seasonData.IsUpdateSeason = bool.Parse(resultProcess.CustomData["updateSeason"]);
                    _seasonData.refreshTimeStamp = DateTimeData.GetDateTimeUTC();

                    if (_seasonData.IsUpdateSeason)
                    {
                        OnSeasonUpdate?.Invoke();
                    }

                    Debug.Log("<color=green>Completed</color> LoadSeasonData");
                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                }
                else
                {
                    Debug.LogFormat("<color=red>Failed</color> LoadSeasonData : {0}", resultProcess.MessageDetail);
                    if (onFail != null)
                    {
                        onFail.Invoke(resultProcess.MessageDetail);
                    }
                }
            }
            , delegate (PlayFabError error)
            {
                Debug.LogFormat("<color=red>Failed</color> LoadSeasonData : {0}", error.ErrorMessage);

                GameHelper.DelayCallback(
                      1.0f
                    , delegate ()
                    {
                        Debug.Log("Retry LoadSeasonData ...");
                        LoadSeasonData(onComplete, onFail);
                    }
                );
            }
        );
    }

    public int GetCurrentSeasonIndex()
    {
        return _seasonData.CurrentSeason;
    }

    public SeasonData GetSeasonData()
    {
        return _seasonData;
    }

    public SeasonInfo GetSeasonInfoData(int seasonIndex)
    {
        SeasonInfo result;
        if (_seasonData.SeasonDataList.TryGetValue(seasonIndex.ToString(), out result))
        {
            return result;
        }
        else
        {
            Debug.LogErrorFormat("GetSeasonInfoData: Season data key not found. {0}", seasonIndex);

            return new SeasonInfo();
        }
    }

    private int GetNextSeasonIndex()
    {
        int currentSeasonIndex = _seasonData.CurrentSeason;

        bool hasNextSeason = _seasonData.SeasonDataList.ContainsKey((currentSeasonIndex + 1).ToString());
        return hasNextSeason ? currentSeasonIndex + 1 : currentSeasonIndex;
    }

    public string GetSeasonStatus()
    {
        try
        {
            int currentSeason = GetCurrentSeasonIndex();
            int nextSeason = GetNextSeasonIndex();

            TimeSpan time = GetSeasonInfoData(currentSeason).EndDateTimeUTC - DateTimeData.GetDateTimeUTC();
            if (time.TotalSeconds > 0)
            {
                string timeLeftText = time.Days > 0 ? time.Days + " " + LocalizationManager.Instance.GetText("DAILY_DAY") : time.ToString(@"hh\:mm\:ss");
                return String.Format(LocalizationManager.Instance.GetText("SEASON_STATUS_CURRENT_END_DATE"), timeLeftText);
            }
            else
            {
                bool hasNextSeason = GetNextSeasonIndex() - currentSeason > 0 ? true : false;
                if (hasNextSeason)
                {
                    return String.Format(LocalizationManager.Instance.GetText("SEASON_STATUS_NEXT_START_DATE"), nextSeason, GetSeasonInfoData(nextSeason).StartDateTimeUTC.ToString(GameHelper.DateTimeFormatSeason));
                }
                else
                {
                    return String.Format(LocalizationManager.Instance.GetText("SEASON_STATUS_LAST_SEASON"), (currentSeason + 1));
                }
            }
        }
        catch (Exception e)
        {
            return "";
        }
    }

    //----------------------------------------------------------------------------------------

    public void LoadSeasonRankRewardData(UnityAction onComplete, UnityAction<string> onFail)
    {
        Debug.Log("Start LoadSeasonRankRewardData ...");

        PlayFabManager.Instance.GetTitleData(
              _titleRankRewardDataPlayfabKey
            , delegate (GetTitleDataResult result)
            {
                OnLoadSeasonRankRewardDataComplete(result);

                Debug.Log("<color=green>Completed</color> LoadSeasonRankRewardData");
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            }
            , delegate (PlayFabError error)
            {
                Debug.LogFormat("<color=red>Failed</color> LoadSeasonRankRewardData : {0}", error.ErrorMessage);
                if (onFail != null)
                {
                    onFail.Invoke(error.Error.ToString());
                }
            }
        );
    }

    private void OnLoadSeasonRankRewardDataComplete(GetTitleDataResult result)
    {
        if (result.Data.ContainsKey(_titleLevelRewardDataPlayfabKey))
        {
            SeasonRankRewardDB.Instance.LoadFromJSON(result.Data[_titleRankRewardDataPlayfabKey]);
        }

        //Debug.Log("LoadRankRewardData <color=green>Completed</color>");
    }

    private List<ItemData> CreateItemDataList(Dictionary<string, int> itemList)
    {
        List<ItemData> result = new List<ItemData>();
        foreach (KeyValuePair<string, int> item in itemList)
        {
            result.Add(new ItemData(item));
        }
        return result;
    }
    #endregion

    #region Feature Unlock
    public void LoadTitleFeatureUnlock(UnityAction onComplete, UnityAction<string> onFail)
    {
        Debug.Log("Start LoadTitleFeatureUnlock ...");

        PlayFabManager.Instance.GetTitleData(
              _titleFeautureUnlockPlayfabKey
            , delegate (GetTitleDataResult result)
            {
                OnLoadTitleFeatureUnlockComplete(result);

                Debug.Log("<color=green>Completed</color> LoadTitleFeatureUnlock");
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            }
            , delegate (PlayFabError error)
            {
                Debug.LogFormat("<color=red>Failed</color> LoadTitleFeatureUnlock : {0}", error.ErrorMessage);
                if (onFail != null)
                {
                    onFail.Invoke(error.Error.ToString());
                }
            }
        );
    }

    private void OnLoadTitleFeatureUnlockComplete(GetTitleDataResult result)
    {
        if (result.Data.ContainsKey(_titleFeautureUnlockPlayfabKey))
        {
            string json = result.Data[_titleFeautureUnlockPlayfabKey];

            try
            {
                _titleFeatureUnlockDict = JsonConvert.DeserializeObject<Dictionary<string, FeatureUnlockData>>(json);
            }
            catch
            {
                _titleFeatureUnlockDict = new Dictionary<string, FeatureUnlockData>();
            }
        }
    }

    public void LoadPlayerFeatureUnlock(UnityAction onComplete, UnityAction<string> onFail)
    {
        Debug.Log("Start LoadPlayerFeatureUnlock ...");

        PlayFabManager.Instance.GetPlayerReadOnlyData(
              _playerFeatureUnlockPlayfabKey
            , delegate (GetUserDataResult result)
            {
                OnLoadPlayerFeatureUnlockComplete(result);

                Debug.Log("<color=green>Completed</color> LoadPlayerFeatureUnlock");
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            }
            , delegate (PlayFabError error)
            {
                Debug.LogFormat("<color=red>Failed</color> LoadPlayerFeatureUnlock : {0}", error.ErrorMessage);
                if (onFail != null)
                {
                    onFail.Invoke(error.Error.ToString());
                }
            }
        );
    }

    private void OnLoadPlayerFeatureUnlockComplete(GetUserDataResult result)
    {
        if (result.Data.ContainsKey(_playerFeatureUnlockPlayfabKey))
        {
            UserDataRecord data = result.Data[_playerFeatureUnlockPlayfabKey];

            try
            {
                _playerFeatureUnlockDict = JsonConvert.DeserializeObject<Dictionary<string, FeatureUnlockData>>(data.Value);
            }
            catch
            {
                _playerFeatureUnlockDict = new Dictionary<string, FeatureUnlockData>();
            }

            if (OnUpdateFeatureUnlock != null)
            {
                OnUpdateFeatureUnlock.Invoke();
            }
        }
    }

    public bool IsFeatureEnable(FeatureUnlock feature)
    {
        return IsFeatureEnable(feature.ToString());
    }

    private bool IsFeatureEnable(string key)
    {
        if (_titleFeatureUnlockDict.ContainsKey(key) && _titleFeatureUnlockDict[key].IsEnable) // IsEnable => disable from server.
        {
            if (_titleFeatureUnlockDict[key].UnlockCount < 0) // IsEnable by ignore player data.
            {
                return true;
            }

            if (_playerFeatureUnlockDict.ContainsKey(key))
            {
                if (_playerFeatureUnlockDict[key].IsEnable)
                {
                    return true;
                }
                else
                {
                    if (_playerFeatureUnlockDict[key].UnlockCount >= _titleFeatureUnlockDict[key].UnlockCount)
                    {
                        _playerFeatureUnlockDict[key].IsEnable = true;
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public bool GetIsEnable(FeatureUnlock feature, out bool isEnable)
    {
        return GetIsEnable(feature.ToString(), out isEnable);
    }

    private bool GetIsEnable(string key, out bool isEnable)
    {
        if (_playerFeatureUnlockDict.ContainsKey(key))
        {
            isEnable = _playerFeatureUnlockDict[key].IsEnable;
            return true;
        }

        isEnable = false;
        return false;
    }

    public bool GetUnlockCount(FeatureUnlock feature, out int unlockCount)
    {
        return GetUnlockCount(feature.ToString(), out unlockCount);
    }

    private bool GetUnlockCount(string key, out int unlockCount)
    {
        if (_playerFeatureUnlockDict.ContainsKey(key))
        {
            unlockCount = _playerFeatureUnlockDict[key].UnlockCount;
            return true;
        }

        unlockCount = 0;
        return false;
    }

    public bool SetIsEnable(FeatureUnlock feature, bool isEnable)
    {
        return SetIsEnable(feature.ToString(), isEnable);
    }

    private bool SetIsEnable(string key, bool isEnable)
    {
        if (_playerFeatureUnlockDict.ContainsKey(key))
        {
            _playerFeatureUnlockDict[key].IsEnable = isEnable;
            return true;
        }

        return false;
    }

    public bool SetUnlockCount(FeatureUnlock feature, int unlockCount)
    {
        return SetUnlockCount(feature.ToString(), unlockCount);
    }

    private bool SetUnlockCount(string key, int unlockCount)
    {
        if (_playerFeatureUnlockDict.ContainsKey(key))
        {
            _playerFeatureUnlockDict[key].UnlockCount = unlockCount;
            return true;
        }

        return false;
    }

    public void AddUnlockCount(FeatureUnlock feature, int unlockCount = 1)
    {
        AddUnlockCount(feature.ToString(), unlockCount);
    }

    private void AddUnlockCount(string key, int unlockCount = 1)
    {
        if (_playerFeatureUnlockDict.ContainsKey(key))
        {
            int newValue = _playerFeatureUnlockDict[key].UnlockCount + unlockCount;
            _playerFeatureUnlockDict[key].UnlockCount = newValue;
        }
        else
        {
            FeatureUnlockData data = new FeatureUnlockData(false, unlockCount);
            _playerFeatureUnlockDict.Add(key, data);
        }
    }

    public void RemoveUnlockCount(FeatureUnlock feature, int unlockCount = 1)
    {
        RemoveUnlockCount(feature.ToString(), unlockCount);
    }

    private void RemoveUnlockCount(string key, int unlockCount = 1)
    {
        if (_playerFeatureUnlockDict.ContainsKey(key))
        {
            int newValue = _playerFeatureUnlockDict[key].UnlockCount - unlockCount;
            if (newValue > 0)
            {
                _playerFeatureUnlockDict[key].UnlockCount = newValue;
            }
            else
            {
                _playerFeatureUnlockDict.Remove(key);
            }
        }
    }

    public void UpdatePlayerFeatureUnlock(UnityAction onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.UpdatePlayerFeatureUnlock(
              _playerFeatureUnlockDict
            , delegate (ExecuteCloudScriptResult result)
            {
                onComplete?.Invoke();
            }
            , delegate (PlayFabError error)
            {
                onFail?.Invoke(error.ErrorMessage);
            }
        );
    }
    #endregion

    #region Mission Methods
    private void OnLoadTitleMissionDBComplete(GetTitleDataResult result)
    {
        if (result.Data.ContainsKey(_titleMissionDBDataPlayfabKey))
        {
            MissionDB.Instance.LoadFromJSON(result.Data[_titleMissionDBDataPlayfabKey]);
            Debug.Log("LoadTitleMissionDBData <color=green>Completed</color>");
        }
        else
        {
            Debug.Log("LoadTitleMissionDBData <color=red>Fail</color>");
        }
    }

    private void OnLoadPlayerMissionDataComplete(GetUserDataResult result)
    {
        if (result.Data.ContainsKey(_playerMissionDataPlayfabKey))
        {
            _playerMissionData.SetData(result.Data[_playerMissionDataPlayfabKey].Value);
        }
        else
        {
            _playerMissionData.SetData(string.Empty);
        }

        Debug.Log("LoadPlayerMissionData <color=green>Completed</color>");
    }

    //public void RefreshMissionData(UnityAction onComplete, UnityAction<string> onFail)
    //{
    //    PlayFabManager.Instance.RefreshMissionData(
    //        delegate (ExecuteCloudScriptResult result)
    //        {
    //            CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
    //            if (resultProcess.IsSuccess)
    //            {
    //                _playerMissionData.SetData(resultProcess.CustomData["mission_data"]);
    //                onComplete?.Invoke();
    //            }
    //            else
    //            {
    //                onFail?.Invoke(resultProcess.MessageDetail);
    //            }
    //        }
    //        , delegate (PlayFabError error)
    //        {
    //            onFail?.Invoke(error.Error.ToString());
    //        }
    //    );
    //}

    public void CheckServerMissionComplete(string missionID, UnityAction<List<ItemData>> onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.CheckMissionComplete(
            missionID
            , delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    _playerMissionData.SetData(resultProcess.CustomData["mission_data"]);
                    _playerFeatureUnlockDict = JsonConvert.DeserializeObject<Dictionary<string, FeatureUnlockData>>(resultProcess.CustomData["feature_unlock"]);
                    Dictionary<string, int> reward = JsonConvert.DeserializeObject<Dictionary<string, int>>(resultProcess.CustomData["reward"]);
                    List<ItemData> rewardResult = ConvertDictionaryToItemDataList(reward);

                    OnMissionComplete?.Invoke(missionID);
                    OnUpdateFeatureUnlock?.Invoke();

                    onComplete?.Invoke(rewardResult);
                }
                else
                {
                    onFail?.Invoke(resultProcess.MessageDetail);
                }
            }
            , delegate (PlayFabError error)
            {
                onFail?.Invoke(error.Error.ToString());
            }
        );
    }
    #endregion

    #region Message Inbox
    public void LoadAllMessageInbox(UnityAction onComplete, UnityAction<string> onFail)
    {
        Debug.Log("Start LoadAllMessageInbox ...");

        TaskLoader<string> taskLoader = new TaskLoader<string>();
        taskLoader.AddTask(RefreshMessageInboxPreset);
        taskLoader.AddTask(LoadTitleMessageInboxAnnouncement);
        taskLoader.JoinTask(LoadPlayerMessageInbox);
        taskLoader.StartTasks(
            delegate ()
            {
                // Setup MessageInbox data list
                SetupAllMessageInboxData();

                Debug.Log("<color=green>Completed</color> LoadAllMessageInbox");
                onComplete?.Invoke();
            }
            , delegate (List<string> failList)
            {
                if (onFail != null)
                {
                    string failText = "";
                    foreach (string item in failList)
                    {
                        failText += " " + item;
                    }

                    Debug.LogFormat("<color=red>Failed</color> LoadAllMessageInbox : {0}", failText);
                    onFail.Invoke(failText);
                }
            }
        );
    }

    public void LoadTitleMessageInboxAnnouncement(UnityAction onComplete, UnityAction<string> onFail)
    {
        Debug.Log("Start LoadTitleMessageInboxAnnouncement ...");

        PlayFabManager.Instance.GetTitleData(
              _titleMessageInboxAnnouncementDataPlayfabKey
            , delegate (GetTitleDataResult result)
            {
                OnLoadTitleMessageInboxAnnouncementComplete(result);
                Debug.Log("<color=green>Completed</color> LoadTitleMessageInboxAnnouncement");
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            }
            , delegate (PlayFabError error)
            {
                Debug.LogFormat("<color=red>Failed</color> LoadTitleMessageInboxAnnouncement : {0}", error.ErrorMessage);
                if (onFail != null)
                {
                    onFail.Invoke(error.Error.ToString());
                }
            }
        );
    }

    private void LoadPlayerMessageInbox(UnityAction onComplete, UnityAction<string> onFail)
    {
        Debug.Log("Start LoadPlayerMessageInbox ...");

        PlayFabManager.Instance.GetPlayerReadOnlyData(new List<string>()
            { _playerMessageInboxDataPlayfabKey
            , _playerMessageInboxStatusPlayfabKey }
            , delegate (GetUserDataResult result)
            {
                OnLoadPlayerMessageInboxComplete(result);

                Debug.Log("<color=green>Completed</color> LoadPlayerMessageInbox");
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            }
            , delegate (PlayFabError error)
            {
                Debug.LogFormat("<color=red>Failed</color> LoadPlayerMessageInbox : {0}", error.ErrorMessage);
                if (onFail != null)
                {
                    onFail.Invoke(error.Error.ToString());
                }
            }
        );
    }

    private void OnLoadTitleMessageInboxAnnouncementComplete(GetTitleDataResult result)
    {
        if (result.Data.ContainsKey(_titleMessageInboxAnnouncementDataPlayfabKey))
        {
            Dictionary<string, object> data = JsonConvert.DeserializeObject<Dictionary<string, object>>(result.Data[_titleMessageInboxAnnouncementDataPlayfabKey]);
            SetTitleMessageInboxAnnouncement(data);

            //Debug.Log("LoadTitleMessageInbox <color=green>Completed</color>");
        }
    }

    private void OnLoadPlayerMessageInboxComplete(GetUserDataResult result)
    {
        if (result.Data.ContainsKey(_playerMessageInboxDataPlayfabKey))
        {
            Dictionary<string, object> data = JsonConvert.DeserializeObject<Dictionary<string, object>>(result.Data[_playerMessageInboxDataPlayfabKey].Value);
            SetPlayerMessageInboxData(data);
        }

        if (result.Data.ContainsKey(_playerMessageInboxStatusPlayfabKey))
        {
            Dictionary<string, List<string>> status = JsonConvert.DeserializeObject<Dictionary<string, List<string>>>(result.Data[_playerMessageInboxStatusPlayfabKey].Value);
            SetPlayerMessageInboxStatus(status);
        }

        //Debug.Log("LoadPlayerMessageInbox <color=green>Completed</color>");
    }

    private void SetTitleMessageInboxAnnouncement(Dictionary<string, object> data)
    {
        if (data != null)
        {
            _titleMessageInboxAnnouncementData = new Dictionary<string, object>(data);
        }
    }

    private void SetPlayerMessageInboxData(Dictionary<string, object> messageData)
    {
        if (messageData != null)
        {
            _playerMessageInboxData = new Dictionary<string, object>(messageData);
        }
    }

    private void SetPlayerMessageInboxStatus(Dictionary<string, List<string>> messageStatus)
    {
        if (messageStatus != null)
        {
            _playerMessageInboxStatus = new Dictionary<string, List<string>>(messageStatus);
        }
    }

    private List<string> GetReadMessageInboxID()
    {
        List<string> result = new List<string>();
        if (_playerMessageInboxStatus.ContainsKey("read"))
        {
            result = new List<string>(_playerMessageInboxStatus["read"]);
        }
        return result;
    }

    private List<string> GetDeletedMessageInboxID()
    {
        List<string> result = new List<string>();
        if (_playerMessageInboxStatus.ContainsKey("deleted"))
        {
            result = new List<string>(_playerMessageInboxStatus["deleted"]);
        }
        return result;
    }

    private List<string> GetClaimedMessageInboxID()
    {
        List<string> result = new List<string>();
        if (_playerMessageInboxStatus.ContainsKey("claimed"))
        {
            result = new List<string>(_playerMessageInboxStatus["claimed"]);
        }
        return result;
    }

    private void SetupAllMessageInboxData()
    {
        List<string> readMessageInbox = GetReadMessageInboxID();
        List<string> deletedMessageInbox = GetDeletedMessageInboxID();
        List<string> claimedMessageInbox = GetClaimedMessageInboxID();

        _messageInboxDataList = new List<MessageInboxData>();

        foreach (KeyValuePair<string, object> item in _titleMessageInboxAnnouncementData)
        {
            _messageInboxDataList.Add(new MessageInboxData(item.Key, JsonConvert.DeserializeObject<Dictionary<string, object>>(item.Value.ToString())));
        }

        foreach (KeyValuePair<string, object> item in _playerMessageInboxData)
        {
            _messageInboxDataList.Add(new MessageInboxData(item.Key, JsonConvert.DeserializeObject<Dictionary<string, object>>(item.Value.ToString())));
        }

        foreach (MessageInboxData data in _messageInboxDataList)
        {
            if (deletedMessageInbox.Contains(data.ID))
            {
                data.SetDelete();
            }

            if (claimedMessageInbox.Contains(data.ID))
            {
                data.SetClaim();
            }

            if (readMessageInbox.Contains(data.ID))
            {
                data.SetRead();
            }
        }
    }

    public List<MessageInboxData> GetAllMessageInboxData()
    {
        List<MessageInboxData> messageInboxSortedDataList = _messageInboxDataList.OrderBy(message => message.ReceivedDateTimeUTC).ToList();
        return messageInboxSortedDataList;
    }

    public void ReadMessageInbox(string messageID, UnityAction onComplete, UnityAction<string> onFail)
    {
        int index = _messageInboxDataList.FindIndex(x => x.ID == messageID);
        if (index >= 0)
        {
            _messageInboxDataList[index].SetRead();
        }

        PlayFabManager.Instance.ReadMessageInbox(messageID
            , delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                }
                else
                {
                    if (onFail != null)
                    {
                        onFail.Invoke(resultProcess.MessageDetail);
                    }
                }
            }
            , delegate (PlayFabError error)
            {
                if (onFail != null)
                {
                    onFail.Invoke(error.Error.ToString());
                }
            }
        );
    }

    public void DeleteMessageInbox(string messageID, UnityAction onComplete, UnityAction<string> onFail)
    {
        int index = _messageInboxDataList.FindIndex(x => x.ID == messageID);
        if (index >= 0)
        {
            _messageInboxDataList[index].SetDelete();
        }

        PlayFabManager.Instance.DeleteMessageInbox(messageID
            , delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                }
                else
                {
                    if (onFail != null)
                    {
                        onFail.Invoke(resultProcess.MessageDetail);
                    }
                }
            }
            , delegate (PlayFabError error)
            {
                if (onFail != null)
                {
                    onFail.Invoke(error.Error.ToString());
                }
            }
        );
    }

    public void DeleteAllMessageInbox(UnityAction onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.DeleteAllMessageInbox(
            delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    List<string> deletedMessageIDList = JsonConvert.DeserializeObject<List<string>>(resultProcess.CustomData["result"]);
                    for (int index = 0; index < deletedMessageIDList.Count; ++index)
                    {
                        int msgIndex = _messageInboxDataList.FindIndex(x => x.ID == deletedMessageIDList[index]);
                        if (msgIndex >= 0 && msgIndex < _messageInboxDataList.Count)
                        {
                            _messageInboxDataList[msgIndex].SetDelete();
                        }
                    }

                    onComplete?.Invoke();
                }
                else
                {
                    if (onFail != null)
                    {
                        onFail.Invoke(resultProcess.MessageDetail);
                    }
                }
            }
            , delegate (PlayFabError error)
            {
                if (onFail != null)
                {
                    onFail.Invoke(error.Error.ToString());
                }
            }
        );
    }

    public void ClaimMessageInboxItem(string messageID, UnityAction onComplete, UnityAction<string> onFail)
    {
        int index = _messageInboxDataList.FindIndex(x => x.ID == messageID);
        if (index >= 0)
        {
            _messageInboxDataList[index].SetClaim();
        }

        PlayFabManager.Instance.ClaimMessageInboxItem(messageID
            , delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    LoadInventory(null, null);
                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                }
                else
                {
                    if (onFail != null)
                    {
                        onFail.Invoke(resultProcess.MessageDetail);
                    }
                }
            }
            , delegate (PlayFabError error)
            {
                if (onFail != null)
                {
                    onFail.Invoke(error.Error.ToString());
                }
            }
        );
    }

    public void ClaimAllMessageInboxItem(UnityAction<List<ItemData>> onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.ClaimAllMessageInboxItem(
            delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    foreach (MessageInboxData data in _messageInboxDataList)
                    {
                        data.SetClaim();
                        data.SetRead();
                    }

                    List<ItemData> itemList = new List<ItemData>();
                    Dictionary<string, int> itemResult = JsonConvert.DeserializeObject<Dictionary<string, int>>(resultProcess.CustomData["result"]);
                    foreach (KeyValuePair<string, int> item in itemResult)
                    {
                        itemList.Add(new ItemData(item));
                    }

                    LoadInventory(null, null);
                    if (onComplete != null)
                    {
                        onComplete.Invoke(itemList);
                    }
                }
                else
                {
                    if (onFail != null)
                    {
                        onFail.Invoke(resultProcess.MessageDetail);
                    }
                }
            }
            , delegate (PlayFabError error)
            {
                if (onFail != null)
                {
                    onFail.Invoke(error.Error.ToString());
                }
            }
        );
    }
    #endregion

    #region Store & Catalog Methods
    public void LoadCatalogItems(UnityAction onComplete, UnityAction<string> onFail)
    {
        Debug.Log("Start LoadCatalogItems ...");

        PlayFabManager.Instance.GetCatalogItems(
              PlayFabManager.CatalogMainID
            , delegate (GetCatalogItemsResult result)
            {
                OnLoadCatalogItemsComplete(result);

                Debug.Log("<color=green>Completed</color> LoadCatalogItems");
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            }
            , delegate (PlayFabError error)
            {
                Debug.LogFormat("<color=red>Failed</color> LoadCatalogItems : {0}", error.ErrorMessage);

                Debug.Log("Retry LoadCatalogItems ...");
                LoadCatalogItems(onComplete, onFail);
            }
        );
    }

    private void OnLoadCatalogItemsComplete(GetCatalogItemsResult result)
    {
        _catalogItemDataList.Clear();
        List<string> itemIDList = new List<string>();
        foreach (CatalogItem item in result.Catalog)
        {
            CatalogItemData catalogItemData = CatalogItemData.CreateCatalogItemData(item);
            _catalogItemDataList.Add(catalogItemData.ItemCatalogID, catalogItemData);

            if (catalogItemData.ItemType == CatalogItemType.Card)
            {
                itemIDList.Add(catalogItemData.ItemCatalogID);
            }
        }

        // Set CardDataList for caching
        CardData.SetCardDataList(itemIDList);

        Debug.Log("LoadCatalog <color=green>Completed</color>");
    }

    public bool IsItemCatalogTagListContain(string itemID, params string[] tags)
    {
        if (!_catalogItemDataList.ContainsKey(itemID))
        {
            return false;
        }

        foreach (string tag in tags)
        {
            if (!_catalogItemDataList[itemID].ItemTags.Contains(tag))
            {
                return false;
            }
        }

        return true;
    }

    public List<CatalogItemData> GetCatalog()
    {
        List<CatalogItemData> catalog = new List<CatalogItemData>();

        if (_catalogItemDataList != null)
        {
            foreach (KeyValuePair<string, CatalogItemData> item in _catalogItemDataList)
            {
                catalog.Add(item.Value);
            }
        }
        else
        {
            Debug.LogError("DataManager/GetCardCatalog: Not found inventory.");
        }

        return catalog;
    }

    public List<CatalogItemData> GetCatalog(CatalogItemType itemType)
    {
        List<CatalogItemData> catalog = new List<CatalogItemData>();

        if (_catalogItemDataList != null)
        {
            foreach (KeyValuePair<string, CatalogItemData> item in _catalogItemDataList)
            {
                if (item.Value.ItemType == itemType)
                {
                    catalog.Add(item.Value);
                }
            }
        }
        else
        {
            Debug.LogError("DataManager/GetCardCatalog: Not found inventory.");
        }

        return catalog;
    }

    public List<CatalogCardData> GetCardCatalog()
    {
        List<CatalogCardData> cardCatalog = new List<CatalogCardData>();

        if (_catalogItemDataList != null)
        {
            foreach (KeyValuePair<string, CatalogItemData> item in _catalogItemDataList)
            {
                if (item.Value.ItemType == CatalogItemType.Card)
                {
                    cardCatalog.Add((item.Value as CatalogCardData));
                }
            }
        }
        else
        {
            Debug.LogError("DataManager/GetCardCatalog: Not found inventory.");
        }

        return cardCatalog;
    }

    public CardListData GetCardListCatalog()
    {
        CardListData catalogDataStack = new CardListData();
        List<CatalogCardData> catalogCardDataList = GetCardCatalog();
        CardData cardData = null;

        foreach (CatalogCardData catalogCardData in catalogCardDataList)
        {
            if (CardData.GetCardData(catalogCardData.ItemCatalogID, out cardData))
            {
                if (cardData.Type == CardType.Minion) // only minion. 
                {
                    catalogDataStack.AddCard(catalogCardData.ItemCatalogID, 0);
                }
            }
        }

        return catalogDataStack;
    }

    public bool GetCatalogItem(string itemID, out CatalogItemData item)
    {
        if (_catalogItemDataList != null)
        {
            if (_catalogItemDataList.ContainsKey(itemID))
            {
                item = new CatalogItemData(_catalogItemDataList[itemID]);
                return true;
            }
        }

        Debug.Log("DataManager/GetCatalogItem: Not found item. " + itemID);

        item = null;
        return false;
    }

    public void RefreshWeeklyDeal(UnityAction onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.RefreshWeeklyDeal(
            delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    string itemKey = resultProcess.CustomData["itemKey"];
                    string timestampStr = resultProcess.CustomData["timestampStr"];
                    string timeExpireStr = resultProcess.CustomData["timeExpireStr"];
                    bool isRedeem = bool.Parse(resultProcess.CustomData["isRedeem"]);

                    _weeklyDealRecord = new DealRecord(itemKey, timestampStr, timeExpireStr, isRedeem);

                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                }
                else
                {
                    if (onFail != null)
                    {
                        onFail.Invoke(resultProcess.MessageDetail);
                    }
                }
            }
            , delegate (PlayFabError error)
            {
                Debug.Log("<color=red>RefreshWeeklyDeal</color> " + error.ToString());
                RefreshWeeklyDeal(onComplete, onFail);
            }
        );
    }

    public void RevokeWeeklyDeal(UnityAction onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.RevokeWeeklyDeal(
            delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    string itemKey = resultProcess.CustomData["itemKey"];
                    string timestampStr = resultProcess.CustomData["timestampStr"];
                    string timeExpireStr = resultProcess.CustomData["timeExpireStr"];
                    bool isRedeem = bool.Parse(resultProcess.CustomData["isRedeem"]);

                    _weeklyDealRecord = new DealRecord(itemKey, timestampStr, timeExpireStr, isRedeem);

                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                }
                else
                {
                    if (onFail != null)
                    {
                        onFail.Invoke(resultProcess.MessageDetail);
                    }
                }
            }
            , delegate (PlayFabError error)
            {
                if (onFail != null)
                {
                    onFail.Invoke(error.Error.ToString());
                }
            }
        );
    }

    public void RefreshDailyDeal(UnityAction onComplete, UnityAction<string> onFail)
    {
        Debug.Log("Start RefreshDailyDeal ...");

        PlayFabManager.Instance.RefreshDailyDeal(
            delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    string itemKey = resultProcess.CustomData["itemKey"];
                    string timestampStr = resultProcess.CustomData["timestampStr"];
                    string timeExpireStr = resultProcess.CustomData["timeExpireStr"];
                    bool isRedeem = bool.Parse(resultProcess.CustomData["isRedeem"]);

                    _dailyDealRecord = new DealRecord(itemKey, timestampStr, timeExpireStr, isRedeem);

                    Debug.Log("<color=green>Completed</color> RefreshDailyDeal");
                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                }
                else
                {
                    Debug.LogFormat("<color=red>Failed</color> RefreshDailyDeal : {0}", resultProcess.MessageDetail);
                    if (onFail != null)
                    {
                        onFail.Invoke(resultProcess.MessageDetail);
                    }
                }
            }
            , delegate (PlayFabError error)
            {
                Debug.LogFormat("<color=red>Failed</color> RefreshDailyDeal : {0}", error.ErrorMessage);

                Debug.Log("Retry RefreshDailyDeal ...");
                RefreshFreeDeal(onComplete, onFail);
            }
        );
    }

    public void RevokeDailyDeal(UnityAction onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.RevokeDailyDeal(
            delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    string itemKey = resultProcess.CustomData["itemKey"];
                    string timestampStr = resultProcess.CustomData["timestampStr"];
                    string timeExpireStr = resultProcess.CustomData["timeExpireStr"];
                    bool isRedeem = bool.Parse(resultProcess.CustomData["isRedeem"]);

                    _dailyDealRecord = new DealRecord(itemKey, timestampStr, timeExpireStr, isRedeem);

                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                }
                else
                {
                    if (onFail != null)
                    {
                        onFail.Invoke(resultProcess.MessageDetail);
                    }
                }
            }
            , delegate (PlayFabError error)
            {
                if (onFail != null)
                {
                    onFail.Invoke(error.Error.ToString());
                }
            }
        );
    }

    public void RefreshFreeDeal(UnityAction onComplete, UnityAction<string> onFail)
    {
        Debug.Log("Start RefreshFreeDeal ...");

        PlayFabManager.Instance.RefreshFreeDeal(
            delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    string itemKey = resultProcess.CustomData["itemKey"];
                    string timestampStr = resultProcess.CustomData["timestampStr"];
                    string timeExpireStr = resultProcess.CustomData["timeExpireStr"];
                    bool isRedeem = bool.Parse(resultProcess.CustomData["isRedeem"]);

                    _freeDealRecord = new DealRecord(itemKey, timestampStr, timeExpireStr, isRedeem);

                    Debug.Log("<color=green>Completed</color> RefreshFreeDeal");
                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                }
                else
                {
                    Debug.LogFormat("<color=red>Failed</color> RefreshFreeDeal : {0}", resultProcess.MessageDetail);
                    if (onFail != null)
                    {
                        onFail.Invoke(resultProcess.MessageDetail);
                    }
                }
            }
            , delegate (PlayFabError error)
            {
                Debug.LogFormat("<color=red>Failed</color> RefreshFreeDeal : {0}", error.ErrorMessage);

                Debug.Log("Retry RefreshFreeDeal ...");
                RefreshFreeDeal(onComplete, onFail);
            }
        );
    }

    public void ClaimFreeDeal(UnityAction onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.ClaimFreeDeal(
            delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    string itemKey = resultProcess.CustomData["itemKey"];
                    string timestampStr = resultProcess.CustomData["timestampStr"];
                    string timeExpireStr = resultProcess.CustomData["timeExpireStr"];
                    bool isRedeem = bool.Parse(resultProcess.CustomData["isRedeem"]);

                    Dictionary<string, int> itemIDList = null;
                    Dictionary<string, ItemData> itemList = new Dictionary<string, ItemData>();
                    if (resultProcess.CustomData.ContainsKey("itemList"))
                    {
                        itemIDList = JsonConvert.DeserializeObject<Dictionary<string, int>>(resultProcess.CustomData["itemList"]);
                        if (itemIDList != null && itemIDList.Count > 0)
                        {
                            foreach (KeyValuePair<string, int> id in itemIDList)
                            {
                                itemList.Add(id.Key, new ItemData(id));
                            }
                        }
                    }

                    _freeDealRecord = new DealRecord(itemKey, timestampStr, timeExpireStr, isRedeem);
                    _freeDealRecord.SetItemList(itemList);

                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                }
                else
                {
                    if (onFail != null)
                    {
                        onFail.Invoke(resultProcess.MessageDetail);
                    }
                }
            }
            , delegate (PlayFabError error)
            {
                if (onFail != null)
                {
                    onFail.Invoke(error.Error.ToString());
                }
            }
        );
    }

    public DealRecord GetWeeklyDealRecord()
    {
        return _weeklyDealRecord;
    }

    public DealRecord GetDailyDealRecord()
    {
        return _dailyDealRecord;
    }

    public DealRecord GetFreeDealRecord()
    {
        return _freeDealRecord;
    }

    #region Shop
    public void LoadStoreData(UnityAction onComplete, UnityAction<string> onFail)
    {
        Debug.Log("Start LoadStoreData ...");

        TaskLoader<string> taskList = new TaskLoader<string>();
        _storeIDTypeLoader = new Dictionary<ShopTypes, List<string>>();
        foreach (KeyValuePair<ShopTypes, ShopTypeData> shop in _shopTypeDataList)
        {
            foreach (string storeID in shop.Value.StoreID)
            {
                if (!_storeIDTypeLoader.ContainsKey(shop.Key))
                {
                    _storeIDTypeLoader.Add(shop.Key, new List<string>());
                }
                _storeIDTypeLoader[shop.Key].Add(storeID);

                taskList.JoinTask(StartLoadStoreData);
            }
        }

        taskList.StartTasks(
            delegate ()
            {
                Debug.Log("<color=green>Completed</color> LoadStoreData");
                onComplete?.Invoke();
            }
            , delegate (List<string> failList)
            {
                if (onFail != null)
                {
                    string failText = "";
                    foreach (string item in failList)
                    {
                        failText += "|" + item;
                    }

                    Debug.LogFormat("<color=red>Failed</color> LoadStoreData : {0}", failText);
                    onFail.Invoke(failText);
                }
            }
        );
    }

    private void StartLoadStoreData(UnityAction onComplete, UnityAction<string> onFail)
    {
        string storeID = string.Empty;
        ShopTypes type = ShopTypes.None;
        foreach (KeyValuePair<ShopTypes, List<string>> data in _storeIDTypeLoader)
        {
            if (data.Value.Count > 0)
            {
                storeID = data.Value[0];
                type = data.Key;

                data.Value.RemoveAt(0);
                break;
            }
        }

        if (type == ShopTypes.None)
        {
            if (onComplete != null)
            {
                onComplete.Invoke();
            }
            return;
        }

        PlayFabManager.Instance.GetStoreItems(
            storeID
            , delegate (GetStoreItemsResult result)
            {
                ShopData shopData = null;
                if (type == ShopTypes.BoosterPack)
                {
                    shopData = new BoosterPackShopData(type, storeID);
                }
                else
                {
                    shopData = new NormalShopData(type, storeID);
                }

                foreach (StoreItem item in result.Store)
                {
                    foreach (KeyValuePair<string, uint> Price in item.VirtualCurrencyPrices)
                    {
                        VirtualCurrency currency;
                        if (GameHelper.StrToEnum<VirtualCurrency>(Price.Key, out currency))
                        {
                            ShopItemData itemData = new ShopItemData(storeID, type, item, currency, Convert.ToInt32(Price.Value));
                            shopData.AddItem(itemData);
                        }
                    }
                }
                if (result.MarketingData.Metadata != null)
                {
                    shopData.SetCustomData(result.MarketingData.Metadata.ToString());
                }
                _shopTypeDataList[type].SetShopData(shopData);

                Debug.Log(type + " is load completed = " + _shopTypeDataList[type].IsLoadCompleted);

                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            }
            , delegate (PlayFabError error)
            {
                if (onFail != null)
                {
                    onFail.Invoke(error.Error.ToString());
                }
            }
        );
    }

    private void OnLoadShopTypeDataComplete(GetTitleDataResult result)
    {
        if (result.Data.ContainsKey(_titleStoreDataPlayfabKey))
        {
            string json = result.Data[_titleStoreDataPlayfabKey];
            Dictionary<string, string[]> storeKeys = JsonConvert.DeserializeObject<Dictionary<string, string[]>>(json);
            SetShopTypeData(storeKeys);
        }

        Debug.Log("LoadStoreDataComplete <color=green>Completed</color>");
    }

    private void SetShopTypeData(Dictionary<string, string[]> keyData)
    {
        _shopTypeDataList = new Dictionary<ShopTypes, ShopTypeData>();
        foreach (KeyValuePair<string, string[]> storeKeyData in keyData)
        {
            ShopTypes type;
            if (GameHelper.StrToEnum(storeKeyData.Key, out type))
            {
                ShopTypeData data = new ShopTypeData(type, storeKeyData.Value);
                _shopTypeDataList.Add(type, data);
            }
        }
    }

    public Dictionary<ShopTypes, ShopTypeData> GetShopTypeDataList()
    {
        return _shopTypeDataList;
    }

    public ShopTypeData GetShopTypeData(ShopTypes type)
    {
        if (_shopTypeDataList.ContainsKey(type))
        {
            return _shopTypeDataList[type];
        }

        return null;
    }
    #endregion

    #endregion

    #region Craft & Recycle Card Methods
    private void OnLoadTitleCardPriceOverrideDBComplete(GetTitleDataResult result)
    {
        if (result.Data.ContainsKey(_titleCardPriceOverridePlayfabKey))
        {
            CardPriceOverrideDB.Instance.LoadFromJSON(result.Data[_titleCardPriceOverridePlayfabKey]);
        }

        Debug.Log("LoadTitleCardPriceOverrideData <color=green>Completed</color>");
    }

    private void LoadPlayerCardPriceOverrideData(UnityAction onComplete, UnityAction<string> onFail)
    {
        Debug.Log("Start LoadPlayerCardPriceOverrideData ...");

        PlayFabManager.Instance.GetPlayerReadOnlyData(_playerCardPriceOverridePlayfabKey
            , delegate (GetUserDataResult result)
            {
                OnLoadPlayerCardPriceOverrideDataComplete(result);

                Debug.Log("<color=green>Completed</color> LoadPlayerCardPriceOverrideData");
                onComplete?.Invoke();
            }
            , delegate (PlayFabError error)
            {
                Debug.LogFormat("<color=red>Failed</color> LoadPlayerCardPriceOverrideData : {0}", error.ErrorMessage);
                onFail?.Invoke(error.Error.ToString());
            }
        );
    }

    private void OnLoadPlayerCardPriceOverrideDataComplete(GetUserDataResult result)
    {
        if (result.Data.ContainsKey(_playerCardPriceOverridePlayfabKey))
        {
            _playerCardPriceOverrideData = new PlayerCardPriceOverrideData(result.Data[_playerCardPriceOverridePlayfabKey].Value);
        }

        Debug.Log("LoadPlayerCardPriceOverrideData <color=green>Completed</color>");
    }

    public void RefreshCardPriceOverrideQuota(UnityAction onComplete, UnityAction<string> onFail)
    {
        Debug.Log("Start RefreshCardPriceOverrideQuota ...");

        PlayFabManager.Instance.RefreshCardPriceOverrideQuota(
            delegate (ExecuteCloudScriptResult result)
            {
                Debug.Log("<color=green>Completed</color> RefreshCardPriceOverrideQuota");
                onComplete?.Invoke();
            }
            , delegate (PlayFabError error)
            {
                Debug.LogFormat("<color=red>Failed</color> RefreshCardPriceOverrideQuota : {0}", error.ErrorMessage);
                RefreshCardPriceOverrideQuota(onComplete, onFail);
            }
        );
    }

    public void RecycleCard(string itemFullID, UnityAction onRecycleComplete, UnityAction<string> onRecycleFail)
    {
        if (!DataManager.Instance.IsFeatureEnable(FeatureUnlock.CardCrafting))
        {
            onRecycleFail?.Invoke("ERROR_RECYCLE_NOT_COMPLETE");
            return;
        }

        if (_isCardRecycleCompleted == false)
        {
            onRecycleFail?.Invoke("ERROR_RECYCLE_NOT_COMPLETE");
            return;
        }

        CatalogCardData catalogItemData = _catalogItemDataList[itemFullID] as CatalogCardData;
        _isCardRecycleCompleted = false;
        PlayFabManager.Instance.RecycleCard(itemFullID
            , (result) =>
            {
                _isCardRecycleCompleted = true;
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    List<ItemData> itemDataList = new List<ItemData>();
                    itemDataList.Add(new ItemData(itemFullID, -1));
                    foreach (KeyValuePair<VirtualCurrency, int> price in catalogItemData.RecyclePrice)
                    {
                        itemDataList.Add(new ItemData(string.Format("VC_{0}", price.Key.ToString()), price.Value));
                    }

                    //update Inventory
                    _playerInventoryData.GrantItemByItemList(itemDataList);

                    //update quota & card price
                    _playerCardPriceOverrideData = new PlayerCardPriceOverrideData(resultProcess.CustomData["player_price_override"]);
                    catalogItemData.UpdateCardPriceData();

                    if (onRecycleComplete != null)
                    {
                        onRecycleComplete.Invoke();
                    }
                }
                else
                {
                    catalogItemData.UpdateCardPriceData();

                    if (onRecycleFail != null)
                    {
                        onRecycleFail.Invoke(resultProcess.MessageDetail);
                    }
                }
            }
            , (error) =>
            {
                _isCardRecycleCompleted = true;
                catalogItemData.UpdateCardPriceData();

                if (onRecycleFail != null)
                {
                    onRecycleFail.Invoke(error.Error.ToString());
                }
            }
        );
    }

    public void RecycleCardList(List<ItemData> itemList, UnityAction onComplete, UnityAction<string> onFail, UnityAction<Dictionary<string, int>> onQueueFinish = null)
    {
        if (!DataManager.Instance.IsFeatureEnable(FeatureUnlock.CardCrafting))
        {
            onFail?.Invoke("ERROR_RECYCLE_ALL_NOT_COMPLETE");
            return;
        }

        if (itemList.Count == 0)
        {
            onComplete?.Invoke();
            return;
        }

        if (_isCardRecycleAllCompleted == false)
        {
            onFail?.Invoke("ERROR_RECYCLE_ALL_NOT_COMPLETE");
            return;
        }

        _isCardRecycleAllCompleted = false;
        int excuteLimit = 12;
        ExcecuteCloudScriptQueueManager queue = ExcecuteCloudScriptQueueManager.Instance;
        while (itemList.Count > 0)
        {
            Dictionary<string, int> itemDataDict = ConvertItemDataListToDictionary(itemList.CutFirst(excuteLimit));
            queue.AddQueue(
                "RecycleCardList"
                , new { ItemDataList = itemDataDict }
                , (result) =>
                {
                    onQueueFinish?.Invoke(itemDataDict);
                }
            );
        }
        queue.ASynchronousExcecute(
            1.6f
            , () =>
            {
                _isCardRecycleAllCompleted = true;
                LoadPlayerCardPriceOverrideData(
                    delegate ()
                    {
                        LoadCatalogInventory(onComplete, onFail);
                    }
                    , null
                );
            }
            , (failList) =>
            {
                _isCardRecycleAllCompleted = true;
                onFail?.Invoke(ListErrorToString(failList));
            }
        );
    }

    private static string ListErrorToString(List<string> errorList)
    {
        string failText = "";
        foreach (string item in errorList)
        {
            failText += "|" + item;
        }
        return failText;
    }

    public static Dictionary<string, int> ConvertItemDataListToDictionary(List<ItemData> itemList)
    {
        Dictionary<string, int> itemDataDict = new Dictionary<string, int>();
        foreach (ItemData item in itemList)
        {
            if (!itemDataDict.ContainsKey(item.ItemID))
            {
                itemDataDict.Add(item.ItemID, 0);
            }

            itemDataDict[item.ItemID] += item.Amount;
        }

        return itemDataDict;
    }

    public static List<ItemData> ConvertDictionaryToItemDataList(Dictionary<string, int> dict)
    {
        List<ItemData> result = new List<ItemData>();
        foreach (KeyValuePair<string, int> item in dict)
        {
            result.Add(new ItemData(item.Key, item.Value));
        }

        return result;
    }

    public void CraftCard(string itemFullID, UnityAction onCraftComplete, UnityAction<string> onCraftFail)
    {
        if (!DataManager.Instance.IsFeatureEnable(FeatureUnlock.CardCrafting))
        {
            onCraftFail?.Invoke("ERROR_CRAFT_NOT_COMPLETE");
            return;
        }

        if (_isCardCraftCompleted == false)
        {
            onCraftFail?.Invoke("ERROR_CRAFT_NOT_COMPLETE");
            return;
        }

        _isCardCraftCompleted = false;
        CatalogCardData catalogItemData = _catalogItemDataList[itemFullID] as CatalogCardData;
        PlayFabManager.Instance.CraftCard(itemFullID
            , (result) =>
            {
                _isCardCraftCompleted = true;
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    List<ItemData> itemDataList = new List<ItemData>();
                    itemDataList.Add(new ItemData(itemFullID, 1));
                    foreach (KeyValuePair<VirtualCurrency, int> price in catalogItemData.CraftPrice)
                    {
                        itemDataList.Add(new ItemData(string.Format("VC_{0}", price.Key.ToString()), -price.Value));
                    }

                    //update Inventory
                    _playerInventoryData.GrantItemByItemList(itemDataList);

                    //update quota & card price
                    _playerCardPriceOverrideData = new PlayerCardPriceOverrideData(resultProcess.CustomData["player_price_override"]);
                    catalogItemData.UpdateCardPriceData();

                    if (onCraftComplete != null)
                    {
                        onCraftComplete.Invoke();
                    }
                }
                else
                {
                    catalogItemData.UpdateCardPriceData();

                    if (onCraftFail != null)
                    {
                        Debug.Log(resultProcess.MessageDetail);
                        onCraftFail.Invoke(resultProcess.MessageDetail);
                    }
                }
            }
            , (error) =>
            {
                _isCardCraftCompleted = true;
                catalogItemData.UpdateCardPriceData();

                if (onCraftFail != null)
                {
                    onCraftFail.Invoke(error.Error.ToString());
                }
            }
        );
    }

    public void CraftCardList(List<ItemData> itemList, UnityAction onComplete, UnityAction<string> onFail, UnityAction<Dictionary<string, int>> onQueueFinish = null)
    {
        string failMessage = LocalizationManager.Instance.GetText("ERROR_CRAFT_ALL_NOT_COMPLETE");
        if (!DataManager.Instance.IsFeatureEnable(FeatureUnlock.CardCrafting))
        {

            onFail?.Invoke(failMessage);
            return;
        }

        if (itemList.Count == 0)
        {
            onComplete?.Invoke();
            return;
        }

        if (_isCardCraftAllCompleted == false)
        {
            onFail?.Invoke(failMessage);
            return;
        }

        _isCardCraftAllCompleted = false;
        int excuteLimit = 6;
        ExcecuteCloudScriptQueueManager queue = ExcecuteCloudScriptQueueManager.Instance;
        while (itemList.Count > 0)
        {
            Dictionary<string, int> itemDataDict = ConvertItemDataListToDictionary(itemList.CutFirst(excuteLimit));
            queue.AddQueue(
                "CraftCardList"
                , new { ItemDataList = itemDataDict }
                , (result) =>
                {
                    onQueueFinish?.Invoke(itemDataDict);
                }
            );
        }
        queue.ASynchronousExcecute(
            1.6f
            , () =>
            {
                _isCardCraftAllCompleted = true;
                LoadPlayerCardPriceOverrideData(
                    delegate ()
                    {
                        LoadCatalogInventory(onComplete, onFail);
                    }
                    , null
                );
            }
            , (failList) =>
            {
                _isCardCraftAllCompleted = true;
                onFail?.Invoke(ListErrorToString(failList));
            }
        );
    }

    public void TransformCard(string itemFullID, UnityAction onTranformComplete, UnityAction<string> onTranformFail)
    {
        if (!DataManager.Instance.IsFeatureEnable(FeatureUnlock.CardCrafting))
        {
            onTranformFail?.Invoke("ERROR_TRANSFORM_NOT_COMPLETE");
            return;
        }

        if (_isCardTransformCompleted == false)
        {
            onTranformFail?.Invoke("ERROR_TRANSFORM_NOT_COMPLETE");
            return;
        }

        _isCardTransformCompleted = false;
        CatalogCardData catalogItemData = _catalogItemDataList[itemFullID] as CatalogCardData;

        PlayFabManager.Instance.TransformCard(itemFullID
            , (result) =>
            {
                _isCardTransformCompleted = true;
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    List<ItemData> itemDataList = new List<ItemData>();
                    itemDataList.Add(new ItemData(itemFullID, -1));
                    itemDataList.Add(new ItemData(catalogItemData.TransformItemID, 1));
                    foreach (KeyValuePair<VirtualCurrency, int> price in catalogItemData.TransformPrice)
                    {
                        itemDataList.Add(new ItemData(string.Format("VC_{0}", price.Key.ToString()), -price.Value));
                    }

                    //update Inventory
                    _playerInventoryData.GrantItemByItemList(itemDataList);

                    //update quota & card price
                    _playerCardPriceOverrideData = new PlayerCardPriceOverrideData(resultProcess.CustomData["player_price_override"]);
                    catalogItemData.UpdateCardPriceData();

                    if (onTranformComplete != null)
                    {
                        onTranformComplete.Invoke();
                    }
                }
                else
                {
                    catalogItemData.UpdateCardPriceData();

                    if (onTranformFail != null)
                    {
                        onTranformFail.Invoke(resultProcess.MessageDetail);
                    }
                }
            }
            , (error) =>
            {
                _isCardTransformCompleted = true;
                catalogItemData.UpdateCardPriceData();

                if (onTranformFail != null)
                {
                    onTranformFail.Invoke(error.Error.ToString());
                }
            }
        );
    }
    #endregion

    #region Star Exchange

    public void LoadStarExchangeData(UnityAction onComplete, UnityAction<string> onFail)
    {
        Debug.Log("Start LoadStarExchangeData ...");

        PlayFabManager.Instance.ExecuteCloudScript(
            "GetStarExchangeInfo",
            null,
            delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                string json = resultProcess.CustomData["result"];
                _starExchangeData = new StarExchangeData(json);

                Debug.Log("<color=green>Completed</color> LoadStarExchangeData");
                onComplete?.Invoke();
            }
            , null
        );
    }

    public void RefreshRotaionPool(UnityAction onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.ExecuteCloudScript(
               "RefreshRotaionPool",
               null,
               delegate (ExecuteCloudScriptResult result)
               {
                   CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                   if (resultProcess.IsSuccess)
                   {
                       string json = resultProcess.CustomData["result"];
                       _starExchangeData = new StarExchangeData(json);
                       onComplete?.Invoke();
                   }
                   else
                   {
                       onFail?.Invoke(resultProcess.MessageDetail);
                   }
               }, null);
    }

    public void PurchaseStarExchange(string itemID, string currency, UnityAction onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.ExecuteCloudScript(
                  "PurchaseStarExchange",
                  new
                  {
                      ItemID = itemID,
                      Currency = currency
                  },
                  delegate (ExecuteCloudScriptResult result)
                  {
                      CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                      if (resultProcess.IsSuccess)
                      {
                          onComplete?.Invoke();
                      }
                      else
                      {
                          onFail?.Invoke(resultProcess.MessageDetail);
                      }
                  }, null);
    }

    public StarExchangeData GetStarExchangeData()
    {
        return _starExchangeData;
    }

    #endregion

    #region IAP

    public void InitailizeIAP(UnityAction onComplete, UnityAction<string> onFail)
    {
        Debug.Log("Start InitailizeIAP ...");

        IAPManager.Instance.InitializePurchasing(delegate (bool isSuccess, string error)
        {
            if (isSuccess)
            {
                Debug.Log("<color=green>Completed</color> InitailizeIAP");
                onComplete?.Invoke();
            }
            else
            {
                Debug.LogFormat("<color=red>Failed</color> InitailizeIAP : {0}", error);

                PopupUIManager.Instance.ShowPopup_Error(
                      "IAP ERROR"
                    , error
                    , delegate ()
                    {
                        onFail?.Invoke(error);
                    }
                );
            }
        });

    }

    #endregion

    #region Gain System Item

    public DailyLoginData GetDailyLoginData()
    {
        return _dailyLoginData;
    }

    public SpecialLoginData GetSpecialLoginData()
    {
        return _specialLoginData;
    }

    #endregion

    #region FriendMode
    public void InviteFriendModeRequest(InviteFriendModeRequest request, UnityAction onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.InviteFriendModeRequest(
            request
            , delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    onComplete?.Invoke();
                }
                else
                {
                    onFail?.Invoke(resultProcess.MessageDetail);
                }
            }
            , delegate (PlayFabError error)
            {
                onFail?.Invoke(error.Error.ToString());
            }
        );
    }

    public void ClearFriendMatchRequest(UnityAction onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.ClearFriendMatchRequest(
              delegate (ExecuteCloudScriptResult result)
              {
                  CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                  if (resultProcess.IsSuccess)
                  {
                      _friendMatchDataList.Clear();
                      onComplete?.Invoke();
                  }
                  else
                  {
                      onFail?.Invoke(resultProcess.MessageDetail);
                  }
              }
             , delegate (PlayFabError error)
             {
                 onFail?.Invoke(error.Error.ToString());
             }
         );
    }

    public void DeclineFriendMatchRequest(string playerID, UnityAction onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.DeclineFriendMatchRequest(
              playerID
            , delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    int index = _friendMatchDataList.FindIndex(x => x.PlayerID == playerID);
                    if (index >= 0 && index < _friendMatchDataList.Count)
                    {
                        _friendMatchDataList.RemoveAt(index);
                    }

                    onComplete?.Invoke();
                }
                else
                {
                    onFail?.Invoke(resultProcess.MessageDetail);
                }
            }
            , delegate (PlayFabError error)
            {
                onFail?.Invoke(error.Error.ToString());
            }
        );
    }

    public void RefreshFriendMatchRequest(UnityAction onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.RefreshFriendMatchRequest(
              delegate (ExecuteCloudScriptResult result)
              {
                  CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                  if (resultProcess.IsSuccess)
                  {
                      Dictionary<string, Dictionary<string, string>> rawData = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, string>>>(resultProcess.CustomData["result"]);
                      _friendMatchDataList.Clear();
                      foreach (KeyValuePair<string, Dictionary<string, string>> data in rawData)
                      {
                          _friendMatchDataList.Add(new FriendMatchData(data.Key, data.Value));
                      }

                      onComplete?.Invoke();
                  }
                  else
                  {
                      onFail?.Invoke(resultProcess.MessageDetail);
                  }
              }
             , delegate (PlayFabError error)
             {
                 onFail?.Invoke(error.Error.ToString());
             }
         );
    }

    public List<FriendMatchData> GetFriendMatchDataList()
    {
        return _friendMatchDataList;
    }

    public List<FriendMatchData> GetFriendMatchDataList(FriendMatchData.StatusType type)
    {
        return _friendMatchDataList.FindAll(player => player.Status == type);
    }
    #endregion

    #region Friend/Player Methods
    public void RefreshAllFriendAndLastMatchPlayerDataAndReferreeList(UnityAction onComplete, UnityAction<string> onFail)
    {
        TaskLoader<string> taskLoader = new TaskLoader<string>();
        taskLoader.AddTask(LoadLastMatchPlayers);
        taskLoader.AddTask(RefreshAllFriendList);
        taskLoader.AddTask(RefreshAllReferreeList);
        taskLoader.StartTasks(onComplete
            , delegate (List<string> failList)
            {
                if (onFail != null)
                {
                    string failText = "";
                    foreach (string item in failList)
                    {
                        failText += "|" + item;
                    }
                    onFail.Invoke(failText);
                }
            }
        );
    }

    #region Utilities
    private void LoadPlayerInfoList(List<string> playerIDList, UnityAction<List<PlayerInfoData>> onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.GetPlayerInfoList(
            playerIDList
            , delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    List<PlayerInfoData> playerInfoDataList = new List<PlayerInfoData>();
                    Dictionary<string, object> data = JsonConvert.DeserializeObject<Dictionary<string, object>>(resultProcess.CustomData["result"]);
                    foreach (KeyValuePair<string, object> item in data)
                    {
                        playerInfoDataList.Add(new PlayerInfoData(JsonConvert.DeserializeObject<Dictionary<string, object>>(item.Value.ToString())));
                    }

                    if (onComplete != null)
                    {
                        onComplete.Invoke(playerInfoDataList);
                    }
                }
                else
                {
                    if (onFail != null)
                    {
                        onFail.Invoke(resultProcess.MessageDetail);
                    }
                }
            }
            , delegate (PlayFabError error)
            {
                if (onFail != null)
                {
                    onFail.Invoke(error.Error.ToString());
                }
            }
        );
    }
    #endregion

    #region Last Match Player
    public void UpdateLastMatchPlayer(PlayerInfoData playerData, UnityAction onComplete, UnityAction<string> onFail)
    {
        _lastMatchPlayerInfoData = new PlayerInfoData(playerData);
        PlayFabManager.Instance.SetLastMatchPlayer(
            playerData.PlayerID,
             delegate (ExecuteCloudScriptResult result)
             {
                 CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                 if (resultProcess.IsSuccess)
                 {
                     _lastMatchPlayerIDListCache = JsonConvert.DeserializeObject<List<string>>(resultProcess.CustomData["data"]);
                     if (onComplete != null)
                     {
                         onComplete.Invoke();
                     }
                 }
                 else
                 {
                     if (onFail != null)
                     {
                         onFail.Invoke(resultProcess.MessageDetail);
                     }
                 }
             },
             delegate (PlayFabError error)
             {
                 if (onFail != null)
                 {
                     onFail.Invoke(error.Error.ToString());
                 }
             }
        );
    }

    public void LoadLastMatchPlayers(UnityAction onComplete, UnityAction<string> onFail)
    {
        Debug.Log("Start LoadLastMatchPlayers ...");

        PlayFabManager.Instance.GetPlayerReadOnlyData(
            _recordMatchPlayersKey
            , delegate (GetUserDataResult result)
            {
                if (result.Data.ContainsKey(_recordMatchPlayersKey))
                {
                    List<string> data = JsonConvert.DeserializeObject<List<string>>(result.Data[_recordMatchPlayersKey].Value);

                    OnLoadLastMatchPlayersComplete(
                          data
                        , delegate ()
                        {
                            Debug.Log("<color=green>Completed</color> LoadLastMatchPlayers");
                            onComplete?.Invoke();
                        }
                        , delegate (string errorMsg)
                        {
                            Debug.LogFormat("<color=red>Failed</color> LoadLastMatchPlayers : {0}", errorMsg);
                            onFail?.Invoke(errorMsg);
                        }
                    );
                }
                else
                {
                    _lastMatchPlayerIDListCache = new List<string>();

                    Debug.Log("<color=green>Completed</color> LoadLastMatchPlayers");
                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                }
            }
            , delegate (PlayFabError error)
            {
                Debug.LogFormat("<color=red>Failed</color> LoadLastMatchPlayers : {0}", error.ErrorMessage);

                GameHelper.DelayCallback(
                      1.0f
                    , delegate ()
                    {
                        Debug.Log("Retry LoadLastMatchPlayers ...");
                        LoadLastMatchPlayers(onComplete, onFail);
                    }
                );
            }
        );
    }

    private void OnLoadLastMatchPlayersComplete(List<string> data, UnityAction onComplete, UnityAction<string> onFail)
    {
        TaskLoader<string> taskLoader = new TaskLoader<string>();
        _lastMatchPlayerInfoDataList = new List<PlayerInfoData>();
        _lastMatchPlayerIDList = new List<string>(data);
        _lastMatchPlayerIDListCache = new List<string>(_lastMatchPlayerIDList);

        int loopCount = Mathf.CeilToInt((float)_lastMatchPlayerIDListCache.Count / (float)_maxLoadCallForGetPlayerInfo);
        for (int i = 0; i < loopCount; i++)
        {
            taskLoader.AddTask(LoadLastMatchPlayerInfoData);
        }

        taskLoader.StartTasks(
             onComplete
             , delegate (List<string> failList)
             {
                 if (onFail != null)
                 {
                     string failText = "";
                     foreach (string item in failList)
                     {
                         failText += "|" + item;
                     }
                     onFail.Invoke(failText);
                 }
             }
         );
    }

    private void LoadLastMatchPlayerInfoData(UnityAction onComplete, UnityAction<string> onFail)
    {
        List<string> dataList = new List<string>();
        foreach (string item in _lastMatchPlayerIDListCache)
        {
            if (dataList.Count < _maxLoadCallForGetPlayerInfo)
            {
                dataList.Add(item);
            }
            else
            {
                break;
            }
        }

        if (dataList.Count > 0)
        {
            _lastMatchPlayerIDListCache.RemoveRange(0, dataList.Count);
            LoadPlayerInfoList(
                dataList
                , delegate (List<PlayerInfoData> infoData)
                {
                    _lastMatchPlayerInfoDataList.AddRange(infoData);
                    OrderLastMatchPlayerInfoDataList();
                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                }
                , onFail
            );
        }
        else
        {
            if (onComplete != null)
            {
                onComplete.Invoke();
            }
        }
    }

    private void OrderLastMatchPlayerInfoDataList()
    {
        if (_lastMatchPlayerIDList == null || _lastMatchPlayerIDList.Count == 0)
            return;

        _lastMatchPlayerInfoDataList = _lastMatchPlayerInfoDataList.OrderBy(info => _lastMatchPlayerIDList).ToList();
        _lastMatchPlayerInfoDataList.Reverse();
    }

    public List<PlayerInfoData> GetLastMatchPlayerList()
    {
        return _lastMatchPlayerInfoDataList;
    }
    #endregion

    #region Friend
    private void AddFriendIDList(string playerID, FriendStatusData statusData)
    {
        _friendIDList.Add(playerID);

        if (!_friendStatusDataList.ContainsKey(playerID))
        {
            _friendStatusDataList.Add(playerID, new FriendStatusData());
        }
        _friendStatusDataList[playerID] = statusData;
    }

    private void RefreshAllFriendList(UnityAction onComplete, UnityAction<string> onFail)
    {
        DateTime timestampBeginProcess = DateTime.Now;

        PlayFabManager.Instance.RefreshFriendRequest(
            delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    if (timestampBeginProcess < _friendInfoDataListUpdateTimestamp)
                    {
                        foreach (FriendInfoData friend in _friendInfoDataList)
                        {
                            AddFriendIDList(friend.PlayerInfo.PlayerID, friend.StatusData);
                        }

                        int loopCount = Mathf.CeilToInt((float)_friendIDList.Count / (float)_maxLoadCallForGetPlayerInfo);
                        TaskLoader<string> taskLoader = new TaskLoader<string>();
                        for (int i = 0; i < loopCount; i++)
                        {
                            taskLoader.JoinTask(LoadFriendInfoData);
                        }

                        taskLoader.StartTasks(
                            onComplete
                            , delegate (List<string> failList)
                            {
                                if (onFail != null)
                                {
                                    string failText = "";
                                    foreach (string item in failList)
                                    {
                                        failText += "|" + item;
                                    }
                                    onFail.Invoke(failText);
                                }
                            }
                        );
                        return;
                    }

                    bool isNew = false;
                    List<string> friendList = JsonConvert.DeserializeObject<List<string>>(resultProcess.CustomData["result"]);
                    List<PlayerInfoData> friendDataList = GetFriendList(FriendStatus.Friend);
                    friendDataList.AddRange(GetFriendList(FriendStatus.Request_Sender));

                    if (friendList != null)
                    {
                        if (friendList.Count != friendDataList.Count)
                        {
                            // not equal sender
                            isNew = true;
                        }
                        else
                        {
                            // equal sender
                            // check one by one. Is totally equal?
                            foreach (string item in friendList)
                            {
                                // check if this ID doesn't exist in old list
                                if (!friendDataList.Exists(x => x.PlayerID == item))
                                {
                                    isNew = true;
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        // sender is null
                        isNew = true;
                    }

                    if (isNew)
                    {
                        LoadAllFriendList(onComplete, onFail);
                    }
                    else
                    {
                        foreach (FriendInfoData friend in _friendInfoDataList)
                        {
                            AddFriendIDList(friend.PlayerInfo.PlayerID, friend.StatusData);
                        }

                        int loopCount = Mathf.CeilToInt((float)_friendIDList.Count / (float)_maxLoadCallForGetPlayerInfo);
                        TaskLoader<string> taskLoader = new TaskLoader<string>();
                        for (int i = 0; i < loopCount; i++)
                        {
                            taskLoader.AddTask(LoadFriendInfoData);
                        }

                        taskLoader.StartTasks(
                            () =>
                            {
                                _friendInfoDataList = new List<FriendInfoData>(_friendInfoDataListTmp);
                                onComplete?.Invoke();
                            }
                            , delegate (List<string> failList)
                            {
                                if (onFail != null)
                                {
                                    string failText = "";
                                    foreach (string item in failList)
                                    {
                                        failText += "|" + item;
                                    }
                                    onFail.Invoke(failText);
                                }
                            }
                        );
                    }
                }
                else
                {
                    onComplete?.Invoke();
                }
            }
            , delegate (PlayFabError error)
            {
                Debug.Log(error.ToString());
                RefreshAllFriendList(onComplete, onFail);
            }
        );
    }

    public void LoadAllFriendList(UnityAction onComplete, UnityAction<string> onFail)
    {
        Debug.Log("Start LoadAllFriendList ...");

        UnityAction<GetFriendsListResult> onLoadFriendListComplete = delegate (GetFriendsListResult result)
        {
            OnLoadAllFriendListComplete(
                result
                , delegate ()
                {
                    Debug.Log("<color=green>Completed</color> LoadAllFriendList");
                    onComplete?.Invoke();
                }
                , delegate (string errorMsg)
                {
                    Debug.LogFormat("<color=red>Failed</color> LoadAllFriendList : {0}", errorMsg);
                    onFail?.Invoke(errorMsg);
                }
            );
        };

        UnityAction<PlayFabError> onLoadFriendListFail = delegate (PlayFabError error)
        {
            Debug.LogFormat("<color=red>Failed</color> LoadAllFriendList : {0}", error.ErrorMessage);
            if (error.Error == PlayFabErrorCode.FacebookAPIError || error.Error == PlayFabErrorCode.InvalidFacebookToken)
            {
                if (GameHelper.IsInLoginScene() && PlayerAccountInfo.IsLinkedFacebook)
                {
                    PopupUIManager.Instance.ShowPopup_SureCheck(
                        LocalizationManager.Instance.GetText("ERROR_FACEBOOK_API_RELOGIN_TITLE")
                        , LocalizationManager.Instance.GetText("ERROR_FACEBOOK_API_RELOGIN_MESSAGE")
                        , delegate ()
                        {
                            AuthenticationManager.AuthenticateFacebook(
                                delegate ()
                                {
                                    Debug.Log("Retry LoadAllFriendList ...");
                                    LoadAllFriendList(onComplete, onFail);
                                }
                                , onFail
                            );
                        }
                        , delegate ()
                        {
                            onFail?.Invoke(error.ErrorMessage);
                        }
                    );
                }
                return;
            }

            Debug.LogFormat("<color=red>Failed</color> LoadAllFriendList : {0}", error.ErrorMessage);
            onFail?.Invoke(error.ErrorMessage);
        };

        PlayFabManager.Instance.GetFriendList(onLoadFriendListComplete, onLoadFriendListFail);
    }

    private void OnLoadAllFriendListComplete(GetFriendsListResult result, UnityAction onComplete, UnityAction<string> onFail)
    {
        _friendInfoDataListTmp = new List<FriendInfoData>();
        _friendIDList = new List<string>();

        TaskLoader<string> taskLoader = new TaskLoader<string>();
        List<FriendInfo> friendInfoList = new List<FriendInfo>(result.Friends);
        foreach (FriendInfo friendInfo in friendInfoList)
        {
            if (friendInfo.Tags != null && friendInfo.Tags.Count > 0)
            {
                AddFriendIDList(friendInfo.FriendPlayFabId, new FriendStatusData(friendInfo.Tags[0]));
            }
        }

        int loopCount = Mathf.CeilToInt((float)_friendIDList.Count / (float)_maxLoadCallForGetPlayerInfo);
        for (int i = 0; i < loopCount; i++)
        {
            taskLoader.AddTask(LoadFriendInfoData);
        }

        taskLoader.StartTasks(
            () =>
            {
                //string friendText = "";
                //foreach (FriendInfoData friendData in _friendInfoDataListTmp)
                //{
                //    if (friendText.Length > 0)
                //    {
                //        friendText += ", ";
                //    }

                //    friendText += friendData.ToString();
                //}
                //Debug.LogFormat("OnLoadAllFriendListComplete: {0}", friendText);

                _friendInfoDataList = new List<FriendInfoData>(_friendInfoDataListTmp);
                onComplete?.Invoke();
            }
            , delegate (List<string> failList)
            {
                if (onFail != null)
                {
                    string failText = "";
                    foreach (string item in failList)
                    {
                        failText += "|" + item;
                    }
                    onFail.Invoke(failText);
                }
            }
        );
    }

    private void LoadFriendInfoData(UnityAction onComplete, UnityAction<string> onFail)
    {
        List<string> idList = new List<string>();
        foreach (string item in _friendIDList)
        {
            if (idList.Count < _maxLoadCallForGetPlayerInfo)
            {
                idList.Add(item);
            }
            else
            {
                break;
            }
        }

        if (idList.Count > 0)
        {
            _friendIDList.RemoveRange(0, idList.Count);
            LoadPlayerInfoList(
                  idList
                , delegate (List<PlayerInfoData> result)
                {
                    _friendInfoDataListUpdateTimestamp = DateTime.Now;
                    foreach (PlayerInfoData item in result)
                    {
                        if (_friendInfoDataListTmp.Exists(x => x.PlayerInfo.PlayerID == item.PlayerID))
                        {
                            _friendInfoDataListTmp.RemoveAt(_friendInfoDataListTmp.FindIndex(x => x.PlayerInfo.PlayerID == item.PlayerID));
                        }
                        _friendInfoDataListTmp.Add(new FriendInfoData(item, _friendStatusDataList[item.PlayerID]));
                    }

                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                }
                , delegate (string error)
                {
                    onFail?.Invoke(error);
                }
            );
        }
        else
        {
            if (onComplete != null)
            {
                onComplete.Invoke();
            }
        }
    }

    public List<PlayerInfoData> GetFriendList(FriendStatus status)
    {
        List<PlayerInfoData> result = new List<PlayerInfoData>();
        foreach (FriendInfoData data in _friendInfoDataList)
        {
            if (data.StatusData.Status == status)
            {
                if (data.StatusData.IsExpired())
                {
                    RemoveFriend(data.PlayerInfo.PlayerID, null, null);
                }
                else
                {
                    result.Add(data.PlayerInfo);
                }
            }
        }
        List<PlayerInfoData> dataSorted = result.OrderBy(friend => friend.DisplayName).ToList();
        return dataSorted;
    }

    public void SearchAndAddFriendRequestByDisplayName(string displayName, UnityAction onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.GetPlayerAccountInfoByDisplayName(
            displayName
            , delegate (GetAccountInfoResult result)
            {
                string playerID = result.AccountInfo.PlayFabId;
                AddFriendRequest(playerID, onComplete, onFail);
            }
            , delegate (PlayFabError error)
            {
                if (onFail != null)
                {
                    onFail.Invoke(error.Error.ToString());
                }
            }
        );
    }

    public void SearchAndAddFriendRequestByPlayerID(string playerID, UnityAction onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.GetPlayerAccountInfoByPlayerID(
            playerID
            , delegate (GetAccountInfoResult result)
            {
                AddFriendRequest(playerID, onComplete, onFail);
            }
            , delegate (PlayFabError error)
            {
                if (onFail != null)
                {
                    onFail.Invoke(error.Error.ToString());
                }
            }
        );
    }

    public void AddFriendRequest(string playerID, UnityAction onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.AddFriendRequest(playerID
            , delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    Dictionary<string, object> data = JsonConvert.DeserializeObject<Dictionary<string, object>>(resultProcess.CustomData["result"]);
                    Dictionary<string, object> dataResult = JsonConvert.DeserializeObject<Dictionary<string, object>>(data[playerID].ToString());
                    FriendStatusData statusData = new FriendStatusData(resultProcess.CustomData["tag"]);

                    _friendInfoDataListUpdateTimestamp = DateTime.Now;
                    _friendInfoDataList.Add(new FriendInfoData(new PlayerInfoData(dataResult), statusData));

                    if (statusData.Status == FriendStatus.Friend)
                    {
                        for (int i = 0; i < _lastMatchPlayerInfoDataList.Count; i++)
                        {
                            if (_lastMatchPlayerInfoDataList[i].PlayerID == playerID)
                            {
                                _lastMatchPlayerInfoDataList.RemoveAt(i);
                                break;
                            }
                        }
                    }

                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                }
                else
                {
                    if (onFail != null)
                    {
                        onFail.Invoke(resultProcess.MessageDetail);
                    }
                }
            }
            , delegate (PlayFabError error)
            {
                if (onFail != null)
                {
                    onFail.Invoke(error.Error.ToString());
                }
            }
        );
    }

    public void AcceptFriendRequest(string playerID, UnityAction onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.AcceptFriendRequest(playerID
            , delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    for (int i = 0; i < _friendInfoDataList.Count; i++)
                    {
                        if (_friendInfoDataList[i].PlayerInfo.PlayerID == playerID)
                        {
                            // Move FriendInfoData to new Status
                            _friendInfoDataListUpdateTimestamp = DateTime.Now;
                            _friendInfoDataList[i].SetFriendStatus(new FriendStatusData(FriendStatus.Friend));
                            break;
                        }
                    }

                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                }
                else
                {
                    if (onFail != null)
                    {
                        onFail.Invoke(resultProcess.MessageDetail);
                    }
                }
            }
            , delegate (PlayFabError error)
            {
                if (onFail != null)
                {
                    onFail.Invoke(error.Error.ToString());
                }
            }
        );
    }

    public void RemoveFriend(string playerID, UnityAction onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.RemoveFriendRequest(playerID
            , delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    for (int i = 0; i < _friendInfoDataList.Count; i++)
                    {
                        if (_friendInfoDataList[i].PlayerInfo.PlayerID == playerID)
                        {
                            // Remove FriendInfoData
                            _friendInfoDataListUpdateTimestamp = DateTime.Now;
                            _friendInfoDataList.RemoveAt(i);
                            break;
                        }
                    }

                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                }
                else
                {
                    if (onFail != null)
                    {
                        onFail.Invoke(resultProcess.MessageDetail);
                    }
                }
            }
            , delegate (PlayFabError error)
            {
                if (onFail != null)
                {
                    onFail.Invoke(error.Error.ToString());
                }
            }
        );
    }
    #endregion

    #region Referral
    public string GetCurrentPlayerReferralCode()
    {
        return PlayerInfo.PlayerID;
    }

    public List<ReferreeInfoData> GetReferreeList()
    {
        return _referreeInfoDataList;
    }

    public void RegisterFriendReferralCode(string referralCode, UnityAction<List<ItemData>> onComplete, UnityAction<string> onFail)
    {
        PopupUIManager.Instance.Show_MidSoftLoad(true);
        List<ItemData> rewardClaimedList = new List<ItemData>();
        PlayFabManager.Instance.RegisterFriendReferralCode(
            referralCode,
            delegate (ExecuteCloudScriptResult result)
            {
                PopupUIManager.Instance.Show_MidSoftLoad(false);
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    OnApplyReferral?.Invoke();
                    if (resultProcess.CustomData.ContainsKey("rewardClaimed") && resultProcess.CustomData["rewardClaimed"] != null)
                    {
                        string json = resultProcess.CustomData["rewardClaimed"];
                        Dictionary<string, object> rawItemList = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);

                        foreach (KeyValuePair<string, object> rawItem in rawItemList)
                        {
                            string itemID = rawItem.Key;
                            int amount = int.Parse(rawItem.Value.ToString());

                            rewardClaimedList.Add(new ItemData(itemID, amount));
                        }
                    }

                    PlayerReferredInfoData.SetHasRegisteredReferralCode(true, referralCode);
                    PlayerReferredInfoData.SetHasClaimedLevel3Reward(rewardClaimedList.Count > 0);
                    onComplete?.Invoke(rewardClaimedList);
                }
                else
                {
                    Debug.Log(resultProcess.MessageDetail.ToString());
                    onFail?.Invoke(resultProcess.MessageDetail.ToString());
                }
            }
            ,
            null
        );
    }

    public void RequestClaimReferreeLevel7Reward(string playerID, UnityAction<List<ItemData>> onComplete, UnityAction<string> onFail)
    {
        PopupUIManager.Instance.Show_MidSoftLoad(true);
        List<ItemData> rewardClaimedList = new List<ItemData>();
        PlayFabManager.Instance.ClaimReferralLevel7Reward(
            playerID,
            delegate (ExecuteCloudScriptResult result)
            {
                PopupUIManager.Instance.Show_MidSoftLoad(false);
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    OnClaimedReferral?.Invoke();
                    if (resultProcess.CustomData.ContainsKey("rewardClaimed") && resultProcess.CustomData["rewardClaimed"] != null)
                    {
                        string json = resultProcess.CustomData["rewardClaimed"];
                        Dictionary<string, object> rawItemList = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);

                        foreach (KeyValuePair<string, object> rawItem in rawItemList)
                        {
                            string itemID = rawItem.Key;
                            int amount = int.Parse(rawItem.Value.ToString());

                            rewardClaimedList.Add(new ItemData(itemID, amount));
                        }
                    }

                    onComplete?.Invoke(rewardClaimedList);
                }
                else
                {
                    Debug.Log(resultProcess.MessageDetail.ToString());
                    onFail?.Invoke(resultProcess.MessageDetail.ToString());
                }
            }
            , null
        );
    }

    private void OnLoadPlayerRegisteredReferralCode(GetUserDataResult result)
    {
        if (result.Data.ContainsKey(_playerRegisteredReferralCodeKey))
        {
            string json = result.Data[_playerRegisteredReferralCodeKey].Value;
            Dictionary<string, object> rawObject = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
            if (!rawObject.ContainsKey("referralCode") || !rawObject.ContainsKey("claimedRewardLevel3"))
            {
                Debug.Log("Player have registered code but don't have correct JSON structure! *Should inspect* <color=green>Completed</color>");
            }
            else
            {
                PlayerReferredInfoData = new ReferredInfoData(true,
                    rawObject["referralCode"].ToString(),
                    (bool)rawObject["claimedRewardLevel3"]
                    );
                Debug.Log("OnLoadPlayerRegisteredReferralCode <color=green>Completed</color>");
            }
        }
        else
        {
            Debug.Log("OnLoadPlayerRegisteredReferralCodeComplete (Haven't registered referral code) <color=green>Completed</color>");
        }
    }

    private void RefreshAllReferreeList(UnityAction onComplete, UnityAction<string> onFail)
    {
        int claimedRewardTimes = 0;
        foreach (var referree in _referreeInfoDataList)
        {
            if (referree.ClaimedRewardLevel7)
            {
                claimedRewardTimes++;
            }
        }
        //If already claimed reward maximum times -> no more update calls
        if (claimedRewardTimes >= 10)
        {
            onComplete?.Invoke();
            return;
        }

        PlayFabManager.Instance.RefreshReferralList(
            delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    _referreeInfoDataList.Clear();
                    if (resultProcess.CustomData.ContainsKey("referreeProfilelist") && resultProcess.CustomData["referreeProfilelist"] != null)
                    {
                        Dictionary<string, object> data = JsonConvert.DeserializeObject<Dictionary<string, object>>(resultProcess.CustomData["referreeProfilelist"]);
                        foreach (KeyValuePair<string, object> item in data)
                        {
                            _referreeInfoDataList.Add(new ReferreeInfoData(JsonConvert.DeserializeObject<Dictionary<string, object>>(item.Value.ToString())));
                        }
                    }

                    onComplete?.Invoke();
                }
                else
                {
                    Debug.Log("<color=red>RefreshAllReferreeList fail:</color> " + resultProcess.MessageDetail.ToString());
                    onFail?.Invoke(resultProcess.MessageDetail.ToString());
                }

            }
            , delegate (PlayFabError error)
            {
                Debug.Log("<color=red>RefreshAllReferreeList error:</color> " + error.ToString());
                GameHelper.DelayCallback(
                        1.0f
                    , delegate ()
                    {
                        Debug.Log("Retry RefreshAllReferreeList ...");
                        RefreshAllReferreeList(onComplete, onFail);
                    }
                );
            }
        );
    }
    #endregion

    #endregion

    #region Tutorial Methods

    private void OnLoadTutorialRewardComplete(GetTitleDataResult result)
    {
        if (result.Data.ContainsKey(_titleTutorialRewardKey))
        {
            _tutorialRewardDataList = new Dictionary<int, List<ItemData>>();
            Dictionary<int, List<object>> rawDatas;
            if (GameHelper.TryDeserializeJSONStr(result.Data[_titleTutorialRewardKey], out rawDatas))
            {
                foreach (KeyValuePair<int, List<object>> data in rawDatas)
                {
                    List<ItemData> itemList = new List<ItemData>();
                    for (int i = 0; i < data.Value.Count; i++)
                    {
                        itemList.Add(new ItemData(data.Value[i].ToString()));
                    }
                    _tutorialRewardDataList.Add(data.Key, itemList);
                }
            }
            Debug.Log("OnLoadTutorialRewardComplete <color=green>Completed</color>");
        }
        else
        {
            Debug.Log("OnLoadTutorialRewardComplete <color=red>Fail</color>");
        }
    }

    // index start at 0.
    public List<ItemData> GetTutorialReward(int index)
    {
        List<ItemData> result = new List<ItemData>();
        _tutorialRewardDataList.TryGetValue(index, out result);
        return result;
    }

    public bool IsCompletedTutorial(GameMode mode)
    {
        int tutorialIndex = -1;
        switch (mode)
        {
            case GameMode.Tutorial_1: tutorialIndex = 1; break;
            case GameMode.Tutorial_2: tutorialIndex = 2; break;
            case GameMode.Tutorial_3: tutorialIndex = 3; break;
            case GameMode.Tutorial_4: tutorialIndex = 4; break;
            default: return false;
        }

        string tutorialKey = string.Format("{0}_{1}_{2}", StatKeys.W.ToString(), StatKeys.TUT.ToString(), tutorialIndex);

        Dictionary<string, int> playerStat = PlayerInfo.GetPlayerStatData();
        if (playerStat.ContainsKey(tutorialKey))
        {
            int tutorialStep = playerStat[tutorialKey];
            if (tutorialStep <= 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        else
        {
            // บางครั้ง Stat W_TUT_1 จะไม่ถูกยิงขึ้นมา
            if (mode == GameMode.Tutorial_1)
            {
                bool isCompleteTutorial_1 = false;

                isCompleteTutorial_1 |= IsPlayedTutorial(GameMode.Tutorial_2);
                isCompleteTutorial_1 |= IsCompletedTutorial(GameMode.Tutorial_2);
                isCompleteTutorial_1 |= IsPlayedTutorial(GameMode.Tutorial_3);
                isCompleteTutorial_1 |= IsCompletedTutorial(GameMode.Tutorial_3);
                isCompleteTutorial_1 |= IsPlayedTutorial(GameMode.Tutorial_4);
                isCompleteTutorial_1 |= IsCompletedTutorial(GameMode.Tutorial_4);

                return isCompleteTutorial_1;
            }
        }

        return false;
    }

    public bool IsPlayedTutorial(GameMode mode)
    {
        int tutorialIndex = -1;
        switch (mode)
        {
            case GameMode.Tutorial_1: tutorialIndex = 1; break;
            case GameMode.Tutorial_2: tutorialIndex = 2; break;
            case GameMode.Tutorial_3: tutorialIndex = 3; break;
            case GameMode.Tutorial_4: tutorialIndex = 4; break;
            default: return false;
        }

        string tutorialKey = string.Format("{0}_{1}_{2}", StatKeys.W.ToString(), StatKeys.TUT.ToString(), tutorialIndex);

        Dictionary<string, int> playerStat = PlayerInfo.GetPlayerStatData();
        if (playerStat.ContainsKey(tutorialKey))
        {
            int tutorialStep = playerStat[tutorialKey];
            if (tutorialStep <= 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        return false;
    }

    public void SetSkipTutorial(bool isSkip)
    {
        PlayerPrefs.SetInt(_tutorialSkipKey, (isSkip) ? 1 : 0);
    }

    public bool IsSkipTutorial()
    {
        return (PlayerPrefs.GetInt(_tutorialSkipKey, 0) != 0);
    }

    public void LoadPlayerTutorialStatus(UnityAction onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.GetPlayerReadOnlyData(
              _playerTutorialStatusKey
            , delegate (GetUserDataResult result)
            {
                OnLoadPlayerTutorialStatus(result);

                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            }
            , delegate (PlayFabError error)
            {
                if (onFail != null)
                {
                    onFail.Invoke(error.Error.ToString());
                }
            }
        );
    }

    private void OnLoadPlayerTutorialStatus(GetUserDataResult result)
    {
        bool isPlayerRedeemReward = false;
        if (result.Data.ContainsKey(_playerTutorialStatusKey))
        {
            // Get save data.
            isPlayerRedeemReward = bool.Parse(result.Data[_playerTutorialStatusKey].Value);
        }
        SetIsClaimTutorialReward(isPlayerRedeemReward);
    }

    public void RequestClaimTutorialReward(UnityAction<List<ItemData>> onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.ClaimTutorialReward(
            delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    string json = resultProcess.CustomData["item_list"];
                    List<Dictionary<string, object>> rawItemList = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(json);
                    List<ItemData> itemList = new List<ItemData>();

                    foreach (Dictionary<string, object> rawItem in rawItemList)
                    {
                        string itemID = (string)rawItem["ItemID"];
                        int amount = int.Parse(rawItem["Amount"].ToString());

                        itemList.Add(new ItemData(itemID, amount));
                    }

                    SetIsClaimTutorialReward(true);

                    onComplete?.Invoke(itemList);
                }
                else
                {
                    onFail?.Invoke(resultProcess.MessageDetail);
                }
            }
            , delegate (PlayFabError error)
            {
                onFail?.Invoke(error.ErrorMessage);
            }
        );
    }

    private void SetIsClaimTutorialReward(bool isPlayerRedeemReward)
    {
        _isPlayerRedeemReward = isPlayerRedeemReward;
        OnUpdateClaimTutorialReward?.Invoke(isPlayerRedeemReward);
    }

    public bool GetIsClaimTutorialReward()
    {
        return _isPlayerRedeemReward;
    }
    #region Tutorial 2 Reward

    [Obsolete]
    public void RequestCheckClaimFinishTutorial2Reward(UnityAction onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.CheckClaimFinishTutorial2Reward(
                delegate (ExecuteCloudScriptResult result)
                {
                    CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                    bool isCanClaim = resultProcess.IsSuccess;
                    SetPlayerCanRedeemTutorial2Reward(isCanClaim);

                    Debug.Log("<color=green>Completed</color> CheckClaimFinishTutorial2Reward");
                    onComplete?.Invoke();
                }
                , delegate (PlayFabError error)
                {
                    SetPlayerCanRedeemTutorial2Reward(false);
                    onFail?.Invoke(error.ErrorMessage);
                }
            );
    }

    [Obsolete]
    public void RequestClaimFinishTutorial2Reward(UnityAction<List<ItemData>> onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.ClaimFinishTutorial2Reward(
              delegate (ExecuteCloudScriptResult result)
              {
                  CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                  if (resultProcess.IsSuccess)
                  {
                      string json = resultProcess.CustomData["item_list"];
                      List<Dictionary<string, object>> rawItemList = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(json);
                      List<ItemData> itemList = new List<ItemData>();

                      foreach (Dictionary<string, object> rawItem in rawItemList)
                      {
                          string itemID = (string)rawItem["ItemID"];
                          int amount = int.Parse(rawItem["Amount"].ToString());

                          itemList.Add(new ItemData(itemID, amount));
                      }
                      SetPlayerCanRedeemTutorial2Reward(false);
                      onComplete?.Invoke(itemList);
                  }
                  else
                  {
                      SetPlayerCanRedeemTutorial2Reward(false);
                      onFail?.Invoke(resultProcess.MessageDetail);
                  }
              }
              , delegate (PlayFabError error)
              {
                  SetPlayerCanRedeemTutorial2Reward(false);
                  onFail?.Invoke(error.ErrorMessage);
              }
          );
    }

    private void SetPlayerCanRedeemTutorial2Reward(bool isCanRedeem)
    {
        _isPlayerRedeemReward2 = isCanRedeem;
    }

    public bool GetPlayerCanRedeemTutorial2Reward()
    {
        return _isPlayerRedeemReward2;
    }

    #endregion

    public void ClaimTutorialRewardByTutorialIndex(int tutorialIndex)
    {
        if (tutorialIndex >= _redeemTutorialRewardStatusList.Length)
            return;

        object param = new
        {
            TutorialIndex = tutorialIndex
        };
        PlayFabManager.Instance.ExecuteCloudScript("ClaimTutorialRewardByTutorialIndex", param, null, null);
    }

    public void SetPlayerRedeemTutorialStatusToLocal(int tutorialIndex)
    {
        if (tutorialIndex >= _redeemTutorialRewardStatusList.Length)
            return;
        _redeemTutorialRewardStatusList[tutorialIndex] = true;
    }

    public bool[] GetPlayerRedeemTutorialStatus()
    {
        return _redeemTutorialRewardStatusList;
    }

    #endregion

    #region Adventure Methods
    public Dictionary<AdventureChapterID, List<AdventureStageData>> GetAdventureDataDict()
    {
        return _adventureDataDict;
    }

    public Dictionary<string, bool> GetRewardedAdventureDict()
    {
        return _rewardedAdventureDict;
    }

    public void RequestClaimAdventureReward(AdventureChapterID chapterID, AdventureStageID stageID, UnityAction<CloudScriptResultProcess> onComplete)
    {
        PlayFabManager.Instance.ClaimAdventureReward(
            chapterID,
            stageID,
            delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);

                onComplete?.Invoke(resultProcess);
            }
            , null
        );
    }

    private void OnLoadAdventureDBComplete(GetTitleDataResult result)
    {
        _adventureDataDict = new Dictionary<AdventureChapterID, List<AdventureStageData>>();
        foreach (string adventureKey in _adventureKeyList)
        {
            if (result.Data.ContainsKey(adventureKey))
            {
                Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(result.Data[adventureKey]);
                if (!GameHelper.StrToEnum(rawData["chapter_id"].ToString(), out AdventureChapterID chapterID))
                {
                    Debug.Log("No Chapter ID: " + rawData["chapter_id"].ToString() + " in enums");
                }
                _adventureDataDict[chapterID] = new List<AdventureStageData>();

                //Add stages
                Dictionary<string, object> rawStagesData = JsonConvert.DeserializeObject<Dictionary<string, object>>(rawData["stages"].ToString());
                foreach (KeyValuePair<string, object> item in rawStagesData)
                {
                    _adventureDataDict[chapterID].Add(new AdventureStageData(chapterID, item.Key, item.Value.ToString()));
                }
            }
        }

        Debug.Log("OnLoadAdventureDB <color=green>Completed</color>");
    }

    private void OnLoadPlayerAdventureRewarded(GetUserDataResult result)
    {
        _rewardedAdventureDict = new Dictionary<string, bool>();
        if (result.Data.ContainsKey(_rewardedAdvKey))
        {
            string json = result.Data[_rewardedAdvKey].Value;
            _rewardedAdventureDict = JsonConvert.DeserializeObject<Dictionary<string, bool>>(json);
        }
    }
    #endregion

    #region Weekly Rank Reward
    public WeeklyRankRewardData GetWeeklyRankRewardData()
    {
        return _weeklyRankRewardData;
    }

    public void RefreshWeeklyRankReward(UnityAction<List<ItemData>> onComplete, UnityAction<string> onFail = null)
    {
#if !DEBUG_TEST
        if (!GameHelper.IsNewWeekUTC(_weeklyRankRewardData.LastUpdateTimestamp))
        {
            onComplete?.Invoke(null);
            return;
        }
#endif

        PlayFabManager.Instance.RefreshWeeklyRankRewardStatus(
           delegate (ExecuteCloudScriptResult result)
           {
               CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
               if (resultProcess.IsSuccess)
               {
                   //Update newly stamped weekly rank reward data
                   if (resultProcess.CustomData.ContainsKey("weeklyRankRewardStatus") && resultProcess.CustomData["weeklyRankRewardStatus"] != null)
                   {
                       string json = resultProcess.CustomData["weeklyRankRewardStatus"];
                       Dictionary<string, object> rawDataDict = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                       _weeklyRankRewardData.SetStampedWeeklyRankStats(rawDataDict);
                   }

                   //reward
                   List<ItemData> rewardClaimedList = new List<ItemData>();
                   if (resultProcess.CustomData.ContainsKey("rewardClaimed") && resultProcess.CustomData["rewardClaimed"] != null)
                   {
                       string json = resultProcess.CustomData["rewardClaimed"];
                       Dictionary<string, object> rawItemList = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                       foreach (KeyValuePair<string, object> rawItem in rawItemList)
                       {
                           string itemID = rawItem.Key;
                           int amount = int.Parse(rawItem.Value.ToString());

                           rewardClaimedList.Add(new ItemData(itemID, amount));
                       }
                   }

                   onComplete?.Invoke(rewardClaimedList);
               }
               else
               {
                   Debug.Log(resultProcess.MessageDetail.ToString());
                   onFail?.Invoke(resultProcess.MessageDetail);
               }
           }
           ,
           null
       );
    }

    private void OnLoadWeeklyRankRewardDataDBComplete(GetTitleDataResult result)
    {
        if (result.Data.ContainsKey(_weeklyRankRewardDBKey))
        {
            Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(result.Data[_weeklyRankRewardDBKey]);
            _weeklyRankRewardData.RewardChestList = new List<WeeklyRankRewardChest>();
            foreach (KeyValuePair<string, object> item in rawData)
            {
                WeeklyRankRewardChest chestToAdd = new WeeklyRankRewardChest();
                Dictionary<string, object> chestData = JsonConvert.DeserializeObject<Dictionary<string, object>>(item.Value.ToString());
                chestToAdd.SetMinMaxRank(int.Parse(chestData["min"].ToString()), int.Parse(chestData["max"].ToString()));
                Dictionary<string, int> rawRewardData = JsonConvert.DeserializeObject<Dictionary<string, int>>(chestData["rewards"].ToString());
                foreach (KeyValuePair<string, int> rewardDetail in rawRewardData)
                {
                    chestToAdd.RewardList.Add(new ItemData(rewardDetail));
                }

                _weeklyRankRewardData.RewardChestList.Add(chestToAdd);
            }

            Debug.Log("OnLoadWeeklyRankRewardDataDB <color=green>Completed</color>");
        }
        else
        {
            Debug.Log("Not found key: " + _weeklyRankRewardDBKey);
        }
    }

    private void OnLoadPlayerWeeklyRankRewardStatus(GetUserDataResult result)
    {
        if (result.Data.ContainsKey(_weeklyRankRewardStatus))
        {
            string json = result.Data[_weeklyRankRewardStatus].Value;
            Dictionary<string, object> rawDataDict = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
            _weeklyRankRewardData.SetStampedWeeklyRankStats(rawDataDict);

            Debug.Log("OnLoadPlayerWeeklyRankRewardStatus <color=green>Completed</color>");
        }
        else
        {
            Debug.Log("Not found key: " + _weeklyRankRewardStatus);
        }
    }
    #endregion

    #region Shop

    public void UpdatePurchaseLog(ShopItemData item, UnityAction onComplete)
    {
        object param = new
        {
            ItemID = item.ItemId,
            Price = item.Price,
            Currency = item.Currency
        };
        PlayFabManager.Instance.ExecuteCloudScript("UpdatePurchaseLog", param, (result) => { onComplete?.Invoke(); }, null);
    }

    #endregion

    #region FirstTopup

    public void CheckAndUpdateFirstTopupItem(UnityAction<ItemData> onComplete)
    {
        PlayFabManager.Instance.ExecuteCloudScript(
            "UpdateFirstTopupItem",
            new { Currency = VirtualCurrency.RM.ToString() },
            (result) =>
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                FirstTopupStatus = true;
                if (resultProcess.IsSuccess)
                {
                    string json = resultProcess.CustomData["result"];
                    ItemData itemData = new ItemData(json);

                    onComplete?.Invoke(itemData);
                }
                else
                {
                    onComplete?.Invoke(null);
                }
            },
            null);
    }

    private void OnLoadPlayerFirstTopupStatus(GetUserDataResult result)
    {
        FirstTopupStatus = false;
        if (result.Data.ContainsKey("player_first_topup_status"))
        {
            FirstTopupStatus = bool.Parse(result.Data["player_first_topup_status"].Value);
            Debug.Log("OnLoadPlayerFirstTopupStatus <color=green>Completed</color>");
        }
    }

    #endregion

    #region Shop Limited

    private PlayerPurchaseLimitedData _playerPurchaseLimitedData;

    public PlayerPurchaseLimitedData GetPurchaseLimitedItemLogs()
    {
        return _playerPurchaseLimitedData;
    }

    private void OnLoadPlayerPurchaseLimitedData(GetUserDataResult result)
    {
        if (result.Data.ContainsKey(_shopLimitedKey))
        {
            string json = result.Data[_shopLimitedKey].Value;
            _playerPurchaseLimitedData = new PlayerPurchaseLimitedData(json);
        }
        else
        {
            _playerPurchaseLimitedData = new PlayerPurchaseLimitedData(GetCurrentSeasonIndex());
        }
    }

    public void RequestPurchaseLimitedItem(string itemID, UnityAction onComplete)
    {
        _playerPurchaseLimitedData.AddPurchaseLog(new ItemPurchaseLogData(itemID, DateTimeData.GetDateTimeUTC()));
        onComplete?.Invoke();
    }
    #endregion

    #region WildOffer

    private WildOfferDB _wildOfferDB;
    public WildOfferDB GetWildOfferDB()
    {
        return _wildOfferDB;
    }

    private void OnLoadWildOfferDBComplete(GetTitleDataResult result)
    {
        if (result.Data.ContainsKey("wild_offer_db"))
        {
            string json = result.Data["wild_offer_db"];
            _wildOfferDB = new WildOfferDB(json);
            Debug.Log("OnLoadWildOfferDBComplete <color=green>Completed</color>");
        }
        else
        {
            Debug.Log("OnLoadWildOfferDBComplete <color=red>Don't Have WildOfferDB</color>");
        }

    }

    private PlayerWildOfferData _playerWildOfferData = new PlayerWildOfferData();
    private void OnLoadPlayerWildOfferData(GetUserDataResult result)
    {
        if (result.Data.ContainsKey(_playerWildOffer))
        {
            string json = result.Data[_playerWildOffer].Value;
            _playerWildOfferData = new PlayerWildOfferData(json);
        }
    }

    public PlayerWildOfferData GetPlayerWildOfferData()
    {
        return _playerWildOfferData;
    }

    public bool AddWildOffer(string key, string itemID, UnityAction onComplete = null, string customData = null)
    {
        PlayerWildOfferData playerData = GetPlayerWildOfferData();
        if (playerData.IsCanAddWildOffer(key, itemID))
        {
            // add to local
            WildOfferItemDBData item = WildOfferManager.Instance.WildOfferDB.GetWildOfferItemData()[key];
            if (playerData.GetAllData().ContainsKey(key) == false)
            {
                playerData.GetAllData().Add(key, new WildOfferData(item));
            }

            WildOfferData wildOfferData = playerData.GetAllData()[key];
            wildOfferData.AddLogItem(itemID, item.Duration);
            wildOfferData.AddCustomData(customData);

            // add to cloudscript.
            PlayFabManager.Instance.AddWildOffer(key, itemID, () =>
            {
                onComplete?.Invoke();
                OnWildOfferAdded?.Invoke();
            }, customData);
            return true;
        }
        else
        {
            onComplete?.Invoke();
            return false;
        }
    }

    public bool UpdatePurchaseWildOfferStatus(string key, UnityAction onComplete)
    {
        PlayerWildOfferData playerWildOfferData = GetPlayerWildOfferData();
        Dictionary<string, WildOfferData> dataList = playerWildOfferData.GetAllData();
        if (dataList.ContainsKey(key) == false)
        {
            Debug.LogError("SetPurchaseWildOfferStatus Key not found : " + key);
            return false;
        }
        dataList[key].PurchaseCount += 1;
        PlayFabManager.Instance.UpdatePurchaseWildOfferStatus(key);
        return true;
    }

    #endregion

    #region PlayerSave

    private void LoadPlayerSave(UnityAction onComplete, UnityAction<string> onFail)
    {
        Debug.Log("Start LoadPlayerSave ...");

        PlayFabManager.Instance.ExecuteCloudScript(
            "GetPlayerSave"
            , null
            , (result) =>
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    string json = resultProcess.CustomData["player_save"];
                    Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    _playerSave = rawData == null ? new Dictionary<string, object>() : rawData;

                    Debug.Log("<color=green>Completed</color> LoadPlayerSave");
                    onComplete?.Invoke();
                }
                else
                {
                    Debug.LogFormat("<color=red>Failed</color> LoadPlayerSave : {0}", resultProcess.MessageDetail);
                    onFail?.Invoke(resultProcess.MessageDetail);
                }
            }, (error) =>
            {
                Debug.LogFormat("<color=red>Failed</color> LoadPlayerSave : {0}", error.ToString());

                Debug.Log("Retry LoadPlayerSave ...");
                LoadPlayerSave(onComplete, onFail);
            }
        );
    }

    public void SetPlayerSave(string key, object value, UnityAction onComplete = null, UnityAction<string> onFail = null)
    {
        Dictionary<string, object> data = new Dictionary<string, object>();
        data.Add(key, value);
        SetPlayerSave(data, onComplete, onFail);
    }

    public void SetPlayerSave(Dictionary<string, object> data, UnityAction onComplete = null, UnityAction<string> onFail = null)
    {
        if (onComplete == null)
        {
            SetPlayerSaveLocal(data);
        }

        PlayFabManager.Instance.ExecuteCloudScript("SetPlayerSave", new { Data = data },
            (result) =>
            {
                if (onComplete != null)
                {
                    SetPlayerSaveLocal(data);
                    onComplete.Invoke();
                }
            },
            (error) => onFail?.Invoke(error.ToString()));
    }

    private void SetPlayerSaveLocal(Dictionary<string, object> data)
    {
        foreach (KeyValuePair<string, object> item in data)
        {
            if (_playerSave.ContainsKey(item.Key) == false)
                _playerSave.Add(item.Key, null);
            _playerSave[item.Key] = JsonConvert.SerializeObject(item.Value);
        }
    }

    public Dictionary<string, object> GetPlayerSave()
    {
        return _playerSave;
    }

    #endregion

    #region Star Pack


    private void OnLoadStarItemDBComplete(GetTitleDataResult result)
    {
        if (result.Data.ContainsKey(_starItemDBKey))
        {
            _starItemDB = new StarItemDB(result.Data[_starItemDBKey]);
            Debug.Log(JsonConvert.SerializeObject(_starItemDB));
            Debug.Log("OnLoadStarItemDBComplete <color=green>Completed</color>");
        }
        else
        {
            Debug.Log("OnLoadStarItemDBComplete <color=red>Failed</color>");
        }
    }

    public StarItemDB GetStarItemDB()
    {
        return _starItemDB;
    }

    #endregion

    #region Banner
    private List<BannerData> _mainBannerDataList = new List<BannerData>();
    private List<BannerData> _shopBannerDataList = new List<BannerData>();

    private void OnLoadBannerDBComplete(GetTitleDataResult result)
    {
        if (result.Data.ContainsKey(_bannerDBKey))
        {
            List<object> rawDataList = JsonConvert.DeserializeObject<List<object>>(result.Data[_bannerDBKey]);
            for (int i = 0; i < rawDataList.Count; i++)
            {
                _mainBannerDataList.Add(new BannerData(rawDataList[i].ToString()));
            }
        }
        if (result.Data.ContainsKey(_bannerShopDBKey))
        {
            List<object> rawDataList = JsonConvert.DeserializeObject<List<object>>(result.Data[_bannerShopDBKey]);
            for (int i = 0; i < rawDataList.Count; i++)
            {
                _shopBannerDataList.Add(new BannerData(rawDataList[i].ToString()));
            }
        }

        Debug.Log("OnLoadBannerDB <color=green>Completed</color>");
    }

    public List<BannerData> GetMainBannerDataList()
    {
        return _mainBannerDataList;
    }

    public List<BannerData> GetShopBannerDataList()
    {
        return _shopBannerDataList;
    }

    #endregion

    #region CardChangeLog
    private void OnLoadCardChangeLogComplete(GetTitleDataResult result)
    {
        if (result.Data.ContainsKey(_cardChangeLogPlayfabKey))
        {
            _cardChangeLogData = new CardChangeLogData(result.Data[_cardChangeLogPlayfabKey]);
        }

        Debug.Log("OnLoadCardChangeLogComplete <color=green>Completed</color>");
    }

    public CardChangeLogData GetCardChangeLogData()
    {
        if (_cardChangeLogData == null)
        {
            _cardChangeLogData = new CardChangeLogData();
        }

        return _cardChangeLogData;
    }

    #endregion
    //-----------------------------------------------------------------------

    #region Language Methods
    public Language GetLanguage()
    {
        if (PlayerPrefs.HasKey(_playerLanguageKey))
        {
            string languageText = PlayerPrefs.GetString(_playerLanguageKey, Language.English.ToString());

            Language result;
            bool isSuccess = GameHelper.StrToEnum<Language>(languageText, out result);
            if (isSuccess)
            {
                return result;
            }
            else
            {
                return Language.English;
            }
        }
        else
        {
            switch (Application.systemLanguage)
            {
                case SystemLanguage.Thai: return Language.Thai;

                case SystemLanguage.English:
                default:
                    return Language.English;
            }
        }
    }

    public void SetLanguage(Language language)
    {
        PlayerPrefs.SetString(_playerLanguageKey, language.ToString());
    }
    #endregion

    #region FPS Methods
    public int GetFPSSetting()
    {
        return PlayerPrefs.GetInt(_fpsSettingKey, 30);
    }

    public void SetFPSSetting(int fps)
    {
        PlayerPrefs.SetInt(_fpsSettingKey, fps);
    }

    public bool IsSetFPSSetting()
    {
        return PlayerPrefs.HasKey(_fpsSettingKey);
    }
    #endregion

    #region Sound BGM Methods
    public bool GetBGMOn()
    {
        return (PlayerPrefs.GetInt(_bgmOnKey, 1) != 0);
    }

    public void SetBGMOn(bool isOn)
    {
        PlayerPrefs.SetInt(_bgmOnKey, (isOn ? 1 : 0));
    }

    public bool GetSFXOn()
    {
        return (PlayerPrefs.GetInt(_sfxOnKey, 1) != 0);
    }

    public void SetSFXOn(bool isOn)
    {
        PlayerPrefs.SetInt(_sfxOnKey, (isOn ? 1 : 0));
    }

    public bool GetVoiceOn()
    {
        return (PlayerPrefs.GetInt(_voiceOnKey, 1) != 0);
    }

    public void SetVoiceOn(bool isOn)
    {
        PlayerPrefs.SetInt(_voiceOnKey, (isOn ? 1 : 0));
    }
    #endregion

    #region Screen Quality Methods
    public ScreenQuality GetScreenQuality()
    {
        return (ScreenQuality)(PlayerPrefs.GetInt(_gameQualityKey, 1));
    }

    public void SetScreenQuality(ScreenQuality quality)
    {
        PlayerPrefs.SetInt(_gameQualityKey, (int)quality);
    }

    public float GetScreenRatio()
    {
        return PlayerPrefs.GetFloat(_screenRatioKey, _screenRatioDefault);
    }

    public void SetScreenRatio(float ratio)
    {
        PlayerPrefs.SetFloat(_screenRatioKey, ratio);
    }

    public bool IsSetScreenRatioSetting()
    {
        return PlayerPrefs.HasKey(_screenRatioKey);
    }
    #endregion

    #region Debug Log Methods
    public bool GetShowDebugLog()
    {
        if (GameHelper.IsATNVersion_RealDevelopment)
        {
            return (PlayerPrefs.GetInt(_debugLogKey, 1) != 0) ? true : false;
        }
        else
        {
            return false;
        }
    }

    public void SetShowDebugLog(bool isShowDebug)
    {
        PlayerPrefs.SetInt(_debugLogKey, isShowDebug ? 1 : 0);
    }
    #endregion

    #region Room Name Methods
    public string GetRoomName()
    {
        return PlayerPrefs.GetString(_roomNameKey, "");
    }

    public void SetRoomName(string roomName)
    {
        PlayerPrefs.SetString(_roomNameKey, roomName);
    }
    #endregion

    #region Welcome CBT Methods
    public bool GetIsWelcomeCBT()
    {
        return ((PlayerPrefs.GetInt(_welcomeCBTKey, 0) != 0) ? false : true);
    }

    public void SetIsWelcomeCBT(bool isAlreadyWelcome)
    {
        PlayerPrefs.SetInt(_welcomeCBTKey, isAlreadyWelcome ? 1 : 0);
    }
    #endregion

    #region Version Methods
    public void LoadValidVersion(UnityAction onComplete, UnityAction<string> onFail)
    {
        List<string> keyList = new List<string>();
        keyList.Add(_validServerVersionPlayfabKey);
        keyList.Add(_whiteListPlayerPlayfabKey);
        keyList.Add(_validServerSettingKey);

        PlayFabManager.Instance.GetTitleData(
              keyList
            , delegate (GetTitleDataResult result)
            {
                string json = "";
                ClientVersion version = null;
                List<string> whiteList = null;
                Dictionary<RuntimePlatform, Dictionary<string, ATNVersion>> serverSetting = null;

                if (result != null)
                {
                    if (result.Data.ContainsKey(_validServerVersionPlayfabKey))
                    {
                        json = result.Data[_validServerVersionPlayfabKey];
                        version = ClientVersion.JsonToClientVersion(json);
                    }

                    try
                    {
                        if (result.Data.ContainsKey(_whiteListPlayerPlayfabKey))
                        {
                            json = result.Data[_whiteListPlayerPlayfabKey];
                            whiteList = JsonConvert.DeserializeObject<List<string>>(json);

                            SetWhiteList(whiteList);
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.LogFormat("LoadValidVersion: Failed to read white list. {0}", e.Message);
                    }

                    try
                    {
                        if (result.Data.ContainsKey(_validServerSettingKey))
                        {
                            json = result.Data[_validServerSettingKey];

                            serverSetting = JsonConvert.DeserializeObject<Dictionary<RuntimePlatform, Dictionary<string, ATNVersion>>>(json);

                            SetServerSetting(serverSetting);
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.LogFormat("LoadValidVersion: Failed to read server setting. {0}", e.Message);
                    }
                }

                if (version != null)
                {
                    SetValidVersion(version);

                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                }
                else
                {
                    if (onFail != null)
                    {
                        onFail.Invoke("LoadValidVersion: Failed to parse version json. " + json);
                    }
                }
            }
            , delegate (PlayFab.PlayFabError error)
            {
                if (onFail != null)
                {
                    onFail.Invoke(error.Error.ToString());
                }
            }
        );
    }

    public ClientVersion GetValidVersion()
    {
        return _version;
    }

    private void SetValidVersion(ClientVersion version)
    {
        _version = new ClientVersion(version);
    }

    private void SetWhiteList(List<string> whiteList)
    {
        if (whiteList != null && whiteList.Count > 0)
        {
            _whiteList = new List<string>(whiteList);
        }
        else
        {
            _whiteList = new List<string>();
        }
    }

    public ATNVersion GetServerSetting()
    {
        try
        {
            if (_serverSetting != null)
            {
                string currentVersion = GameHelper.GetVersionText();

                RuntimePlatform runtimePlatform;
#if UNITY_ANDROID
                runtimePlatform = RuntimePlatform.Android;
#elif UNITY_IOS
                runtimePlatform = RuntimePlatform.IPhonePlayer;
#else
                return GameHelper.ATNVersion;
#endif
                if (_serverSetting.ContainsKey(runtimePlatform))
                {
                    if (_serverSetting[runtimePlatform].ContainsKey(currentVersion))
                    {
                        return _serverSetting[runtimePlatform][currentVersion];
                    }
                }
            }

            return GameHelper.ATNVersion;

        }
        catch (Exception e)
        {
            return GameHelper.ATNVersion;
        }
    }

    private void SetServerSetting(Dictionary<RuntimePlatform, Dictionary<string, ATNVersion>> serverSetting)
    {
        _serverSetting = new Dictionary<RuntimePlatform, Dictionary<string, ATNVersion>>(serverSetting);
    }

    public void CheckIsValidUserAndVersion(UnityAction onComplete, UnityAction<string> onFail)
    {
        TaskLoader<string> taskLoader = new TaskLoader<string>();
        taskLoader.JoinTask(CheckSessionValid);
        taskLoader.JoinTask(CheckVersionValid);
        taskLoader.StartTasks(
              onComplete
            , delegate (List<string> failList)
            {
                if (onFail != null)
                {
                    string failText = "";
                    foreach (string errorKey in failList)
                    {
                        failText += "|" + errorKey;
                    }
                    onFail.Invoke(failText);
                }
            }
        );
    }

    public void CheckSessionValid(UnityAction onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.CheckSessionValid(
            delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    onComplete?.Invoke();
                }
                else
                {
                    onFail?.Invoke(resultProcess.MessageDetail);
                }
            }
            , delegate (PlayFabError error)
            {
                onFail?.Invoke(error.Error.ToString());
            }
        );

    }

    public IEnumerator IECheckSessionValid(UnityAction<bool> onComplete)
    {
        bool isFinish = false;
        CheckSessionValid(
            delegate ()
            {
                isFinish = true;
                onComplete?.Invoke(true);
            },
            delegate (string error)
            {
                isFinish = true;
                onComplete?.Invoke(false);
            }
        );
        yield return new WaitUntil(() => isFinish == true);
    }

    public IEnumerator IECheckVersionValid(UnityAction<bool, string> onComplete)
    {
        bool isFinish = false;
        CheckVersionValid(
            delegate ()
            {
                isFinish = true;
                onComplete?.Invoke(true, "");
            },
            delegate (string error)
            {
                isFinish = true;
                onComplete?.Invoke(false, error);
            }
        );
        yield return new WaitUntil(() => isFinish == true);
    }

    public IEnumerator IECheckIsValidUserAndVersion(UnityAction<bool, string> onComplete)
    {
        bool isFinish = false;
        CheckIsValidUserAndVersion(
            delegate ()
            {
                isFinish = true;
                onComplete?.Invoke(true, "");
            },
            delegate (string error)
            {
                isFinish = true;
                onComplete?.Invoke(false, error);
            }
        );
        yield return new WaitUntil(() => isFinish == true);
    }

    public void CheckVersionValid(UnityAction onComplete, UnityAction<string> onFail)
    {
        LoadValidVersion(delegate ()
        {
            if (IsVersionValid() || IsWhiteList())
            {
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            }
            else
            {
                if (onFail != null)
                {
                    onFail.Invoke("VERSION_INVALID");
                }
            }
        }
        , delegate (string error)
        {
            if (onFail != null)
            {
                onFail.Invoke(error);
            }
        });
    }

    public ClientStatus GetVersionStatus()
    {
        if (_version != null)
        {
            string currentVersion = GameHelper.GetVersionText();
            if (
                   (GameHelper.ATNVersion == ATNVersion.Development)
            //|| (GameHelper.ATNVersion == ATNVersion.IGA19)
            //|| (GameHelper.ATNVersion == ATNVersion.Tester)
            )
            {
                return ClientStatus.Open;
            }
            else
            {
                RuntimePlatform runtimePlatform;

#if UNITY_ANDROID
                runtimePlatform = RuntimePlatform.Android;
#elif UNITY_IOS
                runtimePlatform = RuntimePlatform.IPhonePlayer;
#else
                return ClientStatus.Close;
#endif

                VersionData version;
                if (_version.GetVersion(runtimePlatform, currentVersion, out version))
                {
                    return version.Status;
                }
            }
        }

        return ClientStatus.Close;
    }

    public bool IsVersionValid()
    {
        return (GetVersionStatus() == ClientStatus.Open);
    }

    public bool IsWhiteList()
    {
        return _whiteList.Contains(PlayerInfo.PlayerID);
    }
    #endregion

    #region Asset Bundle Methods
    public void GetAssetBundlePath(List<string> assetKeys, ATNVersion atnVersion, RuntimePlatform platform, string buildDate, UnityAction<Dictionary<string, string>> onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.GetAssetBundlePath(
              assetKeys
            , atnVersion
            , platform
            , buildDate
            , delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    string json = resultProcess.CustomData["file_path"];
                    Dictionary<string, object> versionData = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);

                    Dictionary<string, string> paths = JsonConvert.DeserializeObject<Dictionary<string, string>>(versionData["file_path_list"].ToString());

                    onComplete?.Invoke(paths);
                }
                else
                {
                    onFail?.Invoke(LocalizationManager.Instance.GetText(resultProcess.MessageDetail));
                }
            }
        );
    }

    public void GetStarterAssetBundlePath(ATNVersion atnVersion, RuntimePlatform platform, string buildDate, UnityAction<Dictionary<string, string>> onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.GetStarterAssetBundlePath(
              atnVersion
            , platform
            , buildDate
            , delegate (ExecuteCloudScriptResult result)
            {
                CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                if (resultProcess.IsSuccess)
                {
                    string json = resultProcess.CustomData["file_path"];
                    Dictionary<string, object> versionData = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);

                    Dictionary<string, string> paths = JsonConvert.DeserializeObject<Dictionary<string, string>>(versionData["file_path_list"].ToString());

                    onComplete?.Invoke(paths);
                }
                else
                {
                    onFail?.Invoke(LocalizationManager.Instance.GetText(resultProcess.MessageDetail));
                }
            }
        );
    }
    #endregion

    #region Event Battle Methods
    private void OnLoadEventBattleDBComplete(GetTitleDataResult result)
    {
        if (result.Data.ContainsKey(_titleEventBattleDBPlayfabKey))
        {
            string json = result.Data[_titleEventBattleDBPlayfabKey];
            EventBattleDB.Instance.LoadFromJSON(json);
        }
    }

    public DeckData GetEventBattleDeck(string profileID)
    {
        return DeckData.GetEventBattleDeck(profileID);
    }

    public DeckData GetRandomEventBattleDeck()
    {
        return DeckData.GetRandomEventBattleDeck();
    }
    #endregion

    #region Permission Methods
    public bool IsHasStoragePermission()
    {
#if PLATFORM_ANDROID
        return Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite);
#else
        return true;
#endif
    }

    public void RequestStoragePermission()
    {
        if (!IsHasStoragePermission())
        {
#if UNITY_ANDROID
            Permission.RequestUserPermission(Permission.ExternalStorageWrite);
#endif
        }
    }
    #endregion

    #region Server Time Methods
    public void GetRealServerTime(UnityAction<DateTime> onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.GetRealServerTime(
            delegate (GetTimeResult result)
            {
                onComplete?.Invoke(result.Time);
            }
            , delegate (PlayFabError error)
            {
                onFail?.Invoke(error.ErrorMessage);
            }
        );
    }
    #endregion

    #region Specific Method
    public void ExecuteSpecificCloudFunction(UnityAction onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.ExecuteSpecificCloudFunction(
            delegate (ExecuteCloudScriptResult result)
            {
                onComplete?.Invoke();
            }
            , delegate (PlayFabError error)
            {
                ExecuteSpecificCloudFunction(onComplete, onFail);
            }
        );
    }
    #endregion

    #endregion

    #region Event Trigger
    public void AddListenerOnVIPUpdate(UnityAction onVIPUpdate)
    {
        if (_onVIPStatusUpdate == null)
        {
            _onVIPStatusUpdate = new UnityEvent();
        }

        _onVIPStatusUpdate.AddListener(onVIPUpdate);
    }

    public void RemoveListenerOnVIPUpdate(UnityAction onVIPUpdate)
    {
        if (_onVIPStatusUpdate != null)
        {
            _onVIPStatusUpdate.RemoveListener(onVIPUpdate);
        }
    }

    public void RemoveAllListenersOnVIPUpdate()
    {
        if (_onVIPStatusUpdate != null)
        {
            _onVIPStatusUpdate.RemoveAllListeners();
        }
    }

    public void OnVIPStatusUpdate()
    {
        if (_onVIPStatusUpdate != null)
        {
            _onVIPStatusUpdate.Invoke();
        }
    }

    public void SetLastGameMode(GameMode mode)
    {
        LastGameMode = mode;
    }
    #endregion
}