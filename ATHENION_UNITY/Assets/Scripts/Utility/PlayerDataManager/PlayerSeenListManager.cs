﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

public class PlayerSeenListManager : MonoSingleton<PlayerSeenListManager>
{
    #region Private Properties
    private const string _playerSeenCardPlayfabKey = "seen_list_card";
    private const string _playerSeenAvatarPlayfabKey = "seen_list_avatar";
    private const string _playerSeenTitlePlayfabKey = "seen_list_title";
    private const string _playerSeenHeroPlayfabKey = "seen_list_hero";
    private const string _playerSeenCardBackPlayfabKey = "seen_list_card_back";
    private const string _playerSeenDockPlayfabKey = "seen_list_dock";
    private const string _playerSeenSeasonPassQuestPlayfabKey = "seen_list_sp_quest";
    private const string _playerSeenPromotionPlayfabKey = "seen_list_promotion";
    private const string _playerSeenEventPlayfabKey = "seen_list_event";

    private PlayerSeenListData _data = new PlayerSeenListData();
    private PlayerSeenListData _tempData = new PlayerSeenListData();
    private bool _isLoaded = false;
    #endregion

    #region Methods
    public void SetData(Dictionary<string, string> rawData)
    {
        if (rawData.ContainsKey(_playerSeenCardPlayfabKey))
        {
            List<string> cardList = JsonConvert.DeserializeObject<List<string>>(rawData[_playerSeenCardPlayfabKey]);
            _data.SetSeenItemList(SeenItemTypes.Card, cardList);
        }

        if (rawData.ContainsKey(_playerSeenAvatarPlayfabKey))
        {
            List<string> avatarList = JsonConvert.DeserializeObject<List<string>>(rawData[_playerSeenAvatarPlayfabKey]);
            _data.SetSeenItemList(SeenItemTypes.Avatar, avatarList);
        }

        if (rawData.ContainsKey(_playerSeenTitlePlayfabKey))
        {
            List<string> titleList = JsonConvert.DeserializeObject<List<string>>(rawData[_playerSeenTitlePlayfabKey]);
            _data.SetSeenItemList(SeenItemTypes.Title, titleList);
        }

        if (rawData.ContainsKey(_playerSeenHeroPlayfabKey))
        {
            List<string> heroList = JsonConvert.DeserializeObject<List<string>>(rawData[_playerSeenHeroPlayfabKey]);
            _data.SetSeenItemList(SeenItemTypes.Hero, heroList);
        }

        if (rawData.ContainsKey(_playerSeenCardBackPlayfabKey))
        {
            List<string> cardBackList = JsonConvert.DeserializeObject<List<string>>(rawData[_playerSeenCardBackPlayfabKey]);
            _data.SetSeenItemList(SeenItemTypes.CardBack, cardBackList);
        }

        if (rawData.ContainsKey(_playerSeenDockPlayfabKey))
        {
            List<string> dockList = JsonConvert.DeserializeObject<List<string>>(rawData[_playerSeenDockPlayfabKey]);
            _data.SetSeenItemList(SeenItemTypes.Dock, dockList);
        }

        if (rawData.ContainsKey(_playerSeenSeasonPassQuestPlayfabKey))
        {
            List<string> seasonPassQuestList = JsonConvert.DeserializeObject<List<string>>(rawData[_playerSeenSeasonPassQuestPlayfabKey]);
            _data.SetSeenItemList(SeenItemTypes.SeasonPassQuest, seasonPassQuestList);

            // new season pass
            if (DataManager.Instance.PlayerSeasonPassOld.SeasonPassIndex < DataManager.Instance.PlayerSeasonPass.SeasonPassIndex)
            {
                _data.SetSeenItemList(SeenItemTypes.SeasonPassQuest, new List<string>());
                DataManager.Instance.UpdatePlayerSeenData();
            }
        }

        if (rawData.ContainsKey(_playerSeenPromotionPlayfabKey))
        {
            List<string> promotionSeenList = JsonConvert.DeserializeObject<List<string>>(rawData[_playerSeenPromotionPlayfabKey]);
            _data.SetSeenItemList(SeenItemTypes.Promotion, promotionSeenList);
        }

        if (rawData.ContainsKey(_playerSeenEventPlayfabKey))
        {
            List<string> eventKeyList = JsonConvert.DeserializeObject<List<string>>(rawData[_playerSeenEventPlayfabKey]);
            _data.SetSeenItemList(SeenItemTypes.Event, eventKeyList);
        }

        _tempData = new PlayerSeenListData(_data);
        _isLoaded = true;
    }

    public void SyncUpdateSeenListData()
    {
        _data = new PlayerSeenListData(_tempData);
    }

    public void AddSeenListData(SeenItemTypes type, bool isImmediatelyUpdate, params string[] items)
    {
        _tempData.AddSeenItemList(type, items);

        if (isImmediatelyUpdate)
        {
            _data.AddSeenItemList(type, items);
        }
    }

    public void SetSeenListData(SeenItemTypes type, bool isImmediatelyUpdate, List<string> items)
    {
        _tempData.SetSeenItemList(type, items);

        if (isImmediatelyUpdate)
        {
            _data.SetSeenItemList(type, items);
        }
    }

    public Dictionary<string, string> GetUpdatedPlayerSeenData()
    {
        if (_isLoaded == false)
        {
            return new Dictionary<string, string>();
        }

        Dictionary<string, string> updatedData = new Dictionary<string, string>();
        updatedData.Add(_playerSeenCardPlayfabKey, JsonConvert.SerializeObject(_data.GetSeenItemList(SeenItemTypes.Card)));
        updatedData.Add(_playerSeenSeasonPassQuestPlayfabKey, JsonConvert.SerializeObject(_data.GetSeenItemList(SeenItemTypes.SeasonPassQuest)));
        updatedData.Add(_playerSeenPromotionPlayfabKey, JsonConvert.SerializeObject(_data.GetSeenItemList(SeenItemTypes.Promotion)));
        updatedData.Add(_playerSeenEventPlayfabKey, JsonConvert.SerializeObject(_data.GetSeenItemList(SeenItemTypes.Event)));

        return updatedData;
    }

    public bool IsSeenEvent()
    {
        try
        {
            List<string> seenList = _data.GetSeenItemList(SeenItemTypes.Event);
            List<string> gameEventInfoKeyList = DataManager.Instance.GetGameEventDB().GetValidGameEventInfoKeyList();

            if (gameEventInfoKeyList.Count != seenList.Count)
                return false;

            for (int i = 0; i < seenList.Count; i++)
            {
                string eventKey = seenList[i];
                if (gameEventInfoKeyList.Contains(eventKey) == false)
                {
                    return false;
                }
            }
            return true;
        }
        catch (Exception e)
        {
            return true;
        }
    }

    public bool IsSeenPromotion()
    {
        try
        {
            ShopTypeData shopTypeData = DataManager.Instance.GetShopTypeData(ShopTypes.Promotion);
            ShopData ShopData = shopTypeData.ShopDict.First().Value;
            List<string> seenList = _data.GetSeenItemList(SeenItemTypes.Promotion);

            //if (ShopData.ShopItemDataList.Count != seenList.Count)
            //    return false;
            List<string> validPromotionItemID = ShopData.GetAllShopItemIDList((item) => item.IsTimeValid == true);
            for (int i = 0; i < validPromotionItemID.Count; i++)
            {
                string itemID = validPromotionItemID[i];
                if (seenList.Contains(itemID) == false)
                {
                    return false;
                }
            }
            return true;
        }
        catch (Exception e)
        {
            return true;
        }
    }
    #endregion

    #region Get Methods
    public bool IsNewCard(string cardFullID)
    {
        PlayerInventoryData inventoryData = DataManager.Instance.InventoryData;
        List<string> seenList = _data.GetSeenItemList(SeenItemTypes.Card);

        if (seenList.Contains(cardFullID))
        {
            return false;
        }
        else
        {
            if (!inventoryData.CardInventoryList.ContainKey(cardFullID))
            {
                return false;
            }
            else
            {
                CardData data;
                if (CardData.GetCardData(cardFullID, out data))
                {
                    if (data.SeriesKey == "Basic")
                    {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    public bool IsNewAvatar(string avatarID)
    {
        PlayerInventoryData inventoryData = DataManager.Instance.InventoryData;
        List<string> seenList = _data.GetSeenItemList(SeenItemTypes.Avatar);

        if (seenList.Contains(avatarID))
        {
            return false;
        }
        else
        {
            if (!inventoryData.AvatarIDList.Contains(avatarID))
            {
                return false;
            }
        }

        return true;
    }

    public bool IsHasNewAvatar()
    {
        PlayerInventoryData inventoryData = DataManager.Instance.InventoryData;
        List<string> seenList = _data.GetSeenItemList(SeenItemTypes.Avatar);

        foreach (string item in inventoryData.AvatarIDList)
        {
            if (seenList.Contains(item))
            {
                return false;
            }
        }

        return true;
    }

    public bool IsNewTitle(string titleID)
    {
        PlayerInventoryData inventoryData = DataManager.Instance.InventoryData;
        List<string> seenList = _data.GetSeenItemList(SeenItemTypes.Title);

        if (seenList.Contains(titleID))
        {
            return false;
        }
        else
        {
            if (!inventoryData.TitleIDList.Contains(titleID))
            {
                return false;
            }
        }

        return true;
    }

    public bool IsHasNewTitle()
    {
        PlayerInventoryData inventoryData = DataManager.Instance.InventoryData;
        List<string> seenList = _data.GetSeenItemList(SeenItemTypes.Title);

        foreach (string item in inventoryData.TitleIDList)
        {
            if (seenList.Contains(item))
            {
                return false;
            }
        }

        return true;
    }

    public bool IsNewHero(string heroID)
    {
        PlayerInventoryData inventoryData = DataManager.Instance.InventoryData;
        List<string> seenList = _data.GetSeenItemList(SeenItemTypes.Hero);

        if (seenList.Contains(heroID))
        {
            return false;
        }
        else
        {
            if (!inventoryData.HeroIDList.Contains(heroID))
            {
                return false;
            }
        }

        return true;
    }

    public bool IsHasNewHero()
    {
        PlayerInventoryData inventoryData = DataManager.Instance.InventoryData;
        List<string> seenList = _data.GetSeenItemList(SeenItemTypes.Hero);

        foreach (string item in inventoryData.HeroIDList)
        {
            if (seenList.Contains(item))
            {
                return false;
            }
        }

        return true;
    }

    public bool IsNewCardBack(string cardBackID)
    {
        PlayerInventoryData inventoryData = DataManager.Instance.InventoryData;
        List<string> seenList = _data.GetSeenItemList(SeenItemTypes.CardBack);

        if (seenList.Contains(cardBackID))
        {
            return false;
        }
        else
        {
            if (!inventoryData.CardBackIDList.Contains(cardBackID))
            {
                return false;
            }
        }

        return true;
    }

    public bool IsHasNewCardBack()
    {
        PlayerInventoryData inventoryData = DataManager.Instance.InventoryData;
        List<string> seenList = _data.GetSeenItemList(SeenItemTypes.CardBack);

        foreach (string item in inventoryData.CardBackIDList)
        {
            if (seenList.Contains(item))
            {
                return false;
            }
        }

        return true;
    }

    public bool IsNewDock(string dockID)
    {
        PlayerInventoryData inventoryData = DataManager.Instance.InventoryData;
        List<string> seenList = _data.GetSeenItemList(SeenItemTypes.Dock);

        if (seenList.Contains(dockID))
        {
            return false;
        }
        else
        {
            if (!inventoryData.DockIDList.Contains(dockID))
            {
                return false;
            }
        }

        return true;
    }

    public bool IsHasNewDock()
    {
        PlayerInventoryData inventoryData = DataManager.Instance.InventoryData;
        List<string> seenList = _data.GetSeenItemList(SeenItemTypes.Dock);

        foreach (string item in inventoryData.DockIDList)
        {
            if (seenList.Contains(item))
            {
                return false;
            }
        }

        return true;
    }

    public bool IsNewSeasonPassQuest(string questID)
    {
        return _data.GetSeenItemList(SeenItemTypes.SeasonPassQuest).Contains(questID) == false;
    }
    #endregion
}