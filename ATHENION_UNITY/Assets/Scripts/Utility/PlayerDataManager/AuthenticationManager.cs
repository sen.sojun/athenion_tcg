﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using PlayFab;
using PlayFab.ClientModels;
using Facebook.Unity;

#if UNITY_ANDROID
using GooglePlayGames;
using GooglePlayGames.BasicApi;
#endif

public class AuthenticationManager
{
    #region Public Properties
    public static PlayFab.ClientModels.LoginResult AuthResult { get; private set; }
    #endregion

    #region Private Properties
    private static readonly string _authSetKey = "AuthSetKey";
    private static readonly string _authIDKey = "AuthIDKey";
    private static readonly string _authTypeKey = "AuthTypeKey";

    private static UnityAction _onComplete;
    private static UnityAction<string> _onFail;
    #endregion

    #region Methods
    public static bool IsAutoAuth()
    {
        return PlayerPrefs.GetInt(_authSetKey, 0) == 0 ? false : true;
    }

    public static void SetAutoAuth(int isAuto)
    {
        PlayerPrefs.SetInt(_authSetKey, isAuto);
    }

    public static string GetAuthID()
    {
        return PlayerPrefs.GetString(_authIDKey, Guid.NewGuid().ToString());
    }

    private static void SetAuthID(string authID)
    {
        PlayerPrefs.SetString(_authIDKey, authID);
    }

    public static string GetAuthTypeString()
    {
        return PlayerPrefs.GetString(
               _authTypeKey,
               AuthTypes.None.ToString());
    }

    public static AuthTypes GetAuthType()
    {
        AuthTypes type = AuthTypes.None;

        string result = PlayerPrefs.GetString(_authTypeKey, AuthTypes.None.ToString());
        GameHelper.StrToEnum(result, out type);

        return type;
    }

    public static bool IsBeGuest()
    {
        AuthTypes type = GetAuthType();
        return type == AuthTypes.None || type == AuthTypes.Guest;
    }

    private static void SetAuthType(string authType)
    {
        PlayerPrefs.SetString(_authTypeKey, authType);
    }

    #region Authentication Auto
    public static void AuthenticateAutoAuth(UnityAction onComplete, UnityAction<string> onFail)
    {
        _onComplete = onComplete;
        _onFail = onFail;

        PlayFabManager.Instance.AuthenticateCustomID(
            GetAuthID()
            , delegate (PlayFab.ClientModels.LoginResult result)
            {
                AuthResult = result;
                OnAuthenticationComplete(GetAuthType(), result, _onComplete, _onFail);
            }
            , delegate (PlayFabError error)
            {
                if (onFail != null)
                {
                    onFail.Invoke(error.Error.ToString());
                }
            }
        );
    }
    #endregion

    #region Authentication Guest
    public static void AuthenticateAsGuest(UnityAction onComplete, UnityAction<string> onFail)
    {
        _onComplete = onComplete;
        _onFail = onFail;

        PlayFabManager.Instance.AuthenticateAsGuest(
            delegate (PlayFab.ClientModels.LoginResult result)
            {
                AuthResult = result;
                OnAuthenticationComplete(AuthTypes.Guest, result, _onComplete, _onFail);
            }
            , delegate (PlayFabError error)
            {
                if (onFail != null)
                {
                    onFail.Invoke(error.Error.ToString());
                }
            }
        );
    }

    private static void RememberMeGuest()
    {
        SetAuthType(AuthTypes.Guest.ToString());
        SetAutoAuth(1);
    }
    #endregion

    #region Authentication EmailPassword
    public static void AuthenticateEmailPassword(string email, string password, UnityAction onComplete, UnityAction<string> onFail)
    {
        _onComplete = onComplete;
        _onFail = onFail;

        PlayFabManager.Instance.AuthenticateEmailPassword(
            email
            , password
            , delegate (PlayFab.ClientModels.LoginResult result)
            {
                AuthResult = result;
                OnAuthenticationComplete(AuthTypes.EmailAndPassword, result, _onComplete, _onFail);
            }
            , delegate (PlayFabError error)
            {
                if (onFail != null)
                {
                    onFail.Invoke(error.Error.ToString());
                }
            }
        );
    }

    public static void RegisterEmailPassword(string email, string password, UnityAction onComplete, UnityAction<string> onFail)
    {
        _onComplete = onComplete;
        _onFail = onFail;

        PlayFabManager.Instance.AddEmailAndPassword(
            email
            , password
            , delegate (PlayFab.ClientModels.LoginResult result)
            {
                AuthResult = result;
                OnAuthenticationComplete(AuthTypes.EmailAndPassword, result, _onComplete, _onFail);
            }
            , delegate (PlayFabError error)
            {
                if (onFail != null)
                {
                    onFail.Invoke(error.Error.ToString());
                }
            }
        );
    }
    #endregion

    #region Authenticate Facebook
    private static List<string> _facebookPermission = new List<string>() { "public_profile", "email" };

    private static void FacebookLogin(FacebookDelegate<ILoginResult> resultDelegete)
    {
        FB.LogOut();
        FB.LogInWithReadPermissions(
            _facebookPermission
            , resultDelegete
        );
    }

    public static void AuthenticateFacebook(UnityAction onComplete, UnityAction<string> onFail)
    {
        _onComplete = onComplete;
        _onFail = onFail;

        if (FB.IsInitialized)
        {
            FacebookLogin(OnHandleFBResult);
        }
        else
        {
            FB.Init(OnFBInitComplete, OnFBHideUnity);
        }
    }

    private static void OnHandleFBResult(ILoginResult loginResult)
    {
        if (!loginResult.Cancelled && loginResult.Error == null)
        {
            PlayFabManager.Instance.AuthenticateFacebook(
                loginResult.AccessToken.TokenString
                , delegate (PlayFab.ClientModels.LoginResult result)
                {
                    AuthResult = result;
                    OnFacebookLoginComplete();
                }
                , delegate (PlayFabError error)
                {
                    if (_onFail != null)
                    {
                        _onFail.Invoke(error.Error.ToString());
                    }
                }
            );
        }
        else
        {
            if (_onFail != null)
            {
                _onFail.Invoke("AUTH_CANCEL");
            }
        }
    }

    private static void OnFBInitComplete()
    {
        FacebookLogin(OnHandleFBResult);
    }

    private static void OnFBHideUnity(bool isUnityShown)
    {
        if (_onFail != null)
        {
            _onFail.Invoke("AUTH_CANCEL");
        }
    }

    private static void OnFacebookLoginComplete()
    {
        FB.API("/me?fields=id,name,email,picture", HttpMethod.GET, GetFacebookInfo, new Dictionary<string, string>() { });
    }

    private static void GetFacebookInfo(IGraphResult result)
    {
        if(result.ResultDictionary.ContainsKey("email") == false)
        {
            //_onFail?.Invoke("FACEBOOK_PERMISSION_NOT_GRANTED");
            OnAuthenticationComplete(AuthTypes.Facebook, AuthResult, _onComplete, _onFail);
            return;
        }

        DataManager.Instance.UpdateContactEmail(
            result.ResultDictionary["email"].ToString()
             , delegate ()
             {
                 OnAuthenticationComplete(AuthTypes.Facebook, AuthResult, _onComplete, _onFail);
             }
             , _onFail
         );
    }
    #endregion

    #region Authentication GooglePlayGames
    public static void AuthenticateGooglePlayGames(UnityAction onComplete, UnityAction<string> onFail)
    {
        #if UNITY_ANDROID
        _onComplete = delegate ()
        {
            DataManager.Instance.UpdateContactEmail(
                PlayGamesPlatform.Instance.GetUserEmail()
                , delegate ()
                {
                    onComplete?.Invoke();
                }
                , onFail
            );
        };
        _onFail = onFail;

        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
        .AddOauthScope("profile")
        .RequestEmail()
        .RequestServerAuthCode(false)
        .Build();

        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.Activate();
        string serverAuthCode = string.Empty;

        Social.Active.localUser.Authenticate(delegate (bool isSocialSuccess)
        {
            if (isSocialSuccess)
            {
                serverAuthCode = PlayGamesPlatform.Instance.GetServerAuthCode();
                if (!string.IsNullOrEmpty(serverAuthCode))
                {
                    PlayFabManager.Instance.AuthenticateGooglePlayGames(
                        serverAuthCode
                        , delegate (PlayFab.ClientModels.LoginResult result)
                        {
                            AuthResult = result;
                            OnAuthenticationComplete(AuthTypes.GooglePlayGames, result, _onComplete, _onFail);
                        }
                        , delegate (PlayFabError error)
                        {
                            if (_onFail != null)
                            {
                                _onFail.Invoke(error.Error.ToString());
                            }
                        }
                    );
                }
                else
                {
                    if (_onFail != null)
                    {
                        _onFail.Invoke("GOOGLE_400");
                    }
                }
            }
            else
            {
                if (_onFail != null)
                {
                    _onFail.Invoke("GOOGLE_LOGIN");
                }
            }
        });
#endif
    }
    #endregion

    #region Remember Me
    private static void RememberMeLogin(UnityAction onComplete, UnityAction<string> onFail)
    {
        SetAuthID(Guid.NewGuid().ToString());
        PlayFabManager.Instance.LinkCustomID(
            GetAuthID()
            , true
            , delegate (LinkCustomIDResult result)
            {
                SetAutoAuth(1);
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            }
            , delegate (PlayFabError error)
            {
                if (onFail != null)
                {
                    onFail.Invoke(error.Error.ToString());
                }
            }
        );
    }

    public static void ClearRememberMe(UnityAction onComplete, UnityAction<string> onFail)
    {
        SetAutoAuth(0);
        AuthTypes type = GetAuthType();
        switch (type)
        {
            case AuthTypes.Guest:
                {
                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                }
                break;

            default:
                {
                    PlayFabManager.Instance.UnlinkCustomID(
                        GetAuthID()
                        , delegate (UnlinkCustomIDResult result)
                        {
                            if (onComplete != null)
                            {
                                onComplete.Invoke();
                            }
                        }
                        , delegate (PlayFabError error)
                        {
                            if (onFail != null)
                            {
                                onFail.Invoke(error.Error.ToString());
                            }
                        }
                    );
                }
                break;
        }

    }
    #endregion

    #region Recovery Email
    public static void SendRecoveryEmail(string email, UnityAction onComplete, UnityAction<string> onFail)
    {
        PlayFabManager.Instance.SendRecoveryEmail(
            email
            , delegate (SendAccountRecoveryEmailResult result)
            {
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            }
            , delegate (PlayFabError error)
            {
                if (onFail != null)
                {
                    onFail.Invoke(error.Error.ToString());
                }
            }
        );
    }
    #endregion

    #region Link Account

    #region Link Facebook
    public static void LinkAccountFacebook(UnityAction onComplete, UnityAction<string> onFail)
    {
        _onComplete = onComplete;
        _onFail = onFail;

        if (FB.IsInitialized)
        {
            FacebookLogin(OnHandleFBLinkResult);
        }
        else
        {
            FB.Init(OnFBLinkInitComplete, OnFBLinkHideUnity);
        }
    }

    private static void OnFBLinkInitComplete()
    {
        FacebookLogin(OnHandleFBLinkResult);
    }

    private static void OnFBLinkHideUnity(bool isUnityShown)
    {
        if (_onFail != null)
        {
            _onFail.Invoke("AUTH_CANCEL");
        }
    }

    private static void OnHandleFBLinkResult(ILoginResult loginResult)
    {
        if (!loginResult.Cancelled && loginResult.Error == null)
        {
            PlayFabManager.Instance.LinkWithFacebookAccount(
                loginResult.AccessToken.TokenString
                , delegate (LinkFacebookAccountResult result)
                {
                    SetupAuthenticationData(
                        AuthTypes.Facebook
                        , OnFacebookLinkComplete
                        , _onFail
                    );
                }
                , delegate (PlayFabError error)
                {
                    if (_onFail != null)
                    {
                        _onFail.Invoke(error.Error.ToString());
                    }
                }
            );
        }
        else
        {
            if (_onFail != null)
            {
                _onFail.Invoke("AUTH_CANCEL");
            }
        }
    }

    private static void OnFacebookLinkComplete()
    {
        FB.API("/me?fields=id,name,email,picture", HttpMethod.GET, GetFacebookLinkInfo, new Dictionary<string, string>() { });
    }

    private static void GetFacebookLinkInfo(IGraphResult result)
    {
        if (result.ResultDictionary.ContainsKey("email") == false)
        {
            //_onFail?.Invoke("FACEBOOK_PERMISSION_NOT_GRANTED");
            SetupAuthenticationData(AuthTypes.Facebook, _onComplete, _onFail);
            return;
        }

        DataManager.Instance.UpdateContactEmail(
            result.ResultDictionary["email"].ToString()
             , delegate ()
             {
                 SetupAuthenticationData(AuthTypes.Facebook, _onComplete, _onFail);
             }
             , _onFail
         );
    }
    #endregion

    #region Link GooglePlayGames
    public static void LinkAccountGooglePlayGames(UnityAction onComplete, UnityAction<string> onFail)
    {
        #if UNITY_ANDROID
        _onComplete = delegate ()
        {
            DataManager.Instance.UpdateContactEmail(
                PlayGamesPlatform.Instance.GetUserEmail()
                , delegate ()
                {
                    onComplete?.Invoke();
                }
                , onFail
            );
        };
        _onFail = onFail;

        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
        .AddOauthScope("profile")
        .RequestEmail()
        .RequestServerAuthCode(false)
        .Build();

        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.Activate();
        string serverAuthCode = string.Empty;

        Social.Active.localUser.Authenticate(delegate (bool isSocialSuccess)
        {
            if (isSocialSuccess)
            {
                serverAuthCode = PlayGamesPlatform.Instance.GetServerAuthCode();
                if (!string.IsNullOrEmpty(serverAuthCode))
                {
                    PlayFabManager.Instance.LinkWithGoogleAccount(
                        serverAuthCode
                        , delegate (LinkGoogleAccountResult result)
                        {
                            SetupAuthenticationData(AuthTypes.GooglePlayGames, _onComplete, _onFail);
                        }
                        , delegate (PlayFabError error)
                        {
                            if (_onFail != null)
                            {
                                _onFail.Invoke(error.Error.ToString());
                            }
                        }
                    );
                }
                else
                {
                    if (_onFail != null)
                    {
                        _onFail.Invoke("GOOGLE_SERVER_CODE");
                    }
                }
            }
            else
            {
                if (_onFail != null)
                {
                    _onFail.Invoke("GOOGLE_LINK");
                }
            }
        });
        #endif
    }
    #endregion

    #endregion

    private static void OnAuthenticationComplete(AuthTypes authType, PlayFab.ClientModels.LoginResult result, UnityAction onComplete, UnityAction<string> onFail)
    {
        AuthResult = result;
        DataManager.Instance.SetPlayerID(result.PlayFabId);
        switch (authType)
        {
            case AuthTypes.Guest:
            {
                AppsFlyerObject.AddLoginEvent(DataManager.Instance.PlayerInfo.PlayerID);

                TaskLoader<string> taskLoader = new TaskLoader<string>();
                RememberMeGuest();
                DataManager.Instance.UpdatePlayerDisplayName(
                    string.Format("ATN_{0}", result.PlayFabId)
                    , delegate()
                    {
                        DataManager.Instance.LoadAllPlayerInfoData(onComplete, onFail);
                    }
                    , onFail
                );
            }
            break;

            case AuthTypes.EmailAndPassword:
            case AuthTypes.Facebook:
            case AuthTypes.GooglePlayGames:
            {
                AppsFlyerObject.AddLoginEvent(DataManager.Instance.PlayerInfo.PlayerID);

                SetupAuthenticationData(authType, onComplete, onFail);
            }
            break;

            default:
            {
                if (onFail != null)
                {
                    onFail.Invoke("ERROR_AUTHETICATION_FAILED");
                }
            }
            break;
        }
    }

    private static void SetupAuthenticationData(AuthTypes authType, UnityAction onComplete, UnityAction<string> onFail)
    {
        TaskLoader<string> taskLoader = new TaskLoader<string>();
        SetAuthType(authType.ToString());
        taskLoader.JoinTask(RememberMeLogin);
        taskLoader.JoinTask(DataManager.Instance.LoadAllPlayerInfoData);
        taskLoader.StartTasks(
            delegate()
            {
                onComplete?.Invoke();
            }
            , delegate (List<string> failList)
            {
                if (onFail != null)
                {
                    string failText = "";
                    foreach (string item in failList)
                    {
                        failText += "|" + item;
                    }
                    onFail.Invoke(failText);
                }
            }
        );
    }
    #endregion
}