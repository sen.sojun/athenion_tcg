﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetUIContentPosition : MonoBehaviour
{
    [SerializeField]
    private Vector2 targetPosition;

    private void OnEnable()
    {
        GetComponent<RectTransform>().anchoredPosition = new Vector2(targetPosition.x, targetPosition.y);
    }
}
