﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameplayDebugMenu : MonoBehaviour
{
    public GameObject GROUP_Debug;

    [Header("BTN")]
    public Button BTN_AddHP;
    public Button BTN_AddAP;
    public Button BTN_CreateCard;
    public Button BTN_RemoveDeck;
    public Button BTN_DebugLog;
    public TextMeshProUGUI DebugButtonText;

    [Header("Input")]
    public TMP_InputField INPUT_TextCardID;

    #region Init
    private void Start()
    {
        InitDebug();
    }

    private void InitDebug()
    {
        InitBTN(GameHelper.IsATNVersion_RealDevelopment);
    }

    private void InitBTN(bool isDebug)
    {
        GROUP_Debug.SetActive(isDebug);

        if (!isDebug) return;

        BTN_AddHP.onClick.RemoveAllListeners();
        BTN_AddHP.onClick.AddListener(Add_FullHP);

        BTN_AddAP.onClick.RemoveAllListeners();
        BTN_AddAP.onClick.AddListener(Add_1AP);

        BTN_CreateCard.onClick.RemoveAllListeners();
        BTN_CreateCard.onClick.AddListener(CreateCard);

        BTN_DebugLog.onClick.RemoveAllListeners();
        BTN_DebugLog.onClick.AddListener(OnClickDebugLog);

        BTN_RemoveDeck.onClick.RemoveAllListeners();
        BTN_RemoveDeck.onClick.AddListener(RemoveDeck);
    }
    #endregion

    #region Debug Methods
    public void Add_1AP()
    {
        PlayerIndex playerIndex = GameManager.Instance.GetLocalPlayerIndex();
        byte gamePhase_byte = GameHelper.ConvertGamePhaseToByte(GameManager.Instance.Phase);

        // Queue is not Ready, then reject this command.
        if (!CommandQueue.Instance.IsReady)
        {
            return;
        }

        if (GameManager.Instance.IsNetworkPlay())
        {
            GameManager.Instance.PhotonRequestAddPlayerAP(playerIndex, 1);
        }
        else
        {
            GameManager.Instance.RpcRequestAddPlayerAP(-1, gamePhase_byte, (int)playerIndex, 1);
        }
    }

    public void Add_1HP()
    {
        PlayerIndex playerIndex = GameManager.Instance.GetLocalPlayerIndex();
        byte gamePhase_byte = GameHelper.ConvertGamePhaseToByte(GameManager.Instance.Phase);

        // Queue is not Ready, then reject this command.
        if (!CommandQueue.Instance.IsReady)
        {
            return;
        }

        if (GameManager.Instance.IsNetworkPlay())
        {
            GameManager.Instance.PhotonRequestAddPlayerHP(playerIndex, 1);
        }
        else
        {
            GameManager.Instance.RpcRequestAddPlayerHP(-1, gamePhase_byte, (int)playerIndex, 1);
        }
       
    }

    public void Add_FullHP()
    {
        PlayerIndex playerIndex = GameManager.Instance.GetLocalPlayerIndex();
        byte gamePhase_byte = GameHelper.ConvertGamePhaseToByte(GameManager.Instance.Phase);

        // Queue is not Ready, then reject this command.
        if (!CommandQueue.Instance.IsReady)
        {
            return;
        }

        if (GameManager.Instance.IsNetworkPlay())
        {
            GameManager.Instance.PhotonRequestAddPlayerHP(playerIndex, 30);
        }
        else
        {
            GameManager.Instance.RpcRequestAddPlayerHP(-1, gamePhase_byte, (int)playerIndex, 30);
        }

    }

    public void CreateCard()
    {
        PlayerIndex playerIndex = GameManager.Instance.GetLocalPlayerIndex();
        byte gamePhase_byte = GameHelper.ConvertGamePhaseToByte(GameManager.Instance.Phase);
        string cardID = INPUT_TextCardID.text.ToUpper();

        // Queue is not Ready, then reject this command.
        if(!CommandQueue.Instance.IsReady)
        {
            return;
        }

        if (GameManager.Instance.IsNetworkPlay())
        {
            GameManager.Instance.PhotonRequestCreateCardToHand(playerIndex, cardID);
        }
        else
        {
            GameManager.Instance.RpcRequestCreateCardToHand(-1, gamePhase_byte, (int)playerIndex, cardID);
        }
    }

    public void RemoveDeck()
    {
        PlayerIndex playerIndex = GameManager.Instance.GetLocalPlayerIndex();
        byte gamePhase_byte = GameHelper.ConvertGamePhaseToByte(GameManager.Instance.Phase);

        // Queue is not Ready, then reject this command.
        if (!CommandQueue.Instance.IsReady)
        {
            return;
        }

        if (GameManager.Instance.IsNetworkPlay())
        {
            GameManager.Instance.PhotonRequestRemoveDeck(playerIndex);
        }
        else
        {
            GameManager.Instance.RpcRequestRemoveDeck(-1, gamePhase_byte, (int)playerIndex);
        }
    }

    public void OnClickDebugLog()
    {
        GameManager.Instance.SetShowDebugLog(!DataManager.Instance.GetShowDebugLog());
        UpdateDebugButton();
    }

    private void UpdateDebugButton()
    {
        if (DataManager.Instance.GetShowDebugLog())
        {
            DebugButtonText.text = "<color=green>ENABLE</color> DEBUG LOG";
        }
        else
        {
            DebugButtonText.text = "<color=red>DISABLE</color> DEBUG LOG";
        }
    }
    #endregion
}
