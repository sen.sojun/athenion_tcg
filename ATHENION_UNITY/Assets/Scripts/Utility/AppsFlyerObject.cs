﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class AppsFlyerObject : MonoBehaviour
{
    public static readonly string AppsFlyerDevKey = "mtBJ66mKgQ2QqatTVVpYej";
    public static readonly string GameAppID_iOS = "1456589064"; // NOTE: You should enter the number only and not the "ID" prefix 

    void Start()
    {
        // Mandatory - set your AppsFlyer’s Developer key.
        AppsFlyer.setAppsFlyerKey(AppsFlyerDevKey);

        // For detailed logging
        // AppsFlyer.setIsDebug (true);
        #if UNITY_IOS
        {
            // Mandatory - set your apple app ID
            AppsFlyer.setAppID (GameAppID_iOS);
            AppsFlyer.getConversionData();
            AppsFlyer.trackAppLaunch();
        }
        #elif UNITY_ANDROID
        {
            // For getting the conversion data in Android, you need to add the "AppsFlyerTrackerCallbacks" listener.
            AppsFlyer.init(AppsFlyerDevKey, "AppsFlyerTrackerCallbacks");
        }
        #endif
    }

    public static void AddLoginEvent(string playerID)
    {
        Dictionary<string, string> richEvent = new Dictionary<string, string>();

        richEvent.Add(AFInAppEvents.CUSTOMER_USER_ID, playerID);

        AppsFlyer.trackRichEvent(AFInAppEvents.LOGIN, richEvent);
    }

    public static void AddSpentCreditEvent(string orderID, string playerID, string itemID, VirtualCurrency currencyCode, uint purchasePrice, uint quantity)
    {
        Dictionary<string, string> richEvent = new Dictionary<string, string>();

        richEvent.Add(AFInAppEvents.ORDER_ID, orderID);
        richEvent.Add(AFInAppEvents.CUSTOMER_USER_ID, playerID);
        richEvent.Add(AFInAppEvents.CONTENT_ID, itemID);
        richEvent.Add(AFInAppEvents.CURRENCY, currencyCode.ToString());
        richEvent.Add(AFInAppEvents.PRICE, purchasePrice.ToString());
        richEvent.Add(AFInAppEvents.QUANTITY, quantity.ToString());

        AppsFlyer.trackRichEvent(AFInAppEvents.SPENT_CREDIT, richEvent);
    }

    public static void AddInAppPurchaseEvent(string orderID, string playerID, string itemID, string currencyCode, uint purchasePrice, uint quantity)
    {
        Dictionary<string, string> richEvent = new Dictionary<string, string>();

        float price = purchasePrice / 100.0f;

        richEvent.Add(AFInAppEvents.ORDER_ID, orderID);
        richEvent.Add(AFInAppEvents.CUSTOMER_USER_ID, playerID);
        richEvent.Add(AFInAppEvents.CONTENT_ID, itemID);
        richEvent.Add(AFInAppEvents.CURRENCY, currencyCode);
        richEvent.Add(AFInAppEvents.REVENUE, price.ToString());
        richEvent.Add(AFInAppEvents.PRICE, price.ToString());
        richEvent.Add(AFInAppEvents.QUANTITY, quantity.ToString());

        AppsFlyer.trackRichEvent(AFInAppEvents.PURCHASE, richEvent);
    }
}