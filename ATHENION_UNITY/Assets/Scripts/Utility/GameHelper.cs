﻿using DG.Tweening;
using GameAnalyticsSDK;
using Newtonsoft.Json;
using Photon.Pun;
using Photon.Chat;
using PlayFab;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Text.RegularExpressions;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
#if UNITY_IOS
using UnityEngine.iOS;
#endif

[assembly: AssemblyVersion("1.0.*")]

public static class GameHelper
{
    public static ATNVersion ATNVersion { get { return _atnVersion; } }
    private static ATNVersion _atnVersion = ATNVersion.Development;

    /// <summary>
    /// ATNVersion at first launch (has been set before server-switching)
    /// </summary>
    public static ATNVersion OriginATNVersion { get { return _originATNVersion; } }
    private static ATNVersion _originATNVersion = _atnVersion;

    /// <summary>
    /// true if ATNVersion and OriginATNVersion are equal to ATNVersion.Development
    /// </summary>
    public static bool IsATNVersion_RealDevelopment { get { return IsRealDev(); } }

    public static readonly int MaxCardPerDeck = 40;
    public static readonly int MaxCardPerBaseID = 3;

    public static readonly double Screen16by9_Ratio = 1.78; // 16:9 ratio
    public static readonly string DateTimeFormatPlayfab = "yyyy-MM-ddTHH:mm:ss.fffZ";
    public static readonly string DateTimeFormatSeason = "yyyy-MM-dd";
    public static readonly string TimeFormat = @"hh\:mm\:ss";

    public const string PRICE_STRING_FORMAT = "{0:#,##0.##}";

    public static float QualityLowValue = 0.5f;
    public static float QualityMedValue = 0.75f;
    public static float QualityHighValue = 1.0f;

    public static readonly string LoginSceneName = "0_LoginScene";

    public static readonly string NavigatorSceneName = "1_Navigator";

    public static readonly string HomeSceneName = "2_Home";
    public static readonly string CollectionSceneName = "2_Collection";
    public static readonly string ShopSceneName = "2_Shop";
    public static readonly string StarExchangeSceneName = "2_StarExchangeScene";

    public static readonly string PackOpeningSceneName = "2A_PackOpening";
    public static readonly string MainEventSceneName = "2A_MainEventScene";
    public static readonly string EventBattleSceneName = "2A_EventBattleScene";
    public static readonly string MatchingSceneName = "2A_LobbyScene";

    public static readonly string BattleSceneName = "3_BattleScene";
    public static readonly string BattleUISceneName = "3A_BattleUIScene";

    public static readonly string ContactUsEmail = "athenion@zerobit.co.th";
    public static readonly string ATNFacebookTHLink = "https://www.facebook.com/Athenion.TH/";
    public static readonly string ATNFacebookGlobalLink = "https://www.facebook.com/Athenion.EN/";
    public static readonly string ATNTwitterLink = "https://twitter.com/athenion_global?lang=en";
    public static readonly string ATNYouTubeLink = "https://www.youtube.com/channel/UCPvzhIt01OaSdnrKPQF70Uw";
    public static readonly string ATNRedditLink = "https://www.reddit.com/r/Athenion/";

    public static readonly string TermConditionLink = "http://www.playathenion.com/landing/athenion-terms.html";
    public static readonly string PrivacyLink = "http://www.playathenion.com/landing/athenion-privacy.html";

    public static readonly string LoadATNLink_Android = "market://details?id=com.zerobit.athenion";
    public static readonly string LoadATNLink_iOS = "itms-apps://itunes.apple.com/app/id1456589064";

    private static readonly string _battleSceneName = "BattleScene";
    private static readonly string _cardNameLocalizeKey = "CARD_NAME_";
    private static readonly string _cardFlavorLocalizeKey = "CARD_FLAVOR_";
    private static readonly string _cardAbilityDescriptionLocalizeKey = "ABILITY_DESCRIPTION_";
    private static readonly string _currencyLocalizeKey = "CURRENCY_";
    private static readonly float _floatEpsilon = Mathf.Epsilon;
    private static readonly string _specialChar = @" !#$%&'()*+,-./:;<=>?@฿[\]^_`{|}~" + "\"" + @"\|!#$%&/()=?+-*฿»«@£§€{}.-;:' <>_,[]";
    private static readonly string _rankKey = "RANK_NAME_{0}";

    private const string _itemStoreKeyPrefix = "com.zerobit.athenion.";

    public static readonly string PlayFabTitleID_Development = "DC17E";
    public static readonly string PlayFabTitleID_PreProduction = "10A6E";
    public static readonly string PlayFabTitleID_Production = "7847";

    // Multiplayer
    public static readonly string PhotonAppID_Development = "01424fab-61e5-476a-8071-e96f4d12bb92"; // 20 CCU
    public static readonly string PhotonAppID_PreProduction = "fe774874-5a88-47c5-b2f0-0c8099f6e616"; // 20 CCU
    public static readonly string PhotonAppID_Production = "ea38f62e-072a-47ed-8691-e419af0a4b09"; // 500 CCU

    // Chat
    public static readonly string PhotonChatAppID_Development = "91b9b726-6789-46bd-8c9b-a12aad35a304"; // 20 CCU
    public static readonly string PhotonChatAppID_PreProduction = "172abe00-b93a-42ca-9e14-96d7cbf57bcc"; // 20 CCU
    public static readonly string PhotonChatAppID_Production = "3a64f27e-67e7-4e92-8226-9477909742f4"; // 20 CCU

    public static Vector2 CenterScreen { get { return new Vector2(Screen.width / 2, Screen.height / 2); } }

    private static int _encodeIntKey = 0;
    private static bool _isSubscribeLogMessageReceived = false;

    public static bool IsRealDev()
    {
        if (ATNVersion == ATNVersion.Development)
        {
            if (OriginATNVersion == ATNVersion.Development)
            {
                return true;
            }
        }

        return false;
    }

    public static void FirstInit()
    {
        Debug.unityLogger.logEnabled = true;

#if UNITY_EDITOR
        {
            Debug.unityLogger.logEnabled = true;
        }
#else
        switch (ATNVersion)
        {
            case ATNVersion.Development:
            default:
            {
                Debug.unityLogger.logEnabled = true;
            }
            break;

            case ATNVersion.PreProduction:
            {
                Debug.unityLogger.logEnabled = true;
            }
            break;

            case ATNVersion.Production:
            {
                Debug.unityLogger.logEnabled = false;
            }
            break;
        }
#endif

        // Photon Server Setting
        switch (ATNVersion)
        {
            case ATNVersion.Development:
            default:
            {
                PhotonNetwork.PhotonServerSettings.AppSettings.AppIdRealtime = PhotonAppID_Development;
                PhotonNetwork.PhotonServerSettings.AppSettings.AppIdChat = PhotonChatAppID_Development;
            }
            break;

            case ATNVersion.PreProduction:
            {
                PhotonNetwork.PhotonServerSettings.AppSettings.AppIdRealtime = PhotonAppID_PreProduction;
                PhotonNetwork.PhotonServerSettings.AppSettings.AppIdChat = PhotonChatAppID_PreProduction;
            }
            break;

            case ATNVersion.Production:
            {
                PhotonNetwork.PhotonServerSettings.AppSettings.AppIdRealtime = PhotonAppID_Production;
                PhotonNetwork.PhotonServerSettings.AppSettings.AppIdChat = PhotonChatAppID_Production;
            }
            break;
        }
        Debug.LogFormat("<color=blue>PUN</color> Server ID : <color=blue>{0}</color>", PhotonNetwork.PhotonServerSettings.AppSettings.AppIdRealtime);

        // Playfab Server Setting
        switch (ATNVersion)
        {
            case ATNVersion.Development:
            default:
            {
                PlayFabSettings.TitleId = PlayFabTitleID_Development;
            }
            break;

            case ATNVersion.PreProduction:
            {
                PlayFabSettings.TitleId = PlayFabTitleID_PreProduction;
            }
            break;

            case ATNVersion.Production:
            {
                PlayFabSettings.TitleId = PlayFabTitleID_Production;
            }
            break;
        }
        Debug.LogFormat("<color=orange>Playfab</color> Server ID : <color=orange>{0}</color>", PlayFabSettings.TitleId);

        if (!DataManager.Instance.IsSetFPSSetting() || !DataManager.Instance.IsSetScreenRatioSetting())
        {
            InitOptionSetting();
        }

        DOTween.Init();
        GameAnalytics.Initialize();

        Application.targetFrameRate = DataManager.Instance.GetFPSSetting();
        SoundManager.InitVolume();

        // Subscribe unity log event.
        SubscribeLogMessageReceived();

#if UNITY_ANDROID || UNITY_IOS
        ScreenManager.SetScreenSize(DataManager.Instance.GetScreenRatio());
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
#else
            Screen.SetResolution(564, 960, false);
            Screen.fullScreen = false;
#endif

        Input.multiTouchEnabled = false;

        QualitySettings.asyncUploadTimeSlice = 4;
        QualitySettings.asyncUploadBufferSize = 16;
        QualitySettings.asyncUploadPersistentBuffer = true;

        if (!Debug.isDebugBuild)
        {
            RemoveLogReport();
        }
    }

    public static void SetATNVersion(ATNVersion atnVersion)
    {
        _atnVersion = atnVersion;
    }

    #region Unity Callback
    public static void SubscribeLogMessageReceived()
    {
        if (!_isSubscribeLogMessageReceived)
        {
            Application.logMessageReceived += GameHelper.HandleLog;
            _isSubscribeLogMessageReceived = true;
        }
    }

    public static void UnsubscribeLogMessageReceived()
    {
        if (_isSubscribeLogMessageReceived)
        {
            Application.logMessageReceived -= GameHelper.HandleLog;
            _isSubscribeLogMessageReceived = false;
        }
    }

    private static void HandleLog(string logString, string stackTrace, LogType type)
    {
#if !UNITY_EDITOR
        if (GameHelper.IsATNVersion_RealDevelopment || DataManager.Instance.IsWhiteList())
#endif
        {
            switch (type)
            {
                case LogType.Error:
                //case LogType.Assert:
                //case LogType.Warning:
                //case LogType.Log:
                case LogType.Exception:
                    {
                        PopupUIManager.Instance.ShowPopup_Error(type.ToString(), logString);
                        PopupUIManager.Instance.ShowPopup_Error(type.ToString() + " stackTrace", stackTrace);
                    }
                    break;
            }
        }
    }
    #endregion

    public static void Clear(this StringBuilder value)
    {
        value.Length = 0;
        value.Capacity = 0;
    }

    public static void CopyToClipboard(string text)
    {
        GUIUtility.systemCopyBuffer = text;
    }

    public static string PasteFromClipboard()
    {
        return GUIUtility.systemCopyBuffer;
    }

    public static void RemoveLogReport()
    {
        Reporter reporter = GameObject.FindObjectOfType<Reporter>();
        if (reporter != null)
        {
            GameObject.Destroy(reporter.gameObject);
        }
    }

    public static T FindSceneObjects<T>(string sceneName) where T : UnityEngine.Object
    {
        GameObject[] objList = GameObject.FindObjectsOfType<GameObject>();
        foreach (GameObject go in objList)
        {
            if (go.scene.name.CompareTo(sceneName) == 0)
            {
                T result = go.GetComponent<T>();
                if (result != null)
                    return result;
            }
        }
        return null;
    }

    public static bool StrToEnum<T>(string inputStr, out T result)
    {
        try
        {
            result = (T)Enum.Parse(typeof(T), inputStr);
            if (Enum.IsDefined(typeof(T), result))
            {
                return true;
            }
        }
        catch (ArgumentException e)
        {
            //Debug.LogError(e.Message);
        }

        result = default(T);
        return false;
    }

    public static bool TryDeserializeJSONStr<T>(string jsonStr, out T result)
    {
        try
        {
            result = JsonConvert.DeserializeObject<T>(jsonStr);
            if (result != null)
            {
                return true;
            }
        }
        catch (ArgumentException e)
        {
            Debug.LogError(e.Message);
        }

        result = default(T);
        return false;
    }

    public static List<T> CloneList<T>(List<T> list) where T : ICloneable, new()
    {
        List<T> result = new List<T>();

        foreach (T item in list)
        {
            object obj = item.Clone();
            result.Add((T)obj);
        }

        return result;
    }

    public static bool IsUTCDateTimeAlreadyPass(DateTime startDateTime)
    {
        DateTime currentDateTime = DateTimeData.GetDateTimeUTC();
        if (startDateTime < currentDateTime)
        {
            return true;
        }

        return false;
    }

    public static bool IsUTCDateTimeAlreadyPassEqual(DateTime startDateTime)
    {
        DateTime currentDateTime = DateTimeData.GetDateTimeUTC();
        if (startDateTime <= currentDateTime)
        {
            return true;
        }

        return false;
    }

    public static DateTime GetNextDayUTC(DateTime datetime)
    {
        DateTime datetimeOffset = DateTimeData.ConvertToUTCOffset(datetime);
        DateTime result = new DateTime(datetimeOffset.Year, datetimeOffset.Month, datetimeOffset.Day, 0, 0, 0);
        return DateTimeData.ConvertOffsetToUTC(result.AddDays(1));
    }

    public static DateTime GetNextWeekDate(DateTime inputDateTime)
    {
        int thursdayIndex = 4;
        int nextThursdayIndex = 11; // thursday + 7 day
        int hourOffset = 2;

        DateTime currentDate = inputDateTime.Date;
        int day = (int)currentDate.DayOfWeek;
        if (day < thursdayIndex)
        {
            int diffDays = thursdayIndex - day;
            DateTime targetTime = currentDate.AddDays(diffDays).AddHours(hourOffset);
            return targetTime;
        }
        else if (day == thursdayIndex)
        {
            DateTime targetTime = currentDate.AddHours(hourOffset);
            if (targetTime > inputDateTime)
            {
                return targetTime;
            }
            else
            {
                int diffDays = nextThursdayIndex - day;
                targetTime = currentDate.AddDays(diffDays).AddHours(hourOffset);
                return targetTime;
            }
        }
        else
        {
            int diffDays = nextThursdayIndex - day;
            DateTime targetTime = currentDate.AddDays(diffDays).AddHours(hourOffset);
            return targetTime;
        }
    }

    public static bool IsNewWeekUTC(DateTime weekTimeStamp)
    {
        DateTime currentTime = DateTimeData.GetDateTimeUTC();
        if (currentTime < weekTimeStamp) // should not happen.
        {
            return false;
        }

        DateTime nextThursdayOfWeekTimeStamp = GetNextWeekDate(weekTimeStamp);
        DateTime nextThursdayOfCurrentTime = GetNextWeekDate(currentTime);
        bool isSameThursdayDate = (nextThursdayOfWeekTimeStamp.Date == nextThursdayOfCurrentTime.Date);
        return isSameThursdayDate == false;
    }

    public static bool IsNewDayUTC(DateTime startDateTime)
    {
        DateTime currentDateTime = DateTimeData.GetDateTimeUTCOffset();
        startDateTime = DateTimeData.ConvertToUTCOffset(startDateTime);
        if (currentDateTime > startDateTime)
        {
            if (currentDateTime.Day > startDateTime.Day
               || currentDateTime.Month > startDateTime.Month
               || currentDateTime.Year > startDateTime.Year)
            {
                return true;
            }
        }

        return false;
    }

    public static bool IsCurrentUTCDateTimeValid(DateTime startDateTime, DateTime endDateTime)
    {
        return IsUTCDateTimeAlreadyPassEqual(startDateTime) && !IsUTCDateTimeAlreadyPassEqual(endDateTime);
    }

    public static string GetCountdownText(TimeSpan timeLeft)
    {
        string text = "";
        if (timeLeft.Days > 0)
        {
            text = string.Format("{0} {1} {2}", timeLeft.Days, LocalizationManager.Instance.GetText("DAILY_DAY"), timeLeft.ToString(GameHelper.TimeFormat));
        }
        else if (timeLeft.TotalSeconds > 0)
        {
            text = timeLeft.ToString(GameHelper.TimeFormat);
        }
        else
        {
            text = "00:00:00";
        }

        return text;
    }

    public static List<PlayerIndex> GetPlayerIndexList(int playerCount)
    {
        List<PlayerIndex> result = new List<PlayerIndex>();

        int index = 0;
        foreach (PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
        {
            result.Add(playerIndex);
            ++index;

            if (index >= playerCount)
            {
                break;
            }
        }

        return result;
    }

    public static bool GetOppositeDirection(CardDirectionType cardDirectionType, out CardDirectionType result)
    {
        result = CardDirectionType.N;
        switch (cardDirectionType)
        {
            case CardDirectionType.N: result = CardDirectionType.S; return true;
            case CardDirectionType.NE: result = CardDirectionType.SW; return true;
            case CardDirectionType.E: result = CardDirectionType.W; return true;
            case CardDirectionType.SE: result = CardDirectionType.NW; return true;
            case CardDirectionType.S: result = CardDirectionType.N; return true;
            case CardDirectionType.SW: result = CardDirectionType.NE; return true;
            case CardDirectionType.W: result = CardDirectionType.E; return true;
            case CardDirectionType.NW: result = CardDirectionType.SE; return true;
        }
        return false;
    }

    public static CardDirectionType GetOppositeDirection(CardDirectionType cardDirectionType)
    {
        switch (cardDirectionType)
        {
            case CardDirectionType.N: return CardDirectionType.S;
            case CardDirectionType.NE: return CardDirectionType.SW;
            case CardDirectionType.E: return CardDirectionType.W;
            case CardDirectionType.SE: return CardDirectionType.NW;
            case CardDirectionType.S: return CardDirectionType.N;
            case CardDirectionType.SW: return CardDirectionType.NE;
            case CardDirectionType.W: return CardDirectionType.E;
            case CardDirectionType.NW: return CardDirectionType.SE;
        }

        return CardDirectionType.N;
    }

    public static string ConvertItemIDToItemKey(string itemID)
    {
        string realItemID = itemID.Replace(_itemStoreKeyPrefix, "").ToUpper(); // เอารหัส Store Bundle ออกไป
        return realItemID;
    }

    public static void DestroyAllMonoSingleton()
    {
        GameObject[] monoSingletons = GameObject.FindGameObjectsWithTag("MonoSingleton");
        foreach (GameObject obj in monoSingletons)
        {
            obj.SendMessage("DestroySelf", SendMessageOptions.DontRequireReceiver);
            //GameObject.Destroy(obj);
        }
    }

    public static bool IsNearlyEqual(float value1, float value2)
    {
        return (Math.Abs(value1 - value2) < _floatEpsilon);
    }

    public static List<string> SplitParam(string text)
    {
        List<string> result = new List<string>();

        int blanketCount = 0;
        int startIndex = 0;

        for (int index = 0; index < text.Length; ++index)
        {
            char c = text[index];

            switch (c)
            {
                case ',':
                    {
                        if (blanketCount <= 0)
                        {
                            if (c == ',')
                            {
                                string param = text.Substring(startIndex, index - startIndex);
                                result.Add(param);

                                startIndex = index + 1;
                            }
                        }
                    }
                    break;

                case '(':
                    {
                        blanketCount++;
                    }
                    break;

                case ')':
                    {
                        if (blanketCount > 0)
                        {
                            blanketCount--;
                        }
                    }
                    break;
            }
        }

        if (startIndex < text.Length)
        {
            string param = text.Substring(startIndex, text.Length - startIndex);
            result.Add(param);
        }

        return result;
    }

    public static string GetCardBaseID(string cardFullID)
    {
        string[] cardIDSplit = cardFullID.Split('_', ':');
        return cardIDSplit[0];
    }

    public static string GetCardName(string cardID)
    {
        string key = _cardNameLocalizeKey + cardID;
        string result;
        bool isFound = LocalizationManager.Instance.GetText(key, out result);
        if (isFound)
        {
            if (result != "-")
            {
                return result;
            }
            else
            {
                return "";
            }
        }
        else
        {
            return key;
        }
    }

    public static string GetCardAbilityDescription(string cardBaseId)
    {
        string key = _cardAbilityDescriptionLocalizeKey + cardBaseId;
        if (LocalizationManager.Instance.GetText(key, out string result))
        {
            if (result != "-")
            {
                return result;
            }

            return "";
        }

        return key;
    }

    public static string GetCardFlavor(string cardID)
    {
        string key = _cardFlavorLocalizeKey + cardID;
        string result;
        bool isFound = LocalizationManager.Instance.GetText(key, out result);
        if (isFound)
        {
            if (result != "-")
            {
                return result;
            }
            else
            {
                return "";
            }
        }
        else
        {
            return key;
        }
    }

    public static string GetIntToRomanNumber(int value)
    {
        string result = "";
        switch (value)
        {
            case 0:
                result = "";
                break;
            case 1:
                result = "I";
                break;
            case 2:
                result = "II";
                break;
            case 3:
                result = "III";
                break;
            case 4:
                result = "IV";
                break;
            default:
                result = "Max";
                break;
        }
        return result;
    }

    public static float GetRandomFloat(float minValue, float maxValue, int decimalPoint = 4)
    {
        System.Random r = new System.Random();
        float decimalValue = (int)Mathf.Pow(10.0f, (float)decimalPoint);
        float ratio = r.Next(0, Mathf.FloorToInt(decimalValue + 1)) / decimalValue; // 4 decimal point
        float result = minValue + ((maxValue - minValue) * ratio);

        return result;
    }

    #region UI Color
    /// <summary>
    /// Use to convert color from HTML Code (#RRGGBBAA)
    /// </summary>
    /// <param name="code">"#RRGGBBAA"</param>
    /// <returns></returns>
    public static Color GetColorByHtmlCode(string code)
    {
        Color color;
        if (ColorUtility.TryParseHtmlString(code, out color))
        {
            return color;
        }
        else
        {
            Debug.LogWarning("GetColorByHtmlCode: ERROR can't parse color code." + code);
            return Color.magenta;
        }

    }

    public static Color GetColor_PlayerBlue()
    {
        return GetColorByHtmlCode("#0098FFFF");
    }

    public static Color GetColor_PlayerRed()
    {
        return GetColorByHtmlCode("#DE381FFF");
    }

    public static Color GetColor_ATK_PlayerBlue()
    {
        return GetColorByHtmlCode("#00FFFFFF");
    }

    public static Color GetColor_ATK_PlayerRed()
    {
        return GetColorByHtmlCode("#FFB800");
    }

    public static Color GetColor_Number_Normal()
    {
        return GetColorByHtmlCode("#E9E9E9");
    }

    public static Color GetColor_Number_Decrease()
    {
        return GetColorByHtmlCode("#FF5555");
    }

    public static Color GetColor_Number_Increase()
    {
        //return Color.green;
        return GetColorByHtmlCode("#3FEC48");
    }

    public static Color GetColor_Faction_Fire()
    {
        return GetColorByHtmlCode("#FF0000FF");
    }

    public static Color GetColor_Faction_Water()
    {
        return GetColorByHtmlCode("#00A1FFFF");
    }

    public static Color GetColor_Faction_Air()
    {
        return GetColorByHtmlCode("#3FEC48FF");
    }

    public static Color GetColor_Faction_Earth()
    {
        return GetColorByHtmlCode("#FF9300FF");
    }

    public static Color GetColor_Faction_Holy()
    {
        return GetColorByHtmlCode("#FFDA55FF");
    }

    public static Color GetColor_Faction_Dark()
    {
        return GetColorByHtmlCode("#BA00FDFF");
    }
    #endregion

    public static bool IsLegendPack(string packID)
    {
        return packID.ToUpper().Contains("LEGEND");
    }

    public static string SlotIDToSlotCode(int id)
    {
        if (id >= 0 && id < (GameManager.Instance.BoardWidth * GameManager.Instance.BoardHeight))
        {
            int row = id / GameManager.Instance.BoardWidth;
            int column = id % GameManager.Instance.BoardWidth;

            string rowCode = "";
            string columnCode = "";

            rowCode = (row + 1).ToString();
            columnCode = ((char)('A' + column)).ToString();

            return string.Format("{0}{1}", rowCode, columnCode);
        }

        return "";
    }

    public static int SlotCodeToSlotID(string code)
    {
        if (!string.IsNullOrEmpty(code) && code.Length == 2)
        {
            char rowCode = code.ToUpper()[0];
            char columnCode = code.ToUpper()[1];

            int row = (rowCode - '0') - 1;
            int column = (columnCode - 'A');

            int slotIndex = (row * GameManager.Instance.BoardWidth) + column;

            if (slotIndex >= 0 && slotIndex < (GameManager.Instance.BoardWidth * GameManager.Instance.BoardHeight))
            {
                return slotIndex;
            }
        }

        return -1;
    }

    public static bool IsInScene(string targetSceneName)
    {
        // TODO
        return false;

    }

    public static bool IsInLoginScene()
    {
        return (SceneManager.GetActiveScene().name == LoginSceneName);
    }

    public static bool IsInBattleScene()
    {
        return (SceneManager.GetActiveScene().name == _battleSceneName);
    }

    public static bool IsHaveObject(Type type)
    {
        object obj = GameObject.FindObjectOfType(type);
        return (obj != null);
    }

    public static bool IsVirtualCurrency(string itemID)
    {
        string[] splitTexts = itemID.Split('_');
        return splitTexts[0].ToUpper() == "VC";
    }

    public static bool IsEventCurrency(string itemID)
    {
        string[] splitTexts = itemID.Split('_');
        return splitTexts[0].ToUpper() == "EC";
    }

    public static float GetHorizontalAngle(Camera camera)
    {
        float vFOVrad = camera.fieldOfView * Mathf.Deg2Rad;
        float cameraHeightAt1 = Mathf.Tan(vFOVrad * .5f);
        return Mathf.Atan(cameraHeightAt1 * camera.aspect) * 2f * Mathf.Rad2Deg;
    }

    public static float GetVerticalAngle(Camera camera)
    {
        return camera.fieldOfView;
    }

    public static ScreenFitMode GetScreenFitMode()
    {
        double screenRatio = (double)Screen.height / (double)Screen.width;
        //Debug.Log(screenRatio);
        if (screenRatio > GameHelper.Screen16by9_Ratio)  // 16:9
        {
            return ScreenFitMode.Width;
        }
        else
        {
            return ScreenFitMode.Height;
        }
    }

    public static Sprite ConvertTexture2DToSprite(Texture2D texture)
    {
        if (texture != null)
        {
            Sprite sprite = Sprite.Create(
                  texture
                , new Rect(0f, 0f, texture.width, texture.height)
                , new Vector2(0.5f, 0.5f) // pivot center.
            );

            return sprite;
        }

        return null;
    }

    public static Sprite[] ConvertTexture2DArrayToSpriteArray(Texture2D[] texture)
    {
        if (texture != null)
        {
            List<Sprite> spriteList = new List<Sprite>();

            foreach (Texture2D texture2D in texture)
            {
                Sprite sprite = Sprite.Create(
                    texture2D
                    , new Rect(0f, 0f, texture2D.width, texture2D.height)
                    , new Vector2(0.5f, 0.5f) // pivot center.
                );
            }

            return spriteList.ToArray();
        }
        return null;
    }

    // Convert an object to a byte array
    public static byte[] ObjectToByteArray(object obj)
    {
        BinaryFormatter bf = new BinaryFormatter();
        using (var ms = new MemoryStream())
        {
            bf.Serialize(ms, obj);
            return ms.ToArray();
        }
    }

    // Convert a byte array to an Object
    public static object ByteArrayToObject(byte[] arrBytes)
    {
        using (var memStream = new MemoryStream())
        {
            var binForm = new BinaryFormatter();
            memStream.Write(arrBytes, 0, arrBytes.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            var obj = binForm.Deserialize(memStream);
            return obj;
        }
    }

    public static void LaunchEmail()
    {
        string email = ContactUsEmail;
        string subject = MyEscapeURL("Contect Us - " + DataManager.Instance.PlayerInfo.DisplayName);
        string body = MyEscapeURL("");
        Application.OpenURL(string.Format(
              "mailto:{0}?subject={1}{2}"
            , email
            , subject
            , ((body.Length > 0) ? ("&body=" + body) : "")
        ));
    }

    private static string MyEscapeURL(string url)
    {
        return UnityWebRequest.EscapeURL(url).Replace("+", "%20");
    }

    public static bool ShowReview()
    {
        bool isActive = false;

#if UNITY_IOS
        {
            bool isShow = Device.RequestStoreReview();
            if(isShow)
            {
                isActive = true;
            }
        }
#elif UNITY_ANDROID
        {
            PopupUIManager.Instance.ShowPopup_TwoChoices(
                  LocalizationManager.Instance.GetText("RATE_SHOW_REVIEW_HEADER")
                , LocalizationManager.Instance.GetText("RATE_SHOW_REVIEW_DESCRIPTION")
                , LocalizationManager.Instance.GetText("BUTTON_RATE_NOW")
                , LocalizationManager.Instance.GetText("BUTTON_NOT_NOW")
                , delegate ()
                {
                    Application.OpenURL(GameHelper.LoadATNLink_Android);
                }
                , null
            );

            isActive = true;
        }
#endif

        return isActive;
    }

    public static void ShowFacebookPopupFirstTime()
    {
        string path = "{\"image_path\": \"Images/BannerPopup/BannerFacebookPopup_FirstTime\"}";
        Dictionary<string, object> rawData = JsonConvert.DeserializeObject<Dictionary<string, object>>(path);
        string imagePath = rawData["image_path"].ToString();

        PopupUIManager.Instance.ShowFacebookBannerPopup_FirstTime("BannerPopup", imagePath, imagePath);
    }

    public static void LoadSceneAsync(string sceneName, LoadSceneMode loadSceneMode, UnityAction onComplete, UnityAction<float> onLoading)
    {
        GameObject obj = new GameObject();
        SceneLoader sceneLoader = obj.AddComponent<SceneLoader>();
        obj.name = string.Format("SceneLoader({0})", sceneName);
        sceneLoader.StartLoadAsync(sceneName, loadSceneMode, onComplete, onLoading);
    }

    public static void UnLoadSceneAsync(string sceneName, UnityAction onComplete, bool isUnloadUnusedAsset = true)
    {
        GameObject obj = new GameObject();
        SceneLoader sceneLoader = obj.AddComponent<SceneLoader>();
        obj.name = string.Format("SceneLoader({0})", sceneName);
        sceneLoader.StartUnLoadSceneAsync(
              sceneName
            , delegate ()
            {
                if (isUnloadUnusedAsset)
                {
                    Resources.UnloadUnusedAssets();
                }

                onComplete?.Invoke();
            }
        );
    }

    public static string GetVersionText()
    {
#if UNITY_ANDROID
        return GetVersionText(RuntimePlatform.Android);
#elif UNITY_IOS
        return GetVersionText(RuntimePlatform.IPhonePlayer);
#elif UNITY_EDITOR
        return GetVersionText(RuntimePlatform.WindowsEditor);
#else
        return GetDefaultVersionText();
#endif
    }

    public static string GetBuildDate()
    {
        return BuildDate().ToString("yyyyMMdd");
    }

    public static string GetAssetBundleFolder()
    {
        string buildDate = GameHelper.GetBuildDate();
        switch (GameHelper.ATNVersion)
        {
            case ATNVersion.Development:
            {
                return "Default";
            }

            case ATNVersion.PreProduction:
            {
                return string.Format("PRE_{0}", GameHelper.GetBuildDate());
            }

            case ATNVersion.Production:
            default:
            {
                return GameHelper.GetBuildDate();
            }
        }
    }

    public static string GetLocalizeCurrencyText(VirtualCurrency currency)
    {
        return LocalizationManager.Instance.GetText(_currencyLocalizeKey + currency.ToString());
    }

    public static string GetVersionText(RuntimePlatform platform)
    {
        switch (platform)
        {
            case RuntimePlatform.Android:
                {
                    return string.Format("ATHENION_APK_{0} Built {1}{2}", Application.version, GetBuildDate(), GetVersionPostfix());
                }

            case RuntimePlatform.IPhonePlayer:
                {
                    return string.Format("ATHENION_iOS_{0} Built {1}{2}", Application.version, GetBuildDate(), GetVersionPostfix());
                }

            case RuntimePlatform.WindowsEditor:
            case RuntimePlatform.OSXEditor:
            case RuntimePlatform.LinuxEditor:
            {
                return string.Format("ATHENION_EDITOR_{0} Built {1}{2}", Application.version, GetBuildDate(), GetVersionPostfix());
            }

            default:
            {
                return GetDefaultVersionText();
            }
        }
    }

    public static string GetDefaultVersionText()
    {
        return string.Format("ATHENION_{0} Built {1}{2}", Application.version, GetBuildDate(), GetVersionPostfix());
    }

    private static string GetVersionPostfix()
    {
        string versionPostfix = "";

        switch (ATNVersion)
        {
            case ATNVersion.Development: versionPostfix = "_DEV"; break;
            case ATNVersion.PreProduction: versionPostfix = "_PRE"; break;

            default: versionPostfix = ""; break;
        }

        return versionPostfix;
    }

    public static string GetLoadATNLink()
    {
#if UNITY_ANDROID
        return LoadATNLink_Android;
#elif UNITY_IOS
            return LoadATNLink_iOS;
#else
            return ATNFacebookTHLink;
#endif
    }

    private static System.Version Version()
    {
        return Assembly.GetExecutingAssembly().GetName().Version;
    }

    private static System.DateTime BuildDate()
    {
        System.Version version = Version();
        System.DateTime startDate = new System.DateTime(2000, 1, 1, 0, 0, 0);
        System.TimeSpan span = new System.TimeSpan(version.Build, 0, 0, version.Revision * 2);
        System.DateTime buildDate = startDate.Add(span);
        return buildDate;
    }

    public static NetworkReachability CheckNetworkReachability()
    {
        return Application.internetReachability;
    }

    public static RuntimePlatform GetRuntimePlatform()
    {
#if UNITY_ANDROID
        return RuntimePlatform.Android;
#elif UNITY_IOS
        return RuntimePlatform.IPhonePlayer;
#else
        return Application.platform;
#endif
    }

    public static bool IsNotchScreen()
    {
#if UNITY_IOS
        switch (Device.generation)
        {
            // iPhone
            case DeviceGeneration.iPhoneXR:
            case DeviceGeneration.iPhoneX:
            case DeviceGeneration.iPhoneXS:
            case DeviceGeneration.iPhoneXSMax:
            {
                return true;
            }
            break;

            case DeviceGeneration.iPhoneUnknown:
            {
                // https://www.paintcodeapp.com/news/ultimate-guide-to-iphone-resolutions

                Vector2Int[] screen_List = {
                      new Vector2Int(828, 1792) // 11
                    , new Vector2Int(1125, 2436) // 11 Pro
                    , new Vector2Int(1242, 2688) // 11 Pro Max
                };

                foreach (Vector2Int currentScreen in screen_List)
                {
                    if (Screen.width == currentScreen.x && Screen.height == currentScreen.y)
                {
                    return true;
                }
                else
                {
                    float ratio = (float)Screen.width / (float)Screen.height;
                    float iPhone11ratio = (float)currentScreen.x / (float)currentScreen.y;

                    if (IsNearlyEqual(ratio, iPhone11ratio))
                    {
                        return true;
                    }
                }
                }
            }
            break;
        }
#elif UNITY_EDITOR
        {
            // https://www.paintcodeapp.com/news/ultimate-guide-to-iphone-resolutions

            Vector2Int[] screen_List = {
                      new Vector2Int(828, 1792) // 11
                    , new Vector2Int(1125, 2436) // 11 Pro
                    , new Vector2Int(1242, 2688) // 11 Pro Max
                };

            foreach (Vector2Int currentScreen in screen_List)
            {
                if (Screen.width == currentScreen.x && Screen.height == currentScreen.y)
                {
                    return true;
                }
                else
                {
                    float ratio = (float)Screen.width / (float)Screen.height;
                    float iPhone11ratio = (float)currentScreen.x / (float)currentScreen.y;

                    if (IsNearlyEqual(ratio, iPhone11ratio))
                    {
                        return true;
                    }
                }
            }
        }
#endif

        return false;
    }

    private static void InitOptionSetting()
    {
        int processorScore = (SystemInfo.processorCount * SystemInfo.processorFrequency);
        //int memoryScore = (SystemInfo.systemMemorySize + SystemInfo.graphicsMemorySize);

        Debug.LogFormat("processorScore : {0} = {1}x{2}", processorScore, SystemInfo.processorCount, SystemInfo.processorFrequency);

        switch (Application.platform)
        {
            case RuntimePlatform.Android:
                {
                    Debug.Log("== Android ==");
                    if (processorScore <= 13000)
                    {
                        SetOptionSettingDefault(ScreenQuality.Low);
                    }
                    else if (processorScore <= 15000)
                    {
                        SetOptionSettingDefault(ScreenQuality.Med);
                    }
                    else
                    {
                        SetOptionSettingDefault(ScreenQuality.High);
                    }
                }
                break;

#if UNITY_IOS
            case RuntimePlatform.IPhonePlayer:
            {
                Debug.Log("== iOS ==");

                switch(Device.generation)
                {
                    // iPhone
                    case DeviceGeneration.iPhone6:      // Late 2014
                    case DeviceGeneration.iPhone6Plus:  // Late 2014
                    case DeviceGeneration.iPhone6S:     // Late 2015
                    case DeviceGeneration.iPhone6SPlus: // Late 2015

                    // iPad
                    case DeviceGeneration.iPad3Gen:     // Early 2012
                    case DeviceGeneration.iPadAir1:     // Late 2013
                    case DeviceGeneration.iPadAir2:     // Late 2014
                    case DeviceGeneration.iPadMini1Gen: // Late 2012
                    case DeviceGeneration.iPadMini2Gen: // Late 2013
                    case DeviceGeneration.iPadMini3Gen: // Late 2014
                    {
                        SetOptionSettingDefault(ScreenQuality.Med);
                    }
                    break;

                    // iPhone
                    case DeviceGeneration.iPhone7:      // Late 2016
                    case DeviceGeneration.iPhone7Plus:  // Late 2016
                    case DeviceGeneration.iPhone8:      // Late 2017
                    case DeviceGeneration.iPhone8Plus:  // Late 2017
                    case DeviceGeneration.iPhoneX:      // Late 2017
                    case DeviceGeneration.iPhoneXR:     // Late 2018
                    case DeviceGeneration.iPhoneXS:     // Late 2018
                    case DeviceGeneration.iPhoneXSMax:  // Late 2018

                    // iPad
                    case DeviceGeneration.iPad4Gen:     // Late 2012
                    case DeviceGeneration.iPad5Gen:
                    case DeviceGeneration.iPadPro10Inch1Gen:
                    case DeviceGeneration.iPadPro10Inch2Gen:
                    case DeviceGeneration.iPadPro11Inch:
                    case DeviceGeneration.iPadPro1Gen:
                    case DeviceGeneration.iPadPro2Gen:
                    case DeviceGeneration.iPadPro3Gen:
                    case DeviceGeneration.iPadMini4Gen: // Late 2015
                    {
                        SetOptionSettingDefault(ScreenQuality.High);
                    }
                    break;

                    default:
                    {
                        SetOptionSettingDefault(ScreenQuality.Low);
                    }
                    break;
                }
            }
            break;
#endif

            case RuntimePlatform.OSXPlayer:
            case RuntimePlatform.OSXEditor:
            case RuntimePlatform.WindowsPlayer:
            case RuntimePlatform.WindowsEditor:
                {
                    SetOptionSettingDefault(ScreenQuality.High);
                }
                break;

            default:
                {
                    SetOptionSettingDefault(ScreenQuality.Med);
                }
                break;
        }
    }

    private static void SetOptionSettingDefault(ScreenQuality quality)
    {
        DataManager.Instance.SetSFXOn(true);
        DataManager.Instance.SetBGMOn(true);
        DataManager.Instance.SetVoiceOn(true);

        switch (quality)
        {
            case ScreenQuality.Low:
                {
                    // Low Teirs.
                    DataManager.Instance.SetFPSSetting(30);
                    DataManager.Instance.SetScreenQuality(ScreenQuality.Low);
                    DataManager.Instance.SetScreenRatio(GameHelper.QualityLowValue);
                }
                break;

            case ScreenQuality.Med:
                {
                    // Mid Teirs.
                    DataManager.Instance.SetFPSSetting(30);
                    DataManager.Instance.SetScreenQuality(ScreenQuality.Med);
                    DataManager.Instance.SetScreenRatio(GameHelper.QualityMedValue);
                }
                break;

            case ScreenQuality.High:
                {
                    // High Teirs.
                    DataManager.Instance.SetFPSSetting(30);
                    DataManager.Instance.SetScreenQuality(ScreenQuality.High);
                    DataManager.Instance.SetScreenRatio(GameHelper.QualityHighValue);
                }
                break;
        }
    }

    public static DateTime ISOTimeStrToUTCDateTime(string isoTimeStr)
    {
        return Convert.ToDateTime(isoTimeStr).ToUniversalTime();

        /*
        DateTime date = DateTime.Parse(
              isoTimeStr
            , CultureInfo.InvariantCulture
            , System.Globalization.DateTimeStyles.RoundtripKind
        );

        return date;
        */
    }

    public static void DelayCallback(float time, UnityAction onComplete = null)
    {
        UtilityItem obj = UtilityPool.Instance.CreateItem(UtilityPool.UtilityType.DelayObject);
        DelayObject delayObj = obj.GetComponent<DelayObject>();

        delayObj.StartDelay(time, onComplete);
    }

    public static IEnumerator Delay(float time, UnityAction onComplete = null)
    {
        yield return new WaitForSeconds(time);
        if (onComplete != null)
            onComplete.Invoke();
    }

    public static IEnumerator ExecuteIEnumerator(IEnumerator command, UnityAction onComplete = null)
    {
        yield return command;
        onComplete?.Invoke();
    }

    #region UI Rank Helper
    public static string GetRankID(int rankIndex)
    {
        string negativePrefix = "";
        if (rankIndex < 0)
        {
            negativePrefix = "N";
        }

        int absoluteRankIndex = Mathf.Abs(rankIndex);
        return string.Format("RK{0}{1}", negativePrefix, absoluteRankIndex.ToString("D3"));
    }

    public static string GetRankName(string rankId)
    {
        string rankNameKey = string.Format(_rankKey, rankId);
        string rankName = LocalizationManager.Instance.GetText(rankNameKey);

        return rankName;
    }

    public static string GetRankName(int rankIndex)
    {
        string rankId = GetRankID(rankIndex);
        string rankNameKey = string.Format(_rankKey, rankId);
        string rankName = LocalizationManager.Instance.GetText(rankNameKey);

        return rankName;
    }

    #endregion

    #region Currency Sprite
    public static string TMPro_GetEmojiCurrency(string currency)
    {
        if (currency.Contains("_") && !currency.Contains("EC"))
        {
            currency = currency.Split('_')[1];
        }
        return string.Format("<sprite name=S_{0}>", currency);
    }

    public static string TMPro_GetEmojiCurrency(VirtualCurrency currency)
    {
        return string.Format("<sprite name=S_{0}>", currency.ToString());
    }

    #endregion

    #region UI Helper
    /// <summary>
    /// Must have component "CanvasGroup" in target game object.
    /// </summary>
    /// <param name="gameObject"></param>
    public static Sequence UITransition_FadeIn(GameObject gameObject, UnityAction onComplete = null)
    {
        float duration = 0.3f;
        if (gameObject == null)
            return null;

        CanvasGroup canvasGroup = gameObject.GetComponent<CanvasGroup>();

        if (canvasGroup != null)
        {
            canvasGroup.interactable = true;
            Sequence sq = DOTween.Sequence();

            gameObject.SetActive(true);
            canvasGroup.alpha = 0;

            sq.Append(canvasGroup.DOFade(1.0f, duration));

            sq.OnComplete(delegate
                        {
                            if (onComplete != null)
                            {
                                onComplete.Invoke();
                            }
                        }
            );

            return sq;
        }
        else
        {
            gameObject.SetActive(true);
            Debug.LogWarning("No Canvas Group in " + gameObject.name);
            if (onComplete != null)
            {
                onComplete.Invoke();
            }
            return null;
        }
    }

    /// <summary>
    /// Must have component "CanvasGroup" in target game object.
    /// </summary>
    /// <param name="gameObject"></param>
    public static Sequence UITransition_FadeOut(GameObject gameObject, UnityAction onComplete = null)
    {
        float duration = 0.15f;
        if (gameObject == null)
            return null;

        CanvasGroup canvasGroup = gameObject.GetComponent<CanvasGroup>();

        if (canvasGroup != null)
        {
            canvasGroup.interactable = false;
            Sequence sq = DOTween.Sequence();

            sq.Append(canvasGroup.DOFade(0.0f, duration));

            sq.OnComplete(delegate
            {
                gameObject.SetActive(false);
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            });

            return sq;
        }
        else
        {
            gameObject.SetActive(false);
            if (onComplete != null)
            {
                onComplete.Invoke();
            }
            Debug.LogWarning("No Canvas Group in " + gameObject.name);
            return null;
        }
    }

    /// <summary>
    /// Must have component "CanvasGroup" in target game object.
    /// </summary>
    /// <param name="gameObject"></param>
    public static Sequence UITransition_ZoomFadeIn(GameObject gameObject, UnityAction onComplete = null)
    {
        float duration = 0.3f;
        float scale = 0.8f;
        if (gameObject == null)
            return null;

        CanvasGroup canvasGroup = gameObject.GetComponent<CanvasGroup>();

        if (canvasGroup != null)
        {
            Sequence sq = DOTween.Sequence();
            gameObject.SetActive(true);

            gameObject.transform.localScale = new Vector3(scale, scale, scale);
            sq.Append(gameObject.transform.DOScale(1.0f, duration).SetEase(Ease.OutExpo));

            canvasGroup.alpha = 0;
            sq.Join(canvasGroup.DOFade(1.0f, duration));

            sq.OnComplete(delegate
            {
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            });

            return sq;
        }
        else
        {
            gameObject.SetActive(true);
            Debug.LogWarning("No Canvas Group in " + gameObject.name);
            if (onComplete != null)
            {
                onComplete.Invoke();
            }
            return null;
        }
    }

    /// <summary>
    /// Must have component "CanvasGroup" in target game object.
    /// </summary>
    /// <param name="gameObject"></param>
    public static Sequence UITransition_ZoomFadeOut(GameObject gameObject, UnityAction onComplete = null)
    {
        float duration = 0.3f;
        float scale = 0.8f;
        if (gameObject == null)
            return null;

        CanvasGroup canvasGroup = gameObject.GetComponent<CanvasGroup>();

        if (canvasGroup != null)
        {
            Sequence sq = DOTween.Sequence();
            sq.Append(gameObject.transform.DOScale(scale, duration).SetEase(Ease.OutExpo));

            sq.Join(canvasGroup.DOFade(0.0f, duration));

            sq.OnComplete(delegate
            {
                gameObject.SetActive(false);
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            });

            return sq;
        }
        else
        {
            gameObject.SetActive(false);
            Debug.LogWarning("No Canvas Group in " + gameObject.name);
            if (onComplete != null)
            {
                onComplete.Invoke();
            }
            return null;
        }
    }

    /// <summary>
    /// Must have component "CanvasGroup" in target game object.
    /// </summary>
    /// <param name="group">Gameobject parent.</param>
    /// <param name="mainWindow">Assign object that need to play move transition.</param>
    /// <param name="x">Offset value from [-1] to [1].</param>
    /// <param name="y">Offset value from [-1] to [1].</param>
    public static Sequence UITransition_FadeMoveIn(GameObject group, GameObject mainWindow, float x = 0.0f, float y = -0.5f, float duration = 0.3f, UnityAction onComplete = null)
    {
        if (group == null)
            return null;

        UITransitionData uiData = mainWindow.GetComponent<UITransitionData>();
        if (uiData == null)
        {
            uiData = mainWindow.AddComponent<UITransitionData>();
            uiData.Init();
        }

        // Calculate start position
        RectTransform rectWindow = mainWindow.GetComponent<RectTransform>();
        Vector2 windowSize = rectWindow.RectTransformGetSize();
        float offsetX = uiData.OriginalPosition.x + (windowSize.x * x);
        float offsetY = uiData.OriginalPosition.y + (windowSize.y * y);

        Vector2 movedPosition = new Vector2(offsetX, offsetY);
        Vector2 originalPosition = uiData.OriginalPosition;

        CanvasGroup canvasGroup = group.GetComponent<CanvasGroup>();
        Sequence sq = DOTween.Sequence();

        if (canvasGroup != null)
        {
            group.SetActive(true);
            canvasGroup.alpha = 0;

            sq.Append(canvasGroup.DOFade(1.0f, duration));

            sq.OnComplete(delegate
            {
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            });
        }
        else
        {
            group.SetActive(true);
            Debug.LogWarning("No Canvas Group in " + group.name);
            if (onComplete != null)
            {
                onComplete.Invoke();
            }
            return null;
        }

        // Animate
        mainWindow.GetComponent<RectTransform>().anchoredPosition = movedPosition;
        sq.Join(mainWindow.GetComponent<RectTransform>().DOAnchorPos(originalPosition, duration).SetEase(Ease.OutExpo));

        return sq;
    }

    /// <summary>
    /// Must have component "CanvasGroup" in target game object.
    /// </summary>
    /// <param name="group">Gameobject parent.</param>
    /// <param name="mainWindow">Assign object that need to play move transition.</param>
    /// <param name="x">Offset value from [-1] to [1].</param>
    /// <param name="y">Offset value from [-1] to [1].</param>
    public static Sequence UITransition_FadeMoveOut(GameObject group, GameObject mainWindow, float x = 0.0f, float y = -0.5f, float duration = 0.3f, UnityAction onComplete = null)
    {
        if (group == null)
            return null;

        UITransitionData uiData = mainWindow.GetComponent<UITransitionData>();
        if (uiData == null)
        {
            uiData = mainWindow.AddComponent<UITransitionData>();
            uiData.Init();
        }

        // Calculate start position
        RectTransform rectWindow = mainWindow.GetComponent<RectTransform>();
        Vector2 windowSize = rectWindow.RectTransformGetSize();
        float offsetX = uiData.OriginalPosition.x + (windowSize.x * x);
        float offsetY = uiData.OriginalPosition.y + (windowSize.y * y);

        Vector2 movedPosition = new Vector2(offsetX, offsetY);
        Vector2 originalPosition = uiData.OriginalPosition;

        CanvasGroup canvasGroup = group.GetComponent<CanvasGroup>();

        Sequence sq = DOTween.Sequence();

        if (canvasGroup != null)
        {
            canvasGroup.alpha = 1;

            sq.Append(canvasGroup.DOFade(0.0f, duration).OnComplete(delegate
            {
                group.SetActive(false);
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            }));
        }
        else
        {
            group.SetActive(false);
            Debug.LogWarning("No Canvas Group in " + group.name);
            if (onComplete != null)
            {
                onComplete.Invoke();
            }
            return null;
        }

        sq.Join(mainWindow.GetComponent<RectTransform>().DOAnchorPos(movedPosition, duration).SetEase(Ease.OutExpo).OnComplete(delegate
        {
            mainWindow.GetComponent<RectTransform>().anchoredPosition = originalPosition;
        }));

        return sq;
    }

    /// </summary>
    /// Must have component "CanvasGroup" in target gameObject.
    /// </summary>
    /// <param name="gameObject"></param>
    public static Sequence UITransition_Shrink(GameObject gameObject, UnityAction onComplete = null)
    {
        float duration = 0.3f;
        if (gameObject == null)
            return null;

        CanvasGroup canvasGroup = gameObject.GetComponent<CanvasGroup>();

        if (canvasGroup != null)
        {
            Sequence sq = DOTween.Sequence();

            Vector3 oriScale = canvasGroup.transform.localScale;
            sq.Append(canvasGroup.transform.DOScale(0.0f, duration));

            sq.OnComplete(delegate
            {
                gameObject.SetActive(false);
                canvasGroup.transform.localScale = oriScale;
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            });

            return sq;
        }
        else
        {
            gameObject.SetActive(false);
            Debug.LogWarning("No Canvas Group in " + gameObject.name);
            if (onComplete != null)
            {
                onComplete.Invoke();
            }
            return null;
        }
    }

    public static Sequence UITransition_Fade_DynamicMoveIn(GameObject group, GameObject mainWindow, float x = 0.5f, float y = 0.7f, float duration = 0.3f, UnityAction onComplete = null)
    {
        if (group == null)
            return null;

        Vector2 savePivot = mainWindow.GetComponent<RectTransform>().pivot;
        CanvasGroup canvasGroup = group.GetComponent<CanvasGroup>();

        Sequence sq = DOTween.Sequence();

        if (canvasGroup != null)
        {
            group.SetActive(true);
            canvasGroup.alpha = 0;

            sq.Append(canvasGroup.DOFade(1.0f, duration));

            sq.OnComplete(delegate
            {
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            });
        }
        else
        {
            group.SetActive(true);
            Debug.LogWarning("No Canvas Group in " + group.name);
            if (onComplete != null)
            {
                onComplete.Invoke();
            }
            return null;
        }

        mainWindow.GetComponent<RectTransform>().pivot = new Vector2(x, y);
        sq.Join(mainWindow.GetComponent<RectTransform>().DOPivot(savePivot, duration).SetEase(Ease.OutExpo));

        return sq;
    }

    public static Sequence UIAnimation_Bounce(GameObject group, float maxScale = 1.5f,  float inDuration = 0.3f, float outDuration = 0.3f)
    {
        float startScale = 1.0f;
        Sequence sq = DOTween.Sequence();
        sq.Append(group.transform.DOScale(maxScale, inDuration));
        sq.Append(group.transform.DOScale(startScale, outDuration));
        return sq;
    }

    public static IEnumerator UITextTypingEffect(TextMeshProUGUI textMesh)
    {
        int counter = 0;
        int characterCount = textMesh.text.Length;
        textMesh.maxVisibleCharacters = 0;

        while (counter <= characterCount)
        {
            textMesh.maxVisibleCharacters = counter;
            counter++;
            yield return new WaitForSecondsRealtime(0.02f);
        }
    }

    public static void UITextTypingEffect_Skip(TextMeshProUGUI textMesh)
    {
        textMesh.maxVisibleCharacters = 99999;
    }

    public static void RectUI_SetAndStretchToParentSize(RectTransform rectTransform)
    {
        rectTransform.pivot = new Vector2(0.5f, 0.5f);
        rectTransform.anchorMin = new Vector2(0.0f, 0.0f);
        rectTransform.anchorMax = new Vector2(1.0f, 1.0f);
        rectTransform.sizeDelta = rectTransform.parent.GetComponent<RectTransform>().sizeDelta;
    }

    public static Vector2 RectUI_GetScreenAnchor(ScreenAnchor anchor)
    {
        Vector2 anchorPt = Vector2.zero;

        switch (anchor)
        {
            case ScreenAnchor.TopLeft:
                {
                    anchorPt = new Vector2(0, 1);
                    break;
                }
            case ScreenAnchor.TopCenter:
                {
                    anchorPt = new Vector2(0.5f, 1);
                    break;
                }
            case ScreenAnchor.TopRight:
                {
                    anchorPt = new Vector2(1, 1);
                    break;
                }

            case ScreenAnchor.MidLeft:
                {
                    anchorPt = new Vector2(0, 0.5f);
                    break;
                }
            case ScreenAnchor.MidCenter:
                {
                    anchorPt = new Vector2(0.5f, 0.5f);
                    break;
                }
            case ScreenAnchor.MidRight:
                {
                    anchorPt = new Vector2(1, 0.5f);
                    break;
                }

            case ScreenAnchor.BotLeft:
                {
                    anchorPt = new Vector2(0, 0);
                    break;
                }
            case ScreenAnchor.BotCenter:
                {
                    anchorPt = new Vector2(0.5f, 0);
                    break;
                }
            case ScreenAnchor.BotRight:
                {
                    anchorPt = new Vector2(1, 0);
                    break;
                }
        }

        return anchorPt;
    }

    public static void Layout_ForceUpdate(RectTransform rectTransform)
    {
        LayoutRebuilder.ForceRebuildLayoutImmediate(rectTransform);
    }

    public static bool IsVector2InsideRect(Vector2 vector, RectTransform rect)
    {
        try
        {
            Vector3[] r_corners = new Vector3[4];
            rect.GetWorldCorners(r_corners);

            float min_x = float.MaxValue;
            float min_y = float.MaxValue;
            float max_x = float.MinValue;
            float max_y = float.MinValue;

            foreach (Vector3 corner in r_corners)
            {
                min_x = Mathf.Min(min_x, corner.x);
                min_y = Mathf.Min(min_y, corner.y);
                max_x = Mathf.Max(max_x, corner.x);
                max_y = Mathf.Max(max_y, corner.y);
            }

            if (vector.x < min_x || vector.x > max_x)
            {
                return false;
            }
            else if (vector.y < min_y || vector.y > max_y)
            {
                return false;
            }

            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }

    public static bool IsRectInsideRect(RectTransform r1, RectTransform r2)
    {
        // [[r1] r2]

        try
        {
            Vector3[] r1_corners = new Vector3[4];
            r1.GetWorldCorners(r1_corners);

            Vector3[] r2_corners = new Vector3[4];
            r2.GetWorldCorners(r2_corners);

            float min_x = float.MaxValue;
            float min_y = float.MaxValue;
            float max_x = float.MinValue;
            float max_y = float.MinValue;

            foreach (Vector3 corner in r2_corners)
            {
                min_x = Mathf.Min(min_x, corner.x);
                min_y = Mathf.Min(min_y, corner.y);
                max_x = Mathf.Max(max_x, corner.x);
                max_y = Mathf.Max(max_y, corner.y);
            }

            foreach (Vector3 corner in r1_corners)
            {
                if (corner.x < min_x || corner.x > max_x)
                {
                    return false;
                }
                else if (corner.y < min_y || corner.y > max_y)
                {
                    return false;
                }
            }

            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }

    public static bool IsRectTouchRect(RectTransform r1, RectTransform r2)
    {
        // [r1[]r2]
        return _CalIsRectInsideRect(r1, r2) || _CalIsRectInsideRect(r2, r1) || _CalIsLineIntersect(r1, r2);
    }

    private static bool _CalIsRectInsideRect(RectTransform r1, RectTransform r2)
    {
        // [r1[]r2]

        try
        {
            Vector3[] r1_corners = new Vector3[4];
            r1.GetWorldCorners(r1_corners);

            Vector3[] r2_corners = new Vector3[4];
            r2.GetWorldCorners(r2_corners);

            float min_x = float.MaxValue;
            float min_y = float.MaxValue;
            float max_x = float.MinValue;
            float max_y = float.MinValue;

            foreach (Vector3 corner in r2_corners)
            {
                min_x = Mathf.Min(min_x, corner.x);
                min_y = Mathf.Min(min_y, corner.y);
                max_x = Mathf.Max(max_x, corner.x);
                max_y = Mathf.Max(max_y, corner.y);
            }

            foreach (Vector3 corner in r1_corners)
            {
                if (corner.x >= min_x && corner.x <= max_x)
                {
                    if (corner.y >= min_y && corner.y <= max_y)
                    {
                        return true;
                    }
                }
            }

            return false;
        }
        catch (Exception e)
        {
            return false;
        }
    }

    private static bool _CalIsLineIntersect(RectTransform r1, RectTransform r2)
    {
        // [r1[]r2]
        Vector3[] r1_corners = new Vector3[4];
        r1.GetWorldCorners(r1_corners);

        Vector3[] r2_corners = new Vector3[4];
        r2.GetWorldCorners(r2_corners);

        Vector2 intersectPoint = Vector2.zero;
        Vector2 p1_start = Vector2.zero;
        Vector2 p1_end = Vector2.zero;
        Vector2 p2_start = Vector2.zero;
        Vector2 p2_end = Vector2.zero;

        for (int i = 0; i < r1_corners.Length; ++i)
        {
            p1_start = new Vector2(r1_corners[i].x, r1_corners[i].y);
            p1_end = Vector2.zero;
            if (i + 1 < r1_corners.Length)
            {
                p1_end = new Vector2(r1_corners[i + 1].x, r1_corners[i + 1].y);
            }
            else
            {
                p1_end = new Vector2(r1_corners[0].x, r1_corners[0].y);
            }

            for (int j = 0; j < r2_corners.Length; ++j)
            {
                p2_start = new Vector2(r2_corners[j].x, r2_corners[j].y);
                p2_end = Vector2.zero;
                if (j + 1 < r2_corners.Length)
                {
                    p2_end = new Vector2(r2_corners[j + 1].x, r2_corners[j + 1].y);
                }
                else
                {
                    p2_end = new Vector2(r2_corners[0].x, r2_corners[0].y);
                }

                if (LineSegementsIntersect(p1_start, p1_end, p2_start, p2_end, out intersectPoint))
                {
                    return true;
                }
            }
        }

        return false;
    }

    /// <summary>
    /// Test whether two line segments intersect. If so, calculate the intersection point.
    /// <see cref="http://stackoverflow.com/a/14143738/292237"/>
    /// </summary>
    /// <param name="p1_start">Vector to the start point of p.</param>
    /// <param name="p1_end">Vector to the end point of p.</param>
    /// <param name="p2_start">Vector to the start point of q.</param>
    /// <param name="p2_end">Vector to the end point of q.</param>
    /// <param name="intersection">The point of intersection, if any.</param>
    /// </param>
    /// <returns>True if an intersection point was found.</returns>
    public static bool LineSegementsIntersect(Vector2 p1_start, Vector2 p1_end, Vector2 p2_start, Vector2 p2_end, out Vector2 intersection)
    {
        intersection = new Vector2();

        Vector2 r = p1_end - p1_start;
        Vector2 s = p2_end - p2_start;
        float rxs = r.Cross(s);
        float qpxr = (p2_start - p1_start).Cross(r);

        // If r x s = 0 and (q - p) x r = 0, then the two lines are collinear.
        if (rxs.IsZero() && qpxr.IsZero())
        {
            // 2. If neither 0 <= (q - p) * r = r * r nor 0 <= (p - q) * s <= s * s
            // then the two lines are collinear but disjoint.
            // No need to implement this expression, as it follows from the expression above.
            return false;
        }

        // 3. If r x s = 0 and (q - p) x r != 0, then the two lines are parallel and non-intersecting.
        if (rxs.IsZero() && !qpxr.IsZero())
            return false;

        // t = (q - p) x s / (r x s)
        float t = (p2_start - p1_start).Cross(s) / rxs;

        // u = (q - p) x r / (r x s)

        float u = (p2_start - p1_start).Cross(r) / rxs;

        // 4. If r x s != 0 and 0 <= t <= 1 and 0 <= u <= 1
        // the two line segments meet at the point p + t r = q + u s.
        if (!rxs.IsZero() && (0 <= t && t <= 1) && (0 <= u && u <= 1))
        {
            // We can calculate the intersection point using either t or u.
            intersection = p1_start + (r * t);

            // An intersection was found.
            return true;
        }

        // 5. Otherwise, the two line segments are not parallel but do not intersect.
        return false;
    }

    public static float Cross(this Vector2 v1, Vector2 v2)
    {
        return (v1.x * v2.y) - (v1.y * v2.x);
    }

    public static bool IsZero(this float value)
    {
        return (Math.Abs(value) < Mathf.Epsilon);
    }

    public static bool IsCardKey(string key)
    {
        // Check if key start with 'C' and also follow by number like "C0001"
        if (key.StartsWith("C") && int.TryParse(key[1].ToString(), out int result))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    #region Extensions
    public static Vector2 RectTransformGetSize(this RectTransform rt)
    {
        return rt.rect.size;
    }

    public static void RectTransformSetLeft(this RectTransform rt, float left)
    {
        rt.offsetMin = new Vector2(left, rt.offsetMin.y);
    }

    public static void RectTransformSetRight(this RectTransform rt, float right)
    {
        rt.offsetMax = new Vector2(-right, rt.offsetMax.y);
    }

    public static void RectTransformSetTop(this RectTransform rt, float top)
    {
        rt.offsetMax = new Vector2(rt.offsetMax.x, -top);
    }

    public static void RectTransformSetBottom(this RectTransform rt, float bottom)
    {
        rt.offsetMin = new Vector2(rt.offsetMin.x, bottom);
    }

    public static float RectTransformGetLeft(this RectTransform rt)
    {
        return rt.offsetMin.x;
    }

    public static float RectTransformGetRight(this RectTransform rt)
    {
        return -rt.offsetMax.x;
    }

    public static float RectTransformGetTop(this RectTransform rt)
    {
        return -rt.offsetMax.y;
    }

    public static float RectTransformGetButtom(this RectTransform rt)
    {
        return rt.offsetMin.y;
    }

    public static void SetActiveFade(this GameObject gameObj, bool isActive, UnityAction onComplete = null)
    {
        if (isActive)
        {
            UITransition_FadeIn(gameObj, onComplete);
        }
        else
        {
            UITransition_FadeOut(gameObj, onComplete);
        }

    }

    public static void ScrollToTop(this ScrollRect scrollRect)
    {
        scrollRect.normalizedPosition = new Vector2(0, 1);
    }
    public static void ScrollToBottom(this ScrollRect scrollRect)
    {
        scrollRect.normalizedPosition = new Vector2(0, 0);
    }
    #endregion

    #region ParseData

    public static PlayerActiveStatus ToPlayerServerStatus(this PlayerActivity activity)
    {
        string activityStr = activity.ToString().ToLower();
        if (activityStr.Contains("in_"))
            return PlayerActiveStatus.Online;
        else if (activityStr.Contains("play_"))
            return PlayerActiveStatus.Playing;
        else
            return PlayerActiveStatus.Offline;
    }

    #endregion

    #region Name
    private static NameValidCase CheckValidName(string name)
    {
        if (string.IsNullOrEmpty(name)) { return NameValidCase.Empty; }

        name = name.ToLower();
        if (IsSpecialName(name))
        {
            return NameValidCase.SpecialName;
        }
        else if (IsProfanityWords(name))
        {
            return NameValidCase.ProfanityWords;
        }
        else if (HasSpecialChars(name))
        {
            return NameValidCase.SpecialAlphabet;
        }
        else if (IsNotAlphanumeric(name))
        {
            return NameValidCase.NotAlphanumeric;
        }
        else if (name == DataManager.Instance.PlayerInfo.DisplayName.ToLower())
        {
            return NameValidCase.SameCurrent;
        }
        return NameValidCase.Valid;
    }

    public static bool CheckValidNameContains(string name, params NameValidCase[] nameValidCases)
    {
        NameValidCase validResult;
        return CheckValidNameContains(name, out validResult, nameValidCases);
    }

    public static bool CheckValidNameContains(string name, out NameValidCase validResult, params NameValidCase[] nameValidCases)
    {
        List<NameValidCase> nameValidCaseList = new List<NameValidCase>();
        if (nameValidCases != null && nameValidCases.Length > 0)
        {
            nameValidCaseList = nameValidCases.ToList();
        }

        validResult = CheckValidName(name);
        return nameValidCaseList.Contains(validResult);
    }

    public static bool IsSpecialName(string name)
    {
        string nameLower = name.ToLower();
        string[] specialName = new string[] { "developer", "admin", "athenion", "zerobit" };
        string[] specialStartName = new string[] { "atn_" };
        foreach (string invalidName in specialName)
        {
            if (nameLower.Contains(invalidName))
                return true;
        }

        foreach (string invalidName in specialStartName)
        {
            if (nameLower.StartsWith(invalidName))
                return true;
        }

        return false;
    }

    public static bool IsProfanityWords(string name)
    {
        List<string> words = ProfanityDB.Instance.GetProfanityWords();
        foreach (string word in words)
        {
            if (name.ToLower().Contains(word.ToLower()))
                return true;
        }
        return false;
    }

    public static bool IsNotAlphanumeric(string name)
    {
        Regex r = new Regex("^[a-zA-Z0-9]*$");
        return !r.IsMatch(name);
    }

    public static bool HasSpecialChars(string str)
    {
        return str.Any(ch => !Char.IsLetterOrDigit(ch));
    }
    #endregion

    #region Event Icon
    public static string GetEventThemeKey()
    {
        return DataManager.Instance.GetGameEventDB().GetCurrentTheme().Theme;
    }
    #endregion

    public static int CreateEncodeKey(int value_1, int value_2)
    {
        int result = value_1 + value_2;

        return result;
    }

    public static int EncodeInt(int originalValue, int key)
    {
        int encodeValue = (originalValue ^ _encodeIntKey);

        return encodeValue;
    }

    public static int DecodeInt(int encodeValue, int key)
    {
        int originalValue = (encodeValue ^ _encodeIntKey);

        return originalValue;
    }

    public static string ConvertIntToCustomStr(int value)
    {
        string hexStr = value.ToString("X").ToUpper();
        string customStr = "";

        foreach (char c in hexStr)
        {
            switch (c)
            {
                case '0': customStr += "ฑ"; break;
                case '1': customStr += "ฒ"; break;
                case '2': customStr += "ฬ"; break;
                case '3': customStr += "ฃ"; break;
                case '4': customStr += "ซ"; break;
                case '5': customStr += "ฌ"; break;
                case '6': customStr += "ฮ"; break;
                case '7': customStr += "ฅ"; break;
                case '8': customStr += "ญ"; break;
                case '9': customStr += "ฆ"; break;
                case 'A': customStr += "ฎ"; break;
                case 'B': customStr += "ศ"; break;
                case 'C': customStr += "ฏ"; break;
                case 'D': customStr += "ษ"; break;
                case 'E': customStr += "ฐ"; break;
                case 'F': customStr += "ส"; break;
            }
        }

        return customStr;
    }

    public static int ConvertCustomStrToInt(string customStr)
    {
        string hexStr = "";

        foreach (char c in customStr)
        {
            switch (c)
            {
                case 'ฑ': hexStr += "0"; break;
                case 'ฒ': hexStr += "1"; break;
                case 'ฬ': hexStr += "2"; break;
                case 'ฃ': hexStr += "3"; break;
                case 'ซ': hexStr += "4"; break;
                case 'ฌ': hexStr += "5"; break;
                case 'ฮ': hexStr += "6"; break;
                case 'ฅ': hexStr += "7"; break;
                case 'ญ': hexStr += "8"; break;
                case 'ฆ': hexStr += "9"; break;
                case 'ฎ': hexStr += "A"; break;
                case 'ศ': hexStr += "B"; break;
                case 'ฏ': hexStr += "C"; break;
                case 'ษ': hexStr += "D"; break;
                case 'ฐ': hexStr += "E"; break;
                case 'ส': hexStr += "F"; break;
            }
        }

        int value = int.Parse(hexStr, System.Globalization.NumberStyles.HexNumber);
        return value;
    }

    public static void ForceRebuildAllLayoutImmediate(RectTransform root)
    {
        LayoutRebuilder.ForceRebuildLayoutImmediate(root);

        //List<RectTransform> rects = new List<RectTransform>();
        if (root.childCount > 0)
        {
            for (int index = 0; index < root.childCount; ++index)
            {
                RectTransform child = root.GetChild(index) as RectTransform;
                ForceRebuildAllLayoutImmediate(child);
            }
        }
    }

    #region Convert
    public static byte ConvertIntToByte(int value)
    {
        byte value_byte = Convert.ToByte(value);
        return value_byte;
    }

    public static int ConvertByteToInt(byte value_byte)
    {
        int value = Convert.ToInt32(value_byte);
        return value;
    }

    public static byte ConvertPlayerIndexToByte(PlayerIndex value)
    {
        byte value_byte = ConvertIntToByte((int)value);
        return value_byte;
    }

    public static byte ConvertGamePhaseToByte(GamePhase value)
    {
        byte value_byte = ConvertIntToByte((int)value);
        return value_byte;
    }

    public static PlayerIndex ConvertByteToPlayerIndex(byte value_byte)
    {
        PlayerIndex value = (PlayerIndex)ConvertByteToInt(value_byte);
        return value;
    }

    public static GamePhase ConvertByteToGamePhase(byte value_byte)
    {
        GamePhase value = (GamePhase)ConvertByteToInt(value_byte);
        return value;
    }

    public static string ConvertIntToHexString(int number, int hexDigit)
    {
        if (number >= Math.Pow(16, hexDigit))
        {
            throw new Exception("Input number is more than target hex digit");
        }

        return number.ToString("X" + hexDigit.ToString());
    }

    public static int ConvertHexStringToInt(string hexString)
    {
        //Allow 0-9, a-f, A-F & length > 0
        if (!Regex.IsMatch(hexString, "^[0-9A-Fa-f]+$"))
        {
            throw new Exception("Invalid input format");
        }

        return int.Parse(hexString, System.Globalization.NumberStyles.HexNumber);
    }
    #endregion

    #region Release
    public static void DestroySingletons()
    {
        DataManager.Instance.DestroySelf();
    }
    #endregion
}

[System.Serializable]
public class IntEvent : UnityEvent<int>
{
}

[System.Serializable]
public class RequesterEvent : UnityEvent<Requester>
{
}

[System.Serializable]
public class StringEvent : UnityEvent<string>
{
}

[System.Serializable]
public class CloudScriptResultProcessEvent : UnityEvent<CloudScriptResultProcess>
{
}

[System.Serializable]
public class FloatEvent : UnityEvent<float>
{
}

[System.Serializable]
public class BoolEvent : UnityEvent<bool>
{
}

[System.Serializable]
public class PlayFabLoadEvent : UnityEvent<UnityAction, UnityAction<string>>
{
}

[System.Serializable]
public class TParamEvent<TParam> : UnityEvent<TParam>
{
}

[System.Serializable]
public class LoginResultEvent : UnityEvent<PlayFab.ClientModels.LoginResult>
{
}

[System.Serializable]
public class PlayFabErrorEvent : UnityEvent<PlayFab.PlayFabError>
{
}

[System.Serializable]
public class PointerEventDataEvent : UnityEvent<PointerEventData>
{
}
#endregion