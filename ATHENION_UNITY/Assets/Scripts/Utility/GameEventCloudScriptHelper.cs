﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEventCloudScriptHelper
{
    private static readonly string EndMatchCSFunctionName = "CheckGameEventStatChange";

    public static void EndMatchExecute(GameMode mode)
    {
        if(mode == GameMode.Event)
            DataManager.Instance.ExcecuteEventCloudScript(EndMatchCSFunctionName, null, null, null);
    }
}
