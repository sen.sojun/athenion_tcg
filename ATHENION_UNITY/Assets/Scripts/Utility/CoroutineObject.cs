﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CoroutineObject : MonoBehaviour
{
    private List<IEnumerator> _taskList = null;

    /*
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }
    */

    public void StartTask(IEnumerator task)
    {
        _taskList = new List<IEnumerator>();
        _taskList.Add(task);

        StartCoroutine(DoingTask());
    }

    public void StartTask(List<IEnumerator> taskList)
    {
        _taskList = new List<IEnumerator>(taskList);

        StartCoroutine(DoingTask());
    }

    private IEnumerator DoingTask()
    {
        IEnumerator task = null;

        for(int index = 0; index < _taskList.Count; ++index)
        {
            task = _taskList[index];
            yield return task;
        }

        Destroy(gameObject);
        yield break;
    }
}
