﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using JetBrains.Annotations;
using Karamucho;
using UnityEngine.Events;
using UnityEngine.Video;

/// <summary>
/// Authour : Chinnawat Sirima.<para/>
/// Description : Control Video player.
/// </summary>
[RequireComponent(typeof(VideoPlayer))]
[RequireComponent(typeof(AudioSource))]
public class VideoReader : MonoBehaviour
{
    [SerializeField] private long _frame;
    [SerializeField] private double _time;
    public bool Loop;
    public bool Mute;
    public bool PlayOnAwake;
    private VideoPlayer _videoPlayer;
    private AudioSource _audioSource;
    public Action OnVideoPlayed;
    public Action OnVideoFinished;

    public bool IsPlaying => _videoPlayer.isPlaying;
    public long FrameCount => System.Convert.ToInt64(_videoPlayer.frameCount);
    public long CurrentFrame => _videoPlayer.frame;

    public delegate void VideoAtSecond(double second);
    public static event VideoAtSecond OnThisVideoPlaying;

    public delegate void VideoAction();
    public static event VideoAction OnVideoEnd;

    public float speed
    {
        get => _videoPlayer.playbackSpeed;
        set => _videoPlayer.playbackSpeed = value;
    }

    public double CurrentTime => _videoPlayer.time;

    public void Init(UnityAction onComplete)
    {
        _videoPlayer = GetComponent<VideoPlayer>();
        _audioSource = GetComponent<AudioSource>();
        _videoPlayer.isLooping = Loop;

        onComplete?.Invoke();
    }
    void OnEnable()
    {
        _videoPlayer.loopPointReached += VideoPlayer_loopPointReached;
        _videoPlayer.prepareCompleted += VideoPlayer_prepareCompleted;
    }
    void OnDisable()
    {
        _videoPlayer.loopPointReached -= VideoPlayer_loopPointReached;
        _videoPlayer.prepareCompleted -= VideoPlayer_prepareCompleted;
        if (_videoPlayer.targetTexture != null)
            _videoPlayer.targetTexture.Release();
    }

    private void Start()
    {
        if (PlayOnAwake)
        {
            if (_videoPlayer.clip != null)
                Play();
            else
                print("Please Insert VideoClip.");
        }
    }
    #region Methods
    public void Play()
    {
        if (_videoPlayer.isPlaying == false || _videoPlayer.playbackSpeed == 0)
        {
            _videoPlayer.playbackSpeed = 1;
            _videoPlayer.Play();
        }
    }
    public void Play(string path)
    {
        if (!_videoPlayer.isPlaying || _videoPlayer.playbackSpeed == 0 || _videoPlayer.url != path)
        {
            _videoPlayer.source = VideoSource.Url;
            if (_videoPlayer.targetTexture != null)
                _videoPlayer.targetTexture.Release();

            //set audio.
            if (Mute)
            {
                _videoPlayer.audioOutputMode = VideoAudioOutputMode.None;
                _videoPlayer.controlledAudioTrackCount = 0;
                _videoPlayer.EnableAudioTrack(0, false);
            }
            else
            {
                _videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;
                _videoPlayer.controlledAudioTrackCount = 1;
                _videoPlayer.EnableAudioTrack(0, true);
                _videoPlayer.SetTargetAudioSource(0, _audioSource);
            }

            //set video
            _videoPlayer.playbackSpeed = 1;
            _videoPlayer.url = path;
            _videoPlayer.Prepare();
            //videoPlayer.url = Application.dataPath + "/../output/" + fileName + ".mp4";
        }
    }
    private void Update()
    {
        _frame = CurrentFrame;
        _time = CurrentTime;

        //Debug.Log($"Frame: {_frame}");
        //Debug.Log($"Time: {_time}");

        if (_videoPlayer != null)
        {
            OnThisVideoPlaying?.Invoke(_time);
        }
    }
    private void Seek(long frame)
    {
        if (CurrentFrame == FrameCount - 1 && frame > FrameCount - 1)
            return;
        if (CurrentFrame == 0 && frame <= 0)
            return;

        if (frame >= FrameCount - 1)
            _videoPlayer.frame = FrameCount - 1;
        else if (frame < 0)
            _videoPlayer.frame = 0;
        else
            _videoPlayer.frame = frame;
    }
    public void Skip(long frameAmount)
    {
        Seek(CurrentFrame + frameAmount);
    }
    public void SkipTo(long frame)
    {
        Seek(frame);
    }
    public void Stop()
    {
        if (_videoPlayer.isPlaying)
            _videoPlayer.Stop();
    }
    public void Pause()
    {
        if (_videoPlayer.isPlaying)
            _videoPlayer.Pause();
    }

    public void SetVideo(string videoName)
    {
        if(_videoPlayer == null)
            _videoPlayer = GetComponent<VideoPlayer>();
        _videoPlayer.clip = ResourceManager.Load<VideoClip>($"Video/{videoName}");
    }
    public void Looping(bool isLoop)
    {
        Loop = isLoop;
    }
    public VideoClip GetVideoClip()
    {
        return _videoPlayer.clip;
    }
    #endregion

    #region Video Callback
    private void VideoPlayer_loopPointReached(VideoPlayer source)
    {
        if (Loop == false)
        {
            _videoPlayer.Pause();
            OnVideoEnd?.Invoke();
        }
        //if (OnVideoFinished != null)
        //    OnVideoFinished();
    }

    private void VideoPlayer_prepareCompleted(VideoPlayer source)
    {
        _videoPlayer.Play();
        if (OnVideoPlayed != null)
            OnVideoPlayed();
    }
    
    #endregion
}
