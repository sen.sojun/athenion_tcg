﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlexiblePooling
{
    private List<GameObject> objects = new List<GameObject>();
    private GameObject prefab;
    private Transform transform;

    public FlexiblePooling(Transform owner, GameObject prefab, int initialAmount)
    {
        this.prefab = prefab;
        this.transform = owner;
        InitialPrefabs(initialAmount);
    }

    private void InitialPrefabs(int initialAmount)
    {
        for (int i = 0; i < initialAmount; i++)
        {
            CreateAndAddToList();
        }
    }

    public void HideAllObject()
    {
        foreach(var item in objects)
        {
            item.SetActive(false);
        }
    }

    public GameObject GetObject()
    {
        var obj = objects.Find(x => (x.activeSelf == false));
        if (obj == null)
            obj = CreateAndAddToList();
        return obj;
    }

    private GameObject CreateAndAddToList()
    {
        GameObject obj;
        if (transform == null)
        {
            obj = UnityEngine.Object.Instantiate(prefab);
        }
        else
        {
            obj = UnityEngine.Object.Instantiate(prefab, transform);
        }
             
        obj.SetActive(false);
        objects.Add(obj);
        return obj;
    }
}