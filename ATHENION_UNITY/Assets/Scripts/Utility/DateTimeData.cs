﻿using System;
using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine.Events;

public class DateTimeData
{
    public static int Offset { get { return _offset; } }

    private static DateTime _referenceTime = DateTime.UtcNow;
    private static DateTime _loginTimeStamp = DateTime.UtcNow;
    private static float _receiveRequestTime = 0.0f;
    private static int _offset;

    public static void LoadServerTimeData(UnityAction onComplete, UnityAction<string> onFail)
    {
        Debug.Log("Start LoadServerTimeData ...");

        float startRequestTime = Time.realtimeSinceStartup;

        PlayFabManager.Instance.GetServerTime(
           delegate (ExecuteCloudScriptResult result)
           {
               CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
               if (resultProcess.IsSuccess)
               {
                   DateTime serverUTCTime = Convert.ToDateTime(resultProcess.CustomData["data"]).ToUniversalTime();
                   _offset = Convert.ToInt32(resultProcess.CustomData["offset"]);

                   _receiveRequestTime = Time.realtimeSinceStartup;
                   SetReferenceTime(serverUTCTime);
                   UpdateLoginTimeStamp();

                   Debug.Log("<color=green>Completed</color> LoadServerTimeData");
                   if (onComplete != null)
                   {
                       onComplete.Invoke();
                   }
               }
               else
               {
                   Debug.LogFormat("<color=red>Failed</color> LoadServerTimeData : {0}", resultProcess.MessageDetail);

                   if (onFail != null)
                   {
                       onFail.Invoke(resultProcess.MessageDetail);
                   }
               }
           }
           , delegate (PlayFabError error)
           {
               Debug.LogFormat("<color=red>Failed</color> LoadServerTimeData : {0}", error.ErrorMessage);

               Debug.Log("Retry LoadServerTimeData ...");
               LoadServerTimeData(onComplete, onFail);
           }
       );
    }

    public static void GetServerTime(UnityAction<DateTime> onComplete, UnityAction<string> onFail)
    {
        float startRequestTime = Time.realtimeSinceStartup;

        PlayFabManager.Instance.GetServerTime(
           delegate (ExecuteCloudScriptResult result)
           {
               CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
               if (resultProcess.IsSuccess)
               {
                   DateTime serverUTCTime = Convert.ToDateTime(resultProcess.CustomData["data"]).ToUniversalTime();

                   if (onComplete != null)
                   {
                       onComplete.Invoke(serverUTCTime);
                   }
               }
               else
               {
                   if (onFail != null)
                   {
                       onFail.Invoke(resultProcess.MessageDetail);
                   }
               }
           }
           , delegate (PlayFabError error)
           {
               Debug.LogError(error.ToString());
               onFail?.Invoke(error.ToString());
           }
       );
    }

    private static void UpdateLoginTimeStamp()
    {
        _loginTimeStamp = GetDateTimeUTC();
    }

    public static DateTime GetLoginTimeStamp()
    {
        return _loginTimeStamp;
    }

    private static void SetReferenceTime(DateTime refTime)
    {
        _referenceTime = refTime;
    }

    public static DateTime GetDateTimeUTC()
    {
        return _referenceTime + TimeSpan.FromSeconds(Time.realtimeSinceStartup - _receiveRequestTime);
    }

    public static DateTime GetDateTimeUTCOffset()
    {
        if (_offset > 0)
        {
            return GetDateTimeUTC().Add(new TimeSpan(_offset, 0, 0));
        }
        else
        {
            return GetDateTimeUTC().Subtract(new TimeSpan(-_offset, 0, 0));
        }
    }

    public static DateTime ConvertToUTCOffset(DateTime utcDateTime)
    {
        TimeSpan timeSpan = new TimeSpan(Math.Abs(_offset), 0, 0);
        if (_offset > 0)
        {
            return utcDateTime.Add(timeSpan);
        }
        else
        {
            if (utcDateTime - DateTime.MinValue >= timeSpan)
            {
                return utcDateTime.Subtract(timeSpan);
            }
            else
            {
                return DateTime.MinValue;
            }
        }
    }

    public static DateTime ConvertOffsetToUTC(DateTime utcOffsetDateTime)
    {
        TimeSpan timeSpan = new TimeSpan(Math.Abs(_offset), 0, 0);
        if (_offset > 0)
        {
            return utcOffsetDateTime.Subtract(timeSpan);
        }
        else
        {
            if (utcOffsetDateTime - DateTime.MinValue >= timeSpan)
            {
                return utcOffsetDateTime.Add(timeSpan);
            }
            else
            {
                return DateTime.MinValue;
            }
        }
    }
}