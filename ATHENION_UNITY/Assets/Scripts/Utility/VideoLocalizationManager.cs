﻿using System.Collections.Generic;

public class VideoLocalizationManager : Singleton<VideoLocalizationManager>
{
    #region Public Properties
    public Language CurrentLanguage => DataManager.Instance.GetLanguage();
    #endregion

    #region Methods
    public Dictionary<double, VideoSubtitleData> GetSubtitle(string key)
    {
        if (GetSubtitle(key, CurrentLanguage, out var result))
        {
            return result;
        }

        return null;
    }

    public bool GetSubtitle(string key, out Dictionary<double, VideoSubtitleData> result)
    {
        return GetSubtitle(key, CurrentLanguage, out result);
    }

    public bool GetSubtitle(string key, Language language, out Dictionary<double, VideoSubtitleData> result)
    {
        bool isSuccess = VideoLocalizationDB.Instance.GetData(key.ToUpper(), out VideoLocalizationDBData data);
        if (isSuccess)
        {
            result = ConvertData(data, language);
            return true;
        }

        result = null;
        return false;
    }

    private Dictionary<double, VideoSubtitleData> ConvertData(VideoLocalizationDBData data, Language language)
    {
        Dictionary<double, VideoSubtitleData> result = new Dictionary<double, VideoSubtitleData>();

        foreach (VideoLocalizationData subTitleData in data.VideoLocalizationDataList)
        {
            VideoSubtitleData newData = new VideoSubtitleData(subTitleData, language);
            result.Add(newData.StartSecond, newData);
        }

        return result;
    }
    #endregion
}
