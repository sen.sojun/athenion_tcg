﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TaskManager : MonoSingleton<TaskManager>
{ 
    public Coroutine StartTask(IEnumerator task, UnityAction onComplete = null)
    {
        return StartCoroutine(DoTask(task, onComplete));
    }

    public void StopTask(Coroutine task)
    {
        StopCoroutine(task);
    }

    public IEnumerator CreateSequence(params IEnumerator[] task)
    {
        yield return CreateSequence(task.ToList());
    }

    public IEnumerator CreateSequence(List<IEnumerator> task)
    {
        for (int i = 0; i < task.Count; i++)
        {
            yield return task[i];
        }
    }

    public IEnumerator CreateParallel(params IEnumerator[] task)
    {
        yield return CreateParallel(task.ToList());
    }

    public IEnumerator CreateParallel(List<IEnumerator> task)
    {
        int taskCount = task.Count;
        for (int i = 0; i < task.Count; i++)
        { 
            StartCoroutine(DoTask(task[i], () => taskCount--));
        }
        yield return new WaitUntil(() => taskCount <= 0);
    }

    IEnumerator DoTask(IEnumerator task, UnityAction onComplete)
    {
        yield return task;
        if (onComplete != null)
            onComplete.Invoke();
    }
}
