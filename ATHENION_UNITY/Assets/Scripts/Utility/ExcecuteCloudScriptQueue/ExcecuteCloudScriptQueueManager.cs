﻿using PlayFab;
using PlayFab.ClientModels;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ExcecuteCloudScriptQueueManager : MonoSingleton<ExcecuteCloudScriptQueueManager>
{
    private List<Command> commandList = new List<Command>();

    public void AddQueue(string methodName, object parameters, UnityAction<CloudScriptResultProcess> onComplete = null, UnityAction<string> onfail = null)
    {
        commandList.Add(new Command(methodName, parameters, onComplete, onfail));
    }

    /// <summary>
    /// If not sure don't call it.
    /// </summary>
    public void ClearCommand()
    {
        StopAllCoroutines();
        commandList.Clear();
    }

    #region Synchronous Excecute Queue

    public void SynchronousExcecute(float delay, UnityAction onComplete = null, UnityAction<string> onFail = null)
    {
        List<Command> cmd = new List<Command>(commandList);
        StartCoroutine(SynchronousExcecuteCommandList(cmd, delay, onComplete, onFail));
        commandList.Clear();
    }

    private IEnumerator SynchronousExcecuteCommandList(List<Command> commandList, float delay, UnityAction onComplete, UnityAction<string> onFail)
    {
        while (commandList.Count > 0)
        {
            yield return commandList[0].Excecute();
            if (commandList[0].HasError)
            {
                onFail?.Invoke(commandList[0].ErrorText);
                yield break;
            }
            commandList.RemoveAt(0);
            yield return new WaitForSeconds(delay);
        }
        onComplete?.Invoke();
        ClearCommand();
    }

    #endregion

    #region Asynchronous Excecute Queue

    public void ASynchronousExcecute(float delay, UnityAction onComplete = null, UnityAction<List<string>> onFail = null)
    {
        StartCoroutine(ASynchronousExcecuteCommandList(commandList, delay, onComplete, onFail));
    }

    private IEnumerator ASynchronousExcecuteCommandList(List<Command> commandList, float delay, UnityAction onComplete, UnityAction<List<string>> onFail)
    {
        List<string> errorList = new List<string>();
        for (int i = 0; i < commandList.Count; i++)
        {
            int index = i;
            commandList[i].AddListener(
                (result) => commandList[index] = null
                , (error) =>
                {
                    commandList[index] = null;
                    errorList.Add(error);
                }
            );
            StartCoroutine(commandList[i].Excecute());
            yield return new WaitForSeconds(delay);
        }

        while (FindNullCommand(commandList) != null)
        {
            yield return new WaitForSeconds(0.1f);
        }

        if (errorList.Count == 0)
        {
            onComplete?.Invoke();
        }
        else
        {
            onFail.Invoke(errorList);
        }
        ClearCommand();
    }

    private static Command FindNullCommand(List<Command> commandList)
    {
        return commandList.Find(cmd => cmd != null);
    }

    #endregion

    private class Command
    {
        private string _methodName;
        private object _parameters;

        public string ErrorText { get; private set; }
        public bool HasError { get => ErrorText == string.Empty; }


        public CloudScriptResultProcessEvent OnComplete
        {
            get
            {
                if (_onComplete == null)
                    _onComplete = new CloudScriptResultProcessEvent();
                return _onComplete;
            }
        }
        private CloudScriptResultProcessEvent _onComplete;

        public StringEvent OnFail
        {
            get
            {
                if (_onFail == null)
                    _onFail = new StringEvent();
                return _onFail;
            }
        }
        private StringEvent _onFail;


        public Command(string methodName, object parameters, UnityAction<CloudScriptResultProcess> onComplete = null, UnityAction<string> onFail = null)
        {
            this._methodName = methodName;
            this._parameters = parameters;

            AddListener(onComplete, onFail);
        }

        public void AddListener(UnityAction<CloudScriptResultProcess> onComplete = null, UnityAction<string> onFail = null)
        {
            if (onComplete != null)
            {
                OnComplete.AddListener(onComplete);
            }
            if (onFail != null)
            {
                OnFail.AddListener(onFail);
            }
        }

        public IEnumerator Excecute()
        {
            bool isFinish = false;
            PlayFabManager.Instance.ExecuteCloudScript(_methodName, _parameters,
                delegate (ExecuteCloudScriptResult result)
                {
                    CloudScriptResultProcess resultProcess = new CloudScriptResultProcess(result);
                    OnComplete?.Invoke(resultProcess);
                    isFinish = true;
                }
                , (error) =>
                {
                    ErrorText = error.ToString();
                    OnFail?.Invoke(error.ToString());
                    isFinish = true;
                }
            );
            yield return new WaitUntil(() => isFinish);
        }
    }
}


