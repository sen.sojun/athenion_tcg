﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenManager
{
    #region Enums
    public enum ScreenSize
    {
          Full
        , Half
        , Quarter
    }
    #endregion

    #region Private Properties
    private static int _width;
    private static int _height;
    private static ScreenOrientation _orientation;

    private static float _ratio = 1.0f;

    private static bool _isCache = false;
    #endregion

    #region Event
    public static event Action OnUpdateScreenRatio;
    #endregion

    #region Methods
    public static void SetScreenSize(float percent)
    {
        if (!_isCache)
        {
            _width = Screen.width;
            _height = Screen.height;

            if (_width > _height)
            {
                _orientation = ScreenOrientation.Landscape;
            }
            else
            {
                _orientation = ScreenOrientation.Portrait;
            }

            _isCache = true;
        }

        #if UNITY_IOS || UNITY_ANDROID
        int width = 0;
        int height = 0;
        if (Screen.width > Screen.height)
        {
            // Landscape
            if (_orientation == ScreenOrientation.Landscape)
            {
                width = Mathf.FloorToInt(_width * percent);
                height = Mathf.FloorToInt(_height * percent);
            }
            else
            {
                width = Mathf.FloorToInt(_height * percent);
                height = Mathf.FloorToInt(_width * percent);
            }
        }
        else
        {
            // Portrait
            if (_orientation == ScreenOrientation.Portrait)
            {
                width = Mathf.FloorToInt(_width * percent);
                height = Mathf.FloorToInt(_height * percent);
                
            }
            else
            {
                width = Mathf.FloorToInt(_height * percent);
                height = Mathf.FloorToInt(_width * percent);
            }
        }

        Screen.SetResolution(width, height, Screen.fullScreen);
        DataManager.Instance.SetScreenRatio(percent);
        Debug.LogFormat("SetScreenSize : {0}x{1}", width, height);
        Debug.LogFormat("ScreenSize : {0}x{1}" , Screen.currentResolution.width, Screen.currentResolution.height);

        OnUpdateScreenRatio?.Invoke();
        #endif
    }

    public static float GetScreenSize()
    {
        return DataManager.Instance.GetScreenRatio();
    }

    public static string GetDebugText()
    {
        return string.Format("{0} x {1}, {2}%"
            , Screen.width
            , Screen.height
            , DataManager.Instance.GetScreenRatio() * 100.0f
        );
    }
    #endregion
}