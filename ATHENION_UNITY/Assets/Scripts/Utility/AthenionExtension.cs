﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public static class AthenionExtension
{
    public static T Find<T>(this T[] array, Predicate<T> predicate)
    {
        for (int i = 0; i < array.Length; i++)
        {
            if (predicate(array[i]))
            {
                return array[i];
            }
        }
        return default(T);
    }

    public static TEnum ToEnum<TEnum>(this string value)
    {
        return (TEnum)Enum.Parse(typeof(TEnum), value);
    }

    /// <summary>
    /// provide time format more than 24 hr.
    /// </summary>
    /// <param name="timeSpan">Time span</param>
    /// <returns>HH:MM:SS</returns>
    public static string ToTimeFormat(this TimeSpan timeSpan)
    {
        return string.Format("{0}:{1}:{2}",
                     timeSpan.TotalHours.ToString("00"),
                     timeSpan.Minutes.ToString("00"),
                     timeSpan.Seconds.ToString("00"));
    }

    public static Vector3 ToVector3(this Vector2 vector)
    {
        return new Vector3(vector.x, vector.y, 0);
    }

    public static List<T> Shuffle<T>(this List<T> list)
    {
        System.Random rnd = new System.Random();
        for (int i = 0; i < list.Count; i++)
        {
            int k = rnd.Next(0, i);
            T value = list[k];
            list[k] = list[i];
            list[i] = value;
        }
        return list;
    }

    public static List<T> CutFirst<T>(this List<T> list, int amount)
    {
        List<T> result = new List<T>();
        for (int i = 0; i < list.Count; i++)
        {
            if (amount > 0)
            {
                result.Add(list[0]);
                list.RemoveAt(0);
                amount--;
                i--;
            }
            else
                break;
        }
        return result;
    }

    public static List<int> ReturnSelectedElements<T>(this T input) where T : Enum
    {
        List<int> selectedElements = new List<int>();
        for (int i = 0; i < System.Enum.GetValues(typeof(T)).Length; i++)
        {
            int layer = 1 << i;
            if ((Convert.ToInt32(input) & layer) != 0)
            {
                selectedElements.Add(i);
            }
        }
        return selectedElements;
    }
}

public class EnumFlagsAttribute : PropertyAttribute
{
    public EnumFlagsAttribute() { }
}