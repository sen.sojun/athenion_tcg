﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;

public class UITabSelector : MonoBehaviour
{
    #region Inspector Properties
    [SerializeField] private List<TMP_InputField> _InputFieldList;
    #endregion

    #region Private Properties
    private int _currentIndex = 0;
    #endregion

    #region Methods
    private void Update()
    {
        _currentIndex = _InputFieldList.FindIndex(x => x.isFocused);

        if(_currentIndex > -1)
        {
            if (Input.GetKeyDown(KeyCode.Tab))
            {
                if (_InputFieldList.Count > _currentIndex + 1)
                {
                    _currentIndex++;
                }
                else
                {
                    _currentIndex = 0;
                }

                EventSystem.current.SetSelectedGameObject(_InputFieldList[_currentIndex].gameObject, null);
            }
        }     
    }
    #endregion
}