﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

namespace TestDeckCode
{
    public class TestDeckCodeManager : MonoBehaviour
    {
        //Static parameters
        public static readonly int FACTION_HEX_DIGIT = 2;
        public static readonly int UNIQUE_CARD_AMOUNT_HEX_DIGIT = 2;
        public static readonly int N_COPY_CARD_AMOUNT_HEX_DIGIT = 2;
        public static readonly int CARD_CODE_HEX_DIGIT = 4;
        public static readonly int CARD_CODE_DB_DIGIT = 4;
        public static readonly int MAXIMUM_DUPLICATE_CARDS = 3;

        public Transform CardDataUIList;
        public CardDataUIObject CardDataUIObjectPrefab;
        public InputField DeckCodeTextField;
        public InputField UnencodedDeckCodeTextField;

        public void AddCardDataUIObject()
        {
            Instantiate(CardDataUIObjectPrefab, CardDataUIList);
        }

        public void GenDeckCodeFromDict()
        {
            DeckCodeTextField.text = DeckCodeManager.GetDeckCode(GenDictFromUI(), CardElementType.DARK);
            byte[] byteData = System.Convert.FromBase64String(DeckCodeTextField.text);
            UnencodedDeckCodeTextField.text = System.Text.ASCIIEncoding.ASCII.GetString(byteData);
        }

        public void GenDeckListFromCode()
        {
            Tuple<CardElementType, Dictionary<string, int>> deckData = DeckCodeManager.DecodeDeckCode(DeckCodeTextField.text);
            Dictionary<string, int> cardDict = deckData.Item2;

            foreach (CardDataUIObject item in CardDataUIList.GetComponentsInChildren<CardDataUIObject>())
            {
                Destroy(item.gameObject);
            }

            foreach (string key in cardDict.Keys)
            {
                CardDataUIObject newCardUIObject = Instantiate(CardDataUIObjectPrefab, CardDataUIList);
                newCardUIObject.CardCode.text = key;
                newCardUIObject.CardAmount.text = cardDict[key].ToString();
            }
        }

        public void TestConvertIntToHexString(int number)
        {
            Debug.Log(GameHelper.ConvertIntToHexString(number, 2));
        }

        public void TestConvertHexStringToInt(string hexString)
        {
            Debug.Log(GameHelper.ConvertHexStringToInt(hexString));
        }

        private Dictionary<string, int> GenDictFromUI()
        {
            //Get all children from cardDataUIList
            CardDataUIObject[] cardDataObjects = CardDataUIList.GetComponentsInChildren<CardDataUIObject>();

            //Loop extract text and put to dict
            Dictionary<string, int> outputDict = new Dictionary<string, int>();
            foreach (var item in cardDataObjects)
            {
                outputDict[item.CardCode.text] = int.Parse(item.CardAmount.text);
            }

            return outputDict;
        }
    }
}
