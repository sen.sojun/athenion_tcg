﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace TestDeckCode
{
    public class CardDataUIObject : MonoBehaviour
    {
        public InputField CardCode;
        public InputField CardAmount;

        public void RemoveThis()
        {
            Destroy(gameObject);
        }
    }
}
