﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;

public class DeckCodeManager
{
    #region Public Properties
    public static readonly int FACTION_HEX_DIGIT = 2;
    public static readonly int UNIQUE_CARD_AMOUNT_HEX_DIGIT = 2;
    public static readonly int N_COPY_CARD_AMOUNT_HEX_DIGIT = 2;
    public static readonly int CARD_CODE_HEX_DIGIT = 4;
    public static readonly int CARD_CODE_DB_DIGIT = 4;
    public static readonly int MAXIMUM_DUPLICATE_CARDS = 3;
    #endregion

    #region Methods
    public static string GetDeckCode(Dictionary<string, int> dictInput, CardElementType faction)
    {
        string deckCode = null;

        //Validate cardDictInput
        if (dictInput == null || dictInput.Count == 0)
        {
            return null;
        }

        foreach (string key in dictInput.Keys)
        {
            //Start with 'C'
            //Follow with 0-9 & length > 0
            if (!Regex.IsMatch(key, "^C[0-9]+$") || dictInput[key] < 1)
            {
                throw new Exception("Invalid input format");
            }
        }

        StringBuilder deckCodeStringBuilder = new StringBuilder();
        string hexToAppend = "";

        //Faction
        hexToAppend = GameHelper.ConvertIntToHexString(((int)faction), FACTION_HEX_DIGIT);
        deckCodeStringBuilder.Append(hexToAppend);

        //Initialize Dict<[amount], [list of card codes]> with keys [1, 2, 3]
        Dictionary<int, List<string>> cardAmountDict = new Dictionary<int, List<string>>();
        for (int amount = 1; amount <= MAXIMUM_DUPLICATE_CARDS; amount++)
        {
            cardAmountDict[amount] = new List<string>();
        }

        foreach (string key in dictInput.Keys)
        {
            int amount = dictInput[key];

            //n-copy cards case
            if (!cardAmountDict.ContainsKey(amount))
            {
                cardAmountDict[amount] = new List<string>();
            }
            cardAmountDict[amount].Add(key);
        }

        //1-3-copy cards
        for (int amount = 1; amount <= MAXIMUM_DUPLICATE_CARDS; amount++)
        {
            hexToAppend = GameHelper.ConvertIntToHexString(cardAmountDict[amount].Count, UNIQUE_CARD_AMOUNT_HEX_DIGIT);
            deckCodeStringBuilder.Append(hexToAppend);
            cardAmountDict[amount].Sort();

            //Add card codes
            foreach (string code in cardAmountDict[amount])
            {
                //Remove "C" prefix
                hexToAppend = GameHelper.ConvertIntToHexString(int.Parse(code.Substring(1)), CARD_CODE_HEX_DIGIT);
                deckCodeStringBuilder.Append(hexToAppend);
            }
        }

        //N-copy unique card amount
        List<int> cardAmountList = new List<int>(cardAmountDict.Keys);
        cardAmountList.Sort();
        int sumOfNUniqueCards = 0;
        for(int listIndex = MAXIMUM_DUPLICATE_CARDS; listIndex < cardAmountList.Count; listIndex++)
        {
            int amount = cardAmountList[listIndex];
            sumOfNUniqueCards += cardAmountDict[amount].Count;
        }

        hexToAppend = GameHelper.ConvertIntToHexString(sumOfNUniqueCards, UNIQUE_CARD_AMOUNT_HEX_DIGIT);
        deckCodeStringBuilder.Append(hexToAppend);

        //N-copy cards
        for (int listIndex = MAXIMUM_DUPLICATE_CARDS; listIndex < cardAmountList.Count; listIndex++)
        {
            int amount = cardAmountList[listIndex];
            cardAmountDict[amount].Sort();

            //Add card codes
            foreach (string code in cardAmountDict[amount])
            {
                //Remove "C" prefix
                hexToAppend = GameHelper.ConvertIntToHexString(int.Parse(code.Substring(1)), CARD_CODE_HEX_DIGIT);
                deckCodeStringBuilder.Append(hexToAppend);
                
                //Amount 
                hexToAppend = GameHelper.ConvertIntToHexString(amount, N_COPY_CARD_AMOUNT_HEX_DIGIT);
                deckCodeStringBuilder.Append(hexToAppend);
            }
        }

        Debug.Log("Deck code (before encoded): " + deckCodeStringBuilder.ToString());
        byte[] byteData = System.Text.ASCIIEncoding.ASCII.GetBytes(deckCodeStringBuilder.ToString());
        deckCode = System.Convert.ToBase64String(byteData);
        Debug.Log("Deck code (after encoded): " + deckCode);

        return deckCode;
    }

    public static Tuple<CardElementType, Dictionary<string, int>> DecodeDeckCode(string deckCode)
    {
        CardElementType deckFaction = CardElementType.NEUTRAL;
        Dictionary<string, int> cardDictOutput = null;
        
        string base64Decoded;
        byte[] byteData = System.Convert.FromBase64String(deckCode);
        base64Decoded = System.Text.ASCIIEncoding.ASCII.GetString(byteData);

        //Validate length
        int minimumInputLength = FACTION_HEX_DIGIT + ((MAXIMUM_DUPLICATE_CARDS + 1) * UNIQUE_CARD_AMOUNT_HEX_DIGIT);
        if (string.IsNullOrWhiteSpace(base64Decoded) || base64Decoded.Length < minimumInputLength)
        {
            throw new Exception("Invalid input format");
        }

        //Increases for each substring calls
        int currentSubStringStartIndex = 0;

        //Faction
        string factionText = base64Decoded.Substring(currentSubStringStartIndex, FACTION_HEX_DIGIT);
        currentSubStringStartIndex += FACTION_HEX_DIGIT;
        int convertedIntFromHex = GameHelper.ConvertHexStringToInt(factionText);
        deckFaction = (CardElementType)convertedIntFromHex;

        cardDictOutput = new Dictionary<string, int>();
        //1-3-copy cards
        for (int cardAmount = 1; cardAmount <= MAXIMUM_DUPLICATE_CARDS; cardAmount++)
        {
            string uniqueCardsAmountText = base64Decoded.Substring(currentSubStringStartIndex, UNIQUE_CARD_AMOUNT_HEX_DIGIT);
            currentSubStringStartIndex += UNIQUE_CARD_AMOUNT_HEX_DIGIT;
            for (int i = 0; i < GameHelper.ConvertHexStringToInt(uniqueCardsAmountText); i++)
            {
                string hexCardCode = base64Decoded.Substring(currentSubStringStartIndex, CARD_CODE_HEX_DIGIT);
                string cardCode = "C" + GameHelper.ConvertHexStringToInt(hexCardCode).ToString().PadLeft(CARD_CODE_DB_DIGIT, '0');
                currentSubStringStartIndex += CARD_CODE_HEX_DIGIT;
                cardDictOutput[cardCode] = cardAmount;
            }
        }

        //n-copy cards -> try get next nCardOffset characters
        int nCardOffset = CARD_CODE_HEX_DIGIT + UNIQUE_CARD_AMOUNT_HEX_DIGIT;
        string uniqueNCardsAmountText = base64Decoded.Substring(currentSubStringStartIndex, UNIQUE_CARD_AMOUNT_HEX_DIGIT);
        currentSubStringStartIndex += UNIQUE_CARD_AMOUNT_HEX_DIGIT;
        for (int i = 0; i < GameHelper.ConvertHexStringToInt(uniqueNCardsAmountText); i++)
        {
            if (currentSubStringStartIndex + nCardOffset > base64Decoded.Length)
            {
                break;
            }

            string cardCodeAndAmountText = base64Decoded.Substring(currentSubStringStartIndex, nCardOffset);
            currentSubStringStartIndex += nCardOffset;
            string cardCode = "C" + GameHelper.ConvertHexStringToInt(cardCodeAndAmountText.Substring(0, CARD_CODE_HEX_DIGIT)).ToString().PadLeft(CARD_CODE_DB_DIGIT, '0');
            int cardAmount = GameHelper.ConvertHexStringToInt(cardCodeAndAmountText.Substring(CARD_CODE_HEX_DIGIT, N_COPY_CARD_AMOUNT_HEX_DIGIT));
            cardDictOutput[cardCode] = cardAmount;
        }

        return new Tuple<CardElementType, Dictionary<string, int>>(deckFaction, cardDictOutput);
    }
    #endregion
}
