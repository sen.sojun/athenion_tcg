﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TaskLoader<T>
{
    #region Public Properties
    #endregion

    #region Private Properties
    List<List<UnityAction<UnityAction, UnityAction<T>>>> _tasksList = new List<List<UnityAction<UnityAction, UnityAction<T>>>>();

    private UnityAction<float> _onProgressUpdate;
    private UnityAction _onComplete;
    private UnityAction<List<T>> _onGenericsFail;

    private float _allTaskCount = 0;
    private float _taskCount = 0;

    private int _taskAmount = 0;
    private int _failedCount = 0; 
    private int _completedCount = 0;
    private List<T> _failValueList = new List<T>();
    #endregion

    #region Methods
    private void OnCallbackComplete()
    {
        if (_onProgressUpdate != null)
        {
            _onProgressUpdate.Invoke(++_taskCount / _allTaskCount);
        }

        _completedCount++;
        if (_completedCount == _taskAmount)
        {
            OnDoTasksComplete();
        }
        else
        {
            OnDoTasksFail();
        }
    }

    private void OnCallbackFail(T value)
    {
        _failedCount++;
        _failValueList.Add(value);
        OnDoTasksFail();
    }

    public void AddTask(UnityAction<UnityAction, UnityAction<T>> task)
    {
        _tasksList.Add(new List<UnityAction<UnityAction, UnityAction<T>>>() { task });
    }

    public void JoinTask(UnityAction<UnityAction, UnityAction<T>> task)
    {
        if(_tasksList.Count == 0)
        {
            AddTask(task);
        }
        else
        {
            _tasksList.Last().Add(task);
        }
    }

    public void StartTasks(UnityAction<float> onProgressUpdate, UnityAction onComplete, UnityAction<List<T>> onFail)
    {
        _onProgressUpdate = onProgressUpdate;
        _onComplete = onComplete;
        _onGenericsFail = onFail;
        
        foreach(List<UnityAction<UnityAction, UnityAction<T>>> tasks in _tasksList)
        {
            _allTaskCount += tasks.Count;
        }

        DoTasks();
    }

    public void StartTasks(UnityAction onComplete, UnityAction<List<T>> onFail)
    {
        _onComplete = onComplete;
        _onGenericsFail = onFail;

        foreach (List<UnityAction<UnityAction, UnityAction<T>>> tasks in _tasksList)
        {
            _allTaskCount += tasks.Count;
        }

        DoTasks();
    }

    private void DoTasks()
    {
        if(_tasksList.Count > 0)
        {
            _completedCount = 0;
            _failedCount = 0;
            _taskAmount = _tasksList[0].Count;
            foreach (UnityAction<UnityAction, UnityAction<T>> task in _tasksList[0])
            {
                if (task != null)
                {
                    task.Invoke(OnCallbackComplete, OnCallbackFail);
                }
            }
        }
        else
        {
            if (_onComplete != null)
            {
                _onComplete.Invoke();
            }
        }
    }

    private void OnDoTasksComplete()
    {
        _tasksList.RemoveAt(0);
        if(_tasksList.Count > 0)
        {
            DoTasks();
        }
        else
        {
            if (_onComplete != null)
            {
                _onComplete.Invoke();
            }
        }
    }

    private void OnDoTasksFail()
    {
        if (_failedCount + _completedCount == _taskAmount)
        {
            if (_onGenericsFail != null)
            {
                _onGenericsFail.Invoke(_failValueList);
            }
        }
    }
    #endregion
}