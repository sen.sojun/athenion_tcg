﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class LocalizationManager : Singleton<LocalizationManager>
{
    #region Public Properties
    public Language CurrentLanguage { get { return DataManager.Instance.GetLanguage(); } }

    public const string ItemNamePrefix = "ITEM_NAME";
    public const string ItemDescriptionPrefix = "ITEM_DESCRIPTION";
    public const string ItemShortDescriptionPrefix = "ITEM_SHORT_DESCRIPTION";

    public const string CardNamePrefix = "CARD_NAME";
    public const string HeroNamePrefix = "HERO_NAME";

    public const string QuestTitleSuffix = "TITLE";
    public const string QuestDescriptionSuffix = "DESCRIPTION";
    public const string QuestCompletedSuffix = "COMPLETE";
    public const string AchievementTitleSuffix = "TITLE";
    public const string AchievementDescriptionSuffix = "DESCRIPTION";

    public const string EventNamePrefix = "EVENT";
    #endregion

    #region Private Properties
    private UnityEvent _toggleLanguageEvent;
    #endregion

    #region Methods

    #region General Text
    public bool GetText(string key, out string resultText)
    {
        return GetText(key, DataManager.Instance.GetLanguage(), out resultText);
    }

    public string GetText(string key)
    {
        string resultText;

        if (GetText(key, CurrentLanguage, out resultText))
        {
            return resultText;
        }
            
        return key;
    }

    public bool GetText(string key, Language language, out string resultText)
    {
        LocalizationDBData data;
        bool isSuccess = LocalizationDB.Instance.GetData(key.ToUpper(), out data);
        if (isSuccess)
        {
            resultText = data.TextList[(int)language];
            return true;
        }

        resultText = "";
        return false;
    }

    public string GetText(string key, Language language)
    {
        string resultText;

        if (GetText(key, language, out resultText))
        {
            return resultText;
        }

        return key;
    }
    #endregion

    #region Item
    public bool GetItemName(string itemID, out string itemName)
    {
        return GetText(string.Format("{0}_{1}", ItemNamePrefix, GameHelper.ConvertItemIDToItemKey(itemID)), out itemName);
    }

    public string GetItemName(string itemID)
    {
        return GetText(string.Format("{0}_{1}", ItemNamePrefix, GameHelper.ConvertItemIDToItemKey(itemID)));
    }

    public bool GetItemName(string itemID, Language language, out string itemName)
    {
        return GetText(string.Format("{0}_{1}", ItemNamePrefix, GameHelper.ConvertItemIDToItemKey(itemID)), language, out itemName);
    }

    public string GetItemName(string itemID, Language language)
    {
        return GetText(string.Format("{0}_{1}", ItemNamePrefix, GameHelper.ConvertItemIDToItemKey(itemID)), language);
    }

    public bool GetItemDescription(string itemID, out string description)
    {
        return GetText(string.Format("{0}_{1}", ItemDescriptionPrefix, GameHelper.ConvertItemIDToItemKey(itemID)), out description);
    }

    public string GetItemDescription(string itemID)
    {
        return GetText(string.Format("{0}_{1}", ItemDescriptionPrefix, GameHelper.ConvertItemIDToItemKey(itemID)));
    }

    public bool GetItemShortDescription(string itemID, out string shortDescription)
    {
        return GetText(string.Format("{0}_{1}", ItemShortDescriptionPrefix, GameHelper.ConvertItemIDToItemKey(itemID)), out shortDescription);
    }

    public string GetItemShortDescription(string itemID)
    {
        return GetText(string.Format("{0}_{1}", ItemShortDescriptionPrefix, GameHelper.ConvertItemIDToItemKey(itemID)));
    }
    #endregion

    #region Card
    public bool GetCardName(string cardBaseID, out string cardName)
    {
        return GetText(string.Format("{0}_{1}", CardNamePrefix, cardBaseID), out cardName);
    }

    public string GetCardName(string cardBaseID)
    {
        return GetText(string.Format("{0}_{1}", CardNamePrefix, cardBaseID));
    }

    public bool GetCardName(string cardBaseID, Language language, out string cardName)
    {
        return GetText(string.Format("{0}_{1}", CardNamePrefix, cardBaseID), language, out cardName);
    }

    public string GetCardName(string cardBaseID, Language language)
    {
        return GetText(string.Format("{0}_{1}", CardNamePrefix, cardBaseID), language);
    }
    #endregion

    #region Hero
    public bool GetHeroName(string heroID, out string heroName)
    {
        return GetText(string.Format("{0}_{1}", HeroNamePrefix, heroID), out heroName);
    }

    public string GetHeroName(string heroID)
    {
        return GetText(string.Format("{0}_{1}", HeroNamePrefix, heroID));
    }

    public bool GetHeroName(string heroID, Language language, out string heroName)
    {
        return GetText(string.Format("{0}_{1}", HeroNamePrefix, heroID), language, out heroName);
    }

    public string GetHeroName(string heroID, Language language)
    {
        return GetText(string.Format("{0}_{1}", HeroNamePrefix, heroID), language);
    }
    #endregion

    #region Quest
    public bool GetQuestTitle(string questID, out string questTitle)
    {
        return GetText(string.Format("{0}_{1}", questID, QuestTitleSuffix), out questTitle);
    }

    public string GetQuestTitle(string questID)
    {
        return GetText(string.Format("{0}_{1}", questID, QuestTitleSuffix));
    }

    public bool GetQuestTitle(string questID, Language language, out string questTitle)
    {
        return GetText(string.Format("{0}_{1}", questID, QuestTitleSuffix), language, out questTitle);
    }

    public string GetQuestTitle(string questID, Language language)
    {
        return GetText(string.Format("{0}_{1}", questID, QuestTitleSuffix), language);
    }

    public bool GetQuestDescription(string questID, out string questDescription)
    {
        return GetText(string.Format("{0}_{1}", questID, QuestDescriptionSuffix), out questDescription);
    }

    public string GetQuestDescription(string questID)
    {
        return GetText(string.Format("{0}_{1}", questID, QuestDescriptionSuffix));
    }

    public bool GetQuestDescription(string questID, Language language, out string questDescription)
    {
        return GetText(string.Format("{0}_{1}", questID, QuestDescriptionSuffix), language, out questDescription);
    }

    public string GetQuestDescription(string questID, Language language)
    {
        return GetText(string.Format("{0}_{1}", questID, QuestDescriptionSuffix), language);
    }

    public bool GetQuestCompleted(string questID, out string questCompleted)
    {
        return GetText(string.Format("{0}_{1}", questID, QuestCompletedSuffix), out questCompleted);
    }

    public string GetQuestCompleted(string questID)
    {
        return GetText(string.Format("{0}_{1}", questID, QuestCompletedSuffix));
    }

    public bool GetQuestCompleted(string questID, Language language, out string questCompleted)
    {
        return GetText(string.Format("{0}_{1}", questID, QuestCompletedSuffix), language, out questCompleted);
    }

    public string GetQuestCompleted(string questID, Language language)
    {
        return GetText(string.Format("{0}_{1}", questID, QuestCompletedSuffix), language);
    }
    #endregion

    #region Achievement
    public bool GetAchievementTitle(string achievementID, out string achievementTitle)
    {
        return GetText(string.Format("{0}_{1}", achievementID, AchievementTitleSuffix), out achievementTitle);
    }

    public string GetAchievementTitle(string achievementID)
    {
        return GetText(string.Format("{0}_{1}", achievementID, AchievementTitleSuffix));
    }

    public bool GetAchievementTitle(string achievementID, Language language, out string achievementTitle)
    {
        return GetText(string.Format("{0}_{1}", achievementID, AchievementTitleSuffix), language, out achievementTitle);
    }

    public string GetAchievementTitle(string achievementID, Language language)
    {
        return GetText(string.Format("{0}_{1}", achievementID, AchievementTitleSuffix), language);
    }

    public bool GetAchievementDescription(string achievementID, out string achievementDescription)
    {
        return GetText(string.Format("{0}_{1}", achievementID, AchievementDescriptionSuffix), out achievementDescription);
    }

    public string GetAchievementDescription(string achievementID)
    {
        return GetText(string.Format("{0}_{1}", achievementID, AchievementDescriptionSuffix));
    }

    public bool GetAchievementDescription(string achievementID, Language language, out string achievementDescription)
    {
        return GetText(string.Format("{0}_{1}", achievementID, AchievementDescriptionSuffix), language, out achievementDescription);
    }

    public string GetAchievementDescription(string achievementID, Language language)
    {
        return GetText(string.Format("{0}_{1}", achievementID, AchievementDescriptionSuffix), language);
    }
    #endregion

    #region Event
    public bool GetEventName(string eventID, out string eventName)
    {
        return GetText(string.Format("{0}_{1}", EventNamePrefix, eventID), out eventName);
    }

    public string GetEventName(string eventID)
    {
        return GetText(string.Format("{0}_{1}", EventNamePrefix, eventID));
    }

    public bool GetEventName(string eventID, Language language, out string eventName)
    {
        return GetText(string.Format("{0}_{1}", EventNamePrefix, eventID), language, out eventName);
    }

    public string GetEventName(string eventID, Language language)
    {
        return GetText(string.Format("{0}_{1}", EventNamePrefix, eventID), language);
    }
    #endregion

    public void BindToggleLanguageEvent(UnityAction onToggleLanguage)
    {
        if (_toggleLanguageEvent == null)
        {
            _toggleLanguageEvent = new UnityEvent();
        }

        _toggleLanguageEvent.AddListener(onToggleLanguage);
    }

    public void UnbindToggleLanguageEvent(UnityAction onToggleLanguage)
    {
        if (_toggleLanguageEvent == null)
        {
            _toggleLanguageEvent = new UnityEvent();
        }

        _toggleLanguageEvent.RemoveListener(onToggleLanguage);
    }

    public void ClearAllToggleEvent()
    {
        if (_toggleLanguageEvent != null)
        {
            _toggleLanguageEvent.RemoveAllListeners();
        }
    }

    public void ToggleLanguage()
    {
        Language currentLanguage = CurrentLanguage;
        switch (currentLanguage)
        {
            case Language.English:
            {
                DataManager.Instance.SetLanguage(Language.Thai);
            }
            break;

            case Language.Thai:
            {
                DataManager.Instance.SetLanguage(Language.Japanese);
            }
            break;

            case Language.Japanese:
            {
                DataManager.Instance.SetLanguage(Language.English);
            }
            break;
        }

        if (_toggleLanguageEvent != null)
        {
            _toggleLanguageEvent.Invoke();
        }
    }

    public void SetLanguage(Language language)
    {
        DataManager.Instance.SetLanguage(language);

        if (_toggleLanguageEvent != null)
        {
            _toggleLanguageEvent.Invoke();
        }
    }
    #endregion
}