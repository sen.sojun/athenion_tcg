﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class CSVReader
{
    public static bool ReadCSV(string csvText, out List<List<string>> result)
    {
        List<List<string>> data = new List<List<string>>();

        if (csvText != null)
        {
            string[] records = SplitCSVRow(csvText);

            for (int index = 1; index < records.Length; ++index)
            {
                List<string> row = new List<string>();
                string[] fields = SplitCSVColumn(records[index]);
                foreach (string field in fields)
                {
                    row.Add(field);
                }
                data.Add(row);
            }

            result = data;
            return true;
        }

        result = null;
        return false;
    }

    public static bool ReadCSV(TextAsset csvFile, out List<List<string>> result)
    {
        if (csvFile == null)
        {
            result = null;
            return false;
        }

        return ReadCSV(csvFile.text, out result); 
    }

    public static bool ReadCSVWithTitle(TextAsset csvFile, out List<List<string>> result)
    {
        List<List<string>> data = new List<List<string>>();

        if (csvFile != null)
        {
            string[] records = SplitCSVRow(csvFile.text);

            for (int index = 0; index < records.Length; index++)
            {
                List<string> row = new List<string>();
                string[] fields = SplitCSVColumn(records[index]);
                foreach (string field in fields)
                {
                    row.Add(field);
                }
                data.Add(row);
            }

            result = data;
            return true;
        }

        result = null;
        return false;
    }
    // split CSV row 
    private static string[] SplitCSVRow(string text)
    {
        List<string> result = new List<string>();
        StringBuilder sbRowText = new StringBuilder();
        bool isFoundQuote = false;

        for (int index = 0; index < text.Length; ++index)
        {
            char c = text[index];
            if (!isFoundQuote)
            {
                if (c == '\"')
                {
                    // Start Qoute
                    isFoundQuote = true;
                }
                else if (c == '\r')
                {
                    // Ignore \r
                    continue;
                }
                else if (c == '\n')
                {
                    // Split line
                    result.Add(sbRowText.ToString());
                    ClearStringBuilder(sbRowText);
                    continue;
                }
            }
            else
            {
                if (c == '\"')
                {
                    // End Qoute
                    isFoundQuote = false;
                }
                /*
                else if(c == '\r')
                {
                    // Ignore \r
                    continue;
                }
                */
            }

            //rowText += c;
            sbRowText.Append(c);

            if (index + 1 >= text.Length && sbRowText.Length > 0)
            {
                result.Add(sbRowText.ToString());
                ClearStringBuilder(sbRowText);
                continue;
            }
        }

        /*
        // Print Debug
        string debugText = "";
        for (int i = 0; i < result.Count; ++i)
        {
            debugText += "[" + result[i] + "]";
        }
         //Debug.Log(text +"\n" + debugText);
        */

        return (result.ToArray());
    }

    // split CSV column 
    private static string[] SplitCSVColumn(string text)
    {
        List<string> result = new List<string>();
        StringBuilder sbColumnText = new StringBuilder();
        //string columnText = "";
        bool isFoundQuote = false;

        for (int index = 0; index < text.Length; ++index)
        {
            char c = text[index];
            if (!isFoundQuote)
            {
                if (c == ',')
                {
                    // Split column
                    result.Add(sbColumnText.ToString());
                    ClearStringBuilder(sbColumnText);
                    continue;
                }
                else if (c == '\"')
                {
                    isFoundQuote = true;
                    continue;

                }
                else if (c == '\n' && index == 0)
                {
                    // remove first \n
                    continue;
                }
            }
            else
            {
                if (c == '\"')
                {
                    // end quote
                    isFoundQuote = false;

                    if (index + 1 < text.Length)
                    {
                        char c_next = text[index + 1];
                        if (c_next == '\"')
                        {
                            // found double quote \"\"
                            sbColumnText.Append("\"");
                            isFoundQuote = true; // false end quote
                            ++index;
                            continue;
                        }
                    }

                    if (index + 1 >= text.Length && sbColumnText.Length > 0)
                    {
                        // last character
                        result.Add(sbColumnText.ToString());
                        ClearStringBuilder(sbColumnText);
                        continue;
                    }
                    else
                    {
                        continue;
                    }
                }
            }
            sbColumnText.Append(c);
            // //Debug.Log(columnText);

            if (index + 1 >= text.Length && sbColumnText.Length > 0)
            {
                // last character
                result.Add(sbColumnText.ToString());
                ClearStringBuilder(sbColumnText);
                continue;
            }
        }

        /*
        // Print Debug
        string debugText = "";
        for (int i = 0; i < result.Count; ++i)
        {
            debugText += "[" + result[i] + "]";
        }
        Debug.Log("Column:" +"\n" + debugText);
        */

        return (result.ToArray());
    }

    private static void ClearStringBuilder(StringBuilder value)
    {
        value.Length = 0;
        value.Capacity = 0;
    }
}
