﻿using System.Collections;
using System.Linq;
using System;
using System.IO;
using System.Collections.Generic;

public class CSVWriter
{
    public static bool WriteCSV(string fileName, List<Dictionary<string, string>> csvTable)
    {
        string csvText = string.Empty;
        string columnStr = "";
        string dataStr = "";
        if (csvTable.Count > 0)
        {
            string[] keys = csvTable[0].Keys.ToArray();
            foreach (string key in keys)
            {
                string nameTmp = key;
                if (nameTmp.Contains(",") || nameTmp.Contains("\""))
                {
                    nameTmp = nameTmp.Replace("\"", "\"\"");
                    nameTmp = "\"" + nameTmp + "\"";
                }

                if (columnStr != "")
                {
                    columnStr += ",";
                }
                columnStr += nameTmp;
            }
            csvText += columnStr;

            foreach (Dictionary<string, string> row in csvTable)
            {
                dataStr = "";
                string dataTmp = "-";
                foreach (string key in keys)
                {
                    if(row.ContainsKey(key))
                    {
                        dataTmp = row[key];

                        if(dataTmp != null)
                        {
                            if (dataTmp.Contains(",") || dataTmp.Contains("\""))
                            {
                                dataTmp = dataTmp.Replace("\"", "\"\"");
                                dataTmp = "\"" + dataTmp + "\"";
                            }
                        }      
                    }
                    else
                    {
                        dataTmp = "N/A";
                    }

                    if (dataStr != "")
                    {
                        dataStr += ",";
                    }
                    dataStr += dataTmp;
                }
                csvText += Environment.NewLine + dataStr;
            }

            if (File.Exists(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), fileName + ".csv")))
            {
                File.Delete(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), fileName + ".csv"));
            }
            File.WriteAllText(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), fileName + ".csv"), csvText);

            return true;
        }
        else
        {
            return false;
        }
    }
}
