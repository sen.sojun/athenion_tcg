﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Karamucho;

public abstract class PopupItem : PoolItem
{
    #region Public Properties
    public PopupPool.PopupType Type { get; protected set; }
    #endregion

    #region Methods
    public void SetPopupType(PopupPool.PopupType type)
    {
        Type = type;
    }
    #endregion
}

public class PopupPool : PrefabPool<PopupPool, PopupItem>
{
    protected string RootPrefabPath = "Prefabs/Pool/Popup";

    public enum PopupType
    {
        DealDamage,
        DealDamage_Tutorial,
        DealDamage_BattleArmor,
        Heal,
        BoostAp
    }

    public override sealed PopupItem CreateItem()
    {
        // Hide public reference.
        return base.CreateItem();
    }

    public PopupItem CreateItem(PopupType popupType)
    {
        if (!IsInit) Init();

        for (int i = 0; i < _pool.Count; i++)
        {
            PopupItem item = (PopupItem)_pool[i];
            if (item != null && !item.IsLock && item.Type == popupType)
            {
                item.SetLock(true);
                item.GetGameObject().SetActive(true);
                return _pool[i];
            }
        }

        //Debug.Log("PrefabPath : " + GetPrefabPath(vfxType));
        GameObject go = MonoBehaviour.Instantiate(ResourceManager.Load(GetPrefabPath(popupType))) as GameObject;
        PopupItem component = go.GetComponent<PopupItem>();
        component.SetPopupType(popupType);

        _pool.Add(component);
        component.SetLock(true);

        return component;
    }

    protected string GetPrefabPath(PopupType type)
    {
        switch (type)
        {
            case PopupType.DealDamage:
            case PopupType.DealDamage_BattleArmor:
                return RootPrefabPath + "/" + PopupType.DealDamage.ToString();

            default:
                return RootPrefabPath + "/" + type.ToString();
        }
    }
}
