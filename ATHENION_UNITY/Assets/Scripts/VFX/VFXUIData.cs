﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VFXUIData
{
    #region Public Properties
    public PerformTypes PerformType { get { return _performType; } }
    #endregion

    #region Private Properties
    private const string _defaultVFXKey = "-";
    private PerformTypes _performType;
    private Dictionary<PerformSteps, string> _vfxKeys = new Dictionary<PerformSteps, string>();
    #endregion

    #region Constructor
    public VFXUIData(DataParam param)
    {
        if (param != null)
        {
            GameHelper.StrToEnum(param.Parameters[0], out _performType);

            _vfxKeys.Add(PerformSteps.Start, param.Parameters[1]);
            _vfxKeys.Add(PerformSteps.Moving, param.Parameters[2]);
            _vfxKeys.Add(PerformSteps.Arrived, param.Parameters[3]);
        }
    }

    public VFXUIData(VFXUIData data)
    {
        _performType = data._performType;
        _vfxKeys = new Dictionary<PerformSteps, string>(data._vfxKeys);
    }
    #endregion

    #region Methods
    private void AddVFXPerformStepKey(PerformSteps step, string paramKey)
    {
        if(_vfxKeys.ContainsKey(step) == false)
        {
            _vfxKeys.Add(step, _defaultVFXKey);
        }

        _vfxKeys[step] = paramKey;
    }

    public string GetVFXPerformStepKey(PerformSteps step)
    {
        string result = _defaultVFXKey;
        if (_vfxKeys.ContainsKey(step) == true)
        {
            result = _vfxKeys[step];
        }

        return result;
    }
    #endregion

}
