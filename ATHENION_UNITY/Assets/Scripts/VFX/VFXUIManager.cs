﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TargetPositionData
{
    #region Public Properties
    public int ID { get; private set; }
    public Vector3 Position { get; private set; }
    #endregion

    #region Contructors
    public TargetPositionData(int id, Vector3 position)
    {
        ID = id;
        Position = position;
    }
    #endregion
}

public abstract class VFXUIManager
{
    #region Private Properties
    private static Requester _requester;
    private static int _value;
    private static VFXUIData _vfxUIData;
    private static UnityAction<int> _onEachComplete;
    private static UnityAction _onAllComplete;

    private static int _countCallback = 0;
    private static int _maxCallback = 0;

    private static Vector3 _requesterPosition;
    private static List<TargetPositionData> _targetDataList = new List<TargetPositionData>();
    #endregion

    #region Methods
    protected static void ActionPerformVFX(Requester requester, List<TargetPositionData> targetDataList, int value, VFXUIData vfxUIData, UnityAction<int> onEachComplete, UnityAction onAllComplete)
    {
        _requester = requester;
        _targetDataList = new List<TargetPositionData>(targetDataList);
        _value = value;
        _vfxUIData = new VFXUIData(vfxUIData);
        _onEachComplete = onEachComplete;
        _onAllComplete = onAllComplete;

        switch (requester.Type)
        {
            case Requester.RequesterType.Minion:
            {
                _requesterPosition = Karamucho.UI.BoardManager.Instance.GetSlotHolder(requester.Minion.SlotID).GetHolderPosition();
            }
            break;
        }

        if (_targetDataList != null && _targetDataList.Count > 0)
        {
            switch (_vfxUIData.PerformType)
            {
                case PerformTypes.Global:
                {
                    StartGlobal();
                }
                break;

                case PerformTypes.Group:
                {
                    StartGroup();
                }
                break;

                case PerformTypes.Single:
                {
                    StartSingle();
                }
                break;

                case PerformTypes.Area:
                {
                    StartArea();
                }
                break;

                case PerformTypes.Multi:
                {
                    StartMulti();
                }
                break;

                case PerformTypes.DirAuto:
                {
                    StartDirAuto();
                }
                break;

                case PerformTypes.DirCustom:
                {
                    OnAllActionComplete();
                }
                break;

                case PerformTypes.DirCross:
                {
                    StartDirCross();
                }
                break;

                case PerformTypes.DirPlus:
                {
                    StartDirPlus();
                }
                break;

                default:
                {
                    OnAllActionComplete();
                }
                break;
            }
            return;
        }
        else
        {
            OnAllActionComplete();
        }
    }

    private static void OnAllActionComplete()
    {
        _onAllComplete?.Invoke();
    }

    private static void OnEachActionComplete(int targetID)
    {
        _onEachComplete?.Invoke(targetID);

        _countCallback++;
        if(_countCallback == _maxCallback)
        {
            OnAllActionComplete();
        }
    }

    #region Global
    private static void StartGlobal()
    {
        // Show casting + burst at caster.
        UIManager.Instance.RequestDealDamageGlobal(_vfxUIData.GetVFXPerformStepKey(PerformSteps.Start));

        if (_targetDataList.Count > 0)
        {
            _countCallback = 0;
            _maxCallback = _targetDataList.Count;
            foreach (TargetPositionData item in _targetDataList)
            {
                int uniqueID = item.ID;
                UIManager.Instance.RequestDealDamage_Single(
                      _requesterPosition
                    , item.Position
                    , _value
                    , "-"
                    , _vfxUIData.GetVFXPerformStepKey(PerformSteps.Moving)
                    , _vfxUIData.GetVFXPerformStepKey(PerformSteps.Arrived)
                    , false
                    , () => OnEachActionComplete(uniqueID)
                );
            }
        }
    }
    #endregion

    #region Single
    private static void StartSingle()
    {
        if (_targetDataList.Count > 0)
        {
            _countCallback = 0;
            _maxCallback = _targetDataList.Count;
            foreach (TargetPositionData item in _targetDataList)
            {
                int uniqueID = item.ID;
                UIManager.Instance.RequestDealDamage_Single(
                      _requesterPosition
                    , item.Position
                    , _value
                    , _vfxUIData.GetVFXPerformStepKey(PerformSteps.Start)
                    , _vfxUIData.GetVFXPerformStepKey(PerformSteps.Moving)
                    , _vfxUIData.GetVFXPerformStepKey(PerformSteps.Arrived)
                    , true
                    , () => OnEachActionComplete(uniqueID)
                );
            }
        }
    }
    #endregion

    #region Area
    private static void StartArea()
    {
        // Show casting + burst at caster.
        UIManager.Instance.RequestDealDamageGroup(_requesterPosition, _vfxUIData.GetVFXPerformStepKey(PerformSteps.Start));

        if (_targetDataList.Count > 0)
        {
            _countCallback = 0;
            _maxCallback = _targetDataList.Count;
            foreach (TargetPositionData item in _targetDataList)
            {
                int uniqueID = item.ID;
                UIManager.Instance.RequestDealDamage_Single(
                      _requesterPosition
                    , item.Position
                    , _value
                    , "-"
                    , _vfxUIData.GetVFXPerformStepKey(PerformSteps.Moving)
                    , _vfxUIData.GetVFXPerformStepKey(PerformSteps.Arrived)
                    , false
                    , () => OnEachActionComplete(uniqueID)
                );
            }
        }
    }
    #endregion

    #region Group
    private static void StartGroup()
    {
        if (_targetDataList.Count > 0)
        {
            int index = 0;
            _countCallback = 0;
            _maxCallback = _targetDataList.Count;
            foreach (TargetPositionData item in _targetDataList)
            {
                int uniqueID = item.ID;
                if (index == 0)
                {                 
                    UIManager.Instance.RequestDealDamage_Single(
                          _requesterPosition
                        , item.Position
                        , _value
                        , _vfxUIData.GetVFXPerformStepKey(PerformSteps.Start)
                        , _vfxUIData.GetVFXPerformStepKey(PerformSteps.Moving)
                        , _vfxUIData.GetVFXPerformStepKey(PerformSteps.Arrived)
                        , true
                        , () => OnEachActionComplete(uniqueID)
                    );
                }
                else
                {
                    UIManager.Instance.RequestDealDamage_Single(
                          _requesterPosition
                        , item.Position
                        , _value
                        , "-"
                        , _vfxUIData.GetVFXPerformStepKey(PerformSteps.Moving)
                        , _vfxUIData.GetVFXPerformStepKey(PerformSteps.Arrived)
                        , false
                        , () => OnEachActionComplete(uniqueID)
                    );
                }

                index++;
            }
        }
    }
    #endregion

    #region Multi
    private static void StartMulti()
    {
        if (_targetDataList.Count > 0)
        {
            int index = 0;
            _countCallback = 0;
            _maxCallback = _targetDataList.Count;
            foreach (TargetPositionData item in _targetDataList)
            {
                int uniqueID = item.ID;
                UIManager.Instance.RequestDealDamage_Single(
                      _requesterPosition
                    , item.Position
                    , _value
                    , _vfxUIData.GetVFXPerformStepKey(PerformSteps.Start)
                    , _vfxUIData.GetVFXPerformStepKey(PerformSteps.Moving)
                    , _vfxUIData.GetVFXPerformStepKey(PerformSteps.Arrived)
                    , index == 0
                    , () => OnEachActionComplete(uniqueID)
                );

                index++;
            }
        }
    }
    #endregion

    #region DirPlus
    private static void StartDirPlus()
    {
        if (_targetDataList.Count > 0)
        {
            // Direction
            List<CardDirectionType> directionList
                = new List<CardDirectionType> {
                    CardDirectionType.N,
                    CardDirectionType.E,
                    CardDirectionType.S,
                    CardDirectionType.W
                };


            int index = 0;
            _countCallback = 0;
            _maxCallback = _targetDataList.Count;
            foreach (TargetPositionData item in _targetDataList)
            {
                int uniqueID = item.ID;
                if (index == 0)
                {
                    UIManager.Instance.RequestDealDamageRow(
                        item.Position
                        , _vfxUIData.GetVFXPerformStepKey(PerformSteps.Arrived)
                        , directionList
                        , true
                        , () => OnEachActionComplete(uniqueID)
                    );
                }
                else
                {
                    UIManager.Instance.RequestDealDamageRow(
                        item.Position
                        , _vfxUIData.GetVFXPerformStepKey(PerformSteps.Arrived)
                        , directionList
                        , false
                        , () => OnEachActionComplete(uniqueID)
                    );
                }

                index++;
            }
        }
    }
    #endregion

    #region DirCross
    private static void StartDirCross()
    {
        if (_targetDataList.Count > 0)
        {
            // Direction
            List<CardDirectionType> directionList
                = new List<CardDirectionType> {
                    CardDirectionType.NE,
                    CardDirectionType.SE,
                    CardDirectionType.SW,
                    CardDirectionType.NW
                };


            int index = 0;
            _countCallback = 0;
            _maxCallback = _targetDataList.Count;
            foreach (TargetPositionData item in _targetDataList)
            {
                int uniqueID = item.ID;
                if (index == 0)
                {
                    UIManager.Instance.RequestDealDamageRow(
                        item.Position
                        , _vfxUIData.GetVFXPerformStepKey(PerformSteps.Arrived)
                        , directionList
                        , true
                        , () => OnEachActionComplete(uniqueID)
                    );
                }
                else
                {
                    UIManager.Instance.RequestDealDamageRow(
                        item.Position
                        , _vfxUIData.GetVFXPerformStepKey(PerformSteps.Arrived)
                        , directionList
                        , false
                        , () => OnEachActionComplete(uniqueID)
                    );
                }

                index++;
            }
        }
    }
    #endregion

    #region DirAuto
    private static void StartDirAuto()
    {
        if (_targetDataList.Count > 0)
        {
            // Direction
            List<CardDirectionType> directionList = _requester.Minion.CardDir.GetAllDirection();

            int index = 0;
            _countCallback = 0;
            _maxCallback = _targetDataList.Count;
            foreach (TargetPositionData item in _targetDataList)
            {
                int uniqueID = item.ID;
                if (index == 0)
                {
                    UIManager.Instance.RequestDealDamageRow(
                        _requesterPosition
                        , _vfxUIData.GetVFXPerformStepKey(PerformSteps.Arrived)
                        , directionList
                        , true
                        , () => OnEachActionComplete(uniqueID)
                    );
                }
                else
                {
                    UIManager.Instance.RequestDealDamage_Single(
                          _requesterPosition
                        , item.Position
                        , _value
                        , "-"
                        , "-"
                        , "-"
                        , false
                        , () => OnEachActionComplete(uniqueID)
                    );
                }

                index++;
            }
        }
    }
    #endregion


    #endregion
}

public class VFXUIManager_Slot : VFXUIManager
{
    public static void ActionPerformVFXSlot(Requester requester, List<int> slotIDList, int value, VFXUIData vfxUIData, UnityAction<int> onEachComplete, UnityAction onAllComplete)
    {        
        // Find minion position
        List<TargetPositionData> targetIDDict = new List<TargetPositionData>();
        foreach (int item in slotIDList)
        {
            Vector3 slotPosition = Karamucho.UI.BoardManager.Instance.GetSlotHolder(item).GetHolderPosition();
            targetIDDict.Add(new TargetPositionData(item, slotPosition));
        }

        // Action
        ActionPerformVFX(requester, targetIDDict, value, vfxUIData, onEachComplete, onAllComplete);
    }
}

public class VFXUIManager_Minion : VFXUIManager
{
    public static void ActionPerformVFXMinion(Requester requester, List<int> targetIDList, int value, VFXUIData vfxUIData, UnityAction<int> onEachComplete, UnityAction onAllComplete)
    {
        // Find minion position
        List<TargetPositionData> targetIDDict = new List<TargetPositionData>();
        MinionData minionData;
        foreach (int item in targetIDList)
        {
            GameManager.Instance.FindMinionBattleCard(item, out minionData);
            Vector3 minionPosition = Karamucho.UI.BoardManager.Instance.GetSlotHolder(minionData.SlotID).GetHolderPosition();
            targetIDDict.Add(new TargetPositionData(item, minionPosition));
        }

        // Action
        ActionPerformVFX(requester, targetIDDict, value, vfxUIData, onEachComplete, onAllComplete);
    }
}

public class VFXUIManager_Player : VFXUIManager
{
    private static UnityAction<PlayerIndex> _onEachComplete;

    public static void ActionPerformVFXPlayer(Requester requester, List<PlayerIndex> targetIDList, int value, VFXUIData vfxUIData, UnityAction<PlayerIndex> onEachComplete, UnityAction onAllComplete)
    {
        _onEachComplete = onEachComplete;
        List<TargetPositionData> targetIDDict = new List<TargetPositionData>();
        foreach (PlayerIndex item in targetIDList)
        {
            // Find Position
            Vector3 targetPosition;
            if (GameManager.Instance.IsLocalPlayer(item))
            {
                targetPosition = UIManager.Instance.UIPlayerBottom.GetHeroPosition();
            }
            else
            {
                targetPosition = UIManager.Instance.UIPlayerTop.GetHeroPosition();
            }

            // Save Dict Position
            int key = (int)item;
            targetIDDict.Add(new TargetPositionData(key, targetPosition));
        }

        // Action
        ActionPerformVFX(requester, targetIDDict, value, vfxUIData, OnEachComplete, onAllComplete);
    }

    private static void OnEachComplete(int playerIndex)
    {
        _onEachComplete?.Invoke((PlayerIndex)playerIndex);
    }
}