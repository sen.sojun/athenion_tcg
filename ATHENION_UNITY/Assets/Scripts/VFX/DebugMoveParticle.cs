﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DebugMoveParticle : MonoBehaviour {

    public Transform StartPostion;
    public Transform EndPostion;
    public Transform MovingObject;
    public GameObject Particle;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            SpawnParticle();
        }
	}

    void SpawnParticle()
    {
        GameObject obj = Instantiate(Particle, MovingObject);
        Sequence sq = DOTween.Sequence();
        obj.transform.localPosition = Vector3.zero;
        MovingObject.transform.position = StartPostion.position;
        sq.Append(MovingObject.transform.DOMove(EndPostion.position, 2.0f));
        sq.OnComplete(delegate { Destroy(obj); });
    }
}
