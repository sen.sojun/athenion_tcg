﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using Karamucho;

public abstract class VFXItem : PoolItem
{
    #region Public Properties
    public string PrefabPath { get; protected set; }
    #endregion

    #region Methods
    public void SetPrefabPath(string path)
    {
        PrefabPath = path;
    }
    #endregion
}

public class CardVFXPool : PrefabPool<CardVFXPool, CardVFXItem> 
{
    public static readonly string RootVFXPath = "Prefabs/Pool/VFX/";
    public static readonly string RootSpawnEFXPath = RootVFXPath + "SpawnEffects/";
    public static readonly string RootAttackEFXPath = RootVFXPath + "AttackEffects/";
    public static readonly string RootDeathEFXPath = RootVFXPath + "DeathEffects/";
    public static readonly string RootAbilityEFXPath = RootVFXPath + "AbilityEffects/";
    public static readonly string RootHeroEFXPath = RootVFXPath + "HeroEffects/";

    public enum VFXType
    {
          CardDiscard
        , CardDiscard1
        , SpiritCircle_blue
        , SpiritCircle_red
        , CardDiscardFromDraw
        , Status_FreezeIn
        , Status_FreezeOut
        , Status_PowerUp
        , Status_PowerDown
        , Status_SilenceIn
        , Status_SilenceOut
        , Status_TauntIn
        , Status_LockIn
        , Status_LockOut
        , Status_AuraIn
        , Status_DarkMatterIn_Ally
        , Status_DarkMatterOut_Ally
        , Status_DarkMatterIn_Enemy
        , Status_DarkMatterOut_Enemy
        , Status_DarkMatterIn_Token
        , Status_DarkMatterOut_Token
        , TokenArrowChange
    }

    public override sealed CardVFXItem CreateItem()
    {
        // Hide public reference.
        return base.CreateItem();
    }

    public CardVFXItem CreateItem(VFXType vfxType)
    {
        return CreateItem(GetPrefabPath(vfxType));
    }

    public CardVFXItem CreateSpawnEffect(string spawnEffectKey)
    {
        return CreateItem(GetSpawnEffectPath(spawnEffectKey));
    }

    public CardVFXItem CreateAttackEffect(string attackEffectKey)
    {
        return CreateItem(GetAttackEffectPath(attackEffectKey));
    }

    public CardVFXItem CreateDeathEffect(string deathEffectKey)
    {
        return CreateItem(GetDeathEffectPath(deathEffectKey));
    }

    public CardVFXItem CreateItem(string prefabPath)
    {
        if (!IsInit) Init();

        for (int i = 0; i < _pool.Count; i++)
        {
            CardVFXItem item = _pool[i];
            if (item != null && !item.IsLock && item.PrefabPath == prefabPath)
            {
                item.SetLock(true);
                item.GetGameObject().SetActive(true);
                item.SetEnableUpdateFacing(false);
                return _pool[i];
            }
        }

        Debug.Log("Try to create prefab. Path : " + prefabPath);
        GameObject go = MonoBehaviour.Instantiate(ResourceManager.Load(prefabPath)) as GameObject;
        CardVFXItem component = go.GetComponent<CardVFXItem>();
        component.SetPrefabPath(prefabPath);

        _pool.Add(component);
        component.SetLock(true);

        return component;
    }

    protected string GetPrefabPath(VFXType type)
    {
        return RootVFXPath + type.ToString();
    }

    protected string GetSpawnEffectPath(string spawnEffectKey)
    {
        return RootSpawnEFXPath + spawnEffectKey;
    }

    protected string GetAttackEffectPath(string attackEffectKey)
    {
        return RootAttackEFXPath + attackEffectKey;
    }

    protected string GetDeathEffectPath(string deathEffectKey)
    {
        return RootDeathEFXPath + deathEffectKey;
    }
}

