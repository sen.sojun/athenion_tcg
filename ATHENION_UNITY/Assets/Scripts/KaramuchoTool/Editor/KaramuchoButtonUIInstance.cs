﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class KaramuchoButtonUIInstance : Editor
{
    static GameObject _clickGameObject;

    [MenuItem("GameObject/Karamucho UI/Button", priority = 0)]
    public static void AddButton()
    {
        Create("BTN_");
    }

    private static GameObject Create(string objectName)
    {
        GameObject instance = Instantiate(Resources.Load<GameObject>(objectName));
        instance.name = objectName;
        _clickGameObject = UnityEditor.Selection.activeGameObject as GameObject;
        if (_clickGameObject != null)
        {
            instance.transform.SetParent(_clickGameObject.transform, false);
        }
        return instance;
    }
}
