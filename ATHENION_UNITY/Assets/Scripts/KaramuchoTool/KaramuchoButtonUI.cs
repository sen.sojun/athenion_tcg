﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Image))]
[RequireComponent(typeof(ButtonSound))]

[ExecuteInEditMode]
public class KaramuchoButtonUI : Button, IPointerClickHandler
{
    private Color _targetColor = Color.white;
    private bool _interactState;
    private bool _isFirstTime = true;

    private void Update()
    {
        if (_interactState == interactable && !_isFirstTime) return;

        if (interactable)
        {
            _interactState = true;
            _targetColor = colors.normalColor;
        }
        else
        {
            _interactState = false;
            _targetColor = colors.disabledColor;
        }

        ChangeLockEnable(!_interactState);
        ChangeAllColor(GetAllText(), _targetColor);
        //ChangeAllColor(GetAllImage(), _targetColor);
        _isFirstTime = false;
    }

    #region Methods
    private List<TextMeshProUGUI> GetAllText()
    {
        List<TextMeshProUGUI> textList = new List<TextMeshProUGUI>();
        foreach (Transform item in transform)
        {
            TextMeshProUGUI text = item.GetComponent<TextMeshProUGUI>();
            if (text != null)
            {
                textList.Add(text);
            }
        }

        return textList;
    }

    private List<Image> GetAllImage()
    {
        List<Image> imgList = new List<Image>();
        foreach (Transform item in transform)
        {
            Image text = item.GetComponent<Image>();
            if (text != null)
            {
                imgList.Add(text);
            }
        }

        return imgList;
    }

    private void ChangeLockEnable(bool isLock)
    {
        PopupAttachment popup = GetComponent<PopupAttachment>();
        if (popup != null)
        {
            popup.SetEnable(isLock);
        }
    }

    private void ChangeAllColor(List<TextMeshProUGUI> textList, Color color)
    {
        foreach (TextMeshProUGUI item in textList)
        {
            item.faceColor = color;
        }
    }

    private void ChangeAllColor(List<Image> list, Color color)
    {
        foreach (Image item in list)
        {
            item.color = color;
        }
    }
    #endregion
}

