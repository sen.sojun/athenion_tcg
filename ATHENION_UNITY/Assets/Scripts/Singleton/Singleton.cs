﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Singleton<T> where T : new()
{
    #region Public Properties
    public static T Instance
    {
        get
        {
            if (_instance == null)
            {
                Init();
            }

            return _instance;
        }
    }
    #endregion

    #region Protected Properties
    protected static T _instance = default(T);
    #endregion

    #region Methods
    public static void ReInit()
    {
        Init();
    }

    private static void Init()
    {
        _instance = new T();

        //Debug.Log(string.Format("<b>[Singleton]</b> Created {0}.", typeof(T).ToString()));
    }

    public static void DeInit()
    {
        //Debug.Log(string.Format("<b>[MonoSingleton]</b> Removed {0}.", typeof(T).ToString()));

        _instance = default(T);
    }
    #endregion
}
