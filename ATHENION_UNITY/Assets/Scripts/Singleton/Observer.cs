﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class Observer<TClass, TParam> : Singleton<TClass> where TClass : new() where TParam : new()
{
    #region Private Properties
    private Dictionary<string, UnityEvent<TParam>> _events;
    #endregion

    #region Subscribe Methods
    /// <summary>
    /// Subscribe event which be subscribed with the key
    /// </summary>
    /// <typeparam name="T">Type</typeparam>
    /// <param name="key"></param>
    /// <param name="callback"></param>
    public virtual void Subscribe<T>(T key, UnityAction<TParam> callback)
    {
        string keyStr = key.GetType().ToString() + "." + key.ToString();
        if (_events == null)
        {
            _events = new Dictionary<string, UnityEvent<TParam>>();
        }

        if (_events != null)
        {
            if (_events.ContainsKey(keyStr))
            {
                _events[keyStr].AddListener(callback);
            }
            else
            {
                TParamEvent<TParam> unityEvent = new TParamEvent<TParam>();
                unityEvent.AddListener(callback);

                _events.Add(keyStr, unityEvent);
            }
        }
    }
    #endregion

    #region Unsubscribe Methods
    /// <summary>
    /// Unsubscribe event which be subscribed with the key
    /// </summary>
    /// <typeparam name="T">Type</typeparam>
    /// <param name="key"></param>
    /// <param name="callback"></param>
    /// <returns></returns>
    public virtual bool Unsubscribe<T>(T key, UnityAction<TParam> callback)
    {
        string keyStr = key.GetType().ToString() + "." + key.ToString();
        if (_events != null)
        {
            if (_events.ContainsKey(keyStr))
            {
                _events[keyStr].RemoveListener(callback);
                return true;
            }
        }

        return false;
    }
    #endregion

    #region Clear Methods
    /// <summary>
    /// Clear event which be subscribed with the key
    /// </summary>
    /// <typeparam name="T">Type</typeparam>
    /// <param name="key"></param>
    public virtual void Clear<T>(T key)
    {
        string keyStr = key.GetType().ToString() + "." + key.ToString();
        if (_events != null)
        {
            if (_events.ContainsKey(keyStr))
            {
                _events[keyStr].RemoveAllListeners();
            }
        }
    }

    /// <summary>
    /// Clear all subscribed events
    /// </summary>
    public void ClearAll()
    {
        if (_events != null)
        {
            _events.Clear();
        }
    }
    #endregion

    #region Trigger Methods
    /// <summary>
    /// Triggers event which be subscibed with the key
    /// </summary>
    /// <typeparam name="T">Type</typeparam>
    /// <param name="key"></param>
    public virtual void Trigger<T>(T key)
    {
        string keyStr = key.GetType().ToString() + "." + key.ToString();
        if (_events != null)
        {
            if (_events.ContainsKey(keyStr) && _events[keyStr] != null)
            {
                _events[keyStr].Invoke(default(TParam));
            }
        }
    }

    /// <summary>
    /// Triggers event which be subscibed with the key
    /// </summary>
    /// <typeparam name="T">Type</typeparam>
    /// <param name="key"></param>
    /// <param name="param"></param>
    public virtual void Trigger<T>(T key, TParam param)
    {
        string keyStr = key.GetType().ToString() + "." + key.ToString();
        if (_events != null)
        {
            if (_events.ContainsKey(keyStr) && _events[keyStr] != null)
            {
                _events[keyStr].Invoke(param);
            }
        }
    }
    #endregion
}