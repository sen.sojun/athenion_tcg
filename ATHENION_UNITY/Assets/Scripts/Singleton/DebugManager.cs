﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class DebugManager : MonoSingleton<DebugManager>
{
    #region Private Properties
    private readonly float _widthMargin = 20.0f;
    private readonly float _heightMargin = 20.0f;
    private readonly int _maxText = 4000;

    private float _deltaTime = 0.0f;
    private float _msec;
    private float _fps;

    private bool _isHaveGameManager = false;

    private StringBuilder _debugLogText;
    private StringBuilder _debugText;
    private string _debugShowText;
    private Rect _debugTextRect;
    #endregion

    // Use this for initialization
    void Start()
    {
        _debugTextRect = new Rect(
              _widthMargin
            , _heightMargin
            , Screen.width - (2.0f * _widthMargin)
            , Screen.height - (2.0f * _heightMargin)
        );

        _isHaveGameManager = FindMonoSingleton<GameManager>();
    }

    #region Update
    // Update is called once per frame
    void Update()
    {
        UpdateFPS();
    }

    void UpdateFPS()
    {
        _deltaTime += (Time.unscaledDeltaTime - _deltaTime) * 0.1f;
        _msec = _deltaTime * 1000.0f;
        _fps = 1.0f / _deltaTime;
    }
    #endregion

    #region Methods
    private void OnGUI()
    {
        if (_debugText == null) _debugText = new StringBuilder();
        _debugText.Clear();

        // FPS
        {
            _debugText.AppendFormat(
                "<b>{0:0.0} ms (<color=#{2}>{1:0.} FPS</color>)</b>"
                , _msec
                , _fps
                , ((_fps >= 30.0f) ? ((_fps >= 50.0f) ? "00FF00FF" : "FFFF00FF") : "FF0000FF")
            );
        }

        // Screen Size
        _debugText.AppendFormat("\n\n{0}", ScreenManager.GetDebugText());

        // Game Manager
        if (_isHaveGameManager)
        {
            _debugText.AppendFormat("\n\n{0}", GameManager.Instance.GetDebugText());
        }

        // Action Ability Queue
        if (_isHaveGameManager)
        {
            int count = 0;
            if (GameManager.Instance.ActionAbilityCommandList != null)
            {
                count = GameManager.Instance.ActionAbilityCommandList.Count;
            }

            _debugText.AppendFormat(
                "\n\n<color=#{0}>Action Queue : <b>{1}</b></color>"
                , (count > 0) ? "FF0000FF" : "00FF00FF"
                , count
            );
        }

        // Command Queue
        {
            _debugText.AppendFormat("\n\n{0}", CommandQueue.Instance.GetDebugText());
        }

        // UI Queue
        if (_isHaveGameManager && GameManager.Instance.Phase != GamePhase.Init)
        {
            _debugText.AppendFormat("\n\n{0}", UIManager.Instance.GetUIQueueDebugText());
        }

        //// Battle Queue
        //{
        //    _debugText.AppendFormat("\n\n{0}", BattleQueue.Instance.GetDebugText());
        //}

        // Draw debug
        if (_debugText.Length > 0)
        {
            if (_debugText.Length > _maxText)
            {
                _debugShowText = _debugText.ToString().Substring(0, _maxText);
            }
            else
            {
                _debugShowText = _debugText.ToString();
            }

            GUI.Label(_debugTextRect, "<color=white>" + _debugShowText+"</color>");
        }
    }    
    #endregion
}