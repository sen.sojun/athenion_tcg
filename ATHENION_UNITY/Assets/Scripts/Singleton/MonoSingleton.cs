﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public abstract class MonoSingleton<T> : MonoBehaviour where T : MonoSingleton<T>
{
    #region Public Properties
    public static T Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<T>();
            }

            if (_instance == null)
            {
                GameObject obj = new GameObject();
                _instance = obj.AddComponent<T>();
                _instance.InitSelf();

                //Debug.Log(string.Format("<b>[MonoSingleton]</b> Created {0}.", typeof(T).ToString()));
            }

            return _instance;
        }
    }
    #endregion

    #region Protected Properties
    protected static T _instance = null;
    #endregion

    protected virtual void Awake()
    {
        if (_instance != null && _instance != this)
        {
            DestroySelf();
        }
        else if (_instance == null)
        {
            _instance = gameObject.GetComponent<T>();
            _instance.InitSelf();

            //Debug.Log(string.Format("<b>[MonoSingleton]</b> Awaked {0}.", typeof(T).ToString()));
        }
    }

    protected virtual void OnDestroy()
    {
        if (_instance != null && _instance == this)
        {
            _instance = null;
        }
    }

    #region Methods
    protected virtual void InitSelf()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
        SceneManager.sceneUnloaded += OnSceneUnloaded;

        _instance.gameObject.name = "(singleton) " + typeof(T).ToString();
        _instance.gameObject.tag = "MonoSingleton";
        DontDestroyOnLoad(_instance.gameObject);
    }

    public virtual void DestroySelf()
    {
        //Debug.Log(string.Format("<b>[MonoSingleton]</b> Destroyed {0}.", typeof(T).ToString()));

        SceneManager.sceneLoaded -= OnSceneLoaded;
        SceneManager.sceneUnloaded -= OnSceneUnloaded;

        if (_instance != null && _instance == this)
        {
            _instance = null;
        }

        if (Application.isPlaying)
        {
            Destroy(gameObject);
        }
        else
        {
            DestroyImmediate(this);
        }
    }

    public static bool FindMonoSingleton<TClass>() where TClass : MonoSingleton<TClass>
    {
        TClass obj = (TClass)GameObject.FindObjectOfType<TClass>();

        if (obj != null)
        {
            return true;
        }

        return false;
    }
    #endregion

    #region Event Methods
    protected virtual void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
    }

    protected virtual void OnSceneUnloaded(Scene current)
    {
    }
    #endregion
}
