﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestScreenScript : MonoBehaviour
{
    public Slider Slider;
    public Text PercentText;

	// Use this for initialization
	void Start ()
    {
        Slider.value = (100.0f / 5.0f);
        OnValueChanged();
    }
	
	// Update is called once per frame
	void Update ()
    {
	}

    public void OnValueChanged()
    {
        PercentText.text =  Mathf.RoundToInt(Slider.value * 5).ToString() + "%";
    }

    public void OnClickSetup()
    {
        float ratio = (Slider.value * 5)/100.0f;
        ScreenManager.SetScreenSize(ratio);
    }
}
