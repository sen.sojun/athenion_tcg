﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestPool : PrefabPool<TestPool, TestPoolItem>
{
    public override void Init()
    {
        PrefabPath = "Prefabs/TestCube";

        base.Init();
    }
}
