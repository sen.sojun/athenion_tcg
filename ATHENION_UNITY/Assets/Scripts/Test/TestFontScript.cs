﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class TestFontScript : MonoBehaviour
{
    private List<int> _vowelAboveList = new List<int> 
    {
          0xE31 // ั
        , 0xE34 // ิ
        , 0xE35 // ี
        , 0xE36 // ึ
        , 0xE37 // ื
        , 0xE47 // ็
        , 0xE4D // ํ 
    };

    private List<int> _vowelUnderList = new List<int> 
    {
          0xE38 // ุ
        , 0xE39 // ู
        , 0xE3A // ฺ
    };

    private List<int> _vowelPostfixList = new List<int> 
    {
          0xE33 // ำ
    };

    private List<int> _toneList = new List<int> 
    {
          0xE48 // ่
        , 0xE49 // ้
        , 0xE4A // ๊
        , 0xE4B // ๋
        , 0xE4C // ์
    };

    private List<int> _charLongHairList = new List<int>
    {
          0xE1B // ป
        , 0xE1D // ฝ
        , 0xE1F // ฟ
        , 0xE2C // ฬ
    };

    private List<int> _charLongTailList = new List<int>
    {
          0xE0D // ญ
        , 0xE0E // ฎ
        , 0xE0F // ฏ
        , 0xE10 // ฐ
        , 0xE24 // ฤ
        , 0xE26 // ฦ
    };

    // Start is called before the first frame update
    void Start()
    {
        WriteFile(GenText());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public string GenText()
    {
        string result = "";

        /*
        - m_FirstGlyph: 3636
          m_FirstGlyphAdjustments:
            xPlacement: 0
            yPlacement: 0
            xAdvance: 0
            yAdvance: 0
          m_SecondGlyph: 3656
          m_SecondGlyphAdjustments:
            xPlacement: 0
            yPlacement: 8.7
            xAdvance: 0
            yAdvance: 0
           xOffset: 0
         */

        result += string.Format("  m_kerningInfo:\n");
        result += string.Format("    kerningPairs:\n");

        // ยกวรรณยุกต์ เมื่ออยู่กับสระบน
        {
            foreach (int vowelIndex in _vowelAboveList)
            {
                foreach (int toneIndex in _toneList)
                {
                    result += string.Format("    - m_FirstGlyph: {0}\n", vowelIndex);
                    result += string.Format("      m_FirstGlyphAdjustments:\n");
                    result += string.Format("        xPlacement: {0}\n", 0);
                    result += string.Format("        yPlacement: {0}\n", 0);
                    result += string.Format("        xAdvance: {0}\n", 0);
                    result += string.Format("        yAdvance: {0}\n", 0);

                    result += string.Format("      m_SecondGlyph: {0}\n", toneIndex);
                    result += string.Format("      m_SecondGlyphAdjustments:\n");
                    result += string.Format("        xPlacement: {0}\n", 0);
                    result += string.Format("        yPlacement: {0}\n", 11.0f);
                    result += string.Format("        xAdvance: {0}\n", 0);
                    result += string.Format("        yAdvance: {0}\n", 0);

                    result += string.Format("      xOffset: {0}\n", 0);
                }
            }

            foreach (int toneIndex in _toneList)
            {
                foreach (int vowelIndex in _vowelPostfixList)
                {
                    result += string.Format("    - m_FirstGlyph: {0}\n", toneIndex);
                    result += string.Format("      m_FirstGlyphAdjustments:\n");
                    result += string.Format("        xPlacement: {0}\n", 0);
                    result += string.Format("        yPlacement: {0}\n", 10.0f);
                    result += string.Format("        xAdvance: {0}\n", 0);
                    result += string.Format("        yAdvance: {0}\n", 0);

                    result += string.Format("      m_SecondGlyph: {0}\n", vowelIndex);
                    result += string.Format("      m_SecondGlyphAdjustments:\n");
                    result += string.Format("        xPlacement: {0}\n", 0);
                    result += string.Format("        yPlacement: {0}\n", 0);
                    result += string.Format("        xAdvance: {0}\n", 0);
                    result += string.Format("        yAdvance: {0}\n", 0);

                    result += string.Format("      xOffset: {0}\n", 0);
                }
            }
        }

        // อักษร "ผมยาว" เลื่อนสระบนไปข้างหน้า
        {
            foreach (int charIndex in _charLongHairList)
            {
                foreach (int vowelIndex in _vowelAboveList)
                {
                    result += string.Format("    - m_FirstGlyph: {0}\n", charIndex);
                    result += string.Format("      m_FirstGlyphAdjustments:\n");
                    result += string.Format("        xPlacement: {0}\n", 0);
                    result += string.Format("        yPlacement: {0}\n", 0);
                    result += string.Format("        xAdvance: {0}\n", 0);
                    result += string.Format("        yAdvance: {0}\n", 0);

                    result += string.Format("      m_SecondGlyph: {0}\n", vowelIndex);
                    result += string.Format("      m_SecondGlyphAdjustments:\n");
                    result += string.Format("        xPlacement: {0}\n", -7.0f);
                    result += string.Format("        yPlacement: {0}\n", 0);
                    result += string.Format("        xAdvance: {0}\n", 0);
                    result += string.Format("        yAdvance: {0}\n", 0);

                    result += string.Format("      xOffset: {0}\n", 0);
                }
            }
        }

        // อักษร "หางยาว" เลื่อนสระล่างลง
        {
            foreach (int charIndex in _charLongTailList)
            {
                foreach (int vowelIndex in _vowelUnderList)
                {
                    result += string.Format("    - m_FirstGlyph: {0}\n", charIndex);
                    result += string.Format("      m_FirstGlyphAdjustments:\n");
                    result += string.Format("        xPlacement: {0}\n", 0);
                    result += string.Format("        yPlacement: {0}\n", 0);
                    result += string.Format("        xAdvance: {0}\n", 0);
                    result += string.Format("        yAdvance: {0}\n", 0);

                    result += string.Format("      m_SecondGlyph: {0}\n", vowelIndex);
                    result += string.Format("      m_SecondGlyphAdjustments:\n");
                    result += string.Format("        xPlacement: {0}\n", 0);
                    result += string.Format("        yPlacement: {0}\n", -11.25f);
                    result += string.Format("        xAdvance: {0}\n", 0);
                    result += string.Format("        yAdvance: {0}\n", 0);

                    result += string.Format("      xOffset: {0}\n", 0);
                }
            }
        }

        return result;
    }

    public void WriteFile(string text)
    {
        string path = "Assets/Resources/font.txt";

        //Write some text to the test.txt file
        StreamWriter writer = new StreamWriter(path, true);
        writer.Write(text);
        writer.Close();

        //Print the text from the file
        Debug.Log("Write successful. " + path);
    }
}
