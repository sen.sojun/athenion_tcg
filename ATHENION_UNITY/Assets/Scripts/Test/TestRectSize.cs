﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestRectSize : MonoBehaviour
{
    public void GetRectSize(GameObject gObj)
    {
        RectTransform rectTransform = gObj.GetComponent<RectTransform>();
        Debug.Log(gObj.name + " " + rectTransform.rect.size);

        rectTransform.anchoredPosition = new Vector2(-rectTransform.rect.size.x / 2, -rectTransform.rect.size.x / 2);
    }

}
