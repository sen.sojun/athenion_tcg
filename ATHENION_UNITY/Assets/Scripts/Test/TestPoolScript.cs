﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class TestPoolScript : MonoBehaviour
{
    #region Public Porperties
    public Vector2 AreaSize;
    #endregion

    #region Private Properties
    private float _timer = 0.0f;
    private StringBuilder _debugText;
    #endregion

    // Use this for initialization
    void Start ()
    {
    }
	
	// Update is called once per frame
	void Update ()
    {
        _timer -= Time.deltaTime;
        if (_timer <= 0.0f)
        {
            _timer += Random.Range(0.1f, 0.5f);
            SpawnObject();
        }
	}

    private void SpawnObject()
    {
         TestPoolItem item = TestPool.Instance.CreateItem();
         item.transform.position = GetRandomPosition();
         item.Init(1.0f, RemoveObject);
    }

    private void RemoveObject(TestPoolItem item)
    {
        TestPool.Instance.ReleaseItem(item);
    }

    private Vector3 GetRandomPosition()
    {
        Vector3 position = transform.position;

        position += new Vector3(
              Random.Range(-AreaSize.x * 0.5f, AreaSize.x * 0.5f)
            , 0.0f
            , Random.Range(-AreaSize.y * 0.5f, AreaSize.y * 0.5f)
        );

        return position;
    }

    private void OnGUI()
    {
        if (_debugText == null) _debugText = new StringBuilder();
        _debugText.Clear();

        // Command Queue
        _debugText.Append(TestPool.Instance.GetDebugText());

        GUI.Label(
            new Rect(10, 10, Screen.width - 20, Screen.height - 20)
            , _debugText.ToString()
        );
    }
}
