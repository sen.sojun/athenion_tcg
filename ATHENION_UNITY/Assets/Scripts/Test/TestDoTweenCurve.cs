﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TestDoTweenCurve : MonoBehaviour {

    public Transform Actor;
    public Transform StartLocation;
    public Transform EndLocation;
    public Ease Ease;

    private Vector3 _jumpVector = new Vector3(0, 1.0f, 0);

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            MoveToTarget(Actor);
        }
	}

    public void MoveToTarget(Transform actor)
    {
        actor.position = StartLocation.position;

        Sequence sq = DOTween.Sequence();
        //sq.Append(actor.DOMove(EndLocation.position, 1.0f));
        sq.Append(actor.DOJump(EndLocation.position, 2.0f, 1, 1.0f).SetEase(Ease));
    }
}
