﻿using PlayFab;
using PlayFab.ClientModels;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class TestAssetBundleScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Caching.ClearCache();

        /*
        AssetBundleManager.LoadAssetBundleFromFile(
              "AssetBundles/Android/databasebundle"
            , 0
            , delegate(AssetBundle bundle1)
            {
                AssetBundleManager.LoadAssetBundleFromFile(
                      "AssetBundles/Android/localizebundle"
                    , 0
                    , delegate (AssetBundle bundle2)
                    {
                        DBManager.LoadDB();
                    }
                    , ShowProgress
                );
            }
            , ShowProgress
        );
        */
    }

    public void ShowProgress(float progress)
    {
        Debug.LogFormat("Progress : {0}", progress);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
