﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class TestQueueScript : MonoBehaviour
{
    private float _timer = 0.0f;
    private StringBuilder _debugText;

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        _timer -= Time.deltaTime;

        if (_timer <= 0.0f)
        {
            _timer += Random.Range(0.5f, 0.6f);

            if (Random.Range(0, 100) % 2 == 0)
            {
                AddWaitQueue(1.0f);
            }
            else
            {
                JoinWaitQueue(1.0f);
            }
        }
    }

    #region Methods
    void AddWaitQueue(float waitTime)
    {
        WaitCommand waitCmd = new WaitCommand(waitTime);
        CommandQueue.Instance.Add(waitCmd);
    }

    void JoinWaitQueue(float waitTime)
    {
        WaitCommand waitCmd = new WaitCommand(waitTime);
        CommandQueue.Instance.Join(waitCmd);
    }

    private void OnGUI()
    {
        if (_debugText == null) _debugText = new StringBuilder();
        _debugText.Clear();

        // Command Queue
        _debugText.Append(CommandQueue.Instance.GetDebugText());

        GUI.Label(
            new Rect(10, 10, Screen.width - 20, Screen.height - 20)
            , _debugText.ToString()
        );
    }
    #endregion
}
