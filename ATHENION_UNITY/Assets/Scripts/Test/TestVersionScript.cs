﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestVersionScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        CreateVersionData();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CreateVersionData()
    {
        ClientVersion version = ClientVersion.CreateVersionData();

        Debug.Log(version.ToJson());
    }
}
