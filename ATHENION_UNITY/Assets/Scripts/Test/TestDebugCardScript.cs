﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Karamucho.UI;
using Karamucho.Collection;
using TMPro;
using System.Linq;
using System;

public class TestDebugCardScript : MonoBehaviour
{
    [Header("Input")]
    public TMP_InputField TEXT_Input;

    [Header("Super Size")]
    public int superSize = 1;

    [Header("Card")]
    public CardUI_Collection CardUI;
    public CardUI TokenUI;
    public TextMeshProUGUI CardText;

    private List<CardDBData> _dataList = null;
    private int _index = 0;

    private string _cardID;

	// Use this for initialization
	void Start () 
    {
        Init();
    }
	
	// Update is called once per frame
	void Update () 
    {
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            ShowNext();
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            ShowPrev();
        }
    }

    public void Init()
    {
        if (CardDB.Instance.GetAllData(out _dataList))
        {
            SetCardInfo(0);
        }
    }

    public void SetCardInfo(int index)
    {
        _index = index;

        if (_dataList != null && _dataList.Count > _index)
        {
            //Debug.LogFormat("[{0}] {1}", _dataList[_index].ID , _dataList[_index].Name);
            CardData cardData = CardData.CreateCard(_dataList[_index].ID);
            CardUIData cardUIData = new CardUIData(-1, cardData, DataManager.Instance.DefaultCardBackIDList[0]);
            _cardID = cardUIData.FullID;

            CardUI.SetData(cardUIData);
            CardUI.SetOwned(true);
            CardUI.SetCardAmount(1);

            CardText.text = _dataList[_index].ID;

            //SoundManager.PlayMinionSummonVoice(cardData.BaseID);
        }
    }

    public void SetCardInfo(string cardID)
    {
        CardData cardData = CardData.CreateCard(cardID);
        CardUIData cardUIData = new CardUIData(-1, cardData, DataManager.Instance.DefaultCardBackIDList[0]);
        _cardID = cardUIData.FullID;

        CardUI.SetData(cardUIData);
        CardUI.SetOwned(true);
        CardUI.SetCardAmount(1);

        CardText.text = cardID;
    }

    public void ShowNext()
    {
        if (_dataList != null)
        {
            _index = _index + 1;
            if (_index >= _dataList.Count)
            {
                StopAllCoroutines();
                Debug.Log("<b>Hide Soft Load</b>");
                PopupUIManager.Instance.HideAllSoftLoad();
                _index = 0;
            }

            SetCardInfo(_index);
        }
    }

    public void ShowPrev()
    {
        if (_dataList != null)
        {
            _index = _index - 1;
            if (_index < 0) _index = _dataList.Count - 1;

            SetCardInfo(_index);
        }
    }

    public void GoToMenu()
    {
        PopupUIManager.Instance.Show_MidSoftLoad(true);

        GameHelper.LoadSceneAsync(
              GameHelper.NavigatorSceneName
            , LoadSceneMode.Single
            , delegate () { PopupUIManager.Instance.HideAllSoftLoad();}
            , null
        );
    }

    #region Capture Methods
    public void AutoCapture()
    {
        StartCoroutine(CourotineAutoCapture());
    }

    public IEnumerator CourotineAutoCapture()
    {
        Debug_CaptureScreen();
        for (int i = 0; i < _dataList.Count; i++)
        {
            ShowNext();
            yield return new WaitForEndOfFrame();
            Debug_CaptureScreen();
        }

    }

    public void Debug_CaptureScreen()
    {
        // No Input
        Debug.Log("Capture :" + " Card_" + _cardID.ToString());
        string imagePath = string.Format("Screenshot/{1}/{1}_Card_{0}{2}", _cardID, LocalizationManager.Instance.CurrentLanguage.ToString(), ".png");
        ScreenCapture.CaptureScreenshot(imagePath, superSize);
    }

    public void SearchCard()
    {
        if (string.IsNullOrEmpty(TEXT_Input.text)) return;
        // Calculate input
        //string[] cardID;
        string input = TEXT_Input.text.ToUpper();
        //cardID = input.Split('-');
        //Debug.Log("First:" + cardID[0].ToString() + "|Last:" + cardID.Last().ToString());

        //List<int> result = new List<int>();
        //foreach (string item in cardID)
        //{
        //    string code = item.Remove(0); // remove 'C' on card id (C0001)
        //    int number = 0;
        //    Int32.TryParse(code, out number);

        //    if (number != 0)
        //    {
        //        result.Add(number);
        //    }
        //}

        //int startNumber = result.First();
        //int endNumber = result.Last();
        //int delta = endNumber - startNumber;

        //List<string> cardIDList = new List<string>();
        //for (int i = 0; i < delta; i++)
        //{
        //    string cardKey = "C0000";
        //    int id = startNumber + i;

        //    if (id < 10)
        //    {
        //        cardKey = "C000{0}";
        //    }
        //    else if (id < 100)
        //    {
        //        cardKey = "C00{0}";
        //    }
        //    else if(id < 1000)
        //    {
        //        cardKey = "C0{0}";
        //    }
        //    else
        //    {
        //        cardKey = "C{0}";
        //    }

        //    cardIDList.Add(string.Format(cardKey, id));
        //}

        SetCardInfo(input);
    }
    #endregion

    #region Loading Methods
    public void StartLoading()
    {
        StartCoroutine(LoadingMemory());
    }

    public IEnumerator LoadingMemory()
    {
        PopupUIManager.Instance.Show_MidSoftLoad(true);
        for(int i = 0; i < _dataList.Count; i++)
        {
            ShowNext();
            yield return new WaitForEndOfFrame();
        }
    }
    #endregion


}
