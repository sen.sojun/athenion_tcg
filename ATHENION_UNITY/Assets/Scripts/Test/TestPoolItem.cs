﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TestPoolItem : PoolItem
{
    #region Public Properties
    public float Timer { get { return _timer; } }
    #endregion

    #region Private Properties
    private float _timer = 0.0f;
    private bool _isInit = false;
    private UnityAction<TestPoolItem> _onComplete;
    #endregion

    // Use this for initialization
    void Start ()
    {
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (_isInit)
        {
            _timer -= Time.deltaTime;
            if (_timer <= 0.0f)
            {
                // done
                OnTimeout();
            }
        }
    }

    public void Init(float waitTime, UnityAction<TestPoolItem> onComplete)
    {
        _timer = waitTime;
        _onComplete = onComplete;

        _isInit = true;
    }

    private void OnTimeout()
    {
        _isInit = false;

        if (_onComplete != null)
        {
            _onComplete.Invoke(this);
        }
    }

    private void OnGUI()
    {
        
    }
}
