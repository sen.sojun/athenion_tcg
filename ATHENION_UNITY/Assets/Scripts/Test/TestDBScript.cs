﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class TestDBScript : MonoBehaviour
{
    private StringBuilder _debugText;

    // Use this for initialization
    void Start()
    {
        InitDB();
    }

	// Update is called once per frame
	void Update ()
    {
	}

    #region Methods
    void InitDB()
    {
        foreach (DBManager.DBType dbType in System.Enum.GetValues(typeof(DBManager.DBType)))
        {
            Debug.Log(string.Format("<b>{0}</b>", dbType));
            DBManager.LoadDB(dbType);

            if (DBManager.IsLoad(dbType))
            {
                Debug.Log(DBManager.GetDebugText(dbType));
            }
        }
    }

    private void OnGUI()
    {
        if (_debugText == null) _debugText = new StringBuilder();
        _debugText.Clear();

        foreach (DBManager.DBType dbType in System.Enum.GetValues(typeof(DBManager.DBType)))
        {
            string text = string.Format(
                  "{0} : {1}"
                , dbType
                , (DBManager.IsLoad(dbType) ? "<color=#00FF00FF>Loaded</color>" : "<color=#FF0000FF>Unload</color>")
            );

            if (_debugText.Length > 0)
            {
                _debugText.AppendFormat("\n{0}", text);
            }
            else
            {
                _debugText.Append(text);
            }
        }

        GUI.Label(
            new Rect(10, 10, Screen.width - 20, Screen.height - 20)
            , _debugText.ToString()
        );
    }
    #endregion
}