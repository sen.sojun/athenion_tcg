﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationSound : MonoBehaviour
{
    public SoundManager.SFX SFX;

    public void Play_SFX()
    {
        SoundManager.PlaySFX(SFX);
    }

    public void Play_CustomSFX(SoundManager.SFX sfx)
    {
        SoundManager.PlaySFX(sfx);
    }
}
