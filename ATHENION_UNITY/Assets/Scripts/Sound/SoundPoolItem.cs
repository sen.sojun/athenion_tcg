﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;

[RequireComponent(typeof(AudioSource))]
public class SoundPoolItem : PoolItem
{
    #region Public Propertries
    public int SoundIndex { get; private set; }
    public string Key { get; private set; }
    public SoundManager.SoundType Type { get; private set; }
    #endregion

    #region Private Properties
    private static readonly float _defaultFadeTime = 2.0f;

    private AudioSource _source = null;
    private bool _isStartPlay = false;

    private float _fadeTime = _defaultFadeTime;
    private float _fadeSpeed = 0.0f;
    private float _volume;
    private float _targetVolume;
    private bool _isStop = false;
    private UnityAction _onComplete;

    private Sequence _sq = null;
    #endregion

    #region Update
    void Update()
    {
        if (_source != null && _isStartPlay)
        {
            if (!_source.isPlaying)
            {
                OnPlayComplete();
            }
        }
    }
    #endregion

    #region Methods
    public void SetSoundIndex(int index)
    {
        SoundIndex = index;
    }

    public void SetKey(string key)
    {
        Key = key;
    }

    public void SetType(SoundManager.SoundType type)
    {
        Type = type;
    }

    public void SetLoop(bool isLoop)
    {
        _source = GetComponent<AudioSource>();
        if (_source != null)
        {
            _source.loop = isLoop;
        }
    }

    public void SetVolume(float volume, float fadeTime = 0.0f, UnityAction onFadeComplete = null)
    { 
        _targetVolume = volume;
        _fadeTime = fadeTime;

        if (_sq != null)
        {
            if (_sq.onComplete != null) _sq.onComplete.Invoke();
            _sq.Kill();
        }

        if (_fadeTime <= 0.0f)
        {
            _SetVolume(_targetVolume);
        }
        else
        {
            _sq = DOTween.Sequence();
            _sq.Append(DOTween.To(
                 () => _volume
                , x => _SetVolume(x)
                , _targetVolume
                , fadeTime
            ));

            _sq.SetEase(Ease.Linear).OnComplete(delegate() 
            {
                if (onFadeComplete != null)
                {
                    onFadeComplete.Invoke();
                }
            });

            _sq.Play();
        }
    }

    private void _SetVolume(float volume)
    {
        _volume = volume;
        _source = GetComponent<AudioSource>();

        if (_source != null)
        {
            _source.volume = volume;
        }

        if (_isStop)
        {
            if (_volume == _targetVolume)
            {
                OnStopComplete();
            }
        }
    }

    public void SetPriority(int priority)
    {
        _source = GetComponent<AudioSource>();
        if (_source != null)
        {
            _source.priority = priority;
        }
    }

    public void Play(AudioClip clip, UnityAction onComplete)
    {
        _source = GetComponent<AudioSource>();
        if (_source != null)
        {
            _source.clip = clip;
            _source.Play();

            _onComplete = onComplete;
            _isStartPlay = true;
            _isStop = false;

            if (_source.loop)
            {
                if (_onComplete != null)
                {
                    _onComplete.Invoke();
                }

                _onComplete = null;
            }
        }
    }

    public void Stop(float fadeTime = 0.0f, UnityAction onStop = null)
    {
        _isStop = true;
        SetVolume(
              0.0f
            , fadeTime
            , delegate() 
            {
                if (onStop != null)
                {
                    onStop.Invoke();
                }
                OnStopComplete();
            }
        );
    }

    private void OnStopComplete()
    {
        if (_isStartPlay)
        {
            _source = GetComponent<AudioSource>();
            _source.Stop();
            OnPlayComplete();
        }
    }

    public void OnPlayComplete()
    {
        _isStartPlay = false;
        _isStop = false;

        if (_onComplete != null)
        {
            _onComplete.Invoke();
        }

        SoundPool.Instance.ReleaseItem(this);
    }
    #endregion
}
