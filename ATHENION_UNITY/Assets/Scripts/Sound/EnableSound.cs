﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableSound : MonoBehaviour
{
    public SoundManager.SFX Sound;

    /*
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    */

    private void OnEnable()
    {
        PlaySound();
    }

    private void PlaySound()
    {
        SoundManager.PlaySFX(Sound);
    }
}
