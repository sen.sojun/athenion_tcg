﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class ToggleSound : MonoBehaviour, IPointerClickHandler
{
    public SoundManager.SFX Sound;


    private void PlaySound()
    {
        SoundManager.PlaySFX(Sound);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        PlaySound();
    }
}
