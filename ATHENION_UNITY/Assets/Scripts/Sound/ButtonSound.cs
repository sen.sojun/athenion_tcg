﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class ButtonSound : MonoBehaviour, IPointerClickHandler
{
    public SoundManager.SFX Sound;

    private void PlaySound()
    {
        SoundManager.PlaySFX(Sound);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        Button btn = GetComponent<Button>();
        if (btn != null)
        {
            if(btn.interactable) PlaySound();
        }
        else
        {
            Debug.LogError(this.gameObject.name + "Not have Button");
        }

    }
}
