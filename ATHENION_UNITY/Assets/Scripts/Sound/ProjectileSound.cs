﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileSound : MonoBehaviour
{
    public SoundManager.SFX _sfx;

    private void OnEnable()
    {
        Play_SFX();
    }

    public void Play_SFX()
    {
        SoundManager.PlaySFX(_sfx);
    }

}
