﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundHeroData
{
    #region Public Properties
    public string HeroID { get { return _heroID; } }
    public HeroVoice HeroVoiceIndex { get { return _heroVoiceIndex; } }
    public string VoiceKey { get { return _heroVoiceKey; } }
    #endregion

    #region Prefix Key
    private const string _heroBeginPrefix = "HERO_TALK_BEGIN";
    private const string _heroLosePrefix = "HERO_TALK_LOSE";

    private const string _heroGreetingPrefix = "HERO_TALK_GREETING";
    private const string _heroThreatenPrefix = "HERO_TALK_THREATEN";
    private const string _heroGoodMovePrefix = "HERO_TALK_GOODMOVE";
    private const string _heroMistakePrefix = "HERO_TALK_MISTAKE";
    private const string _heroThankPrefix = "HERO_TALK_THANK";
    private const string _heroSorryPrefix = "HERO_TALK_SORRY";
    #endregion

    #region Private Properties
    private string _heroID = "";
    private HeroVoice _heroVoiceIndex;
    private string _heroVoiceKey = "";
    #endregion

    public SoundHeroData(string heroID, HeroVoice heroVoiceIndex)
    {
        _heroID = heroID;
        _heroVoiceIndex = heroVoiceIndex;

        _heroVoiceKey = GetPlayHeroVoiceKey();
    }

    public SoundHeroData(string heroID, BaseHeroAnimation.HeroAnimationState state)
    {
        _heroID = heroID;
        _heroVoiceIndex = GetHeroVoice(state);

        _heroVoiceKey = GetPlayHeroVoiceKey();
    }

    private string GetPlayHeroVoiceKey()
    {
        string key = "";
        int index = Random.Range(1, 3);

        switch (_heroVoiceIndex)
        {
            case HeroVoice.Begin: key = string.Format("{0}_{1}_{2}", _heroBeginPrefix, index, _heroID); break;
            case HeroVoice.Lose: key = string.Format("{0}_{1}", _heroLosePrefix, _heroID); break;
            case HeroVoice.Greeting: key = string.Format("{0}_{1}", _heroGreetingPrefix, _heroID); break;
            case HeroVoice.Threaten: key = string.Format("{0}_{1}", _heroThreatenPrefix, _heroID); break;
            case HeroVoice.GoodMove: key = string.Format("{0}_{1}", _heroGoodMovePrefix, _heroID); break;
            case HeroVoice.Mistake: key = string.Format("{0}_{1}", _heroMistakePrefix, _heroID); break;
            case HeroVoice.Thank: key = string.Format("{0}_{1}", _heroThankPrefix, _heroID); break;
            case HeroVoice.Sorry: key = string.Format("{0}_{1}", _heroSorryPrefix, _heroID); break;
        }

        return key;
    }

    private HeroVoice GetHeroVoice(BaseHeroAnimation.HeroAnimationState state)
    {
        return BaseHeroAnimation.GetVoiceIndexFromAnimationState(state);
    }

}
