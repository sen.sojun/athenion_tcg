﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Karamucho;

public class SoundManager //: Singleton<SoundManager>
{
    #region Enums
    public enum SoundType
    {
        SFX = 1
        , BGM = 2
        , Voice = 3
    }

    public enum SFX
    {
        // Out-Game
        __________OutGame__________ = 0
        , Button_Click = 1
        , Cancel_Click = 2
        , More_Click = 3
        , Scroll_Sound = 4
        , Pack_Opening = 5
        , Pack_Flip_Rare = 6
        , Pack_Flip_Epic = 7
        , Pack_Flip_Legendary = 8
        , GotReward_Fanfare = 9
        , GotReward_ItemPop = 10
        , Collection_Craft = 11
        , Collection_Destroy = 12
        , MainMenu_Click = 13
        , MainMenu_Battle_Click = 14
        , Tutorial_UnlockStage = 15
        , RankUpdate_Ambient = 1001
        , RankUpdate_Show = 1002

        // In-Game
        , __________InGame__________ = 10000
        , Open_Log = 10001
        , Close_Log = 10002
        , End_Turn_Click = 10003
        , Card_Click = 10004
        , Slot_Click = 10005
        , Graveyard_Click = 10006
        , Open_Close_Graveyard = 10007
        , Toggle_Hand_Card = 10008
        , Draw_Card = 10009
        , Draw_Skull = 10010
        , Draw_Skull_Burn = 10011
        , Coin_Flick = 10012
        , Turn_Start = 10013
        , Mulligan_Countdown_Tick = 10014
        , Mulligan_Countdown_Ended = 10015
        , Countdown_Ended = 10016
        , Countdown_Loop = 10017
        , Countdown_Start = 10018
        , Drag_Card_Loop = 10019
        , Heal = 10020
        , Immune_Barrier_Activated = 10021
        , Minion_Attack = 10022
        , Minion_Be_Attacked = 10023
        , Minion_Armor_Be_Attacked = 10024
        , Minion_Summon_Start = 10025
        , Minion_Landing_Bronze = 10026
        , Minion_Landing_Silver = 10027
        , Minion_Landing_Gold = 10028
        , Shuffle_Deck = 10029
        , Summon_Card = 10030
        , Token_Destroy = 10031
        , Minion_Move = 10032
        , Graveyard_Open = 10033
        , Token_Banish = 10034
        , Turn_ButtonUnlock = 10035

        // Card Ability     
        , __________CardAbility__________ = 20000
        , Buff_Sound = 20001
        , Debuff_Sound = 20002
        , Ability_Activating = 20003
        , Taunt_Start = 20004
        , Arrow_Changed = 20005
            , _________AbilityIn__________ = 21000
            , Ability_Aura_In = 21001
            , Ability_Backstab_In = 21002
            , Ability_DarkMatter_In = 21003
            , Ability_DarkPower_In = 21004
            , Ability_Freeze_In = 21005
            , Ability_Lock_In = 21006
            , Ability_Silence_In = 21007
            , Ability_Stealth_In = 21008
            , __________AbilityOut__________ = 22000
            , Ability_DarkMatter_Out = 22003
            , Ability_Lock_Out = 22005

        // Deal Damage
        , __________DealDamage__________ = 30000
        , Deal_Damage_Start_Rock = 30001
        , Deal_Damage_Start_Arrow = 30003
        , Deal_Damage_Start_Water = 30005
        , Deal_Damage_Start_Magic = 30007
        , Deal_Damage_Start_Fire_Ball = 30009
        , Deal_Damage_Start_Lightning = 30011

        , Deal_Damage_Hit_Rock = 30002
        , Deal_Damage_Hit_Arrow = 30004
        , Deal_Damage_Hit_Water = 30006
        , Deal_Damage_Hit_Magic = 30008
        , Deal_Damage_Hit_Fire_Ball = 30010
        , Deal_Damage_Hit_Lightning = 30012

        // Soul Attack
        , __________SoulAttack__________ = 40000
        , Soul_Attack_Start_Fire = 40001
        , Soul_Attack_Start_Water = 40003
        , Soul_Attack_Start_Earth = 40005
        , Soul_Attack_Start_Wind = 40007
        , Soul_Attack_Start_Holy = 40009
        , Soul_Attack_Start_Dark = 40011

        , Soul_Attack_Hit_Fire = 40002
        , Soul_Attack_Hit_Water = 40004
        , Soul_Attack_Hit_Earth = 40006
        , Soul_Attack_Hit_Wind = 40008
        , Soul_Attack_Hit_Holy = 40010
        , Soul_Attack_Hit_Dark = 40012

        , Soul_Attack_Collected = 40013

        // End Game
        , __________EndGame__________ = 50000
        , Match_Victory = 50001
        , Match_Lose = 50002
        , End_Game_Hand_Card_Burn = 50003
        , End_Game_Hero_Token_Break = 50004
        , End_Game_Result_Flag = 50005
        , End_Game_Win_Crystal = 50006
        , End_Game_Lose_Crystal = 50007
        , End_Game_Button_Click = 50008
        , Exp_Increasing_Loop = 50009
        , Exp_Level_Up = 50010

        , End_Game_Crystal_Lost_Breaking = 50011
        , End_Game_Hero_Breaking = 50012
        , End_Game_Lost_Charging = 50013
        , End_Game_Lost_Horror_Chord = 50014

        // Intro Game
        , __________IntroGame__________ = 60000
        , Intro_HeroIn = 60001
        , Intro_HeroOut = 60002
        , Intro_PlayerIn = 60003
        , Intro_Versus = 60004

        // Event
        , __________Event_____________ = 70000
        , SFX_OPEN_TREASURE_HUNTER_2019 = 70001
        , SFX_OPEN_FIND_TURTLE_2019 = 70002
        , SFX_RPS_BUBBLE_SPAWN = 70003
        , SFX_RPS_VICTORY = 70004
        , SFX_RPS_DEFEATED = 70005

        // Card SFX  --> 1 + CardID + | 0Cast, 1Projectile, 2Burst, 3Spawn, 4Death (exm 1_0052_0)
        , __________CARDSFX_____________ = 100000
            , SFX_BURST_C0051 = 100512
            , SFX_BURST_C0052 = 100522
            , SFX_BURST_C0182 = 101822
            , SFX_BURST_C0185 = 101852
            , SFX_BURST_C0272 = 102722
            , SFX_CAST_C0052 = 100520
            , SFX_CAST_C0185 = 101850
            , SFX_CAST_C0272 = 102720
            , SFX_PROJECTILE_C0272 = 102721
            , SFX_SPAWN_C0013 =100131
            , SFX_SPAWN_C0049 = 100493
            , SFX_SPAWN_C0050 = 100503
            , SFX_SPAWN_C0051 = 100513
            , SFX_SPAWN_C0052 = 100523
            , SFX_SPAWN_C0053 = 100533
            , SFX_SPAWN_C0054 = 100543
            , SFX_SPAWN_C0102 = 101023
            , SFX_SPAWN_C0161 = 101613
            , SFX_SPAWN_C0164 = 101643
            , SFX_SPAWN_C0165 = 101653
            , SFX_SPAWN_C0180 = 101803
            , SFX_SPAWN_C0182 = 101823
            , SFX_SPAWN_C0185 = 101853
            , SFX_SPAWN_C0212 = 102123
            , SFX_SPAWN_C0216 = 102163
            , SFX_SPAWN_C0236 = 102363
            , SFX_SPAWN_C0237 = 102373
            , SFX_SPAWN_C0245 = 102453
            , SFX_SPAWN_C0248 = 102483
            , SFX_SPAWN_C0249 = 102493
            , SFX_SPAWN_C0271 = 102713
            , SFX_SPAWN_C0272 = 102723
            , SFX_SPAWN_C0274 = 102743
            , SFX_SPAWN_C0304 = 103043
            , SFX_SPAWN_C0318 = 103183
            , SFX_SPAWN_C0347 = 103473
    }

    public enum BGM
    {
        Login = 0
        , Menu = 1
        , Intro = 2
        , Battle = 3
        , Collection = 4
        , Matching = 5
    }
    #endregion

    #region Properties
    private static int _sfxIndex = 0;
    private static readonly string _rootSFXPath = "Sounds/SFX/";
    private static readonly string _rootBGMPath = "Sounds/BGM/";
    private static readonly string _rootVoicePath = "Sounds/Voices/";
    private static readonly string _minionVoiceFolder = "Minions/";
    private static readonly string _heroVoiceFolder = "Heroes/";
    private static readonly string _TutorialFolder = "Tutorials/";
    private static readonly string _AdventureFolder = "Tutorials/";

    private static readonly string _minionSummonPrefix = "VOICE_SUMMON";
    private static readonly string _minionDeathPrefix = "VOICE_DEATH";

    private static float _defaultBGMVolume = 0.25f;
    private static float _defaultSFXVolume = 0.5f;
    private static float _defaultVoiceVolume = 0.6f;

    /*
    private static readonly int _bgmPriority = 0;
    private static readonly int _sfxPriority = 255;
    private static readonly int _voicePriority = 128;
    */

    private static bool _isBGMOn = true;
    private static bool _isSFXOn = true;
    private static bool _isVoiceOn = true;

    private static SoundPoolItem _currentBGM = null;

    private static int _index = 0;
    private static List<SoundPoolItem> _currentBGMList = null;
    private static List<SoundPoolItem> _currentVoiceList = null;
    private static List<SoundPoolItem> _currentSFXList = null;
    #endregion

    #region Methods
    public static void InitVolume()
    {
        _isBGMOn = DataManager.Instance.GetBGMOn();
        _isSFXOn = DataManager.Instance.GetSFXOn();
        _isVoiceOn = DataManager.Instance.GetVoiceOn();
    }

    public static int PlaySFX(SFX sfx, bool isLoop = false, UnityAction onComplete = null)
    {
        AudioClip clip = ResourceManager.Load<AudioClip>(_rootSFXPath + sfx.ToString());
        if (clip != null)
        {
            return PlaySFX(sfx.ToString(), clip, isLoop, onComplete);
        }
        else
        {
            if (onComplete != null)
            {
                onComplete.Invoke();
            }
        }

        return -1;
    }

    public static int PlaySFX(string key, bool isLoop = false, UnityAction onComplete = null)
    {
        AudioClip clip = ResourceManager.Load<AudioClip>(_rootSFXPath + key);
        if (clip != null)
        {
            return PlaySFX(key, clip, isLoop, onComplete);
        }
        else
        {
            if (onComplete != null)
            {
                onComplete.Invoke();
            }
        }

        return -1;
    }

    public static int PlaySFX(string key, AudioClip clip, bool isLoop, UnityAction onComplete = null)
    {
        if (_currentSFXList == null)
        {
            _currentSFXList = new List<SoundPoolItem>();
        }

        if (clip != null)
        {
            SoundPoolItem sfxItem = SoundPool.Instance.CreateItem();
            sfxItem.gameObject.name = "SFX_" + key;
            MonoBehaviour.DontDestroyOnLoad(sfxItem.gameObject);

            int soundIndex = RequestSoundIndex();
            sfxItem.SetSoundIndex(soundIndex);
            sfxItem.SetKey(key);
            sfxItem.SetLoop(isLoop);
            sfxItem.SetType(SoundType.SFX);
            if (_isSFXOn)
            {
                sfxItem.SetVolume(_defaultSFXVolume);
            }
            else
            {
                sfxItem.SetVolume(0.0f);
            }

            _currentSFXList.Insert(0, sfxItem);
            UpdatePriority();

            if (isLoop)
            {
                sfxItem.Play(clip, null);

                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            }
            else
            {
                sfxItem.Play(clip, delegate ()
                {
                    RemoveSFX(sfxItem.SoundIndex);
                    UpdatePriority();

                    if (onComplete != null)
                    {
                        onComplete.Invoke();
                    }
                });
            }

            return soundIndex;
        }
        else
        {
            Debug.Log("PlaySFX: clip is null. " + key);
            if (onComplete != null)
            {
                onComplete.Invoke();
            }
        }

        return -1;
    }

    private static void RemoveSFX(int soundIndex)
    {
        for (int index = 0; index < _currentSFXList.Count; ++index)
        {
            if (_currentSFXList[index].SoundIndex == soundIndex)
            {
                SoundPoolItem soundItem = _currentSFXList[index];
                _currentSFXList.RemoveAt(index);
                UpdatePriority();

                return;
            }
        }
    }

    public static void StopSFX(int soundIndex, float fadeTime)
    {
        for (int index = 0; index < _currentSFXList.Count; ++index)
        {
            if (_currentSFXList[index].SoundIndex == soundIndex)
            {
                SoundPoolItem sfxItem = _currentSFXList[index];
                _currentSFXList.RemoveAt(index);

                sfxItem.Stop(fadeTime, delegate ()
                {
                    _currentSFXList.Remove(sfxItem);
                    UpdatePriority();
                });
                return;
            }
        }
    }

    public static void PlayEventBGM(float fadeTime)
    {
        GameEventDB gameEventDb = DataManager.Instance.GetGameEventDB();
        GameEventThemeInfo eventThemeInfo = gameEventDb.GetCurrentTheme();
        if (eventThemeInfo != null)
        {
            SoundManager.PlayBGM(eventThemeInfo.Theme, fadeTime);
        }
    }

    public static void PlayBGM(BGM bgm, float fadeTime)
    {
        /*
        int maxTrack = 2;
        string bgmName;
        if (bgm == BGM.Battle)
        {
            int index = Random.Range(1, maxTrack + 1); // random index track
            bgmName = string.Format("{0}_{1}", bgm, index);
        }
        else
        {
            bgmName = bgm.ToString();
        }

        PlayBGM(bgmName, fadeTime);
        */

        PlayBGM(bgm.ToString(), fadeTime);
    }

    public static void PlayBGM(string bgmName, float fadeTime)
    {
        AudioClip clip = ResourceManager.Load<AudioClip>(_rootBGMPath + bgmName);
        if (clip != null)
        {
            PlayBGM(bgmName, clip, fadeTime);
        }
    }

    public static void PlayBGM(string key, AudioClip clip, float fadeTime)
    {
        if (_currentBGM != null && _currentBGM.Key == key)
        {
            // Already play this BGM.
            return;
        }

        if (_currentBGMList == null)
        {
            _currentBGMList = new List<SoundPoolItem>();
        }

        if (_currentBGM != null)
        {
            StopBGM(fadeTime);
        }

        _currentBGM = SoundPool.Instance.CreateItem();
        _currentBGM.gameObject.name = "BGM_" + key;
        MonoBehaviour.DontDestroyOnLoad(_currentBGM.gameObject);

        int soundIndex = RequestSoundIndex();
        _currentBGM.SetSoundIndex(soundIndex);
        _currentBGM.SetKey(key);
        _currentBGM.SetType(SoundType.BGM);
        _currentBGM.SetLoop(true);
        _currentBGM.SetVolume(0.0f, 0.0f);
        if (_isBGMOn)
        {
            _currentBGM.SetVolume(_defaultBGMVolume, fadeTime);
        }

        _currentBGMList.Insert(0, _currentBGM);
        UpdatePriority();

        _currentBGM.Play(clip, null);
    }

    private static void RemoveBGM(int soundIndex)
    {
        for (int index = 0; index < _currentBGMList.Count; ++index)
        {
            if (_currentBGMList[index].SoundIndex == soundIndex)
            {
                SoundPoolItem soundItem = _currentBGMList[index];
                _currentBGMList.RemoveAt(index);
                UpdatePriority();

                return;
            }
        }
    }

    public static void StopBGM(float fadeTime)
    {
        if (_currentBGM != null)
        {
            SoundPoolItem bgmItem = _currentBGM;
            bgmItem.Stop(
                  fadeTime
                , delegate ()
                {
                    RemoveBGM(bgmItem.SoundIndex);
                    UpdatePriority();
                }
            );
        }
    }

    public static void PlayHeroVoice(SoundHeroData data, UnityAction onComplete = null)
    {
        if (data.VoiceKey.Length > 0)
        {
            PlayVoice(_heroVoiceFolder + data.VoiceKey, onComplete);
        }
    }

    public static int PlayMinionSummonVoice(string cardID, UnityAction onComplete = null)
    {
        string key = _minionVoiceFolder + string.Format("{0}_{1}", _minionSummonPrefix, cardID);
        return PlayVoice(key, onComplete);
    }

    public static int PlayMinionDeathVoice(string cardID, UnityAction onComplete = null)
    {
        string key = _minionVoiceFolder + string.Format("{0}_{1}", _minionDeathPrefix, cardID);
        return PlayVoice(key, onComplete);
    }

    public static float GetTutorialVoiceDuration(string key)
    {
        string filePath = _TutorialFolder + key;
        AudioClip clip = ResourceManager.Load<AudioClip>(_rootVoicePath + filePath);
        if (clip != null)
        {
            return clip.length;
        }

        return 0.0f;
    }

    public static int PlayVoice(string key, GameMode mode, UnityAction onComplete)
    {
        string filePath = "";
        switch (mode)
        {
            case GameMode.Tutorial_1:
            case GameMode.Tutorial_2:
            case GameMode.Tutorial_3:
            case GameMode.Tutorial_4:
                filePath += _TutorialFolder + key;
                break;
            case GameMode.Adventure:
                filePath += _AdventureFolder + key;
                break;
        }
        return PlayVoice(filePath, onComplete);
    }

    public static int PlayVoice(string key, UnityAction onComplete = null)
    {
        if (_currentVoiceList == null)
        {
            _currentVoiceList = new List<SoundPoolItem>();
        }

        Debug.Log("Try PlayVoice : " + key);
        AudioClip clip = ResourceManager.Load<AudioClip>(_rootVoicePath + key);
        if (clip != null)
        {
            SoundPoolItem soundPoolItem = SoundPool.Instance.CreateItem();
            soundPoolItem.gameObject.name = "VOICE_" + key;
            MonoBehaviour.DontDestroyOnLoad(soundPoolItem.gameObject);

            int soundIndex = RequestSoundIndex();
            soundPoolItem.SetSoundIndex(soundIndex);
            soundPoolItem.SetKey(key);
            soundPoolItem.SetLoop(false);
            soundPoolItem.SetType(SoundType.Voice);
            if (_isVoiceOn)
            {
                soundPoolItem.SetVolume(_defaultVoiceVolume);
            }
            else
            {
                soundPoolItem.SetVolume(0.0f);
            }

            _currentVoiceList.Insert(0, soundPoolItem);
            UpdatePriority();

            soundPoolItem.Play(clip, delegate ()
            {
                RemoveVoice(soundPoolItem.SoundIndex);
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
            });

            return soundIndex;
        }
        else
        {
            Debug.Log("PlayVoice: not found voice. " + key);
            if (onComplete != null)
            {
                onComplete.Invoke();
            }
        }

        return -1;
    }

    private static void RemoveVoice(int soundIndex)
    {
        for (int index = 0; index < _currentVoiceList.Count; ++index)
        {
            if (_currentVoiceList[index].SoundIndex == soundIndex)
            {
                SoundPoolItem soundItem = _currentVoiceList[index];
                _currentVoiceList.RemoveAt(index);
                UpdatePriority();

                return;
            }
        }
    }

    public static void StopVoice(int soundIndex, float fadeTime)
    {
        for (int index = 0; index < _currentVoiceList.Count; ++index)
        {
            if (_currentVoiceList[index].SoundIndex == soundIndex)
            {
                SoundPoolItem voiceItem = _currentVoiceList[index];
                _currentVoiceList.RemoveAt(index);

                voiceItem.Stop(
                      fadeTime
                    , delegate ()
                    {
                        _currentVoiceList.Remove(voiceItem);
                        UpdatePriority();
                    }
                );

                return;
            }
        }
    }

    public static void SetBGMVolume(float volume)
    {
        _defaultBGMVolume = volume;

        if (_currentBGM != null)
        {
            if (_isBGMOn)
            {
                _currentBGM.SetVolume(_defaultBGMVolume);
            }
            else
            {
                _currentBGM.SetVolume(0.0f);
            }
        }
    }

    public static void SetSFXVolume(float volume)
    {
        _defaultSFXVolume = volume;

        SoundPoolItem[] sounds = GameObject.FindObjectsOfType<SoundPoolItem>();
        foreach (SoundPoolItem s in sounds)
        {
            if (s.Type == SoundType.SFX)
            {
                if (_isSFXOn)
                {
                    s.SetVolume(_defaultSFXVolume);
                }
                else
                {
                    s.SetVolume(0.0f);
                }
            }
        }
    }

    public static void SetVoiceVolume(float volume)
    {
        _defaultVoiceVolume = volume;

        SoundPoolItem[] sounds = GameObject.FindObjectsOfType<SoundPoolItem>();
        foreach (SoundPoolItem s in sounds)
        {
            if (s.Type == SoundType.Voice)
            {
                if (_isVoiceOn)
                {
                    s.SetVolume(_defaultVoiceVolume);
                }
                else
                {
                    s.SetVolume(0.0f);
                }
            }
        }
    }

    public static void SetBGMOn(bool isOn)
    {
        _isBGMOn = isOn;

        SetBGMVolume(_defaultBGMVolume);
    }

    public static void SetSFXOn(bool isOn)
    {
        _isSFXOn = isOn;

        SetSFXVolume(_defaultSFXVolume);
    }

    public static void SetVoiceOn(bool isOn)
    {
        _isVoiceOn = isOn;

        SetVoiceVolume(_defaultVoiceVolume);
    }

    private static void UpdatePriority()
    {
        _index = 0;

        // BGM
        if (_currentBGMList != null && _currentBGMList.Count > 0)
        {
            for (int i = 0; i < _currentBGMList.Count; ++i)
            {
                if (_index < 255)
                {
                    _currentBGMList[i].SetPriority(_index++);
                }
                else
                {
                    _currentBGMList[i].SetPriority(_index);
                }
            }
        }

        // Voice
        if (_currentVoiceList != null && _currentVoiceList.Count > 0)
        {
            for (int i = 0; i < _currentVoiceList.Count; ++i)
            {
                if (_index < 255)
                {
                    _currentVoiceList[i].SetPriority(_index++);
                }
                else
                {
                    _currentVoiceList[i].SetPriority(_index);
                }
            }
        }

        // SFX
        if (_currentSFXList != null && _currentSFXList.Count > 0)
        {
            for (int i = 0; i < _currentSFXList.Count; ++i)
            {
                if (_index < 255)
                {
                    _currentSFXList[i].SetPriority(_index++);
                }
                else
                {
                    _currentSFXList[i].SetPriority(_index);
                }
            }
        }
    }

    public static void StopAll()
    {
        if (_currentBGMList != null)
        {
            // BGM
            for (int i = 0; i < _currentBGMList.Count; ++i)
            {
                _currentBGMList[i].Stop();
            }
            _currentBGMList.Clear();
        }

        if (_currentVoiceList != null)
        {
            // Voice
            for (int i = 0; i < _currentVoiceList.Count; ++i)
            {
                _currentVoiceList[i].Stop();
            }
            _currentVoiceList.Clear();
        }

        if (_currentSFXList != null)
        {
            // SFX
            for (int i = 0; i < _currentSFXList.Count; ++i)
            {
                _currentSFXList[i].Stop();
            }
            _currentSFXList.Clear();
        }
    }

    public static void ClearAll()
    {
        StopAll();

        SoundPool.Instance.Clear();
    }

    private static int RequestSoundIndex()
    {
        if (_sfxIndex <= 0)
        {
            _sfxIndex = 1; // index of 0 is reserved for BGM.
        }

        return _sfxIndex++;
    }
    #endregion
}
