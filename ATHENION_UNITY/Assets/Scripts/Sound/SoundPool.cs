﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundPool : PrefabPool<SoundPool, SoundPoolItem>
{
    public override void Init()
    {
        PrefabPath = "Prefabs/Pool/SoundPrefab";
        
        base.Init();
    }
}
