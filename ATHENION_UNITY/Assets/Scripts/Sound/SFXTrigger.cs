﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXTrigger : MonoBehaviour {

    public SoundManager.SFX SFX;

	// Use this for initialization
	void Start () {
        SoundManager.PlaySFX(SFX);
	}	

}
