using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using Photon.Pun;
using Photon.Realtime;
using Karamucho.UI;
using System;
using Firebase.Analytics;

public class GameManager : MonoSingleton<GameManager>
{
    #region Event
    public delegate void MatchingManagerHandler(GameMode mode);
    public static event MatchingManagerHandler OnPlayGame;
    public static event Action<GameMode, bool, EndGameType> OnEndGameEvent;
    public static event Action<string, HeroVoice> OnPlayerSendEmote;

    //public static event Action OnSafeToLeave;
    public static event Action<string, PlayerIndex, BattleData, string> OnImpossibleError;
    public static event Action OnUpdatePassingTime;

    public static event Action OnDestroy;
    public static event Action<BattleCardData> OnUseCard;
    public static event Action<MinionData, int> OnMinionMoveToSlot;
    #endregion

    #region Enums
    public enum EndGameType
    {
        Disconnect = 0
        , Normal
        , OtherLeftRoom
        , Surrender
        , OtherSurrender

        , SafeToLeave
        , PlayTimeHack
    }

    public enum DrawType
    {
        DrawByAP
        , DrawFullHand
        , DrawOne
    }
    #endregion

    #region Public Properties
    public readonly int BoardWidth = 4;
    public readonly int BoardHeight = 4;
    public int BoardSize { get { return BoardHeight * BoardWidth; } }

    public readonly int MaxHandCard = 5;
    public readonly int AFKLose = 3;
    public readonly int MinSubTurnStatCount = 7;
    public readonly int SkipTurnDiscard = 1;

    //public readonly int WinPVPExp = 100;
    //public readonly int WinChallengeExp = 50;
    //public readonly int WinArenaExp = 50;
    //public readonly int LoseANYExp = 0;

    public readonly bool IsAFKPernalty = false;
    public readonly bool IsAutoEndTurn = false;
    public readonly bool IsCanUseHeroSkill = false;

    public static readonly float ReConnectTimeout = 60.0f;
    public static readonly float WaitReConnectTimeout = 60.0f;
    private static readonly int _reconnectCountLimit = 10;
    private static readonly float _disconnectTimeLimit = 15.0f;

    public static readonly float PokeRate = 1.5f;
    public static readonly float MaybeDisconnectDelay = (PokeRate * 2.0f);
    public static readonly float MaybeOpponentDisconnectDelay = (MaybeDisconnectDelay + (PokeRate * 2.0f));

    public static readonly float SafeToLeaveTime = (60.0f * 5.5f);
    public static readonly float AllowServerTimeDelay = 8.0f;

    public readonly int Lifepoint = 30;

    public int ActionPoint = 3;
    public int Inithand = 4;
    public float TurnTime = 70.0f;
    public float EmptyHandTurnTime = 5.0f;
    public float AFKTime_1 = 70.0f;
    public float AFKTime_2 = 30.0f;
    public float AFKTime = 10.0f;
    public float ActionTime = 21.0f;
    public float CountdownTime = 20.0f;
    public float SelectDecisionTime = 15.0f;

    public bool IsShuffleDeck = true;

    public GameMode Mode { get { return MatchData.GameMode; } }

    public GamePhase Phase = GamePhase.Init;

    public PlayerIndex CurrentPlayerIndex { get { return _currentPlayer; } }
    private PlayerIndex _currentPlayer;

    public List<PlayerIndex> PlayerIndexList
    {
        get
        {
            if (_playerIndexList == null)
            {
                _playerIndexList = GameHelper.GetPlayerIndexList(GetPlayerCount());
            }

            return _playerIndexList;
        }
    }
    private List<PlayerIndex> _playerIndexList = null;

    public BattleData Data { get { return _data; } }
    private BattleData _data;

    public List<MinionData> AttackerList { get { return _attackerList; } }
    public List<MinionData> _attackerList = new List<MinionData>();

    public int Turn { get; set; }
    public int SubTurn { get; set; }

    public int LocalTurn { get { return _localTurn; } }
    private int _localTurn = 0;

    public bool IsConfirmRehand
    {
        get
        {
            if (_isConfirmReHand != null && _isConfirmReHand.Length > 0)
            {
                foreach (bool isConfirm in _isConfirmReHand)
                {
                    if (!isConfirm) return false;
                }

                return true;
            }

            return false;
        }
    }

    public bool IsCanNextPhase
    {
        get
        {
            if (Phase == GamePhase.Play)
            {
                if (_useCardCommandList != null && _useCardCommandList.Count > 0)
                {
                    return false;
                }
            }
            return (CommandQueue.Instance.IsReady && UIManager.Instance.GetUIQueueIsReady() && !IsEndGame);
        }
    }

    public MatchData MatchData
    {
        get
        {
            if (_matchData == null)
            {
                MatchDataContainer matchDataContainer = GameObject.FindObjectOfType<MatchDataContainer>();
                if (matchDataContainer != null)
                {
                    _matchData = matchDataContainer.MatchData;
                    Destroy(matchDataContainer.gameObject);
                }
            }
            return _matchData;
        }
    }
    private MatchData _matchData;

    // battle log
    public BattleLog BattleLog { get { return _battleLog; } }
    private BattleLog _battleLog = null;

    public Bot Bot { get => _bot; private set => _bot = value; }
    private Bot _bot;

    public bool IsTimeout { get { return ((_timer <= 0.0f) && IsTimer); } }
    public float RemainingTimer { get { return _timer; } }
    public bool IsTimer { get { return (_isTimer && !_isDisableTimer); } }
    public bool IsPlayerActive { get { return (_activeCount > 0); } }

    public bool IsEndTurn { get { return _isEndTurn; } }
    public bool _isEndTurn = false;

    public bool IsCallEndTurn { get; set; }

    public bool IsEndGame { get { return _isEndGame; } }
    private bool _isEndGame = false;

    public bool IsDisconnect { get { return (IsNetworkPlay() && _isDisconnect); } }

    public bool IsMessageMute { get { return _isMessageMute; } }
    private bool _isMessageMute = false;

    public int PlayCardCount { get { return _playCardCount; } }
    private int _playCardCount = 0;

    public bool IsStartWithEmptyHand { get { return _isStartWithEmptyHand; } }
    public bool _isStartWithEmptyHand = false;

    public EndGameType EndType { get { return _endGameType; } }
    private EndGameType _endGameType = EndGameType.Normal;

    public List<CommandData> ActionAbilityCommandList { get { return _actionAbilityCommandList; } }
    #endregion

    #region Private Properties
    public GameManagerController Controller { get; private set; }

    private bool _isSetupGameMode = false;
    private bool _isCacheLocalPlayerIndex = false;
    private PlayerIndex _localPlayerIndex;
    public int[] _afkCount = null;
    private DebugManager _debugManager;
    private WaitObject _timeoutCounter = null;

    private int _reconnectLimit = 0;
    private bool _isStartReconnect = false;
    public bool _isReconnectTimeout = false;
    private bool _isSyncRPC = false;
    private int _latestReceivedRequestIndex = -1;
    private bool _isRejectCmd = false;
    private Coroutine _tryReconnectCoroutine = null;
    private Coroutine _requestResendRPCCoroutine = null;

    // DEBUG CARD
    private bool _isDebugCard = false;
    //private bool _isDebugCard = true;

    // turn 
    public bool IsTurnTimeout { get; set; }

    // timer
    public DateTime _startPlayServerTime;
    public DateTime _endPlayServerTime;

    public float _startPlayStamp;
    public float _endPlayStamp;

    public DateTime _startTurnStamp;
    public float _timer = 0.0f;
    public float _turnTimer = 0.0f;
    public float _playTime = 0.0f;
    private float _safeToLeaveTimer = 0.0f;
    public bool _isTimer = false;
    public bool _isDisableTimer = false;

    private int _randomSeed = 0;
    private int _activeCount = 0;
    private bool _isGotEndGame = false;
    private bool _isYouWin = false;
    public bool _isMePlayFirst = false;
    public int _playerDrawCount = 0;
    private bool _isResign = false;
    private bool _isDisconnect = false;
    private bool _isCompleteIntro = false;
    public bool _isNewTurn = true;
    public bool _isTimeoutCallEndTurn = false;

    private int _startRank = 0;
    private PlayerLevelData _startLevelData;

    // battle phase
    public int _attackerListCount = 0;

    // setup data
    public bool _isPlayFirstReady;

    // Phase Ready
    private int[,] _phaseReadyIndex;

    // re-hand   
    public bool[] _isConfirmReHand;
    private UnityAction<List<int>> _onMinionSelected = null;
    private WaitObject _rehandTimer = null;

    // select minion
    private Requester _selectedMinionRequester;
    private UnityAction<List<int>> _onSelectedMinionCompelete;
    private WaitObject _selectMinionTimer = null;

    // select slot
    private Requester _selectedSlotRequester;
    private UnityAction<List<int>> _onSelectedSlotCompelete;
    private WaitObject _selectSlotTimer = null;

    // select card
    private Requester _selectedCardRequester;
    private UnityAction<List<int>> _onSelectedCardCompelete;
    private WaitObject _selectCardTimer = null;

    // Use card command
    private List<CommandData> _actionAbilityCommandList;

    // action ability command
    private Queue<CommandData> _useCardCommandList;

    // battle save key
    private List<string> _battleSaveKeyList;

    private StringBuilder _sb;

    // Bot Event
    private event Action<PlayerIndex, int> _onShowSpiritAttackOnPlayerComplete;
    private event Action<int> _onPlayerPlayHandCardComplete;

    // Poke
    private float _pokeTimer = 0.0f;
    public bool _isStartPoke = false;
    private DateTime[] _pokeStamp;
    #endregion

    #region Awake
    protected override void Awake()
    {
        // Init Controller
        Controller = CreateController(Mode);
        DataManager.Instance.SetLastGameMode(Mode);

        InitPhaseReady();
        //PlayerPrefs.DeleteAll();

        _isPlayFirstReady = false;
        _isCacheLocalPlayerIndex = false;

        PhotonEventCapture capture = GetComponent<PhotonEventCapture>();
        if (capture != null)
        {
            capture.SubscribeConnectedEvent(this.OnConnect);
            capture.SubscribeConnectedToMasterEvent(this.OnConnectToMaster);
            capture.SubscribeDisconnectedEvent(this.OnDisconnect);
            capture.SubscribeLeftRoomEvent(this.OnLeftRoom);
            capture.SubscribePlayerLeftRoomEvent(this.OnPlayerLeftRoom);
            capture.SubscribeJoinedRoomEvent(this.OnJoinedRoom);
            capture.SubscribePlayerEnteredRoomEvent(this.OnPlayerEnteredRoom);
            capture.SubscribeJoinRoomFailedEvent(this.OnJoinRoomFailed);
            capture.SubscribePlayerPropertiesUpdateEvent(this.OnPlayerPropertiesUpdate);
        }

        SetShowDebugLog(DataManager.Instance.GetShowDebugLog());
        ClearAllSaveKey();

        ShowFullLoad(true);

        base.Awake();
    }
    #endregion

    #region Update
    private void Update()
    {
        /*
        if(IsNetworkPlay() && _isDisconnect)
        {
            return;
        }
        */

        UpdateTimer();

        if (!IsEndGame)
        {
            if (_isStartPoke)
            {
                _pokeTimer -= Time.deltaTime;

                if (_pokeTimer <= 0.0f)
                {
                    _pokeTimer += PokeRate;
                    PhotonRequestPoke();

                    OnCheckPoke();
                }
            }

            switch (Phase)
            {
                case GamePhase.Init: Controller.OnInitPhase(); break;
                case GamePhase.SetupData: Controller.OnSetupDataPhase(); break;
                case GamePhase.Intro: Controller.OnIntroPhase(); break;
                case GamePhase.ReHand: Controller.OnReHandPhase(); break;
                case GamePhase.Begin: Controller.OnBeginPhase(); break;
                case GamePhase.Spirit: Controller.OnSpiritPhase(); break;
                case GamePhase.Draw: Controller.OnDrawPhase(); break;
                case GamePhase.PostDraw: Controller.OnPostDrawPhase(); break;
                case GamePhase.PrePlay: Controller.OnPrePlayPhase(); break;
                case GamePhase.Play: Controller.OnPlayPhase(); break;
                case GamePhase.PostPlay: Controller.OnPostPlayPhase(); break;
                case GamePhase.PreBattle: Controller.OnPreBattlePhase(); break;
                case GamePhase.Battle: Controller.OnBattlePhase(); break;
                case GamePhase.PostBattle: Controller.OnPostBattlePhase(); break;
                case GamePhase.End: Controller.OnEndPhase(); break;
            }
        }
    }

    private void UpdateTimer()
    {
        _playTime += Time.deltaTime;
        _safeToLeaveTimer += Time.deltaTime;

        if (IsTimer)
        {
            _turnTimer += Time.deltaTime;

            if (
                IsHaveTimer()
                && !IsEndGame
                && (!IsNetworkPlay() || !IsDisconnect)
            )
            {
                /*
                if (_isDisconnect && !IsLocalPlayer(CurrentPlayerIndex))
                {
                    // If not my turn and opponent already left.
                    _timer = 0.0f; // Force timeout.
                }
                else
                */
                {
                    _timer -= Time.deltaTime;
                }

                UIManager.Instance.TimerUpdate(
                      _timer
                    , IsLocalPlayer(CurrentPlayerIndex) ? Karamucho.UI.CardUIManager.PlayerPosition.Bottom : Karamucho.UI.CardUIManager.PlayerPosition.Top
                );
            }
        }

        if (!IsEndGame)
        {
            if (IsHaveTimer())
            {
                if (_safeToLeaveTimer >= SafeToLeaveTime && _endGameType != EndGameType.SafeToLeave)
                {
                    OnTriggerSafeToLeave();
                }
            }
        }
    }
    #endregion

    #region Methods
    public int GetReconnectCountLimit()
    {
        object value = null;
        if (DataManager.Instance.GetGameValue("ReconnectCountLimit", out value))
        {
            return int.Parse(value.ToString());
        }
        else
        {
            return _reconnectCountLimit;
        }
    }

    public float GetDisconnectTimeLimit()
    {
        object value = null;
        if (DataManager.Instance.GetGameValue("DisconnectTimeLimit", out value))
        {
            return float.Parse(value.ToString());
        }
        else
        {
            return _disconnectTimeLimit;
        }
    }

    public void SetLoadingProgress(float ratio)
    {
        PopupUIManager.Instance.SetLoadingProgress(ratio);
    }

    public void ShowFullLoad(bool isShow, string loadingText = "")
    {
        PopupUIManager.Instance.Show_FullLoad(isShow, loadingText);
    }

    public string GetBoardID()
    {
        return Controller.GetBoardID();
    }

    public Bot CreateBot(PlayerIndex playerIndex, bool hasDecisionTime, BotWeight weight, bool isEnable)
    {
        GameObject obj = new GameObject();
        AIBot bot = obj.AddComponent<AIBot>();
        if (bot != null)
        {
            bot.SetPlayerIndex(playerIndex);
            bot.SetHaveDecisionTime(hasDecisionTime);
            bot.SetWeight(weight);
            bot.SetEnable(isEnable);
        }
        Bot = bot;
        return Bot;
    }

    public BoardManager GetBoardManager()
    {
        return BoardManager.Instance;
    }

    private GameManagerController CreateController(GameMode mode)
    {
        switch (mode)
        {
            case GameMode.Tutorial_1: return new Tutorial1Controller(this);
            case GameMode.Tutorial_2: return new Tutorial2Controller(this);
            case GameMode.Tutorial_3: return new Tutorial3Controller(this);
            case GameMode.Tutorial_4: return new Tutorial4Controller(this);
            case GameMode.Adventure:
                {
                    Type t = Type.GetType(MatchData.Property.AdventureStageData.AdventureStageID + "AdventureController"); //AT_001AdventureController
                    return (AdventureController)Activator.CreateInstance(t, new object[] { this });
                }
            case GameMode.Event: return new EventBattleController(this);
            default: return new DefaultController(this);
        }
    }

    public void GetServerUTCTime(UnityAction<DateTime> onComplete)
    {
        PopupUIManager.Instance.Show_SoftLoad(true);

        DataManager.Instance.GetRealServerTime(
           delegate (DateTime serverUTCTime)
           {
               if (onComplete != null)
               {
                   onComplete.Invoke(serverUTCTime);
               }

               PopupUIManager.Instance.Show_SoftLoad(false);
           }
           , delegate (string error)
           {
               Debug.LogError(error.ToString());

               // Retry
               GetServerUTCTime(onComplete);
           }
       );
    }

    public void SetupData()
    {
        SetLoadingProgress(0.5f);

        _isEndGame = false;
        _data = new BattleData();
        //_data.PlayerDataList = new List<BattlePlayerData>();

        CommandQueue.Instance.Init();
        UIManager.Instance.Init();
        UIManager.Instance.InitUIQueue();

        BattleCardData.ResetUniqueID();
        CardAbilityData.ResetIndex();
        EffectData.ResetIndex();
        MinionBuffData.ResetIndex();
        SlotBuffData.ResetIndex();

        CardVFXPool.Instance.Clear();
        PopupPool.Instance.Clear();
        UtilityPool.Instance.Clear();

        // Pre init pool
        SoundPool.Instance.Init();
        CardVFXPool.Instance.Init();
        PopupPool.Instance.Init();
        UtilityPool.Instance.Init();

        CommandQueue.Instance.OnActiveCompleteEvent += this.OnCommmandActiveComplete;
        UIQueue.Instance.OnActiveCompleteEvent += this.OnCommmandActiveComplete;

        _startRank = DataManager.Instance.PlayerInfo.GetPlayerCurrentRank();

        if (GetPlayerCount() == 2) // 2 players
        {
            // Init Flag
            _isConfirmReHand = new bool[] { false, false };

            // Setup Random Seed
            {
                SetRandomSeed(MatchData.PlayerData[0].RandomSeed);
            }

            // Setup slot/board
            {
                //_data.SlotDatas = new SlotData[BoardHeight * BoardWidth];

                int slotIndex = 0;
                for (int row = 0; row < BoardHeight; ++row)
                {
                    for (int column = 0; column < BoardWidth; ++column)
                    {
                        _data.SlotDatas[slotIndex] = new SlotData(slotIndex);
                        ++slotIndex;
                    }
                }
            }

            // Setup players
            {
                int encodeKey = 0;

                if (IsNetworkPlay() && !IsVSBot())
                {
                    char c1 = MatchData.PlayerData[0].PlayerInfo.PlayerID[0];
                    char c2 = MatchData.PlayerData[1].PlayerInfo.PlayerID[1];
                    char c3 = MatchData.PlayerData[0].PlayerInfo.PlayerID[2];
                    char c4 = MatchData.PlayerData[1].PlayerInfo.PlayerID[3];
                    char c5 = MatchData.PlayerData[0].PlayerInfo.PlayerID[4];
                    char c6 = MatchData.PlayerData[1].PlayerInfo.PlayerID[5];

                    string hex = string.Format("{0}{1}{2}{3}{4}{5}", c1, c2, c3, c4, c5, c6);
                    encodeKey = int.Parse(hex, System.Globalization.NumberStyles.HexNumber);
                }
                else
                {
                    System.Random r = new System.Random();
                    encodeKey = r.Next(0, 0xFFFFFF);
                }

#if UNITY_EDITOR
                Debug.LogFormat("encodeKey: {0}", Convert.ToString(encodeKey, 16));
#endif

                _playerIndexList = GameHelper.GetPlayerIndexList(GetPlayerCount());
                _afkCount = new int[PlayerIndexList.Count];

                // Setting Player Hero
                foreach (PlayerIndex playerIndex in PlayerIndexList) // two players
                {
                    MatchPlayerData matchPlayerData = MatchData.PlayerData[(int)playerIndex];

                    // Init data
                    HeroData heroData = HeroData.CreateHero(matchPlayerData.Deck.HeroID, playerIndex);
                    HeroAttackData heroAttack = HeroAttackData.CreateHeroAttackData(heroData.ActivePowerEffectKey, IsLocalPlayer(playerIndex));
                    if (IsLocalPlayer(playerIndex))
                    {
                        UIEffectManager.Instance.HeroAttack_Bot.Setup(heroAttack);
                    }
                    else
                    {
                        UIEffectManager.Instance.HeroAttack_Top.Setup(heroAttack);
                    }

                    int lifepoint = Controller.GetLifePoint(playerIndex, Mode);

                    BattlePlayerData battlePlayerData = new BattlePlayerData(
                          playerIndex
                        , lifepoint
                        , ActionPoint
                        , matchPlayerData.PlayerInfo
                        , matchPlayerData.Deck
                        , encodeKey
                    );

                    // Init Data
                    {
                        battlePlayerData.CardList = new Dictionary<CardZone, List<BattleCardData>>();
                        battlePlayerData.CardList.Add(CardZone.Deck, new List<BattleCardData>());
                        battlePlayerData.CardList.Add(CardZone.Hand, new List<BattleCardData>());
                        battlePlayerData.CardList.Add(CardZone.Battlefield, new List<BattleCardData>());
                        battlePlayerData.CardList.Add(CardZone.Graveyard, new List<BattleCardData>());
                        battlePlayerData.CardList.Add(CardZone.Remove, new List<BattleCardData>());
                        battlePlayerData.CardList.Add(CardZone.Hero, new List<BattleCardData>());
                        battlePlayerData.SetImmuneCounter(0);
                    }

                    // Setup card deck.
                    {
                        bool isSuffleDeck = IsShuffleDeck;

                        if (EventBattleDB.Instance.IsVSBot)
                        {
                            // ใน Event Battle - Pre-GrandMaster
                            // 1. บอทล็อคมือ
                            // 2. บอทเริ่มก่อนเสมอ
                            // 3. บอท Hard/Defensive
                            if (Mode == GameMode.Event)
                            {
                                if (playerIndex == PlayerIndex.Two)
                                {
                                    isSuffleDeck = false;
                                }
                            }
                        }

                        List<BattleCardData> deck = new List<BattleCardData>();
                        if (isSuffleDeck)
                        {
                            foreach (KeyValuePair<string, int> item in matchPlayerData.Deck.Deck)
                            {
                                for (int num = 0; num < item.Value; ++num)
                                {
                                    BattleCardData battleCard = BattleCardData.CreateBattleCard(item.Key, matchPlayerData.Deck.CardBackID);
                                    if (battleCard != null)
                                    {
                                        battleCard.SetOwner(battlePlayerData.PlayerIndex);
                                        battleCard.SetCurrentZone(CardZone.Deck);
                                        battleCard.InitAbility();
                                        deck.Add(battleCard);
                                    }
                                }
                            }
                        }
                        else
                        {
                            foreach (string cardID in matchPlayerData.Deck.Deck.GetOrderList())
                            {
                                BattleCardData battleCard = BattleCardData.CreateBattleCard(cardID, matchPlayerData.Deck.CardBackID);
                                if (battleCard != null)
                                {
                                    battleCard.SetOwner(battlePlayerData.PlayerIndex);
                                    battleCard.SetCurrentZone(CardZone.Deck);
                                    battleCard.InitAbility();
                                    deck.Add(battleCard);
                                }
                            }
                        }
                        battlePlayerData.CardList[CardZone.Deck] = new List<BattleCardData>(deck);

                        _data.PlayerDataList.Add(battlePlayerData);
                        _afkCount[(int)playerIndex] = 0;

                        // Suffle Deck
                        if (isSuffleDeck)
                        {
                            _data.PlayerDataList[(int)playerIndex].ShuffleDeck(false);
                        }
                    }

                    UIManager.Instance.InitUIPlayerInfo(
                          playerIndex
                        , battlePlayerData.PlayerInfo.DisplayName
                        , battlePlayerData.PlayerInfo.AvatarID
                        , battlePlayerData.Hero.ID
                        , 0
                        , battlePlayerData.CurrentHP
                        , battlePlayerData.DeckCount
                        , battlePlayerData.CardList[CardZone.Graveyard].Count
                    );
                }

                // Setting Playboard
                UIManager.Instance.SetupPlayboard(GetBoardID());
            }

            // Setup bot
            {
                SetupBot();
            }
        }

        if (IsVSPlayer())
        {
            BattlePlayerData opponent = Data.PlayerDataList.Find(data => data.PlayerInfo.PlayerID != DataManager.Instance.PlayerInfo.PlayerID);
            if (opponent != null)
            {
                DataManager.Instance.UpdateLastMatchPlayer(opponent.PlayerInfo, null, null);
            }
        }

        // On StartMatch()
        AnalyticTriggerStartMatch();

        OnPlayGame?.Invoke(Mode);

        _reconnectLimit = GetReconnectCountLimit();
        _isSetupGameMode = true;
        if (IsNetworkPlay())
        {
            PhotonRequestPhaseReady(GamePhase.SetupData, GetLocalPlayerIndex(), SubTurn);
        }
    }

    protected virtual void SetupBot()
    {
        if (IsVSBot())
        {
            foreach (PlayerIndex playerIndex in PlayerIndexList)
            {
                if (!IsLocalPlayer(playerIndex) || Mode == GameMode.BotSimulate)
                {
                    switch (GetBotType())
                    {
                        case BotType.TutorialBot:
                        case BotType.AdventureBot:
                            {
                                ((StoryGameController)Controller).SetupBot(playerIndex, this.Mode);
                            }
                            break;

                        case BotType.SimpleBot:
                            {
                                GameObject obj = new GameObject();
                                Bot bot = obj.AddComponent<Bot>();
                                if (bot != null)
                                {
                                    Debug.LogFormat("Create SimpleBot for {0}", playerIndex);
                                    bot.SetPlayerIndex(playerIndex);

                                    this._onShowSpiritAttackOnPlayerComplete += bot.OnSpiritAttackOnPlayer;
                                    this._onPlayerPlayHandCardComplete += bot.OnPlayHandCard;

                                    bot.SetEnable(true);
                                    Bot = bot;
                                }
                            }
                            break;

                        //default:
                        case BotType.AIBot:
                            {
                                GameObject obj = new GameObject();
                                AIBot bot = obj.AddComponent<AIBot>();
                                if (bot != null)
                                {
                                    Debug.LogFormat("Create AIBot for {0} : {1}", playerIndex, GetBotLevel());

                                    bot.SetPlayerIndex(playerIndex);
                                    bot.SetHaveDecisionTime(IsHaveTimer());
                                    bot.SetWeight(AIBot.GetBotWeight(
                                            GetBotLevel()
                                        , Data.PlayerDataList[(int)playerIndex].Hero.ElementType
                                    ));

                                    this._onShowSpiritAttackOnPlayerComplete += bot.OnSpiritAttackOnPlayer;
                                    this._onPlayerPlayHandCardComplete += bot.OnPlayHandCard;

                                    bot.SetEnable(true);
                                    Bot = bot;
                                }
                            }
                            break;
                    }
                }
            }
        }
    }

    public void InitDraw()
    {
        foreach (PlayerIndex playerIndex in PlayerIndexList)
        {
            RequestDrawCard(playerIndex, Inithand);
        }
    }

    public void ReHand()
    {
        PlayerIndex localPlayerIndex = GetLocalPlayerIndex();

        // Show UI and select and send selected card list to OnSelectReHand.
        _isConfirmReHand[(int)localPlayerIndex] = false;
        List<CardUIData> cardUIDatas = new List<CardUIData>();
        foreach (BattleCardData item in Data.PlayerDataList[(int)localPlayerIndex].CardList[CardZone.Hand])
        {
            CardUIData data = new CardUIData(item);
            cardUIDatas.Add(data);
        }
        string titleRehand = LocalizationManager.Instance.GetText("BATTLE_REHAND_START");
        if (_isMePlayFirst)
        {
            string firstText = LocalizationManager.Instance.GetText("BATTLE_REHAND_FIRST");
            UIManager.Instance.ShowRedraw(titleRehand, firstText, true, cardUIDatas, 0, OnSelectReHand);
        }
        else
        {
            string secondText = LocalizationManager.Instance.GetText("BATTLE_REHAND_SECOND");
            UIManager.Instance.ShowRedraw(titleRehand, secondText, false, cardUIDatas, 0, OnSelectReHand);
        }

        _rehandTimer = ActionStartTriggerTimer(
              SelectDecisionTime
            , delegate ()
            {
                OnSelectReHand(GetLocalPlayerIndex(), new List<int>());
            }
            , SoundManager.SFX.Mulligan_Countdown_Tick
            , SoundManager.SFX.Mulligan_Countdown_Ended
            , UIManager.Instance.TimerRedraw
        );

        if (IsVSBot())
        {
            // All bot re-hand
            Bot[] bots = FindObjectsOfType<Bot>();
            foreach (Bot bot in bots)
            {
                bot.ReHand();
            }
        }
    }

    public void OnSelectReHand(PlayerIndex playerIndex, List<int> uniqueIDList)
    {
        if (IsNetworkPlay())
        {
            PhotonRequestReHand(playerIndex, uniqueIDList);
        }
        else
        {
            _OnSelectReHand(playerIndex, uniqueIDList);
        }
    }

    private void _OnSelectReHand(PlayerIndex playerIndex, List<int> uniqueIDList)
    {
        if (playerIndex == GetLocalPlayerIndex())
        {
            if (_rehandTimer != null)
            {
                _rehandTimer.StopTimer(false);
                _rehandTimer = null;
            }
        }

        if (uniqueIDList != null && uniqueIDList.Count > 0)
        {
            RequestReHand(playerIndex, uniqueIDList);
            RequestDrawCard(playerIndex, uniqueIDList.Count);
            // Shuffle
            RequestShuffleDeck(playerIndex);
        }

        Debug.Log("_OnSelectReHand " + playerIndex);
        _isConfirmReHand[(int)playerIndex] = true;
    }

    public void UpdateInfoAllPlayers()
    {
        foreach (PlayerIndex item in PlayerIndexList)
        {
            BattlePlayerData data = Data.PlayerDataList[(int)item];
            UIManager.Instance.UpdateUIPlayerInfo(
                  item
                , GetSumSpirit(item)
                , data.CurrentHP
                , data.DeckCount
                , data.CardList[CardZone.Graveyard].Count
            );
        }
    }

    public static PlayerIndex GetOpponentPlayerIndex(PlayerIndex myIndex)
    {
        PlayerIndex enemyIndex;
        if (myIndex == PlayerIndex.One)
        {
            enemyIndex = PlayerIndex.Two;
        }
        else
        {
            enemyIndex = PlayerIndex.One;
        }

        return enemyIndex;
    }

    public void NextPlayer()
    {
        SubTurn++;
        if (IsLocalPlayer(_currentPlayer))
        {
            _localTurn++;
        }

        int playerCount = GetPlayerCount();
        int playerIndexIndex = (int)_currentPlayer;

        if ((int)_currentPlayer == (playerCount - 1))
        {
            _currentPlayer = PlayerIndex.One;
        }
        else
        {
            _currentPlayer++;
        }

        if (SubTurn % playerCount != 0)
        {
            Turn++;
            _isNewTurn = true;
        }

        ResetSafeToLeaveTimer();
    }

    public PlayerIndex GetLocalPlayerIndex()
    {
        if (IsNetworkPlay())
        {
            if (!_isCacheLocalPlayerIndex)
            {
                foreach (PlayerIndex playerIndex in PlayerIndexList)
                {
                    if (DataManager.Instance.PlayerInfo.DisplayName == MatchData.PlayerData[(int)playerIndex].PlayerInfo.DisplayName)
                    {
                        _localPlayerIndex = playerIndex;
                        _isCacheLocalPlayerIndex = true;

                        return playerIndex;
                    }
                }

                Debug.LogWarning("Not found local playerIndex.");
            }
            else
            {
                return _localPlayerIndex;
            }
        }

        return PlayerIndex.One;
    }

    private PlayerIndex RandomPlayFirst()
    {
        int index = UnityEngine.Random.Range(0, PlayerIndexList.Count);
        return PlayerIndexList[index];
    }

    public bool FindBattleCard(string cardID, CardZone zone, PlayerIndex playerIndex, out BattleCardData battleCard)
    {
        if (_data != null)
        {
            return _data.FindBattleCard(cardID, zone, playerIndex, out battleCard);
        }

        battleCard = null;
        return false;
    }

    public bool FindBattleCard(int uniqueID, out BattleCardData battleCard)
    {
        if (_data != null)
        {
            return _data.FindBattleCard(uniqueID, out battleCard);
        }

        battleCard = null;
        return false;
    }

    public bool FindBattleCard(int uniqueID, out BattleCardData battleCard, out CardZone cardZone)
    {
        if (_data != null)
        {
            return _data.FindBattleCard(uniqueID, out battleCard, out cardZone);
        }

        battleCard = null;
        cardZone = CardZone.Deck;
        return false;
    }

    public bool FindBattleCard(List<int> uniqueIDList, out List<BattleCardData> battleCardList)
    {
        battleCardList = new List<BattleCardData>();
        if (_data != null && uniqueIDList != null && uniqueIDList.Count > 0)
        {
            BattleCardData battleCard = null;
            foreach (int uniqueID in uniqueIDList)
            {
                if (FindBattleCard(uniqueID, out battleCard))
                {
                    battleCardList.Add(battleCard);
                }
            }

            return (battleCardList.Count > 0);
        }

        return false;
    }

    public bool FindHeroBattleCard(int uniqueID, out MinionData heroSkill)
    {
        if (_data != null)
        {
            return _data.FindHeroBattleCard(uniqueID, out heroSkill);
        }

        heroSkill = null;
        return false;
    }

    public bool FindMinionBattleCard(int uniqueID, out MinionData minion)
    {
        if (_data != null)
        {
            return _data.FindMinionBattleCard(uniqueID, out minion);
        }

        minion = null;
        return false;
    }

    public bool FindMinionBattleCard(List<int> uniqueIDList, out List<MinionData> minionList)
    {
        minionList = new List<MinionData>();
        if (_data != null && uniqueIDList != null && uniqueIDList.Count > 0)
        {
            MinionData minion = null;
            foreach (int uniqueID in uniqueIDList)
            {
                if (FindMinionBattleCard(uniqueID, out minion))
                {
                    minionList.Add(minion);
                }
            }

            return (minionList.Count > 0);
        }

        return false;
    }

    public bool FindMinionBattleCard(string cardBaseID, out List<MinionData> minionList)
    {
        if (_data != null)
        {
            return _data.FindMinionBattleCard(cardBaseID, out minionList);
        }

        minionList = null;
        return false;
    }

    public bool FindSpellBattleCard(int uniqueID, out SpellData spell)
    {
        if (_data != null)
        {
            return _data.FindSpellBattleCard(uniqueID, out spell);
        }

        spell = null;
        return false;
    }

    public bool FindSpellBattleCard(List<int> uniqueIDList, out List<SpellData> spellList)
    {
        spellList = new List<SpellData>();
        if (_data != null && uniqueIDList != null && uniqueIDList.Count > 0)
        {
            SpellData spell = null;
            foreach (int uniqueID in uniqueIDList)
            {
                if (FindSpellBattleCard(uniqueID, out spell))
                {
                    spellList.Add(spell);
                }
            }

            return (spellList.Count > 0);
        }

        return false;
    }

    public void InitPhaseReady()
    {
        _phaseReadyIndex = new int[System.Enum.GetValues(typeof(GamePhase)).Length, System.Enum.GetValues(typeof(PlayerIndex)).Length];
        foreach (GamePhase phase in System.Enum.GetValues(typeof(GamePhase)))
        {
            foreach (PlayerIndex playerIndex in System.Enum.GetValues(typeof(PlayerIndex)))
            {
                _phaseReadyIndex[(int)phase, (int)playerIndex] = -1;
            }
        }
    }

    public bool IsCanShowSpiritEffect()
    {
        return Controller.IsCanShowSpiritEffect();
    }

    public void InitPokeStamp()
    {
        _pokeStamp = new DateTime[PlayerIndexList.Count];

        for (int index = 0; index < _pokeStamp.Length; ++index)
        {
            _pokeStamp[index] = DateTimeData.GetDateTimeUTC();
        }

        ResetSafeToLeaveTimer();
    }

    public bool IsLocalPlayer(PlayerIndex playerIndex)
    {
        if (playerIndex == GetLocalPlayerIndex())
        {
            return true;
        }

        return false;
    }

    public bool IsFriend(PlayerIndex player1, PlayerIndex player2)
    {
        return (player1 == player2);
    }

    public bool IsValidSlotID(int slotID)
    {
        return Controller.IsValidSlotID(slotID);
    }

    public bool GetIsConfirmReHand(PlayerIndex playerIndex)
    {
        int index = (int)playerIndex;
        if (_isConfirmReHand != null && _isConfirmReHand.Length > index)
        {
            return _isConfirmReHand[index];
        }

        return false;
    }

    public bool IsPhaseReady(GamePhase phase)
    {
        if (IsNetworkPlay())
        {
            if (_isEndGame || _isGotEndGame)
            {
                return true;
            }

            if (_isSetupGameMode && _phaseReadyIndex != null)
            {
                bool isReady = true;

                foreach (PlayerIndex playerIndex in PlayerIndexList)
                {
                    if (!IsPhaseReady(phase, playerIndex))
                    {
                        isReady = false;
                        break;
                    }
                }

                return isReady;
            }

            return false;
        }
        else
        {
            return true;
        }
    }

    public bool IsPhaseReady(GamePhase phase, PlayerIndex playerIndex)
    {
        if (IsNetworkPlay())
        {
            if (_isSetupGameMode && _phaseReadyIndex != null)
            {
                return (_phaseReadyIndex[(int)phase, (int)playerIndex] == SubTurn);
            }

            return false;
        }
        else
        {
            return true;
        }
    }

    public bool IsCanEndTurn(PlayerIndex playerIndex)
    {
        if (Phase == GamePhase.Play)
        {
            if (GameManager.Instance.CurrentPlayerIndex == playerIndex)
            {
                if (IsStoryMode())
                {
                    if (((StoryGameController)Controller).ControllerData.IsCanEndTurn)
                    {
                        if (!IsCallEndTurn)
                        {
                            return !_isEndTurn;
                        }
                    }
                }
                else
                {
                    if (!IsCallEndTurn)
                    {
                        return !_isEndTurn;
                    }
                }
            }
        }

        return false;
    }

    public bool IsCanSpawnToken(int slotID, int uniqueID)
    {
        MinionData minion;
        if (FindMinionBattleCard(uniqueID, out minion))
        {
            return IsSlotEmpty(slotID);
        }

        return false;
    }

    public bool IsLocalCanSpawnToken(int slotID)
    {
        return IsSlotEmpty(slotID) && (CurrentPlayerIndex == GetLocalPlayerIndex());
    }

    public bool IsAllEmptyDeck()
    {
        foreach (PlayerIndex playerIndex in PlayerIndexList)
        {
            if (!IsEmptyDeck(playerIndex))
            {
                return false;
            }
        }
        return true;
    }

    public bool IsEmptyDeck(PlayerIndex playerIndex)
    {
        if (Data.PlayerDataList[(int)playerIndex].CardList[CardZone.Deck].Count > 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public bool IsCanUseHandCard(PlayerIndex playerIndex)
    {
        foreach (BattleCardData card in Data.PlayerDataList[(int)playerIndex].CardList[CardZone.Hand])
        {
            if (IsCanUseCard(card))
            {
                return true;
            }
        }

        return false;
    }

    public bool IsCanUseCard(int uniqueCardID)
    {
        MinionData minion;
        if (FindMinionBattleCard(uniqueCardID, out minion))
        {
            return (IsCanUseCard(minion));
        }

        SpellData spell;
        if (FindSpellBattleCard(uniqueCardID, out spell))
        {
            //if (!IsActivePlay)
            {
                return true;
            }
        }

        return false;
    }

    private bool IsCanUseCard(BattleCardData card)
    {
        if (card.Type == CardType.Minion || card.Type == CardType.Minion_NotInDeck)
        {
            MinionData minion = card as MinionData;
            return IsCanUseCard(minion);
        }
        else
        {
            return true;
        }
    }

    private bool IsCanUseCard(MinionData minion)
    {
        return Controller.IsCanUseCard(minion);
    }

    public bool IsAllEmptyHand()
    {
        foreach (PlayerIndex playerIndex in PlayerIndexList)
        {
            if (!IsEmptyHand(playerIndex))
            {
                return false;
            }
        }
        return true;
    }

    public bool IsEmptyHand(PlayerIndex playerIndex)
    {
        if (GetHandCount(playerIndex) > 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public int GetHandCount(PlayerIndex playerIndex)
    {
        if (Data != null)
        {
            if (Data.PlayerDataList.Count > (int)playerIndex)
            {
                return Data.PlayerDataList[(int)playerIndex].CardList[CardZone.Hand].Count;
            }
        }

        return 0;
    }

    public List<SlotData> GetAllEmptySlots()
    {
        List<SlotData> slotDatas = new List<SlotData>();
        foreach (SlotData slot in Data.SlotDatas)
        {
            if (slot.IsSlotEmpty)
            {
                slotDatas.Add(slot);
            }
        }

        return slotDatas;
    }

    public bool IsHaveEmptySlot()
    {
        foreach (SlotData slot in Data.SlotDatas)
        {
            if (slot.IsSlotEmpty)
            {
                return true;
            }
        }

        return false;
    }

    public bool IsSlotEmpty(int slotID)
    {
        if (IsValidSlotID(slotID))
        {
            foreach (SlotData slot in Data.SlotDatas)
            {
                if (slot.SlotID == slotID)
                {
                    return slot.IsSlotEmpty;
                }
            }
        }

        return false;
    }

    public bool IsAllEmpty()
    {
        foreach (PlayerIndex playerIndex in PlayerIndexList)
        {
            if (!IsEmptyDeck(playerIndex) || !IsEmptyHand(playerIndex))
            {
                return false;
            }
        }
        return true;
    }

    public bool IsSkillCanUse(PlayerIndex playerIndex)
    {
        return Data.PlayerDataList[(int)playerIndex].Hero.IsSkillCanUsed;
    }

    public bool IsMasterClient()
    {
        return PhotonNetwork.IsMasterClient;
    }

    public bool IsNetworkPlay()
    {
        if (MatchData != null && MatchData.Property != null)
        {
            return (MatchData.Property.IsNetwork);
        }

        return false;
    }

    public bool IsHaveTimer()
    {
        if (MatchData != null && MatchData.Property != null)
        {
            return (MatchData.Property.IsHaveTimer);
        }

        return false;
    }

    public bool IsVSBot()
    {
        if (MatchData != null && MatchData.Property != null)
        {
            return (MatchData.Property.IsVSBot);
        }

        return false;
    }

    public bool IsVSPlayer()
    {
        List<GameMode> valideModeList = new List<GameMode>() { GameMode.Casual, GameMode.Rank, GameMode.Event, GameMode.Friendly };
        if (MatchData != null && MatchData.Property != null)
        {
            switch (Mode)
            {
                case GameMode.Casual:
                case GameMode.Friendly:
                case GameMode.Rank:
                    {
                        return true;
                    }
                case GameMode.Event:
                    {
                        return (MatchData.Property.IsNetwork);
                    }
            }
        }

        return false;
    }

    public BotType GetBotType(int index = 0)
    {
        if (MatchData != null && MatchData.Property != null && index < MatchData.Property.BotTypeList.Count)
        {
            return (MatchData.Property.BotTypeList[index]);
        }

        return BotType.None;
    }

    public BotLevel GetBotLevel(int index = 0)
    {
        if (MatchData != null && MatchData.Property != null && index < MatchData.Property.BotLevelList.Count)
        {
            return (MatchData.Property.BotLevelList[index]);
        }

        return BotLevel.None;
    }

    public bool IsTutorialMode()
    {
        if (MatchData != null && MatchData.Property != null)
        {
            switch (MatchData.GameMode)
            {
                case GameMode.Tutorial_1:
                case GameMode.Tutorial_2:
                case GameMode.Tutorial_3:
                case GameMode.Tutorial_4:
                    {
                        return true;
                    }

                default: return false;
            }
        }

        return false;
    }

    public bool IsStoryMode()
    {
        if (MatchData != null && MatchData.Property != null)
        {
            switch (MatchData.GameMode)
            {
                case GameMode.Tutorial_1:
                case GameMode.Tutorial_2:
                case GameMode.Tutorial_3:
                case GameMode.Tutorial_4:
                case GameMode.Adventure:
                    {
                        return true;
                    }

                default: return false;
            }
        }

        return false;
    }

    public int GetPlayerCount()
    {
        if (MatchData != null && MatchData.Property != null)
        {
            return (MatchData.Property.PlayerCount);
        }

        return 0;
    }

    public int GetSumSpirit(PlayerIndex playerIndex)
    {
        if (Data != null)
        {
            List<BattleCardData> cardList = Data.PlayerDataList[(int)playerIndex].CardList[CardZone.Battlefield];
            MinionData minion;
            if (cardList != null && cardList.Count > 0)
            {
                int sum = 0;
                foreach (BattleCardData card in cardList)
                {
                    if (card.Type == CardType.Minion || card.Type == CardType.Minion_NotInDeck)
                    {
                        minion = card as MinionData;
                        sum += minion.SpiritCurrent;
                    }
                }
                return sum;
            }
        }

        return 0;
    }

    public void UpdateLocalHandCard()
    {
        PlayerIndex localPlayerIndex = GetLocalPlayerIndex();
        if (!UIManager.Instance.IsMulliganShowing)
        {
            foreach (BattleCardData card in Data.PlayerDataList[(int)localPlayerIndex].CardList[CardZone.Hand])
            {
                if (
                       localPlayerIndex == CurrentPlayerIndex
                    && IsCanUseCard(card)
                    && UIQueue.Instance.IsReady
                    && CommandQueue.Instance.IsReady
                )
                {
                    UIManager.Instance.ShowCardsIsReady(card.UniqueID);
                }
                else
                {
                    UIManager.Instance.HideCardsIsReady(card.UniqueID);
                }
            }
        }
    }

    public void ActionAddAttackerList(MinionData minion, UnityAction onComplete = null)
    {
        if (_attackerList == null)
        {
            _attackerList = new List<MinionData>();
        }

        if (!_attackerList.Contains(minion) && Phase < GamePhase.Battle)
        {
            _attackerList.Add(minion);
            UIManager.Instance.RequestTokenGlowActivate(minion.UniqueID, true, false);

            Requester triggerer = new Requester(minion);
            List<EventTriggerData> triggerList = new List<EventTriggerData>();
            triggerList.Add(new EventTriggerData(EventKey.OnMeAddedToAttackerList, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnFriendAddedToAttackerList, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnEnemyAddedToAttackerList, triggerer));

            ActionAddCommandListToSecondQueue(new List<CommandData>(EventTrigger.TriggerCommandList(triggerList)), true);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionRemoveAttackerFromList(MinionData minion, UnityAction onComplete = null)
    {
        if (_attackerList == null)
        {
            _attackerList = new List<MinionData>();
        }

        int index = _attackerList.FindIndex(x => x.UniqueID == minion.UniqueID);
        if (index > -1 && Phase < GamePhase.Battle)
        {
            _attackerList.RemoveAt(index);
            UIManager.Instance.RequestTokenGlowActivate(minion.UniqueID, false, false);

            Requester triggerer = new Requester(minion);
            List<EventTriggerData> triggerList = new List<EventTriggerData>();
            triggerList.Add(new EventTriggerData(EventKey.OnMeRemovedFromAttackerList, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnFriendRemovedFromAttackerList, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnEnemyRemovedFromAttackerList, triggerer));

            ActionAddCommandListToSecondQueue(new List<CommandData>(EventTrigger.TriggerCommandList(triggerList)), true);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionRemoveAttackerList(MinionData minion)
    {
        if (_attackerList == null)
        {
            _attackerList = new List<MinionData>();
        }

        if (_attackerList.Contains(minion))
        {
            _attackerList.Remove(minion);
            UIManager.Instance.RequestTokenGlowActivate(minion.UniqueID, false, false);
        }
    }

    public void UpdateAllUIInfo(bool isAdd = true)
    {
        foreach (PlayerIndex playerIndex in PlayerIndexList)
        {
            BattlePlayerData player = Data.PlayerDataList[(int)playerIndex];
            UIManager.Instance.RequestUpdateUIInfo(
                  playerIndex
                , GetSumSpirit(playerIndex)
                , player.CurrentHP
                , player.DeckCount
                , player.CardList[CardZone.Graveyard].Count
                , isAdd
            );
        }
    }

    public void ResetActiveCount()
    {
        _activeCount = 0;
    }

    public void SetActiveCount(int value)
    {
        _activeCount = value;
    }

    public void AddActiveCount(int value)
    {
        _activeCount = _activeCount + value;
    }

    public void ResetPlayCardCount()
    {
        _playCardCount = 0;
    }

    public void SetPlayCardCount(int value)
    {
        _playCardCount = value;
    }

    public void AddPlayCardCount(int value)
    {
        _playCardCount = _playCardCount + value;
    }

    public void SetRandomSeed(int seed)
    {
        _randomSeed = seed;
        UnityEngine.Random.InitState(_randomSeed);
    }

    public string CreateSaveKey(string key, Requester requester)
    {
        return string.Format(
                  "{0}_{1}"
                , key
                , requester.GetUniqueID()
            //, GetLocalPlayerIndex()
            );
    }

    public void LoadScene(string sceneName, LoadSceneMode loadSceneMode, bool isAsync, UnityAction onComplete)
    {
        if (isAsync)
        {
            StartCoroutine(LoadingSceneAsync(sceneName, loadSceneMode, onComplete));
        }
        else
        {
            SceneManager.LoadScene(sceneName, loadSceneMode);
        }
    }

    private IEnumerator LoadingSceneAsync(string sceneName, LoadSceneMode loadSceneMode, UnityAction onComplete)
    {
        Debug.LogFormat("Loading scene {0}", sceneName);

        yield return new WaitForSeconds(2.0f);

        AsyncOperation asyncLoadUI = SceneManager.LoadSceneAsync(sceneName, loadSceneMode);

        // Wait until the asynchronous scene fully loads
        while (!asyncLoadUI.isDone)
        {
            PopupUIManager.Instance.SetLoadingProgress(asyncLoadUI.progress);

            yield return null;
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    private void ShowEndGameHint(EndGameType endGameType)
    {
        string text = "";

        switch (endGameType)
        {
            case EndGameType.Disconnect: text = string.Format("<color=red>{0}</color>", LocalizationManager.Instance.GetText("BATTLE_GAME_DISCONNECT")); break;
            case EndGameType.OtherLeftRoom: text = string.Format("<color=red>{0}</color>", LocalizationManager.Instance.GetText("BATTLE_OPPONENT_LEFTED")); break;
            case EndGameType.OtherSurrender: text = string.Format("<color=red>{0}</color>", LocalizationManager.Instance.GetText("BATTLE_GAME_OTHER_SURRENDER")); break;
            default: return;
        }

        // Show status
        UIManager.Instance.ShowHint(text);
    }

    private void ShowRedHint(string text)
    {
        string showText = "";

        showText = string.Format("<color=red>{0}</color>", text);

        // Show status
        UIManager.Instance.ShowHint(showText);
    }

    private void ShowRedHint(string text, int timer)
    {
        string showText = "";

        showText = string.Format("<color=red>{0} {1}...</color>", text, timer);

        // Show status
        UIManager.Instance.ShowHint(showText);
    }

    private void ShowGreenHint(string text)
    {
        string showText = "";

        showText = string.Format("<color=green>{0}</color>", text);

        // Show status
        UIManager.Instance.ShowHint(showText);
    }

    private void ShowGreenHintAutoClose(string text, float delayClose)
    {
        string showText = "";

        showText = string.Format("<color=green>{0}</color>", text);

        // Show status
        UIManager.Instance.ShowHint(showText);
        GameHelper.DelayCallback(delayClose, UIManager.Instance.HideHint);
    }

    private void ShowEndGameResult(bool isVictory)
    {
        SoundManager.StopBGM(0.0f);
        PlayerIndex losePlayer = GetLosePlayer(isVictory);

        if (IsNetworkPlay())
        {
            PhotonRequestEndGame(losePlayer, _endGameType);
        }

        UIManager.Instance.ShowActionLog(false);
        UIManager.Instance.ShowBTNOverlayUI(false);
        UIManager.Instance.BTN_EndTurn.SetEnable(false);
        UIManager.Instance.RequestShowDestroyAllCard(losePlayer);
        //ActionSendMessage(losePlayer, HeroVoice.Lose, null);
        UIManager.Instance.RequestShowHeroDeadCard(losePlayer);

        DataManager.Instance.IsWinLastMatch = isVictory;
        if (IsStoryMode())
        {
            if (_endGameType == EndGameType.Normal)
            {
                StoryGameController c = (StoryGameController)Controller;
                c.OnShowEndGameResult(isVictory, Mode);
            }
            else
            {
                ActionSendMessage(losePlayer, HeroVoice.Lose, null);
            }
        }
        else
        {
            ActionSendMessage(losePlayer, HeroVoice.Lose, null);
        }

        CommandQueue.Instance.Clear(); // Clear queue
        CommandQueue.Instance.ClearPauseState();

        // Count stat
        PopupUIManager.Instance.Show_SoftLoad(true);

        PlayerStatManager.Instance.DoEndGameResultEvent(
              isVictory
            , delegate ()
            {

                Debug.Log("TIER: " + DataManager.Instance.PlayerInfo.GetPlayerTier().ToString());

                if (isVictory)
                {
                    GameEventCloudScriptHelper.EndMatchExecute(Mode);
                    UIManager.Instance.ShowLevelSummary(isVictory, () => ShowAllEndGameSummary(() => Controller.OnCheckClaimReward(ExitGame)));
                }
                else
                {
                    UIManager.Instance.ShowLevelSummary(isVictory, () => ShowAllEndGameSummary(ExitGame));
                }
                PopupUIManager.Instance.Show_SoftLoad(false);
            }
        );
    }

    private PlayerIndex GetLosePlayer(bool isVictory)
    {
        PlayerIndex losePlayer;
        if (isVictory)
            losePlayer = GetLocalPlayerIndex() == PlayerIndex.One ? PlayerIndex.Two : PlayerIndex.One;
        else
            losePlayer = GetLocalPlayerIndex();

        return losePlayer;
    }

    private void ShowAllEndGameSummary(UnityAction onComplete)
    {
        if (Mode == GameMode.Rank)
        {
            ShowRankUpdate(onComplete);
        }
        else
        {
            onComplete?.Invoke();
        }
    }

    private void ShowLevelReward(UnityAction onComplete)
    {
        List<ItemData> itemDataList = new List<ItemData>();
        PlayerLevelData newLevelData = DataManager.Instance.PlayerInfo.LevelData;

        List<LevelRewardDBData> dataList;
        if (LevelRewardDB.Instance.GetAllData(out dataList))
        {
            foreach (LevelRewardDBData data in dataList)
            {
                if (data.Level <= newLevelData.Level
                   && !DataManager.Instance.PlayerLevelRewardRecord.IsRewardClaimed(data.Level))
                {
                    itemDataList.AddRange(data.RewardItemList);
                    DataManager.Instance.PlayerLevelRewardRecord.AddRewardData(data.Level);
                }
            }
        }

        if (itemDataList.Count > 0)
        {
            string title = LocalizationManager.Instance.GetText("TEXT_LEVEL_UP");
            string message = LocalizationManager.Instance.GetText("TEXT_GOT_REWARD");
            PopupUIManager.Instance.ShowLevelUpReward(title, message, itemDataList, onComplete);
            return;
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    private void ShowRankUpdate(UnityAction onComplete)
    {
        int currentRank = DataManager.Instance.PlayerInfo.GetPlayerCurrentRank();
        if (_startRank != currentRank)
        {
            PopupUIManager.Instance.ShowRankUpdate("RANK_UPDATE", "RANK_UPDATE", _startRank, currentRank, onComplete);
            return;
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    private void UpdatePassingTime()
    {
        if (IsHaveTimer())
        {
            //Debug.Log("UpdatePassingTime");

            DateTime currentTimeStamp = DateTimeData.GetDateTimeUTC();
            float passTime = (float)(currentTimeStamp - _startTurnStamp).TotalSeconds;

            float timer = 0.0f;
            if (
                  _afkCount != null
                && (_afkCount.Length > (int)CurrentPlayerIndex)
                && (_afkCount[(int)CurrentPlayerIndex] > 0)
            )
            {
                timer = AFKTime - passTime;
            }
            else if (IsStartWithEmptyHand)
            {
                timer = EmptyHandTurnTime - passTime;
            }
            else
            {
                timer = TurnTime - passTime;
            }
            _timer = timer;

            OnUpdatePassingTime?.Invoke();
        }
    }

    public void ForceExitGame()
    {
        _isTimer = false;
        RequestLocalSurrender(true);
    }

    private void RequestLocalSurrender(bool isCallEndGame = true)
    {
        if (!_isResign)
        {
            _isResign = true;
            _endGameType = EndGameType.Surrender;

            if (IsNetworkPlay())
            {
                PhotonRequestSurrender(GetLocalPlayerIndex());
            }

            if (isCallEndGame)
            {
                OnEndGame();
            }
        }
    }

    private void ExitGame()
    {
        _isEndGame = true;
        ShowFullLoad(true);

        //Destroy bot
        if (IsVSBot())
        {
            Bot[] bots = GameObject.FindObjectsOfType<Bot>();
            for (int i = 0; i < bots.Length; i++)
            {
                bots[i].SetEnable(false);
                Destroy(bots[i].gameObject);
            }
        }

        if (_debugManager != null)
        {
            _debugManager.gameObject.SetActive(false);
            Destroy(_debugManager.gameObject);
        }

        DebugManager debugManager = Transform.FindObjectOfType<DebugManager>();
        if (debugManager != null)
        {
            debugManager.gameObject.SetActive(false);
            Destroy(debugManager.gameObject);
        }

        Time.timeScale = 1.0f; // Set time scale to default.

        CommandQueue.Instance.OnActiveCompleteEvent -= this.OnCommmandActiveComplete;
        UIQueue.Instance.OnActiveCompleteEvent -= this.OnCommmandActiveComplete;

        CommandQueue.Instance.Clear();
        UIQueue.Instance.Clear();
        CommandQueue.Instance.ClearPauseState();
        UIQueue.Instance.ClearPauseState();
        ActionLogPool.Instance.Clear();

        //UIManager.Instance.InitUIQueue();
        //BattleQueue.Instance.Clear();

        BattleCardData.ResetUniqueID();

        SoundManager.ClearAll();
        CardVFXPool.Instance.Clear();
        PopupPool.Instance.Clear();
        PhotonQueue.Instance.Clear();
        UtilityPool.Instance.Clear();
        Karamucho.UI.CardResourceManager.ClearCache();
        Karamucho.ResourceManager.Clear();
        ClearAllSaveKey();

        // Destroy All Singleton
        CommandQueue.Instance.DestroySelf();
        UIQueue.Instance.DestroySelf();

        UIEffectManager.Instance.DestroySelf();
        CardUIManager.Instance.DestroySelf();
        BoardManager.Instance.DestroySelf();
        UIManager.Instance.DestroySelf();

        if (IsNetworkPlay())
        {
            PhotonNetwork.Disconnect();

            Destroy(GetComponent<PhotonView>());
            Destroy(GetComponent<PhotonEventCapture>());
        }
        StopAllCoroutines();

        OnEndGameEvent?.Invoke(Mode, _isYouWin, _endGameType);

        UnityAction loadMenuScene = delegate ()
        {
            LoadScene(GameHelper.NavigatorSceneName, LoadSceneMode.Single, true, delegate
            {
                OnDestroy?.Invoke();

                // Destroy All Singleton
                CommandQueue.Instance.DestroySelf();
                UIQueue.Instance.DestroySelf();

                UIEffectManager.Instance.DestroySelf();
                CardUIManager.Instance.DestroySelf();
                BoardManager.Instance.DestroySelf();
                UIManager.Instance.DestroySelf();

                this.DestroySelf();
            });
        };

        DataManager.Instance.LoadInventory(
              loadMenuScene
            , delegate (string error)
            {
                Debug.LogWarning(error);
                loadMenuScene?.Invoke();
            }
        );
    }

    public void DisconnectPhoton()
    {
        if (IsNetworkPlay())
        {
            PhotonNetwork.LeaveRoom(false);

            PhotonNetwork.Disconnect();
        }
    }

    public void ResetSafeToLeaveTimer()
    {
        _safeToLeaveTimer = 0.0f;
    }

    public void SetShowDebugLog(bool isShow)
    {
        if (_debugManager == null)
        {
            _debugManager = Transform.FindObjectOfType<DebugManager>();
        }

        if (_debugManager != null)
        {
            DataManager.Instance.SetShowDebugLog(isShow);
            _debugManager.gameObject.SetActive(isShow);
        }
    }

    public string GetDebugText()
    {
        if (_sb == null) _sb = new StringBuilder();
        _sb.Clear();

        // attacker list
        string attackerText = "";
        if (_attackerList != null && _attackerList.Count > 0)
        {
            foreach (MinionData card in _attackerList)
            {
                attackerText += string.Format(" {0}:{1}", card.UniqueID, card.Name);
            }
        }
        else
        {
            attackerText = " Empty";
        }

        _sb.AppendFormat(
              "<b>CurrentPlayer:</b> {0}\t<b>Phase:</b> {1}\t<b>Battle Phase:</b> {2}\n" +
              "<b>Turn:</b> {3}\t<b>SubTurn:</b> {4}\t<b>Timer:</b> {5}\n" +
              "<b>RandomSeed:</b> {6}\n" +
              "<b>Attacker List:</b> {7}\n" +
              "Board:\n{8}\n\n"
            , CurrentPlayerIndex.ToString()
            , Phase.ToString()
            , BattleManager.Instance.Phase.ToString()
            , Turn.ToString()
            , SubTurn.ToString()
            , _timer.ToString("0.00")
            , _randomSeed
            , attackerText
            , (Data != null) ? Data.GetBoardDebugText() : "Empty"
        );

        if (PlayerIndexList != null && PlayerIndexList.Count > 0)
        {
            if (IsNetworkPlay())
            {
                string phaseReadyText = "";
                foreach (GamePhase phase in System.Enum.GetValues(typeof(GamePhase)))
                {
                    phaseReadyText += phase.ToString() + "\t\t";
                    foreach (PlayerIndex playerIndex in PlayerIndexList)
                    {
                        phaseReadyText += string.Format(
                              "<color={0}>[{1}]</color>"
                            , (_phaseReadyIndex[(int)phase, (int)playerIndex] < SubTurn) ? "red" : "green"
                            , _phaseReadyIndex[(int)phase, (int)playerIndex]
                        );
                    }
                    phaseReadyText += "\n";
                }
                _sb.AppendFormat("Phase Ready:\n{0}\n", phaseReadyText);
            }
        }

        if (_data != null && _data.PlayerDataList != null)
        {
            foreach (PlayerIndex playerIndex in PlayerIndexList)
            {
                int index = (int)playerIndex;

                _sb.AppendFormat(
                      "{0}<b>[{1} {2}]</b>\n{3}"
                    , (index == 0) ? "" : "\n\n"
                    , playerIndex.ToString()
                    , _data.PlayerDataList[index].PlayerInfo.DisplayName
                    , _data.PlayerDataList[index].GetDebugText()
                );
            }
        }

        // Battle log
        /*
        if (_battleLog != null)
        {
            _sb.Append("\nBattle Log\n");
            int count = 20; // show last 20 logs.
            for (int index = _battleLog.Count - 1; index >= 0 && count >= 0; --index)
            {
                _sb.AppendFormat("\n[{0}] {1}", index, _battleLog[index].ToString());
                --count;
            }
        }
        */

        return _sb.ToString();
    }
    #endregion

    #region Save Methods
    public void SaveInteger(string key, int value)
    {
        CacheSaveKey(key);
        PlayerPrefs.SetInt(key, value);
    }

    public void SaveString(string key, string value)
    {
        CacheSaveKey(key);
        PlayerPrefs.SetString(key, value);
    }

    public void SavePlayerIndexList(string key, List<PlayerIndex> value)
    {
        List<string> stringList = new List<string>();
        foreach (PlayerIndex playerIndex in value)
        {
            stringList.Add(playerIndex.ToString());
        }

        CacheSaveKey(key);
        PlayerPrefsX.SetStringArray(key, stringList.ToArray());
    }

    public void SaveMinionList(string key, List<MinionData> value)
    {
        List<int> idList = new List<int>();
        foreach (MinionData card in value)
        {
            idList.Add(card.UniqueID);
        }

        SaveMinionIDList(key, idList);
    }

    public void SaveMinionIDList(string key, List<int> value)
    {
        if (value != null)
        {
            List<int> idList = new List<int>(value);

            CacheSaveKey(key);
            PlayerPrefsX.SetIntArray(key, idList.ToArray());
        }
    }

    public void SaveSlotList(string key, List<SlotData> value)
    {
        List<int> idList = new List<int>();
        foreach (SlotData slot in value)
        {
            idList.Add(slot.SlotID);
        }
        SaveSlotIDList(key, idList);
    }

    public void SaveSlotIDList(string key, List<int> value)
    {
        if (value != null)
        {
            List<int> idList = new List<int>(value);

            CacheSaveKey(key);
            PlayerPrefsX.SetIntArray(key, idList.ToArray());
        }
    }

    private void CacheSaveKey(string key)
    {
        if (_battleSaveKeyList == null)
        {
            _battleSaveKeyList = new List<string>();
        }

        if (!_battleSaveKeyList.Contains(key))
        {
            _battleSaveKeyList.Add(key);
        }
        PlayerPrefsX.SetStringArray("BattleKeys", _battleSaveKeyList.ToArray());
    }

    public void ClearAllSaveKey()
    {
        List<string> battleSaveKeyList = new List<string>();
        battleSaveKeyList.AddRange(PlayerPrefsX.GetStringArray("BattleKeys"));

        if (battleSaveKeyList.Count > 0)
        {
            foreach (string key in battleSaveKeyList)
            {
                PlayerPrefs.DeleteKey(key);
            }

            PlayerPrefs.DeleteKey("BattleKeys");

            if (_battleSaveKeyList != null && _battleSaveKeyList.Count > 0)
            {
                _battleSaveKeyList.Clear();
            }
        }
    }

    public bool GetSavedInteger(string key, out int value)
    {
        if (PlayerPrefs.HasKey(key))
        {
            value = PlayerPrefs.GetInt(key);
            return true;
        }

        value = 0;
        return false;
    }

    public bool GetSavedString(string key, out string value)
    {
        if (PlayerPrefs.HasKey(key))
        {
            value = PlayerPrefs.GetString(key);
            return true;
        }

        value = "";
        return false;
    }

    public bool GetSavedPlayerIndexList(string key, out List<PlayerIndex> value)
    {
        value = new List<PlayerIndex>();
        if (PlayerPrefs.HasKey(key))
        {
            string[] strArray = PlayerPrefsX.GetStringArray(key);

            PlayerIndex playerIndex;
            foreach (string playerIndexText in strArray)
            {
                if (GameHelper.StrToEnum<PlayerIndex>(playerIndexText, out playerIndex))
                {
                    value.Add(playerIndex);
                }
            }
        }

        return (value != null && value.Count > 0);
    }

    public bool GetSavedMinionList(string key, out List<MinionData> value)
    {
        value = new List<MinionData>();
        if (PlayerPrefs.HasKey(key))
        {
            int[] intArray = PlayerPrefsX.GetIntArray(key);
            MinionData minion;
            foreach (int minionUniqueID in intArray)
            {
                if (Data.FindMinionBattleCard(minionUniqueID, out minion))
                {
                    value.Add(minion);
                }
            }
        }

        return (value != null && value.Count > 0);
    }

    public bool GetSavedSlotList(string key, out List<SlotData> value)
    {
        value = new List<SlotData>();
        if (PlayerPrefs.HasKey(key))
        {
            int[] intArray = PlayerPrefsX.GetIntArray(key);
            foreach (int id in intArray)
            {
                value.Add(Data.SlotDatas[id]);
            }
        }

        return (value != null && value.Count > 0);
    }
    #endregion

    #region Event Methods
    private void OnConnect()
    {
        Debug.Log("OnConnect");
    }

    private void OnConnectToMaster()
    {
        Debug.Log("OnConnectToMaster");
    }

    private void OnDisconnect(DisconnectCause cause)
    {
        Debug.LogFormat("OnDisconnect: {0}", cause.ToString());

        if (IsNetworkPlay())
        {
            if (!_isDisconnect)
            {
                if (!_isEndGame && !_isGotEndGame && !_isResign)
                {
                    _isDisconnect = true;

                    _endGameType = EndGameType.Disconnect;

                    // Show status
                    ShowEndGameHint(EndGameType.Disconnect);

                    if (Phase == GamePhase.Init
                        || Phase == GamePhase.SetupData
                        || Phase == GamePhase.Intro
                        || Phase == GamePhase.ReHand
                    )
                    {
                        // Disconnect before start play. 
                        PhotonNetwork.Disconnect();

                        _isEndGame = true;
                        Invoke("ExitGame", 2.0f);
                    }
                    else
                    {
                        //if (PhotonNetwork.NetworkClientState == ClientState.Disconnected)
                        {
                            // Try reconnect
                            TryReconnect();
                        }
                    }
                }
            }
        }
    }

    private void TryReconnect()
    {
        Debug.Log("TryReconnect...");

        if (!_isStartReconnect)
        {
            if (_tryReconnectCoroutine != null)
            {
                StopCoroutine(_tryReconnectCoroutine);
                _tryReconnectCoroutine = null;
            }

            if (_timeoutCounter != null)
            {
                _timeoutCounter.StopTimer(false);
                _timeoutCounter = null;
            }

            _timeoutCounter = ActionStartTriggerTimer(
                  Mathf.Max(0.1f, ReConnectTimeout)
                , OnReconnectTimeout
                , delegate (float timer)
                {
                    // Show status
                    {
                        ShowRedHint(LocalizationManager.Instance.GetText("BATTLE_GAME_DISCONNECT"), Mathf.RoundToInt(timer));
                    }
                }
            );

            _timeoutCounter.gameObject.name = "WaitObject_Reconnect";

            if (_reconnectLimit > 0)
            {
                _isStartReconnect = true;
                _tryReconnectCoroutine = StartCoroutine(OnTryReconnect());
            }
            else
            {
                ShowRedHint(LocalizationManager.Instance.GetText("BATTLE_GAME_TOO_MANY_DISCONNECT"));
                OnReconnectTimeout();
            }
        }
    }

    private IEnumerator OnTryReconnect()
    {
        while (!_isReconnectTimeout)
        {
            //Debug.LogFormat("OnTryReconnect... {0}", PhotonNetwork.NetworkingClient.LoadBalancingPeer.PeerState);

            switch (PhotonNetwork.NetworkingClient.LoadBalancingPeer.PeerState)
            {
                case ExitGames.Client.Photon.PeerStateValue.Disconnected:
                    {
                        string roomName = DataManager.Instance.GetRoomName();
                        if (!string.IsNullOrEmpty(roomName))
                        {
                            Debug.Log("Start ReconnectAndRejoin...");
                            PhotonNetwork.ReconnectAndRejoin();
                            break;
                        }
                        else
                        {
                            OnReconnectTimeout();
                            yield break;
                        }
                    }
                    break;
            }

            // wait 0.5 second
            yield return new WaitForSeconds(0.5f);
        }
    }

    private void OnLeftRoom()
    {
        Debug.Log("OnLeftRoom");

        if (IsNetworkPlay())
        {
            if (!_isDisconnect)
            {
                if (_isEndGame || _isGotEndGame)
                {
                    Debug.Log("Start Disconnect...");
                    PhotonNetwork.Disconnect();
                }
            }
        }
    }

    private void OnPlayerLeftRoom(Player otherPlayer)
    {
        Debug.LogFormat(
              "OnPlayerLeftRoom : {0}"
            , (otherPlayer != null) ? otherPlayer.NickName : "null"
        );

        if (IsNetworkPlay())
        {
            if (!_isDisconnect)
            {
                _isDisconnect = true;

                if (!_isEndGame && !_isGotEndGame && !_isResign)
                {
                    _endGameType = EndGameType.OtherLeftRoom;

                    if (Phase == GamePhase.Init
                       || Phase == GamePhase.SetupData
                       || Phase == GamePhase.Intro
                       || Phase == GamePhase.ReHand
                    )
                    {
                        _isEndGame = true;

                        PlayerStatManager.Instance.DoClearPlayerMatchTicket(
                            DataManager.Instance.GetRoomName()
                            , delegate ()
                            {
                                // Disconnect before start play. 

                                GameManager.Instance.DisconnectPhoton();
                                ExitGame();
                            }
                        );
                    }
                    else
                    {
                        // Show status
                        ShowEndGameHint(EndGameType.OtherLeftRoom);

                        _timeoutCounter = ActionStartTriggerTimer(
                              Mathf.Max(0.1f, WaitReConnectTimeout)
                            , OnWaitOpponentReconnectTimeout
                            , delegate (float timer)
                            {
                                ShowRedHint(LocalizationManager.Instance.GetText("BATTLE_OPPONENT_LEFTED"), Mathf.RoundToInt(timer));
                            }
                        );

                        _timeoutCounter.gameObject.name = "WaitObject_WaitReconnect";
                    }
                }
            }
        }
    }

    private void OnJoinedRoom()
    {
        Debug.Log("OnJoinedRoom");

        if (IsNetworkPlay())
        {
            if (_isDisconnect)
            {
                _isDisconnect = false;

                if (!_isEndGame && !_isGotEndGame && !_isResign)
                {
                    _isSyncRPC = true;

                    Debug.Log("OnJoinedRoom: Start Sync");
                    _isStartReconnect = false;

                    DateTime currentTimeStamp = DateTimeData.GetDateTimeUTC();
                    float passTime = (float)(currentTimeStamp - _startTurnStamp).TotalSeconds;
                    if (passTime >= GetDisconnectTimeLimit())
                    {
                        _reconnectLimit--;
                    }

                    InitPokeStamp();

                    _endGameType = EndGameType.Normal;

                    // Update passing time.
                    UpdatePassingTime();

                    if (_tryReconnectCoroutine != null)
                    {
                        StopCoroutine(_tryReconnectCoroutine);
                        _tryReconnectCoroutine = null;
                    }

                    if (_timeoutCounter != null)
                    {
                        _timeoutCounter.StopTimer(false);
                        _timeoutCounter = null;
                    }

                    ShowGreenHintAutoClose(LocalizationManager.Instance.GetText("BATTLE_RECONNECTED"), 1.0f);

                    PhotonStartSyncRPC();
                }
            }
        }
    }

    private void OnPlayerEnteredRoom(Player newPlayer)
    {
        Debug.Log("OnPlayerEnteredRoom");

        if (IsNetworkPlay())
        {
            if (Phase >= GamePhase.Begin && Phase <= GamePhase.End)
            {
                if (_isDisconnect)
                {
                    _isDisconnect = false;

                    ShowGreenHintAutoClose(LocalizationManager.Instance.GetText("BATTLE_OPPONENT_RECONNECTED"), 1.0f);

                    InitPokeStamp();

                    if (!_isEndGame && !_isGotEndGame && !_isResign)
                    {
                        _isSyncRPC = true;

                        Debug.Log("OnPlayerEnteredRoom: Start Sync");

                        _endGameType = EndGameType.Normal;

                        // Update passing time.
                        UpdatePassingTime();

                        if (_timeoutCounter != null)
                        {
                            _timeoutCounter.StopTimer(false);
                            _timeoutCounter = null;
                        }

                        PhotonStartSyncRPC();
                    }
                }
            }
        }
    }

    private void OnJoinRoomFailed(short returnCode, string message)
    {
        Debug.Log("OnJoinRoomFailed");

        if (IsNetworkPlay())
        {
            //if (_isDisconnect)
            {
                _isDisconnect = true;
                _isStartReconnect = false;

                // Show status
                {
                    ShowRedHint(LocalizationManager.Instance.GetText("BATTLE_FAIL_RECONNECT"));
                }

                if (!_isReconnectTimeout)
                {
                    //if (PhotonNetwork.NetworkClientState == ClientState.Disconnected)
                    {
                        // Try reconnect
                        TryReconnect();
                    }
                }
            }
        }
    }

    private void OnPlayerPropertiesUpdate(Player target, ExitGames.Client.Photon.Hashtable changedProps)
    {
        Debug.LogFormat(
              "OnPlayerPropertiesUpdate : {0} Inactive: {1}"
            , target.NickName
            , target.IsInactive ? "<color=red>Inactive</color>" : "<color=green>Active</color>"
        );
    }

    private void OnReconnectTimeout()
    {
        _isReconnectTimeout = true;
        _isStartReconnect = false;

        if (_tryReconnectCoroutine != null)
        {
            StopCoroutine(_tryReconnectCoroutine);
            _tryReconnectCoroutine = null;
        }

        if (_timeoutCounter != null)
        {
            _timeoutCounter.StopTimer(false);
            _timeoutCounter = null;
        }

        // Show status
        {
            ShowRedHint(LocalizationManager.Instance.GetText("BATTLE_FAIL_RECONNECT"));
        }

        OnEndGame();
    }

    private void OnWaitOpponentReconnectTimeout()
    {
        _isReconnectTimeout = true;

        if (_timeoutCounter != null)
        {
            _timeoutCounter.StopTimer(false);
            _timeoutCounter = null;
        }

        string debugText = string.Format(
              "<color=red>{0}</color>"
            , LocalizationManager.Instance.GetText("BATTLE_OPPONENT_LEFTED")
        );

        UIManager.Instance.ShowHint(debugText);

        OnEndGame();
    }

    public void TryNormalEndGame()
    {
        if (!_isEndGame)
        {
            Debug.Log("Gameover!");

            _endGameType = EndGameType.Normal;
            OnEndGame();
        }
    }

    public void OnEndGame()
    {
        Debug.Log("OnEndGame");

        if (!_isEndGame)
        {
            _isEndGame = true;
            _isTimer = false;
            Phase = GamePhase.End;

            UIManager.Instance.HideHint();
            PlayerIndex localPlayer = GetLocalPlayerIndex();
            BattlePlayerData playerData = Data.PlayerDataList[(int)localPlayer];

            {
                _isYouWin = (playerData.CurrentHP > 0 /*&& _afkCount[(int)localPlayer] < AFKLose*/);
            }

            GetServerUTCTime(
                delegate (DateTime serverUTCTime)
                {
                    _endPlayServerTime = serverUTCTime;
                    _endPlayStamp = Time.realtimeSinceStartup;

                    TimeSpan playTimeServer = (_endPlayServerTime - _startPlayServerTime);
                    float playTime = (_endPlayStamp - _startPlayStamp);

                    string log = string.Format("{0} - {1} = {2} > {3} - {4} = {5}"
                        , _startPlayStamp, _endPlayStamp, playTime
                        , _startPlayServerTime, _endPlayServerTime, playTimeServer.TotalSeconds
                    );
                    Debug.Log(log);

                    if ((playTimeServer.TotalSeconds + AllowServerTimeDelay) < playTime) // add offset for server delay.
                    {
                        _endGameType = EndGameType.PlayTimeHack;
                    }

                    if (_endGameType != EndGameType.Normal)
                    {
                        CardUIManager.Instance.ForceCancelSelected();
                        UIManager.Instance.HideSelectionSlot(null);

                        CommandQueue.Instance.Clear();
                        UIQueue.Instance.Clear();
                        //_actionAbilityCommandList.Clear();

                        CommandQueue.Instance.ClearPauseState();
                        UIQueue.Instance.ClearPauseState();
                    }

                    switch (_endGameType)
                    {
                        case EndGameType.SafeToLeave:
                            {
                                _isYouWin = false;
                                AnalyticTriggerEndMatch();
                                Phase = GamePhase.End;

                                ExitGame();
                                return;
                            }
                            break;

                        case EndGameType.Disconnect:
                        case EndGameType.Surrender:
                            {
                                _isYouWin = false;
                                ShowEndGameResult(false);
                            }
                            break;

                        case EndGameType.OtherLeftRoom:
                        case EndGameType.OtherSurrender:
                            {
                                if (_endGameType == EndGameType.OtherSurrender)
                                {
                                    // Show status
                                    ShowEndGameHint(EndGameType.OtherSurrender);
                                }

                                _isYouWin = true;
                                ShowEndGameResult(true);
                            }
                            break;

                        case EndGameType.PlayTimeHack:
                            {
                                _isYouWin = false;
                                string errorLog = string.Format("{0} - {1} = {2} > {3} - {4} = {5}"
                                    , _startPlayStamp, _endPlayStamp, playTime
                                    , _startPlayServerTime, _endPlayServerTime, playTimeServer.TotalSeconds
                                );

                                OnTriggerImpossibleError(
                                      "ERROR_PLAY_TIME_HACK"
                                    , GetLocalPlayerIndex()
                                    , GameManager.Instance.Data
                                    , errorLog
                                    , false
                                );

                                // error
                                Debug.LogErrorFormat("OnEndGame: [Error] Imposible error.\n{0}", errorLog);

                                RequestLocalSurrender(false);
                                ShowEndGameResult(false);
                            }
                            break;

                        case EndGameType.Normal:
                        default:
                            {
                                ShowEndGameResult(_isYouWin);
                            }
                            break;
                    }

                    // Trigger Analytic
                    AnalyticTriggerEndMatch();
                }
            );
        }
    }

    private int GetActiveMinionCount()
    {
        int count = 0;
        if (Data != null)
        {
            foreach (PlayerIndex playerIndex in PlayerIndexList)
            {
                count += Data.PlayerDataList[(int)playerIndex].CardList[CardZone.Battlefield].Count;
            }
        }

        return count;
    }

    private void OnCheckPoke()
    {
        if (_pokeStamp != null)
        {
            if (!IsEndGame && !_isGotEndGame)
            {
                DateTime currentTime = DateTimeData.GetDateTimeUTC();

                /*
                Debug.LogFormat(
                      "OnCheckPoke [{0}, {1}]"
                    , (currentTime - _pokeStamp[0]).TotalSeconds
                    , (currentTime - _pokeStamp[1]).TotalSeconds
                );
                */

                foreach (PlayerIndex playerIndex in PlayerIndexList)
                {
                    TimeSpan duration = currentTime - _pokeStamp[(int)playerIndex];

                    if (playerIndex == GetLocalPlayerIndex())
                    {
                        if (!IsDisconnect || _endGameType == EndGameType.OtherLeftRoom)
                        {
                            // My poke
                            if (duration.TotalSeconds >= MaybeDisconnectDelay)
                            {
                                // Maybe I am disconnected.
                                Debug.Log("Maybe I am <color=red>disconnected</color>.");

                                _isDisconnect = false;
                                OnDisconnect(DisconnectCause.ClientTimeout);
                                break;
                            }
                        }
                    }
                    else
                    {
                        if (!IsDisconnect)
                        {
                            // Opponent poke
                            if (duration.TotalSeconds >= MaybeOpponentDisconnectDelay)
                            {
                                // Maybe Other am disconnected.
                                Debug.LogFormat("Maybe other <color=yellow>disconnected</color>.");

                                OnPlayerLeftRoom(null);
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

    private void OnApplicationFocus(bool isFocus)
    {
        if (!IsEndGame)
        {
            if (isFocus)
            {
                // became focus
                UpdatePassingTime();
            }
        }
    }

    private void OnApplicationPause(bool isPause)
    {
        if (!IsEndGame)
        {
            if (!isPause)
            {
                // resume
                UpdatePassingTime();
            }
        }
    }

    private void OnApplicationQuit()
    {
        Time.timeScale = 1.0f;
        ClearAllSaveKey();
        //Debug.LogWarning("App is closing");
    }

    public void OnCommmandActiveComplete()
    {
        ResetSafeToLeaveTimer();
    }

    public void OnTriggerImpossibleError(string eventKey, PlayerIndex abnormalIndex, BattleData data, string detail, bool isForceExit = true)
    {
        OnImpossibleError?.Invoke(eventKey, abnormalIndex, data, detail);

        if (isForceExit)
        {
            ForceExitGame();
        }
    }

    public void OnTriggerSafeToLeave()
    {
        // Count stat
        PopupUIManager.Instance.Show_SoftLoad(true);

        _isEndGame = true;
        _isTimer = false;
        _isYouWin = false;
        _endGameType = EndGameType.SafeToLeave;

        string errorLog = string.Format(
              "CurrentPlayer={0} SubTurn={1} Phase={2}"
            + " CmdIsReady={3}:{4} UIIsReady={5}:{6}"
            , CurrentPlayerIndex
            , SubTurn
            , Phase
            , CommandQueue.Instance.IsReady, CommandQueue.Instance.PauseCount
            , UIQueue.Instance.IsReady, UIQueue.Instance.PauseCount
        );
        Debug.LogFormat("<color=red>OnTriggerSafeToLeave</color>: {0}", errorLog);

        OnTriggerImpossibleError(
              "ERROR_SAFE_TO_LEAVE"
            , GetLocalPlayerIndex()
            , Data
            , errorLog
        );

        AnalyticTriggerEndMatch();
        Phase = GamePhase.End;

        PlayerStatManager.Instance.DoClearPlayerMatchTicket(
              DataManager.Instance.GetRoomName()
            , delegate ()
            {
                PlayerStatManager.Instance.DoEndGameResultEvent(
                      _isYouWin
                    , delegate ()
                    {
                        PopupUIManager.Instance.ShowPopup_Error(
                              LocalizationManager.Instance.GetText("ERROR_POPUP_HEADER")
                            , LocalizationManager.Instance.GetText("ERROR_SAFE_TO_LEAVE")
                            , ExitGame
                        );

                        PopupUIManager.Instance.Show_SoftLoad(false);
                    }
                );
            }
        );
    }

    // Bot Event
    public void OnShowSpiritAttackOnPlayerComplete(PlayerIndex playerIndex, int damage)
    {
        _onShowSpiritAttackOnPlayerComplete?.Invoke(playerIndex, damage);
    }

    public void OnPlayerPlayHandCardComplete(int unique)
    {
        _onPlayerPlayHandCardComplete?.Invoke(unique);
    }
    #endregion

    #region Request RPC Methods
    public PhotonView GetPhotonView()
    {
        return PhotonView.Get(this);
    }

    private void PhotonRequestRPC(int requestIndex, UnityAction request)
    {
        PhotonData data = new PhotonData(requestIndex, request);
        PhotonQueue.Instance.Add(requestIndex, data);
        Debug.LogFormat("PhotonRequestRPC: Add request. Index: {0}", requestIndex);

        if (!IsDisconnect)
        {
            Debug.LogFormat("PhotonRequestRPC: <color=green>Active index = {0}</color>", requestIndex);
            PhotonQueue.Instance.Active(requestIndex);
        }
        else
        {
            Debug.LogFormat("PhotonRequestRPC: <color=red>Can't send active index = {0}</color>", requestIndex);
        }
    }

    private void ResendRPC(int latestRequestIndex)
    {
        int nextRequestIndex = latestRequestIndex + 1;
        StartCoroutine(OnResendRPC(nextRequestIndex));
    }

    private IEnumerator OnResendRPC(int requestIndex)
    {
        //Debug.LogFormat("OnResendRPC: [Start] index: {0}", index);

        bool isCanSend = true;

        if (!PhotonQueue.Instance.ContainKey(requestIndex))
        {
            isCanSend = false;
            PhotonRequestCompleteSyncRPC(requestIndex);
        }

        while (isCanSend)
        {
            if (!IsDisconnect)
            {
                //if(CommandQueue.Instance.IsPause || CommandQueue.Instance.IsReady)
                {
                    Debug.LogFormat("OnResendRPC: Resending RPC to other {0} ...", requestIndex);
                    PhotonQueue.Instance.Active(requestIndex);
                    PhotonRequestCompleteResendRPC(requestIndex);
                    break;
                }

                // wait 2 frames.
                yield return new WaitForEndOfFrame();
                yield return new WaitForEndOfFrame();
            }
            else
            {
                PhotonRequestFailResendRPC(requestIndex);
                Debug.LogWarningFormat("OnResendRPC: Disconnected while Resending. Stop resend.");
                break;
            }
        }

        //Debug.LogFormat("OnResendRPC [End] index: {0}", index);
    }

    public void PhotonStartSyncRPC()
    {
        Debug.LogFormat("PhotonStartSyncRPC");

        /*
        PhotonView photonView = GameManager.Instance.GetPhotonView();
        if (photonView != null)
        {
            if (!_isDisconnect)
            {
                _isSyncRPC = true;
                photonView.RPC("RpcStartSyncRPC", RpcTarget.Others);
            }
        }
        */

        GameHelper.DelayCallback(1.0f, RpcStartSyncRPC);

        //RpcStartSyncRPC();
    }

    public void PhotonRequestResendRPC(int latestRequestIndex)
    {
        //Debug.LogFormat("PhotonRequestResendRPC: latestRequestIndex = {0}", latestRequestIndex);

        PhotonView photonView = GameManager.Instance.GetPhotonView();
        if (photonView != null)
        {
            if (!_isDisconnect)
            {
                photonView.RPC("RpcResendRPC", RpcTarget.Others, latestRequestIndex);
            }
        }
    }

    public void PhotonRequestCompleteResendRPC(int requestIndex)
    {
        //Debug.LogFormat("PhotonRequestCompleteResendRPC: requestIndex = {0}", requestIndex);

        PhotonView photonView = GameManager.Instance.GetPhotonView();
        if (photonView != null)
        {
            if (!_isDisconnect)
            {
                photonView.RPC("RpcCompleteResendRPC", RpcTarget.Others, requestIndex);
            }
        }
    }

    public void PhotonRequestFailResendRPC(int requestIndex)
    {
        Debug.LogFormat("PhotonRequestFailResendRPC: requestIndex = {0}", requestIndex);

        PhotonView photonView = GameManager.Instance.GetPhotonView();
        if (photonView != null)
        {
            if (!_isDisconnect)
            {
                photonView.RPC("RpcFailResendRPC", RpcTarget.Others, requestIndex);
            }
        }
    }

    public void PhotonRequestCompleteSyncRPC(int requestIndex)
    {
        //Debug.LogFormat("PhotonRequestCompleteSyncRPC: requestIndex = {0}", requestIndex);

        PhotonView photonView = GameManager.Instance.GetPhotonView();
        if (photonView != null)
        {
            if (!_isDisconnect)
            {
                photonView.RPC("RpcCompleteSyncRPC", RpcTarget.Others, requestIndex);
            }
        }
    }

    public void PhotonSyncPlayFirst(PlayerIndex playerIndex)
    {
        int requestIndex = PhotonQueue.Instance.RequestUniqueIndex();
        byte gamePhase_byte = GameHelper.ConvertGamePhaseToByte(Phase);
        byte playerIndex_byte = GameHelper.ConvertPlayerIndexToByte(playerIndex);

        PhotonRequestRPC(requestIndex, delegate ()
        {
            PhotonView photonView = GameManager.Instance.GetPhotonView();
            if (photonView != null)
            {
                photonView.RPC("RpcSyncPlayFirst", RpcTarget.Others, requestIndex, gamePhase_byte, playerIndex_byte);
            }
        });

        RpcSyncPlayFirst(-1, gamePhase_byte, playerIndex_byte);
    }

    public void PhotonRequestReHand(PlayerIndex playerIndex, List<int> uniqueIDList)
    {
        int requestIndex = PhotonQueue.Instance.RequestUniqueIndex();
        byte gamePhase_byte = GameHelper.ConvertGamePhaseToByte(Phase);
        byte playerIndex_byte = GameHelper.ConvertPlayerIndexToByte(playerIndex);

        string uniqueIDList_str = "";
        if (uniqueIDList != null && uniqueIDList.Count > 0)
        {
            foreach (int uniqueID in uniqueIDList)
            {
                if (uniqueIDList_str.Length > 0)
                {
                    uniqueIDList_str += "$" + uniqueID.ToString();
                }
                else
                {
                    uniqueIDList_str += uniqueID.ToString();
                }
            }
        }

        PhotonRequestRPC(requestIndex, delegate ()
        {
            PhotonView photonView = GameManager.Instance.GetPhotonView();
            if (photonView != null)
            {
                photonView.RPC("RpcRequestReHand", RpcTarget.Others, requestIndex, gamePhase_byte, playerIndex_byte, uniqueIDList_str);
            }
        });

        RpcRequestReHand(-1, gamePhase_byte, playerIndex_byte, uniqueIDList_str);
    }

    public void PhotonRequestUseCard(int uniqueID, int slotID, bool isAddFirst)
    {
        int requestIndex = PhotonQueue.Instance.RequestUniqueIndex();
        byte gamePhase_byte = GameHelper.ConvertGamePhaseToByte(Phase);
        byte slotID_byte = GameHelper.ConvertIntToByte(slotID);

        PhotonRequestRPC(requestIndex, delegate ()
        {
            PhotonView photonView = GameManager.Instance.GetPhotonView();
            if (photonView != null)
            {
                photonView.RPC("RpcRequestUseCard", RpcTarget.Others, requestIndex, gamePhase_byte, uniqueID, slotID_byte, isAddFirst);
            }
        });

        RpcRequestUseCard(-1, gamePhase_byte, uniqueID, slotID_byte, isAddFirst);
    }

    public void PhotonRequestActionAbility(int uniqueCardID, int uniqueAbiliyID, int ramdomSeed, Dictionary<string, string> paramList)
    {
        int requestIndex = PhotonQueue.Instance.RequestUniqueIndex();
        byte gamePhase_byte = GameHelper.ConvertGamePhaseToByte(Phase);
        string paramList_str = "";

        foreach (KeyValuePair<string, string> para in paramList)
        {
            if (paramList_str.Length > 0)
            {
                paramList_str += string.Format("${0}฿{1}", para.Key, para.Value);
            }
            else
            {
                paramList_str = string.Format("{0}฿{1}", para.Key, para.Value);
            }
        }

        PhotonRequestRPC(requestIndex, delegate ()
        {
            PhotonView photonView = GameManager.Instance.GetPhotonView();
            if (photonView != null)
            {
                photonView.RPC("RpcRequestActionAbility", RpcTarget.Others, requestIndex, gamePhase_byte, uniqueCardID, uniqueAbiliyID, ramdomSeed, paramList_str);
            }
        });

        RpcRequestActionAbility(-1, gamePhase_byte, uniqueCardID, uniqueAbiliyID, ramdomSeed, paramList_str);
    }

    public void PhotonRequestPrepareFailCommand()
    {
        int requestIndex = PhotonQueue.Instance.RequestUniqueIndex();
        byte gamePhase_byte = GameHelper.ConvertGamePhaseToByte(Phase);

        PhotonRequestRPC(requestIndex, delegate ()
        {
            PhotonView photonView = GameManager.Instance.GetPhotonView();
            if (photonView != null)
            {
                photonView.RPC("RpcPrepareFailCommand", RpcTarget.Others, requestIndex, gamePhase_byte);
            }
        });

        RpcPrepareFailCommand(-1, gamePhase_byte);
    }

    public void PhotonRequestEndTurn(PlayerIndex playerIndex, int subTurn, bool isActive)
    {
        int requestIndex = PhotonQueue.Instance.RequestUniqueIndex();
        byte gamePhase_byte = GameHelper.ConvertGamePhaseToByte(Phase);
        byte playerIndex_byte = GameHelper.ConvertPlayerIndexToByte(playerIndex);

        PhotonRequestRPC(requestIndex, delegate ()
        {
            PhotonView photonView = GameManager.Instance.GetPhotonView();
            if (photonView != null)
            {
                photonView.RPC("RpcRequestEndTurn", RpcTarget.Others, requestIndex, gamePhase_byte, playerIndex_byte, subTurn, isActive);
            }
        });

        RpcRequestEndTurn(-1, gamePhase_byte, playerIndex_byte, subTurn, isActive);
    }

    public void PhotonRequestPauseCommand(bool isPause)
    {
        int requestIndex = PhotonQueue.Instance.RequestUniqueIndex();
        byte gamePhase_byte = GameHelper.ConvertGamePhaseToByte(Phase);

        PhotonRequestRPC(requestIndex, delegate ()
        {
            PhotonView photonView = GameManager.Instance.GetPhotonView();
            if (photonView != null)
            {
                photonView.RPC("RpcRequestPauseCommand", RpcTarget.Others, requestIndex, gamePhase_byte, isPause);
            }
        });

        RpcRequestPauseCommand(-1, gamePhase_byte, isPause);
    }

    public void PhotonRequestPhaseReady(GamePhase phase, PlayerIndex playerIndex, int turnIndex)
    {
        int requestIndex = PhotonQueue.Instance.RequestUniqueIndex();
        byte gamePhase_byte = GameHelper.ConvertGamePhaseToByte(Phase);
        byte phaseIndex_byte = GameHelper.ConvertIntToByte((int)phase);
        byte playerIndex_byte = GameHelper.ConvertPlayerIndexToByte(playerIndex);

        PhotonRequestRPC(requestIndex, delegate ()
        {
            PhotonView photonView = GameManager.Instance.GetPhotonView();
            if (photonView != null)
            {
                photonView.RPC("RpcSetPhaseReady", RpcTarget.Others, requestIndex, gamePhase_byte, phaseIndex_byte, playerIndex_byte, turnIndex); // send to other
            }
        });

        RpcSetPhaseReady(-1, gamePhase_byte, phaseIndex_byte, playerIndex_byte, turnIndex); // call myself
    }

    public void PhotonRequestHoverCard(int uniqueID, bool isSelect)
    {
        byte gamePhase_byte = GameHelper.ConvertGamePhaseToByte(Phase);

        PhotonView photonView = GameManager.Instance.GetPhotonView();
        if (photonView != null)
        {
            if (!_isDisconnect)
            {
                photonView.RPC("RpcHoverCard", RpcTarget.Others, -1, gamePhase_byte, uniqueID, isSelect); // send to other
            }
        }
    }

    public void PhotonRequestShowEffectActivating(int uniqueID, bool isShow)
    {
        int requestIndex = PhotonQueue.Instance.RequestUniqueIndex();
        byte gamePhase_byte = GameHelper.ConvertGamePhaseToByte(Phase);

        PhotonRequestRPC(requestIndex, delegate ()
        {
            PhotonView photonView = GameManager.Instance.GetPhotonView();
            if (photonView != null)
            {
                photonView.RPC("RpcShowEffectActivateCard", RpcTarget.Others, requestIndex, gamePhase_byte, uniqueID, isShow);
            }
        });

        RpcShowEffectActivateCard(-1, gamePhase_byte, uniqueID, isShow);
    }

    public void PhotonRequestShowPreUseCard(int uniqueID, bool isShow)
    {
        byte gamePhase_byte = GameHelper.ConvertGamePhaseToByte(Phase);

        PhotonView photonView = GameManager.Instance.GetPhotonView();
        if (photonView != null)
        {
            if (!_isDisconnect)
            {
                photonView.RPC("RpcShowPreUseCard", RpcTarget.Others, -1, gamePhase_byte, uniqueID, isShow); // send to other
            }
        }
    }

    public void PhotonSyncTimer(float timer)
    {
        int requestIndex = PhotonQueue.Instance.RequestUniqueIndex();
        byte gamePhase_byte = GameHelper.ConvertGamePhaseToByte(Phase);

        PhotonRequestRPC(requestIndex, delegate ()
        {
            PhotonView photonView = GameManager.Instance.GetPhotonView();
            if (photonView != null)
            {
                photonView.RPC("RpcSyncTimer", RpcTarget.Others, requestIndex, gamePhase_byte, timer);
            }
        });

        RpcSyncTimer(-1, gamePhase_byte, timer);
    }

    public void PhotonRequestSendMessage(PlayerIndex playerIndex, HeroVoice heroVoiceIndex)
    {
        byte gamePhase_byte = GameHelper.ConvertGamePhaseToByte(Phase);
        byte playerIndex_byte = GameHelper.ConvertPlayerIndexToByte(playerIndex);
        byte heroVoiceIndex_byte = GameHelper.ConvertIntToByte((int)heroVoiceIndex);

        PhotonView photonView = GameManager.Instance.GetPhotonView();
        if (photonView != null)
        {
            if (!_isDisconnect)
            {
                photonView.RPC("RpcSendMessage", RpcTarget.Others, -1, gamePhase_byte, playerIndex_byte, heroVoiceIndex_byte); // send to other
            }
        }
        RpcSendMessage(-1, gamePhase_byte, playerIndex_byte, heroVoiceIndex_byte);
    }

    public void PhotonRequestShowWaitingOpponent(bool isShow)
    {
        byte gamePhase_byte = GameHelper.ConvertGamePhaseToByte(Phase);

        PhotonView photonView = GameManager.Instance.GetPhotonView();
        if (photonView != null)
        {
            if (!_isDisconnect)
            {
                photonView.RPC("RpcShowWaiting", RpcTarget.Others, -1, gamePhase_byte, isShow); // send to other
            }
        }
    }

    public void PhotonRequestSurrender(PlayerIndex playerIndex)
    {
        int requestIndex = PhotonQueue.Instance.RequestUniqueIndex();
        byte gamePhase_byte = GameHelper.ConvertGamePhaseToByte(Phase);
        byte playerIndex_byte = GameHelper.ConvertPlayerIndexToByte(playerIndex);

        PhotonRequestRPC(requestIndex, delegate ()
        {
            PhotonView photonView = GameManager.Instance.GetPhotonView();
            if (photonView != null)
            {
                photonView.RPC("RpcSurrender", RpcTarget.Others, requestIndex, gamePhase_byte, playerIndex_byte);
            }
        });
    }

    public void PhotonRequestEndGame(PlayerIndex playerIndex, EndGameType endGameType)
    {
        byte gamePhase_byte = GameHelper.ConvertGamePhaseToByte(Phase);
        string matchName = DataManager.Instance.GetRoomName();
        byte playerIndex_byte = GameHelper.ConvertPlayerIndexToByte(playerIndex);
        byte endGameType_byte = GameHelper.ConvertIntToByte((int)endGameType);

        PhotonView photonView = GameManager.Instance.GetPhotonView();
        if (photonView != null)
        {
            photonView.RPC("RpcEndGame", RpcTarget.AllBufferedViaServer, -1, gamePhase_byte, matchName, playerIndex_byte, endGameType_byte);
        }

        //RpcEndGame(-1, gamePhase_byte, playerIndex_byte, endGameType_byte);
    }

    public void PhotonRequestAddPlayerHP(PlayerIndex playerIndex, int hp)
    {
        int requestIndex = PhotonQueue.Instance.RequestUniqueIndex();
        byte gamePhase_byte = GameHelper.ConvertGamePhaseToByte(Phase);

        PhotonRequestRPC(requestIndex, delegate ()
        {
            PhotonView photonView = GameManager.Instance.GetPhotonView();
            if (photonView != null)
            {
                photonView.RPC("RpcRequestAddPlayerHP", RpcTarget.Others, requestIndex, gamePhase_byte, (int)playerIndex, hp);
            }
        });

        RpcRequestAddPlayerHP(-1, gamePhase_byte, (int)playerIndex, hp);
    }

    public void PhotonRequestAddPlayerAP(PlayerIndex playerIndex, int ap)
    {
        int requestIndex = PhotonQueue.Instance.RequestUniqueIndex();
        byte gamePhase_byte = GameHelper.ConvertGamePhaseToByte(Phase);

        PhotonRequestRPC(requestIndex, delegate ()
        {
            PhotonView photonView = GameManager.Instance.GetPhotonView();
            if (photonView != null)
            {
                photonView.RPC("RpcRequestAddPlayerAP", RpcTarget.Others, requestIndex, gamePhase_byte, (int)playerIndex, ap);
            }
        });

        RpcRequestAddPlayerAP(-1, gamePhase_byte, (int)playerIndex, ap);
    }

    public void PhotonRequestCreateCardToHand(PlayerIndex playerIndex, string cardId)
    {
        int requestIndex = PhotonQueue.Instance.RequestUniqueIndex();
        byte gamePhase_byte = GameHelper.ConvertGamePhaseToByte(Phase);

        PhotonRequestRPC(requestIndex, delegate ()
        {
            PhotonView photonView = GameManager.Instance.GetPhotonView();
            if (photonView != null)
            {
                photonView.RPC("RpcRequestCreateCardToHand", RpcTarget.Others, requestIndex, gamePhase_byte, (int)playerIndex, cardId);
            }
        });

        RpcRequestCreateCardToHand(-1, gamePhase_byte, (int)playerIndex, cardId);
    }

    public void PhotonRequestRemoveDeck(PlayerIndex playerIndex)
    {
        int requestIndex = PhotonQueue.Instance.RequestUniqueIndex();
        byte gamePhase_byte = GameHelper.ConvertGamePhaseToByte(Phase);

        PhotonRequestRPC(requestIndex, delegate ()
        {
            PhotonView photonView = GameManager.Instance.GetPhotonView();
            if (photonView != null)
            {
                photonView.RPC("RpcRequestRemoveDeck", RpcTarget.Others, requestIndex, gamePhase_byte, (int)playerIndex);
            }
        });

        RpcRequestRemoveDeck(-1, gamePhase_byte, (int)playerIndex);
    }

    private void PhotonRequestPoke()
    {
        if (!_isDisconnect || _endGameType == EndGameType.OtherLeftRoom)
        {
            PlayerIndex playerIndex = GetLocalPlayerIndex();
            byte playerIndex_byte = GameHelper.ConvertPlayerIndexToByte(playerIndex);

            PhotonView photonView = GameManager.Instance.GetPhotonView();
            if (photonView != null)
            {
                photonView.RPC("RpcRequestPoke", RpcTarget.AllViaServer, playerIndex_byte);
            }
        }
    }

    private bool IsRequestIndexValid(int requestIndex, GamePhase gamePhase, bool ignoreGamePhase = false)
    {
        if (IsNetworkPlay())
        {
            if (requestIndex < 0)
            {
                //Debug.LogFormat("<color=orange>Accepted request #{0}({1})</color>", requestIndex, gamePhase);

                return true;
            }
            else if (requestIndex == _latestReceivedRequestIndex + 1)
            {
                if (gamePhase == Phase || ignoreGamePhase)
                {
                    Debug.LogFormat("<color=orange>Accepted request #{0}({1})</color>", requestIndex, gamePhase);

                    _isRejectCmd = false;
                    _latestReceivedRequestIndex = requestIndex;
                    return true;
                }
                else
                {
                    Debug.LogFormat("<color=yellow>Rejected request #{0}({1}) Invalid phase: Current phase = {2}</color>", requestIndex, gamePhase, Phase);

                    if (!_isRejectCmd)
                    {
                        _isRejectCmd = true;

                        PhotonStartSyncRPC();
                        return false;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else
            {
                Debug.LogFormat("<color=red>Rejected request #{0}({1}). Invalid index: Last valid index = {2}</color>", requestIndex, gamePhase, _latestReceivedRequestIndex);

                PhotonStartSyncRPC();
                return false;
            }
        }
        else
        {
            return true;
        }
    }

    [PunRPC]
    public void RpcSyncPlayFirst(int requestIndex, byte gamePhase_byte, byte playerIndex_byte)
    {
        GamePhase phase = GameHelper.ConvertByteToGamePhase(gamePhase_byte);

        if (IsRequestIndexValid(requestIndex, phase, true))
        {
            _currentPlayer = GameHelper.ConvertByteToPlayerIndex(playerIndex_byte);
            _isPlayFirstReady = true;

            if (IsLocalPlayer(_currentPlayer))
            {
                _isMePlayFirst = true;
            }
            else
            {
                _isMePlayFirst = false;
            }
        }
    }

    [PunRPC]
    public void RpcRequestReHand(int requestIndex, byte gamePhase_byte, byte playerIndex_byte, string uniqueIDList_str)
    {
        GamePhase phase = GameHelper.ConvertByteToGamePhase(gamePhase_byte);

        if (IsRequestIndexValid(requestIndex, phase))
        {
            PlayerIndex playerIndex = GameHelper.ConvertByteToPlayerIndex(playerIndex_byte);

            List<int> uniqueIDList = new List<int>();
            if (uniqueIDList_str != null && uniqueIDList_str.Length > 0)
            {
                string[] uids = uniqueIDList_str.Split('$');
                foreach (string uid in uids)
                {
                    uniqueIDList.Add(int.Parse(uid));
                }
            }

            _OnSelectReHand(playerIndex, uniqueIDList);
        }
    }

    [PunRPC]
    public void RpcRequestUseCard(int requestIndex, byte gamePhase_byte, int uniqueID, byte slotID_byte, bool isAddFirst)
    {
        GamePhase phase = GameHelper.ConvertByteToGamePhase(gamePhase_byte);

        if (IsRequestIndexValid(requestIndex, phase, true))
        {
            if (_useCardCommandList == null)
            {
                _useCardCommandList = new Queue<CommandData>();
            }

            int slotID = GameHelper.ConvertByteToInt(slotID_byte);

            BattleCardData battleCard;
            if (FindBattleCard(uniqueID, out battleCard))
            {
                int usedAP = 0;

                switch (battleCard.Type)
                {
                    case CardType.Minion:
                    case CardType.Minion_NotInDeck:
                        {
                            MinionData minion = battleCard as MinionData;

                            //if (IsCanSpawnToken(slotID, battleCard.Owner))
                            {
                                usedAP = minion.SpiritCurrent;

                                if (Data.PlayerDataList[(int)minion.Owner].CurrentAP >= usedAP)
                                {
                                    SpawnTokenCommand cmd = new SpawnTokenCommand(minion, slotID, true);
                                    _useCardCommandList.Enqueue(cmd);

                                    _activeCount++;

                                    if (GetLocalPlayerIndex() == CurrentPlayerIndex)
                                    {
                                        UIManager.Instance.EndButtonIsPass(false); // yellow
                                    }

                                    // Give turn time
                                    if (_timer < ActionTime)
                                    {
                                        if (IsNetworkPlay())
                                        {
                                            PhotonSyncTimer(ActionTime);
                                        }
                                        else
                                        {
                                            RpcSyncTimer(-1, GameHelper.ConvertGamePhaseToByte(Phase), ActionTime);
                                        }
                                    }
                                }
                                else
                                {
                                    Debug.LogError("ERROR_AP_EXCEED");

                                    //OnTriggerImpossibleError(
                                    //    "ERROR_AP_EXCEED"
                                    //    , minion.Owner
                                    //    , Data
                                    //    , string.Format("{0} >= {1}", Data.PlayerDataList[(int)minion.Owner].CurrentAP, usedAP)
                                    //);
                                    //return;
                                }
                            }
                        }
                        break;

                        /*
                        case CardType.Spell:
                        case CardType.Spell_NotInDeck:
                        {
                            SpellData spell = battleCard as SpellData;

                            //if (spell.IsCanUse)
                            {
                                ActivateSpellCommand cmd = new ActivateSpellCommand(spell);
                                _useCardCommandList.Enqueue(cmd);

                                _activeCount++;
                                AddPlayCardCount(1);

                                // Give turn time
                                if (_timer < ActionTime)
                                {
                                    if (IsNetworkPlay())
                                    {
                                        PhotonSyncTimer(ActionTime);
                                    }
                                    else
                                    {
                                        RpcSyncTimer(-1, ActionTime);
                                    }
                                }
                            }
                        }
                        break;
                        */
                }

                AddPlayerAPCommand cmd2 = new AddPlayerAPCommand(battleCard.Owner, -usedAP);
                _useCardCommandList.Enqueue(cmd2);
                OnUseCard?.Invoke(battleCard);
            }
            else
            {
                Debug.LogErrorFormat("RpcRequestUseCard: [ERROR] not found battle card. {0}", uniqueID);
            }

            ActionGetUseCard();
        }
    }

    [PunRPC]
    public void RpcRequestActionAbility(int requestIndex, byte gamePhase_byte, int uniqueCardID, int uniqueAbiliyID, int randomSeed, string paramArray_str)
    {
        GamePhase phase = GameHelper.ConvertByteToGamePhase(gamePhase_byte);

        if (IsRequestIndexValid(requestIndex, phase, true))
        {
            Dictionary<string, string> paramList = new Dictionary<string, string>();
            Debug.LogFormat("RpcRequestActionAbility: paramArray_str={0}", paramArray_str);

            if (paramArray_str != null && paramArray_str.Length > 0)
            {
                string[] ps = paramArray_str.Split('$');
                string param_key;
                string param_value;
                foreach (string p in ps)
                {
                    string[] texts = p.Split('฿');
                    param_key = texts[0];
                    param_value = texts[1];
                    paramList.Add(param_key, param_value);
                }
            }

            string logText = "";
            foreach (KeyValuePair<string, string> param in paramList)
            {
                logText += string.Format("[{0}:{1}]", param.Key, param.Value);
            }

            Debug.LogFormat("<color=yellow>Got</color> RpcRequestActionAbility : {0} {1} param: {2}"
                , uniqueCardID
                , uniqueAbiliyID
                , logText
            );

            if (_actionAbilityCommandList == null)
            {
                _actionAbilityCommandList = new List<CommandData>();
            }

            ActionAbilityCommand cmd = new ActionAbilityCommand(uniqueCardID, uniqueAbiliyID, paramList, randomSeed);
            _actionAbilityCommandList.Add(cmd);

            ActionGetActionAbility();
        }
    }

    [PunRPC]
    public void RpcPrepareFailCommand(int requestIndex, byte gamePhase_byte)
    {
        GamePhase phase = GameHelper.ConvertByteToGamePhase(gamePhase_byte);

        if (IsRequestIndexValid(requestIndex, phase, true))
        {
            if (_actionAbilityCommandList == null)
            {
                _actionAbilityCommandList = new List<CommandData>();
            }

            PauseCommand cmd = new PauseCommand(false);
            _actionAbilityCommandList.Add(cmd);

            ActionGetActionAbility();
        }
    }

    [PunRPC]
    public void RpcRequestEndTurn(int requestIndex, byte gamePhase_byte, byte playerIndex_byte, int subTurn, bool isActive)
    {
        GamePhase phase = GameHelper.ConvertByteToGamePhase(gamePhase_byte);

        if (IsRequestIndexValid(requestIndex, phase, true))
        {
            PlayerIndex playerIndex = GameHelper.ConvertByteToPlayerIndex(playerIndex_byte);

            RequestEndTurn(playerIndex, subTurn, isActive);
        }
    }

    [PunRPC]
    public void RpcRequestPauseCommand(int requestIndex, byte gamePhase_byte, bool isWait)
    {
        GamePhase phase = GameHelper.ConvertByteToGamePhase(gamePhase_byte);

        if (IsRequestIndexValid(requestIndex, phase, true))
        {
            CommandQueue.Instance.SetPause(isWait);

            if (!isWait)
            {
                UIManager.Instance.HideWaiting();
            }
        }
    }

    [PunRPC]
    public void RpcSetPhaseReady(int requestIndex, byte gamePhase_byte, byte phaseIndex_byte, byte playerIndex_byte, int turnIndex)
    {
        GamePhase phase = GameHelper.ConvertByteToGamePhase(gamePhase_byte);

        if (IsRequestIndexValid(requestIndex, phase, true))
        {
            PlayerIndex playerIndex = GameHelper.ConvertByteToPlayerIndex(playerIndex_byte);
            int phaseIndex = GameHelper.ConvertByteToInt(phaseIndex_byte);

            _phaseReadyIndex[phaseIndex, (int)playerIndex] = turnIndex;
        }
    }

    [PunRPC]
    public void RpcHoverCard(int requestIndex, byte gamePhase_byte, int uniqueID, bool isSelect)
    {
        GamePhase phase = GameHelper.ConvertByteToGamePhase(gamePhase_byte);

        if (IsRequestIndexValid(requestIndex, phase, true))
        {
            UIManager.Instance.ReceivePlayerHoverCard(uniqueID, isSelect);
        }
    }

    [PunRPC]
    public void RpcShowEffectActivateCard(int requestIndex, byte gamePhase_byte, int uniqueID, bool isShow)
    {
        GamePhase phase = GameHelper.ConvertByteToGamePhase(gamePhase_byte);

        if (IsRequestIndexValid(requestIndex, phase, true))
        {
            if (isShow)
            {
                UIManager.Instance.ShowEffectAcitvating(uniqueID, null);
            }
            else
            {
                UIManager.Instance.HideEffectAcitvating(uniqueID, null);
            }
        }
    }

    [PunRPC]
    public void RpcShowPreUseCard(int requestIndex, byte gamePhase_byte, int uniqueID, bool isShow)
    {
        GamePhase phase = GameHelper.ConvertByteToGamePhase(gamePhase_byte);

        if (IsRequestIndexValid(requestIndex, phase, true))
        {
            UIManager.Instance.ShowMoveToCardPreUse(uniqueID, isShow, null);
        }
    }

    [PunRPC]
    public void RpcSyncTimer(int requestIndex, byte gamePhase_byte, float timer)
    {
        GamePhase phase = GameHelper.ConvertByteToGamePhase(gamePhase_byte);

        if (IsRequestIndexValid(requestIndex, phase, true))
        {
            _timer = timer;
            if (_timer > 0.0f && !IsEndGame)
            {
                _isTimer = true;
            }
        }
    }

    [PunRPC]
    public void RpcSendMessage(int requestIndex, byte gamePhase_byte, byte playerIndex_byte, byte heroVoiceIndex_byte)
    {
        GamePhase phase = GameHelper.ConvertByteToGamePhase(gamePhase_byte);

        if (IsRequestIndexValid(requestIndex, phase, true))
        {
            PlayerIndex playerIndex = GameHelper.ConvertByteToPlayerIndex(playerIndex_byte);
            HeroVoice heroVoiceIndex = (HeroVoice)GameHelper.ConvertByteToInt(heroVoiceIndex_byte);

            if (playerIndex == GetLocalPlayerIndex())
            {
                ActionSendMessage(playerIndex, heroVoiceIndex, null);
            }
            else
            {
                if (!_isMessageMute)
                {
                    ActionSendMessage(playerIndex, heroVoiceIndex, null);
                }
            }
        }
    }

    [PunRPC]
    public void RpcShowWaiting(int requestIndex, byte gamePhase_byte, bool isShow)
    {
        GamePhase phase = (GamePhase)gamePhase_byte;

        if (IsRequestIndexValid(requestIndex, phase, true))
        {
            ActionShowWaiting(isShow, null);
        }
    }

    [PunRPC]
    public void RpcSurrender(int requestIndex, byte gamePhase_byte, byte playerIndex_byte)
    {
        GamePhase phase = (GamePhase)gamePhase_byte;

        if (IsRequestIndexValid(requestIndex, phase, true))
        {
            if (!_isEndGame && !_isGotEndGame)
            {
                PlayerIndex playerIndex = GameHelper.ConvertByteToPlayerIndex(playerIndex_byte);

                if (!_isResign && _endGameType != EndGameType.Surrender)
                {
                    _isResign = true;
                    _isTimer = false;

                    Debug.LogFormat("RpcSurrender: Got surrender event. {0} : _endGameType={1}", playerIndex, _endGameType);

                    if (GetLocalPlayerIndex() != playerIndex)
                    {
                        _endGameType = EndGameType.OtherSurrender;
                    }

                    OnEndGame();
                }
                else
                {
                    // already got surrender
                    Debug.LogWarningFormat("RpcSurrender: Already got surrender event. {0} : _endGameType={1}", playerIndex, _endGameType);
                }
            }
        }
    }

    [PunRPC]
    public void RpcEndGame(int requestIndex, byte gamePhase_byte, string matchName, byte playerIndex_byte, byte endGameType_byte)
    {
        GamePhase phase = (GamePhase)gamePhase_byte;

        if (
            IsRequestIndexValid(requestIndex, phase, true)
            && matchName == DataManager.Instance.GetRoomName()
        )
        {
            // First player that got end game.
            PlayerIndex losePlayerIndex = GameHelper.ConvertByteToPlayerIndex(playerIndex_byte);
            EndGameType endGameType = (EndGameType)GameHelper.ConvertByteToInt(endGameType_byte);
            bool isMeLose = (losePlayerIndex == GetLocalPlayerIndex());

            if (!_isGotEndGame)
            {
                Debug.LogFormat("RpcEndGame : Got lose player {0} : {1}", losePlayerIndex, endGameType);

                if (!_isEndGame)
                {
                    _isGotEndGame = true;

                    switch (endGameType)
                    {
                        case EndGameType.Disconnect:
                            {
                                if (isMeLose)
                                {
                                    // I am disconnected.
                                    ShowRedHint(LocalizationManager.Instance.GetText("BATTLE_FAIL_RECONNECT"));
                                    OnEndGame();
                                }
                                else
                                {
                                    // Disconnect from other, Opponent leave room.
                                    _endGameType = EndGameType.OtherLeftRoom;
                                    ShowRedHint(LocalizationManager.Instance.GetText("BATTLE_OPPONENT_LEFTED"));
                                    OnEndGame();
                                }
                            }
                            break;

                        case EndGameType.Surrender:
                            {
                                if (isMeLose)
                                {
                                    // I am surrender.
                                    OnEndGame();
                                }
                                else
                                {
                                    // Opponent surrender.
                                    _endGameType = EndGameType.OtherSurrender;
                                    ShowRedHint(LocalizationManager.Instance.GetText("BATTLE_GAME_OTHER_SURRENDER"));
                                    OnEndGame();
                                }
                            }
                            break;

                        case EndGameType.OtherLeftRoom:
                            {
                                if (isMeLose)
                                {
                                    // Leave room by me, I am disconnected.
                                    _endGameType = EndGameType.Disconnect;
                                    ShowRedHint(LocalizationManager.Instance.GetText("BATTLE_FAIL_RECONNECT"));
                                    OnEndGame();
                                }
                                else
                                {
                                    // Leave room by other, Opponent leave room.
                                    ShowRedHint(LocalizationManager.Instance.GetText("BATTLE_OPPONENT_LEFTED"));
                                    OnEndGame();
                                }
                            }
                            break;

                        case EndGameType.OtherSurrender:
                            {
                                if (isMeLose)
                                {
                                    // I got opponent surrender, Opponent surrender.
                                    ShowRedHint(LocalizationManager.Instance.GetText("BATTLE_GAME_OTHER_SURRENDER"));
                                    OnEndGame();
                                }
                                else
                                {
                                    // Opponent got opponent surrender, I am surrender.
                                    OnEndGame();
                                }
                            }
                            break;

                        case EndGameType.Normal:
                            {
                                if (isMeLose)
                                {
                                    // I lose.
                                }
                                else
                                {
                                    // I win.
                                }
                            }
                            break;
                    }
                }
            }
        }
    }

    [PunRPC]
    public void RpcStartSyncRPC()
    {
        Debug.LogFormat("RpcStartSyncRPC");
        RequestResendRPC();
    }

    [PunRPC]
    public void RpcResendRPC(int latestRequestIndex)
    {
        // other request rpc 
        Debug.LogFormat("RpcResendRPC: Other request RPC. latestRequestIndex = {0}", latestRequestIndex);
        ResendRPC(latestRequestIndex);
    }

    [PunRPC]
    public void RpcCompleteResendRPC(int completeIndex)
    {
        //Debug.LogFormat("RpcCompleteResendRPC: completeIndex = {0}", completeIndex);
        // Request Next.
        RequestResendRPC();
    }

    [PunRPC]
    public void RpcRequestAddPlayerHP(int requestIndex, byte gamePhase_byte, int playerIndexIndex, int hp)
    {
        GamePhase phase = (GamePhase)gamePhase_byte;

        if (IsRequestIndexValid(requestIndex, phase, true))
        {
            PlayerIndex playerIndex = (PlayerIndex)playerIndexIndex;

            List<CommandData> commandList = new List<CommandData>();
            RestoreLifePointDeltaCommand cmd = new RestoreLifePointDeltaCommand(playerIndex, hp);
            commandList.Add(cmd);

            ActionAddCommandListToSecondQueue(commandList, true);
        }
    }

    [PunRPC]
    public void RpcRequestAddPlayerAP(int requestIndex, byte gamePhase_byte, int playerIndexIndex, int ap)
    {
        GamePhase phase = (GamePhase)gamePhase_byte;

        if (IsRequestIndexValid(requestIndex, phase, true))
        {
            PlayerIndex playerIndex = (PlayerIndex)playerIndexIndex;

            List<CommandData> commandList = new List<CommandData>();
            AddPlayerAPCommand cmd = new AddPlayerAPCommand(playerIndex, ap);
            commandList.Add(cmd);

            ActionAddCommandListToSecondQueue(commandList, true);
        }
    }

    [PunRPC]
    public void RpcRequestCreateCardToHand(int requestIndex, byte gamePhase_byte, int playerIndexIndex, string cardID)
    {
        GamePhase phase = (GamePhase)gamePhase_byte;

        if (IsRequestIndexValid(requestIndex, phase, true))
        {
            PlayerIndex playerIndex = (PlayerIndex)playerIndexIndex;

            List<CommandData> commandList = new List<CommandData>();
            CreateCardToZoneCommand cmd = new CreateCardToZoneCommand(cardID, playerIndex, CardZone.Hand, -1, null);
            commandList.Add(cmd);

            ActionAddCommandListToSecondQueue(commandList, true);
        }
    }

    [PunRPC]
    public void RpcRequestRemoveDeck(int requestIndex, byte gamePhase_byte, int playerIndexIndex)
    {
        GamePhase phase = (GamePhase)gamePhase_byte;

        if (IsRequestIndexValid(requestIndex, phase, true))
        {
            PlayerIndex playerIndex = (PlayerIndex)playerIndexIndex;

            List<CommandData> commandList = new List<CommandData>();
            RemoveDeckCommand cmd = new RemoveDeckCommand(playerIndex, null);
            commandList.Add(cmd);

            ActionAddCommandListToSecondQueue(commandList, true);
        }
    }

    [PunRPC]
    public void RpcRequestPoke(byte playerIndex_byte)
    {
        if (_pokeStamp == null)
        {
            InitPokeStamp();
        }

        PlayerIndex playerIndex = GameHelper.ConvertByteToPlayerIndex(playerIndex_byte);
        DateTime currentTime = DateTimeData.GetDateTimeUTC();
        _pokeStamp[playerIndex_byte] = currentTime;

        //Debug.LogFormat("Got RpcRequestPoke {0}", playerIndex_byte);

        if (_isDisconnect)
        {
            switch (_endGameType)
            {
                case EndGameType.Disconnect:
                    {
                        // I am disconnect.

                        if (playerIndex == GetLocalPlayerIndex())
                        {
                            OnJoinedRoom();
                        }
                    }
                    break;

                case EndGameType.OtherLeftRoom:
                    {
                        // Other is disconnect.

                        if (playerIndex != GetLocalPlayerIndex())
                        {
                            OnPlayerEnteredRoom(null);
                        }
                    }
                    break;
            }
        }
    }

    private void RequestResendRPC()
    {
        _isSyncRPC = true;

        if (_requestResendRPCCoroutine != null)
        {
            StopCoroutine(_requestResendRPCCoroutine);
            _requestResendRPCCoroutine = null;
        }

        _requestResendRPCCoroutine = StartCoroutine(OnRequestResendRPC());
    }

    private IEnumerator OnRequestResendRPC()
    {
        //Debug.LogFormat("OnRequestResendRPC");
        while (true)
        {
            if (!IsDisconnect)
            {
                if (CommandQueue.Instance.IsReady || CommandQueue.Instance.IsPause)
                {
                    Debug.LogFormat("OnRequestResendRPC: PhotonRequestResendRPC  MY latestRequestIndex = {0}", _latestReceivedRequestIndex);
                    PhotonRequestResendRPC(_latestReceivedRequestIndex);
                    break;
                }

                // wait 2 frames.
                yield return new WaitForEndOfFrame();
                yield return new WaitForEndOfFrame();
            }
            else
            {
                _isSyncRPC = false;
                Debug.LogWarningFormat("OnRequestResendRPC: Disconnected while wait resend. Stop!");
                break;
            }
        }
    }

    [PunRPC]
    public void RpcFailResendRPC(int failIndex)
    {
        Debug.LogFormat("RpcFailResendRPC: error = {0}", failIndex);
        _isSyncRPC = false;
    }

    [PunRPC]
    public void RpcCompleteSyncRPC(int completeIndex)
    {
        Debug.LogFormat("RpcCompleteSyncRPC: completeIndex = {0}", completeIndex);
        _isSyncRPC = false;
    }
    #endregion

    #region Request Methods
    public void RequestReHand(PlayerIndex playerIndex, List<int> uniqueIDList, bool isAddFirst = false)
    {
        ReHandCommand cmd = new ReHandCommand(playerIndex, uniqueIDList);

        if (isAddFirst)
        {
            CommandQueue.Instance.AddFirst(cmd);
        }
        else
        {
            CommandQueue.Instance.Add(cmd);
        }
    }

    public void RequestShuffleDeck(PlayerIndex playerIndex, bool isAddFirst = false)
    {
        ShuffleDeckCommand cmd = new ShuffleDeckCommand(playerIndex);

        if (isAddFirst)
        {
            CommandQueue.Instance.AddFirst(cmd);
        }
        else
        {
            CommandQueue.Instance.Add(cmd);
        }
    }

    public void RequestDrawCard(PlayerIndex playerIndex, int num, bool isUseAP = false, bool isAddFirst = false)
    {
        DrawCardCommand cmd = new DrawCardCommand(playerIndex, num, isUseAP);
        if (isAddFirst)
        {
            CommandQueue.Instance.AddFirst(cmd);
        }
        else
        {
            CommandQueue.Instance.Add(cmd);
        }
    }

    public void RequestDiscardDeck(PlayerIndex playerIndex, int num, bool isAddFirst = false)
    {
        DiscardDeckCommand cmd = new DiscardDeckCommand(playerIndex, num);

        if (isAddFirst)
        {
            CommandQueue.Instance.AddFirst(cmd);
        }
        else
        {
            CommandQueue.Instance.Add(cmd);
        }
    }

    public void RequestDiscardHand(PlayerIndex playerIndex, List<int> uniqueCardIDList, bool isAddFirst = false)
    {
        DiscardHandCommand cmd = new DiscardHandCommand(playerIndex, uniqueCardIDList);

        if (isAddFirst)
        {
            CommandQueue.Instance.AddFirst(cmd);
        }
        else
        {
            CommandQueue.Instance.Add(cmd);
        }
    }

    public void RequestBoostPlayerAP(Requester requester, PlayerIndex playerIndex, int num, DataParam vfxParam, bool isAddFirst = false)
    {
        BoostPlayerAPCommand cmd = new BoostPlayerAPCommand(requester, playerIndex, num, vfxParam);

        if (isAddFirst)
        {
            CommandQueue.Instance.AddFirst(cmd);
        }
        else
        {
            CommandQueue.Instance.Add(cmd);
        }
    }

    public void RequestAddPlayerAP(PlayerIndex playerIndex, int num, bool isAddFirst = false)
    {
        AddPlayerAPCommand cmd = new AddPlayerAPCommand(playerIndex, num);

        if (isAddFirst)
        {
            CommandQueue.Instance.AddFirst(cmd);
        }
        else
        {
            CommandQueue.Instance.Add(cmd);
        }
    }

    public void RequestSetPlayerAP(PlayerIndex playerIndex, int num, bool isAddFirst = false)
    {
        SetPlayerAPCommand cmd = new SetPlayerAPCommand(playerIndex, num);

        if (isAddFirst)
        {
            CommandQueue.Instance.AddFirst(cmd);
        }
        else
        {
            CommandQueue.Instance.Add(cmd);
        }
    }

    public void RequestLocalUseCard(int uniqueID, int slotID, bool isAddFirst = false, UnityAction onCancel = null)
    {
        if (
               Phase == GamePhase.Play
            && !IsEndTurn
            && !IsCallEndTurn
            && UIQueue.Instance.IsReady
            && CommandQueue.Instance.IsReady
        )
        {
            BattleCardData battleCard = null;
            if (FindBattleCard(uniqueID, out battleCard))
            {
                switch (battleCard.Type)
                {
                    case CardType.Minion:
                    case CardType.Minion_NotInDeck:
                        {
                            if (IsLocalCanSpawnToken(slotID))
                            {
                                MinionData minion = battleCard as MinionData;

                                if (IsCanUseCard(minion))
                                {
                                    if (IsNetworkPlay())
                                    {
                                        PhotonRequestUseCard(uniqueID, slotID, isAddFirst);
                                    }
                                    else
                                    {
                                        RpcRequestUseCard(-1, GameHelper.ConvertGamePhaseToByte(Phase), uniqueID, GameHelper.ConvertIntToByte(slotID), isAddFirst);
                                    }
                                    return;
                                }
                            }
                        }
                        break;

                    case CardType.Spell:
                    case CardType.Spell_NotInDeck:
                        {
                            SpellData spell = battleCard as SpellData;
                            if (spell.IsCanUse)
                            {
                                if (IsNetworkPlay())
                                {
                                    PhotonRequestUseCard(uniqueID, slotID, isAddFirst);
                                }
                                else
                                {
                                    RpcRequestUseCard(-1, GameHelper.ConvertGamePhaseToByte(Phase), uniqueID, GameHelper.ConvertIntToByte(slotID), isAddFirst);
                                }
                                return;
                            }
                        }
                        break;
                }
            }
        }

        if (onCancel != null)
        {
            onCancel.Invoke();
        }
    }

    public void RequestLocalEndTurn(bool isActive)
    {
        if (IsCanEndTurn(GetLocalPlayerIndex()))
        {
            // In play phase and My turn 

            if (IsNetworkPlay())
            {
                PhotonRequestEndTurn(GetLocalPlayerIndex(), SubTurn, isActive);
            }
            else
            {
                RequestEndTurn(GetLocalPlayerIndex(), SubTurn, isActive);
            }
        }
    }

    public void RequestLocalSendMessage(HeroVoice heroVoiceIndex)
    {
        if (IsNetworkPlay())
        {
            PhotonRequestSendMessage(GetLocalPlayerIndex(), heroVoiceIndex);
        }
        else
        {
            ActionSendMessage(GetLocalPlayerIndex(), heroVoiceIndex, null);
        }
    }

    public void RequestShowWaitingOpponent(PlayerIndex requestPlayerIndex, bool isShow)
    {
        if (IsNetworkPlay())
        {
            if (IsLocalPlayer(requestPlayerIndex))
            {
                PhotonRequestShowWaitingOpponent(isShow);
            }
        }
        else
        {
            if (!IsLocalPlayer(requestPlayerIndex))
            {
                ActionShowWaiting(isShow, null);
            }
        }
    }

    public void RequestLocalSendMessage(PlayerIndex playerIndex, HeroVoice heroVoiceIndex, UnityAction onComplete)
    {
        if (IsNetworkPlay())
        {
            PhotonRequestSendMessage(playerIndex, heroVoiceIndex);

            onComplete?.Invoke();
        }
        else
        {
            ActionSendMessage(playerIndex, heroVoiceIndex, onComplete);
        }
    }

    public void RequestUseMinion(MinionData minion, int slotID, bool isAwaken, bool isAddFirst = false)
    {
        SpawnTokenCommand cmd = new SpawnTokenCommand(minion as MinionData, slotID, isAwaken);

        if (isAddFirst)
        {
            CommandQueue.Instance.AddFirst(cmd);
        }
        else
        {
            CommandQueue.Instance.Add(cmd);
        }
    }

    public void RequestEndTurn(PlayerIndex playerIndex, int subTurn, bool isActive, bool isAddFirst = false)
    {
        if (!IsCallEndTurn)
        {
            /* Stop Timer -----------------*/
            _isTimer = false;
            _isDisableTimer = true;
            UIManager.Instance.HideUITimer();
            /*------------------------------*/

            /* Close end turn button -------*/
            if (GetLocalPlayerIndex() == playerIndex)
            {
                PopupUIManager.Instance.ClearPopup_SectionMid();

                if (isActive)
                {
                    UIManager.Instance.EndButtonIsOpen(false, false);
                }
                else
                {
                    UIManager.Instance.EndButtonIsOpen(false, false);
                    UIManager.Instance.EndBTNComplete();
                }


            }
            /*------------------------------*/

            IsCallEndTurn = true;

            if (isActive)
            {
                _activeCount++;
            }

            EndTurnCommand cmd = new EndTurnCommand(playerIndex, subTurn, isActive);

            List<CommandData> commandList = new List<CommandData>();
            commandList.Add(cmd);

            ActionAddCommandListToSecondQueue(commandList, false);
        }
    }

    public void RequestCreateToken(string cardID, PlayerIndex owner, int slotID, bool isAwaken, bool isAddFirst = false)
    {
        CreateTokenCommand cmd = new CreateTokenCommand(cardID, owner, slotID, isAwaken);

        if (isAddFirst)
        {
            CommandQueue.Instance.AddFirst(cmd);
        }
        else
        {
            CommandQueue.Instance.Add(cmd);
        }
    }

    public void RequestMoveToken(Requester requester, int uniqueID, int targetSlotID, bool isAddFirst = false)
    {
        MoveTokenCommand cmd = new MoveTokenCommand(requester, uniqueID, targetSlotID);

        if (isAddFirst)
        {
            CommandQueue.Instance.AddFirst(cmd);
        }
        else
        {
            CommandQueue.Instance.Add(cmd);
        }
    }

    public void RequestMoveCardToZone(int uniqueID, CardZone startZone, CardZone destination, int slotID, UnityAction onComplete = null, bool isAddFirst = false)
    {
        MoveCardToZoneCommand cmd = new MoveCardToZoneCommand(uniqueID, startZone, destination, slotID, onComplete);

        if (isAddFirst)
        {
            CommandQueue.Instance.AddFirst(cmd);
        }
        else
        {
            CommandQueue.Instance.Add(cmd);
        }
    }

    public void RequestCreateCardToZone(string cardID, PlayerIndex owner, CardZone destination, int slotID, UnityAction<int> onComplete = null, bool isAddFirst = false)
    {
        CreateCardToZoneCommand cmd = new CreateCardToZoneCommand(cardID, owner, destination, slotID, onComplete);

        if (isAddFirst)
        {
            CommandQueue.Instance.AddFirst(cmd);
        }
        else
        {
            CommandQueue.Instance.Add(cmd);
        }
    }

    public void RequestDeadBattleCard(List<int> uniqueIDList, bool isTriggerDead, MinionDeathTypes deathType, bool isJoined = false, bool isAddFirst = false)
    {
        DeadBattleCardCommand cmd = new DeadBattleCardCommand(uniqueIDList, isTriggerDead, deathType);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestSpiritDamage(PlayerIndex playerIndex, int spiritDamage, bool isAddFirst = false)
    {
        SpiritDamageCommand cmd = new SpiritDamageCommand(playerIndex, spiritDamage);

        if (isAddFirst)
        {
            CommandQueue.Instance.AddFirst(cmd);
        }
        else
        {
            CommandQueue.Instance.Add(cmd);
        }
    }

    public void RequestSpiritDamageTutorial(PlayerIndex playerIndex, int spiritDamage, bool isAddFirst = false)
    {
        SpiritDamageTutorialCommand cmd = new SpiritDamageTutorialCommand(playerIndex, spiritDamage);

        if (isAddFirst)
        {
            CommandQueue.Instance.AddFirst(cmd);
        }
        else
        {
            CommandQueue.Instance.Add(cmd);
        }
    }

    public void RequestDrawDamage(PlayerIndex playerIndex, int spiritDamage, bool isAddFirst = false)
    {
        DrawDamageCommand cmd = new DrawDamageCommand(playerIndex, spiritDamage);

        if (isAddFirst)
        {
            CommandQueue.Instance.AddFirst(cmd);
        }
        else
        {
            CommandQueue.Instance.Add(cmd);
        }
    }

    public void RequestRestoreLifePointDelta(PlayerIndex playerIndex, int value, bool isAddFirst = false)
    {
        RestoreLifePointDeltaCommand cmd = new RestoreLifePointDeltaCommand(playerIndex, value);

        if (isAddFirst)
        {
            CommandQueue.Instance.AddFirst(cmd);
        }
        else
        {
            CommandQueue.Instance.Add(cmd);
        }
    }

    public void RequestSetMinionHP(Requester requester, List<int> targetIDList, int value, bool isJoined = false, bool isAddFirst = false)
    {
        SetHPMinionCommand cmd = new SetHPMinionCommand(requester, targetIDList, value);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestSetMinionAP(Requester requester, List<int> targetIDList, int value, bool isJoined = false, bool isAddFirst = false)
    {
        SetAPMinionCommand cmd = new SetAPMinionCommand(requester, targetIDList, value);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestSetMinionSP(Requester requester, List<int> targetIDList, int value, bool isJoined = false, bool isAddFirst = false)
    {
        SetSPMinionCommand cmd = new SetSPMinionCommand(requester, targetIDList, value);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestSetMinionArmor(Requester requester, List<int> targetIDList, int value, bool isJoined = false, bool isAddFirst = false)
    {
        SetArmorMinionCommand cmd = new SetArmorMinionCommand(requester, targetIDList, value);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestSetMinionAPHP(Requester requester, List<int> targetIDList, int apValue, int hpValue, bool isJoined = false, bool isAddFirst = false)
    {
        SetAPHPMinionCommand cmd = new SetAPHPMinionCommand(requester, targetIDList, apValue, hpValue);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestBoostMinionHP(Requester requester, List<int> targetIDList, int value, bool isJoined = false, bool isAddFirst = false)
    {
        BoostHPMinionCommand cmd = new BoostHPMinionCommand(requester, targetIDList, value);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestBoostMinionAP(Requester requester, List<int> targetIDList, int value, bool isJoined = false, bool isAddFirst = false)
    {
        BoostAPMinionCommand cmd = new BoostAPMinionCommand(requester, targetIDList, value);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestBoostMinionAPCritical(Requester requester, List<int> targetIDList, int value, bool isJoined = false, bool isAddFirst = false)
    {
        BoostAPCriticalMinionCommand cmd = new BoostAPCriticalMinionCommand(requester, targetIDList, value);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestBoostMinionSP(Requester requester, List<int> targetIDList, int value, bool isJoined = false, bool isAddFirst = false)
    {
        BoostSPMinionCommand cmd = new BoostSPMinionCommand(requester, targetIDList, value);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestBoostMinionAPHP(Requester requester, List<int> targetIDList, int ap, int hp, bool isJoined = false, bool isAddFirst = false)
    {
        BoostAPHPMinionCommand cmd = new BoostAPHPMinionCommand(requester, targetIDList, ap, hp);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestBoostMinionArmor(Requester requester, List<int> targetIDList, int armor, bool isJoined = false, bool isAddFirst = false)
    {
        BoostArmorMinionCommand cmd = new BoostArmorMinionCommand(requester, targetIDList, armor);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestBoostBuffPassiveMinionHP(Requester requester, List<int> targetIDList, int value, bool isJoined = false, bool isAddFirst = false)
    {
        BoostBuffPassiveHPMinionCommand cmd = new BoostBuffPassiveHPMinionCommand(requester, targetIDList, value);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestBoostBuffPasiveMinionAP(Requester requester, List<int> targetIDList, int value, bool isJoined = false, bool isAddFirst = false)
    {
        BoostBuffPassiveAPMinionCommand cmd = new BoostBuffPassiveAPMinionCommand(requester, targetIDList, value);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestBoostBuffPasiveMinionAPCritical(Requester requester, List<int> targetIDList, int value, bool isJoined = false, bool isAddFirst = false)
    {
        BoostBuffPassiveAPCriticalMinionCommand cmd = new BoostBuffPassiveAPCriticalMinionCommand(requester, targetIDList, value);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestBoostBuffPasiveMinionSP(Requester requester, List<int> targetIDList, int value, bool isJoined = false, bool isAddFirst = false)
    {
        BoostBuffPassiveSPMinionCommand cmd = new BoostBuffPassiveSPMinionCommand(requester, targetIDList, value);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestBoostBuffPasiveMinionAPHP(Requester requester, List<int> targetIDList, int ap, int hp, bool isJoined = false, bool isAddFirst = false)
    {
        BoostBuffPassiveAPHPMinionCommand cmd = new BoostBuffPassiveAPHPMinionCommand(requester, targetIDList, ap, hp);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestBoostBuffPasiveMinionArmor(Requester requester, List<int> targetIDList, int armor, bool isJoined = false, bool isAddFirst = false)
    {
        BoostBuffPassiveArmorMinionCommand cmd = new BoostBuffPassiveArmorMinionCommand(requester, targetIDList, armor);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestBoostHeroSP(Requester requester, PlayerIndex playerTarget, int value, bool isJoined = false, bool isAddFirst = false)
    {
        BoostHeroSPCommand cmd = new BoostHeroSPCommand(requester, playerTarget, value);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestSwapMinionHP(Requester requester, int battleCardUniqueID_1, int battleCardUniqueID_2, bool isJoined = false, bool isAddFirst = false)
    {
        SwapMinionHPCommand cmd = new SwapMinionHPCommand(requester, battleCardUniqueID_1, battleCardUniqueID_2);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestSwapMinionAP(Requester requester, int battleCardUniqueID_1, int battleCardUniqueID_2, bool isJoined = false, bool isAddFirst = false)
    {
        SwapMinionAPCommand cmd = new SwapMinionAPCommand(requester, battleCardUniqueID_1, battleCardUniqueID_2);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestSwapMinionSP(Requester requester, int battleCardUniqueID_1, int battleCardUniqueID_2, bool isJoined = false, bool isAddFirst = false)
    {
        SwapMinionSPCommand cmd = new SwapMinionSPCommand(requester, battleCardUniqueID_1, battleCardUniqueID_2);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestSwapMinionAPHP(Requester requester, int battleCardUniqueID_1, int battleCardUniqueID_2, bool isJoined = false, bool isAddFirst = false)
    {
        SwapMinionAPHPCommand cmd = new SwapMinionAPHPCommand(requester, battleCardUniqueID_1, battleCardUniqueID_2);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestSwapMinionSlot(Requester requester, int battleCardUniqueID_1, int battleCardUniqueID_2, bool isJoined = false, bool isAddFirst = false)
    {
        SwapMinionSlotCommand cmd = new SwapMinionSlotCommand(requester, battleCardUniqueID_1, battleCardUniqueID_2);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestRestoreMinionHP(Requester requester, List<int> targetIDList, int value, bool isJoined = false, bool isAddFirst = false)
    {
        RestoreHPMinionCommand cmd = new RestoreHPMinionCommand(requester, targetIDList, value);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestResetMinion(Requester requester, List<int> targetIDList, bool isJoined = false, bool isAddFirst = false)
    {
        ResetMinionCommand cmd = new ResetMinionCommand(requester, targetIDList);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestAddMinionDirTaunted(int bufferUniqueID, int battleCardUID, CardDirection buffDir, bool isJoined = false, bool isAddFirst = false)
    {
        AddMinionDirTauntedCommand cmd = new AddMinionDirTauntedCommand(bufferUniqueID, battleCardUID, buffDir);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestRemoveMinionDirTaunted(int bufferUniqueID, int battleCardUID, CardDirection buffDir, bool isJoined = false, bool isAddFirst = false)
    {
        RemoveMinionDirTauntedCommand cmd = new RemoveMinionDirTauntedCommand(bufferUniqueID, battleCardUID, buffDir);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestAddMinionDirBuffPassive(int bufferUniqueID, List<int> battleCardUID, CardDirection buffDir, bool isJoined = false, bool isAddFirst = false)
    {
        AddMinionDirBuffPassiveCommand cmd = new AddMinionDirBuffPassiveCommand(bufferUniqueID, battleCardUID, buffDir);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestAddMinionDir(List<int> battleCardUID, CardDirection buffDir, bool isJoined = false, bool isAddFirst = false)
    {
        AddMinionDirCommand cmd = new AddMinionDirCommand(battleCardUID, buffDir);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestBuffFear(Requester requester, int uniqueID, bool isFear, bool isJoined = false, bool isAddFirst = false)
    {
        BuffFearCommand cmd = new BuffFearCommand(requester, uniqueID, isFear);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestBuffFreeze(Requester requester, int uniqueID, bool isFreeze, bool isJoined = false, bool isAddFirst = false)
    {
        BuffFreezeCommand cmd = new BuffFreezeCommand(requester, uniqueID, isFreeze);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestBuffSilence(Requester requester, int uniqueID, bool isFreeze, bool isJoined = false, bool isAddFirst = false)
    {
        BuffSilenceCommand cmd = new BuffSilenceCommand(requester, uniqueID, isFreeze);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestBuffStealth(Requester requester, int uniqueID, bool isStealth, bool isJoined = false, bool isAddFirst = false)
    {
        BuffStealthCommand cmd = new BuffStealthCommand(requester, uniqueID, isStealth);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestBuffInvincible(Requester requester, int uniqueID, bool isInvincible, bool isJoined = false, bool isAddFirst = false)
    {
        BuffInvincibleCommand cmd = new BuffInvincibleCommand(requester, uniqueID, isInvincible);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestBuffUntargetable(Requester requester, int uniqueID, bool isUntargetable, bool isJoined = false, bool isAddFirst = false)
    {
        BuffUntargetableCommand cmd = new BuffUntargetableCommand(requester, uniqueID, isUntargetable);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestBuffEffectInvincible(Requester requester, int uniqueID, bool isInvincible, bool isJoined = false, bool isAddFirst = false)
    {
        BuffEffectInvincibleCommand cmd = new BuffEffectInvincibleCommand(requester, uniqueID, isInvincible);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestBuffPiercing(Requester requester, int uniqueID, int buffUniqueID, bool isPiercing, int range, bool isJoined = false, bool isAddFirst = false)
    {
        BuffPiercingCommand cmd = new BuffPiercingCommand(requester, uniqueID, buffUniqueID, isPiercing, range);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestBuffOverrideEffectDamage(Requester requester, int uniqueID, int buffUniqueID, bool isOverride, int damage, bool isJoined = false, bool isAddFirst = false)
    {
        BuffOverrideEffectDamageCommand cmd = new BuffOverrideEffectDamageCommand(requester, uniqueID, buffUniqueID, isOverride, damage);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestBuffMinionDarkMetter(int uniqueID, bool isCanBeSelected, bool isJoined = false, bool isAddFirst = false)
    {
        BuffMinionDarkMatterCommand cmd = new BuffMinionDarkMatterCommand(uniqueID, isCanBeSelected);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestBuffSlotLock(int slotID, int counterAmount, bool isLock, PlayerIndex owner, bool isJoined = false, bool isAddFirst = false)
    {
        BuffSlotLockCommand cmd = new BuffSlotLockCommand(slotID, counterAmount, owner, isLock);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestBuffSlotDarkMatter(Requester requester, int slotID, bool isDarkMatter, PlayerIndex owner, bool isJoined = false, bool isAddFirst = false)
    {
        BuffSlotDarkMatterCommand cmd = new BuffSlotDarkMatterCommand(requester, slotID, isDarkMatter, owner);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestBuffEmpty(Requester requester, int uniqueID, bool isJoined = false, bool isAddFirst = false)
    {
        BuffEmptyCommand cmd = new BuffEmptyCommand(requester, uniqueID);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestDealDamageMinion(Requester requester, List<int> targetIDList, int damage, DataParam vfxParam, bool isJoined = false, bool isAddFirst = false)
    {
        DealDamageMinionCommand cmd = new DealDamageMinionCommand(requester, targetIDList, damage, vfxParam);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestDealPlayerDamage(Requester requester, List<PlayerIndex> playerList, int damage, DataParam vfxParam, bool isJoined = false, bool isAddFirst = false)
    {
        DealDamagePlayerCommand cmd = new DealDamagePlayerCommand(requester, playerList, damage, vfxParam);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestPrepareAbility(int uniqueCardID, int uniqueAbiliyID, Requester triggerer, EventKey triggerKey, bool isAddFirst = false)
    {
        //Debug.LogFormat("RequestPrepareAbility " + uniqueCardID + ":" + triggererUniqueID);

        PrepareAbilityCommand cmd = new PrepareAbilityCommand(uniqueCardID, uniqueAbiliyID, triggerer, triggerKey);

        if (isAddFirst)
        {
            CommandQueue.Instance.AddFirst(cmd);
        }
        else
        {
            CommandQueue.Instance.Add(cmd);
        }
    }

    public void RequestActionAbility(int uniqueCardID, int uniqueAbiliyID, Dictionary<string, string> paramList, int randomSeed, bool isAddFirst = false)
    {
        ActionAbilityCommand cmd = new ActionAbilityCommand(uniqueCardID, uniqueAbiliyID, paramList, randomSeed);

        if (isAddFirst)
        {
            CommandQueue.Instance.AddFirst(cmd);
        }
        else
        {
            CommandQueue.Instance.Add(cmd);
        }
    }

    public void RequestAddMinionBuff(int uniqueCardID, MinionBuffData buffData, bool isJoined = false, bool isAddFirst = false)
    {
        AddMinionBuffCommand cmd = new AddMinionBuffCommand(uniqueCardID, buffData);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestUpdateMinionBuff(int uniqueCardID, string key, bool isJoined = false, bool isAddFirst = false)
    {
        UpdateMinionBuffCommand cmd = new UpdateMinionBuffCommand(uniqueCardID, key);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestRemoveMinionBuff(int uniqueCardID, string key, bool isJoined = false, bool isAddFirst = false)
    {
        RemoveMinionBuffCommand cmd = new RemoveMinionBuffCommand(uniqueCardID, key);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestRemoveMinionBuff(int uniqueCardID, MinionBuffTypes type, bool isJoined = false, bool isAddFirst = false)
    {
        RemoveMinionBuffTypeCommand cmd = new RemoveMinionBuffTypeCommand(uniqueCardID, type);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestAddSlotBuff(int slotID, SlotBuffData buffData, bool isJoined = false, bool isAddFirst = false)
    {
        AddSlotBuffCommand cmd = new AddSlotBuffCommand(slotID, buffData);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestRemoveSlotBuff(int slotID, string key, bool isJoined = false, bool isAddFirst = false)
    {
        RemoveSlotBuffCommand cmd = new RemoveSlotBuffCommand(slotID, key);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestRemoveSlotBuff(int slotID, SlotBuffTypes type, bool isJoined = false, bool isAddFirst = false)
    {
        RemoveSlotBuffTypeCommand cmd = new RemoveSlotBuffTypeCommand(slotID, type);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestUpdateLinkOnSlot(int slotID, bool isJoined = false, bool isAddFirst = false)
    {
        UpdateLinkOnSlotCommand cmd = new UpdateLinkOnSlotCommand(slotID);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }

    public void RequestAddMinionToAttackerList(MinionData minion, bool isJoined = false, bool isAddFirst = false)
    {
        AddMinionToAttackerListCommand cmd = new AddMinionToAttackerListCommand(minion);

        if (isAddFirst)
        {
            if (isJoined)
            {
                CommandQueue.Instance.JoinFirst(cmd);
            }
            else
            {
                CommandQueue.Instance.AddFirst(cmd);
            }
        }
        else
        {
            if (isJoined)
            {
                CommandQueue.Instance.Join(cmd);
            }
            else
            {
                CommandQueue.Instance.Add(cmd);
            }
        }
    }
    #endregion

    #region Action Methods
    public void ActionWaitActionAbility()
    {
        Debug.LogFormat("GameManager/ActionGetActionAbility:");
        CommandQueue.Instance.SetPause(true);

        if (_actionAbilityCommandList != null && _actionAbilityCommandList.Count > 0)
        {
            // Get action BEFORE start wait
            ActionGetActionAbility();
        }
    }

    public void ActionGetActionAbility()
    {
        int count = 0;

        if (_actionAbilityCommandList != null)
        {
            count = _actionAbilityCommandList.Count;
        }

        Debug.LogFormat(
              "GameManager/ActionGetActionAbility: <color={0}>{1}[{2}]</color> | cmd: [{3}]"
            , CommandQueue.Instance.IsPause ? "red" : "green"
            , CommandQueue.Instance.IsPause ? "Pause" : "Play"
            , CommandQueue.Instance.PauseCount
            , count
        );

        if (CommandQueue.Instance.IsPause && _actionAbilityCommandList != null && _actionAbilityCommandList.Count > 0)
        {
            while (CommandQueue.Instance.IsPause && _actionAbilityCommandList.Count > 0)
            {
                // Get action AFTER start wait
                CommandData cmd = _actionAbilityCommandList[0]; // pop
                _actionAbilityCommandList.RemoveAt(0);

                if (cmd.GetType() == typeof(PauseCommand))
                {
                    PauseCommand pauseCmd = (PauseCommand)cmd;
                    CommandQueue.Instance.SetPause(false);
                }
                else
                {
                    UIManager.Instance.HideWaiting();

                    ActionAddCommandListToSecondQueue(new List<CommandData>() { cmd }, true);

                    //Debug.LogFormat("GameManager/ActionGetActionAbility: Add cmd to 2nd queue {0}", cmd.GetDebugText());
                    CommandQueue.Instance.SetPause(false);
                }
            }
        }
    }

    public void ActionReadyActionAbility()
    {
        int count = 0;

        if (_actionAbilityCommandList != null)
        {
            count = _actionAbilityCommandList.Count;
        }

        Debug.LogFormat(
              "GameManager/ActionReadyActionAbility: <color={0}>{1}[{2}]</color> | cmd: [{0}]"
            , CommandQueue.Instance.IsPause ? "red" : "green"
            , CommandQueue.Instance.IsPause ? "Pause" : "Play"
            , CommandQueue.Instance.PauseCount
            , count
        );

        CommandQueue.Instance.SetPause(true);

        ActionGetActionAbility();
    }

    public void ActionGetUseCard()
    {
        if (Phase == GamePhase.Play)
        {
            while (true)
            {
                if (_useCardCommandList != null && _useCardCommandList.Count > 0)
                {
                    CommandData cmd = _useCardCommandList.Dequeue(); // pop
                    CommandQueue.Instance.Add(cmd);
                    CommandData cmd2 = _useCardCommandList.Dequeue(); // pop
                    CommandQueue.Instance.Join(cmd2);

                    _activeCount++;
                }
                else
                {
                    break;
                }
            }
        }
    }

    public void ActionAddCommandListToSecondQueue(List<CommandData> commandList, bool isAddFirst = true)
    {
        if (commandList != null && commandList.Count > 0)
        {
            if (isAddFirst)
            {
                CommandQueue.Instance.AddFirstSecondQueue(commandList);
            }
            else
            {
                CommandQueue.Instance.AddSecondQueue(commandList);
            }
        }
    }

    public void ActionRandomPlayOrder(UnityAction onComplete)
    {
        SetPlayOrder(RandomPlayFirst(), onComplete);
    }

    public void SetPlayOrder(PlayerIndex playerIndex, UnityAction onComplete)
    {
        _currentPlayer = playerIndex;

        Debug.LogFormat("Player {0} play first.", _currentPlayer.ToString());

        if (IsLocalPlayer(_currentPlayer))
        {
            _isMePlayFirst = true;
        }
        else
        {
            _isMePlayFirst = false;
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionShuffleDeck(PlayerIndex playerIndex, UnityAction onComplete)
    {
        if (_data != null)
        {
            _data.PlayerDataList[(int)playerIndex].ShuffleDeck(false);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionDrawCard(PlayerIndex playerIndex, UnityAction onComplete)
    {
        if (_data != null)
        {
            // Count PlayerStat
            PlayerStatManager.Instance.DoDrawCardEvent(playerIndex, null);

            if (_data.PlayerDataList[(int)playerIndex].CardList[CardZone.Deck].Count > 0)
            {
                BattleCardData battleCard = _data.PlayerDataList[(int)playerIndex].CardList[CardZone.Deck][0];

                ActionMoveCardToZone(battleCard.UniqueID, CardZone.Deck, CardZone.Hand, -1, onComplete);
                return;
            }
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionDiscardDeck(PlayerIndex playerIndex, UnityAction onComplete)
    {
        if (_data != null)
        {
            if (_data.PlayerDataList[(int)playerIndex].CardList[CardZone.Deck].Count > 0)
            {
                BattleCardData battleCard = _data.PlayerDataList[(int)playerIndex].CardList[CardZone.Deck][0];

                ActionMoveCardToZone(battleCard.UniqueID, CardZone.Deck, CardZone.Graveyard, -1, onComplete);
                return;
            }
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionDiscardHand(PlayerIndex playerIndex, int uniqueCardID, UnityAction onComplete)
    {
        ActionMoveCardToZone(uniqueCardID, CardZone.Hand, CardZone.Graveyard, -1, onComplete);
    }

    public void ActionRemoveHand(PlayerIndex playerIndex, int uniqueCardID, UnityAction onComplete)
    {
        ActionMoveCardToZone(uniqueCardID, CardZone.Hand, CardZone.Remove, -1, onComplete);
    }

    public void ActionBoostAPPlayer(Requester requester, PlayerIndex playerIndex, int apValue, string movingVFXKey, string arrivedVFXKey, UnityAction onComplete)
    {
        CardUIData cardUIData = new CardUIData(requester.Minion);
        if (_data != null)
        {
            int previousAP = _data.PlayerDataList[(int)playerIndex].CurrentAP;
            _data.PlayerDataList[(int)playerIndex].SetAPDeltaClamp(apValue);
            int currentAP = _data.PlayerDataList[(int)playerIndex].CurrentAP;
        }
        UpdateLocalHandCard();

        UnityAction onBoostPlayerAPComplete = delegate
        {
            UnityAction onVFXArrivedComplete = delegate ()
            {
                UpdateLocalHandCard();
                UIManager.Instance.ShowRegenAP(playerIndex, apValue, onComplete);
            };

            if (arrivedVFXKey != "-")
            {
                // TODO:
            }

            if (onVFXArrivedComplete != null)
            {
                onVFXArrivedComplete.Invoke();
            }
        };

        if (apValue > 0)
        {
            if (movingVFXKey != "-")
            {
                UIManager.Instance.RequestBoostAPFromMinionToPlayer(
                      cardUIData
                    , playerIndex
                    , apValue
                    , movingVFXKey
                );
            }
        }

        if (onBoostPlayerAPComplete != null)
        {
            onBoostPlayerAPComplete.Invoke();
        }
    }

    public void ActionAddPlayerAP(PlayerIndex playerIndex, int num, UnityAction onComplete)
    {
        //Debug.Log("ActionAddPlayerAP " + num);
        if (_data != null)
        {
            int previousAP = _data.PlayerDataList[(int)playerIndex].CurrentAP;
            _data.PlayerDataList[(int)playerIndex].SetAPDeltaClamp(num);
            int currentAP = _data.PlayerDataList[(int)playerIndex].CurrentAP;

            if (previousAP == currentAP)
            {
                // nothing change.
                num = 0;
            }

            if (num > 0)
            {
                //Debug.Log("num > 0");
                UpdateLocalHandCard();
                UIManager.Instance.ShowRegenAP(playerIndex, num, onComplete);
                return;
            }
            else if (num < 0)
            {
                //Debug.Log("num < 0");
                num *= -1;
                switch (Phase)
                {
                    /*
                    case GamePhase.Draw:
                    {
                        //Debug.Log("at draw");
                        UIManager.Instance.RequesetShowDrawEffect(playerIndex, num);
                    }
                    break;
                    */

                    default:
                        {
                            //Debug.Log("other phase");
                            UpdateLocalHandCard();
                            UIManager.Instance.ShowUseCardAP(playerIndex, num, onComplete);
                        }
                        break;
                }
            }


        }

        UpdateLocalHandCard();
        //Debug.Log("ActionAddPlayerAP onComplete");
        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionSetPlayerAP(PlayerIndex playerIndex, int num, UnityAction onComplete)
    {
        //Debug.Log("ActionAddPlayerAP " + num);
        if (_data != null)
        {
            int previousAP = _data.PlayerDataList[(int)playerIndex].CurrentAP;
            _data.PlayerDataList[(int)playerIndex].SetAPClamp(num);
            int currentAP = _data.PlayerDataList[(int)playerIndex].CurrentAP;

            int deltaValue = currentAP - previousAP;

            if (deltaValue > 0)
            {
                UpdateLocalHandCard();
                UIManager.Instance.ShowRegenAP(playerIndex, deltaValue, onComplete);
                return;
            }
            else if (deltaValue < 0)
            {
                deltaValue *= -1;
                UpdateLocalHandCard();
                UIManager.Instance.ShowUseCardAP(playerIndex, deltaValue, onComplete);
            }
        }

        UpdateLocalHandCard();

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionDeadMinion(List<int> uniqueIDList, bool isTriggerEvent, MinionDeathTypes deathType, UnityAction onComplete)
    {
        List<CommandData> commandList = new List<CommandData>();
        MinionData minion;
        int uniqueID;

        //for (int i = uniqueIDList.Count - 1; i >= 0; i--)
        for (int i = 0; i < uniqueIDList.Count; i++)
        {
            uniqueID = uniqueIDList[i];
            bool isSuccess = FindMinionBattleCard(uniqueID, out minion);

            if (isSuccess && !minion.IsAlreadyDead)
            {
                minion.SetMinionDead();

                // Add Action Log
                {
                    DeathLog log = new DeathLog(minion);
                    ActionAddBattleLog(log);
                }

                // Trigger Analytic
                {
                    AnalyticTriggerCardDestroy(minion);
                }

                // Count PlayerStat
                PlayerStatManager.Instance.DoDestroyEnemyCardEvent(minion, null);

                List<EventTriggerData> triggerList = new List<EventTriggerData>();
                Requester triggerer = new Requester(minion);
                triggerList.Add(new EventTriggerData(EventKey.LastWish, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnMeExitBattlefield, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnFriendExitBattlefield, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnEnemyExitBattlefield, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnMinionDied, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnFriendDied, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnEnemyDied, triggerer));

                switch (deathType)
                {
                    case MinionDeathTypes.Battle:
                        {
                            triggerList.Add(new EventTriggerData(EventKey.OnMeDiedByBattle, triggerer));
                            triggerList.Add(new EventTriggerData(EventKey.OnMinionDiedByBattle, triggerer));
                            triggerList.Add(new EventTriggerData(EventKey.OnFriendDiedByBattle, triggerer));
                            triggerList.Add(new EventTriggerData(EventKey.OnEnemyDiedByBattle, triggerer));
                        }
                        break;

                    case MinionDeathTypes.Effect:
                        {
                            triggerList.Add(new EventTriggerData(EventKey.OnMeDiedByEffect, triggerer));
                            triggerList.Add(new EventTriggerData(EventKey.OnMinionDiedByEffect, triggerer));
                            triggerList.Add(new EventTriggerData(EventKey.OnFriendDiedByEffect, triggerer));
                            triggerList.Add(new EventTriggerData(EventKey.OnEnemyDiedByEffect, triggerer));
                        }
                        break;
                }

                commandList.AddRange(EventTrigger.TriggerCommandList(triggerList));
                commandList.Add(new MoveCardToZoneCommand(uniqueID, CardZone.Battlefield, CardZone.Graveyard, -1, null));
            }
        }

        ActionAddCommandListToSecondQueue(commandList, true);

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionUpdateMinionActiveZone(int uniqueID, CardZone activeZone, UnityAction onComplete)
    {
        bool isSuccess = false;
        BattleCardData battleCard = null;
        isSuccess = FindBattleCard(uniqueID, out battleCard);
        if (isSuccess)
        {
            battleCard.SetActiveZone(activeZone);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionMoveCardToZone(int uniqueID, CardZone startZone, CardZone targetZone, int slotID, UnityAction onComplete)
    {
        bool isSuccess = false;
        BattleCardData battleCard = null;
        CardZone currentZone = CardZone.Deck;

        onComplete += delegate { this.UpdateAllUIInfo(false); };

        isSuccess = FindBattleCard(uniqueID, out battleCard, out currentZone);
        if (isSuccess)
        {
            if (currentZone == startZone)
            {
                if (
                       currentZone != targetZone
                    && ((targetZone != CardZone.Battlefield) || (targetZone == CardZone.Battlefield && IsValidSlotID(slotID)))
                )
                {
                    BattlePlayerData battlePlayerData = _data.PlayerDataList[(int)battleCard.Owner];

                    // Move data
                    int index = battlePlayerData.CardList[currentZone].IndexOf(battleCard);
                    battleCard.SetCurrentZone(targetZone);
                    battleCard.SetActiveZone(targetZone);
                    battlePlayerData.CardList[targetZone].Add(battleCard);
                    battlePlayerData.CardList[currentZone].RemoveAt(index);

                    // Update slot ID
                    foreach (SlotData slotData in Data.SlotDatas)
                    {
                        if (slotData.BattleCardUniqueID == battleCard.UniqueID)
                        {
                            slotData.ResetBattleCardUniqueID();
                            break;
                        }
                    }

                    switch (targetZone)
                    {
                        case CardZone.Battlefield:
                        {
                            switch (startZone)
                            {
                                case CardZone.Graveyard:
                                case CardZone.Remove:
                                {
                                    battleCard.SetCurrentZone(startZone);
                                    battleCard.SetActiveZone(startZone);

                                    // Card that "touch" graveyard or remove zone has to be reset default before come to battlefield.
                                    battleCard.ResetToDefault(false);

                                    battleCard.SetCurrentZone(CardZone.Battlefield);
                                    battleCard.SetActiveZone(CardZone.Battlefield);
                                }
                                break;
                            }

                            if (battleCard.Type == CardType.Minion || battleCard.Type == CardType.Minion_NotInDeck)
                            {
                                battleCard.SetSlotID(slotID);
                                battleCard.SetSpawnedSlotID(slotID);
                                Data.SlotDatas[slotID].SetBattleCardUniqueID(battleCard.UniqueID);
                                ActionAddAttackerList(battleCard as MinionData); // add to attacker list
                                Data.AddBattleCardActiveID(battleCard.UniqueID); // add BattleCard list

                                // trigger analytic
                                if (currentZone != CardZone.Hand)
                                {
                                    AnalyticTriggerCardSummon(battleCard);
                                }
                            }
                        }
                        break;

                        case CardZone.Hand:
                        {
                            switch (startZone)
                            {
                                case CardZone.Battlefield:
                                {
                                    // before card go into hand, it must be reset default first.
                                    battleCard.ResetToDefault();

                                    MinionData minion = battleCard as MinionData;
                                    Requester triggerer = new Requester(minion);

                                    List<EventTriggerData> triggerList_postReset = new List<EventTriggerData>();
                                    triggerList_postReset.Add(new EventTriggerData(EventKey.OnMeMovedToHandFromBattlefield, triggerer));
                                    triggerList_postReset.Add(new EventTriggerData(EventKey.OnFriendMovedToHandFromBattlefield, triggerer));
                                    triggerList_postReset.Add(new EventTriggerData(EventKey.OnEnemyMovedToHandFromBattlefield, triggerer));

                                    List<CommandData> commandList = new List<CommandData>();
                                    commandList.AddRange(EventTrigger.TriggerCommandList(triggerList_postReset));
                                    ActionAddCommandListToSecondQueue(commandList, true);
                                }
                                break;

                                case CardZone.Deck:
                                {
                                    // draw card after 'Draw Phase'
                                    if(Phase >= GamePhase.Play)
                                    {
                                        MinionData minion = battleCard as MinionData;

                                        Requester triggerer = new Requester(minion);
                                        List<EventTriggerData> triggerList_postReset = new List<EventTriggerData>();
                                        triggerList_postReset.Add(new EventTriggerData(EventKey.OnMeMovedToHandFromDeck, triggerer));
                                        triggerList_postReset.Add(new EventTriggerData(EventKey.OnFriendMovedToHandFromDeck, triggerer));
                                        triggerList_postReset.Add(new EventTriggerData(EventKey.OnEnemyMovedToHandFromDeck, triggerer));

                                        List<CommandData> commandList = new List<CommandData>();
                                        commandList.AddRange(EventTrigger.TriggerCommandList(triggerList_postReset));
                                        ActionAddCommandListToSecondQueue(commandList, true);
                                    }
                                }
                                break;
                            }
                        }
                        break;

                        case CardZone.Deck:
                        {
                            switch (startZone)
                            {
                                case CardZone.Battlefield:
                                {
                                    // before card go into deck, it must be reset default first.
                                    battleCard.ResetToDefault();
                                }
                                break;
                            }
                        }
                        break;

                        case CardZone.Graveyard:
                        {
                            MinionData minion = battleCard as MinionData;

                            Requester triggerer = new Requester(minion);

                            List<EventTriggerData> triggerList_preReset = new List<EventTriggerData>();
                            triggerList_preReset.Add(new EventTriggerData(EventKey.OnMeMovingToGraveyard, triggerer));
                            triggerList_preReset.Add(new EventTriggerData(EventKey.OnFriendMovingToGraveyard, triggerer));
                            triggerList_preReset.Add(new EventTriggerData(EventKey.OnEnemyMovingToGraveyard, triggerer));

                            ResetDefaultMinionInZoneCommand resetDefaultCommand = new ResetDefaultMinionInZoneCommand(
                                    new Requester(battleCard.Owner)
                                , new List<int>() { battleCard.UniqueID }
                                , CardZone.Graveyard
                            );

                            List<EventTriggerData> triggerList_postReset = new List<EventTriggerData>();
                            triggerList_postReset.Add(new EventTriggerData(EventKey.OnMeMovedToGraveyard, triggerer));
                            triggerList_postReset.Add(new EventTriggerData(EventKey.OnFriendMovedToGraveyard, triggerer));
                            triggerList_postReset.Add(new EventTriggerData(EventKey.OnEnemyMovedToGraveyard, triggerer));

                            List<CommandData> commandList = new List<CommandData>();
                            commandList.AddRange(EventTrigger.TriggerCommandList(triggerList_preReset));
                            commandList.Add(resetDefaultCommand);
                            commandList.AddRange(EventTrigger.TriggerCommandList(triggerList_postReset));
                            ActionAddCommandListToSecondQueue(commandList, true);
                        }
                        break;

                        case CardZone.Remove:
                        {
                            battleCard.ResetToDefault();

                            MinionData minion = battleCard as MinionData;
                            PlayerStatManager.Instance.DoRemoveEnemyCardEvent(minion, null);
                        }
                        break;

                        default:
                        {
                            //battleCard.ResetSlotID();
                        }
                        break;
                    }

                    // Update UI
                    CardUIData cardUIData = new CardUIData(battleCard);
                    switch (currentZone)
                    {
                        case CardZone.Deck:
                        {
                            switch (targetZone)
                            {
                                case CardZone.Hand:
                                {
                                    // deck to hand

                                    // Draw card from deck
                                    if (Phase == GamePhase.ReHand)
                                    {
                                        if (IsLocalPlayer(battleCard.Owner))
                                        {
                                            UIManager.Instance.RequestDrawCardToSlot(cardUIData, Karamucho.UI.CardUIManager.PlayerPosition.Bottom);
                                        }
                                        else
                                        {
                                            UIManager.Instance.RequestDrawCardToSlot(cardUIData, Karamucho.UI.CardUIManager.PlayerPosition.Top);
                                        }
                                    }
                                    else if (Phase == GamePhase.Intro)
                                    {
                                        // Create card UI
                                        if (IsLocalPlayer(battleCard.Owner))
                                        {
                                            UIManager.Instance.RequestDrawFast(cardUIData, Karamucho.UI.CardUIManager.PlayerPosition.Bottom);
                                        }
                                        else
                                        {
                                            UIManager.Instance.RequestDrawFast(cardUIData, Karamucho.UI.CardUIManager.PlayerPosition.Top);
                                        }
                                    }
                                    else
                                    {
                                        // Create card UI
                                        if (IsLocalPlayer(battleCard.Owner))
                                        {
                                            UIManager.Instance.RequestDrawCardToSlot(cardUIData, Karamucho.UI.CardUIManager.PlayerPosition.Bottom);
                                        }
                                        else
                                        {
                                            UIManager.Instance.RequestDrawCardToSlot(cardUIData, Karamucho.UI.CardUIManager.PlayerPosition.Top);
                                        }
                                    }
                                }
                                break;

                                case CardZone.Battlefield:
                                {
                                    // battlefield to deck

                                    // Summon from deck
                                    UIManager.Instance.RequestSpawnTokenToBoard(cardUIData, onComplete);
                                    ActionUpdateBuffSlotDarkMatter(slotID);
                                    return;
                                }
                                break;

                                case CardZone.Graveyard:
                                {
                                    // deck to graveyard

                                    if (IsLocalPlayer(battleCard.Owner))
                                    {
                                        // Show feedback discard from deck to graveyard
                                        UIManager.Instance.RequestDrawCardToDestroy(cardUIData, Karamucho.UI.CardUIManager.PlayerPosition.Bottom);
                                    }
                                    else
                                    {
                                        // Show feedback discard from deck to graveyard
                                        UIManager.Instance.RequestDrawCardToDestroy(cardUIData, Karamucho.UI.CardUIManager.PlayerPosition.Top);
                                    }
                                }
                                break;

                                case CardZone.Remove:
                                {
                                    // deck to remove zone

                                    if (IsLocalPlayer(battleCard.Owner))
                                    {
                                        // Show feedback discard from deck to remove zone
                                        UIManager.Instance.RequestDrawCardToDestroy(cardUIData, Karamucho.UI.CardUIManager.PlayerPosition.Bottom);
                                    }
                                    else
                                    {
                                        // Show feedback discard from deck to remove zone
                                        UIManager.Instance.RequestDrawCardToDestroy(cardUIData, Karamucho.UI.CardUIManager.PlayerPosition.Top);
                                    }
                                }
                                break;
                            }
                        }
                        break;

                        case CardZone.Hand:
                        {
                            switch (targetZone)
                            {
                                case CardZone.Deck:
                                {
                                    // hand to deck

                                    UIManager.Instance.RequestReturnCardToDeck(cardUIData);
                                }
                                break;

                                case CardZone.Battlefield:
                                {
                                    // hand to battlefield

                                    SoundManager.PlaySFX(SoundManager.SFX.Summon_Card);

                                    // Summon from hand
                                    UIManager.Instance.RequestSpawnTokenToBoard(cardUIData, onComplete);
                                    ActionUpdateBuffSlotDarkMatter(slotID);
                                    return;
                                }
                                break;

                                case CardZone.Graveyard:
                                {
                                    // hand to graveyard

                                    // discard from hand to graveyard
                                    UIManager.Instance.RequestDiscard(cardUIData);
                                }
                                break;

                                case CardZone.Remove:
                                {
                                    // hand to remove zone

                                    // discard from hand to remove zone
                                    UIManager.Instance.RequestRemoveCard(cardUIData);
                                }
                                break;
                            }
                        }
                        break;

                        case CardZone.Battlefield:
                        {
                            switch (targetZone)
                            {
                                case CardZone.Deck:
                                {
                                    // battlefield to deck

                                    ActionRemoveAttackerList(battleCard as MinionData);
                                    Data.RemoveBattleCardActiveID(battleCard.UniqueID); // Remove battle active list
                                    UIManager.Instance.RequestDestroyToken(cardUIData, false);

                                    Data.PlayerDataList[(int)battleCard.Owner].ShuffleDeck(false);
                                }
                                break;

                                case CardZone.Hand:
                                {
                                    // battlefield to hand

                                    ActionRemoveAttackerList(battleCard as MinionData);
                                    Data.RemoveBattleCardActiveID(battleCard.UniqueID); // Remove battle active list
                                    UIManager.Instance.RequestMoveTokenToHand(
                                            cardUIData
                                        , (IsLocalPlayer(battleCard.Owner) ? Karamucho.UI.CardUIManager.PlayerPosition.Bottom : Karamucho.UI.CardUIManager.PlayerPosition.Top)
                                    );
                                }
                                break;

                                case CardZone.Graveyard:
                                {
                                    // battlefield to graveyard

                                    ActionRemoveAttackerList(battleCard as MinionData);
                                    Data.RemoveBattleCardActiveID(battleCard.UniqueID); // Remove battle active list
                                    UIManager.Instance.RequestDestroyToken(cardUIData, false); // Dead
                                }
                                break;

                                case CardZone.Remove:
                                {
                                    // battlefield to remove zone

                                    ActionRemoveAttackerList(battleCard as MinionData);
                                    Data.RemoveBattleCardActiveID(battleCard.UniqueID); // Remove battle active list
                                    UIManager.Instance.RequestBanishToken(cardUIData, false); // Remove from game
                                }
                                break;
                            }
                            break;
                        }

                        case CardZone.Graveyard:
                        {
                            if (battleCard.Type == CardType.Minion || battleCard.Type == CardType.Minion_NotInDeck)
                            {
                                MinionData minion = battleCard as MinionData;
                                minion.SetEnableLastWish();
                                minion.SetEnableSacrificed();
                            }

                            switch (targetZone)
                            {
                                case CardZone.Deck:
                                {
                                    // graveyard back to deck
                                    Data.PlayerDataList[(int)battleCard.Owner].ShuffleDeck(false);
                                }
                                break;

                                case CardZone.Hand:
                                {
                                    // graveyard back to hand

                                    // create card UI
                                    if (IsLocalPlayer(battleCard.Owner))
                                    {
                                        UIManager.Instance.RequestDrawCardFromGrave(cardUIData, Karamucho.UI.CardUIManager.PlayerPosition.Bottom);
                                    }
                                    else
                                    {
                                        UIManager.Instance.RequestDrawCardFromGrave(cardUIData, Karamucho.UI.CardUIManager.PlayerPosition.Top);
                                    }
                                }
                                break;

                                case CardZone.Battlefield:
                                {
                                    // summon from graveyard (Reborn)
                                    UIManager.Instance.RequestSpawnTokenToBoard(cardUIData, onComplete);
                                    ActionUpdateBuffSlotDarkMatter(slotID);
                                    return;
                                }
                                break;

                                case CardZone.Remove:
                                {

                                }
                                break;
                            }
                        }
                        break;
                    }
                }
                else
                {
                    if (currentZone == targetZone)
                    {
                        Debug.LogWarningFormat("ActionMoveCardToZone: Rejected. move to same location. ({0}:{1})", uniqueID, targetZone);
                    }
                    if (targetZone == CardZone.Battlefield && !IsValidSlotID(slotID))
                    {
                        Debug.LogErrorFormat("ActionMoveCardToZone: Failed to move to battlefield. In-valid slotID. ({0}[{1}])", uniqueID, slotID);
                    }
                }
            }
            else
            {
                Debug.LogErrorFormat("ActionMoveCardToZone: Error card current zone not same as start zone. ({0}:{1}->{2})", uniqueID, currentZone, startZone);
            }
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionCreateCardToZone(string cardID, PlayerIndex owner, CardZone targetZone, int slotID, UnityAction<int> onComplete)
    {
        BattleCardData battleCard = BattleCardData.CreateBattleCard(
              cardID
            , GameManager.Instance.Data.PlayerDataList[(int)owner].CardBackID
        );

        if (battleCard == null)
        {
            Debug.LogError("Fail to CreateCardToZone");
            if (onComplete != null)
            {
                onComplete.Invoke(-1); // failed to create.
            }
            return;
        }

        battleCard.SetOwner(owner);
        battleCard.SetCurrentZone(CardZone.Deck);
        battleCard.InitAbility();

        if ((targetZone == CardZone.Battlefield && IsValidSlotID(slotID)) || (targetZone != CardZone.Battlefield))
        {
            BattlePlayerData battlePlayerData = _data.PlayerDataList[(int)battleCard.Owner];

            // add data
            battleCard.SetCurrentZone(targetZone);
            battlePlayerData.CardList[targetZone].Add(battleCard);

            // update slot ID
            if (targetZone == CardZone.Battlefield)
            {
                battleCard.SetSlotID(slotID);
                battleCard.SetSpawnedSlotID(slotID);
                Data.SlotDatas[slotID].SetBattleCardUniqueID(battleCard.UniqueID);
            }
            else
            {
                //battleCard.ResetSlotID();
            }
            CardUIData cardUIData = new CardUIData(battleCard);
            // update UI
            switch (targetZone)
            {
                case CardZone.Deck:
                    {
                        // create card to deck
                        battlePlayerData.ShuffleDeck(false);
                    }
                    break;

                case CardZone.Hand:
                    {
                        // create card to hand

                        // create card UI
                        if (IsLocalPlayer(battleCard.Owner))
                        {
                            UIManager.Instance.RequestDrawCardToSlot(cardUIData, Karamucho.UI.CardUIManager.PlayerPosition.Bottom);
                        }
                        else
                        {
                            UIManager.Instance.RequestDrawCardToSlot(cardUIData, Karamucho.UI.CardUIManager.PlayerPosition.Top);
                        }
                    }
                    break;

                case CardZone.Battlefield:
                    {
                        // create card to summon
                        UIManager.Instance.RequestSpawnTokenToBoard(cardUIData, delegate
                        {
                            onComplete.Invoke(cardUIData.UniqueID);
                        });
                        if (battleCard.Type == CardType.Minion || battleCard.Type == CardType.Minion_NotInDeck)
                        {
                            ActionAddAttackerList(battleCard as MinionData); // add to attacker list
                            Data.AddBattleCardActiveID(battleCard.UniqueID); // add BattleCard list
                            ActionUpdateBuffSlotDarkMatter(slotID);
                        }
                        return;
                    }
                    break;

                case CardZone.Graveyard:
                    {
                        // create card to graveyard  
                    }
                    break;
            }

            // Update all player info
            UpdateInfoAllPlayers();

            if (onComplete != null)
            {
                onComplete.Invoke(battleCard.UniqueID);
            }
            return;
        }

        if (onComplete != null)
        {
            onComplete.Invoke(-1); // failed to create.
        }
    }

    public void ActionMoveMinionToSlot(int uniqueID, int slotID, UnityAction onComplete, bool isAdd)
    {
        bool isSuccess = false;
        MinionData minion = null;

        if (Data.SlotDatas != null && slotID >= 0 && slotID < Data.SlotDatas.Length)
        {
            isSuccess = FindMinionBattleCard(uniqueID, out minion);
            if (isSuccess)
            {
                if (minion.CurrentZone == CardZone.Battlefield && minion.SlotID != slotID)
                {
                    // Check is slot empty
                    if (Data.SlotDatas[slotID].IsSlotEmpty)
                    {
                        // Update Data
                        int oldSlotID = minion.SlotID;
                        if (oldSlotID >= 0 && oldSlotID < Data.SlotDatas.Length)
                        {
                            Data.SlotDatas[oldSlotID].ResetBattleCardUniqueID();
                        }

                        minion.SetSlotID(slotID);
                        Data.SlotDatas[slotID].SetBattleCardUniqueID(minion.UniqueID);

                        // Update UI
                        CardUIData cardUIData = new CardUIData(minion);
                        UIManager.Instance.RequestMoveTokenToSlot(cardUIData, slotID, isAdd);

                        ActionUpdateBuffSlotDarkMatter(slotID);

                        // Ready for battle after successfully move.
                        ActionAddAttackerList(minion);

                        OnMinionMoveToSlot?.Invoke(minion, slotID);
                    }
                }
            }
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionSpiritDamage(PlayerIndex playerIndex, int spiritDamage, UnityAction onComplete)
    {
        int realDamage = spiritDamage;
        if (Data.PlayerDataList[(int)playerIndex].IsImmune)
        {
            realDamage = 0;
        }

        // Count PlayerStat
        PlayerStatManager.Instance.DoDealDamageEnemyHeroEvent(playerIndex, realDamage, null);

        Data.PlayerDataList[(int)playerIndex].SetHPDeltaClamp(-realDamage);
        if (spiritDamage > 0)
        {
            UIManager.Instance.RequestSpiritAttackOnPlayer(spiritDamage, playerIndex);
            UpdateAllUIInfo();
        }

        if (Data.PlayerDataList[(int)playerIndex].CurrentHP <= 0)
        {
            TryNormalEndGame();
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionSpiritDamageTutorial(PlayerIndex playerIndex, int spiritDamage, UnityAction onComplete)
    {
        int realDamage = spiritDamage;
        if (Data.PlayerDataList[(int)playerIndex].IsImmune)
        {
            realDamage = 0;
        }

        // Count PlayerStat
        PlayerStatManager.Instance.DoDealDamageEnemyHeroEvent(playerIndex, realDamage, null);

        Data.PlayerDataList[(int)playerIndex].SetHPDeltaClamp(-realDamage);
        if (spiritDamage > 0)
        {
            UIManager.Instance.RequestSpiritAttackOnPlayerTutorial(spiritDamage, playerIndex);
            UpdateAllUIInfo();
        }

        if (Data.PlayerDataList[(int)playerIndex].CurrentHP <= 0)
        {
            TryNormalEndGame();
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionDrawDamage(PlayerIndex playerIndex, int spiritDamage, UnityAction onComplete)
    {
        int realDamage = spiritDamage;
        if (Data.PlayerDataList[(int)playerIndex].IsImmune)
        {
            realDamage = 0;
        }

        Data.PlayerDataList[(int)playerIndex].SetHPDeltaClamp(-realDamage);
        if (spiritDamage > 0)
        {
            UIManager.Instance.RequestDrawNoCardToDestroy(playerIndex);
            UpdateAllUIInfo();
            UIManager.Instance.RequestShowPopupDamageOnPlayer(spiritDamage, playerIndex);
        }

        if (Data.PlayerDataList[(int)playerIndex].CurrentHP <= 0)
        {
            TryNormalEndGame();
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionRestoreLifePointDelta(PlayerIndex playerIndex, int value, UnityAction onComplete)
    {
        if (Data.PlayerDataList[(int)playerIndex].CurrentHP < Lifepoint)
        {
            Data.PlayerDataList[(int)playerIndex].SetHPDeltaClamp(value);
            if (value > 0)
            {
                UIManager.Instance.RequestShowPopupHealOnPlayer(value, playerIndex);
                UpdateAllUIInfo();
            }

            if (Data.PlayerDataList[(int)playerIndex].CurrentHP <= 0)
            {
                TryNormalEndGame();
            }
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    #region Deal Damage
    public void ActionDealDamageMinion(Requester requester, int targetID, int damage, UnityAction onComplete)
    {
        MinionData minion;
        bool isClearFreeze = false;
        if (FindMinionBattleCard(targetID, out minion))
        {
            CardUIData uiDataAttacker = null;
            //Debug.LogWarning(requester.Type);
            switch (requester.Type)
            {
                case Requester.RequesterType.Minion:
                    {
                        uiDataAttacker = new CardUIData(requester.Minion);
                    }
                    break;
            }

            int minionHP = minion.HPCurrent;

            // if damage < 0
            if (damage < 0)
            {
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
                return;
            }
            // if damage >= 0
            else
            {
                // override damage
                if (minion.IsBeOverrideEffectDamage)
                {
                    damage = minion.OverrideEffectDamage;
                }

                // if minion is be effect invincible
                if (minion.IsBeEffectInvincible)
                {
                    damage = 0;
                }
                minion.ModifyHP(new HPStatData(HPModifyKey.SET_CURRENT_DELTA, -damage));
            }

            CardUIData uiDataDefender = new CardUIData(minion);
            if (uiDataDefender == null)
            {
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
                return;
            }

            #region Update Trigger Effect
            // check to destroy freeze status
            if (minionHP > minion.HPCurrent)
            {
                isClearFreeze = true;

                Requester triggerer = new Requester(minion);
                List<EventTriggerData> triggerList = new List<EventTriggerData>();
                triggerList.Add(new EventTriggerData(EventKey.OnMeDamaged, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnFriendDamaged, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnEnemyDamaged, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnMeDamagedByEffect, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnFriendDamagedByEffect, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnEnemyDamagedByEffect, triggerer));
                if (Phase == GamePhase.Battle)
                {
                    triggerList.Add(new EventTriggerData(EventKey.OnMeDamagedInBattle, triggerer));
                    triggerList.Add(new EventTriggerData(EventKey.OnFriendDamagedInBattle, triggerer));
                    triggerList.Add(new EventTriggerData(EventKey.OnEnemyDamagedInBattle, triggerer));
                }
                else
                {
                    triggerList.Add(new EventTriggerData(EventKey.OnMeDamagedOutBattle, triggerer));
                    triggerList.Add(new EventTriggerData(EventKey.OnFriendDamagedOutBattle, triggerer));
                    triggerList.Add(new EventTriggerData(EventKey.OnEnemyDamagedOutBattle, triggerer));
                }

                ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
            }
            #endregion

            // Update UI
            UIManager.Instance.ShowPopupDamageOnCard(damage, uiDataDefender, null);
            UIManager.Instance.UpdateCardUI(uiDataDefender, null);

            // On Complete
            if (
                    minion.IsAlive
                && (minion.ActiveZone == CardZone.Battlefield)
                && minion.IsBeFreeze
                && isClearFreeze
                )
            {
                minion.RemoveBuff(MinionBuffTypes.BuffMinionFreeze, onComplete);
                return;
            }
            else
            {
                onComplete?.Invoke();
            }
        }
    }

    public void ActionDestroyMinion(Requester requester, int targetID, UnityAction onComplete)
    {
        MinionData minion;
        if (FindMinionBattleCard(targetID, out minion))
        {
            CardUIData uiDataAttacker = null;
            //Debug.LogWarning(requester.Type);
            switch (requester.Type)
            {
                case Requester.RequesterType.Minion:
                    {
                        uiDataAttacker = new CardUIData(requester.Minion);
                    }
                    break;
            }

            int damage = minion.HPCurrent;
            minion.ModifyHPByIgnoreDamageImmune(new HPStatData(HPModifyKey.SET_CURRENT_DELTA, -minion.HPCurrent));

            CardUIData uiDataDefender = new CardUIData(minion);
            if (uiDataDefender == null)
            {
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
                return;
            }

            #region Update Trigger Effect
            Requester triggerer = new Requester(minion);
            List<EventTriggerData> triggerList = new List<EventTriggerData>();
            triggerList.Add(new EventTriggerData(EventKey.OnMeDestroyed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnFriendDestroyed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnEnemyDestroyed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnMinionDestroyed, triggerer));

            ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
            #endregion

            // Update UI
            UIManager.Instance.ShowPopupDamageOnCard(damage, uiDataDefender, null);
            UIManager.Instance.UpdateCardUI(uiDataDefender, null);

            onComplete?.Invoke();

        }
    }

    public void ActionSacrificeMinion(Requester requester, int targetID, UnityAction onComplete)
    {
        MinionData minion;
        if (FindMinionBattleCard(targetID, out minion))
        {
            CardUIData uiDataAttacker = null;
            //Debug.LogWarning(requester.Type);
            switch (requester.Type)
            {
                case Requester.RequesterType.Minion:
                    {
                        uiDataAttacker = new CardUIData(requester.Minion);
                    }
                    break;
            }
            int damage = minion.HPCurrent;
            minion.ModifyHPByIgnoreDamageImmune(new HPStatData(HPModifyKey.SET_CURRENT_DELTA, -minion.HPCurrent));

            CardUIData uiDataDefender = new CardUIData(minion);
            if (uiDataDefender == null)
            {
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
                return;
            }

            #region Trigger Update
            Requester triggerer = new Requester(minion);
            List<EventTriggerData> triggerList = new List<EventTriggerData>();
            triggerList.Add(new EventTriggerData(EventKey.OnMeSacrificed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnFriendSacrificed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnMinionSacrificed, triggerer));

            ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
            #endregion

            // Update UI
            UIManager.Instance.ShowPopupDamageOnCard(damage, uiDataDefender, null);
            UIManager.Instance.UpdateCardUI(uiDataDefender, null);

            onComplete?.Invoke();

        }
    }

    public void ActionDealDamagePlayer(Requester requester, PlayerIndex player, int spiritDamage, UnityAction onComplete)
    {
        CardUIData uiDataAttacker = null;
        switch (requester.Type)
        {
            case Requester.RequesterType.Minion:
                {
                    uiDataAttacker = new CardUIData(requester.Minion);
                }
                break;
        }

        int realDamage = spiritDamage;
        if (Data.PlayerDataList[(int)player].IsImmune)
        {
            realDamage = 0;
        }
        Data.PlayerDataList[(int)player].SetHPDeltaClamp(-realDamage);

        // Update UI
        UpdateAllUIInfo();

        if (Data.PlayerDataList[(int)player].CurrentHP <= 0)
        {
            TryNormalEndGame();
        }

        UIManager.Instance.ShowTauntAnimation(player, BaseHeroAnimation.HeroAnimationState.HIT);
        UIManager.Instance.ShowPopupDamageOnPlayer(realDamage, player, null);

        onComplete?.Invoke();
    }
    #endregion

    public void ActionSetHPMinion(Requester requester, int targetID, int value, UnityAction onComplete)
    {
        MinionData minion;
        if (FindMinionBattleCard(targetID, out minion))
        {
            int hp = minion.HPCurrent;
            minion.ModifyHP(new HPStatData(HPModifyKey.SET_ALL_TO, value));
            CardUIData uiBattleCard = new CardUIData(minion);
            if (value >= hp)
            {
                UIManager.Instance.RequestBoostActivate(uiBattleCard);
            }
            else
            {
                UIManager.Instance.RequestWeakenActivate(uiBattleCard);
            }
            UIManager.Instance.RequestUpdateCardUI(uiBattleCard, false);

            // Add battle log.
            {
                SetHPLog log = new SetHPLog(requester, minion, value);
                ActionAddBattleLog(log);
            }

            Requester triggerer = new Requester(minion);
            List<EventTriggerData> triggerList = new List<EventTriggerData>();

            triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer));
            ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);

            if (minion.IsDead
            && (Phase != GamePhase.Battle
                || (Phase == GamePhase.Battle
                    && BattleManager.Instance.Phase >= BattleManager.BattlePhase.Solve)))
            {
                minion.SetKillerPlayerIndex(requester.PlayerIndex);
                ActionDeadMinion(new List<int>() { targetID }, true, MinionDeathTypes.Effect, onComplete);
                return;
            }
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionSetAPMinion(Requester requester, int targetID, int value, UnityAction onComplete)
    {
        MinionData minion;
        if (FindMinionBattleCard(targetID, out minion))
        {
            int ap = minion.ATKCurrent;
            minion.ModifyATK(new ATKStatData(ATKModifyKey.SET_TO, value));
            CardUIData uiBattleCard = new CardUIData(minion);
            if (value >= ap)
            {
                UIManager.Instance.RequestBoostActivate(uiBattleCard);
            }
            else
            {
                UIManager.Instance.RequestWeakenActivate(uiBattleCard);
            }
            UIManager.Instance.RequestUpdateCardUI(uiBattleCard, false);

            // Add battle log.
            {
                SetAPLog log = new SetAPLog(requester, minion, value);
                ActionAddBattleLog(log);
            }

            Requester triggerer = new Requester(minion);
            List<EventTriggerData> triggerList = new List<EventTriggerData>();

            triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer));
            ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionSetSPMinion(Requester requester, int targetID, int value, UnityAction onComplete)
    {
        MinionData minion;
        if (FindMinionBattleCard(targetID, out minion))
        {
            int sp = minion.SpiritCurrent;
            minion.ModifySpirit(new SpiritStatData(SpiritModifyKey.SET_TO, value));
            CardUIData uiBattleCard = new CardUIData(minion);
            if (value >= sp)
            {
                UIManager.Instance.RequestBoostActivate(uiBattleCard);
            }
            else
            {
                UIManager.Instance.RequestWeakenActivate(uiBattleCard);
            }
            UIManager.Instance.RequestUpdateCardUI(uiBattleCard, false);
            UpdateAllUIInfo();

            // Add battle log.
            {
                SetSPLog log = new SetSPLog(requester, minion, value);
                ActionAddBattleLog(log);
            }

            // Count PlayerStat
            PlayerStatManager.Instance.DoActCheckMinionSPEvent(minion, null);

            Requester triggerer = new Requester(minion);
            List<EventTriggerData> triggerList = new List<EventTriggerData>();

            triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer));
            ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionSetArmorMinion(Requester requester, int targetID, int value, UnityAction onComplete)
    {
        MinionData minion;
        if (FindMinionBattleCard(targetID, out minion))
        {
            int armor = minion.ArmorCurrent;
            minion.ModifyArmor(new ArmorStatData(ArmorModifyKey.SET_TO, value));
            CardUIData uiBattleCard = new CardUIData(minion);
            if (value >= armor)
            {
                UIManager.Instance.RequestBoostActivate(uiBattleCard);
            }
            else
            {
                UIManager.Instance.RequestWeakenActivate(uiBattleCard);
            }
            UIManager.Instance.RequestUpdateCardUI(uiBattleCard, false);

            Requester triggerer = new Requester(minion);
            List<EventTriggerData> triggerList = new List<EventTriggerData>();

            triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer));
            ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionSetAPHPMinion(Requester requester, int targetID, int apValue, int hpValue, UnityAction onComplete)
    {
        MinionData minion;
        if (FindMinionBattleCard(targetID, out minion))
        {
            int ap = minion.ATKCurrent;
            int hp = minion.HPCurrent;
            minion.ModifyATK(new ATKStatData(ATKModifyKey.SET_TO, apValue));
            minion.ModifyHP(new HPStatData(HPModifyKey.SET_ALL_TO, hpValue));
            CardUIData uiBattleCard = new CardUIData(minion);
            if (apValue >= ap || hpValue >= hp)
            {
                UIManager.Instance.RequestBoostActivate(uiBattleCard);
            }
            else
            {
                UIManager.Instance.RequestWeakenActivate(uiBattleCard);
            }
            UIManager.Instance.RequestUpdateCardUI(uiBattleCard, false);

            // Add battle log.
            {
                SetAPHPLog log = new SetAPHPLog(requester, minion, apValue, hpValue);
                ActionAddBattleLog(log);
            }

            Requester triggerer = new Requester(minion);
            List<EventTriggerData> triggerList = new List<EventTriggerData>();

            triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer));
            ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);

            if (minion.IsDead
            && (Phase != GamePhase.Battle
                || (Phase == GamePhase.Battle
                    && BattleManager.Instance.Phase >= BattleManager.BattlePhase.Solve)))
            {
                minion.SetKillerPlayerIndex(requester.PlayerIndex);
                ActionDeadMinion(new List<int>() { targetID }, true, MinionDeathTypes.Effect, onComplete);
                return;
            }
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionBoostHPMinion(Requester requester, int targetID, int value, UnityAction onComplete)
    {
        MinionData minion;
        if (FindMinionBattleCard(targetID, out minion))
        {
            if (minion.ActiveZone != CardZone.Battlefield)
            {
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
                return;
            }

            CardUIData uiBattleCard = new CardUIData(minion);
            if (value > 0)
            {
                if (minion.IsCanAbilityActive())
                {
                    minion.ModifyHP(new HPStatData(HPModifyKey.BOOST_DELTA, value));
                    UIManager.Instance.RequestBoostActivate(uiBattleCard);
                }
                else
                {
                    onComplete?.Invoke();
                    return;
                }
            }
            else if (value < 0)
            {
                minion.ModifyHP(new HPStatData(HPModifyKey.BOOST_DELTA, value));
                UIManager.Instance.RequestWeakenActivate(uiBattleCard);
            }
            UIManager.Instance.RequestUpdateCardUI(new CardUIData(minion), false);

            // Add battle log.
            {
                BoostHPLog log = new BoostHPLog(requester, minion, value);
                ActionAddBattleLog(log);
            }

            Requester triggerer = new Requester(minion);
            List<EventTriggerData> triggerList = new List<EventTriggerData>();

            if (value > 0 && minion.IsAlive)
            {
                triggerList.Add(new EventTriggerData(EventKey.OnMeHPIncrease, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnFriendHPIncrease, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnEnemyHPIncrease, triggerer));
            }

            triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer));
            ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);

            if (minion.IsDead
            && (Phase != GamePhase.Battle
                || (Phase == GamePhase.Battle
                    && BattleManager.Instance.Phase >= BattleManager.BattlePhase.Solve)))
            {
                minion.SetKillerPlayerIndex(requester.PlayerIndex);
                ActionDeadMinion(new List<int>() { targetID }, true, MinionDeathTypes.Effect, onComplete);
                return;
            }
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionBoostAPMinion(Requester requester, int targetID, int value, UnityAction onComplete)
    {
        MinionData minion;
        if (FindMinionBattleCard(targetID, out minion))
        {
            if (minion.ActiveZone != CardZone.Battlefield)
            {
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
                return;
            }

            minion.ModifyATK(new ATKStatData(ATKModifyKey.BOOST_DELTA, value));
            CardUIData uiBattleCard = new CardUIData(minion);
            if (value > 0)
            {
                UIManager.Instance.RequestBoostActivate(uiBattleCard);
            }
            else if (value < 0)
            {
                UIManager.Instance.RequestWeakenActivate(uiBattleCard);
            }
            UIManager.Instance.RequestUpdateCardUI(uiBattleCard, false);

            // Add battle log.
            {
                BoostAPLog log = new BoostAPLog(requester, minion, value);
                ActionAddBattleLog(log);
            }

            Requester triggerer = new Requester(minion);
            List<EventTriggerData> triggerList = new List<EventTriggerData>();

            if (value > 0 && minion.IsAlive)
            {
                triggerList.Add(new EventTriggerData(EventKey.OnMeAPIncrease, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnFriendAPIncrease, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnEnemyAPIncrease, triggerer));
            }

            triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer));
            ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionBoostAPMinionCritical(Requester requester, int targetID, int value, UnityAction onComplete)
    {
        MinionData minion;
        if (FindMinionBattleCard(targetID, out minion))
        {
            if (minion.ActiveZone != CardZone.Battlefield)
            {
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
                return;
            }

            minion.ModifyATK(new ATKStatData(ATKModifyKey.BOOST_DELTA, value));
            CardUIData uiBattleCard = new CardUIData(minion);
            UIManager.Instance.RequestUpdateCardUI(uiBattleCard, false);

            // Add battle log.
            {
                BoostAPLog log = new BoostAPLog(requester, minion, value);
                ActionAddBattleLog(log);
            }

            Requester triggerer = new Requester(minion);
            List<EventTriggerData> triggerList = new List<EventTriggerData>();
            triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer));
            ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionBoostSPMinion(Requester requester, int targetID, int value, UnityAction onComplete)
    {
        MinionData minion;
        if (FindMinionBattleCard(targetID, out minion))
        {
            if (minion.ActiveZone != CardZone.Battlefield)
            {
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
                return;
            }

            minion.ModifySpirit(new SpiritStatData(SpiritModifyKey.BOOST_DELTA, value));
            CardUIData uiBattleCard = new CardUIData(minion);
            if (value > 0)
            {
                UIManager.Instance.RequestBoostActivate(uiBattleCard);
            }
            else if (value < 0)
            {
                UIManager.Instance.RequestWeakenActivate(uiBattleCard);
            }
            UIManager.Instance.RequestUpdateCardUI(uiBattleCard, false);
            UpdateAllUIInfo(false);

            // Add battle log.
            {
                BoostSPLog log = new BoostSPLog(requester, minion, value);
                ActionAddBattleLog(log);
            }

            // Count PlayerStat
            PlayerStatManager.Instance.DoActCheckMinionSPEvent(minion, null);

            Requester triggerer = new Requester(minion);
            List<EventTriggerData> triggerList = new List<EventTriggerData>();
            triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer));
            ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionBoostAPHPMinion(Requester requester, int targetID, int ap, int hp, UnityAction onComplete)
    {
        MinionData minion;
        if (FindMinionBattleCard(targetID, out minion))
        {
            if (minion.ActiveZone != CardZone.Battlefield)
            {
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
                return;
            }

            CardUIData uiBattleCard = new CardUIData(minion);
            if (ap >= 0 || hp >= 0)
            {
                if (minion.IsCanAbilityActive())
                {
                    minion.ModifyATK(new ATKStatData(ATKModifyKey.BOOST_DELTA, ap));
                    minion.ModifyHP(new HPStatData(HPModifyKey.BOOST_DELTA, hp));
                    UIManager.Instance.RequestBoostActivate(uiBattleCard);
                }
                else
                {
                    onComplete?.Invoke();
                    return;
                }
            }
            else
            {
                minion.ModifyATK(new ATKStatData(ATKModifyKey.BOOST_DELTA, ap));
                minion.ModifyHP(new HPStatData(HPModifyKey.BOOST_DELTA, hp));
                UIManager.Instance.RequestWeakenActivate(uiBattleCard);
            }
            UIManager.Instance.RequestUpdateCardUI(new CardUIData(minion), false);

            // Add battle log.
            {
                BoostAPHPLog log = new BoostAPHPLog(requester, minion, ap, hp);
                ActionAddBattleLog(log);
            }

            Requester triggerer = new Requester(minion);
            List<EventTriggerData> triggerList = new List<EventTriggerData>();

            if (hp > 0 && minion.IsAlive)
            {
                triggerList.Add(new EventTriggerData(EventKey.OnMeHPIncrease, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnFriendHPIncrease, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnEnemyHPIncrease, triggerer));
            }
            if (ap > 0 && minion.IsAlive)
            {
                triggerList.Add(new EventTriggerData(EventKey.OnMeAPIncrease, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnFriendAPIncrease, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnEnemyAPIncrease, triggerer));
            }

            triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer));
            ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);

            if (minion.IsDead
            && (Phase != GamePhase.Battle
                || (Phase == GamePhase.Battle
                    && BattleManager.Instance.Phase >= BattleManager.BattlePhase.Solve)))
            {
                minion.SetKillerPlayerIndex(requester.PlayerIndex);
                ActionDeadMinion(new List<int>() { targetID }, true, MinionDeathTypes.Effect, onComplete);
                return;
            }
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionBoostArmorMinion(Requester requester, int targetID, int armor, UnityAction onComplete)
    {
        MinionData minion;
        if (FindMinionBattleCard(targetID, out minion))
        {
            if (minion.ActiveZone != CardZone.Battlefield)
            {
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
                return;
            }

            minion.ModifyArmor(new ArmorStatData(ArmorModifyKey.BOOST_DELTA, armor));
            CardUIData uiBattleCard = new CardUIData(minion);
            if (armor > 0)
            {
                UIManager.Instance.RequestBoostActivate(uiBattleCard);
            }
            else if (armor < 0)
            {
                UIManager.Instance.RequestWeakenActivate(uiBattleCard);
            }
            UIManager.Instance.RequestUpdateCardUI(uiBattleCard, false);

            // Add battle log.
            {
                BoostArmorLog log = new BoostArmorLog(requester, minion, armor);
                ActionAddBattleLog(log);
            }

            Requester triggerer = new Requester(minion);
            List<EventTriggerData> triggerList = new List<EventTriggerData>();

            if (armor > 0 && minion.IsAlive)
            {
                triggerList.Add(new EventTriggerData(EventKey.OnMeArmorIncrease, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnFriendArmorIncrease, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnEnemyArmorIncrease, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnMeArmorIncreaseByBoost, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnFriendArmorIncreaseByBoost, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnEnemyArmorIncreaseByBoost, triggerer));
            }

            triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer));
            ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionBoostBuffPassiveHPMinion(Requester requester, int targetID, int value, UnityAction onComplete)
    {
        MinionData minion;
        if (FindMinionBattleCard(targetID, out minion))
        {
            if (minion.ActiveZone != CardZone.Battlefield)
            {
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
                return;
            }

            minion.ModifyBuffPassiveHP(value);
            CardUIData uiBattleCard = new CardUIData(minion);
            if (value > 0)
            {
                UIManager.Instance.RequestBoostActivate(uiBattleCard);
            }
            else if (value < 0)
            {
                UIManager.Instance.RequestWeakenActivate(uiBattleCard);
            }
            UIManager.Instance.RequestUpdateCardUI(uiBattleCard, false);

            // Add battle log.
            {
                BoostHPLog log = new BoostHPLog(requester, minion, value);
                ActionAddBattleLog(log);
            }

            Requester triggerer = new Requester(minion);
            List<EventTriggerData> triggerList = new List<EventTriggerData>();

            if (value > 0)
            {
                triggerList.Add(new EventTriggerData(EventKey.OnMeHPIncrease, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnFriendHPIncrease, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnEnemyHPIncrease, triggerer));
            }

            triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer));
            ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);

            if (minion.IsDead
            && (Phase != GamePhase.Battle
                || (Phase == GamePhase.Battle
                    && BattleManager.Instance.Phase >= BattleManager.BattlePhase.Solve)))
            {
                minion.SetKillerPlayerIndex(requester.PlayerIndex);
                ActionDeadMinion(new List<int>() { targetID }, true, MinionDeathTypes.Effect, onComplete);
                return;
            }
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionBoostBuffPassiveAPMinion(Requester requester, int targetID, int value, UnityAction onComplete)
    {
        MinionData minion;
        if (FindMinionBattleCard(targetID, out minion))
        {
            if (minion.ActiveZone != CardZone.Battlefield)
            {
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
                return;
            }

            minion.ModifyBuffPassiveATK(value);
            CardUIData uiBattleCard = new CardUIData(minion);
            if (value > 0)
            {
                UIManager.Instance.RequestBoostActivate(uiBattleCard);
            }
            else if (value < 0)
            {
                UIManager.Instance.RequestWeakenActivate(uiBattleCard);
            }
            UIManager.Instance.RequestUpdateCardUI(uiBattleCard, false);

            // Add battle log.
            {
                BoostAPLog log = new BoostAPLog(requester, minion, value);
                ActionAddBattleLog(log);
            }

            Requester triggerer = new Requester(minion);
            List<EventTriggerData> triggerList = new List<EventTriggerData>();

            if (value > 0)
            {
                triggerList.Add(new EventTriggerData(EventKey.OnMeAPIncrease, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnFriendAPIncrease, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnEnemyAPIncrease, triggerer));
            }

            triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer));
            ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionBoostBuffPassiveAPCriticalMinion(Requester requester, int targetID, int value, UnityAction onComplete)
    {
        MinionData minion;
        if (FindMinionBattleCard(targetID, out minion))
        {
            if (minion.ActiveZone != CardZone.Battlefield)
            {
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
                return;
            }

            minion.ModifyBuffPassiveATK(value);
            CardUIData uiBattleCard = new CardUIData(minion);
            UIManager.Instance.RequestUpdateCardUI(uiBattleCard, false);

            // Add battle log.
            {
                BoostAPLog log = new BoostAPLog(requester, minion, value);
                ActionAddBattleLog(log);
            }

            Requester triggerer = new Requester(minion);
            List<EventTriggerData> triggerList = new List<EventTriggerData>();
            triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer));
            ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionBoostBuffPassiveSPMinion(Requester requester, int targetID, int value, UnityAction onComplete)
    {
        MinionData minion;
        if (FindMinionBattleCard(targetID, out minion))
        {
            if (minion.ActiveZone != CardZone.Battlefield)
            {
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
                return;
            }

            minion.ModifyBuffPassiveSpirit(value);
            CardUIData uiBattleCard = new CardUIData(minion);
            if (value > 0)
            {
                UIManager.Instance.RequestBoostActivate(uiBattleCard);
            }
            else if (value < 0)
            {
                UIManager.Instance.RequestWeakenActivate(uiBattleCard);
            }
            UIManager.Instance.RequestUpdateCardUI(uiBattleCard, false);
            UpdateAllUIInfo(false);

            // Add battle log.
            {
                BoostSPLog log = new BoostSPLog(requester, minion, value);
                ActionAddBattleLog(log);
            }

            // Count PlayerStat
            PlayerStatManager.Instance.DoActCheckMinionSPEvent(minion, null);

            Requester triggerer = new Requester(minion);
            List<EventTriggerData> triggerList = new List<EventTriggerData>();
            triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer));
            ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionBoostBuffPassiveAPHPMinion(Requester requester, int targetID, int ap, int hp, UnityAction onComplete)
    {
        MinionData minion;
        if (FindMinionBattleCard(targetID, out minion))
        {
            if (minion.ActiveZone != CardZone.Battlefield)
            {
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
                return;
            }

            minion.ModifyBuffPassiveATK(ap);
            minion.ModifyBuffPassiveHP(hp);
            CardUIData uiBattleCard = new CardUIData(minion);
            if (ap >= 0 || hp >= 0)
            {
                UIManager.Instance.RequestBoostActivate(uiBattleCard);
            }
            else
            {
                UIManager.Instance.RequestWeakenActivate(uiBattleCard);
            }
            UIManager.Instance.RequestUpdateCardUI(uiBattleCard, false);

            // Add battle log.
            {
                BoostAPHPLog log = new BoostAPHPLog(requester, minion, ap, hp);
                ActionAddBattleLog(log);
            }

            Requester triggerer = new Requester(minion);
            List<EventTriggerData> triggerList = new List<EventTriggerData>();

            if (hp > 0)
            {
                triggerList.Add(new EventTriggerData(EventKey.OnMeHPIncrease, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnFriendHPIncrease, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnEnemyHPIncrease, triggerer));
            }
            if (ap > 0)
            {
                triggerList.Add(new EventTriggerData(EventKey.OnMeAPIncrease, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnFriendAPIncrease, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnEnemyAPIncrease, triggerer));
            }

            triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer));
            ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);

            if (minion.IsDead
                && (Phase != GamePhase.Battle
                    || (Phase == GamePhase.Battle
                        && BattleManager.Instance.Phase >= BattleManager.BattlePhase.Solve)))
            {
                minion.SetKillerPlayerIndex(requester.PlayerIndex);
                ActionDeadMinion(new List<int>() { targetID }, true, MinionDeathTypes.Effect, onComplete);
                return;
            }
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionBoostBuffPassiveArmorMinion(Requester requester, int targetID, int armor, UnityAction onComplete)
    {
        MinionData minion;
        if (FindMinionBattleCard(targetID, out minion))
        {
            if (minion.ActiveZone != CardZone.Battlefield)
            {
                if (onComplete != null)
                {
                    onComplete.Invoke();
                }
                return;
            }

            minion.ModifyBuffPassiveArmor(armor);
            CardUIData uiBattleCard = new CardUIData(minion);
            if (armor > 0)
            {
                UIManager.Instance.RequestBoostActivate(uiBattleCard);
            }
            else if (armor < 0)
            {
                UIManager.Instance.RequestWeakenActivate(uiBattleCard);
            }
            UIManager.Instance.RequestUpdateCardUI(uiBattleCard, false);

            // Add battle log.
            {
                BoostArmorLog log = new BoostArmorLog(requester, minion, armor);
                ActionAddBattleLog(log);
            }

            Requester triggerer = new Requester(minion);
            List<EventTriggerData> triggerList = new List<EventTriggerData>();

            if (armor > 0)
            {
                triggerList.Add(new EventTriggerData(EventKey.OnMeArmorIncrease, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnFriendArmorIncrease, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnEnemyArmorIncrease, triggerer));
            }

            triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer));
            ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionBoostHeroSP(Requester requester, PlayerIndex playerTarget, int value, UnityAction onComplete)
    {
        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionSwapHPMinion(Requester requester, int minionUniqueID_1, int minionUniqueID_2, UnityAction onComplete)
    {
        bool isSuccess = true;
        MinionData minion_1;
        MinionData minion_2;
        isSuccess &= FindMinionBattleCard(minionUniqueID_1, out minion_1);
        isSuccess &= FindMinionBattleCard(minionUniqueID_2, out minion_2);
        if (isSuccess)
        {
            MinionData minionTmp = new MinionData(minion_1);
            minion_1.ModifyHP(new HPStatData(HPModifyKey.SET_DEFAULT_TO, minion_2.HPDefault)
                             , new HPStatData(HPModifyKey.SET_MAX_TO, minion_2.HPMax)
                             , new HPStatData(HPModifyKey.SET_CURRENT_TO, minion_2.HPCurrent));
            minion_2.ModifyHP(new HPStatData(HPModifyKey.SET_DEFAULT_TO, minionTmp.HPDefault)
                             , new HPStatData(HPModifyKey.SET_MAX_TO, minionTmp.HPMax)
                             , new HPStatData(HPModifyKey.SET_CURRENT_TO, minionTmp.HPCurrent));
            CardUIData uiBattleCard_1 = new CardUIData(minion_1);
            CardUIData uiBattleCard_2 = new CardUIData(minion_2);
            UIManager.Instance.RequestBoostActivate(uiBattleCard_1);
            UIManager.Instance.RequestUpdateCardUI(uiBattleCard_1, false);
            UIManager.Instance.RequestWeakenActivate(uiBattleCard_2, false);
            UIManager.Instance.RequestUpdateCardUI(uiBattleCard_2, false);

            // Add battle log.
            {
                SwapHPLog log = new SwapHPLog(
                      requester
                    , minion_1, minion_2.HPCurrent
                    , minion_2, minion_1.HPCurrent
                );
                ActionAddBattleLog(log);
            }

            if (requester.PlayerIndex == minion_1.Owner)
            {
                {
                    Requester triggerer_1 = new Requester(minion_1);
                    List<EventTriggerData> triggerList = new List<EventTriggerData>();
                    triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer_1));
                    triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer_1));
                    triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer_1));
                    ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
                }
                {
                    Requester triggerer_2 = new Requester(minion_2);
                    List<EventTriggerData> triggerList = new List<EventTriggerData>();
                    triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer_2));
                    triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer_2));
                    triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer_2));
                    ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
                }
            }
            else
            {
                {
                    Requester triggerer_2 = new Requester(minion_2);
                    List<EventTriggerData> triggerList = new List<EventTriggerData>();
                    triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer_2));
                    triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer_2));
                    triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer_2));
                    ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
                }
                {
                    Requester triggerer_1 = new Requester(minion_1);
                    List<EventTriggerData> triggerList = new List<EventTriggerData>();
                    triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer_1));
                    triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer_1));
                    triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer_1));
                    ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
                }
            }

            List<int> deadTargetList = new List<int>();
            if (minion_1.IsDead)
            {
                minion_1.SetKillerPlayerIndex(requester.PlayerIndex);
                deadTargetList.Add(minionUniqueID_1);
            }

            if (minion_2.IsDead)
            {
                minion_2.SetKillerPlayerIndex(requester.PlayerIndex);
                deadTargetList.Add(minionUniqueID_2);
            }

            if (deadTargetList.Count > 0)
            {
                ActionDeadMinion(deadTargetList, true, MinionDeathTypes.Effect, onComplete);
                return;
            }
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionSwapAPMinion(Requester requester, int minionUniqueID_1, int minionUniqueID_2, UnityAction onComplete)
    {
        bool isSucces = true;
        MinionData minion_1;
        MinionData minion_2;
        isSucces &= FindMinionBattleCard(minionUniqueID_1, out minion_1);
        isSucces &= FindMinionBattleCard(minionUniqueID_2, out minion_2);
        if (isSucces)
        {
            MinionData minionTmp = new MinionData(minion_1);
            minion_1.ModifyATK(new ATKStatData(ATKModifyKey.SET_TO, minion_2.ATKCurrent));
            minion_2.ModifyATK(new ATKStatData(ATKModifyKey.SET_TO, minionTmp.ATKCurrent));
            CardUIData uiBattleCard_1 = new CardUIData(minion_1);
            CardUIData uiBattleCard_2 = new CardUIData(minion_2);
            UIManager.Instance.RequestBoostActivate(uiBattleCard_1);
            UIManager.Instance.RequestUpdateCardUI(uiBattleCard_1, false);
            UIManager.Instance.RequestWeakenActivate(uiBattleCard_2, false);
            UIManager.Instance.RequestUpdateCardUI(uiBattleCard_2, false);

            // Add battle log.
            {
                SwapAPLog log = new SwapAPLog(
                      requester
                    , minion_1, minion_2.ATKCurrent
                    , minion_2, minion_1.ATKCurrent
                );
                ActionAddBattleLog(log);
            }

            if (requester.PlayerIndex == minion_1.Owner)
            {
                {
                    Requester triggerer_1 = new Requester(minion_1);
                    List<EventTriggerData> triggerList = new List<EventTriggerData>();
                    triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer_1));
                    triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer_1));
                    triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer_1));
                    ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
                }
                {
                    Requester triggerer_2 = new Requester(minion_2);
                    List<EventTriggerData> triggerList = new List<EventTriggerData>();
                    triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer_2));
                    triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer_2));
                    triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer_2));
                    ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
                }
            }
            else
            {
                {
                    Requester triggerer_2 = new Requester(minion_2);
                    List<EventTriggerData> triggerList = new List<EventTriggerData>();
                    triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer_2));
                    triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer_2));
                    triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer_2));
                    ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
                }
                {
                    Requester triggerer_1 = new Requester(minion_1);
                    List<EventTriggerData> triggerList = new List<EventTriggerData>();
                    triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer_1));
                    triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer_1));
                    triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer_1));
                    ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
                }
            }
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionSwapSPMinion(Requester requester, int minionUniqueID_1, int minionUniqueID_2, UnityAction onComplete)
    {
        bool isSucces = true;
        MinionData minion_1;
        MinionData minion_2;
        isSucces &= FindMinionBattleCard(minionUniqueID_1, out minion_1);
        isSucces &= FindMinionBattleCard(minionUniqueID_2, out minion_2);
        if (isSucces)
        {
            MinionData minionTmp = new MinionData(minion_1);
            minion_1.ModifySpirit(new SpiritStatData(SpiritModifyKey.SET_TO, minion_2.SpiritCurrent));
            minion_2.ModifySpirit(new SpiritStatData(SpiritModifyKey.SET_TO, minionTmp.SpiritCurrent));
            CardUIData uiBattleCard_1 = new CardUIData(minion_1);
            CardUIData uiBattleCard_2 = new CardUIData(minion_2);
            UIManager.Instance.RequestBoostActivate(uiBattleCard_1);
            UIManager.Instance.RequestUpdateCardUI(uiBattleCard_1, false);
            UIManager.Instance.RequestWeakenActivate(uiBattleCard_2, false);
            //UIManager.Instance.ShowPopupHealOnCard(uiBattleCard, value);
            UIManager.Instance.RequestUpdateCardUI(uiBattleCard_2, false);

            // Add battle log.
            {
                SwapSPLog log = new SwapSPLog(
                      requester
                    , minion_1, minion_2.SpiritCurrent
                    , minion_2, minion_1.SpiritCurrent
                );
                ActionAddBattleLog(log);
            }

            // Count PlayerStat
            PlayerStatManager.Instance.DoActCheckMinionSPEvent(minion_1, null);
            PlayerStatManager.Instance.DoActCheckMinionSPEvent(minion_2, null);

            if (requester.PlayerIndex == minion_1.Owner)
            {
                {
                    Requester triggerer_1 = new Requester(minion_1);
                    List<EventTriggerData> triggerList = new List<EventTriggerData>();
                    triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer_1));
                    triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer_1));
                    triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer_1));
                    ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
                }
                {
                    Requester triggerer_2 = new Requester(minion_2);
                    List<EventTriggerData> triggerList = new List<EventTriggerData>();
                    triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer_2));
                    triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer_2));
                    triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer_2));
                    ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
                }
            }
            else
            {
                {
                    Requester triggerer_2 = new Requester(minion_2);
                    List<EventTriggerData> triggerList = new List<EventTriggerData>();
                    triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer_2));
                    triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer_2));
                    triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer_2));
                    ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
                }
                {
                    Requester triggerer_1 = new Requester(minion_1);
                    List<EventTriggerData> triggerList = new List<EventTriggerData>();
                    triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer_1));
                    triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer_1));
                    triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer_1));
                    ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
                }
            }
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionSwapAPHPMinion(Requester requester, int minionUniqueID_1, int minionUniqueID_2, UnityAction onComplete)
    {
        bool isSucces = true;
        MinionData minion_1;
        MinionData minion_2;
        isSucces &= FindMinionBattleCard(minionUniqueID_1, out minion_1);
        isSucces &= FindMinionBattleCard(minionUniqueID_2, out minion_2);
        if (isSucces)
        {
            MinionData minionTmp = new MinionData(minion_1);
            minion_1.ModifyATK(new ATKStatData(ATKModifyKey.SET_TO, minion_2.ATKCurrent));
            minion_2.ModifyATK(new ATKStatData(ATKModifyKey.SET_TO, minionTmp.ATKCurrent));
            minion_1.ModifyHP(new HPStatData(HPModifyKey.SET_DEFAULT_TO, minion_2.HPDefault)
                             , new HPStatData(HPModifyKey.SET_MAX_TO, minion_2.HPMax)
                             , new HPStatData(HPModifyKey.SET_CURRENT_TO, minion_2.HPCurrent));
            minion_2.ModifyHP(new HPStatData(HPModifyKey.SET_DEFAULT_TO, minionTmp.HPDefault)
                             , new HPStatData(HPModifyKey.SET_MAX_TO, minionTmp.HPMax)
                             , new HPStatData(HPModifyKey.SET_CURRENT_TO, minionTmp.HPCurrent));
            CardUIData uiBattleCard_1 = new CardUIData(minion_1);
            CardUIData uiBattleCard_2 = new CardUIData(minion_2);
            UIManager.Instance.RequestBoostActivate(uiBattleCard_1);
            UIManager.Instance.RequestUpdateCardUI(uiBattleCard_1, false);
            UIManager.Instance.RequestWeakenActivate(uiBattleCard_2, false);
            //UIManager.Instance.ShowPopupHealOnCard(uiBattleCard, value);
            UIManager.Instance.RequestUpdateCardUI(uiBattleCard_2, false);

            // Add battle log.
            {
                SwapAPHPLog log = new SwapAPHPLog(
                      requester
                    , minion_1, minion_2.ATKCurrent, minion_2.HPCurrent
                    , minion_2, minion_1.ATKCurrent, minion_1.HPCurrent
                );
                ActionAddBattleLog(log);
            }

            if (requester.PlayerIndex == minion_1.Owner)
            {
                {
                    Requester triggerer_1 = new Requester(minion_1);
                    List<EventTriggerData> triggerList = new List<EventTriggerData>();
                    triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer_1));
                    triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer_1));
                    triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer_1));
                    ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
                }
                {
                    Requester triggerer_2 = new Requester(minion_2);
                    List<EventTriggerData> triggerList = new List<EventTriggerData>();
                    triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer_2));
                    triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer_2));
                    triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer_2));
                    ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
                }
            }
            else
            {
                {
                    Requester triggerer_2 = new Requester(minion_2);
                    List<EventTriggerData> triggerList = new List<EventTriggerData>();
                    triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer_2));
                    triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer_2));
                    triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer_2));
                    ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
                }
                {
                    Requester triggerer_1 = new Requester(minion_1);
                    List<EventTriggerData> triggerList = new List<EventTriggerData>();
                    triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer_1));
                    triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer_1));
                    triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer_1));
                    ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
                }
            }

            List<int> deadTargetList = new List<int>();
            if (minion_1.IsDead)
            {
                minion_1.SetKillerPlayerIndex(requester.PlayerIndex);
                deadTargetList.Add(minionUniqueID_1);
            }

            if (minion_2.IsDead)
            {
                minion_2.SetKillerPlayerIndex(requester.PlayerIndex);
                deadTargetList.Add(minionUniqueID_2);
            }

            if (deadTargetList.Count > 0)
            {
                ActionDeadMinion(deadTargetList, true, MinionDeathTypes.Effect, onComplete);
                return;
            }
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionSwapSlotMinion(Requester requester, int minionUniqueID_1, int minionUniqueID_2, UnityAction onComplete)
    {
        bool isSucces = true;
        MinionData minion_1;
        MinionData minion_2;
        isSucces &= FindMinionBattleCard(minionUniqueID_1, out minion_1);
        isSucces &= FindMinionBattleCard(minionUniqueID_2, out minion_2);
        if (isSucces)
        {
            int slotID_1 = -1;
            int slotID_2 = -1;
            if (minion_1.CurrentZone == CardZone.Battlefield && minion_1.IsCanMove) slotID_1 = minion_1.SlotID;
            if (minion_2.CurrentZone == CardZone.Battlefield && minion_2.IsCanMove) slotID_2 = minion_2.SlotID;

            if (IsValidSlotID(slotID_1) && IsValidSlotID(slotID_2))
            {
                Data.SlotDatas[slotID_1].ResetBattleCardUniqueID();
                Data.SlotDatas[slotID_2].ResetBattleCardUniqueID();
                minion_1.SetSlotID(-1);
                minion_2.SetSlotID(-1);

                ActionMoveMinionToSlot(minion_1.UniqueID, slotID_2, null, true);
                ActionMoveMinionToSlot(minion_2.UniqueID, slotID_1, onComplete, false);
                return;
            }
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionSetActiveLinkMinion(int targetID, bool isActiveLink)
    {
        List<EventTriggerData> triggerList = new List<EventTriggerData>();
        MinionData minion;
        if (FindMinionBattleCard(targetID, out minion))
        {
            minion.SetBeLink(isActiveLink);
            UIManager.Instance.RequestUpdateCardPassive(new CardUIData(minion), CardStatusType.Be_Link, isActiveLink, false);

            Requester triggerer = new Requester(minion);
            triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer));
        }
        ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
    }

    public void ActionSetBerserkMinion(int targetID, bool isBerserked)
    {
        MinionData minion;
        if (FindMinionBattleCard(targetID, out minion))
        {
            if (minion.IsBerserkActive == false && isBerserked)
            {
                // count player stat
                PlayerStatManager.Instance.DoActMinionBerserkEvent(minion, null);
            }

            minion.SetBerserkActive(isBerserked);
            UIManager.Instance.RequestUpdateCardPassive(new CardUIData(minion), CardStatusType.BerserkActive, isBerserked, false);
        }
    }

    public void ActionRestoreHPMinion(Requester requester, int targetID, int value, UnityAction onComplete)
    {
        bool isCanRestore = false;
        MinionData minion;
        if (FindMinionBattleCard(targetID, out minion))
        {
            int restoreCap = minion.HPMax - minion.HPCurrent;
            int restorePoint = 0;

            if (value < 0)
            {
                restorePoint = restoreCap;
            }
            else
            {
                restorePoint = Mathf.Min(restoreCap, value);
            }

            minion.ModifyHPByIgnoreDamageImmune(new HPStatData(HPModifyKey.SET_CURRENT_DELTA, restorePoint));

            if (restoreCap > 0)
            {
                isCanRestore = true;
            }

            CardUIData uiBattleCard = new CardUIData(minion);
            UIManager.Instance.RequestBoostActivate(uiBattleCard);
            UIManager.Instance.RequestShowPopupHealOnCard(restorePoint, uiBattleCard, false);
            UIManager.Instance.RequestUpdateCardUI(uiBattleCard, false);

            Requester triggerer = new Requester(minion);
            List<EventTriggerData> triggerList = new List<EventTriggerData>();

            if (isCanRestore)
            {
                triggerList.Add(new EventTriggerData(EventKey.OnMeHPIncrease, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnFriendHPIncrease, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnEnemyHPIncrease, triggerer));
            }

            // ไม่ว่ายังไงก็ตาม โดน Heal จะ trigger event นะ
            triggerList.Add(new EventTriggerData(EventKey.OnMeHPRestore, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnFriendHPRestore, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnEnemyHPRestore, triggerer));

            triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer));

            ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);

            // count player stat
            PlayerStatManager.Instance.DoActRestoreMinionHPEvent(requester.Minion, minion, restorePoint, null);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionResetMinion(Requester requester, int targetID, UnityAction onComplete)
    {
        MinionData minion;
        if (FindMinionBattleCard(targetID, out minion))
        {
            minion.ResetToDefault();
            CardUIData uiBattleCard = new CardUIData(minion);
            UIManager.Instance.RequestBoostActivate(uiBattleCard);
            UIManager.Instance.RequestUpdateCardUI(uiBattleCard, false);

            Requester triggerer = new Requester(minion);
            List<EventTriggerData> triggerList = new List<EventTriggerData>();
            triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer));
            ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionAddMinionDirTaunted(int bufferUniqueID, int targetID, CardDirection buffDir, UnityAction onComplete)
    {
        MinionData minion;
        if (FindMinionBattleCard(targetID, out minion))
        {
            minion.AddCardDirTaunted(bufferUniqueID, buffDir);

            CardUIData uiBattleCard = new CardUIData(minion);
            UIManager.Instance.RequestDirEffectChange(uiBattleCard);
            UIManager.Instance.RequestUpdateCardUI(uiBattleCard);

            Requester triggerer = new Requester(minion);
            List<EventTriggerData> triggerList = new List<EventTriggerData>();
            triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnMeChangeDir, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnFriendChangeDir, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnEnemyChangeDir, triggerer));
            ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionRemoveMinionDirTauned(int bufferUniqueID, int targetID, CardDirection buffDir, UnityAction onComplete)
    {
        MinionData minion;
        if (FindMinionBattleCard(targetID, out minion))
        {
            minion.RemoveCardDirTaunted(bufferUniqueID, buffDir);

            CardUIData uiBattleCard = new CardUIData(minion);
            UIManager.Instance.RequestUpdateCardUI(uiBattleCard);

            Requester triggerer = new Requester(minion);
            List<EventTriggerData> triggerList = new List<EventTriggerData>();
            triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnMeChangeDir, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnFriendChangeDir, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnEnemyChangeDir, triggerer));
            ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionAddMinionDirPassiveBuff(int bufferUniqueID, int targetID, CardDirection buffDir, UnityAction onComplete)
    {
        MinionData minion;
        if (FindMinionBattleCard(targetID, out minion))
        {
            CardDirection oldDir = new CardDirection(minion.CardDir);
            minion.AddCardDirBuff(bufferUniqueID, buffDir);

            if (oldDir != minion.CardDir)
            {
                CardUIData uiBattleCard = new CardUIData(minion);
                UIManager.Instance.RequestDirEffectChange(uiBattleCard);
                UIManager.Instance.RequestUpdateCardUI(uiBattleCard);

                Requester triggerer = new Requester(minion);
                List<EventTriggerData> triggerList = new List<EventTriggerData>();
                triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnMeChangeDir, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnFriendChangeDir, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnEnemyChangeDir, triggerer));
                ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
            }
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionAddMinionDir(int targetID, CardDirection buffDir, UnityAction onComplete)
    {
        MinionData minion;
        if (FindMinionBattleCard(targetID, out minion))
        {
            CardDirection oldDir = new CardDirection(minion.CardDir);
            minion.AddCardDir(buffDir);

            if (oldDir != minion.CardDir)
            {
                CardUIData uiBattleCard = new CardUIData(minion);
                UIManager.Instance.RequestDirEffectChange(uiBattleCard);
                UIManager.Instance.RequestUpdateCardUI(uiBattleCard);

                Requester triggerer = new Requester(minion);
                List<EventTriggerData> triggerList = new List<EventTriggerData>();
                triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnMeChangeDir, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnFriendChangeDir, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnEnemyChangeDir, triggerer));
                ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
            }
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionSetMinionDir(int targetID, CardDirection buffDir, UnityAction onComplete)
    {
        MinionData minion;
        if (FindMinionBattleCard(targetID, out minion))
        {
            CardDirection oldDir = new CardDirection(minion.CardDir);
            minion.SetCardDir(buffDir);

            if (oldDir != minion.CardDir)
            {
                CardUIData uiBattleCard = new CardUIData(minion);
                UIManager.Instance.RequestDirEffectChange(uiBattleCard);
                UIManager.Instance.RequestUpdateCardUI(uiBattleCard);

                Requester triggerer = new Requester(minion);
                List<EventTriggerData> triggerList = new List<EventTriggerData>();
                triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnMeChangeDir, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnFriendChangeDir, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnEnemyChangeDir, triggerer));
                ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
            }
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionBuffFear(Requester requester, int targetID, bool isFear, UnityAction onComplete)
    {
        MinionData minion;
        if (FindMinionBattleCard(targetID, out minion))
        {
            //Debug.Log("Set Fear : " + isFear);
            bool isRepeat = false;
            if (minion.IsBeFear && isFear)
            {
                // repeat fear
                isRepeat = true;
            }

            minion.SetBeFear(isFear);

            CardUIData uiBattleCard = new CardUIData(minion);
            UpdateAllUIInfo();
            UIManager.Instance.RequestUpdateCardPassive(uiBattleCard, CardStatusType.Be_Fear, isFear, false);

            // Add battle log.
            {
                BuffFearLog log = new BuffFearLog(requester, minion, isFear, isRepeat);
                ActionAddBattleLog(log);
            }

            Requester triggerer = new Requester(minion);
            List<EventTriggerData> triggerList_1 = new List<EventTriggerData>();
            if (isRepeat)
            {
                triggerList_1.Add(new EventTriggerData(EventKey.OnMeDidRepeatFear, triggerer));
                triggerList_1.Add(new EventTriggerData(EventKey.OnFriendDidRepeatFear, triggerer));
                triggerList_1.Add(new EventTriggerData(EventKey.OnEnemyDidRepeatFear, triggerer));
            }
            ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(requester, triggerList_1), true);

            List<EventTriggerData> triggerList = new List<EventTriggerData>();
            triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer));
            if (isRepeat)
            {
                triggerList.Add(new EventTriggerData(EventKey.OnFriendGotFearRepeat, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnEnemyGotFearRepeat, triggerer));
            }
            ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionBuffFreeze(Requester requester, int targetID, bool isFreeze, UnityAction onComplete)
    {
        MinionData minion;
        if (FindMinionBattleCard(targetID, out minion))
        {
            //Debug.Log("Set Freeze : " + isFreeze);
            bool isRepeat = false;
            if (minion.IsBeFreeze && isFreeze)
            {
                // repeat freeze
                isRepeat = true;
            }

            minion.SetBeFreeze(isFreeze);

            UpdateAllUIInfo(false);
            CardUIData uiBattleCard = new CardUIData(minion);
            UIManager.Instance.RequestUpdateCardPassive(uiBattleCard, CardStatusType.Be_Freeze, isFreeze, false);

            // Add battle log.
            {
                BuffFreezeLog log = new BuffFreezeLog(requester, minion, isFreeze, isRepeat);
                ActionAddBattleLog(log);
            }

            Requester triggerer = new Requester(minion);
            List<EventTriggerData> triggerList_1 = new List<EventTriggerData>();
            if (isRepeat)
            {
                triggerList_1.Add(new EventTriggerData(EventKey.OnMeDidRepeatFreeze, triggerer));
                triggerList_1.Add(new EventTriggerData(EventKey.OnFriendDidRepeatFreeze, triggerer));
                triggerList_1.Add(new EventTriggerData(EventKey.OnEnemyDidRepeatFreeze, triggerer));
            }
            ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(requester, triggerList_1), true);

            List<EventTriggerData> triggerList = new List<EventTriggerData>();
            triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer));

            // if this unit is be freezed
            if (isFreeze)
            {
                triggerList.Add(new EventTriggerData(EventKey.OnMeBeFreeze, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnFriendBeFreeze, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeFreeze, triggerer));

                // count player stat
                PlayerStatManager.Instance.DoActBuffMinionFreezeEvent(requester.Minion, minion, null);
            }
            else
            {
                triggerList.Add(new EventTriggerData(EventKey.OnMeBeRemoveFreeze, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnFriendBeRemoveFreeze, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeRemoveFreeze, triggerer));
            }

            if (isRepeat)
            {
                triggerList.Add(new EventTriggerData(EventKey.OnFriendGotFreezeRepeat, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnEnemyGotFreezeRepeat, triggerer));
            }
            ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionBuffSilence(Requester requester, int targetID, bool isSilence, UnityAction onComplete)
    {
        MinionData minion;
        if (FindMinionBattleCard(targetID, out minion))
        {
            //Debug.Log("Set Silence : " + isSilence);
            bool isRepeat = false;
            if (minion.IsBeSilence && isSilence)
            {
                // repeat silence
                isRepeat = true;
            }
            minion.SetBeSilence(isSilence);

            UpdateAllUIInfo();
            CardUIData uiBattleCard = new CardUIData(minion);
            UIManager.Instance.RequestUpdateCardPassive(uiBattleCard, CardStatusType.Be_Silence, isSilence, false);

            // Add battle log.
            {
                BuffSilenceLog log = new BuffSilenceLog(requester, minion, isSilence, isRepeat);
                ActionAddBattleLog(log);
            }

            Requester triggerer = new Requester(minion);
            List<EventTriggerData> triggerList_1 = new List<EventTriggerData>();
            if (isRepeat)
            {
                triggerList_1.Add(new EventTriggerData(EventKey.OnMeDidRepeatSilence, triggerer));
                triggerList_1.Add(new EventTriggerData(EventKey.OnFriendDidRepeatSilence, triggerer));
                triggerList_1.Add(new EventTriggerData(EventKey.OnEnemyDidRepeatSilence, triggerer));
            }
            ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(requester, triggerList_1), true);

            List<EventTriggerData> triggerList = new List<EventTriggerData>();
            triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer));
            if (isRepeat)
            {
                triggerList.Add(new EventTriggerData(EventKey.OnFriendGotSilenceRepeat, triggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnEnemyGotSilenceRepeat, triggerer));
            }
            ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionBuffStealth(Requester requester, int targetID, bool isStealth, UnityAction onComplete)
    {
        MinionData minion;
        if (FindMinionBattleCard(targetID, out minion))
        {
            //Debug.Log("Set Stealth : " + isStealth);
            bool isRepeat = false;
            if (minion.IsBeStealth && isStealth)
            {
                // repeat silence
                isRepeat = true;
            }
            minion.SetBeStealth(isStealth);

            UpdateAllUIInfo();
            CardUIData uiBattleCard = new CardUIData(minion);
            UIManager.Instance.RequestUpdateCardPassive(uiBattleCard, CardStatusType.Be_Stealth, isStealth, false);

            // Add battle log.
            {
                BuffStealthLog log = new BuffStealthLog(requester, minion, isStealth, isRepeat);
                ActionAddBattleLog(log);
            }

            Requester triggerer = new Requester(minion);
            List<EventTriggerData> triggerList = new List<EventTriggerData>();
            triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer));
            ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionBuffMinionDarkMatter(int targetID, bool isDarkMatter, UnityAction onComplete)
    {
        MinionData minion;
        if (Data.FindMinionBattleCard(targetID, out minion))
        {
            bool isRepeat = false;
            if (minion.IsBeDarkMatter && isDarkMatter)
            {
                // repeat silence
                isRepeat = true;
            }
            minion.SetBeDarkMatter(isDarkMatter);
            CardUIData data = new CardUIData(minion);
            UIManager.Instance.RequestUpdateCardPassive(data, CardStatusType.Be_DarkMatter, isDarkMatter);

            // Add battle log.
            {
                BuffDarkMatterLog log = new BuffDarkMatterLog(minion, isDarkMatter, isRepeat);
                ActionAddBattleLog(log);
            }

            Requester triggerer = new Requester(minion);
            List<EventTriggerData> triggerList = new List<EventTriggerData>();
            triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer));
            ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionBuffInvincible(Requester requester, int targetID, bool isInvincible, UnityAction onComplete)
    {
        MinionData minion;
        if (FindMinionBattleCard(targetID, out minion))
        {
            bool isRepeat = false;
            if (minion.IsBeInvincible && isInvincible)
            {
                // repeat silence
                isRepeat = true;
            }
            minion.SetInvincible(isInvincible);

            UpdateAllUIInfo();
            CardUIData uiBattleCard = new CardUIData(minion);
            UIManager.Instance.RequestUpdateCardPassive(uiBattleCard, CardStatusType.Be_Invincible, isInvincible, false);

            // Add battle log.
            {
                BuffInvincibleLog log = new BuffInvincibleLog(requester, minion, isInvincible, isRepeat);
                ActionAddBattleLog(log);
            }

            Requester triggerer = new Requester(minion);
            List<EventTriggerData> triggerList = new List<EventTriggerData>();
            triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer));
            ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionBuffUntargetable(Requester requester, int targetID, bool isUntargetable, UnityAction onComplete)
    {
        MinionData minion;
        if (FindMinionBattleCard(targetID, out minion))
        {
            bool isRepeat = false;
            if (minion.IsBeUntargetable && isUntargetable)
            {
                // repeat silence
                isRepeat = true;
            }
            minion.SetUntargetable(isUntargetable);

            UpdateAllUIInfo();
            CardUIData uiBattleCard = new CardUIData(minion);
            UIManager.Instance.RequestUpdateCardPassive(uiBattleCard, CardStatusType.Be_Untargetable, isUntargetable, false);

            // Add battle log.
            {
                BuffUntargetableLog log = new BuffUntargetableLog(requester, minion, isUntargetable, isRepeat);
                ActionAddBattleLog(log);
            }

            Requester triggerer = new Requester(minion);
            List<EventTriggerData> triggerList = new List<EventTriggerData>();
            triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer));
            ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionBuffEffectInvincible(Requester requester, int targetID, bool isInvincible, UnityAction onComplete)
    {
        MinionData minion;
        if (FindMinionBattleCard(targetID, out minion))
        {
            bool isRepeat = false;
            if (minion.IsBeInvincible && isInvincible)
            {
                // repeat silence
                isRepeat = true;
            }
            minion.SetEffectInvincible(isInvincible);

            UpdateAllUIInfo();
            CardUIData uiBattleCard = new CardUIData(minion);
            UIManager.Instance.RequestUpdateCardPassive(uiBattleCard, CardStatusType.Be_EffectInvincible, isInvincible, false);

            // Add battle log.
            {
                BuffInvincibleLog log = new BuffInvincibleLog(requester, minion, isInvincible, isRepeat);
                ActionAddBattleLog(log);
            }

            Requester triggerer = new Requester(minion);
            List<EventTriggerData> triggerList = new List<EventTriggerData>();
            triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer));
            ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionBuffPiercing(Requester requester, int targetID, int buffUniqueID, bool isPiercing, int range, UnityAction onComplete)
    {
        MinionData minion;
        if (FindMinionBattleCard(targetID, out minion))
        {
            bool isRepeat = false;
            if (minion.IsBePiercing && isPiercing)
            {
                // repeat silence
                isRepeat = true;
            }
            minion.SetPiercing(buffUniqueID, isPiercing, range);

            UpdateAllUIInfo();
            CardUIData uiBattleCard = new CardUIData(minion);
            UIManager.Instance.RequestUpdateCardPassive(uiBattleCard, CardStatusType.Be_Piercing, isPiercing, false);

            // Add battle log.
            {
                BuffPiercingLog log = new BuffPiercingLog(requester, minion, isPiercing, isRepeat);
                ActionAddBattleLog(log);
            }

            Requester triggerer = new Requester(minion);
            List<EventTriggerData> triggerList = new List<EventTriggerData>();
            triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer));
            ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionBuffOverrideEffectDamage(Requester requester, int targetID, int buffUniqueID, bool isOverride, int damage, UnityAction onComplete)
    {
        MinionData minion;
        if (FindMinionBattleCard(targetID, out minion))
        {
            minion.SetOverrideEffectDamage(buffUniqueID, isOverride, damage);

            Requester triggerer = new Requester(minion);
            List<EventTriggerData> triggerList = new List<EventTriggerData>();
            triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer));
            ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionBuffSlotLock(int slotID, int counterAmount, bool isLock, PlayerIndex owner, UnityAction onComplete)
    {
        if (slotID < Data.SlotDatas.Length && slotID >= 0)
        {
            //Debug.LogWarning("SlotID : " + slotID + " set to " + isEnabled);
            SlotData slot = Data.SlotDatas[slotID];

            // Add battle log.
            {
                Requester requester = new Requester(owner);
                BuffSlotLockLog log = new BuffSlotLockLog(requester, slotID, isLock);
                ActionAddBattleLog(log);
            }

            //Debug.LogFormat("<color=brown>ActionBuffSlotLock</color>: {0}:{1} ({2}, {3})", slotID, owner, counterAmount, isLock);

            if (!isLock)
            {
                // Doing unlock slot.
                // Remove all type of lock buff.
                for (int index = 0; index < slot.BuffDataList.Count; ++index)
                {
                    if (slot.BuffDataList[index].GetType() == typeof(BuffSlotLock))
                    {
                        BuffSlotLock buffSlot = slot.BuffDataList[index] as BuffSlotLock;

                        // Found BuffSlotEnabled buff. Remove it.
                        slot.BuffDataList[index].UnsubscribeAll();
                        slot.BuffDataList.RemoveAt(index);
                        --index;
                    }
                }

                if (!slot.IsBeUnlock)
                {
                    slot.SetBeLock(false);
                    UIManager.Instance.RequestHideLock(slot.SlotID, null);
                }
            }
            else
            {
                // Doing lock slot.
                if (!slot.IsBeUnlock)
                {
                    // Slot is already lock. Replace lock slot buff.
                    for (int index = 0; index < slot.BuffDataList.Count - 1; ++index)
                    {
                        if (slot.BuffDataList[index].GetType() == typeof(BuffSlotLock))
                        {
                            // Remove all lock buff.
                            BuffSlotLock buffSlot = slot.BuffDataList[index] as BuffSlotLock;

                            // Found lock buff. Remove it.
                            slot.BuffDataList[index].UnsubscribeAll();
                            slot.BuffDataList.RemoveAt(index);
                            --index;
                        }
                    }
                }

                // count player stat
                PlayerStatManager.Instance.DoActLockSlotEvent(owner, null);

                // Add lock slot buff.
                slot.SetBeLock(true);
                UIManager.Instance.RequestShowLock(slotID, counterAmount, owner, null);
            }

            UpdateAllUIInfo();
        }

        ActionUpdateBuffSlotDarkMatter(slotID);

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionBuffSlotDarkMatter(int slotID, bool isDarkMatter, PlayerIndex owner, UnityAction onComplete)
    {
        SlotData slot = Data.SlotDatas[slotID];
        PlayerIndex darkMatterOwner = owner;

        // Add battle log.
        {
            Requester requester = new Requester(owner);
            BuffSlotDarkMatterLog log = new BuffSlotDarkMatterLog(requester, slotID, isDarkMatter);
            ActionAddBattleLog(log);
        }

        //Debug.LogFormat("<color=purple>ActionBuffSlotDarkMatter</color>: {0}:{1}({2})", slotID, owner, isDarkMatter);

        if (isDarkMatter)
        {
            if (slot.IsSlotEmpty) // if dark matter and slot is empty
            {
                // Remove all type of dark matter buff.
                for (int index = 0; index < slot.BuffDataList.Count - 1; ++index)
                {
                    if (slot.BuffDataList[index].GetType() == typeof(BuffSlotDarkMatter))
                    {
                        BuffSlotDarkMatter buffSlot = slot.BuffDataList[index] as BuffSlotDarkMatter;
                        darkMatterOwner = buffSlot.Requester.PlayerIndex;
                        // Found BuffSlotDarkMatter buff. Remove it.
                        slot.BuffDataList[index].UnsubscribeAll();
                        slot.BuffDataList.RemoveAt(index);
                        --index;
                    }
                }

                if (slot.IsBeDarkMatter)
                {
                    UIManager.Instance.RequestHideDarkMatter(slotID, darkMatterOwner, null);
                    UIManager.Instance.RequestShowDarkMatter(slotID, owner, null);
                }
                else
                {
                    slot.SetBeDarkMatter(true);
                    UIManager.Instance.RequestShowDarkMatter(slotID, owner, null);
                }
            }
            else // if dark matter and slot is NOT empty
            {
                MinionData minion;
                if (Data.FindMinionBattleCard(slot.BattleCardUniqueID, out minion)) // if there is minion
                {
                    if (minion.IsAlive) // if minion is alive
                    {
                        // Remove all type of dark matter buff.
                        for (int index = 0; index < slot.BuffDataList.Count; ++index)
                        {
                            if (slot.BuffDataList[index].GetType() == typeof(BuffSlotDarkMatter))
                            {
                                BuffSlotDarkMatter buffSlot = slot.BuffDataList[index] as BuffSlotDarkMatter;
                                darkMatterOwner = buffSlot.Requester.PlayerIndex;
                                // Found BuffSlotDarkMatter buff. Remove it.
                                slot.BuffDataList[index].UnsubscribeAll();
                                slot.BuffDataList.RemoveAt(index);
                                --index;
                            }
                        }

                        if (minion.Owner == owner) // if minion owner is same
                        {
                            // minion owner == dark matter owner => add dark matter to minion
                            ActionBuffMinionDarkMatter(slot.BattleCardUniqueID, isDarkMatter, null);

                            if (slot.IsBeDarkMatter)
                            {
                                slot.SetBeDarkMatter(false);
                                UIManager.Instance.RequestHideDarkMatter(slotID, owner, null);
                            }
                        }
                        else // if minion owner is different
                        {
                            if (slot.IsBeDarkMatter)
                            {
                                slot.SetBeDarkMatter(false);
                                UIManager.Instance.RequestHideDarkMatter(slotID, owner, null);
                            }
                        }
                    }
                    else // if minion is NOT alive
                    {
                        // Remove all type of dark matter buff.
                        for (int index = 0; index < slot.BuffDataList.Count - 1; ++index)
                        {
                            if (slot.BuffDataList[index].GetType() == typeof(BuffSlotDarkMatter))
                            {
                                BuffSlotDarkMatter buffSlot = slot.BuffDataList[index] as BuffSlotDarkMatter;
                                darkMatterOwner = buffSlot.Requester.PlayerIndex;

                                // Found BuffSlotDarkMatter buff. Remove it.
                                slot.BuffDataList[index].UnsubscribeAll();
                                slot.BuffDataList.RemoveAt(index);
                                --index;
                            }
                        }

                        slot.SetBeDarkMatter(true);
                        UIManager.Instance.RequestShowDarkMatter(slotID, owner, null);
                    }
                }
                else // if there is NOT minion
                {
                    if (slot.IsBeDarkMatter)
                    {
                        slot.SetBeDarkMatter(false);
                        UIManager.Instance.RequestHideDarkMatter(slotID, owner, null);
                    }
                }
            }
        }
        else
        {
            // Remove all type of dark matter buff.
            for (int index = 0; index < slot.BuffDataList.Count; ++index)
            {
                if (slot.BuffDataList[index].GetType() == typeof(BuffSlotDarkMatter))
                {
                    BuffSlotDarkMatter buffSlot = slot.BuffDataList[index] as BuffSlotDarkMatter;
                    darkMatterOwner = buffSlot.Requester.PlayerIndex;
                    // Found BuffSlotDarkMatter buff. Remove it.
                    slot.BuffDataList[index].UnsubscribeAll();
                    slot.BuffDataList.RemoveAt(index);
                    --index;
                }
            }

            if (slot.IsBeDarkMatter)
            {
                // Add dark matter slot buff.
                slot.SetBeDarkMatter(false);
                UIManager.Instance.RequestHideDarkMatter(slotID, owner, null);
            }
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionBuffEmpty(Requester requester, int targetID, UnityAction onComplete)
    {
        MinionData minion;
        if (FindMinionBattleCard(targetID, out minion))
        {
            Requester triggerer = new Requester(minion);
            List<EventTriggerData> triggerList = new List<EventTriggerData>();
            triggerList.Add(new EventTriggerData(EventKey.OnMeBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnFriendBeBuffed, triggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnEnemyBeBuffed, triggerer));
            ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    private void ActionUpdateBuffSlotDarkMatter(int slotID)
    {
        SlotData slot = Data.SlotDatas[slotID];
        PlayerIndex owner = PlayerIndex.One;
        MinionData minion;
        if (!slot.IsSlotEmpty)
        {
            // if slot is not empty && be dark matter
            if (slot.IsBeDarkMatter)
            {
                // Find owner of dark matter
                for (int index = 0; index < slot.BuffDataList.Count; ++index)
                {
                    if (slot.BuffDataList[index].GetType() == typeof(BuffSlotDarkMatter))
                    {
                        BuffSlotDarkMatter buffSlot = slot.BuffDataList[index] as BuffSlotDarkMatter;
                        owner = buffSlot.Requester.PlayerIndex;
                    }
                }

                slot.SetBeDarkMatter(false);
                UIManager.Instance.RequestHideDarkMatter(slotID, owner, null);

                // if there is a minion on slot
                if (Data.FindMinionBattleCard(slot.BattleCardUniqueID, out minion))
                {
                    if (minion.Owner == owner)
                    {
                        // minion owner == dark matter owner => add dark matter to minion
                        ActionBuffMinionDarkMatter(slot.BattleCardUniqueID, true, null);
                    }
                }

                // Remove all type of dark matter buff.
                for (int index = 0; index < slot.BuffDataList.Count; ++index)
                {
                    if (slot.BuffDataList[index].GetType() == typeof(BuffSlotDarkMatter))
                    {
                        BuffSlotDarkMatter buffSlot = slot.BuffDataList[index] as BuffSlotDarkMatter;

                        // Found BuffSlotDarkMatter buff. Remove it.
                        slot.BuffDataList[index].UnsubscribeAll();
                        slot.BuffDataList.RemoveAt(index);
                        --index;
                    }
                }
            }
        }
    }

    public void ActionPlayerSelectPlayer(Requester requester, List<PlayerIndex> playerIndexList, int count, UnityAction<List<PlayerIndex>> onSelected, UnityAction onCancel)
    {
        if (playerIndexList != null && playerIndexList.Count > 0 && count > 0)
        {
            // Show select UI. 
            if (IsLocalPlayer(requester.PlayerIndex) && Mode != GameMode.BotSimulate)
            {
                // Show select UI.
                AutoSelectPlayer(playerIndexList, count, onSelected);
                return;
            }
            else
            {
                // auto select.
                if (IsNetworkPlay())
                {
                    UIManager.Instance.ShowWaiting();
                    AutoSelectPlayer(playerIndexList, count, onSelected);
                }
                else
                {
                    // VS Bot
                    Bot[] bots = GameObject.FindObjectsOfType<Bot>();
                    foreach (Bot bot in bots)
                    {
                        if (bot.PlayerIndex == requester.PlayerIndex)
                        {
                            onSelected.Invoke(bot.SelectPlayer(requester, playerIndexList, count));
                            break;
                        }
                    }
                }
                return;
            }
        }

        if (onCancel != null)
        {
            onCancel.Invoke();
        }
    }

    public void ActionPlayerSelectMinion(Requester requester, List<int> minionIDList, int count, string info, UnityAction<List<int>> onSelected, UnityAction onCancel)
    {
        if (minionIDList != null && minionIDList.Count > 0 && count > 0)
        {
            _selectedMinionRequester = requester;
            _onSelectedMinionCompelete = onSelected;

            // Find all minion's slot.
            List<int> slotIDList = new List<int>();
            MinionData minion;
            foreach (int minionUniqueID in minionIDList)
            {
                if (FindMinionBattleCard(minionUniqueID, out minion))
                {
                    slotIDList.Add(minion.SlotID);
                }
            }

            // Show select UI. 
            if (IsLocalPlayer(requester.PlayerIndex) && Mode != GameMode.BotSimulate)
            {
                if (_timer > 0)
                {
                    RequestShowWaitingOpponent(_selectedMinionRequester.PlayerIndex, true);

                    UIManager.Instance.RequestShowSelectionSlot(
                          _selectedMinionRequester
                        , slotIDList
                        , count
                        , (MatchData.Property.IsHaveTimer ? SelectDecisionTime : -1.0f)
                        , info
                        , delegate (List<int> selectedList)
                        {
                            // Give turn time
                            if (_timer < ActionTime)
                            {
                                if (IsNetworkPlay())
                                {
                                    PhotonSyncTimer(ActionTime);
                                }
                                else
                                {
                                    RpcSyncTimer(-1, GameHelper.ConvertGamePhaseToByte(Phase), ActionTime);
                                }
                            }

                            OnSelectMinionComplete(selectedList);
                        }
                        , delegate ()
                        {
                            AutoSelectMinionSlot(slotIDList, count, OnSelectMinionComplete);
                        }
                    );
                }
                else
                {
                    AutoSelectMinionSlot(slotIDList, count, OnSelectMinionComplete);
                }

                return;
            }
            else
            {
                // auto select.
                if (IsNetworkPlay())
                {
                    UIManager.Instance.ShowWaiting();
                    AutoSelectMinionSlot(slotIDList, count, OnSelectMinionComplete);
                }
                else
                {
                    // VS Bot
                    Bot[] bots = GameObject.FindObjectsOfType<Bot>();
                    foreach (Bot bot in bots)
                    {
                        if (bot.PlayerIndex == requester.PlayerIndex)
                        {
                            OnSelectMinionComplete(bot.SelectMinionSlot(requester, slotIDList, count));
                            break;
                        }
                    }
                }
                return;
            }
        }

        if (onCancel != null)
        {
            onCancel.Invoke();
        }
    }

    public void ActionAddLinkDirOnSlot(int slotID, CardDirectionType dir, UnityAction onComplete)
    {
        Data.SlotDatas[slotID].AddLinkDir(dir);

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionRemoveLinkDirOnSlot(int slotID, CardDirectionType dir, UnityAction onComplete)
    {
        Data.SlotDatas[slotID].RemoveLinkDir(dir);

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionResetLinkDirOnSlot(int slotID, UnityAction onComplete)
    {
        Data.SlotDatas[slotID].ResetLinkDir();

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionUpdateShowLinkDirOnSlot(int slotID, UnityAction onComplete)
    {
        SlotData slot = Data.SlotDatas[slotID];
        if (slot.IsLinkStatusUpdated() == false)
        {
            List<CardDirectionType> dirList = new List<CardDirectionType>();
            for (int i = 0; i < slot.LinkDir.Length; i++)
            {
                if (slot.LinkDir[i])
                {
                    dirList.Add((CardDirectionType)i);
                }
            }

            slot.UpdateLinkStatus();
            UIManager.Instance.RequestShowLinkOnSlot(slotID, dirList, null);
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    private void OnSelectMinionComplete(List<int> selectedSlotList)
    {
        RequestShowWaitingOpponent(_selectedMinionRequester.PlayerIndex, false);

        if (_selectMinionTimer != null)
        {
            _selectMinionTimer.StopTimer(false);
            _selectMinionTimer = null;
        }

        List<int> minionIDList = new List<int>();
        foreach (int slotID in selectedSlotList)
        {
            if (!Data.SlotDatas[slotID].IsSlotEmpty)
            {
                minionIDList.Add(Data.SlotDatas[slotID].BattleCardUniqueID);
            }
        }

        if (_onSelectedMinionCompelete != null)
        {
            _onSelectedMinionCompelete.Invoke(minionIDList);
        }
    }

    private void OnMinionSelected(List<int> slotIDList)
    {
        List<int> idList = new List<int>();
        foreach (int slotID in slotIDList)
        {
            if (!Data.SlotDatas[slotID].IsSlotEmpty)
            {
                idList.Add(Data.SlotDatas[slotID].BattleCardUniqueID);
            }
        }

        if (_onMinionSelected != null)
        {
            _onMinionSelected.Invoke(idList);
        }
    }

    public void ActionPlayerSelectSlot(Requester requester, List<int> slotIDList, int count, string info, UnityAction<List<int>> onSelected, UnityAction onCancel)
    {
        if (slotIDList != null && slotIDList.Count > 0 && count > 0)
        {
            _onSelectedSlotCompelete = onSelected;
            _selectedSlotRequester = requester;

            // Show select UI.
            if (IsLocalPlayer(requester.PlayerIndex) && Mode != GameMode.BotSimulate)
            {
                if (_timer > 0)
                {
                    RequestShowWaitingOpponent(_selectedSlotRequester.PlayerIndex, true);

                    UIManager.Instance.RequestShowSelectionSlot(
                          _selectedSlotRequester
                        , slotIDList
                        , count
                        , (MatchData.Property.IsHaveTimer ? SelectDecisionTime : -1.0f)
                        , info
                        , delegate (List<int> selectedList)
                        {
                            // Give turn time
                            if (_timer < ActionTime)
                            {
                                if (IsNetworkPlay())
                                {
                                    PhotonSyncTimer(ActionTime);
                                }
                                else
                                {
                                    RpcSyncTimer(-1, GameHelper.ConvertGamePhaseToByte(Phase), ActionTime);
                                }
                            }

                            OnSelectSlotComplete(selectedList);
                        }
                        , delegate ()
                        {
                            AutoSelectSlot(slotIDList, count, OnSelectSlotComplete);
                        }
                    );
                }
                else
                {
                    AutoSelectSlot(slotIDList, count, OnSelectSlotComplete);
                }

                return;
            }
            else
            {
                // auto select.
                if (IsNetworkPlay())
                {
                    UIManager.Instance.ShowWaiting();
                    AutoSelectSlot(slotIDList, count, OnSelectSlotComplete);
                }
                else
                {
                    // VS Bot
                    Bot[] bots = GameObject.FindObjectsOfType<Bot>();
                    foreach (Bot bot in bots)
                    {
                        if (bot.PlayerIndex == requester.PlayerIndex)
                        {
                            OnSelectSlotComplete(bot.SelectSlot(requester, slotIDList, count));
                            break;
                        }
                    }
                }
                return;
            }
        }

        if (onCancel != null)
        {
            onCancel.Invoke();
        }
    }

    private void OnSelectSlotComplete(List<int> selectedList)
    {
        RequestShowWaitingOpponent(_selectedSlotRequester.PlayerIndex, false);

        if (_selectSlotTimer != null)
        {
            _selectSlotTimer.StopTimer(false);
            _selectSlotTimer = null;
        }

        if (_onSelectedSlotCompelete != null)
        {
            _onSelectedSlotCompelete.Invoke(selectedList);
        }
    }

    public void ActionPlayerSelectCard(Requester requester, PlayerIndex playerIndex, List<int> uniqueIDList, int count, UnityAction<List<int>> onSelected, UnityAction onCancel)
    {
        if (uniqueIDList != null && uniqueIDList.Count > 0 && count > 0)
        {
            _onSelectedCardCompelete = onSelected;
            _selectedCardRequester = requester;

            // Find all card.
            List<BattleCardData> cardList = new List<BattleCardData>();
            List<CardUIData> cardUIDataList = new List<CardUIData>();
            BattleCardData battleCard;
            foreach (int uniqueID in uniqueIDList)
            {
                if (FindBattleCard(uniqueID, out battleCard))
                {
                    if (battleCard.CurrentZone != CardZone.Battlefield)
                    {
                        cardList.Add(battleCard);
                        cardUIDataList.Add(new CardUIData(battleCard));
                    }
                }
            }

            // Show select UI.
            if (IsLocalPlayer(requester.PlayerIndex) && Mode != GameMode.BotSimulate)
            {
                RequestShowWaitingOpponent(_selectedCardRequester.PlayerIndex, true);
                string title = LocalizationManager.Instance.GetText("BATTLE_REHAND");
                string subTitle = LocalizationManager.Instance.GetText("BATTLE_REHAND_DESCRIPTION");
                UIManager.Instance.ShowRedraw(
                      title
                    , subTitle
                    , true
                    , cardUIDataList
                    , count
                    , delegate (PlayerIndex selectedPlayerIndex, List<int> selectList)
                    {
                        // Give turn time
                        if (_timer < ActionTime)
                        {
                            if (IsNetworkPlay())
                            {
                                PhotonSyncTimer(ActionTime);
                            }
                            else
                            {
                                RpcSyncTimer(-1, GameHelper.ConvertGamePhaseToByte(Phase), ActionTime);
                            }
                        }

                        OnSelectCardComplete(selectList);
                    }
                );

                _selectCardTimer = ActionStartTriggerTimer(
                      SelectDecisionTime
                    , delegate ()
                    {
                        AutoSelectCard(uniqueIDList, count, OnSelectCardComplete);
                    }
                    , SoundManager.SFX.Mulligan_Countdown_Tick
                    , SoundManager.SFX.Mulligan_Countdown_Ended
                    , UIManager.Instance.TimerRedraw
                );
                _selectCardTimer.gameObject.name = "SelectCardTimer_" + requester.PlayerIndex;

                return;
            }
            else
            {
                // auto select.
                if (IsNetworkPlay())
                {
                    UIManager.Instance.ShowWaiting();
                    AutoSelectCard(uniqueIDList, count, OnSelectCardComplete);
                }
                else
                {
                    // VS Bot
                    Bot[] bots = GameObject.FindObjectsOfType<Bot>();
                    foreach (Bot bot in bots)
                    {
                        if (bot.PlayerIndex == requester.PlayerIndex)
                        {
                            OnSelectCardComplete(bot.SelectCard(requester, uniqueIDList, count));
                            break;
                        }
                    }
                }
                return;
            }
        }

        if (onCancel != null)
        {
            onCancel.Invoke();
        }
    }

    private void OnSelectCardComplete(List<int> selectedCardList)
    {
        RequestShowWaitingOpponent(_selectedCardRequester.PlayerIndex, false);

        if (_selectCardTimer != null)
        {
            _selectCardTimer.StopTimer(false);
            _selectCardTimer = null;
        }

        UIManager.Instance.HideRedraw();

        if (_onSelectedCardCompelete != null)
        {
            _onSelectedCardCompelete.Invoke(selectedCardList);
        }
    }

    private void AutoSelectPlayer(List<PlayerIndex> playerIndexList, int count, UnityAction<List<PlayerIndex>> onSelected)
    {
        List<PlayerIndex> selectedList = new List<PlayerIndex>();
        List<PlayerIndex> choice = new List<PlayerIndex>(playerIndexList);

        if (choice != null && choice.Count > 0)
        {
            if (count >= playerIndexList.Count)
            {
                // Select All.
                selectedList = new List<PlayerIndex>(playerIndexList);
            }
            else
            {
                System.Random r = new System.Random();

                for (int i = 0; i < count; ++i)
                {
                    int index = r.Next(0, choice.Count);
                    selectedList.Add(choice[index]);
                    choice.RemoveAt(index);
                }
            }
        }

        if (onSelected != null)
        {
            onSelected(selectedList);
        }
    }

    private void AutoSelectMinionSlot(List<int> minionSlotIDList, int count, UnityAction<List<int>> onSelected)
    {
        List<int> selectedList = new List<int>();
        List<int> choice = new List<int>(minionSlotIDList);

        if (choice != null && choice.Count > 0)
        {
            if (count >= minionSlotIDList.Count)
            {
                // Select All.
                selectedList = new List<int>(minionSlotIDList);
            }
            else
            {
                System.Random r = new System.Random();

                for (int i = 0; i < count; ++i)
                {
                    int index = r.Next(0, choice.Count);
                    selectedList.Add(choice[index]);
                    choice.RemoveAt(index);
                }
            }
        }

        if (onSelected != null)
        {
            onSelected(selectedList);
        }
    }

    private void AutoSelectSlot(List<int> slotIDList, int count, UnityAction<List<int>> onSelected)
    {
        List<int> selectedList = new List<int>();
        List<int> choice = new List<int>(slotIDList);

        if (choice != null && choice.Count > 0)
        {
            if (count >= slotIDList.Count)
            {
                // Select All.
                selectedList = new List<int>(slotIDList);
            }
            else
            {
                System.Random r = new System.Random();

                for (int i = 0; i < count; ++i)
                {
                    int index = r.Next(0, choice.Count);
                    selectedList.Add(choice[index]);
                    choice.RemoveAt(index);
                }
            }
        }

        if (onSelected != null)
        {
            onSelected(selectedList);
        }
    }

    private void AutoSelectCard(List<int> uniqueIDList, int count, UnityAction<List<int>> onSelected)
    {
        List<int> selectedList = new List<int>();
        List<int> choice = new List<int>(uniqueIDList);

        if (choice != null && choice.Count > 0)
        {
            if (count >= uniqueIDList.Count)
            {
                // Select All.
                selectedList = new List<int>(uniqueIDList);
            }
            else
            {
                System.Random r = new System.Random();

                for (int i = 0; i < count; ++i)
                {
                    int index = r.Next(0, choice.Count);
                    selectedList.Add(choice[index]);
                    choice.RemoveAt(index);
                }
            }
        }

        if (onSelected != null)
        {
            onSelected(selectedList);
        }
    }

    public void ActionEndTurn(PlayerIndex playerIndex, int subTurn, bool isActive = true)
    {
        if (playerIndex == CurrentPlayerIndex)
        {
            if (Phase == GamePhase.Play && SubTurn == subTurn)
            {
                if (!_isEndTurn)
                {
                    if (isActive)
                    {
                        _activeCount++;
                    }

                    _isEndTurn = true;
                }
                else
                {
                    Debug.LogErrorFormat("ActionEndTurn: Multiple call end turn. Phase:{0}_{1} Call by:{2}", Phase, subTurn, playerIndex);
                }
            }
            else
            {
                Debug.LogErrorFormat("ActionEndTurn: Error end turn call outside play phase. Phase:{0}_{1} Call by:{2}", Phase, subTurn, playerIndex);
            }
        }
        else
        {
            Debug.LogErrorFormat("ActionEndTurn: Error end turn by other player. Current:{0}_{1} Call by:{2}", CurrentPlayerIndex, subTurn, playerIndex);
        }
    }

    public void ActionShowGraveyard(LocalPlayerIndex localPlayerIndex)
    {
        PlayerIndex playerIndex = PlayerIndex.One;
        List<CardUIData> cardList = new List<CardUIData>();
        if (localPlayerIndex == LocalPlayerIndex.Me)
        {
            playerIndex = GetLocalPlayerIndex();
        }
        else
        {
            if (GetLocalPlayerIndex() == PlayerIndex.One)
            {
                playerIndex = PlayerIndex.Two;
            }
            else if (GetLocalPlayerIndex() == PlayerIndex.Two)
            {
                playerIndex = PlayerIndex.One;
            }
        }
        List<BattleCardData> graveyardList = Data.PlayerDataList[(int)playerIndex].CardList[CardZone.Graveyard];
        for (int index = graveyardList.Count - 1; index >= 0; --index)
        {
            CardUIData cardUIData = new CardUIData(graveyardList[index]);
            cardList.Add(cardUIData);
        }

        UIManager.Instance.ShowGraveyard(cardList, IsLocalPlayer(playerIndex));
    }

    public WaitObject ActionStartTriggerTimer(float time, UnityAction onComplete, UnityAction<float> onUpdate)
    {
        UtilityItem item = UtilityPool.Instance.CreateItem(UtilityPool.UtilityType.WaitObject);
        if (item != null)
        {
            WaitObject waitObj = item.GetComponent<WaitObject>();
            if (waitObj != null)
            {
                waitObj.StartTimer(time, onComplete, onUpdate);
                return waitObj;
            }
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
        return null;
    }

    public WaitObject ActionStartTriggerTimer(float time, UnityAction onComplete, ITimerUI timerUI)
    {
        UtilityItem item = UtilityPool.Instance.CreateItem(UtilityPool.UtilityType.WaitObject);
        if (item != null)
        {
            WaitObject waitObj = item.GetComponent<WaitObject>();
            if (waitObj != null)
            {
                waitObj.StartTimer(time, onComplete, timerUI);
                return waitObj;
            }
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
        return null;
    }

    public WaitObject ActionStartTriggerTimer(float time, UnityAction onComplete, SoundManager.SFX tickSFX, SoundManager.SFX endSFX, ITimerUI timerUI)
    {
        UtilityItem item = UtilityPool.Instance.CreateItem(UtilityPool.UtilityType.WaitObject);
        if (item != null)
        {
            WaitObject waitObj = item.GetComponent<WaitObject>();
            if (waitObj != null)
            {
                waitObj.StartTimer(time, onComplete, tickSFX, endSFX, timerUI);
                return waitObj;
            }
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
        return null;
    }

    public void ActionAddBattleLog(ActionLog log)
    {
        if (_battleLog == null)
        {
            _battleLog = new BattleLog();
        }

        if (log != null)
        {
            log.SetIndex(_battleLog.Count);
            _battleLog.AddLog(log);

            UIManager.Instance.UpdateActionLog();
        }
    }

    public void ActionSendMessage(PlayerIndex playerIndex, HeroVoice heroVoiceIndex, UnityAction onComplete)
    {
        // TO DO: tell UI to show message.
        Debug.LogFormat(
              "ActionSendMessage: PlayerIndex:{0} HeroVoiceIndex:{1}"
            , playerIndex
            , heroVoiceIndex
        );
        string heroID = Data.PlayerDataList[(int)playerIndex].Hero.ID;
        if (IsLocalPlayer(playerIndex))
        {
            // record emote action
            PlayerStatManager.Instance.DoSendEmoteEvent(heroVoiceIndex, null);
            OnPlayerSendEmote?.Invoke(heroID, heroVoiceIndex);
        }

        UIManager.Instance.ShowTauntAnimation(playerIndex, heroVoiceIndex, delegate
        {
            if (IsVSBot())
            {
                AIBot aiBot = Transform.FindObjectOfType<AIBot>();
                if (aiBot != null && playerIndex != aiBot.PlayerIndex)
                {
                    aiBot.OnGotInGameMessage(heroVoiceIndex);
                }
            }

            onComplete?.Invoke();
        });
    }

    public void ActionPlayCustomVoice(PlayerIndex playerIndex, string key, string text, UnityAction onComplete, float offset = 0f)
    {
#if TEST_DEBUG
        float duration = 1f;

        UIManager.Instance.ShowTauntTalkDialog(playerIndex, text, duration);
        GameHelper.DelayCallback(duration, onComplete);
#else
        float duration = SoundManager.GetTutorialVoiceDuration(key);
        float showTime = duration < 0.01f ? 0 : duration + offset;
        UIManager.Instance.ShowTauntTalkDialog(playerIndex, LocalizationManager.Instance.GetText(text), showTime);
        // play sound by sound's duration and add offset.
        SoundManager.PlayVoice(key, Mode, () =>
        {
            if (offset == 0)
            {
                onComplete?.Invoke();
            }
            else
            {
                GameHelper.DelayCallback(offset, onComplete);
            }
        });
#endif
    }

    public void ActionShowWaiting(bool isShow, UnityAction onComplete)
    {
        if (isShow)
        {
            UIManager.Instance.ShowWaiting();
        }
        else
        {
            UIManager.Instance.HideWaiting();
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    public void ActionSetMessageMute(bool isMute)
    {
        _isMessageMute = isMute;
    }
    #endregion

    #region Analytic Methods
    public void AnalyticTriggerStartMatch()
    {
        AnalyticEventStartMatchData analyticData = new AnalyticEventStartMatchData(
            Data
            , GetLocalPlayerIndex()
            , GetOpponentPlayerIndex(GetLocalPlayerIndex())
            , Mode
            , GetBotLevel()
            , MatchData.Property.IsVSBot
            , DataManager.Instance.GetRoomName()
        );
        AnalyticManager.Instance.LogAnalyticEvent(analyticData);
    }

    public void AnalyticTriggerEndMatch()
    {
        PlayerIndex localPlayer = GetLocalPlayerIndex();
        PlayerIndex opponentPlayer = GetOpponentPlayerIndex(localPlayer);
        AnalyticEventEndMatchData analyticData = new AnalyticEventEndMatchData(
            Data
            , localPlayer
            , opponentPlayer
            , Mode
            , GetBotLevel()
            , GetSumSpirit(localPlayer)
            , GetSumSpirit(opponentPlayer)
            , MatchData.Property.IsVSBot
            , DataManager.Instance.GetRoomName()
            , _isYouWin
            , _endGameType
            , _isMePlayFirst
            , _playTime
            , Turn
        );
        AnalyticManager.Instance.LogAnalyticEvent(analyticData);
    }

    public void AnalyticTriggerStartTurn()
    {
        if (IsNetworkPlay())
        {
            if (IsLocalPlayer(CurrentPlayerIndex))
            {
                AnalyticEventStartTurnData analyticData = new AnalyticEventStartTurnData(
                    Data
                    , CurrentPlayerIndex
                    , Mode
                    , GetBotLevel()
                    , GetSumSpirit(CurrentPlayerIndex)
                    , Turn
                    , _playerDrawCount
                    , _playTime
                );
                AnalyticManager.Instance.LogAnalyticEvent(analyticData);
            }
        }
        else
        {
            AnalyticEventStartTurnData analyticData = new AnalyticEventStartTurnData(
                Data
                , CurrentPlayerIndex
                , Mode
                , GetBotLevel()
                , GetSumSpirit(CurrentPlayerIndex)
                , Turn
                , _playerDrawCount
                , _playTime
            );
            Parameter[] param = analyticData.GetFirebaseParameters();
            AnalyticManager.Instance.LogAnalyticEvent(analyticData);
        }
    }

    public void AnalyticTriggerEndTurn()
    {
        if (IsNetworkPlay())
        {
            if (IsLocalPlayer(CurrentPlayerIndex))
            {
                AnalyticEventEndTurnData analyticData = new AnalyticEventEndTurnData(
                    Data
                    , CurrentPlayerIndex
                    , Mode
                    , GetBotLevel()
                    , GetSumSpirit(CurrentPlayerIndex)
                    , Turn
                    , _attackerListCount
                    , _turnTimer
                );
                AnalyticManager.Instance.LogAnalyticEvent(analyticData);
            }
        }
        else
        {
            AnalyticEventEndTurnData analyticData = new AnalyticEventEndTurnData(
                Data
                , CurrentPlayerIndex
                , Mode
                , GetBotLevel()
                , GetSumSpirit(CurrentPlayerIndex)
                , Turn
                , _attackerListCount
                , _turnTimer
            );
            AnalyticManager.Instance.LogAnalyticEvent(analyticData);
        }
    }

    public void AnalyticTriggerCardPlay(BattleCardData cardData)
    {
        if (IsNetworkPlay())
        {
            if (cardData.Owner == _localPlayerIndex)
            {
                AnalyticEventCardPlayData analyticData = new AnalyticEventCardPlayData(
                    Data.PlayerDataList[(int)CurrentPlayerIndex]
                    , cardData
                    , Mode
                    , GetBotLevel()
                    , Turn
                );
                AnalyticManager.Instance.LogAnalyticEvent(analyticData);
            }
        }
        else
        {
            AnalyticEventCardPlayData analyticData = new AnalyticEventCardPlayData(
                Data.PlayerDataList[(int)CurrentPlayerIndex]
                , cardData
                , Mode
                , GetBotLevel()
                , Turn
            );
            AnalyticManager.Instance.LogAnalyticEvent(analyticData);
        }
    }

    public void AnalyticTriggerCardSummon(BattleCardData cardData)
    {
        if (IsNetworkPlay())
        {
            if (cardData.Owner == _localPlayerIndex)
            {
                AnalyticEventCardSummonData analyticData = new AnalyticEventCardSummonData(
                    Data.PlayerDataList[(int)CurrentPlayerIndex]
                    , cardData
                    , Mode
                    , GetBotLevel()
                    , Turn
                );
                AnalyticManager.Instance.LogAnalyticEvent(analyticData);
            }
        }
        else
        {
            AnalyticEventCardSummonData analyticData = new AnalyticEventCardSummonData(
                Data.PlayerDataList[(int)CurrentPlayerIndex]
                , cardData
                , Mode
                , GetBotLevel()
                , Turn
            );
            AnalyticManager.Instance.LogAnalyticEvent(analyticData);
        }
    }

    public void AnalyticTriggerCardDestroy(BattleCardData cardData)
    {
        if (IsNetworkPlay())
        {
            if (cardData.Owner == _localPlayerIndex)
            {
                AnalyticEventCardDestroyData analyticData = new AnalyticEventCardDestroyData(
                    Data.PlayerDataList[(int)CurrentPlayerIndex]
                    , cardData
                    , Mode
                    , GetBotLevel()
                    , Turn
                );
                AnalyticManager.Instance.LogAnalyticEvent(analyticData);
            }
        }
        else
        {
            AnalyticEventCardDestroyData analyticData = new AnalyticEventCardDestroyData(
                Data.PlayerDataList[(int)CurrentPlayerIndex]
                , cardData
                , Mode
                , GetBotLevel()
                , Turn
            );
            AnalyticManager.Instance.LogAnalyticEvent(analyticData);
        }
    }

    public void AnalyticTriggerCardAttack(BattleCardData cardData, int damage, bool isBackstab)
    {
        if (IsNetworkPlay())
        {
            if (cardData.Owner == _localPlayerIndex)
            {
                AnalyticEventCardAttackData analyticData = new AnalyticEventCardAttackData(
                    Data.PlayerDataList[(int)CurrentPlayerIndex]
                    , cardData
                    , Mode
                    , GetBotLevel()
                    , Turn
                    , damage
                    , isBackstab
                );
                AnalyticManager.Instance.LogAnalyticEvent(analyticData);
            }
        }
        else
        {
            AnalyticEventCardAttackData analyticData = new AnalyticEventCardAttackData(
                Data.PlayerDataList[(int)CurrentPlayerIndex]
                , cardData
                , Mode
                , GetBotLevel()
                , Turn
                , damage
                , isBackstab
            );
            AnalyticManager.Instance.LogAnalyticEvent(analyticData);
        }
    }
    #endregion
}