﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

public class BattleTargetData
{
    #region Public Properties
    public List<int> DefenderUniqueIDList { get { return _defenderUniqueIDList; } }
    #endregion

    #region Private Properties
    private List<int> _defenderUniqueIDList = new List<int>();
    private Dictionary<int, List<int>> _piercingDefenderUniqueIDList = new Dictionary<int, List<int>>();
    #endregion

    #region Contructors
    public BattleTargetData() { }

    public BattleTargetData(BattleTargetData data)
    {
        _defenderUniqueIDList = new List<int>(data._defenderUniqueIDList);
        _piercingDefenderUniqueIDList = new Dictionary<int, List<int>>(data._piercingDefenderUniqueIDList);
    }
    #endregion

    #region Methods
    public List<int> GetPiercingDefenderUniqueIDList(int defenderUniqueID)
    {
        if (_piercingDefenderUniqueIDList.ContainsKey(defenderUniqueID))
        {
            return new List<int>(_piercingDefenderUniqueIDList[defenderUniqueID]);
        }

        return new List<int>();
    }

    public void AddDefenderUniqueID(int uniqueID)
    {
        _defenderUniqueIDList.Add(uniqueID);
    }

    public void AddPiercingDefenderUniqueID(int defenderUniqueID, int piercingUniqueID)
    {
        if (_piercingDefenderUniqueIDList.ContainsKey(defenderUniqueID) == false)
        {
            _piercingDefenderUniqueIDList.Add(defenderUniqueID, new List<int>());
        }

        _piercingDefenderUniqueIDList[defenderUniqueID].Add(piercingUniqueID);
    }
    #endregion
}

public class BattleManager : MonoSingleton<BattleManager>
{
    #region Event
    public static event Action<Action> OnPreAttackEvent;
    public static event Action<Action> OnPreDefendPhaseEvent;
    public static event Action<List<MinionData>> OnResolveDeadEvent;
    #endregion

    #region Enums
    public enum BattlePhase
    {
        Begin = 0
        , PreAttack = 1
        , Attack = 2
        , PostAttack = 3
        , PreDefend = 4
        , Defend = 5
        , PostDefend = 6
        , PreSolve = 7
        , Solve = 8
        , PostSolve = 9
        , End = 10
    }
    #endregion

    #region Public Properties
    public BattlePhase Phase { get { return _phase; } }
    public bool IsBattleActive { get { return _isBattleActive; } }
    public MinionData CurrentAttacker { get; private set; }
    public MinionData CurrentDefender { get; private set; }
    public List<MinionData> CurrentPiercingDefenderList { get; private set; }
    public List<MinionData> CurrentAllDefenderList { get; private set; }
    #endregion

    #region Private Properties
    private BattlePhase _phase = BattlePhase.Begin;
    private bool _isBattleActive = false;
    private bool _isPhaseActive = false;
    private UnityAction _onBattleComplete = null;

    private List<MinionData> _attackerList = null;
    private List<MinionData> _defenderList = null;
    private List<int> _attackedList = null;
    private Queue<BattleStartAttackCommand> _battleCommandWaitQueue = null;

    // data of attacker target list. , Key is the attacker unique ID.
    //private Dictionary<int, List<int>> _battleTargetList = new Dictionary<int, List<int>>();
    private Dictionary<int, BattleTargetData> _battleTargetDataList = new Dictionary<int, BattleTargetData>();

    private List<int> _freezeBattleCardUniqueIDList = new List<int>();
    private Dictionary<int, int> _minionDamageGained = new Dictionary<int, int>();
    private Dictionary<int, int> _minionDefendedCount = new Dictionary<int, int>();
    #endregion

    #region Start
    private void Start()
    {
    }
    #endregion

    #region Update
    private void Update()
    {
        if (IsBattleActive)
        {
            switch (Phase)
            {
                case BattlePhase.Begin: OnBeginPhase(); break;
                case BattlePhase.PreAttack: OnPreAttackPhase(); break;
                case BattlePhase.Attack: OnAttackPhase(); break;
                case BattlePhase.PostAttack: OnPostAttackPhase(); break;
                case BattlePhase.PreDefend: OnPreDefendPhase(); break;
                case BattlePhase.Defend: OnDefendPhase(); break;
                case BattlePhase.PostDefend: OnPostDefendPhase(); break;
                case BattlePhase.PreSolve: OnPreSolvePhase(); break;
                case BattlePhase.Solve: OnSolvePhase(); break;
                case BattlePhase.PostSolve: OnPostSolvePhase(); break;
                case BattlePhase.End: OnEndPhase(); break;
            }
        }
    }
    #endregion

    #region Begin
    private void OnBeginPhase()
    {
        if (_isPhaseActive) return;
        _isPhaseActive = true;

        ActionBattlePreparation(NextPhase);
    }
    #endregion

    #region PreAttack
    private void OnPreAttackPhase()
    {
        if (_isPhaseActive) return;
        _isPhaseActive = true;

        // Generate all attacker battle in this turn.
        _battleCommandWaitQueue = new Queue<BattleStartAttackCommand>();
        List<MinionData> defenderList = new List<MinionData>();
        foreach (MinionData attacker in _attackerList)
        {
            if (_battleTargetDataList.ContainsKey(attacker.UniqueID))
            {
                BattleTargetData data = _battleTargetDataList[attacker.UniqueID];
                foreach (int defenderUniqueID in data.DefenderUniqueIDList)
                {
                    BattleStartAttackCommand cmd = new BattleStartAttackCommand(attacker.UniqueID, defenderUniqueID, data.GetPiercingDefenderUniqueIDList(defenderUniqueID));
                    _battleCommandWaitQueue.Enqueue(cmd);

                    if (GameManager.Instance.FindMinionBattleCard(defenderUniqueID, out MinionData defender))
                    {
                        defenderList.Add(defender);
                    }
                }

                // Count PlayerStat
                PlayerStatManager.Instance.DoActBattleAttackEvent(attacker, defenderList, null);
                defenderList.Clear();
            }
        }

        if (OnPreAttackEvent != null)
        {
            OnPreAttackEvent.Invoke(NextPhase);
        }
        else
        {
            NextPhase();
        }
    }
    #endregion

    #region Attack
    private void OnAttackPhase()
    {
        if (CommandQueue.Instance.IsReady)
        {
            if (_battleCommandWaitQueue != null && _battleCommandWaitQueue.Count > 0)
            {
                CommandQueue.Instance.Add(_battleCommandWaitQueue.Dequeue()); // push command in queue.
            }
            else
            {
                // battle waiting cmd list is empty
                NextPhase();
            }
        }
    }
    #endregion

    #region PostAttack
    private void OnPostAttackPhase()
    {
        if (_isPhaseActive) return;
        _isPhaseActive = true;

        NextPhase();
    }
    #endregion

    #region PreDefend
    private void OnPreDefendPhase()
    {
        if (_isPhaseActive) return;
        _isPhaseActive = true;

        // Generate all defender battle in this turn.
        _battleCommandWaitQueue = new Queue<BattleStartAttackCommand>();
        foreach (MinionData attacker in _defenderList) // defender counter-attack turn, will call them as attacker from now on.
        {
            if (_battleTargetDataList.ContainsKey(attacker.UniqueID))
            {
                BattleTargetData data = _battleTargetDataList[attacker.UniqueID];
                foreach (int defenderUniqueID in data.DefenderUniqueIDList)
                {
                    BattleStartAttackCommand cmd = new BattleStartAttackCommand(attacker.UniqueID, defenderUniqueID, data.GetPiercingDefenderUniqueIDList(defenderUniqueID));
                    _battleCommandWaitQueue.Enqueue(cmd);
                }
            }
        }

        if (OnPreDefendPhaseEvent != null)
        {
            OnPreDefendPhaseEvent.Invoke(NextPhase);
        }
        else
        {
            NextPhase();
        }
    }
    #endregion

    #region Defend
    private void OnDefendPhase()
    {
        if (CommandQueue.Instance.IsReady)
        {
            if (_battleCommandWaitQueue != null && _battleCommandWaitQueue.Count > 0)
            {
                CommandQueue.Instance.Add(_battleCommandWaitQueue.Dequeue()); // push command in queue.
            }
            else
            {
                // battle waiting cmd list is empty
                NextPhase();
            }
        }
    }
    #endregion

    #region PostDefend
    private void OnPostDefendPhase()
    {
        if (_isPhaseActive) return;
        _isPhaseActive = true;

        NextPhase();
    }
    #endregion

    #region PreSolve
    private void OnPreSolvePhase()
    {
        //if (_isPhaseActive) return;
        //_isPhaseActive = true;

        if (CommandQueue.Instance.IsReady && UIManager.Instance.GetUIQueueIsReady())
        {
            // Wait unitil command queue empty. 
            if (_attackerList != null)
            {
                foreach (MinionData minion in _attackerList)
                {
                    minion.ClearAttackedList();
                    UIManager.Instance.RequestTokenGlowActivate(minion.UniqueID, false, false);
                }
            }

            if (_defenderList != null)
            {
                foreach (MinionData minion in _defenderList)
                {
                    minion.ClearAttackedList();
                }
            }

            // Add battle log.
            {
                PhaseLog log = new PhaseLog(DisplayGamePhase.Resolve);
                GameManager.Instance.ActionAddBattleLog(log);
            }
            NextPhase();
        }
    }
    #endregion

    #region Solve
    private void OnSolvePhase()
    {
        if (_isPhaseActive) return;
        _isPhaseActive = true;

        ActionBattleSolve();

        NextPhase();
    }
    #endregion

    #region PostSolve
    private void OnPostSolvePhase()
    {
        if (_isPhaseActive) return;
        _isPhaseActive = true;

        NextPhase();
    }
    #endregion

    #region End
    private void OnEndPhase()
    {
        //if (_isPhaseActive) return;
        //_isPhaseActive = true;

        if (CommandQueue.Instance.IsReady)
        {
            _isBattleActive = false;

            if (_onBattleComplete != null)
            {
                _onBattleComplete.Invoke();
            }
        }
    }
    #endregion

    #region Methods
    private void NextPhase()
    {
        if (_phase == BattlePhase.End)
        {
            _phase = BattlePhase.Begin; //first BattlePhase
            _isBattleActive = false;
        }
        else
        {
            _phase++;
        }

        _isPhaseActive = false;
        GameManager.Instance.ResetSafeToLeaveTimer();
    }

    private void SetPhase(BattlePhase phase)
    {
        _phase = phase;
        _isPhaseActive = false;
    }

    private void ActionBattleSolve()
    {
        List<int> deadTargetList = new List<int>();
        List<int> minionIDList = new List<int>();
        MinionData minion = null;
        List<CommandData> commandList = new List<CommandData>();

        // Resolve Freeze 
        foreach (int id in _freezeBattleCardUniqueIDList)
        {
            if (GameManager.Instance.FindMinionBattleCard(id, out minion))
            {
                //if (minion.IsAlive)
                {
                    commandList.Add(new RemoveMinionBuffTypeCommand(minion.UniqueID, MinionBuffTypes.BuffMinionFreeze));
                }
            }
        }

        // Resolve Dead
        {
            List<MinionData> deadMinionDatas = new List<MinionData>();
            foreach (int item in _attackedList)
            {
                if (!minionIDList.Contains(item))
                {
                    minionIDList.Add(item);
                    if (GameManager.Instance.FindMinionBattleCard(item, out minion))
                    {
                        // Count PlayerStat
                        PlayerStatManager.Instance.DoActBattleDefenseEvent(minion, GetMinionDamage(minion.UniqueID), null);
                        PlayerStatManager.Instance.DoActBattleDefenseCountEvent(minion, GetMinionDefendedCount(minion.UniqueID), null);

                        if (minion.IsDead)
                        {
                            minion.SetKillerPlayerIndex(minion.Owner == PlayerIndex.One ? PlayerIndex.Two : PlayerIndex.One);
                            deadTargetList.Add(minion.UniqueID);
                            deadMinionDatas.Add(minion);
                        }
                    }
                }
            }

            DeadBattleCardCommand cmd = new DeadBattleCardCommand(deadTargetList, true, MinionDeathTypes.Battle);

            commandList.Add(cmd);

            ClearMinionDamage();
            ClearMinionDefendedCount();
            OnResolveDeadEvent?.Invoke(deadMinionDatas);
        }

        GameManager.Instance.ActionAddCommandListToSecondQueue(commandList, true);
        _freezeBattleCardUniqueIDList.Clear();
    }

    public void TriggerBeginBattle(UnityAction onBattleComplete)
    {
        //Debug.Log("TriggerBeginBattle");

        SetPhase(BattlePhase.Begin);

        _attackerList = new List<MinionData>(GameManager.Instance.AttackerList);
        _defenderList = new List<MinionData>();
        _attackedList = new List<int>();
        _battleCommandWaitQueue = new Queue<BattleStartAttackCommand>();
        _battleTargetDataList = new Dictionary<int, BattleTargetData>();

        _onBattleComplete = onBattleComplete;

        _isBattleActive = true;
    }

    public bool IsCriticalAttack(int attackerUniqueID, int defenderUniqueID)
    {
        MinionData attacker = null;
        MinionData defender = null;
        bool isSuccess = true;

        isSuccess &= GameManager.Instance.FindMinionBattleCard(attackerUniqueID, out attacker);
        isSuccess &= GameManager.Instance.FindMinionBattleCard(defenderUniqueID, out defender);
        if (isSuccess)
        {
            bool result = true;
            SlotData targetSlot;
            SlotData attackerSlot = new SlotData(attacker.SlotID);
            SlotData defenderSlot = new SlotData(defender.SlotID);
            foreach (CardDirectionType dir in System.Enum.GetValues(typeof(CardDirectionType)))
            {
                if (defender.CardDir.IsDirection(dir))
                {
                    if (defenderSlot.GetAdjacentSlot(dir, out targetSlot))
                    {
                        if (targetSlot.SlotID == attackerSlot.SlotID)
                        {
                            result = false;
                            break;
                        }
                    }
                }
            }

            return result;
        }

        return false;
    }
    #endregion

    #region Action Methods
    private void ActionBattlePreparation(UnityAction onComplete)
    {
        //Debug.Log("ActionBattlePreparation");

        if (_attackerList != null && _attackerList.Count > 0)
        {
            foreach (MinionData attacker in _attackerList)
            {
                // check alive
                if (attacker.IsAlive == false)
                {
                    continue;
                }

                // loop all direction
                foreach (CardDirectionType attackerDirType in System.Enum.GetValues(typeof(CardDirectionType)))
                {
                    MinionData defender;
                    if (attacker.GetAdjacentMinion(attackerDirType, out defender))
                    {
                        // check alive
                        if (defender.IsAlive == false)
                        {
                            continue;
                        }

                        // same owner
                        if (attacker.Owner == defender.Owner)
                        {
                            continue;
                        }

                        _defenderList.Add(defender);

                        // Check can attack.
                        if (attacker.IsCanAttack && !defender.IsMinionStatus(CardStatusType.Be_Stealth))
                        {
                            if (attacker.CardDir.IsDirection(attackerDirType))
                            {
                                AddTargetAttack(attacker, defender, attackerDirType);
                            }
                        }

                        // Check can counter-attack.
                        if (defender.IsCanAttack && !attacker.IsMinionStatus(CardStatusType.Be_Stealth))
                        {
                            CardDirectionType defenderDirType;
                            if (GameHelper.GetOppositeDirection(attackerDirType, out defenderDirType))
                            {
                                if (defender.CardDir.IsDirection(defenderDirType))
                                {
                                    AddTargetAttack(defender, attacker, defenderDirType);
                                }
                            }
                        }
                    }
                }
            }
        }

        if (_defenderList == null || _defenderList.Count <= 0)
        {
            // If not defender in this battle, Change to end phase.
            SetPhase(BattlePhase.PreSolve);
            return;
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    private void AddTargetAttack(MinionData attacker, MinionData defender, CardDirectionType dir)
    {
        if (_battleTargetDataList == null)
        {
            _battleTargetDataList = new Dictionary<int, BattleTargetData>();
        }

        if (!_battleTargetDataList.ContainsKey(attacker.UniqueID))
        {
            _battleTargetDataList.Add(attacker.UniqueID, new BattleTargetData());
        }

        _battleTargetDataList[attacker.UniqueID].AddDefenderUniqueID(defender.UniqueID);

        int range = attacker.PiercingRange;
        MinionData piercingTarget = defender;
        SlotData slot = null;

        // fnd piercing target
        while (true)
        {
            if (GameManager.Instance.Data.SlotDatas[piercingTarget.SlotID].GetAdjacentSlot(dir, out slot))
            {
                // slot not empty
                if (!slot.IsSlotEmpty)
                {
                    // found minion
                    if (GameManager.Instance.FindMinionBattleCard(slot.BattleCardUniqueID, out piercingTarget))
                    {
                        // minion is alive and same owner to defender
                        if (piercingTarget.IsAlive && piercingTarget.Owner == defender.Owner)
                        {
                            _battleTargetDataList[attacker.UniqueID].AddPiercingDefenderUniqueID(defender.UniqueID, piercingTarget.UniqueID);
                            range--;

                            if (range <= 0)
                            {
                                break;
                            }

                            continue;
                        }
                    }
                }
            }
            break;
        }
    }

    public void ActionBattleStartAttack(int atkUniqueID, int defUniqueID, List<int> piercingUniqueIDList, UnityAction onComplete)
    {
        MinionData attacker = null;
        MinionData defender = null;
        bool isSuccess = true;
        isSuccess &= GameManager.Instance.FindMinionBattleCard(atkUniqueID, out attacker);
        isSuccess &= GameManager.Instance.FindMinionBattleCard(defUniqueID, out defender);
        if (isSuccess)
        {
            if (!attacker.IsAlreadyAttack(defender.UniqueID))
            {
                // Inqueue attack request.
                //RequestAttack(atkUniqueID, defUniqueID);
                List<CommandData> commandList = new List<CommandData>();
                commandList.Add(new CallbackCommand(delegate ()
                {
                    RequestAttack(atkUniqueID, defUniqueID, piercingUniqueIDList);
                }));
                GameManager.Instance.ActionAddCommandListToSecondQueue(commandList, true);

                // Do pre-attack
                ActionAttackPreparation(atkUniqueID, defUniqueID, piercingUniqueIDList);
            }
        }

        if (onComplete != null)
        {
            onComplete.Invoke();
        }
    }

    private void ActionAttackPreparation(int atkUniqueID, int defUniqueID, List<int> piercingUniqueIDList)
    {
        MinionData attacker = null;
        MinionData defender = null;
        bool isSuccess = true;

        isSuccess &= GameManager.Instance.FindMinionBattleCard(atkUniqueID, out attacker);
        isSuccess &= GameManager.Instance.FindMinionBattleCard(defUniqueID, out defender);
        if (isSuccess)
        {
            CurrentAttacker = attacker;
            CurrentDefender = defender;

            Requester atkTriggerer = new Requester(attacker);
            Requester defTriggerer = new Requester(defender);
            List<CommandData> commandList = new List<CommandData>();
            List<EventTriggerData> triggerList = new List<EventTriggerData>();
            triggerList.Add(new EventTriggerData(EventKey.OnPreAttack, atkTriggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnPreDefend, defTriggerer));

            if (IsCurrentCritical() && atkTriggerer.Minion.IsAbilityBackstab && !atkTriggerer.Minion.IsBeSilence)
            {
                triggerList.Add(new EventTriggerData(EventKey.OnFriendDoBackstab, atkTriggerer));
            }

            commandList.AddRange(EventTrigger.TriggerCommandList(triggerList));

            GameManager.Instance.ActionAddCommandListToSecondQueue(commandList, true);
        }
    }

    public void ActionBattleAttack(int atkUniqueID, int defUniqueID, List<int> piercingUniqueIDList, UnityAction onComplete)
    {
        MinionData attacker = null;
        MinionData defender = null;

        bool isSuccess = true;
        isSuccess &= GameManager.Instance.FindMinionBattleCard(atkUniqueID, out attacker);
        isSuccess &= GameManager.Instance.FindMinionBattleCard(defUniqueID, out defender);

        CurrentPiercingDefenderList = new List<MinionData>();
        if (attacker.IsBePiercing)
        {
            MinionData piercingMinion = null;
            foreach (int piercingID in piercingUniqueIDList)
            {
                isSuccess &= GameManager.Instance.FindMinionBattleCard(piercingID, out piercingMinion);
                if (isSuccess)
                {
                    CurrentPiercingDefenderList.Add(piercingMinion);
                }
            }

            // Count PlayerStat
            PlayerStatManager.Instance.DoActBattleAttackPiercingEvent(attacker, CurrentPiercingDefenderList, null);
        }

        // save all defenders
        CurrentAllDefenderList = new List<MinionData>() { defender };
        CurrentAllDefenderList.AddRange(CurrentPiercingDefenderList);

        if (isSuccess)
        {
            int rawDamage = attacker.ATKCurrent;

            // defender
            int realDamage = Mathf.Max(rawDamage - defender.ArmorCurrent, 0);
            if (realDamage > 0 && defender.IsBeFreeze)
            {
                if (!_freezeBattleCardUniqueIDList.Contains(defUniqueID))
                {
                    _freezeBattleCardUniqueIDList.Add(defUniqueID);
                }
            }

            if(_attackedList.Contains(defUniqueID) == false)
            {
                _attackedList.Add(defUniqueID);
            }

            defender.ModifyHP(new HPStatData(HPModifyKey.SET_CURRENT_DELTA, -realDamage, true));
            CountMinionDamage(defUniqueID, realDamage);
            CountMinionDefended(defUniqueID, 1);

            int piercingDamage = 0;
            // loop all piecing target
            foreach (MinionData piercingTarget in CurrentPiercingDefenderList)
            {
                realDamage = Mathf.Max(rawDamage - piercingTarget.ArmorCurrent, 0);
                if (realDamage > 0 && piercingTarget.IsBeFreeze)
                {
                    if (!_freezeBattleCardUniqueIDList.Contains(piercingTarget.UniqueID))
                    {
                        _freezeBattleCardUniqueIDList.Add(piercingTarget.UniqueID);
                    }
                }

                if (_attackedList.Contains(piercingTarget.UniqueID) == false)
                {
                    _attackedList.Add(piercingTarget.UniqueID);
                }

                piercingTarget.ModifyHP(new HPStatData(HPModifyKey.SET_CURRENT_DELTA, -realDamage, true));

                piercingDamage += realDamage;

                CountMinionDamage(piercingTarget.UniqueID, realDamage);
                CountMinionDefended(piercingTarget.UniqueID, 1);
            }

            if(piercingDamage > 0)
            {
                // Count PlayerStat
                PlayerStatManager.Instance.DoActCountBattleAttackPiercingDamageEvent(attacker, piercingDamage, null);
            }

            CardUIData uIDataAttacker = new CardUIData(attacker);
            CardUIData uIDataDefender = new CardUIData(defender);
            List<CardUIData> uIDataPiercingTarget = new List<CardUIData>();
            foreach (MinionData piercingTarget in CurrentPiercingDefenderList)
            {
                uIDataPiercingTarget.Add(new CardUIData(piercingTarget));
            }

            bool isCriticalAttack = IsCriticalAttack(atkUniqueID, defUniqueID);
            bool isHasBackstab = (uIDataAttacker.IsAbilityFeedback(AbilityFeedback.Backstab) && !uIDataAttacker.IsMinionStatus(CardStatusType.Be_Silence));

            UIManager.Instance.RequestTokenAttackTo(uIDataAttacker, uIDataDefender, uIDataPiercingTarget, rawDamage, isCriticalAttack && isHasBackstab);

            // Trigger Analytic
            {
                // Find direction.
                CardDirectionType directionType = CardDirectionType.N;
                foreach (CardDirectionType dir in System.Enum.GetValues(typeof(CardDirectionType)))
                {
                    SlotData slot;
                    if (GameManager.Instance.Data.SlotDatas[attacker.SlotID].GetAdjacentSlot(dir, out slot))
                    {
                        if (slot.SlotID == defender.SlotID)
                        {
                            directionType = dir;
                            break;
                        }
                    }
                }

                // trigger analytic
                GameManager.Instance.AnalyticTriggerCardAttack(attacker, realDamage, isCriticalAttack);
            }

            // Add log
            if (GameManager.Instance.AttackerList.Contains(attacker))
            {
                AttackLog log = new AttackLog(attacker, defender, realDamage);
                GameManager.Instance.ActionAddBattleLog(log);
            }
            else
            {
                CounterLog log = new CounterLog(attacker, defender, realDamage);
                GameManager.Instance.ActionAddBattleLog(log);
            }

            attacker.AddAttacked(defender.UniqueID);
            // Call when complete attack.
            Requester atkTriggerer = new Requester(attacker);
            Requester defTriggerer = new Requester(defender);
            List<Requester> piercingTriggererList = new List<Requester>();
            List<EventTriggerData> triggerList = new List<EventTriggerData>();
            if (realDamage > 0)
            {
                // Trigger damaged event.
                triggerList.Add(new EventTriggerData(EventKey.OnMeDamaged, defTriggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnFriendDamaged, defTriggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnEnemyDamaged, defTriggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnMeDamagedByBattle, defTriggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnFriendDamagedByBattle, defTriggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnEnemyDamagedByBattle, defTriggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnMeDamagedInBattle, defTriggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnFriendDamagedInBattle, defTriggerer));
                triggerList.Add(new EventTriggerData(EventKey.OnEnemyDamagedInBattle, defTriggerer));

                Requester piercingTriggerer;
                foreach (MinionData minion in CurrentPiercingDefenderList)
                {
                    piercingTriggerer = new Requester(minion);
                    piercingTriggererList.Add(piercingTriggerer);
                    triggerList.Add(new EventTriggerData(EventKey.OnMeDamaged, piercingTriggerer));
                    triggerList.Add(new EventTriggerData(EventKey.OnFriendDamaged, piercingTriggerer));
                    triggerList.Add(new EventTriggerData(EventKey.OnEnemyDamaged, piercingTriggerer));
                    triggerList.Add(new EventTriggerData(EventKey.OnMeDamagedByBattle, piercingTriggerer));
                    triggerList.Add(new EventTriggerData(EventKey.OnFriendDamagedByBattle, piercingTriggerer));
                    triggerList.Add(new EventTriggerData(EventKey.OnEnemyDamagedByBattle, piercingTriggerer));
                    triggerList.Add(new EventTriggerData(EventKey.OnMeDamagedInBattle, piercingTriggerer));
                    triggerList.Add(new EventTriggerData(EventKey.OnFriendDamagedInBattle, piercingTriggerer));
                    triggerList.Add(new EventTriggerData(EventKey.OnEnemyDamagedInBattle, piercingTriggerer));
                }
            }
            triggerList.Add(new EventTriggerData(EventKey.OnPostAttack, atkTriggerer));
            triggerList.Add(new EventTriggerData(EventKey.OnPostDefend, defTriggerer));
            foreach (Requester triggerer in piercingTriggererList)
            {
                triggerList.Add(new EventTriggerData(EventKey.OnPostDefend, triggerer));
            }

            GameManager.Instance.ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(triggerList), true);

            if (onComplete != null)
            {
                onComplete.Invoke();
            }
        }
    }
    #endregion

    #region Request Methods
    public void RequestAttack(int atkUniqueID, int defUniqueID, List<int> piercingUniqueIDList)
    {
        BattleAttackCommand cmd = new BattleAttackCommand(atkUniqueID, defUniqueID, piercingUniqueIDList);
        CommandQueue.Instance.Add(cmd);
        CommandQueue.Instance.Add(new WaitCommand(0.2f));
    }
    #endregion

    #region Methods
    private int GetMinionDamage(int minionUniqueID)
    {
        int result = 0;
        if (_minionDamageGained.ContainsKey(minionUniqueID))
        {
            result = _minionDamageGained[minionUniqueID];
        }
        return result;
    }

    private void CountMinionDamage(int defUniqueID, int damage)
    {
        if (_minionDamageGained.ContainsKey(defUniqueID) == false)
        {
            _minionDamageGained.Add(defUniqueID, 0);
        }
        _minionDamageGained[defUniqueID] += damage;
    }

    private void ClearMinionDamage()
    {
        _minionDamageGained.Clear();
    }

    private int GetMinionDefendedCount(int minionUniqueID)
    {
        int result = 0;
        if (_minionDefendedCount.ContainsKey(minionUniqueID))
        {
            result = _minionDefendedCount[minionUniqueID];
        }
        return result;
    }

    private void CountMinionDefended(int defUniqueID, int damage)
    {
        if (_minionDefendedCount.ContainsKey(defUniqueID) == false)
        {
            _minionDefendedCount.Add(defUniqueID, 0);
        }
        _minionDefendedCount[defUniqueID] += damage;
    }

    private void ClearMinionDefendedCount()
    {
        _minionDefendedCount.Clear();
    }

    public bool IsCurrentCritical()
    {
        if (CurrentAttacker != null &&
            CurrentDefender != null)
        {
            SlotData targetSlot;
            SlotData attackerSlot = new SlotData(CurrentAttacker.SlotID);
            SlotData defenderSlot = new SlotData(CurrentDefender.SlotID);
            foreach (CardDirectionType dir in System.Enum.GetValues(typeof(CardDirectionType)))
            {
                if (CurrentDefender.CardDir.IsDirection(dir))
                {
                    if (defenderSlot.GetAdjacentSlot(dir, out targetSlot))
                    {
                        if (targetSlot.SlotID == attackerSlot.SlotID)
                        {
                            return false;
                        }
                    }
                }
            }
        }

        return true;
    }
    #endregion
}