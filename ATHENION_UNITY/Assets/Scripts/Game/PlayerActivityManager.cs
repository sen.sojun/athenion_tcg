﻿using GameEvent;
using Karamucho.Network;
using Karamucho.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerActivityManager : MonoSingleton<PlayerActivityManager>
{
    private static float _updateTime = 20.0f; // prevent too many request.
    private Coroutine _updateServerStatusRoutine;

    public static event Action<PlayerActivity> OnUpdatePlayerActivity;
    #region Subscription Event

    private void OnEnable()
    {
        NavigatorController.OnLogout += NavigatorController_OnLogout;

        GameManager.OnPlayGame += GameManager_OnPlayGame;
        MatchingManager.OnInLobby += MatchingManager_OnInLobby;
        MatchingManager.OnExit += MatchingManager_OnExit;

        ShopManager.OnInit += ShopManager_OnInitialized;
        CollectionManager.OnInit += CollectionManager_OnInit;
        HomeManager.OnInit += HomeManager_OnInit;
        StarExchangeManager.OnInit += StarExchangeManager_OnInit;

        EventBattleManager.OnInit += EventBattleManager_OnInit;
        EventBattleManager.OnQuit += EventBattleManager_OnQuit;

        EventManager.OnInit += GameEventManager_OnInit;
        EventManager.OnQuit += GameEventManager_OnQuit;

        UITutorialPanel.OnInit += UITutorialPanel_OnInit;
        UITutorialPanel.OnQuit += UITutorialPanel_OnQuit;

        PopupPlayModeUI.OnShowUI += PopupPlayModeUI_OnShow;
        PopupPlayModeUI.OnHideUI += PopupPlayModeUI_OnHide;

        OptionController.OnShowUI += OptionController_OnShowUI;
        OptionController.OnHideUI += OptionController_OnHideUI;

        MissionController.Controller.OnEnterMissionEvent += Controller_OnEnterMissionEvent;
        MissionController.Controller.OnExitMissionEvent += Controller_OnExitMissionEvent;
        MissionController.Controller.OnSkipMissionEvent += Controller_OnSkipMissionEvent;
    }

    private void OnDisable()
    {
        NavigatorController.OnLogout -= NavigatorController_OnLogout;

        GameManager.OnPlayGame -= GameManager_OnPlayGame;
        MatchingManager.OnInLobby -= MatchingManager_OnInLobby;
        MatchingManager.OnExit -= MatchingManager_OnExit;

        ShopManager.OnInit -= ShopManager_OnInitialized;
        CollectionManager.OnInit -= CollectionManager_OnInit;
        HomeManager.OnInit -= HomeManager_OnInit;
        StarExchangeManager.OnInit -= StarExchangeManager_OnInit;

        EventBattleManager.OnInit -= EventBattleManager_OnInit;
        EventBattleManager.OnQuit -= EventBattleManager_OnQuit;

        EventManager.OnInit -= GameEventManager_OnInit;
        EventManager.OnQuit -= GameEventManager_OnQuit;

        UITutorialPanel.OnInit -= UITutorialPanel_OnInit;
        UITutorialPanel.OnQuit -= UITutorialPanel_OnQuit;

        PopupPlayModeUI.OnShowUI -= PopupPlayModeUI_OnShow;
        PopupPlayModeUI.OnHideUI -= PopupPlayModeUI_OnHide;

        OptionController.OnShowUI -= OptionController_OnShowUI;
        OptionController.OnHideUI -= OptionController_OnHideUI;

        MissionController.Controller.OnEnterMissionEvent -= Controller_OnEnterMissionEvent;
        MissionController.Controller.OnExitMissionEvent -= Controller_OnExitMissionEvent;
        MissionController.Controller.OnSkipMissionEvent -= Controller_OnSkipMissionEvent;
    }

    #endregion

    #region Event Callback

    #region GamePlay Activity

    private void UITutorialPanel_OnQuit()
    {
        UpdatePlayerActivity(PlayerActivity.In_MainMenu);
    }

    private void UITutorialPanel_OnInit()
    {
        UpdatePlayerActivity(PlayerActivity.In_Tutorial);
    }

    private void PopupPlayModeUI_OnShow()
    {
        UpdatePlayerActivity(PlayerActivity.In_Mode_Select);
    }

    private void PopupPlayModeUI_OnHide()
    {
        UpdatePlayerActivity(PlayerActivity.In_MainMenu);
    }

    private void OptionController_OnShowUI()
    {
        if (SceneManager.GetActiveScene().name == GameHelper.LoginSceneName)
            return;

        if (DataManager.Instance.PlayerInfo.ActiveStatus == PlayerActiveStatus.Online)
        {
            UpdatePlayerActivity(PlayerActivity.In_Option);
        }
    }

    private void OptionController_OnHideUI()
    {
        if (SceneManager.GetActiveScene().name == GameHelper.LoginSceneName)
            return;

        if (DataManager.Instance.PlayerInfo.ActiveStatus != PlayerActiveStatus.Playing)
        {
            UpdatePlayerActivity(PlayerActivity.In_MainMenu);
        }
    }

    private void GameEventManager_OnInit()
    {
        UpdatePlayerActivity(PlayerActivity.In_GameEvent);
    }

    private void GameEventManager_OnQuit()
    {
        UpdatePlayerActivity(PlayerActivity.In_MainMenu);
    }

    private void EventBattleManager_OnInit()
    {
        UpdatePlayerActivity(PlayerActivity.In_EventBattle);
    }

    private void EventBattleManager_OnQuit()
    {
        UpdatePlayerActivity(PlayerActivity.In_MainMenu);
    }

    private void MatchingManager_OnInLobby(GameMode mode)
    {
        switch (mode)
        {
            case GameMode.Friendly:
                UpdatePlayerActivity(PlayerActivity.In_Lobby_Friendly);
                break;
            case GameMode.Casual:
                UpdatePlayerActivity(PlayerActivity.In_Lobby_Casual);
                break;
            case GameMode.Rank:
                UpdatePlayerActivity(PlayerActivity.In_Lobby_Rank);
                break;
            case GameMode.Arena:
                UpdatePlayerActivity(PlayerActivity.In_Lobby_Arena);
                break;
            case GameMode.Event:
                UpdatePlayerActivity(PlayerActivity.In_Lobby_EventBattle);
                break;
            default:
                break;
        }
    }

    private void MatchingManager_OnExit()
    {
        UpdatePlayerActivity(PlayerActivity.In_MainMenu);
    }

    private void GameManager_OnPlayGame(GameMode mode)
    {
        switch (mode)
        {
            case GameMode.Friendly:
                UpdatePlayerActivity(PlayerActivity.Play_Friendly);
                break;
            case GameMode.Casual:
                UpdatePlayerActivity(PlayerActivity.Play_Casual);
                break;
            case GameMode.Rank:
                UpdatePlayerActivity(PlayerActivity.Play_Rank);
                break;
            case GameMode.Arena:
                UpdatePlayerActivity(PlayerActivity.Play_Arena);
                break;
            case GameMode.Event:
                UpdatePlayerActivity(PlayerActivity.Play_EventBattle);
                break;
            case GameMode.Practice:
                UpdatePlayerActivity(PlayerActivity.Play_Practice);
                break;
            case GameMode.Adventure:
                UpdatePlayerActivity(PlayerActivity.Play_Practice);
                break;
            case GameMode.Tutorial_1:
            case GameMode.Tutorial_2:
            case GameMode.Tutorial_3:
            case GameMode.Tutorial_4:
                UpdatePlayerActivity(PlayerActivity.Play_Tutorial);
                break;
            default:
                break;
        }
    }

    #endregion

    #region MainActivity

    private void NavigatorController_OnLogout()
    {
        UpdatePlayerActivity(PlayerActivity.Quit);
        this.DestroySelf();
    }

    private void CollectionManager_OnInit()
    {
        UpdatePlayerActivity(PlayerActivity.In_Deck);
    }

    private void HomeManager_OnInit()
    {
        UpdatePlayerActivity(PlayerActivity.In_MainMenu);
    }

    private void ShopManager_OnInitialized()
    {
        UpdatePlayerActivity(PlayerActivity.In_Shop);
    }

    private void StarExchangeManager_OnInit()
    {
        UpdatePlayerActivity(PlayerActivity.In_StarExchange);
    }

    #endregion

    #region Mission Activity
    private void Controller_OnSkipMissionEvent()
    {
        DataManager.Instance.UpdatePlayerIsPlayingMissionStatus(false);
    }

    private void Controller_OnExitMissionEvent()
    {
        DataManager.Instance.UpdatePlayerIsPlayingMissionStatus(false);
    }

    private void Controller_OnEnterMissionEvent()
    {
        DataManager.Instance.UpdatePlayerIsPlayingMissionStatus(true);
    }
    #endregion

    #endregion

    #region Methods

    private void UpdatePlayerActivity(PlayerActivity activity)
    {
        Debug.Log(string.Format("<color=\"red\"> Update Activity : </color>{0}", activity));
        if (_updateServerStatusRoutine != null)
        {
            StopCoroutine(_updateServerStatusRoutine);
        }
        _updateServerStatusRoutine = StartCoroutine(UpdatePlayerActivityLoop(activity));

        OnUpdatePlayerActivity?.Invoke(activity);
    }

    private IEnumerator UpdatePlayerActivityLoop(PlayerActivity activity)
    {
        while (true)
        {
            if (this == null)
            {
                break;
            }

            DataManager.Instance.UpdatePlayerActivityToServer(activity);
            yield return new WaitForSecondsRealtime(_updateTime);
        }

        yield break;
    }

    protected override void OnDestroy()
    {
        StopAllCoroutines();
        base.OnDestroy();
    }

    private void OnApplicationQuit()
    {
        UpdatePlayerActivity(PlayerActivity.Quit);
    }

    #endregion

}
