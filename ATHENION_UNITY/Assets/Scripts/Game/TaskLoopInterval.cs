﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TaskLoopInterval : MonoBehaviour
{
    public delegate IEnumerator IEnumeratorHandler();
    public static TaskLoopInterval Instance
    {
        get
        {
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<TaskLoopInterval>();
            return _instance;
        }
    }

    private static TaskLoopInterval _instance;

    private Dictionary<string, Coroutine> _coroutines = new Dictionary<string, Coroutine>();

    private void Awake()
    {
        if (_instance == null)
            _instance = this;
        else if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
    }

    public void AddTask(string key, float delay, IEnumeratorHandler function)
    {
        Debug.Log("TaskLoopInterval AddTask : " + key);
        if (!_coroutines.ContainsKey(key))
        {
            _coroutines.Add(key, StartCoroutine(TaskLoop(function, delay)));
        }
    }

    public void StopTask(string key)
    {
        if (_coroutines.ContainsKey(key) && _coroutines[key] != null)
        {
            StopCoroutine(_coroutines[key]);
            _coroutines.Remove(key);
        }
    }

    public void StopAllTask()
    {
        Debug.Log("TaskLoopInterval : StopAllTask");
        foreach (KeyValuePair<string, Coroutine> task in _coroutines)
        {
            if (task.Value != null)
            {
                StopCoroutine(task.Value);
            }
        }
        _coroutines.Clear();
    }

    private IEnumerator TaskLoop(IEnumeratorHandler function, float delay)
    {
        while (true)
        {
            if (Instance == null)
                yield break;

            yield return function.Invoke();
            float time = delay;

            while (time > 0)
            {
                yield return null;
                time -= Time.deltaTime;
                //Debug.LogWarning(time);
            }
        }
    }

    private void OnDisable()
    {
        StopAllTask();
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        if (_instance != null && _instance == this)
            _instance = null;
    }
}
