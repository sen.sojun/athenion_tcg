﻿using System.Collections.Generic;

public class EventTriggerData
{
    public EventKey Key;
    public Requester Triggerer;

    public EventTriggerData(EventKey key, Requester triggerer = null)
    {
        Key = key;
        Triggerer = triggerer;
    }
}

public static class EventTrigger 
{
    #region Private Properties
    #endregion

    #region Methods
    /// <summary>
    /// Call this method to define the trigger data into the trigger command list.
    /// </summary>
    /// <param name="triggerDatas">Group of trigger data.</param>
    /// <returns></returns>
    public static List<CommandData> TriggerCommandList(params EventTriggerData[] triggerDatas)
    {
        List<EventTriggerData> triggerList = new List<EventTriggerData>();
        foreach(EventTriggerData data in triggerDatas)
        {
            triggerList.Add(data);
        }
        return TriggerCommandList(triggerList);
    }

    public static List<CommandData> TriggerCommandList(Requester activer, params EventTriggerData[] triggerDatas)
    {
        List<EventTriggerData> triggerList = new List<EventTriggerData>();
        foreach (EventTriggerData data in triggerDatas)
        {
            triggerList.Add(data);
        }
        return TriggerCommandList(activer, triggerList);
    }

    public static List<CommandData> TriggerCommandList(List<EventTriggerData> triggerDatas)
    {
        List<CommandData> _commandList = new List<CommandData>();
        MinionData minion;

        EventKey key;
        //MinionData minionTriggerer;
        //SlotData slotTriggerer;
        Requester triggerer;

        foreach (EventTriggerData triggerData in triggerDatas)
        {
            key = triggerData.Key;
            if(triggerData.Triggerer != null)
            {
                // Minion
                triggerer = triggerData.Triggerer;
                switch (key)
                {
                    case EventKey.Sentinel:
                    case EventKey.OnEnemyAwaken:
                    case EventKey.OnEnemyBeBuffed:
                    case EventKey.OnEnemyBeFreeze:
                    case EventKey.OnEnemyDied:
                    case EventKey.OnEnemyDiedByBattle:
                    case EventKey.OnEnemyDiedByEffect:
                    case EventKey.OnEnemyExitBattlefield:
                    case EventKey.OnEnemyDamaged:
                    case EventKey.OnEnemyDamagedByBattle:
                    case EventKey.OnEnemyDamagedByEffect:
                    case EventKey.OnEnemyDamagedInBattle:
                    case EventKey.OnEnemyDamagedOutBattle:
                    case EventKey.OnEnemyDestroyed:
                    case EventKey.OnEnemyMoved:
                    case EventKey.OnEnemyMovingToGraveyard:
                    case EventKey.OnEnemyMovedToGraveyard:
                    case EventKey.OnEnemyGotFearRepeat:
                    case EventKey.OnEnemyGotFreezeRepeat:
                    case EventKey.OnEnemyGotSilenceRepeat:
                    case EventKey.OnEnemyHPIncrease:
                    case EventKey.OnEnemyHPRestore:
                    case EventKey.OnEnemyAPIncrease:
                    case EventKey.OnEnemyArmorIncrease:
                    case EventKey.OnEnemyArmorIncreaseByBoost:
                    case EventKey.OnEnemyChangeDir:
                    case EventKey.OnEnemySlotLock:
                    case EventKey.OnEnemySlotUnlock:
                    case EventKey.OnEnemySelected:
                    case EventKey.OnEnemyAddedToAttackerList:
                    case EventKey.OnEnemyRemovedFromAttackerList:
                    case EventKey.OnEnemySlotBeDarkMatter:
                    case EventKey.OnEnemySlotBeNotDarkMatter:
                    case EventKey.OnEnemyMovedToHandFromBattlefield:
                    case EventKey.OnEnemyMovedToHandFromDeck:
                    {
                        //Triggerer's Enemy.
                        foreach(int id in GameManager.Instance.Data.BattleCardActiveIDList)
                        {
                            if (GameManager.Instance.FindMinionBattleCard(id, out minion))
                            {
                                if (!GameManager.Instance.IsFriend(minion.Owner, triggerer.PlayerIndex) &&
                                    minion.IsKeySubscribed(key))
                                {
                                    _commandList.Add(new TriggerMinionCommand(minion, key, triggerer));
                                }
                            }
                        }

                        foreach (SlotData slot in GameManager.Instance.Data.SlotDatas)
                        {
                            if (slot.IsKeySubscribed(key))
                            {
                                _commandList.Add(new TriggerSlotCommand(slot, key, triggerer));
                            }
                        }
                        break;
                    }

                    case EventKey.Rally:
                    case EventKey.OnFriendBeBuffed:
                    case EventKey.OnFriendBeFreeze:
                    case EventKey.OnFriendDied:
                    case EventKey.OnFriendDiedByBattle:
                    case EventKey.OnFriendDiedByEffect:
                    case EventKey.OnFriendExitBattlefield:
                    case EventKey.OnFriendDoBackstab:
                    case EventKey.OnFriendDamaged:
                    case EventKey.OnFriendDamagedByBattle:
                    case EventKey.OnFriendDamagedByEffect:
                    case EventKey.OnFriendDamagedInBattle:
                    case EventKey.OnFriendDamagedOutBattle:
                    case EventKey.OnFriendDestroyed:
                    case EventKey.OnFriendSacrificed:
                    case EventKey.OnFriendMoved:
                    case EventKey.OnFriendMovingToGraveyard:
                    case EventKey.OnFriendMovedToGraveyard:
                    case EventKey.OnFriendGotFearRepeat:
                    case EventKey.OnFriendGotFreezeRepeat:
                    case EventKey.OnFriendGotSilenceRepeat:
                    case EventKey.OnFriendAwaken:
                    case EventKey.OnFriendHPIncrease:
                    case EventKey.OnFriendHPRestore:
                    case EventKey.OnFriendAPIncrease:
                    case EventKey.OnFriendArmorIncrease:
                    case EventKey.OnFriendArmorIncreaseByBoost:
                    case EventKey.OnFriendChangeDir:
                    case EventKey.OnFriendSlotLock:
                    case EventKey.OnFriendSlotUnlock:
                    case EventKey.OnFriendSelected:
                    case EventKey.OnFriendAddedToAttackerList:
                    case EventKey.OnFriendRemovedFromAttackerList:
                    case EventKey.OnFriendSlotBeDarkMatter:
                    case EventKey.OnFriendSlotBeNotDarkMatter:
                    case EventKey.OnFriendMovedToHandFromBattlefield:
                    case EventKey.OnFriendMovedToHandFromDeck:
                    {
                        //Triggerer's Friend.
                        foreach (int id in GameManager.Instance.Data.BattleCardActiveIDList)
                        {
                            if (GameManager.Instance.FindMinionBattleCard(id, out minion))
                            {
                                if (
                                        GameManager.Instance.IsFriend(minion.Owner, triggerer.PlayerIndex)
                                        && (triggerer.Type != Requester.RequesterType.Minion || minion.UniqueID != triggerer.Minion.UniqueID)
                                        && minion.IsKeySubscribed(key)
                                    )
                                {
                                    _commandList.Add(new TriggerMinionCommand(minion, key, triggerer));
                                }
                            }
                        }

                        foreach (SlotData slot in GameManager.Instance.Data.SlotDatas)
                        {
                            if (slot.IsKeySubscribed(key))
                            {
                                _commandList.Add(new TriggerSlotCommand(slot, key, triggerer));
                            }
                        }
                        break;
                    }

                    case EventKey.Absorb:
                    case EventKey.Awaken:
                    case EventKey.Chain_1:
                    case EventKey.Chain_2:
                    case EventKey.Chain_3:
                    case EventKey.Chain_4:
                    case EventKey.OnEnter:
                    case EventKey.OnPreAttack:
                    case EventKey.OnPreDefend:
                    case EventKey.OnPostAttack:
                    case EventKey.OnPostDefend:
                    case EventKey.OnMeDamaged:
                    case EventKey.OnMeDamagedByBattle:
                    case EventKey.OnMeDamagedByEffect:
                    case EventKey.OnMeDamagedInBattle:
                    case EventKey.OnMeDamagedOutBattle:
                    case EventKey.OnMeDestroyed:
                    case EventKey.OnMeSacrificed:
                    case EventKey.LastWish:
                    case EventKey.OnMeDiedByBattle:
                    case EventKey.OnMeDiedByEffect:
                    case EventKey.OnMeExitBattlefield:
                    case EventKey.OnMeMoved:
                    case EventKey.OnMeMovingToGraveyard:
                    case EventKey.OnMeMovedToGraveyard:
                    case EventKey.OnMeBeBuffed:
                    case EventKey.OnMeBeFreeze:
                    case EventKey.OnMeHPIncrease:
                    case EventKey.OnMeHPRestore:
                    case EventKey.OnMeAPIncrease:
                    case EventKey.OnMeArmorIncrease:
                    case EventKey.OnMeArmorIncreaseByBoost:
                    case EventKey.OnMeChangeDir:
                    case EventKey.OnMeSelected:
                    case EventKey.OnMeAddedToAttackerList:
                    case EventKey.OnMeRemovedFromAttackerList:
                    case EventKey.OnMeMovedToHandFromBattlefield:
                    case EventKey.OnMeMovedToHandFromDeck:
                    {
                        // Only Trigger Minion.
                        switch(triggerer.Type)
                        {
                            case Requester.RequesterType.Minion:
                            {
                                if (triggerer.Minion.IsKeySubscribed(key))
                                {
                                    _commandList.Add(new TriggerMinionCommand(triggerer.Minion, key, triggerer));
                                }
                                break;
                            }
                        }

                        // Trigger Slot
                        foreach (SlotData slot in GameManager.Instance.Data.SlotDatas)
                        {
                            if (slot.IsKeySubscribed(key))
                            {
                                _commandList.Add(new TriggerSlotCommand(slot, key, triggerer));
                            }
                        }
                        break;
                    }

                    case EventKey.OnSlotLock:
                    case EventKey.OnSlotUnlock:
                    case EventKey.OnSlotBeDarkMatter:
                    case EventKey.OnSlotBeNotDarkMatter:
                    {
                        // Everyone.
                        foreach (int id in GameManager.Instance.Data.BattleCardActiveIDList)
                        {
                            if (GameManager.Instance.FindMinionBattleCard(id, out minion))
                            {
                                if (minion.IsKeySubscribed(key))
                                {
                                    _commandList.Add(new TriggerMinionCommand(minion, key, triggerer));
                                }
                            }
                        }

                        foreach (SlotData slot in GameManager.Instance.Data.SlotDatas)
                        {
                            if (slot.IsKeySubscribed(key))
                            {
                                _commandList.Add(new TriggerSlotCommand(slot, key, triggerer));
                            }
                        }
                        break;
                    }

                    case EventKey.OnMinionDied:
                    case EventKey.OnMinionDiedByBattle:
                    case EventKey.OnMinionDiedByEffect:
                    case EventKey.OnMinionSummoned:
                    case EventKey.OnMinionDestroyed:
                    case EventKey.OnMinionSacrificed:
                    {
                        // Everyone.
                        foreach (int id in GameManager.Instance.Data.BattleCardActiveIDList)
                        {
                            if (GameManager.Instance.FindMinionBattleCard(id, out minion))
                            {
                                if (minion.IsKeySubscribed(key))
                                {
                                    _commandList.Add(new TriggerMinionCommand(minion, key, triggerer));
                                }
                            }
                        }

                        foreach (SlotData slot in GameManager.Instance.Data.SlotDatas)
                        {
                            if (slot.IsKeySubscribed(key))
                            {
                                _commandList.Add(new TriggerSlotCommand(slot, key, triggerer));
                            }
                        }
                        break;
                    }
                }
            }
            else
            {
                switch(key)
                {
                    case EventKey.BeginPhase:
                    case EventKey.PowerPhase:
                    case EventKey.DrawPhase:
                    case EventKey.PrePlayPhase:
                    //case EventKey.PlayPhase:
                    case EventKey.PostPlayPhase:
                    case EventKey.PreBattlePhase:
                    case EventKey.BattlePhase:
                    case EventKey.PostBattlePhase:
                    case EventKey.EndPhase:
                    {
                        // Everyone.
                        foreach (int id in GameManager.Instance.Data.BattleCardActiveIDList)
                        {
                            if (GameManager.Instance.FindMinionBattleCard(id, out minion))
                            {
                                if (minion.IsKeySubscribed(key))
                                {
                                    _commandList.Add(new TriggerMinionCommand(minion, key, null));
                                }
                            }
                        }

                        foreach (SlotData slot in GameManager.Instance.Data.SlotDatas)
                        {
                            if (slot.IsKeySubscribed(key))
                            {
                                _commandList.Add(new TriggerSlotCommand(slot, key, null));
                            }
                        }

                        //foreach (BattlePlayerData player in GameManager.Instance.Data.PlayerDataList)
                        //{
                        //    if (player.Hero.SkillCard != null && player.Hero.SkillCard.IsKeySubscribed(key))
                        //    {
                        //        _commandList.Add(new TriggerMinionCommand(player.Hero.SkillCard, key, null));
                        //    }
                        //}
                        break;
                    }
                }
            }
        }

        //GameManager.Instance.ActionAddCommandList(_commandList, null, true);
        return new List<CommandData>(_commandList);
    }

    public static List<CommandData> TriggerCommandList(Requester activer, List<EventTriggerData> triggerDatas, bool isAddFirst = false)
    {
        List<CommandData> _commandList = new List<CommandData>();
        MinionData minion;

        EventKey key;
        Requester triggerer;

        foreach (EventTriggerData triggerData in triggerDatas)
        {
            key = triggerData.Key;
            if (triggerData.Triggerer != null)
            {
                // Minion
                triggerer = triggerData.Triggerer;
                switch (key)
                {
                    case EventKey.OnMeDidRepeatFear:
                    case EventKey.OnMeDidRepeatFreeze:
                    case EventKey.OnMeDidRepeatSilence:
                    {
                        // Only Trigger Minion.
                        switch (activer.Type)
                        {
                            case Requester.RequesterType.Minion:
                            {
                                if (activer.Minion.IsKeySubscribed(key))
                                {
                                    _commandList.Add(new TriggerMinionCommand(activer.Minion, key, activer));
                                }
                                break;
                            }
                        }
                        break;
                    }

                    case EventKey.OnEnemyDidRepeatFear:
                    case EventKey.OnEnemyDidRepeatFreeze:
                    case EventKey.OnEnemyDidRepeatSilence:
                    {
                        //Triggerer's Enemy.
                        foreach (int id in GameManager.Instance.Data.BattleCardActiveIDList)
                        {
                            if (GameManager.Instance.FindMinionBattleCard(id, out minion))
                            {
                                if (!GameManager.Instance.IsFriend(minion.Owner, activer.PlayerIndex) &&
                                    minion.IsKeySubscribed(key))
                                {
                                    _commandList.Add(new TriggerMinionCommand(minion, key, activer));
                                }
                            }
                        }
                        break;
                    }

                    case EventKey.OnFriendDidRepeatFear:
                    case EventKey.OnFriendDidRepeatFreeze:
                    case EventKey.OnFriendDidRepeatSilence:
                    {
                        // Activer's Friend.
                        foreach (int id in GameManager.Instance.Data.BattleCardActiveIDList)
                        {
                            if (GameManager.Instance.FindMinionBattleCard(id, out minion))
                            {
                                if (triggerer.Type == Requester.RequesterType.Minion)
                                {
                                    if (GameManager.Instance.IsFriend(minion.Owner, activer.PlayerIndex)
                                                                      && minion.UniqueID != activer.Minion.UniqueID
                                                                      && minion.IsKeySubscribed(key))
                                    {
                                        _commandList.Add(new TriggerMinionCommand(minion, key, activer));
                                    }
                                }
                            }
                        }
                        break;
                    }
                }
            }
        }
        //GameManager.Instance.ActionAddCommandList(_commandList, null, true);

        return new List<CommandData>(_commandList);
    }
    #endregion
}
