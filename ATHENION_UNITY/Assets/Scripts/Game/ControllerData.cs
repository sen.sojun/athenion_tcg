﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Karamucho.UI;
using System;

public class ControllerData
{
    #region Enums
    public enum TutorialPlayState
    {
        Idle
        , Selecting
        , Draging
    };

    #endregion

    #region Properties
    public GameMode TutorialMode { get { return GameManager.Instance.Mode; } }
    public bool IsBotCanAction { get; private set; }
    public bool IsTutorialPhaseActive { get; private set; }
    public bool IsCanEndTurn { get; private set; }

    public FixedBot Bot { get; private set; }
    #endregion

    #region Public Properties
    public int TutorialIndex { get; set; }
    public bool IsSelectedCard { get; set; }
    public bool IsDragCard { get; set; }
    public bool IsPlayedCard { get; set; }
    #endregion

    #region Event
    private TutorialPlayState _tutorialPlayState;
    private UnityAction _onIdleToSelect;
    private UnityAction _onSelectToIdle;
    private UnityAction _onSelectToDrag;
    private UnityAction _onDragToSelect;
    private UnityAction _onPlayCardFail;
    private UnityAction _onDragToIdle;
    #endregion

    #region Constructors
    public ControllerData()
    {
        Init();
    }
    #endregion

    #region Methods
    private void Init()
    {
        TutorialIndex = 0;
        IsBotCanAction = false;
        IsTutorialPhaseActive = false;
        IsCanEndTurn = false;

        IsSelectedCard = false;
        IsDragCard = false;
        IsPlayedCard = false;

        ClearPlayPhaseEventCallback();
    }

    public void SetNextTutorialIndex()
    {
        TutorialIndex++;
    }

    public void SetIsBotCanAction(bool isCanAction)
    {
        IsBotCanAction = isCanAction;
    }

    public void SetIsTutorialPhaseActive(bool isTutorialPhaseActive)
    {
        IsTutorialPhaseActive = isTutorialPhaseActive;
    }

    public void SetIsCanEndTurn(bool isCanEndTurn)
    {
        IsCanEndTurn = isCanEndTurn;
    }

    public void SetBot(FixedBot bot)
    {
        Bot = bot;
    }
    #endregion

    #region PlayPhase Methods
    public void ClearPlayPhaseEventCallback()
    {
        _onIdleToSelect = null;
        _onSelectToIdle = null;
        _onSelectToDrag = null;
        _onDragToSelect = null;
        _onDragToIdle = null;
        _onPlayCardFail = null;

        _tutorialPlayState = TutorialPlayState.Idle;
    }

    public void SetCallbackOnIdleToSelect(UnityAction callback)
    {
        _onIdleToSelect = callback;
    }

    public void SetCallbackOnSelectToIdle(UnityAction callback)
    {
        _onSelectToIdle = callback;
    }

    public void SetCallbackOnSelectToDrag(UnityAction callback)
    {
        _onSelectToDrag = callback;
    }

    public void SetCallbackOnDragToSelect(UnityAction callback)
    {
        _onDragToSelect = callback;
    }

    public void SetCallbackOnPlayCardFail(UnityAction callback)
    {
        _onPlayCardFail = callback;
    }

    public void SetCallbackOnDragToIdle(UnityAction callback)
    {
        _onDragToIdle = callback;
    }

    public void OnPlayCardFail()
    {
        //TODO: change location on this line.
        SoundManager.PlaySFX(SoundManager.SFX.Graveyard_Click);
        _onPlayCardFail?.Invoke();
    }

    public void OnTutorialPlayPhase()
    {
        switch (_tutorialPlayState)
        {
            case TutorialPlayState.Idle:
                {
                    if (CardUIManager.Instance.IsSelectedCard())
                    {
                        _tutorialPlayState = TutorialPlayState.Selecting;
                        _onIdleToSelect?.Invoke();
                    }
                }
                break;

            case TutorialPlayState.Selecting:
                {
                    if (!CardUIManager.Instance.IsSelectedCard())
                    {
                        if (CardUIManager.Instance.IsDragCard())
                        {
                            _tutorialPlayState = TutorialPlayState.Draging;
                            _onSelectToDrag?.Invoke();
                        }
                        else
                        {
                            _tutorialPlayState = TutorialPlayState.Idle;
                            _onSelectToIdle?.Invoke();
                        }
                    }
                }
                break;

            case TutorialPlayState.Draging:
                {
                    if (!CardUIManager.Instance.IsDragCard())
                    {
                        if (CardUIManager.Instance.IsSelectedCard())
                        {
                            _tutorialPlayState = TutorialPlayState.Selecting;
                            _onDragToSelect?.Invoke();
                        }
                        else
                        {
                            _tutorialPlayState = TutorialPlayState.Idle;
                            _onDragToIdle?.Invoke();
                        }
                    }
                }
                break;
        }
    }
    #endregion
}
