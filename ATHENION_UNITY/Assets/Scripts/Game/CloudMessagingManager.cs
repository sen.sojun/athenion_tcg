﻿using Newtonsoft.Json;
using PlayFab;
using PlayFab.ClientModels;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

#region Global Structs
public struct CloudMessageData
{
    public string Title { get; private set; }
    public string Message { get; private set; }

    public CloudMessageData(string title, string message)
    {
        Title = title;
        Message = message;
    }
}
#endregion

[System.Obsolete("No use anymore.", true)]
public class CloudMessagingManager : MonoSingleton<CloudMessagingManager>
{
    #region Public Properties
    public UnityEvent<CloudMessageData> OnReceiveMessage;
    #endregion

    #region Private Properties
    private string _pushToken;
    private string _lastMsg;
    private bool _isInit;
    #endregion

    #region Unity Activity
    #endregion

    #region Notification Manager
    public void Init()
    {
#if UNITY_ANDROID
        if (!_isInit)
        {
            _isInit = true;
            Firebase.Messaging.FirebaseMessaging.TokenReceived += OnTokenReceived;
            Firebase.Messaging.FirebaseMessaging.MessageReceived += OnMessageReceived;
        }

        RegisterForPush();
#endif
    }

    public void DeInit()
    {
        string dummyToken = "arn:aws:sns:us-west-2:388128039633:endpoint/GCM/ATHENION/00000000-0000-0000-0000-000000000000";
        RegisterDeviceToken(dummyToken);
    }

    private void RegisterForPush()
    {
        if (string.IsNullOrEmpty(_pushToken) || string.IsNullOrEmpty(DataManager.Instance.PlayerInfo.PlayerID))
            return;
        RegisterDeviceToken(_pushToken);
    }

    private void RegisterDeviceToken(string token)
    {
#if UNITY_ANDROID
        var request = new AndroidDevicePushNotificationRegistrationRequest
        {
            DeviceToken = token,
            SendPushNotificationConfirmation = false,
            //ConfirmationMessage = "Push notifications registered successfully"
        };
        PlayFabClientAPI.AndroidDevicePushNotificationRegistration(request, OnPfAndroidReg, OnPfFail);
#endif
    }

    private void OnPfAndroidReg(AndroidDevicePushNotificationRegistrationResult result)
    {
        Debug.Log("PlayFab: Push Registration Successful");
    }

    private void OnPfFail(PlayFabError error)
    {
        Debug.Log("PlayFab: api error: " + error.GenerateErrorReport());
    }

    private void OnTokenReceived(object sender, Firebase.Messaging.TokenReceivedEventArgs token)
    {
        Debug.Log("PlayFab: Received Registration Token: " + token.Token);
        _pushToken = token.Token;
        RegisterForPush();
    }

    private void OnMessageReceived(object sender, Firebase.Messaging.MessageReceivedEventArgs e)
    {
        Debug.Log("PlayFab: Received a new message from: " + e.Message.From);
        _lastMsg = "";

        if (e.Message.Data != null)
        {
            _lastMsg += "DATA: " + JsonConvert.SerializeObject(e.Message.Data) + "\n";
            Debug.Log("PlayFab: Received a message with data:");
            foreach (var pair in e.Message.Data)
                Debug.Log("PlayFab data element: " + pair.Key + "," + pair.Value);
        }
        if (e.Message.Notification != null)
        {
            Debug.Log("PlayFab: Received a notification:");
            _lastMsg += "TITLE: " + e.Message.Notification.Title + "\n";
            _lastMsg += "BODY: " + e.Message.Notification.Body + "\n";

            // Invoke subscribed event
            CloudMessageData messageData = new CloudMessageData(e.Message.Notification.Title, e.Message.Notification.Body);
            //NotificationController.Instance.ShowUI(FeedbackUI.Status.Success, e.Message.Notification.Title, e.Message.Notification.Body);
            PopupUIManager.Instance.ShowPopup_ConfirmOnly(e.Message.Notification.Title, e.Message.Notification.Body, null);
            OnReceiveMessage?.Invoke(messageData);
        }
    }
    #endregion
}
