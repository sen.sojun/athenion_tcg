﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheaterDebugger : MonoBehaviour
{
    private void OnEnable()
    {
        GameManager.OnImpossibleError += GameManager_OnImpossibleError;
    }


    private void OnDisable()
    {
        GameManager.OnImpossibleError -= GameManager_OnImpossibleError;
    }

    private void GameManager_OnImpossibleError(string eventKey, PlayerIndex abnormalIndex, BattleData data, string detail = "")
    {
        try
        {
            string roomName = DataManager.Instance.GetRoomName();
            string playerID = DataManager.Instance.PlayerInfo.PlayerID;
            string abnormalPlayerID = data.PlayerDataList[(int)abnormalIndex].PlayerInfo.PlayerID;
            string opponentID = "";

            foreach (BattlePlayerData playerData in data.PlayerDataList)
            {
                if (playerID != playerData.PlayerInfo.PlayerID)
                {
                    opponentID = playerData.PlayerInfo.PlayerID;
                    break;
                }
            }

            object param = new
            {
                  eventKey = eventKey
                , roomName = roomName
                , playerID = playerID
                , opponentID = opponentID
                , abnormalPlayerID = abnormalPlayerID
                , detail = detail
            };

            Debug.LogFormat("<color=red>GameManager_OnImpossibleError:</color> {0}", JsonConvert.SerializeObject(param));

            PlayFabManager.Instance.ExecuteCloudScript("CheckAbnormalEvent", param, null, null);
        }
        catch (Exception e)
        {
            Debug.Log(e.ToString());
        }
    }
}
