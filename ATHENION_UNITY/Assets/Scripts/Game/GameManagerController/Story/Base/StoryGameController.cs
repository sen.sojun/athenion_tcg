﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;
using Karamucho.UI;

public abstract class StoryGameController : GameManagerController
{
    protected CardDirection N_Dir = new CardDirection(CardDirectionType.N);
    protected CardDirection E_Dir = new CardDirection(CardDirectionType.E);
    protected CardDirection S_Dir = new CardDirection(CardDirectionType.S);
    protected CardDirection W_Dir = new CardDirection(CardDirectionType.W);

    private struct VoiceData
    {
        public PlayerIndex PlayerIndex;
        public string ID;
        public string Text;
        public float SoundOffset;

        public VoiceData(PlayerIndex playerIndex, string iD, string text, float soundOffset = 0)
        {
            PlayerIndex = playerIndex;
            ID = iD;
            Text = text;
            SoundOffset = soundOffset;
        }
    }

    public const string HIGHLIGHT_POINTER_BLUE = "#38ACEC";
    public const string HIGHLIGHT_POINTER_GREEN = "#43BFC7";
    public const string HIGHLIGHT_POINTER_PURE_GREEN = "#00FF00";

    public const float PLAY_HIGHLIGHT_SLOT_TIME = 2f;
    public const float PLAY_HIGHLIGHT_ARROW_TIME = 2f;
    public const float PLAY_CARD_DETAIL_TIME = 3f;
    public const float PLAY_PLAYER_DOCK_TIME = 2.5f;

    public ControllerData ControllerData;
    protected FixedBot FixedBot;

    protected readonly List<int> AllSlots = new List<int>() { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };

    public StoryGameController(GameManager manager) : base(manager)
    {
        ControllerData = new ControllerData();
        _manager.IsShuffleDeck = false;

        BattleManager.OnPreAttackEvent += OnBeginPreAttack;
        BattleManager.OnPreDefendPhaseEvent += OnPreDefendPhase;
        BattleManager.OnResolveDeadEvent += OnResolveDead;
    }

    public override void DeInitialize()
    {
        BattleManager.OnPreAttackEvent -= OnBeginPreAttack;
        BattleManager.OnPreDefendPhaseEvent -= OnPreDefendPhase;
        BattleManager.OnResolveDeadEvent -= OnResolveDead;
    }

    #region Methods
    /// <summary>
    /// Hard code for stop updating Power when Spawn unit to Battle field.
    /// </summary>
    /// <returns></returns>
    public virtual bool CanSetPower()
    {
        return true;
    }

    public virtual List<CardUI_DetailHover.HighlightList> OnTouchUIDetailCard() { return new List<CardUI_DetailHover.HighlightList>(); }

    public virtual void SetupBot(PlayerIndex playerIndex, GameMode gameMode)
    {
        GameObject obj = new GameObject();
        TutorialBot bot = obj.AddComponent<TutorialBot>();
        if (bot != null)
        {
            Debug.LogFormat("Create TutorialBot for {0}", playerIndex);
            bot.SetPlayerIndex(playerIndex);
            bot.SetTutorialMode(gameMode);

            //this._onShowSpiritAttackOnPlayerComplete += bot.OnSpiritAttackOnPlayer;
            //this._onPlayerPlayHandCardComplete += bot.OnPlayHandCard;

            bot.SetEnable(true);

            ControllerData.SetBot(bot);
            FixedBot = bot;
        }
    }

    public override bool IsCanUseCard(MinionData minion)
    {
        if (minion.SpiritCurrent <= _manager.Data.PlayerDataList[(int)minion.Owner].CurrentAP)
        {
            return true;
        }
        return false;
    }

    public virtual bool CanEndTurn(PlayerIndex playerIndex)
    {
        bool isInvalidSlot = _manager.IsHaveEmptySlot() == false;
        if (isInvalidSlot)
            return true;

        int handCount = _manager.GetHandCount(playerIndex);
        if (handCount == 0)
            return true;

        int sp = _manager.Data.PlayerDataList[(int)playerIndex].CurrentAP;

        for (int i = 0; i < handCount; i++)
        {
            BattleCardData battleCardData = _manager.Data.PlayerDataList[(int)playerIndex].CardList[CardZone.Hand][i];
            int spirit = battleCardData.RawData.Spirit;
            if (spirit <= sp)
                return false;
        }

        //if (sp == 0)
        //    return true;

        return true;
    }

    protected override void NextPhase()
    {
        base.NextPhase();

        ControllerData.SetIsTutorialPhaseActive(false);
        ControllerData.SetIsCanEndTurn(false);
        ControllerData.SetIsBotCanAction(false);
    }

    public override bool IsValidSlotID(int slotID)
    {
        if (IsTutorialValidSlotID(slotID))
        {
            if (slotID >= 0 && slotID < _manager.BoardSize)
            {
                return true;
            }
        }

        return false;

    }
    public abstract bool IsTutorialValidSlotID(int slotID);

    public override void OnPlayCardFail()
    {
        ControllerData.OnPlayCardFail();
    }
    #endregion

    #region Game Phase

    #region Intro Phase

    protected override IEnumerator IntroPhase()
    {
        yield return StartIntroPhase();
        yield return MiddleIntroPhase();
        yield return EndIntroPhase();

        NextPhase();
    }

    protected virtual IEnumerator StartIntroPhase()
    {
        _manager.ShowFullLoad(false);
        SoundManager.PlayBGM(SoundManager.BGM.Intro, 3.0f);
        // Waiting opponent to ready.

        // Start play intro
        if (_manager.PlayerIndexList != null && _manager.PlayerIndexList.Count == 2) // 2 players
        {
            foreach (PlayerIndex playerIndex in _manager.PlayerIndexList)
            {
                if (_manager.IsLocalPlayer(playerIndex))
                {
                    BattlePlayerData player = _manager.Data.PlayerDataList[(int)playerIndex];
                    UIManager.Instance.SetPlayerInfo(Karamucho.UI.CardUIManager.PlayerPosition.Bottom, player);
                }
                else
                {
                    BattlePlayerData player = _manager.Data.PlayerDataList[(int)playerIndex];
                    UIManager.Instance.SetPlayerInfo(Karamucho.UI.CardUIManager.PlayerPosition.Top, player);
                }
                UIManager.Instance.SetPlayerAP(playerIndex, 0, null);
            }
        }
        _manager.UpdateAllUIInfo();

        while (!_manager.IsCanNextPhase)
        {
            //wait
            yield return null;
        }

    }

    protected virtual IEnumerator MiddleIntroPhase()
    {
        bool _isCompleteIntro = false;
        _manager._isMePlayFirst = _manager.IsLocalPlayer(_manager.CurrentPlayerIndex);
        UIManager.Instance.StartIntro(_manager.IsLocalPlayer(_manager.CurrentPlayerIndex), delegate () { _isCompleteIntro = true; });

        while (!_manager.IsCanNextPhase || !_isCompleteIntro)
        {
            //wait
            yield return null;
        }
        //switch (_manager.Mode)
        //{
        //    case GameMode.Tutorial_3:
        //        {
        //            // Init Draw
        //            _manager.InitDraw();
        //        }
        //        break;
        //}

        while (!_manager.IsCanNextPhase)
        {
            yield return null;
        }

    }

    protected virtual IEnumerator EndIntroPhase()
    {
        yield break;
    }
    #endregion

    #region ReHand Phase

    protected override IEnumerator ReHandPhase()
    {
        UIManager.Instance.HideRedraw();

        SoundManager.PlayBGM(SoundManager.BGM.Battle, 5.0f);

        // stamp match ticket
        bool isFinish = false;
        PlayerStatManager.Instance.DoUpdatePlayerMatchTicket(DataManager.Instance.GetRoomName(), () => isFinish = true);
        yield return new WaitUntil(() => isFinish == true);

        UIManager.Instance.ShowBTNOverlayUI(true);
        NextPhase();
    }

    #endregion

    #region BeginPhase

    public override void OnBeginPhase()
    {
        if (!_isPhaseActive)
        {
            if (!_manager.IsCanNextPhase) return;
            _isPhaseActive = true;

            _manager.UpdateInfoAllPlayers();
            _manager.StartCoroutine(IEBeginPhase());
        }
    }

    protected override IEnumerator IEBeginPhase()
    {
        bool isShowTurnBoxComplete = false;
        _manager.ResetPlayCardCount();

        //Show Glow Player Turn
        UIManager.Instance.ShowTurnGlow(_manager.CurrentPlayerIndex);
        UIManager.Instance.ShowTurnBox(_manager.CurrentPlayerIndex, delegate
        {
            isShowTurnBoxComplete = true;
        });

        while (!_manager.IsCanNextPhase || !isShowTurnBoxComplete)
        {
            yield return null;
        }

        _manager.ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(new EventTriggerData(EventKey.BeginPhase)), true);

        while (!_manager.IsCanNextPhase)
        {
            yield return null;
        }

        while (!_manager.IsPhaseReady(GamePhase.Begin))
        {
            yield return null;
        }

        if (_manager.IsLocalPlayer(_manager.CurrentPlayerIndex))
        {
            UIManager.Instance.ShowHand();
        }

        // Add action log.

        if (_manager._isNewTurn)
        {
            TurnSummaryLog log = new TurnSummaryLog(_manager.Turn, _manager.Data.PlayerDataList[0], _manager.Data.PlayerDataList[1]);
            _manager.ActionAddBattleLog(log);
            _manager._isNewTurn = false;
        }

        {
            StartTurnLog log = new StartTurnLog(_manager.Turn, _manager.SubTurn, _manager.Data.PlayerDataList[(int)_manager.CurrentPlayerIndex]);
            _manager.ActionAddBattleLog(log);
        }

        yield return IEOnEndBeginPhase();
        NextPhase();
    }
    public abstract IEnumerator IEOnEndBeginPhase();

    #endregion

    #region Spirit Phase

    public override void OnSpiritPhase()
    {
        if (_isPhaseActive)
        {
            // Spirit phase update loop.
            if (_manager.IsCanNextPhase)
            {
                if (_manager.IsPhaseReady(GamePhase.Spirit))
                {
                    UnityAction action = delegate ()
                    {
                        // Immune update
                        _manager.Data.PlayerDataList[(int)_manager.CurrentPlayerIndex].UpdateImmune();
                        NextPhase();
                    };

                    if (!ControllerData.IsTutorialPhaseActive)
                    {
                        ControllerData.SetIsTutorialPhaseActive(true);

                        _manager.StartCoroutine(OnEndSpiritPhaseTutorial(action));
                    }
                }
            }
        }
        else
        {
            // Spirit phase update loop first time.
            _isPhaseActive = true;

            switch (_manager.GetPlayerCount())
            {
                case 2:
                default:
                    {
                        SpiritPhaseTutorial();
                    }
                    break;
            }

            // Update all player info 
            _manager.UpdateInfoAllPlayers();

            _manager.ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(new EventTriggerData(EventKey.PowerPhase)), true);
        }
    }

    public virtual void SpiritPhaseTutorial()
    {
        int sum = _manager.GetSumSpirit(_manager.CurrentPlayerIndex);
        foreach (PlayerIndex playerIndex in _manager.PlayerIndexList)
        {
            if (playerIndex != _manager.CurrentPlayerIndex)
            {
                _manager.RequestSpiritDamage(playerIndex, sum);
            }
        }
    }

    public abstract IEnumerator OnEndSpiritPhaseTutorial(UnityAction onComplete);

    #endregion

    #region Draw Phase
    Coroutine DrawPhaseRoutine;
    public override void OnDrawPhase()
    {
        if (DrawPhaseRoutine == null)
        {
            DrawPhaseRoutine = _manager.StartCoroutine(IEOnDrawPhase());
        }
    }
    protected IEnumerator IEOnDrawPhase()
    {
        _isPhaseActive = true;
        _manager._playerDrawCount = 0;

        {
            int toDrawCount = _manager.MaxHandCard;
            int handCount = _manager.Data.PlayerDataList[(int)_manager.CurrentPlayerIndex].CardList[CardZone.Hand].Count;
            int deckCount = _manager.Data.PlayerDataList[(int)_manager.CurrentPlayerIndex].CardList[CardZone.Deck].Count;
            int handSlot = toDrawCount - handCount;

            int drawCount = Mathf.Min(handSlot, deckCount);
            int drawDamage = Mathf.Max(handSlot - drawCount, 0);

            yield return BeforeAddAP();
            yield return AddingAP();
            yield return AfterAddAP();

            bool isDrawAP = false;
            OnSetDrawCount(_manager.SubTurn, ref drawCount, ref isDrawAP);

            if (isDrawAP)
            {
                _manager.RequestCreateCardToZone("CAP", _manager.CurrentPlayerIndex, CardZone.Hand, -1); // Add AP card.
                drawCount--;
            }

            // Draw cards.
            if (drawCount > 0)
            {
                _manager._playerDrawCount = drawCount;
                _manager.RequestDrawCard(_manager.CurrentPlayerIndex, drawCount, false);
            }

            if (drawDamage > 0)
            {
                _manager.RequestDrawDamage(_manager.CurrentPlayerIndex, drawDamage);
            }
        }


        _manager.ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(new EventTriggerData(EventKey.DrawPhase)), true);
        yield return null;

        while (_manager.IsCanNextPhase == false)
        {
            yield return null;
        }
        if (_manager.IsPhaseReady(GamePhase.Draw))
        {
            NextPhase();
            DrawPhaseRoutine = null;
            yield break;
        }
    }
    public abstract void OnSetDrawCount(int subturn, ref int drawCount, ref bool isDrawAP);
    public virtual IEnumerator BeforeAddAP() { yield break; }
    public virtual IEnumerator AddingAP()
    {
        _manager.RequestSetPlayerAP(_manager.CurrentPlayerIndex, 3);
        yield return null;
    }
    public virtual IEnumerator AfterAddAP() { yield break; }
    #endregion

    #region PrePlay Phase

    public override void OnPrePlayPhase()
    {
        if (_isPhaseActive)
        {
            // Pre-play phase update loop.

            if (_manager.IsCanNextPhase)
            {
                if (_manager.IsPhaseReady(GamePhase.PrePlay))
                {
                    UnityAction action = delegate ()
                    {
                        PhaseLog log = new PhaseLog(DisplayGamePhase.Play);
                        _manager.ActionAddBattleLog(log);

                        NextPhase();
                    };

                    if (!ControllerData.IsTutorialPhaseActive)
                    {
                        ControllerData.SetIsTutorialPhaseActive(true);

                        _manager.StartCoroutine(OnEndPrePlayPhaseTutorial(action));
                    }
                }
            }
        }
        else
        {
            // Pre-play phase update loop first time.

            _isPhaseActive = true;

            //if (_manager.EndType != GameManager.EndGameType.Normal)
            //{
            //    _manager.OnEndGame();
            //    return;
            //}

            _manager.UpdateLocalHandCard();

            if (_manager.CountdownTime > _manager.TurnTime)
            {
                _manager.CountdownTime = _manager.TurnTime;
            }

            _manager.ResetActiveCount();

            if (
                (
                    !_manager.IsCanUseHandCard(_manager.CurrentPlayerIndex)     // Out of AP
                    || _manager.IsEmptyHand(_manager.CurrentPlayerIndex)        // Empty hand
                    || !_manager.IsHaveEmptySlot()                              // No empty slot
                )
                && _manager.IsAutoEndTurn
            )
            {
                {
                    _manager._timer = 0.0f;
                    _manager.SetActiveCount(1); // Set one action
                }
            }
            else
            {
                if (_manager._afkCount[(int)_manager.CurrentPlayerIndex] > 0)
                {
                    _manager._timer = _manager.AFKTime;
                }
                else
                {
                    _manager._timer = _manager.TurnTime;
                }
            }
            _manager._startTurnStamp = DateTimeData.GetDateTimeUTC();

            UIManager.Instance.SetTargetTimer(Mathf.Min(_manager.CountdownTime, _manager._timer));
            _manager._isTimer = true;
            _manager._isEndTurn = false;
            _manager._isTimeoutCallEndTurn = false;
            _manager._turnTimer = 0.0f;
            _manager.IsTurnTimeout = false;

            // Trigger Analytic
            {
                _manager.AnalyticTriggerStartTurn();
            }

            _manager.ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(new EventTriggerData(EventKey.PrePlayPhase)), true);

            if (_manager.GetLocalPlayerIndex() == _manager.CurrentPlayerIndex)
            {
                UIManager.Instance.EndButtonIsOpen(true);
            }
        }
    }
    public abstract IEnumerator OnEndPrePlayPhaseTutorial(UnityAction onComplete);

    #endregion

    #region Play Phase

    public override void OnPlayPhase()
    {
        if (!_isPhaseActive)
        {
            // Play phase update loop first time.
            _isPhaseActive = true;

            if (!ControllerData.IsTutorialPhaseActive)
            {
                OnBeginPlayPhaseTutorial();
            }
        }

        if (!ControllerData.IsTutorialPhaseActive)
        {
            OnPlayPhaseTutorial();

            ControllerData.OnTutorialPlayPhase();
        }

        _manager.ActionGetUseCard();

        if (!_manager.IsEndTurn)
        {
            // Play phase update loop.
            if (_manager.IsTimeout)
            {
                _manager.IsTurnTimeout = true;

                if (!_manager._isTimeoutCallEndTurn)
                {
                    SoundManager.PlaySFX(SoundManager.SFX.Countdown_Ended);
                    _manager._isTimeoutCallEndTurn = true;
                }
                _manager.RequestEndTurn(_manager.CurrentPlayerIndex, _manager.SubTurn, false);
            }

            if (
                //   (!_manager.IsCanUseHandCard(_manager.CurrentPlayerIndex)     // Out of AP
                //|| _manager.IsEmptyHand(_manager.CurrentPlayerIndex)            // Empty hand
                //|| !_manager.IsHaveEmptySlot())                        // No empty slot
                CanEndTurn(_manager.CurrentPlayerIndex) && _manager.IsCanNextPhase
            )
            {
                if (_manager.IsAutoEndTurn)
                {
                    _manager.RequestEndTurn(_manager.CurrentPlayerIndex, _manager.SubTurn, true);

                }
                else
                {
                    // Pop end turn button.
                    if (_manager.IsCanNextPhase)
                    {
                        if (_manager.GetLocalPlayerIndex() == _manager.CurrentPlayerIndex)
                        {
                            UIManager.Instance.EndButtonIsOpen(true);
                            UIManager.Instance.EndTurnIsFinish(true);
                        }
                    }
                }
            }

            _manager.UpdateLocalHandCard();
        }
        else
        {
            if (_manager.IsCanNextPhase)
            {
                if (_manager.IsPhaseReady(GamePhase.Play))
                {
                    if (!ControllerData.IsTutorialPhaseActive)
                    {
                        ControllerData.SetIsTutorialPhaseActive(true);
                        ControllerData.ClearPlayPhaseEventCallback();

                        OnEndPlayPhaseTutorial(NextPhase);
                        return;
                    }
                }
            }
        }
    }
    public abstract void OnBeginPlayPhaseTutorial();
    public abstract void OnPlayPhaseTutorial();
    public abstract void OnEndPlayPhaseTutorial(UnityAction onComplete);

    #endregion
    #region Battle Phase

    /// <summary>
    /// after use card.
    /// </summary>
    public override void OnPreBattlePhase()
    {
        if (_isPhaseActive)
        {
            // Pre-battle phase update loop.

            if (_manager.IsCanNextPhase)
            {
                if (_manager.IsPhaseReady(GamePhase.PreBattle))
                {
                    UnityAction action = delegate ()
                    {
                        PhaseLog log = new PhaseLog(DisplayGamePhase.Battle);
                        _manager.ActionAddBattleLog(log);
                        NextPhase();
                    };

                    if (!ControllerData.IsTutorialPhaseActive)
                    {
                        ControllerData.SetIsTutorialPhaseActive(true);
                        ControllerData.IsSelectedCard = false;
                        ControllerData.IsDragCard = false;
                        ControllerData.IsPlayedCard = false;

                        _manager.StartCoroutine(OnEndPreBattlePhaseTutorial(action));
                    }

                }
            }
        }
        else
        {
            // Pre-battle phase update loop first time.

            _isPhaseActive = true;

            _manager.ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(new EventTriggerData(EventKey.PreBattlePhase)), true);
        }
    }
    public abstract IEnumerator OnEndPreBattlePhaseTutorial(UnityAction onComplete);

    public void OnBeginPreAttack(Action onComplete)
    {
        _manager.StartCoroutine(IEOnBeginPreAttack(onComplete));
    }

    protected virtual IEnumerator IEOnBeginPreAttack(Action onComplete)
    {
        onComplete?.Invoke();
        yield break;
    }

    public void OnPreDefendPhase(Action onComplete)
    {
        _manager.StartCoroutine(IEOnPreDefendPhase(onComplete));
    }

    protected virtual IEnumerator IEOnPreDefendPhase(Action onComplete)
    {
        onComplete?.Invoke();
        yield break;
    }

    public virtual void OnResolveDead(List<MinionData> deadTargetList) { }

    public override void OnPostBattlePhase()
    {
        if (_isPhaseActive)
        {
            // Post-battle phase update loop.

            if (_manager.IsCanNextPhase)
            {
                if (_manager.IsPhaseReady(GamePhase.PostBattle))
                {
                    UnityAction action = delegate () { NextPhase(); };

                    if (!ControllerData.IsTutorialPhaseActive)
                    {
                        ControllerData.SetIsTutorialPhaseActive(true);

                        _manager.StartCoroutine(OnPostBattlePhaseTutorial(action));

                    }
                }
            }
        }
        else
        {
            _isPhaseActive = true;
            _manager.ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(new EventTriggerData(EventKey.PostBattlePhase)), true);
        }
    }
    public abstract IEnumerator OnPostBattlePhaseTutorial(UnityAction onComplete);

    #endregion

    #region End Phase

    public override void OnEndPhase()
    {
        if (_isPhaseActive)
        {
            // End phase update loop.
            if (_manager.IsCanNextPhase)
            {
                if (_manager.IsPhaseReady(GamePhase.End))
                {
                    ControllerData.TutorialIndex = 0;
                    _manager.StartCoroutine(IEOnEndPhase(() =>
                    {
                        NextPhase();
                        _manager.NextPlayer();
                    }));
                }
            }
        }
        else
        {
            // End phase update loop first time. 
            ControllerData.SetIsBotCanAction(false);

            _isPhaseActive = true;
            _manager.IsCallEndTurn = false;
            _manager.ResetPlayCardCount();

            if (_manager._attackerList == null)
            {
                _manager._attackerList = new List<MinionData>();
            }
            else
            {
                _manager._attackerListCount = _manager._attackerList.Count;
                _manager._attackerList.Clear();
            }

            // Trigger Analytic
            {
                _manager.AnalyticTriggerEndTurn();
            }

            _manager.ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(new EventTriggerData(EventKey.EndPhase)), true);
        }
    }

    protected virtual IEnumerator IEOnEndPhase(Action onComplete)
    {
        onComplete?.Invoke();
        yield break;
    }

    #endregion

    #endregion

    #region Tutorial Methods

    protected IEnumerator FixedDrawCardToHand(string baseID, PlayerIndex playerIndex)
    {
        BattleCardData card;
        if (_manager.FindBattleCard(baseID, CardZone.Deck, playerIndex, out card))
        {
            bool isFinish = false;
            _manager.RequestMoveCardToZone(card.UniqueID, CardZone.Deck, CardZone.Hand, -1, () => { isFinish = true; });
            yield return new WaitUntil(() => isFinish == true);
        }
        else
        {
            Debug.LogError("Not found " + baseID);
        }
    }

    protected CardUIData GetSelectCardData()
    {
        if (CardUIManager.Instance.GetCurrentCardHighlight() != null)
            return CardUIManager.Instance.GetCurrentCardHighlight().Data;
        else if (CardUIManager.Instance.GetCurrentCard() != null)
            return CardUIManager.Instance.GetCurrentCard().Data;
        return null;
    }

    protected MinionData GetMinionDataInfoFromHand(int index)
    {
        try
        {
            return _manager.Data.PlayerDataList[(int)PlayerIndex.One].CardList[CardZone.Hand][index] as MinionData;
        }
        catch (Exception e)
        {
            return null;
        }
    }

    protected Vector2 GetSlotPosition(int slotIndex)
    {
        return _manager.GetBoardManager().GetSlotHolder(slotIndex).GetHolderPosition();
    }

    protected Vector2 GetSlotScreenPosition(int slotIndex)
    {
        return Camera.main.WorldToScreenPoint(_manager.GetBoardManager().GetSlotHolder(slotIndex).GetHolderPosition());
    }

    protected Vector2 GetSlotScreenPosition(int slotIndex, Vector3 offset)
    {
        return Camera.main.WorldToScreenPoint(_manager.GetBoardManager().GetSlotHolder(slotIndex).GetHolderPosition() + offset);
    }

    protected void ShowHighlightBoard(List<int> slotsIndex, Karamucho.UI.BoardSlotHolder.SlotHilightType type)
    {
        _manager.GetBoardManager().Tutorial_SlotHilight(slotsIndex, true, type, false);
    }

    protected void ShowHighlightBoard(List<int> slotsIndex, Color color)
    {
        _manager.GetBoardManager().Tutorial_SlotHilight(slotsIndex, true, color, false);
    }

    protected void HideHighlightBoard(List<int> slotsIndex)
    {
        _manager.GetBoardManager().Tutorial_SlotHilight(slotsIndex, false, Karamucho.UI.BoardSlotHolder.SlotHilightType.Normal, false);
    }

    protected void ShowHighlightATKToken(int slotIndex, bool isShow)
    {
        int uniqueID = _manager.GetBoardManager().GetTokenBySlotID(slotIndex).GetUniqueID();
        _manager.GetBoardManager().Tutorial_TokenHilightAtk(uniqueID, isShow);
    }

    protected void ShowHighlightHPToken(int slotIndex, bool isShow)
    {
        int uniqueID = _manager.GetBoardManager().GetTokenBySlotID(slotIndex).GetUniqueID();
        _manager.GetBoardManager().Tutorial_TokenHilightHp(uniqueID, isShow);
    }

    protected void ShowHighlightArmorToken(List<int> slotIndex, bool isShow)
    {
        _manager.GetBoardManager().Tutorial_TokenHilightArmorBySlots(slotIndex, isShow);
    }

    protected void ShowHighlightSoulToken(List<int> slotIndex, bool isShow)
    {
        _manager.GetBoardManager().Tutorial_TokenHilightSoulBySlots(slotIndex, isShow);
    }

    protected void ShowHighlightArrowToken(int slotIndex, CardDirection direction, bool isShow)
    {
        _manager.GetBoardManager().Tutorial_SlotHilightArrow(slotIndex, direction, isShow);
    }

    protected void PointerPlayerCard(int index, bool isShow, Color color)
    {
        int uniqueID = _manager.Data.PlayerDataList[(int)PlayerIndex.One].CardList[CardZone.Hand][index].UniqueID;
        CardUIManager.Instance.Tutorial_HighlightCard_Pointer(uniqueID, isShow, color);
    }

    protected void HighlightPlayerHand(int index, List<TutorialCardPopupHighlight.HighlightType> highlightList, bool isShow)
    {
        int uniqueID = _manager.Data.PlayerDataList[(int)PlayerIndex.One].CardList[CardZone.Hand][index].UniqueID;
        for (int i = 0; i < highlightList.Count; i++)
        {
            switch (highlightList[i])
            {
                case TutorialCardPopupHighlight.HighlightType.Arrow:
                    CardUIManager.Instance.Tutorial_HighlightCard_Direction(uniqueID, isShow);
                    break;
                case TutorialCardPopupHighlight.HighlightType.ATK:
                    CardUIManager.Instance.Tutorial_HighlightCard_Atk(uniqueID, isShow);
                    break;
                case TutorialCardPopupHighlight.HighlightType.HP:
                    CardUIManager.Instance.Tutorial_HighlightCard_Hp(uniqueID, isShow);
                    break;
                case TutorialCardPopupHighlight.HighlightType.Soul:
                    CardUIManager.Instance.Tutorial_HighlightCard_Soul(uniqueID, isShow);
                    break;
                case TutorialCardPopupHighlight.HighlightType.Armor:
                    CardUIManager.Instance.Tutorial_HighlightCard_Armor(uniqueID, isShow);
                    break;
                default:
                    break;
            }
        }
    }

    protected void SetActiveHandPointer(bool isShow, Vector2 screenPos)
    {
        if (isShow)
        {
            TutorialPopupManager.Instance.ShowPointingHandOnSlot(screenPos);
        }
        else
        {
            TutorialPopupManager.Instance.HidePointerHandOnSlot();
        }
    }

    protected void SetActiveHandGuide(bool isShow, Vector2 screenPos)
    {
        if (isShow)
        {
            TutorialPopupManager.Instance.ShowPointingHand(screenPos);
        }
        else
        {
            TutorialPopupManager.Instance.HidePointingHand();
        }
    }

    #region Player Dock

    protected void ShowRemind()
    {
        TutorialPopupManager.Instance.ShowHighlightHand();
    }

    protected void HideRemind()
    {
        TutorialPopupManager.Instance.HideHighlightHand();
    }

    protected void HighlightLabelDock(UIPlayerDock.HighlightDock highlight, bool isEnemy, bool isShow)
    {
        string key = isEnemy == true ? "TU_ENEMY_" : "TU_YOUR_";
        if (isShow)
        {
            switch (highlight)
            {
                case UIPlayerDock.HighlightDock.HP:
                    key += "HP";
                    break;
                case UIPlayerDock.HighlightDock.SOUL:
                    key += "SOUL";
                    break;
                default:
                    return;
            }
            UIManager.Instance.GetUITutorialManager().RequestShowPopup(key);
        }
        else
        {
            switch (highlight)
            {
                case UIPlayerDock.HighlightDock.HP:
                    key += "HP";
                    break;
                case UIPlayerDock.HighlightDock.SOUL:
                    key += "SOUL";
                    break;
                default:
                    return;
            }
            UIManager.Instance.GetUITutorialManager().RequestHidePopup(key);
        }
    }

    protected void HighlightPlayerDock(UIPlayerDock.HighlightDock highlight, float duration, bool isShow = true)
    {
        switch (highlight)
        {
            case UIPlayerDock.HighlightDock.AP:
                UIManager.Instance.UIPlayerBottom.Tutorial_Hilight_AP(isShow, duration);
                break;
            case UIPlayerDock.HighlightDock.HP:
                UIManager.Instance.UIPlayerBottom.Tutorial_Hilight_HP(isShow, duration);
                break;
            case UIPlayerDock.HighlightDock.SOUL:
                UIManager.Instance.UIPlayerBottom.Tutorial_Hilight_Soul(isShow, duration);
                break;
            default:
                break;
        }
    }

    protected void HighlightEnemyDock(UIPlayerDock.HighlightDock highlight, float duration, bool isShow = true)
    {
        switch (highlight)
        {
            case UIPlayerDock.HighlightDock.AP:
                UIManager.Instance.UIPlayerTop.Tutorial_Hilight_AP(isShow, duration);
                break;
            case UIPlayerDock.HighlightDock.HP:
                UIManager.Instance.UIPlayerTop.Tutorial_Hilight_HP(isShow, duration);
                break;
            case UIPlayerDock.HighlightDock.SOUL:
                UIManager.Instance.UIPlayerTop.Tutorial_Hilight_Soul(isShow, duration);
                break;
            default:
                break;
        }
    }

    protected void ShowPlayerFloatingSoul(string soul, float duration = 2.0f)
    {
        Vector2 targetPosition = Camera.main.WorldToScreenPoint(UIManager.Instance.UIPlayerBottom.GetSoulPosition());
        UIManager.Instance.AddFloatTextSoul(soul, targetPosition, duration, true);
    }

    protected void ShowEnemyFloatingSoul(string soul, float duration = 2.0f)
    {
        Vector2 targetPosition = Camera.main.WorldToScreenPoint(UIManager.Instance.UIPlayerTop.GetSoulPosition());
        UIManager.Instance.AddFloatTextSoul(soul, targetPosition, duration, false);
    }

    protected void ShowPlayerFloatingAp(string value, float duration = 2.0f)
    {
        Vector2 targetPosition = Camera.main.WorldToScreenPoint(UIManager.Instance.UIPlayerBottom.GetAPPosition());
        UIManager.Instance.AddFloatTextAP(value, targetPosition, duration, true);
    }

    protected void ShowEnemyFloatingAp(string value, float duration = 2.0f)
    {
        Vector2 targetPosition = Camera.main.WorldToScreenPoint(UIManager.Instance.UIPlayerTop.GetAPPosition());
        UIManager.Instance.AddFloatTextAP(value, targetPosition, duration, false);
    }
    #endregion

    List<string> _playSoundList = new List<string>();
    protected IEnumerator PlayVoice(PlayerIndex playerIndex, string id, string text, float soundOffset = 0)
    {
        _playSoundList.Add(id);
        bool isFinish = false;
        _manager.ActionPlayCustomVoice(playerIndex, id, text, () => isFinish = true, soundOffset);
        yield return new WaitUntil(() => isFinish == true);
        _playSoundList.Remove(id);
    }

    protected void PlayVoiceWithOutCallback(PlayerIndex playerIndex, string id, string text)
    {
        _manager.ActionPlayCustomVoice(playerIndex, id, text, null, 0);
    }

    List<VoiceData> voiceData = new List<VoiceData>();
    protected void AddVoiceToList(PlayerIndex playerIndex, string id, string text, float soundOffset = 0)
    {
        voiceData.Add(new VoiceData(playerIndex, id, text, soundOffset));
    }

    protected IEnumerator PlayVoiceList()
    {
        while (voiceData.Count > 0)
        {
            VoiceData data = voiceData[0];
            yield return PlayVoice(data.PlayerIndex, data.ID, data.Text, data.SoundOffset);
            voiceData.RemoveAt(0);
        }
        voiceData.Clear();
    }

    protected IEnumerator WaitUntilPlaySoundFinish()
    {
        while (_playSoundList.Count > 0 || voiceData.Count > 0)
        {
            yield return null;
        }
        yield return null;
        yield break;
    }

    protected IEnumerator ShowTutorialPopup(string id, UnityAction onShowComplete = null)
    {
        bool isFinish = false;
        UIManager.Instance.GetUITutorialManager().RequestShowPopup(id, onShowComplete, () => isFinish = true);
        yield return new WaitUntil(() => isFinish == true);
    }

    protected IEnumerator HideTutorialPopup(string id)
    {
        bool isFinish = false;
        UIManager.Instance.GetUITutorialManager().RequestHidePopup(id, () => isFinish = true);
        yield return new WaitUntil(() => isFinish == true);
    }

    protected void CreateDynamicTutorialPopup(string id, bool show = false, UnityAction onClickOK = null)
    {
        TutorialPopupManager.Instance.CreateDynamicPopup(id, show, onClickOK);
    }

    protected void CreateDynamicTutorialPopup(string id, Vector3 fxPos, bool show = false, UnityAction onClickOK = null)
    {
        TutorialPopupManager.Instance.CreateDynamicPopup(id, fxPos, show, onClickOK);
    }

    #region CardDetailUI

    protected void ShowCardDetailUI(int uniqueID)
    {
        TutorialPopupManager.Instance.ShowCardUIManual(uniqueID);
    }

    protected void HighlightCardDetailUI(List<TutorialCardPopupHighlight.HighlightType> highlightList)
    {
        TutorialPopupManager.Instance.ShowCardUIHighlight(highlightList);
        TutorialPopupManager.Instance.ShowHighlightLabel(highlightList);
    }

    protected void HideCardDetailUI()
    {
        TutorialPopupManager.Instance.HideAllCardUIHighlight();
        TutorialPopupManager.Instance.HideCardUI();
    }

    protected IEnumerator ShowCardDetailFromHandAutoHide(int handIndex, float showTime, List<TutorialCardPopupHighlight.HighlightType> highlight)
    {
        int uniqueID = _manager.Data.PlayerDataList[(int)PlayerIndex.One].CardList[CardZone.Hand][handIndex].UniqueID;

        ShowCardDetailUI(uniqueID);
        TutorialPopupManager.Instance.ClearAllHighlight();
        yield return new WaitForSeconds(0.5f);
        HighlightCardDetailUI(highlight);
        yield return new WaitForSeconds(showTime);
        HideCardDetailUI();
    }
    #endregion

    #region ToolTip Popup

    protected void ShowPopupToolTip(string msg, Vector3 showPos)
    {
        TutorialPopupManager.Instance.ShowTooltip(msg, showPos, null);
    }

    protected void HidePopupToolTip()
    {
        TutorialPopupManager.Instance.HideTooltip(null);
    }

    protected void ShowPopupToolTipAutoHide(string msg, float showTime, Vector3 showPos, UnityAction onShowComplete, UnityAction onHideComplete)
    {
        TutorialPopupManager.Instance.ShowAutoHideTooltip(msg, showTime, showPos, onShowComplete, onHideComplete);
    }

    protected void ShowFadeBlackTop(bool isShow)
    {
        if (isShow)
        {
            TutorialPopupManager.Instance.ShowFadeBlack_Top();
        }
        else
        {
            TutorialPopupManager.Instance.HideFadeBlack_Top();
        }
    }
    #endregion

    #endregion

    #region Tutorial Phase Methods

    public void OnShowEndGameResult(bool isVictory, GameMode mode)
    {
        _manager.StartCoroutine(OnPlayEndGameSound(isVictory));
    }
    protected abstract IEnumerator OnPlayEndGameSound(bool isVictory);

    #endregion
}
