﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Tutorial4Controller : StoryGameController
{
    int? spiritTreeID = null;
    int? goblinBomberID = null;
    int? hellDragonID = null;

    int? cliffordID = null;

    bool canPlayCard = false;
    List<string> _soundKeys = new List<string>();

    public Tutorial4Controller(GameManager manager) : base(manager)
    {
    }

    public override int GetLifePoint(PlayerIndex playerIndex, GameMode mode)
    {
        return 30;
    }

    public override string GetBoardID()
    {
        return "TUT_4";
    }

    public override bool IsTutorialValidSlotID(int slotID) { return true; }

    public override bool IsCanUseCard(MinionData minion)
    {
        switch (GameManager.Instance.SubTurn)
        {
            case 2:
                {
                    if (GameManager.Instance.GetHandCount(PlayerIndex.One) == 5)
                    {
                        return minion.BaseID == "CAP";
                    }
                    else
                    {
                        return base.IsCanUseCard(minion);
                    }
                }
        }

        return base.IsCanUseCard(minion);
    }

    #region Phase

    #region SetupPhase

    public override IEnumerator SetupPhase()
    {
        _manager.SetPlayOrder(PlayerIndex.Two, null);
        _manager._isPlayFirstReady = true;

        while (!_manager.IsCanNextPhase || !_manager.IsPhaseReady(GamePhase.SetupData) || !_manager._isPlayFirstReady)
        {
            yield return null;
        }

        _manager.SetRandomSeed(_manager.MatchData.PlayerData[1].RandomSeed);
        _manager.SetLoadingProgress(1.0f);
        NextPhase();

        UIManager.Instance.FadeBlackIn();
    }

    #endregion

    #region Intro Phase

    protected override IEnumerator MiddleIntroPhase()
    {
        Debug.Log("เปิดมาด้วยสกรีนสีดำ ที่พอ fade ออกไป");
        yield return new WaitForSeconds(1f);

        Debug.Log("ก็เห็น token ของ Nyx อยู่ในฝั่ง Player โดยที่ Token ของฝั่งศัตรูยังไม่มีบอกว่าเป็นใคร");
        UIManager.Instance.SpotLight_ShowHeroBot(true);
        yield return new WaitForSeconds(1f);

        yield return PlayVoice(PlayerIndex.One, "TUT_4_001", "TU4_VOICE_001", 2f);

        yield return PlayVoice(PlayerIndex.Two, "TUT_4_002", "TU4_VOICE_002", 0.2f);
        Debug.Log("แล้วก็ปรากฎเป็น token ของ Mephisto ลอยอยู่.");
        {
            UIManager.Instance.UIPlayerTop.Intro_State_ShowAtCenter();
            UIManager.Instance.SpotLight_ShowMid(true);
        }

        yield return new WaitForSeconds(1f);

        Debug.Log("smash ลงช่อง token ของศัตรู แล้ว fade ดำก็หายไป");
        {
            UIManager.Instance.UIPlayerTop.Intro_State_Default();
            UIManager.Instance.SpotLight_ShowMid(false);
            UIManager.Instance.SpotLight_ShowHeroBot(false);
            UIManager.Instance.FadeBlackOut();
        }

        yield return PlayVoice(PlayerIndex.One, "TUT_4_003", "TU4_VOICE_003", 0.3f);
        yield return PlayVoice(PlayerIndex.One, "TUT_4_004", "TU4_VOICE_004", 0.3f);
        yield return PlayVoice(PlayerIndex.Two, "TUT_4_005", "TU4_VOICE_005", 0.2f);
        // Init Draw
        //_manager.InitDraw();
        _manager.RequestDrawCard(PlayerIndex.One, 4);
        while (!_manager.IsCanNextPhase)
        {
            yield return null;
        }

    }

    protected override IEnumerator EndIntroPhase()
    {
        bool isFinish = false;
        CreateDynamicTutorialPopup("TU4_DPUP_1", GameHelper.CenterScreen, true, () => isFinish = true);
        yield return new WaitUntil(() => isFinish == true);
    }
    #endregion

    #region Begin Phase

    public override IEnumerator IEOnEndBeginPhase()
    {
        switch (_manager.SubTurn)
        {
            default:
                break;
        }
        yield break;
    }

    #endregion

    #region Spirit Phase

    //TODO: implement using key instead.
    public override IEnumerator OnEndSpiritPhaseTutorial(UnityAction onComplete)
    {
        if (_manager.SubTurn > 2)
        {
            int playerHP = _manager.Data.PlayerDataList[(int)PlayerIndex.One].CurrentHP;
            int enemyHP = _manager.Data.PlayerDataList[(int)PlayerIndex.Two].CurrentHP;

            int playerMaxHP = _manager.Data.PlayerDataList[(int)PlayerIndex.One].MaxHP;
            int enemyMaxHP = _manager.Data.PlayerDataList[(int)PlayerIndex.Two].MaxHP;

            // Receive Damage First time.
            {
                if (_manager.SubTurn % 2 == 0)
                {
                    bool isEnemyReceiveDamage = enemyHP < enemyMaxHP;
                    if (isEnemyReceiveDamage && _soundKeys.Contains("TU4_VOICE_016") == false)
                    {
                        _soundKeys.Add("TU4_VOICE_016");
                        yield return PlayVoice(PlayerIndex.Two, "TUT_4_016", "TU4_VOICE_016", 0.2f);
                    }
                }
                else
                {
                    bool isPlayerReceiveDamage = playerHP < playerMaxHP;
                    if (isPlayerReceiveDamage && _soundKeys.Contains("TU4_VOICE_015") == false)
                    {
                        _soundKeys.Add("TU4_VOICE_015");
                        yield return PlayVoice(PlayerIndex.One, "TUT_4_015", "TU4_VOICE_015", 0.2f);
                    }
                }
            }

            // Check if HP <= 8
            {
                if (_manager.SubTurn % 2 == 0)
                {
                    if (enemyHP <= 5 && _soundKeys.Contains("TU4_VOICE_025") == false)
                    {
                        _soundKeys.Add("TU4_VOICE_025");
                        yield return PlayVoice(PlayerIndex.Two, "TUT_4_025", "TU4_VOICE_025", 0f);
                    }
                    if (enemyHP <= 8 && _soundKeys.Contains("TU4_VOICE_010") == false)
                    {
                        _soundKeys.Add("TU4_VOICE_010");
                        yield return PlayVoice(PlayerIndex.One, "TUT_4_010", "TU4_VOICE_010", 0.2f);
                    }
                }
                else
                {
                    if (playerHP <= 5 && _soundKeys.Contains("TU4_VOICE_012") == false)
                    {
                        _soundKeys.Add("TU4_VOICE_012");
                        yield return PlayVoice(PlayerIndex.One, "TUT_4_012", "TU4_VOICE_012", 0.2f);
                    }
                    if (playerHP <= 8 && _soundKeys.Contains("TU4_VOICE_011") == false)
                    {
                        _soundKeys.Add("TU4_VOICE_011");
                        yield return PlayVoice(PlayerIndex.Two, "TUT_4_011", "TU4_VOICE_011", 0.2f);
                    }
                }
            }

        }
        onComplete?.Invoke();
        yield break;
    }

    #endregion

    #region Draw Phase

    public override IEnumerator AfterAddAP()
    {
        switch (_manager.SubTurn)
        {
            case 6:
                {
                    {
                        BattleCardData card;
                        if (_manager.FindBattleCard("CT0614", CardZone.Deck, PlayerIndex.One, out card))
                        {
                            cliffordID = card.UniqueID;
                            bool isFinish = false;
                            _manager.RequestMoveCardToZone(card.UniqueID, CardZone.Deck, CardZone.Hand, -1, () => { isFinish = true; });
                            yield return new WaitUntil(() => isFinish == true);

                        }
                    }
                    {
                        BattleCardData card;
                        if (_manager.FindBattleCard("CT0612", CardZone.Battlefield, PlayerIndex.Two, out card))
                        {
                            _manager.StartCoroutine(PlayVoice(PlayerIndex.One, "TUT_4_018", "TU4_VOICE_018", 0f));
                        }
                        else
                        {
                            _manager.StartCoroutine(PlayVoice(PlayerIndex.One, "TUT_4_017", "TU4_VOICE_017", 0f));
                        }
                    }
                }
                break;
        }
    }

    public override void OnSetDrawCount(int subturn, ref int drawCount, ref bool isDrawAP)
    {
        isDrawAP = false;
        switch (_manager.SubTurn)
        {
            case 1: drawCount = 3; break;
            case 2:
                drawCount = 0;
                isDrawAP = true;
                break;
            case 3: drawCount = 2; break;
            case 4: drawCount = 5; break;
            case 5: drawCount = 2; break;
            case 7: drawCount = 1; break;
            case 9: drawCount = 1; break;
            default:
                int toDrawCount = _manager.MaxHandCard;
                int handCount = _manager.Data.PlayerDataList[(int)_manager.CurrentPlayerIndex].CardList[CardZone.Hand].Count;
                int deckCount = _manager.Data.PlayerDataList[(int)_manager.CurrentPlayerIndex].CardList[CardZone.Deck].Count;
                int handSlot = toDrawCount - handCount;

                drawCount = Mathf.Min(handSlot, deckCount);
                break;
        }
    }
    #endregion

    #region PrePlay Phase

    public override IEnumerator OnEndPrePlayPhaseTutorial(UnityAction onComplete)
    {
        switch (_manager.SubTurn)
        {
            case 1:
                {
                    yield return PlayVoice(PlayerIndex.Two, "TUT_4_006", "TU4_VOICE_006", 0f);
                    yield return PlayVoice(PlayerIndex.One, "TUT_4_007", "TU4_VOICE_007", 0f);
                }
                break;
            default:
                {
                    if (_manager.SubTurn % 2 == 0)// Player turn
                    {
                        if (_soundKeys.Contains("TU4_VOICE_020"))
                            break;
                        BattleCardData card;
                        if (_manager.FindBattleCard("CT0602", CardZone.Hand, PlayerIndex.One, out card))
                        {
                            goblinBomberID = card.UniqueID;
                            _soundKeys.Add("TU4_VOICE_020");
                            yield return PlayVoice(PlayerIndex.One, "TUT_4_020", "TU4_VOICE_020", 0f);
                        }
                    }
                }
                break;
        }
        onComplete?.Invoke();
        yield break;
    }

    #endregion

    #region Play Phase

    public override void OnBeginPlayPhaseTutorial()
    {
        switch (_manager.SubTurn)
        {
            case 2:
                {
                    int cacheHandCount = _manager.GetHandCount(PlayerIndex.One);
                    bool isShow = true;
                    base.ShowPopupToolTip("TU4_TOOLTIP_001", base.GetSlotScreenPosition(13));
                    base.PointerPlayerCard(4, true, GameHelper.GetColorByHtmlCode(StoryGameController.HIGHLIGHT_POINTER_GREEN));

                    UnityAction onIdleToSelect = delegate ()
                    {
                        base.HidePopupToolTip();
                    };
                    UnityAction onSelectToIdle = delegate ()
                    {
                        if (isShow)
                        {
                            base.ShowPopupToolTip("TU4_TOOLTIP_001", base.GetSlotScreenPosition(13));
                        }
                    };
                    UnityAction onSelectToDrag = delegate () { };
                    UnityAction onDragToSelect = delegate () { };

                    UnityAction onDragToIdle = delegate ()
                    {
                        int handCount = _manager.GetHandCount(PlayerIndex.One);
                        if (cacheHandCount != handCount)
                        {
                            if (isShow == true)
                            {
                                base.HidePopupToolTip();
                                ShowFadeBlackTop(true);
                                GameHelper.DelayCallback(4f, delegate
                                {
                                    ShowFadeBlackTop(false);
                                });
                                HighlightPlayerDock(UIPlayerDock.HighlightDock.AP, 9999, true);
                                base.CreateDynamicTutorialPopup("TU4_DPUP_2", base.GetSlotScreenPosition(13));
                                isShow = false;
                            }
                        }
                        else
                        {
                            if (isShow)
                            {
                                base.ShowPopupToolTip("TU4_TOOLTIP_001", base.GetSlotScreenPosition(13));
                            }
                        }
                        if (CanEndTurn(_manager.CurrentPlayerIndex))
                        {
                            HighlightPlayerDock(UIPlayerDock.HighlightDock.AP, 9999, false);
                            GameHelper.DelayCallback(1f, delegate
                            {
                                UIManager.Instance.GetUITutorialManager().RequestShowPopup("T_ArrowEndTurn");
                                ControllerData.SetIsCanEndTurn(true);
                            });
                            ControllerData.ClearPlayPhaseEventCallback();
                        }
                    };

                    ControllerData.SetCallbackOnIdleToSelect(onIdleToSelect);
                    ControllerData.SetCallbackOnSelectToIdle(onSelectToIdle);
                    ControllerData.SetCallbackOnSelectToDrag(onSelectToDrag);
                    ControllerData.SetCallbackOnDragToSelect(onDragToSelect);
                    ControllerData.SetCallbackOnDragToIdle(onDragToIdle);

                    ControllerData.SetIsBotCanAction(false);
                    ControllerData.SetIsCanEndTurn(false);
                }
                break;
            case 4:
                {
                    int cacheHandCount = _manager.GetHandCount(PlayerIndex.One);
                    bool isShow = true;
                    base.ShowPopupToolTip("TU4_TOOLTIP_002", base.GetSlotScreenPosition(13));
                    UnityAction onIdleToSelect = delegate ()
                    {
                        base.HidePopupToolTip();
                    };
                    UnityAction onSelectToIdle = delegate ()
                    {
                        if (isShow)
                        {
                            base.ShowPopupToolTip("TU4_TOOLTIP_002", base.GetSlotScreenPosition(13));
                        }
                    };
                    UnityAction onSelectToDrag = delegate () { };
                    UnityAction onDragToSelect = delegate () { };

                    UnityAction onDragToIdle = delegate ()
                    {
                        int handCount = _manager.GetHandCount(PlayerIndex.One);
                        if (cacheHandCount != handCount)
                        {
                            if (isShow)
                            {
                                base.HidePopupToolTip();
                                base.CreateDynamicTutorialPopup("TU4_DPUP_3", base.GetSlotScreenPosition(13));
                                isShow = false;
                            }
                        }
                        else
                        {
                            if (isShow)
                            {
                                base.ShowPopupToolTip("TU4_TOOLTIP_002", base.GetSlotScreenPosition(13));
                            }
                        }
                        if (CanEndTurn(_manager.CurrentPlayerIndex))
                        {
                            GameHelper.DelayCallback(1f, delegate
                            {
                                UIManager.Instance.GetUITutorialManager().RequestShowPopup("T_ArrowEndTurn");
                                ControllerData.SetIsCanEndTurn(true);
                            });
                            ControllerData.ClearPlayPhaseEventCallback();
                        }
                    };

                    ControllerData.SetCallbackOnIdleToSelect(onIdleToSelect);
                    ControllerData.SetCallbackOnSelectToIdle(onSelectToIdle);
                    ControllerData.SetCallbackOnSelectToDrag(onSelectToDrag);
                    ControllerData.SetCallbackOnDragToSelect(onDragToSelect);
                    ControllerData.SetCallbackOnDragToIdle(onDragToIdle);

                    ControllerData.SetIsBotCanAction(false);
                    ControllerData.SetIsCanEndTurn(false);
                }
                break;
            default:
                {
                    if (_manager.SubTurn % 2 == 0)
                    {
                        UnityAction onIdleToSelect = delegate () { };
                        UnityAction onSelectToIdle = delegate () { };
                        UnityAction onSelectToDrag = delegate () { };
                        UnityAction onDragToSelect = delegate () { };

                        UnityAction onDragToIdle = delegate ()
                        {
                            if (CanEndTurn(_manager.CurrentPlayerIndex))
                            {
                                _manager.StartCoroutine(GameHelper.ExecuteIEnumerator(WaitUntilPlaySoundFinish(), () =>
                                {
                                    ControllerData.SetIsCanEndTurn(true);
                                }));
                                ControllerData.ClearPlayPhaseEventCallback();
                            }
                        };

                        ControllerData.SetCallbackOnIdleToSelect(onIdleToSelect);
                        ControllerData.SetCallbackOnSelectToIdle(onSelectToIdle);
                        ControllerData.SetCallbackOnSelectToDrag(onSelectToDrag);
                        ControllerData.SetCallbackOnDragToSelect(onDragToSelect);
                        ControllerData.SetCallbackOnDragToIdle(onDragToIdle);

                        ControllerData.SetIsBotCanAction(false);
                        ControllerData.SetIsCanEndTurn(false);
                    }
                    else
                    {
                        ControllerData.SetIsBotCanAction(true);
                        ControllerData.SetIsCanEndTurn(true);
                    }
                }
                break;
        }
    }

    public override void OnEndPlayPhaseTutorial(UnityAction onComplete)
    {
        _manager.StartCoroutine(IEOnEndPlayPhaseTutorial(onComplete));
    }

    // before battle.
    private IEnumerator IEOnEndPlayPhaseTutorial(UnityAction onComplete)
    {
        UIManager.Instance.GetUITutorialManager().RequestHidePopup("T_ArrowEndTurn");
        switch (_manager.SubTurn)
        {
            case 1:
                _manager.Bot.SetEnable(true);
                ControllerData.Bot.SetEnable(false);
                break;
            case 2:
                _manager.Bot.SetEnable(false);
                ControllerData.Bot.SetEnable(true);
                break;
            case 3:

                if (_soundKeys.Contains("TU4_VOICE_022") == false)
                {
                    BattleCardData card;
                    if (_manager.FindBattleCard("CT0612", CardZone.Battlefield, PlayerIndex.Two, out card))
                    {
                        spiritTreeID = card.UniqueID;
                        _soundKeys.Add("TU4_VOICE_022");
                        yield return PlayVoice(PlayerIndex.Two, "TUT_4_022", "TU4_VOICE_022", 0f);
                        yield return PlayVoice(PlayerIndex.One, "TUT_4_023", "TU4_VOICE_023", 0f);
                    }
                }
                _manager.Bot.SetEnable(true);
                ControllerData.Bot.SetEnable(false);
                break;
            default:
                if (_manager.SubTurn % 2 == 1)// AI turn
                {
                    if (_soundKeys.Contains("TU4_VOICE_008") == false)
                    {
                        BattleCardData card;
                        if (_manager.FindBattleCard("CT0652", CardZone.Battlefield, PlayerIndex.Two, out card))
                        {
                            _soundKeys.Add("TU4_VOICE_008");
                            _manager.StartCoroutine(PlayVoice(PlayerIndex.Two, "TUT_4_008", "TU4_VOICE_008", 0f));
                        }
                    }

                    if (_soundKeys.Contains("TU4_VOICE_024") == false)
                    {
                        BattleCardData card;
                        if (_manager.FindBattleCard("CT0653", CardZone.Battlefield, PlayerIndex.Two, out card))
                        {
                            hellDragonID = card.UniqueID;
                            _soundKeys.Add("TU4_VOICE_024");
                            yield return PlayVoice(PlayerIndex.Two, "TUT_4_024", "TU4_VOICE_024", 0f);
                        }
                    }

                }
                break;
        }
        onComplete?.Invoke();
        yield break;
    }
    #endregion

    #region Battle Phase

    public override IEnumerator OnEndPreBattlePhaseTutorial(UnityAction onComplete)
    {
        onComplete?.Invoke();
        yield break;
    }

    public override IEnumerator OnPostBattlePhaseTutorial(UnityAction onComplete)
    {
        switch (_manager.SubTurn)
        {
            default:
                if (spiritTreeID != null)
                {
                    if (_soundKeys.Contains("TU4_VOICE_019") == false)
                    {
                        BattleCardData card;
                        if (_manager.FindBattleCard((int)spiritTreeID, out card))
                        {
                            if (card.CurrentZone == CardZone.Graveyard)
                            {
                                _soundKeys.Add("TU4_VOICE_019");
                                yield return PlayVoice(PlayerIndex.One, "TUT_4_019", "TU4_VOICE_019", 0f);
                            }
                        }
                    }
                }

                if (cliffordID != null)
                {
                    if (_soundKeys.Contains("TU4_VOICE_009") == false)
                    {
                        BattleCardData card;
                        if (_manager.FindBattleCard((int)cliffordID, out card))
                        {
                            if (card.CurrentZone == CardZone.Graveyard)
                            {
                                _soundKeys.Add("TU4_VOICE_009");
                                _manager.StartCoroutine(PlayVoice(PlayerIndex.Two, "TUT_4_009", "TU4_VOICE_009", 0f));
                            }
                        }
                    }
                }

                break;
        }
        onComplete?.Invoke();
        yield break;
    }

    #endregion

    #region End Game
    protected override IEnumerator OnPlayEndGameSound(bool isVictory)
    {
        TutorialPopupManager.Instance.BTNShowDynamicPopup.gameObject.SetActive(false);
        if (isVictory)
        {
            yield return PlayVoice(PlayerIndex.One, "TUT_4_013", "TU4_VOICE_013", 0.2f);
        }
        else
        {
            yield return PlayVoice(PlayerIndex.Two, "TUT_4_014", "TU4_VOICE_014", 0f);
        }
    }

    public override void OnPlayPhaseTutorial() { }

    public override void OnCheckClaimReward(UnityAction onComplete)
    {
        DataManager.Instance.ClaimTutorialRewardByTutorialIndex(3);
        onComplete?.Invoke();
    }

    #endregion

    #endregion
}
