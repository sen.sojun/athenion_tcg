﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Tutorial2Controller : StoryGameController
{
    private List<string> _soundKeys = new List<string>();

    public Tutorial2Controller(GameManager manager) : base(manager)
    {
        GameManager.OnUseCard += GameManager_OnUseCard;
        GameManager.OnDestroy += GameManager_OnDestroy;
    }

    private void GameManager_OnDestroy()
    {
        GameManager.OnUseCard -= GameManager_OnUseCard;
        GameManager.OnDestroy -= GameManager_OnDestroy;
    }

    public override int GetLifePoint(PlayerIndex playerIndex, GameMode mode)
    {
        if (playerIndex == PlayerIndex.Two)
            return 11;
        else
            return 20;
    }

    public override string GetBoardID()
    {
        return "TUT_2";
    }

    public override bool IsTutorialValidSlotID(int slotID)
    {
        /*
        [00][01][02][03]
        [04][05][06][07]
        [08][09][10][11]
        [12][13][14][15]
        */

        if (GameManager.Instance.Phase == GamePhase.Play)
        {
            switch (GameManager.Instance.SubTurn)
            {
                case 2:
                    return slotID == GameHelper.SlotCodeToSlotID("2C");
                case 4:
                    return slotID == GameHelper.SlotCodeToSlotID("1B");
                case 6:
                    return slotID == GameHelper.SlotCodeToSlotID("2C");
                case 8:
                    if (GetSelectCardData() == null)
                    {
                        return false;
                    }
                    else
                    {
                        return (slotID == GameHelper.SlotCodeToSlotID("3B") && GetSelectCardData().BaseID == "CT0584") ||
                              (slotID == GameHelper.SlotCodeToSlotID("4B") && GetSelectCardData().BaseID == "CT0585");
                    }
                case 10:
                    return slotID == GameHelper.SlotCodeToSlotID("4A");
                case 12:
                    return slotID == GameHelper.SlotCodeToSlotID("3B");
                default: return true;
            }
        }

        return true;
    }


    #region Phase

    #region SetupPhase

    public override IEnumerator SetupPhase()
    {
        _manager.SetPlayOrder(PlayerIndex.Two, null);
        _manager._isPlayFirstReady = true;

        while (!_manager.IsCanNextPhase || !_manager.IsPhaseReady(GamePhase.SetupData) || !_manager._isPlayFirstReady)
        {
            yield return null;
        }

        _manager.SetRandomSeed(_manager.MatchData.PlayerData[1].RandomSeed);
        _manager.SetLoadingProgress(1.0f);
        NextPhase();

        UIManager.Instance.FadeBlackIn();
    }
    #endregion

    #region Intro Phase
    protected override IEnumerator MiddleIntroPhase()
    {
        Debug.Log("เปิดมาด้วยสกรีนสีดำ ที่พอ fade ออกไป ก็เห็น token ของ Mary อยู่ในฝั่ง Player โดยที่ Token ของฝั่งศัตรูยังไม่มีบอกว่าเป็นใคร");
        UIManager.Instance.SpotLight_ShowHeroBot(true);
        yield return PlayVoice(PlayerIndex.One, "TUT_2_001", "TU2_VOICE_001", 0);

        Debug.Log("fade ดำ 1.5 วิ");
        yield return new WaitForSeconds(1.5f);
        {
            UIManager.Instance.UIPlayerTop.Intro_State_ShowAtCenter();
            UIManager.Instance.SpotLight_ShowMid(true);
        }
        Debug.Log("แล้วก็ปรากฎเป็น token ของ Kaare ลอยอยู่");
        Debug.Log("smash ลงช่อง token ของศัตรู");
        Debug.Log("fade ดำ หายไป");
        yield return new WaitForSeconds(1.0f);
        {
            UIManager.Instance.UIPlayerTop.Intro_State_Default();
            UIManager.Instance.SpotLight_ShowMid(false);
            UIManager.Instance.SpotLight_ShowHeroBot(false);
            UIManager.Instance.FadeBlackOut();
        }

        yield return PlayVoice(PlayerIndex.Two, "TUT_2_002", "TU2_VOICE_002", 0.1f);
        yield return PlayVoice(PlayerIndex.One, "TUT_2_003", "TU2_VOICE_003", 0);
        yield return PlayVoice(PlayerIndex.One, "TUT_2_004", "TU2_VOICE_004", 0.1f);

    }

    protected override IEnumerator EndIntroPhase()
    {
        {
            bool isFinish = false;
            Debug.Log(GameHelper.CenterScreen);
            CreateDynamicTutorialPopup("TU2_DPUP_1", GameHelper.CenterScreen, true, () => isFinish = true);
            yield return new WaitUntil(() => isFinish == true);
        }

        yield break;
    }
    #endregion

    #region Begin Phase

    public override IEnumerator IEOnEndBeginPhase()
    {
        switch (_manager.SubTurn)
        {
            case 4:
                //yield return PlayVoice(PlayerIndex.One, "TUT_2_013", "TU2_VOICE_013", 0f);
                yield return PlayVoice(PlayerIndex.One, "TUT_2_014", "TU2_VOICE_014", 0f);
                break;
            case 5:
                {
                    //yield return PlayVoice(PlayerIndex.Two, "TUT_2_015", "TU2_VOICE_015", 1f);
                }
                break;
            case 6:
                yield return PlayVoice(PlayerIndex.One, "TUT_2_019", "TU2_VOICE_019", 0f);
                break;
            case 7:
                //yield return PlayVoice(PlayerIndex.Two, "TUT_2_025", "TU2_VOICE_025", 1f);
                break;
            case 8:
                //yield return PlayVoice(PlayerIndex.One, "TUT_2_028", "TU2_VOICE_028", 0f);
                break;
            case 9:
                PlayVoiceWithOutCallback(PlayerIndex.Two, "TUT_2_030", "TU2_VOICE_030");
                break;
            case 10:
                //yield return PlayVoice(PlayerIndex.One, "TUT_2_031", "TU2_VOICE_031", 0f);
                //yield return PlayVoice(PlayerIndex.One, "TUT_2_032", "TU2_VOICE_032", 0f);
                //yield return PlayVoice(PlayerIndex.One, "TUT_2_033", "TU2_VOICE_033", 0f);
                break;
            case 11:
                _manager.StartCoroutine(PlayVoice(PlayerIndex.Two, "TUT_2_036", "TU2_VOICE_036", 0f));
                break;
            case 12:
                {
                    yield return PlayVoice(PlayerIndex.One, "TUT_2_038", "TU2_VOICE_038", 0f);
                    _manager.StartCoroutine(PlayVoice(PlayerIndex.One, "TUT_2_039", "TU2_VOICE_039", 0f));
                }
                break;
            case 14:
                {
                    yield return PlayVoice(PlayerIndex.One, "TUT_2_051", "TU2_VOICE_051", 0f);
                }
                break;
            default:
                if (_manager.SubTurn % 2 == 0)
                {
                    int enemyHP = _manager.Data.PlayerDataList[(int)PlayerIndex.Two].CurrentHP;
                    if (enemyHP - _manager.GetSumSpirit(PlayerIndex.One) <= 0)
                    {
                        yield return PlayVoice(PlayerIndex.Two, "TUT_2_055", "TU2_VOICE_055", 0f);
                    }
                }
                yield break;
        }
    }

    #endregion
    #region SpiritPhase

    public override IEnumerator OnEndSpiritPhaseTutorial(UnityAction onComplete)
    {
        switch (_manager.SubTurn)
        {
            case 12:
                {
                    yield return PlayVoice(PlayerIndex.Two, "TUT_2_040", "TU2_VOICE_040", 1f);
                    //yield return PlayVoice(PlayerIndex.One, "TUT_2_041", "TU2_VOICE_041", 0f);
                    //yield return PlayVoice(PlayerIndex.One, "TUT_2_042", "TU2_VOICE_042", 0f);
                }
                break;
            case 14:
                {
                    yield return PlayVoice(PlayerIndex.Two, "TUT_2_052", "TU2_VOICE_052", .1f);
                    //yield return PlayVoice(PlayerIndex.One, "TUT_2_053", "TU2_VOICE_053", 0f);
                    _manager.StartCoroutine(PlayVoice(PlayerIndex.One, "TUT_2_054", "TU2_VOICE_054", 0f));
                }
                break;
            default:
                if (_manager.SubTurn % 2 == 0)
                {
                    int enemyHP = _manager.Data.PlayerDataList[(int)PlayerIndex.Two].CurrentHP;
                    if (enemyHP <= 8 && _soundKeys.Contains("TU2_VOICE_057") == false)
                    {
                        _soundKeys.Add("TU2_VOICE_057");
                        PlayVoiceWithOutCallback(PlayerIndex.One, "TUT_2_057", "TU2_VOICE_057");
                    }
                }
                break;
        }
        onComplete?.Invoke();
        yield break;
    }

    #endregion

    #region Draw Phase
    public override void OnSetDrawCount(int subturn, ref int drawCount, ref bool isDrawAP)
    {
        isDrawAP = false;
        switch (_manager.SubTurn)
        {
            case 1: drawCount = 1; break;
            case 2: drawCount = 1; break;
            case 3: drawCount = 1; break;
            case 4: drawCount = 1; break;
            case 5: drawCount = 1; break;

            case 6: drawCount = 1; break;
            case 7: drawCount = 1; break;

            case 8: drawCount = 2; break;
            case 9: drawCount = 2; break;
            case 10: drawCount = 1; break;
            case 11: drawCount = 2; break;
            case 12: drawCount = 1; break;
            case 13: drawCount = 2; break;
            case 14: drawCount = 2; break;

            default:
                {
                    int toDrawCount = /*_manager.MaxHandCard*/ 2;
                    int handCount = _manager.Data.PlayerDataList[(int)_manager.CurrentPlayerIndex].CardList[CardZone.Hand].Count;
                    int deckCount = _manager.Data.PlayerDataList[(int)_manager.CurrentPlayerIndex].CardList[CardZone.Deck].Count;
                    int handSlot = toDrawCount - handCount;

                    drawCount = Mathf.Min(handSlot, deckCount);
                }
                break;
        }
    }

    public override IEnumerator BeforeAddAP()
    {
        switch (_manager.SubTurn)
        {
            default:
                break;
        }
        yield break;
    }

    public override IEnumerator AddingAP()
    {
        switch (_manager.SubTurn)
        {
            case 10:
                _manager.RequestSetPlayerAP(_manager.CurrentPlayerIndex, 1);
                yield return new WaitForSeconds(.2f);
                _manager.RequestSetPlayerAP(_manager.CurrentPlayerIndex, 2);
                yield return new WaitForSeconds(.2f);
                _manager.RequestSetPlayerAP(_manager.CurrentPlayerIndex, 3);
                yield return new WaitForSeconds(.2f);
                break;
            default:
                _manager.RequestSetPlayerAP(_manager.CurrentPlayerIndex, 3);
                break;
        }
    }

    public override IEnumerator AfterAddAP()
    {
        switch (_manager.SubTurn)
        {
            case 10:
                base.ShowPlayerFloatingAp("+3");
                ShowPopupToolTip("TU2_TOOLTIP_001", GetSlotScreenPosition(10));
                break;
        }
        yield break;
    }
    #endregion

    #region PrePlay Phase

    public override IEnumerator OnEndPrePlayPhaseTutorial(UnityAction onComplete)
    {
        switch (_manager.SubTurn)
        {
            case 1:
                yield return PlayVoice(PlayerIndex.Two, "TUT_2_005", "TU2_VOICE_005", 0f);
                break;
            case 3:
                //yield return PlayVoice(PlayerIndex.Two, "TUT_2_008", "TU2_VOICE_008", 0f);
                break;
            case 6:
                {
                    //yield return PlayVoice(PlayerIndex.One, "TUT_2_020", "TU2_VOICE_020", 0.2f);
                    //yield return PlayVoice(PlayerIndex.One, "TUT_2_021", "TU2_VOICE_021", 0.2f);
                    break;
                }
            case 8:
                {
                    List<TutorialCardPopupHighlight.HighlightType> highlight = new List<TutorialCardPopupHighlight.HighlightType>() {
                         TutorialCardPopupHighlight.HighlightType.Soul
                    };
                    yield return ShowCardDetailFromHandAutoHide(0, PLAY_CARD_DETAIL_TIME, highlight);

                    HighlightPlayerHand(0, highlight, true);
                    HighlightPlayerHand(1, highlight, true);
                }
                break;
            case 10:
                {
                    AddVoiceToList(PlayerIndex.One, "TUT_2_034", "TU2_VOICE_034", 0f);
                    AddVoiceToList(PlayerIndex.One, "TUT_2_035", "TU2_VOICE_035", 0f);
                    _manager.StartCoroutine(PlayVoiceList());
                }
                break;
            case 12:
                {
                    List<TutorialCardPopupHighlight.HighlightType> highlight = new List<TutorialCardPopupHighlight.HighlightType>() {
                         TutorialCardPopupHighlight.HighlightType.Ability
                    };
                    yield return ShowCardDetailFromHandAutoHide(0, PLAY_CARD_DETAIL_TIME, highlight);

                    AddVoiceToList(PlayerIndex.One, "TUT_2_044", "TU2_VOICE_044", 0f);
                    AddVoiceToList(PlayerIndex.One, "TUT_2_045", "TU2_VOICE_045", 0f);
                    _manager.StartCoroutine(PlayVoiceList());
                }
                break;
            case 13:
                _manager.StartCoroutine(PlayVoice(PlayerIndex.Two, "TUT_2_046", "TU2_VOICE_046", 0f));
                break;
            case 15:
                break;
            default:
                break;
        }
        onComplete?.Invoke();
    }

    #endregion

    #region Play Phase

    public override void OnBeginPlayPhaseTutorial()
    {
        switch (_manager.SubTurn)
        {
            case 2:
                {
                    List<int> highlightBoard = new List<int>() { 6 };
                    ShowHighlightBoard(highlightBoard, Karamucho.UI.BoardSlotHolder.SlotHilightType.Hilight);

                    UnityAction onIdleToSelect = delegate () { };
                    UnityAction onSelectToIdle = delegate () { };
                    UnityAction onSelectToDrag = delegate () { };
                    UnityAction onDragToSelect = delegate () { };

                    UnityAction onDragToIdle = delegate ()
                    {
                        if (_manager.IsEmptyHand(_manager.CurrentPlayerIndex))
                        {
                            HideHighlightBoard(highlightBoard);
                            ShowHighlightArrowToken(6, S_Dir, true);

                            ShowPopupToolTip("TU2_TOOLTIP_002", base.GetSlotScreenPosition(13));
                            base.CreateDynamicTutorialPopup("TU2_DPUP_2", base.GetSlotScreenPosition(13));
                            base.ShowRemind();
                            GameHelper.DelayCallback(2f, delegate
                            {
                                UIManager.Instance.GetUITutorialManager().RequestShowPopup("T_ArrowEndTurn");
                                ControllerData.SetIsCanEndTurn(true);
                            });
                            ControllerData.ClearPlayPhaseEventCallback();
                        }
                    };

                    ControllerData.SetCallbackOnIdleToSelect(onIdleToSelect);
                    ControllerData.SetCallbackOnSelectToIdle(onSelectToIdle);
                    ControllerData.SetCallbackOnSelectToDrag(onSelectToDrag);
                    ControllerData.SetCallbackOnDragToSelect(onDragToSelect);
                    ControllerData.SetCallbackOnDragToIdle(onDragToIdle);

                    ControllerData.SetIsBotCanAction(false);
                    ControllerData.SetIsCanEndTurn(false);
                }
                break;
            case 4:
                {
                    List<int> highlightBoard = new List<int>() { 1 };
                    ShowHighlightBoard(highlightBoard, Karamucho.UI.BoardSlotHolder.SlotHilightType.Hilight);

                    UnityAction onIdleToSelect = delegate () { };
                    UnityAction onSelectToIdle = delegate () { };
                    UnityAction onSelectToDrag = delegate () { };
                    UnityAction onDragToSelect = delegate () { };

                    UnityAction onDragToIdle = delegate ()
                    {
                        if (_manager.IsEmptyHand(_manager.CurrentPlayerIndex))
                        {
                            HideHighlightBoard(highlightBoard);

                            GameHelper.DelayCallback(1f, delegate
                            {
                                UIManager.Instance.GetUITutorialManager().RequestShowPopup("T_ArrowEndTurn");
                                ControllerData.SetIsCanEndTurn(true);
                            });
                            ControllerData.ClearPlayPhaseEventCallback();
                        }
                    };

                    ControllerData.SetCallbackOnIdleToSelect(onIdleToSelect);
                    ControllerData.SetCallbackOnSelectToIdle(onSelectToIdle);
                    ControllerData.SetCallbackOnSelectToDrag(onSelectToDrag);
                    ControllerData.SetCallbackOnDragToSelect(onDragToSelect);
                    ControllerData.SetCallbackOnDragToIdle(onDragToIdle);

                    ControllerData.SetIsBotCanAction(false);
                    ControllerData.SetIsCanEndTurn(false);
                }
                break;
            case 6:
                {
                    List<int> highlightBoard = new List<int>() { 6 };
                    ShowHighlightBoard(highlightBoard, Karamucho.UI.BoardSlotHolder.SlotHilightType.Hilight);

                    UnityAction onIdleToSelect = delegate () { };
                    UnityAction onSelectToIdle = delegate () { };
                    UnityAction onSelectToDrag = delegate () { };
                    UnityAction onDragToSelect = delegate () { };

                    UnityAction onDragToIdle = delegate ()
                    {
                        if (_manager.IsEmptyHand(_manager.CurrentPlayerIndex))
                        {
                            HideHighlightBoard(highlightBoard);

                            GameHelper.DelayCallback(1f, delegate
                            {
                                UIManager.Instance.GetUITutorialManager().RequestShowPopup("T_ArrowEndTurn");
                                ControllerData.SetIsCanEndTurn(true);
                            });
                            ControllerData.ClearPlayPhaseEventCallback();
                        }
                    };

                    ControllerData.SetCallbackOnIdleToSelect(onIdleToSelect);
                    ControllerData.SetCallbackOnSelectToIdle(onSelectToIdle);
                    ControllerData.SetCallbackOnSelectToDrag(onSelectToDrag);
                    ControllerData.SetCallbackOnDragToSelect(onDragToSelect);
                    ControllerData.SetCallbackOnDragToIdle(onDragToIdle);

                    ControllerData.SetIsBotCanAction(false);
                    ControllerData.SetIsCanEndTurn(false);
                }
                break;
            case 8:
                {
                    Vector3 offset = new Vector3(0, 0, -1 * UIManager.SLOT_WIDTH);
                    ShowPopupToolTip("TU2_TOOLTIP_007", base.GetSlotScreenPosition(12, offset));
                    HighlightPlayerDock(UIPlayerDock.HighlightDock.AP, 9999, true);

                    base.PointerPlayerCard(0, true, GameHelper.GetColorByHtmlCode(StoryGameController.HIGHLIGHT_POINTER_BLUE));
                    base.PointerPlayerCard(1, true, GameHelper.GetColorByHtmlCode(StoryGameController.HIGHLIGHT_POINTER_PURE_GREEN));

                    List<int> highlight3B = new List<int>() { 9 }; //3B
                    List<int> highlight4B = new List<int>() { 13 }; //4B

                    ShowHighlightBoard(highlight3B, GameHelper.GetColorByHtmlCode(StoryGameController.HIGHLIGHT_POINTER_BLUE));
                    ShowHighlightBoard(highlight4B, GameHelper.GetColorByHtmlCode(StoryGameController.HIGHLIGHT_POINTER_PURE_GREEN));

                    CardUIData lastCardSelected = null;
                    UnityAction onIdleToSelect = delegate ()
                    {
                        lastCardSelected = GetSelectCardData();
                        if (lastCardSelected.BaseID == "CT0584")
                        {
                            HideHighlightBoard(highlight4B);
                            ShowHighlightBoard(highlight3B, GameHelper.GetColorByHtmlCode(StoryGameController.HIGHLIGHT_POINTER_BLUE));
                        }
                        else // CT0585
                        {
                            HideHighlightBoard(highlight3B);
                            ShowHighlightBoard(highlight4B, GameHelper.GetColorByHtmlCode(StoryGameController.HIGHLIGHT_POINTER_PURE_GREEN));
                        }
                    };
                    UnityAction onSelectToIdle = delegate ()
                    {
                        ShowHighlightBoard(highlight3B, GameHelper.GetColorByHtmlCode(StoryGameController.HIGHLIGHT_POINTER_BLUE));
                        ShowHighlightBoard(highlight4B, GameHelper.GetColorByHtmlCode(StoryGameController.HIGHLIGHT_POINTER_PURE_GREEN));
                    };
                    UnityAction onSelectToDrag = delegate () { };
                    UnityAction onDragToSelect = delegate () { };

                    int handCount = _manager.GetHandCount(PlayerIndex.One);

                    UnityAction onDragToIdle = delegate ()
                    {
                        if (handCount != _manager.GetHandCount(PlayerIndex.One))
                        {
                            if (lastCardSelected.BaseID == "CT0584")
                            {
                                HideHighlightBoard(highlight3B);
                            }
                            else
                            {
                                HideHighlightBoard(highlight4B);
                            }
                            handCount = _manager.GetHandCount(PlayerIndex.One);
                            if (handCount == 1)
                            {
                                HidePopupToolTip();
                                base.CreateDynamicTutorialPopup("TU2_DPUP_5", base.GetSlotScreenPosition(12, offset));
                            }
                        }
                        if (_manager.IsEmptyHand(_manager.CurrentPlayerIndex))
                        {
                            HideHighlightBoard(highlight3B);
                            HideHighlightBoard(highlight4B);
                            HighlightPlayerDock(UIPlayerDock.HighlightDock.AP, 9999, false);
                            GameHelper.DelayCallback(1f, delegate
                            {
                                UIManager.Instance.GetUITutorialManager().RequestShowPopup("T_ArrowEndTurn");
                                ControllerData.SetIsCanEndTurn(true);
                            });
                            _manager.StartCoroutine(PlayVoice(PlayerIndex.Two, "TUT_2_029", "TU2_VOICE_029", 0f));
                            ControllerData.ClearPlayPhaseEventCallback();
                        }
                        if (handCount == 2)
                        {
                            ShowHighlightBoard(highlight3B, GameHelper.GetColorByHtmlCode(StoryGameController.HIGHLIGHT_POINTER_BLUE));
                            ShowHighlightBoard(highlight4B, GameHelper.GetColorByHtmlCode(StoryGameController.HIGHLIGHT_POINTER_PURE_GREEN));
                        }
                    };

                    ControllerData.SetCallbackOnIdleToSelect(onIdleToSelect);
                    ControllerData.SetCallbackOnSelectToIdle(onSelectToIdle);
                    ControllerData.SetCallbackOnSelectToDrag(onSelectToDrag);
                    ControllerData.SetCallbackOnDragToSelect(onDragToSelect);
                    ControllerData.SetCallbackOnDragToIdle(onDragToIdle);

                    ControllerData.SetIsBotCanAction(false);
                    ControllerData.SetIsCanEndTurn(false);
                }
                break;
            case 10:
                {
                    List<int> highlightBoard = new List<int>() { 12 };
                    ShowHighlightBoard(highlightBoard, Karamucho.UI.BoardSlotHolder.SlotHilightType.Hilight);

                    UnityAction onIdleToSelect = delegate () { };
                    UnityAction onSelectToIdle = delegate () { };
                    UnityAction onSelectToDrag = delegate () { };
                    UnityAction onDragToSelect = delegate () { };

                    UnityAction onDragToIdle = delegate ()
                    {
                        if (_manager.IsEmptyHand(_manager.CurrentPlayerIndex))
                        {
                            HideHighlightBoard(highlightBoard);
                            HidePopupToolTip();

                            CreateDynamicTutorialPopup("TU2_DPUP_6", base.GetSlotScreenPosition(10));
                            ControllerData.ClearPlayPhaseEventCallback();
                            UIManager.Instance.GetUITutorialManager().RequestShowPopup("T_ArrowEndTurn");
                            ControllerData.SetIsCanEndTurn(true);
                            //_manager.StartCoroutine(GameHelper.ExecuteIEnumerator(WaitUntilPlaySoundFinish(), () => { ControllerData.SetIsCanEndTurn(true); }));
                        }
                    };

                    ControllerData.SetCallbackOnIdleToSelect(onIdleToSelect);
                    ControllerData.SetCallbackOnSelectToIdle(onSelectToIdle);
                    ControllerData.SetCallbackOnSelectToDrag(onSelectToDrag);
                    ControllerData.SetCallbackOnDragToSelect(onDragToSelect);
                    ControllerData.SetCallbackOnDragToIdle(onDragToIdle);

                    ControllerData.SetIsBotCanAction(false);
                    ControllerData.SetIsCanEndTurn(false);
                }
                break;
            case 12:
                {
                    List<int> highlightBoard = new List<int>() { 9 };
                    ShowHighlightBoard(highlightBoard, Karamucho.UI.BoardSlotHolder.SlotHilightType.Hilight);

                    UnityAction onDragToIdle = delegate ()
                    {
                        if (_manager.IsEmptyHand(_manager.CurrentPlayerIndex))
                        {
                            HideHighlightBoard(highlightBoard);
                            ControllerData.ClearPlayPhaseEventCallback();
                            _manager.StartCoroutine(GameHelper.ExecuteIEnumerator(WaitUntilPlaySoundFinish(), () =>
                            {
                                UIManager.Instance.GetUITutorialManager().RequestShowPopup("T_ArrowEndTurn");
                                ControllerData.SetIsCanEndTurn(true);
                            }));
                        }
                    };

                    ControllerData.SetCallbackOnDragToIdle(onDragToIdle);

                    ControllerData.SetIsBotCanAction(false);
                    ControllerData.SetIsCanEndTurn(false);
                }
                break;
            case 14:
                {
                    UnityAction onDragToIdle = delegate ()
                    {
                        if (_manager.IsEmptyHand(_manager.CurrentPlayerIndex))
                        {
                            UIManager.Instance.GetUITutorialManager().RequestShowPopup("T_ArrowEndTurn");
                            ControllerData.SetIsCanEndTurn(true);
                            ControllerData.ClearPlayPhaseEventCallback();
                        }
                    };
                    ControllerData.SetCallbackOnDragToIdle(onDragToIdle);

                    ControllerData.SetIsBotCanAction(false);
                    ControllerData.SetIsCanEndTurn(false);
                }
                break;
            default:
                {
                    if (_manager.SubTurn % 2 == 0)
                    {
                        UnityAction onDragToIdle = delegate ()
                        {
                            if (CanEndTurn(_manager.CurrentPlayerIndex))
                            {
                                GameHelper.DelayCallback(1f, delegate
                                {
                                    ControllerData.SetIsCanEndTurn(true);
                                });
                                ControllerData.ClearPlayPhaseEventCallback();
                            }
                        };
                        ControllerData.SetCallbackOnDragToIdle(onDragToIdle);

                        ControllerData.SetIsBotCanAction(false);
                        ControllerData.SetIsCanEndTurn(false);
                    }
                    else
                    {
                        ControllerData.SetIsBotCanAction(true);
                        ControllerData.SetIsCanEndTurn(true);
                    }
                }
                break;
        }
    }

    public override void OnPlayPhaseTutorial()
    {
        switch (_manager.SubTurn)
        {
            default:
                {
                }
                break;
        }
    }

    private void GameManager_OnUseCard(BattleCardData token)
    {
        if (_manager.SubTurn >= 8)
        {
            if (token.Owner == PlayerIndex.One)
            {
                base.ShowPlayerFloatingAp("-" + token.RawData.Spirit, 3.0f);
            }
            else
            {
                base.ShowEnemyFloatingAp("-" + token.RawData.Spirit, 3.0f);
            }
            Debug.Log(token.Owner + " " + token.BaseID + " " + token.RawData.Spirit);
        }
    }

    public override void OnEndPlayPhaseTutorial(UnityAction onComplete)
    {
        _manager.StartCoroutine(IEOnEndPlayPhaseTutorial(onComplete));
    }

    // before battle.
    private IEnumerator IEOnEndPlayPhaseTutorial(UnityAction onComplete)
    {
        UIManager.Instance.GetUITutorialManager().RequestHidePopup("T_ArrowEndTurn");
        switch (_manager.SubTurn)
        {
            case 2:
                {
                    HidePopupToolTip();
                    yield return PlayVoice(PlayerIndex.One, "TUT_2_007", "TU2_VOICE_007", 0f);
                    ShowHighlightArrowToken(6, S_Dir, false);
                    yield return null;
                    base.HideRemind();
                }
                break;
            case 3:
                {
                    //yield return PlayVoice(PlayerIndex.One, "TUT_2_010", "TU2_VOICE_010", 0f);
                    //yield return PlayVoice(PlayerIndex.One, "TUT_2_011", "TU2_VOICE_011", 0f);
                    //yield return PlayVoice(PlayerIndex.One, "TUT_2_012", "TU2_VOICE_012", 0f);
                    List<int> slots = new List<int>() { 6 };
                    ShowHighlightBoard(slots, Karamucho.UI.BoardSlotHolder.SlotHilightType.Normal);
                    ShowPopupToolTip("TU2_TOOLTIP_003", base.GetSlotScreenPosition(10));
                    yield return new WaitForSeconds(3f);
                    HidePopupToolTip();
                    HideHighlightBoard(slots);
                    base.CreateDynamicTutorialPopup("TU2_DPUP_3", base.GetSlotScreenPosition(13));
                }
                break;
            case 4:
                {
                    Debug.Log("Show ดาบขาวดำที่มีเครื่องหมายห้าม ระหว่าง crazy bear กับ Mysticlancer");
                    UIManager.Instance.GetUITutorialManager().RequestShowPopup("T2_SwordDisable_1");
                    Vector2 offset = new Vector2(-1 * UIManager.SLOT_WIDTH * 0.5f, 0);
                    ShowPopupToolTip("TU2_TOOLTIP_004", base.GetSlotScreenPosition(10, offset));
                    List<int> cantAtkSlot = new List<int>() { 5, 6 };
                    ShowHighlightBoard(cantAtkSlot, Karamucho.UI.BoardSlotHolder.SlotHilightType.Normal);
                    yield return new WaitForSeconds(2f);
                    UIManager.Instance.GetUITutorialManager().RequestHidePopup("T2_SwordDisable_1");

                    HideHighlightBoard(cantAtkSlot);
                    Debug.Log("Hide ดาบไขว่ขาวดำที่ห้าม");
                    HidePopupToolTip();
                    yield return new WaitForSeconds(1f);

                    base.ShowHighlightArrowToken(1, S_Dir, true);
                    base.ShowHighlightArrowToken(5, N_Dir, true);
                    UIManager.Instance.GetUITutorialManager().RequestShowPopup("T2_SwordEnable_1");
                    Debug.Log("Show ดาบแดงที่มีให้ตี ระหว่าง crazy bear กับ Mysticlancer");
                    ShowPopupToolTip("TU2_TOOLTIP_005", base.GetSlotScreenPosition(9));

                    List<int> attackableSlot = new List<int>() { 1, 5 };
                    ShowHighlightBoard(attackableSlot, Karamucho.UI.BoardSlotHolder.SlotHilightType.Normal);
                    yield return new WaitForSeconds(2f);
                    UIManager.Instance.GetUITutorialManager().RequestHidePopup("T2_SwordEnable_1");

                    base.ShowHighlightArrowToken(1, S_Dir, false);
                    base.ShowHighlightArrowToken(5, N_Dir, false);
                    HideHighlightBoard(attackableSlot);
                    HidePopupToolTip();
                    base.CreateDynamicTutorialPopup("TU2_DPUP_4", base.GetSlotScreenPosition(13));
                }
                break;
            case 5:
                {
                    UIManager.Instance.GetUITutorialManager().RequestShowPopup("T2_SwordDisable_2");
                    UIManager.Instance.GetUITutorialManager().RequestShowPopup("T2_SwordDisable_4");

                    List<int> unAttackableSlot = new List<int>() { 1, 5, 6 };
                    ShowHighlightBoard(unAttackableSlot, Karamucho.UI.BoardSlotHolder.SlotHilightType.Normal);

                    yield return new WaitForSeconds(2f);
                    UIManager.Instance.GetUITutorialManager().RequestHidePopup("T2_SwordDisable_2");
                    UIManager.Instance.GetUITutorialManager().RequestHidePopup("T2_SwordDisable_4");
                    HideHighlightBoard(unAttackableSlot);

                    List<int> attackableSlot = new List<int>() { 1, 2, 6 };
                    UIManager.Instance.GetUITutorialManager().RequestShowPopup("T2_SwordEnable_2");
                    UIManager.Instance.GetUITutorialManager().RequestShowPopup("T2_SwordEnable_3");
                    ShowHighlightBoard(attackableSlot, Karamucho.UI.BoardSlotHolder.SlotHilightType.Normal);
                    base.ShowHighlightArrowToken(1, E_Dir, true);

                    CardDirection WplusSDir = new CardDirection();
                    WplusSDir.Add(CardDirectionType.W);
                    WplusSDir.Add(CardDirectionType.S);

                    base.ShowHighlightArrowToken(2, WplusSDir, true);

                    yield return new WaitForSeconds(2);
                    HideHighlightBoard(attackableSlot);
                    UIManager.Instance.GetUITutorialManager().RequestHidePopup("T2_SwordEnable_2");
                    UIManager.Instance.GetUITutorialManager().RequestHidePopup("T2_SwordEnable_3");

                    base.ShowHighlightArrowToken(1, E_Dir, false);
                    base.ShowHighlightArrowToken(2, WplusSDir, false);

                }
                break;
            case 6:
                {
                    UIManager.Instance.GetUITutorialManager().RequestShowPopup("T2_SwordDisable_3");

                    List<int> unAttackableSlot = new List<int>() { 1, 5 };
                    ShowHighlightBoard(unAttackableSlot, Karamucho.UI.BoardSlotHolder.SlotHilightType.Normal);

                    yield return new WaitForSeconds(2);
                    UIManager.Instance.GetUITutorialManager().RequestHidePopup("T2_SwordDisable_3");
                    HideHighlightBoard(unAttackableSlot);

                    List<int> attackableSlot = new List<int>() { 5, 6 };
                    UIManager.Instance.GetUITutorialManager().RequestShowPopup("T2_SwordEnable_4");
                    ShowHighlightBoard(attackableSlot, Karamucho.UI.BoardSlotHolder.SlotHilightType.Normal);

                    base.ShowHighlightArrowToken(5, E_Dir, true);
                    base.ShowHighlightArrowToken(6, W_Dir, true);

                    yield return new WaitForSeconds(2);
                    HideHighlightBoard(attackableSlot);
                    UIManager.Instance.GetUITutorialManager().RequestHidePopup("T2_SwordEnable_4");

                    base.ShowHighlightArrowToken(5, E_Dir, false);
                    base.ShowHighlightArrowToken(6, W_Dir, false);

                }
                break;
            case 8:
                break;
            case 13:
                {
                    yield return WaitUntilPlaySoundFinish();
                    List<int> highlight = new List<int>() { 9 };
                    ShowHighlightBoard(highlight, Karamucho.UI.BoardSlotHolder.SlotHilightType.Target);
                    Vector2 offset = new Vector2(0, -1 * UIManager.SLOT_WIDTH / 4);
                    ShowPopupToolTip("TU2_TOOLTIP_006", base.GetSlotScreenPosition(1, offset));
                    yield return new WaitForSeconds(PLAY_HIGHLIGHT_SLOT_TIME);
                    HidePopupToolTip();
                    HideHighlightBoard(highlight);
                    base.CreateDynamicTutorialPopup("TU2_DPUP_7", base.GetSlotScreenPosition(5));

                    //yield return PlayVoice(PlayerIndex.Two, "TUT_2_047", "TU2_VOICE_047", 0f);
                    //yield return PlayVoice(PlayerIndex.One, "TUT_2_049", "TU2_VOICE_049", 0.2f);
                    //yield return PlayVoice(PlayerIndex.One, "TUT_2_050", "TU2_VOICE_050", 0f);
                }
                break;
        }
        onComplete?.Invoke();
        yield break;
    }
    #endregion

    #region Battle Phase

    public override IEnumerator OnEndPreBattlePhaseTutorial(UnityAction onComplete)
    {
        switch (_manager.SubTurn)
        {
        }
        onComplete?.Invoke();
        yield break;
    }

    public override IEnumerator OnPostBattlePhaseTutorial(UnityAction onComplete)
    {
        switch (_manager.SubTurn)
        {
            case 5:
                {
                    //yield return PlayVoice(PlayerIndex.Two, "TUT_2_018", "TU2_VOICE_018", 1f);
                }
                break;
            case 6:
                {
                    //yield return PlayVoice(PlayerIndex.Two, "TUT_2_022", "TU2_VOICE_022", 0.2f);
                    PlayVoiceWithOutCallback(PlayerIndex.One, "TUT_2_023", "TU2_VOICE_023");
                    //yield return PlayVoice(PlayerIndex.One, "TUT_2_024", "TU2_VOICE_024", 0.2f);
                }
                break;
            case 7:
                {
                    yield return PlayVoice(PlayerIndex.One, "TUT_2_027", "TU2_VOICE_027", 0.2f);
                    PlayVoiceWithOutCallback(PlayerIndex.Two, "TUT_2_026", "TU2_VOICE_026");
                }
                break;
            case 11:
                {
                    yield return PlayVoice(PlayerIndex.Two, "TUT_2_037", "TU2_VOICE_037", 0.2f);
                }
                break;
            default:
                break;
        }
        onComplete?.Invoke();
        yield break;
    }
    #endregion

    #region End Game
    protected override IEnumerator OnPlayEndGameSound(bool isVictory)
    {
        if (isVictory)
        {
            yield return PlayVoice(PlayerIndex.One, "TUT_2_056", "TU2_VOICE_056", 0f);
        }
    }

    public override void OnCheckClaimReward(UnityAction onComplete)
    {
        DataManager.Instance.ClaimTutorialRewardByTutorialIndex(1);
        onComplete?.Invoke();
        //PopupUIManager.Instance.Show_MidSoftLoad(true);
        //DataManager.Instance.RequestClaimFinishTutorial2Reward(
        //   delegate (List<ItemData> itemList)
        //   {
        //       PopupUIManager.Instance.Show_MidSoftLoad(false);
        //       string title = "TITLE_GOT_TUTORIAL2_REWARD";
        //       string message = "MESSAGE_GOT_TUTORIAL2_REWARD";
        //       PopupUIManager.Instance.ShowPopup_GotLegendPack(title, message, () =>
        //       {
        //           onComplete?.Invoke();
        //       });
        //   },
        //   delegate (string errorMessage)
        //    {
        //        PopupUIManager.Instance.Show_MidSoftLoad(false);
        //        Debug.Log(errorMessage);
        //        onComplete?.Invoke();
        //    }
        //);
    }

    #endregion

    #endregion
}
