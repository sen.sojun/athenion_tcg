﻿using System;
using System.Collections;
using System.Collections.Generic;
using Karamucho.UI;
using UnityEngine;
using UnityEngine.Events;

public class Tutorial1Controller : StoryGameController
{
    List<string> _soundKey = new List<string>();

    public Tutorial1Controller(GameManager manager) : base(manager)
    {
    }

    public override int GetLifePoint(PlayerIndex playerIndex, GameMode mode)
    {
        if (playerIndex == PlayerIndex.Two)
            return 7;
        else
            return 15;
    }

    public override string GetBoardID()
    {
        return "TUT_1";
    }

    public override bool IsCanShowSpiritEffect()
    {
        switch (_manager.SubTurn)
        {
            case 5:
            case 8:
                return false;
            default:
                return true;
        }
    }

    public override bool CanSetPower()
    {
        switch (_manager.SubTurn)
        {
            case 5:
            case 8:
                return false;
            default:
                return true;
        }
    }

    private bool CanEndTurnTutorial1 = true;
    public override bool CanEndTurn(PlayerIndex playerIndex)
    {
        return base.CanEndTurn(playerIndex) && CanEndTurnTutorial1;
    }
    /*
        [00][01][02][03]
        [04][05][06][07]
        [08][09][10][11]
        [12][13][14][15]
        */
    public override bool IsTutorialValidSlotID(int slotID)
    {
        if (GameManager.Instance.Phase == GamePhase.Play)
        {
            switch (GameManager.Instance.SubTurn)
            {
                case 2:
                    return slotID == GameHelper.SlotCodeToSlotID("2C");
                case 4:
                    return slotID == GameHelper.SlotCodeToSlotID("1C");
                case 6:
                    return slotID == GameHelper.SlotCodeToSlotID("3D");
                case 8:
                    return slotID == GameHelper.SlotCodeToSlotID("2D");
                //case 10:
                //    return slotID == GameHelper.SlotCodeToSlotID("3C");

                default: return true;
            }
        }

        return true;
    }

    #region Phase

    #region SetupPhase

    public override IEnumerator SetupPhase()
    {
        _manager.SetPlayOrder(PlayerIndex.Two, null);
        _manager._isPlayFirstReady = true;

        while (!_manager.IsCanNextPhase || !_manager.IsPhaseReady(GamePhase.SetupData) || !_manager._isPlayFirstReady)
        {
            yield return null;
        }

        _manager.SetRandomSeed(_manager.MatchData.PlayerData[1].RandomSeed);
        _manager.SetLoadingProgress(1.0f);
        NextPhase();

        UIManager.Instance.FadeBlackIn();
        UIManager.Instance.ShowDeck(false, PlayerIndex.Two);
        UIManager.Instance.ShowDeck(false, PlayerIndex.One);
    }

    #endregion

    #region Intro Phase

    protected override IEnumerator MiddleIntroPhase()
    {
        if (DataManager.Instance.IsCompletedTutorial(GameMode.Tutorial_1) == false)
        {
            bool isFinish = false;
            UIManager.Instance.ShowTutorialMap(() => isFinish = true);
            yield return new WaitUntil(() => isFinish == true);
        }
        yield return new WaitForSeconds(2f);
        Debug.Log("PlayBlack screen and fade 2 - 3 sec");

        Debug.Log("See Mary Token in player's dock. 1 sec");
        UIManager.Instance.SpotLight_ShowHeroBot(true);
        yield return new WaitForSeconds(1f);

        Debug.Log("fade black 1.5sec and playsound Grunt.");
        //start coroutine play grunt's sound.    
        yield return PlayVoice(PlayerIndex.Two, "TUT_1_000", "TU1_VOICE_000", 0.2f);
        //yield return new WaitForSeconds(1.5f);

        Debug.Log("grunt smash to his token and remove fade black");
        UIManager.Instance.UIPlayerTop.Intro_State_ShowAtCenter(null);
        UIManager.Instance.SpotLight_ShowMid(true);
        yield return new WaitForSeconds(0.75f);

        UIManager.Instance.SpotLight_ShowMid(false);
        UIManager.Instance.SpotLight_ShowHeroBot(false);
        UIManager.Instance.FadeBlackOut();

        {
            bool isFinish = false;
            UIManager.Instance.UIPlayerTop.Intro_State_Default(() => isFinish = true);
            yield return new WaitUntil(() => isFinish == true);
        }
        yield return PlayVoice(PlayerIndex.One, "TUT_1_001", "TU1_VOICE_001", 0.2f);
        yield return PlayVoice(PlayerIndex.One, "TUT_1_002", "TU1_VOICE_002", 0.2f);

        yield return PlayVoice(PlayerIndex.Two, "TUT_1_003", "TU1_VOICE_003", 0.2f);
    }

    protected override IEnumerator EndIntroPhase()
    {
        yield return PlayVoice(PlayerIndex.One, "TUT_1_004", "TU1_VOICE_004", 0f);
        {
            bool isFinish = false;
            UIManager.Instance.Tutorial_PlayIntro(() => isFinish = true);
            yield return new WaitUntil(() => isFinish == true);
        }
        {
            bool isFinish = false;
            UIManager.Instance.Tutorial_PlayCardToDeck(() => isFinish = true);
            yield return new WaitUntil(() => isFinish == true);
        }
        {
            bool isFinish = false;
            CreateDynamicTutorialPopup("TU1_DPUP_1", GameHelper.CenterScreen, true, () => isFinish = true);
            yield return new WaitUntil(() => isFinish == true);
        }
        yield break;
    }
    #endregion

    #region Begin Phase

    public override IEnumerator IEOnEndBeginPhase()
    {
        switch (_manager.SubTurn)
        {
            case 1:
                yield return PlayVoice(PlayerIndex.Two, "TUT_1_005", "TU1_VOICE_005", 0f);
                break;
            case 2:
                yield return PlayVoice(PlayerIndex.One, "TUT_1_006", "TU1_VOICE_006", 0f);
                break;
            case 4:
                break;
            case 6:
                PlayVoiceWithOutCallback(PlayerIndex.One, "TUT_1_016", "TU1_VOICE_016");
                break;
            case 7:
                {
                    yield return PlayVoice(PlayerIndex.Two, "TUT_1_019", "TU1_VOICE_019", 0f);
                    HighlightEnemyDock(UIPlayerDock.HighlightDock.SOUL, PLAY_PLAYER_DOCK_TIME);
                    Vector2 offset = new Vector2(-1 * UIManager.SLOT_WIDTH / 4, 0);
                    ShowPopupToolTip("TU1_TOOLTIP_001", base.GetSlotScreenPosition(1, offset));
                    yield return new WaitForSeconds(PLAY_PLAYER_DOCK_TIME);
                    HidePopupToolTip();
                }
                break;
            case 8:
                //yield return PlayVoice(PlayerIndex.One, "TUT_1_023", "TU1_VOICE_023", 0f);
                break;
            case 10:
                {
                    HighlightPlayerDock(UIPlayerDock.HighlightDock.SOUL, PLAY_PLAYER_DOCK_TIME);
                    Vector2 offset = new Vector2(-1 * UIManager.SLOT_WIDTH / 4, 0);
                    ShowPopupToolTip("TU1_TOOLTIP_001", base.GetSlotScreenPosition(13, offset));
                    yield return new WaitForSeconds(PLAY_PLAYER_DOCK_TIME);
                    HidePopupToolTip();
                }
                break;
            case 12:
                HighlightPlayerDock(UIPlayerDock.HighlightDock.SOUL, PLAY_PLAYER_DOCK_TIME);
                yield return new WaitForSeconds(PLAY_PLAYER_DOCK_TIME);
                yield return PlayVoice(PlayerIndex.One, "TUT_1_031", "TU1_VOICE_031", 0f);
                break;
            case 13:
                PlayVoiceWithOutCallback(PlayerIndex.Two, "TUT_1_035", "TU1_VOICE_035");
                break;
            default:
                if (_manager.SubTurn % 2 == 0)
                {
                    int enemyHP = _manager.Data.PlayerDataList[(int)PlayerIndex.Two].CurrentHP;
                    if (enemyHP - _manager.GetSumSpirit(PlayerIndex.One) <= 0)
                    {
                        yield return PlayVoice(PlayerIndex.Two, "TUT_1_036", "TU1_VOICE_036", 0f);
                        yield return PlayVoice(PlayerIndex.One, "TUT_1_037", "TU1_VOICE_037", 0f);
                    }
                }
                break;
        }
    }

    #endregion

    #region Spirit Phase

    public override void SpiritPhaseTutorial()
    {
        int sum = _manager.GetSumSpirit(_manager.CurrentPlayerIndex);
        foreach (PlayerIndex playerIndex in _manager.PlayerIndexList)
        {
            if (playerIndex != _manager.CurrentPlayerIndex)
            {
                if (_manager.SubTurn == 5 || _manager.SubTurn == 7 || _manager.SubTurn == 10)
                {
                    _manager.RequestSpiritDamageTutorial(playerIndex, sum);
                }
                else
                {
                    _manager.RequestSpiritDamage(playerIndex, sum);
                }
            }
        }
    }

    public override IEnumerator OnEndSpiritPhaseTutorial(UnityAction onComplete)
    {
        switch (_manager.SubTurn)
        {
            case 7:
                yield return PlayVoice(PlayerIndex.One, "TUT_1_020", "TU1_VOICE_020", 0f);
                HighlightPlayerDock(UIPlayerDock.HighlightDock.HP, PLAY_PLAYER_DOCK_TIME);
                ShowPopupToolTip("TU1_TOOLTIP_002", base.GetSlotScreenPosition(13));
                yield return new WaitForSeconds(PLAY_PLAYER_DOCK_TIME);
                HidePopupToolTip();
                CreateDynamicTutorialPopup("TU1_DPUP_4", false);
                break;
            case 10:
                HighlightEnemyDock(UIPlayerDock.HighlightDock.HP, PLAY_PLAYER_DOCK_TIME);
                ShowPopupToolTip("TU1_TOOLTIP_003", base.GetSlotScreenPosition(1));
                yield return new WaitForSeconds(PLAY_PLAYER_DOCK_TIME);
                HidePopupToolTip();
                break;
            case 12:
                _manager.StartCoroutine(PlayVoice(PlayerIndex.Two, "TUT_1_032", "TU1_VOICE_032", 0f));
                break;
        }
        onComplete?.Invoke();
        yield break;
    }

    #endregion

    #region Draw Phase
    public override void OnSetDrawCount(int subturn, ref int drawCount, ref bool isDrawAP)
    {
        isDrawAP = false;
        switch (_manager.SubTurn)
        {
            case 1: drawCount = 1; break;
            case 2: drawCount = 1; break;
            case 3: drawCount = 1; break;
            case 4: drawCount = 1; break;
            case 5: drawCount = 1; break;
            case 6: drawCount = 1; break;
            case 7: drawCount = 1; break;
            case 8: drawCount = 1; break;
            case 9: drawCount = 1; break;
            case 10: drawCount = 1; break;
            case 11: drawCount = 2; break;
            case 12: drawCount = 1; break;
            case 13: drawCount = 1; break;

            default:
                {
                    int toDrawCount = /*_manager.MaxHandCard*/1;
                    int handCount = _manager.Data.PlayerDataList[(int)_manager.CurrentPlayerIndex].CardList[CardZone.Hand].Count;
                    int deckCount = _manager.Data.PlayerDataList[(int)_manager.CurrentPlayerIndex].CardList[CardZone.Deck].Count;
                    int handSlot = toDrawCount - handCount;

                    drawCount = Mathf.Min(handSlot, deckCount);
                }
                break;
        }
    }
    #endregion

    #region PrePlay Phase

    public override IEnumerator OnEndPrePlayPhaseTutorial(UnityAction onComplete)
    {
        switch (_manager.SubTurn)
        {
            case 2:
                {
                    List<TutorialCardPopupHighlight.HighlightType> highlight = new List<TutorialCardPopupHighlight.HighlightType>() {
                         TutorialCardPopupHighlight.HighlightType.Arrow
                    };
                    yield return ShowCardDetailFromHandAutoHide(0, PLAY_CARD_DETAIL_TIME, highlight);
                }
                break;
            case 4:
                {
                    //List<TutorialCardPopupHighlight.HighlightType> highlight = new List<TutorialCardPopupHighlight.HighlightType>() {
                    //     TutorialCardPopupHighlight.HighlightType.ATK,
                    //      TutorialCardPopupHighlight.HighlightType.HP
                    //};
                    //yield return ShowCardDetailFromHandAutoHide(0, 4.5f, highlight);
                }
                break;
            case 7:
                //yield return PlayVoice(PlayerIndex.Two, "TUT_1_021", "TU1_VOICE_021", 0f);
                break;
            case 8:
                _manager.StartCoroutine(PlayVoice(PlayerIndex.One, "TUT_1_024", "TU1_VOICE_024", 0f));
                break;
            case 9:
                //yield return PlayVoice(PlayerIndex.Two, "TUT_1_026", "TU1_VOICE_026", 0.2f);
                break;
            case 10:
                {
                    //List<TutorialCardPopupHighlight.HighlightType> highlight = new List<TutorialCardPopupHighlight.HighlightType>() {
                    //     TutorialCardPopupHighlight.HighlightType.Armor
                    //};
                    //yield return ShowCardDetailFromHandAutoHide(0, 4.5f, highlight);

                    //_manager.StartCoroutine(PlayVoice(PlayerIndex.One, "TUT_1_027", "TU1_VOICE_027", 0.2f));
                }
                break;
            case 11:
                //yield return PlayVoice(PlayerIndex.Two, "TUT_1_028", "TU1_VOICE_028", 0f);
                break;
            case 12:
                //AddVoiceToList(PlayerIndex.One, "TUT_1_033", "TU1_VOICE_033", 0f);
                AddVoiceToList(PlayerIndex.One, "TUT_1_034", "TU1_VOICE_034", 0f);
                _manager.StartCoroutine(PlayVoiceList());
                break;
            case 16:
                _manager.StartCoroutine(PlayVoice(PlayerIndex.One, "TUT_1_040", "TU1_VOICE_040", 0f));
                break;
        }
        onComplete?.Invoke();
    }

    #endregion

    #region Play Phase

    List<CardUI_DetailHover.HighlightList> none = new List<CardUI_DetailHover.HighlightList>();
    public override List<CardUI_DetailHover.HighlightList> OnTouchUIDetailCard()
    {
        switch (_manager.SubTurn)
        {
            case 2:
                return new List<CardUI_DetailHover.HighlightList>() { CardUI_DetailHover.HighlightList.Arrow };
            case 4:
                return new List<CardUI_DetailHover.HighlightList>() { CardUI_DetailHover.HighlightList.ATK, CardUI_DetailHover.HighlightList.HP };
            case 10:
                return new List<CardUI_DetailHover.HighlightList>() { CardUI_DetailHover.HighlightList.Armor };
            default:
                return none;
        }
    }

    public override void OnBeginPlayPhaseTutorial()
    {
        switch (_manager.SubTurn)
        {
            case 2:
                {
                    List<TutorialCardPopupHighlight.HighlightType> highlight = new List<TutorialCardPopupHighlight.HighlightType>() {
                         TutorialCardPopupHighlight.HighlightType.Arrow
                    };
                    HighlightPlayerHand(0, highlight, true);
                    base.PointerPlayerCard(0, true, GameHelper.GetColorByHtmlCode(StoryGameController.HIGHLIGHT_POINTER_GREEN));
                    SetActiveHandGuide(true, GetSlotPosition(GameHelper.SlotCodeToSlotID("2C")));
                    List<int> highlightBoard = new List<int>() { 6 };
                    ShowHighlightBoard(highlightBoard, Karamucho.UI.BoardSlotHolder.SlotHilightType.Hilight);

                    UnityAction onIdleToSelect = delegate ()
                    {
                        SetActiveHandGuide(false, GetSlotPosition(GameHelper.SlotCodeToSlotID("2C")));
                        base.PointerPlayerCard(0, false, GameHelper.GetColorByHtmlCode(StoryGameController.HIGHLIGHT_POINTER_GREEN));
                        ShowHighlightBoard(highlightBoard, Karamucho.UI.BoardSlotHolder.SlotHilightType.Hilight);
                    };

                    UnityAction onSelectToIdle = delegate ()
                    {
                        SetActiveHandGuide(true, GetSlotPosition(GameHelper.SlotCodeToSlotID("2C")));
                        base.PointerPlayerCard(0, true, GameHelper.GetColorByHtmlCode(StoryGameController.HIGHLIGHT_POINTER_GREEN));
                    };

                    UnityAction onSelectToDrag = delegate () { };
                    UnityAction onDragToSelect = delegate () { };
                    UnityAction onDragToIdle = delegate ()
                    {
                        //
                        if (UIManager.Instance)
                            if (_manager.IsEmptyHand(_manager.CurrentPlayerIndex))
                            {
                                SetActiveHandGuide(false, GetSlotPosition(GameHelper.SlotCodeToSlotID("2C")));
                                HideHighlightBoard(highlightBoard);
                                CreateDynamicTutorialPopup("TU1_DPUP_2", false);
                                base.ShowRemind();
                                UIManager.Instance.GetUITutorialManager().RequestShowPopup("T_ArrowEndTurn");
                                ControllerData.SetIsCanEndTurn(true);
                                ControllerData.ClearPlayPhaseEventCallback();
                            }
                            else
                            {
                                SetActiveHandGuide(true, GetSlotPosition(GameHelper.SlotCodeToSlotID("2C")));
                                ShowHighlightBoard(highlightBoard, Karamucho.UI.BoardSlotHolder.SlotHilightType.Hilight);
                                base.PointerPlayerCard(0, true, GameHelper.GetColorByHtmlCode(StoryGameController.HIGHLIGHT_POINTER_GREEN));
                            }
                    };

                    ControllerData.SetCallbackOnIdleToSelect(onIdleToSelect);
                    ControllerData.SetCallbackOnSelectToIdle(onSelectToIdle);
                    ControllerData.SetCallbackOnSelectToDrag(onSelectToDrag);
                    ControllerData.SetCallbackOnDragToSelect(onDragToSelect);
                    ControllerData.SetCallbackOnDragToIdle(onDragToIdle);

                    ControllerData.SetIsBotCanAction(false);
                    ControllerData.SetIsCanEndTurn(false);
                }
                break;

            case 4:
                {
                    List<int> highlightBoard = new List<int>() { 2 };
                    base.PointerPlayerCard(0, true, GameHelper.GetColorByHtmlCode(StoryGameController.HIGHLIGHT_POINTER_GREEN));
                    ShowHighlightBoard(highlightBoard, Karamucho.UI.BoardSlotHolder.SlotHilightType.Hilight);
                    UIManager.Instance.GetUITutorialManager().RequestShowPopup("T1_EnemyAtkAndHp");

                    List<TutorialCardPopupHighlight.HighlightType> highlightList = new List<TutorialCardPopupHighlight.HighlightType>()
                    {
                        TutorialCardPopupHighlight.HighlightType.ATK,
                        TutorialCardPopupHighlight.HighlightType.HP
                    };
                    HighlightPlayerHand(0, highlightList, true);

                    UnityAction onIdleToSelect = delegate ()
                    {
                        UIManager.Instance.GetUITutorialManager().RequestHidePopup("T1_EnemyAtkAndHp");
                        base.PointerPlayerCard(0, false, GameHelper.GetColorByHtmlCode(StoryGameController.HIGHLIGHT_POINTER_GREEN));
                        ShowHighlightBoard(highlightBoard, Karamucho.UI.BoardSlotHolder.SlotHilightType.Hilight);
                    };

                    UnityAction onSelectToIdle = delegate ()
                    {
                        UIManager.Instance.GetUITutorialManager().RequestShowPopup("T1_EnemyAtkAndHp");
                        base.PointerPlayerCard(0, true, GameHelper.GetColorByHtmlCode(StoryGameController.HIGHLIGHT_POINTER_GREEN));
                    };

                    UnityAction onSelectToDrag = delegate () { };

                    UnityAction onDragToSelect = delegate () { };
                    UnityAction onDragToIdle = delegate ()
                    {
                        if (_manager.IsEmptyHand(_manager.CurrentPlayerIndex))
                        {
                            HideHighlightBoard(highlightBoard);
                            UIManager.Instance.GetUITutorialManager().RequestShowPopup("T_ArrowEndTurn");
                            ControllerData.SetIsCanEndTurn(true);
                            ControllerData.ClearPlayPhaseEventCallback();
                        }
                        else
                        {
                            UIManager.Instance.GetUITutorialManager().RequestShowPopup("T1_EnemyAtkAndHp");
                            ShowHighlightBoard(highlightBoard, Karamucho.UI.BoardSlotHolder.SlotHilightType.Hilight);
                            base.PointerPlayerCard(0, true, GameHelper.GetColorByHtmlCode(StoryGameController.HIGHLIGHT_POINTER_GREEN));
                        }
                    };

                    ControllerData.SetCallbackOnIdleToSelect(onIdleToSelect);
                    ControllerData.SetCallbackOnSelectToIdle(onSelectToIdle);
                    ControllerData.SetCallbackOnSelectToDrag(onSelectToDrag);
                    ControllerData.SetCallbackOnDragToSelect(onDragToSelect);
                    ControllerData.SetCallbackOnDragToIdle(onDragToIdle);

                    ControllerData.SetIsBotCanAction(false);
                    ControllerData.SetIsCanEndTurn(false);
                }
                break;
            case 6:
                {
                    List<int> highlightBoard = new List<int>() { 11 };
                    ShowHighlightBoard(highlightBoard, Karamucho.UI.BoardSlotHolder.SlotHilightType.Hilight);

                    UnityAction onIdleToSelect = delegate () { };
                    UnityAction onSelectToIdle = delegate () { };
                    UnityAction onSelectToDrag = delegate () { };
                    UnityAction onDragToSelect = delegate () { };

                    UnityAction onDragToIdle = delegate ()
                    {
                        if (_manager.IsEmptyHand(_manager.CurrentPlayerIndex))
                        {
                            HideHighlightBoard(highlightBoard);
                            UIManager.Instance.GetUITutorialManager().RequestShowPopup("T_ArrowEndTurn");
                            ControllerData.SetIsCanEndTurn(true);
                            ControllerData.ClearPlayPhaseEventCallback();
                        }
                    };

                    ControllerData.SetCallbackOnIdleToSelect(onIdleToSelect);
                    ControllerData.SetCallbackOnSelectToIdle(onSelectToIdle);
                    ControllerData.SetCallbackOnSelectToDrag(onSelectToDrag);
                    ControllerData.SetCallbackOnDragToSelect(onDragToSelect);
                    ControllerData.SetCallbackOnDragToIdle(onDragToIdle);

                    ControllerData.SetIsBotCanAction(false);
                    ControllerData.SetIsCanEndTurn(false);
                }
                break;
            case 8:
                {
                    List<int> highlightBoard = new List<int>() { 7 };
                    ShowHighlightBoard(highlightBoard, Karamucho.UI.BoardSlotHolder.SlotHilightType.Hilight);

                    CanEndTurnTutorial1 = false;
                    UnityAction onIdleToSelect = delegate () { };
                    UnityAction onSelectToIdle = delegate () { };
                    UnityAction onSelectToDrag = delegate () { };
                    UnityAction onDragToSelect = delegate () { };

                    UnityAction onDragToIdle = delegate ()
                    {
                        if (_manager.IsEmptyHand(_manager.CurrentPlayerIndex))
                        {
                            HideHighlightBoard(highlightBoard);
                            _manager.StartCoroutine(
                              TutorialSoulSubturn8(() =>
                              {
                                  CanEndTurnTutorial1 = true;
                                  UIManager.Instance.GetUITutorialManager().RequestShowPopup("T_ArrowEndTurn");
                                  ControllerData.SetIsCanEndTurn(true);
                              })
                          );
                            ControllerData.ClearPlayPhaseEventCallback();
                        }
                    };

                    ControllerData.SetCallbackOnIdleToSelect(onIdleToSelect);
                    ControllerData.SetCallbackOnSelectToIdle(onSelectToIdle);
                    ControllerData.SetCallbackOnSelectToDrag(onSelectToDrag);
                    ControllerData.SetCallbackOnDragToSelect(onDragToSelect);
                    ControllerData.SetCallbackOnDragToIdle(onDragToIdle);

                    ControllerData.SetIsBotCanAction(false);
                    ControllerData.SetIsCanEndTurn(false);
                }
                break;
            case 10:
                {
                    UnityAction onIdleToSelect = delegate () { };
                    UnityAction onSelectToIdle = delegate () { };
                    UnityAction onSelectToDrag = delegate () { };
                    UnityAction onDragToSelect = delegate () { };

                    UnityAction onDragToIdle = delegate ()
                    {
                        if (_manager.IsEmptyHand(_manager.CurrentPlayerIndex))
                        {

                            ControllerData.ClearPlayPhaseEventCallback();
                            _manager.StartCoroutine(GameHelper.ExecuteIEnumerator(WaitUntilPlaySoundFinish(), () =>
                            {
                                // find
                                BattleCardData battleCardData;
                                if (_manager.FindBattleCard("CT0623", CardZone.Battlefield, PlayerIndex.One, out battleCardData))
                                {
                                    int slotID = battleCardData.SlotID;
                                    List<int> slotList = new List<int>() { slotID };
                                    ShowHighlightArmorToken(slotList, true);
                                }
                                UIManager.Instance.GetUITutorialManager().RequestShowPopup("T_ArrowEndTurn");
                                ControllerData.SetIsCanEndTurn(true);
                            }));
                        }
                    };

                    ControllerData.SetCallbackOnIdleToSelect(onIdleToSelect);
                    ControllerData.SetCallbackOnSelectToIdle(onSelectToIdle);
                    ControllerData.SetCallbackOnSelectToDrag(onSelectToDrag);
                    ControllerData.SetCallbackOnDragToSelect(onDragToSelect);
                    ControllerData.SetCallbackOnDragToIdle(onDragToIdle);

                    ControllerData.SetIsBotCanAction(false);
                    ControllerData.SetIsCanEndTurn(false);
                }
                break;
            default:
                {
                    if (_manager.SubTurn % 2 == 0)
                    {
                        UnityAction onDragToIdle = delegate ()
                        {
                            if (CanEndTurn(_manager.CurrentPlayerIndex))
                            {
                                _manager.StartCoroutine(
                                    GameHelper.ExecuteIEnumerator(WaitUntilPlaySoundFinish()
                                    , () => { ControllerData.SetIsCanEndTurn(true); })
                                );
                                ControllerData.ClearPlayPhaseEventCallback();
                            }
                        };
                        ControllerData.SetCallbackOnDragToIdle(onDragToIdle);

                        ControllerData.SetIsBotCanAction(false);
                        ControllerData.SetIsCanEndTurn(false);
                    }
                    else
                    {
                        ControllerData.SetIsBotCanAction(true);
                        ControllerData.SetIsCanEndTurn(true);
                    }
                }
                break;
        }
    }

    IEnumerator TutorialSoulSubturn8(Action onComplete)
    {
        yield return new WaitForSeconds(0.25f);
        List<int> slotList = new List<int>() { 7 };
        ShowHighlightSoulToken(slotList, true);
        yield return new WaitForSeconds(PLAY_HIGHLIGHT_SLOT_TIME);

        ShowHighlightSoulToken(slotList, false);
        yield return new WaitForSeconds(PLAY_HIGHLIGHT_SLOT_TIME);

        HighlightPlayerDock(UIPlayerDock.HighlightDock.SOUL, PLAY_PLAYER_DOCK_TIME);
        yield return PlayMinionSoulEffectBySlotID(7);
        ShowPlayerFloatingSoul("+2", PLAY_PLAYER_DOCK_TIME);

        UIManager.Instance.UIPlayerBottom.SetPower(2);
        yield return new WaitForSeconds(PLAY_PLAYER_DOCK_TIME);

        HighlightPlayerDock(UIPlayerDock.HighlightDock.SOUL, 2.5f);
        HighlightLabelDock(UIPlayerDock.HighlightDock.SOUL, false, true);
        yield return new WaitForSeconds(PLAY_PLAYER_DOCK_TIME);
        HighlightLabelDock(UIPlayerDock.HighlightDock.SOUL, false, false);

        yield return WaitUntilPlaySoundFinish();
        onComplete?.Invoke();
    }

    public override void OnPlayPhaseTutorial()
    {
    }

    public override void OnEndPlayPhaseTutorial(UnityAction onComplete)
    {
        _manager.StartCoroutine(IEOnEndPlayPhaseTutorial(onComplete));
    }

    // before battle.
    private IEnumerator IEOnEndPlayPhaseTutorial(UnityAction onComplete)
    {
        UIManager.Instance.GetUITutorialManager().RequestHidePopup("T_ArrowEndTurn");
        switch (_manager.SubTurn)
        {
            case 2:
                PlayVoiceWithOutCallback(PlayerIndex.One, "TUT_1_007", "TU1_VOICE_007");
                //Debug.Log("Show highlight arrow of 2 units for 2 sec.");
                {
                    List<int> slotList = new List<int>() { 5, 6 };
                    _manager.GetBoardManager().Tutorial_SlotHilightArrow(slotList, true);
                    yield return new WaitForSeconds(PLAY_HIGHLIGHT_ARROW_TIME);
                    _manager.GetBoardManager().Tutorial_SlotHilightArrow(slotList, false);
                }
                base.HideRemind();
                break;
            case 3:
                //yield return PlayVoice(PlayerIndex.Two, "TUT_1_010", "TU1_VOICE_010", 0.4f);
                yield return PlayVoice(PlayerIndex.One, "TUT_1_011", "TU1_VOICE_011", 0f);
                break;
            case 4:
                {
                    CardDirection elvenMagicianDir = new CardDirection();
                    elvenMagicianDir.Add(CardDirectionType.SE);
                    elvenMagicianDir.Add(CardDirectionType.NW);

                    //yield return PlayVoice(PlayerIndex.One, "TUT_1_012", "TU1_VOICE_012", 0.4f);
                    UIManager.Instance.GetUITutorialManager().RequestHidePopup("T1_EnemyAtkAndHp");
                    base.ShowHighlightArrowToken(2, elvenMagicianDir, true);
                    base.ShowHighlightArrowToken(7, W_Dir, true);

                    yield return new WaitForSeconds(PLAY_HIGHLIGHT_ARROW_TIME);

                    base.ShowHighlightArrowToken(2, elvenMagicianDir, false);
                    base.ShowHighlightArrowToken(7, W_Dir, false);
                }
                break;
            case 5:
                {
                    yield return PlayVoice(PlayerIndex.Two, "TUT_1_015", "TU1_VOICE_015", 0f);

                    List<int> slotList = new List<int>() { 3 };
                    ShowHighlightSoulToken(slotList, true);
                    yield return new WaitForSeconds(PLAY_HIGHLIGHT_SLOT_TIME);

                    ShowHighlightSoulToken(slotList, false);
                    yield return new WaitForSeconds(0.2f);

                    yield return PlayMinionSoulEffectBySlotID(3);

                    HighlightEnemyDock(UIPlayerDock.HighlightDock.SOUL, 3f);
                    ShowEnemyFloatingSoul("+2", PLAY_PLAYER_DOCK_TIME);
                    UIManager.Instance.UIPlayerTop.SetPower(2);
                    yield return new WaitForSeconds(PLAY_PLAYER_DOCK_TIME);

                    HighlightEnemyDock(UIPlayerDock.HighlightDock.SOUL, 2.5f);
                    HighlightLabelDock(UIPlayerDock.HighlightDock.SOUL, true, true);
                    yield return new WaitForSeconds(PLAY_PLAYER_DOCK_TIME);
                    HighlightLabelDock(UIPlayerDock.HighlightDock.SOUL, true, false);

                    CreateDynamicTutorialPopup("TU1_DPUP_3", false, null);
                }
                break;
            case 6:
                {
                    base.ShowHighlightArrowToken(11, N_Dir, true);
                    yield return new WaitForSeconds(PLAY_HIGHLIGHT_ARROW_TIME);
                    base.ShowHighlightArrowToken(11, N_Dir, false);
                }
                break;
            case 10:
                {
                    ShowHighlightArmorToken(AllSlots, false);
                    CreateDynamicTutorialPopup("TU1_DPUP_5", false, null);
                }
                break;
            default:
                if (_manager.SubTurn % 2 == 0)
                {
                    int sum = _manager.GetSumSpirit(_manager.CurrentPlayerIndex);
                    if (_soundKey.Contains("TU1_VOICE_041") == false && sum >= 5)
                    {
                        _soundKey.Add("TU1_VOICE_041");
                        PlayVoiceWithOutCallback(PlayerIndex.Two, "TUT_1_041", "TU1_VOICE_041");
                    }
                }
                break;
        }
        onComplete?.Invoke();
    }

    private IEnumerator PlayMinionSoulEffectBySlotID(int slotID)
    {
        int cardUniqueID = _manager.Data.SlotDatas[slotID].BattleCardUniqueID;
        CardUI cardUI;
        if (BoardManager.Instance.FindTokenById(cardUniqueID, out cardUI))
        {
            bool isFinish = false;
            CardUIManager.Instance.SoulEffectFromMinion(new List<CardUI> { cardUI }, () => isFinish = true);
            yield return new WaitUntil(() => isFinish == true);
        }
        yield break;
    }
    #endregion

    #region Battle Phase

    protected override IEnumerator IEOnBeginPreAttack(Action onComplete)
    {
        switch (_manager.SubTurn)
        {
            case 4:
                {
                    ShowHighlightATKToken(GameHelper.SlotCodeToSlotID("1C"), true);
                    ShowHighlightHPToken(GameHelper.SlotCodeToSlotID("2D"), true);
                    yield return new WaitForSeconds(PLAY_HIGHLIGHT_ARROW_TIME);
                    ShowHighlightATKToken(GameHelper.SlotCodeToSlotID("1C"), false);
                    ShowHighlightHPToken(GameHelper.SlotCodeToSlotID("2D"), false);
                }
                break;
            default:
                break;
        }
        onComplete?.Invoke();
        yield break;
    }

    public override IEnumerator OnEndPreBattlePhaseTutorial(UnityAction onComplete)
    {
        onComplete?.Invoke();
        yield break;
    }

    public override IEnumerator OnPostBattlePhaseTutorial(UnityAction onComplete)
    {
        switch (_manager.SubTurn)
        {
            case 2:
                PlayVoiceWithOutCallback(PlayerIndex.Two, "TUT_1_009", "TU1_VOICE_009");
                break;
            case 4:
                //yield return PlayVoice(PlayerIndex.One, "TUT_1_013", "TU1_VOICE_013", 0.2f);
                //yield return PlayVoice(PlayerIndex.Two, "TUT_1_014", "TU1_VOICE_014", 0.2f);
                break;
            case 6:
                //yield return PlayVoice(PlayerIndex.One, "TUT_1_017", "TU1_VOICE_017", 0.2f);
                //yield return PlayVoice(PlayerIndex.Two, "TUT_1_018", "TU1_VOICE_018", 0.2f);
                break;
            //case 7:
            //    yield return PlayVoice(PlayerIndex.One, "TUT_1_022", "TU1_VOICE_022", 0.2f);
            //    break;
            case 8:
                PlayVoiceWithOutCallback(PlayerIndex.Two, "TUT_1_025", "TU1_VOICE_025");
                break;
            case 11:
                {
                    //yield return PlayVoice(PlayerIndex.Two, "TUT_1_029", "TU1_VOICE_029", 0.2f);
                    //yield return PlayVoice(PlayerIndex.One, "TUT_1_030", "TU1_VOICE_030", 0.2f); 
                }
                break;
        }
        onComplete?.Invoke();
        yield break;
    }
    #endregion

    #region End Game
    protected override IEnumerator OnPlayEndGameSound(bool isVictory)
    {
        TutorialPopupManager.Instance.BTNShowDynamicPopup.gameObject.SetActive(false);
        yield return PlayVoice(PlayerIndex.Two, "TUT_1_038", "TU1_VOICE_038", 0.2f);
        yield return PlayVoice(PlayerIndex.One, "TUT_1_039", "TU1_VOICE_039", 0.2f);
    }

    public override void OnCheckClaimReward(UnityAction onComplete)
    {
        DataManager.Instance.ClaimTutorialRewardByTutorialIndex(0);
        onComplete?.Invoke();
    }

    #endregion

    #endregion
}
