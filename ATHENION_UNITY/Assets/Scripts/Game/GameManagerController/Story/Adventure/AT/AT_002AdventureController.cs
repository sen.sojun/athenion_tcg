﻿using Karamucho.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AT_002AdventureController : AdventureController
{
    public AT_002AdventureController(GameManager manager) : base(manager)
    {
        _manager.IsShuffleDeck = true;
    }

    public override AdventureStageID StageID => AdventureStageID.AT_002;

    #region Methods
    public override void SetupBot(PlayerIndex playerIndex, GameMode gameMode)
    {
        GameObject obj = new GameObject();

        AdventureBot adventureBot = obj.AddComponent<AT_002AdventureBot>();
        if (adventureBot != null)
        {
            Debug.LogFormat("Create AdventureBot for {0}", playerIndex);
            adventureBot.SetPlayerIndex(playerIndex);
            adventureBot.SetEnable(true);
            ControllerData.SetBot(adventureBot);
        }
    }

    public override bool IsTutorialValidSlotID(int slotID)
    {
        if (GameManager.Instance.Phase == GamePhase.Play)
        {
            switch (GameManager.Instance.SubTurn)
            {
                case 1:
                    if(_manager.GetHandCount(PlayerIndex.One) == 2)
                    {
                        return slotID == GameHelper.SlotCodeToSlotID("4C");
                    }
                    else if(_manager.GetHandCount(PlayerIndex.One) == 1)
                    {
                        return slotID == GameHelper.SlotCodeToSlotID("4D");
                    }
                    break;
                case 3:
                    return slotID == GameHelper.SlotCodeToSlotID("3D");
                default: return true;
            }
        }
        return true;
    }

    public override bool IsCanUseCard(MinionData minion)
    {
        switch (GameManager.Instance.SubTurn)
        {
            case 1:
            {
                if (GameManager.Instance.GetHandCount(PlayerIndex.One) == 2)
                {
                    return minion.FullID == "C0029";
                }
                else
                {
                    return base.IsCanUseCard(minion);
                }
            }
        }

        return base.IsCanUseCard(minion);
    }

    public override bool IsCanShowRedButton(int subturn)
    {
        return subturn > 3;
    }
    #endregion

    #region Game Phase
    public override IEnumerator SetupPhase()
    {
        _manager.SetPlayOrder(PlayerIndex.One, null);
        _manager._isPlayFirstReady = true;

        while (!_manager.IsCanNextPhase || !_manager.IsPhaseReady(GamePhase.SetupData) || !_manager._isPlayFirstReady)
        {
            yield return null;
        }

        _manager.SetRandomSeed(_manager.MatchData.PlayerData[1].RandomSeed);
        _manager.SetLoadingProgress(1.0f);
        NextPhase();

        //_manager.InitDraw(); จั่วมือเริ่ม
        UIManager.Instance.FadeBlackIn();
    }

    protected override IEnumerator MiddleIntroPhase()
    {
        Debug.Log("เปิดมาด้วยสกรีนสีดำ ที่พอ fade ออกไป");
        yield return new WaitForSeconds(1f);

        Debug.Log("ก็เห็น token ของ Player อยู่ในฝั่ง Player โดยที่ Token ของฝั่งศัตรูยังไม่มีบอกว่าเป็นใคร");
        UIManager.Instance.SpotLight_ShowHeroBot(true);
        yield return new WaitForSeconds(1f);

        yield return PlayVoice(PlayerIndex.One, "AT_2_001", "DIALOG_AT_002_001", 1.5f);
        yield return PlayVoice(PlayerIndex.One, "AT_2_002", "DIALOG_AT_002_002", 0.8f);
        yield return PlayVoice(PlayerIndex.One, "AT_2_003", "DIALOG_AT_002_003", 1.5f);

        Debug.Log("แล้วก็ปรากฎเป็น token ของ Enemy ลอยอยู่.");
        {
            UIManager.Instance.UIPlayerTop.Intro_State_ShowAtCenter();
            UIManager.Instance.SpotLight_ShowMid(true);
        }
        yield return new WaitForSeconds(1.5f);

        Debug.Log("smash ลงช่อง token ของศัตรู แล้ว fade ดำก็หายไป");
        {
            UIManager.Instance.UIPlayerTop.Intro_State_Default();
            UIManager.Instance.SpotLight_ShowMid(false);
            UIManager.Instance.SpotLight_ShowHeroBot(false);
            UIManager.Instance.FadeBlackOut();
        }

        yield return new WaitForSeconds(0.5f);
        yield return PlayVoice(PlayerIndex.Two, "AT_2_018", "DIALOG_AT_002_004", 0f);

        while (!_manager.IsCanNextPhase)
        {
            yield return null;
        }

    }

    public override IEnumerator IEOnEndBeginPhase()
    {
        if (_manager.SubTurn % 2 == 1) // Player turn.
        {
            int enemyHP = _manager.Data.PlayerDataList[(int)PlayerIndex.Two].CurrentHP;
            if (enemyHP - _manager.GetSumSpirit(PlayerIndex.One) <= 0)
            {
                yield return PlayVoice(PlayerIndex.One, "AT_2_014", "DIALOG_AT_002_016", 0.8f);
            }
        }
        yield break;
    }

    public override IEnumerator OnEndSpiritPhaseTutorial(UnityAction onComplete)
    {
        int playerHP = _manager.Data.PlayerDataList[(int)PlayerIndex.One].CurrentHP;
        int enemyHP = _manager.Data.PlayerDataList[(int)PlayerIndex.Two].CurrentHP;

        if (_manager.SubTurn % 2 == 1) // Player turn.
        {
            if (enemyHP <= 5 && !_playedSoundKeys.Contains("DIALOG_AT_002_015"))
            {
                _playedSoundKeys.Add("DIALOG_AT_002_015");
                yield return PlayVoice(PlayerIndex.Two, "AT_2_018", "DIALOG_AT_002_015", 0.8f);
            }
        }
        else  // Enemy turn.
        {
            if (playerHP <= 5 && !_playedSoundKeys.Contains("DIALOG_AT_002_014"))
            {
                _playedSoundKeys.Add("DIALOG_AT_002_014");
                yield return PlayVoice(PlayerIndex.One, "AT_2_013", "DIALOG_AT_002_014", 0.8f);
            }
        }
        onComplete?.Invoke();
        yield break;
    }

    //Fix draw
    public override IEnumerator AfterAddAP()
    {
        switch (_manager.SubTurn)
        {
            case 1:
                yield return FixedDrawCardToHand("C0029", PlayerIndex.One);
                yield return FixedDrawCardToHand("C0135", PlayerIndex.One);
                break;
            case 2:
                yield return FixedDrawCardToHand("C0129", PlayerIndex.Two);
                break;
            case 3:
                yield return FixedDrawCardToHand("C0090", PlayerIndex.One);
                break;
            case 13:
                yield return PlayVoice(PlayerIndex.Two, "AT_2_018", "DIALOG_AT_002_020", 0.8f);
                yield return PlayVoice(PlayerIndex.One, "AT_2_016", "DIALOG_AT_002_021", 1f);
                break;
        }
    }

    public override void OnSetDrawCount(int subturn, ref int drawCount, ref bool isDrawAP)
    {
        switch (_manager.SubTurn)
        {
            case 1: drawCount = 0; break;
            case 2: drawCount = 0; break;
            case 3: drawCount = 0; break;
            default:
                int toDrawCount = _manager.MaxHandCard;
                int handCount = _manager.Data.PlayerDataList[(int)_manager.CurrentPlayerIndex].CardList[CardZone.Hand].Count;
                int deckCount = _manager.Data.PlayerDataList[(int)_manager.CurrentPlayerIndex].CardList[CardZone.Deck].Count;
                int handSlot = toDrawCount - handCount;

                drawCount = Mathf.Min(handSlot, deckCount);
                break;
        }
    }

    public override IEnumerator OnEndPrePlayPhaseTutorial(UnityAction onComplete)
    {
        switch (_manager.SubTurn)
        {
            case 1:
                AddVoiceToList(PlayerIndex.One, "AT_2_004", "DIALOG_AT_002_005", 0.8f);
                AddVoiceToList(PlayerIndex.One, "AT_2_005", "DIALOG_AT_002_006", 1f);
                _manager.StartCoroutine(PlayVoiceList());
                break;
            case 3:
                AddVoiceToList(PlayerIndex.One, "AT_2_007", "DIALOG_AT_002_008", 0.3f);
                AddVoiceToList(PlayerIndex.One, "AT_2_008", "DIALOG_AT_002_009", 1f);
                _manager.StartCoroutine(PlayVoiceList());
                break;
            default:
                if (_manager.SubTurn % 2 == 1 && _manager.FindBattleCard("C0245", CardZone.Hand, PlayerIndex.One, out BattleCardData card) && !_playedSoundKeys.Contains("DIALOG_AT_002_012"))
                {
                    _playedSoundKeys.Add("DIALOG_AT_002_012");
                    AddVoiceToList(PlayerIndex.One, "AT_2_011", "DIALOG_AT_002_012", 0.5f);
                    AddVoiceToList(PlayerIndex.One, "AT_2_012", "DIALOG_AT_002_013", 1f);
                    _manager.StartCoroutine(PlayVoiceList());
                }
                break;
        }
        onComplete?.Invoke();
        yield break;
    }

    public override void OnBeginPlayPhaseTutorial()
    {
        switch (_manager.SubTurn)
        {
            case 1:
                {
                    base.PointerPlayerCard(0, true, GameHelper.GetColorByHtmlCode(StoryGameController.HIGHLIGHT_POINTER_GREEN));
                    List<int> highlightBoard = new List<int>() { GameHelper.SlotCodeToSlotID("4C") };
                    ShowHighlightBoard(highlightBoard, Karamucho.UI.BoardSlotHolder.SlotHilightType.Hilight);

                    UnityAction onIdleToSelect = delegate ()
                    {
                        ShowHighlightBoard(highlightBoard, Karamucho.UI.BoardSlotHolder.SlotHilightType.Hilight);
                    };

                    UnityAction onDragToIdle = delegate ()
                    {
                        if (_manager.GetHandCount(PlayerIndex.One) == 1)
                        {
                            HideHighlightBoard(highlightBoard);
                            base.PointerPlayerCard(0, true, GameHelper.GetColorByHtmlCode(StoryGameController.HIGHLIGHT_POINTER_GREEN));
                            highlightBoard = new List<int>() { GameHelper.SlotCodeToSlotID("4D") };
                            ShowHighlightBoard(highlightBoard, Karamucho.UI.BoardSlotHolder.SlotHilightType.Hilight);
                        }
                        else if (_manager.IsEmptyHand(_manager.CurrentPlayerIndex))
                        {
                            HideHighlightBoard(highlightBoard);
                            _manager.StartCoroutine(GameHelper.ExecuteIEnumerator(WaitUntilPlaySoundFinish(), () =>
                            {
                                GameHelper.DelayCallback(0.5f, delegate ()
                                {
                                    _manager.ActionPlayCustomVoice(PlayerIndex.One, "AT_2_006", "DIALOG_AT_002_007", () =>
                                    {
                                        ControllerData.SetIsCanEndTurn(true);
                                        ControllerData.ClearPlayPhaseEventCallback();
                                    }, 0.8f);
                                });
                            }));
                        }
                    };

                    ControllerData.SetCallbackOnIdleToSelect(onIdleToSelect);
                    ControllerData.SetCallbackOnDragToIdle(onDragToIdle);

                    ControllerData.SetIsBotCanAction(false);
                    ControllerData.SetIsCanEndTurn(false);

                }
                break;
            case 3:
                {
                    base.PointerPlayerCard(0, true, GameHelper.GetColorByHtmlCode(StoryGameController.HIGHLIGHT_POINTER_GREEN));
                    List<int> highlightBoard = new List<int>() { GameHelper.SlotCodeToSlotID("3D") };
                    ShowHighlightBoard(highlightBoard, Karamucho.UI.BoardSlotHolder.SlotHilightType.Hilight);

                    UnityAction onDragToIdle = delegate ()
                    {
                        if (_manager.IsEmptyHand(_manager.CurrentPlayerIndex))
                        {
                            HideHighlightBoard(highlightBoard);
                            _manager.StartCoroutine(GameHelper.ExecuteIEnumerator(WaitUntilPlaySoundFinish(), () =>
                            {
                                GameHelper.DelayCallback(0.5f, delegate ()
                                {
                                    _manager.ActionPlayCustomVoice(PlayerIndex.One, "AT_2_009", "DIALOG_AT_002_010", () =>
                                    {
                                        ControllerData.SetIsCanEndTurn(true);
                                        ControllerData.ClearPlayPhaseEventCallback();
                                    }, 1f);
                                });
                            }));
                        }
                    };
                    ControllerData.SetCallbackOnDragToIdle(onDragToIdle);
                }
                break;
            default:
                {
                    ControllerData.SetIsBotCanAction(true);
                    _manager.StartCoroutine(GameHelper.ExecuteIEnumerator(WaitUntilPlaySoundFinish(), () => { ControllerData.SetIsCanEndTurn(true); }));
                }
                break;
        }
    }

    public override void OnPlayPhaseTutorial()
    {
    }

    public override void OnEndPlayPhaseTutorial(UnityAction onComplete)
    {
        onComplete?.Invoke();
    }

    public override IEnumerator OnEndPreBattlePhaseTutorial(UnityAction onComplete)
    {
        onComplete?.Invoke();
        yield break;
    }

    public override IEnumerator OnPostBattlePhaseTutorial(UnityAction onComplete)
    {
        int amount = 0;
        List<MinionData> minionDatas = _manager.Data.GetAllMinionsInSlot();
        for (int i = 0; i < minionDatas.Count; i++)
        {
            if (minionDatas[i].Owner == PlayerIndex.One && minionDatas[i].IsBeLink)
            {
                amount++;
            }
        }
        if (amount > 2 && !_playedSoundKeys.Contains("DIALOG_AT_002_022") && _manager.SubTurn > 4)
        {
            _playedSoundKeys.Add("DIALOG_AT_002_022");
            yield return PlayVoice(PlayerIndex.One, "AT_2_017", "DIALOG_AT_002_022", 0.5f);
        }

        onComplete?.Invoke();
        yield break;
    }

    protected override IEnumerator OnPlayEndGameSound(bool isVictory)
    {
        if (isVictory == false)
        {
            yield return PlayVoice(PlayerIndex.One, "AT_2_010", "DIALOG_AT_002_011", 0.8f);
        }
        yield break;
    }

    public override void OnResolveDead(List<MinionData> deadTargetList)
    {
        if (_manager.SubTurn > 3)
        {
            List<MinionData> enemyMinions = deadTargetList.FindAll((minion) => minion.Owner != _manager.CurrentPlayerIndex && minion.SpiritCurrent >= 3);
            if (enemyMinions.Count == 0)
                return;
            bool playerTurn = _manager.SubTurn % 2 == 1;
            if (playerTurn && _playedSoundKeys.Contains("DIALOG_AT_002_019") == false)
            {
                _playedSoundKeys.Add("DIALOG_AT_002_019");
                _manager.ActionPlayCustomVoice(PlayerIndex.Two, "AT_2_018", "DIALOG_AT_002_019", null, 0.8f);

            }
            else if (playerTurn == false && _playedSoundKeys.Contains("DIALOG_AT_002_017") == false)
            {
                _playedSoundKeys.Add("DIALOG_AT_002_017");
                _manager.ActionPlayCustomVoice(PlayerIndex.Two, "AT_2_018", "DIALOG_AT_002_017", () =>
                {
                    _manager.ActionPlayCustomVoice(PlayerIndex.One, "AT_2_015", "DIALOG_AT_002_018", null, 0.5f);
                });
            }
        }
    }
    #endregion
}
