﻿using Karamucho.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AT_001AdventureController : AdventureController
{
    public AT_001AdventureController(GameManager manager) : base(manager)
    {
        _manager.IsShuffleDeck = true;
    }

    public override AdventureStageID StageID => AdventureStageID.AT_001;

    #region Methods

    public override void SetupBot(PlayerIndex playerIndex, GameMode gameMode)
    {
        GameObject obj = new GameObject();

        AdventureBot adventureBot = obj.AddComponent<AT_001AdventureBot>();
        if (adventureBot != null)
        {
            Debug.LogFormat("Create AdventureBot for {0}", playerIndex);
            adventureBot.SetPlayerIndex(playerIndex);
            adventureBot.SetEnable(true);
            ControllerData.SetBot(adventureBot);
        }
    }

    public override bool IsTutorialValidSlotID(int slotID)
    {
        if (GameManager.Instance.Phase == GamePhase.Play)
        {
            switch (GameManager.Instance.SubTurn)
            {
                case 1:
                    return slotID == GameHelper.SlotCodeToSlotID("2A");
                default: return true;
            }
        }
        return true;
    }

    public override bool IsCanUseCard(MinionData minion)
    {
        switch (GameManager.Instance.SubTurn)
        {
            case 3:
                {
                    if (GameManager.Instance.GetHandCount(PlayerIndex.One) == 2)
                    {
                        return minion.FullID == "C0089";
                    }
                    else
                    {
                        return base.IsCanUseCard(minion);
                    }
                }
        }

        return base.IsCanUseCard(minion);
    }

    public override bool IsCanShowRedButton(int subturn)
    {
        return subturn > 3;
    }
    #endregion

    #region Game Phase
    public override IEnumerator SetupPhase()
    {
        _manager.SetPlayOrder(PlayerIndex.One, null);
        _manager._isPlayFirstReady = true;

        while (!_manager.IsCanNextPhase || !_manager.IsPhaseReady(GamePhase.SetupData) || !_manager._isPlayFirstReady)
        {
            yield return null;
        }

        _manager.SetRandomSeed(_manager.MatchData.PlayerData[1].RandomSeed);
        _manager.SetLoadingProgress(1.0f);
        NextPhase();

        //_manager.InitDraw(); จั่วมือเริ่ม
        UIManager.Instance.FadeBlackIn();
    }

    protected override IEnumerator MiddleIntroPhase()
    {
        Debug.Log("เปิดมาด้วยสกรีนสีดำ ที่พอ fade ออกไป");
        yield return new WaitForSeconds(1f);

        Debug.Log("ก็เห็น token ของ Player อยู่ในฝั่ง Player โดยที่ Token ของฝั่งศัตรูยังไม่มีบอกว่าเป็นใคร");
        UIManager.Instance.SpotLight_ShowHeroBot(true);
        yield return new WaitForSeconds(1f);

        yield return PlayVoice(PlayerIndex.One, "AT_1_001", "DIALOG_AT_001_001", 0.8f);
        yield return PlayVoice(PlayerIndex.One, "AT_1_002", "DIALOG_AT_001_002", 0.5f);
        yield return PlayVoice(PlayerIndex.One, "AT_1_003", "DIALOG_AT_001_003", 0.8f);

        Debug.Log("แล้วก็ปรากฎเป็น token ของ Enemy ลอยอยู่.");
        {
            UIManager.Instance.UIPlayerTop.Intro_State_ShowAtCenter();
            UIManager.Instance.SpotLight_ShowMid(true);
        }
        yield return new WaitForSeconds(1.5f);

        Debug.Log("smash ลงช่อง token ของศัตรู แล้ว fade ดำก็หายไป");
        {
            UIManager.Instance.UIPlayerTop.Intro_State_Default();
            UIManager.Instance.SpotLight_ShowMid(false);
            UIManager.Instance.SpotLight_ShowHeroBot(false);
            UIManager.Instance.FadeBlackOut();
        }

        yield return new WaitForSeconds(0.5f);
        yield return PlayVoice(PlayerIndex.Two, "AT_1_021", "DIALOG_AT_001_004", 0.5f);

        while (!_manager.IsCanNextPhase)
        {
            yield return null;
        }

    }

    public override IEnumerator IEOnEndBeginPhase()
    {
        if (_manager.SubTurn % 2 == 1) // Player turn.
        {
            int enemyHP = _manager.Data.PlayerDataList[(int)PlayerIndex.Two].CurrentHP;
            if (enemyHP - _manager.GetSumSpirit(PlayerIndex.One) <= 0)
            {
                yield return PlayVoice(PlayerIndex.One, "AT_1_016", "DIALOG_AT_001_019", 0.3f);
            }
        }
        yield break;
    }

    public override IEnumerator OnEndSpiritPhaseTutorial(UnityAction onComplete)
    {
        int playerHP = _manager.Data.PlayerDataList[(int)PlayerIndex.One].CurrentHP;
        int enemyHP = _manager.Data.PlayerDataList[(int)PlayerIndex.Two].CurrentHP;

        if (_manager.SubTurn % 2 == 1) // Player turn.
        {
            if (enemyHP <= 5 && !_playedSoundKeys.Contains("DIALOG_AT_001_018"))
            {
                _playedSoundKeys.Add("DIALOG_AT_001_018");
                yield return PlayVoice(PlayerIndex.Two, "AT_1_021", "DIALOG_AT_001_018", 0.5f);
            }
        }
        else  // Enemy turn.
        {
            if (playerHP <= 5 && !_playedSoundKeys.Contains("DIALOG_AT_001_017"))
            {
                _playedSoundKeys.Add("DIALOG_AT_001_017");
                yield return PlayVoice(PlayerIndex.One, "AT_1_015", "DIALOG_AT_001_017", 0.8f);
            }
        }
        onComplete?.Invoke();
        yield break;
    }

    //Fix draw
    public override IEnumerator AfterAddAP()
    {
        int amount = 0;
        List<MinionData> minionDatas = _manager.Data.GetAllMinionsInSlot();
        for (int i = 0; i < minionDatas.Count; i++)
        {
            if (minionDatas[i].Owner == PlayerIndex.One && minionDatas[i].IsBerserkActive)
            {
                amount++;
            }
        }
        if (_manager.SubTurn % 2 == 1 && amount >= 2 && !_playedSoundKeys.Contains("DIALOG_AT_001_025"))
        {
            _playedSoundKeys.Add("DIALOG_AT_001_025");
            yield return PlayVoice(PlayerIndex.One, "AT_1_020", "DIALOG_AT_001_025", 0.5f);
        }

        switch (_manager.SubTurn)
        {
            case 1:
                yield return FixedDrawCardToHand("C0005", PlayerIndex.One);
                break;
            case 2:
                yield return PlayVoice(PlayerIndex.Two, "AT_1_021", "DIALOG_AT_001_006", 0.5f);
                yield return FixedDrawCardToHand("C0135", PlayerIndex.Two);
                break;
            case 3:
                yield return FixedDrawCardToHand("C0089", PlayerIndex.One);
                yield return FixedDrawCardToHand("C0147", PlayerIndex.One);
                break;
            case 4:
                yield return PlayVoice(PlayerIndex.Two, "AT_1_011", "DIALOG_AT_001_013", 0.5f);
                break;
            case 13:
                yield return PlayVoice(PlayerIndex.One, "AT_1_019", "DIALOG_AT_001_023", 0.5f);
                yield return PlayVoice(PlayerIndex.Two, "AT_1_021", "DIALOG_AT_001_024", 0.5f);
                break;
        }
    }

    public override void OnSetDrawCount(int subturn, ref int drawCount, ref bool isDrawAP)
    {
        switch (_manager.SubTurn)
        {
            case 1: drawCount = 0; break;
            case 2: drawCount = 0; break;
            case 3: drawCount = 0; break;
            default:
                int toDrawCount = _manager.MaxHandCard;
                int handCount = _manager.Data.PlayerDataList[(int)_manager.CurrentPlayerIndex].CardList[CardZone.Hand].Count;
                int deckCount = _manager.Data.PlayerDataList[(int)_manager.CurrentPlayerIndex].CardList[CardZone.Deck].Count;
                int handSlot = toDrawCount - handCount;

                drawCount = Mathf.Min(handSlot, deckCount);
                break;
        }
    }

    public override IEnumerator OnEndPrePlayPhaseTutorial(UnityAction onComplete)
    {
        switch (_manager.SubTurn)
        {
            case 1:
                AddVoiceToList(PlayerIndex.One, "AT_1_004", "DIALOG_AT_001_005", 1f);
                _manager.StartCoroutine(PlayVoiceList());
                break;
            case 3:
                AddVoiceToList(PlayerIndex.One, "AT_1_007", "DIALOG_AT_001_009", 0.8f);
                AddVoiceToList(PlayerIndex.One, "AT_1_008", "DIALOG_AT_001_010", 1f);
                _manager.StartCoroutine(PlayVoiceList());
                break;
            default:
                if(_manager.SubTurn % 2 == 1 && _manager.FindBattleCard("C0182", CardZone.Hand, PlayerIndex.One, out BattleCardData card) && !_playedSoundKeys.Contains("DIALOG_AT_001_015"))
                {
                    _playedSoundKeys.Add("DIALOG_AT_001_015");
                    AddVoiceToList(PlayerIndex.One, "AT_1_013", "DIALOG_AT_001_015", 0.3f);
                    AddVoiceToList(PlayerIndex.One, "AT_1_014", "DIALOG_AT_001_016", 1f);
                    _manager.StartCoroutine(PlayVoiceList());
                }
                break;
        }
        onComplete?.Invoke();
        yield break;
    }

    public override void OnBeginPlayPhaseTutorial()
    {
        switch (_manager.SubTurn)
        {
            case 1:
                {
                    base.PointerPlayerCard(0, true, GameHelper.GetColorByHtmlCode(StoryGameController.HIGHLIGHT_POINTER_GREEN));
                    List<int> highlightBoard = new List<int>() { GameHelper.SlotCodeToSlotID("2A") };
                    ShowHighlightBoard(highlightBoard, Karamucho.UI.BoardSlotHolder.SlotHilightType.Hilight);

                    UnityAction onIdleToSelect = delegate ()
                    {
                        ShowHighlightBoard(highlightBoard, Karamucho.UI.BoardSlotHolder.SlotHilightType.Hilight);
                    };

                    UnityAction onDragToIdle = delegate ()
                    {
                        if (_manager.IsEmptyHand(_manager.CurrentPlayerIndex))
                        {
                            HideHighlightBoard(highlightBoard);
                            _manager.StartCoroutine(GameHelper.ExecuteIEnumerator(WaitUntilPlaySoundFinish(), () => { ControllerData.SetIsCanEndTurn(true); }));
                            ControllerData.ClearPlayPhaseEventCallback();
                        }
                    };

                    ControllerData.SetCallbackOnIdleToSelect(onIdleToSelect);
                    ControllerData.SetCallbackOnDragToIdle(onDragToIdle);

                    ControllerData.SetIsBotCanAction(false);
                    ControllerData.SetIsCanEndTurn(false);

                }
                break;
            case 3:
                {
                    base.PointerPlayerCard(0, true, GameHelper.GetColorByHtmlCode(StoryGameController.HIGHLIGHT_POINTER_GREEN));

                    UnityAction onDragToIdle = delegate ()
                    {
                        if (_manager.GetHandCount(PlayerIndex.One) == 1)
                        {
                            //Highlight remaining card to be played
                            base.PointerPlayerCard(0, true, GameHelper.GetColorByHtmlCode(StoryGameController.HIGHLIGHT_POINTER_GREEN));
                        }
                        else if (_manager.IsEmptyHand(_manager.CurrentPlayerIndex))
                        {
                            BattleCardData card;
                            if (!_manager.FindBattleCard("C0089", CardZone.Battlefield, PlayerIndex.One, out card))
                            {
                                Debug.Log("Cannot find the card on the field");
                            }

                            SetActiveHandPointer(true, base.GetSlotScreenPosition(card.SlotID));
                            BoardManager.Instance.SetDeactivatedSlotList(new List<int> { GameHelper.SlotCodeToSlotID("2A") });
                            BoardManager.Instance.AddEventOnSelectedComplete(delegate (List<int> slots)
                            {
                                BoardManager.Instance.ClearDeactivatedSlotList();
                                SetActiveHandPointer(false, base.GetSlotScreenPosition(card.SlotID));
                                _manager.StartCoroutine(GameHelper.ExecuteIEnumerator(WaitUntilPlaySoundFinish(), () =>
                                {
                                    GameHelper.DelayCallback(0.5f, delegate ()
                                    {
                                        _manager.ActionPlayCustomVoice(PlayerIndex.One, "AT_1_009", "DIALOG_AT_001_011", () =>
                                        _manager.ActionPlayCustomVoice(PlayerIndex.One, "AT_1_010", "DIALOG_AT_001_012", () =>
                                        {
                                            ControllerData.SetIsCanEndTurn(true);
                                            ControllerData.ClearPlayPhaseEventCallback();
                                        }, 0.8f)
                                        , 0.5f);
                                    });
                                }));

                            });
                        }
                    };
                    ControllerData.SetCallbackOnDragToIdle(onDragToIdle);
                }
                break;
            default:
                {
                    ControllerData.SetIsBotCanAction(true);
                    _manager.StartCoroutine(GameHelper.ExecuteIEnumerator(WaitUntilPlaySoundFinish(), () => { ControllerData.SetIsCanEndTurn(true); }));
                }
                break;
        }
    }

    public override void OnPlayPhaseTutorial()
    {
    }

    public override void OnEndPlayPhaseTutorial(UnityAction onComplete)
    {
        onComplete?.Invoke();
    }

    public override IEnumerator OnEndPreBattlePhaseTutorial(UnityAction onComplete)
    {
        onComplete?.Invoke();
        yield break;
    }

    public override IEnumerator OnPostBattlePhaseTutorial(UnityAction onComplete)
    {
        switch (_manager.SubTurn)
        {
            case 2:
                yield return PlayVoice(PlayerIndex.One, "AT_1_005", "DIALOG_AT_001_007", 0.8f);
                yield return PlayVoice(PlayerIndex.One, "AT_1_006", "DIALOG_AT_001_008", 1f);
                break;
            default:
                break;
        }
        onComplete?.Invoke();
        yield break;
    }

    protected override IEnumerator OnPlayEndGameSound(bool isVictory)
    {
        if (isVictory == false)
        {
            yield return PlayVoice(PlayerIndex.One, "AT_1_012", "DIALOG_AT_001_014", 1f);
        }
        yield break;
    }

    public override void OnResolveDead(List<MinionData> deadTargetList)
    {
        if (_manager.SubTurn > 3)
        {
            List<MinionData> enemyMinions = deadTargetList.FindAll((minion) => minion.Owner != _manager.CurrentPlayerIndex && minion.SpiritCurrent >= 3);
            if (enemyMinions.Count == 0)
                return;
            bool playerTurn = _manager.SubTurn % 2 == 1;
            if (playerTurn && _playedSoundKeys.Contains("DIALOG_AT_001_022") == false)
            {
                _playedSoundKeys.Add("DIALOG_AT_001_022");
                _manager.ActionPlayCustomVoice(PlayerIndex.One, "AT_1_018", "DIALOG_AT_001_022", () => 
                    {
                        _manager.ActionPlayCustomVoice(PlayerIndex.Two, "AT_1_021", "DIALOG_AT_001_024", null, 0f);
                    }, 0.3f);
            }
            else if (playerTurn == false && _playedSoundKeys.Contains("DIALOG_AT_001_020") == false)
            {
                _playedSoundKeys.Add("DIALOG_AT_001_020");
                _manager.ActionPlayCustomVoice(PlayerIndex.Two, "AT_1_021", "DIALOG_AT_001_020", () =>
                    {
                        _manager.ActionPlayCustomVoice(PlayerIndex.One, "AT_1_017", "DIALOG_AT_001_021", null, 1f);
                    }, 0.8f);
            }
        }
    }
    #endregion
}
