﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AT_004AdventureController : AdventureController
{
    public override AdventureStageID StageID => AdventureStageID.AT_004;
    public AT_004AdventureController(GameManager manager) : base(manager)
    {
        _manager.IsShuffleDeck = true;
        GameManager.OnMinionMoveToSlot += GameManager_OnMinionMoveToSlot;
    }

    public override void DeInitialize()
    {
        GameManager.OnMinionMoveToSlot -= GameManager_OnMinionMoveToSlot;
        base.DeInitialize();
    }


    #region Methods

    public override void SetupBot(PlayerIndex playerIndex, GameMode gameMode)
    {
        GameObject obj = new GameObject();

        AdventureBot adventureBot = obj.AddComponent<AT_004AdventureBot>();
        if (adventureBot != null)
        {
            Debug.LogFormat("Create AdventureBot for {0}", playerIndex);
            adventureBot.SetPlayerIndex(playerIndex);
            //bot.SetTutorialMode(gameMode);

            //this._onShowSpiritAttackOnPlayerComplete += bot.OnSpiritAttackOnPlayer;
            //this._onPlayerPlayHandCardComplete += bot.OnPlayHandCard;

            adventureBot.SetEnable(true);
            ControllerData.SetBot(adventureBot);
        }
    }

    public override bool IsCanShowRedButton(int subturn)
    {
        return subturn > 4;
    }

    public override bool IsTutorialValidSlotID(int slotID)
    {
        if (GameManager.Instance.Phase == GamePhase.Play)
        {
            switch (GameManager.Instance.SubTurn)
            {
                case 2:
                    return slotID == GameHelper.SlotCodeToSlotID("1A");
                case 4:
                    return slotID == GameHelper.SlotCodeToSlotID("2A");
                default: return true;
            }
        }
        return true;
    }

    private void GameManager_OnMinionMoveToSlot(MinionData minion, int slotID)
    {
        if (_manager.SubTurn > 4 && _manager.SubTurn % 2 == 0)
        {
            if (minion.Owner != PlayerIndex.One)
                return;
            if (_playedSoundKeys.Contains("DIALOG_AT_004_024") == false)
            {
                _playedSoundKeys.Add("DIALOG_AT_004_024");
                _manager.ActionPlayCustomVoice(PlayerIndex.One, "AT_4_020", "DIALOG_AT_004_024", null);
            }
        }
    }

    #endregion

    #region Game Phase

    #region SetupPhase

    public override IEnumerator SetupPhase()
    {
        _manager.SetPlayOrder(PlayerIndex.Two, null);
        _manager._isPlayFirstReady = true;

        while (!_manager.IsCanNextPhase || !_manager.IsPhaseReady(GamePhase.SetupData) || !_manager._isPlayFirstReady)
        {
            yield return null;
        }

        _manager.SetRandomSeed(_manager.MatchData.PlayerData[1].RandomSeed);
        _manager.SetLoadingProgress(1.0f);
        NextPhase();

        UIManager.Instance.FadeBlackIn();
    }

    #endregion

    #region Intro Phase

    protected override IEnumerator MiddleIntroPhase()
    {
        Debug.Log("เปิดมาด้วยสกรีนสีดำ ที่พอ fade ออกไป");
        yield return new WaitForSeconds(1f);

        Debug.Log("ก็เห็น token ของ Nyx อยู่ในฝั่ง Player โดยที่ Token ของฝั่งศัตรูยังไม่มีบอกว่าเป็นใคร");
        UIManager.Instance.SpotLight_ShowHeroBot(true);
        yield return new WaitForSeconds(1f);

        yield return PlayVoice(PlayerIndex.One, "AT_4_001", "DIALOG_AT_004_001", 1f);
        yield return PlayVoice(PlayerIndex.One, "AT_4_002", "DIALOG_AT_004_002", 1.5f);

        Debug.Log("แล้วก็ปรากฎเป็น token ของ Enemy ลอยอยู่.");
        {
            UIManager.Instance.UIPlayerTop.Intro_State_ShowAtCenter();
            UIManager.Instance.SpotLight_ShowMid(true);
        }
        yield return new WaitForSeconds(1.5f);

        Debug.Log("smash ลงช่อง token ของศัตรู แล้ว fade ดำก็หายไป");
        {
            UIManager.Instance.UIPlayerTop.Intro_State_Default();
            UIManager.Instance.SpotLight_ShowMid(false);
            UIManager.Instance.SpotLight_ShowHeroBot(false);
            UIManager.Instance.FadeBlackOut();
        }

        yield return new WaitForSeconds(0.5f);
        yield return PlayVoice(PlayerIndex.Two, "AT_4_023", "DIALOG_AT_004_003", 0f);
        yield return PlayVoice(PlayerIndex.One, "AT_4_003", "DIALOG_AT_004_004", 0.5f);

        while (!_manager.IsCanNextPhase)
        {
            yield return null;
        }

    }

    #endregion

    #region Begin Phase

    public override IEnumerator IEOnEndBeginPhase()
    {
        if (_manager.SubTurn % 2 == 0) // Player turn.
        {
            int enemyHP = _manager.Data.PlayerDataList[(int)PlayerIndex.Two].CurrentHP;
            if (enemyHP - _manager.GetSumSpirit(PlayerIndex.One) <= 0)
            {
                yield return PlayVoice(PlayerIndex.One, "AT_4_016", "DIALOG_AT_004_018", 0.3f);
            }
        }
        yield break;
    }

    #endregion

    #region Spirit Phase

    public override IEnumerator OnEndSpiritPhaseTutorial(UnityAction onComplete)
    {
        int playerHP = _manager.Data.PlayerDataList[(int)PlayerIndex.One].CurrentHP;
        int enemyHP = _manager.Data.PlayerDataList[(int)PlayerIndex.Two].CurrentHP;

        if (_manager.SubTurn % 2 == 0) // Player turn.
        {
            if (enemyHP <= 5)
            {
                if (_playedSoundKeys.Contains("DIALOG_AT_004_017") == false)
                {
                    _playedSoundKeys.Add("DIALOG_AT_004_017");
                    yield return PlayVoice(PlayerIndex.Two, "AT_4_022", "DIALOG_AT_004_017", 0.3f);
                }
            }
        }
        else  // Enemy turn.
        {
            if (playerHP <= 5)
            {
                if (_playedSoundKeys.Contains("DIALOG_AT_004_016") == false)
                {
                    _playedSoundKeys.Add("DIALOG_AT_004_016");
                    yield return PlayVoice(PlayerIndex.One, "AT_4_015", "DIALOG_AT_004_016", 0.5f);
                }
            }
        }
        onComplete?.Invoke();
        yield break;
    }

    #endregion

    #region Draw Phase

    public override IEnumerator AfterAddAP()
    {
        switch (_manager.SubTurn)
        {
            case 1:
                yield return FixedDrawCardToHand("C0028", PlayerIndex.Two);
                break;
            case 2:
                yield return FixedDrawCardToHand("C0003", PlayerIndex.One);
                break;
            case 3:
                yield return FixedDrawCardToHand("C0114", PlayerIndex.Two);
                yield return FixedDrawCardToHand("C0114", PlayerIndex.Two);
                break;
            case 4:
                yield return FixedDrawCardToHand("C0106", PlayerIndex.One);
                break;
            case 12:
                yield return PlayVoice(PlayerIndex.Two, "AT_4_022", "DIALOG_AT_004_022", 0.3f);
                yield return PlayVoice(PlayerIndex.One, "AT_4_019", "DIALOG_AT_004_023", 0.8f);
                break;
            default:
                break;
        }
        yield break;
    }

    public override void OnSetDrawCount(int subturn, ref int drawCount, ref bool isDrawAP)
    {
        isDrawAP = false;
        switch (_manager.SubTurn)
        {
            case 1: drawCount = 0; break;
            case 2: drawCount = 0; break;
            case 3: drawCount = 0; break;
            case 4: drawCount = 0; break;
            default:
                int toDrawCount = _manager.MaxHandCard;
                int handCount = _manager.Data.PlayerDataList[(int)_manager.CurrentPlayerIndex].CardList[CardZone.Hand].Count;
                int deckCount = _manager.Data.PlayerDataList[(int)_manager.CurrentPlayerIndex].CardList[CardZone.Deck].Count;
                int handSlot = toDrawCount - handCount;

                drawCount = Mathf.Min(handSlot, deckCount);
                break;
        }
    }
    #endregion

    #region PrePlay Phase 
    public override IEnumerator OnEndPrePlayPhaseTutorial(UnityAction onComplete)
    {
        switch (_manager.SubTurn)
        {
            case 2:

                AddVoiceToList(PlayerIndex.One, "AT_4_004", "DIALOG_AT_004_005", 0f);
                AddVoiceToList(PlayerIndex.One, "AT_4_005", "DIALOG_AT_004_006", 0f);
                _manager.StartCoroutine(PlayVoiceList());
                break;
            case 4:
                AddVoiceToList(PlayerIndex.One, "AT_4_007", "DIALOG_AT_004_008", 0f);
                AddVoiceToList(PlayerIndex.One, "AT_4_008", "DIALOG_AT_004_009", 0f);
                _manager.StartCoroutine(PlayVoiceList());
                break;
            default:
                {
                    if (_playedSoundKeys.Contains("DIALOG_AT_004_014") == false && _manager.SubTurn % 2 == 0)
                    {
                        BattleCardData card;
                        if (_manager.FindBattleCard("C0212", CardZone.Hand, PlayerIndex.One, out card))
                        {
                            _playedSoundKeys.Add("DIALOG_AT_004_014"); 
                            AddVoiceToList(PlayerIndex.One, "AT_4_013", "DIALOG_AT_004_014", 0.3f);
                            AddVoiceToList(PlayerIndex.One, "AT_4_014", "DIALOG_AT_004_015", 0.5f);
                            _manager.StartCoroutine(PlayVoiceList());
                        }
                    }
                }
                break;
        }
        onComplete?.Invoke();
        yield break;
    }

    #endregion

    #region Play Phase

    public override void OnBeginPlayPhaseTutorial()
    {
        switch (_manager.SubTurn)
        {
            case 2:
                {
                    base.PointerPlayerCard(0, true, GameHelper.GetColorByHtmlCode(StoryGameController.HIGHLIGHT_POINTER_GREEN));
                    List<int> highlightBoard = new List<int>() { GameHelper.SlotCodeToSlotID("1A") };
                    ShowHighlightBoard(highlightBoard, Karamucho.UI.BoardSlotHolder.SlotHilightType.Hilight);

                    UnityAction onIdleToSelect = delegate ()
                    {
                        ShowHighlightBoard(highlightBoard, Karamucho.UI.BoardSlotHolder.SlotHilightType.Hilight);
                    };

                    UnityAction onSelectToIdle = delegate () { };
                    UnityAction onSelectToDrag = delegate () { };
                    UnityAction onDragToSelect = delegate () { };

                    UnityAction onDragToIdle = delegate ()
                    {
                        if (_manager.IsEmptyHand(_manager.CurrentPlayerIndex))
                        {
                            HideHighlightBoard(highlightBoard);
                            _manager.StartCoroutine(GameHelper.ExecuteIEnumerator(WaitUntilPlaySoundFinish(), () => { ControllerData.SetIsCanEndTurn(true); }));
                            ControllerData.ClearPlayPhaseEventCallback();
                        }
                    };

                    ControllerData.SetCallbackOnIdleToSelect(onIdleToSelect);
                    ControllerData.SetCallbackOnSelectToIdle(onSelectToIdle);
                    ControllerData.SetCallbackOnSelectToDrag(onSelectToDrag);
                    ControllerData.SetCallbackOnDragToSelect(onDragToSelect);
                    ControllerData.SetCallbackOnDragToIdle(onDragToIdle);

                    ControllerData.SetIsBotCanAction(false);
                    ControllerData.SetIsCanEndTurn(false);
                }
                break;
            case 4:
                {
                    base.PointerPlayerCard(0, true, GameHelper.GetColorByHtmlCode(StoryGameController.HIGHLIGHT_POINTER_GREEN));
                    List<int> highlightBoard = new List<int>() { GameHelper.SlotCodeToSlotID("2A") };
                    ShowHighlightBoard(highlightBoard, Karamucho.UI.BoardSlotHolder.SlotHilightType.Hilight);

                    UnityAction onIdleToSelect = delegate ()
                    {
                        ShowHighlightBoard(highlightBoard, Karamucho.UI.BoardSlotHolder.SlotHilightType.Hilight);
                    };

                    UnityAction onSelectToIdle = delegate () { };
                    UnityAction onSelectToDrag = delegate () { };
                    UnityAction onDragToSelect = delegate () { };

                    UnityAction onDragToIdle = delegate ()
                    {
                        if (_manager.IsEmptyHand(_manager.CurrentPlayerIndex))
                        {
                            HideHighlightBoard(highlightBoard);
                            _manager.GetBoardManager().SetEventOnSelectSlot((slot) =>
                            {
                                if (slot == 1)
                                {
                                    _manager.GetBoardManager().ClearEventOnSelectSlot();
                                    _manager.StartCoroutine(GameHelper.ExecuteIEnumerator(WaitUntilPlaySoundFinish(), () =>
                                    {
                                        _manager.ActionPlayCustomVoice(PlayerIndex.One, "AT_4_009", "DIALOG_AT_004_010", () =>
                                        {
                                            ControllerData.SetIsCanEndTurn(true);
                                            ControllerData.ClearPlayPhaseEventCallback();
                                        }, 1f);
                                    }));
                                    
                                }
                            });
                        }
                    };

                    ControllerData.SetCallbackOnIdleToSelect(onIdleToSelect);
                    ControllerData.SetCallbackOnSelectToIdle(onSelectToIdle);
                    ControllerData.SetCallbackOnSelectToDrag(onSelectToDrag);
                    ControllerData.SetCallbackOnDragToSelect(onDragToSelect);
                    ControllerData.SetCallbackOnDragToIdle(onDragToIdle);

                    ControllerData.SetIsBotCanAction(false);
                    ControllerData.SetIsCanEndTurn(false);
                }
                break;
            default:
                {
                    ControllerData.SetIsBotCanAction(true);
                    _manager.StartCoroutine(GameHelper.ExecuteIEnumerator(WaitUntilPlaySoundFinish(), () => { ControllerData.SetIsCanEndTurn(true); }));
                }
                break;
        }
    }

    public override void OnEndPlayPhaseTutorial(UnityAction onComplete)
    {
        _manager.StartCoroutine(IEOnEndPlayPhaseTutorial(onComplete));
    }

    // before battle.
    private IEnumerator IEOnEndPlayPhaseTutorial(UnityAction onComplete)
    {
        switch (_manager.SubTurn)
        {
            default:
                break;
        }
        onComplete?.Invoke();
        yield break;
    }
    #endregion

    #region Battle Phase

    public override IEnumerator OnEndPreBattlePhaseTutorial(UnityAction onComplete)
    {
        onComplete?.Invoke();
        yield break;
    }

    public override IEnumerator OnPostBattlePhaseTutorial(UnityAction onComplete)
    {
        switch (_manager.SubTurn)
        {
            case 2:
                yield return PlayVoice(PlayerIndex.One, "AT_4_006", "DIALOG_AT_004_007", 1f);
                break;
            case 4:
                yield return PlayVoice(PlayerIndex.One, "AT_4_010", "DIALOG_AT_004_011", 1f);
                yield return PlayVoice(PlayerIndex.One, "AT_4_011", "DIALOG_AT_004_012", 0.5f);
                break;
            case 12:
                break;
            default:
                break;
        }
        onComplete?.Invoke();
        yield break;
    }

    public override void OnResolveDead(List<MinionData> deadTargetList)
    {
        if (_manager.SubTurn > 4)
        {
            CheckFirstKillSoul3(deadTargetList);
            CheckKillMorethan3(deadTargetList);
        }
    }

    private void CheckKillMorethan3(List<MinionData> deadTargetList)
    {
        bool playerTurn = _manager.SubTurn % 2 == 0;
        if (playerTurn == false)
            return;

        List<MinionData> enemyMinions = deadTargetList.FindAll((minion) => minion.Owner != _manager.CurrentPlayerIndex);
        if (enemyMinions.Count < 3)
            return;

        if (playerTurn && _playedSoundKeys.Contains("DIALOG_AT_004_025") == false)
        {
            _playedSoundKeys.Add("DIALOG_AT_004_025");
            _manager.ActionPlayCustomVoice(PlayerIndex.One, "AT_4_021", "DIALOG_AT_004_025", () =>
            {
                _manager.ActionPlayCustomVoice(PlayerIndex.Two, "AT_4_022", "DIALOG_AT_004_026", null);
            });
        }
    }

    private void CheckFirstKillSoul3(List<MinionData> deadTargetList)
    {
        List<MinionData> enemyMinions = deadTargetList.FindAll((minion) => minion.Owner != _manager.CurrentPlayerIndex && minion.SpiritCurrent >= 3);
        if (enemyMinions.Count > 0)
        {
            bool playerTurn = _manager.SubTurn % 2 == 0;
            if (playerTurn && _playedSoundKeys.Contains("DIALOG_AT_004_021") == false)
            {
                _playedSoundKeys.Add("DIALOG_AT_004_021");
                _manager.ActionPlayCustomVoice(PlayerIndex.One, "AT_4_018", "DIALOG_AT_004_021", null, 0.5f);

            }
            else if (playerTurn == false && _playedSoundKeys.Contains("DIALOG_AT_004_019") == false)
            {
                _playedSoundKeys.Add("DIALOG_AT_004_019");
                _manager.ActionPlayCustomVoice(PlayerIndex.Two, "AT_4_022", "DIALOG_AT_004_019", () =>
                {
                    _manager.ActionPlayCustomVoice(PlayerIndex.One, "AT_4_017", "DIALOG_AT_004_020", null, 0.5f);
                });
            }
        }
    }
    #endregion

    #region End Game

    public override void OnPlayPhaseTutorial() { }

    protected override IEnumerator OnPlayEndGameSound(bool isVictory)
    {
        if (isVictory == false)
        {
            yield return PlayVoice(PlayerIndex.One, "AT_4_012", "DIALOG_AT_004_013", 0.2f);
        }
        yield break;
    }

    #endregion

    #endregion
}
