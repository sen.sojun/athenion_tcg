﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AT_003AdventureController : AdventureController
{
    public AT_003AdventureController(GameManager manager) : base(manager)
    {
        _manager.IsShuffleDeck = true;
    }

    public override AdventureStageID StageID => AdventureStageID.AT_003;

    #region Methods
    public override void SetupBot(PlayerIndex playerIndex, GameMode gameMode)
    {
        GameObject obj = new GameObject();

        AdventureBot adventureBot = obj.AddComponent<AT_003AdventureBot>();
        if (adventureBot != null)
        {
            Debug.LogFormat("Create AdventureBot for {0}", playerIndex);
            adventureBot.SetPlayerIndex(playerIndex);
            adventureBot.SetEnable(true);
            ControllerData.SetBot(adventureBot);
        }
    }

    public override bool IsTutorialValidSlotID(int slotID)
    {
        switch (GameManager.Instance.SubTurn)
        {
            case 1:
                return slotID == GameHelper.SlotCodeToSlotID("1D");
            case 3:
                if(_manager.GetHandCount(_manager.CurrentPlayerIndex) == 2)
                    return slotID == GameHelper.SlotCodeToSlotID("1B");
                else
                    return true;
            default: return true;
        }
    }

    public override bool IsCanUseCard(MinionData minion)
    {
        switch (GameManager.Instance.SubTurn)
        {
            case 3:
                {
                    if (GameManager.Instance.GetHandCount(_manager.CurrentPlayerIndex) == 2)
                    {
                        return minion.FullID == "C0113";
                    }
                    else
                    {
                        return base.IsCanUseCard(minion);
                    }
                }
        }

        return base.IsCanUseCard(minion);
    }

    public override bool IsCanShowRedButton(int subturn)
    {
        return subturn > 3;
    }
    #endregion

    #region Game Phase
    public override IEnumerator SetupPhase()
    {
        _manager.SetPlayOrder(PlayerIndex.One, null);
        _manager._isPlayFirstReady = true;

        while (!_manager.IsCanNextPhase || !_manager.IsPhaseReady(GamePhase.SetupData) || !_manager._isPlayFirstReady)
        {
            yield return null;
        }

        _manager.SetRandomSeed(_manager.MatchData.PlayerData[1].RandomSeed);
        _manager.SetLoadingProgress(1.0f);
        NextPhase();

        //_manager.InitDraw(); จั่วมือเริ่ม
        UIManager.Instance.FadeBlackIn();
    }

    protected override IEnumerator MiddleIntroPhase()
    {
        Debug.Log("เปิดมาด้วยสกรีนสีดำ ที่พอ fade ออกไป");
        yield return new WaitForSeconds(1f);

        Debug.Log("ก็เห็น token ของ Player อยู่ในฝั่ง Player โดยที่ Token ของฝั่งศัตรูยังไม่มีบอกว่าเป็นใคร");
        UIManager.Instance.SpotLight_ShowHeroBot(true);
        yield return new WaitForSeconds(1f);

        yield return PlayVoice(PlayerIndex.One, "AT_3_001", "DIALOG_AT_003_001", 1f);
        yield return PlayVoice(PlayerIndex.One, "AT_3_002", "DIALOG_AT_003_002", 1.2f);
        yield return PlayVoice(PlayerIndex.One, "AT_3_003", "DIALOG_AT_003_003", 1f);

        Debug.Log("แล้วก็ปรากฎเป็น token ของ Enemy ลอยอยู่.");
        {
            UIManager.Instance.UIPlayerTop.Intro_State_ShowAtCenter();
            UIManager.Instance.SpotLight_ShowMid(true);
        }
        yield return new WaitForSeconds(1.5f);

        Debug.Log("smash ลงช่อง token ของศัตรู แล้ว fade ดำก็หายไป");
        {
            UIManager.Instance.UIPlayerTop.Intro_State_Default();
            UIManager.Instance.SpotLight_ShowMid(false);
            UIManager.Instance.SpotLight_ShowHeroBot(false);
            UIManager.Instance.FadeBlackOut();
        }

        yield return new WaitForSeconds(0.5f);
        yield return PlayVoice(PlayerIndex.Two, "AT_3_019", "DIALOG_AT_003_004", 0.5f);

        while (!_manager.IsCanNextPhase)
        {
            yield return null;
        }

    }

    public override IEnumerator IEOnEndBeginPhase()
    {
        if (_manager.SubTurn % 2 == 1) // Player turn.
        {
            int enemyHP = _manager.Data.PlayerDataList[(int)PlayerIndex.Two].CurrentHP;
            if (enemyHP - _manager.GetSumSpirit(PlayerIndex.One) <= 0)
            {
                yield return PlayVoice(PlayerIndex.One, "AT_3_014", "DIALOG_AT_003_017", 0.5f);
            }
        }
        yield break;
    }

    public override IEnumerator OnEndSpiritPhaseTutorial(UnityAction onComplete)
    {
        int playerHP = _manager.Data.PlayerDataList[(int)PlayerIndex.One].CurrentHP;
        int enemyHP = _manager.Data.PlayerDataList[(int)PlayerIndex.Two].CurrentHP;

        if (_manager.SubTurn % 2 == 1) // Player turn.
        {
            if (enemyHP <= 5 && !_playedSoundKeys.Contains("DIALOG_AT_003_016"))
            {
                _playedSoundKeys.Add("DIALOG_AT_003_016");
                yield return PlayVoice(PlayerIndex.Two, "AT_3_019", "DIALOG_AT_003_016", 0.5f);
            }
        }
        else  // Enemy turn.
        {
            if (playerHP <= 5 && !_playedSoundKeys.Contains("DIALOG_AT_003_015"))
            {
                _playedSoundKeys.Add("DIALOG_AT_003_015");
                yield return PlayVoice(PlayerIndex.One, "AT_3_013", "DIALOG_AT_003_015", 1f);
            }
        }
        onComplete?.Invoke();
        yield break;
    }

    //Fix draw
    public override IEnumerator AfterAddAP()
    {
        switch (_manager.SubTurn)
        {
            case 1:
                yield return FixedDrawCardToHand("C0091", PlayerIndex.One);
                break;
            case 2:
                yield return FixedDrawCardToHand("C0028", PlayerIndex.Two);
                break;
            case 3:
                AddVoiceToList(PlayerIndex.One, "AT_3_006", "DIALOG_AT_003_008", 0.8f);
                AddVoiceToList(PlayerIndex.One, "AT_3_007", "DIALOG_AT_003_009", 1f);
                _manager.StartCoroutine(PlayVoiceList());
                yield return FixedDrawCardToHand("C0113", PlayerIndex.One);
                yield return FixedDrawCardToHand("C0060", PlayerIndex.One);
                break;
            case 13:
                yield return PlayVoice(PlayerIndex.One, "AT_3_017", "DIALOG_AT_003_021", 0.5f);
                yield return PlayVoice(PlayerIndex.Two, "AT_3_019", "DIALOG_AT_003_022", 0.5f);
                break;
        }
    }

    public override void OnSetDrawCount(int subturn, ref int drawCount, ref bool isDrawAP)
    {
        switch (_manager.SubTurn)
        {
            case 1: drawCount = 0; break;
            case 2: drawCount = 0; break;
            case 3: drawCount = 0; break;
            default:
                int toDrawCount = _manager.MaxHandCard;
                int handCount = _manager.Data.PlayerDataList[(int)_manager.CurrentPlayerIndex].CardList[CardZone.Hand].Count;
                int deckCount = _manager.Data.PlayerDataList[(int)_manager.CurrentPlayerIndex].CardList[CardZone.Deck].Count;
                int handSlot = toDrawCount - handCount;

                drawCount = Mathf.Min(handSlot, deckCount);
                break;
        }
    }

    public override IEnumerator OnEndPrePlayPhaseTutorial(UnityAction onComplete)
    {
        switch (_manager.SubTurn)
        {
            case 2:
                yield return PlayVoice(PlayerIndex.Two, "AT_3_019", "DIALOG_AT_003_007", 0.5f);
                break;
            default:
                if (_manager.SubTurn % 2 == 1 && _manager.FindBattleCard("C0274", CardZone.Hand, PlayerIndex.One, out BattleCardData card) && !_playedSoundKeys.Contains("DIALOG_AT_003_013"))
                {
                    _playedSoundKeys.Add("DIALOG_AT_003_013");
                    AddVoiceToList(PlayerIndex.One, "AT_3_011", "DIALOG_AT_003_013", 0.5f);
                    AddVoiceToList(PlayerIndex.One, "AT_3_012", "DIALOG_AT_003_014", 1f);
                    _manager.StartCoroutine(PlayVoiceList());
                }
                break;
        }
        onComplete?.Invoke();
        yield break;
    }

    public override void OnBeginPlayPhaseTutorial()
    {
        switch (_manager.SubTurn)
        {
            case 1:
                {
                    base.PointerPlayerCard(0, true, GameHelper.GetColorByHtmlCode(StoryGameController.HIGHLIGHT_POINTER_GREEN));
                    List<int> highlightBoard = new List<int>() { GameHelper.SlotCodeToSlotID("1D") };
                    ShowHighlightBoard(highlightBoard, Karamucho.UI.BoardSlotHolder.SlotHilightType.Hilight);

                    bool selectSlotPhase = false;
                    UnityAction onDragToIdle = delegate ()
                    {
                        if (_manager.IsEmptyHand(_manager.CurrentPlayerIndex) && selectSlotPhase == false)
                        {
                            selectSlotPhase = true;
                            HideHighlightBoard(AllSlots);

                            List<int> deactivateSlot = new List<int>(AllSlots);
                            deactivateSlot.Remove(GameHelper.SlotCodeToSlotID("1C"));
                            _manager.GetBoardManager().SetDeactivatedSlotList(deactivateSlot);
                            _manager.GetBoardManager().AddEventOnSelectedComplete((slots) =>
                            {
                                _manager.GetBoardManager().ClearDeactivatedSlotList();
                            });
                            GameHelper.DelayCallback(0.75f, () =>
                            {
                                SetActiveHandPointer(true, base.GetSlotScreenPosition(GameHelper.SlotCodeToSlotID("1C")));
                            });
                            _manager.GetBoardManager().SetEventOnSelectSlot((slot) =>
                            {
                                if (slot == GameHelper.SlotCodeToSlotID("1C"))
                                {
                                    deactivateSlot.Remove(GameHelper.SlotCodeToSlotID("2D"));
                                    _manager.GetBoardManager().SetDeactivatedSlotList(deactivateSlot);
                                    SetActiveHandPointer(true, base.GetSlotScreenPosition(GameHelper.SlotCodeToSlotID("2D")));
                                }
                                else if (slot == GameHelper.SlotCodeToSlotID("2D"))
                                {
                                    SetActiveHandPointer(false, base.GetSlotScreenPosition(GameHelper.SlotCodeToSlotID("2D")));
                                    _manager.GetBoardManager().ClearEventOnSelectSlot();
                                }
                            });
                        }

                        if (_manager.IsEmptyHand(_manager.CurrentPlayerIndex))
                        {
                            _manager.StartCoroutine(GameHelper.ExecuteIEnumerator(WaitUntilPlaySoundFinish(), () => { ControllerData.SetIsCanEndTurn(true); }));
                            ControllerData.ClearPlayPhaseEventCallback();
                        }
                    };

                    ControllerData.SetCallbackOnDragToIdle(onDragToIdle);

                    ControllerData.SetIsBotCanAction(false);
                    ControllerData.SetIsCanEndTurn(false);
                }
                break;
            case 3:
                {
                    base.PointerPlayerCard(0, true, GameHelper.GetColorByHtmlCode(StoryGameController.HIGHLIGHT_POINTER_GREEN));
                    List<int> highlightBoard = new List<int>() { GameHelper.SlotCodeToSlotID("1B") };
                    ShowHighlightBoard(highlightBoard, Karamucho.UI.BoardSlotHolder.SlotHilightType.Hilight);

                    UnityAction onDragToIdle = delegate ()
                    {
                        if (_manager.GetHandCount(_manager.CurrentPlayerIndex) == 1)
                        {
                            HideHighlightBoard(AllSlots);
                            base.PointerPlayerCard(0, true, GameHelper.GetColorByHtmlCode(StoryGameController.HIGHLIGHT_POINTER_GREEN));
                        }
                        else if (_manager.IsEmptyHand(_manager.CurrentPlayerIndex))
                        {
                            HideHighlightBoard(AllSlots);
                            _manager.StartCoroutine(GameHelper.ExecuteIEnumerator(WaitUntilPlaySoundFinish(), () => { ControllerData.SetIsCanEndTurn(true); }));
                            ControllerData.ClearPlayPhaseEventCallback();
                        }
                    };
                    ControllerData.SetCallbackOnDragToIdle(onDragToIdle);

                    ControllerData.SetIsBotCanAction(false);
                    ControllerData.SetIsCanEndTurn(false);
                }
                break;
            default:
                {
                    ControllerData.SetIsBotCanAction(true);
                    _manager.StartCoroutine(GameHelper.ExecuteIEnumerator(WaitUntilPlaySoundFinish(), () => { ControllerData.SetIsCanEndTurn(true); }));
                }
                break;
        }
    }

    public override void OnPlayPhaseTutorial()
    {
    }

    public override void OnEndPlayPhaseTutorial(UnityAction onComplete)
    {
        onComplete?.Invoke();
    }

    public override IEnumerator OnEndPreBattlePhaseTutorial(UnityAction onComplete)
    {
        switch (_manager.SubTurn)
        {
            case 1:
                yield return PlayVoice(PlayerIndex.One, "AT_3_004", "DIALOG_AT_003_005", 1f);
                yield return PlayVoice(PlayerIndex.One, "AT_3_005", "DIALOG_AT_003_006", 1f);
                break;
            case 3:
                _manager.StartCoroutine(GameHelper.ExecuteIEnumerator(WaitUntilPlaySoundFinish(), () =>
                {
                    AddVoiceToList(PlayerIndex.One, "AT_3_008", "DIALOG_AT_003_010", 1f);
                    AddVoiceToList(PlayerIndex.One, "AT_3_009", "DIALOG_AT_003_011", 1f);
                    _manager.StartCoroutine(PlayVoiceList());
                }));
                
                break;
            default:
                break;
        }
        onComplete?.Invoke();
        yield break;
    }

    public override IEnumerator OnPostBattlePhaseTutorial(UnityAction onComplete)
    {
        int amount = 0;
        foreach (SlotData slot in _manager.Data.SlotDatas)
        {
            if (!slot.IsBeUnlock)
                amount++;
        }
        if (amount > 2 && !_playedSoundKeys.Contains("DIALOG_AT_003_023"))
        {
            _playedSoundKeys.Add("DIALOG_AT_003_023");
            yield return PlayVoice(PlayerIndex.One, "AT_3_018", "DIALOG_AT_003_023", 1f);
            yield return PlayVoice(PlayerIndex.Two, "AT_3_019", "DIALOG_AT_003_024", 0.5f);
        }

        onComplete?.Invoke();
        yield break;
    }

    protected override IEnumerator OnPlayEndGameSound(bool isVictory)
    {
        if (isVictory == false)
        {
            yield return PlayVoice(PlayerIndex.One, "AT_3_010", "DIALOG_AT_003_012", 0.8f);
        }
        yield break;
    }

    public override void OnResolveDead(List<MinionData> deadTargetList)
    {
        if (_manager.SubTurn > 3)
        {
            List<MinionData> enemyMinions = deadTargetList.FindAll((minion) => minion.Owner != _manager.CurrentPlayerIndex && minion.SpiritCurrent >= 3);
            if (enemyMinions.Count == 0)
                return;
            bool playerTurn = _manager.SubTurn % 2 == 1;
            if (playerTurn && _playedSoundKeys.Contains("DIALOG_AT_003_020") == false)
            {
                _playedSoundKeys.Add("DIALOG_AT_003_020");
                _manager.ActionPlayCustomVoice(PlayerIndex.One, "AT_3_016", "DIALOG_AT_003_020", null, 0.5f);

            }
            else if (playerTurn == false && _playedSoundKeys.Contains("DIALOG_AT_003_018") == false)
            {
                _playedSoundKeys.Add("DIALOG_AT_003_018");
                _manager.ActionPlayCustomVoice(PlayerIndex.Two, "AT_3_019", "DIALOG_AT_003_018", 
                    () =>
                    {
                        _manager.ActionPlayCustomVoice(PlayerIndex.One, "AT_3_015", "DIALOG_AT_003_019", null, 0.8f);
                    });
            }
        }
    }
    #endregion
}
