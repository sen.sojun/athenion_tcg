﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AT_005AdventureController : AdventureController
{
    public override AdventureStageID StageID => AdventureStageID.AT_005;
    private bool isFinishPlayVoice = false;

    public AT_005AdventureController(GameManager manager) : base(manager)
    {
        _manager.IsShuffleDeck = true;
    }


    #region Methods

    public override void SetupBot(PlayerIndex playerIndex, GameMode gameMode)
    {
        GameObject obj = new GameObject();

        AdventureBot adventureBot = obj.AddComponent<AT_005AdventureBot>();
        if (adventureBot != null)
        {
            Debug.LogFormat("Create AdventureBot for {0}", playerIndex);
            adventureBot.SetPlayerIndex(playerIndex);
            //bot.SetTutorialMode(gameMode);

            //this._onShowSpiritAttackOnPlayerComplete += bot.OnSpiritAttackOnPlayer;
            //this._onPlayerPlayHandCardComplete += bot.OnPlayHandCard;

            adventureBot.SetEnable(true);
            ControllerData.SetBot(adventureBot);
        }
    }

    public override bool IsCanShowRedButton(int subturn)
    {
        return subturn > 4;
    }

    public override bool IsCanUseCard(MinionData minion)
    {
        switch (_manager.SubTurn)
        {
            case 1:
                {
                    if (_manager.GetHandCount(PlayerIndex.One) == 2)
                    {
                        return minion.BaseID == "C0019";
                    }
                    else if (_manager.GetHandCount(PlayerIndex.One) == 1)
                    {
                        return minion.BaseID == "C0046" && isFinishPlayVoice;
                    }
                    else
                    {
                        return base.IsCanUseCard(minion);
                    }
                }
        }

        return base.IsCanUseCard(minion);
    }

    public override bool IsTutorialValidSlotID(int slotID)
    {
        if (GameManager.Instance.Phase == GamePhase.Play)
        {
            switch (GameManager.Instance.SubTurn)
            {
                case 1:
                    {
                        if (_manager.GetHandCount(PlayerIndex.One) == 2)
                        {
                            return slotID == GameHelper.SlotCodeToSlotID("1D");
                        }
                        else if (_manager.GetHandCount(PlayerIndex.One) == 1)
                        {
                            return slotID == GameHelper.SlotCodeToSlotID("2D") && isFinishPlayVoice;
                        }
                        else
                            return true;
                    }
                case 3:
                    return slotID == GameHelper.SlotCodeToSlotID("1C");
                default: return true;
            }
        }
        return true;
    }

    #endregion

    #region Game Phase

    #region SetupPhase

    public override IEnumerator SetupPhase()
    {
        _manager.SetPlayOrder(PlayerIndex.One, null);
        _manager._isPlayFirstReady = true;

        while (!_manager.IsCanNextPhase || !_manager.IsPhaseReady(GamePhase.SetupData) || !_manager._isPlayFirstReady)
        {
            yield return null;
        }

        _manager.SetRandomSeed(_manager.MatchData.PlayerData[1].RandomSeed);
        _manager.SetLoadingProgress(1.0f);
        NextPhase();

        UIManager.Instance.FadeBlackIn();
    }

    #endregion

    #region Intro Phase

    protected override IEnumerator MiddleIntroPhase()
    {
        Debug.Log("เปิดมาด้วยสกรีนสีดำ ที่พอ fade ออกไป");
        yield return new WaitForSeconds(1f);

        Debug.Log("ก็เห็น token ของ Nyx อยู่ในฝั่ง Player โดยที่ Token ของฝั่งศัตรูยังไม่มีบอกว่าเป็นใคร");
        UIManager.Instance.SpotLight_ShowHeroBot(true);
        yield return new WaitForSeconds(1f);

        yield return PlayVoice(PlayerIndex.One, "AT_5_001", "DIALOG_AT_005_001", 0.5f);
        yield return PlayVoice(PlayerIndex.One, "AT_5_002", "DIALOG_AT_005_002", 0.8f);
        yield return PlayVoice(PlayerIndex.One, "AT_5_003", "DIALOG_AT_005_003", 0.8f);

        Debug.Log("แล้วก็ปรากฎเป็น token ของ Enemy ลอยอยู่.");
        {
            UIManager.Instance.UIPlayerTop.Intro_State_ShowAtCenter();
            UIManager.Instance.SpotLight_ShowMid(true);
        }
        yield return new WaitForSeconds(1.5f);

        Debug.Log("smash ลงช่อง token ของศัตรู แล้ว fade ดำก็หายไป");
        {
            UIManager.Instance.UIPlayerTop.Intro_State_Default();
            UIManager.Instance.SpotLight_ShowMid(false);
            UIManager.Instance.SpotLight_ShowHeroBot(false);
            UIManager.Instance.FadeBlackOut();
        }

        yield return new WaitForSeconds(0.5f);
        yield return PlayVoice(PlayerIndex.Two, "AT_5_021", "DIALOG_AT_005_004", 0.2f);

        while (!_manager.IsCanNextPhase)
        {
            yield return null;
        }

    }

    #endregion

    #region Begin Phase

    public override IEnumerator IEOnEndBeginPhase()
    {
        if (_manager.SubTurn % 2 == 1) // Player turn.
        {
            int enemyHP = _manager.Data.PlayerDataList[(int)PlayerIndex.Two].CurrentHP;
            if (enemyHP - _manager.GetSumSpirit(PlayerIndex.One) <= 0)
            {
                yield return PlayVoice(PlayerIndex.One, "AT_5_016", "DIALOG_AT_005_018", 0.8f);
            }
        }
        yield break;
    }

    #endregion

    #region Spirit Phase

    public override IEnumerator OnEndSpiritPhaseTutorial(UnityAction onComplete)
    {
        int playerHP = _manager.Data.PlayerDataList[(int)PlayerIndex.One].CurrentHP;
        int enemyHP = _manager.Data.PlayerDataList[(int)PlayerIndex.Two].CurrentHP;

        if (_manager.SubTurn % 2 == 1) // Player turn.
        {
            if (enemyHP <= 5)
            {
                if (_playedSoundKeys.Contains("DIALOG_AT_005_017") == false)
                {
                    _playedSoundKeys.Add("DIALOG_AT_005_017");
                    yield return PlayVoice(PlayerIndex.Two, "AT_5_021", "DIALOG_AT_005_017", 0.5f);
                }
            }
        }
        else  // Enemy turn.
        {
            if (playerHP <= 5)
            {
                if (_playedSoundKeys.Contains("DIALOG_AT_005_016") == false)
                {
                    _playedSoundKeys.Add("DIALOG_AT_005_016");
                    yield return PlayVoice(PlayerIndex.One, "AT_5_015", "DIALOG_AT_005_016", 0.5f);
                }
            }
        }
        onComplete?.Invoke();
        yield break;
    }

    #endregion

    #region Draw Phase

    public override IEnumerator AfterAddAP()
    {
        switch (_manager.SubTurn)
        {
            case 1:
                yield return FixedDrawCardToHand("C0019", PlayerIndex.One);
                yield return FixedDrawCardToHand("C0046", PlayerIndex.One);
                break;
            case 2:
                yield return FixedDrawCardToHand("C0073", PlayerIndex.Two);
                break;
            case 3:
                yield return FixedDrawCardToHand("C0018", PlayerIndex.One);
                break;
            case 13:
                yield return PlayVoice(PlayerIndex.Two, "AT_5_021", "DIALOG_AT_005_022", 0.8f);
                yield return PlayVoice(PlayerIndex.One, "AT_5_019", "DIALOG_AT_005_023", 0.5f);
                break;
            default:
                break;
        }
        yield break;
    }

    public override void OnSetDrawCount(int subturn, ref int drawCount, ref bool isDrawAP)
    {
        isDrawAP = false;
        switch (_manager.SubTurn)
        {
            case 1: drawCount = 0; break;
            case 2: drawCount = 0; break;
            case 3: drawCount = 0; break;
            default:
                int toDrawCount = _manager.MaxHandCard;
                int handCount = _manager.Data.PlayerDataList[(int)_manager.CurrentPlayerIndex].CardList[CardZone.Hand].Count;
                int deckCount = _manager.Data.PlayerDataList[(int)_manager.CurrentPlayerIndex].CardList[CardZone.Deck].Count;
                int handSlot = toDrawCount - handCount;

                drawCount = Mathf.Min(handSlot, deckCount);
                break;
        }
    }
    #endregion

    #region PrePlay Phase 
    public override IEnumerator OnEndPrePlayPhaseTutorial(UnityAction onComplete)
    {
        switch (_manager.SubTurn)
        {
            case 1:
                yield return PlayVoice(PlayerIndex.One, "AT_5_004", "DIALOG_AT_005_005", 0.8f);
                yield return PlayVoice(PlayerIndex.One, "AT_5_005", "DIALOG_AT_005_006", 1f);
                break;
            case 3:
                AddVoiceToList(PlayerIndex.One, "AT_5_008", "DIALOG_AT_005_009", 0f);
                AddVoiceToList(PlayerIndex.One, "AT_5_009", "DIALOG_AT_005_010", 0f);
                _manager.StartCoroutine(PlayVoiceList());
                break;
            default:
                {
                    if (_playedSoundKeys.Contains("DIALOG_AT_005_014") == false && _manager.SubTurn % 2 == 1)
                    {
                        BattleCardData card;
                        if (_manager.FindBattleCard("C0161", CardZone.Hand, PlayerIndex.One, out card))
                        {
                            _playedSoundKeys.Add("DIALOG_AT_005_014");
                            AddVoiceToList(PlayerIndex.One, "AT_5_013", "DIALOG_AT_005_014", 0f);
                            AddVoiceToList(PlayerIndex.One, "AT_5_014", "DIALOG_AT_005_015", 0f);
                            _manager.StartCoroutine(PlayVoiceList());
                        }
                    }
                }
                break;
        }
        onComplete?.Invoke();
        yield break;
    }

    #endregion

    #region Play Phase

    public override void OnBeginPlayPhaseTutorial()
    {
        switch (_manager.SubTurn)
        {
            case 1:
                {
                    Action UpdatePointer = delegate ()
                    {
                        if (_manager.GetHandCount(PlayerIndex.One) == 2 ||
                            (_manager.GetHandCount(PlayerIndex.One) == 1 && isFinishPlayVoice))
                        {
                            base.PointerPlayerCard(0, true, GameHelper.GetColorByHtmlCode(StoryGameController.HIGHLIGHT_POINTER_GREEN));
                        }
                    };

                    Action UpdateSlotHighlight = delegate ()
                    {
                        int handCount = _manager.GetHandCount(PlayerIndex.One);
                        if (handCount == 2)
                        {
                            HideHighlightBoard(AllSlots);
                            List<int> slots = new List<int>() { GameHelper.SlotCodeToSlotID("1D") };
                            ShowHighlightBoard(slots, Karamucho.UI.BoardSlotHolder.SlotHilightType.Hilight);
                        }
                        else if (handCount == 1)
                        {
                            if (_playedSoundKeys.Contains("DIALOG_AT_005_007") == false)
                            {
                                HideHighlightBoard(AllSlots);
                                _playedSoundKeys.Add("DIALOG_AT_005_007");
                                _manager.ActionPlayCustomVoice(PlayerIndex.One, "AT_5_006", "DIALOG_AT_005_007", () =>
                                {
                                }, 0f);
                                isFinishPlayVoice = true;
                                UpdatePointer();
                                List<int> slots = new List<int>() { GameHelper.SlotCodeToSlotID("2D") };
                                ShowHighlightBoard(slots, Karamucho.UI.BoardSlotHolder.SlotHilightType.Hilight);
                            }
                        }
                    };

                    UpdatePointer();
                    UpdateSlotHighlight();

                    UnityAction onDragToIdle = delegate ()
                    {
                        UpdatePointer();
                        UpdateSlotHighlight();

                        if (_manager.IsEmptyHand(_manager.CurrentPlayerIndex))
                        {
                            HideHighlightBoard(AllSlots);
                            ControllerData.SetIsCanEndTurn(true);
                            ControllerData.ClearPlayPhaseEventCallback();
                        }
                    };

                    ControllerData.SetCallbackOnDragToIdle(onDragToIdle);

                    ControllerData.SetIsBotCanAction(false);
                    ControllerData.SetIsCanEndTurn(false);
                }
                break;
            case 3:
                {
                    base.PointerPlayerCard(0, true, GameHelper.GetColorByHtmlCode(StoryGameController.HIGHLIGHT_POINTER_GREEN));
                    List<int> highlightBoard = new List<int>() { GameHelper.SlotCodeToSlotID("1C") };
                    ShowHighlightBoard(highlightBoard, Karamucho.UI.BoardSlotHolder.SlotHilightType.Hilight);

                    UnityAction onDragToIdle = delegate ()
                    {
                        if (_manager.IsEmptyHand(_manager.CurrentPlayerIndex))
                        {
                            HideHighlightBoard(highlightBoard);
                            _manager.StartCoroutine(GameHelper.ExecuteIEnumerator(WaitUntilPlaySoundFinish(), () => { ControllerData.SetIsCanEndTurn(true); }));
                            ControllerData.ClearPlayPhaseEventCallback();
                        }
                    };

                    ControllerData.SetCallbackOnDragToIdle(onDragToIdle);

                    ControllerData.SetIsBotCanAction(false);
                    ControllerData.SetIsCanEndTurn(false);
                }
                break;
            default:
                {
                    ControllerData.SetIsBotCanAction(true);
                    _manager.StartCoroutine(GameHelper.ExecuteIEnumerator(WaitUntilPlaySoundFinish(), () => { ControllerData.SetIsCanEndTurn(true); }));
                }
                break;
        }
    }

    public override void OnEndPlayPhaseTutorial(UnityAction onComplete)
    {
        _manager.StartCoroutine(IEOnEndPlayPhaseTutorial(onComplete));
    }

    // before battle.
    private IEnumerator IEOnEndPlayPhaseTutorial(UnityAction onComplete)
    {
        switch (_manager.SubTurn)
        {
            default:
                break;
        }
        onComplete?.Invoke();
        yield break;
    }
    #endregion

    #region Battle Phase

    public override IEnumerator OnEndPreBattlePhaseTutorial(UnityAction onComplete)
    {
        if (_manager.SubTurn > 4 && _manager.SubTurn % 2 == 1)
        {
            if (_playedSoundKeys.Contains("DIALOG_AT_005_024") == false)
            {
                int auraUnitAmount = FindAuraAmount();
                if (auraUnitAmount > 2)
                {
                    _playedSoundKeys.Add("DIALOG_AT_005_024");
                    yield return PlayVoice(PlayerIndex.One, "AT_5_020", "DIALOG_AT_005_024", 0.3f);
                }
            }
        }

        onComplete?.Invoke();
        yield break;
    }

    private int FindAuraAmount()
    {
        int amount = 0;
        List<MinionData> minionDatas = _manager.Data.GetAllMinionsInSlot();
        for (int i = 0; i < minionDatas.Count; i++)
        {
            if (minionDatas[i].Owner == PlayerIndex.One && minionDatas[i].IsAbilityAura)
            {
                amount++;
            }
        }
        return amount;
    }

    public override IEnumerator OnPostBattlePhaseTutorial(UnityAction onComplete)
    {
        switch (_manager.SubTurn)
        {
            case 1:
                yield return PlayVoice(PlayerIndex.One, "AT_5_007", "DIALOG_AT_005_008", 1f);
                break;
            case 3:
                yield return PlayVoice(PlayerIndex.One, "AT_5_010", "DIALOG_AT_005_011", 1f);
                yield return PlayVoice(PlayerIndex.One, "AT_5_011", "DIALOG_AT_005_012", 1f);
                break;
            default:
                break;
        }
        onComplete?.Invoke();
        yield break;
    }

    public override void OnResolveDead(List<MinionData> deadTargetList)
    {
        if (_manager.SubTurn > 4)
        {
            List<MinionData> enemyMinions = deadTargetList.FindAll((minion) => minion.Owner != _manager.CurrentPlayerIndex && minion.SpiritCurrent >= 3);
            if (enemyMinions.Count == 0)
                return;
            bool playerTurn = _manager.SubTurn % 2 == 1;
            if (playerTurn && _playedSoundKeys.Contains("DIALOG_AT_005_021") == false)
            {
                _playedSoundKeys.Add("DIALOG_AT_005_021");
                _manager.ActionPlayCustomVoice(PlayerIndex.One, "AT_5_018", "DIALOG_AT_005_021", null, 0.3f);

            }
            else if (playerTurn == false && _playedSoundKeys.Contains("DIALOG_AT_005_019") == false)
            {
                _playedSoundKeys.Add("DIALOG_AT_005_019");
                _manager.ActionPlayCustomVoice(PlayerIndex.Two, "AT_5_021", "DIALOG_AT_005_019", () =>
                {
                    _manager.ActionPlayCustomVoice(PlayerIndex.One, "AT_5_017", "DIALOG_AT_005_020", null, 0.5f);
                });
            }
        }
    }
    #endregion

    #region End Game

    public override void OnPlayPhaseTutorial() { }

    protected override IEnumerator OnPlayEndGameSound(bool isVictory)
    {
        if (isVictory == false)
        {
            yield return PlayVoice(PlayerIndex.One, "AT_5_012", "DIALOG_AT_005_013", 0.2f);
        }
        yield break;
    }

    #endregion

    #endregion
}
