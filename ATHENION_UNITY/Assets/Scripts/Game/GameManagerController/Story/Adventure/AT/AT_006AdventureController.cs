﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AT_006AdventureController : AdventureController
{
    public override AdventureStageID StageID => AdventureStageID.AT_006;
    //TODO: implement using key instead.
    private List<string> _soundKeys = new List<string>();
    private bool isPlayVoiceSubturn4Complete = false;

    public AT_006AdventureController(GameManager manager) : base(manager)
    {
        _manager.IsShuffleDeck = true;
    }


    #region Methods

    public override void SetupBot(PlayerIndex playerIndex, GameMode gameMode)
    {
        GameObject obj = new GameObject();

        AdventureBot adventureBot = obj.AddComponent<AT_006AdventureBot>();
        if (adventureBot != null)
        {
            Debug.LogFormat("Create AdventureBot for {0}", playerIndex);
            adventureBot.SetPlayerIndex(playerIndex);
            //bot.SetTutorialMode(gameMode);

            //this._onShowSpiritAttackOnPlayerComplete += bot.OnSpiritAttackOnPlayer;
            //this._onPlayerPlayHandCardComplete += bot.OnPlayHandCard;

            adventureBot.SetEnable(true);
            ControllerData.SetBot(adventureBot);
        }
    }

    public override bool IsCanShowRedButton(int subturn)
    {
        return subturn > 4;
    }

    public override bool IsCanUseCard(MinionData minion)
    {
        switch (_manager.SubTurn)
        {
            case 4:
                {
                    if (_manager.GetHandCount(PlayerIndex.One) == 2)
                    {
                        return minion.BaseID == "C0036";
                    }
                    else if (_manager.GetHandCount(PlayerIndex.One) == 1 && isPlayVoiceSubturn4Complete)
                    {
                        return minion.BaseID == "C0130";
                    }
                    else
                    {
                        return base.IsCanUseCard(minion);
                    }
                }
        }

        return base.IsCanUseCard(minion);
    }

    public override bool IsTutorialValidSlotID(int slotID)
    {
        if (GameManager.Instance.Phase == GamePhase.Play)
        {
            switch (GameManager.Instance.SubTurn)
            {
                case 2:
                    return slotID == GameHelper.SlotCodeToSlotID("3C");
                case 4:
                    if (_manager.GetHandCount(PlayerIndex.One) == 2)
                        return slotID == GameHelper.SlotCodeToSlotID("2A");
                    else if (_manager.GetHandCount(PlayerIndex.One) == 1)
                        return slotID == GameHelper.SlotCodeToSlotID("2D");
                    else
                        return true;
                default: return true;
            }
        }
        return true;
    }

    #endregion

    #region Game Phase

    #region SetupPhase

    public override IEnumerator SetupPhase()
    {
        _manager.SetPlayOrder(PlayerIndex.Two, null);
        _manager._isPlayFirstReady = true;

        while (!_manager.IsCanNextPhase || !_manager.IsPhaseReady(GamePhase.SetupData) || !_manager._isPlayFirstReady)
        {
            yield return null;
        }

        _manager.SetRandomSeed(_manager.MatchData.PlayerData[1].RandomSeed);
        _manager.SetLoadingProgress(1.0f);
        NextPhase();

        UIManager.Instance.FadeBlackIn();
    }

    #endregion

    #region Intro Phase

    protected override IEnumerator MiddleIntroPhase()
    {
        Debug.Log("เปิดมาด้วยสกรีนสีดำ ที่พอ fade ออกไป");
        yield return new WaitForSeconds(1f);

        Debug.Log("ก็เห็น token ของ Nyx อยู่ในฝั่ง Player โดยที่ Token ของฝั่งศัตรูยังไม่มีบอกว่าเป็นใคร");
        UIManager.Instance.SpotLight_ShowHeroBot(true);
        yield return new WaitForSeconds(1f);

        yield return PlayVoice(PlayerIndex.One, "AT_6_001", "DIALOG_AT_006_001", 0.5f);
        yield return PlayVoice(PlayerIndex.One, "AT_6_002", "DIALOG_AT_006_002", 0.8f);
        yield return PlayVoice(PlayerIndex.One, "AT_6_003", "DIALOG_AT_006_003", 0.5f);


        Debug.Log("แล้วก็ปรากฎเป็น token ของ Enemy ลอยอยู่.");
        {
            UIManager.Instance.UIPlayerTop.Intro_State_ShowAtCenter();
            UIManager.Instance.SpotLight_ShowMid(true);
        }
        yield return new WaitForSeconds(1.5f);

        Debug.Log("smash ลงช่อง token ของศัตรู แล้ว fade ดำก็หายไป");
        {
            UIManager.Instance.UIPlayerTop.Intro_State_Default();
            UIManager.Instance.SpotLight_ShowMid(false);
            UIManager.Instance.SpotLight_ShowHeroBot(false);
            UIManager.Instance.FadeBlackOut();
        }

        yield return new WaitForSeconds(0.5f);

        while (!_manager.IsCanNextPhase)
        {
            yield return null;
        }

    }

    #endregion

    #region Begin Phase

    public override IEnumerator IEOnEndBeginPhase()
    {
        if (_manager.SubTurn % 2 == 0) // Player turn.
        {
            int enemyHP = _manager.Data.PlayerDataList[(int)PlayerIndex.Two].CurrentHP;
            if (enemyHP - _manager.GetSumSpirit(PlayerIndex.One) <= 0)
            {
                yield return PlayVoice(PlayerIndex.One, "AT_6_016", "DIALOG_AT_006_018", 0.3f);
            }
        }
        yield break;
    }

    #endregion

    #region Spirit Phase

    public override IEnumerator OnEndSpiritPhaseTutorial(UnityAction onComplete)
    {
        int playerHP = _manager.Data.PlayerDataList[(int)PlayerIndex.One].CurrentHP;
        int enemyHP = _manager.Data.PlayerDataList[(int)PlayerIndex.Two].CurrentHP;

        if (_manager.SubTurn % 2 == 0) // Player turn.
        {
            if (enemyHP <= 5)
            {
                if (_soundKeys.Contains("DIALOG_AT_006_017") == false)
                {
                    _soundKeys.Add("DIALOG_AT_006_017");
                    yield return PlayVoice(PlayerIndex.Two, "AT_6_021", "DIALOG_AT_006_017", 0.3f);
                }
            }
        }
        else  // Enemy turn.
        {
            if (playerHP <= 5)
            {
                if (_soundKeys.Contains("DIALOG_AT_006_016") == false)
                {
                    _soundKeys.Add("DIALOG_AT_006_016");
                    yield return PlayVoice(PlayerIndex.One, "AT_6_015", "DIALOG_AT_006_016", 0.3f);
                }
            }
        }
        onComplete?.Invoke();
        yield break;
    }

    #endregion

    #region Draw Phase

    public override IEnumerator AfterAddAP()
    {
        switch (_manager.SubTurn)
        {
            case 1:
                yield return FixedDrawCardToHand("C0046", PlayerIndex.Two);
                break;
            case 2:
                yield return FixedDrawCardToHand("C0073", PlayerIndex.One);
                break;
            case 3:
                yield return FixedDrawCardToHand("C0030", PlayerIndex.Two);
                break;
            case 4:
                yield return FixedDrawCardToHand("C0036", PlayerIndex.One);
                yield return FixedDrawCardToHand("C0130", PlayerIndex.One);
                break;
            case 12:
                yield return PlayVoice(PlayerIndex.Two, "AT_6_023", "DIALOG_AT_006_021", 0.3f);
                yield return PlayVoice(PlayerIndex.One, "AT_6_018", "DIALOG_AT_006_022", 0.5f);
                break;
            default:
                break;
        }
        yield break;
    }

    public override void OnSetDrawCount(int subturn, ref int drawCount, ref bool isDrawAP)
    {
        isDrawAP = false;
        switch (_manager.SubTurn)
        {
            case 1: drawCount = 0; break;
            case 2: drawCount = 0; break;
            case 3: drawCount = 0; break;
            case 4: drawCount = 0; break;
            default:
                int toDrawCount = _manager.MaxHandCard;
                int handCount = _manager.Data.PlayerDataList[(int)_manager.CurrentPlayerIndex].CardList[CardZone.Hand].Count;
                int deckCount = _manager.Data.PlayerDataList[(int)_manager.CurrentPlayerIndex].CardList[CardZone.Deck].Count;
                int handSlot = toDrawCount - handCount;

                drawCount = Mathf.Min(handSlot, deckCount);
                break;
        }
    }
    #endregion

    #region PrePlay Phase 
    public override IEnumerator OnEndPrePlayPhaseTutorial(UnityAction onComplete)
    {
        switch (_manager.SubTurn)
        {
            case 2:
                AddVoiceToList(PlayerIndex.One, "AT_6_004", "DIALOG_AT_006_005", 0f);
                AddVoiceToList(PlayerIndex.One, "AT_6_005", "DIALOG_AT_006_006", 0f);
                _manager.StartCoroutine(PlayVoiceList()); 
                break;
            case 4:
                AddVoiceToList(PlayerIndex.One, "AT_6_007", "DIALOG_AT_006_008", 0f);
                AddVoiceToList(PlayerIndex.One, "AT_6_008", "DIALOG_AT_006_009", 0f);
                _manager.StartCoroutine(PlayVoiceList()); 
                break;
            default:
                {
                    if (_soundKeys.Contains("DIALOG_AT_006_014") == false && _manager.SubTurn % 2 == 0)
                    {
                        BattleCardData card;
                        if (_manager.FindBattleCard("C0241", CardZone.Hand, PlayerIndex.One, out card))
                        {
                            _soundKeys.Add("DIALOG_AT_006_014"); 
                            AddVoiceToList(PlayerIndex.One, "AT_6_013", "DIALOG_AT_006_014", 0f);
                            AddVoiceToList(PlayerIndex.One, "AT_6_014", "DIALOG_AT_006_015", 0f);
                            _manager.StartCoroutine(PlayVoiceList()); 
                        }
                    }
                }
                break;
        }
        onComplete?.Invoke();
        yield break;
    }

    #endregion

    #region Play Phase

    public override void OnBeginPlayPhaseTutorial()
    {
        switch (_manager.SubTurn)
        {
            case 2:
                {
                    base.PointerPlayerCard(0, true, GameHelper.GetColorByHtmlCode(StoryGameController.HIGHLIGHT_POINTER_GREEN));
                    List<int> highlightBoard = new List<int>() { GameHelper.SlotCodeToSlotID("3C") };
                    ShowHighlightBoard(highlightBoard, Karamucho.UI.BoardSlotHolder.SlotHilightType.Hilight);

                    UnityAction onIdleToSelect = delegate ()
                    {
                        ShowHighlightBoard(highlightBoard, Karamucho.UI.BoardSlotHolder.SlotHilightType.Hilight);
                    };

                    UnityAction onSelectToIdle = delegate () { };
                    UnityAction onSelectToDrag = delegate () { };
                    UnityAction onDragToSelect = delegate () { };

                    UnityAction onDragToIdle = delegate ()
                    {
                        if (_manager.IsEmptyHand(_manager.CurrentPlayerIndex))
                        {
                            HideHighlightBoard(highlightBoard);
                            _manager.StartCoroutine(GameHelper.ExecuteIEnumerator(WaitUntilPlaySoundFinish(), () => { ControllerData.SetIsCanEndTurn(true); }));
                            ControllerData.ClearPlayPhaseEventCallback();
                        }
                    };

                    ControllerData.SetCallbackOnIdleToSelect(onIdleToSelect);
                    ControllerData.SetCallbackOnSelectToIdle(onSelectToIdle);
                    ControllerData.SetCallbackOnSelectToDrag(onSelectToDrag);
                    ControllerData.SetCallbackOnDragToSelect(onDragToSelect);
                    ControllerData.SetCallbackOnDragToIdle(onDragToIdle);

                    ControllerData.SetIsBotCanAction(false);
                    ControllerData.SetIsCanEndTurn(false);
                }
                break;
            case 4:
                {
                    Action UpdatePointer = delegate ()
                    {
                        if (_manager.GetHandCount(PlayerIndex.One) > 0)
                        {
                            base.PointerPlayerCard(0, true, GameHelper.GetColorByHtmlCode(StoryGameController.HIGHLIGHT_POINTER_GREEN));
                        }
                    };

                    Action UpdateSlotHighlight = delegate ()
                    { 
                        HideHighlightBoard(AllSlots);
                        int handCount = _manager.GetHandCount(PlayerIndex.One);
                        if (handCount == 2)
                        {
                            List<int> slots = new List<int>() { GameHelper.SlotCodeToSlotID("2A") };
                            ShowHighlightBoard(slots, Karamucho.UI.BoardSlotHolder.SlotHilightType.Hilight);
                        }
                        else if (handCount == 1)
                        {
                            List<int> slots = new List<int>() { GameHelper.SlotCodeToSlotID("2D") };
                            ShowHighlightBoard(slots, Karamucho.UI.BoardSlotHolder.SlotHilightType.Hilight);
                        }
                    };

                    UpdatePointer();
                    UpdateSlotHighlight();

                    bool selectSlotPhase = false;
                    UnityAction onDragToIdle = delegate ()
                    {
                        if (_manager.GetHandCount(PlayerIndex.One) == 1 && selectSlotPhase == false)
                        {
                            HideHighlightBoard(AllSlots);

                            selectSlotPhase = true;
                            List<int> deactivateSlot = new List<int>(AllSlots);
                            deactivateSlot.Remove(GameHelper.SlotCodeToSlotID("2D"));
                            _manager.GetBoardManager().SetDeactivatedSlotList(deactivateSlot);
                            _manager.GetBoardManager().AddEventOnSelectedComplete((slots) =>
                            {
                                _manager.GetBoardManager().ClearDeactivatedSlotList();
                                UpdateSlotHighlight();
                            });
                            GameHelper.DelayCallback(0.75f, () =>
                            {
                                SetActiveHandPointer(true, base.GetSlotScreenPosition(GameHelper.SlotCodeToSlotID("2D")));
                            });
                            _manager.GetBoardManager().SetEventOnSelectSlot((slot) =>
                            { 
                                if (slot == GameHelper.SlotCodeToSlotID("2D"))
                                {
                                    deactivateSlot.Remove(GameHelper.SlotCodeToSlotID("3D"));
                                    _manager.GetBoardManager().SetDeactivatedSlotList(deactivateSlot);
                                    SetActiveHandPointer(true, base.GetSlotScreenPosition(GameHelper.SlotCodeToSlotID("3D")));
                                }
                                else if (slot == GameHelper.SlotCodeToSlotID("3D"))
                                {
                                    SetActiveHandPointer(false, base.GetSlotScreenPosition(GameHelper.SlotCodeToSlotID("3D")));
                                    _manager.ActionPlayCustomVoice(PlayerIndex.One, "AT_6_009", "DIALOG_AT_006_010", () =>
                                    {
                                        UpdatePointer();
                                        isPlayVoiceSubturn4Complete = true;
                                    }, 1.2f);
                                }
                            });
                        }
                        if (_manager.IsEmptyHand(_manager.CurrentPlayerIndex))
                        {
                            _manager.GetBoardManager().ClearEventOnSelectSlot();
                            ControllerData.SetCallbackOnDragToIdle(null);
                            HideHighlightBoard(AllSlots);
                            _manager.StartCoroutine(GameHelper.ExecuteIEnumerator(WaitUntilPlaySoundFinish(), () => { ControllerData.SetIsCanEndTurn(true); }));
                            ControllerData.ClearPlayPhaseEventCallback();
                        }
                    };

                    ControllerData.SetCallbackOnDragToIdle(onDragToIdle);

                    ControllerData.SetIsBotCanAction(false);
                    ControllerData.SetIsCanEndTurn(false);
                }
                break;
            default:
                {
                    ControllerData.ClearPlayPhaseEventCallback();
                    ControllerData.SetIsBotCanAction(true);
                    _manager.StartCoroutine(GameHelper.ExecuteIEnumerator(WaitUntilPlaySoundFinish(), () => { ControllerData.SetIsCanEndTurn(true); }));
                }
                break;
        }
    }

    public override void OnEndPlayPhaseTutorial(UnityAction onComplete)
    {
        _manager.StartCoroutine(IEOnEndPlayPhaseTutorial(onComplete));
    }

    // before battle.
    private IEnumerator IEOnEndPlayPhaseTutorial(UnityAction onComplete)
    {
        switch (_manager.SubTurn)
        {
            case 4:
                _manager.GetBoardManager().ClearOnSelectedCompleteEvent();
                break;
            default:
                break;
        }
        onComplete?.Invoke();
        yield break;
    }
    #endregion

    #region Battle Phase

    public override IEnumerator OnEndPreBattlePhaseTutorial(UnityAction onComplete)
    {
        if (_manager.SubTurn > 4 && _manager.SubTurn % 2 == 0)
        {
            if (_soundKeys.Contains("DIALOG_AT_006_023") == false)
            {
                int darkMatterAmount = FindDarkMatterAmount();
                if (darkMatterAmount > 2)
                {
                    _soundKeys.Add("DIALOG_AT_006_023");
                    yield return PlayVoice(PlayerIndex.One, "AT_6_019", "DIALOG_AT_006_023", 0.8f);
                }
            }
        }

        onComplete?.Invoke();
        yield break;
    }

    public override IEnumerator OnPostBattlePhaseTutorial(UnityAction onComplete)
    {
        switch (_manager.SubTurn)
        {
            case 1:
                yield return PlayVoice(PlayerIndex.Two, "AT_6_020", "DIALOG_AT_006_004", 0.8f);
                break;
            case 2:
                yield return PlayVoice(PlayerIndex.One, "AT_6_006", "DIALOG_AT_006_007", 0.8f);
                break;
            case 4:
                yield return PlayVoice(PlayerIndex.One, "AT_6_010", "DIALOG_AT_006_011", 0.3f);
                yield return PlayVoice(PlayerIndex.One, "AT_6_011", "DIALOG_AT_006_012", 0.5f);
                break;
            default:
                break;
        }
        onComplete?.Invoke();
        yield break;
    }

    protected override IEnumerator IEOnEndPhase(Action onComplete)
    {
        onComplete?.Invoke();
        yield break;
    }

    private int FindDarkMatterAmount()
    {
        int amount = 0;
        foreach (SlotData slot in _manager.Data.SlotDatas)
        {
            if (slot.IsBeDarkMatter)
                amount++;
        }
        return amount;
    }

    public override void OnResolveDead(List<MinionData> deadTargetList)
    {
        if (_manager.SubTurn > 4)
        {
            List<MinionData> enemyMinions = deadTargetList.FindAll((minion) => minion.Owner != _manager.CurrentPlayerIndex && minion.SpiritCurrent >= 3);
            if (enemyMinions.Count == 0)
                return;
            bool playerTurn = _manager.SubTurn % 2 == 0;
            if (playerTurn && _soundKeys.Contains("DIALOG_AT_006_020") == false)
            {
                _soundKeys.Add("DIALOG_AT_006_020");
                _manager.ActionPlayCustomVoice(PlayerIndex.One, "AT_6_017", "DIALOG_AT_006_020", null, 0.5f);

            }
            else if (playerTurn == false && _soundKeys.Contains("DIALOG_AT_006_019") == false)
            {
                _soundKeys.Add("DIALOG_AT_006_019");
                _manager.ActionPlayCustomVoice(PlayerIndex.Two, "AT_6_022", "DIALOG_AT_006_019", null, 0.3f);
            }
        }
    }
    #endregion

    #region End Game

    public override void OnPlayPhaseTutorial() { }

    protected override IEnumerator OnPlayEndGameSound(bool isVictory)
    {
        if (isVictory == false)
        {
            yield return PlayVoice(PlayerIndex.One, "AT_6_012", "DIALOG_AT_006_013", 0.2f);
        }
        yield break;
    }

    #endregion

    #endregion
}
