﻿using Karamucho.UI;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class AdventureController : StoryGameController
{
    public abstract AdventureStageID StageID { get; }

    protected List<string> _playedSoundKeys = new List<string>();

    public AdventureController(GameManager manager) : base(manager)
    {
    }
    #region Methods

    public override void OnCheckClaimReward(UnityAction onComplete)
    {
        string advStageID = _manager.MatchData.Property.AdventureStageData.AdventureStageID.ToString();
        DataManager.Instance.GetRewardedAdventureDict().TryGetValue(advStageID, out bool isAdvRewarded);
        if (isAdvRewarded)
        {
            onComplete?.Invoke();
            return;
        }

        PopupUIManager.Instance.Show_SoftLoad(true);
        AdventureStageData adventureStageData = _manager.MatchData.Property.AdventureStageData;
        DataManager.Instance.RequestClaimAdventureReward(
            adventureStageData.AdventureChapterID,
            adventureStageData.AdventureStageID,
            (resultProcess) =>
            {
                PopupUIManager.Instance.Show_SoftLoad(false);
                string json = resultProcess.CustomData["rewardClaimed"];
                Dictionary<string, object> rawItemList = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                List<ItemData> itemList = new List<ItemData>();

                foreach (KeyValuePair<string, object> rawItem in rawItemList)
                {
                    string itemID = rawItem.Key;
                    int amount = int.Parse(rawItem.Value.ToString());

                    itemList.Add(new ItemData(itemID, amount));
                }
                DataManager.Instance.GetRewardedAdventureDict()[advStageID] = true;
                if (itemList.Count > 0)
                {
                    PopupUIManager.Instance.ShowPopup_GotReward(
                                LocalizationManager.Instance.GetText("TEXT_ADVENTURE_TITLE_REWARD"),
                                LocalizationManager.Instance.GetText("TEXT_ADVENTURE_BODY_REWARD"),
                                itemList,
                                delegate 
                                {
                                    onComplete?.Invoke();
                                });
                }
                else
                {
                    onComplete?.Invoke();
                }
            }
          );
    }

    public override int GetLifePoint(PlayerIndex playerIndex, GameMode mode)
    {
        return _manager.MatchData.Property.AdventureStageData.MatchPlayerDataConfigs[(int)playerIndex].HP;
    }

    public override string GetBoardID()
    {
        return "ADV_" + StageID.ToString().ToUpper();
    }

    public override void OnPrePlayPhase()
    {
        if (_isPhaseActive)
        {
            // Pre-play phase update loop.

            if (_manager.IsCanNextPhase)
            {
                if (_manager.IsPhaseReady(GamePhase.PrePlay))
                {
                    UnityAction action = delegate ()
                    {
                        PhaseLog log = new PhaseLog(DisplayGamePhase.Play);
                        _manager.ActionAddBattleLog(log);

                        NextPhase();
                    };

                    if (!ControllerData.IsTutorialPhaseActive)
                    {
                        ControllerData.SetIsTutorialPhaseActive(true);

                        _manager.StartCoroutine(OnEndPrePlayPhaseTutorial(action));
                    }
                }
            }
        }
        else
        {
            // Pre-play phase update loop first time.

            _isPhaseActive = true;

            //if (_manager.EndType != GameManager.EndGameType.Normal)
            //{
            //    _manager.OnEndGame();
            //    return;
            //}

            _manager.UpdateLocalHandCard();

            if (_manager.CountdownTime > _manager.TurnTime)
            {
                _manager.CountdownTime = _manager.TurnTime;
            }

            _manager.ResetActiveCount();

            if (
                (
                    !_manager.IsCanUseHandCard(_manager.CurrentPlayerIndex)     // Out of AP
                    || _manager.IsEmptyHand(_manager.CurrentPlayerIndex)        // Empty hand
                    || !_manager.IsHaveEmptySlot()                              // No empty slot
                )
                && _manager.IsAutoEndTurn
            )
            {
                {
                    _manager._timer = 0.0f;
                    _manager.SetActiveCount(1); // Set one action
                }
            }
            else
            {
                if (_manager._afkCount[(int)_manager.CurrentPlayerIndex] > 0)
                {
                    _manager._timer = _manager.AFKTime;
                }
                else
                {
                    _manager._timer = _manager.TurnTime;
                }
            }
            _manager._startTurnStamp = DateTimeData.GetDateTimeUTC();

            UIManager.Instance.SetTargetTimer(Mathf.Min(_manager.CountdownTime, _manager._timer));
            _manager._isTimer = true;
            _manager._isEndTurn = false;
            _manager._isTimeoutCallEndTurn = false;
            _manager._turnTimer = 0.0f;
            _manager.IsTurnTimeout = false;

            // Trigger Analytic
            {
                _manager.AnalyticTriggerStartTurn();
            }

            _manager.ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(new EventTriggerData(EventKey.PrePlayPhase)), true);

            if (_manager.GetLocalPlayerIndex() == _manager.CurrentPlayerIndex)
            {
                if (IsCanShowRedButton(_manager.SubTurn) == false)
                {
                    UIManager.Instance.EndButtonIsOpen(true);
                }
                else
                {
                    int handCount = _manager.Data.PlayerDataList[(int)_manager.CurrentPlayerIndex].CardList[CardZone.Hand].Count;
                    int currentSP = _manager.GetSumSpirit(_manager.CurrentPlayerIndex);

                    if (handCount == 0 || currentSP > 0)
                    {
                        UIManager.Instance.EndButtonIsOpen(true); // yellow
                    }
                    else
                    {
                        UIManager.Instance.EndButtonIsOpen(true, true); // red
                    }
                }
            }
        }
    }

    public virtual bool IsCanShowRedButton(int subturn)
    {
        return false;
    }
    #endregion

}
