﻿using Karamucho.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Tutorial3Controller : StoryGameController
{
    bool canPlayCard = false;
    //TODO: implement using key instead.
    List<string> _soundKeys = new List<string>();

    public Tutorial3Controller(GameManager manager) : base(manager)
    {
    }

    public override int GetLifePoint(PlayerIndex playerIndex, GameMode mode)
    {
        return 20;
    }

    public override string GetBoardID()
    {
        return "TUT_3";
    }

    /*
        [00][01][02][03]
        [04][05][06][07]
        [08][09][10][11]
        [12][13][14][15]
        */

    public override bool IsTutorialValidSlotID(int slotID)
    {
        if (GameManager.Instance.Phase == GamePhase.Play)
        {
            switch (GameManager.Instance.SubTurn)
            {
                case 2:
                    return slotID == GameHelper.SlotCodeToSlotID("2B");
                case 4:
                    return slotID == GameHelper.SlotCodeToSlotID("1B");
                case 6:
                    return slotID == GameHelper.SlotCodeToSlotID("2C");
                case 8:
                    return slotID == GameHelper.SlotCodeToSlotID("2B");
                default: return true;
            }
        }

        return true;
    }

    #region Phase

    #region SetupPhase

    public override IEnumerator SetupPhase()
    {
        _manager.SetPlayOrder(PlayerIndex.Two, null);
        _manager._isPlayFirstReady = true;

        while (!_manager.IsCanNextPhase || !_manager.IsPhaseReady(GamePhase.SetupData) || !_manager._isPlayFirstReady)
        {
            yield return null;
        }

        _manager.SetRandomSeed(_manager.MatchData.PlayerData[1].RandomSeed);
        _manager.SetLoadingProgress(1.0f);
        NextPhase();

        UIManager.Instance.FadeBlackIn();
    }

    #endregion

    #region Intro Phase

    protected override IEnumerator MiddleIntroPhase()
    {
        Debug.Log("เปิดมาด้วยสกรีนสีดำ ที่พอ fade ออกไป");
        yield return new WaitForSeconds(1f);

        Debug.Log("ก็เห็น token ของ Nyx อยู่ในฝั่ง Player โดยที่ Token ของฝั่งศัตรูยังไม่มีบอกว่าเป็นใคร");
        UIManager.Instance.SpotLight_ShowHeroBot(true);
        yield return new WaitForSeconds(1f);

        yield return PlayVoice(PlayerIndex.One, "TUT_3_001", "TU3_VOICE_001", 0f);
        yield return PlayVoice(PlayerIndex.One, "TUT_3_002", "TU3_VOICE_002", 0f);
        yield return PlayVoice(PlayerIndex.One, "TUT_3_003", "TU3_VOICE_003", 1f);

        Debug.Log("แล้วก็ปรากฎเป็น token ของ Sheila ลอยอยู่.");
        {
            UIManager.Instance.UIPlayerTop.Intro_State_ShowAtCenter();
            UIManager.Instance.SpotLight_ShowMid(true);
        }

        Debug.Log("smash ลงช่อง token ของศัตรู แล้ว fade ดำก็หายไป");
        yield return new WaitForSeconds(1f);
        {
            UIManager.Instance.UIPlayerTop.Intro_State_Default();
            UIManager.Instance.SpotLight_ShowMid(false);
            UIManager.Instance.SpotLight_ShowHeroBot(false);
            UIManager.Instance.FadeBlackOut();
        }

        yield return PlayVoice(PlayerIndex.Two, "TUT_3_004", "TU3_VOICE_004", 0.2f);
        yield return PlayVoice(PlayerIndex.Two, "TUT_3_005", "TU3_VOICE_005", 0.2f);

        yield return PlayVoice(PlayerIndex.One, "TUT_3_006", "TU3_VOICE_006", 0.2f);
        yield return PlayVoice(PlayerIndex.One, "TUT_3_007", "TU3_VOICE_007", 0.2f);

        yield return PlayVoice(PlayerIndex.Two, "TUT_3_008", "TU3_VOICE_008", 0.2f);
    }

    protected override IEnumerator EndIntroPhase()
    {
        bool isFinish = false;
        CreateDynamicTutorialPopup("TU3_DPUP_1", GameHelper.CenterScreen, true, () => isFinish = true);
        yield return new WaitUntil(() => isFinish == true);
    }
    #endregion

    #region Begin Phase

    public override IEnumerator IEOnEndBeginPhase()
    {
        switch (_manager.SubTurn)
        {
            //case 5:
            //    yield return PlayVoice(PlayerIndex.Two, "TUT_3_020", "TU3_VOICE_020", 0f);
            //    break;
            case 7:
                yield return PlayVoice(PlayerIndex.Two, "TUT_3_026", "TU3_VOICE_026", 0f);
                break;
            default:
                if (_manager.SubTurn > 7 && _manager.SubTurn % 2 == 0) // Player turn.
                {
                    int enemyHP = _manager.Data.PlayerDataList[(int)PlayerIndex.Two].CurrentHP;
                    if (enemyHP - _manager.GetSumSpirit(PlayerIndex.One) <= 0)
                    {
                        yield return PlayVoice(PlayerIndex.Two, "TUT_3_040", "TU3_VOICE_040", 0f);
                    }
                }
                else if (_manager.SubTurn > 7 && _manager.SubTurn % 2 == 1) // Enemy turn.
                {
                    int playerHP = _manager.Data.PlayerDataList[(int)PlayerIndex.One].CurrentHP;
                    if (playerHP - _manager.GetSumSpirit(PlayerIndex.Two) <= 0)
                    {
                        yield return PlayVoice(PlayerIndex.Two, "TUT_3_041", "TU3_VOICE_041", 0f);
                        yield return PlayVoice(PlayerIndex.One, "TUT_3_042", "TU3_VOICE_042", 0f);
                    }
                }
                break;
        }
    }

    #endregion

    #region Spirit Phase

    public override IEnumerator OnEndSpiritPhaseTutorial(UnityAction onComplete)
    {
        switch (_manager.SubTurn)
        {
            default:
                break;
        }
        onComplete?.Invoke();
        yield break;
    }

    #endregion

    #region Draw Phase
    public override void OnSetDrawCount(int subturn, ref int drawCount, ref bool isDrawAP)
    {
        isDrawAP = false;
        switch (_manager.SubTurn)
        {
            case 1: drawCount = 2; break;
            case 2: drawCount = 1; break;//p
            case 3: drawCount = 2; break;
            case 4: drawCount = 1; break;//p
            case 5: drawCount = 3; break;
            case 6: drawCount = 1; break;//p
            case 7: drawCount = 2; break;
            case 8: drawCount = 1; break;//p
            case 9: drawCount = 1; break;
            case 10: drawCount = 3; break;//p
            case 11: drawCount = 2; break;
            case 12: drawCount = 2; break;//p
            case 13: drawCount = 1; break;
            case 14: drawCount = 2; break;//p
            case 15: drawCount = 1; break;
            case 16: drawCount = 2; break;//p
            case 17: drawCount = 1; break;
            case 18: drawCount = 3; break;//p
            case 19: drawCount = 1; break;
            case 20: drawCount = 1; break;//p
            case 21: drawCount = 1; break;
            case 22: drawCount = 1; break;//p
            case 23: drawCount = 1; break;
            case 24: drawCount = 2; break;//p
            case 25: drawCount = 1; break;
            case 26: drawCount = 2; break;//p
            case 27: drawCount = 1; break;
            case 28: drawCount = 1; break;//p
            case 29: drawCount = 1; break;
            case 30: drawCount = 2; break;//p
            case 31: drawCount = 1; break;
            case 32: drawCount = 2; break;//p
            case 33: drawCount = 1; break;
            case 34: drawCount = 1; break;//p
            case 35: drawCount = 1; break;
            case 36: drawCount = 1; break;//p


            default:
                {
                    int toDrawCount = _manager.MaxHandCard;
                    int handCount = _manager.Data.PlayerDataList[(int)_manager.CurrentPlayerIndex].CardList[CardZone.Hand].Count;
                    int deckCount = _manager.Data.PlayerDataList[(int)_manager.CurrentPlayerIndex].CardList[CardZone.Deck].Count;
                    int handSlot = toDrawCount - handCount;

                    drawCount = Mathf.Min(handSlot, deckCount);
                }
                break;
        }
    }
    #endregion

    #region PrePlay Phase

    public override IEnumerator OnEndPrePlayPhaseTutorial(UnityAction onComplete)
    {
        switch (_manager.SubTurn)
        {
            case 1:
                {
                    _manager.StartCoroutine(PlayVoice(PlayerIndex.Two, "TUT_3_011", "TU3_VOICE_011", 0.1f));
                }
                break;
            case 2:
                {
                    List<TutorialCardPopupHighlight.HighlightType> highlight = new List<TutorialCardPopupHighlight.HighlightType>() {
                         TutorialCardPopupHighlight.HighlightType.Ability
                    };
                    yield return ShowCardDetailFromHandAutoHide(0, PLAY_CARD_DETAIL_TIME, highlight);

                    //AddVoiceToList(PlayerIndex.One, "TUT_3_013", "TU3_VOICE_013", 0f);
                    //AddVoiceToList(PlayerIndex.One, "TUT_3_014", "TU3_VOICE_014", 0f);
                    //_manager.StartCoroutine(PlayVoiceList());
                    yield return PlayVoice(PlayerIndex.One, "TUT_3_013", "TU3_VOICE_013", 0f);
                    yield return PlayVoice(PlayerIndex.One, "TUT_3_014", "TU3_VOICE_014", 0f);
                }
                break;
            case 4:
                {
                    //List<TutorialCardPopupHighlight.HighlightType> highlight = new List<TutorialCardPopupHighlight.HighlightType>() {
                    //     TutorialCardPopupHighlight.HighlightType.Rarity
                    //};
                    //yield return ShowCardDetailFromHandAutoHide(0, PLAY_CARD_DETAIL_TIME, highlight);
                    AddVoiceToList(PlayerIndex.One, "TUT_3_018", "TU3_VOICE_018", 0f);
                    AddVoiceToList(PlayerIndex.One, "TUT_3_019", "TU3_VOICE_019", 0f);
                    _manager.StartCoroutine(PlayVoiceList());
                    CreateDynamicTutorialPopup("TU3_DPUP_3");
                }
                break;
            case 5:
                _manager.StartCoroutine(PlayVoice(PlayerIndex.Two, "TUT_3_021", "TU3_VOICE_021", 0f));
                break;
            case 6:
                AddVoiceToList(PlayerIndex.One, "TUT_3_024", "TU3_VOICE_024", 0f);
                AddVoiceToList(PlayerIndex.One, "TUT_3_025", "TU3_VOICE_025", 0f);
                _manager.StartCoroutine(PlayVoiceList());
                break;
            case 7:
                {
                    _manager.StartCoroutine(PlayVoice(PlayerIndex.Two, "TUT_3_027", "TU3_VOICE_027", 0f));
                }
                break;
            case 8:
                yield return PlayVoice(PlayerIndex.One, "TUT_3_030", "TU3_VOICE_030", 0f);
                yield return PlayVoice(PlayerIndex.One, "TUT_3_031", "TU3_VOICE_031", 0f);
                _manager.StartCoroutine(PlayVoice(PlayerIndex.One, "TUT_3_032", "TU3_VOICE_032", 0f));
                break;
            case 9:
                _manager.StartCoroutine(PlayVoice(PlayerIndex.Two, "TUT_3_035", "TU3_VOICE_035", 0f));
                break;
            case 10:
                AddVoiceToList(PlayerIndex.One, "TUT_3_036", "TU3_VOICE_036", 0f);
                AddVoiceToList(PlayerIndex.One, "TUT_3_037", "TU3_VOICE_037", 0f);
                _manager.StartCoroutine(PlayVoiceList());
                break;
            default:
                if (_manager.SubTurn > 10)
                {
                    if (_soundKeys.Contains("TU3_VOICE_045") == false)
                    {
                        BattleCardData card;
                        if (_manager.FindBattleCard("CT0602", CardZone.Hand, PlayerIndex.One, out card))
                        {
                            _soundKeys.Add("TU3_VOICE_045");
                            _manager.StartCoroutine(PlayVoice(PlayerIndex.One, "TUT_3_045", "TU3_VOICE_045", 0f));
                        }
                    }
                }
                break;
        }
        onComplete?.Invoke();
    }

    #endregion

    #region Play Phase

    public override void OnBeginPlayPhaseTutorial()
    {
        switch (_manager.SubTurn)
        {
            case 2:
                {
                    ShowPopupToolTip("TU3_TOOLTIP_001", GetSlotScreenPosition(13));
                    List<int> highlightBoard = new List<int>() { 5 };
                    ShowHighlightBoard(highlightBoard, Karamucho.UI.BoardSlotHolder.SlotHilightType.Hilight);

                    UnityAction onIdleToSelect = delegate ()
                    {
                        ShowHighlightBoard(highlightBoard, Karamucho.UI.BoardSlotHolder.SlotHilightType.Hilight);
                    };

                    UnityAction onSelectToIdle = delegate () { };

                    UnityAction onSelectToDrag = delegate () { };

                    UnityAction onDragToSelect = delegate () { };
                    UnityAction onDragToIdle = delegate ()
                    {
                        if (_manager.IsEmptyHand(_manager.CurrentPlayerIndex))
                        {
                            HidePopupToolTip();
                            base.CreateDynamicTutorialPopup("TU3_DPUP_2", base.GetSlotScreenPosition(13));

                            HideHighlightBoard(highlightBoard);
                            ControllerData.ClearPlayPhaseEventCallback();
                            //_manager.StartCoroutine(GameHelper.ExecuteIEnumerator(WaitUntilPlaySoundFinish(),
                            //    () =>
                            //    {
                            UIManager.Instance.GetUITutorialManager().RequestShowPopup("T_ArrowEndTurn");
                            ControllerData.SetIsCanEndTurn(true);
                            //}));
                        }
                    };

                    ControllerData.SetCallbackOnIdleToSelect(onIdleToSelect);
                    ControllerData.SetCallbackOnSelectToIdle(onSelectToIdle);
                    ControllerData.SetCallbackOnSelectToDrag(onSelectToDrag);
                    ControllerData.SetCallbackOnDragToSelect(onDragToSelect);
                    ControllerData.SetCallbackOnDragToIdle(onDragToIdle);

                    ControllerData.SetIsBotCanAction(false);
                    ControllerData.SetIsCanEndTurn(false);
                }
                break;
            case 4:
                {
                    List<int> highlightBoard = new List<int>() { 1 };
                    ShowHighlightBoard(highlightBoard, Karamucho.UI.BoardSlotHolder.SlotHilightType.Hilight);

                    UnityAction onIdleToSelect = delegate ()
                    {
                        ShowHighlightBoard(highlightBoard, Karamucho.UI.BoardSlotHolder.SlotHilightType.Hilight);
                    };

                    UnityAction onSelectToIdle = delegate () { };

                    UnityAction onSelectToDrag = delegate () { };

                    UnityAction onDragToSelect = delegate () { };
                    UnityAction onDragToIdle = delegate ()
                    {
                        if (_manager.IsEmptyHand(_manager.CurrentPlayerIndex))
                        {
                            HideHighlightBoard(highlightBoard);
                            //_manager.StartCoroutine(GameHelper.ExecuteIEnumerator(WaitUntilPlaySoundFinish(), () =>
                            //{
                            UIManager.Instance.GetUITutorialManager().RequestShowPopup("T_ArrowEndTurn");
                            ControllerData.SetIsCanEndTurn(true);
                            //}));
                            ControllerData.ClearPlayPhaseEventCallback();
                        }
                    };

                    ControllerData.SetCallbackOnIdleToSelect(onIdleToSelect);
                    ControllerData.SetCallbackOnSelectToIdle(onSelectToIdle);
                    ControllerData.SetCallbackOnSelectToDrag(onSelectToDrag);
                    ControllerData.SetCallbackOnDragToSelect(onDragToSelect);
                    ControllerData.SetCallbackOnDragToIdle(onDragToIdle);

                    ControllerData.SetIsBotCanAction(false);
                    ControllerData.SetIsCanEndTurn(false);
                }
                break;
            case 6:
                {
                    List<int> highlightBoard = new List<int>() { 6 };
                    ShowHighlightBoard(highlightBoard, Karamucho.UI.BoardSlotHolder.SlotHilightType.Hilight);

                    UnityAction onIdleToSelect = delegate ()
                    {
                        ShowHighlightBoard(highlightBoard, Karamucho.UI.BoardSlotHolder.SlotHilightType.Hilight);
                    };

                    UnityAction onSelectToIdle = delegate () { };

                    UnityAction onSelectToDrag = delegate () { };

                    UnityAction onDragToSelect = delegate () { };
                    UnityAction onDragToIdle = delegate ()
                    {
                        if (_manager.IsEmptyHand(_manager.CurrentPlayerIndex))
                        {
                            HideHighlightBoard(highlightBoard);
                            ControllerData.ClearPlayPhaseEventCallback();
                            //_manager.StartCoroutine(GameHelper.ExecuteIEnumerator(WaitUntilPlaySoundFinish(), () =>
                            //{
                            UIManager.Instance.GetUITutorialManager().RequestShowPopup("T_ArrowEndTurn");
                            ControllerData.SetIsCanEndTurn(true);
                            //}));
                        }
                    };

                    ControllerData.SetCallbackOnIdleToSelect(onIdleToSelect);
                    ControllerData.SetCallbackOnSelectToIdle(onSelectToIdle);
                    ControllerData.SetCallbackOnSelectToDrag(onSelectToDrag);
                    ControllerData.SetCallbackOnDragToSelect(onDragToSelect);
                    ControllerData.SetCallbackOnDragToIdle(onDragToIdle);

                    ControllerData.SetIsBotCanAction(false);
                    ControllerData.SetIsCanEndTurn(false);
                }
                break;
            case 8:
                {
                    List<int> highlightBoard = new List<int>() { 5 };
                    ShowHighlightBoard(highlightBoard, Karamucho.UI.BoardSlotHolder.SlotHilightType.Hilight);

                    UnityAction onIdleToSelect = delegate ()
                    {
                        ShowHighlightBoard(highlightBoard, Karamucho.UI.BoardSlotHolder.SlotHilightType.Hilight);
                    };

                    UnityAction onDragToIdle = delegate ()
                    {
                        if (_manager.IsEmptyHand(_manager.CurrentPlayerIndex))
                        {
                            HideHighlightBoard(highlightBoard);

                            _manager.StartCoroutine(AfterUseCardSubturn8(delegate
                            {
                                UIManager.Instance.GetUITutorialManager().RequestShowPopup("T_ArrowEndTurn");
                                ControllerData.SetIsCanEndTurn(true);
                            }));
                            ControllerData.ClearPlayPhaseEventCallback();
                        }
                    };

                    ControllerData.SetCallbackOnIdleToSelect(onIdleToSelect);
                    ControllerData.SetCallbackOnDragToIdle(onDragToIdle);

                    ControllerData.SetIsBotCanAction(false);
                    ControllerData.SetIsCanEndTurn(false);
                }
                break;
            default:
                {
                    if (_manager.SubTurn % 2 == 0)
                    {
                        UnityAction onIdleToSelect = delegate () { };
                        UnityAction onSelectToIdle = delegate () { };
                        UnityAction onSelectToDrag = delegate () { };
                        UnityAction onDragToSelect = delegate () { };

                        UnityAction onDragToIdle = delegate ()
                        {
                            if (CanEndTurn(_manager.CurrentPlayerIndex))
                            {
                                if (_manager.SubTurn == 10)
                                {
                                    _manager.StartCoroutine(GameHelper.ExecuteIEnumerator(WaitUntilPlaySoundFinish(), () => { ControllerData.SetIsCanEndTurn(true); }));
                                }
                                else
                                {
                                    GameHelper.DelayCallback(1f, delegate
                                    {
                                        ControllerData.SetIsCanEndTurn(true);
                                    });
                                }
                                ControllerData.ClearPlayPhaseEventCallback();
                            }
                        };

                        ControllerData.SetCallbackOnIdleToSelect(onIdleToSelect);
                        ControllerData.SetCallbackOnSelectToIdle(onSelectToIdle);
                        ControllerData.SetCallbackOnSelectToDrag(onSelectToDrag);
                        ControllerData.SetCallbackOnDragToSelect(onDragToSelect);
                        ControllerData.SetCallbackOnDragToIdle(onDragToIdle);

                        ControllerData.SetIsBotCanAction(false);
                        ControllerData.SetIsCanEndTurn(false);
                    }
                    else
                    {
                        ControllerData.SetIsBotCanAction(true);
                        ControllerData.SetIsCanEndTurn(true);
                    }
                }
                break;
        }
    }

    private IEnumerator AfterUseCardSubturn8(Action onComplete)
    {
        yield return new WaitForSeconds(1.5f);
        yield return PlayVoice(PlayerIndex.Two, "TUT_3_033", "TU3_VOICE_033", .5f);
        //yield return PlayVoice(PlayerIndex.One, "TUT_3_034", "TU3_VOICE_034", .5f);
        onComplete?.Invoke();
    }

    public override void OnPlayPhaseTutorial()
    {
        switch (_manager.SubTurn)
        {
            default:
                {
                }
                break;
        }
    }

    public override void OnEndPlayPhaseTutorial(UnityAction onComplete)
    {
        _manager.StartCoroutine(IEOnEndPlayPhaseTutorial(onComplete));
    }

    // before battle.
    private IEnumerator IEOnEndPlayPhaseTutorial(UnityAction onComplete)
    {
        UIManager.Instance.GetUITutorialManager().RequestHidePopup("T_ArrowEndTurn");
        switch (_manager.SubTurn)
        {
            case 5:
                yield return PlayVoice(PlayerIndex.One, "TUT_3_022", "TU3_VOICE_022", 0.2f);
                yield return HintReadDetailCard();
                _manager.StartCoroutine(PlayVoice(PlayerIndex.One, "TUT_3_023", "TU3_VOICE_023", 0f));
                break;
            case 7:
                _manager.StartCoroutine(PlayVoice(PlayerIndex.Two, "TUT_3_028", "TU3_VOICE_028", 0.2f));
                //yield return PlayVoice(PlayerIndex.One, "TUT_3_029", "TU3_VOICE_029", 0.6f);
                break;
        }
        onComplete?.Invoke();
        yield break;
    }

    bool isRead;
    private IEnumerator HintReadDetailCard()
    {
        SetActiveHandPointer(true, base.GetSlotScreenPosition(2));
        ShowPopupToolTip("TU3_TOOLTIP_002", base.GetSlotScreenPosition(13));
        while (true)
        {
            if (UIManager.Instance.IsHilightBoxShowing)
            {
                if (isRead == false &&
                    UIManager.Instance.CardUIDetailHover != null &&
                    UIManager.Instance.CardUIDetailHover.Data.BaseID == "CT0601")
                {
                    isRead = true;
                }
            }
            else
            {
                if (isRead)
                {
                    SetActiveHandPointer(false, base.GetSlotScreenPosition(2));
                    HidePopupToolTip();
                    break;
                }
            }
            yield return null;
        }
    }
    #endregion

    #region Battle Phase

    int? plantEaterID = null;
    public override IEnumerator OnEndPreBattlePhaseTutorial(UnityAction onComplete)
    {
        if (_manager.SubTurn > 10) // enemy turn.
        {
            if (_soundKeys.Contains("TU3_VOICE_038") == false)
            {
                BattleCardData card;
                if (_manager.FindBattleCard("CT0635", CardZone.Battlefield, PlayerIndex.Two, out card))
                {
                    plantEaterID = card.UniqueID;
                    _soundKeys.Add("TU3_VOICE_038");
                    yield return PlayVoice(PlayerIndex.Two, "TUT_3_038", "TU3_VOICE_038", 0f);
                    yield return PlayVoice(PlayerIndex.One, "TUT_3_009", "TU3_VOICE_009", 0f);
                }
            }
        }
        onComplete?.Invoke();
        yield break;
    }

    public override IEnumerator OnPostBattlePhaseTutorial(UnityAction onComplete)
    {
        switch (_manager.SubTurn)
        {
            case 1:
                yield return PlayVoice(PlayerIndex.One, "TUT_3_012", "TU3_VOICE_012", 0f);
                break;
            case 2:
                _manager.StartCoroutine(PlayVoice(PlayerIndex.Two, "TUT_3_015", "TU3_VOICE_015", 0.2f));
                break;
            //case 3:
            //    yield return PlayVoice(PlayerIndex.One, "TUT_3_016", "TU3_VOICE_016", 0.2f);
            //    yield return PlayVoice(PlayerIndex.One, "TUT_3_017", "TU3_VOICE_017", 0.2f);
            //    break;
            default:
                {
                    //if (_manager.SubTurn > 10)
                    //{
                    //    if (plantEaterID != null)
                    //    {
                    //        BattleCardData card;
                    //        if (_manager.FindBattleCard((int)plantEaterID, out card))
                    //        {
                    //            bool isDead = card.CurrentZone == CardZone.Graveyard;
                    //            if (isDead)
                    //            {
                    //                plantEaterID = null;
                    //                if (_soundKeys.Contains("TU3_VOICE_039") == false)
                    //                {
                    //                    _soundKeys.Add("TU3_VOICE_039");
                    //                    //yield return PlayVoice(PlayerIndex.One, "TUT_3_010", "TU3_VOICE_010", 0f);
                    //                    //yield return PlayVoice(PlayerIndex.Two, "TUT_3_039", "TU3_VOICE_039", 0f);
                    //                }
                    //            }
                    //        }
                    //    }
                    //}
                }
                break;
        }
        onComplete?.Invoke();
        yield break;
    }
    #endregion

    #region End Game
    protected override IEnumerator OnPlayEndGameSound(bool isVictory)
    {
        TutorialPopupManager.Instance.BTNShowDynamicPopup.gameObject.SetActive(false);
        if (isVictory)
        {
            yield return PlayVoice(PlayerIndex.Two, "TUT_3_043", "TU3_VOICE_043", 0.2f);
        }
        else
        {
            yield return PlayVoice(PlayerIndex.One, "TUT_3_044", "TU3_VOICE_044", 0f);
        }
    }

    public override void OnCheckClaimReward(UnityAction onComplete)
    {
        DataManager.Instance.ClaimTutorialRewardByTutorialIndex(2);
        onComplete?.Invoke();
    }

    #endregion

    #endregion
}
