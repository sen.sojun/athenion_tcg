﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using Karamucho.UI;

public abstract class GameManagerController
{
    protected bool _isPhaseActive = false;
    protected GameManager _manager;

    #region Events
    public static event Action<GamePhase> OnGamePhaseUpdate;
    #endregion

    public GameManagerController(GameManager manager)
    {
        this._manager = manager;

        GameManager.OnDestroy += DeInitialize;
    }

    public abstract void DeInitialize();

    #region Game Phase
    public void OnInitPhase()
    {
        if (!_isPhaseActive)
        {
            // Init phase update loop first time.

            _isPhaseActive = true;
            _manager.ShowFullLoad(true);

            Karamucho.UI.CardResourceManager.ClearCache();
            Karamucho.ResourceManager.Clear();

            _manager.GetServerUTCTime(
                delegate (DateTime serverUTCTime)
                {
                    _manager._startPlayServerTime = serverUTCTime;
                    _manager._startPlayStamp = Time.realtimeSinceStartup;

                    _manager.Turn = 1;
                    _manager.SubTurn = 1;
                    _manager._timer = 0.0f;
                    _manager._playTime = 0.0f;

                    _manager.LoadScene(GameHelper.BattleUISceneName, LoadSceneMode.Additive, true, NextPhase);
                }
            );
        }
    }

    public void OnSetupDataPhase()
    {
        if (!_isPhaseActive)
        {
            // SetupData phase update loop first time.

            _isPhaseActive = true;

            UIManager.Instance.ShowBTNOverlayUI(false);
            _manager.SetupData();
            _manager.StartCoroutine(SetupPhase());
        }
    }

    public abstract IEnumerator SetupPhase();

    public void OnIntroPhase()
    {
        if (!_isPhaseActive)
        {
            // Intro phase update loop first time.

            _isPhaseActive = true;

            _manager.StartCoroutine(IntroPhase());
        }
    }
    protected abstract IEnumerator IntroPhase();

    public void OnReHandPhase()
    {
        if (!_isPhaseActive)
        {
            // Rehand phase update loop first time.

            _isPhaseActive = true;

            _manager.StartCoroutine(ReHandPhase());
        }
    }
    protected abstract IEnumerator ReHandPhase();

    public virtual void OnBeginPhase()
    {
        if (!_isPhaseActive)
        {
            if (!_manager.IsCanNextPhase) return;

            // Begin phase update loop first time.
            _isPhaseActive = true;

            _manager.UpdateInfoAllPlayers();

            if (_manager.IsNetworkPlay())
            {
                if (_manager.SubTurn == 1)
                {
                    _manager.InitPokeStamp();
                    _manager._isStartPoke = true;
                }
            }

            _manager.StartCoroutine(IEBeginPhase());
        }
    }

    protected abstract IEnumerator IEBeginPhase();

    public abstract void OnSpiritPhase();

    public abstract void OnDrawPhase();

    public void OnPostDrawPhase()
    {
        if (_isPhaseActive)
        {
            // Post-Draw phase update loop.
            if (_manager.IsCanNextPhase)
            {
                if (_manager.IsPhaseReady(GamePhase.PostDraw))
                {
                    NextPhase();
                }
                else if (!_manager.IsPhaseReady(GamePhase.PostDraw, _manager.GetLocalPlayerIndex()))
                {
                    if (_manager.IsNetworkPlay())
                    {
                        _manager.PhotonRequestPhaseReady(GamePhase.PostDraw, _manager.GetLocalPlayerIndex(), _manager.SubTurn);
                    }
                }
            }
        }
        else
        {
            // Post-Draw phase update loop first time.
            _isPhaseActive = true;
            _manager.ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(new EventTriggerData(EventKey.PostDrawPhase)), true);
        }
    }

    public abstract void OnPrePlayPhase();

    public abstract void OnPlayPhase();

    public void OnPostPlayPhase()
    {
        if (_isPhaseActive)
        {
            // Post-play phase update loop.

            if (_manager.IsCanNextPhase)
            {
                if (_manager.IsPhaseReady(GamePhase.PostPlay))
                {
                    NextPhase();
                }
                else if (!_manager.IsPhaseReady(GamePhase.PostPlay, _manager.GetLocalPlayerIndex()))
                {
                    if (_manager.IsNetworkPlay())
                    {
                        _manager.PhotonRequestPhaseReady(GamePhase.PostPlay, _manager.GetLocalPlayerIndex(), _manager.SubTurn);
                    }
                }
            }
        }
        else
        {
            // Post-play phase update loop first time.
            _isPhaseActive = true;

            _manager._isTimer = false;
            _manager._isDisableTimer = false;

            if (_manager.IsLocalPlayer(_manager.CurrentPlayerIndex))
            {
                UIManager.Instance.HideCardsIsReady();
            }
            UIManager.Instance.HideUITimer();
            CardUIManager.Instance.CancelSelectCard();

            if (_manager.IsPlayerActive)
            {
                _manager._afkCount[(int)_manager.CurrentPlayerIndex] = 0;
            }
            else
            {
                _manager._afkCount[(int)_manager.CurrentPlayerIndex]++;

                //if (_manager._afkCount[(int)_manager.CurrentPlayerIndex] >= _manager.AFKLose)
                //{
                //    // too much AFK
                //    _manager.OnEndGame();
                //    return;
                //}
            }

            int handCount = _manager.GetHandCount(_manager.CurrentPlayerIndex);
            int currentSP = _manager.GetSumSpirit(_manager.CurrentPlayerIndex);
            if (handCount > 0)
            {
                if (currentSP == 0 && _manager.PlayCardCount == 0)
                {
                    List<int> handUniqueIDList = new List<int>();
                    for (int index = 0; index < _manager.GetHandCount(_manager.CurrentPlayerIndex); ++index)
                    {
                        int uniqueID = _manager.Data.PlayerDataList[(int)_manager.CurrentPlayerIndex].CardList[CardZone.Hand][index].UniqueID;
                        handUniqueIDList.Add(uniqueID);
                    }

                    // AP is 0 and not play card.
                    for (int count = 0; count < _manager.SkipTurnDiscard; ++count)
                    {
                        if (handUniqueIDList != null && handUniqueIDList.Count > 0)
                        {
                            int index = UnityEngine.Random.Range(0, handUniqueIDList.Count);
                            int uniqueID = handUniqueIDList[index];
                            handUniqueIDList.RemoveAt(index);

                            _manager.RequestMoveCardToZone(
                                  uniqueID
                                , CardZone.Hand
                                , CardZone.Remove
                                , -1, null
                                , false
                            );
                        }
                        else
                        {
                            _manager.ActionDrawDamage(
                                _manager.CurrentPlayerIndex
                                , 1
                                , null
                            );
                        }
                    }
                }
            }

            //if (_manager._endGameType != GameManager.EndGameType.Normal)
            //{
            //    _manager.OnEndGame();
            //    return;
            //}

            _manager.ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(new EventTriggerData(EventKey.PostPlayPhase)), true);
        }
    }

    public abstract void OnPreBattlePhase();

    public void OnBattlePhase()
    {
        if (!_isPhaseActive)
        {
            // Battle phase update loop first time.
            _isPhaseActive = true;
            _manager.ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(new EventTriggerData(EventKey.BattlePhase)), true);

            // Trigger battle via BattleManager
            BattleManager.Instance.TriggerBeginBattle(delegate ()
            {
                if (_manager.IsNetworkPlay())
                {
                    _manager.PhotonRequestPhaseReady(GamePhase.Battle, _manager.GetLocalPlayerIndex(), _manager.SubTurn);
                }
                NextPhase();
            });
        }
    }

    public abstract void OnPostBattlePhase();

    public abstract void OnEndPhase();
    #endregion

    #region Methods
    protected virtual void NextPhase()
    {
        if (_manager.Phase == GamePhase.End)
        {
            _manager.Phase = GamePhase.Begin;
        }
        else
        {
            _manager.Phase++;
        }

        OnGamePhaseUpdate?.Invoke(_manager.Phase);
        _isPhaseActive = false;
        _manager.ResetSafeToLeaveTimer();
    }

    public abstract int GetLifePoint(PlayerIndex playerIndex, GameMode mode);
    public abstract bool IsValidSlotID(int slotID);
    public virtual bool IsCanShowSpiritEffect() { return true; }

    public virtual void OnPlayCardFail() { }

    public virtual string GetBoardID()
    {
        return "DEFAULT";
    }

    public virtual bool IsCanUseCard(MinionData minion)
    {
        if (minion.SpiritCurrent <= _manager.Data.PlayerDataList[(int)minion.Owner].CurrentAP)
        {
            return true;
        }

        return false;
    }

    public virtual void OnCheckClaimReward(UnityAction onComplete)
    {
        onComplete?.Invoke();
    }

    #endregion
}