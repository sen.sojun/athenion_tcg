﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;

public class DefaultController : GameManagerController
{
    public DefaultController(GameManager manager) : base(manager)
    {
    }

    public override void DeInitialize()
    {
    }
    #region Game Phase

    public override IEnumerator SetupPhase()
    {
        if (_manager.IsNetworkPlay())
        {
            if (_manager.IsMasterClient())
            {
                _manager.ActionRandomPlayOrder(null);
                _manager.PhotonSyncPlayFirst(_manager.CurrentPlayerIndex);
            }
        }
        else
        {
            switch (_manager.Mode)
            {
                case GameMode.BotSimulate:
                    {
                        _manager.SetPlayOrder(PlayerIndex.One, null);
                    }
                    break;

                default:
                    {
                        _manager.ActionRandomPlayOrder(null);
                    }
                    break;
            }

            _manager._isPlayFirstReady = true;
        }

        while (!_manager.IsCanNextPhase || !_manager.IsPhaseReady(GamePhase.SetupData) || !_manager._isPlayFirstReady)
        {
            yield return null;
        }

        //bool isFinish = false;
        //PlayerStatManager.Instance.DoUpdatePlayerMatchTicket(DataManager.Instance.GetRoomName(), () => isFinish = true);
        //yield return new WaitUntil(() => isFinish == true);

        _manager.SetRandomSeed(_manager.MatchData.PlayerData[1].RandomSeed);
        _manager.SetLoadingProgress(1.0f);

        NextPhase();
    }

    protected override IEnumerator IntroPhase()
    {
        _manager.ShowFullLoad(false);
        SoundManager.PlayBGM(SoundManager.BGM.Intro, 3.0f);
        // Waiting opponent to ready.

        // Start play intro
        bool _isCompleteIntro = false;
        if (_manager.PlayerIndexList != null && _manager.PlayerIndexList.Count == 2) // 2 players
        {
            foreach (PlayerIndex playerIndex in _manager.PlayerIndexList)
            {
                if (_manager.IsLocalPlayer(playerIndex))
                {
                    BattlePlayerData player = _manager.Data.PlayerDataList[(int)playerIndex];
                    UIManager.Instance.SetPlayerInfo(Karamucho.UI.CardUIManager.PlayerPosition.Bottom, player);
                }
                else
                {
                    BattlePlayerData player = _manager.Data.PlayerDataList[(int)playerIndex];
                    UIManager.Instance.SetPlayerInfo(Karamucho.UI.CardUIManager.PlayerPosition.Top, player);
                }
                UIManager.Instance.SetPlayerAP(playerIndex, 0, null);
            }
        }
        _manager.UpdateAllUIInfo();

        while (!_manager.IsCanNextPhase)
        {
            //wait
            yield return null;
        }

        _manager._isMePlayFirst = _manager.IsLocalPlayer(_manager.CurrentPlayerIndex);

        UIManager.Instance.StartIntro(_manager.IsLocalPlayer(_manager.CurrentPlayerIndex), delegate () { _isCompleteIntro = true; });

        while (!_manager.IsCanNextPhase || !_isCompleteIntro)
        {
            //wait
            yield return null;
        }

        // Init Draw
        _manager.InitDraw();

        while (!_manager.IsCanNextPhase)
        {
            //wait
            yield return null;
        }

        if (_manager.IsNetworkPlay())
        {
            _manager.PhotonRequestPhaseReady(GamePhase.Intro, _manager.GetLocalPlayerIndex(), _manager.SubTurn);
        }
        NextPhase();
    }

    protected override IEnumerator ReHandPhase()
    {
        switch (_manager.Mode)
        {
            case GameMode.BotSimulate:
                {
                    // Skip re-hand
                    UIManager.Instance.HideRedraw();
                }
                break;

            default:
                {
                    // stamp match ticket
                    bool isFinish = false;
                    PlayerStatManager.Instance.DoUpdatePlayerMatchTicket(DataManager.Instance.GetRoomName(), () => isFinish = true);
                    yield return new WaitUntil(() => isFinish == true);

                    // re-hand
                    _manager.ReHand();

                    while (!_manager.IsCanNextPhase || !_manager._isConfirmReHand[(int)_manager.GetLocalPlayerIndex()])
                    {
                        yield return null;
                    }

                    UIManager.Instance.HideRedraw();

                    // Show waiting
                    UIManager.Instance.ShowWaiting();

                    while (!_manager.IsCanNextPhase || !_manager.IsConfirmRehand)
                    {
                        yield return null;
                    }

                    // Hide waiting
                    UIManager.Instance.HideWaiting();
                }
                break;
        }
        
        SoundManager.PlayBGM(SoundManager.BGM.Battle, 5.0f);
        if (_manager.IsNetworkPlay())
        {
            _manager.PhotonRequestPhaseReady(GamePhase.ReHand, _manager.GetLocalPlayerIndex(), _manager.SubTurn);
        }

        UIManager.Instance.ShowBTNOverlayUI(true);
        NextPhase();
    }

    protected override IEnumerator IEBeginPhase()
    {
        bool isShowTurnBoxComplete = false;
        _manager.ResetPlayCardCount();

        //Show Glow Player Turn
        UIManager.Instance.ShowTurnGlow(_manager.CurrentPlayerIndex);
        UIManager.Instance.ShowTurnBox(_manager.CurrentPlayerIndex, delegate
        {
            isShowTurnBoxComplete = true;
        });

        while (!_manager.IsCanNextPhase || !isShowTurnBoxComplete)
        {
            yield return null;
        }

        _manager.ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(new EventTriggerData(EventKey.BeginPhase)), true);

        while (!_manager.IsCanNextPhase)
        {
            yield return null;
        }

        if (_manager.IsNetworkPlay())
        {
            _manager.PhotonRequestPhaseReady(GamePhase.Begin, _manager.GetLocalPlayerIndex(), _manager.SubTurn);
        }

        while (!_manager.IsPhaseReady(GamePhase.Begin))
        {
            yield return null;
        }

        if (_manager.IsLocalPlayer(_manager.CurrentPlayerIndex))
        {
            UIManager.Instance.ShowHand();
        }

        // Add action log.

        if (_manager._isNewTurn)
        {
            TurnSummaryLog log = new TurnSummaryLog(_manager.Turn, _manager.Data.PlayerDataList[0], _manager.Data.PlayerDataList[1]);
            _manager.ActionAddBattleLog(log);
            _manager._isNewTurn = false;
        }

        {
            StartTurnLog log = new StartTurnLog(_manager.Turn, _manager.SubTurn, _manager.Data.PlayerDataList[(int)_manager.CurrentPlayerIndex]);
            _manager.ActionAddBattleLog(log);
        }

        // Play Hero Begin Voice.
        if (_manager.SubTurn == 1)
        {
            if (_manager.GetPlayerCount() == 2) // Is two players
            {
                PlayerIndex myIndex = _manager.GetLocalPlayerIndex();
                PlayerIndex enemyIndex;

                enemyIndex = GameManager.GetOpponentPlayerIndex(myIndex);

                System.Random r = new System.Random();
                _manager.ActionSendMessage(myIndex, HeroVoice.Begin, delegate ()
                {
                    _manager.ActionSendMessage(enemyIndex, HeroVoice.Begin, null);
                });
            }
        }

        NextPhase();
        yield break;
    }

    public override void OnSpiritPhase()
    {
        if (_isPhaseActive)
        {
            // Spirit phase update loop.
            if (_manager.IsCanNextPhase)
            {
                if (_manager.IsPhaseReady(GamePhase.Spirit))
                {
                    // Immune update
                    _manager.Data.PlayerDataList[(int)_manager.CurrentPlayerIndex].UpdateImmune();
                    NextPhase();

                }
                else if (!_manager.IsPhaseReady(GamePhase.Spirit, _manager.GetLocalPlayerIndex()))
                {
                    if (_manager.IsNetworkPlay())
                    {
                        _manager.PhotonRequestPhaseReady(GamePhase.Spirit, _manager.GetLocalPlayerIndex(), _manager.SubTurn);
                    }
                }
            }
        }
        else
        {
            // Spirit phase update loop first time.
            _isPhaseActive = true;

            switch (_manager.GetPlayerCount())
            {
                case 2:     // 2 players mode
                default:    // Free for all mode
                    {
                        int sum = _manager.GetSumSpirit(_manager.CurrentPlayerIndex);
                        foreach (PlayerIndex playerIndex in _manager.PlayerIndexList)
                        {
                            if (playerIndex != _manager.CurrentPlayerIndex)
                            {
                                _manager.RequestSpiritDamage(playerIndex, sum);
                            }
                        }
                    }
                    break;
            }

            // Update all player info 
            _manager.UpdateInfoAllPlayers();


            _manager.ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(new EventTriggerData(EventKey.PowerPhase)), true);
        }
    }

    public override void OnDrawPhase()
    {
        if (_isPhaseActive)
        {
            // Draw phase update loop.
            if (_manager.IsCanNextPhase)
            {
                if (_manager.IsPhaseReady(GamePhase.Draw))
                {
                    NextPhase();
                }
                else if (!_manager.IsPhaseReady(GamePhase.Draw, _manager.GetLocalPlayerIndex()))
                {
                    if (_manager.IsNetworkPlay())
                    {
                        _manager.PhotonRequestPhaseReady(GamePhase.Draw, _manager.GetLocalPlayerIndex(), _manager.SubTurn);
                    }
                }
            }
        }
        else
        {
            // Draw phase update loop first time.
            _isPhaseActive = true;
            _manager._playerDrawCount = 0;

            {
                int toDrawCount = _manager.MaxHandCard;
                int handCount = _manager.Data.PlayerDataList[(int)_manager.CurrentPlayerIndex].CardList[CardZone.Hand].Count;
                int deckCount = _manager.Data.PlayerDataList[(int)_manager.CurrentPlayerIndex].CardList[CardZone.Deck].Count;
                int handSlot = toDrawCount - handCount;

                int drawCount = Mathf.Min(handSlot, deckCount);
                int drawDamage = Mathf.Max(handSlot - drawCount, 0);

                _manager.RequestSetPlayerAP(_manager.CurrentPlayerIndex, 3);

                bool isDrawAP = false;

                if (_manager.SubTurn == 2) // draw AP card for 1st turn of 2nd player
                {
                    isDrawAP = true;
                }

                if (isDrawAP)
                {
                    _manager.RequestCreateCardToZone("CAP", _manager.CurrentPlayerIndex, CardZone.Hand, -1); // Add AP card.
                    drawCount--;
                }

                // Draw cards.
                if (drawCount > 0)
                {
                    _manager._playerDrawCount = drawCount;
                    _manager.RequestDrawCard(_manager.CurrentPlayerIndex, drawCount, false);
                }

                if (drawDamage > 0)
                {
                    _manager.RequestDrawDamage(_manager.CurrentPlayerIndex, drawDamage);
                }
            }


            _manager.ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(new EventTriggerData(EventKey.DrawPhase)), true);
        }
    }

    public override void OnPrePlayPhase()
    {
        if (_isPhaseActive)
        {
            // Pre-play phase update loop.

            if (_manager.IsCanNextPhase)
            {
                if (_manager.IsPhaseReady(GamePhase.PrePlay))
                {
                    PhaseLog log = new PhaseLog(DisplayGamePhase.Play);
                    _manager.ActionAddBattleLog(log);

                    NextPhase();
                }
                else if (!_manager.IsPhaseReady(GamePhase.PrePlay, _manager.GetLocalPlayerIndex()))
                {
                    if (_manager.IsNetworkPlay())
                    {
                        _manager.PhotonRequestPhaseReady(GamePhase.PrePlay, _manager.GetLocalPlayerIndex(), _manager.SubTurn);
                    }
                }
            }
        }
        else
        {
            // Pre-play phase update loop first time.

            _isPhaseActive = true;

            int handCount = _manager.Data.PlayerDataList[(int)_manager.CurrentPlayerIndex].CardList[CardZone.Hand].Count;
            int currentSP = _manager.GetSumSpirit(_manager.CurrentPlayerIndex);

            //if (_manager.EndType != GameManager.EndGameType.Normal)
            //{
            //    _manager.OnEndGame();
            //    return;
            //}

            _manager.UpdateLocalHandCard();

            if (_manager.CountdownTime > _manager.TurnTime)
            {
                _manager.CountdownTime = _manager.TurnTime;
            }

            _manager.ResetActiveCount();

            if (
                (
                    !_manager.IsCanUseHandCard(_manager.CurrentPlayerIndex)     // Out of AP
                    || _manager.IsEmptyHand(_manager.CurrentPlayerIndex)        // Empty hand
                    || !_manager.IsHaveEmptySlot()                              // No empty slot
                )
                && _manager.IsAutoEndTurn
            )
            {
                {
                    _manager._isStartWithEmptyHand = false;
                    _manager._timer = 0.0f;
                    _manager.SetActiveCount(1); // Set one action
                }
            }
            else
            {
                int afkCount = _manager._afkCount[(int)_manager.CurrentPlayerIndex];
                if (handCount <= 0)
                {
                    _manager._isStartWithEmptyHand = true;
                    _manager._timer = _manager.EmptyHandTurnTime;
                }
                else if (afkCount > 0)
                {
                    _manager._isStartWithEmptyHand = false;

                    switch (afkCount)
                    {
                        case 1: _manager._timer = _manager.AFKTime_1; break;
                        case 2: _manager._timer = _manager.AFKTime_2; break;

                        default: _manager._timer = _manager.AFKTime; break;
                    }
                }
                else
                {
                    _manager._isStartWithEmptyHand = false;
                    _manager._timer = _manager.TurnTime;
                }
            }
            _manager._startTurnStamp = DateTimeData.GetDateTimeUTC();

            UIManager.Instance.SetTargetTimer(Mathf.Min(_manager.CountdownTime, _manager._timer));
            _manager._isTimer = true;
            _manager._isEndTurn = false;
            _manager._isTimeoutCallEndTurn = false;
            _manager._turnTimer = 0.0f;
            _manager.IsTurnTimeout = false;

            // Trigger Analytic
            {
                _manager.AnalyticTriggerStartTurn();
            }

            _manager.ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(new EventTriggerData(EventKey.PrePlayPhase)), true);

            if (_manager.GetLocalPlayerIndex() == _manager.CurrentPlayerIndex)
            {


                if (handCount == 0 || currentSP > 0)
                {
                    UIManager.Instance.EndButtonIsOpen(true); // yellow
                }
                else
                {
                    UIManager.Instance.EndButtonIsOpen(true, true); // red
                }
            }
        }
    }

    public override void OnPlayPhase()
    {
        if (!_isPhaseActive)
        {
            // Play phase update loop first time.
            _isPhaseActive = true;

        }

        _manager.ActionGetUseCard();

        if (!_manager.IsEndTurn)
        {
            // Play phase update loop.
            if (_manager.IsTimeout)
            {
                _manager.IsTurnTimeout = true;

                if (!_manager._isTimeoutCallEndTurn)
                {
                    SoundManager.PlaySFX(SoundManager.SFX.Countdown_Ended);
                    _manager._isTimeoutCallEndTurn = true;
                }

                if (_manager.IsNetworkPlay())
                {
                    if (!_manager.IsDisconnect)
                    {
                        if (_manager.IsLocalPlayer(_manager.CurrentPlayerIndex))
                        {
                            _manager.RequestLocalEndTurn(false);
                        }
                        else
                        {
                            // wait other to send end turn.
                        }
                    }
                    else
                    {
                        if (_manager._isReconnectTimeout)
                        {
                            // Disconnect and not be return...
                            _manager.RequestEndTurn(_manager.CurrentPlayerIndex, _manager.SubTurn, false);
                        }
                        else
                        {
                            // wait reconnecting.
                        }
                    }
                }
                else
                {
                    _manager.RequestEndTurn(_manager.CurrentPlayerIndex, _manager.SubTurn, false);
                }
            }

            if (
                   (!_manager.IsCanUseHandCard(_manager.CurrentPlayerIndex)     // Out of AP
                || _manager.IsEmptyHand(_manager.CurrentPlayerIndex)            // Empty hand
                || !_manager.IsHaveEmptySlot())                                 // No empty slot
                && _manager.IsCanNextPhase
            )
            {
                if (_manager.IsAutoEndTurn)
                {
                    if (_manager.IsNetworkPlay())
                    {
                        if (_manager.IsLocalPlayer(_manager.CurrentPlayerIndex))
                        {
                            _manager.RequestLocalEndTurn(true);
                        }
                    }
                    else
                    {
                        _manager.RequestEndTurn(_manager.CurrentPlayerIndex, _manager.SubTurn, true);
                    }
                }
                else
                {
                    // Pop end turn button.
                    if (_manager.IsCanNextPhase)
                    {
                        if (_manager.GetLocalPlayerIndex() == _manager.CurrentPlayerIndex)
                        {
                            UIManager.Instance.EndButtonIsOpen(true);
                            UIManager.Instance.EndTurnIsFinish(true);
                        }
                    }
                }
            }

            _manager.UpdateLocalHandCard();
        }
        else
        {
            if (_manager.IsCanNextPhase)
            {
                if (_manager.IsPhaseReady(GamePhase.Play))
                {
                    NextPhase();
                }
                else if (!_manager.IsPhaseReady(GamePhase.Play, _manager.GetLocalPlayerIndex()))
                {
                    if (_manager.IsNetworkPlay())
                    {
                        _manager.PhotonRequestPhaseReady(GamePhase.Play, _manager.GetLocalPlayerIndex(), _manager.SubTurn);
                    }
                }
            }
        }
    }

    public override void OnPreBattlePhase()
    {
        if (_isPhaseActive)
        {
            // Pre-battle phase update loop.

            if (_manager.IsCanNextPhase)
            {
                if (_manager.IsPhaseReady(GamePhase.PreBattle))
                {
                    PhaseLog log = new PhaseLog(DisplayGamePhase.Battle);
                    _manager.ActionAddBattleLog(log);
                    NextPhase();
                }
                else if (!_manager.IsPhaseReady(GamePhase.PreBattle, _manager.GetLocalPlayerIndex()))
                {
                    if (_manager.IsNetworkPlay())
                    {
                        _manager.PhotonRequestPhaseReady(GamePhase.PreBattle, _manager.GetLocalPlayerIndex(), _manager.SubTurn);
                    }
                }
            }
        }
        else
        {
            // Pre-battle phase update loop first time.

            _isPhaseActive = true;

            //if (_manager.EndType != GameManager.EndGameType.Normal)
            //{
            //    _manager.OnEndGame();
            //    return;
            //}

            _manager.ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(new EventTriggerData(EventKey.PreBattlePhase)), true);
        }
    }

    public override void OnPostBattlePhase()
    {
        if (_isPhaseActive)
        {
            // Post-battle phase update loop.

            if (_manager.IsCanNextPhase)
            {
                if (_manager.IsPhaseReady(GamePhase.PostBattle))
                {
                    NextPhase();
                }
                else if (!_manager.IsPhaseReady(GamePhase.PostBattle, _manager.GetLocalPlayerIndex()))
                {
                    if (_manager.IsNetworkPlay())
                    {
                        _manager.PhotonRequestPhaseReady(GamePhase.PostBattle, _manager.GetLocalPlayerIndex(), _manager.SubTurn);
                    }
                }
            }
        }
        else
        {
            // Post-battle phase update loop first time.

            //if (_manager.EndType != GameManager.EndGameType.Normal)
            //{
            //    _manager.OnEndGame();
            //    return;
            //}

            _isPhaseActive = true;
            _manager.ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(new EventTriggerData(EventKey.PostBattlePhase)), true);

            // Refresh Card
            int maxSlotID = GameManager.Instance.BoardHeight * GameManager.Instance.BoardWidth;
            for (int slotID=0;slotID < maxSlotID;slotID++)
            {
                Karamucho.UI.CardUI card = _manager.GetBoardManager().GetTokenBySlotID(slotID);
                if (card != null)
                {
                    card.RefreshCardPosition();
                }
            }
           
        }
    }

    public override void OnEndPhase()
    {
        if (_isPhaseActive)
        {
            // End phase update loop.
            if (_manager.IsCanNextPhase)
            {
                if (_manager.IsPhaseReady(GamePhase.End))
                {
                    NextPhase();
                    _manager.NextPlayer();
                }
                else if (!_manager.IsPhaseReady(GamePhase.End, _manager.GetLocalPlayerIndex()))
                {
                    if (_manager.IsNetworkPlay())
                    {
                        _manager.PhotonRequestPhaseReady(GamePhase.End, _manager.GetLocalPlayerIndex(), _manager.SubTurn);
                    }
                }
            }
        }
        else
        {
            // End phase update loop first time.

            _isPhaseActive = true;
            _manager.IsCallEndTurn = false;
            _manager.ResetPlayCardCount();

            if (_manager._attackerList == null)
            {
                _manager._attackerList = new List<MinionData>();
            }
            else
            {
                _manager._attackerListCount = _manager._attackerList.Count;
                _manager._attackerList.Clear();
            }

            // Trigger Analytic
            {
                _manager.AnalyticTriggerEndTurn();
            }

            _manager.ActionAddCommandListToSecondQueue(EventTrigger.TriggerCommandList(new EventTriggerData(EventKey.EndPhase)), true);
        }
    }

    #endregion

    #region Methods

    public override bool IsValidSlotID(int slotID)
    {
        if (slotID >= 0 && slotID < (_manager.BoardWidth * _manager.BoardHeight))
        {
            return true;
        }
        return false;
    }

    public override int GetLifePoint(PlayerIndex playerIndex, GameMode mode)
    {
        return _manager.Lifepoint;
    }

    #endregion
}
