﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventBattleController : DefaultController
{
    public EventBattleController(GameManager manager) : base(manager)
    {
    }
    
    public override IEnumerator SetupPhase()
    {
        if (EventBattleDB.Instance.IsVSBot)
        {
            //if (GameHelper.IsEventBattle_PreGrandMaster)
            //{
            //    // ใน Event Battle - Pre-GrandMaster
            //    // 1. บอทล็อคมือ
            //    // 2. บอทเริ่มก่อนเสมอ
            //    // 3. บอท Hard/Defensive
            //    _manager.SetPlayOrder(PlayerIndex.Two, null);
            //}
            //else
            //{
            //    _manager.ActionRandomPlayOrder(null);
            //}

            // ใน Event Battle - Pre-GrandMaster
            // 1. บอทล็อคมือ
            // 2. บอทเริ่มก่อนเสมอ
            // 3. บอท Hard/Defensive
            _manager.SetPlayOrder(PlayerIndex.Two, null);
        }

        _manager._isPlayFirstReady = true;
        while (!_manager.IsCanNextPhase || !_manager.IsPhaseReady(GamePhase.SetupData) || !_manager._isPlayFirstReady)
        {
            yield return null;
        }

        //bool isFinish = false;
        //PlayerStatManager.Instance.DoUpdatePlayerMatchTicket(DataManager.Instance.GetRoomName(), () => isFinish = true);
        //yield return new WaitUntil(() => isFinish == true);

        _manager.SetRandomSeed(_manager.MatchData.PlayerData[1].RandomSeed);
        _manager.SetLoadingProgress(1.0f);

        NextPhase();
    }

    protected override IEnumerator ReHandPhase()
    {
        // stamp match ticket
        bool isFinish = false;
        PlayerStatManager.Instance.DoUpdatePlayerMatchTicket(DataManager.Instance.GetRoomName(), () => isFinish = true);
        yield return new WaitUntil(() => isFinish == true);

        // re-hand
        _manager.ReHand();

        while (!_manager.IsCanNextPhase || !_manager._isConfirmReHand[(int)_manager.GetLocalPlayerIndex()])
        {
            yield return null;
        }

        UIManager.Instance.HideRedraw();

        // Show waiting
        UIManager.Instance.ShowWaiting();

        while (!_manager.IsCanNextPhase || !_manager.IsConfirmRehand)
        {
            yield return null;
        }

        // Hide waiting
        UIManager.Instance.HideWaiting();
        SoundManager.PlayEventBGM(5.0f);
        UIManager.Instance.ShowBTNOverlayUI(true);
        NextPhase();
    }

    public override int GetLifePoint(PlayerIndex playerIndex, GameMode mode)
    {
        List<EventBattleDBData> profileList;
        if (!EventBattleDB.Instance.GetAllData(out profileList))
        {
            return EventBattleDBData.DEFAULT_HP;
        }

        if (playerIndex == PlayerIndex.One)
        {
            return profileList[0].HP;
        }
        else
        {
            if (EventBattleDB.Instance.IsVSBot)
            {
                return profileList[profileList.Count - 1].HP;
            }
            else
            {
                return profileList[1].HP;
            }
        }
    }
}
