
Hero_Jack_Helloween19.png
size: 4096,4096
format: RGBA8888
filter: Linear,Linear
repeat: none
Hat
  rotate: false
  xy: 683, 136
  size: 1055, 631
  orig: 1055, 631
  offset: 0, 0
  index: -1
Hat_p_01
  rotate: false
  xy: 1576, 2306
  size: 258, 179
  orig: 258, 179
  offset: 0, 0
  index: -1
Hat_p_02
  rotate: false
  xy: 611, 2141
  size: 245, 196
  orig: 245, 196
  offset: 0, 0
  index: -1
Hat_p_03
  rotate: true
  xy: 4, 2387
  size: 98, 126
  orig: 98, 126
  offset: 0, 0
  index: -1
Hat_p_04
  rotate: false
  xy: 335, 2368
  size: 140, 117
  orig: 140, 117
  offset: 0, 0
  index: -1
Scarf
  rotate: false
  xy: 2841, 1704
  size: 435, 322
  orig: 435, 322
  offset: 0, 0
  index: -1
Scarf_tail_01
  rotate: false
  xy: 3373, 2025
  size: 618, 266
  orig: 618, 266
  offset: 0, 0
  index: -1
Scarf_tail_02
  rotate: false
  xy: 2783, 529
  size: 1212, 582
  orig: 1212, 582
  offset: 0, 0
  index: -1
Scarf_tail_03
  rotate: false
  xy: 2905, 2030
  size: 464, 261
  orig: 464, 261
  offset: 0, 0
  index: -1
arm_L_01
  rotate: false
  xy: 2077, 1745
  size: 406, 298
  orig: 406, 298
  offset: 0, 0
  index: -1
arm_L_02
  rotate: true
  xy: 1638, 1772
  size: 282, 435
  orig: 282, 435
  offset: 0, 0
  index: -1
arm_L_03
  rotate: true
  xy: 276, 1860
  size: 277, 343
  orig: 277, 343
  offset: 0, 0
  index: -1
arm_L_04
  rotate: false
  xy: 2487, 1711
  size: 350, 329
  orig: 350, 329
  offset: 0, 0
  index: -1
arm_R_01
  rotate: true
  xy: 3318, 1115
  size: 497, 527
  orig: 497, 527
  offset: 0, 0
  index: -1
arm_R_02
  rotate: false
  xy: 1742, 4
  size: 644, 644
  orig: 644, 644
  offset: 0, 0
  index: -1
arm_R_07
  rotate: true
  xy: 2251, 1284
  size: 423, 491
  orig: 423, 491
  offset: 0, 0
  index: -1
arm_R_B_01
  rotate: true
  xy: 2114, 652
  size: 580, 665
  orig: 580, 665
  offset: 0, 0
  index: -1
arm_R_B_02
  rotate: false
  xy: 4, 323
  size: 675, 640
  orig: 675, 640
  offset: 0, 0
  index: -1
bag
  rotate: true
  xy: 1155, 2110
  size: 214, 218
  orig: 214, 218
  offset: 0, 0
  index: -1
belt
  rotate: false
  xy: 1838, 2299
  size: 407, 186
  orig: 407, 186
  offset: 0, 0
  index: -1
coat_L
  rotate: true
  xy: 2531, 2044
  size: 247, 370
  orig: 247, 370
  offset: 0, 0
  index: -1
coat_LB_01
  rotate: false
  xy: 4, 2191
  size: 254, 191
  orig: 254, 191
  offset: 0, 0
  index: -1
coat_R
  rotate: true
  xy: 4, 967
  size: 528, 676
  orig: 528, 676
  offset: 0, 0
  index: -1
eye_01
  rotate: false
  xy: 2249, 2295
  size: 345, 190
  orig: 345, 190
  offset: 0, 0
  index: -1
eye_02
  rotate: false
  xy: 479, 2352
  size: 336, 133
  orig: 336, 133
  offset: 0, 0
  index: -1
eye_03
  rotate: false
  xy: 819, 2341
  size: 337, 144
  orig: 337, 144
  offset: 0, 0
  index: -1
eye_EM01
  rotate: false
  xy: 2598, 2295
  size: 345, 190
  orig: 345, 190
  offset: 0, 0
  index: -1
eye_EM02
  rotate: false
  xy: 2947, 2295
  size: 345, 190
  orig: 345, 190
  offset: 0, 0
  index: -1
eye_EM04
  rotate: false
  xy: 3296, 2295
  size: 345, 190
  orig: 345, 190
  offset: 0, 0
  index: -1
eye_EM05
  rotate: false
  xy: 3645, 2295
  size: 345, 190
  orig: 345, 190
  offset: 0, 0
  index: -1
eye_EM06
  rotate: false
  xy: 262, 2158
  size: 345, 190
  orig: 345, 190
  offset: 0, 0
  index: -1
foot_L
  rotate: true
  xy: 1354, 2328
  size: 157, 218
  orig: 157, 218
  offset: 0, 0
  index: -1
hand_L_01
  rotate: true
  xy: 1902, 2058
  size: 237, 248
  orig: 237, 248
  offset: 0, 0
  index: -1
hand_R_01
  rotate: true
  xy: 3713, 1616
  size: 405, 354
  orig: 405, 354
  offset: 0, 0
  index: -1
hand_R_02
  rotate: false
  xy: 1377, 2086
  size: 280, 216
  orig: 280, 216
  offset: 0, 0
  index: -1
hand_R_03
  rotate: false
  xy: 2154, 2047
  size: 373, 244
  orig: 373, 244
  offset: 0, 0
  index: -1
hand_R_04
  rotate: true
  xy: 1796, 1331
  size: 410, 451
  orig: 410, 451
  offset: 0, 0
  index: -1
hand_R_05
  rotate: false
  xy: 974, 1806
  size: 349, 300
  orig: 349, 300
  offset: 0, 0
  index: -1
hand_R_06
  rotate: true
  xy: 4, 1502
  size: 354, 473
  orig: 354, 473
  offset: 0, 0
  index: -1
hand_R_07
  rotate: true
  xy: 890, 1403
  size: 371, 449
  orig: 371, 449
  offset: 0, 0
  index: -1
hand_R_07_01
  rotate: true
  xy: 1343, 1397
  size: 371, 449
  orig: 371, 449
  offset: 0, 0
  index: -1
hand_R_08
  rotate: false
  xy: 481, 1499
  size: 405, 354
  orig: 405, 354
  offset: 0, 0
  index: -1
head
  rotate: false
  xy: 684, 851
  size: 635, 548
  orig: 635, 548
  offset: 0, 0
  index: -1
lag_L_01
  rotate: false
  xy: 1327, 1778
  size: 307, 304
  orig: 307, 304
  offset: 0, 0
  index: -1
lag_R_01
  rotate: true
  xy: 1323, 771
  size: 556, 787
  orig: 556, 787
  offset: 0, 0
  index: -1
main_cloth
  rotate: false
  xy: 2746, 1236
  size: 568, 443
  orig: 568, 443
  offset: 0, 0
  index: -1
mouth
  rotate: false
  xy: 134, 2386
  size: 197, 99
  orig: 197, 99
  offset: 0, 0
  index: -1
should_R04_07
  rotate: false
  xy: 860, 2139
  size: 291, 198
  orig: 291, 198
  offset: 0, 0
  index: -1
shouldbow_L01
  rotate: false
  xy: 1661, 2062
  size: 237, 233
  orig: 237, 233
  offset: 0, 0
  index: -1
shouldbow_L02
  rotate: true
  xy: 1160, 2337
  size: 148, 190
  orig: 148, 190
  offset: 0, 0
  index: -1
shouldbow_R04
  rotate: false
  xy: 4, 1888
  size: 268, 266
  orig: 268, 266
  offset: 0, 0
  index: -1
shouldbow_R05
  rotate: false
  xy: 3280, 1683
  size: 429, 338
  orig: 429, 338
  offset: 0, 0
  index: -1
shouldbow_R05_01
  rotate: false
  xy: 623, 1857
  size: 347, 278
  orig: 347, 278
  offset: 0, 0
  index: -1
