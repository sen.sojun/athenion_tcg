
Hero_Sadia_School19.png
size: 4096,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
B_Collar
  rotate: false
  xy: 1102, 4
  size: 136, 56
  orig: 136, 56
  offset: 0, 0
  index: -1
B_Hair
  rotate: true
  xy: 4, 6
  size: 303, 299
  orig: 303, 299
  offset: 0, 0
  index: -1
Bag
  rotate: false
  xy: 477, 31
  size: 253, 284
  orig: 253, 284
  offset: 0, 0
  index: -1
Body
  rotate: false
  xy: 569, 639
  size: 254, 381
  orig: 254, 381
  offset: 0, 0
  index: -1
Book
  rotate: true
  xy: 1271, 784
  size: 236, 149
  orig: 236, 149
  offset: 0, 0
  index: -1
Book_Co1
  rotate: true
  xy: 399, 319
  size: 242, 103
  orig: 242, 103
  offset: 0, 0
  index: -1
Book_Co1a
  rotate: true
  xy: 2084, 890
  size: 130, 117
  orig: 130, 117
  offset: 0, 0
  index: -1
Book_Co2
  rotate: false
  xy: 1656, 812
  size: 150, 208
  orig: 150, 208
  offset: 0, 0
  index: -1
Book_M1
  rotate: true
  xy: 1533, 177
  size: 183, 149
  orig: 183, 149
  offset: 0, 0
  index: -1
Book_Th1
  rotate: false
  xy: 734, 52
  size: 340, 266
  orig: 340, 266
  offset: 0, 0
  index: -1
Bread
  rotate: true
  xy: 1928, 290
  size: 122, 89
  orig: 122, 89
  offset: 0, 0
  index: -1
Bread2
  rotate: true
  xy: 2009, 512
  size: 122, 89
  orig: 122, 89
  offset: 0, 0
  index: -1
Bread_G
  rotate: true
  xy: 1927, 126
  size: 128, 118
  orig: 128, 118
  offset: 0, 0
  index: -1
Collar
  rotate: true
  xy: 1589, 593
  size: 210, 121
  orig: 210, 121
  offset: 0, 0
  index: -1
F_Hair
  rotate: false
  xy: 1092, 645
  size: 132, 135
  orig: 132, 135
  offset: 0, 0
  index: -1
Face
  rotate: false
  xy: 1426, 364
  size: 177, 220
  orig: 177, 220
  offset: 0, 0
  index: -1
L_Arm
  rotate: true
  xy: 1810, 845
  size: 175, 142
  orig: 175, 142
  offset: 0, 0
  index: -1
L_Arm_Co1
  rotate: true
  xy: 1360, 588
  size: 192, 75
  orig: 192, 75
  offset: 0, 0
  index: -1
L_Arm_Co2
  rotate: true
  xy: 1714, 631
  size: 177, 158
  orig: 177, 158
  offset: 0, 0
  index: -1
L_Arm_M1
  rotate: false
  xy: 1251, 580
  size: 105, 200
  orig: 105, 200
  offset: 0, 0
  index: -1
L_Bag
  rotate: false
  xy: 307, 11
  size: 166, 298
  orig: 166, 298
  offset: 0, 0
  index: -1
L_Ear
  rotate: true
  xy: 1482, 334
  size: 26, 45
  orig: 26, 45
  offset: 0, 0
  index: -1
L_Eye1
  rotate: true
  xy: 1297, 6
  size: 58, 50
  orig: 58, 50
  offset: 0, 0
  index: -1
L_Eye2
  rotate: true
  xy: 1351, 6
  size: 58, 50
  orig: 58, 50
  offset: 0, 0
  index: -1
L_Eye3
  rotate: true
  xy: 1405, 9
  size: 55, 50
  orig: 55, 50
  offset: 0, 0
  index: -1
L_Eye4
  rotate: true
  xy: 1753, 412
  size: 61, 48
  orig: 61, 48
  offset: 0, 0
  index: -1
L_Eye5
  rotate: true
  xy: 1805, 412
  size: 61, 48
  orig: 61, 48
  offset: 0, 0
  index: -1
L_Eye6
  rotate: true
  xy: 1857, 412
  size: 61, 48
  orig: 61, 48
  offset: 0, 0
  index: -1
L_Eye7
  rotate: true
  xy: 1909, 416
  size: 61, 48
  orig: 61, 48
  offset: 0, 0
  index: -1
L_Eye8
  rotate: true
  xy: 1961, 416
  size: 61, 48
  orig: 61, 48
  offset: 0, 0
  index: -1
L_Eyebrow
  rotate: true
  xy: 858, 565
  size: 72, 43
  orig: 72, 43
  offset: 0, 0
  index: -1
L_Eyebrow2
  rotate: true
  xy: 905, 565
  size: 72, 43
  orig: 72, 43
  offset: 0, 0
  index: -1
L_Eyebrow3
  rotate: true
  xy: 952, 565
  size: 72, 43
  orig: 72, 43
  offset: 0, 0
  index: -1
L_Finger
  rotate: true
  xy: 1242, 5
  size: 59, 51
  orig: 59, 51
  offset: 0, 0
  index: -1
L_Finger_Co1
  rotate: true
  xy: 933, 12
  size: 36, 53
  orig: 36, 53
  offset: 0, 0
  index: -1
L_Finger_Th1
  rotate: false
  xy: 999, 573
  size: 82, 64
  orig: 82, 64
  offset: 0, 0
  index: -1
L_Hair
  rotate: false
  xy: 288, 313
  size: 107, 248
  orig: 107, 248
  offset: 0, 0
  index: -1
L_HairTail
  rotate: false
  xy: 723, 322
  size: 330, 239
  orig: 330, 239
  offset: 0, 0
  index: -1
L_HairTail2
  rotate: false
  xy: 1640, 7
  size: 176, 166
  orig: 176, 166
  offset: 0, 0
  index: -1
L_Hand
  rotate: false
  xy: 2013, 421
  size: 105, 87
  orig: 105, 87
  offset: 0, 0
  index: -1
L_Hand_Co1
  rotate: true
  xy: 1424, 797
  size: 223, 115
  orig: 223, 115
  offset: 0, 0
  index: -1
L_Hand_Co2
  rotate: false
  xy: 1820, 5
  size: 96, 115
  orig: 96, 115
  offset: 0, 0
  index: -1
L_Hand_M1
  rotate: false
  xy: 2102, 518
  size: 78, 117
  orig: 78, 117
  offset: 0, 0
  index: -1
L_Hand_Th1
  rotate: false
  xy: 1920, 5
  size: 96, 115
  orig: 96, 115
  offset: 0, 0
  index: -1
L_Leg
  rotate: false
  xy: 4, 313
  size: 280, 707
  orig: 280, 707
  offset: 0, 0
  index: -1
L_Ribbon
  rotate: true
  xy: 767, 569
  size: 66, 87
  orig: 66, 87
  offset: 0, 0
  index: -1
L_Shoulder
  rotate: true
  xy: 506, 320
  size: 241, 213
  orig: 241, 213
  offset: 0, 0
  index: -1
L_Shoulder_Co2
  rotate: false
  xy: 1078, 64
  size: 109, 264
  orig: 109, 264
  offset: 0, 0
  index: -1
L_Shoulder_M1
  rotate: false
  xy: 1543, 807
  size: 109, 213
  orig: 109, 213
  offset: 0, 0
  index: -1
Mouth
  rotate: false
  xy: 541, 7
  size: 45, 20
  orig: 45, 20
  offset: 0, 0
  index: -1
Mouth2
  rotate: true
  xy: 1876, 635
  size: 52, 45
  orig: 52, 45
  offset: 0, 0
  index: -1
Mouth3
  rotate: false
  xy: 990, 21
  size: 52, 27
  orig: 52, 27
  offset: 0, 0
  index: -1
Mouth4
  rotate: false
  xy: 1046, 21
  size: 52, 27
  orig: 52, 27
  offset: 0, 0
  index: -1
Mouth5
  rotate: false
  xy: 1426, 333
  size: 52, 27
  orig: 52, 27
  offset: 0, 0
  index: -1
Mouth6
  rotate: false
  xy: 734, 6
  size: 36, 42
  orig: 36, 42
  offset: 0, 0
  index: -1
Nose
  rotate: false
  xy: 902, 10
  size: 27, 38
  orig: 27, 38
  offset: 0, 0
  index: -1
R_Arm
  rotate: false
  xy: 1444, 140
  size: 85, 189
  orig: 85, 189
  offset: 0, 0
  index: -1
R_Arm_G1
  rotate: false
  xy: 1686, 232
  size: 110, 171
  orig: 110, 171
  offset: 0, 0
  index: -1
R_Arm_Thx1
  rotate: true
  xy: 569, 565
  size: 70, 194
  orig: 70, 194
  offset: 0, 0
  index: -1
R_Bag1
  rotate: true
  xy: 1820, 124
  size: 130, 103
  orig: 130, 103
  offset: 0, 0
  index: -1
R_Bag2
  rotate: true
  xy: 1085, 575
  size: 62, 162
  orig: 62, 162
  offset: 0, 0
  index: -1
R_Eye1
  rotate: false
  xy: 1978, 638
  size: 48, 49
  orig: 48, 49
  offset: 0, 0
  index: -1
R_Eye2
  rotate: false
  xy: 2098, 368
  size: 48, 49
  orig: 48, 49
  offset: 0, 0
  index: -1
R_Eye3
  rotate: false
  xy: 1925, 638
  size: 49, 49
  orig: 49, 49
  offset: 0, 0
  index: -1
R_Eye4
  rotate: true
  xy: 1686, 178
  size: 50, 45
  orig: 50, 45
  offset: 0, 0
  index: -1
R_Eye5
  rotate: true
  xy: 2021, 271
  size: 61, 48
  orig: 61, 48
  offset: 0, 0
  index: -1
R_Eye6
  rotate: true
  xy: 2049, 206
  size: 61, 48
  orig: 61, 48
  offset: 0, 0
  index: -1
R_Eye7
  rotate: true
  xy: 2073, 271
  size: 61, 48
  orig: 61, 48
  offset: 0, 0
  index: -1
R_Eye8
  rotate: true
  xy: 2049, 141
  size: 61, 48
  orig: 61, 48
  offset: 0, 0
  index: -1
R_Eyebrow
  rotate: false
  xy: 477, 6
  size: 60, 21
  orig: 60, 21
  offset: 0, 0
  index: -1
R_Eyebrow2
  rotate: false
  xy: 774, 9
  size: 60, 39
  orig: 60, 39
  offset: 0, 0
  index: -1
R_Eyebrow3
  rotate: false
  xy: 838, 9
  size: 60, 39
  orig: 60, 39
  offset: 0, 0
  index: -1
R_Finger
  rotate: false
  xy: 2030, 639
  size: 81, 73
  orig: 81, 73
  offset: 0, 0
  index: -1
R_Finger_G1
  rotate: false
  xy: 2115, 639
  size: 81, 73
  orig: 81, 73
  offset: 0, 0
  index: -1
R_Finger_G2
  rotate: true
  xy: 2021, 336
  size: 81, 73
  orig: 81, 73
  offset: 0, 0
  index: -1
R_Hair
  rotate: false
  xy: 1244, 334
  size: 178, 237
  orig: 178, 237
  offset: 0, 0
  index: -1
R_HairTail
  rotate: false
  xy: 1191, 68
  size: 249, 260
  orig: 249, 260
  offset: 0, 0
  index: -1
R_HairTail2
  rotate: false
  xy: 1439, 603
  size: 146, 190
  orig: 146, 190
  offset: 0, 0
  index: -1
R_Hand
  rotate: false
  xy: 2020, 4
  size: 85, 118
  orig: 85, 118
  offset: 0, 0
  index: -1
R_Hand_G1
  rotate: false
  xy: 1753, 477
  size: 124, 150
  orig: 124, 150
  offset: 0, 0
  index: -1
R_Hand_G2
  rotate: false
  xy: 1800, 258
  size: 124, 150
  orig: 124, 150
  offset: 0, 0
  index: -1
R_Hand_Thx1
  rotate: false
  xy: 1876, 691
  size: 124, 150
  orig: 124, 150
  offset: 0, 0
  index: -1
R_Hand_Thx2
  rotate: false
  xy: 1881, 481
  size: 124, 150
  orig: 124, 150
  offset: 0, 0
  index: -1
R_Hand_Thx3
  rotate: false
  xy: 1956, 870
  size: 124, 150
  orig: 124, 150
  offset: 0, 0
  index: -1
R_Hand_Thx4
  rotate: false
  xy: 2004, 716
  size: 124, 150
  orig: 124, 150
  offset: 0, 0
  index: -1
R_Leg
  rotate: true
  xy: 288, 565
  size: 455, 277
  orig: 455, 277
  offset: 0, 0
  index: -1
R_Ribbon
  rotate: true
  xy: 1735, 179
  size: 49, 81
  orig: 49, 81
  offset: 0, 0
  index: -1
R_Shoulder
  rotate: false
  xy: 1057, 332
  size: 183, 237
  orig: 183, 237
  offset: 0, 0
  index: -1
R_Shoulder_Thx1
  rotate: false
  xy: 1092, 784
  size: 175, 236
  orig: 175, 236
  offset: 0, 0
  index: -1
Ribbon
  rotate: false
  xy: 1459, 4
  size: 177, 132
  orig: 177, 132
  offset: 0, 0
  index: -1
Skirt
  rotate: true
  xy: 827, 641
  size: 379, 261
  orig: 379, 261
  offset: 0, 0
  index: -1
Waist
  rotate: true
  xy: 1607, 407
  size: 182, 142
  orig: 182, 142
  offset: 0, 0
  index: -1
