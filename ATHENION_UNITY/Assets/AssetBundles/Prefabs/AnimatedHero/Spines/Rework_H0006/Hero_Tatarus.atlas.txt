
Hero_Tatarus.png
size: 4096,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
01Thank/01Thank_Rhand
  rotate: false
  xy: 3739, 229
  size: 152, 168
  orig: 152, 168
  offset: 0, 0
  index: -1
03Threaten/03Threaten_Lhand_1
  rotate: false
  xy: 3012, 76
  size: 182, 184
  orig: 182, 184
  offset: 0, 0
  index: -1
03Threaten/03Threaten_Lhand_2
  rotate: true
  xy: 2157, 5
  size: 146, 184
  orig: 146, 184
  offset: 0, 0
  index: -1
03Threaten/03Threaten_eff_eye_1
  rotate: true
  xy: 2061, 152
  size: 117, 73
  orig: 117, 73
  offset: 0, 0
  index: -1
03Threaten/03Threaten_eff_eye_2
  rotate: false
  xy: 1307, 15
  size: 336, 113
  orig: 336, 113
  offset: 0, 0
  index: -1
03Threaten/03Threaten_eff_eye_3
  rotate: false
  xy: 1307, 132
  size: 440, 90
  orig: 440, 90
  offset: 0, 0
  index: -1
04Greeting/04Greeting_Lhand
  rotate: false
  xy: 3580, 192
  size: 155, 205
  orig: 155, 205
  offset: 0, 0
  index: -1
04Greeting/04Greeting_Rhand
  rotate: true
  xy: 3356, 13
  size: 170, 192
  orig: 170, 192
  offset: 0, 0
  index: -1
06Mistake/06Mistake_Rhand
  rotate: true
  xy: 1647, 12
  size: 116, 220
  orig: 116, 220
  offset: 0, 0
  index: -1
Eye_L
  rotate: false
  xy: 1835, 227
  size: 49, 42
  orig: 49, 42
  offset: 0, 0
  index: -1
Eye_L_c
  rotate: false
  xy: 1888, 227
  size: 49, 42
  orig: 49, 42
  offset: 0, 0
  index: -1
Eye_R
  rotate: false
  xy: 2212, 156
  size: 62, 44
  orig: 62, 44
  offset: 0, 0
  index: -1
Eye_R_c
  rotate: false
  xy: 1767, 228
  size: 64, 41
  orig: 64, 41
  offset: 0, 0
  index: -1
Lhand
  rotate: false
  xy: 2834, 108
  size: 174, 218
  orig: 174, 218
  offset: 0, 0
  index: -1
Llowerarm
  rotate: false
  xy: 4, 6
  size: 326, 198
  orig: 326, 198
  offset: 0, 0
  index: -1
Lupperarm
  rotate: true
  xy: 2301, 279
  size: 296, 477
  orig: 296, 477
  offset: 0, 0
  index: -1
Mouth
  rotate: true
  xy: 2267, 208
  size: 61, 50
  orig: 61, 50
  offset: 0, 0
  index: -1
Mouth2
  rotate: true
  xy: 2321, 214
  size: 61, 50
  orig: 61, 50
  offset: 0, 0
  index: -1
Mouth3
  rotate: false
  xy: 2321, 160
  size: 60, 50
  orig: 60, 50
  offset: 0, 0
  index: -1
Mouth4
  rotate: true
  xy: 2375, 215
  size: 60, 50
  orig: 60, 50
  offset: 0, 0
  index: -1
Mouth6
  rotate: false
  xy: 2138, 203
  size: 65, 66
  orig: 65, 66
  offset: 0, 0
  index: -1
Mouth7
  rotate: true
  xy: 2207, 204
  size: 65, 56
  orig: 65, 56
  offset: 0, 0
  index: -1
Nose
  rotate: true
  xy: 2385, 161
  size: 50, 55
  orig: 50, 55
  offset: 0, 0
  index: -1
Rhand
  rotate: false
  xy: 2345, 9
  size: 179, 147
  orig: 179, 147
  offset: 0, 0
  index: -1
Rlowerarm
  rotate: false
  xy: 2782, 330
  size: 247, 253
  orig: 247, 253
  offset: 0, 0
  index: -1
Rupperarm
  rotate: true
  xy: 334, 26
  size: 178, 330
  orig: 178, 330
  offset: 0, 0
  index: -1
backhair
  rotate: false
  xy: 2528, 19
  size: 302, 256
  orig: 302, 256
  offset: 0, 0
  index: -1
backwaistcloth
  rotate: false
  xy: 1767, 273
  size: 530, 279
  orig: 530, 279
  offset: 0, 0
  index: -1
belteye
  rotate: true
  xy: 668, 25
  size: 179, 269
  orig: 179, 269
  offset: 0, 0
  index: -1
body
  rotate: true
  xy: 2985, 625
  size: 394, 489
  orig: 394, 489
  offset: 0, 0
  index: -1
cape
  rotate: true
  xy: 3478, 401
  size: 618, 590
  orig: 618, 590
  offset: 0, 0
  index: -1
ear
  rotate: false
  xy: 1929, 146
  size: 128, 77
  orig: 128, 77
  offset: 0, 0
  index: -1
earhair
  rotate: false
  xy: 3198, 46
  size: 154, 214
  orig: 154, 214
  offset: 0, 0
  index: -1
eyebrow_L
  rotate: false
  xy: 1941, 237
  size: 54, 32
  orig: 54, 32
  offset: 0, 0
  index: -1
eyebrow_R
  rotate: false
  xy: 2138, 155
  size: 70, 44
  orig: 70, 44
  offset: 0, 0
  index: -1
face
  rotate: false
  xy: 3415, 187
  size: 161, 210
  orig: 161, 210
  offset: 0, 0
  index: -1
frontcape
  rotate: false
  xy: 2493, 587
  size: 488, 432
  orig: 488, 432
  offset: 0, 0
  index: -1
fronthair
  rotate: true
  xy: 3552, 13
  size: 170, 191
  orig: 170, 191
  offset: 0, 0
  index: -1
fronthair2
  rotate: true
  xy: 1751, 145
  size: 77, 174
  orig: 77, 174
  offset: 0, 0
  index: -1
horn
  rotate: true
  xy: 2002, 36
  size: 106, 151
  orig: 106, 151
  offset: 0, 0
  index: -1
legL
  rotate: true
  xy: 807, 556
  size: 463, 1051
  orig: 463, 1051
  offset: 0, 0
  index: -1
legR
  rotate: true
  xy: 807, 226
  size: 326, 956
  orig: 326, 956
  offset: 0, 0
  index: -1
lioncloth
  rotate: true
  xy: 1862, 579
  size: 440, 627
  orig: 440, 627
  offset: 0, 0
  index: -1
neck
  rotate: false
  xy: 1871, 4
  size: 127, 137
  orig: 127, 137
  offset: 0, 0
  index: -1
tail
  rotate: true
  xy: 4, 208
  size: 811, 799
  orig: 811, 799
  offset: 0, 0
  index: -1
waistclothL
  rotate: false
  xy: 3033, 264
  size: 378, 357
  orig: 378, 357
  offset: 0, 0
  index: -1
waistclothR
  rotate: true
  xy: 941, 7
  size: 215, 362
  orig: 215, 362
  offset: 0, 0
  index: -1
