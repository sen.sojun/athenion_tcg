
Hero_Tear_Valentine20.png
size: 4096,2048
format: RGBA8888
filter: Linear,Linear
repeat: none
Acc
  rotate: true
  xy: 2750, 670
  size: 258, 85
  orig: 258, 85
  offset: 0, 0
  index: -1
Arrow
  rotate: false
  xy: 3258, 1917
  size: 774, 127
  orig: 774, 127
  offset: 0, 0
  index: -1
Aura
  rotate: false
  xy: 2582, 274
  size: 198, 194
  orig: 198, 194
  offset: 0, 0
  index: -1
B_Hair
  rotate: true
  xy: 4, 41
  size: 496, 726
  orig: 496, 726
  offset: 0, 0
  index: -1
B_Skirt_b
  rotate: false
  xy: 1720, 1127
  size: 721, 397
  orig: 721, 397
  offset: 0, 0
  index: -1
B_Skirt_c
  rotate: false
  xy: 1720, 1528
  size: 822, 516
  orig: 822, 516
  offset: 0, 0
  index: -1
Belt
  rotate: false
  xy: 2539, 124
  size: 242, 146
  orig: 242, 146
  offset: 0, 0
  index: -1
Body
  rotate: false
  xy: 3654, 1316
  size: 314, 349
  orig: 314, 349
  offset: 0, 0
  index: -1
Bow
  rotate: true
  xy: 4, 541
  size: 230, 1243
  orig: 230, 1243
  offset: 0, 0
  index: -1
Bow_Thx1
  rotate: false
  xy: 4, 775
  size: 568, 1269
  orig: 568, 1269
  offset: 0, 0
  index: -1
Bow_Thx2
  rotate: false
  xy: 576, 775
  size: 568, 1269
  orig: 568, 1269
  offset: 0, 0
  index: -1
Bow_Thx3
  rotate: false
  xy: 1148, 775
  size: 568, 1269
  orig: 568, 1269
  offset: 0, 0
  index: -1
F_Line
  rotate: false
  xy: 734, 130
  size: 507, 407
  orig: 507, 407
  offset: 0, 0
  index: -1
Face
  rotate: false
  xy: 3827, 616
  size: 212, 287
  orig: 212, 287
  offset: 0, 0
  index: -1
LF_Fin
  rotate: false
  xy: 2785, 200
  size: 156, 177
  orig: 156, 177
  offset: 0, 0
  index: -1
LU_Fin
  rotate: false
  xy: 3826, 1695
  size: 193, 218
  orig: 193, 218
  offset: 0, 0
  index: -1
L_Acc_Fin
  rotate: true
  xy: 1567, 14
  size: 170, 218
  orig: 170, 218
  offset: 0, 0
  index: -1
L_Arm
  rotate: false
  xy: 3513, 485
  size: 139, 140
  orig: 139, 140
  offset: 0, 0
  index: -1
L_Arm_S1
  rotate: true
  xy: 3001, 380
  size: 187, 140
  orig: 187, 140
  offset: 0, 0
  index: -1
L_Arm_Thx2
  rotate: true
  xy: 2496, 472
  size: 194, 251
  orig: 194, 251
  offset: 0, 0
  index: -1
L_Eye
  rotate: false
  xy: 2821, 4
  size: 60, 42
  orig: 60, 42
  offset: 0, 0
  index: -1
L_Eye2
  rotate: false
  xy: 2885, 4
  size: 60, 42
  orig: 60, 42
  offset: 0, 0
  index: -1
L_Eye3
  rotate: true
  xy: 4043, 674
  size: 77, 42
  orig: 77, 42
  offset: 0, 0
  index: -1
L_Eye4
  rotate: false
  xy: 2740, 4
  size: 77, 42
  orig: 77, 42
  offset: 0, 0
  index: -1
L_Eyebrow
  rotate: true
  xy: 3488, 17
  size: 63, 13
  orig: 63, 13
  offset: 0, 0
  index: -1
L_Eyebrow2
  rotate: true
  xy: 2265, 298
  size: 84, 35
  orig: 84, 35
  offset: 0, 0
  index: -1
L_Eyebrow3
  rotate: true
  xy: 3249, 4
  size: 64, 57
  orig: 64, 57
  offset: 0, 0
  index: -1
L_Hair
  rotate: true
  xy: 1720, 776
  size: 347, 585
  orig: 347, 585
  offset: 0, 0
  index: -1
L_HandF
  rotate: true
  xy: 3255, 320
  size: 123, 137
  orig: 123, 137
  offset: 0, 0
  index: -1
L_HandF_S1
  rotate: false
  xy: 3378, 84
  size: 123, 137
  orig: 123, 137
  offset: 0, 0
  index: -1
L_HandF_Th1
  rotate: false
  xy: 3255, 447
  size: 125, 143
  orig: 125, 143
  offset: 0, 0
  index: -1
L_HandF_Th2
  rotate: false
  xy: 3384, 467
  size: 125, 143
  orig: 125, 143
  offset: 0, 0
  index: -1
L_HandF_Thx2
  rotate: true
  xy: 3803, 487
  size: 125, 143
  orig: 125, 143
  offset: 0, 0
  index: -1
L_HandU
  rotate: false
  xy: 2315, 4
  size: 168, 107
  orig: 168, 107
  offset: 0, 0
  index: -1
L_HandU_S1
  rotate: true
  xy: 4012, 412
  size: 62, 79
  orig: 62, 79
  offset: 0, 0
  index: -1
L_HandU_Th1
  rotate: false
  xy: 2445, 1411
  size: 97, 113
  orig: 97, 113
  offset: 0, 0
  index: -1
L_HandU_Th2
  rotate: true
  xy: 3244, 219
  size: 97, 113
  orig: 97, 113
  offset: 0, 0
  index: -1
L_HandU_Thx1
  rotate: false
  xy: 2785, 53
  size: 159, 143
  orig: 159, 143
  offset: 0, 0
  index: -1
L_Knee
  rotate: true
  xy: 2546, 1711
  size: 333, 708
  orig: 333, 708
  offset: 0, 0
  index: -1
L_Leg
  rotate: true
  xy: 3258, 1669
  size: 244, 564
  orig: 244, 564
  offset: 0, 0
  index: -1
L_Shirt_b
  rotate: true
  xy: 1789, 30
  size: 159, 291
  orig: 159, 291
  offset: 0, 0
  index: -1
L_Shoulder
  rotate: false
  xy: 2496, 670
  size: 250, 258
  orig: 250, 258
  offset: 0, 0
  index: -1
L_Shoulder_S1
  rotate: true
  xy: 1245, 29
  size: 155, 318
  orig: 155, 318
  offset: 0, 0
  index: -1
L_Shoulder_Thx2
  rotate: true
  xy: 3555, 907
  size: 155, 323
  orig: 155, 323
  offset: 0, 0
  index: -1
L_Skirt_a
  rotate: false
  xy: 1251, 401
  size: 530, 370
  orig: 530, 370
  offset: 0, 0
  index: -1
L_Skirt_b
  rotate: false
  xy: 3555, 1066
  size: 375, 246
  orig: 375, 246
  offset: 0, 0
  index: -1
L_Skirt_c
  rotate: true
  xy: 2870, 915
  size: 404, 342
  orig: 404, 342
  offset: 0, 0
  index: -1
L_Wing
  rotate: true
  xy: 1785, 386
  size: 386, 515
  orig: 386, 515
  offset: 0, 0
  index: -1
Mouth
  rotate: false
  xy: 4, 13
  size: 37, 24
  orig: 37, 24
  offset: 0, 0
  index: -1
Mouth2
  rotate: true
  xy: 4043, 857
  size: 47, 49
  orig: 47, 49
  offset: 0, 0
  index: -1
Mouth3
  rotate: true
  xy: 4043, 806
  size: 47, 49
  orig: 47, 49
  offset: 0, 0
  index: -1
Mouth4
  rotate: true
  xy: 4043, 755
  size: 47, 49
  orig: 47, 49
  offset: 0, 0
  index: -1
Mouth5
  rotate: false
  xy: 3972, 1316
  size: 47, 23
  orig: 47, 23
  offset: 0, 0
  index: -1
Mouth6
  rotate: false
  xy: 4023, 1316
  size: 47, 23
  orig: 47, 23
  offset: 0, 0
  index: -1
Neck
  rotate: false
  xy: 3232, 594
  size: 127, 162
  orig: 127, 162
  offset: 0, 0
  index: -1
Nose
  rotate: false
  xy: 4074, 1930
  size: 18, 22
  orig: 18, 22
  offset: 0, 0
  index: -1
OO
  rotate: false
  xy: 2839, 709
  size: 249, 202
  orig: 249, 202
  offset: 0, 0
  index: -1
R_Acc_Fin
  rotate: true
  xy: 2487, 4
  size: 107, 139
  orig: 107, 139
  offset: 0, 0
  index: -1
R_Arm
  rotate: true
  xy: 3085, 571
  size: 134, 143
  orig: 134, 143
  offset: 0, 0
  index: -1
R_Arm_Co1
  rotate: true
  xy: 3950, 478
  size: 134, 142
  orig: 134, 142
  offset: 0, 0
  index: -1
R_Arm_Co2
  rotate: false
  xy: 2784, 381
  size: 213, 186
  orig: 213, 186
  offset: 0, 0
  index: -1
R_Arm_G1
  rotate: true
  xy: 3656, 491
  size: 134, 143
  orig: 134, 143
  offset: 0, 0
  index: -1
R_Arm_G3
  rotate: true
  xy: 2839, 571
  size: 134, 242
  orig: 134, 242
  offset: 0, 0
  index: -1
R_Arm_S1
  rotate: false
  xy: 2309, 929
  size: 132, 194
  orig: 132, 194
  offset: 0, 0
  index: -1
R_Arm_Th1
  rotate: false
  xy: 3363, 614
  size: 143, 142
  orig: 143, 142
  offset: 0, 0
  index: -1
R_Arm_Th3
  rotate: false
  xy: 3934, 1147
  size: 158, 165
  orig: 158, 165
  offset: 0, 0
  index: -1
R_Arm_Th4
  rotate: false
  xy: 2084, 24
  size: 227, 165
  orig: 227, 165
  offset: 0, 0
  index: -1
R_Arm_Th5
  rotate: false
  xy: 3099, 211
  size: 141, 165
  orig: 141, 165
  offset: 0, 0
  index: -1
R_Eye
  rotate: false
  xy: 2949, 4
  size: 42, 42
  orig: 42, 42
  offset: 0, 0
  index: -1
R_Eye2
  rotate: false
  xy: 2995, 4
  size: 42, 42
  orig: 42, 42
  offset: 0, 0
  index: -1
R_Eye3
  rotate: false
  xy: 4036, 2002
  size: 56, 42
  orig: 56, 42
  offset: 0, 0
  index: -1
R_Eye4
  rotate: false
  xy: 4036, 1956
  size: 56, 42
  orig: 56, 42
  offset: 0, 0
  index: -1
R_Eyebrow
  rotate: false
  xy: 4043, 616
  size: 41, 25
  orig: 41, 25
  offset: 0, 0
  index: -1
R_Eyebrow2
  rotate: false
  xy: 4043, 645
  size: 49, 25
  orig: 49, 25
  offset: 0, 0
  index: -1
R_Eyebrow3
  rotate: true
  xy: 3882, 1013
  size: 49, 48
  orig: 49, 48
  offset: 0, 0
  index: -1
R_Fin
  rotate: true
  xy: 1016, 11
  size: 115, 224
  orig: 115, 224
  offset: 0, 0
  index: -1
R_Foot
  rotate: true
  xy: 734, 20
  size: 106, 278
  orig: 106, 278
  offset: 0, 0
  index: -1
R_Hair
  rotate: true
  xy: 3114, 1328
  size: 337, 536
  orig: 337, 536
  offset: 0, 0
  index: -1
R_HandF
  rotate: true
  xy: 3361, 225
  size: 91, 106
  orig: 91, 106
  offset: 0, 0
  index: -1
R_HandF_Co2
  rotate: true
  xy: 3934, 1006
  size: 137, 157
  orig: 137, 157
  offset: 0, 0
  index: -1
R_HandF_G1
  rotate: true
  xy: 3396, 320
  size: 143, 121
  orig: 143, 121
  offset: 0, 0
  index: -1
R_HandF_G2
  rotate: true
  xy: 3521, 338
  size: 143, 126
  orig: 143, 126
  offset: 0, 0
  index: -1
R_HandF_G3
  rotate: false
  xy: 2309, 777
  size: 148, 148
  orig: 148, 148
  offset: 0, 0
  index: -1
R_HandF_G4
  rotate: true
  xy: 3145, 391
  size: 176, 106
  orig: 176, 106
  offset: 0, 0
  index: -1
R_HandF_S1
  rotate: true
  xy: 2630, 4
  size: 116, 106
  orig: 116, 106
  offset: 0, 0
  index: -1
R_HandF_S3
  rotate: true
  xy: 3378, 4
  size: 76, 106
  orig: 76, 106
  offset: 0, 0
  index: -1
R_HandF_Th1
  rotate: false
  xy: 3249, 72
  size: 125, 143
  orig: 125, 143
  offset: 0, 0
  index: -1
R_HandF_Th2
  rotate: false
  xy: 2315, 115
  size: 220, 189
  orig: 220, 189
  offset: 0, 0
  index: -1
R_HandF_Th3
  rotate: false
  xy: 2948, 50
  size: 150, 147
  orig: 150, 147
  offset: 0, 0
  index: -1
R_HandF_Th4
  rotate: false
  xy: 3972, 1343
  size: 109, 111
  orig: 109, 111
  offset: 0, 0
  index: -1
R_HandF_Thx2
  rotate: true
  xy: 3102, 54
  size: 153, 143
  orig: 153, 143
  offset: 0, 0
  index: -1
R_HandU
  rotate: true
  xy: 3471, 225
  size: 91, 106
  orig: 91, 106
  offset: 0, 0
  index: -1
R_Knee
  rotate: false
  xy: 2945, 201
  size: 150, 175
  orig: 150, 175
  offset: 0, 0
  index: -1
R_Leg
  rotate: true
  xy: 1245, 188
  size: 209, 532
  orig: 209, 532
  offset: 0, 0
  index: -1
R_Shirt_b
  rotate: true
  xy: 3216, 760
  size: 197, 302
  orig: 197, 302
  offset: 0, 0
  index: -1
R_Shoulder
  rotate: true
  xy: 2304, 472
  size: 300, 188
  orig: 300, 188
  offset: 0, 0
  index: -1
R_Skirt_a
  rotate: true
  xy: 2445, 932
  size: 387, 421
  orig: 387, 421
  offset: 0, 0
  index: -1
R_Skirt_b
  rotate: false
  xy: 3522, 629
  size: 301, 274
  orig: 301, 274
  offset: 0, 0
  index: -1
R_Skirt_c
  rotate: false
  xy: 3216, 961
  size: 335, 363
  orig: 335, 363
  offset: 0, 0
  index: -1
R_Wing
  rotate: true
  xy: 1781, 193
  size: 189, 480
  orig: 189, 480
  offset: 0, 0
  index: -1
SA_1
  rotate: true
  xy: 3505, 48
  size: 65, 88
  orig: 65, 88
  offset: 0, 0
  index: -1
SA_2
  rotate: true
  xy: 3972, 1458
  size: 89, 120
  orig: 89, 120
  offset: 0, 0
  index: -1
SA_3
  rotate: false
  xy: 3651, 338
  size: 74, 63
  orig: 74, 63
  offset: 0, 0
  index: -1
SA_4
  rotate: false
  xy: 4036, 1917
  size: 34, 35
  orig: 34, 35
  offset: 0, 0
  index: -1
SB_1
  rotate: true
  xy: 3685, 152
  size: 81, 73
  orig: 81, 73
  offset: 0, 0
  index: -1
SB_2
  rotate: false
  xy: 3927, 401
  size: 81, 73
  orig: 81, 73
  offset: 0, 0
  index: -1
SB_3
  rotate: false
  xy: 3701, 75
  size: 58, 73
  orig: 58, 73
  offset: 0, 0
  index: -1
SB_4
  rotate: false
  xy: 3310, 13
  size: 50, 55
  orig: 50, 55
  offset: 0, 0
  index: -1
SC_1
  rotate: false
  xy: 3882, 908
  size: 100, 94
  orig: 100, 94
  offset: 0, 0
  index: -1
SC_2
  rotate: false
  xy: 3986, 908
  size: 100, 94
  orig: 100, 94
  offset: 0, 0
  index: -1
SC_3
  rotate: false
  xy: 3597, 24
  size: 100, 94
  orig: 100, 94
  offset: 0, 0
  index: -1
SC_4
  rotate: true
  xy: 3505, 4
  size: 40, 42
  orig: 40, 42
  offset: 0, 0
  index: -1
SD_1
  rotate: true
  xy: 2445, 1325
  size: 82, 97
  orig: 82, 97
  offset: 0, 0
  index: -1
SD_2
  rotate: true
  xy: 3656, 405
  size: 82, 97
  orig: 82, 97
  offset: 0, 0
  index: -1
SD_2a
  rotate: false
  xy: 3972, 1551
  size: 120, 140
  orig: 120, 140
  offset: 0, 0
  index: -1
SD_3
  rotate: false
  xy: 3671, 237
  size: 82, 97
  orig: 82, 97
  offset: 0, 0
  index: -1
SD_4
  rotate: true
  xy: 3114, 1670
  size: 37, 53
  orig: 37, 53
  offset: 0, 0
  index: -1
SE_1
  rotate: false
  xy: 2751, 592
  size: 81, 74
  orig: 81, 74
  offset: 0, 0
  index: -1
SE_2
  rotate: false
  xy: 3757, 409
  size: 81, 74
  orig: 81, 74
  offset: 0, 0
  index: -1
SE_3
  rotate: false
  xy: 3842, 409
  size: 81, 74
  orig: 81, 74
  offset: 0, 0
  index: -1
SE_4
  rotate: false
  xy: 2265, 246
  size: 34, 48
  orig: 34, 48
  offset: 0, 0
  index: -1
SF_1
  rotate: false
  xy: 3505, 117
  size: 86, 104
  orig: 86, 104
  offset: 0, 0
  index: -1
SF_2
  rotate: false
  xy: 3581, 230
  size: 86, 104
  orig: 86, 104
  offset: 0, 0
  index: -1
SF_3
  rotate: false
  xy: 3595, 122
  size: 86, 104
  orig: 86, 104
  offset: 0, 0
  index: -1
SF_4
  rotate: false
  xy: 2740, 84
  size: 41, 36
  orig: 41, 36
  offset: 0, 0
  index: -1
Shirt_a
  rotate: false
  xy: 2304, 308
  size: 274, 160
  orig: 274, 160
  offset: 0, 0
  index: -1
Skirt_d
  rotate: false
  xy: 2546, 1323
  size: 564, 384
  orig: 564, 384
  offset: 0, 0
  index: -1
U_Skirt_a
  rotate: true
  xy: 3092, 735
  size: 176, 113
  orig: 176, 113
  offset: 0, 0
  index: -1
