
Hero_Brundo_japansummer19.png
size: 4096,4096
format: RGBA8888
filter: Linear,Linear
repeat: none
Arm_L_main
  rotate: false
  xy: 4, 4
  size: 244, 131
  orig: 244, 131
  offset: 0, 0
  index: -1
Back
  rotate: false
  xy: 469, 1399
  size: 428, 425
  orig: 428, 425
  offset: 0, 0
  index: -1
Shirt_LB_01
  rotate: true
  xy: 4, 569
  size: 111, 321
  orig: 111, 321
  offset: 0, 0
  index: -1
Shirt_LB_Threanten_03
  rotate: true
  xy: 4, 569
  size: 111, 321
  orig: 111, 321
  offset: 0, 0
  index: -1
Shirt_LB_GT_SP01
  rotate: false
  xy: 4, 932
  size: 351, 137
  orig: 351, 137
  offset: 0, 0
  index: -1
Shirt_LB_GT_SP02
  rotate: false
  xy: 3063, 513
  size: 181, 386
  orig: 181, 386
  offset: 0, 0
  index: -1
Shirt_LB_greeting_01
  rotate: true
  xy: 4, 431
  size: 134, 320
  orig: 134, 320
  offset: 0, 0
  index: -1
Shirt_LB_greeting_04
  rotate: true
  xy: 349, 698
  size: 117, 337
  orig: 117, 337
  offset: 0, 0
  index: -1
Shirt_RB_greeting_01
  rotate: true
  xy: 349, 698
  size: 117, 337
  orig: 117, 337
  offset: 0, 0
  index: -1
Shirt_LT_01
  rotate: true
  xy: 4, 1073
  size: 149, 353
  orig: 149, 353
  offset: 0, 0
  index: -1
Shirt_LT_GT_SP01
  rotate: false
  xy: 3888, 1697
  size: 197, 49
  orig: 197, 49
  offset: 0, 0
  index: -1
Shirt_LT_Threanten_03
  rotate: true
  xy: 888, 1061
  size: 149, 356
  orig: 149, 356
  offset: 0, 0
  index: -1
Shirt_LT_greeting_01
  rotate: true
  xy: 423, 967
  size: 154, 352
  orig: 154, 352
  offset: 0, 0
  index: -1
Shirt_LT_greeting_04
  rotate: true
  xy: 407, 819
  size: 144, 349
  orig: 144, 349
  offset: 0, 0
  index: -1
Shirt_RB_01
  rotate: false
  xy: 1236, 523
  size: 255, 328
  orig: 255, 328
  offset: 0, 0
  index: -1
Shirt_RB_GT_SP01
  rotate: false
  xy: 891, 1214
  size: 363, 131
  orig: 363, 131
  offset: 0, 0
  index: -1
Shirt_RB_GT_SP02
  rotate: true
  xy: 3783, 621
  size: 148, 279
  orig: 148, 279
  offset: 0, 0
  index: -1
Shirt_RB_greeting_02
  rotate: true
  xy: 873, 525
  size: 148, 319
  orig: 148, 319
  offset: 0, 0
  index: -1
Shirt_RB_greeting_05
  rotate: true
  xy: 884, 677
  size: 151, 344
  orig: 151, 344
  offset: 0, 0
  index: -1
Shirt_RT_01
  rotate: true
  xy: 349, 457
  size: 237, 330
  orig: 237, 330
  offset: 0, 0
  index: -1
Shirt_RT_GT_SP01
  rotate: false
  xy: 4, 1226
  size: 363, 98
  orig: 363, 98
  offset: 0, 0
  index: -1
Shirt_RT_GT_SP02
  rotate: true
  xy: 4, 684
  size: 244, 341
  orig: 244, 341
  offset: 0, 0
  index: -1
Shirt_RT_greeting_01
  rotate: false
  xy: 1550, 1337
  size: 211, 325
  orig: 211, 325
  offset: 0, 0
  index: -1
Shirt_RT_greeting_02
  rotate: true
  xy: 426, 1125
  size: 168, 356
  orig: 168, 356
  offset: 0, 0
  index: -1
Shirt_RT_greeting_05
  rotate: true
  xy: 885, 832
  size: 225, 347
  orig: 225, 347
  offset: 0, 0
  index: -1
Sword_B
  rotate: true
  xy: 3085, 1356
  size: 142, 165
  orig: 142, 165
  offset: 0, 0
  index: -1
Sword_BL_greeting
  rotate: false
  xy: 671, 273
  size: 179, 116
  orig: 179, 116
  offset: 0, 0
  index: -1
Sword_B_Threanten_03
  rotate: true
  xy: 3888, 1750
  size: 59, 199
  orig: 59, 199
  offset: 0, 0
  index: -1
Sword_B_greeting
  rotate: true
  xy: 2004, 864
  size: 151, 230
  orig: 151, 230
  offset: 0, 0
  index: -1
Sword_B_greeting_04
  rotate: true
  xy: 2335, 984
  size: 139, 176
  orig: 139, 176
  offset: 0, 0
  index: -1
Sword_B_thank
  rotate: true
  xy: 3888, 1813
  size: 62, 202
  orig: 62, 202
  offset: 0, 0
  index: -1
Sword_T
  rotate: true
  xy: 3260, 2008
  size: 316, 683
  orig: 316, 683
  offset: 0, 0
  index: -1
Sword_T02
  rotate: true
  xy: 3161, 2231
  size: 70, 95
  orig: 70, 95
  offset: 0, 0
  index: -1
Sword_T_Threanten_03
  rotate: true
  xy: 2517, 2305
  size: 227, 739
  orig: 227, 739
  offset: 0, 0
  index: -1
Sword_T_greeting
  rotate: true
  xy: 1772, 2300
  size: 232, 741
  orig: 232, 741
  offset: 0, 0
  index: -1
Sword_T_greeting_04
  rotate: false
  xy: 1765, 1019
  size: 566, 491
  orig: 566, 491
  offset: 0, 0
  index: -1
Sword_T_greeting_05
  rotate: true
  xy: 2517, 888
  size: 518, 542
  orig: 518, 542
  offset: 0, 0
  index: -1
Sword_T_thank
  rotate: true
  xy: 3260, 2328
  size: 204, 706
  orig: 204, 706
  offset: 0, 0
  index: -1
arm_L_Com_01
  rotate: true
  xy: 3888, 1529
  size: 164, 196
  orig: 164, 196
  offset: 0, 0
  index: -1
arm_L_Threanten_03
  rotate: false
  xy: 4, 139
  size: 244, 132
  orig: 244, 132
  offset: 0, 0
  index: -1
arm_L_greeting_01
  rotate: false
  xy: 3888, 1315
  size: 195, 210
  orig: 195, 210
  offset: 0, 0
  index: -1
arm_L_greeting_02
  rotate: false
  xy: 507, 72
  size: 160, 226
  orig: 160, 226
  offset: 0, 0
  index: -1
arm_L_greeting_03
  rotate: true
  xy: 3947, 2040
  size: 233, 145
  orig: 233, 145
  offset: 0, 0
  index: -1
arm_L_greeting_04
  rotate: true
  xy: 1763, 887
  size: 128, 237
  orig: 128, 237
  offset: 0, 0
  index: -1
arm_L_greeting_05
  rotate: false
  xy: 683, 393
  size: 179, 148
  orig: 179, 148
  offset: 0, 0
  index: -1
arm_R
  rotate: false
  xy: 1549, 1105
  size: 181, 228
  orig: 181, 228
  offset: 0, 0
  index: -1
arm_R_Threanten_03
  rotate: false
  xy: 328, 302
  size: 254, 151
  orig: 254, 151
  offset: 0, 0
  index: -1
arm_R_greeting_01
  rotate: false
  xy: 2335, 1127
  size: 177, 228
  orig: 177, 228
  offset: 0, 0
  index: -1
arm_R_greeting_02
  rotate: false
  xy: 3970, 2277
  size: 122, 255
  orig: 122, 255
  offset: 0, 0
  index: -1
arm_R_greeting_05
  rotate: false
  xy: 3063, 903
  size: 182, 237
  orig: 182, 237
  offset: 0, 0
  index: -1
arm_R_thank_01
  rotate: false
  xy: 4, 275
  size: 251, 152
  orig: 251, 152
  offset: 0, 0
  index: -1
armor_L
  rotate: true
  xy: 3783, 301
  size: 316, 275
  orig: 316, 275
  offset: 0, 0
  index: -1
armor_L_02
  rotate: true
  xy: 3791, 1028
  size: 283, 300
  orig: 283, 300
  offset: 0, 0
  index: -1
armor_L_03
  rotate: false
  xy: 2358, 1359
  size: 155, 341
  orig: 155, 341
  offset: 0, 0
  index: -1
armor_R
  rotate: false
  xy: 259, 56
  size: 244, 242
  orig: 244, 242
  offset: 0, 0
  index: -1
armor_head
  rotate: true
  xy: 361, 1086
  size: 136, 58
  orig: 136, 58
  offset: 0, 0
  index: -1
armor_head_L
  rotate: false
  xy: 2513, 741
  size: 158, 143
  orig: 158, 143
  offset: 0, 0
  index: -1
armor_head_R
  rotate: true
  xy: 1370, 163
  size: 120, 131
  orig: 120, 131
  offset: 0, 0
  index: -1
armor_head_main
  rotate: false
  xy: 1980, 601
  size: 153, 109
  orig: 153, 109
  offset: 0, 0
  index: -1
armor_horn
  rotate: true
  xy: 866, 306
  size: 215, 247
  orig: 215, 247
  offset: 0, 0
  index: -1
bead_01
  rotate: false
  xy: 3888, 1955
  size: 55, 49
  orig: 55, 49
  offset: 0, 0
  index: -1
bead_02
  rotate: false
  xy: 690, 719
  size: 181, 76
  orig: 181, 76
  offset: 0, 0
  index: -1
bead_03
  rotate: false
  xy: 2358, 1726
  size: 43, 44
  orig: 43, 44
  offset: 0, 0
  index: -1
bead_04
  rotate: true
  xy: 3791, 1321
  size: 73, 57
  orig: 73, 57
  offset: 0, 0
  index: -1
bead_05
  rotate: false
  xy: 3888, 1882
  size: 54, 69
  orig: 54, 69
  offset: 0, 0
  index: -1
bead_06
  rotate: true
  xy: 371, 1276
  size: 48, 51
  orig: 48, 51
  offset: 0, 0
  index: -1
bead_07
  rotate: true
  xy: 3085, 1597
  size: 68, 71
  orig: 68, 71
  offset: 0, 0
  index: -1
bead_08
  rotate: false
  xy: 3085, 1502
  size: 50, 46
  orig: 50, 46
  offset: 0, 0
  index: -1
body
  rotate: false
  xy: 4, 1328
  size: 424, 356
  orig: 424, 356
  offset: 0, 0
  index: -1
bow_L
  rotate: true
  xy: 3852, 1353
  size: 41, 32
  orig: 41, 32
  offset: 0, 0
  index: -1
bow_L_under
  rotate: false
  xy: 2335, 1412
  size: 15, 98
  orig: 15, 98
  offset: 0, 0
  index: -1
bow_R
  rotate: true
  xy: 901, 1779
  size: 45, 35
  orig: 45, 35
  offset: 0, 0
  index: -1
bow_R_under
  rotate: false
  xy: 1550, 1863
  size: 18, 97
  orig: 18, 97
  offset: 0, 0
  index: -1
cloth_L_main
  rotate: true
  xy: 941, 1627
  size: 333, 605
  orig: 333, 605
  offset: 0, 0
  index: -1
cloth_L_mini
  rotate: false
  xy: 1495, 705
  size: 172, 130
  orig: 172, 130
  offset: 0, 0
  index: -1
cloth_R_armor
  rotate: true
  xy: 901, 1349
  size: 274, 372
  orig: 274, 372
  offset: 0, 0
  index: -1
cloth_R_back
  rotate: false
  xy: 1117, 306
  size: 113, 215
  orig: 113, 215
  offset: 0, 0
  index: -1
cloth_R_main
  rotate: false
  xy: 1277, 1255
  size: 268, 368
  orig: 268, 368
  offset: 0, 0
  index: -1
cloth_R_main_green
  rotate: true
  xy: 3791, 773
  size: 251, 297
  orig: 251, 297
  offset: 0, 0
  index: -1
cloth_R_main_under
  rotate: true
  xy: 3254, 882
  size: 512, 533
  orig: 512, 533
  offset: 0, 0
  index: -1
cloth_R_mini
  rotate: false
  xy: 1236, 855
  size: 260, 133
  orig: 260, 133
  offset: 0, 0
  index: -1
effect_sword_01
  rotate: true
  xy: 3255, 1398
  size: 606, 629
  orig: 606, 629
  offset: 0, 0
  index: -1
effect_sword_02
  rotate: true
  xy: 1772, 2014
  size: 282, 629
  orig: 282, 629
  offset: 0, 0
  index: -1
effect_sword_03
  rotate: false
  xy: 4, 1828
  size: 933, 704
  orig: 933, 704
  offset: 0, 0
  index: -1
effect_sword_04
  rotate: true
  xy: 1768, 1774
  size: 236, 620
  orig: 236, 620
  offset: 0, 0
  index: -1
effect_sword_05
  rotate: true
  xy: 2517, 1669
  size: 632, 640
  orig: 632, 640
  offset: 0, 0
  index: -1
effect_sword_06
  rotate: true
  xy: 941, 1964
  size: 321, 628
  orig: 321, 628
  offset: 0, 0
  index: -1
eye_L_01
  rotate: true
  xy: 3947, 2296
  size: 28, 16
  orig: 28, 16
  offset: 0, 0
  index: -1
eye_L_02
  rotate: true
  xy: 2335, 1381
  size: 27, 11
  orig: 27, 11
  offset: 0, 0
  index: -1
eye_R_01
  rotate: false
  xy: 2358, 1706
  size: 28, 16
  orig: 28, 16
  offset: 0, 0
  index: -1
eye_R_02
  rotate: true
  xy: 1550, 1830
  size: 29, 15
  orig: 29, 15
  offset: 0, 0
  index: -1
eyeblow_L
  rotate: true
  xy: 4062, 571
  size: 46, 28
  orig: 46, 28
  offset: 0, 0
  index: -1
eyeblow_R
  rotate: true
  xy: 901, 1729
  size: 46, 33
  orig: 46, 33
  offset: 0, 0
  index: -1
fing_01
  rotate: true
  xy: 2405, 2097
  size: 60, 106
  orig: 60, 106
  offset: 0, 0
  index: -1
fing_02
  rotate: false
  xy: 2412, 883
  size: 97, 97
  orig: 97, 97
  offset: 0, 0
  index: -1
fing_greeting05
  rotate: true
  xy: 804, 1285
  size: 110, 83
  orig: 110, 83
  offset: 0, 0
  index: -1
fing_greeting05_02
  rotate: false
  xy: 3085, 1552
  size: 60, 41
  orig: 60, 41
  offset: 0, 0
  index: -1
fur_R
  rotate: false
  xy: 1258, 992
  size: 263, 259
  orig: 263, 259
  offset: 0, 0
  index: -1
fur_R_back
  rotate: true
  xy: 586, 330
  size: 123, 81
  orig: 123, 81
  offset: 0, 0
  index: -1
hair_1_s
  rotate: true
  xy: 2405, 2161
  size: 135, 108
  orig: 135, 108
  offset: 0, 0
  index: -1
hair_4_top_s
  rotate: false
  xy: 3248, 408
  size: 531, 470
  orig: 531, 470
  offset: 0, 0
  index: -1
hair_5_s
  rotate: false
  xy: 1550, 1666
  size: 213, 158
  orig: 213, 158
  offset: 0, 0
  index: -1
hand _R_greeting_02
  rotate: true
  xy: 941, 2289
  size: 243, 827
  orig: 243, 827
  offset: 0, 0
  index: -1
hand_L_Com_01
  rotate: true
  xy: 1234, 262
  size: 130, 132
  orig: 130, 132
  offset: 0, 0
  index: -1
hand_L_Com_02
  rotate: true
  xy: 760, 799
  size: 151, 120
  orig: 151, 120
  offset: 0, 0
  index: -1
hand_L_Com_03
  rotate: false
  xy: 1835, 495
  size: 141, 125
  orig: 141, 125
  offset: 0, 0
  index: -1
hand_L_Hit_01
  rotate: false
  xy: 1400, 287
  size: 140, 125
  orig: 140, 125
  offset: 0, 0
  index: -1
hand_L_Thank_01
  rotate: true
  xy: 779, 954
  size: 161, 102
  orig: 161, 102
  offset: 0, 0
  index: -1
hand_L_Threanten_01
  rotate: false
  xy: 3161, 1698
  size: 88, 144
  orig: 88, 144
  offset: 0, 0
  index: -1
hand_L_Threanten_02
  rotate: false
  xy: 1495, 559
  size: 167, 142
  orig: 167, 142
  offset: 0, 0
  index: -1
hand_L_Threanten_03
  rotate: false
  xy: 1573, 1828
  size: 190, 172
  orig: 190, 172
  offset: 0, 0
  index: -1
hand_L_greeting_01
  rotate: false
  xy: 1982, 714
  size: 169, 146
  orig: 169, 146
  offset: 0, 0
  index: -1
hand_L_greeting_02
  rotate: false
  xy: 786, 1119
  size: 98, 162
  orig: 98, 162
  offset: 0, 0
  index: -1
hand_L_greeting_03
  rotate: false
  xy: 3161, 1502
  size: 84, 192
  orig: 84, 192
  offset: 0, 0
  index: -1
hand_L_greeting_04
  rotate: true
  xy: 2238, 834
  size: 146, 170
  orig: 146, 170
  offset: 0, 0
  index: -1
hand_L_greeting_04_05
  rotate: false
  xy: 690, 545
  size: 179, 170
  orig: 179, 170
  offset: 0, 0
  index: -1
hand_L_greeting_05
  rotate: true
  xy: 3161, 2128
  size: 99, 94
  orig: 99, 94
  offset: 0, 0
  index: -1
hand_L_main
  rotate: true
  xy: 1544, 277
  size: 135, 113
  orig: 135, 113
  offset: 0, 0
  index: -1
hand_L_sorry_01
  rotate: true
  xy: 2155, 694
  size: 136, 155
  orig: 136, 155
  offset: 0, 0
  index: -1
hand_L_thank_02
  rotate: true
  xy: 1763, 787
  size: 96, 215
  orig: 96, 215
  offset: 0, 0
  index: -1
hand_R_Threanten_03
  rotate: false
  xy: 1495, 416
  size: 164, 139
  orig: 164, 139
  offset: 0, 0
  index: -1
hand_R_greeting
  rotate: true
  xy: 1671, 637
  size: 146, 169
  orig: 146, 169
  offset: 0, 0
  index: -1
hand_R_greeting_04
  rotate: false
  xy: 2314, 704
  size: 144, 126
  orig: 144, 126
  offset: 0, 0
  index: -1
hand_R_greeting_05
  rotate: true
  xy: 1666, 515
  size: 118, 165
  orig: 118, 165
  offset: 0, 0
  index: -1
hand_R_main
  rotate: false
  xy: 3947, 1879
  size: 145, 157
  orig: 145, 157
  offset: 0, 0
  index: -1
hand_R_thnak
  rotate: false
  xy: 1234, 396
  size: 162, 123
  orig: 162, 123
  offset: 0, 0
  index: -1
head
  rotate: true
  xy: 1663, 381
  size: 130, 149
  orig: 130, 149
  offset: 0, 0
  index: -1
leg_L
  rotate: true
  xy: 1767, 1514
  size: 256, 587
  orig: 256, 587
  offset: 0, 0
  index: -1
leg_R
  rotate: true
  xy: 2517, 1410
  size: 255, 564
  orig: 255, 564
  offset: 0, 0
  index: -1
mustache_Center
  rotate: true
  xy: 361, 959
  size: 123, 42
  orig: 123, 42
  offset: 0, 0
  index: -1
mustache_L_1
  rotate: true
  xy: 3161, 1846
  size: 172, 90
  orig: 172, 90
  offset: 0, 0
  index: -1
mustache_main
  rotate: true
  xy: 1844, 624
  size: 159, 132
  orig: 159, 132
  offset: 0, 0
  index: -1
nose
  rotate: false
  xy: 371, 1226
  size: 49, 46
  orig: 49, 46
  offset: 0, 0
  index: -1
rope_L
  rotate: true
  xy: 3063, 1250
  size: 102, 183
  orig: 102, 183
  offset: 0, 0
  index: -1
rope_M
  rotate: true
  xy: 3063, 1144
  size: 102, 183
  orig: 102, 183
  offset: 0, 0
  index: -1
rope_Main copy 2
  rotate: false
  xy: 4, 1688
  size: 461, 136
  orig: 461, 136
  offset: 0, 0
  index: -1
rope_R
  rotate: false
  xy: 3161, 2022
  size: 93, 102
  orig: 93, 102
  offset: 0, 0
  index: -1
rope_Tail_L
  rotate: false
  xy: 2405, 1704
  size: 102, 389
  orig: 102, 389
  offset: 0, 0
  index: -1
rope_Tail_R
  rotate: true
  xy: 432, 1297
  size: 98, 368
  orig: 98, 368
  offset: 0, 0
  index: -1
shoulder_L_01
  rotate: false
  xy: 1573, 2004
  size: 191, 281
  orig: 191, 281
  offset: 0, 0
  index: -1
shoulder_R_01
  rotate: false
  xy: 1525, 839
  size: 234, 262
  orig: 234, 262
  offset: 0, 0
  index: -1
