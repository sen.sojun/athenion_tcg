
Hero_Tatarus_Form_2.png
size: 4096,2048
format: RGBA8888
filter: Linear,Linear
repeat: none
01Thank/01Thank_Rhand
  rotate: false
  xy: 3042, 1175
  size: 152, 168
  orig: 152, 168
  offset: 0, 0
  index: -1
03Threaten/03Threaten_Lhand_1
  rotate: false
  xy: 2496, 32
  size: 182, 184
  orig: 182, 184
  offset: 0, 0
  index: -1
03Threaten/03Threaten_Lhand_2
  rotate: true
  xy: 2728, 223
  size: 146, 184
  orig: 146, 184
  offset: 0, 0
  index: -1
03Threaten/03Threaten_eff_eye_1
  rotate: false
  xy: 2976, 1064
  size: 117, 73
  orig: 117, 73
  offset: 0, 0
  index: -1
03Threaten/03Threaten_eff_eye_2
  rotate: false
  xy: 1165, 1131
  size: 336, 113
  orig: 336, 113
  offset: 0, 0
  index: -1
03Threaten/03Threaten_eff_eye_3
  rotate: false
  xy: 1178, 263
  size: 440, 90
  orig: 440, 90
  offset: 0, 0
  index: -1
04Greeting/04Greeting_Lhand
  rotate: false
  xy: 2724, 1134
  size: 155, 205
  orig: 155, 205
  offset: 0, 0
  index: -1
04Greeting/04Greeting_Rhand
  rotate: false
  xy: 2682, 25
  size: 170, 192
  orig: 170, 192
  offset: 0, 0
  index: -1
06Mistake/06Mistake_Rhand
  rotate: true
  xy: 2310, 1158
  size: 116, 220
  orig: 116, 220
  offset: 0, 0
  index: -1
Eff_fire_01
  rotate: false
  xy: 3112, 34
  size: 469, 343
  orig: 469, 343
  offset: 0, 0
  index: -1
Eff_fire_02
  rotate: false
  xy: 1691, 459
  size: 694, 573
  orig: 694, 573
  offset: 0, 0
  index: -1
Eff_fire_03
  rotate: false
  xy: 2456, 1649
  size: 403, 392
  orig: 403, 392
  offset: 0, 0
  index: -1
Eff_fire_04
  rotate: true
  xy: 2863, 1654
  size: 387, 367
  orig: 387, 367
  offset: 0, 0
  index: -1
Eff_fire_05
  rotate: false
  xy: 1557, 39
  size: 285, 166
  orig: 285, 166
  offset: 0, 0
  index: -1
Eff_fire_06
  rotate: false
  xy: 2037, 1155
  size: 269, 119
  orig: 269, 119
  offset: 0, 0
  index: -1
Eff_fire_07
  rotate: true
  xy: 2896, 1468
  size: 182, 156
  orig: 182, 156
  offset: 0, 0
  index: -1
Eff_fire_08
  rotate: false
  xy: 3373, 1734
  size: 122, 146
  orig: 122, 146
  offset: 0, 0
  index: -1
Eff_fire_09
  rotate: false
  xy: 1997, 314
  size: 64, 42
  orig: 64, 42
  offset: 0, 0
  index: -1
Eye_L
  rotate: true
  xy: 2133, 314
  size: 49, 42
  orig: 49, 42
  offset: 0, 0
  index: -1
Eye_L_c
  rotate: true
  xy: 2068, 15
  size: 49, 42
  orig: 49, 42
  offset: 0, 0
  index: -1
Eye_R
  rotate: false
  xy: 1569, 215
  size: 62, 44
  orig: 62, 44
  offset: 0, 0
  index: -1
Eye_R_c
  rotate: false
  xy: 2065, 322
  size: 64, 41
  orig: 64, 41
  offset: 0, 0
  index: -1
Lhand
  rotate: true
  xy: 1846, 17
  size: 189, 218
  orig: 189, 218
  offset: 0, 0
  index: -1
Lhand_tt1
  rotate: false
  xy: 3369, 1884
  size: 131, 157
  orig: 131, 157
  offset: 0, 0
  index: -1
Lhand_tt2
  rotate: false
  xy: 3234, 1720
  size: 135, 155
  orig: 135, 155
  offset: 0, 0
  index: -1
Lhand_tt3
  rotate: false
  xy: 2916, 236
  size: 192, 135
  orig: 192, 135
  offset: 0, 0
  index: -1
Llowerarm
  rotate: false
  xy: 824, 4
  size: 430, 198
  orig: 430, 198
  offset: 0, 0
  index: -1
Llowerarm_tt1
  rotate: true
  xy: 2068, 68
  size: 140, 259
  orig: 140, 259
  offset: 0, 0
  index: -1
Llowerarm_tt2
  rotate: true
  xy: 386, 44
  size: 154, 434
  orig: 154, 434
  offset: 0, 0
  index: -1
Lupperarm
  rotate: true
  xy: 1868, 1278
  size: 296, 507
  orig: 296, 507
  offset: 0, 0
  index: -1
Mouth
  rotate: false
  xy: 1252, 209
  size: 61, 50
  orig: 61, 50
  offset: 0, 0
  index: -1
Mouth2
  rotate: false
  xy: 1317, 209
  size: 61, 50
  orig: 61, 50
  offset: 0, 0
  index: -1
Mouth3
  rotate: false
  xy: 1382, 209
  size: 60, 50
  orig: 60, 50
  offset: 0, 0
  index: -1
Mouth4
  rotate: false
  xy: 1446, 209
  size: 60, 50
  orig: 60, 50
  offset: 0, 0
  index: -1
Mouth6
  rotate: true
  xy: 3030, 35
  size: 65, 66
  orig: 65, 66
  offset: 0, 0
  index: -1
Mouth7
  rotate: false
  xy: 1928, 300
  size: 65, 56
  orig: 65, 56
  offset: 0, 0
  index: -1
Nose
  rotate: true
  xy: 1510, 209
  size: 50, 55
  orig: 50, 55
  offset: 0, 0
  index: -1
Rhand
  rotate: false
  xy: 3012, 504
  size: 179, 147
  orig: 179, 147
  offset: 0, 0
  index: -1
Rhand_tt1
  rotate: false
  xy: 2896, 1347
  size: 153, 117
  orig: 153, 117
  offset: 0, 0
  index: -1
Rhand_tt3
  rotate: false
  xy: 3159, 1358
  size: 176, 130
  orig: 176, 130
  offset: 0, 0
  index: -1
Rlowerarm
  rotate: true
  xy: 2379, 1312
  size: 331, 253
  orig: 331, 253
  offset: 0, 0
  index: -1
Rupperarm
  rotate: true
  xy: 4, 20
  size: 178, 378
  orig: 178, 378
  offset: 0, 0
  index: -1
Skull_R_01
  rotate: true
  xy: 908, 1034
  size: 210, 253
  orig: 210, 253
  offset: 0, 0
  index: -1
Skull_R_02
  rotate: false
  xy: 2883, 1141
  size: 155, 198
  orig: 155, 198
  offset: 0, 0
  index: -1
Skull_R_03
  rotate: true
  xy: 2818, 504
  size: 147, 190
  orig: 147, 190
  offset: 0, 0
  index: -1
Skull_R_04
  rotate: true
  xy: 2531, 221
  size: 147, 193
  orig: 147, 193
  offset: 0, 0
  index: -1
Skull_R_05
  rotate: true
  xy: 2607, 504
  size: 147, 207
  orig: 147, 207
  offset: 0, 0
  index: -1
Skull_R_06
  rotate: true
  xy: 2284, 220
  size: 147, 243
  orig: 147, 243
  offset: 0, 0
  index: -1
Skull_R_07
  rotate: true
  xy: 807, 206
  size: 147, 367
  orig: 147, 367
  offset: 0, 0
  index: -1
Skull_R_08
  rotate: true
  xy: 1622, 293
  size: 60, 302
  orig: 60, 302
  offset: 0, 0
  index: -1
Skull_R_09
  rotate: true
  xy: 1486, 1036
  size: 91, 272
  orig: 91, 272
  offset: 0, 0
  index: -1
backhair
  rotate: true
  xy: 2636, 1343
  size: 302, 256
  orig: 302, 256
  offset: 0, 0
  index: -1
backwaistcloth
  rotate: true
  xy: 4, 1017
  size: 1024, 900
  orig: 1024, 900
  offset: 0, 0
  index: -1
belt01
  rotate: true
  xy: 2860, 1041
  size: 89, 112
  orig: 89, 112
  offset: 0, 0
  index: -1
belt02
  rotate: false
  xy: 2357, 1038
  size: 175, 116
  orig: 175, 116
  offset: 0, 0
  index: -1
belteye
  rotate: true
  xy: 1258, 14
  size: 191, 295
  orig: 191, 295
  offset: 0, 0
  index: -1
body
  rotate: true
  xy: 1963, 1647
  size: 394, 489
  orig: 394, 489
  offset: 0, 0
  index: -1
ear
  rotate: true
  xy: 3030, 104
  size: 128, 77
  orig: 128, 77
  offset: 0, 0
  index: -1
earhair
  rotate: true
  xy: 2389, 497
  size: 154, 214
  orig: 154, 214
  offset: 0, 0
  index: -1
eff_fire01
  rotate: true
  xy: 2870, 375
  size: 125, 178
  orig: 125, 178
  offset: 0, 0
  index: -1
eff_fire02
  rotate: true
  xy: 3208, 1517
  size: 133, 170
  orig: 133, 170
  offset: 0, 0
  index: -1
eff_fire03
  rotate: true
  xy: 2536, 1040
  size: 132, 184
  orig: 132, 184
  offset: 0, 0
  index: -1
eff_fire04
  rotate: true
  xy: 2534, 1176
  size: 132, 186
  orig: 132, 186
  offset: 0, 0
  index: -1
eff_fire05
  rotate: true
  xy: 2670, 373
  size: 127, 196
  orig: 127, 196
  offset: 0, 0
  index: -1
eff_fire06
  rotate: true
  xy: 2461, 372
  size: 121, 205
  orig: 121, 205
  offset: 0, 0
  index: -1
eff_fire07
  rotate: true
  xy: 1686, 1139
  size: 105, 175
  orig: 105, 175
  offset: 0, 0
  index: -1
eff_fire08
  rotate: true
  xy: 1505, 1137
  size: 107, 177
  orig: 107, 177
  offset: 0, 0
  index: -1
eff_fire09
  rotate: true
  xy: 1865, 1143
  size: 101, 168
  orig: 101, 168
  offset: 0, 0
  index: -1
eff_fire10
  rotate: true
  xy: 1922, 1037
  size: 102, 152
  orig: 102, 152
  offset: 0, 0
  index: -1
eff_fire11
  rotate: true
  xy: 1762, 1036
  size: 99, 156
  orig: 99, 156
  offset: 0, 0
  index: -1
eff_fire12
  rotate: true
  xy: 1813, 210
  size: 79, 160
  orig: 79, 160
  offset: 0, 0
  index: -1
eff_fire13
  rotate: true
  xy: 2175, 371
  size: 84, 144
  orig: 84, 144
  offset: 0, 0
  index: -1
eff_fire14
  rotate: true
  xy: 1977, 212
  size: 84, 155
  orig: 84, 155
  offset: 0, 0
  index: -1
eff_fire15
  rotate: true
  xy: 1691, 370
  size: 85, 171
  orig: 85, 171
  offset: 0, 0
  index: -1
eff_fire16
  rotate: true
  xy: 2022, 367
  size: 88, 149
  orig: 88, 149
  offset: 0, 0
  index: -1
eff_fire17
  rotate: true
  xy: 2724, 1044
  size: 86, 132
  orig: 86, 132
  offset: 0, 0
  index: -1
eff_fire18
  rotate: true
  xy: 2323, 371
  size: 84, 134
  orig: 84, 134
  offset: 0, 0
  index: -1
eff_fire19
  rotate: true
  xy: 2136, 220
  size: 90, 144
  orig: 90, 144
  offset: 0, 0
  index: -1
eff_fire20
  rotate: true
  xy: 1866, 360
  size: 95, 152
  orig: 95, 152
  offset: 0, 0
  index: -1
eff_fire21
  rotate: true
  xy: 2222, 1046
  size: 105, 131
  orig: 105, 131
  offset: 0, 0
  index: -1
eff_fire22
  rotate: false
  xy: 3053, 1354
  size: 102, 110
  orig: 102, 110
  offset: 0, 0
  index: -1
eff_fire23
  rotate: true
  xy: 2078, 1038
  size: 113, 140
  orig: 113, 140
  offset: 0, 0
  index: -1
eff_fire24
  rotate: true
  xy: 3052, 381
  size: 119, 168
  orig: 119, 168
  offset: 0, 0
  index: -1
eyebrow_L
  rotate: false
  xy: 386, 8
  size: 54, 32
  orig: 54, 32
  offset: 0, 0
  index: -1
eyebrow_R
  rotate: false
  xy: 1178, 215
  size: 70, 44
  orig: 70, 44
  offset: 0, 0
  index: -1
face
  rotate: false
  xy: 2331, 6
  size: 161, 210
  orig: 161, 210
  offset: 0, 0
  index: -1
face1
  rotate: false
  xy: 3234, 1879
  size: 131, 162
  orig: 131, 162
  offset: 0, 0
  index: -1
fronthair
  rotate: false
  xy: 2856, 28
  size: 170, 191
  orig: 170, 191
  offset: 0, 0
  index: -1
fronthair2
  rotate: true
  xy: 1635, 212
  size: 77, 174
  orig: 77, 174
  offset: 0, 0
  index: -1
horn
  rotate: true
  xy: 1165, 1017
  size: 110, 317
  orig: 110, 317
  offset: 0, 0
  index: -1
legL
  rotate: true
  xy: 908, 1578
  size: 463, 1051
  orig: 463, 1051
  offset: 0, 0
  index: -1
legR
  rotate: true
  xy: 908, 1248
  size: 326, 956
  orig: 326, 956
  offset: 0, 0
  index: -1
neck
  rotate: true
  xy: 3056, 1492
  size: 158, 148
  orig: 158, 148
  offset: 0, 0
  index: -1
tail
  rotate: true
  xy: 4, 202
  size: 811, 799
  orig: 811, 799
  offset: 0, 0
  index: -1
waistclothL
  rotate: true
  xy: 807, 357
  size: 656, 880
  orig: 656, 880
  offset: 0, 0
  index: -1
waistclothR
  rotate: true
  xy: 2389, 655
  size: 379, 657
  orig: 379, 657
  offset: 0, 0
  index: -1
