
using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using KTPlaySDKJson;

#if UNITY_IOS
public class KTLeaderboardiOS : MonoBehaviour
{
	public const string LIB_NAME = "__Internal";

	[DllImport (LIB_NAME)]	
	private static extern void KT_ReportScoreCallback(string GameobjectName,string methodName,long score,string leaderboardId,string scoreTag);
	public static void ReportScore(long score,string leaderboardId,string scoreTag,MonoBehaviour obj,KTLeaderboardCommon.Callback callbackMethod)
	{
		if(obj!=null && callbackMethod!=null)
		{
			GameObject gameObj = obj.gameObject;
			if(gameObj != null && callbackMethod != null)
			{
				string methodName = ((System.Delegate)callbackMethod).Method.Name;
				if(methodName != null)
				{
					KT_ReportScoreCallback(gameObj.name,methodName,score,leaderboardId,scoreTag);
				}
			}
		}
	}
}
#endif
