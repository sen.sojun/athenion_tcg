
using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using KTPlaySDKJson;

#if UNITY_IOS
public class KTAccountManageriOS : MonoBehaviour
{
	public const string LIB_NAME = "__Internal";

	[DllImport (LIB_NAME)]
	private static extern void KT_SetLoginStatusChangeCallback (string GameobjectName, string methodName);
	public static void SetLoginStatusChangeCallback (MonoBehaviour obj, KTAccountManagerCommon.Callback callbackMethod)
	{
		if (obj != null && callbackMethod != null) {
			GameObject gameObj = obj.gameObject;
			if (gameObj != null && callbackMethod != null) {
				string methodName = ((System.Delegate)callbackMethod).Method.Name;
				if (methodName != null) {
					KT_SetLoginStatusChangeCallback (gameObj.name, methodName);
				}
			}
		}
	}

	[DllImport (LIB_NAME)]
	private static extern void KT_UserProfileCallback (string GameobjectName, string methodName, string userId);
	public static void UserProfile (string userId, MonoBehaviour obj, KTAccountManagerCommon.Callback callbackMethod)
	{
		if (obj != null && callbackMethod != null) {
			GameObject gameObj = obj.gameObject;
			if (gameObj != null && callbackMethod != null) {
				string methodName = ((System.Delegate)callbackMethod).Method.Name;
				if (methodName != null) {
					KT_UserProfileCallback (gameObj.name, methodName, userId);
				}
			}
		}
	}

	[DllImport (LIB_NAME)]
	private static extern void KT_LoginWithGameUser (string GameobjectName, string methodName, string gameUserId);
	public static void loginWithGameUser (string gameUserId, MonoBehaviour obj, KTAccountManagerCommon.Callback callbackMethod)
	{
		if (obj != null && callbackMethod != null) {
			GameObject gameObj = obj.gameObject;
			if (gameObj != null && callbackMethod != null) {
				string methodName = ((System.Delegate)callbackMethod).Method.Name;
				if (methodName != null) {
					KT_LoginWithGameUser (gameObj.name, methodName, gameUserId);
				}
			}
		}
	}

	[DllImport (LIB_NAME)]
	private static extern void KT_Logout ();
	public static void Logout ()
	{	
		KT_Logout ();
	}

	[DllImport (LIB_NAME)]
	private static extern void KT_ShowLoginView (bool closeable, string GameobjectName, string methodName);
	public static void ShowLoginView (bool closeable, MonoBehaviour obj, KTAccountManagerCommon.Callback callbackMethod)
	{									
		if (obj != null && callbackMethod != null) {
			GameObject gameObj = obj.gameObject;
			if (gameObj != null && callbackMethod != null) {
				string methodName = ((System.Delegate)callbackMethod).Method.Name;
				if (methodName != null) {
					KT_ShowLoginView (closeable, gameObj.name, methodName);
				}
			}
		}
	}

	[DllImport (LIB_NAME)]
	private static extern void KT_UpdateProfile (string GameobjectName, string methodName, string nickname, string avatarPath, int gender);
	public static void UpdateProfile (string nickname, string avatarPath, int gender, MonoBehaviour obj, KTAccountManagerCommon.Callback callbackMethod)
	{									
		if (obj != null && callbackMethod != null) {
			GameObject gameObj = obj.gameObject;
			if (gameObj != null && callbackMethod != null) {
				string methodName = ((System.Delegate)callbackMethod).Method.Name;
				if (methodName != null) {
					KT_UpdateProfile (gameObj.name, methodName,nickname, avatarPath, gender);
				}
			}
		}
	}

    [DllImport (LIB_NAME)]
    private static extern void KT_UpdateVipLevel (string GameobjectName, string methodName, int vipLevel);
	public static void UpdateVipLevel (int vipLevel, MonoBehaviour obj, KTAccountManagerCommon.Callback callbackMethod)
	{									
		if (obj != null && callbackMethod != null) {
			GameObject gameObj = obj.gameObject;
			if (gameObj != null && callbackMethod != null) {
				string methodName = ((System.Delegate)callbackMethod).Method.Name;
				if (methodName != null) {
					KT_UpdateVipLevel (gameObj.name, methodName, vipLevel);
				}
			}
		}
	}

	[DllImport (LIB_NAME)]
	private static extern void KT_setUserProfileLocks (int properties);
	public static void setUserProfileLocks (int properties)
	{									
		KT_setUserProfileLocks (properties);
	}

	[DllImport (LIB_NAME)]
	private static extern bool KT_IsLoggedIn ();
	public static bool IsLoggedIn ()
	{
		return KT_IsLoggedIn ();
	}

	[DllImport (LIB_NAME)]
	private static extern string  KT_CurrentAccount ();
	public static KTUser CurrentAccount ()
	{
		string str = (string)KT_CurrentAccount ();
		Hashtable data = (Hashtable)KTJSON.jsonDecode (str);
		return new KTUser (data);
	}
}

#endif
