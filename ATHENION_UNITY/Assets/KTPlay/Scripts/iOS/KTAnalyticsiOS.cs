
using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using KTPlaySDKJson;

#if UNITY_IOS
public class KTAnalyticsiOS : MonoBehaviour
{
	public const string LIB_NAME = "__Internal";

	[DllImport (LIB_NAME)]
	private static extern void KT_SetUserProperty(string jsonData);
	public static void SetUserProperty(string name, object value)
	{
		Hashtable dict=new Hashtable(); 
		dict.Add ("value", value);
		dict.Add ("name", name);
		string s_properties = KTJSON.jsonEncode(dict);
		KT_SetUserProperty(s_properties);
	}

	[DllImport (LIB_NAME)]
	private static extern void KT_LogEvent(string name, string properties);
	public static void LogEvent(string name, Hashtable properties)
	{
		string s_properties = KTJSON.jsonEncode(properties);
		KT_LogEvent(name,s_properties);
	}
}
#endif
