
using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using KTPlaySDKJson;

#if UNITY_IOS
public class KTPlayiOS : MonoBehaviour
{
	public const string LIB_NAME = "__Internal";

	[DllImport (LIB_NAME)]
	private static extern void KT_SetOnAppearCallback(string GameobjectName,string methodName);
	public static void SetViewDidAppearCallback(MonoBehaviour obj,KTPlayCommon.Callback callbackMethod)
	{
		if(obj!=null && callbackMethod!=null)
		{
			GameObject gameObj = obj.gameObject;
			if(gameObj != null && callbackMethod != null)
			{
				string methodName = ((System.Delegate)callbackMethod).Method.Name;
				if(methodName != null)
				{
					KT_SetOnAppearCallback(gameObj.name,methodName);
				}
			}
		}
	}

	[DllImport (LIB_NAME)]
	private static extern void KT_SetOnDisappearCallback(string GameobjectName,string methodName);
	public static void SetViewDidDisappearCallback(MonoBehaviour obj,KTPlayCommon.Callback callbackMethod)
	{
		if(obj!=null && callbackMethod!=null)
		{
			GameObject gameObj = obj.gameObject;
			if(gameObj != null && callbackMethod != null)
			{
				string methodName = ((System.Delegate)callbackMethod).Method.Name;
				if(methodName != null)
				{
					KT_SetOnDisappearCallback(gameObj.name,methodName);
				}
			}
		}
	}

	[DllImport (LIB_NAME)]
	private static extern void KT_SetOnSoundStartCallback(string GameobjectName,string methodName);
	public static void SetOnSoundStartCallback(MonoBehaviour obj,KTPlayCommon.Callback callbackMethod)
	{
		if(obj!=null && callbackMethod!=null)
		{
			GameObject gameObj = obj.gameObject;
			if(gameObj != null && callbackMethod != null)
			{
				string methodName = ((System.Delegate)callbackMethod).Method.Name;
				if(methodName != null)
				{
					KT_SetOnSoundStartCallback(gameObj.name,methodName);
				}
			}
		}
	}
	
	[DllImport (LIB_NAME)]
	private static extern void KT_SetOnSoundStopCallback(string GameobjectName,string methodName);
	public static void SetOnSoundStopCallback(MonoBehaviour obj,KTPlayCommon.Callback callbackMethod)
	{
		if(obj!=null && callbackMethod!=null)
		{
			GameObject gameObj = obj.gameObject;
			if(gameObj != null && callbackMethod != null)
			{
				string methodName = ((System.Delegate)callbackMethod).Method.Name;
				if(methodName != null)
				{
					KT_SetOnSoundStopCallback(gameObj.name,methodName);
				}
			}
		}
	}

	[DllImport (LIB_NAME)]
	private static extern void KT_SetOnDispatchRewardsCallback(string GameobjectName,string methodName);
	public static void SetDidDispatchRewardsCallback(MonoBehaviour obj,KTPlayCommon.Callback callbackMethod)
	{
		if(obj!=null && callbackMethod!=null)
		{
			GameObject gameObj = obj.gameObject;
			if(gameObj != null && callbackMethod != null)
			{
				string methodName = ((System.Delegate)callbackMethod).Method.Name;
				if(methodName != null)
				{
					KT_SetOnDispatchRewardsCallback(gameObj.name,methodName);
				}
			}
		}
	}

	[DllImport (LIB_NAME)]
	private static extern void KT_SetOnActivityStatusChangedCallback(string GameobjectName,string methodName);
	public static void SetActivityStatusChangedCallback(MonoBehaviour obj,KTPlayCommon.Callback callbackMethod)
	{
		if(obj!=null && callbackMethod!=null)
		{
			GameObject gameObj = obj.gameObject;
			if(gameObj != null && callbackMethod != null)
			{
				string methodName = ((System.Delegate)callbackMethod).Method.Name;
				if(methodName != null)
				{
					KT_SetOnActivityStatusChangedCallback(gameObj.name,methodName);
				}
			}
		}
	}

	[DllImport (LIB_NAME)]
	private static extern void KT_SetOnAvailabilityChangedCallback(string GameobjectName,string methodName);
	public static void SetAvailabilityChangedCallback(MonoBehaviour obj,KTPlayCommon.Callback callbackMethod)
	{
		if(obj!=null && callbackMethod!=null)
		{
			GameObject gameObj = obj.gameObject;
			if(gameObj != null && callbackMethod != null)
			{
				string methodName = ((System.Delegate)callbackMethod).Method.Name;
				if(methodName != null)
				{
					KT_SetOnAvailabilityChangedCallback(gameObj.name,methodName);
				}
			}
		}
	}
	[DllImport (LIB_NAME)]
	private static extern void KT_SetDeepLinkCallback(string GameobjectName,string methodName);
	public static void SetDeepLinkCallback(MonoBehaviour obj,KTPlayCommon.Callback callbackMethod)
	{
		if(obj!=null && callbackMethod!=null)
		{
			GameObject gameObj = obj.gameObject;
			if(gameObj != null && callbackMethod != null)
			{
				string methodName = ((System.Delegate)callbackMethod).Method.Name;
				if(methodName != null)
				{
					KT_SetDeepLinkCallback(gameObj.name,methodName);
				}
			}
		}
	}


	[DllImport (LIB_NAME)]
	private static extern void KT_ShowKTPlayView();
	public static void Show()
	{
		KT_ShowKTPlayView();
	}


	[DllImport (LIB_NAME)]
	private static extern void KT_DismissKTPlayView();
	public static void Dismiss()
	{
		KT_DismissKTPlayView();
	}

	[DllImport (LIB_NAME)]
	private static extern bool KT_IsEnabled();
	public static bool IsEnabled()
	{
		return KT_IsEnabled();
	}

	[DllImport (LIB_NAME)]
	private static extern bool KT_IsKTPlayViewShowing();
	public static bool IsShowing()
	{
		return KT_IsKTPlayViewShowing();
	}

	[DllImport (LIB_NAME)]
	private static extern void KT_SetNotificationEnabled( bool enabled);
	public static void SetNotificationEnabled(bool enabled)
	{
		 KT_SetNotificationEnabled(enabled);
	}

	[DllImport (LIB_NAME)]
	private static extern void KT_ShareImageToKT(string imagePath,string title, string description);
	public static void ShareImageToKT(string imagePath,string title, string description)
	{
		 KT_ShareImageToKT(imagePath, title, description);
	}

	[DllImport (LIB_NAME)]
	private static extern void KT_ShareVideoToKT(string videoPath, string title, string description);
	public static void ShareVideoToKT(string videoPath,string title, string description)
	{
		KT_ShareVideoToKT(videoPath, title, description);
	}

	[DllImport (LIB_NAME)]
	private static extern void KT_ShowInterstitialNoti(string GameobjectName,string methodName, string identifier);
	public static void ShowInterstitialNoti(MonoBehaviour obj,KTPlayCommon.Callback callbackMethod, string identifier)
	{
		if(obj!=null && callbackMethod!=null)
		{
			GameObject gameObj = obj.gameObject;
			if(gameObj != null && callbackMethod != null)
			{
				string methodName = ((System.Delegate)callbackMethod).Method.Name;
				if(methodName != null)
				{
					KT_ShowInterstitialNoti(gameObj.name, methodName, identifier);
				}
			}
		}
	}

	[DllImport (LIB_NAME)]
	private static extern bool KT_HasInterstitialNotification(string identifier);
	public static bool HasInterstitialNotification(string identifier)
	{
		return KT_HasInterstitialNotification(identifier);
	}

	[DllImport (LIB_NAME)]
	private static extern bool KT_RequestInterstitialNotification(string identifier);
	public static bool RequestInterstitialNotification(string identifier)
	{
		return KT_RequestInterstitialNotification(identifier);
	}

	[DllImport (LIB_NAME)]
	private static extern void KT_startWithAppKey(string appKey,string appSecret);
	public static void startWithAppKey(string appKey,string appSecret)
	{
		KT_startWithAppKey(appKey,appSecret);
	}

	[DllImport (LIB_NAME)]
	private static extern bool KT_setLanguage(string preferredLanguage,string alternateLanguage);
	public static bool SetLanguage(string preferredLanguage,string alternateLanguage)
	{
		return KT_setLanguage(preferredLanguage,alternateLanguage);
	}

	[DllImport (LIB_NAME)]
	private static extern void KT_OpenDeepLink(string deepLinkId);
	public static void OpenDeepLink(string deepLinkId)
	{
		 KT_OpenDeepLink(deepLinkId);
	}
}

#endif
