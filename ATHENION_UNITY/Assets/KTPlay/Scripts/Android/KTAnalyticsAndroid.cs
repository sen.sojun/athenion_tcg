
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using KTPlaySDKJson;

#if UNITY_ANDROID
public class KTAnalyticsAndroid : MonoBehaviour 
{
	public static void SetUserProperty(string name, object value)
	{
		if(value is int)
		{
			AndroidJavaObject jo = new AndroidJavaObject("java.lang.Integer", (int)value);
			KTPlayAndroid.joKTPlayAdapter.CallStatic("setUserProperty",name, jo);
		}
		else if(value is long)
		{
			AndroidJavaObject jo = new AndroidJavaObject("java.lang.Long", (long)value);
			KTPlayAndroid.joKTPlayAdapter.CallStatic("setUserProperty",name, jo);
		}
		else if(value is double)
		{
			AndroidJavaObject jo = new AndroidJavaObject("java.lang.Double", (double)value);
			KTPlayAndroid.joKTPlayAdapter.CallStatic("setUserProperty",name, jo);
		}
		else if(value is string)
		{
			KTPlayAndroid.joKTPlayAdapter.CallStatic("setUserProperty",name, (string)value);
		}
		else if(value is bool)
		{
			AndroidJavaObject jo = new AndroidJavaObject("java.lang.Boolean", (bool)value);
			KTPlayAndroid.joKTPlayAdapter.CallStatic("setUserProperty",name, jo);
		}
	}

	public static void LogEvent(string name, Hashtable properties)
	{
		if(properties == null)
		{
		     return;
		}
		AndroidJavaObject map = new AndroidJavaObject("java.util.HashMap");
		foreach(DictionaryEntry kvp in properties)
		{
			if(kvp.Value is int)
			{
				AndroidJavaObject jo = new AndroidJavaObject("java.lang.Integer", (int)kvp.Value);
				map.Call<string>("put", kvp.Key, jo);
			}
			else if(kvp.Value is long)
			{
				AndroidJavaObject jo = new AndroidJavaObject("java.lang.Long", (long)kvp.Value);
				map.Call<string>("put", kvp.Key, jo);
			}
			
			else if(kvp.Value is double)
			{
				AndroidJavaObject jo = new AndroidJavaObject("java.lang.Double", (double)kvp.Value);
				map.Call<string>("put", kvp.Key, jo);
			}
			else if(kvp.Value is string)
			{
				map.Call<string>("put", kvp.Key, (string)kvp.Value);
			}
			else if(kvp.Value is bool)
			{
				AndroidJavaObject jo = new AndroidJavaObject("java.lang.Boolean", (bool)kvp.Value);
				map.Call<string>("put", kvp.Key, jo);
			}
		}
		KTPlayAndroid.joKTPlayAdapter.CallStatic("logEvent", name, map);
	}
}
#endif