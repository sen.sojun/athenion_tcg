
using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using KTPlaySDKJson;

#if UNITY_ANDROID
public class KTPlayAndroid
{
	//private static AndroidJavaObject joKTPlayInternal = null;
	public static AndroidJavaObject joKTPlayAdapter = null;

	public static void Init()
	{
		//joKTPlayInternal = new AndroidJavaObject("com.ktplay.core.KryptaniumInternal");
		joKTPlayAdapter = new AndroidJavaObject("com.ktplay.open.KryptaniumAdapter");

	}

	public static void SetViewDidAppearCallback(MonoBehaviour obj,KTPlayCommon.Callback callbackMethod)
	{
		if(obj!=null && callbackMethod!=null)
		{
			GameObject gameObj = obj.gameObject;
			if(gameObj != null && callbackMethod != null)
			{
				string methodName = ((System.Delegate)callbackMethod).Method.Name;
				if(methodName != null)
				{
					joKTPlayAdapter.CallStatic("setOnAppearListener", gameObj.name, methodName);

				}
			}
		}
	}

	public static void SetViewDidDisappearCallback(MonoBehaviour obj,KTPlayCommon.Callback callbackMethod)
	{
		if(obj!=null && callbackMethod!=null)
		{
			GameObject gameObj = obj.gameObject;
			if(gameObj != null && callbackMethod != null)
			{
				string methodName = ((System.Delegate)callbackMethod).Method.Name;
				if(methodName != null)
				{
					joKTPlayAdapter.CallStatic("setOnDisappearListener", gameObj.name, methodName);
				}
			}
		}
	}

	public static void SetDidDispatchRewardsCallback(MonoBehaviour obj,KTPlayCommon.Callback callbackMethod)
	{
		if(obj!=null && callbackMethod!=null)
		{
			GameObject gameObj = obj.gameObject;
			if(gameObj != null && callbackMethod != null)
			{
				string methodName = ((System.Delegate)callbackMethod).Method.Name;
				if(methodName != null)
				{
					joKTPlayAdapter.CallStatic("setOnDispatchRewardsListener", gameObj.name, methodName);
				}
			}
		}
	}

	public static void SetActivityStatusChangedCallback(MonoBehaviour obj,KTPlayCommon.Callback callbackMethod)
	{
		if(obj!=null && callbackMethod!=null)
		{
			GameObject gameObj = obj.gameObject;
			if(gameObj != null && callbackMethod != null)
			{
				string methodName = ((System.Delegate)callbackMethod).Method.Name;
				if(methodName != null)
				{
					joKTPlayAdapter.CallStatic("setOnActivityStatusChangedListener", gameObj.name, methodName);
				}
			}
		}
	}

	public static void SetAvailabilityChangedCallback(MonoBehaviour obj,KTPlayCommon.Callback callbackMethod)
	{
		if(obj!=null && callbackMethod!=null)
		{
			GameObject gameObj = obj.gameObject;
			if(gameObj != null && callbackMethod != null)
			{
				string methodName = ((System.Delegate)callbackMethod).Method.Name;
				if(methodName != null)
				{
					joKTPlayAdapter.CallStatic("setOnAvailabilityChangedListener", gameObj.name, methodName);
				}
			}
		}
	}
	public static void SetDeepLinkCallback(MonoBehaviour obj,KTPlayCommon.Callback callbackMethod)
	{
		if(obj!=null && callbackMethod!=null)
		{
			GameObject gameObj = obj.gameObject;
			if(gameObj != null && callbackMethod != null)
			{
				string methodName = ((System.Delegate)callbackMethod).Method.Name;
				if(methodName != null)
				{
					joKTPlayAdapter.CallStatic("setOnDeepLinkListener", gameObj.name, methodName);
				}
			}
		}
	}

	public static void startWithAppKey(string appKey,string appSecret)
	{
		Debug.Log ("[Android]startWithAppKey(" + appKey + "," + appSecret + ")");
		AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer"); 
		AndroidJavaObject jo = jc.GetStatic<AndroidJavaObject>("currentActivity");
		AndroidJavaClass ktplay = new AndroidJavaClass("com.ktplay.open.KTPlay");
		ktplay.CallStatic("startWithAppKey",jo,appKey,appSecret);
		Debug.Log ("[Android]startWithAppKey,context=" + jo);
	}

	public static void HanldeApplicationPause (bool pauseStatus)
	{
		AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer"); 
		AndroidJavaObject jo = jc.GetStatic<AndroidJavaObject>("currentActivity");
		AndroidJavaClass ktplay = new AndroidJavaClass("com.ktplay.open.KTPlay");
		if(pauseStatus){
			ktplay.CallStatic("onPause",jo);
		}else{
			ktplay.CallStatic("onResume",jo);
		}
	}

	public static void Show()
	{
		joKTPlayAdapter.CallStatic("showKryptaniumView");
	}

	public static void ShowRedemptionView()
	{
		joKTPlayAdapter.CallStatic("showRedemptionView");
	}

	public static void ShowInterstitialNotification()
	{
		joKTPlayAdapter.CallStatic("showInterstitialNotification");
	}

	public static void Dismiss()
	{
		joKTPlayAdapter.CallStatic("dismiss");
	}

	public static bool IsEnabled()
	{
		return joKTPlayAdapter.CallStatic<bool>("isEnabled");
	}

	public static bool IsShowing()
	{
		return joKTPlayAdapter.CallStatic<bool>("isShowing");
	}

	public static void ShareImageToKT(string imagePath,string description)
	{
		joKTPlayAdapter.CallStatic("shareImageToKT", imagePath, description);
	}

	public static void ShareImageToKT(string imagePath,string title,string description)
	{
		joKTPlayAdapter.CallStatic("shareImageToKT", imagePath, title, description);
	}

	public static void ShareVideoToKT(string videoPath,string description)
	{
		joKTPlayAdapter.CallStatic("shareVideoToKT", videoPath, description);
	}

	public static void ShareVideoToKT(string videoPath,string title,string description)
	{
		joKTPlayAdapter.CallStatic("shareVideoToKT", videoPath, title, description);
	}

	public static void SetNotificationEnabled(bool isEnabled)
	{
		joKTPlayAdapter.CallStatic("setNotificationEnabled", isEnabled);
	}

	public static void showInterstitialNotification(MonoBehaviour obj,KTPlayCommon.Callback callbackMethod,string identifier)
	{
		if(obj!=null && callbackMethod!=null && identifier!=null)
		{
			GameObject gameObj = obj.gameObject;
			if(gameObj != null && callbackMethod != null)
			{
				string methodName = ((System.Delegate)callbackMethod).Method.Name;
				if(methodName != null)
				{
					joKTPlayAdapter.CallStatic("showInterstitialNotification", gameObj.name, methodName,identifier);
				}
			}
		}
	}
	
	public static void requestInterstitialNotification(string identifier)
	{
		joKTPlayAdapter.CallStatic("requestInterstitialNotification", identifier);
	}
	
	public static bool hasInterstitialNotification(string identifier)
	{
		return joKTPlayAdapter.CallStatic<bool>("hasInterstitialNotification", identifier);
	}

	public static void SetOnSoundStartCallback(MonoBehaviour obj,KTPlayCommon.Callback callbackMethod)
	{
		if(obj!=null && callbackMethod!=null)
		{
			GameObject gameObj = obj.gameObject;
			if(gameObj != null && callbackMethod != null)
			{
				string methodName = ((System.Delegate)callbackMethod).Method.Name;
				if(methodName != null)
				{
					joKTPlayAdapter.CallStatic("setOnSoundStartListener", gameObj.name, methodName);
				}
			}
		}
	}

	public static void SetOnSoundStopCallback(MonoBehaviour obj,KTPlayCommon.Callback callbackMethod)
	{
		if(obj!=null && callbackMethod!=null)
		{
			GameObject gameObj = obj.gameObject;
			if(gameObj != null && callbackMethod != null)
			{
				string methodName = ((System.Delegate)callbackMethod).Method.Name;
				if(methodName != null)
				{
					joKTPlayAdapter.CallStatic("setOnSoundStopListener", gameObj.name, methodName);
				}
			}
		}
	}

	public static bool SetLanguage(string preferredLanguage,string alternateLanguage)
	{
		return joKTPlayAdapter.CallStatic<bool>("setLanguage", preferredLanguage, alternateLanguage);
	}

	public static void OpenDeepLink(string deepLinkId)
	{
		joKTPlayAdapter.CallStatic("openDeepLink", deepLinkId);
	}
}
#endif