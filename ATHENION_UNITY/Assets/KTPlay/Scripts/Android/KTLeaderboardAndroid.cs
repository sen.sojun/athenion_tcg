
using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using KTPlaySDKJson;
#if UNITY_ANDROID
public class KTLeaderboardAndroid : MonoBehaviour
{
	public static void ReportScore(long score,string leaderboardId,string scoreTag, MonoBehaviour obj,KTLeaderboardCommon.Callback callbackMethod)
	{
		if(obj!=null && callbackMethod!=null)
		{
			GameObject gameObj = obj.gameObject;
			if(gameObj != null && callbackMethod != null)
			{
				string methodName = ((System.Delegate)callbackMethod).Method.Name;
				if(methodName != null)
				{
					KTPlayAndroid.joKTPlayAdapter.CallStatic("reportScore", score, leaderboardId, scoreTag, gameObj.name, methodName);
				}
			}
		}
	}
}
#endif