
using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using KTPlaySDKJson;

#if UNITY_ANDROID
public class KTAccountManagerAndroid : MonoBehaviour
{
	public static void SetLoginStatusChangeCallback(MonoBehaviour obj,KTAccountManagerCommon.Callback callbackMethod)
	{
		if(obj!=null && callbackMethod!=null)
		{
			GameObject gameObj = obj.gameObject;
			if(gameObj != null && callbackMethod != null)
			{
				string methodName = ((System.Delegate)callbackMethod).Method.Name;
				if(methodName != null)
				{
					KTPlayAndroid.joKTPlayAdapter.CallStatic("setLoginStatusChangedListener", gameObj.name, methodName);
				}
			}
		}

	}

	public static void UserProfile(string userId,MonoBehaviour obj,KTAccountManagerCommon.Callback callbackMethod)
	{	
		if(obj!=null && callbackMethod!=null)
		{
			GameObject gameObj = obj.gameObject;
			if(gameObj != null && callbackMethod != null)
			{
				string methodName = ((System.Delegate)callbackMethod).Method.Name;
				if(methodName != null)
				{
					KTPlayAndroid.joKTPlayAdapter.CallStatic("userProfile", userId, gameObj.name, methodName);
				}
			}
		}
	}

	public static void ShowLoginView(bool closeable, MonoBehaviour obj,KTAccountManagerCommon.Callback callbackMethod)
	{
		if(obj!=null && callbackMethod!=null)
		{
			GameObject gameObj = obj.gameObject;
			if(gameObj != null && callbackMethod != null)
			{
				string methodName = ((System.Delegate)callbackMethod).Method.Name;
				if(methodName != null)
				{
					KTPlayAndroid.joKTPlayAdapter.CallStatic("showLoginView", closeable, gameObj.name, methodName);
				}
			}
		}
	}

	public static void loginWithGameUser(string gameUserId, MonoBehaviour obj,KTAccountManagerCommon.Callback callbackMethod)
	{
		if(obj!=null && callbackMethod!=null)
		{
			GameObject gameObj = obj.gameObject;
			if(gameObj != null && callbackMethod != null)
			{
				string methodName = ((System.Delegate)callbackMethod).Method.Name;
				if(methodName != null)
				{
					KTPlayAndroid.joKTPlayAdapter.CallStatic("loginWithGameUser", gameUserId, gameObj.name, methodName);
				}
			}
		}
	}

	public static void UpdateProfile(string nickName, string avatarPath, int gender, MonoBehaviour obj,KTAccountManagerCommon.Callback callbackMethod)
	{
		if(obj!=null && callbackMethod!=null)
		{
			GameObject gameObj = obj.gameObject;
			if(gameObj != null && callbackMethod != null)
			{
				string methodName = ((System.Delegate)callbackMethod).Method.Name;
				if(methodName != null)
				{
					KTPlayAndroid.joKTPlayAdapter.CallStatic("updateProfile", nickName, avatarPath, gender, gameObj.name, methodName);
				}
			}
		}
	}

    public static void UpdateVipLevel(int vipLevel, MonoBehaviour obj,KTAccountManagerCommon.Callback callbackMethod)
	{
		if(obj!=null && callbackMethod!=null)
		{
			GameObject gameObj = obj.gameObject;
			if(gameObj != null && callbackMethod != null)
			{
				string methodName = ((System.Delegate)callbackMethod).Method.Name;
				if(methodName != null)
				{
					KTPlayAndroid.joKTPlayAdapter.CallStatic("updateVipLevel", vipLevel, gameObj.name, methodName);
				}
			}
		}
	}

	public static void setUserProfileLocks(int properties)
	{
		KTPlayAndroid.joKTPlayAdapter.CallStatic("setUserProfileLocks", properties);
	}

	public static bool IsLoggedIn()
	{
		return KTPlayAndroid.joKTPlayAdapter.CallStatic<bool>("isLoggedIn");
	}

	public static void Logout()
	{
		KTPlayAndroid.joKTPlayAdapter.CallStatic("logout");
	}

	public static KTUser CurrentAccount()
	{
		AndroidJavaObject joUser = null;
		try
		{
			joUser = KTPlayAndroid.joKTPlayAdapter.CallStatic<AndroidJavaObject>("currentAccount");
		}
		catch (System.Exception e)
		{
			return null;
		}
		if(joUser == null)
		{
			return null;
		}
		string userid = joUser.Call<string>("getUserId");
		string headerurl = joUser.Call<string>("getHeaderUrl");
		string nickname = joUser.Call<string>("getNickname");
		int gender = joUser.Call<int>("getGender");
		string city = joUser.Call<string>("getCity");
		string score = joUser.Call<string>("getScore");
		long rank = joUser.Call<long>("getRank");
		string snsuserid = joUser.Call<string>("getSnsUserId");
		string logintype = joUser.Call<string>("getLoginType");
		string gameUserId = joUser.Call<string>("getGameUserId");
		bool needPresentNickname = joUser.Call<bool>("getNeedPresentNickname");

		KTUser user = new KTUser();
		user.setUserId(userid);
		user.setHeaderUrl(headerurl);
		user.setNickname(nickname);
		user.setGender(gender);
		user.setCity(city);
		user.setScore(score);
		user.setRank(rank);
		user.setSnsUserId(snsuserid);
		user.setLoginType(logintype);
		user.setGameUserId(gameUserId);
		user.setNeedPresentNickname(needPresentNickname);

		return user;
	}

	public static void UseExternalLogin()
	{
		KTPlayAndroid.joKTPlayAdapter.CallStatic("useExternalLogin");
	}
}

#endif
