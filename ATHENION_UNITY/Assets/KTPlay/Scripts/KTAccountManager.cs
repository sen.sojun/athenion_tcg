/********************************************************************************

** (C) 2016 KTplay. All Rights Reserved.


*********************************************************************************/

using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using KTPlaySDKJson;

public class KTAccountManager : MonoBehaviour
{

	public const  int KT_USERPROFILE_PROPERTY_NICKNAME = 1 << 0;
	public const  int KT_USERPROFILE_PROPERTY_AVATAR = 1 << 1;
	public const  int KT_USERPROFILE_PROPERTY_GENDER = 1 << 2;

	/// <summary>
	///  KTLoginStatusChangeDelegate
	/// </summary>
	/// <param name="isLogined">isLogined</param>
	/// <param name="user">user info</param>
	public delegate void KTLoginStatusChangeDelegate(bool isLoggedIn, KTUser user);


	/// <summary>
	///  KTGetUserProfileDelegate
	/// </summary>
	/// <param name="user">user info</param>
	/// <param name="error">error info</param>
	public delegate void KTGetUserProfileDelegate(KTUser user,KTError error);

	/// <summary>
	///  KTShowLoginViewDelegate
	/// </summary>
	/// <param name="user">user info</param>
	/// <param name="error">error info</param>
	public delegate void KTShowLoginViewDelegate(KTUser user,KTError error);

	/// <summary>
	///  KTLoginWithGameUserDelegate
	/// </summary>
	/// <param name="user">user info</param>
	/// <param name="error">error info</param>
	public delegate void KTLoginWithGameUserDelegate(KTUser user,KTError error);

	/// <summary>
	///  KTUpdateProfileDelegate
	/// </summary>
	/// <param name="isSuccess">isSuccess</param>
	/// <param name="user">user info</param>
	/// <param name="error">error info</param>
	public delegate void KTUpdateProfileDelegate(bool isSuccess,KTUser user,KTError error);

    /// <summary>
	///  KTUpdateVipLevelDelegate
	/// </summary>
	/// <param name="isSuccess">isSuccess</param>
	/// <param name="user">user info</param>
	/// <param name="error">error info</param>
	public delegate void KTUpdateVipLevelDelegate(bool isSuccess,KTUser user,KTError error);

	private KTLoginStatusChangeDelegate ktLoginStatusChangeDelegate;
	private KTGetUserProfileDelegate ktGetUserProfileDelegate;
	private KTShowLoginViewDelegate ktShowLoginViewDelegate;
	private KTLoginWithGameUserDelegate ktLoginWithGameUserDelegate;
	private KTUpdateProfileDelegate ktUpdateProfileDelegate;
    private KTUpdateVipLevelDelegate ktUpdateVipLevelDelegate;

	private static KTAccountManager instance;
	private static GameObject ktplayObj;

	private static KTAccountManager GetInstance() 
	{
		if (instance == null) 
		{
			ktplayObj = GameObject.Find("KTPlay");
			var existed = ktplayObj.GetComponent <KTAccountManager>();
			instance = existed ?? ktplayObj.AddComponent <KTAccountManager>();

            DontDestroyOnLoad(instance.gameObject);
        }
		return instance;
	}

	/// <summary>
	///   Setup listener for  User login status changed
	/// </summary>
	/// <param name="delegateMethod">login status change callback method</param>
	public static void SetLoginStatusChangeDelegate(KTLoginStatusChangeDelegate delegateMethod) 
	{
		KTAccountManager.GetInstance().SetLoginStatusChangeCallback (delegateMethod);
	}

	/// <summary>
	///  Get user Profile
	/// </summary>
	/// <param name="userId">user id</param>
	/// <param name="delegateMethod">user Profile callback method</param>
	public static void GetUserProfile(string userId, KTGetUserProfileDelegate delegateMethod) 
	{
		KTAccountManager.GetInstance().GetUserProfileCallback (userId, delegateMethod);
	}

	/// <summary>
	///  Show login window
	/// </summary>
	/// <param name="closeable"> The login window is closeable or not</param>
	/// <param name="delegateMethod">login callback method</param>
	public static void ShowLoginView(bool closeable, KTShowLoginViewDelegate delegateMethod) 
	{
		KTAccountManager.GetInstance().ShowLoginViewCallback (closeable, delegateMethod);
	}

	/// <summary>
	///  Login with game user
	/// </summary>
	/// <param name="gameUserId"> game user id</param>
	/// <param name="delegateMethod">login callback method</param>
	public static void LoginWithGameUser(string gameUserId, KTLoginWithGameUserDelegate delegateMethod) 
	{
		KTAccountManager.GetInstance().LoginWithGameUserCallback (gameUserId, delegateMethod);
	}

	/// <summary>
	///  UpdateProfile
	/// </summary>
	/// <param name="nickname">ignore nickname changes when set null</param>
	/// <param name="avatarPath">ignore avatarPath changes when set null</param>
	/// <param name="gender">0:ignore ; 1:male ;2:female</param>
	/// <param name="delegateMethod">UpdateProfile callback method</param>
	public static void UpdateProfile(string nickname, string avatarPath, int gender, KTUpdateProfileDelegate delegateMethod) 
	{
		KTAccountManager.GetInstance().UpdateProfileCallback (nickname,avatarPath,gender, delegateMethod);
	}

    /// <summary>
	///  UpdateVipLevel
	/// </summary>
	/// <param name="vipLevel">great than or equal to zero</param>v
	/// <param name="delegateMethod">UpdateVipLevel callback method</param>
	public static void UpdateVipLevel(int vipLevel, KTUpdateVipLevelDelegate delegateMethod) 
	{
		KTAccountManager.GetInstance().UpdateVipLevelCallback (vipLevel, delegateMethod);
	}

	/// <summary>
	///  The method is used to disable (lock) or enable (unlock) UI for editing User profile properties such as nickname. 
	/// </summary>
	/// <param name="properties">The existence of the following param(s) will imply the respective User Profile’s property will be disabled (locked) for editing.</param>
	/// <see>KT_USERPROFILE_PROPERTY_NICKNAME</see>
	/// <see>KT_USERPROFILE_PROPERTY_AVATAR</see>
	/// <see>KT_USERPROFILE_PROPERTY_GENDER</see>
	public static void setUserProfileLocks(int properties) 
	{
		KTAccountManagerCommon.setUserProfileLocks(properties);
	}

	/// <summary>
	///  Logout
	/// </summary>
	public static void Logout() 
	{
		KTAccountManagerCommon.Logout ();
	}

	/// <summary>
	///  Check if any user is currently logged in.
	/// </summary>
	/// <return>Any user is currently logged in.</return>
	public static bool isLoggedIn() 
	{
		return KTAccountManagerCommon.IsLoggedIn ();
	}

	/// <summary>
	///  Get user information of currently logged in user
	/// </summary>
	/// <return>Information of currently logged in user</return>
	public static KTUser CurrentAccount()
	{
		return KTAccountManagerCommon.CurrentAccount ();
	}

	//=======================================private method====================================
	private void SetLoginStatusChangeCallback(KTLoginStatusChangeDelegate delegateMethod) 
	{
		if (delegateMethod != null)
		{
			this.ktLoginStatusChangeDelegate = delegateMethod;
		}
		KTAccountManagerCommon.SetLoginStatusChange (this, this.onAccountManager);
	}

	private void GetUserProfileCallback(string userId, KTGetUserProfileDelegate delegateMethod) 
	{
		if (delegateMethod != null) 
		{
			this.ktGetUserProfileDelegate = delegateMethod;
		}
		KTAccountManagerCommon.UserProfile (userId, this, this.onAccountManager);
	}

	private void ShowLoginViewCallback(bool closeable, KTShowLoginViewDelegate delegateMethod) 
	{
		if (delegateMethod != null)
		{
			this.ktShowLoginViewDelegate = delegateMethod;
		}
		KTAccountManagerCommon.ShowLoginView (closeable, this, this.onAccountManager);
	}

	private void LoginWithGameUserCallback(string gameUserId, KTLoginWithGameUserDelegate delegateMethod) 
	{
		if (delegateMethod != null) 
		{
			this.ktLoginWithGameUserDelegate = delegateMethod;
		}
		KTAccountManagerCommon.loginWithGameUser (gameUserId, this, this.onAccountManager);
	}

	private void UpdateProfileCallback(string nickname, string avatarPath, int gender, KTUpdateProfileDelegate delegateMethod) 
	{
		if (delegateMethod != null)
		{
			this.ktUpdateProfileDelegate = delegateMethod;
		}
		KTAccountManagerCommon.UpdateProfile (nickname,avatarPath,gender, this, this.onAccountManager);
	}

	private void UpdateVipLevelCallback(int vipLevel, KTUpdateVipLevelDelegate delegateMethod) 
	{
		if (delegateMethod != null)
		{
			this.ktUpdateVipLevelDelegate = delegateMethod;
		}
		KTAccountManagerCommon.UpdateVipLevel (vipLevel, this, this.onAccountManager);
	}

	private void onAccountManager(string param)
	{
		KTAccountManagerCallbackParams accountParam = new KTAccountManagerCallbackParams(param);
		switch(accountParam.accountManagerEventResult)
		{
			case KTAccountManagerCallbackParams.KTAccountManagerEvent.KTPlayAccountEventUserProfile:
			{
				KTError error = (KTError)accountParam.userProfileError;
				KTUser user = accountParam.oneUser as KTUser;
				if (this.ktGetUserProfileDelegate != null) 
				{
					this.ktGetUserProfileDelegate(user,error);
				}
			} break;
			case KTAccountManagerCallbackParams.KTAccountManagerEvent.KTPlayAccountEventStatusChange:
			{
				KTUser user = accountParam.statusUser as KTUser;
				if (this.ktLoginStatusChangeDelegate != null)
				{
					this.ktLoginStatusChangeDelegate(accountParam.isLogin, user);
				}
			} break;
			case KTAccountManagerCallbackParams.KTAccountManagerEvent.KTPlayAccountEventLoginViewLogin:
			{
				KTError error = (KTError)accountParam.loginViewError;
				KTUser user = accountParam.oneUser as KTUser;
				if (this.ktShowLoginViewDelegate != null)
				{
					this.ktShowLoginViewDelegate(user,error);
				}
			} break;
			case KTAccountManagerCallbackParams.KTAccountManagerEvent.KTPlayAccountEventLoginWhithGameUser:
			{
				KTError error = (KTError)accountParam.loginWhithGameUserError;
				KTUser user = accountParam.oneUser as KTUser;
				if (this.ktLoginWithGameUserDelegate != null) 
				{
					this.ktLoginWithGameUserDelegate(user,error);
				}
			} break;
			case KTAccountManagerCallbackParams.KTAccountManagerEvent.KTPlayAccountEventUpdateProfile:
			{
				KTError error = (KTError)accountParam.accountEventUpdateProfileError;
				bool isSuccess = accountParam.isSuccess;
				KTUser user = accountParam.profileUser as KTUser;
				if (this.ktUpdateProfileDelegate != null) 
				{
					this.ktUpdateProfileDelegate (isSuccess,user,error);
				}
			} break;
			case KTAccountManagerCallbackParams.KTAccountManagerEvent.KTPlayAccountEventUpdateVipLevel:
			{
				KTError error = (KTError)accountParam.accountEventUpdateVipLevelError;
				bool isSuccess = accountParam.isSuccess;
				KTUser user = accountParam.profileUser as KTUser;
				if (this.ktUpdateVipLevelDelegate != null) 
				{
					this.ktUpdateVipLevelDelegate (isSuccess,user,error);
				}
			} break;
		}
	}
}
