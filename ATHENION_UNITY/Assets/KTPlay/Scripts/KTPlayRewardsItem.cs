
using System;
using System.Collections;

public class KTPlayRewardsItem
{
	/// <summary>
	///   reward name
	/// </summary>
	public string name;
	/// <summary>
	///   reward id
	/// </summary>
	public string typeId;
	/// <summary>
	///  reward value
	/// </summary>
	public double value;
}

public class KTPlayReward
{
	/// <summary>
	///   reward array
	/// </summary>
	public ArrayList items;
	/// <summary>
	///   message identifier
	/// </summary>
	public string messageId;
	/// <summary>
	///   ktplay user id
	/// </summary>
	public string gameUserId;
	/// <summary>
	///   game user id
	/// </summary>
	public string ktUserId;
}



