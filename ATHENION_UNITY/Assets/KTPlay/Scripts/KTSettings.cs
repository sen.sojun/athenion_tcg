﻿/********************************************************************************

** (C) 2016 KTplay. All Rights Reserved.


*********************************************************************************/


using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;

public  class KTSettings  {
#if UNITY_EDITOR
#elif UNITY_ANDROID
			public static string AppKey = "q6rpoCpuf";
			public static string AppSecret = "518cfef2cba12eea4a1931e15376d2d68e58f2c7";
#elif UNITY_IOS
			public static string AppKey = "q6rpoCpuf";
			public static string AppSecret = "518cfef2cba12eea4a1931e15376d2d68e58f2c7";	
#endif

    /// <summary>
    ///  KTplay Screen Orientation  
    /// </summary>
    public enum KTScreenOrientation
	{
		Auto = 0,
		Portrait,
		Landscape,
	};
	public static KTScreenOrientation ScreenOrientation = KTScreenOrientation.Auto;
}
