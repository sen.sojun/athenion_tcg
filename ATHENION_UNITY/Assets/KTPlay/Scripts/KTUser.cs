using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class KTUser
{
	/// <summary>
	///  User ID
	/// </summary>
	public string userId;
	/// <summary>
	///  User avatar url, you must append a size definition to the url when request an image.
	///  availabe size are _32x32,_50x50,_64x64,_80x80,_120x120,_128x128,_200x200,_256x256。
	/// </summary>
	public string headerUrl;
	/// <summary>
	///   User nickname
	/// </summary>
	public string nickname;
	/// <summary>
	///  Gender  0:Not set;1:male;2:female.
	/// </summary>
	public double gender;
    ///  vipLevel  >0
	/// </summary>
	public double vipLevel;
	/// <summary>
	///   City
	/// </summary>
	public string city;
	/// <summary>
	///  Leaderboard display score,only available for leaderboard user
	/// </summary>
	public string score;
	/// <summary>
	///   Leaderboard rank,only available for leaderboard user
	/// </summary>
	public double rank;
	/// <summary>
	///  Leaderboard score tag,only available for leaderboard user
	/// </summary>
	public string scoreTag;
	/// <summary>
	///   Leaderboard score value,only available for leaderboard user
	/// </summary>
	public double originScore;
	/// <summary>
	///	Preserved
	/// </summary>
	public string snsUserId;
	/// <summary>
	///	Preserved
	/// </summary>
	public string loginType;
	/// <summary>
	///   Game user ID,only available for leaderboard user
	/// </summary>
	public string gameUserId;
	/// <summary>
	///   A flag to indicate if nickname is auto-generated.
	/// </summary>
	public bool needPresentNickname;

	public bool isNicknameChanged;

	public KTUser()
	{
	}

	public void setUserId(string userId)
	{
		this.userId = userId;
	}
	public void setHeaderUrl(string headerUrl)
	{
		this.headerUrl = headerUrl;
	}
	public void setNickname(string nickname)
	{
		this.nickname = nickname;
	}
	public void setGender(int gender)
	{
		this.gender = (double)gender;
	}
    public void setVipLevel(int vipLevel)
	{
		this.vipLevel = (double)vipLevel;
	}

	public void setCity(string city)
	{
		this.city = city;
	}
	public void setScore(string score)
	{
		this.score = score;
	}
	public void setRank(long rank)
	{
		this.rank = (double)rank;
	}
	public void setSnsUserId(string snsUserId)
	{
		this.snsUserId = snsUserId;
	}
	public void setLoginType(string loginType)
	{
		this.loginType = loginType;
	}
	public void setGameUserId(string gameUserId)
	{
		this.gameUserId = gameUserId;
	}
	public void setNeedPresentNickname(bool needPresentNickname)
	{
		this.needPresentNickname = needPresentNickname;
		this.isNicknameChanged = !this.needPresentNickname;
	}
	public void setOriginScore(double originScore)
	{
		this.originScore = originScore;
	}
	public void setScoreTag(string scoreTag)
	{
		this.scoreTag = scoreTag;
	}
	public  KTUser(Hashtable param)
	{
		if((string)param["userId"]!= null) 
		{
			this.userId = (string)param["userId"];
		}
		if((string)param["headerUrl"]!=  null) 
		{
			this.headerUrl = (string)param["headerUrl"];
		}
		if((string)param["nickname"]!= null) 
		{
			this.nickname = (string)param["nickname"];
		}
		if((string)param["city"]!=  null) 
		{
			this.city = (string)param["city"];
		}
		if((string)param["score"]!= null) 
		{
			this.score = (string)param["score"];
		}
		if((string)param["snsUserId"]!=  null) 
		{
			this.snsUserId = (string)param["snsUserId"];
		}
		if((string)param["loginType"]!=  null) 
		{
			this.loginType = (string)param["loginType"];
		}
		if(param["gender"]!= null)
		{
			this.gender = (double)param["gender"];
		}
        if(param["vipLevel"]!= null)
		{
			this.vipLevel = (double)param["vipLevel"];
		}
		if(param["rank"]!= null)
		{
			this.rank = (double)param["rank"];
		}
		if(param["gameUserId"]!=null)
		{
			this.gameUserId = (string)param["gameUserId"];
		}
		if(param["needPresentNickname"]!=null)
		{
			this.needPresentNickname = (bool)param["needPresentNickname"];
		}

		this.isNicknameChanged = !this.needPresentNickname;

		if(param["scoreTag"]!=null)
		{
			this.scoreTag = (string)param["scoreTag"];
		}
		if(param["originScore"]!=null)
		{
			this.originScore = (double)param["originScore"];
		}
	}
}
