/********************************************************************************

** (C) 2016 KTplay. All Rights Reserved.


*********************************************************************************/

using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class KTPlay : MonoBehaviour
{
	public enum KTInterstialNotificationEvent
	{
		InterstitialNotificationWillAppear = 1, 
		InterstitialNotificationDidCancel,   
		InterstitialNotificationDidFinish,     
		InterstitialNotificationUnknown = 1000,     
	};


	/// <summary>
	///  KTPlayDelegate
	/// </summary>
	public delegate void KTPlayDelegate();

	/// <summary>
	///  KTPlayRewardsDelegate
	/// </summary>
	/// <param name="rewards">KTPlayRewardsItem ArrayList</param>

	public delegate void KTPlayRewardsDelegate(KTPlayReward reward);


	/// <summary>
	///  KTPlayActivityStatusDelegate
	/// </summary>
	/// <param name="hasActivityStatus">has unread messages</param>
	public delegate void KTPlayActivityStatusDelegate(bool hasActivityStatus);

	/// <summary>
	///  KTPlayAvailabilityChangedDelegate
	/// </summary>
	/// <param name="isEnabled">SDK is enabled</param>
	public delegate void KTPlayAvailabilityChangedDelegate(bool isEnabled);


	/// <summary>
	///  KTPlayDeepLinkDelegate
	/// </summary>
	/// <param name="linkScheme">linkScheme</param>

	public delegate void KTPlayDeepLinkDelegate(string linkScheme);

	/// <summary>
	///  KTPlayInterstitialNotificationDelegate
	/// </summary>
	/// <param name="interstitialID">trigger point ID</param>
	/// <param name="interstitialState">interstitialState</param>

	public delegate void KTPlayInterstitialNotificationDelegate(string interstitialID, KTPlay.KTInterstialNotificationEvent interstialNotificationEvent);

	private KTPlayDelegate ktAppearDelegate;
	private KTPlayDelegate ktDisappearDelegate;
	private KTPlayDelegate ktSoundStartDelegate;
	private KTPlayDelegate ktSoundStopDelegate;
	private KTPlayRewardsDelegate ktDispatchRewardsDelegate;
	private KTPlayActivityStatusDelegate ktActivityStatusChangedDelegate;
	private KTPlayAvailabilityChangedDelegate ktAvailabilityChangedDelegate;
	private KTPlayDeepLinkDelegate ktDeepLinkDelegate;
	private KTPlayInterstitialNotificationDelegate ktInterstitialNotificationDelegate; //mark

 	private static KTPlay instance;
	private static GameObject ktplayObj;

	private KTPlay () 
	{
	}

	public static void startWithAppKey(string appKey ,string appSecret)
	{
		KTPlay.GetInstance();
		KTPlayCommon.startWithAppKey(appKey,appSecret);
	}

	private static KTPlay GetInstance() 
	{
		if (instance == null) {
			ktplayObj = GameObject.Find("KTPlay");
			var existed = ktplayObj.GetComponent <KTPlay>();
			instance = existed ?? ktplayObj.AddComponent <KTPlay>();

            DontDestroyOnLoad(instance.gameObject);
		}	
		return instance;
	}

	
	void Awake()
	{
		#if UNITY_EDITOR
		#elif  UNITY_ANDROID
			KTPlay.startWithAppKey(KTSettings.AppKey,KTSettings.AppSecret);
		#elif UNITY_IOS
			KTPlay.startWithAppKey(KTSettings.AppKey,KTSettings.AppSecret);
		#endif
		KTPlayCommon.InitKTPlay ();
	}
	
	void Start () 
	{
		KTPlay.GetInstance().SetKtplayInternalCallback ();
	}

	void OnApplicationPause(bool pauseStatus) 
	{
		KTPlayCommon.HanldeApplicationPause (pauseStatus);
	}

	/// <summary>
	///  Setup listener for  window appeared Event
	/// </summary>
	/// <param name="delegateMethod"> callback method</param>
	public static void SetViewDidAppearDelegate(KTPlayDelegate delegateMethod) {
		KTPlay.GetInstance().SetViewDidAppearCallback (delegateMethod);
	}

	/// <summary>
	///  Setup listener for  window disappeared Event
	/// </summary>
	/// <param name="delegateMethod">callback method</param>
	public static void SetViewDidDisappearDelegate(KTPlayDelegate delegateMethod) 
	{
		KTPlay.GetInstance().SetViewDidDisappearCallback (delegateMethod);
	}

	/// <summary>
	///   Setup listener for  Start Playsound Event
	/// </summary>
	/// <param name="delegateMethod">callback method</param>
	public static void SetSoundStartDelegate(KTPlayDelegate delegateMethod) 
	{
		KTPlay.GetInstance().SetOnSoundStartCallback (delegateMethod);
	}
	
	/// <summary>
	///   Setup listener for  Stop Playsound Event
	/// </summary>
	/// <param name="delegateMethod">callback method</param>
	public static void SetSoundStopDelegate(KTPlayDelegate delegateMethod) 
	{
		KTPlay.GetInstance().SetOnSoundStopCallback (delegateMethod);
	}

	/// <summary>
	/// Setup listener for  Reward Sending Event 
	/// </summary>
	/// <param name="delegateMethod">callback method</param>
	public static void SetDidDispatchRewardsDelegate(KTPlayRewardsDelegate delegateMethod) 
	{
		KTPlay.GetInstance().SetDidDispatchRewardsCallback (delegateMethod);
	}

	/// <summary>
	/// Setup listener for Unread Messages
	/// </summary
	/// <param name="delegateMethod">callback method</param>
	public static void SetActivityStatusChangedDelegate(KTPlayActivityStatusDelegate delegateMethod) 
	{
		KTPlay.GetInstance().SetActivityStatusChangedCallback (delegateMethod);
	}

	/// <summary>
	///  Setup listener for  Enabled status changes
	/// </summary>
	/// <param name="delegateMethod">callback method</param>
	public static void SetAvailabilityChangedDelegate(KTPlayAvailabilityChangedDelegate delegateMethod) 
	{
		KTPlay.GetInstance().SetAvailabilityChangedCallback (delegateMethod);
	}

	/// <summary>
	///  Setup listener for  DeepLinks Event
	/// </summary>
	/// <param name="delegateMethod">callback method</param>
	public static void SetDeepLinkDelegate(KTPlayDeepLinkDelegate delegateMethod) {
		KTPlay.GetInstance().SetDeepLinkCallback (delegateMethod);
	}
	
	/// <summary>
	///  Show KTplay window
	/// </summary>
	public static void Show() {
		KTPlayCommon.Show();
	}

	/// <summary>
	///   Show interstital notification
	/// </summary>
	/// <param name="identifier"> Notification trigger point ID </param>
	/// <param name="delegateMethod">callback method</param>
	public static void ShowInterstitialNotification(string identifier, KTPlayInterstitialNotificationDelegate delegateMethod) 
	{
		KTPlay.GetInstance().ShowInterstitialNotificationCallback (identifier, delegateMethod);
	}

	/// <summary>
	/// Check if a specifed interstital notification is downloaded. 
	/// </summary>
	/// <param name="identifier">Notification trigger point ID</param>
	/// <return >If a specifed interstital notification is downloaded.</return>
	public static bool HasInterstitialNotification(string identifier)
	{
		return KTPlayCommon.HasInterstitialNotification (identifier);
	}

	/// <summary>
	///  Request interstital notification data
	/// </summary>
	/// <param name="identifier">Notification trigger point ID</param>
	public static void RequestInterstitialNotification(string identifier)
	{
		KTPlayCommon.RequestInterstitialNotification (identifier);
	}

	/// <summary>
	///   Close KTplay window
	/// </summary>
	public static void Dismiss()
	{
		KTPlayCommon.Dismiss ();
	}

	/// <summary>
	///  Check if SDK is enabled
	///    	SDK mayed disabled in following ：：
	///    	1、Device is not supported by KTplay SDK
	///     2、SDK features disabled in KTplay developer portal
	///    	3、Network not available
	/// </summary>
	/// <return >SDK is enabled</return>
	public static bool IsEnabled()
	{
		return KTPlayCommon.IsEnabled ();
	}

	/// <summary>
	///   Check  KTplay window is opened
	/// </summary>
	/// <return>KTplay window is opened</return>
	public static bool IsShowing()
	{
		return KTPlayCommon.IsShowing ();
	}

	/// <summary>
	///   Show quick share  window
	/// </summary>
	/// <param name="imagePath">Image file path</param>
	/// <param name="title">Topic title</param>
	/// <param name="description">Topic content</param>
	public static void ShareImageToKT(string imagePath, string title, string description)
	{
		KTPlayCommon.ShareImageToKT (imagePath, title, description);
	}
	
	/// <summary>
	///   Show quick share  window
	/// </summary>
	/// <param name="imagePath">Video file path</param>
	/// <param name="title">Topic title</param>
	/// <param name="description">Topic content</param>
	public static void ShareVideoToKT(string videoPath, string title, string description)
	{
		KTPlayCommon.ShareVideoToKT (videoPath, title, description);
	}

	/// <summary>
	///   Enable or disable notification
	/// </summary>
	/// <param name="enabled">enabled Enable or disable notification</param>
	public static void SetNotificationEnabled(bool enabled)
	{
		KTPlayCommon.SetNotificationEnabled (enabled);
	}

	//================================private method===========================
	private void SetKtplayInternalCallback()
	{
		KTPlayCommon.SetViewDidAppearCallback (this, this.OnInternalKTPlay);
		KTPlayCommon.SetViewDidDisappearCallback (this, this.OnInternalKTPlay);
		KTPlayCommon.SetOnSoundStartCallback (this, this.OnInternalKTPlay);
		KTPlayCommon.SetOnSoundStopCallback (this, this.OnInternalKTPlay);
		KTPlayCommon.SetDidDispatchRewardsCallback (this, this.OnInternalKTPlay);
		KTPlayCommon.SetActivityStatusChangedCallback (this, this.OnInternalKTPlay);
		KTPlayCommon.SetAvailabilityChangedCallback (this, this.OnInternalKTPlay);
		KTPlayCommon.SetDeepLinkCallback (this, this.OnInternalKTPlay);
	}

	private void SetViewDidAppearCallback(KTPlayDelegate delegateMethod)
	{
		if (delegateMethod != null)
		{
			this.ktAppearDelegate = delegateMethod;
		}
	}

	private void SetViewDidDisappearCallback(KTPlayDelegate delegateMethod) 
	{
		if (delegateMethod != null) 
		{
			this.ktDisappearDelegate = delegateMethod;
		}
	}

	private void SetOnSoundStartCallback(KTPlayDelegate delegateMethod) 
	{
		if (delegateMethod != null)
		{
			this.ktSoundStartDelegate = delegateMethod;
		}
	}
	
	private void SetOnSoundStopCallback(KTPlayDelegate delegateMethod) {
		
		if (delegateMethod != null) 
		{
			this.ktSoundStopDelegate = delegateMethod;
		}
	}

	private void SetDidDispatchRewardsCallback(KTPlayRewardsDelegate delegateMethod) 
	{
		if (delegateMethod != null) 
		{
			this.ktDispatchRewardsDelegate = delegateMethod;
		}
	}

	private void SetActivityStatusChangedCallback(KTPlayActivityStatusDelegate delegateMethod) 
	{
		if (delegateMethod != null) 
		{
			this.ktActivityStatusChangedDelegate = delegateMethod;
		}
	}

	private void SetAvailabilityChangedCallback(KTPlayAvailabilityChangedDelegate delegateMethod) 
	{
		if (delegateMethod != null) 
		{
			this.ktAvailabilityChangedDelegate = delegateMethod;
		}
	}

	private void SetDeepLinkCallback(KTPlayDeepLinkDelegate delegateMethod) 
	{
		if (delegateMethod != null)
		{
			this.ktDeepLinkDelegate = delegateMethod;
		}
	}

	private void ShowInterstitialNotificationCallback(string identifier, KTPlayInterstitialNotificationDelegate delegateMethod) 
	{
		if (delegateMethod != null) 
		{
			this.ktInterstitialNotificationDelegate = delegateMethod;
		}
		KTPlayCommon.ShowInterstitialNotification (this, this.OnInternalKTPlay, identifier);
	}

	/// <summary>
	///  set KTplay Community language, with the parameters format of LanguageCode-LanguageScript-RegionCode，where 
	///  the LanguageCode is mandatory field，LanguageScript and RegionCode are optional。e.g. acceptable format
	///  are zh-Hant-HK，zh-Hans，zh-CN，zh。
	///  LanguageCode ISO-639-1 2 digits language code, e.g. zh，en。
	///  LanguageScript ISO-15924 4 digits language script，e.g. Hans，Hant。
	///  RegionCode ISO-3166-1 2 digits region code, e.g. CN, US
	/// </summary>
	/// <param name="preferredLanguage">preferred Language</param>
	/// <param name="alternateLanguage">Alternative language, optional, will be used when preferred language is not supported in the device, if alternative language is also not supported, then community will use the device's system language</param>
	/// <return>if set language success</return>
	public static bool SetLanguage(string preferredLanguage, string alternateLanguage)
	{
		return KTPlayCommon.SetLanguage (preferredLanguage, alternateLanguage);
	}


	/// <summary>
	///   open Deeplink
	/// </summary>
	/// <param name="deepLinkId">Deeplink ID</param>
	public static void OpenDeepLink(string deepLinkId)
	{
		 KTPlayCommon.OpenDeepLink (deepLinkId);
	}

	private void OnInternalKTPlay(string param)
	{
		KTPlayCallbackParams ktPlayParams = new KTPlayCallbackParams(param);
		switch(ktPlayParams.KTPlayEventResult) 
		{
			case KTPlayCallbackParams.KTPlayEvent.OnAppear:
			{
				if (this.ktAppearDelegate != null) {
					this.ktAppearDelegate();
				}		
			} break;
			case KTPlayCallbackParams.KTPlayEvent.OnDisappear:
			{
				if (this.ktDisappearDelegate != null) {
					this.ktDisappearDelegate();
				}

			} break;
			case KTPlayCallbackParams.KTPlayEvent.OnActivityStatusChanged:
			{
				if (this.ktActivityStatusChangedDelegate != null) {
					this.ktActivityStatusChangedDelegate(ktPlayParams.hasNewActivity);
				}
			} break;
			case KTPlayCallbackParams.KTPlayEvent.OnDispatchRewards:
			{
				if (this.ktDispatchRewardsDelegate != null) {
					this.ktDispatchRewardsDelegate(ktPlayParams.reward);
				}
			} break;
			case KTPlayCallbackParams.KTPlayEvent.OnAvailabilityChanged:
			{
				if (this.ktAvailabilityChangedDelegate != null) {
					this.ktAvailabilityChangedDelegate(ktPlayParams.isEnabled);
				}
			} break;
			case KTPlayCallbackParams.KTPlayEvent.OnDeepLink:
			{
				if (this.ktDeepLinkDelegate != null) {
					this.ktDeepLinkDelegate(ktPlayParams.linkScheme);
				}
			} break;
			case KTPlayCallbackParams.KTPlayEvent.OnInterstitialShow:
			{
				if (this.ktInterstitialNotificationDelegate != null) {
					this.ktInterstitialNotificationDelegate(ktPlayParams.interstitialID, ktPlayParams.interstialNotificationEvent);
				}
			} break;
			case KTPlayCallbackParams.KTPlayEvent.OnSoundStart: 
			{
				if (this.ktSoundStartDelegate != null) {
					this.ktSoundStartDelegate();
				}
			} break;
			case KTPlayCallbackParams.KTPlayEvent.OnSoundStop: 
			{
				if (this.ktSoundStopDelegate != null) {
					this.ktSoundStopDelegate();
				}
			} break;
		}
	}

	
	

	
}

