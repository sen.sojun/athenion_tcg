/********************************************************************************

** (C) 2016 KTplay. All Rights Reserved.


*********************************************************************************/
using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;


public class KTError
{
	/// <summary>
    ///   error code
    /// </summary>
	public double code;

	/// <summary>
	//   error message
    /// </summary>
	public string description;

	public KTError(Hashtable param)
	{
		if(param["error_code"]!=  null)
		{
			this.code = (double)param["error_code"];
		}
		if((string)param["error_msg"]!=  null)
		{
			this.description = (string)param["error_msg"];
		}
	}
}

