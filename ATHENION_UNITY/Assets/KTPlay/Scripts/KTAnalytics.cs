
using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using System.Collections.Generic;


public class KTAnalytics : MonoBehaviour
{
	/// <summary>
	///  Sets a user property to a given value
	/// </summary>
	/// <param name="name">The name of the user property to set.The name are non-empty string objects with <= 64 characters.</param>
	/// <param name="value">The value of the user property.The value can be booleans, integers, floats , string objects with <= 128 characters.</param>
	/// <example> Example usage:
	///  <code>
	/// 		KTAnalytics.SetUserProperty ("birth_year", 1981);
	///  </code>
	/// </example>
	public static void SetUserProperty(string name, object value)
	{
		 KTAnalyticsCommon.SetUserProperty(name, value);
	}

	/// <summary>
	///  Adds an app specific event to event tracking log
	/// </summary>
	/// <param name="name">The name of the event. The name are non-empty string objects with <= 64 characters.</param>
	/// <param name="properties">An dictionary of properties. Passing nil indicates that the event has no properties.<br/>
	/// Property keys are non-empty string objects with <= 64 characters.<br/>
	/// Property values can be booleans, integers, floats , string objects and with <= 128 characters.</param>
	/// <example> Example usage:<br/>
	///  The following is an example for uploading data of a puschase event
	///  <code>
	/// 		Hashtable properties=new Hashtable(); 
	///			properties.Add ("order_id", "P4z37VjvmNjyEVvb");
	///			properties.Add ("product_id", "0001");
	///			properties.Add ("currency", "USD"); //a ISO4217 currency code
	///			properties.Add ("quantity", 2);
	///			properties.Add ("price", 2);
	///			KTAnalytics.LogEvent ("purchase", properties);
	///  </code>
	/// The following is an example of uploading data of a item consumption event
	///  <code>
	/// 		Hashtable properties=new Hashtable(); 
	///			properties.Add ("item_type", "bomb");
	///			properties.Add ("quantity", 2);
	///			KTAnalytics.LogEvent ("consume_item", properties);
	///  </code>
	/// </example>
	public static void LogEvent(string name, Hashtable properties)
	{
		KTAnalyticsCommon.LogEvent(name, properties);
	}

}

