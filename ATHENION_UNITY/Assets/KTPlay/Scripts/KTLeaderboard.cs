/********************************************************************************

** (C) 2016 KTplay. All Rights Reserved.


*********************************************************************************/
using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class KTLeaderboard : MonoBehaviour
{
	/// <summary>
	///  KTReportScoreDelegate
	/// </summary>
	/// <param name="leaderboardId">Leaderboard ID</param>
	/// <param name="error">error info</param>

	public delegate void KTReportScoreDelegate (string leaderboardId, KTError error);

	private KTReportScoreDelegate ktReportScoreDelegate;
	private static KTLeaderboard instance;
	private static GameObject ktplayObj;

	private static KTLeaderboard GetInstance() 
	{
		if (instance == null) 
		{
			ktplayObj = GameObject.Find("KTPlay");
			var existed = ktplayObj.GetComponent <KTLeaderboard>();
			instance = existed ?? ktplayObj.AddComponent <KTLeaderboard>();
		}
		return instance;
	}

	private KTLeaderboard ()
	{
	}
		

	/// <summary>
	///  Report leaderboard score
	/// </summary>
	/// <param name="score"> Score value</param>
	/// <param name="leaderboardId">Leaderboard ID</param>
	/// <param name="scoreTag"> Score tag</param>
	/// <param name="delegateMethod">Report leaderboard score callback method</param>
	public static void ReportScore (long score, string leaderboardId, string scoreTag, KTReportScoreDelegate delegateMethod)
	{
		KTLeaderboard.GetInstance().ReportScoreCallback (score, leaderboardId, scoreTag, delegateMethod);
	}

	//=======================================private method=============================

	private void ReportScoreCallback (long score, string leaderboardId, string scoreTag, KTReportScoreDelegate delegateMethod)
	{
		if (delegateMethod != null)
		{
			this.ktReportScoreDelegate = delegateMethod;
		}
		KTLeaderboardCommon.ReportScore (score, leaderboardId, scoreTag, this, this.OnLeaderboard);
	}

	private void OnLeaderboard (string param)
	{
		KTLeaderboardCallbackParams leaderParams = new  KTLeaderboardCallbackParams (param);
		switch (leaderParams.leaderboardEventResult) 
		{
			case KTLeaderboardCallbackParams.KTLeaderboardEvent.KTPlayLeaderboardEventReportScore:
			{
				string leaderboardId = leaderParams.leaderboardId;
				KTError error = (KTError)leaderParams.reportScoreLeaderboardError;
				if (this.ktReportScoreDelegate != null) {
					this.ktReportScoreDelegate (leaderboardId, error);
				}
			} break;
		}
	}
}

