
using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class KTAnalyticsCommon : MonoBehaviour
{
	public static void SetUserProperty(string name, object value)
	{
		#if UNITY_EDITOR
		#elif UNITY_ANDROID
		    KTAnalyticsAndroid.SetUserProperty(name, value);
		#elif UNITY_IOS
            #if (UNITY_4_6 || UNITY_4_7 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_4 || UNITY_5_5 || UNITY_5_6 || UNITY_5_7 || UNITY_5_8 || UNITY_5_9)
                KTAnalyticsiOS.SetUserProperty(name, value);
            #else
                if (Application.platform == RuntimePlatform.IPhonePlayer) 
                {
                    KTAnalyticsiOS.SetUserProperty(name, value);
                }
            #endif
		#endif
	}

	public static void LogEvent(string name, Hashtable properties)
	{
		#if UNITY_EDITOR
		#elif UNITY_ANDROID
		    KTAnalyticsAndroid.LogEvent(name, properties);
		#elif UNITY_IOS
            #if (UNITY_4_6 || UNITY_4_7 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_4 || UNITY_5_5 || UNITY_5_6 || UNITY_5_7 || UNITY_5_8 || UNITY_5_9)
                KTAnalyticsiOS.LogEvent(name, properties);
            #else
                if (Application.platform == RuntimePlatform.IPhonePlayer) 
                {
                    KTAnalyticsiOS.LogEvent(name, properties);
                }
            #endif
		#endif
	}
}
