
using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class KTPlayCommon : MonoBehaviour
{
	public delegate void Callback(string s);

	public static void InitKTPlay () 
	{
		#if UNITY_EDITOR
		#elif UNITY_ANDROID
			KTPlayAndroid.Init();
		#endif
	}

	public static void startWithAppKey(string appKey ,string appSecret)
	{
		#if UNITY_EDITOR
		#elif UNITY_ANDROID
		    KTPlayAndroid.startWithAppKey(appKey,appSecret);
		    Debug.Log("[KTPLAY.CS] startWithAppKey ");
		#elif UNITY_IOS
            #if (UNITY_4_6 || UNITY_4_7 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_4 || UNITY_5_5 || UNITY_5_6 || UNITY_5_7 || UNITY_5_8 || UNITY_5_9)
                KTPlayiOS.startWithAppKey(appKey,appSecret);
                Debug.Log("[KTPLAY.CS] startWithAppKey ");
            #else
                if (Application.platform == RuntimePlatform.IPhonePlayer) 
                {
                    KTPlayiOS.startWithAppKey(appKey,appSecret);
                    Debug.Log("[KTPLAY.CS] startWithAppKey ");
                }
            #endif
		#endif
	}

	public static void HanldeApplicationPause(bool pauseStatus)
	{
		#if UNITY_EDITOR
		#elif UNITY_ANDROID
			KTPlayAndroid.HanldeApplicationPause(pauseStatus);
		#endif
	}

	/// <summary>
	///  设置监听者，监听打开KTPlay主窗口事件
	/// </summary>
	/// <param name="obj">MonoBehaviour子类对象</param>
	/// <param name="callbackMethod">监听事件响应</param>
	public static void SetViewDidAppearCallback(MonoBehaviour obj, KTPlayCommon.Callback callbackMethod)
	{
		#if UNITY_EDITOR
		#elif UNITY_ANDROID
		    KTPlayAndroid.SetViewDidAppearCallback(obj, callbackMethod);
		#elif UNITY_IOS
            #if (UNITY_4_6 || UNITY_4_7 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_4 || UNITY_5_5 || UNITY_5_6 || UNITY_5_7 || UNITY_5_8 || UNITY_5_9)
                KTPlayiOS.SetViewDidAppearCallback(obj,callbackMethod);
            #else
                if (Application.platform == RuntimePlatform.IPhonePlayer) 
                {
                    KTPlayiOS.SetViewDidAppearCallback(obj,callbackMethod);
                }
            #endif
		#endif
	}
	/// <summary>
	///  设置监听者，监听关闭KTPlay主窗口事件
	/// </summary>
	/// <param name="obj">MonoBehaviour子类对象</param>
	/// <param name="callbackMethod">监听事件响应</param>
	public static void SetViewDidDisappearCallback(MonoBehaviour obj, KTPlayCommon.Callback callbackMethod)
	{
		#if UNITY_EDITOR
		#elif UNITY_ANDROID
		    KTPlayAndroid.SetViewDidDisappearCallback(obj, callbackMethod);
		#elif UNITY_IOS
            #if (UNITY_4_6 || UNITY_4_7 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_4 || UNITY_5_5 || UNITY_5_6 || UNITY_5_7 || UNITY_5_8 || UNITY_5_9)
                KTPlayiOS.SetViewDidDisappearCallback(obj,callbackMethod);
            #else
                if (Application.platform == RuntimePlatform.IPhonePlayer) 
                {
                    KTPlayiOS.SetViewDidDisappearCallback(obj,callbackMethod);
                }
            #endif
		#endif
	}
	
	public static void SetOnSoundStartCallback(MonoBehaviour obj, KTPlayCommon.Callback callbackMethod)
	{
		#if UNITY_EDITOR
		#elif UNITY_ANDROID
		    KTPlayAndroid.SetOnSoundStartCallback(obj, callbackMethod);
		#elif UNITY_IOS
            #if (UNITY_4_6 || UNITY_4_7 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_4 || UNITY_5_5 || UNITY_5_6 || UNITY_5_7 || UNITY_5_8 || UNITY_5_9)
                KTPlayiOS.SetOnSoundStartCallback(obj,callbackMethod);
            #else
                if (Application.platform == RuntimePlatform.IPhonePlayer) 
                {
                    KTPlayiOS.SetOnSoundStartCallback(obj,callbackMethod);
                }
            #endif
		#endif
	}

	public static void SetOnSoundStopCallback(MonoBehaviour obj, KTPlayCommon.Callback callbackMethod)
	{
		#if UNITY_EDITOR
		#elif UNITY_ANDROID
		    KTPlayAndroid.SetOnSoundStopCallback(obj, callbackMethod);
		#elif UNITY_IOS
            #if (UNITY_4_6 || UNITY_4_7 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_4 || UNITY_5_5 || UNITY_5_6 || UNITY_5_7 || UNITY_5_8 || UNITY_5_9)
                KTPlayiOS.SetOnSoundStopCallback(obj,callbackMethod);
            #else
                if (Application.platform == RuntimePlatform.IPhonePlayer) 
                {
                    KTPlayiOS.SetOnSoundStopCallback(obj,callbackMethod);
                }
            #endif
		#endif
	}

	/// <summary>
	///   设置监听者，监听奖励发放事件
	/// </summary>
	/// <param name="obj">MonoBehaviour子类对象</param>
	/// <param name="callbackMethod">监听事件响应</param>
	public static void SetDidDispatchRewardsCallback(MonoBehaviour obj, KTPlayCommon.Callback callbackMethod)
	{
		#if UNITY_EDITOR
		#elif UNITY_ANDROID
		    KTPlayAndroid.SetDidDispatchRewardsCallback(obj, callbackMethod);
		#elif UNITY_IOS
            #if (UNITY_4_6 || UNITY_4_7 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_4 || UNITY_5_5 || UNITY_5_6 || UNITY_5_7 || UNITY_5_8 || UNITY_5_9)
                KTPlayiOS.SetDidDispatchRewardsCallback(obj,callbackMethod);
            #else
                if (Application.platform == RuntimePlatform.IPhonePlayer) 
                {
                    KTPlayiOS.SetDidDispatchRewardsCallback(obj,callbackMethod);
                }
            #endif
		#endif
	}
	/// <summary>
	///   设置监听者，监听用户新动态
	/// </summary>
	/// <param name="obj">MonoBehaviour子类对象</param>
	/// <param name="callbackMethod">监听事件响应</param>
	public static void SetActivityStatusChangedCallback(MonoBehaviour obj, KTPlayCommon.Callback callbackMethod)
	{
		#if UNITY_EDITOR
		#elif UNITY_ANDROID
		    KTPlayAndroid.SetActivityStatusChangedCallback(obj, callbackMethod);
		#elif UNITY_IOS
            #if (UNITY_4_6 || UNITY_4_7 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_4 || UNITY_5_5 || UNITY_5_6 || UNITY_5_7 || UNITY_5_8 || UNITY_5_9)
                KTPlayiOS.SetActivityStatusChangedCallback(obj,callbackMethod);
            #else
                if (Application.platform == RuntimePlatform.IPhonePlayer) 
                {
                    KTPlayiOS.SetActivityStatusChangedCallback(obj,callbackMethod);
                }
            #endif
		#endif
	}
	/// <summary>
	///   设置监听者，监听KTPlay SDK的可用状态变更
	/// </summary>
	/// <param name="obj">MonoBehaviour子类对象</param>
	/// <param name="callbackMethod">监听事件响应</param>
	public static void SetAvailabilityChangedCallback(MonoBehaviour obj, KTPlayCommon.Callback callbackMethod)
	{
		#if UNITY_EDITOR
		#elif UNITY_ANDROID
		    KTPlayAndroid.SetAvailabilityChangedCallback(obj, callbackMethod);
		#elif UNITY_IOS
            #if (UNITY_4_6 || UNITY_4_7 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_4 || UNITY_5_5 || UNITY_5_6 || UNITY_5_7 || UNITY_5_8 || UNITY_5_9)
                KTPlayiOS.SetAvailabilityChangedCallback(obj,callbackMethod);
            #else
                if (Application.platform == RuntimePlatform.IPhonePlayer) 
                {
                    KTPlayiOS.SetAvailabilityChangedCallback(obj,callbackMethod);
                }
            #endif
		#endif
	}
	/// <summary>
	///   设置监听者，监听DeepLink事件
	/// </summary>
	/// <param name="obj">MonoBehaviour子类对象</param>
	/// <param name="callbackMethod">监听事件响应</param>
	public static void SetDeepLinkCallback(MonoBehaviour obj, KTPlayCommon.Callback callbackMethod)
	{
		#if UNITY_EDITOR
		#elif UNITY_ANDROID
		    KTPlayAndroid.SetDeepLinkCallback(obj,callbackMethod);
		#elif UNITY_IOS
            #if (UNITY_4_6 || UNITY_4_7 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_4 || UNITY_5_5 || UNITY_5_6 || UNITY_5_7 || UNITY_5_8 || UNITY_5_9)
                KTPlayiOS.SetDeepLinkCallback(obj,callbackMethod);
            #else
                if (Application.platform == RuntimePlatform.IPhonePlayer) 
                {
                    KTPlayiOS.SetDeepLinkCallback(obj,callbackMethod);
                }
            #endif
		
		#endif
	}
	/// <summary>
	///   打开KTPlay主窗口
	/// </summary>
	public static void Show()
	{
		#if UNITY_EDITOR
		#elif UNITY_ANDROID
		    KTPlayAndroid.Show();
		#elif UNITY_IOS
            #if (UNITY_4_6 || UNITY_4_7 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_4 || UNITY_5_5 || UNITY_5_6 || UNITY_5_7 || UNITY_5_8 || UNITY_5_9)
                KTPlayiOS.Show();
            #else
                if (Application.platform == RuntimePlatform.IPhonePlayer) 
                {
                    KTPlayiOS.Show();
                }
            #endif
		#endif
	}
	
	/// <summary>
	///   打开KTPlay（插屏通知窗口）
	/// </summary>
	public static void ShowInterstitialNotification(MonoBehaviour obj, KTPlayCommon.Callback callbackMethod, string identifier)
	{
		#if UNITY_EDITOR
		#elif UNITY_ANDROID
		    KTPlayAndroid.showInterstitialNotification(obj,callbackMethod,identifier);
		#elif UNITY_IOS
            #if (UNITY_4_6 || UNITY_4_7 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_4 || UNITY_5_5 || UNITY_5_6 || UNITY_5_7 || UNITY_5_8 || UNITY_5_9)
                KTPlayiOS.ShowInterstitialNoti(obj, callbackMethod, identifier);
            #else
                if (Application.platform == RuntimePlatform.IPhonePlayer) 
                {
                    KTPlayiOS.ShowInterstitialNoti(obj, callbackMethod, identifier);
                }
            #endif
		#endif
	}
	
	public static bool HasInterstitialNotification(string identifier)
	{
		#if UNITY_EDITOR
			return false;
		#elif UNITY_ANDROID
		    return KTPlayAndroid.hasInterstitialNotification(identifier);
		#elif UNITY_IOS
            #if (UNITY_4_6 || UNITY_4_7 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_4 || UNITY_5_5 || UNITY_5_6 || UNITY_5_7 || UNITY_5_8 || UNITY_5_9)
                return KTPlayiOS.HasInterstitialNotification(identifier);
            #else
                if (Application.platform == RuntimePlatform.IPhonePlayer) 
                {
                    return KTPlayiOS.HasInterstitialNotification(identifier);
                }
                else
                {
                    return false;
                }
            #endif
		#else
			return false;
		#endif
	}
	
	public static void RequestInterstitialNotification(string identifier)
	{
		#if UNITY_EDITOR
		#elif UNITY_ANDROID
		    KTPlayAndroid.requestInterstitialNotification(identifier);
		#elif UNITY_IOS
            #if (UNITY_4_6 || UNITY_4_7 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_4 || UNITY_5_5 || UNITY_5_6 || UNITY_5_7 || UNITY_5_8 || UNITY_5_9)
                KTPlayiOS.RequestInterstitialNotification(identifier);
            #else
                if (Application.platform == RuntimePlatform.IPhonePlayer) 
                {
                    KTPlayiOS.RequestInterstitialNotification(identifier);
                }
            #endif
		#endif
	}
	
	/// <summary>
	///   关闭KTPlay主窗口
	/// </summary>
	public static void Dismiss()
	{
		#if UNITY_EDITOR
		#elif UNITY_ANDROID
	        KTPlayAndroid.Dismiss();
		#elif UNITY_IOS
            #if (UNITY_4_6 || UNITY_4_7 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_4 || UNITY_5_5 || UNITY_5_6 || UNITY_5_7 || UNITY_5_8 || UNITY_5_9)
                KTPlayiOS.Dismiss();
            #else
                if (Application.platform == RuntimePlatform.IPhonePlayer) 
                {
                    KTPlayiOS.Dismiss();
                }
            #endif
		#endif
	}

	/// <summary>
	///   判断KTplay是否可用
	///    	KTPlay不可用的情况包括：
	///    	1、设备不被支持
	///     2、在Portal上关闭
	///    	3、未获取到服务器配置信息（断网）
	/// </summary>
	/// <return >KTplay是否可用</return>
	
	public static bool IsEnabled()
	{
		#if UNITY_EDITOR
			return false;
		#elif UNITY_ANDROID
		    return KTPlayAndroid.IsEnabled();
		#elif UNITY_IOS
            #if (UNITY_4_6 || UNITY_4_7 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_4 || UNITY_5_5 || UNITY_5_6 || UNITY_5_7 || UNITY_5_8 || UNITY_5_9)
                return KTPlayiOS.IsEnabled();
            #else
                if (Application.platform == RuntimePlatform.IPhonePlayer) 
                {
                    return KTPlayiOS.IsEnabled();
                }
                else
                {
                    return false;
                }
            #endif
		#else
			return false;
		#endif
	}

	/// <summary>
	///   判断KTplay主窗口是否处于打开状态
	/// </summary>
	/// <return>KTplay主窗口是否打开</return>
	public static bool IsShowing()
	{
		#if UNITY_EDITOR
			return false;
		#elif UNITY_ANDROID
		    return KTPlayAndroid.IsShowing();
		#elif UNITY_IOS
            #if (UNITY_4_6 || UNITY_4_7 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_4 || UNITY_5_5 || UNITY_5_6 || UNITY_5_7 || UNITY_5_8 || UNITY_5_9)
                return KTPlayiOS.IsShowing();
            #else
                if (Application.platform == RuntimePlatform.IPhonePlayer) 
                {
                    return KTPlayiOS.IsShowing();
                }
                else
                {
                    return false;
                }
            #endif
		#else
			return false;
		#endif
	}
	
	/// <summary>
	///   分享图片/文本到KTPlay社区
	/// </summary>
	/// <param name="imagePath">图片的绝对路径,为nil时，没有默认图片</param>
	/// <param name="description">图片的描述,为nil时，没有默认内容描述</param>
	public static void ShareImageToKT(string imagePath, string title, string description)
	{
		#if UNITY_EDITOR
		#elif UNITY_ANDROID
		    KTPlayAndroid.ShareImageToKT(imagePath, title, description);
		#elif UNITY_IOS
            #if (UNITY_4_6 || UNITY_4_7 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_4 || UNITY_5_5 || UNITY_5_6 || UNITY_5_7 || UNITY_5_8 || UNITY_5_9)
                KTPlayiOS.ShareImageToKT(imagePath, title, description);
            #else
                if (Application.platform == RuntimePlatform.IPhonePlayer) 
                {
                    KTPlayiOS.ShareImageToKT(imagePath, title, description);
                }
            #endif
		#endif
	}

	/// <summary>
	///   分享视频/文本到KTPlay社区
	/// </summary>
	/// <param name="videoPath">视频的绝对路径,为nil时，没有默认视频</param>
	/// <param name="description">视频的描述,为nil时，没有默认内容描述</param>
	public static void ShareVideoToKT(string videoPath, string title, string description)
	{
		#if UNITY_EDITOR
		#elif UNITY_ANDROID
		    KTPlayAndroid.ShareVideoToKT(videoPath, title, description);
		#elif UNITY_IOS
            #if (UNITY_4_6 || UNITY_4_7 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_4 || UNITY_5_5 || UNITY_5_6 || UNITY_5_7 || UNITY_5_8 || UNITY_5_9)
                KTPlayiOS.ShareVideoToKT(videoPath, title, description);
            #else
                if (Application.platform == RuntimePlatform.IPhonePlayer) 
                {
                    KTPlayiOS.ShareVideoToKT(videoPath, title, description);
                }
            #endif
		#endif
	}
	
	/// <summary>
	///   启用/禁用通知功能
	/// </summary>
	/// <param name="enabled">YES/NO 启用/禁用</param>
	public static void SetNotificationEnabled(bool enabled)
	{
		#if UNITY_EDITOR
		#elif UNITY_ANDROID
		    KTPlayAndroid.SetNotificationEnabled(enabled);
		#elif UNITY_IOS
            #if (UNITY_4_6 || UNITY_4_7 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_4 || UNITY_5_5 || UNITY_5_6 || UNITY_5_7 || UNITY_5_8 || UNITY_5_9)
                KTPlayiOS.SetNotificationEnabled(enabled);
            #else
                if (Application.platform == RuntimePlatform.IPhonePlayer) 
                {
                    KTPlayiOS.SetNotificationEnabled(enabled);
                }
            #endif
		#endif
	}

	public static bool SetLanguage(string preferredLanguage, string alternateLanguage)
	{
		#if UNITY_EDITOR
			return false;
		#elif UNITY_ANDROID
		    return KTPlayAndroid.SetLanguage(preferredLanguage,alternateLanguage);
		#elif UNITY_IOS
            #if (UNITY_4_6 || UNITY_4_7 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_4 || UNITY_5_5 || UNITY_5_6 || UNITY_5_7 || UNITY_5_8 || UNITY_5_9)
                return KTPlayiOS.SetLanguage(preferredLanguage,alternateLanguage);
            #else
                if (Application.platform == RuntimePlatform.IPhonePlayer) 
                {
                    return KTPlayiOS.SetLanguage(preferredLanguage,alternateLanguage);
                }
                else
                {
                    return false;
                }
            #endif
		#else
			return false;
		#endif
	}

	public static void OpenDeepLink(string deepLinkId)
	{
		#if UNITY_EDITOR
		#elif UNITY_ANDROID
		    KTPlayAndroid.OpenDeepLink(deepLinkId);
		#elif UNITY_IOS
            #if (UNITY_4_6 || UNITY_4_7 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_4 || UNITY_5_5 || UNITY_5_6 || UNITY_5_7 || UNITY_5_8 || UNITY_5_9)
                KTPlayiOS.OpenDeepLink(deepLinkId);
            #else
                if (Application.platform == RuntimePlatform.IPhonePlayer) 
                {
                    KTPlayiOS.OpenDeepLink(deepLinkId);
                }
            #endif
		#endif
	}
}
