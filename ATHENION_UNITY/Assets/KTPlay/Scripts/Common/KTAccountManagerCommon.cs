
using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class KTAccountManagerCommon : MonoBehaviour
{
    public delegate void Callback(string s);
	/// <summary>
	///  设置监听者，监听KTPlay登录状态变更
	/// </summary>
	/// <param name="obj">MonoBehaviour子类对象</param>
	/// <param name="callbackMethod">监听事件响应</param>
	public static void SetLoginStatusChange(MonoBehaviour obj,KTAccountManagerCommon.Callback callbackMethod)
	{
        #if UNITY_EDITOR
        #elif UNITY_ANDROID
		    KTAccountManagerAndroid.SetLoginStatusChangeCallback(obj, callbackMethod);
		#elif UNITY_IOS
    		#if (UNITY_4_6 || UNITY_4_7 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_4 || UNITY_5_5 || UNITY_5_6 || UNITY_5_7 || UNITY_5_8 || UNITY_5_9)
    		    KTAccountManageriOS.SetLoginStatusChangeCallback(obj,callbackMethod);
    		#else
    			if (Application.platform == RuntimePlatform.IPhonePlayer) 
                {
    				KTAccountManageriOS.SetLoginStatusChangeCallback(obj,callbackMethod);
    			}
    		#endif
		#endif
	}
	/// <summary>
	///  获取任意KTplay用户信息
	/// </summary>
	/// <param name="userId">KT用户唯一标示符</param>
	/// <param name="obj">MonoBehaviour子类对象</param>
	/// <param name="callbackMethod">监听事件响应</param>
	public static void UserProfile(string userId,MonoBehaviour obj,KTAccountManagerCommon.Callback callbackMethod)
	{
        #if UNITY_EDITOR
        #elif UNITY_ANDROID
            KTAccountManagerAndroid.UserProfile(userId, obj, callbackMethod);
        #elif UNITY_IOS
            #if (UNITY_4_6 || UNITY_4_7 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_4 || UNITY_5_5 || UNITY_5_6 || UNITY_5_7 || UNITY_5_8 || UNITY_5_9)
                    KTAccountManageriOS.UserProfile(userId,obj,callbackMethod);
            #else
                if (Application.platform == RuntimePlatform.IPhonePlayer)
                {
                    KTAccountManageriOS.UserProfile(userId, obj, callbackMethod);
                }
            #endif
        #endif
    }
    /// <summary>
    ///  打开KTPlay登录界面
    /// </summary>
    /// <param name="closeable">closeable 登录界面是否可由玩家关闭， YES 可以，NO 不可以</param>
    /// <param name="obj">MonoBehaviour子类对象</param>
    /// <param name="callbackMethod">监听事件响应</param>
    public static void ShowLoginView(bool closeable, MonoBehaviour obj,KTAccountManagerCommon.Callback callbackMethod)
	{
		#if UNITY_EDITOR
        #elif UNITY_ANDROID
		    KTAccountManagerAndroid.ShowLoginView(closeable, obj,callbackMethod);
		#elif UNITY_IOS
            #if (UNITY_4_6 || UNITY_4_7 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_4 || UNITY_5_5 || UNITY_5_6 || UNITY_5_7 || UNITY_5_8 || UNITY_5_9)
                KTAccountManageriOS.ShowLoginView(closeable, obj,callbackMethod);
            #else
                if (Application.platform == RuntimePlatform.IPhonePlayer) 
                {
                    KTAccountManageriOS.ShowLoginView(closeable, obj, callbackMethod);
                }
            #endif
        #endif
    }
    /// <summary>
    ///  使用网游帐户登录KTPlay平台
    /// </summary>
    /// <param name="gameUserId">游戏用户ID</param>
    /// <param name="obj">MonoBehaviour子类对象</param>
    /// <param name="callbackMethod">监听事件响应</param>
    public static void loginWithGameUser(string gameUserId, MonoBehaviour obj,KTAccountManagerCommon.Callback callbackMethod)
	{
        #if UNITY_EDITOR
        #elif UNITY_ANDROID
            KTAccountManagerAndroid.loginWithGameUser(gameUserId, obj,callbackMethod);
        #elif UNITY_IOS
            #if (UNITY_4_6 || UNITY_4_7 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_4 || UNITY_5_5 || UNITY_5_6 || UNITY_5_7 || UNITY_5_8 || UNITY_5_9)
                KTAccountManageriOS.loginWithGameUser(gameUserId, obj,callbackMethod);
            #else
                if (Application.platform == RuntimePlatform.IPhonePlayer) 
                {
                    KTAccountManageriOS.loginWithGameUser(gameUserId, obj, callbackMethod);
                }
            #endif
        #endif
    }

    /// <summary>
    ///  登出当前用户
    /// </summary>
    public static void Logout()
	{
        #if UNITY_EDITOR
        #elif UNITY_ANDROID
            KTAccountManagerAndroid.Logout();
        #elif UNITY_IOS
            #if (UNITY_4_6 || UNITY_4_7 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_4 || UNITY_5_5 || UNITY_5_6 || UNITY_5_7 || UNITY_5_8 || UNITY_5_9)
                KTAccountManageriOS.Logout();
            #else
                if (Application.platform == RuntimePlatform.IPhonePlayer) 
                {
                    KTAccountManageriOS.Logout();
                }
            #endif
        #endif
    }
    /// <summary>
    ///  判断是否已有用户登录
    /// </summary>
    /// <return>是否已有用户登录</return>
    public static bool IsLoggedIn()
	{
        #if UNITY_EDITOR
            return false;
        #elif UNITY_ANDROID
            return KTAccountManagerAndroid.IsLoggedIn();
        #elif UNITY_IOS
            #if (UNITY_4_6 || UNITY_4_7 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_4 || UNITY_5_5 || UNITY_5_6 || UNITY_5_7 || UNITY_5_8 || UNITY_5_9)
                return KTAccountManageriOS.IsLoggedIn();
            #else
                if (Application.platform == RuntimePlatform.IPhonePlayer)
                {
                    return KTAccountManageriOS.IsLoggedIn();
                }
                else
                {
                    return false;
                }
            #endif
        #else
            return false;
        #endif
    }
    /// <summary>
    ///  获取当前登录KTplay用户信息
    /// </summary>
    /// <return>返回KTplay用户信息，如果用户未登录返回为nil</return>
    public static KTUser CurrentAccount()
	{
        #if UNITY_EDITOR
            return null;
        #elif UNITY_ANDROID
            return KTAccountManagerAndroid.CurrentAccount();
        #elif UNITY_IOS
            #if (UNITY_4_6 || UNITY_4_7 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_4 || UNITY_5_5 || UNITY_5_6 || UNITY_5_7 || UNITY_5_8 || UNITY_5_9)
                return KTAccountManageriOS.CurrentAccount();
            #else
                if (Application.platform == RuntimePlatform.IPhonePlayer) 
                {
                    return KTAccountManageriOS.CurrentAccount();
                }
                else 
                {
                    return null;
                }
            #endif
        #else
            return null;
        #endif
    }

    public static void UpdateProfile (string nickname, string avatarPath, int gender, MonoBehaviour obj, KTAccountManagerCommon.Callback callbackMethod)
	{
        #if UNITY_EDITOR
        #elif UNITY_ANDROID
            KTAccountManagerAndroid.UpdateProfile(nickname,avatarPath,gender, obj,callbackMethod);
        #elif UNITY_IOS
            #if (UNITY_4_6 || UNITY_4_7 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_4 || UNITY_5_5 || UNITY_5_6 || UNITY_5_7 || UNITY_5_8 || UNITY_5_9)
                KTAccountManageriOS.UpdateProfile(nickname,avatarPath,gender, obj,callbackMethod);
            #else
                if (Application.platform == RuntimePlatform.IPhonePlayer)
                {
                    KTAccountManageriOS.UpdateProfile(nickname, avatarPath, gender, obj, callbackMethod);
                }
            #endif
        #endif
    }

    public static void UpdateVipLevel (int vipLevel, MonoBehaviour obj, KTAccountManagerCommon.Callback callbackMethod)
	{
		#if UNITY_EDITOR
        #elif UNITY_ANDROID
		    KTAccountManagerAndroid.UpdateVipLevel(vipLevel, obj,callbackMethod);
		#elif UNITY_IOS
            #if (UNITY_4_6 || UNITY_4_7 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_4 || UNITY_5_5 || UNITY_5_6 || UNITY_5_7 || UNITY_5_8 || UNITY_5_9)
                KTAccountManageriOS.UpdateVipLevel(vipLevel, obj,callbackMethod);
            #else
                if (Application.platform == RuntimePlatform.IPhonePlayer)
                {
                    KTAccountManageriOS.UpdateVipLevel(vipLevel, obj, callbackMethod);
                }
            #endif
        #endif
    }

    public static void setUserProfileLocks (int properties)
	{
        #if UNITY_EDITOR
        #elif UNITY_ANDROID
            KTAccountManagerAndroid.setUserProfileLocks(properties);
        #elif UNITY_IOS
            #if (UNITY_4_6 || UNITY_4_7 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_4 || UNITY_5_5 || UNITY_5_6 || UNITY_5_7 || UNITY_5_8 || UNITY_5_9)
                KTAccountManageriOS.setUserProfileLocks(properties);
            #else
                if (Application.platform == RuntimePlatform.IPhonePlayer)
                {
                    KTAccountManageriOS.setUserProfileLocks(properties);
                }
            #endif
        #endif
    }



}

