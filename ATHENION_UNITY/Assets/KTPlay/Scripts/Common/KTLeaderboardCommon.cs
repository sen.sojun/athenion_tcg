
using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class KTLeaderboardCommon : MonoBehaviour
{
	public delegate void Callback(string s);
	
	/// <summary>
	///  上传得分
	/// </summary>
	/// <param name="score">游戏得分</param>
	/// <param name="leaderboardId">排行榜id，此数据在开发者网站获取</param>
	/// <param name="scoreTag">分数相关元数据。主要方便游戏记录一些适合自己使用的信息，具体格式和内容由开发者自定义，字串总长度不超过128个字符。</param>
	/// <param name="obj">MonoBehaviour子类对象</param>
	/// <param name="callbackMethod">监听事件响应</param>
	public static void ReportScore(long score,string leaderboardId,string scoreTag ,MonoBehaviour obj,KTLeaderboardCommon.Callback callbackMethod)
	{
		#if UNITY_EDITOR
		#elif UNITY_ANDROID
		    KTLeaderboardAndroid.ReportScore(score, leaderboardId, scoreTag, obj, callbackMethod);
		#elif UNITY_IOS
            #if (UNITY_4_6 || UNITY_4_7 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_4 || UNITY_5_5 || UNITY_5_6 || UNITY_5_7 || UNITY_5_8 || UNITY_5_9)
                KTLeaderboardiOS.ReportScore(score,leaderboardId,scoreTag,obj,callbackMethod);
            #else
                if (Application.platform == RuntimePlatform.IPhonePlayer) 
                {
                    KTLeaderboardiOS.ReportScore(score,leaderboardId,scoreTag,obj,callbackMethod);
                }
            #endif
		#endif
	}
}

