using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Callbacks;
using System.IO;
using System.Xml;
using UnityEditor.KTCodeEditor;
#if UNITY_IOS
using UnityEditor.iOS.Xcode;
#endif

namespace UnityEditor.KTplayEditor
{
	public static class KTplayPostProcess
	{

		#if UNITY_EDITOR
		public static void DeleteDirectory(string dstPath)
		{
			if (Directory.Exists (dstPath))
			{
				string[] files = Directory.GetFiles (dstPath, "*", SearchOption.AllDirectories);
				foreach (var file in files){
					File.Delete(file);
				}
				Directory.Delete (dstPath, true);
			}
		}

		public static void CopyAndReplaceDirectory(string srcPath, string dstPath)
		{
			Directory.CreateDirectory(dstPath);
			foreach (var file in Directory.GetFiles(srcPath))
			{
				File.Copy(file, Path.Combine(dstPath, Path.GetFileName(file)));
			}
			foreach (var dir in Directory.GetDirectories(srcPath)) 
			{
				CopyAndReplaceDirectory(dir, Path.Combine(dstPath, Path.GetFileName(dir)));
			}
		}

		[PostProcessBuild()]

		public static void OnPostProcessBuild( BuildTarget buildTarget, string pathToBuiltProject )
		{
			#if UNITY_IOS
			#if UNITY_4_7 || UNITY_4_6
			if (buildTarget == BuildTarget.iPhone)
			#else
			if (buildTarget == BuildTarget.iOS) 
			#endif
			{
				string projPath = pathToBuiltProject + "/Unity-iPhone.xcodeproj/project.pbxproj";
				PBXProject proj = new PBXProject();
				proj.ReadFromString(File.ReadAllText(projPath));
				string target = proj.TargetGuidByName("Unity-iPhone");
				#if UNITY_4_7 || UNITY_4_6
				string assertPath = Application.dataPath ;
				const string sdkPath = "Plugins/IOS/KTPlay/";
				DeleteDirectory(pathToBuiltProject+"/"+sdkPath);
				CopyAndReplaceDirectory(assertPath+"/"+sdkPath,pathToBuiltProject+"/"+sdkPath);
				//libpath
				List<string> folders = new List<string>();
				folders.Add(sdkPath);
				folders.Add(sdkPath+"Libraries/Masonry_0.6.3/");
				folders.Add(sdkPath+"Libraries/OpenUDID/");
				folders.Add(sdkPath+"Libraries/FMDB_2.5/");
				foreach (string folder in folders) 
				{
					proj.AddBuildProperty(target, "LIBRARY_SEARCH_PATHS", "\"$(SRCROOT)/"+folder+"\"");
				}

				List<string> frameworkfolders = new List<string>();
				frameworkfolders.Add(sdkPath);
				foreach (string folder in frameworkfolders) 
				{
					proj.AddBuildProperty(target, "FRAMEWORK_SEARCH_PATHS", "$(SRCROOT)/"+folder);
				}

				//add file to project 
				List<string> files = new List<string>();
				files.Add(sdkPath+"ktplayVideo.bundle");
				files.Add(sdkPath+"libKtplayVideo.a");
				files.Add(sdkPath+"ktplay.bundle");
				files.Add(sdkPath+"KTPlay.h");
				files.Add(sdkPath+"KTAccountManager.h");
				files.Add(sdkPath+"KTUser.h");
				files.Add(sdkPath+"libKtplay_iphoneos.a");
				files.Add(sdkPath+"libKtplay_iphonesimulator.a");
				files.Add(sdkPath+"Libraries/Masonry_0.6.3/libMasonry.a");
				files.Add(sdkPath+"Libraries/SDWebImage_5.1.0/libSDWebImage.a");
				files.Add(sdkPath+"Libraries/OpenUDID/libOpenUDID.a");
				files.Add(sdkPath+"Libraries/FMDB_2.5/libFMDB.a");
				foreach (string file in files) 
				{
					string fileGuid  = proj.AddFile(file,file,PBXSourceTree.Source);
					proj.AddFileToBuild(target, fileGuid);
				}
			#endif
				proj.SetBuildProperty(target, "ENABLE_BITCODE", "NO");
				proj.SetBuildProperty(target, "GCC_ENABLE_OBJC_EXCEPTIONS", "YES");
				//support ios8+
				proj.AddBuildProperty(target, "OTHER_LDFLAGS", "-ObjC -lxml2 -weak_framework Foundation -weak_framework UIKit");
				proj.AddFrameworkToProject(target,"Security.framework",true);
				proj.AddFrameworkToProject(target,"Photos.framework",true);
				proj.AddFrameworkToProject(target,"EventKit.framework",true);
				proj.AddFrameworkToProject(target,"EventKitUI.framework",true);
				proj.AddFrameworkToProject(target,"OpenGLES.framework",true);
				proj.AddFrameworkToProject(target,"UIKit.framework",true);
				proj.AddFrameworkToProject(target,"SystemConfiguration.framework",true);
				proj.AddFrameworkToProject(target,"QuartzCore.framework",true);
				proj.AddFrameworkToProject(target,"Foundation.framework",true);
				proj.AddFrameworkToProject(target,"CoreGraphics.framework",true);
				proj.AddFrameworkToProject(target,"CFNetwork.framework",true);
				proj.AddFrameworkToProject(target,"AssetsLibrary.framework",true);
				proj.AddFrameworkToProject(target,"CoreText.framework",true);
				proj.AddFrameworkToProject(target,"AdSupport.framework",true);
				proj.AddFrameworkToProject(target,"AVFoundation.framework",true);
				proj.AddFrameworkToProject(target,"AudioToolbox.framework",true);
				proj.AddFrameworkToProject(target,"CoreTelephony.framework",true);
				proj.AddFrameworkToProject(target,"CoreMedia.framework",true);
				proj.AddFrameworkToProject(target,"CoreMotion.framework",true);
				proj.AddFrameworkToProject(target,"MobileCoreServices.framework",true);
				proj.AddFrameworkToProject(target,"MediaPlayer.framework",true);
				proj.AddFrameworkToProject(target,"MessageUI.framework",true);
				proj.AddFrameworkToProject(target,"MapKit.framework",true);
				proj.AddFrameworkToProject(target,"MediaPlayer.framework",true);
				proj.AddFrameworkToProject(target,"MapKit.framework",true);
				proj.AddFrameworkToProject(target,"StoreKit.framework",true);
				proj.AddFrameworkToProject(target,"Social.framework",true);
				proj.AddFrameworkToProject(target,"CoreTelephony.framework",true);
				proj.AddFrameworkToProject(target,"WebKit.framework",true);
				proj.AddFrameworkToProject(target,"ImageIO.framework",true);
				proj.AddFileToBuild (target, proj.AddFile ("usr/lib/libsqlite3.tbd", "Frameworks/libsqlite3.tbd", PBXSourceTree.Sdk));
				proj.AddFileToBuild (target, proj.AddFile ("usr/lib/libz.tbd", "Frameworks/libz.tbd", PBXSourceTree.Sdk));
				proj.AddFileToBuild (target, proj.AddFile ("usr/lib/libiconv.tbd", "Frameworks/libiconv.tbd", PBXSourceTree.Sdk));
				proj.AddFileToBuild (target, proj.AddFile ("usr/lib/libresolv.tbd", "Frameworks/libresolv.tbd", PBXSourceTree.Sdk));
				proj.AddFileToBuild (target, proj.AddFile ("usr/lib/libicucore.tbd", "Frameworks/libicucore.tbd", PBXSourceTree.Sdk));
				proj.AddFileToBuild (target, proj.AddFile ("usr/lib/libc++.tbd", "Frameworks/libc++.tbd", PBXSourceTree.Sdk));
				File.WriteAllText(projPath, proj.WriteToString());
				UpdateIOSPlist (pathToBuiltProject);
				UpdateIOSCode (pathToBuiltProject);
			}
			#endif

			#if UNITY_ANDROID
				if (buildTarget == BuildTarget.Android)
				{
					KTValidateManifest ();
				}
			#endif
		}
		#endif

		private static void UpdateIOSPlist(string path)
		{
			#if UNITY_IOS
				string plistPath = Path.Combine(path, "Info.plist");   
				PlistDocument plist = new PlistDocument();
				plist.ReadFromString (File.ReadAllText (plistPath));
				//Get Root
				PlistElementDict rootDict = plist.root;
				//PlistElementDict transportSecurity = rootDict.CreateDict("NSAppTransportSecurity");
				//transportSecurity.SetBoolean("NSAllowsArbitraryLoads",true);
				if (KTSettings.ScreenOrientation == KTSettings.KTScreenOrientation.Auto) {

				} else if (KTSettings.ScreenOrientation == KTSettings.KTScreenOrientation.Portrait) {
					PlistElementDict ktplay_config = (PlistElementDict)rootDict["ktplay_config"];
					if (ktplay_config == null) {
						ktplay_config = rootDict.CreateDict ("ktplay_config");
					}
					ktplay_config.SetString ("lock_orientation", "portrait");
				} else if (KTSettings.ScreenOrientation == KTSettings.KTScreenOrientation.Landscape) {
					PlistElementDict ktplay_config = (PlistElementDict)rootDict["ktplay_config"];
					if (ktplay_config == null) {
						ktplay_config = rootDict.CreateDict ("ktplay_config");
					}
					ktplay_config.SetString ("lock_orientation", "landscape");
				}

				PlistElementString privacy = (PlistElementString)rootDict["NSPhotoLibraryUsageDescription"];
				if (privacy == null) {	
					rootDict.SetString ("NSPhotoLibraryUsageDescription","Allow access your photos so you can share them with other players");
				}
				
				PlistElementString privacy1 = (PlistElementString)rootDict["NSPhotoLibraryAddUsageDescription"];
				if (privacy1 == null) {	
						rootDict.SetString ("NSPhotoLibraryAddUsageDescription","Allow access your photos so you can save them");
				}
				File.WriteAllText (plistPath, plist.WriteToString ());
			#endif
		}

		private static void UpdateIOSCode(string filePath)
		{
		}
		
		public static void KTGenerateManifest()
		{
			var outputFile = Path.Combine(Application.dataPath, "Plugins/Android/AndroidManifest.xml");
			if (!File.Exists(outputFile))
			{
				var inputFile = Path.Combine(EditorApplication.applicationContentsPath, "PlaybackEngines/androidplayer/AndroidManifest.xml");
				if (!File.Exists (inputFile)) 
				{
					inputFile = Path.Combine(EditorApplication.applicationContentsPath, "PlaybackEngines/AndroidPlayer/Apk/AndroidManifest.xml");
				}
				if (!File.Exists (inputFile)) 
				{
					string s = EditorApplication.applicationPath;
					int index = s.LastIndexOf ("/");
					s = s.Substring (0, index + 1);
					inputFile = Path.Combine (s, "PlaybackEngines/AndroidPlayer/Apk/AndroidManifest.xml");
				}
				File.Copy(inputFile, outputFile);
			}
			KTValidateManifest();
		}
		
		public static bool KTValidateManifest()
		{
			var outputFile = Path.Combine(Application.dataPath, "Plugins/Android/AndroidManifest.xml");
			if (!File.Exists(outputFile))
			{
				KTGenerateManifest();
				return true;
			}
			
			XmlDocument doc = new XmlDocument();
			doc.Load(outputFile);
			
			if (doc == null)
			{
				Debug.LogError("Couldn't load " + outputFile);
				return false;
			}
			
			XmlNode manNode = FindChildNode(doc, "manifest");
			string ns = manNode.GetNamespaceOfPrefix("android");
			XmlNode app = FindChildNode(manNode, "application");
			if (app == null)
			{
				Debug.LogError("Error parsing " + outputFile + ",tag for application not found.");
				return false;
			}
			//Enable hardware acceleration for video play
			XmlElement elem = (XmlElement)app;
			elem.SetAttribute ("hardwareAccelerated", ns,"true");
			elem.SetAttribute ("supportsRtl", ns,"true");
			doc.Save (outputFile);
			return true;
		}
		
		public static XmlNode FindChildNode(XmlNode parent, string name)
		{
			XmlNode curr = parent.FirstChild;
			while (curr != null)
			{
				if (curr.Name.Equals(name))
				{
					return curr;
				}
				curr = curr.NextSibling;
			}
			return null;
		}

		public static XmlNode FindChildNodeWithAttribute(XmlNode parent, string name, string attribute, string value)
		{
			XmlNode curr = parent.FirstChild;
			while (curr != null)
			{
				if (curr.Name.Equals(name) && curr.Attributes[attribute].Value.Equals(value))
				{
					return curr;
				}
				curr = curr.NextSibling;
			}
			return null;
		}
		
	}
}
