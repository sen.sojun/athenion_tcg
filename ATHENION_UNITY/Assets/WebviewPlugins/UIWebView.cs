﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class UIWebView : MonoBehaviour
{
    #region Properties
        
    [Header("Setting")]
    [SerializeField] private string _url;
    [SerializeField] private Image _webViewRenderer;

    private WebViewObject _webViewObject;
    #endregion

    // For testing.
    private void Awake()
    {
        LoadUrl(string.Empty);
    }

    /// <summary>
    /// Call this method to load url.
    /// </summary>
    /// <param name="url">Url website</param>
    public void LoadUrl(string url = "")
    {
        // If it's not a pre-setting from inspector.
        if(!string.IsNullOrEmpty(url))
            _url = url;

        if (_webViewObject != null)
        {
            _webViewObject.SetVisibility(true);
            StartCoroutine(LoadUrlRoutine());
            return;
        }

        _webViewObject = (new GameObject("WebViewObject")).AddComponent<WebViewObject>();
        _webViewObject.Init(
            ld: delegate(string msg)
            {
#if UNITY_EDITOR_OSX || !UNITY_ANDROID
                // NOTE: depending on the situation, you might prefer
                // the 'iframe' approach.
                // cf. https://github.com/gree/unity-webview/issues/189
#if true
                _webViewObject.EvaluateJS(@"
                  if (window && window.webkit && window.webkit.messageHandlers && window.webkit.messageHandlers.unityControl) {
                    window.Unity = {
                      call: function(msg) {
                        window.webkit.messageHandlers.unityControl.postMessage(msg);
                      }
                    }
                  } else {
                    window.Unity = {
                      call: function(msg) {
                        window.location = 'unity:' + msg;
                      }
                    }
                  }
                ");
#else
                _webViewObject.EvaluateJS(@"
                  if (window && window.webkit && window.webkit.messageHandlers && window.webkit.messageHandlers.unityControl) {
                    window.Unity = {
                      call: function(msg) {
                        window.webkit.messageHandlers.unityControl.postMessage(msg);
                      }
                    }
                  } else {
                    window.Unity = {
                      call: function(msg) {
                        var iframe = document.createElement('IFRAME');
                        iframe.setAttribute('src', 'unity:' + msg);
                        document.documentElement.appendChild(iframe);
                        iframe.parentNode.removeChild(iframe);
                        iframe = null;
                      }
                    }
                  }
                ");
#endif
#endif
                _webViewObject.EvaluateJS(@"Unity.call('ua=' + navigator.userAgent)");
            }
            , enableWKWebView: true
        );

        _webViewObject.SetMargins(
            (int) Mathf.Abs(_webViewRenderer.rectTransform.offsetMin.x)
            , (int) Mathf.Abs(_webViewRenderer.rectTransform.offsetMax.y)
            , (int)Mathf.Abs(_webViewRenderer.rectTransform.offsetMax.x)
            , (int) Mathf.Abs(_webViewRenderer.rectTransform.offsetMin.y));
        _webViewObject.SetVisibility(true);

        StartCoroutine(LoadUrlRoutine());
    }

    /// <summary>
    /// Call this method to load UI.
    /// </summary>
    /// <returns></returns>
    private IEnumerator LoadUrlRoutine()
    {
#if !UNITY_WEBPLAYER
        if (_url.StartsWith("http"))
        {
            _webViewObject.LoadURL(_url.Replace(" ", "%20"));
        }
        else
        {
            string[] exts = new string[]{
                ".jpg",
                ".js",
                ".html"  // should be last
            };
            foreach (var ext in exts)
            {
                string url = _url.Replace(".html", ext);
                string src = System.IO.Path.Combine(Application.streamingAssetsPath, url);
                string dst = System.IO.Path.Combine(Application.persistentDataPath, url);
                byte[] result;
                if (src.Contains("://"))
                {  // for Android
                    //var www = new WWW(src);
                    UnityWebRequest myWr = UnityWebRequest.Get(src);
                    //yield return www;
                    yield return myWr.SendWebRequest();
                    //result = www.bytes;
                    result = myWr.downloadHandler.data;
                }
                else
                {
                    result = System.IO.File.ReadAllBytes(src);
                }
                System.IO.File.WriteAllBytes(dst, result);
                if (ext == ".html")
                {
                    _webViewObject.LoadURL("file://" + dst.Replace(" ", "%20"));
                    break;
                }
            }
        }
#else
        if (_url.StartsWith("http")) {
            _webViewObject.LoadURL(Url.Replace(" ", "%20"));
        } else {
            _webViewObject.LoadURL("StreamingAssets/" + Url.Replace(" ", "%20"));
        }
        _webViewObject.EvaluateJS(
            "parent.$(function() {" +
            "   window.Unity = {" +
            "       call:function(msg) {" +
            "           parent.unityWebView.sendMessage('WebViewObject', msg)" +
            "       }" +
            "   };" +
            "});");
#endif
        yield break;
    }

    public void GoBack()
    {
        _webViewObject.GoBack();
    }

    public void GoForward()
    {
        _webViewObject.GoForward();
    }

    public void SetWebViewActiveState(bool state)
    {
        _webViewObject.SetVisibility(state);
    }

    public void DestroyWebView()
    {
        Destroy(_webViewObject.gameObject);
    }
}
