﻿Shader "Custom/UnlitFadeWithShadow" {
	Properties {
		//_Color ("Main Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
	}
	SubShader {
		Tags { 
		"RenderType"="Transparent"
		"Queue"="Transparent"
		}

		ZWrite On
		Lighting Off

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Lambert addshadow alpha:fade

		sampler2D _MainTex;
		//fixed4 _Color;

		struct Input {
			float2 uv_MainTex;
		};

		void surf (Input IN, inout SurfaceOutput o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
			//o.Albedo = c.rgb;
			// Metallic and smoothness come from slider variables
			o.Alpha = c.a;
			o.Emission = c.rgb;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
