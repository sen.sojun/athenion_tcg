﻿ 
Shader "Custom/DistortionOriginal" {
    Properties {
        _MainTex ("Base (RGB)", 2D) = "white" {}
		_Color ("Color", Color) = (1,1,1,1)
		//_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
		_Speed1 ("Speed1",float) = 25
		_Speed2 ("Speed2",float) = 5
		_Speed21 ("Speed21",float) = 5
		_Speed3 ("Speed3",float) = 0.01


    }
    SubShader {
        Tags { "RenderType"="Transparent" 
		"Queue"="Transparent"
		}
     
		ZWrite On

        CGPROGRAM
        #pragma surface surf Lambert vertex:vert alpha:fade
 
        sampler2D _MainTex;
        float4 _MainTex_ST;
		float4 _Color;
		float _Speed1;
		float _Speed2;
		float _Speed21;
		float _Speed3;
 
        struct Input {
            float2 st_MainTex;
        };
 
        void vert (inout appdata_full v, out Input o)
        {
            UNITY_INITIALIZE_OUTPUT(Input,o);
 
            o.st_MainTex = TRANSFORM_TEX(v.texcoord, _MainTex);
 
            // add distortion
            // this is the part you need to modify, i  recomment to expose such
            // hard-coded values to the inspector for easier tweaking.
            //o.st_MainTex.x += sin((o.st_MainTex.x+o.st_MainTex.y)*_Speed1 + _Time.g*_Speed2)*_Speed3;
            //o.st_MainTex.y += cos((o.st_MainTex.x-o.st_MainTex.y)*_Speed1 + _Time.g*_Speed21)*_Speed3;

			o.st_MainTex.x += sin((o.st_MainTex.x+o.st_MainTex.y)*_Speed1 + _Time.g*_Speed2)*_Speed3;
            o.st_MainTex.y += cos((o.st_MainTex.x-o.st_MainTex.y)*_Speed1 + _Time.g*_Speed21)*_Speed3;
        }
 
        void surf (Input IN, inout SurfaceOutput o) {
            fixed4 c = tex2D (_MainTex, IN.st_MainTex) * _Color;
            o.Albedo = c.rgb;
            o.Alpha = c.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
 