//////////////////////////////////////////////////////////////
/// Shadero Sprite: Sprite Shader Editor - by VETASOFT 2018 //
/// Shader generate with Shadero 1.9.0                      //
/// http://u3d.as/V7t #AssetStore                           //
/// http://www.shadero.com #Docs                            //
//////////////////////////////////////////////////////////////

Shader "Shadero Customs/Art_IM0002"
{
Properties
{
[PerRendererData] _MainTex("Sprite Texture", 2D) = "white" {}
ZoomUV_Zoom_1("ZoomUV_Zoom_1", Range(0.2, 4)) = 0.957
ZoomUV_PosX_1("ZoomUV_PosX_1", Range(-3, 3)) = 0.5
ZoomUV_PosY_1("ZoomUV_PosY_1", Range(-3, 3)) =0.5
AnimatedRotationUV_AnimatedRotationUV_Rotation_1("AnimatedRotationUV_AnimatedRotationUV_Rotation_1", Range(-360, 360)) = 0
AnimatedRotationUV_AnimatedRotationUV_PosX_1("AnimatedRotationUV_AnimatedRotationUV_PosX_1", Range(-1, 2)) = 0.5
AnimatedRotationUV_AnimatedRotationUV_PosY_1("AnimatedRotationUV_AnimatedRotationUV_PosY_1", Range(-1, 2)) = 0.5
AnimatedRotationUV_AnimatedRotationUV_Intensity_1("AnimatedRotationUV_AnimatedRotationUV_Intensity_1", Range(0, 4)) = 0.265
AnimatedRotationUV_AnimatedRotationUV_Speed_1("AnimatedRotationUV_AnimatedRotationUV_Speed_1", Range(-10, 10)) = 0.322
AnimatedPingPongOffsetUV_1_OffsetX_1("AnimatedPingPongOffsetUV_1_OffsetX_1", Range(-1, 1)) = -0.021
AnimatedPingPongOffsetUV_1_OffsetY_1("AnimatedPingPongOffsetUV_1_OffsetY_1", Range(-1, 1)) = 0.129
AnimatedPingPongOffsetUV_1_ZoomX_1("AnimatedPingPongOffsetUV_1_ZoomX_1", Range(1, 10)) = 1
AnimatedPingPongOffsetUV_1_ZoomY_1("AnimatedPingPongOffsetUV_1_ZoomY_1", Range(1, 10)) = 1
AnimatedPingPongOffsetUV_1_Speed_1("AnimatedPingPongOffsetUV_1_Speed_1", Range(-1, 1)) = 0.079
_NewTex_1("NewTex_1(RGB)", 2D) = "white" { }
_NewTex_2("NewTex_2(RGB)", 2D) = "white" { }
_Add_Fade_1("_Add_Fade_1", Range(0, 4)) = 1
_SpriteFade("SpriteFade", Range(0, 1)) = 1.0

// required for UI.Mask
[HideInInspector]_StencilComp("Stencil Comparison", Float) = 8
[HideInInspector]_Stencil("Stencil ID", Float) = 0
[HideInInspector]_StencilOp("Stencil Operation", Float) = 0
[HideInInspector]_StencilWriteMask("Stencil Write Mask", Float) = 255
[HideInInspector]_StencilReadMask("Stencil Read Mask", Float) = 255
[HideInInspector]_ColorMask("Color Mask", Float) = 15

}

SubShader
{

Tags {"Queue" = "Transparent" "IgnoreProjector" = "true" "RenderType" = "Transparent" "PreviewType"="Plane" "CanUseSpriteAtlas"="True" }
ZWrite Off Blend SrcAlpha OneMinusSrcAlpha Cull Off

// required for UI.Mask
Stencil
{
Ref [_Stencil]
Comp [_StencilComp]
Pass [_StencilOp]
ReadMask [_StencilReadMask]
WriteMask [_StencilWriteMask]
}

Pass
{

CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#pragma fragmentoption ARB_precision_hint_fastest
#include "UnityCG.cginc"

struct appdata_t{
float4 vertex   : POSITION;
float4 color    : COLOR;
float2 texcoord : TEXCOORD0;
};

struct v2f
{
float2 texcoord  : TEXCOORD0;
float4 vertex   : SV_POSITION;
float4 color    : COLOR;
};

sampler2D _MainTex;
float _SpriteFade;
float ZoomUV_Zoom_1;
float ZoomUV_PosX_1;
float ZoomUV_PosY_1;
float AnimatedRotationUV_AnimatedRotationUV_Rotation_1;
float AnimatedRotationUV_AnimatedRotationUV_PosX_1;
float AnimatedRotationUV_AnimatedRotationUV_PosY_1;
float AnimatedRotationUV_AnimatedRotationUV_Intensity_1;
float AnimatedRotationUV_AnimatedRotationUV_Speed_1;
float AnimatedPingPongOffsetUV_1_OffsetX_1;
float AnimatedPingPongOffsetUV_1_OffsetY_1;
float AnimatedPingPongOffsetUV_1_ZoomX_1;
float AnimatedPingPongOffsetUV_1_ZoomY_1;
float AnimatedPingPongOffsetUV_1_Speed_1;
sampler2D _NewTex_1;
sampler2D _NewTex_2;
float _Add_Fade_1;

v2f vert(appdata_t IN)
{
v2f OUT;
OUT.vertex = UnityObjectToClipPos(IN.vertex);
OUT.texcoord = IN.texcoord;
OUT.color = IN.color;
return OUT;
}


float2 AnimatedPingPongOffsetUV(float2 uv, float offsetx, float offsety, float zoomx, float zoomy, float speed)
{
float time = sin(_Time * 100* speed)  * 0.1;
speed *= time * 25;
uv += float2(offsetx, offsety)*speed;
uv = uv * float2(zoomx, zoomy);
return uv;
}
float2 DistortionUV(float2 p, float WaveX, float WaveY, float DistanceX, float DistanceY, float Speed)
{
Speed *=_Time*100;
p.x= p.x+sin(p.y*WaveX + Speed)*DistanceX*0.05;
p.y= p.y+cos(p.x*WaveY + Speed)*DistanceY*0.05;
return p;
}
float2 ZoomUV(float2 uv, float zoom, float posx, float posy)
{
float2 center = float2(posx, posy);
uv -= center;
uv = uv * zoom;
uv += center;
return uv;
}
float4 OperationBlend(float4 origin, float4 overlay, float blend)
{
float4 o = origin; 
o.a = overlay.a + origin.a * (1 - overlay.a);
o.rgb = (overlay.rgb * overlay.a + origin.rgb * origin.a * (1 - overlay.a)) / (o.a+0.0000001);
o.a = saturate(o.a);
o = lerp(origin, o, blend);
return o;
}
float2 AnimatedRotationUV(float2 uv, float rot, float posx, float posy, float radius, float speed)
{
uv = uv - float2(posx, posy);
float angle = rot * 0.01744444;
angle += sin(_Time * speed * 20) * radius;
float sinX = sin(angle);
float cosX = cos(angle);
float2x2 rotationMatrix = float2x2(cosX, -sinX, sinX, cosX);
uv = mul(uv, rotationMatrix) + float2(posx, posy);
return uv;
}
float4 frag (v2f i) : COLOR
{
float4 _MainTex_1 = tex2D(_MainTex, i.texcoord);
float2 ZoomUV_1 = ZoomUV(i.texcoord,ZoomUV_Zoom_1,ZoomUV_PosX_1,ZoomUV_PosY_1);
float2 AnimatedRotationUV_1 = AnimatedRotationUV(ZoomUV_1,AnimatedRotationUV_AnimatedRotationUV_Rotation_1,AnimatedRotationUV_AnimatedRotationUV_PosX_1,AnimatedRotationUV_AnimatedRotationUV_PosY_1,AnimatedRotationUV_AnimatedRotationUV_Intensity_1,AnimatedRotationUV_AnimatedRotationUV_Speed_1);
float2 AnimatedPingPongOffsetUV_1 = AnimatedPingPongOffsetUV(AnimatedRotationUV_1,AnimatedPingPongOffsetUV_1_OffsetX_1,AnimatedPingPongOffsetUV_1_OffsetY_1,AnimatedPingPongOffsetUV_1_ZoomX_1,AnimatedPingPongOffsetUV_1_ZoomY_1,AnimatedPingPongOffsetUV_1_Speed_1);
float2 DistortionUV_1 = DistortionUV(AnimatedPingPongOffsetUV_1,0,12.344,0.365,0.053,0.386);
float4 NewTex_1 = tex2D(_NewTex_1,DistortionUV_1);
float4 OperationBlend_1 = OperationBlend(_MainTex_1, NewTex_1, 1); 
float2 DistortionUV_2 = DistortionUV(i.texcoord,22.343,30.572,0.332,0.418,0.978);
float4 NewTex_2 = tex2D(_NewTex_2,DistortionUV_2);
OperationBlend_1 = lerp(OperationBlend_1,OperationBlend_1*OperationBlend_1.a + NewTex_2*NewTex_2.a,_Add_Fade_1);
float4 FinalResult = OperationBlend_1;
FinalResult.rgb *= i.color.rgb;
FinalResult.a = FinalResult.a * _SpriteFade * i.color.a;
return FinalResult;
}

ENDCG
}
}
Fallback "Sprites/Default"
}
