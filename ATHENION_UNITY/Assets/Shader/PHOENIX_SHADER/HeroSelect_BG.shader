//////////////////////////////////////////////////////////////
/// Shadero Sprite: Sprite Shader Editor - by VETASOFT 2018 //
/// Shader generate with Shadero 1.9.0                      //
/// http://u3d.as/V7t #AssetStore                           //
/// http://www.shadero.com #Docs                            //
//////////////////////////////////////////////////////////////

Shader "Shadero Customs/HeroSelect_BG"
{
Properties
{
[PerRendererData] _MainTex("Sprite Texture", 2D) = "white" {}
_NewTex_3("NewTex_3(RGB)", 2D) = "white" { }
_NewTex_5("NewTex_5(RGB)", 2D) = "white" { }
_NewTex_4("NewTex_4(RGB)", 2D) = "white" { }
_Add_Fade_3("_Add_Fade_3", Range(0, 4)) = 1
_NewTex_2("NewTex_2(RGB)", 2D) = "white" { }
_MaskChannel_Fade_1("_MaskChannel_Fade_1", Range(0, 1)) = 0
_NewTex_1("NewTex_1(RGB)", 2D) = "white" { }
_SpriteFade("SpriteFade", Range(0, 1)) = 1.0

// required for UI.Mask
[HideInInspector]_StencilComp("Stencil Comparison", Float) = 8
[HideInInspector]_Stencil("Stencil ID", Float) = 0
[HideInInspector]_StencilOp("Stencil Operation", Float) = 0
[HideInInspector]_StencilWriteMask("Stencil Write Mask", Float) = 255
[HideInInspector]_StencilReadMask("Stencil Read Mask", Float) = 255
[HideInInspector]_ColorMask("Color Mask", Float) = 15

}

SubShader
{

Tags {"Queue" = "Transparent" "IgnoreProjector" = "true" "RenderType" = "Transparent" "PreviewType"="Plane" "CanUseSpriteAtlas"="True" }
ZWrite Off Blend SrcAlpha OneMinusSrcAlpha Cull Off

// required for UI.Mask
Stencil
{
Ref [_Stencil]
Comp [_StencilComp]
Pass [_StencilOp]
ReadMask [_StencilReadMask]
WriteMask [_StencilWriteMask]
}

Pass
{

CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#pragma fragmentoption ARB_precision_hint_fastest
#include "UnityCG.cginc"

struct appdata_t{
float4 vertex   : POSITION;
float4 color    : COLOR;
float2 texcoord : TEXCOORD0;
};

struct v2f
{
float2 texcoord  : TEXCOORD0;
float4 vertex   : SV_POSITION;
float4 color    : COLOR;
};

sampler2D _MainTex;
float _SpriteFade;
sampler2D _NewTex_3;
sampler2D _NewTex_5;
sampler2D _NewTex_4;
float _Add_Fade_3;
sampler2D _NewTex_2;
float _MaskChannel_Fade_1;
sampler2D _NewTex_1;

v2f vert(appdata_t IN)
{
v2f OUT;
OUT.vertex = UnityObjectToClipPos(IN.vertex);
OUT.texcoord = IN.texcoord;
OUT.color = IN.color;
return OUT;
}


float2 DistortionUV(float2 p, float WaveX, float WaveY, float DistanceX, float DistanceY, float Speed)
{
Speed *=_Time*100;
p.x= p.x+sin(p.y*WaveX + Speed)*DistanceX*0.05;
p.y= p.y+cos(p.x*WaveY + Speed)*DistanceY*0.05;
return p;
}
float2 RotationUV(float2 uv, float rot, float posx, float posy, float speed)
{
rot=rot+(_Time*speed*360);
uv = uv - float2(posx, posy);
float angle = rot * 0.01744444;
float sinX = sin(angle);
float cosX = cos(angle);
float2x2 rotationMatrix = float2x2(cosX, -sinX, sinX, cosX);
uv = mul(uv, rotationMatrix) + float2(posx, posy);
return uv;
}
float2 AnimatedTwistUV(float2 uv, float value, float posx, float posy, float radius, float speed)
{
float2 center = float2(posx, posy);
float2 tc = uv - center;
float dist = length(tc);
if (dist < radius)
{
float percent = (radius - dist) / radius;
float theta = percent * percent * 16.0 * sin(value);
float s = sin(theta + _Time.y * speed);
float c = cos(theta + _Time.y * speed);
tc = float2(dot(tc, float2(c, -s)), dot(tc, float2(s, c)));
}
tc += center;
return tc;
}
float2 ZoomUV(float2 uv, float zoom, float posx, float posy)
{
float2 center = float2(posx, posy);
uv -= center;
uv = uv * zoom;
uv += center;
return uv;
}
float2 ResizeUV(float2 uv, float offsetx, float offsety, float zoomx, float zoomy)
{
uv += float2(offsetx, offsety);
uv = fmod(uv * float2(zoomx*zoomx, zoomy*zoomy), 1);
return uv;
}

float2 ResizeUVClamp(float2 uv, float offsetx, float offsety, float zoomx, float zoomy)
{
uv += float2(offsetx, offsety);
uv = fmod(clamp(uv * float2(zoomx*zoomx, zoomy*zoomy), 0.0001, 0.9999), 1);
return uv;
}
float4 frag (v2f i) : COLOR
{
float2 AnimatedTwistUV_1 = AnimatedTwistUV(i.texcoord,0.101,0.5,0.5,1,0.1);
float4 NewTex_3 = tex2D(_NewTex_3,AnimatedTwistUV_1);
float2 ZoomUV_2 = ZoomUV(i.texcoord,0.773,0.5,0.5);
float2 ResizeUV_2 = ResizeUVClamp(ZoomUV_2,0,-0.18,1,1.243);
float2 RotationUV_2 = RotationUV(ResizeUV_2,90,0.5,0.5,0.315);
float4 NewTex_5 = tex2D(_NewTex_5,RotationUV_2);
float2 ZoomUV_1 = ZoomUV(i.texcoord,1,0.5,0.5);
float2 ResizeUV_1 = ResizeUVClamp(ZoomUV_1,0,-0.18,1,1.243);
float2 RotationUV_1 = RotationUV(ResizeUV_1,90,0.5,0.5,-0.486);
float4 NewTex_4 = tex2D(_NewTex_4,RotationUV_1);
NewTex_5 = lerp(NewTex_5,NewTex_5*NewTex_5.a + NewTex_4*NewTex_4.a,_Add_Fade_3);
NewTex_3 = lerp(NewTex_3,NewTex_3*NewTex_3.a + NewTex_5*NewTex_5.a,0.429);
float2 DistortionUV_1 = DistortionUV(i.texcoord,19.2,11.2,0.083,0.088,0.45);
float4 NewTex_2 = tex2D(_NewTex_2,DistortionUV_1);
float4 RGBASplit_1 = NewTex_2;
NewTex_3.a = lerp(RGBASplit_1.a, 1 - RGBASplit_1.a ,_MaskChannel_Fade_1);
float4 NewTex_1 = tex2D(_NewTex_1,DistortionUV_1);
NewTex_3 = lerp(NewTex_3,NewTex_3*NewTex_3.a + NewTex_1*NewTex_1.a,0.978 * NewTex_1.a);
float4 FinalResult = NewTex_3;
FinalResult.rgb *= i.color.rgb;
FinalResult.a = FinalResult.a * _SpriteFade * i.color.a;
return FinalResult;
}

ENDCG
}
}
Fallback "Sprites/Default"
}
