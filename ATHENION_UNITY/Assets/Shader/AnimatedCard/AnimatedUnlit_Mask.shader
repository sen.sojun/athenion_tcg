﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/AnimatedUnlit_Mask"
{
	Properties
	{
		[PerRendererData]_MainTex ("Texture", 2D) = "white" {}
		[NoScaleOffset]_ArtTex ("Artwork", 2D) = "white" {}
		[NoScaleOffset]_MaskTex ("Mask", 2D) = "white" {}
		_Color1 ("Color 1", Color) = (1,1,1,1)
		_Color2 ("Color 2", Color) = (1,1,1,1)
		_Color3 ("Color 3", Color) = (1,1,1,1)

		[Header(Effect 1)]
		[NoScaleOffset]_EffectTex1 ("Effect Texture 1", 2D) = "white" {}
		[PowerSlider(3.0)] _Speed1 ("Speed 1", Range (0, 10)) = 0.0
		[PowerSlider(3.0)] _Offset1_X ("Offset 1 X", Range (-1, 1)) = 0.0
		[PowerSlider(3.0)] _Offset1_Y ("Offset 1 Y", Range (-1, 1)) = 0.0
		[PowerSlider(3.0)] _Scale1 ("Scale 1", Range (0, 10)) = 1.0

		[Header(Effect 2)]
		[NoScaleOffset]_EffectTex2 ("Effect Texture 2", 2D) = "white" {}
		[PowerSlider(3.0)] _Speed2 ("Speed 2", Range (0, 10)) = 0.0
		[PowerSlider(3.0)] _Offset2_X ("Offset 2 X", Range (-1, 1)) = 0.0
		[PowerSlider(3.0)] _Offset2_Y ("Offset 2 Y", Range (-1, 1)) = 0.0
		[PowerSlider(3.0)] _Scale2 ("Scale 2", Range (0, 10)) = 1.0

		[Header(Effect 3)]
		[NoScaleOffset]_EffectTex3 ("Distort 3", 2D) = "white" {}
		[PowerSlider(3.0)] _Speed3 ("Speed3", Range (0, 10)) = 0.0
		[PowerSlider(3.0)] _Wave_X ("Wave X", Range (-100, 100)) = 0.0
		[PowerSlider(3.0)] _Wave_Y ("Wave Y", Range (-100, 100)) = 0.0
		[PowerSlider(3.0)] _Distance_X ("Distance X", Range (-2, 2)) = 0.0
		[PowerSlider(3.0)] _Distance_Y ("Distance Y", Range (-2, 2)) = 0.0
	}
	SubShader
	{
		Tags {"Queue" = "Transparent" "IgnoreProjector" = "true" "RenderType" = "Transparent" "CanUseSpriteAtlas"="False" }
		ZWrite Off Blend SrcAlpha OneMinusSrcAlpha Cull Off
		LOD 100

		// required for UI.Mask
		Stencil
		{
			Ref [_Stencil]
			Comp [_StencilComp]
			Pass [_StencilOp]
			ReadMask [_StencilReadMask]
			WriteMask [_StencilWriteMask]
		}

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			// Vertex Shader Input
			struct appdata
			{
				float4 vertex : POSITION;
				float4 color    : COLOR;
				float2 uv : TEXCOORD0;

			};

			// Vertex to fragment output
			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				float4 color    : COLOR;
			};

			sampler2D _MainTex;
			sampler2D _ArtTex;
			sampler2D _MaskTex;
			sampler2D _EffectTex1, _EffectTex2, _EffectTex3;
			fixed4 _Color1, _Color2, _Color3;
			float _Offset1_X, _Offset1_Y;
			float _Offset2_X, _Offset2_Y;
			float _Wave_X, _Wave_Y, _Distance_X, _Distance_Y;
			float _Speed1, _Speed2, _Speed3;
			float _Scale1, _Scale2;

			// Vertex Shader
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				o.color = v.color;				
				return o;
			}

			float2 DistortionUV(float2 p, float WaveX, float WaveY, float DistanceX, float DistanceY, float Speed)
			{
				Speed *= _Time * 100;
				p.x = p.x + sin(p.y * WaveX + Speed) * DistanceX * 0.05;
				p.y = p.y + sin(p.x * WaveY + Speed) * DistanceY * 0.05;
				return p;
			}

			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 main = tex2D(_MainTex, i.uv);
				fixed4 art = tex2D(_ArtTex, i.uv);
				fixed4 mask = tex2D(_MaskTex, i.uv);
				
				fixed4 effect1 = tex2D(_EffectTex1, (i.uv + (float2(_Offset1_X, _Offset1_Y) * _Speed1 * _Time)) * _Scale1);
				fixed4 effect2 = tex2D(_EffectTex2, (i.uv + (float2(_Offset2_X, _Offset2_Y) * _Speed2 * _Time)) * _Scale2);
				fixed4 effect3 = tex2D(_ArtTex, DistortionUV(i.uv, _Wave_X, _Wave_Y, _Distance_X, _Distance_Y, _Speed3));
			
				fixed4 final;
				final = lerp(art, effect3, mask.b);

				// Masking Effect
				if (mask.r > 0) 
				{	
					final += _Color1 * mask.r;
					final += effect1 * mask.r;
				}

				if (mask.g > 0)
				{
					final += _Color2 * mask.g;
					final += effect2 * mask.g;
				}

				if (mask.b > 0) 
				{
					//result -= mask.b;
					final += effect3 * mask.b;
				}

				// Masking Frame Card
				final.a = lerp(main.a * final.a, (1 - main.a) * art.a,0);
				fixed4 FinalResult = final;
				FinalResult.rgb *= i.color.rgb;
				FinalResult.a = FinalResult.a * 1 * i.color.a;
						
				return FinalResult;
			}
			ENDCG
		}
	}
	Fallback "Sprites/Default"
}
