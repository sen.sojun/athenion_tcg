﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SceneAnimationEvent_TutorialIntro : SceneAnimationEvent
{
    protected override SceneAnimation.AnimationName _animationName { get { return SceneAnimation.AnimationName.Tutorial_Intro; } }

    #region Events
    public void ShowBotDeck()
    {
        UIManager.Instance.ShowDeck(true, GameManager.Instance.GetLocalPlayerIndex());
    }

    public void ShowTopDeck()
    {
        UIManager.Instance.ShowDeck(true, GameManager.GetOpponentPlayerIndex(GameManager.Instance.GetLocalPlayerIndex()));
    }
    #endregion



}
