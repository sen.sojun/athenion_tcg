﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Spine.Unity;
using UnityEngine.Events;

public class SceneAnimationEvent_TutorialPackOpen : SceneAnimationEvent
{
    //IPointerClickHandler
    protected override SceneAnimation.AnimationName _animationName => SceneAnimation.AnimationName.Tutorial_PackOpen;

    [Header("Spine")]
    [SerializeField] private SkeletonAnimation Anim;

    private Spine.AnimationState _animationState;
    private bool _isClicked = false;

    //Anim
    private string _anim_normal = "tut_openpack";

    private void Awake()
    {
        _animationState = Anim.AnimationState;
    }

    #region Animation
    public override void PlayAnimation(UnityAction onComplete)
    {
        gameObject.SetActive(true);
        if (onComplete != null)
        {
            _onComplete = onComplete;
        }

        PlayAnimationOpen();

    }

    private void PlayAnimationOpen()
    {
        SoundManager.PlaySFX("SFX_TUT_PackOpen");
        _animationState.SetAnimation(0, _anim_normal, false);
        _animationState.Complete += delegate
        {
            //UIManager.Instance.CameraScreenShake(5,1.5f);
            if (_onComplete != null)
            {
                _onComplete.Invoke();
            }
        };

    }
    #endregion


    #region Events
    //public void OnPointerClick(PointerEventData eventData)
    //{
    //    if (!_isClicked)
    //    {
    //        _isClicked = true;
    //        PlayAnimationOpen();
    //    }

    //}
    #endregion

}
