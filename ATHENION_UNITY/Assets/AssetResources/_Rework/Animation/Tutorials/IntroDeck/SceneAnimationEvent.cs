﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class SceneAnimationEvent : MonoBehaviour
{
    protected abstract SceneAnimation.AnimationName _animationName { get; }
    protected UnityAction _onComplete;

    public SceneAnimation.AnimationName GetAnimationName()
    {
        return _animationName;
    }

    public virtual void PlayAnimation(UnityAction onComplete)
    {
        gameObject.SetActive(true);
        if (onComplete != null)
        {
            _onComplete = onComplete;
        }

    }

    public void OnComplete()
    {
        if (_onComplete != null)
        {
            _onComplete.Invoke();
        }
    }
}
