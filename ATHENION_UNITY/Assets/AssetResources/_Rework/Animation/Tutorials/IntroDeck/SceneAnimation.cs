﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SceneAnimation : MonoBehaviour
{
    #region Enum
    public enum AnimationName
    {
        Tutorial_PackOpen,
        Tutorial_Intro,

        Intro
    }
    #endregion

    #region Animation List
    [SerializeField] private List<SceneAnimationEvent> AnimationEventList;
    #endregion

    #region Methods
    public void Play(AnimationName name, UnityAction onComplete)
    {
        SceneAnimationEvent anim = AnimationEventList.Find(x => x.GetAnimationName() == name);
        Debug.Log(anim);
        if (anim != null)
        {
            anim.PlayAnimation(() =>
            {
                onComplete?.Invoke();
                anim.gameObject.SetActive(false);
            });
        }
    }
    #endregion
}
