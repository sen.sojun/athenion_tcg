﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;
using UnityEngine.Events;


public class TutorialPopup : MonoBehaviour
{
    #region Inspector
    [SerializeField] private TutorialPlayPopup _Popup_PlayTutorial;
    [SerializeField] private TutorialMapAnimation _MapAnimation;

    [Header("Stage Cell")]
    [SerializeField] private StageCell _TutorialStage_1;
    [SerializeField] private StageCell _TutorialStage_2;
    [SerializeField] private StageCell _TutorialStage_3;
    [SerializeField] private StageCell _TutorialStage_4;

    [Header("BTN")]
    [SerializeField] private Button _BTN_Close;
    [SerializeField] private Button _BTN_Skip;

    [Header("Reward")]
    [SerializeField] private GameObject _RewardIcon;
    [SerializeField] private GameObject _RewardIconTutorial2;

    [Header("Fade")]
    [SerializeField] private Image _BlackFade;

    [Header("Hide When First Play")]
    [SerializeField] private GameObject _Toolbar;
    [SerializeField] private GameObject _Header;
    #endregion

    #region Public Properties
    public static event Action OnInit;
    public static event Action OnQuit;
    #endregion

    #region Private Properties
    [SerializeField] private NoticePopup_SureCheck _SkipConfirmPopup;
    private UnityAction<GameMode> _onPlayTutorial;

    private bool _tut1Complete;
    private bool _tut2Complete;
    private bool _tut3Complete;
    private bool _tut4Complete;

    private int _currentStage = 0;
    private bool _isStartPlay = false;
    #endregion

    #region Methods

    public void ShowUI()
    {
        TopToolbar.AddSetting(false, false, null);
        FadeOut();
        _isStartPlay = false;
        // Show Roll
        OnInit?.Invoke();
        GameHelper.UITransition_FadeIn(this.gameObject);

        InitData();
        SetupUI();
        InitBTN();
        StartCoroutine(UpdateStageAnimation());
    }

    public IEnumerator ShowFirstTimeAnimation()
    {
        _MapAnimation.ShowRollIn();
        InitData();

        yield return new WaitForSeconds(1.75f);
        yield return ShowTutorialMapIcon();

        yield return new WaitForSeconds(1.75f);
        _MapAnimation.SelectStage(1);
        FadeIn();
        yield return new WaitForSeconds(0.5f);
    }

    private IEnumerator UpdateStageAnimation()
    {
        yield return ShowTutorialMapIcon();
        yield return ShowPopupRewardConfirm();
        SetEvent();

        UpdateCloseBTN();
    }

    private IEnumerator ShowTutorialMapIcon()
    {
        _TutorialStage_1.ResetIcon();
        _TutorialStage_2.ResetIcon();
        _TutorialStage_3.ResetIcon();
        _TutorialStage_4.ResetIcon();

        //[Obsolete]
        _RewardIcon.SetActive(false);
        _RewardIconTutorial2.SetActive(false);

        bool[] redeemRewardStatus = DataManager.Instance.GetPlayerRedeemTutorialStatus();

        _TutorialStage_1.InitStage(true, _tut1Complete);
        if (redeemRewardStatus[0] == false)
        {
            _TutorialStage_1.SetActiveRewardIcon(true);
        }
        yield return new WaitForSeconds(0.5f);

        _TutorialStage_2.InitStage(_tut1Complete, _tut2Complete);
        if (redeemRewardStatus[1] == false)
        {
            _TutorialStage_2.SetActiveRewardIcon(true);
        }
        yield return new WaitForSeconds(0.5f);

        _TutorialStage_3.InitStage(_tut2Complete, _tut3Complete);
        if (redeemRewardStatus[2] == false)
        {
            _TutorialStage_3.SetActiveRewardIcon(true);
        }
        yield return new WaitForSeconds(0.5f);

        _TutorialStage_4.InitStage(_tut3Complete, _tut4Complete);
        if (redeemRewardStatus[3] == false)
        {
            _TutorialStage_4.SetActiveRewardIcon(true);
        }
        yield return new WaitForSeconds(0.5f);
    }

    private IEnumerator ShowPopupRewardConfirm()
    {
        if (IsTutorialMode(DataManager.Instance.LastGameMode) == false || DataManager.Instance.IsWinLastMatch == false)
            yield break;

        GameMode tutorialMode = DataManager.Instance.LastGameMode;

        bool[] redeemTutorialStatus = DataManager.Instance.GetPlayerRedeemTutorialStatus();
        int tutorialIndex = GetTutorialIndex(tutorialMode);

        bool isFinish = false;
        if (DataManager.Instance.IsCompletedTutorial(tutorialMode) == true && redeemTutorialStatus[tutorialIndex] == false)
        {
            List<ItemData> rewardList = DataManager.Instance.GetTutorialReward(tutorialIndex);
            if (tutorialMode == GameMode.Tutorial_2)
            {
                string title = LocalizationManager.Instance.GetText("TUTORIAL_REWARD_TITLE");
                string message = LocalizationManager.Instance.GetText($"TUTORIAL_{tutorialIndex}_GOT_REWARD");
                PopupUIManager.Instance.ShowPopup_GotLegendPack(title, message, () =>
                {
                    DataManager.Instance.InventoryData.GrantItemByItemList(rewardList);
                    DataManager.Instance.SetPlayerRedeemTutorialStatusToLocal(tutorialIndex);
                    RefreshRewardBox();
                    isFinish = true;
                });
            }
            else
            {
                string title = LocalizationManager.Instance.GetText("TUTORIAL_REWARD_TITLE");
                string message = LocalizationManager.Instance.GetText($"TUTORIAL_{tutorialIndex}_GOT_REWARD");
                PopupUIManager.Instance.ShowPopup_GotRewardAnimated(title, message, rewardList
                , delegate ()
                {
                    DataManager.Instance.InventoryData.GrantItemByItemList(rewardList);
                    DataManager.Instance.SetPlayerRedeemTutorialStatusToLocal(tutorialIndex);
                    RefreshRewardBox();
                    isFinish = true;
                });
            }
        }
        else
        {
            isFinish = true;
        }
        yield return new WaitUntil(() => isFinish == true);
    }

    public void RefreshRewardBox()
    {
        bool[] redeemRewardStatus = DataManager.Instance.GetPlayerRedeemTutorialStatus();

        _TutorialStage_1.SetActiveRewardIcon(redeemRewardStatus[0] == false);
        _TutorialStage_2.SetActiveRewardIcon(redeemRewardStatus[1] == false);
        _TutorialStage_3.SetActiveRewardIcon(redeemRewardStatus[2] == false);
        _TutorialStage_4.SetActiveRewardIcon(redeemRewardStatus[3] == false);
    }

    private int GetTutorialIndex(GameMode mode)
    {
        //Tutorial_1
        string modeStr = mode.ToString().Replace("Tutorial_", "");
        int modeInt = Convert.ToInt32(modeStr);
        return modeInt - 1;
    }

    private bool IsTutorialMode(GameMode mode)
    {
        return mode.ToString().Contains("Tutorial");
    }

    public void HideUI()
    {
        TopToolbar.RemoveLatestSetting();
        OnQuit?.Invoke();
        GameHelper.UITransition_FadeOut(this.gameObject);
    }

    public void SetOnSelectTutorial(UnityAction<GameMode> onClickPlay)
    {
        _onPlayTutorial = onClickPlay;
    }

    public void SetOnSkipEvent(UnityAction onClick)
    {
        _BTN_Skip.onClick.RemoveAllListeners();
        _BTN_Skip.onClick.AddListener(() =>
        {
            string title = LocalizationManager.Instance.GetText("TEXT_TUTORIAL");
            string message = LocalizationManager.Instance.GetText("TEXT_SKIP_TUTORIAL_CONFIRMATION");
            _SkipConfirmPopup.SetData(new PopupUIData(title, message, PopupUITypes.SureCheck));
            _SkipConfirmPopup.SetBTN(onClick, null);
            _SkipConfirmPopup.Show(null);
        });
    }

    public void SetOnCloseEvent(UnityAction onClick)
    {
        _BTN_Close.onClick.RemoveAllListeners();
        _BTN_Close.onClick.AddListener(onClick);
    }

    private void InitData()
    {
        _tut1Complete = DataManager.Instance.IsCompletedTutorial(GameMode.Tutorial_1);
        _tut2Complete = DataManager.Instance.IsCompletedTutorial(GameMode.Tutorial_2);
        _tut3Complete = DataManager.Instance.IsCompletedTutorial(GameMode.Tutorial_3);
        _tut4Complete = DataManager.Instance.IsCompletedTutorial(GameMode.Tutorial_4);
    }

    private void SetupUI()
    {
        _MapAnimation.ShowStandby();

        _Toolbar.SetActive(DataManager.Instance.IsSkipTutorial() || _tut4Complete);
        _Header.SetActive(DataManager.Instance.IsSkipTutorial() || _tut4Complete);

        _BTN_Close.gameObject.SetActive(false);
        _BTN_Skip.gameObject.SetActive(false);
    }

    private void UpdateCloseBTN()
    {

        if (IsCanClosePanel())
        {
            if (IsCanShowSkipButton())
            {
                _BTN_Skip.gameObject.SetActive(true);
            }
            else
            {
                _BTN_Close.gameObject.SetActive(true);
            }
        }
    }

    private bool IsCanShowSkipButton()
    {
        return DataManager.Instance.IsSkipTutorial() == false && _tut4Complete == false && DataManager.Instance.GetIsClaimTutorialReward() == false;
    }

    private bool IsCanClosePanel()
    {
        return _tut2Complete;
    }

    private void InitBTN()
    {
        if (_BTN_Close.onClick == null)
        {
            _BTN_Close.onClick.RemoveAllListeners();
            _BTN_Close.onClick.AddListener(HideUI);
        }

        if (_BTN_Skip.onClick == null)
        {
            _BTN_Skip.onClick.RemoveAllListeners();
            _BTN_Skip.onClick.AddListener(HideUI);
        }
    }

    private bool IsCanShowReward()
    {
        if (!DataManager.Instance.GetIsClaimTutorialReward())
        {
            return true;
        }
        else
        {
            return false;
        }

    }

    private void SetEvent()
    {
        _TutorialStage_1.SetEvent(delegate
        {
            SelectStage(1);
            Popup_ShowPlay(1);
        });
        _TutorialStage_2.SetEvent(delegate
        {
            SelectStage(2);
            Popup_ShowPlay(2);
        });
        _TutorialStage_3.SetEvent(delegate
        {
            SelectStage(3);
            Popup_ShowPlay(3);
        });
        _TutorialStage_4.SetEvent(delegate
        {
            SelectStage(4);
            Popup_ShowPlay(4);
        });
        //_RewardIcon.SetActive(IsCanShowReward());
        //_RewardIconTutorial2.SetActive(DataManager.Instance.GetPlayerCanRedeemTutorial2Reward());
    }

    public void Popup_ShowPlay(int stage)
    {
        _currentStage = stage;
        _Popup_PlayTutorial.ShowStage(_currentStage, delegate
         {
             _Popup_PlayTutorial.HideUI();
             if (!_isStartPlay)
             {
                 GameMode mode = GameMode.Tutorial_1;
                 switch (_currentStage)
                 {
                     case 1: mode = GameMode.Tutorial_1; break;
                     case 2: mode = GameMode.Tutorial_2; break;
                     case 3: mode = GameMode.Tutorial_3; break;
                     case 4: mode = GameMode.Tutorial_4; break;
                 }

                 _isStartPlay = true;
                 FadeIn();
                 _MapAnimation.SelectStage(_currentStage);
                 _onPlayTutorial?.Invoke(mode);
             }
         });
    }

    public void SelectStage(int stage)
    {
        _currentStage = stage;

        _TutorialStage_1.SelectStage(_currentStage == 1);
        _TutorialStage_2.SelectStage(_currentStage == 2);
        _TutorialStage_3.SelectStage(_currentStage == 3);
        _TutorialStage_4.SelectStage(_currentStage == 4);
    }
    #endregion

    #region Fade
    public void FadeIn()
    {
        GameHelper.UITransition_FadeIn(_BlackFade.gameObject);
    }

    public void FadeOut()
    {
        GameHelper.UITransition_FadeOut(_BlackFade.gameObject);
    }
    #endregion
}
