﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.Events;

public class StageCell : MonoBehaviour
{
    #region Inspector
    [Header("BTN")]
    [SerializeField] private Button BTN_Select;

    [Header("ICON")]
    [SerializeField] private GameObject _ICON_Base;
    [SerializeField] private GameObject _ICON_Lock;
    [SerializeField] private GameObject _ICON_Complete;
    [SerializeField] private GameObject _Glow_Selected;
    [SerializeField] private GameObject _RewardBox;
    #endregion

    #region Private Properties
    private bool _isUnlock;
    private bool _isComplete;
    private bool _isSelected;
    #endregion

    #region Methods
    public void InitStage(bool isUnlock, bool isComplete)
    {
        _isUnlock = isUnlock;
        _isComplete = isComplete;

        UpdateStage();

    }

    public void SetActiveRewardIcon(bool isActive)
    {
        _RewardBox.gameObject.SetActive(isActive);
    }

    public void SetEvent(UnityAction onClick = null)
    {
        BTN_Select.interactable = _isUnlock;

        BTN_Select.onClick.RemoveAllListeners();
        BTN_Select.onClick.AddListener(delegate
        {
            onClick?.Invoke();
        });
    }

    public void SelectStage(bool isSelected)
    {
        _isSelected = isSelected;
        _Glow_Selected.SetActive(_isSelected);
    }

    public void ResetIcon()
    {
        // Reset
        _ICON_Base.SetActive(false);
        _ICON_Lock.SetActive(false);
        _ICON_Complete.SetActive(false);
        SetActiveRewardIcon(false);
        BTN_Select.onClick.RemoveAllListeners();
    }

    private void UpdateStage()
    {
        GameHelper.UITransition_FadeMoveIn(this.gameObject, this.gameObject, 0.0f, 1.0f);
        SoundManager.PlaySFX("SFX_TUT_BOSS_SPAWN");

        _ICON_Base.SetActive(_isUnlock && !_isComplete);
        _ICON_Lock.SetActive(!_isUnlock && !_isComplete);
        _ICON_Complete.SetActive(_isUnlock && _isComplete);
    }
    #endregion
}
