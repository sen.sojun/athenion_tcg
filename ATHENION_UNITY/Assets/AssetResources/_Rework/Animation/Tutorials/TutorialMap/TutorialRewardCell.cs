﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialRewardCell : MonoBehaviour
{
    public int TutorialIndex;

    public void OnClick()
    {
        string title = LocalizationManager.Instance.GetText("TUTORIAL_REWARD_TITLE");
        PopupUIManager.Instance.ShowPopup_PreviewReward(title, "", DataManager.Instance.GetTutorialReward(TutorialIndex));
    }
}
