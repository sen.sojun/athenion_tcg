﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;
using UnityEngine.Events;
using TMPro;

public class TutorialPlayPopup : MonoBehaviour
{
    #region Inspector
    [Header("BTN")]
    [SerializeField] private Button _BTN_Close;
    [SerializeField] private Button _BTN_Okay;

    [Header("Sprite Stage")]
    [SerializeField] private TextMeshProUGUI TEXT_Title;
    [SerializeField] private TextMeshProUGUI TEXT_Description;
    [SerializeField] private Image _ICON_Stage;
    [SerializeField] private List<Sprite> _SpriteStageList = new List<Sprite>();
    #endregion

    #region Private Properties
    private int _currentStage = 0;
    #endregion

    #region Methods
    public void ShowStage(int stage,UnityAction onClick)
    {
        _currentStage = stage;
        ShowUI();
        UpdateSpriteStage();
        UpdateText();

        _BTN_Close.onClick.RemoveAllListeners();
        _BTN_Close.onClick.AddListener(HideUI);

        _BTN_Okay.onClick.RemoveAllListeners();
        _BTN_Okay.onClick.AddListener(onClick);
    }

    public void ShowUI()
    {
        GameHelper.UITransition_FadeIn(this.gameObject);
    }

    public void HideUI()
    {
        GameHelper.UITransition_FadeOut(this.gameObject);
    }

    private void UpdateSpriteStage()
    {
        _ICON_Stage.sprite = _SpriteStageList[_currentStage-1];
    }

    private void UpdateText()
    {
        TEXT_Title.text = LocalizationManager.Instance.GetText("TU_TITLE_" + _currentStage.ToString());
        TEXT_Description.text = LocalizationManager.Instance.GetText("TU_DESCRIPTION_" + _currentStage.ToString());
    }
    #endregion
}
