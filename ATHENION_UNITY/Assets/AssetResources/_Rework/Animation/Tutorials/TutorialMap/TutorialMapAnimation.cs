﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialMapAnimation : MonoBehaviour
{
    #region Inspector
    [SerializeField] private Animator _AnimatorController;
    #endregion

    #region Private
    private const string _standby = "Standby";
    private const string _rollin = "RollIn";
    private const string _stage1 = "Stage1";
    private const string _stage2 = "Stage2";
    private const string _stage3 = "Stage3";
    private const string _stage4 = "Stage4";
    #endregion

    #region Methods
    public void SelectStage(int stage)
    {
        string stateName = _standby;

        if (stage == 0) stateName = _standby;
        else if (stage == 1) stateName = _stage1;
        else if (stage == 2) stateName = _stage2;
        else if (stage == 3) stateName = _stage3;
        else if (stage == 4) stateName = _stage4;

        _AnimatorController.CrossFade(stateName, 0.5f);

    }

    public void ShowRollIn()
    {
        _AnimatorController.CrossFade(_rollin, 0.5f);
        SoundManager.PlaySFX("TUT_SFX_SCROLL");
    }

    public void ShowStandby()
    {
        _AnimatorController.CrossFade(_standby, 0.5f);
    }
    #endregion
}
