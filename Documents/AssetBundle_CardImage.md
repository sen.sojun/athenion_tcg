﻿


# Card Image

รูปการ์ดจะประกอบไปด้วย 3 ไฟล์
1. Full Art จะต้องตั้งชื่อ ART_CARD_C0000
2. Token Art จะต้องตั้งชื่อ ART_TOKEN_C0000
3. Banner Art จะต้องตั้งชื่อ ART_BANNER_C0000


# Card Frame Image

รูปขอบการ์ดของแต่ละ Faction
มีทั้งหมด 2 ประเภท
 - Banner Size
 - Full Size

Banner Size 
ปัจจุบันมี Card ทั้งหมด 5 Rarity
1. Banner_Common
2. Banner_Rare
3. Banner_Epic
4. Banner_Legendary
5. Banner_Star Chamber

Full Size 
จะต้องมีไฟล์ 2 แบบคู่กันเสมอ คือ Frame และ Mask
ปัจจุบันมีทั้งหมด 5 Rarity และ 7 Faction (รวม Mercenary)
โดยจะต้องตั้งชื่อดังนี้

1. Card_Common_Air
2. Card_Common_Air_Mask
> Card_rarity_faction
> Card_rarity_faction_Mask


 


