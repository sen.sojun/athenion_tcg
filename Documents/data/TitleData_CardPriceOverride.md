﻿# Card Price Override
 - Source : **Title Data**
 - Key : **card_price_override_db**
 - Detail : ใช้เป็น reference data สำหรับการเปลี่ยนแปลงราคาการ craft/recycle/transformของการ์ด
 - Example Value : 
```json
{
  "20200123_card_stat_change": {
    "start_date": "2020-01-23T02:00:00.000Z",
    "end_date": "2020-02-06T02:00:00.000Z",
    "limits": {
      "min": 0,
      "max": -1
    },
    "card_data": [
      {
        "price_base": "card_stat_change",
        "list": [
          "C0053",
          "C0053:FOIL",
          "C0102",
          "C0102:FOIL",
          "C0494",
          "C0494:FOIL",
          "C0499",
          "C0499:FOIL"
        ]
      }
    ]
  }
}
```
- `Key` : unique key ของการ override ราคา
	- `start_date` : วันเวลาเริ่มต้นการใช้ราคา
	- `end_date` : วันเวลาสิ้นสุดการใช้ราคา
	- `limits` : ปริมาณที่จำกัดสำหรับผู้เล่นแต่ละคน
		- `min` : ปริมาณต่ำสุดที่เป็นไปได้
		- `max` : ปริมาณสูงสุดที่เป็นไปได้ ถ้าเป็น -1 หมายถึงการกำหนดปริมาณสูงสุดเป็นปริมาณที่ผู้เล่นมีอยู่ตอนได้รับการ override ราคา
	- `card_data` : ข้อมูลรายการของการ์ดที่ถูกปรับราคา
		- `price_base` : ราคา base *จะต้องเป็นราคาที่กำหนดอยู่ในข้อมูลของราคาการ์ด
		- `price_ovrr` : ราคาสำหรับการ override ราคา base ซึ่งจะกำหนด Value โครงสร้างเดียวกับราคาที่กำหนดในข้อมูลราคาของการ์ด โดยจะแบ่งตามประเภทของราคา ได้แก่
			- `craft` 
			- `recycle`
			- `transform`
		- `list` : รายการของการ์ดที่ได้รับผลของการ override *ID ของการ์ดจะต้องเป็น FullID
---
