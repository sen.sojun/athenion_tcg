﻿# Player Stat
 - Source : **PlayerData (Title) -> ReadOnlyData**
 - Key : **(prefix) player_stat_**
 - Detail : ใช้เก็บสถิติการเล่นและการทำกิจกรรมต่างๆ ของผู้เล่น
 - Example Value : 
```json
{
  "P_ANY": 44,
  "C_USE": 524,
  "TURN": 278,
  "W_ANY": 26,
  "P_PVE": 1,
  "W_PVE": 1,
  "P_TUT_1": 1,
  "W_TUT_1": 1
}
```
- `Key` : Player Stat Event Key (Player Stat key)
- `Value` : จำนวนครั้งที่ Player Stat นั้นถูกนับ

> ข้อมูล player stat จะถูกจัดเก็บตามข้อกำหนดที่ตั้งไว้ใน [PlayerStatKeyData](TitleData_PlayerStatKeyData.md)
---
