﻿# Achievement DB
 - Source : **Title Data**
 - Key : **achievement_db**
 - Example Value : 
```json
{
  "ach_a1": {
    "detail": {
      "W_RANK,W_CAS": 10
    },
    "reward": {
      "VC_CO": 20
    }
  }
}
```
- `ach_a1` : Achievement Unique ID

	- `detail` : เงื่อนไขการสำเร็จ Achievement
		- `Key` : Player Stat Key 
		
			> หากมีมากกว่า 1 ให้ใช้เครื่องหมาย `,` เป็นตัวคั่น

		- `Value` : จำนวนรวมของ Player Stat Key 
	- `reward` : รางวัลที่จะได้รับเมื่อสำเร็จ Achievement
		- `Key` : Item ID 

			> กรณีที่เป็น Virtual Currency ให้ใช้ **Suffix** เป็น  `VC_`  นำหน้าเสมอ

		- `Value` : จำนวน/ปริมาณ (Integer)
---
