﻿# Player Quest
 - Source : **Player Data**
 - Key : **ไม่ระบุ**
 - Detail : ใช้เป็น data สำหรับอ่านข้อมูลเควสในผู้เล่น
 - Example Value : 
	
```json
{
  "data": {
    "quest_event_2020FEB_0_01": {
      "status": "active",
      "stat": {
        "W_TB": 2
      }
    },
    "quest_event_2020FEB_0_02": {
      "status": "active",
      "stat": {
        "W_TB": 2
      }
    },
    "quest_event_2020FEB_0_03": {
      "status": "active",
      "stat": {
        "H_DAM_TB_E": 39
      }
    },
    "quest_event_2020FEB_0_04": {
      "status": "active",
      "stat": {
        "C_USE_TB_ID_C0593": 0
      }
    },
    "quest_event_2020FEB_0_05": {
      "status": "active",
      "stat": {
        "C_USE_TB_ID_C0593": 0
      }
    },
    "quest_event_2020FEB_0_06": {
      "status": "active",
      "stat": {
        "C_USE_TB_ID_C0593": 0
      }
    },
    "quest_event_2020FEB_0_07": {
      "status": "active",
      "stat": {
        "C_USE_TB_ID_C0594": 0
      }
    },
    "quest_event_2020FEB_0_08": {
      "status": "active",
      "stat": {
        "C_USE_TB_ID_C0594": 0
      }
    },
    "quest_event_2020FEB_0_09": {
      "status": "active",
      "stat": {
        "C_USE_TB_ID_C0594": 0
      }
    },
    "quest_event_2020FEB_0_10": {
      "status": "active",
      "stat": {
        "W_TB_HP_5M": 2
      }
    },
    "quest_event_2020FEB_0_11": {
      "status": "active",
      "stat": {
        "W_TB_HP_10M": 1
      }
    },
    "quest_event_2020FEB_0_12": {
      "status": "active",
      "stat": {
        "LOGIN_DL": 45
      }
    },
    "quest_event_2020FEB_0_13": {
      "status": "active",
      "stat": {
        "LOGIN_DL": 45
      }
    },
    "quest_event_2020FEB_0_14": {
      "status": "active",
      "stat": {
        "LOGIN_DL": 45
      }
    },
    "quest_event_2020FEB_0_15": {
      "status": "active",
      "stat": {
        "LOGIN_DL": 45
      }
    }
  },
  "properties": {
    "pool": {
      "event_quest_2020FEB_0": [
        "quest_event_2020FEB_0_01",
        "quest_event_2020FEB_0_02",
        "quest_event_2020FEB_0_03",
        "quest_event_2020FEB_0_04",
        "quest_event_2020FEB_0_05",
        "quest_event_2020FEB_0_06",
        "quest_event_2020FEB_0_07",
        "quest_event_2020FEB_0_08",
        "quest_event_2020FEB_0_09",
        "quest_event_2020FEB_0_10",
        "quest_event_2020FEB_0_11",
        "quest_event_2020FEB_0_12",
        "quest_event_2020FEB_0_13",
        "quest_event_2020FEB_0_14",
        "quest_event_2020FEB_0_15"
      ]
    },
    "event_key": "event_quest_2020FEB_0"
  }
}
```
- `data` : จัดเก็บข้อมูลของเควสที่ใช้ระบุสถานะและ player stat ที่ถูก stamp ไว้ ณ ตอนที่ได้รับ quest
	- `Key` : quest unique ID
		- `status` : สถานะของเควส ประกอบด้วย `active` และ `clear` (ถูกรับรางวัลแล้ว)
		- `clear_timestamp` : DateTime ที่เควสได้รับสถานะ `clear`
- `properties` : จัดเก็บรายละเอียดเพิ่มเติมของเควสที่ผู้เล่นมีอยู่ สามารถจัดเก็บได้อย่างอิสระตามแต่การออกแบบ data structure ของเควสกลุ่มนั้นๆ
---
