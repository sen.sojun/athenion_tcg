﻿# Quest DB
 - Source : **Title Data**
 - Key : **quest_db**
 - Detail : ใช้เป็น reference data สำหรับ Quest
 - Example Value : 
	
```json
{
  "quest_dw_1": {
    "detail": {
      "W_CAS,W_RANK": 3
    },
    "reward": {
      "VC_CO": 40
    },
    "is_premium": true
  }
}
```
- `quest_dw_1` : Quest Unique ID

	- `detail` : เงื่อนไขการสำเร็จ Quest
		- `Key` : Player Stat Key 
	
			> หากมีมากกว่า 1 ให้ใช้เครื่องหมาย `,` เป็นตัวคั่น

		- `Value` : จำนวนรวมของ Player Stat Key 

	- `reward` : รางวัลที่จะได้รับเมื่อสำเร็จ Quest
		- `Key` : Item ID 

			> กรณีที่เป็น Virtual Currency ให้ใช้ **Suffix** เป็น  `VC_`  นำหน้าเสมอ

		- `Value` : จำนวน/ปริมาณ (Integer)
	- `is_premium` : ตัวระบุความเป็น premium
---
# Quest List
 - Source : **Title Data**
 - Key : **quest_list**
 - Detail : ใช้สำหรับเป็น reference data ในการ Refresh Quest ให้ผู้เล่น
 - Example Value : 
```json
{
  "event": {
    "event_quest_BS19": [
      "quest_event_a1",
      "quest_event_a2",
      "quest_event_a3",
      "quest_event_a4",
      "quest_event_a5",
      "quest_event_a6",
      "quest_event_a7",
      "quest_event_a8",
      "quest_event_a9",
      "quest_event_a10",
      "quest_event_a11",
      "quest_event_a12",
      "quest_event_a13",
      "quest_event_a14",
      "quest_event_a15"
    ]
  },
  "season_pass": {
    "1": {
      "main_daily": {
        "list": [
          "quest_ssp_dq001",
          "quest_ssp_dq002"
        ],
        "refresh_type": "daily"
      },
      "main_weekly": {
        "list": [
          "quest_ssp_wlq001"
        ],
        "refresh_type": "weekly"
      },
      "challenge": {
        "list": [
          "quest_ssp_cq001",
          "quest_ssp_cq002",
          "quest_ssp_cq003",
          "quest_ssp_cq004",
          "quest_ssp_cq005",
          "quest_ssp_cq006",
          "quest_ssp_cq007",
          "quest_ssp_cq008",
          "quest_ssp_cq009",
          "quest_ssp_cq012",
          "quest_ssp_cq013",
          "quest_ssp_cq014",
          "quest_ssp_cq015"
        ]
      },
      "main_week_1": {
        "list": [
          "quest_ssp_pq014",
          "quest_ssp_pq006",
          "quest_ssp_pq015",
          "quest_ssp_wq001",
          "quest_ssp_wq002",
          "quest_ssp_wq013",
          "quest_ssp_wq024"
        ]
      },
      "main_week_2": {
        "list": [
          "quest_ssp_pq001",
          "quest_ssp_pq002",
          "quest_ssp_pq003",
          "quest_ssp_wq003",
          "quest_ssp_wq004",
          "quest_ssp_wq011",
          "quest_ssp_wq005"
        ],
        "valid_date": {
          "start_date": "2019-12-05T02:00:00.000Z"
        }
      },
      "main_week_3": {
        "list": [
          "quest_ssp_pq008",
          "quest_ssp_pq009",
          "quest_ssp_pq019",
          "quest_ssp_wq006",
          "quest_ssp_wq014",
          "quest_ssp_wq022",
          "quest_ssp_wq007"
        ],
        "valid_date": {
          "start_date": "2019-12-12T02:00:00.000Z"
        }
      },
      "main_week_4": {
        "list": [
          "quest_ssp_pq010",
          "quest_ssp_pq011",
          "quest_ssp_pq021",
          "quest_ssp_wq012",
          "quest_ssp_wq016",
          "quest_ssp_wq017",
          "quest_ssp_wq008"
        ],
        "valid_date": {
          "start_date": "2019-12-19T02:00:00.000Z"
        }
      },
      "main_week_5": {
        "list": [
          "quest_ssp_pq012",
          "quest_ssp_pq013",
          "quest_ssp_pq024",
          "quest_ssp_wq023",
          "quest_ssp_wq015",
          "quest_ssp_wq018",
          "quest_ssp_wq010"
        ],
        "valid_date": {
          "start_date": "2019-12-26T02:00:00.000Z"
        }
      },
      "main_week_6": {
        "list": [
          "quest_ssp_pq007",
          "quest_ssp_pq017",
          "quest_ssp_pq018",
          "quest_ssp_wq019",
          "quest_ssp_wq020",
          "quest_ssp_wq021",
          "quest_ssp_wq009"
        ],
        "valid_date": {
          "start_date": "2020-01-02T02:00:00.000Z"
        }
      }
    }
  }
}
```
- `Key` : Quest Group ใช้สำหรับจัดแบ่งกลุ่มของชุด quest สำหรับการ refresh แบ่งออกเป็น
- `event` : ชุด event quest ประกอบด้วย
	- `Key` : key ของ event ที่ต้องการนำ quest ไปใช้
	- `Value` :  list ของ quest ที่ถูกบรรจุอยู่ใน pool
			
- `season_pass` : ชุด season pass quest ประกอบด้วย
	- `Key` : เลขระบุ season pass index
	- `Value` :  ประกอบด้วย object ของหมวดเควส ได้แก่
		- `Key` : ชื่อหมวดของ quest
		- `Value` : ประกอบด้วย
			- `list` : รายการ quest
			- `valid_date` : วันเวลา (UTC) ที่ quest ในรายการจะ valid หากไม่ระบุจะ			ถือว่า valid เสมอโดยจะประกอบด้วย start_date และ end_date 
			- `refresh_type` : ประเภทของการ refresh ข้อมูล quest ได้แก่ daily และ weekly หากไม่ระบุจะถือว่าไม่มีต้องการ refresh
---
