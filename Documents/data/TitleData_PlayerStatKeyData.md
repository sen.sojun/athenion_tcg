﻿# Player Stat Key Data
 - Source : **Title Data**
 - Key : **player_stat_key_data**
 - Detail : ใช้ระบุการจัดเก็บ player stat แยกตาม event key ที่ต้องการ
 - Example Value : 
```json
{
  "key_default": "player_stat",
  "key_list": [
    "player_stat",
    "player_stat_c_use",
    "player_stat_loser",
    "player_stat_tut"
  ],
  "key_rule": {
    "C_USE": "player_stat_c_use",
    "LOSER_": "player_stat_loser",
    "_TUT_": "player_stat_tut"
  }
}
```
- `key_default` : default key สำหรับการเก็บข้อมูล player stat
- `key_list` : list ของ key ที่ใช้สำหรับเก็บข้อมูล player stat
- `key_rule` : กฎการแบ่งเก็บข้อมูล หาก event key ที่จะทำการจัดเก็บนั้นประกอบด้วย `key` ข้อมูล event key ดังกล่าวจะถูกจัดเก็บใน `value` ที่กำหนดไว้ เช่น 
ข้อมูล player stat ที่ถูกส่งมาคือ 
```json
{
  "C_USE_CAS_C0001": 2,
  "W_CAS": 1,
}
```
>`C_USE_CAS_C0001` จะถูกบันทึกลงใน `player_stat_c_use` และ `W_CAS` จะถูกบันทึกลงใน `player_stat` 

---
