﻿# Player Exp
 - Source : **Title Data**
 - Key : **player_exp**
 - Detail : ใช้เป็น reference data สำหรับ Exp ของ Level
 - Example Value : 
	
```json
{
  "1": 0,
  "2": 300,
  "3": 700,
  "4": 1200,
  "5": 1800
}
```
- `Key` : Level
- `Value` : Accumulative Exp value
---

# Player Level Reward
 - Source : **Title Data**
 - Key : **reward_level**
 - Detail : ใช้เป็น reference data สำหรับรางวัล Level
 - Example Value : 
	
```json
{
  "2": {
    "reward": {
      "VC_CO": 100
    }
  },
  "4": {
    "reward": {
      "VC_CO": 100
    }
  }
}
```
- `Key` : Level
	- `reward` : Object contains item data object
		- `Key` : Item ID
		- `Value` : Amount
---
