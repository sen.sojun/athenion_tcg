﻿# Match Reward
 - Source : **Title Data**
 - Key : **match_reward**
 - Detail : ใช้เป็น reference data สำหรับการให้รางวัลหลังเล่นจบ ได้แก่ Exp, Score, Rank
 - Example Value : 
``` json
{
  "match_exp": {
    "W_RANK": 100,
    "W_CAS": 100,
    "L_RANK_MIN": 25,
    "L_CAS_MIN": 25,
    "L_RANK_LP": 25,
    "L_CAS_LP": 25
  },
  "match_exp_faction": {
    "W_RANK_F_FIRE": 100,
    "W_RANK_F_WATER": 100,
    "W_RANK_F_AIR": 100,
    "W_RANK_F_EARTH": 100,
    "W_RANK_F_HOLY": 100,
    "W_RANK_F_DARK": 100,
    "W_CAS_F_FIRE": 100,
    "W_CAS_F_WATER": 100,
    "W_CAS_F_AIR": 100,
    "W_CAS_F_EARTH": 100,
    "W_CAS_F_HOLY": 100,
    "W_CAS_F_DARK": 100
  },
  "match_mmr": {
    "W_CAS": {
      "base": 50,
      "bonus": [
        {
          "delta_min": -1000,
          "delta_max": -601,
          "multiplier": 0.05
        },
        {
          "delta_min": -600,
          "delta_max": -301,
          "multiplier": 0.025
        },
        {
          "delta_min": 301,
          "delta_max": 600,
          "multiplier": 0.025
        },
        {
          "delta_min": 601,
          "delta_max": 1000,
          "multiplier": 0.05
        }
      ]
    },
    "L_CAS": {
      "base": -50,
      "bonus": [
        {
          "delta_min": -1000,
          "delta_max": -601,
          "multiplier": 0.05
        },
        {
          "delta_min": -600,
          "delta_max": -301,
          "multiplier": 0.025
        },
        {
          "delta_min": 301,
          "delta_max": 600,
          "multiplier": 0.025
        },
        {
          "delta_min": 601,
          "delta_max": 1000,
          "multiplier": 0.05
        }
      ]
    }
  },
  "match_rank": {
    "MAX": 88,
    "W_RANK": 1,
    "L_RANK_S_0": 0,
    "L_RANK_S_1": 0,
    "L_RANK_S_2": 0,
    "L_RANK_S_3": 0,
    "L_RANK_S_4": 0,
    "L_RANK_S_5": 0,
    "L_RANK_S_6": 0,
    "L_RANK_S_7": 0,
    "L_RANK_S_8": 0,
    "L_RANK_S_9": 0,
    "L_RANK_S_10": 0,
    "L_RANK_S_11": 0,
    "L_RANK_S_12": 0,
    "L_RANK_S_13": 0,
    "L_RANK_S_14": 0,
    "L_RANK_S_15": 0,
    "L_RANK_S_16": 0,
    "L_RANK_S_17": -1,
    "L_RANK_S_18": -1,
    "L_RANK_S_19": -1,
    "L_RANK_S_20": -1,
    "L_RANK_S_21": 0,
    "L_RANK_S_22": -1,
    "L_RANK_S_23": -1,
    "L_RANK_S_24": -1,
    "L_RANK_S_25": -1,
    "L_RANK_S_26": 0,
    "L_RANK_S_27": -1,
    "L_RANK_S_28": -1,
    "L_RANK_S_29": -1,
    "L_RANK_S_30": -1,
    "L_RANK_S_31": 0,
    "L_RANK_S_32": -1,
    "L_RANK_S_33": -1,
    "L_RANK_S_34": -1,
    "L_RANK_S_35": -1,
    "L_RANK_S_36": -1,
    "L_RANK_S_37": -1,
    "L_RANK_S_38": -1,
    "L_RANK_S_39": -1,
    "L_RANK_S_40": -1,
    "L_RANK_S_41": -1,
    "L_RANK_S_42": -1,
    "L_RANK_S_43": -1,
    "L_RANK_S_44": -1,
    "L_RANK_S_45": -1,
    "L_RANK_S_46": -1,
    "L_RANK_S_47": -1,
    "L_RANK_S_48": -1,
    "L_RANK_S_49": -1,
    "L_RANK_S_50": -1,
    "L_RANK_S_51": -1,
    "L_RANK_S_52": -1,
    "L_RANK_S_53": -1,
    "L_RANK_S_54": -1,
    "L_RANK_S_55": -1,
    "L_RANK_S_56": -1,
    "L_RANK_S_57": -1,
    "L_RANK_S_58": -1,
    "L_RANK_S_59": -1,
    "L_RANK_S_60": -1,
    "L_RANK_S_61": -1,
    "L_RANK_S_62": -1,
    "L_RANK_S_63": -1,
    "L_RANK_S_64": -1,
    "L_RANK_S_65": -1,
    "L_RANK_S_66": -1,
    "L_RANK_S_67": -1,
    "L_RANK_S_68": -1,
    "L_RANK_S_69": -1,
    "L_RANK_S_70": -1,
    "L_RANK_S_71": -1,
    "L_RANK_S_72": -1,
    "L_RANK_S_73": -1,
    "L_RANK_S_74": -1,
    "L_RANK_S_75": -1,
    "L_RANK_S_76": -1,
    "L_RANK_S_77": -1,
    "L_RANK_S_78": -1,
    "L_RANK_S_79": -1,
    "L_RANK_S_80": -1,
    "L_RANK_S_81": -1,
    "L_RANK_S_82": -1,
    "L_RANK_S_83": -1,
    "L_RANK_S_84": -1,
    "L_RANK_S_85": -1,
    "L_RANK_S_86": -1,
    "L_RANK_S_87": -1,
    "L_RANK_S_88": 0
  }
}
```
- `match_exp` : Exp ที่ผู้เล่นจะได้รับหลังเล่นจบ
	> 	`W_RANK` : เมื่อชนะในโหมด Rank
	> 	`W_CAS` : เมื่อชนะในโหมด Casual
	> 	`L_RANK_MIN` : เมื่อแพ้ในโหมด Rank เมื่อผ่านไปอย่างน้อย 3 turn
	> 	`L_CAS_MIN` : เมื่อแพ้ในโหมด Casual เมื่อผ่านไปอย่างน้อย 3 turn
	> 	`L_RANK_LP` : เมื่อแพ้ในโหมด Rank โดยการเสีย Life Point ทั้งหมด
	> 	`L_CAS_LP` : เมื่อแพ้ในโหมด Casual โดยการเสีย Life Point ทั้งหมด

- `match_exp_faction` : Faction Exp ที่ผู้เล่นจะได้รับหลังเล่นจบ 

- `match_mmr` : MMR ที่ผู้เล่นจะได้รับหลังเล่นจบ 
	- `W_CAS` : นำมาคำนวณเมื่อเล่นชนะในโหมด casual
		- `base` : ค่า MMR พื้นฐานที่จะได้รับ
		- `bonus` : ค่า MMR bonus ประกอบด้วยรายการของ object ที่ใช้บ่งบอกเงื่อนไขการคำนวณค่า bonus เช่น
			``` json
			{
	          "delta_min": -1000,
	          "delta_max": -601,
	          "multiplier": 0.05 
	        }     
	        ```
	        - `delta_min` : ค่า opponent mmr - player mmr ขั้นต่ำ
	        - `delta_max` : ค่า opponent mmr - player mmr สูงสุด
	        - `multiplier` : ค่าสำหรับนำไปคูณกับ delta mmr ในการคำนวณหา bonus mmr

- `match_rank` : Season Rank ที่ผู้เล่นจะได้ รับ/เสีย หลังเล่นจบ 
	> 	`MAX` : ค่า Rank Index สูงสุด
	> 	`W_RANK` : เมื่อชนะในโหมด Rank
	> `L_RANK_S_` : เมื่อแพ้ในโหมด Rank ตามด้วย Rank Index ปัจจุบัน
---
