﻿# Season Rank Reset
 - Source : **Title Data**
 - Key : **season_rank_reset**
 - Detail : ใช้เป็น reference data สำหรับกำหนดการปรับ Rank เมื่อจบ Season
 - Example Value : 
```json
{
  "0_20": 0,
  "21_41": 16,
  "42_999": 36
}
```
- `Key` : ช่วง Rank Index
- `Value` : Rank Index หลังถูก reset

# Season Rank Reward
 - Source : **Title Data**
 - Key : **season_rank_reward**
 - Detail : ใช้เป็น reference data สำหรับให้รางวัล Season Rank
 - Example Value : 
```json
{
  "1": {
    "0_3": {
      "reward": {}
    },
    "4_7": {
      "reward": {
        "VC_FR": 110
      }
    },
    "8_11": {
      "reward": {
        "VC_FR": 140
      }
    },
    "12_15": {
      "reward": {
        "VC_FR": 170
      }
    },
    "16_20": {
      "reward": {
        "VC_FR": 200
      }
    },
    "21_25": {
      "reward": {
        "VC_FR": 250
      }
    },
    "26_30": {
      "reward": {
        "VC_FR": 300
      }
    },
    "31_35": {
      "reward": {
        "VC_FR": 350
      }
    },
    "36_41": {
      "reward": {
        "VC_RF": 100,
        "VC_FR": 400
      }
    },
    "42_47": {
      "reward": {
        "VC_RF": 150,
        "VC_FR": 500
      }
    },
    "48_53": {
      "reward": {
        "CONT_WOE": 1,
        "VC_RF": 200,
        "VC_FR": 600
      }
    },
    "54_59": {
      "reward": {
        "CONT_WOE": 1,
        "VC_RF": 250,
        "VC_FR": 700
      }
    },
    "60_66": {
      "reward": {
        "CONT_WOE": 2,
        "VC_RF": 350,
        "VC_FR": 800
      }
    },
    "67_73": {
      "reward": {
        "CONT_WOE": 2,
        "VC_RF": 500,
        "VC_FR": 900
      }
    },
    "74_80": {
      "reward": {
        "TT2NDBORN_1": 1,
        "CONT_WOE": 3,
        "VC_RF": 700,
        "VC_FR": 1000
      }
    },
    "81_87": {
      "reward": {
        "TT1STBORN_1": 1,
        "CONT_WOE": 4,
        "VC_RF": 1000,
        "VC_FR": 1200,
        "CB0011": 1
      }
    }
  }
}
```
- `Key` : เลข Season Index
	- `Key` : ช่วง Rank Index
		- `reward` : รางวัล
			- `Key` : Item ID
				> กรณีที่เป็น Virtual Currency ให้ใช้ **Suffix** เป็น  `VC_`  นำหน้าเสมอ
				
			- `Value` : จำนวน/ปริมาณ (Integer)
---
