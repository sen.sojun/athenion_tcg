﻿# Season Pass Data
 - Source : **PlayerData (Title) -> ReadOnlyData**
 - Key : **season_pass_data**
 - Detail : ใช้บันทึกข้อมูลของผู้เล่นเกี่ยวกับ season pass
 - Example Value : 
```json
{
  "season_index": "2",
  "score": 460,
  "premium_trans": {
    "2": "2019-11-21T08:18:03.452Z"
  }
}
```
- `season_index` : เลข season index ปัจจุบัน
- `score` : แต้ม season pass score ของ season pass ปัจจุบัน
- `premium_trans` : รายการการซื้อ premium season pass
	- `Key` : เลข season index ที่ทำการซื้อ
	- `Value` : DateTime ที่ทำการซื้อ

# Season Pass Quest
 - Source : **PlayerData (Title) -> ReadOnlyData**
 - Key : **season_pass_quest**
 - Detail : บันทึกข้อมูลการรับ season pass quest ของผู้เล่น
 - Example Value : 
```json
{
  "data": {
    "quest_ssp_dq001": {
      "status": "active",
      "stat": {
        "W_CAS": 6,
        "W_RANK": 4
      }
    },
    "quest_ssp_dq002": {
      "status": "active",
      "stat": {
        "W_TB": 2
      }
    },
    "quest_ssp_cq001": {
      "status": "active",
      "stat": {
        "W_CAS_M_C_TAUNT": 0,
        "W_RANK_M_C_TAUNT": 0
      }
    },
    "quest_ssp_cq002": {
      "status": "clear",
      "stat": {
        "W_CAS_M_C_GY_29M": -100,
        "W_RANK_M_C_GY_29M": 0
      },
      "clear_timestamp": "2020-01-29T03:27:40.208Z"
    },
    "quest_ssp_cq003": {
      "status": "active",
      "stat": {
        "W_CAS_M_C_BERSERKING_1M": 0,
        "W_RANK_M_C_BERSERKING_1M": 0
      }
    },
    "quest_ssp_cq004": {
      "status": "active",
      "stat": {
        "W_CAS_M_SL_LOCK_3M": 0,
        "W_RANK_M_SL_LOCK_3M": 0
      }
    },
    "quest_ssp_cq005": {
      "status": "active",
      "stat": {
        "W_CAS_M_C_GY_BACKSTAB_9M": 0,
        "W_RANK_M_C_GY_BACKSTAB_9M": 0
      }
    },
    "quest_ssp_cq006": {
      "status": "active",
      "stat": {
        "W_CAS_M_C_AURA_S3_1M": 0,
        "W_RANK_M_C_AURA_S3_1M": 0
      }
    },
    "quest_ssp_cq007": {
      "status": "active",
      "stat": {
        "W_CAS_M_C_LINK_4M": 0,
        "W_RANK_M_C_LINK_4M": 0
      }
    },
    "quest_ssp_cq008": {
      "status": "clear",
      "stat": {
        "W_CAS_M_C_9M": -100,
        "W_RANK_M_C_9M": 0
      },
      "clear_timestamp": "2020-01-29T03:30:48.103Z"
    },
    "quest_ssp_cq009": {
      "status": "active",
      "stat": {
        "W_CAS_M_C_LESS_E": 0,
        "W_RANK_M_C_LESS_E": 0
      }
    },
    "quest_ssp_cq012": {
      "status": "active",
      "stat": {
        "C_DES_CAS_E_STREAK_5M": 0,
        "C_DES_RANK_E_STREAK_5M": 0
      }
    },
    "quest_ssp_cq013": {
      "status": "active",
      "stat": {
        "W_CAS_M_DECK_UNIQ": 0,
        "W_RANK_M_DECK_UNIQ": 0
      }
    },
    "quest_ssp_cq014": {
      "status": "active",
      "stat": {
        "W_CAS_M_DECK_U_NEUTRAL": 0,
        "W_RANK_M_DECK_U_NEUTRAL": 0
      }
    },
    "quest_ssp_cq015": {
      "status": "active",
      "stat": {
        "W_CAS_HP_1E": 0,
        "W_RANK_HP_1E": 0
      }
    },
    "quest_ssp_pq014": {
      "status": "active",
      "stat": {
        "C_USE_CAS_R_LEGEND": 0,
        "C_USE_RANK_R_LEGEND": 0
      }
    },
    "quest_ssp_pq006": {
      "status": "active",
      "stat": {
        "C_USE_CAS_A_AWAKEN": 5,
        "C_USE_RANK_A_AWAKEN": 1
      }
    },
    "quest_ssp_pq015": {
      "status": "active",
      "stat": {
        "C_DES_CAS_E": 18,
        "C_DES_RANK_E": 0
      }
    },
    "quest_ssp_wq001": {
      "status": "clear",
      "stat": {
        "P_CAS": -10,
        "P_RANK": 0
      },
      "clear_timestamp": "2020-01-29T03:30:48.103Z"
    },
    "quest_ssp_wq002": {
      "status": "clear",
      "stat": {
        "W_CAS": -10,
        "W_RANK": 0
      },
      "clear_timestamp": "2020-01-29T03:30:48.103Z"
    },
    "quest_ssp_wq013": {
      "status": "active",
      "stat": {
        "C_DES_CAS_E": 18,
        "C_DES_RANK_E": 0
      }
    },
    "quest_ssp_wq024": {
      "status": "active",
      "stat": {
        "EMO_MATCH_CAS_GREETING": 0,
        "EMO_MATCH_RANK_GREETING": 0
      }
    },
    "quest_ssp_wlq001": {
      "status": "active",
      "stat": {
        "W_FM": 8,
        "P_FM_ER_NORMAL": 2
      }
    },
    "quest_ssp_pq001": {
      "status": "active",
      "stat": {
        "C_USE_CAS_S_1": 35,
        "C_USE_RANK_S_1": 0
      }
    },
    "quest_ssp_pq002": {
      "status": "active",
      "stat": {
        "C_USE_CAS_S_2": 7,
        "C_USE_RANK_S_2": 1
      }
    },
    "quest_ssp_pq003": {
      "status": "active",
      "stat": {
        "C_USE_CAS_S_3": 19,
        "C_USE_RANK_S_3": 0
      }
    },
    "quest_ssp_wq003": {
      "status": "active",
      "stat": {
        "P_TB": 3
      }
    },
    "quest_ssp_wq004": {
      "status": "active",
      "stat": {
        "W_TB": 2
      }
    },
    "quest_ssp_wq011": {
      "status": "active",
      "stat": {
        "H_DAM_CAS_E": 70,
        "H_DAM_RANK_E": 88
      }
    },
    "quest_ssp_wq005": {
      "status": "active",
      "stat": {
        "W_CAS_M_C_GY_20L": 1,
        "W_RANK_M_C_GY_20L": 4
      }
    },
    "quest_ssp_pq008": {
      "status": "active",
      "stat": {
        "W_CAS_F_FIRE": 0,
        "W_RANK_F_FIRE": 0,
        "W_CAS_F_WATER": 0,
        "W_RANK_F_WATER": 0
      }
    },
    "quest_ssp_pq009": {
      "status": "active",
      "stat": {
        "W_CAS_F_EARTH": 0,
        "W_RANK_F_EARTH": 0,
        "W_CAS_F_AIR": 1,
        "W_RANK_F_AIR": 4
      }
    },
    "quest_ssp_pq019": {
      "status": "active",
      "stat": {
        "C_USE_CAS_A_LASTWISH": 0,
        "C_USE_RANK_A_LASTWISH": 0
      }
    },
    "quest_ssp_wq006": {
      "status": "active",
      "stat": {
        "W_RANK": 4,
        "W_TB": 2
      }
    },
    "quest_ssp_wq014": {
      "status": "active",
      "stat": {
        "C_ACT_CAS_M_BERSERK": 0,
        "C_ACT_RANK_M_BERSERK": 0
      }
    },
    "quest_ssp_wq022": {
      "status": "active",
      "stat": {
        "C_ACT_CAS_M_BUFFFREEZE": 0,
        "C_ACT_RANK_M_BUFFFREEZE": 0
      }
    },
    "quest_ssp_wq007": {
      "status": "active",
      "stat": {
        "W_CAS_M_C_6M": 0,
        "W_RANK_M_C_6M": 1
      }
    },
    "quest_ssp_pq010": {
      "status": "active",
      "stat": {
        "W_CAS_F_DARK": 0,
        "W_RANK_F_DARK": 0,
        "W_CAS_F_HOLY": 0,
        "W_RANK_F_HOLY": 0
      }
    },
    "quest_ssp_pq011": {
      "status": "active",
      "stat": {
        "W_CAS_F_AIR": 1,
        "W_RANK_F_AIR": 4,
        "W_CAS_F_FIRE": 0,
        "W_RANK_F_FIRE": 0
      }
    },
    "quest_ssp_pq021": {
      "status": "active",
      "stat": {
        "C_USE_CAS_A_LOCK": 0,
        "C_USE_RANK_A_LOCK": 0
      }
    },
    "quest_ssp_wq012": {
      "status": "active",
      "stat": {
        "H_DAM_CAS_E_7M": 3,
        "H_DAM_RANK_E_7M": 4
      }
    },
    "quest_ssp_wq016": {
      "status": "active",
      "stat": {
        "C_ACT_CAS_M_MOVE": 3,
        "C_ACT_RANK_M_MOVE": 3
      }
    },
    "quest_ssp_wq017": {
      "status": "active",
      "stat": {
        "C_ACT_CAS_M_SLOTLOCK": 0,
        "C_ACT_RANK_M_SLOTLOCK": 0
      }
    },
    "quest_ssp_wq008": {
      "status": "active",
      "stat": {
        "W_CAS_HP_5L": 0,
        "W_RANK_HP_5L": 0
      }
    }
  },
  "properties": {
    "main_daily": {
      "update_timestamp": "2020-02-03T02:42:21.042Z",
      "list": [
        "quest_ssp_dq001",
        "quest_ssp_dq002"
      ]
    },
    "challenge": {
      "update_timestamp": "2019-11-22T18:45:42.502Z",
      "list": [
        "quest_ssp_cq001",
        "quest_ssp_cq002",
        "quest_ssp_cq003",
        "quest_ssp_cq004",
        "quest_ssp_cq005",
        "quest_ssp_cq006",
        "quest_ssp_cq007",
        "quest_ssp_cq008",
        "quest_ssp_cq009",
        "quest_ssp_cq012",
        "quest_ssp_cq013",
        "quest_ssp_cq014",
        "quest_ssp_cq015"
      ]
    },
    "main_week_1": {
      "update_timestamp": "2019-11-22T18:45:42.502Z",
      "list": [
        "quest_ssp_pq014",
        "quest_ssp_pq006",
        "quest_ssp_pq015",
        "quest_ssp_wq001",
        "quest_ssp_wq002",
        "quest_ssp_wq013",
        "quest_ssp_wq024"
      ]
    },
    "main_weekly": {
      "update_timestamp": "2020-01-30T03:21:26.040Z",
      "list": [
        "quest_ssp_wlq001"
      ]
    },
    "main_week_2": {
      "update_timestamp": "2020-01-20T02:56:49.976Z",
      "list": [
        "quest_ssp_pq001",
        "quest_ssp_pq002",
        "quest_ssp_pq003",
        "quest_ssp_wq003",
        "quest_ssp_wq004",
        "quest_ssp_wq011",
        "quest_ssp_wq005"
      ]
    },
    "main_week_3": {
      "update_timestamp": "2020-01-20T04:43:00.086Z",
      "list": [
        "quest_ssp_pq008",
        "quest_ssp_pq009",
        "quest_ssp_pq019",
        "quest_ssp_wq006",
        "quest_ssp_wq014",
        "quest_ssp_wq022",
        "quest_ssp_wq007"
      ]
    },
    "main_week_4": {
      "update_timestamp": "2020-01-20T04:43:00.086Z",
      "list": [
        "quest_ssp_pq010",
        "quest_ssp_pq011",
        "quest_ssp_pq021",
        "quest_ssp_wq012",
        "quest_ssp_wq016",
        "quest_ssp_wq017",
        "quest_ssp_wq008"
      ]
    }
  }
}
```
- `data` : มีโครงสร้างเช่นเดียวกับที่อธิบายใน [PlayerData_Quest](PlayerData_Quest)
- `properties` : ข้อมูลรายละเอียดของการรับเควส
	- `Key` : unique ID ของชุด season pass quest
		- `update_timestamp` : DateTime ที่เควสชุดนี้ได้รับการอัพเดต
		- `list` : รายการ quest unique ID ของชุดเควสนี้

# Season Pass Tier Reward Claimed
 - Source : **PlayerData (Title) -> ReadOnlyData**
 - Key : **season_pass_tier_reward_claimed**
 - Detail : บันทึกประวัติการรับรางวัล season pass tier reward
 - Example Value : 
```json
{
  "1": {
    "fr1": "2019-11-22T18:48:27.142Z",
    "fr2": "2019-11-22T18:48:27.142Z",
    "pr1": "2019-11-22T18:48:27.142Z",
    "pr2": "2019-11-22T18:48:27.142Z",
    "fr3": "2019-11-22T18:59:33.477Z",
    "pr3": "2019-11-22T18:59:33.477Z",
    "fr4": "2019-11-22T18:59:44.836Z",
    "pr4": "2019-11-22T18:59:44.836Z",
    "fr5": "2019-11-23T03:53:16.510Z",
    "pr5": "2019-11-23T03:53:16.510Z",
    "fr6": "2019-12-19T07:30:33.440Z",
    "pr6": "2019-12-19T07:30:33.440Z",
    "fr7": "2019-12-19T07:39:24.065Z",
    "pr7": "2019-12-19T07:39:24.065Z"
  },
  "2": {
    "fr1": "2019-12-26T11:12:09.993Z",
    "fr2": "2019-12-26T11:12:09.993Z",
    "fr3": "2020-02-03T07:11:46.148Z",
    "fr4": "2020-02-03T07:11:46.148Z",
    "fr5": "2020-02-03T07:11:46.148Z"
  }
}
```
- `Key` : เลข season index
	- `Key` : unique key ของรางวัล season pass tier
	- `Value` : DateTime ที่ทำการรับรางวัล season pass tier
---
