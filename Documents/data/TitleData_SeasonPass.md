﻿# Season Pass DB (Battle Pass)
 - Source : **Title Data**
 - Key : **season_pass_db**
 - Detail : ใช้เป็น reference data สำหรับกำหนดช่วงเวลาเปิด/ปิดของ Season Pass (Battle Pass)
 - Example Value : 
```json
{
  "1": {
    "start_date": "2000-01-01T00:00:00.000Z",
    "end_date": "2020-01-09T02:00:00.000Z"
  },
  "2": {
    "start_date": "2020-01-09T04:00:00.000Z",
    "end_date": "2020-02-20T02:00:00.000Z"
  }
}
```
- `Key` : เลข Season Index
	- `start_date` : วันเวลาเริ่มต้น season
	- `end_date` : วันเวลาสิ้นสุด season

# Season Pass Price
 - Source : **Title Data**
 - Key : **season_pass_price**
 - Detail : ใช้เป็น reference data สำหรับกำหนดราคา premium และราคา tier หรือ battle pass level ของ Season Pass
 - Example Value : 
```json
{
  "premium": [
    {
      "price": {
        "VC_DI": 680
      },
      "start_date": "2000-01-01T00:00:00.000Z",
      "end_date": "2100-01-01T00:00:00.000Z"
    }
  ],
  "tier": [
    {
      "price": {
        "VC_DI": 95
      },
      "start_date": "2000-01-01T00:00:00.000Z",
      "end_date": "2100-01-01T00:00:00.000Z"
    }
  ]
}
```
- `premium` : ข้อมูลราคาการซื้อ premium
	- `price` : ข้อมูลราคา
		- `Key` : ประเภทของ virtual currency
		- `Value` : ปริมาณของ virtual currency
	- `start_date` : วันเวลาเริ่มต้นของราคา
	- `end_date` : วันเวลาสิ้นสุดของราคา

- `tier` : ข้อมูลราคาการซื้อ tier (battle pass level)
	- `price` : ข้อมูลราคา
		- `Key` : ประเภทของ virtual currency
		- `Value` : ปริมาณของ virtual currency
	- `start_date` : วันเวลาเริ่มต้นของราคา
	- `end_date` : วันเวลาสิ้นสุดของราคา

# Season Pass Tier
 - Source : **Title Data**
 - Key : **season_pass_tier**
 - Detail : ใช้เป็น reference data สำหรับกำหนด progression ของ season pass tier ตาม season pass score ของผู้เล่น
 - Example Value : 
```json
{
  "1": 0,
  "2": 100,
  "3": 200,
  "4": 300,
  "5": 400,
  "6": 500,
  "7": 600,
  "8": 700,
  "9": 800,
  "10": 900
}
```
- `Key` : ระดับ season pass tier
- `Value` : ปริมาณ season pass score ของผู้เล่น (accumulative season pass score)

# Season Pass Tier Reward
 - Source : **Title Data**
 - Key : **season_pass_tier_reward**
 - Detail : ใช้เป็น reference data สำหรับกำหนดรางวัลที่ผู้เล่นจะได้รับในแต่ละ tier
 - Example Value : 
```json
{
  "fr60": {
     "tier": 60,
     "reward": {
       "CB0049": 1,
       "CONT_STAR": 3
     }
   },
   "pr1": {
     "tier": 1,
     "reward": {
       "HUD0009": 1,
       "CONT_STAR": 1
     },
     "is_premium": true
   }
}
```
- `Key` : unique key ของรางวัล season pass tier
	- `tier` : ระดับ tier ที่กำหนด
	- `reward` : รางวัลที่ได้รับ
		- `Key` : ItemID ของราวัล
		- `Value` : ปริมาณของรางวัล
	- `is_premium` : boolean ที่ใช้ระบุว่ารางวัลนี้จำกัดสำหรับผู้เล่นที่เป็น premium ใน season pass หรือไม่ หากเว้นว่างไว้จะมีค่า default เป็น false

---
