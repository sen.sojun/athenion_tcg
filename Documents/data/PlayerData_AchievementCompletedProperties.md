﻿# Achievement Completed Properties
- Source : **PlayerData (Title) -> ReadOnlyData**
- Key : **achievement_completed_properties**
- Detail : ใช้เพื่อเก็บข้อมูล Achievement ที่สำเร็จและรับรางวัลแล้วของผู้เล่น
- Example Value : 
```json
{
  "ach_d1": "2019-07-23T01:02:00.739Z",
  "ach_d2": "2019-07-23T01:05:02.314Z"
}
```
- `Key` : Achievement Unique ID
- `Value` : Server Timestamp (UTC) ที่ Achievement ถูกเช็คสำเร็จและให้รางวัล
---
