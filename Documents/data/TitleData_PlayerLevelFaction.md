﻿# Player Exp Faction
 - Source : **Title Data**
 - Key : **player_exp_faction**
 - Detail : ใช้เป็น reference data สำหรับ Exp ของ Faction Level
 - Example Value : 
	
```json
{
  "1": 0,
  "2": 120,
  "3": 270,
  "4": 520,
  "5": 920,
  "6": 1470,
  "7": 2150,
  "8": 3000,
  "9": 4000,
  "10": 5100
}
```
- `Key` : Level
- `Value` : Accumulative Exp value
---
# Player Level Faction Reward
 - Source : **Title Data**
 - Key : **reward_level_faction**
 - Detail : ใช้เป็น reference data สำหรับรางวัล Faction Level
 - Example Value : 
	
```json
{
  "FIRE": {
    "2": {
      "reward": {
        "C0576": "3"
      }
    },
    "3": {
      "reward": {
        "C0569": "3"
      }
    }
  },
  "WATER": {
    "2": {
      "reward": {
        "C0564": "3"
      }
    },
    "3": {
      "reward": {
        "C0573": "3"
      }
    }
  }
}
```
- `Key` : Faction ID (FIRE, WATER, AIR, EARTH, HOLY, DARK)
	- `Key` : Faction Level
		- `reward` : Object contains item data object
			- `Key` : Item ID
			- `Value` : Amount
---
