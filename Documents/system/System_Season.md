[Main](../../README.md).

Update season  **DO NOT REMOVE SEASON 0**

---
**SETTING TITLE DATA IN PLAYFAB**

Add or Change this JSON to Playfab's Title Data.
>key : season_db
value :
```
{
	"current_season": 1,
	"data": {
		"0": {
		"start_date": "2019-05-01T02:00:00.000Z",
		"end_date": "2019-05-02T02:00:00.000Z",
		"reward_date": "2019-05-01T12:00:00.000Z"
		},
		"1": {
		"start_date": "2019-06-15T02:00:00.000Z",
		"end_date": "2019-08-07T02:00:00.000Z",
		"reward_date": "2019-08-07T12:00:00.000Z"
		},
		"2": {
		"start_date": "2019-08-08T02:00:00.000Z",
		"end_date": "2019-09-04T02:00:00.000Z",
		"reward_date": "2019-09-04T12:00:00.000Z"
		}
	}
}
```
reward_date always equal `12:00:00.000Z` and date must equal to `end_date`.

[Setting RewardSeasonRank](../data/TitleData_SeasonRank.md).
---
**SETTING SCHEDULED TASK IN PLAYFAB**

**Do this for a new Playfab's server only.**

1. Go to Automation tab.
2. Click on Scheduled Task.
3. Add new Scheduel

STATUS -> Active : ture
SCHEDULE -> Recurring 
Segment -> All Players

Action
- Excecute Cloud  Script
- Cloud Script -> ClaimSeasonReward