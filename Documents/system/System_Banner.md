
**SETTING TITLE DATA IN PLAYFAB**

Add or Change this JSON to Playfab's Title Data.

>key : banner_db
value :
```js
[
  {
    "release_date": "2019-07-25T02:00:00.000Z",
    "end_date": "2077-08-08T02:00:00.000Z",
    "image_path": "Images/BannerPopup/kt_patch_note_20190725",
    "function": "ShowKTPlayDeeplink",
    "custom_data": "KTRL.1563439461"
  }
]
```
`image_path` จะอยู่ใน AssetBundle เท่านั้น
`function` คือ code ที่อยู่ใน MainMenuManager.cs โดยระบบจะใช้ `SendMessage` ของ Unity
`custom_data` จะเป็น parameter ที่ส่งใน `SendMessage` 

ตัวอย่างของฟังชั่น ที่ต้องรับ `custom_data` ใน MainMenuManager.cs
```c#
public void ShowKTPlayDeeplink(string link)
{
	KTPlayManager.Instance.ShowDeepLink(link);
}
```
ตามตัวอย่างข้างบน เมื่อคลิก Banner ระบบจะ `SendMessage` "ShowKTPlayDeeplink" มี param เป็น "KTRL.1563439461"
  

---