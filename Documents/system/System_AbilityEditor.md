**Ability Editor**
---


**Installation**
---
- เปิด folder `(root ของ .git)\ATHENION_UNITY\ExtraPackage\`
แล้วลงไฟล์ที่ชื่อว่า `AbilityEditor.unitypackage`

---


**How to use**


![abilityEditor01](../src/abilityEditor01.png)


คลิกขวา Create > AbilityGraphEditor.
เมื่อคลิกแล้ว จะสร้างไฟล์ Xnode และหน้าต่างเริ่มต้น โดยมี Node เริ่มต้นคือ [Condition Node](./AbilityEditor/Node.md)
และ [DisplayNode](./AbilityEditor/Node.md)

เราสามารถสร้าง Node โดยการ `คลิกขวา` พื้นที่ว่าง แล้วจะมีรายการ Node มากมาย และถูกแบ่ง ตามประเภทต่างๆ ได้แก่

`NodeType { Effect, Minion, MinionBuff, Slot, SlotBuff, Player, ETC, Integer, Boolean, String }`


**Special Node For AbilityEditor**
---
[Special Node](./AbilityEditor/Node.md)


**Example**
---

ตัวอย่างของ Ability "มอบ +1 HP แก่ยูนิตข้างเคียงของคุณทั้งหมด"

เริ่มต้น เราสร้าง Node Effect ซึ่ง Effect ใน Ability Editor นี้ จะแบ่งเป็นหมวดหมู่เพื่อให้เข้าใจง่าย จากดังตัวอย่าง เป็น Effect ที่ใช้กับ Minion จึงจะอยู่ในหมวดของ EffectWithMinion
ซึ่งเป็นเกี่ยวกับการเพิ่ม HP จึงสร้างNode `DoBoostMinionHP` ดังภาพ 


![abilityEditor02](../src/abilityEditor02.png)


เมื่อสร้างNode `DoBoostMinionHP` จะมี Node ที่ต้องการ 2 ชนิด คือ Node ประเภท Minion และ Node ประเภท Int
ผู้ใช้ต้องสร้าง Node ประเภท Minion ต่อกันดังภาพ จะได้ประโยคที่ว่า `ยูนิตข้างเคียงของคุณทั้งหมด`


![abilityEditor03](../src/abilityEditor03.png)


และต่อด้วย Node Int พร้อมใส่ค่า 1 ลงไป จะได้ `มอบ +1 HP`

สุดท้ายแล้ว ผู้ใช้ต้องกดปุ่ม GetAbilityText จะได้ผลลัพธ์ออกมาดังนี้ `DoBoostMinionHP(MinionFilterMine(MinionAdjacent8(MinionMe)),1)`
