﻿# Card Data & Ability Data
**Card Data** is defined from `CSV-Format Databases` consist of
- `CardDB.csv` : defines card ID, stats and several visual effects for an individual cards.
- `CardAbilityDB.csv` : defines card ability ID, text-based effect data and visual effect for each effect.

**CardDB** and **CardAbilityDB** are ported into DB objects in C# format. Then these data will be combined together to create `CardData` object. The game uses `CardData` as the RAW data for `BattleCardData`, the model for the card that is used in all entire systems.

> BattleCardData is the base class of MinionData

`BattleCardData` contains `CardAbilityData` that used to prepare and activate the ability which consist of the list of `EffectData`.

## Card Data
Contains RAW and serialized data from **CardDB**. The most values are derived from `CardDB.csv` such as
- Card ID
	- FullID (C0001_VARIANT_VARIANT:FOIL)
	- BaseID (C0001)
	- VariantID (VARIANT_VARIANT)
	- IsFoil (Boolean TRUE => :FOIL)
- Element ID
	- FIRE
	- WATER
	- AIR
	- EARTH
	- HOLY
	- DARK
- Series Key
	- Basic
	- WOE
	- GM
- Feedback Key
- Rarity
	- Common
	- Rare
	- Epic
	- Legend
- Type
	- Minion
	- Minion_NotInDeck
- Power (Soul/Spirit)
- ATK
- HP
- Armor (Battle Armor)
- Direction (N,NE,E,SE,S,SW,W,NW)
- Ability ID List (List of Ability ID)
- Spawn Effect Key
- Attack Effect Key
- Death Effect Key
- Info Script Key

## Card Ability Data
There are several types of the parameter in the **CardAbility System**, as shown in **DataParams** sheet in KaramuchoDB such as
- Integer
- Boolean
- String
- MinionList
- SlotList
- PlayerList
- CardDirection
- Effect (or EffectData)
- MinionBuff
- SlotBuff

As mentioned above, `CardAbilityData` contains a list of `EffectData` and after the ability has been triggered via the `EventTrigger` system (as shown in column `Trigger` in `CardAbilityDB.csv`), the `EffectData` will be prepared and activated and deserialize the defined parameters. 

As defined in **DataParams** sheet, some parameters have several input parameters. The system will parse every single parameter and translate it into the C# data type. However, the parameters that have input parameters must be correctly defined as the type of each parameter index.

---
