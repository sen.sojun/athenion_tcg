
**SETTING SHOP IN PLAYFAB**


***

การเพิ่มของใน Shop จะต้องสร้าง Item ให้เสร็จก่อนเสมอจึงจะเริ่มเพิ่มสินค้าใน Shop

**เพิ่มของในร้านค้า**
- เข้าในที่ Economy -> Stores
- คลิกร้านค้าที่สนใจ
- คลิก Add To Store โดยใส่ ItemID ที่ต้องการจะขาย
- ใส่ custom data โดยขึ้นอยู่กับ โครงสร้างของแต่ละค้า โดยจะอธิบายทีละร้านค้าดังนี้

**Promotion Store**

หากสินค้าเป็นแบบซื้อได้จำกัดจำนวนครั้ง Item ต้องเป็นแบบ **Durable** เท่านั้น
โดยสามารถ กำหนด MaxCount เป็นจำนวนครั้งได้
"Tag" จะมีอยู่ 6 แบบ คือ New, Best, Special, Hot, Saving, Great และเพิ่มเติมได้โดยเพิ่ม Localize TAG_ แล้วตามด้วยคำที่ต้องการ
"TagColor" มี "ORANGE" กับ "RED" เท่านั้น
```js
{
  "Option": {
    "MaxCount": 1,
    "ReleaseDate": "2019-11-01T00:00:00Z",
    "EndDate": "2077-02-14T00:00:00Z",
    "Tag": "BEST",
    "TagColor": "RED"
  },
  "ItemDetail": [
    {
      "ItemID": "CONT_WOE",
      "Amount": 12
    },
    {
      "ItemID": "CONT_WOE_LEGEND",
      "Amount": 1
    }
  ]
}
```

**Booster Pack Shop**
ร้าน Booster Pack จะพิเศษกว่าร้านค้าอื่น โดยมีวิธีการจัดการดังนี้

***1. การเพิ่ม Pack ใหม่***
1. สร้าง ร้านค้าใหม่ โดยสร้างชื่อให้เหมาะสม เช่น ST_BOOSTER_+ รหัส
2. เพิ่มชื่อร้านค้า Pack ใหม่ใน Title Data ที่ Key "store_data" จะมี key ชื่อ BoosterPack

```js
{
...,
  "BoosterPack": [
    "ST_BOOSTER_STAR",
    "ST_BOOSTER_WOE",
    "ST_BOOSTER_GM",
    "ST_BOOSTER_+รหัสที่ตั้งใหม่"
  ]
 ... ,
}
```
3. เพิ่ม Custom Data ในร้านค้า หากร้านค้าไม่มี ItemList ก็ไม่ต้องใส่ข้อมูลก้ได้ ที่สำคัญคือต้องระบุวัน ReleaseDate ให้ดี
```js
// ตัวอย่างข้อมูล
{
  "ShopName": "ITEM_NAME_ST_BOOSTER_STAR",
  "PackID": "CONT_STAR",
  "ReleaseDate": "2019-02-14T00:00:00Z",
  "Rate": {
    "Common": "73.10%",
    "Rare": "21.30%",
    "Epic": "4.30%",
    "Legend": "1.30%"
  },
  "ItemList": {
    "item id 01": "เรท(float)ไม่ต้องใส่ %",
    "item id 02": "เรท(float)ไม่ต้องใส่ %",
    "item id 03": "เรท(float)ไม่ต้องใส่ %"
  },
  "CardList": {
    "C0002": "เรท(float)ไม่ต้องใส่ %",
    "C0021": "0.97",
    "C0098": "2.52"
  }
}
```
4.เพิ่ม Content ในร้านค้า ซึ่ง Card pack ควรเป็น Container ดูตัวอย่างจาก Card Pack ที่มีได้เลย
custom data ของสินค้าในร้านค้า ให้ใส่ IsPack : true ดังตัวอย่าง
```js
{
  "ItemDetail": [
    {
      "ItemID": "CONT_STAR",
      "Amount": 5
    }
  ],
  "Option": {
    "IsPack": true
  }
}
```

***Diamond Shop***
ปัจจุบันจะมีสินค้าอยู่ 2 ประเภท คือ Limited กับ ปกติ
โดยให้เซ็ตข้อมูล CustomData ดังนี้
{"Option":{"IsLimited":true,"MaxCount":1},"ItemDetail":[{"ItemID":"VC_DI","Amount":11000}]}

ส่วนสินค้าอื่น
{"Option":{},"ItemDetail":[{"ItemID":"VC_DI","Amount":950}]}

แต่ปกติแล้ว Diamond Shop จะเป็นร้านเดียวที่มีร้าน VIP Subscription อยู่ ซึ่งต้องอ้างอิงกับร้านค้า 
***Subscription VIP***

***Cosmetic Shop***

ผู้ใช้จะต้องกำหนด custom data ดังนี้
```js
{
  "ItemDetail": [
    {
      "ItemID": "xxxxxx",
      "Amount": 1
    }
  ],
  "Option": {
    "MaxCount": 1,
    "ReleaseDate": "2019-10-14T02:00:00Z"
  },
  "ShopItemType": "Cosmetic",
  "CosmeticType": "Skin"
}
```
โดยผู้ใช้ต้องกำหนด CosmeticType ทุกครั้งเมื่อมีสินค้าเข้ามาในร้านนี้ โดยจะแบ่งประเภทดังนี้คือ
```c#
public enum CosmeticType { SKIN, CARDBACK, AVATAR, TITLE, CONSOLE }
```
และ ShopItemType จะต้องเป็น "Cosmetic"

***

***การเพิ่มสินค้าด้วยเงินจริง***

1. ตั้งสินค้าด้วยชื่อที่มีคำว่า com.zerobit.athenion + ***itemID ตัวเล็ก***
2. เพิ่มสินค้าใน Store ต่างๆดังนี้

*การเพิ่มข้อมูลใน Google Play Store*
1. เพิ่มข้อมูลใน [https://play.google.com/](https://play.google.com/) -> Athenion -> store presence (ซ้ายมือ) -> In-app products
2. เพิ่มของโดยใช้ itemID เดียวกันที่มี com.zerobit.athenion ด้วย
3. ใส่ราคาให้ถูกต้อง พร้อมเลือกเป็น Active

*การเพิ่มข้อมูลใน App Store*
1. เพิ่มข้อมูลใน [https://appstoreconnect.apple.com/](https://appstoreconnect.apple.com/)-> In-App Purchases
2. กดปุ่ม + ข้างบนรายการสินค้า -> เลือก Consumable
3. ใส่ข้อมูลให้ครบ
4. Review Information ให้ capture รูปภาพขนาด 640x920 px พร้อมใส่ข้อมูล Review Notes เพื่อให้คนรีวิวเข้าใจว่ามันคือสิ่งนี้นะ ที่ใน shop
5. ใส่ราคา แล้ว Save
6. เวลา submit to store ให้กด ready for submit ด้วย 


***