**Special Node only in AbilityEditor**
---

*DisplayNode*

เป็น Node ที่เอาไว้ แสดงผลลัพธ์ ของ Node ที่อยู่ถัดไปทั้งหมด ซึ่งสามารถต่อกับกี่ Node ก็ได้ ดังตัวอย่าง
 
 

![abilityEditor05](../../src/abilityEditor05.png)

*ConditionNode* 

เป็น Node สำหรับการใส่ Condition ใน Ability โดยสามารถ ต่อกับ Node ที่เป็นประเภท Boolean เท่านั้น ซึ่งสามารถต่อกับกี่ Node ก็ได้ดังตัวอย่าง


![abilityEditor06](../../src/abilityEditor06.png)


*Note*


เป็น กระดานสำหรับจดบันทึกหรือฝากค่าไว้ให้ผู้ใช้อ่าน


*Custom Node*

เป็น Node ที่ใช้สำหรับ Node ที่จะเป็น Node อะไรก็ได้ และต่อกับ Node ประเภทใดก็ได้ เพื่อใช้ในการพิมพ์แทนการสร้าง Node จำนวนมาก พร้อมทั้งยังสามารถใช้สำหรับ Node ใหม่ๆ ที่Editor ยังไม่รองรับก็ได้

โดย ถ้าไม่มี Node ใดต่อท้าย  Custom Node จะ return Text ที่อยู่ใน Node ทั้งหมด ดังภาพ

![abilityEditor06](../../src/abilityEditor07.png)


ถ้า Custom Node มี Node มาต่อท้าย ผลลัพธ์ที่ออกจาก Custom Node จะออกมาเป็น  `Text( ผลลัพธ์ของ Node ที่ต่อท้าย )` 

ถ้า Custom Node มี Node มาต่อท้าย 3 จำนวน ผลลัพธ์จะเป็น `Text(ผลลัพธ์ของ Node ที่ต่อท้ายที่ 1 ,ผลลัพธ์ของ Node ที่ต่อท้ายที่ 2 ,ผลลัพธ์ของ Node ที่ต่อท้ายที่ 3 )`


*Compile Node*


เป็น Node ที่ไว้สำหรับ Check ความถูกต้องของ ผลลัพธ์ ของ Node ที่ต่อท้าย ซึ่งจะมีกี่ Node ก็ได้ 

โดย Node จะรองรับเฉพาะ Node ที่มีใน AbilityEditor เท่านั้น