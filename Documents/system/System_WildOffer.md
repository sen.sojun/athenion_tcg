
**Wild Offer**
 
 Title Data 
```js
{
	"WO001": {
	    "item_list": [
	      "com.zerobit.athenion.bund_wild_fr4000"
	    ],
	    "duration": 172800,
	    "max": 1,
	    "chance": 30,
	    "priority": 1
  }
}
```
WO001 คือรหัสของ wild offer โดยผู้ใช้จะต้องอ้างอิงกับชีท [https://docs.google.com/spreadsheets/d/1nGP7tnHodEbVCVekEYHbYHBQ1YLAY14X-1Z0N0lwIFc/edit#gid=867617905](https://docs.google.com/spreadsheets/d/1nGP7tnHodEbVCVekEYHbYHBQ1YLAY14X-1Z0N0lwIFc/edit#gid=867617905)

**chance** คือโอกาสการออก
**duration** หน่วยเป็นวินาที
**item_list** จะเป็นรายการที่สามารถเสนอได้ ขึ้นอยู่กับ wild offer นั้นๆ

logic การเกิดต่างๆ จะอยู่ใน  Client ทั้งหมด

**การเพิ่มสินค้า**
ใน store จะมีร้านค้าชื่อว่า **ST_WILD_OFFER** โดยผู้ใช้จะต้องเพิ่มสินค้าในร้านค้าก่อน จึงค่อยเพิ่มใน Title Data