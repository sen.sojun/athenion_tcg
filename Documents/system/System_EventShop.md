[Main](../../README.md).

**SETTING TITLE DATA IN PLAYFAB**

Add or Change this JSON to Playfab's Title Data.

>key : store_event
value :
```js
[
  {
    "bundle_id": "AV_E20200206",
    "currency": "EC_2020FEB_0",
    "release_date": "2019-12-12T00:00:00.000Z",
    "end_date": "2020-12-09T01:50:00.000Z",
    "item_detail": [
      {
        "item_id": "AV_E20200206",
        "amount": 1
      }
    ],
    "price": 25,
    "max_count": 1,
    "size": "S",
    "image": "2020FEB_AV_E20200206",
    "localize_key": "ITEM_NAME_AV_E20200206"
  },
  {
    "bundle_id": "BUND_2020FEB_VC_RF_150",
    "currency": "EC_2020FEB_0",
    "release_date": "2019-12-12T00:00:00.000Z",
    "end_date": "2020-12-09T01:50:00.000Z",
    "item_detail": [
      {
        "item_id": "BUND_2020FEB_VC_RF_150",
        "amount": 150
      }
    ],
    "price": 25,
    "max_count": 1,
    "size": "S",
    "image": "BUND_2020FEB_VC_RF_150",
    "localize_key": "ITEM_NAME_BUND_2020FEB_VC_RF_150"
  }
]
```
event store หาก item ไม่ใช้ประเภท **BUND** สามารถใส่ itemID ในช่องของ "bundle_id" ได้เลย แต่ต้องระบุ ItemDetail ด้วย ดังตังอย่าง object ที่ 1
หาก item ที่ขายเป็นประเภท Bundle ก็สามารถใส่ itemID ใน "bundle_id" ได้เช่นเดียวกัน และระบุ ItemDetail ด้วย

ผู้ใช้สามารถ กำหนด image ได้หากรูปอยู่ใน bundle event.
สามารถกำหนด localize_key ได้

ผู้ใช้ต้องกรอด "size" เสมอโดยมี "S" และ "L" โดยเบื้องต้นกำหนด "S" ไปก่อนได้ เพื่อรองรับ feature custom size ในอนาคต


---
