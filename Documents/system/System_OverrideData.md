[Main](../../README.md).
 
**Manage Hot fix with Override System.**
---

เราสามารถ แก้ข้อมูล CardDB, CardAbilityDB, LocalizeDB และ Effect DB ได้ทันที (ดีเลย์ 5 นาทีเพราะ TitleData)
โดยมีชื่อ key ดังนี้  
  1. override_card_db // แก้ข้อมูล CardDB  
  2. override_card_ability_db // แก้ข้อมูล CardAbilityDB  
  3. override_card_effect_db // แก้ข้อมูล CardEffectDB  
  4. override_localize_db // แก้ข้อมูล LocalizeDB  
  
---
**วิธีทำ**

- แก้ข้อมูลในไฟล์ CSV ให้ถูกต้อง  
  
- ปรับ NotePad ++ ให้สามารถมองเห็นทุกตัวอักษร  
  
![ปรับ NotePad ++ ให้สามารถมองเห็นทุกตัวอักษร](../src/showAllSymbol.png)  
  
- Copy ข้อมูล csv ลงใน Notepad ++ ดังตัวอย่างด้านล่าง  
  
![การคัดลอกข้อมูล csv ลงใน Notepad++](../src/copyAllValue.png)  
  
- กด CTR+F แล้วแก้คำของเอกสารทั้งหมดจาก \r\n เป็น \\n (ผลลัพธ์จะเหลือ string แถวเดียว)  
  
![การคัดลอกข้อมูล csv ลงใน Notepad++](../src/renameParameter.png)  
  
- copy ลงใน TitleData ที่ถูกต้อง  
- ใส่ \n หน้าสุดของ TitleData  (ที่มี \n หน้าสุดเพราะว่าการอ่าน csv ของเกมเรามันข้ามบันทัดแรกเพราะเป็น Header)  
- Save and Run (ควรทดสอบจาก Server Development ก่อนทุกครั้ง!)  
---