﻿## ระบบ Asset Bundle Path

เราสามารถกำหนด Asset Bundle Path  จากบน Playfab ได้ โดย  Asset Bundle Path
จะถูกเขียนกำหนดไว้ที่ Title Data ที่ Key "asset_bundle_path"

โดยมีโครงสร้างข้อมูล JSON เป็นดังนี้

 - Athenion Version คือ รุ่นของตัวเกม
		- Production 
		- Development
		
- Platform คือ Device ของตัวเกม 
		- Android
		- iPhonePlayer
		
- Built Date คือ วันที่ Build ตัวเกม
		- เช่น 20190725
		
- "release_date" คือ วันที่อนุญาตให้ Asset Bundle นี้โหลดได้
- "file_path_list" คือ รายการ File ของ Asset Bundle 

#### ตัวอย่าง JSON
```
    "Production" : {
		"IPhonePlayer": {
			"20190725": [
				{	
					// Asset Bundle Version
					"release_date": "2019-07-25T00:00:00.000Z",
					"file_path_list": [
			            "IPhonePlayer/20190725/database_bundle",
			            "IPhonePlayer/20190725/localize_bundle",
			            "IPhonePlayer/20190725/artbannerimages_bundle",
			            "IPhonePlayer/20190725/artcardimages_bundle",
			            "IPhonePlayer/20190725/arttokenimages_bundle",
			            "IPhonePlayer/20190725/frameimages_bundle",
			            "IPhonePlayer/20190725/bgm_bundle",
			            "IPhonePlayer/20190725/sfx_bundle",
			            "IPhonePlayer/20190725/voice_bundle",
			            "IPhonePlayer/20190725/itemimages_bundle",
			            "IPhonePlayer/20190725/shopimages_bundle",
			            "IPhonePlayer/20190725/avatar_bundle",
			            "IPhonePlayer/20190725/cardback_bundle",
			            "IPhonePlayer/20190725/hero_bundle",
			            "IPhonePlayer/20190725/hud_bundle",
			            "IPhonePlayer/20190725/image_bundle",
			            "IPhonePlayer/20190725/title_bundle",
			            "IPhonePlayer/20190725/event_bundle"
					]
				}
			]
		}
    }
```
เราสามารถเพิ่ม version ของ Asset Bundle เข้าไปในรายการได้ด้วย 
โดยระบบจะคำนวนหา Asset Bundle ที่มี *release_date* ใหม่ที่สุด ที่ผ่านมาแล้ว

#### ตัวอย่าง JSON
```
    "Production" : {
		"IPhonePlayer": {
			"20190725": [
				{	
					// Asset Bundle Version 1
					"release_date": "2019-07-25T00:00:00.000Z",
					"file_path_list": [
			            "IPhonePlayer/20190725/database_bundle",
			            "IPhonePlayer/20190725/localize_bundle",
						...
						...
			            "IPhonePlayer/20190725/event_bundle"
					]
				},
				{	
					// Asset Bundle Version 2
					"release_date": "2019-07-28T00:00:00.000Z",
					"file_path_list": [
			            "IPhonePlayer/20190728/database_bundle",
			            "IPhonePlayer/20190728/localize_bundle",
						...
						...
			            "IPhonePlayer/20190728/event_bundle"
					]
				}
			]
		}
    }
```
จากตัวอย่างจะเห็นว่า ในรุ่น Production 20190725 ใส่ Asset Bundle ไว้ 2 รุ่น 
ซึ่งถ้าเราเปิดตัวเกม ในวันที่ 2019-07-26 จะได้ Asset Bundle ของวันที่ 25 
แต่ถ้าเราเปิดตัวเกม ในวันที่ 2019-07-29 จะได้ Asset Bundle ของวันที่ 28 
