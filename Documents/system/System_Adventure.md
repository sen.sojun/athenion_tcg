
**SETTING TITLE DATA IN PLAYFAB**

Add or Change this JSON to Playfab's Title Data.

>key : adventure_db_<chapter_id\>   
value :  
```js
{
"chapter_id": "AT",
	"stages": {
		"AT_001": {
			"show_sprite_at_stage_select": true,
			"required_prev_finished_stage_ids": [],
			"player_use_custom_deck": false,
			"match_player_data_configs": [
				{
					"use_player_name": true,
					"deck_id": "DECK_ADVENTURE_AT_1_PLAYER",
					"deck_list": ["card_id", "card_id", ...],
					"hero_id": "H0001",
					"element_type": "FIRE",
					"hp": 20
				},
				{
					"use_player_name": false,
					"deck_id": "DECK_ADVENTURE_AT_1_ENEMY",
					"deck_list": ["card_id", "card_id", ...],
					"hero_id": "HA0001",
					"element_type": "NEUTRAL",
					"hp": 20
				}
		    ],
			"rewards": {
			    "CONT_WOE": 1
			}
		},
		"AT_002" {
			...
		},
		...
    }
}
```
`chapter_id` เป็น enum ใน "AdventureChapterID" ฝั่ง client
และหากต้องการขึ้น chapter ใหม่ ให้สร้าง Title Data Key ใหม่แยกจาก chapter ก่อนๆ จากนั้นไปเพิ่ม key นั้นใน DataManager.cs -> string[] "adventureKeyList" ด้วย  
`stages` -> `id` จะถูกตั้งชื่อตาม format <chapter_id>_<stage_id> เช่น "AT_001" และจะถูกสร้างเป็น enum ใน "AdventureStageID" ฝั่ง client  
`deck_id` track จากใน DeckDB_Adventure  
`deck_list` track จากใน DeckDB_Adventure  
`show_sprite_at_stage_select` หากเป็น true จะต้องสร้างรูปและตั้งชื่อตรงกับ stage_id ใน "AssetBundles\Images\Adventure\Stages"  

**ใน client จะต้องตั้งชื่อ GameController ด้วย format <stage_id>AdventureController (เพื่อให้ script ส่วนที่ load controller ทางเข้า battle scene ค้นหาเจอโดยอัตโนมัติ)
และ FixedBot ด้วย format <stage_id>AdventureBot**

---