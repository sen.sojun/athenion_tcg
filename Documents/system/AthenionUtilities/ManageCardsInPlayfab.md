
[Main](../../../README.md).

---
**Add Cards in Playfab**
---

1. เปิดเอกสาร KaramuchoNonAssetDB https://docs.google.com/spreadsheets/d/19Pko4ryUGZ_FDdDnDGmuTfkY1wxuOwUYB6mmY9rcZpU/edit#gid=987430850
2. เปิด Tab "CT_CARD"
3. เพิ่มข้อมูลในเอกสาร "อย่างถูกต้อง!!! และ ครบถ้วน" (ถ้าพลาด คุณจอมฆ่าแน่นอน)
    - column "Tags" ["star","epic","craftable"] element แรก คือ series ของการ์ด, สองคือ ความแรร์ และ สุดท้ายคือ craft ได้หรือไม่ ซึ่งถ้าคราฟไม่ได้ ไม่ต้องใส่ "craftable"
    - column "CustomData" เป็นรายละเอียดของราคา

```js
{
  "normal": {
    "recycle": {
      "VC_S1": 50,
      "VC_RF": 250
    },
    "craft": {
      "VC_S1": 160,
      "VC_RF": 800
    }
  },
  "card_stat_change": {
    "recycle": {
      "VC_S1": 160,
      "VC_RF": 800
    },
    "craft": {
      "VC_S1": 160,
      "VC_RF": 800
    }
  }
}
```

normal คือ ราคาปกติในการ ย่อย/ สร้าง
card_stat_change คือ ราคาตอนที่สามารถย่อยได้เต็มราคานั้นเอง


4. ในแถบ menu bar ของ Spread Sheet -> กด Playfab -> Export Sheet as Catalog JSON ->Copy ข้อมูลทั้งหมด สร้างไฟล์ .json เก็บไว้ (ต่อไปนี้ขอนิยามไฟล์นี้ว่า CT_CARD)
5. เปิด Unity กด Window -> PlayfabAthenion -> CardCatalogEditor
    - เมื่อเปิดเข้ามา ให้นำไฟล์ ใส่ตามสิ่งที่ระบบต้องการ
    - Playfab Catalog คือ ไฟล์ JSON ของ Catalog 
    - CardCatalog คือ CT_CARD

6. กด Merge
7. Copy Text ทั้งหมดใส่ในไฟล์ .json (แล้วแต่จะตั้งชื่อเลย)
8. Upload Catalog ด้วยไฟล์ที่ Merge มา (ควรทำใน server development ก่อน)