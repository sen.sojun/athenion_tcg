[Main](../../../README.md).

**Manage Card's Droptable**
---
**1. Update Droptable**
--- 

update droptable จะมีอยู่ 2 วิธีใหญ่ๆด้วยกันคือ Update จาก Playfab เลย หรือ ใช้ AthenionEditor

**Update จาก Playfab**
กด Economy -> CT_Main (Catalog) -> Droptable -> New DropTable/ เลือกDropTable ที่อยากแก้ไข -> ใส่รายละเอียดและ Weight ในการออก


**Update จาก AthenionEditor**
** Update Catalog ก่อนที่จะทำ Drop table ทุกครั้ง **
- download json ของ droptable จาก playfab ก่อน ( Economy -> Catalog ที่สร้างมา -> Droptable )
- เปิด Unity Project 
- เปิดหน้าต่าง Window -> PlayfabAthenion -> Droptable  
- เลือก DropTableEditor จะพบหน้าต่าง โดยใส่ข้อมูลดังนี้
	 - CardDB_CSV : โดยผู้ใช้ต้องลากไฟล์ CSV ของ CardDB
	 - Table ID : ชื่อของ Table ID ที่กรอกไว้ใน Playfab
	 - Series Key : ชื่อแพ็คการ์ดโดยอ้างอิงจาก CardDB เช่น WOE, Star เป็นต้น
	 - IsFoil : กดสำหรับสร้างTable ของการ์ด Foil
	 - Rarity : Common, Rare, Epic, Legend
- เมื่อกรอกข้อมูลแล้วกดปุ่ม Preview แล้ว Copy ทับ Table ในไฟล์ droptable json ที่download มา
- ทำให้ครบทุกๆ Rarity รวมถึง Foil และ ไม่ Foil 
- upload file json ลองใน server development เพื่อทดสอบ
- ถ้าเป็น server จริงให้รอการ update ในวันที่สามารถ update patch ได้เท่านั้น


**2. Update Card's Drop Rate**
--- 
- เปิด Unity Project 
- เปิดหน้าต่าง Window -> PlayfabAthenion -> Droptable  
- เลือก CardPackReviewEditor จะพบหน้าต่าง โดยใส่ข้อมูลดังนี้
	- CardDB_CSV : โดยผู้ใช้ต้องลากไฟล์ CSV ของ CardDB
	- SeriesKey : ชื่อแพ็คการ์ดโดยอ้างอิงจาก CardDB เช่น WOE, Star เป็นต้น 
	- CommonRate : Rate Common รายใบ
	- RareRate : Rate Rare รายใบ
	- EpicRate : Rate Epic รายใบ
	- LegendRate : Rate Legend รายใบ
- Copy ทั้ง object { } ไปใส่ในข้อมูล custom data ของร้านค้า booster pack ตาม series ที่สนใจ
- วางข้อมูลทับข้อมูลของ "CardList": { ... } 
- Save.
