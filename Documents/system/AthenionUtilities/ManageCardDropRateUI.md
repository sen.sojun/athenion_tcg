[Main](../../../README.md).

**Manage Card's Droptable UI**
--- 

# เอกสารเกี่ยวข้อง
1. [Star Card DropRate](https://docs.google.com/spreadsheets/d/16Rainv5_iGsfXWac7DLSi42Xc-IRxjE3pSBMt4L9oB4/edit#gid=1726003637)
2. [Other Pack DropRate](https://docs.google.com/spreadsheets/d/16Rainv5_iGsfXWac7DLSi42Xc-IRxjE3pSBMt4L9oB4/edit#gid=979733188)

*เวลาแก้ rate รายใบ อย่าลืมเพิ่มจำนวนใน sheet excel ด้วย (ตรงช่อง rate จะมีสูตรคำนวนอยู่)

---
**วิธี Update DropRate UI**
---

# การ Generate CardDropRate สำหรับแสดงผลใน UI
1. เปิด Unity
2. Window -> PlayfabAthenion -> DropTable -> CardPackReviewEditor
3. ใส่ CardDB ในช่อง CardDB_CSV
4. กรอก Series Key (อ้างอิงจาก CardDB) เช่น Star, WOE เป็นต้น
5. กรอก Rate รายใบให้ถูกต้อง (ทศนิยม 2 ตำแหน่ง)
6. กดปุ่ม Review

# การ ใส่ข้อมูลลงใน Playfab
1. Playfab -> Economy -> เลือก Catalog -> Store
2. เลือกร้านค้า การ์ดแพ็ค ที่ต้องการจะ Update Droprate UI
3. เมื่อเข้าไปในร้านค้า จะมี *CustomData*
4. นำข้อมูลที่ได้จาก ขั้นตอน **การ Generate CardDropRate สำหรับแสดงผลใน UI** มาใส่ใน CustomData ตรงส่วนของ "CardList" ให้ถูกต้อง

```js
{
  "ShopName": "ITEM_NAME_ST_BOOSTER_STAR",
  "PackID": "CONT_STAR",
  "ReleaseDate": "2019-02-14T00:00:00Z",
  "Rate": {
    "Common": "73.10%",
    "Rare": "21.30%",
    "Epic": "4.30%",
    "Legend": "1.30%"
  },
  "ItemList": {
    "BUND_FR_5": "7.31",
    "BUND_FR_10": "7.31",
    "BUND_RF_5": "7.31",
    "BUND_RF_10": "7.31", 
	.
	.
	.
  },
  "CardList": {
    "C0002": "0.93",
    "C0021": "0.93",
    "C0098": "1.70",
    "C0109": "0.93",
    "C0110": "1.70",
    "C0118": "1.70",
    "C0125": "0.93", 
	.
	.
	.
  }
}
```

กด Save




