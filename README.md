# ATHENION PROJECT

The Tactical CCG. This document are inform about this project include **Source Code** and **How to manage data in Playfab**.

---

# Documents

---
### Athenion's documents.

1. [Athenion's Class Diagrams](https://drive.google.com/file/d/1OSF0AJ_4a3eCuVp1DwYnLsuFfeP2Kvk6/view?usp=sharing)
2. [GDD](https://docs.google.com/document/d/1OhI-EGqeYhaWJ_t45dxUOeh7rHlkYeTA2n-pxD5kxPQ/edit)
3. [Athenion XD](https://xd.adobe.com/view/8a678546-b8b6-4a39-72fc-d8d61ed56ba7-fc56/)
4. [Dev's Design Document](https://docs.google.com/spreadsheets/d/1B1IQiRadbtSK70H2uiQgLj79L2nNxKZjfCgscEWD9Nc/edit#gid=1541180769)
5. [Shop's Document](https://docs.google.com/spreadsheets/d/1G01RptscabLPjBV-IchKzVPZGsa4dsueHi43TuZ9J-o/edit#gid=963107045)
 
---

### Manage Data

1. [Manage Achievement Completed Properties.](Documents/data/PlayerData_AchievementCompletedProperties.md)
2. [Manage PlayerStat.](Documents/data/PlayerData_PlayerStat.md) 
3. [Manage Quest.](Documents/data/PlayerData_Quest.md)
4. [Manage SeasonPass.](Documents/data/TitleData_SeasonPass.md)  
5. [Manage AchievementDB.](Documents/data/TitleData_AchievementDB.md) 
6. [Manage CardPriceOverride.](Documents/data/TitleData_CardPriceOverride.md) 
7. [Manage MatchReward.](Documents/data/TitleData_MatchReward.md) 
8. [Manage PlayerStatKeyData](Documents/data/TitleData_PlayerStatKeyData.md) 
9. [Manage Quest.](Documents/data/TitleData_Quest.md) 
10. [Manage RewardSeasonRank.](Documents/data/TitleData_SeasonRank.md) 
11. [Manage Card's DropTable](Documents/system/AthenionUtilities/ManageCardDroptable.md)
12. [Update Card DropRate for UI InGame](Documents/system/AthenionUtilities/ManageCardDropRateUI.md)
13. [Add new cards to Playfab](Documents/system/AthenionUtilities/ManageCardsInPlayfab.md)
14. [Hot Fix Override Data](Documents/system/System_OverrideData.md)


---
### Manage System

1. [Manage AssetBundle.](Documents/system/System_AssetBundlePath.md) 
2. [Manage Event Shop.](Documents/system/System_EventShop.md) 
3. [Manage Season System.](Documents/system/System_Season.md) 
4. [Manage Banner System.](Documents/system/System_Banner.md) 
5. [Manage Shop System.](Documents/system/System_Shop.md)
6. [Manage Quest System.](Documents/system/System_Quest.md)
7. [Manage Adventure System.](Documents/system/System_Adventure.md)
8. [Manage WildOffer System.](Documents/system/System_WildOffer.md)

### Utilities

1. [Ability Editor.](Documents/system/System_AbilityEditor.md)
2. [AssetBundle CardImage.](Documents/AssetBundle_CardImage.md)
